/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.customtabs.Tabbed
 *  org.netbeans.swing.tabcontrol.customtabs.Tabbed$Accessor
 *  org.netbeans.swing.tabcontrol.event.TabActionEvent
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view.ui;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLayeredPane;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.actions.ActionUtils;
import org.netbeans.core.windows.actions.MaximizeWindowAction;
import org.netbeans.core.windows.view.Controller;
import org.netbeans.core.windows.view.ModeView;
import org.netbeans.core.windows.view.ui.ModeComponent;
import org.netbeans.core.windows.view.ui.slides.SlideBarActionEvent;
import org.netbeans.core.windows.view.ui.slides.SlideOperation;
import org.netbeans.core.windows.view.ui.slides.SlideOperationFactory;
import org.netbeans.swing.tabcontrol.customtabs.Tabbed;
import org.netbeans.swing.tabcontrol.event.TabActionEvent;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public final class TabbedHandler
implements ChangeListener,
ActionListener {
    private final ModeView modeView;
    private final Tabbed tabbed;
    private final int kind;
    private boolean ignoreChange = false;
    private static ActivationManager activationManager = null;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public TabbedHandler(ModeView modeView, int kind, Tabbed tbd) {
        this.modeView = modeView;
        this.kind = kind;
        Class<TabbedHandler> class_ = TabbedHandler.class;
        synchronized (TabbedHandler.class) {
            if (activationManager == null) {
                activationManager = new ActivationManager();
                Toolkit.getDefaultToolkit().addAWTEventListener(activationManager, 16);
            }
            // ** MonitorExit[var4_4] (shouldn't be in output)
            this.tabbed = tbd;
            this.tabbed.addChangeListener((ChangeListener)this);
            this.tabbed.addActionListener((ActionListener)this);
            ((Container)this.tabbed.getComponent()).setFocusCycleRoot(true);
            return;
        }
    }

    public void requestAttention(TopComponent tc) {
        this.tabbed.requestAttention(tc);
    }

    public void cancelRequestAttention(TopComponent tc) {
        this.tabbed.cancelRequestAttention(tc);
    }

    public void setAttentionHighlight(TopComponent tc, boolean highlight) {
        this.tabbed.setAttentionHighlight(tc, highlight);
    }

    public void makeBusy(TopComponent tc, boolean busy) {
        this.tabbed.makeBusy(tc, busy);
    }

    public Component getComponent() {
        return this.tabbed.getComponent();
    }

    public void addTopComponent(TopComponent tc, int kind) {
        this.addTCIntoTab(tc, kind);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setTopComponents(TopComponent[] tcs, TopComponent selected) {
        this.ignoreChange = true;
        try {
            this.tabbed.setTopComponents(tcs, selected);
        }
        finally {
            this.ignoreChange = false;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void addTCIntoTab(TopComponent tc, int kind) {
        if (TabbedHandler.containsTC(this.tabbed, tc)) {
            return;
        }
        Image icon = tc.getIcon();
        try {
            this.ignoreChange = true;
            String title = WindowManagerImpl.getInstance().getTopComponentDisplayName(tc);
            if (title == null) {
                title = "";
            }
            this.tabbed.addTopComponent(title, (Icon)(icon == null ? null : new ImageIcon(icon)), tc, tc.getToolTipText());
        }
        finally {
            this.ignoreChange = false;
        }
    }

    private static boolean containsTC(Tabbed tabbed, TopComponent tc) {
        return tabbed.indexOf((Component)tc) != -1;
    }

    public void removeTopComponent(TopComponent tc) {
        this.removeTCFromTab(tc);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void removeTCFromTab(TopComponent tc) {
        if (this.tabbed.indexOf((Component)tc) != -1) {
            try {
                this.ignoreChange = true;
                this.tabbed.removeComponent((Component)tc);
            }
            finally {
                this.ignoreChange = false;
            }
            tc.getAccessibleContext().setAccessibleParent(null);
        }
    }

    public void topComponentIconChanged(TopComponent tc) {
        int index = this.tabbed.indexOf((Component)tc);
        if (index < 0) {
            return;
        }
        Image icon = tc.getIcon();
        if (null != icon) {
            this.tabbed.setIconAt(index, (Icon)new ImageIcon(tc.getIcon()));
        } else {
            Logger.getLogger(TabbedHandler.class.getName()).log(Level.INFO, "TopComponent has no icon: " + (Object)tc);
        }
    }

    public void topComponentNameChanged(TopComponent tc, int kind) {
        int index = this.tabbed.indexOf((Component)tc);
        if (index < 0) {
            return;
        }
        String title = WindowManagerImpl.getInstance().getTopComponentDisplayName(tc);
        if (title == null) {
            title = "";
        }
        this.tabbed.setTitleAt(index, title);
    }

    public void topComponentToolTipChanged(TopComponent tc) {
        int index = this.tabbed.indexOf((Component)tc);
        if (index < 0) {
            return;
        }
        this.tabbed.setToolTipTextAt(index, tc.getToolTipText());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setSelectedTopComponent(TopComponent tc) {
        if (tc == this.getSelectedTopComponent()) {
            return;
        }
        if (tc == null && !this.isNullSelectionAllowed()) {
            return;
        }
        if (this.tabbed.indexOf((Component)tc) >= 0 || this.isNullSelectionAllowed() && tc == null) {
            try {
                this.ignoreChange = true;
                this.tabbed.setSelectedComponent((Component)tc);
            }
            finally {
                this.ignoreChange = false;
            }
        }
    }

    private boolean isNullSelectionAllowed() {
        return this.kind == 2;
    }

    public TopComponent getSelectedTopComponent() {
        return this.tabbed.getSelectedTopComponent();
    }

    public TopComponent[] getTopComponents() {
        return this.tabbed.getTopComponents();
    }

    public void setActive(boolean active) {
        this.tabbed.setActive(active);
    }

    @Override
    public void stateChanged(ChangeEvent evt) {
        if (this.ignoreChange || evt.getSource() != this.tabbed) {
            return;
        }
        TopComponent selected = this.tabbed.getSelectedTopComponent();
        this.modeView.getController().userSelectedTab(this.modeView, selected);
    }

    public Shape getIndicationForLocation(Point location, TopComponent startingTransfer, Point startingPoint, boolean attachingPossible) {
        return this.tabbed.getIndicationForLocation(location, startingTransfer, startingPoint, attachingPossible);
    }

    public Object getConstraintForLocation(Point location, boolean attachingPossible) {
        return this.tabbed.getConstraintForLocation(location, attachingPossible);
    }

    public Rectangle getTabBounds(int tabIndex) {
        return this.tabbed.getTabBounds(tabIndex);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e instanceof TabActionEvent) {
            TabActionEvent tae = (TabActionEvent)e;
            String cmd = tae.getActionCommand();
            if ("select".equals(cmd)) {
                return;
            }
            tae.consume();
            if ("close" == cmd) {
                TopComponent tc = this.tabbed.getTopComponentAt(tae.getTabIndex());
                if (tc != null && this.modeView != null) {
                    this.modeView.getController().userClosedTopComponent(this.modeView, tc);
                } else {
                    Logger.getLogger(TabbedHandler.class.getName()).warning("TopComponent to be closed is null at index " + tae.getTabIndex());
                }
            } else if ("popup" == cmd) {
                TabbedHandler.handlePopupMenuShowing(tae.getMouseEvent(), tae.getTabIndex());
            } else if ("maximize" == cmd) {
                TabbedHandler.handleMaximization(tae);
            } else if ("closeAll" == cmd) {
                ActionUtils.closeAllDocuments(true);
            } else if ("closeAllButThis" == cmd) {
                TopComponent tc = this.tabbed.getTopComponentAt(tae.getTabIndex());
                ActionUtils.closeAllExcept(tc, true);
            } else if ("enableAutoHide".equals(cmd)) {
                if (Switches.isTopComponentSlidingEnabled() && this.tabbed.getComponent().isShowing()) {
                    TopComponent tc = this.tabbed.getTopComponentAt(tae.getTabIndex());
                    Component tabbedComp = this.tabbed.getComponent();
                    String side = WindowManagerImpl.getInstance().guessSlideSide(tc);
                    SlideOperation operation = SlideOperationFactory.createSlideIntoEdge(tabbedComp, side, true);
                    operation.setStartBounds(new Rectangle(tabbedComp.getLocationOnScreen(), tabbedComp.getSize()));
                    operation.prepareEffect();
                    this.modeView.getController().userEnabledAutoHide(this.modeView, tc);
                    this.modeView.getController().userTriggeredSlideIntoEdge(this.modeView, operation);
                }
            } else if ("minimizeGroup".equals(cmd)) {
                if (Switches.isModeSlidingEnabled()) {
                    TopComponent tc = this.tabbed.getTopComponentAt(0);
                    WindowManagerImpl wm = WindowManagerImpl.getInstance();
                    ModeImpl mode = (ModeImpl)wm.findMode(tc);
                    if (null != mode) {
                        wm.userMinimizedMode(mode);
                    }
                }
            } else if ("restoreGroup".equals(cmd)) {
                String nameOfModeToRestore = tae.getGroupName();
                if (null != nameOfModeToRestore) {
                    TopComponent tc = this.tabbed.getTopComponentAt(0);
                    WindowManagerImpl wm = WindowManagerImpl.getInstance();
                    ModeImpl slidingMode = (ModeImpl)wm.findMode(tc);
                    ModeImpl modeToRestore = (ModeImpl)wm.findMode(nameOfModeToRestore);
                    if (null != modeToRestore && null != slidingMode) {
                        wm.userRestoredMode(slidingMode, modeToRestore);
                    }
                }
            } else if ("closeGroup".equals(cmd) && Switches.isModeClosingEnabled()) {
                TopComponent tc = this.tabbed.getTopComponentAt(0);
                WindowManagerImpl wm = WindowManagerImpl.getInstance();
                ModeImpl mode = (ModeImpl)wm.findMode(tc);
                if (null != mode) {
                    wm.userClosedMode(mode);
                }
            }
        } else if (e instanceof SlideBarActionEvent) {
            TopComponent tc;
            MaximizeWindowAction mwa;
            SlideBarActionEvent sbe = (SlideBarActionEvent)e;
            String cmd = sbe.getActionCommand();
            if ("popup".equals(cmd)) {
                TabbedHandler.handlePopupMenuShowing(sbe.getMouseEvent(), sbe.getTabIndex());
            } else if ("slideIn".equals(cmd)) {
                this.modeView.getController().userTriggeredSlideIn(this.modeView, sbe.getSlideOperation());
            } else if ("slideResize".equals(cmd)) {
                this.modeView.getController().userResizedSlidingWindow(this.modeView, sbe.getSlideOperation());
            } else if ("slideOut".equals(cmd)) {
                ProxySlideOperation op = new ProxySlideOperation(sbe.getSlideOperation(), this.ignoreChange);
                this.modeView.getController().userTriggeredSlideOut(this.modeView, op);
            } else if ("disableAutoHide".equals(cmd)) {
                TopComponent tc2 = this.tabbed.getTopComponentAt(sbe.getTabIndex());
                this.modeView.getController().userDisabledAutoHide(this.modeView, tc2);
            } else if ("slideMaximize" == cmd && (mwa = new MaximizeWindowAction(tc = this.tabbed.getTopComponentAt(sbe.getTabIndex()))).isEnabled()) {
                mwa.actionPerformed(e);
            }
        }
    }

    public static void handlePopupMenuShowing(MouseEvent e, int idx) {
        Component c;
        TopComponent tc;
        for (c = (Component)e.getSource(); c != null && !(c instanceof Tabbed.Accessor); c = c.getParent()) {
        }
        if (c == null) {
            return;
        }
        Tabbed tab = ((Tabbed.Accessor)c).getTabbed();
        Point p = SwingUtilities.convertPoint((Component)e.getSource(), e.getPoint(), c);
        int clickTab = idx;
        Lookup context = null;
        Action[] actions = null;
        if (clickTab >= 0 && (tc = tab.getTopComponentAt(clickTab)) != null) {
            actions = tab.getPopupActions(tc.getActions(), clickTab);
            if (actions == null) {
                actions = tc.getActions();
            }
            if (actions == null || actions.length == 0) {
                return;
            }
            context = tc.getLookup();
        }
        if (null == context) {
            actions = tab.getPopupActions(new Action[0], -1);
            if (actions == null || actions.length == 0) {
                return;
            }
            context = Lookup.getDefault();
        }
        TabbedHandler.showPopupMenu(Utilities.actionsToPopup((Action[])actions, (Lookup)context), p, c);
    }

    private static void showPopupMenu(JPopupMenu popup, Point p, Component comp) {
        popup.show(comp, p.x, p.y);
    }

    public static void handleMaximization(TabActionEvent tae) {
        Component c;
        for (c = (Component)tae.getSource(); c != null && !(c instanceof Tabbed.Accessor); c = c.getParent()) {
        }
        if (c == null) {
            return;
        }
        Tabbed tab = ((Tabbed.Accessor)c).getTabbed();
        TopComponent tc = tab.getTopComponentAt(tae.getTabIndex());
        MaximizeWindowAction mwa = new MaximizeWindowAction(tc);
        if (mwa.isEnabled()) {
            mwa.actionPerformed((ActionEvent)tae);
        }
    }

    private static class ProxySlideOperation
    implements SlideOperation {
        private SlideOperation original;
        private boolean disable;

        public ProxySlideOperation(SlideOperation orig, boolean disableActivation) {
            this.original = orig;
            this.disable = disableActivation;
        }

        @Override
        public Component getComponent() {
            return this.original.getComponent();
        }

        @Override
        public Rectangle getFinishBounds() {
            return this.original.getFinishBounds();
        }

        @Override
        public String getSide() {
            return this.original.getSide();
        }

        @Override
        public Rectangle getStartBounds() {
            return this.original.getStartBounds();
        }

        @Override
        public int getType() {
            return this.original.getType();
        }

        @Override
        public void prepareEffect() {
            this.original.prepareEffect();
        }

        @Override
        public boolean requestsActivation() {
            if (this.disable) {
                return false;
            }
            return this.original.requestsActivation();
        }

        @Override
        public void run(JLayeredPane pane, Integer layer) {
            this.original.run(pane, layer);
        }

        @Override
        public void setFinishBounds(Rectangle bounds) {
            this.original.setFinishBounds(bounds);
        }

        @Override
        public void setStartBounds(Rectangle bounds) {
            this.original.setStartBounds(bounds);
        }
    }

    private static class ActivationManager
    implements AWTEventListener {
        private ActivationManager() {
        }

        @Override
        public void eventDispatched(AWTEvent e) {
            if (e.getID() == 501) {
                this.handleActivation((MouseEvent)e);
            }
        }

        private void handleActivation(MouseEvent evt) {
            ModeComponent modeComp;
            Component comp;
            Object obj = evt.getSource();
            if (!(obj instanceof Component)) {
                return;
            }
            for (comp = (Component)obj; comp != null && !(comp instanceof ModeComponent); comp = comp.getParent()) {
                JComponent c;
                TopComponent tc;
                if (comp instanceof JComponent && Boolean.TRUE.equals((c = (JComponent)comp).getClientProperty("dontActivate"))) {
                    return;
                }
                if (!(comp instanceof TopComponent) || !Boolean.TRUE.equals((tc = (TopComponent)comp).getClientProperty((Object)"isSliding"))) continue;
                tc.requestActive();
                return;
            }
            if (comp instanceof ModeComponent && (modeComp = (ModeComponent)((Object)comp)).getKind() != 2) {
                ModeView modeView = modeComp.getModeView();
                modeView.getController().userActivatedModeView(modeView);
            }
        }
    }

}

