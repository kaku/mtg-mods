/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view;

import java.awt.Rectangle;
import java.util.Map;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.view.Controller;
import org.netbeans.core.windows.view.ModeContainer;
import org.netbeans.core.windows.view.ModeView;
import org.netbeans.core.windows.view.dnd.WindowDnDManager;
import org.netbeans.core.windows.view.ui.slides.SlideBarContainer;
import org.openide.windows.TopComponent;

public class SlidingView
extends ModeView {
    private final String side;
    private Rectangle slideBounds;
    private Map<TopComponent, Integer> slideInSizes;

    public SlidingView(Controller controller, WindowDnDManager windowDnDManager, TopComponent[] topComponents, TopComponent selectedTopComponent, String side, Map<TopComponent, Integer> slideInSizes) {
        super(controller);
        this.side = side;
        this.slideInSizes = slideInSizes;
        this.container = new SlideBarContainer(this, windowDnDManager);
        this.setTopComponents(topComponents, selectedTopComponent);
    }

    public String getSide() {
        return this.side;
    }

    public Rectangle getTabBounds(int tabIndex) {
        return ((SlideBarContainer)this.container).getTabBounds(tabIndex);
    }

    public Rectangle getSlideBounds() {
        Rectangle res = this.slideBounds;
        TopComponent tc = this.getSelectedTopComponent();
        if (null != tc) {
            String tcID;
            WindowManagerImpl wm = WindowManagerImpl.getInstance();
            if (wm.isTopComponentMaximizedWhenSlidedIn(tcID = wm.findTopComponentID(tc))) {
                if ("bottom".equals(this.side) || "top".equals(this.side)) {
                    res.height = Integer.MAX_VALUE;
                } else {
                    res.width = Integer.MAX_VALUE;
                }
            } else {
                Integer prevSlideSize = this.slideInSizes.get((Object)tc);
                if (null != prevSlideSize) {
                    if (null == res) {
                        res = tc.getBounds();
                    }
                    if ("bottom".equals(this.side) || "top".equals(this.side)) {
                        res.height = prevSlideSize;
                    } else {
                        res.width = prevSlideSize;
                    }
                }
            }
        }
        return res;
    }

    public void setSlideBounds(Rectangle slideBounds) {
        this.slideBounds = slideBounds;
    }

    public void setSlideInSizes(Map<TopComponent, Integer> slideInSizes) {
        this.slideInSizes = slideInSizes;
    }
}

