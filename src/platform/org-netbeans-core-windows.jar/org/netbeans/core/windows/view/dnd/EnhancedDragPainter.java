/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view.dnd;

import java.awt.Graphics2D;
import java.awt.Rectangle;

public interface EnhancedDragPainter {
    public void additionalDragPaint(Graphics2D var1);

    public Rectangle getPaintArea();
}

