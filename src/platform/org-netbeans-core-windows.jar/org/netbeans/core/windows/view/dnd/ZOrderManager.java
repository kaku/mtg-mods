/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.windows.view.dnd;

import java.awt.Frame;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import javax.swing.RootPaneContainer;
import javax.swing.SwingUtilities;
import org.openide.windows.WindowManager;

public final class ZOrderManager
extends WindowAdapter {
    private static ZOrderManager instance;
    private static Logger logger;
    private List<WeakReference<RootPaneContainer>> zOrder = new ArrayList<WeakReference<RootPaneContainer>>();
    private Set<WeakReference<RootPaneContainer>> excludeSet = new HashSet<WeakReference<RootPaneContainer>>();

    private ZOrderManager() {
    }

    public static ZOrderManager getInstance() {
        if (instance == null) {
            instance = new ZOrderManager();
        }
        return instance;
    }

    public void attachWindow(RootPaneContainer rpc) {
        logger.entering(this.getClass().getName(), "attachWindow");
        if (!(rpc instanceof Window)) {
            throw new IllegalArgumentException("Argument must be subclas of java.awt.Window: " + rpc);
        }
        if (this.getWeak(rpc) != null) {
            throw new IllegalArgumentException("Window already attached: " + rpc);
        }
        this.zOrder.add(new WeakReference<RootPaneContainer>(rpc));
        ((Window)((Object)rpc)).addWindowListener(this);
    }

    public boolean detachWindow(RootPaneContainer rpc) {
        logger.entering(this.getClass().getName(), "detachWindow");
        if (!(rpc instanceof Window)) {
            throw new IllegalArgumentException("Argument must be subclas of java.awt.Window: " + rpc);
        }
        WeakReference<RootPaneContainer> ww = this.getWeak(rpc);
        if (ww == null) {
            return false;
        }
        ((Window)((Object)rpc)).removeWindowListener(this);
        return this.zOrder.remove(ww);
    }

    public void setExcludeFromOrder(RootPaneContainer rpc, boolean exclude) {
        if (exclude) {
            this.excludeSet.add(new WeakReference<RootPaneContainer>(rpc));
        } else {
            WeakReference<RootPaneContainer> ww = this.getExcludedWeak(rpc);
            if (ww != null) {
                this.excludeSet.remove(ww);
            }
        }
    }

    public void clear() {
        for (WeakReference<RootPaneContainer> elem : this.zOrder) {
            RootPaneContainer rpc = elem.get();
            if (rpc == null) continue;
            ((Window)((Object)rpc)).removeWindowListener(this);
        }
        this.zOrder.clear();
    }

    public boolean isOnTop(RootPaneContainer rpc, Point screenLoc) {
        logger.entering(this.getClass().getName(), "isOnTop");
        int size = this.zOrder.size();
        WeakReference<RootPaneContainer> curWeakW = null;
        RootPaneContainer curRpc = null;
        for (int i = size - 1; i >= 0; --i) {
            curWeakW = this.zOrder.get(i);
            if (curWeakW == null || this.getExcludedWeak(curRpc = curWeakW.get()) != null) continue;
            if (curRpc == rpc) {
                return true;
            }
            Window curW = (Window)((Object)curRpc);
            Point loc = new Point(screenLoc);
            SwingUtilities.convertPointFromScreen(loc, curW);
            if (!curW.contains(loc)) continue;
            return false;
        }
        if (rpc == WindowManager.getDefault().getMainWindow()) {
            return true;
        }
        return false;
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
        logger.entering(this.getClass().getName(), "windowActivated");
        WeakReference<RootPaneContainer> ww = this.getWeak((RootPaneContainer)((Object)e.getWindow()));
        if (ww == null) {
            throw new IllegalArgumentException("Window not attached: " + e.getWindow());
        }
        this.zOrder.remove(ww);
        this.zOrder.add(ww);
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    private WeakReference<RootPaneContainer> getWeak(RootPaneContainer rpc) {
        for (WeakReference<RootPaneContainer> elem : this.zOrder) {
            if (elem.get() != rpc) continue;
            return elem;
        }
        return null;
    }

    private WeakReference<RootPaneContainer> getExcludedWeak(RootPaneContainer rpc) {
        for (WeakReference<RootPaneContainer> elem : this.excludeSet) {
            if (elem.get() != rpc) continue;
            return elem;
        }
        return null;
    }

    static {
        logger = Logger.getLogger("org.netbeans.core.windows.view.dnd");
    }
}

