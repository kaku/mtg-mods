/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerUtils
 *  org.openide.explorer.propertysheet.PropertySheet
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Handle
 *  org.openide.nodes.NodeAdapter
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeOp
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.io.NbMarshalledObject
 *  org.openide.util.io.SafeException
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.windows.view.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.SwingUtilities;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.propertysheet.PropertySheet;
import org.openide.nodes.Node;
import org.openide.nodes.NodeAdapter;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeOp;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.io.NbMarshalledObject;
import org.openide.util.io.SafeException;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public final class NbSheet
extends TopComponent {
    private static final Logger LOG = Logger.getLogger(NbSheet.class.getName());
    private static final String PROP_LONGER_DISPLAY_NAME = "longerDisplayName";
    static final long serialVersionUID = 7807519514644165460L;
    private static NbSheet sharedSheet;
    private final transient Listener listener;
    private final transient SheetNodesListener snListener;
    boolean global;
    private PropertySheet propertySheet;
    private Node[] nodes = new Node[0];
    private static MessageFormat globalPropertiesFormat;
    private static MessageFormat localPropertiesFormat;

    public NbSheet() {
        this(false);
    }

    public NbSheet(boolean global) {
        this.global = global;
        this.propertySheet = new PropertySheet();
        this.putClientProperty((Object)"NamingType", (Object)"BothOnlyCompName");
        this.setLayout((LayoutManager)new BorderLayout());
        this.add((Component)this.propertySheet, (Object)"Center");
        this.setIcon(ImageUtilities.loadImage((String)"org/netbeans/core/windows/resources/properties.gif", (boolean)true));
        this.updateTitle();
        this.putClientProperty((Object)"SlidingName", (Object)NbBundle.getMessage(NbSheet.class, (String)"CTL_PropertiesWindow"));
        this.listener = new Listener();
        this.snListener = new SheetNodesListener();
        this.getAccessibleContext().setAccessibleName(NbBundle.getBundle(NbSheet.class).getString("ACSN_PropertiesSheet"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getBundle(NbSheet.class).getString("ACSD_PropertiesSheet"));
        this.setActivatedNodes(null);
    }

    public static NbSheet findDefault() {
        if (sharedSheet == null) {
            TopComponent tc = WindowManager.getDefault().findTopComponent("properties");
            if (tc != null) {
                if (tc instanceof NbSheet) {
                    sharedSheet = (NbSheet)tc;
                } else {
                    IllegalStateException exc = new IllegalStateException("Incorrect settings file. Unexpected class returned. Expected:" + NbSheet.class.getName() + " Returned:" + tc.getClass().getName());
                    Logger.getLogger(NbSheet.class.getName()).log(Level.WARNING, null, exc);
                    NbSheet.getDefault();
                }
            } else {
                NbSheet.getDefault();
            }
        }
        return sharedSheet;
    }

    protected String preferredID() {
        return "properties";
    }

    public static NbSheet getDefault() {
        if (sharedSheet == null) {
            sharedSheet = new NbSheet(true);
        }
        return sharedSheet;
    }

    public String getShortName() {
        return NbBundle.getMessage(NbSheet.class, (String)"CTL_PropertiesWindow");
    }

    public int getPersistenceType() {
        return 0;
    }

    public HelpCtx getHelpCtx() {
        return this.global ? ExplorerUtils.getHelpCtx((Node[])this.nodes, (HelpCtx)new HelpCtx(NbSheet.class)) : null;
    }

    @Deprecated
    public void requestFocus() {
        super.requestFocus();
        this.propertySheet.requestFocus();
    }

    @Deprecated
    public boolean requestFocusInWindow() {
        super.requestFocusInWindow();
        return this.propertySheet.requestFocusInWindow();
    }

    public void open() {
        super.open();
        if (this.global) {
            SwingUtilities.invokeLater(this.listener);
        }
    }

    protected void updateTitle() {
        Mode ourMode = WindowManager.getDefault().findMode((TopComponent)this);
        String nodeTitle = null;
        ArrayList<Node> copyNodes = new ArrayList<Node>(Arrays.asList(this.nodes));
        Node node = null;
        if (!copyNodes.isEmpty()) {
            node = copyNodes.get(0);
        }
        if (node == null) {
            nodeTitle = "";
        } else {
            nodeTitle = node.getDisplayName();
            Object alternativeDisplayName = node.getValue("longerDisplayName");
            if (alternativeDisplayName instanceof String) {
                nodeTitle = (String)alternativeDisplayName;
            }
        }
        Object[] titleParams = new Object[]{copyNodes.size(), nodeTitle};
        if (ourMode != null && "properties".equals(ourMode.getName())) {
            if (globalPropertiesFormat == null) {
                globalPropertiesFormat = new MessageFormat(NbBundle.getMessage(NbSheet.class, (String)"CTL_FMT_GlobalProperties"));
            }
            this.setName(globalPropertiesFormat.format(titleParams));
        } else {
            if (localPropertiesFormat == null) {
                localPropertiesFormat = new MessageFormat(NbBundle.getMessage(NbSheet.class, (String)"CTL_FMT_LocalProperties"));
            }
            this.setName(localPropertiesFormat.format(titleParams));
        }
        this.setToolTipText(this.getName());
    }

    public void setNodes(Node[] nodes) {
        this.setNodes(nodes, true, "setNodes");
    }

    final void setNodes(Node[] nodes, boolean reattach, String from) {
        LOG.log(Level.FINE, "setNodes({0}, {1}, {2})", new Object[]{Arrays.asList(nodes), reattach, from});
        this.nodes = nodes;
        this.propertySheet.setNodes(nodes);
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                NbSheet.this.updateTitle();
            }
        });
        if (reattach) {
            this.snListener.detach();
            this.snListener.attach(nodes);
        }
        LOG.log(Level.FINE, "finished setNodes({0}, {1}, {2})", new Object[]{Arrays.asList(nodes), reattach, from});
    }

    final Node[] getNodes() {
        return this.nodes;
    }

    public void writeExternal(ObjectOutput out) throws IOException {
        super.writeExternal(out);
        if (this.global) {
            out.writeObject(null);
        } else {
            Node.Handle[] arr = NodeOp.toHandles((Node[])this.nodes);
            out.writeObject(arr);
        }
        out.writeBoolean(this.global);
    }

    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        try {
            super.readExternal(in);
        }
        catch (SafeException se) {
            // empty catch block
        }
        Object obj = in.readObject();
        if (obj instanceof NbMarshalledObject || obj instanceof ExplorerManager) {
            this.global = (Boolean)in.readObject();
        } else {
            Node[] ns;
            if (obj == null) {
                ns = TopComponent.getRegistry().getActivatedNodes();
            } else {
                Node.Handle[] arr = (Node.Handle[])obj;
                try {
                    ns = NodeOp.fromHandles((Node.Handle[])arr);
                }
                catch (IOException ex) {
                    Exceptions.attachLocalizedMessage((Throwable)ex, (String)NbBundle.getBundle(NbSheet.class).getString("EXC_CannotLoadNodes"));
                    Logger.getLogger(NbSheet.class.getName()).log(Level.WARNING, null, ex);
                    ns = new Node[]{};
                }
            }
            this.global = in.readBoolean();
            this.setNodes(ns, true, "readExternal");
        }
    }

    public Object readResolve() throws ObjectStreamException {
        if (this.global) {
            return NbSheet.getDefault();
        }
        if (this.nodes == null || this.nodes.length <= 0) {
            return null;
        }
        return this;
    }

    protected Object writeReplace() throws ObjectStreamException {
        if (this.global) {
            return new Replacer();
        }
        return super.writeReplace();
    }

    private void updateGlobalListening(boolean listen) {
        if (this.global) {
            if (listen) {
                TopComponent.getRegistry().addPropertyChangeListener((PropertyChangeListener)this.listener);
            } else {
                TopComponent.getRegistry().removePropertyChangeListener((PropertyChangeListener)this.listener);
            }
        }
    }

    protected void componentOpened() {
        this.updateGlobalListening(true);
    }

    protected void componentClosed() {
        this.updateGlobalListening(false);
        this.setNodes(new Node[0], true, "componentClosed");
    }

    protected void componentDeactivated() {
        super.componentDeactivated();
        if (Utilities.isMac()) {
            this.propertySheet.firePropertyChange("MACOSX", true, false);
        }
    }

    static {
        globalPropertiesFormat = null;
        localPropertiesFormat = null;
    }

    private class SheetNodesListener
    extends NodeAdapter
    implements Runnable {
        private HashMap<Node, NodeListener> listenerMap;
        private HashMap<Node, PropertyChangeListener> pListenerMap;

        SheetNodesListener() {
        }

        public void nodeDestroyed(final NodeEvent ev) {
            Mutex.EVENT.readAccess(new Runnable(){

                @Override
                public void run() {
                    SheetNodesListener.this.handleNodeDestroyed(ev);
                }
            });
        }

        final void handleNodeDestroyed(NodeEvent ev) {
            assert (EventQueue.isDispatchThread());
            Node destroyedNode = ev.getNode();
            NodeListener listener = this.listenerMap.get((Object)destroyedNode);
            PropertyChangeListener pListener = this.pListenerMap.get((Object)destroyedNode);
            destroyedNode.removeNodeListener(listener);
            destroyedNode.removePropertyChangeListener(pListener);
            this.listenerMap.remove((Object)destroyedNode);
            this.pListenerMap.remove((Object)destroyedNode);
            if (this.listenerMap.isEmpty() && !NbSheet.this.global) {
                NbSheet.this.close();
            } else {
                NbSheet.this.setNodes(this.listenerMap.keySet().toArray((T[])new Node[this.listenerMap.size()]), false, "handleNodeDestroyed");
            }
        }

        public void attach(Node[] nodes) {
            assert (EventQueue.isDispatchThread());
            this.listenerMap = new HashMap(nodes.length * 2);
            this.pListenerMap = new HashMap(nodes.length * 2);
            NodeListener curListener = null;
            PropertyChangeListener pListener = null;
            for (Node n : nodes) {
                curListener = NodeOp.weakNodeListener((NodeListener)this, (Object)n);
                pListener = WeakListeners.propertyChange((PropertyChangeListener)((Object)this), (Object)n);
                this.listenerMap.put(n, curListener);
                this.pListenerMap.put(n, pListener);
                n.addNodeListener(curListener);
                n.addPropertyChangeListener(pListener);
            }
        }

        public void detach() {
            assert (EventQueue.isDispatchThread());
            if (this.listenerMap == null) {
                return;
            }
            for (Map.Entry<Node, NodeListener> curEntry2 : this.listenerMap.entrySet()) {
                curEntry2.getKey().removeNodeListener(curEntry2.getValue());
            }
            for (Map.Entry curEntry : this.pListenerMap.entrySet()) {
                ((Node)curEntry.getKey()).removePropertyChangeListener((PropertyChangeListener)curEntry.getValue());
            }
            this.listenerMap = null;
            this.pListenerMap = null;
        }

        public void propertyChange(PropertyChangeEvent pce) {
            if ("displayName".equals(pce.getPropertyName())) {
                SwingUtilities.invokeLater(this);
            }
        }

        @Override
        public void run() {
            assert (EventQueue.isDispatchThread());
            NbSheet.this.updateTitle();
        }

    }

    private class Listener
    implements Runnable,
    PropertyChangeListener {
        Listener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent ev) {
            if ("activatedNodes".equals(ev.getPropertyName())) {
                this.activate();
            }
        }

        @Override
        public void run() {
            this.activate();
        }

        public void activate() {
            TopComponent tc = TopComponent.getRegistry().getActivated();
            Node[] arr = TopComponent.getRegistry().getActivatedNodes();
            LOG.log(Level.FINE, "Active component {0}", (Object)tc);
            NbSheet.this.setNodes(arr, true, "activate");
        }
    }

    private static final class Replacer
    implements Serializable {
        static final long serialVersionUID = -7897067133215740572L;

        Replacer() {
        }

        private Object readResolve() throws ObjectStreamException {
            return NbSheet.getDefault();
        }
    }

}

