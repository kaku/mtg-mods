/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view;

import java.awt.Rectangle;
import org.netbeans.core.windows.view.ModeAccessor;
import org.netbeans.core.windows.view.ModeStructureAccessor;

interface WindowSystemAccessor {
    public Rectangle getMainWindowBoundsJoined();

    public Rectangle getMainWindowBoundsSeparated();

    public int getMainWindowFrameStateJoined();

    public int getMainWindowFrameStateSeparated();

    public Rectangle getEditorAreaBounds();

    public int getEditorAreaState();

    public int getEditorAreaFrameState();

    public String getToolbarConfigurationName();

    public ModeAccessor getActiveModeAccessor();

    public ModeAccessor getMaximizedModeAccessor();

    public ModeStructureAccessor getModeStructureAccessor();

    public ModeAccessor findModeAccessor(String var1);
}

