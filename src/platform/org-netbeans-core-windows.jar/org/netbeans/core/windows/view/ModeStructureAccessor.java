/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view;

import org.netbeans.core.windows.view.ElementAccessor;
import org.netbeans.core.windows.view.ModeAccessor;
import org.netbeans.core.windows.view.SlidingAccessor;

interface ModeStructureAccessor {
    public ElementAccessor getSplitRootAccessor();

    public ModeAccessor[] getSeparateModeAccessors();

    public SlidingAccessor[] getSlidingModeAccessors();
}

