/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.PrintStream;
import javax.swing.SwingUtilities;
import javax.swing.border.AbstractBorder;
import javax.swing.border.Border;

final class DecorationUtils {
    private DecorationUtils() {
    }

    public static Border createSeparateBorder() {
        return new SeparateBorder();
    }

    public static ResizeHandler createResizeHandler(Insets insets) {
        return new ResizeHandler(insets);
    }

    static class ResizeHandler
    extends MouseAdapter
    implements MouseMotionListener {
        private Insets insets;
        private int cursorType;
        private boolean isPressed = false;
        private Rectangle resizedBounds = new Rectangle();
        private Rectangle movedBounds = new Rectangle();
        private Point startDragLoc;
        private Rectangle startWinBounds;
        private Dimension minSize;

        public ResizeHandler(Insets insets) {
            this.insets = insets;
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            this.check(e);
            Window w = SwingUtilities.getWindowAncestor((Component)e.getSource());
            if (0 == this.cursorType) {
                return;
            }
            Rectangle newBounds = this.computeNewBounds(w, this.getScreenLoc(e));
            if (!w.getBounds().equals(newBounds)) {
                w.setBounds(newBounds);
            }
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            this.check(e);
            Component comp = (Component)e.getSource();
            this.movedBounds = comp.getBounds(this.movedBounds);
            this.cursorType = this.getCursorType(this.movedBounds, e.getPoint());
            comp.setCursor(Cursor.getPredefinedCursor(this.cursorType));
        }

        @Override
        public void mousePressed(MouseEvent e) {
            this.isPressed = true;
            this.startDragLoc = this.getScreenLoc(e);
            Window w = SwingUtilities.getWindowAncestor((Component)e.getSource());
            this.startWinBounds = w.getBounds();
            this.resizedBounds.setBounds(this.startWinBounds);
            this.minSize = w.getMinimumSize();
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            this.isPressed = false;
            this.startDragLoc = null;
            this.startWinBounds = null;
            this.minSize = null;
        }

        @Override
        public void mouseExited(MouseEvent e) {
            Component comp = (Component)e.getSource();
            comp.setCursor(Cursor.getDefaultCursor());
        }

        private int getCursorType(Rectangle b, Point p) {
            boolean isInRightPart;
            int leftDist = p.x - b.x;
            int rightDist = b.x + b.width - p.x;
            int topDist = p.y - b.y;
            int bottomDist = b.y + b.height - p.y;
            boolean isNearTop = topDist >= 0 && topDist <= this.insets.top;
            boolean isNearBottom = bottomDist >= 0 && bottomDist <= this.insets.bottom;
            boolean isNearLeft = leftDist >= 0 && leftDist <= this.insets.left;
            boolean isNearRight = rightDist >= 0 && rightDist <= this.insets.right;
            boolean isInTopPart = topDist >= 0 && topDist <= this.insets.top + 10;
            boolean isInBottomPart = bottomDist >= 0 && bottomDist <= this.insets.bottom + 10;
            boolean isInLeftPart = leftDist >= 0 && leftDist <= this.insets.left + 10;
            boolean bl = isInRightPart = rightDist >= 0 && rightDist <= this.insets.right + 10;
            if (isNearTop && isInLeftPart || isInTopPart && isNearLeft) {
                return 6;
            }
            if (isNearTop && isInRightPart || isInTopPart && isNearRight) {
                return 7;
            }
            if (isNearBottom && isInLeftPart || isInBottomPart && isNearLeft) {
                return 4;
            }
            if (isNearBottom && isInRightPart || isInBottomPart && isNearRight) {
                return 5;
            }
            if (isNearTop) {
                return 8;
            }
            if (isNearLeft) {
                return 10;
            }
            if (isNearRight) {
                return 11;
            }
            if (isNearBottom) {
                return 9;
            }
            return 0;
        }

        private Rectangle computeNewBounds(Window w, Point dragLoc) {
            if (this.startDragLoc == null) {
                throw new IllegalArgumentException("Can't compute bounds when startDragLoc is null");
            }
            int xDiff = dragLoc.x - this.startDragLoc.x;
            int yDiff = dragLoc.y - this.startDragLoc.y;
            this.resizedBounds.setBounds(this.startWinBounds);
            switch (this.cursorType) {
                case 11: {
                    this.resizedBounds.width = this.startWinBounds.width + (dragLoc.x - this.startDragLoc.x);
                    break;
                }
                case 10: {
                    this.resizedBounds.width = this.startWinBounds.width - xDiff;
                    this.resizedBounds.x = this.startWinBounds.x + xDiff;
                    break;
                }
                case 8: {
                    this.resizedBounds.height = this.startWinBounds.height - yDiff;
                    this.resizedBounds.y = this.startWinBounds.y + yDiff;
                    break;
                }
                case 9: {
                    this.resizedBounds.height = this.startWinBounds.height + (dragLoc.y - this.startDragLoc.y);
                    break;
                }
                case 7: {
                    ResizeHandler.resize(this.resizedBounds, 0, yDiff, xDiff, - yDiff, this.minSize);
                    break;
                }
                case 6: {
                    ResizeHandler.resize(this.resizedBounds, xDiff, yDiff, - xDiff, - yDiff, this.minSize);
                    break;
                }
                case 5: {
                    ResizeHandler.resize(this.resizedBounds, 0, 0, xDiff, yDiff, this.minSize);
                    break;
                }
                case 4: {
                    ResizeHandler.resize(this.resizedBounds, xDiff, 0, - xDiff, yDiff, this.minSize);
                    break;
                }
                default: {
                    System.out.println("unknown cursor type : " + this.cursorType);
                }
            }
            return this.resizedBounds;
        }

        private static void resize(Rectangle rect, int xDiff, int yDiff, int widthDiff, int heightDiff, Dimension minSize) {
            rect.x += xDiff;
            rect.y += yDiff;
            rect.height += heightDiff;
            rect.width += widthDiff;
            rect.height = Math.max(rect.height, minSize.height);
            rect.width = Math.max(rect.width, minSize.width);
        }

        private Point getScreenLoc(MouseEvent e) {
            Point screenP = new Point(e.getPoint());
            SwingUtilities.convertPointToScreen(screenP, (Component)e.getSource());
            return screenP;
        }

        private void check(MouseEvent e) {
            Object o = e.getSource();
            if (!(o instanceof Component)) {
                throw new IllegalArgumentException("ResizeHandler works only with Component, not with " + o);
            }
            Window w = SwingUtilities.getWindowAncestor((Component)o);
            if (w == null) {
                throw new IllegalStateException("Can't find and resize the window, not attached.");
            }
        }
    }

    private static class SeparateBorder
    extends AbstractBorder {
        private SeparateBorder() {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return new Insets(3, 3, 3, 3);
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            g.setColor(Color.DARK_GRAY);
            g.drawRect(x, y, width - 1, height - 1);
        }
    }

}

