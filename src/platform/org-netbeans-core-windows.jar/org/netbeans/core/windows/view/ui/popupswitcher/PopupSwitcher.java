/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.StatusDisplayer
 */
package org.netbeans.core.windows.view.ui.popupswitcher;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.netbeans.core.windows.view.ui.popupswitcher.Item;
import org.netbeans.core.windows.view.ui.popupswitcher.Model;
import org.netbeans.core.windows.view.ui.popupswitcher.Table;
import org.openide.awt.StatusDisplayer;

class PopupSwitcher
extends JPanel {
    private final Table table;
    private final JScrollPane scrollPane;
    private final JLabel lblDescription;
    private final ListSelectionListener selectionListener;
    static final char DOTS = '\u2026';

    public PopupSwitcher(boolean documentsOnly, int hits, boolean forward) {
        this(new Model(documentsOnly), hits, forward);
    }

    PopupSwitcher(Model model, int hits, boolean forward) {
        super(new BorderLayout());
        this.table = new Table(model);
        this.scrollPane = new JScrollPane(this.table);
        this.lblDescription = new JLabel(" ");
        this.lblDescription.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, this.table.getGridColor()), BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        this.configureScrollPane();
        this.add((Component)this.scrollPane, "Center");
        this.add((Component)this.lblDescription, "South");
        Border b = UIManager.getBorder("nb.popupswitcher.border");
        if (null == b) {
            b = BorderFactory.createLineBorder(this.table.getGridColor());
        }
        this.setBorder(b);
        this.setBackground(PopupSwitcher.getDefaultBackground());
        this.selectionListener = new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent e) {
                PopupSwitcher.this.updateDescription();
            }
        };
        this.table.getSelectionModel().addListSelectionListener(this.selectionListener);
        this.table.getColumnModel().getSelectionModel().addListSelectionListener(this.selectionListener);
        this.table.setInitialSelection(hits, forward);
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._updateDescription();
    }

    private void configureScrollPane() {
        this.scrollPane.setHorizontalScrollBarPolicy(31);
        this.scrollPane.setBorder(BorderFactory.createEmptyBorder());
        Color bkColor = PopupSwitcher.getDefaultBackground();
        this.scrollPane.getViewport().setBackground(bkColor);
        this.scrollPane.setBackground(bkColor);
    }

    Table getTable() {
        return this.table;
    }

    static Color getDefaultForeground() {
        Color foreground = UIManager.getColor("nb.popupswitcher.foreground");
        if (foreground == null) {
            foreground = UIManager.getColor("ComboBox.foreground");
        }
        return foreground;
    }

    static Color getDefaultBackground() {
        Color background = UIManager.getColor("nb.popupswitcher.background");
        if (background == null) {
            background = UIManager.getColor("ComboBox.background");
        }
        return background;
    }

    static Color getSelectionForeground() {
        Color selForeground = UIManager.getColor("nb.popupswitcher.selectionForeground");
        if (selForeground == null) {
            selForeground = UIManager.getColor("ComboBox.selectionForeground");
        }
        return selForeground;
    }

    static Color getSelectionBackground() {
        Color selBackground = UIManager.getColor("nb.popupswitcher.selectionBackground");
        if (selBackground == null) {
            selBackground = UIManager.getColor("ComboBox.selectionBackground");
        }
        return selBackground;
    }

    private void updateDescription() {
        if (!this.lblDescription.isShowing()) {
            return;
        }
        this._updateDescription();
    }

    private void _updateDescription() {
        if (!this.lblDescription.isValid()) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    PopupSwitcher.this.updateDescription();
                }
            });
            return;
        }
        Item item = this.table.getSelectedItem();
        String statusText = item == null ? null : item.getDescription();
        StatusDisplayer.getDefault().setStatusText(statusText);
        if (null == statusText) {
            int selRow = this.table.getSelectedRow();
            int selCol = this.table.getSelectedColumn();
            if (selRow >= 0 && selCol >= 0) {
                TableCellRenderer ren = this.table.getCellRenderer(selRow, selCol);
                Component c = this.table.prepareRenderer(ren, selRow, selCol);
                if (c.getPreferredSize().width > this.table.getColumnModel().getColumn(selCol).getWidth()) {
                    statusText = this.table.getSelectedItem().getDisplayName();
                }
            }
        }
        this.lblDescription.setText(PopupSwitcher.truncateText(statusText, this.lblDescription.getWidth()));
    }

    static String truncateText(String s, int availPixels) {
        if (null == s) {
            return " ";
        }
        if (s.length() < 3) {
            return s;
        }
        if ((s = Table.stripHtml(s)).length() < 2) {
            return "" + '\u2026' + s;
        }
        JLabel lbl = new JLabel(s);
        while (lbl.getPreferredSize().width > availPixels && s.length() > 0) {
            s = s.substring(1);
            lbl.setText("" + '\u2026' + s);
        }
        return lbl.getText();
    }

}

