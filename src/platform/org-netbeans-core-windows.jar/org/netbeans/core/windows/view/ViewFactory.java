/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view;

import org.netbeans.core.windows.view.ControllerHandler;
import org.netbeans.core.windows.view.DefaultView;
import org.netbeans.core.windows.view.View;

public final class ViewFactory {
    private ViewFactory() {
    }

    public static View createWindowSystemView(ControllerHandler controllerHandler) {
        return new DefaultView(controllerHandler);
    }
}

