/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.TabbedContainer
 *  org.openide.util.Mutex
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.netbeans.core.windows.view.SlidingView;
import org.netbeans.core.windows.view.ViewElement;
import org.netbeans.core.windows.view.ui.MainWindow;
import org.netbeans.core.windows.view.ui.slides.SlideOperation;
import org.netbeans.core.windows.view.ui.slides.SlideOperationFactory;
import org.netbeans.swing.tabcontrol.TabbedContainer;
import org.openide.util.Mutex;
import org.openide.windows.TopComponent;

public final class DesktopImpl {
    private JLayeredPane layeredPane = new JLayeredPane();
    private JPanel desktop;
    private ViewElement splitRoot;
    private Component viewComponent;
    private Set<SlidingView> slidingViews;
    private SlideOperation curSlideIn;
    private static final int MIN_EDITOR_ALIGN_THICK = 80;

    public DesktopImpl() {
        this.layeredPane.setLayout(new LayeredLayout());
        this.desktop = new JPanel(){

            @Override
            public boolean isOpaque() {
                if (UIManager.getBoolean("NbMainWindow.showCustomBackground")) {
                    return false;
                }
                return super.isOpaque();
            }

            @Override
            public void updateUI() {
                Mutex.EVENT.readAccess(new Runnable(){

                    @Override
                    public void run() {
                        1.this.superUpdateUI();
                    }
                });
            }

            private void superUpdateUI() {
                super.updateUI();
            }

        };
        this.desktop.setLayout(new GridBagLayout());
        UIManager.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                DesktopImpl.this.updateLAF();
            }
        });
        this.updateLAF();
        this.layeredPane.add(this.desktop);
    }

    private void updateLAF() {
        Color bkColor;
        if (this.desktop != null && null != (bkColor = UIManager.getColor("NbSplitPane.background"))) {
            this.desktop.setBackground(bkColor);
            this.desktop.setOpaque(true);
        }
    }

    public Component getDesktopComponent() {
        return this.layeredPane;
    }

    public Dimension getInnerPaneDimension() {
        int width = this.desktop.getSize().width;
        int height = this.desktop.getSize().height;
        SlidingView view = this.findView("left");
        width = view != null ? width - view.getComponent().getSize().width : width;
        view = this.findView("right");
        width = view != null ? width - view.getComponent().getSize().width : width;
        view = this.findView("bottom");
        height = view != null ? height - view.getComponent().getSize().height : height;
        view = this.findView("top");
        height = view != null ? height - view.getComponent().getSize().height : height;
        return new Dimension(width, height);
    }

    public void setSplitRoot(ViewElement splitRoot) {
        this.splitRoot = splitRoot;
        if (splitRoot != null) {
            this.setViewComponent(splitRoot.getComponent());
        } else {
            this.setViewComponent(null);
        }
    }

    public void setMaximizedView(ViewElement component) {
        if (component.getComponent() != this.viewComponent) {
            this.setViewComponent(component.getComponent());
        }
    }

    private void setViewComponent(Component component) {
        if (this.viewComponent == component) {
            return;
        }
        if (this.viewComponent != null) {
            this.desktop.remove(this.viewComponent);
        }
        this.viewComponent = component;
        if (this.viewComponent != null) {
            GridBagConstraints constr = new GridBagConstraints();
            constr.gridx = 1;
            constr.gridy = 1;
            constr.fill = 1;
            constr.weightx = 1.0;
            constr.weighty = 1.0;
            constr.anchor = 10;
            Insets insets = UIManager.getInsets("nb.desktop.view.insets");
            if (null != insets) {
                constr.insets = insets;
            }
            this.desktop.add(component, constr);
        }
        this.layeredPane.revalidate();
        this.layeredPane.repaint();
    }

    public ViewElement getSplitRoot() {
        return this.splitRoot;
    }

    public void addSlidingView(SlidingView view) {
        Set<SlidingView> slidingViews = this.getSlidingViews();
        if (slidingViews.contains(view)) {
            return;
        }
        slidingViews.add(view);
        GridBagConstraints constraint = new GridBagConstraints();
        constraint.fill = 1;
        if ("bottom".equals(view.getSide())) {
            constraint.gridx = 0;
            constraint.gridy = 2;
            constraint.gridwidth = 3;
            constraint.anchor = 16;
        } else if ("left".equals(view.getSide())) {
            constraint.gridx = 0;
            constraint.gridy = 1;
            constraint.gridheight = 1;
            constraint.anchor = 18;
        } else if ("right".equals(view.getSide())) {
            constraint.gridx = 2;
            constraint.gridy = 1;
            constraint.gridheight = 1;
            constraint.anchor = 12;
        } else if ("top".equals(view.getSide())) {
            constraint.gridx = 1;
            constraint.gridy = 0;
            constraint.gridheight = 1;
            constraint.gridwidth = 2;
            constraint.anchor = 18;
        }
        this.desktop.add(view.getComponent(), constraint);
        if ("bottom".equals(view.getSide()) && view.getComponent() instanceof JPanel) {
            JPanel panel = (JPanel)view.getComponent();
            MainWindow.getInstance().setStatusBarContainer(panel);
        }
        this.layeredPane.revalidate();
    }

    public void removeSlidingView(SlidingView view) {
        Set<SlidingView> slidingViews = this.getSlidingViews();
        if (!slidingViews.contains(view)) {
            return;
        }
        slidingViews.remove(view);
        this.desktop.remove(view.getComponent());
        this.checkCurSlide();
        this.layeredPane.revalidate();
    }

    private void checkCurSlide() {
        if (this.curSlideIn != null) {
            SlidingView curView2 = null;
            Component curSlideComp = this.curSlideIn.getComponent();
            for (SlidingView curView2 : this.slidingViews) {
                if (!curView2.getTopComponents().contains(curSlideComp)) continue;
                return;
            }
            this.layeredPane.remove(curSlideComp);
        }
    }

    public void performSlideIn(SlideOperation operation, Rectangle editorBounds) {
        Rectangle slideInBounds = this.computeSlideInBounds(operation, editorBounds);
        operation.setFinishBounds(slideInBounds);
        operation.setStartBounds(this.computeThinBounds(operation, slideInBounds));
        this.performSlide(operation);
        this.curSlideIn = operation;
    }

    public void performSlideOut(SlideOperation operation, Rectangle editorBounds) {
        Rectangle slideOutBounds = operation.getComponent().getBounds();
        operation.setStartBounds(slideOutBounds);
        operation.setFinishBounds(this.computeThinBounds(operation, slideOutBounds));
        this.curSlideIn = null;
        this.performSlide(operation);
        this.desktop.revalidate();
        this.desktop.repaint();
    }

    public void performSlideIntoEdge(SlideOperation operation, Rectangle editorBounds) {
        operation.setFinishBounds(this.computeLastButtonBounds(operation));
        Rectangle screenStart = operation.getStartBounds();
        operation.setStartBounds(this.convertRectFromScreen(this.layeredPane, screenStart));
        this.performSlide(operation);
    }

    public void performSlideIntoDesktop(SlideOperation operation, Rectangle editorBounds) {
        Rectangle screenStart = operation.getStartBounds();
        operation.setStartBounds(this.convertRectFromScreen(this.layeredPane, screenStart));
        Rectangle screenFinish = operation.getStartBounds();
        operation.setStartBounds(this.convertRectFromScreen(this.layeredPane, screenFinish));
        this.performSlide(operation);
    }

    public void performSlideResize(SlideOperation operation) {
        this.performSlide(operation);
    }

    public void performSlideToggleMaximize(TopComponent tc, String side, Rectangle editorBounds) {
        Component tabbed = this.findTabbed((Component)tc);
        if (null != tabbed) {
            SlideOperation operation = SlideOperationFactory.createSlideResize(tabbed, side);
            Rectangle slideInBounds = this.computeSlideInBounds(operation, editorBounds);
            operation.setFinishBounds(slideInBounds);
            this.performSlide(operation);
        }
    }

    private Component findTabbed(Component comp) {
        while (comp.getParent() != null) {
            if (comp.getParent() instanceof TabbedContainer) {
                return comp.getParent();
            }
            comp = comp.getParent();
        }
        return null;
    }

    private void performSlide(SlideOperation operation) {
        operation.run(this.layeredPane, 102);
    }

    private Rectangle convertRectFromScreen(Component comp, Rectangle screenRect) {
        if (screenRect == null) {
            screenRect = new Rectangle(0, 0, 0, 0);
        }
        Point leftTop = screenRect.getLocation();
        SwingUtilities.convertPointFromScreen(leftTop, comp);
        return new Rectangle(leftTop, screenRect.getSize());
    }

    private Rectangle computeSlideInBounds(SlideOperation operation, Rectangle editorBounds) {
        Point editorLeftTop = editorBounds.getLocation();
        SwingUtilities.convertPointFromScreen(editorLeftTop, this.layeredPane);
        editorBounds = new Rectangle(editorLeftTop, editorBounds.getSize());
        String side = operation.getSide();
        SlidingView view = this.findView(side);
        return this.computeSlideInBounds(this.viewComponent.getBounds(), side, view.getComponent(), view.getSlideBounds(), view.getSelectedTopComponent());
    }

    Rectangle computeSlideInBounds(Rectangle splitRootRect, String side, Component slideComponent, Rectangle slideBounds, TopComponent selTc) {
        boolean keepPreferredSizeWhenSlidedIn;
        Rectangle result = new Rectangle();
        Rectangle viewRect = slideComponent.getBounds();
        Dimension viewPreferred = slideComponent.getPreferredSize();
        int minThick = 80;
        Dimension tcPreferred = null;
        boolean bl = keepPreferredSizeWhenSlidedIn = null != selTc && Boolean.TRUE.equals(selTc.getClientProperty((Object)"netbeans.winsys.tc.keep_preferred_size_when_slided_in"));
        if (keepPreferredSizeWhenSlidedIn && null == (tcPreferred = selTc.getPreferredSize())) {
            tcPreferred = slideBounds.getSize();
        }
        if (keepPreferredSizeWhenSlidedIn) {
            minThick = 20;
        }
        if ("left".equals(side)) {
            result.x = viewRect.x + Math.max(viewRect.width, viewPreferred.width);
            result.y = viewRect.y;
            result.height = keepPreferredSizeWhenSlidedIn ? tcPreferred.height : splitRootRect.height;
            int n = result.width = keepPreferredSizeWhenSlidedIn ? tcPreferred.width : slideBounds.width;
            if (result.width < minThick) {
                result.width = splitRootRect.width / 3;
            }
            if (result.width > splitRootRect.width) {
                result.width = splitRootRect.width;
            }
        } else if ("right".equals(side)) {
            int rightLimit = this.layeredPane.getBounds().width - Math.max(viewRect.width, viewPreferred.width);
            int width = keepPreferredSizeWhenSlidedIn ? tcPreferred.width : slideBounds.width;
            int n = result.x = width < minThick ? rightLimit - splitRootRect.width / 3 : rightLimit - width;
            if (result.x < 0) {
                result.x = 0;
            }
            result.y = viewRect.y;
            result.height = keepPreferredSizeWhenSlidedIn ? tcPreferred.height : splitRootRect.height;
            result.width = rightLimit - result.x;
        } else if ("bottom".equals(side)) {
            int lowerLimit = viewRect.y + viewRect.height - Math.max(viewRect.height, viewPreferred.height);
            int height = keepPreferredSizeWhenSlidedIn ? tcPreferred.height : slideBounds.height;
            result.x = viewRect.x;
            SlidingView view = this.findView("left");
            result.x += view != null ? view.getComponent().getSize().width : 0;
            int n = result.y = height < minThick ? lowerLimit - splitRootRect.height / 3 : lowerLimit - height;
            if (result.y < 0) {
                result.y = 0;
            }
            result.height = lowerLimit - result.y;
            result.width = keepPreferredSizeWhenSlidedIn ? tcPreferred.width : splitRootRect.width;
        } else if ("top".equals(side)) {
            int height = keepPreferredSizeWhenSlidedIn ? tcPreferred.height : slideBounds.height;
            result.x = viewRect.x;
            SlidingView view = this.findView("left");
            result.x += view != null ? view.getComponent().getSize().width : 0;
            result.y = viewRect.y + Math.max(viewRect.height, viewPreferred.height);
            result.height = height < minThick ? splitRootRect.height / 3 : height;
            result.height = Math.min(splitRootRect.height, height);
            result.width = keepPreferredSizeWhenSlidedIn ? tcPreferred.width : splitRootRect.width;
        }
        return result;
    }

    private Rectangle computeThinBounds(SlideOperation operation, Rectangle slideInFinish) {
        String side = operation.getSide();
        Rectangle result = new Rectangle();
        if ("left".equals(side)) {
            result.x = slideInFinish.x;
            result.y = slideInFinish.y;
            result.height = slideInFinish.height;
            result.width = 0;
        } else if ("right".equals(side)) {
            result.x = slideInFinish.x + slideInFinish.width;
            result.y = slideInFinish.y;
            result.height = slideInFinish.height;
            result.width = 0;
        } else if ("bottom".equals(side)) {
            result.x = slideInFinish.x;
            result.y = slideInFinish.y + slideInFinish.height;
            result.height = 0;
            result.width = slideInFinish.width;
        } else if ("top".equals(side)) {
            result.x = slideInFinish.x;
            result.y = slideInFinish.y;
            result.height = slideInFinish.height;
            result.width = slideInFinish.width;
        }
        return result;
    }

    private Rectangle computeLastButtonBounds(SlideOperation operation) {
        String side = operation.getSide();
        SlidingView view = this.findView(side);
        Rectangle screenRect = view.getTabBounds(view.getTopComponents().size() - 1);
        Point leftTop = screenRect.getLocation();
        if ("bottom".equals(side)) {
            leftTop.y += this.desktop.getHeight() - view.getComponent().getPreferredSize().height;
        } else if ("right".equals(side)) {
            leftTop.x += this.desktop.getWidth() - view.getComponent().getPreferredSize().width;
        }
        return new Rectangle(leftTop, screenRect.getSize());
    }

    private SlidingView findView(String side) {
        for (SlidingView view : this.getSlidingViews()) {
            if (!side.equals(view.getSide())) continue;
            return view;
        }
        return null;
    }

    private Set<SlidingView> getSlidingViews() {
        if (this.slidingViews == null) {
            this.slidingViews = new HashSet<SlidingView>(5);
        }
        return this.slidingViews;
    }

    public void updateCorners() {
        if (UIManager.getBoolean("NbMainWindow.showCustomBackground")) {
            return;
        }
        SlidingView leftSlide = null;
        SlidingView topSlide = null;
        for (SlidingView view : this.slidingViews) {
            if ("left".equals(view.getSide())) {
                leftSlide = view;
            }
            if (!"top".equals(view.getSide())) continue;
            topSlide = view;
        }
        if (null == leftSlide || null == topSlide) {
            return;
        }
        this.desktop.setOpaque(true);
        if (!leftSlide.getTopComponents().isEmpty() || !topSlide.getTopComponents().isEmpty()) {
            this.desktop.setBackground(new JPanel().getBackground());
        } else {
            Color bkColor = UIManager.getColor("NbSplitPane.background");
            if (null != bkColor) {
                this.desktop.setBackground(bkColor);
            }
        }
    }

    private final class LayeredLayout
    implements LayoutManager {
        private Dimension lastSize;

        private LayeredLayout() {
        }

        @Override
        public void layoutContainer(Container parent) {
            SlidingView curView;
            String side;
            Dimension size = parent.getSize();
            DesktopImpl.this.desktop.setBounds(0, 0, size.width, size.height);
            DesktopImpl.this.desktop.invalidate();
            DesktopImpl.this.desktop.validate();
            if (DesktopImpl.this.curSlideIn != null && DesktopImpl.this.curSlideIn.getComponent().isVisible() && (curView = DesktopImpl.this.findView(side = DesktopImpl.this.curSlideIn.getSide())) != null && DesktopImpl.this.viewComponent != null) {
                Component slidedComp = DesktopImpl.this.curSlideIn.getComponent();
                TopComponent tc = curView.getSelectedTopComponent();
                boolean keepPreferredSizeWhenSlidedIn = null != tc && Boolean.TRUE.equals(tc.getClientProperty((Object)"netbeans.winsys.tc.keep_preferred_size_when_slided_in"));
                Rectangle result = slidedComp.getBounds();
                Rectangle viewRect = curView.getComponent().getBounds();
                Dimension viewPrefSize = curView.getComponent().getPreferredSize();
                Rectangle splitRootRect = DesktopImpl.this.viewComponent.getBounds();
                if ("left".equals(side)) {
                    if (!keepPreferredSizeWhenSlidedIn) {
                        result.height = splitRootRect.height;
                    }
                    if (this.lastSize != null && !this.lastSize.equals(size)) {
                        int wid = curView.getSlideBounds().width;
                        result.width = wid > size.width - viewRect.width ? size.width - size.width / 10 : wid;
                    }
                } else if ("right".equals(side)) {
                    if (!keepPreferredSizeWhenSlidedIn) {
                        result.height = splitRootRect.height;
                    }
                    if (this.lastSize != null && !this.lastSize.equals(size)) {
                        int wid;
                        int avail = size.width - Math.max(viewRect.width, viewPrefSize.width);
                        if (avail - (wid = curView.getSlideBounds().width) < wid / 10) {
                            result.x = 0 + wid / 10;
                            result.width = avail - wid / 10;
                        } else {
                            result.x = avail - result.width;
                            result.width = wid;
                        }
                    }
                } else if ("bottom".equals(side)) {
                    if (!keepPreferredSizeWhenSlidedIn) {
                        result.width = splitRootRect.width;
                    }
                    if (this.lastSize != null && !this.lastSize.equals(size)) {
                        int avail = size.height - Math.max(viewRect.height, viewPrefSize.height);
                        int hei = viewRect.height;
                        if (hei < curView.getSlideBounds().height) {
                            hei = curView.getSlideBounds().height;
                        }
                        if (avail - hei < hei / 10) {
                            result.y = 0 + hei / 10;
                            result.height = avail - hei / 10;
                        } else {
                            result.y = avail - hei;
                            result.height = hei;
                        }
                    }
                } else if ("top".equals(side)) {
                    if (!keepPreferredSizeWhenSlidedIn) {
                        result.width = splitRootRect.width;
                    }
                    if (this.lastSize != null && !this.lastSize.equals(size)) {
                        int hei = curView.getSlideBounds().height;
                        result.height = hei > size.height - viewRect.height ? size.height - size.height / 10 : hei;
                    }
                }
                slidedComp.setBounds(result);
            }
            this.lastSize = size;
        }

        @Override
        public Dimension minimumLayoutSize(Container parent) {
            return DesktopImpl.this.desktop.getMinimumSize();
        }

        @Override
        public Dimension preferredLayoutSize(Container parent) {
            return DesktopImpl.this.desktop.getPreferredSize();
        }

        @Override
        public void addLayoutComponent(String name, Component comp) {
        }

        @Override
        public void removeLayoutComponent(Component comp) {
        }
    }

}

