/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view;

import java.util.Map;
import org.netbeans.core.windows.view.ModeAccessor;
import org.openide.windows.TopComponent;

interface SlidingAccessor
extends ModeAccessor {
    public String getSide();

    public Map<TopComponent, Integer> getSlideInSizes();
}

