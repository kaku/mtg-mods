/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.settings.Convertor
 *  org.netbeans.spi.settings.Saver
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$Status
 *  org.openide.util.Lookup
 *  org.openide.xml.EntityCatalog
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.core.windows.view.ui.toolbars;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import org.netbeans.core.windows.view.ui.toolbars.ToolbarConfiguration;
import org.netbeans.core.windows.view.ui.toolbars.ToolbarConstraints;
import org.netbeans.core.windows.view.ui.toolbars.ToolbarRow;
import org.netbeans.spi.settings.Convertor;
import org.netbeans.spi.settings.Saver;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.util.Lookup;
import org.openide.xml.EntityCatalog;
import org.openide.xml.XMLUtil;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public final class ToolbarConvertor
extends Convertor {
    private static final String TAG_CONFIGURATION = "Configuration";
    private static final String TAG_ROW = "Row";
    private static final String TAG_TOOLBAR = "Toolbar";
    private static final String ATT_TOOLBAR_NAME = "name";
    private static final String ATT_TOOLBAR_ALIGNMENT = "align";
    private static final String ATT_TOOLBAR_VISIBLE = "visible";
    private static final String ATT_TOOLBAR_DRAGGABLE = "draggable";

    public static ToolbarConvertor create() {
        return new ToolbarConvertor();
    }

    public Object read(Reader r) throws IOException, ClassNotFoundException {
        Lookup lkp = ToolbarConvertor.findContext((Reader)r);
        FileObject fo = (FileObject)lkp.lookup(FileObject.class);
        String displayName = fo.getFileSystem().getStatus().annotateName(fo.getName(), Collections.singleton(fo));
        try {
            XMLReader reader = XMLUtil.createXMLReader((boolean)true);
            ToolbarParser parser = new ToolbarParser();
            reader.setContentHandler(parser);
            reader.setErrorHandler(parser);
            reader.setEntityResolver((EntityResolver)EntityCatalog.getDefault());
            reader.parse(new InputSource(r));
            return parser.createToolbarConfiguration(fo.getName(), displayName);
        }
        catch (SAXException saxE) {
            IOException ioE = new IOException();
            ioE.initCause(saxE);
            throw ioE;
        }
    }

    public void write(Writer writer, Object inst) throws IOException {
        if (!(inst instanceof ToolbarConfiguration)) {
            return;
        }
        BufferedWriter w = new BufferedWriter(writer);
        w.write("<?xml version=\"1.0\"?>");
        w.newLine();
        w.write("<!DOCTYPE Configuration PUBLIC \"-//NetBeans IDE//DTD toolbar 1.1//EN\" \"http://www.netbeans.org/dtds/toolbar1_1.dtd\">");
        w.newLine();
        w.write("<");
        w.write("Configuration");
        w.write(">");
        w.newLine();
        for (List<? extends ToolbarConstraints> row : ((ToolbarConfiguration)inst).getSnapshot()) {
            w.write("\t<");
            w.write("Row");
            w.write(">");
            w.newLine();
            for (ToolbarConstraints tc : row) {
                w.write("\t\t<");
                w.write("Toolbar");
                w.write(" ");
                w.write("name");
                w.write("=\"");
                w.write(tc.getName());
                w.write("\"");
                w.write(" ");
                w.write("visible");
                w.write("=\"");
                w.write(tc.isVisible() ? "true" : "false");
                w.write("\"");
                w.write(" ");
                w.write("draggable");
                w.write("=\"");
                w.write(tc.isDraggable() ? "true" : "false");
                w.write("\"");
                w.write(" ");
                w.write("align");
                w.write("=\"");
                w.write(tc.getAlign().toString());
                w.write("\"");
                w.write("/>");
                w.newLine();
            }
            w.write("\t</");
            w.write("Row");
            w.write(">");
            w.newLine();
        }
        w.write("</");
        w.write("Configuration");
        w.write(">");
        w.newLine();
        w.close();
    }

    public void registerSaver(Object inst, Saver s) {
        if (inst instanceof ToolbarConfiguration) {
            ((ToolbarConfiguration)inst).setSaverCallback(s);
        }
    }

    public void unregisterSaver(Object inst, Saver s) {
        if (inst instanceof ToolbarConfiguration) {
            ((ToolbarConfiguration)inst).setSaverCallback(null);
        }
    }

    private static class ToolbarParser
    extends DefaultHandler {
        private final List<ToolbarRow> rows = new ArrayList<ToolbarRow>(10);
        private ToolbarRow currentRow;

        private ToolbarParser() {
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if ("Row".equals(qName)) {
                this.currentRow = new ToolbarRow();
            } else if ("Toolbar".equals(qName)) {
                boolean draggable;
                String barName = attributes.getValue("name");
                if (null == barName || barName.trim().length() == 0) {
                    return;
                }
                boolean visible = !"false".equals(attributes.getValue("visible"));
                ToolbarConstraints.Align align = ToolbarConstraints.Align.fromString(attributes.getValue("align"));
                boolean bl = draggable = !"false".equals(attributes.getValue("draggable"));
                if ("QuickSearch".equals(barName)) {
                    align = ToolbarConstraints.Align.right;
                }
                ToolbarConstraints tc = new ToolbarConstraints(barName, align, visible, draggable);
                if (null != this.currentRow) {
                    this.currentRow.addConstraint(tc);
                }
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            if ("Row".equals(qName)) {
                if (null != this.currentRow && !this.currentRow.isEmpty()) {
                    this.rows.add(this.currentRow);
                }
                this.currentRow = null;
            }
        }

        private ToolbarConfiguration createToolbarConfiguration(String name, String displayName) {
            return new ToolbarConfiguration(name, displayName, this.rows);
        }
    }

}

