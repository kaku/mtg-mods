/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer
 *  org.netbeans.swing.tabcontrol.customtabs.Tabbed
 *  org.netbeans.swing.tabcontrol.customtabs.TabbedComponentFactory
 *  org.netbeans.swing.tabcontrol.customtabs.TabbedType
 *  org.openide.util.Lookup
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Shape;
import java.awt.Window;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.view.ModeView;
import org.netbeans.core.windows.view.ViewElement;
import org.netbeans.core.windows.view.dnd.TopComponentDraggable;
import org.netbeans.core.windows.view.dnd.TopComponentDroppable;
import org.netbeans.core.windows.view.dnd.WindowDnDManager;
import org.netbeans.core.windows.view.ui.AbstractModeContainer;
import org.netbeans.core.windows.view.ui.ModeComponent;
import org.netbeans.core.windows.view.ui.TabbedHandler;
import org.netbeans.core.windows.view.ui.tabcontrol.TabbedAdapter;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.customtabs.Tabbed;
import org.netbeans.swing.tabcontrol.customtabs.TabbedComponentFactory;
import org.netbeans.swing.tabcontrol.customtabs.TabbedType;
import org.openide.util.Lookup;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public final class DefaultSplitContainer
extends AbstractModeContainer {
    private final JPanel panel;

    public DefaultSplitContainer(ModeView modeView, WindowDnDManager windowDnDManager, int kind) {
        super(modeView, windowDnDManager, kind);
        this.panel = new ModePanel(this);
        this.panel.add(this.tabbedHandler.getComponent(), "Center");
    }

    @Override
    public void requestAttention(TopComponent tc) {
        this.tabbedHandler.requestAttention(tc);
    }

    @Override
    public void cancelRequestAttention(TopComponent tc) {
        this.tabbedHandler.cancelRequestAttention(tc);
    }

    @Override
    public void setAttentionHighlight(TopComponent tc, boolean highlight) {
        this.tabbedHandler.setAttentionHighlight(tc, highlight);
    }

    @Override
    public void makeBusy(TopComponent tc, boolean busy) {
        this.tabbedHandler.makeBusy(tc, busy);
    }

    @Override
    protected Component getModeComponent() {
        return this.panel;
    }

    @Override
    protected Tabbed createTabbed() {
        TabbedComponentFactory factory = (TabbedComponentFactory)Lookup.getDefault().lookup(TabbedComponentFactory.class);
        TabbedType type = this.getKind() == 1 ? TabbedType.EDITOR : TabbedType.VIEW;
        return factory.createTabbedComponent(type, (WinsysInfoForTabbedContainer)new TabbedAdapter.WinsysInfo(this.getKind()));
    }

    @Override
    protected void updateTitle(String title) {
    }

    @Override
    protected void updateActive(boolean active) {
        Window window;
        if (active && (window = SwingUtilities.getWindowAncestor(this.panel)) != null && !window.isActive() && WindowManagerImpl.getInstance().getEditorAreaState() == 1) {
            window.toFront();
        }
    }

    @Override
    public boolean isActive() {
        Window window = SwingUtilities.getWindowAncestor(this.panel);
        return window != null ? window.isActive() : false;
    }

    @Override
    protected boolean isAttachingPossible() {
        return true;
    }

    @Override
    protected TopComponentDroppable getModeDroppable() {
        return (ModePanel)this.panel;
    }

    static class ModePanel
    extends JPanel
    implements ModeComponent,
    TopComponentDroppable {
        private final AbstractModeContainer abstractModeContainer;

        public ModePanel(AbstractModeContainer abstractModeContainer) {
            super(new BorderLayout());
            this.abstractModeContainer = abstractModeContainer;
            this.enableEvents(16);
            if (UIManager.getBoolean("NbMainWindow.showCustomBackground")) {
                this.setOpaque(false);
            }
        }

        @Override
        public ModeView getModeView() {
            return this.abstractModeContainer.getModeView();
        }

        @Override
        public int getKind() {
            return this.abstractModeContainer.getKind();
        }

        @Override
        public Shape getIndicationForLocation(Point location) {
            return this.abstractModeContainer.getIndicationForLocation(location);
        }

        @Override
        public Object getConstraintForLocation(Point location) {
            return this.abstractModeContainer.getConstraintForLocation(location);
        }

        @Override
        public Component getDropComponent() {
            return this.abstractModeContainer.getDropComponent();
        }

        @Override
        public ViewElement getDropViewElement() {
            return this.abstractModeContainer.getDropModeView();
        }

        @Override
        public boolean canDrop(TopComponentDraggable transfer, Point location) {
            return this.abstractModeContainer.canDrop(transfer);
        }

        @Override
        public boolean supportsKind(TopComponentDraggable transfer) {
            if (transfer.isModeTransfer()) {
                ModeView mv = this.getModeView();
                Mode mode = WindowManagerImpl.getInstance().findMode(mv.getTopComponents().get(0));
                if (mode.getName().equals(transfer.getMode().getName())) {
                    return false;
                }
            }
            if (transfer.isAllowedToMoveAnywhere()) {
                return true;
            }
            boolean isNonEditor = transfer.getKind() == 0 || transfer.getKind() == 2;
            boolean thisIsNonEditor = this.getKind() == 0 || this.getKind() == 2;
            return isNonEditor == thisIsNonEditor;
        }
    }

}

