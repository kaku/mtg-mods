/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view.ui.slides;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import org.netbeans.core.windows.view.ui.slides.SlideOperation;

public final class SlideBarActionEvent
extends ActionEvent {
    private final MouseEvent mouseEvent;
    private final int tabIndex;
    private final SlideOperation slideOperation;

    public SlideBarActionEvent(Object source, String command, MouseEvent mouseEvent, int tabIndex) {
        this(source, command, null, mouseEvent, tabIndex);
    }

    public SlideBarActionEvent(Object source, String command, SlideOperation slideOperation) {
        this(source, command, slideOperation, null, -1);
    }

    public SlideBarActionEvent(Object source, String command, SlideOperation operation, MouseEvent mouseEvent, int tabIndex) {
        super(source, 1001, command);
        this.tabIndex = tabIndex;
        this.mouseEvent = mouseEvent;
        this.slideOperation = operation;
    }

    public MouseEvent getMouseEvent() {
        return this.mouseEvent;
    }

    public int getTabIndex() {
        return this.tabIndex;
    }

    public SlideOperation getSlideOperation() {
        return this.slideOperation;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("SlideBarActionEvent:");
        sb.append("Tab " + this.tabIndex + " " + this.getActionCommand());
        return sb.toString();
    }
}

