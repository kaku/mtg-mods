/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.core.windows.Debug;
import org.netbeans.core.windows.ModeStructureSnapshot;
import org.netbeans.core.windows.WindowSystemSnapshot;
import org.netbeans.core.windows.model.ModelElement;
import org.netbeans.core.windows.view.ControllerHandler;
import org.netbeans.core.windows.view.ElementAccessor;
import org.netbeans.core.windows.view.ModeAccessor;
import org.netbeans.core.windows.view.ModeStructureAccessor;
import org.netbeans.core.windows.view.ModeStructureAccessorImpl;
import org.netbeans.core.windows.view.SlidingAccessor;
import org.netbeans.core.windows.view.SplitAccessor;
import org.netbeans.core.windows.view.WindowSystemAccessor;
import org.netbeans.core.windows.view.WindowSystemAccessorImpl;
import org.openide.windows.TopComponent;

final class ViewHelper {
    private static final boolean DEBUG = Debug.isLoggable(ViewHelper.class);

    private ViewHelper() {
    }

    public static WindowSystemAccessor createWindowSystemAccessor(WindowSystemSnapshot wss) {
        if (wss == null) {
            return null;
        }
        WindowSystemAccessorImpl wsa = new WindowSystemAccessorImpl();
        ModeStructureAccessorImpl msa = ViewHelper.createModeStructureAccessor(wss.getModeStructureSnapshot());
        wsa.setModeStructureAccessor(msa);
        ModeStructureSnapshot.ModeSnapshot activeSnapshot = wss.getActiveModeSnapshot();
        wsa.setActiveModeAccessor(activeSnapshot == null ? null : msa.findModeAccessor(activeSnapshot.getName()));
        ModeStructureSnapshot.ModeSnapshot maximizedSnapshot = wss.getMaximizedModeSnapshot();
        wsa.setMaximizedModeAccessor(maximizedSnapshot == null ? null : msa.findModeAccessor(maximizedSnapshot.getName()));
        wsa.setMainWindowBoundsJoined(wss.getMainWindowBoundsJoined());
        wsa.setMainWindowBoundsSeparated(wss.getMainWindowBoundsSeparated());
        wsa.setEditorAreaBounds(wss.getEditorAreaBounds());
        wsa.setEditorAreaState(wss.getEditorAreaState());
        wsa.setEditorAreaFrameState(wss.getEditorAreaFrameState());
        wsa.setMainWindowFrameStateJoined(wss.getMainWindowFrameStateJoined());
        wsa.setMainWindowFrameStateSeparated(wss.getMainWindowFrameStateSeparated());
        wsa.setToolbarConfigurationName(wss.getToolbarConfigurationName());
        return wsa;
    }

    private static ModeStructureAccessorImpl createModeStructureAccessor(ModeStructureSnapshot mss) {
        ElementAccessor splitRoot = ViewHelper.createVisibleAccessor(mss.getSplitRootSnapshot());
        Set<ModeAccessor> separateModes = ViewHelper.createSeparateModeAccessors(mss.getSeparateModeSnapshots());
        Set<SlidingAccessor> slidingModes = ViewHelper.createSlidingModeAccessors(mss.getSlidingModeSnapshots());
        ModeStructureAccessorImpl msa = new ModeStructureAccessorImpl(splitRoot, separateModes, slidingModes);
        return msa;
    }

    private static Set<ModeAccessor> createSeparateModeAccessors(ModeStructureSnapshot.ModeSnapshot[] separateModeSnapshots) {
        HashSet<ModeAccessor> s = new HashSet<ModeAccessor>();
        for (int i = 0; i < separateModeSnapshots.length; ++i) {
            ModeStructureSnapshot.ModeSnapshot snapshot = separateModeSnapshots[i];
            if (!snapshot.isVisibleSeparate()) continue;
            s.add(new ModeStructureAccessorImpl.ModeAccessorImpl(snapshot.getOriginator(), snapshot));
        }
        return s;
    }

    private static Set<SlidingAccessor> createSlidingModeAccessors(ModeStructureSnapshot.SlidingModeSnapshot[] slidingModeSnapshots) {
        HashSet<SlidingAccessor> s = new HashSet<SlidingAccessor>();
        for (int i = 0; i < slidingModeSnapshots.length; ++i) {
            ModeStructureSnapshot.SlidingModeSnapshot snapshot = slidingModeSnapshots[i];
            s.add(new ModeStructureAccessorImpl.SlidingAccessorImpl(snapshot.getOriginator(), snapshot, snapshot.getSide(), snapshot.getSlideInSizes()));
        }
        return s;
    }

    private static ElementAccessor createVisibleAccessor(ModeStructureSnapshot.ElementSnapshot snapshot) {
        if (snapshot == null) {
            return null;
        }
        if (snapshot instanceof ModeStructureSnapshot.EditorSnapshot) {
            ModeStructureSnapshot.EditorSnapshot editorSnapshot = (ModeStructureSnapshot.EditorSnapshot)snapshot;
            return new ModeStructureAccessorImpl.EditorAccessorImpl(editorSnapshot.getOriginator(), editorSnapshot, ViewHelper.createVisibleAccessor(editorSnapshot.getEditorAreaSnapshot()), editorSnapshot.getResizeWeight());
        }
        if (snapshot.isVisibleInSplit()) {
            if (snapshot instanceof ModeStructureSnapshot.SplitSnapshot) {
                ModeStructureSnapshot.SplitSnapshot splitSnapshot = (ModeStructureSnapshot.SplitSnapshot)snapshot;
                return ViewHelper.createSplitAccessor(splitSnapshot);
            }
            if (snapshot instanceof ModeStructureSnapshot.ModeSnapshot) {
                ModeStructureSnapshot.ModeSnapshot modeSnapshot = (ModeStructureSnapshot.ModeSnapshot)snapshot;
                return new ModeStructureAccessorImpl.ModeAccessorImpl(modeSnapshot.getOriginator(), modeSnapshot);
            }
        } else if (snapshot instanceof ModeStructureSnapshot.SplitSnapshot) {
            ModeStructureSnapshot.SplitSnapshot splitSnapshot = (ModeStructureSnapshot.SplitSnapshot)snapshot;
            for (ModeStructureSnapshot.ElementSnapshot child : splitSnapshot.getChildSnapshots()) {
                if (!child.hasVisibleDescendant()) continue;
                return ViewHelper.createVisibleAccessor(child);
            }
        }
        return null;
    }

    private static ElementAccessor createSplitAccessor(ModeStructureSnapshot.SplitSnapshot splitSnapshot) {
        List visibleChildren = splitSnapshot.getVisibleChildSnapshots();
        ArrayList<ElementAccessor> children = new ArrayList<ElementAccessor>(visibleChildren.size());
        ArrayList<Double> weights = new ArrayList<Double>(visibleChildren.size());
        int index = 0;
        for (ModeStructureSnapshot.ElementSnapshot child : visibleChildren) {
            ElementAccessor childAccessor = ViewHelper.createVisibleAccessor(child);
            double weight = splitSnapshot.getChildSnapshotSplitWeight(child);
            if (childAccessor instanceof SplitAccessor && ((SplitAccessor)childAccessor).getOrientation() == splitSnapshot.getOrientation()) {
                SplitAccessor subSplit = (SplitAccessor)childAccessor;
                ElementAccessor[] childrenToMerge = subSplit.getChildren();
                double[] weightsToMerge = subSplit.getSplitWeights();
                for (int j = 0; j < childrenToMerge.length; ++j) {
                    children.add(childrenToMerge[j]);
                    weights.add(weightsToMerge[j] * weight);
                }
            } else {
                children.add(childAccessor);
                weights.add(weight);
            }
            ++index;
        }
        ElementAccessor[] childrenAccessors = new ElementAccessor[children.size()];
        double[] splitWeights = new double[children.size()];
        for (int i = 0; i < children.size(); ++i) {
            ElementAccessor ea = (ElementAccessor)children.get(i);
            Double weight = (Double)weights.get(i);
            childrenAccessors[i] = ea;
            splitWeights[i] = weight;
        }
        return new ModeStructureAccessorImpl.SplitAccessorImpl(splitSnapshot.getOriginator(), splitSnapshot, splitSnapshot.getOrientation(), splitWeights, childrenAccessors, splitSnapshot.getResizeWeight());
    }

    public static void setSplitWeights(SplitAccessor splitAccessor, ElementAccessor[] children, double[] splitWeights, ControllerHandler controllerHandler) {
        ModeStructureSnapshot.SplitSnapshot splitSnapshot = (ModeStructureSnapshot.SplitSnapshot)splitAccessor.getSnapshot();
        if (splitSnapshot == null) {
            return;
        }
        ModelElement[] elements = new ModelElement[children.length];
        for (int i = 0; i < children.length; ++i) {
            ModeStructureSnapshot.ElementSnapshot snapshot = ViewHelper.findVisibleSplitSnapshot(children[i].getSnapshot());
            elements[i] = snapshot.getOriginator();
            splitWeights[i] = ViewHelper.correctNestedSplitWeight(snapshot.getParent(), splitWeights[i]);
        }
        controllerHandler.userChangedSplit(elements, splitWeights);
    }

    private static double correctNestedSplitWeight(ModeStructureSnapshot.SplitSnapshot split, double splitWeight) {
        ModeStructureSnapshot.SplitSnapshot parentSplit;
        int nestedSplitOrientation = split.getOrientation();
        for (parentSplit = split.getParent(); null != parentSplit && !parentSplit.isVisibleInSplit(); parentSplit = parentSplit.getParent()) {
            split = parentSplit;
        }
        if (null != parentSplit && parentSplit.getOrientation() == nestedSplitOrientation) {
            double parentSplitWeight = parentSplit.getChildSnapshotSplitWeight(split);
            if (parentSplit.getVisibleChildSnapshots().size() > 1 && parentSplitWeight > 0.0) {
                splitWeight /= parentSplitWeight;
            }
            return ViewHelper.correctNestedSplitWeight(parentSplit, splitWeight);
        }
        return splitWeight;
    }

    private static ModeStructureSnapshot.ElementSnapshot findVisibleSplitSnapshot(ModeStructureSnapshot.ElementSnapshot snapshot) {
        List visibleChildren;
        ModeStructureSnapshot.SplitSnapshot parent = snapshot.getParent();
        if (null != parent && (visibleChildren = parent.getVisibleChildSnapshots()).size() == 1) {
            return ViewHelper.findVisibleSplitSnapshot(parent);
        }
        return snapshot;
    }

    private static void debugLog(String message) {
        Debug.log(ViewHelper.class, message);
    }
}

