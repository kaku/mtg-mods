/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view.dnd;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Set;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.view.Controller;
import org.netbeans.core.windows.view.ModeView;
import org.netbeans.core.windows.view.SlidingView;
import org.netbeans.core.windows.view.dnd.DropTargetGlassPane;
import org.netbeans.core.windows.view.dnd.TopComponentDraggable;
import org.netbeans.core.windows.view.dnd.TopComponentDroppable;
import org.netbeans.core.windows.view.dnd.WindowDnDManager;
import org.netbeans.core.windows.view.ui.ModeComponent;
import org.openide.windows.TopComponent;

final class KeyboardDnd
implements AWTEventListener,
PropertyChangeListener {
    private static KeyboardDnd currentDnd;
    private final WindowDnDManager dndManager;
    private final TopComponentDraggable draggable;
    private final WindowDnDManager.ViewAccessor viewAccessor;
    private final ArrayList<TopComponentDroppable> targets = new ArrayList(30);
    private int currentIndex;
    private ComponentSide currentSide;
    private DropTargetGlassPane lastGlass;

    private KeyboardDnd(WindowDnDManager dndManager, TopComponentDraggable draggable, WindowDnDManager.ViewAccessor viewAccessor) {
        this.currentSide = new ComponentSide(Side.center);
        this.lastGlass = null;
        this.dndManager = dndManager;
        this.draggable = draggable;
        this.viewAccessor = viewAccessor;
    }

    static void start(WindowDnDManager dndManager, TopComponentDraggable draggable, WindowDnDManager.ViewAccessor viewAccessor) {
        if (!SwingUtilities.isEventDispatchThread()) {
            throw new IllegalStateException("This method must be called from EDT.");
        }
        if (null != currentDnd) {
            currentDnd.stop(false);
            currentDnd = null;
        }
        currentDnd = new KeyboardDnd(dndManager, draggable, viewAccessor);
        currentDnd.start();
    }

    private void start() {
        Toolkit.getDefaultToolkit().addAWTEventListener(this, 8);
        TopComponent.getRegistry().addPropertyChangeListener((PropertyChangeListener)this);
        this.dndManager.dragStarting(null, new Point(0, 0), this.draggable);
        for (Component c : this.viewAccessor.getModeComponents()) {
            TopComponentDroppable droppable;
            TopComponent tc;
            ModeView mv;
            ModeComponent mc;
            if (!(c instanceof ModeComponent) || !(c instanceof TopComponentDroppable) || (mv = (mc = (ModeComponent)((Object)c)).getModeView()) instanceof SlidingView || null == (tc = mv.getSelectedTopComponent()) || !(droppable = (TopComponentDroppable)((Object)c)).supportsKind(this.draggable)) continue;
            this.targets.add(droppable);
        }
        Collections.sort(this.targets, new Comparator<TopComponentDroppable>(){

            @Override
            public int compare(TopComponentDroppable d1, TopComponentDroppable d2) {
                boolean floating1 = KeyboardDnd.isDroppableFloating(d1);
                boolean floating2 = KeyboardDnd.isDroppableFloating(d2);
                if (floating1 && !floating2) {
                    return 1;
                }
                if (!floating1 && floating2) {
                    return -1;
                }
                if (!d1.getDropComponent().isShowing() || !d2.getDropComponent().isShowing()) {
                    return 0;
                }
                Point loc1 = d1.getDropComponent().getLocationOnScreen();
                Point loc2 = d2.getDropComponent().getLocationOnScreen();
                int res = loc1.x - loc2.x;
                if (res == 0) {
                    res = loc1.y - loc2.y;
                }
                return res;
            }
        });
        this.targets.add(this.dndManager.getCenterPanelDroppable());
        this.currentIndex = 0;
        this.refresh();
    }

    static void abort() {
        if (null != currentDnd) {
            currentDnd.stop(false);
        }
    }

    private void refresh() {
        TopComponentDroppable droppable = this.targets.get(this.currentIndex);
        DropTargetGlassPane glass = this.findGlassPane(droppable);
        if (null == glass) {
            return;
        }
        Point dropLocation = this.currentSide.getDropLocation(droppable);
        dropLocation = SwingUtilities.convertPoint(droppable.getDropComponent(), dropLocation, glass);
        glass.dragOver(dropLocation, droppable);
        if (null != this.lastGlass && this.lastGlass != glass) {
            this.lastGlass.clearIndications();
        }
        this.lastGlass = glass;
    }

    private DropTargetGlassPane findGlassPane(TopComponentDroppable droppable) {
        Component c;
        Component dropC = droppable.getDropComponent();
        if (dropC instanceof JComponent && (c = ((JComponent)dropC).getRootPane().getGlassPane()) instanceof DropTargetGlassPane) {
            return (DropTargetGlassPane)c;
        }
        return null;
    }

    @Override
    public void eventDispatched(AWTEvent e) {
        if (!(e instanceof KeyEvent)) {
            return;
        }
        KeyEvent ke = (KeyEvent)e;
        ke.consume();
        if (e.getID() == 401) {
            switch (ke.getKeyCode()) {
                case 37: {
                    do {
                        this.currentSide = this.currentSide.moveLeft();
                    } while (!this.checkDropLocation());
                    this.refresh();
                    break;
                }
                case 39: {
                    do {
                        this.currentSide = this.currentSide.moveRight();
                    } while (!this.checkDropLocation());
                    this.refresh();
                    break;
                }
                case 38: {
                    do {
                        this.currentSide = this.currentSide.moveUp();
                    } while (!this.checkDropLocation());
                    this.refresh();
                    break;
                }
                case 40: {
                    do {
                        this.currentSide = this.currentSide.moveDown();
                    } while (!this.checkDropLocation());
                    this.refresh();
                    break;
                }
                case 10: {
                    this.stop(true);
                    break;
                }
                case 27: {
                    KeyboardDnd.abort();
                }
            }
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("activated".equals(evt.getPropertyName()) || "opened".equals(evt.getPropertyName())) {
            KeyboardDnd.abort();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void stop(boolean commitChanges) {
        Toolkit.getDefaultToolkit().removeAWTEventListener(this);
        TopComponent.getRegistry().removePropertyChangeListener((PropertyChangeListener)this);
        try {
            Point dropLocation;
            TopComponentDroppable droppable;
            if (commitChanges && (droppable = this.targets.get(this.currentIndex)).canDrop(this.draggable, dropLocation = this.currentSide.getDropLocation(droppable))) {
                WindowDnDManager.performDrop(this.dndManager.getController(), droppable, this.draggable, dropLocation);
            }
        }
        finally {
            this.dndManager.dragFinished();
            this.dndManager.dragFinishedEx();
            if (currentDnd == this) {
                currentDnd = null;
            }
        }
    }

    private boolean checkDropLocation() {
        TopComponentDroppable droppable = this.targets.get(this.currentIndex);
        DropTargetGlassPane glass = this.findGlassPane(droppable);
        if (null == glass) {
            return false;
        }
        Point dropLocation = this.currentSide.getDropLocation(droppable);
        if (!droppable.canDrop(this.draggable, dropLocation)) {
            return false;
        }
        return droppable.getIndicationForLocation(dropLocation) != null;
    }

    private void decrementIndex() {
        --this.currentIndex;
        if (this.currentIndex < 0) {
            this.currentIndex = this.targets.size() - 1;
        }
    }

    private void incrementIndex() {
        ++this.currentIndex;
        if (this.currentIndex > this.targets.size() - 1) {
            this.currentIndex = 0;
        }
    }

    private boolean isCurrentDroppableFloating() {
        TopComponentDroppable droppable = this.targets.get(this.currentIndex);
        return KeyboardDnd.isDroppableFloating(droppable);
    }

    private static boolean isDroppableFloating(TopComponentDroppable droppable) {
        return droppable instanceof JDialog;
    }

    private static enum Side {
        left,
        right,
        top,
        bottom,
        center;
        

        private Side() {
        }
    }

    private class ComponentSide {
        private final Side side;

        public ComponentSide(Side side) {
            this.side = side;
        }

        ComponentSide moveLeft() {
            if (KeyboardDnd.this.isCurrentDroppableFloating()) {
                KeyboardDnd.this.decrementIndex();
                return new ComponentSide(Side.center);
            }
            switch (this.side) {
                case right: {
                    return new ComponentSide(Side.center);
                }
                case left: {
                    KeyboardDnd.this.decrementIndex();
                    return new ComponentSide(Side.right);
                }
            }
            return new ComponentSide(Side.left);
        }

        ComponentSide moveRight() {
            if (KeyboardDnd.this.isCurrentDroppableFloating()) {
                KeyboardDnd.this.incrementIndex();
                return new ComponentSide(Side.center);
            }
            switch (this.side) {
                case right: {
                    KeyboardDnd.this.incrementIndex();
                    return new ComponentSide(Side.left);
                }
                case left: {
                    return new ComponentSide(Side.center);
                }
            }
            return new ComponentSide(Side.right);
        }

        ComponentSide moveDown() {
            if (KeyboardDnd.this.isCurrentDroppableFloating()) {
                KeyboardDnd.this.incrementIndex();
                return new ComponentSide(Side.center);
            }
            switch (this.side) {
                case bottom: {
                    KeyboardDnd.this.incrementIndex();
                    return new ComponentSide(Side.top);
                }
                case top: {
                    return new ComponentSide(Side.center);
                }
            }
            return new ComponentSide(Side.bottom);
        }

        ComponentSide moveUp() {
            if (KeyboardDnd.this.isCurrentDroppableFloating()) {
                KeyboardDnd.this.decrementIndex();
                return new ComponentSide(Side.center);
            }
            switch (this.side) {
                case top: {
                    KeyboardDnd.this.decrementIndex();
                    return new ComponentSide(Side.top);
                }
                case bottom: {
                    return new ComponentSide(Side.center);
                }
            }
            return new ComponentSide(Side.top);
        }

        Point getDropLocation(TopComponentDroppable droppable) {
            Point res = this.getDropLocation(droppable, 30);
            Shape indication = droppable.getIndicationForLocation(res);
            if (null == indication) {
                res = this.getDropLocation(droppable, 5);
            }
            return res;
        }

        Point getDropLocation(TopComponentDroppable droppable, int dropMargin) {
            Dimension size = droppable.getDropComponent().getSize();
            Point res = new Point();
            switch (this.side) {
                case center: {
                    res.x = size.width / 2;
                    res.y = size.height / 2;
                    break;
                }
                case top: {
                    res.x = size.width / 2;
                    res.y = dropMargin;
                    break;
                }
                case bottom: {
                    res.x = size.width / 2;
                    res.y = size.height - dropMargin;
                    break;
                }
                case left: {
                    res.x = dropMargin;
                    res.y = size.height / 2;
                    break;
                }
                case right: {
                    res.x = size.width - dropMargin;
                    res.y = size.height / 2;
                }
            }
            return res;
        }
    }

}

