/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlRenderer
 *  org.openide.awt.HtmlRenderer$Renderer
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.core.windows.view.ui.popupswitcher;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.lang.ref.SoftReference;
import javax.accessibility.AccessibleContext;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.netbeans.core.windows.view.ui.popupswitcher.Item;
import org.netbeans.core.windows.view.ui.popupswitcher.KeyboardPopupSwitcher;
import org.netbeans.core.windows.view.ui.popupswitcher.Model;
import org.netbeans.core.windows.view.ui.popupswitcher.PopupSwitcher;
import org.openide.awt.HtmlRenderer;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

class Table
extends JTable {
    private static final Border rendererBorder = BorderFactory.createEmptyBorder(2, 5, 0, 5);
    private static final Icon NULL_ICON = new NullIcon(16);
    private Color foreground;
    private Color background;
    private Color selForeground;
    private Color selBackground;
    private static final int MAX_VISIBLE_ROWS = 20;
    private static final int MAX_TOP_COLUMN_WIDTH = 450;
    private static final int MAX_SUB_COLUMN_WIDTH = 225;
    private boolean needCalcRowHeight = true;
    private Dimension prefSize = null;
    private final boolean showIcons;
    private final JPanel topItemPanel = new JPanel();
    private final JLabel rightArrowLabel = new JLabel();
    private static SoftReference<BufferedImage> ctx = null;

    public Table(Model model) {
        super(model);
        this.showIcons = model.hasIcons();
        this.init();
    }

    private void init() {
        this.setOpaque(false);
        this.getSelectionModel().clearSelection();
        this.getSelectionModel().setAnchorSelectionIndex(-1);
        this.getSelectionModel().setLeadSelectionIndex(-1);
        this.setAutoscrolls(false);
        this.setShowHorizontalLines(false);
        this.setShowVerticalLines(false);
        this.setAutoResizeMode(0);
        this.setTableHeader(null);
        this.calcRowHeight(Table.getOffscreenGraphics());
        this.addMouseListener(new MouseAdapter(){

            @Override
            public void mousePressed(MouseEvent e) {
                int row = Table.this.rowAtPoint(e.getPoint());
                int col = Table.this.columnAtPoint(e.getPoint());
                if (row >= 0 && col >= 0 && Table.this.select(row, col)) {
                    Table.this.performSwitching();
                }
            }
        });
        this.rightArrowLabel.setIcon(new ArrowIcon());
        this.rightArrowLabel.setIconTextGap(2);
        this.rightArrowLabel.setHorizontalTextPosition(2);
        this.topItemPanel.setLayout(new BorderLayout(5, 0));
        this.topItemPanel.add((Component)this.rightArrowLabel, "East");
        this.topItemPanel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(Table.class, (String)"ACD_OTHER_EDITORS"));
        this.topItemPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 2));
        for (int col = 0; col < this.getColumnCount(); ++col) {
            if (!this.getSwitcherModel().isTopItemColumn(col)) continue;
            this.adjustColumnWidths(col);
        }
        int maxRowCount = this.getSwitcherModel().getMaxRowCount();
        if (maxRowCount > 20 && this.getRowCount() <= 20) {
            JScrollPane scroll = new JScrollPane();
            scroll.setVerticalScrollBarPolicy(22);
            int scrollWidth = scroll.getVerticalScrollBar().getPreferredSize().width;
            TableColumn tc = this.getColumnModel().getColumn(this.getColumnCount() - 1);
            tc.setMaxWidth(tc.getMaxWidth() + scrollWidth);
            tc.setPreferredWidth(tc.getPreferredWidth() + scrollWidth);
            tc.setWidth(tc.getWidth() + scrollWidth);
        }
    }

    private void adjustColumnWidths(int topColumn) {
        TableColumnModel colModel = this.getColumnModel();
        int colWidth = 0;
        int subColWidth = -1;
        for (int row = 0; row < this.getRowCount(); ++row) {
            Item item = (Item)this.getValueAt(row, topColumn);
            Component ren = this.prepareRenderer(this.getCellRenderer(row, topColumn), row, topColumn, item, true);
            int prefWidth = ren.getPreferredSize().width;
            colWidth = Math.max(colWidth, prefWidth);
            if (null == item || !item.hasSubItems() || topColumn + 1 >= this.getColumnCount() || this.getSwitcherModel().isTopItemColumn(topColumn + 1)) continue;
            Item[] subItems = item.getActivatableSubItems();
            for (int i = 0; i < subItems.length; ++i) {
                ren = this.prepareRenderer(this.getCellRenderer(0, topColumn + 1), 0, topColumn + 1, subItems[i], true);
                prefWidth = ren.getPreferredSize().width;
                subColWidth = Math.max(subColWidth, prefWidth);
            }
        }
        colWidth = Math.min(colWidth, 450);
        TableColumn tc = colModel.getColumn(topColumn);
        tc.setPreferredWidth(colWidth);
        tc.setWidth(colWidth);
        tc.setMaxWidth(colWidth);
        if (subColWidth > 0) {
            subColWidth = Math.min(subColWidth, 225);
            tc = colModel.getColumn(topColumn + 1);
            tc.setPreferredWidth(subColWidth);
            tc.setWidth(subColWidth);
            tc.setMaxWidth(subColWidth);
        }
    }

    @Override
    protected TableColumnModel createDefaultColumnModel() {
        return super.createDefaultColumnModel();
    }

    @Override
    public void createDefaultColumnsFromModel() {
        super.createDefaultColumnsFromModel();
    }

    void setInitialSelection(int hits, boolean forward) {
        int initialRow;
        ++hits;
        int direction = forward ? 1 : -1;
        int initialColumn = this.getSwitcherModel().getInitialColumn();
        int n = initialRow = forward ? 0 : this.getSwitcherModel().getRowCount(initialColumn) - 1;
        while (initialRow > 0 && null == this.getValueAt(initialRow, initialColumn)) {
            --initialRow;
        }
        initialRow += hits * direction;
        initialRow = Math.max(0, initialRow);
        initialRow = Math.min(initialRow, this.getSwitcherModel().getRowCount(initialColumn) - 1);
        this.select(initialRow, initialColumn);
    }

    boolean select(int row, int col) {
        if (null == this.getValueAt(row, col)) {
            return false;
        }
        this.changeSelection(row, col, false, false);
        return true;
    }

    void performSwitching() {
        int selRow = this.getSelectedRow();
        int selCol = this.getSelectedColumn();
        if (selRow < 0 || selCol < 0) {
            return;
        }
        Item selItem = (Item)this.getSwitcherModel().getValueAt(selRow, selCol);
        if (null != selItem) {
            selItem.activate();
            KeyboardPopupSwitcher.hidePopup();
        }
    }

    @Override
    public void updateUI() {
        this.needCalcRowHeight = true;
        super.updateUI();
    }

    @Override
    public void setFont(Font f) {
        this.needCalcRowHeight = true;
        super.setFont(f);
    }

    @Override
    public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        Item item = (Item)this.getSwitcherModel().getValueAt(row, column);
        boolean selected = row == this.getSelectedRow() && column == this.getSelectedColumn() && item != null;
        return this.prepareRenderer(renderer, row, column, item, selected);
    }

    private Component prepareRenderer(TableCellRenderer renderer, int row, int column, Item item, boolean selected) {
        Component ren = renderer.getTableCellRendererComponent(this, item, selected, selected, row, column);
        if (null == item) {
            if (ren instanceof JLabel) {
                ((JLabel)ren).setOpaque(false);
                ((JLabel)ren).setIcon(null);
                ((JLabel)ren).getAccessibleContext().setAccessibleDescription(null);
            }
            return ren;
        }
        return this.configureRenderer(ren, item, selected);
    }

    private Component configureRenderer(Component ren, Item item, boolean selected) {
        if (!(ren instanceof JLabel)) {
            return ren;
        }
        JLabel lbl = (JLabel)ren;
        JLabel prototype = (JLabel)ren;
        lbl = HtmlRenderer.createLabel();
        if (lbl instanceof HtmlRenderer.Renderer) {
            ((HtmlRenderer.Renderer)lbl).setRenderStyle(1);
        }
        lbl.setForeground(prototype.getForeground());
        lbl.setBackground(prototype.getBackground());
        lbl.setFont(prototype.getFont());
        ren = lbl;
        lbl.getAccessibleContext().setAccessibleDescription(null);
        Icon icon = null;
        if (item.isTopItem()) {
            icon = item.getIcon();
            if (this.showIcons && (icon == null || icon.getIconWidth() == 0)) {
                icon = NULL_ICON;
            }
        }
        lbl.setText(selected ? Table.stripHtml(item.getDisplayName()) : item.getDisplayName());
        lbl.setBorder(rendererBorder);
        lbl.setIcon(icon);
        if (null != icon && item.isTopItem()) {
            lbl.setIconTextGap(26 - icon.getIconWidth());
        }
        lbl.setOpaque(true);
        if (item.isTopItem() && item.hasSubItems() && (selected || item.isParentOf(this.getSelectedItem()))) {
            ren = this.configureTopItemRenderer(lbl, item);
        }
        return ren;
    }

    private Component configureTopItemRenderer(JLabel ren, Item item) {
        Item activeSubItem = item.getActiveSubItem();
        if (null != activeSubItem) {
            this.rightArrowLabel.setText(Table.truncateSubItemText(activeSubItem.getDisplayName()));
            this.rightArrowLabel.setForeground(ren.getForeground());
            this.rightArrowLabel.setBackground(ren.getBackground());
        } else {
            this.rightArrowLabel.setText(null);
        }
        this.topItemPanel.setBackground(ren.getBackground());
        this.topItemPanel.removeAll();
        this.topItemPanel.add((Component)ren, "Center");
        this.topItemPanel.add((Component)this.rightArrowLabel, "East");
        return this.topItemPanel;
    }

    static String stripHtml(String htmlText) {
        if (null == htmlText) {
            return null;
        }
        String res = htmlText.replaceAll("<[^>]*>", "");
        res = res.replaceAll("&nbsp;", " ");
        res = res.trim();
        return res;
    }

    private static String truncateSubItemText(String text) {
        text = Table.stripHtml(text);
        StringBuilder sb = new StringBuilder("[" + text + "]");
        JLabel lbl = new JLabel(sb.toString());
        while (lbl.getPreferredSize().width > 225 && sb.length() > 3) {
            if (sb.charAt(sb.length() - 2) != '\u2026') {
                sb.insert(sb.length() - 1, '\u2026');
            }
            sb.deleteCharAt(sb.length() - 3);
            lbl.setText(sb.toString());
        }
        return sb.toString();
    }

    void nextRow() {
        this.changeRow(1);
    }

    void previousRow() {
        this.changeRow(-1);
    }

    private void changeRow(int step) {
        int selCol = Math.max(this.getSelectedColumn(), 0);
        int selRow = Math.max(this.getSelectedRow(), 0);
        if ((selRow += step) < 0) {
            if (!this.changeColumn(step, false)) {
                this.changeColumn(2 * step, false);
            }
            return;
        }
        if (selRow > this.getRowCount() || null == this.getValueAt(selRow, selCol)) {
            if (this.getSwitcherModel().isTopItemColumn(selCol)) {
                if (!this.getSwitcherModel().isTopItemColumn(selCol + step)) {
                    step *= 2;
                }
                this.changeColumn(step, false);
            } else if (!this.select(selRow, selCol - 1)) {
                this.changeColumn(step, false);
            }
            return;
        }
        this.select(selRow, selCol);
    }

    void nextColumn() {
        if (!this.changeColumn(1, true)) {
            this.changeColumn(2, true);
        }
    }

    void previousColumn() {
        if (!this.changeColumn(-1, true)) {
            this.changeColumn(-2, true);
        }
    }

    private boolean changeColumn(int step, boolean keepRowSelection) {
        int selCol = Math.max(this.getSelectedColumn(), 0);
        if ((selCol += step) < 0) {
            selCol = this.getColumnCount() - 1;
        }
        if (selCol >= this.getColumnCount()) {
            selCol = 0;
        }
        int selRow = this.getSelectedRow();
        Model m = this.getSwitcherModel();
        if (!keepRowSelection) {
            selRow = step > 0 ? 0 : this.getRowCount() - 1;
        } else if (step == -1 && !m.isTopItemColumn(this.getSelectedColumn())) {
            Item parent;
            Item child = this.getSelectedItem();
            while (selRow > 0 && null != (parent = (Item)this.getValueAt(selRow--, this.getSelectedColumn() - 1)) && !parent.isParentOf(child)) {
            }
        }
        while (selRow > 0 && null == this.getValueAt(selRow, selCol)) {
            --selRow;
        }
        return this.select(selRow, selCol);
    }

    @Override
    public Color getForeground() {
        if (this.foreground == null) {
            this.foreground = PopupSwitcher.getDefaultForeground();
        }
        return this.foreground != null ? this.foreground : super.getForeground();
    }

    @Override
    public Color getBackground() {
        if (this.background == null) {
            this.background = PopupSwitcher.getDefaultBackground();
        }
        return this.background != null ? this.background : super.getBackground();
    }

    @Override
    public Color getSelectionForeground() {
        if (this.selForeground == null) {
            this.selForeground = PopupSwitcher.getSelectionForeground();
        }
        return this.selForeground != null ? this.selForeground : super.getSelectionForeground();
    }

    @Override
    public Color getSelectionBackground() {
        if (this.selBackground == null) {
            this.selBackground = PopupSwitcher.getSelectionBackground();
        }
        return this.selBackground != null ? this.selBackground : super.getSelectionBackground();
    }

    private void calcRowHeight(Graphics g) {
        Font f = this.getFont();
        FontMetrics fm = g.getFontMetrics(f);
        int height = Math.max(fm.getHeight(), 16) + 4;
        this.needCalcRowHeight = false;
        this.setRowHeight(height);
    }

    private static Graphics2D getOffscreenGraphics() {
        BufferedImage result = null;
        if (ctx != null) {
            result = ctx.get();
        }
        if (result == null) {
            result = new BufferedImage(10, 10, 1);
            ctx = new SoftReference<BufferedImage>(result);
        }
        return (Graphics2D)result.getGraphics();
    }

    @Override
    public Dimension getPreferredScrollableViewportSize() {
        if (this.needCalcRowHeight) {
            this.calcRowHeight(Table.getOffscreenGraphics());
            this.prefSize = null;
        }
        if (null == this.prefSize) {
            Dimension dim = new Dimension();
            for (int i = 0; i < this.getColumnCount(); ++i) {
                TableColumn tc = this.getColumnModel().getColumn(i);
                dim.width += tc.getPreferredWidth();
            }
            int rowCount = Math.min(20, this.getRowCount());
            dim.height = rowCount * this.getRowHeight();
            Rectangle screen = Utilities.getUsableScreenBounds();
            dim.width = Math.min(dim.width, screen.width - 100);
            dim.height = Math.min(dim.height, screen.height - 100);
            this.prefSize = dim;
        }
        return this.prefSize;
    }

    private Model getSwitcherModel() {
        return (Model)this.getModel();
    }

    public Item getSelectedItem() {
        int row = this.getSelectedRow();
        int col = this.getSelectedColumn();
        if (row < 0 || col < 0) {
            return null;
        }
        return (Item)this.getValueAt(row, col);
    }

    @Override
    public void paint(Graphics g) {
        if (this.needCalcRowHeight) {
            this.calcRowHeight(g);
        }
        super.paint(g);
        this.paintVerticalLine(g);
    }

    private void paintVerticalLine(Graphics g) {
        Rectangle clip = g.getClipBounds();
        Rectangle bounds = this.getBounds();
        bounds.y = 0;
        bounds.x = 0;
        if (this.getRowCount() <= 0 || this.getColumnCount() <= 0 || !bounds.intersects(clip)) {
            return;
        }
        boolean ltr = this.getComponentOrientation().isLeftToRight();
        Point upperLeft = clip.getLocation();
        if (!ltr) {
            ++upperLeft.x;
        }
        Point lowerRight = new Point(clip.x + clip.width - (ltr ? 1 : 0), clip.y + clip.height);
        int rMin = this.rowAtPoint(upperLeft);
        int rMax = this.rowAtPoint(lowerRight);
        if (rMin == -1) {
            rMin = 0;
        }
        if (rMax == -1) {
            rMax = this.getRowCount() - 1;
        }
        int cMin = this.columnAtPoint(ltr ? upperLeft : lowerRight);
        int cMax = this.columnAtPoint(ltr ? lowerRight : upperLeft);
        if (cMin == -1) {
            cMin = 0;
        }
        if (cMax == -1) {
            cMax = this.getColumnCount() - 1;
        }
        g.setColor(this.getGridColor());
        Rectangle minCell = this.getCellRect(rMin, cMin, true);
        Rectangle maxCell = this.getCellRect(rMax, cMax, true);
        Rectangle damagedArea = minCell.union(maxCell);
        TableColumnModel cm = this.getColumnModel();
        int tableHeight = damagedArea.y + damagedArea.height;
        if (this.getComponentOrientation().isLeftToRight()) {
            int x = damagedArea.x;
            for (int column = cMin; column <= cMax; ++column) {
                int w = cm.getColumn(column).getWidth();
                x += w;
                if (!this.getSwitcherModel().isTopItemColumn(column + 1) || column <= 0) continue;
                g.drawLine(x - 1, 0, x - 1, tableHeight - 1);
            }
        } else {
            int x = damagedArea.x;
            for (int column = cMax; column >= cMin; --column) {
                int w = cm.getColumn(column).getWidth();
                x += w;
                if (!this.getSwitcherModel().isTopItemColumn(column + 1) || column <= 0) continue;
                g.drawLine(x - 1, 0, x - 1, tableHeight - 1);
            }
        }
    }

    @Override
    public void changeSelection(int rowIndex, int columnIndex, boolean toggle, boolean extend) {
        if (null == this.getModel().getValueAt(rowIndex, columnIndex)) {
            return;
        }
        super.changeSelection(rowIndex, columnIndex, false, false);
        this.getSwitcherModel().setCurrentSelection(rowIndex, columnIndex);
        Rectangle rect = this.getCellRect(rowIndex, columnIndex, true);
        Rectangle visible = new Rectangle();
        this.computeVisibleRect(visible);
        if (visible.width > 0 && visible.height > 0 && !visible.contains(rect)) {
            this.scrollRectToVisible(rect);
        }
    }

    private static class ArrowIcon
    implements Icon {
        private static final int SIZE = 10;

        private ArrowIcon() {
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            g.setColor(c.getForeground());
            Polygon s = new Polygon();
            s.addPoint(x, y);
            s.addPoint(x + 5, y + 5);
            s.addPoint(x, y + 10);
            s.addPoint(x, y);
            g.fillPolygon(s);
        }

        @Override
        public int getIconWidth() {
            return 10;
        }

        @Override
        public int getIconHeight() {
            return 10;
        }
    }

    private static class NullIcon
    implements Icon {
        private final int size;

        public NullIcon(int size) {
            this.size = size;
        }

        @Override
        public int getIconWidth() {
            return this.size;
        }

        @Override
        public int getIconHeight() {
            return this.size;
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
        }
    }

}

