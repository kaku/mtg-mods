/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer
 *  org.netbeans.swing.tabcontrol.customtabs.Tabbed
 *  org.netbeans.swing.tabcontrol.customtabs.TabbedComponentFactory
 *  org.netbeans.swing.tabcontrol.customtabs.TabbedType
 */
package org.netbeans.core.windows.view.ui;

import java.util.prefs.Preferences;
import javax.swing.UIManager;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.options.WinSysPrefs;
import org.netbeans.core.windows.view.ui.tabcontrol.JTabbedPaneAdapter;
import org.netbeans.core.windows.view.ui.tabcontrol.TabbedAdapter;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.customtabs.Tabbed;
import org.netbeans.swing.tabcontrol.customtabs.TabbedComponentFactory;
import org.netbeans.swing.tabcontrol.customtabs.TabbedType;

public class DefaultTabbedComponentFactory
implements TabbedComponentFactory {
    private final boolean isAquaLaF = "Aqua".equals(UIManager.getLookAndFeel().getID());

    public Tabbed createTabbedComponent(TabbedType type, WinsysInfoForTabbedContainer info) {
        if (Switches.isUseSimpleTabs()) {
            boolean multiRow = Switches.isSimpleTabsMultiRow();
            int placement = Switches.getSimpleTabsPlacement();
            JTabbedPaneAdapter tabPane = new JTabbedPaneAdapter(type, info);
            tabPane.setTabPlacement(placement);
            tabPane.setTabLayoutPolicy(multiRow ? 0 : 1);
            return tabPane.getTabbed();
        }
        if (type == TabbedType.EDITOR) {
            boolean multiRow = WinSysPrefs.HANDLER.getBoolean("document.tabs.multirow", false);
            int placement = WinSysPrefs.HANDLER.getInt("document.tabs.placement", 1);
            if (this.isAquaLaF) {
                multiRow = false;
                if (placement == 2 || placement == 4) {
                    placement = 1;
                }
            }
            if (multiRow || placement != 1) {
                JTabbedPaneAdapter tabPane = new JTabbedPaneAdapter(type, info);
                tabPane.setTabPlacement(placement);
                tabPane.setTabLayoutPolicy(multiRow ? 0 : 1);
                return tabPane.getTabbed();
            }
        }
        return new TabbedAdapter(type.toInt(), info).getTabbed();
    }
}

