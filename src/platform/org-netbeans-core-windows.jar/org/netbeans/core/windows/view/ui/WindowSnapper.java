/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view.ui;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.Rectangle;
import java.awt.Robot;
import java.util.prefs.Preferences;
import org.netbeans.core.windows.options.WinSysPrefs;

class WindowSnapper {
    private Robot robot = new Robot();
    private Point lastCursorLocation;
    private static final int SNAP_LIMIT = WinSysPrefs.HANDLER.getInt("snapping.active.size", 20);

    public void cursorMoved() {
        this.lastCursorLocation = this.getCurrentCursorLocation();
    }

    public boolean snapToScreenEdges(Rectangle sourceBounds) {
        if (null == this.lastCursorLocation) {
            this.lastCursorLocation = this.getCurrentCursorLocation();
            return false;
        }
        Rectangle bounds = sourceBounds;
        Rectangle screenBounds = this.getScreenBounds();
        Point cursorLocation = this.getCurrentCursorLocation();
        if (null == cursorLocation || null == screenBounds) {
            return false;
        }
        int dx = cursorLocation.x - this.lastCursorLocation.x;
        int dy = cursorLocation.y - this.lastCursorLocation.y;
        int cursorOffsetX = cursorLocation.x - bounds.x;
        int cursorOffsetY = cursorLocation.y - bounds.y;
        boolean snap = false;
        int newCursorX = cursorLocation.x;
        int newCursorY = cursorLocation.y;
        if (bounds.x < screenBounds.x + SNAP_LIMIT && bounds.x >= screenBounds.x - SNAP_LIMIT && dx < 0) {
            newCursorX = screenBounds.x + cursorOffsetX;
            snap = true;
        } else if (bounds.x + bounds.width > screenBounds.x + screenBounds.width - SNAP_LIMIT && bounds.x + bounds.width <= screenBounds.x + screenBounds.width + SNAP_LIMIT && dx > 0) {
            newCursorX = screenBounds.x + screenBounds.width - bounds.width + cursorOffsetX;
            snap = true;
        }
        if (bounds.y < screenBounds.y + SNAP_LIMIT && bounds.y >= screenBounds.y - SNAP_LIMIT && dy < 0) {
            newCursorY = screenBounds.y + cursorOffsetY;
            snap = true;
        } else if (bounds.y + bounds.height > screenBounds.y + screenBounds.height - SNAP_LIMIT && bounds.y + bounds.height <= screenBounds.y + screenBounds.height + SNAP_LIMIT && dy > 0) {
            newCursorY = screenBounds.y + screenBounds.height - bounds.height + cursorOffsetY;
            snap = true;
        }
        if (snap) {
            this.robot.mouseMove(newCursorX, newCursorY);
        }
        return snap;
    }

    public boolean snapTo(Rectangle srcBounds, Rectangle tgtBounds) {
        boolean snap = false;
        if (null != this.lastCursorLocation) {
            Point cursorLocation = this.getCurrentCursorLocation();
            if (null == cursorLocation) {
                return false;
            }
            int dx = cursorLocation.x - this.lastCursorLocation.x;
            int dy = cursorLocation.y - this.lastCursorLocation.y;
            int cursorOffsetX = cursorLocation.x - srcBounds.x;
            int cursorOffsetY = cursorLocation.y - srcBounds.y;
            int newCursorX = cursorLocation.x;
            int newCursorY = cursorLocation.y;
            if (srcBounds.x < tgtBounds.x + tgtBounds.width + SNAP_LIMIT && srcBounds.x >= tgtBounds.x + tgtBounds.width - SNAP_LIMIT && this.isVerticalProximity(srcBounds, tgtBounds) && dx < 0) {
                newCursorX = tgtBounds.x + tgtBounds.width + cursorOffsetX;
                snap = true;
            } else if (srcBounds.x + srcBounds.width > tgtBounds.x + tgtBounds.width - SNAP_LIMIT && srcBounds.x + srcBounds.width <= tgtBounds.x + tgtBounds.width + SNAP_LIMIT && (srcBounds.y == tgtBounds.y + tgtBounds.height || srcBounds.y + srcBounds.height == tgtBounds.y) && dx > 0) {
                newCursorX = tgtBounds.x + tgtBounds.width - srcBounds.width + cursorOffsetX;
                snap = true;
            } else if (srcBounds.x + srcBounds.width > tgtBounds.x - SNAP_LIMIT && srcBounds.x + srcBounds.width <= tgtBounds.x + SNAP_LIMIT && this.isVerticalProximity(srcBounds, tgtBounds) && dx > 0) {
                newCursorX = tgtBounds.x - srcBounds.width + cursorOffsetX;
                snap = true;
            } else if (srcBounds.x < tgtBounds.x + SNAP_LIMIT && srcBounds.x >= tgtBounds.x - SNAP_LIMIT && (srcBounds.y == tgtBounds.y + tgtBounds.height || srcBounds.y + srcBounds.height == tgtBounds.y) && dx < 0) {
                newCursorX = tgtBounds.x + cursorOffsetX;
                snap = true;
            }
            if (srcBounds.y + srcBounds.height > tgtBounds.y - SNAP_LIMIT && srcBounds.y + srcBounds.height <= tgtBounds.y + SNAP_LIMIT && this.isHorizontalProximity(srcBounds, tgtBounds) && dy > 0) {
                newCursorY = tgtBounds.y - srcBounds.height + cursorOffsetY;
                snap = true;
            } else if (srcBounds.y < tgtBounds.y + SNAP_LIMIT && srcBounds.y >= tgtBounds.y - SNAP_LIMIT && (srcBounds.x == tgtBounds.x + tgtBounds.width || srcBounds.x + srcBounds.width == tgtBounds.x) && dy < 0) {
                newCursorY = tgtBounds.y + cursorOffsetY;
                snap = true;
            } else if (srcBounds.y < tgtBounds.y + tgtBounds.height + SNAP_LIMIT && srcBounds.y >= tgtBounds.y + tgtBounds.height - SNAP_LIMIT && this.isHorizontalProximity(srcBounds, tgtBounds) && dy < 0) {
                newCursorY = tgtBounds.y + tgtBounds.height + cursorOffsetY;
                snap = true;
            } else if (srcBounds.y + srcBounds.height > tgtBounds.y + tgtBounds.height - SNAP_LIMIT && srcBounds.y + srcBounds.height <= tgtBounds.y + tgtBounds.height + SNAP_LIMIT && (srcBounds.x == tgtBounds.x + tgtBounds.width || srcBounds.x + srcBounds.width == tgtBounds.x) && dy > 0) {
                newCursorY = tgtBounds.y + tgtBounds.height - srcBounds.height + cursorOffsetY;
                snap = true;
            }
            if (snap) {
                this.robot.mouseMove(newCursorX, newCursorY);
                this.lastCursorLocation = this.getCurrentCursorLocation();
            }
        }
        return snap;
    }

    private Point getCurrentCursorLocation() {
        Point res = null;
        PointerInfo pi = MouseInfo.getPointerInfo();
        if (null != pi) {
            res = pi.getLocation();
        }
        return res;
    }

    private Rectangle getScreenBounds() {
        GraphicsDevice gd;
        GraphicsConfiguration gc;
        Rectangle res = null;
        PointerInfo pi = MouseInfo.getPointerInfo();
        if (null != pi && (gd = pi.getDevice()) != null && (gc = gd.getDefaultConfiguration()) != null) {
            res = gc.getBounds();
        }
        return res;
    }

    private boolean isVerticalProximity(Rectangle r1, Rectangle r2) {
        r1 = new Rectangle(r1);
        r2 = new Rectangle(r2);
        r1.x = 0;
        r1.width = 1;
        r2.x = 0;
        r2.width = 1;
        return r1.intersection((Rectangle)r2).height > 0;
    }

    private boolean isHorizontalProximity(Rectangle r1, Rectangle r2) {
        r1 = new Rectangle(r1);
        r2 = new Rectangle(r2);
        r1.y = 0;
        r1.height = 1;
        r2.y = 0;
        r2.height = 1;
        return r1.intersection((Rectangle)r2).width > 0;
    }
}

