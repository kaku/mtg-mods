/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view.ui.slides;

public interface SlideController {
    public void userToggledAutoHide(int var1, boolean var2);

    public void userToggledTransparency(int var1);
}

