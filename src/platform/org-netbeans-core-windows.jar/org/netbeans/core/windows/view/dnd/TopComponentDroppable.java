/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view.dnd;

import java.awt.Component;
import java.awt.Point;
import java.awt.Shape;
import org.netbeans.core.windows.view.ViewElement;
import org.netbeans.core.windows.view.dnd.TopComponentDraggable;

public interface TopComponentDroppable {
    public Shape getIndicationForLocation(Point var1);

    public Object getConstraintForLocation(Point var1);

    public Component getDropComponent();

    public ViewElement getDropViewElement();

    public boolean canDrop(TopComponentDraggable var1, Point var2);

    public boolean supportsKind(TopComponentDraggable var1);

    public int getKind();
}

