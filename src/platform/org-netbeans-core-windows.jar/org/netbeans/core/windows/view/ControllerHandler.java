/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view;

import java.awt.Rectangle;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.model.ModelElement;
import org.netbeans.core.windows.view.dnd.TopComponentDraggable;
import org.openide.windows.TopComponent;

public interface ControllerHandler {
    public void userActivatedMode(ModeImpl var1);

    public void userActivatedModeWindow(ModeImpl var1);

    public void userActivatedEditorWindow();

    public void userActivatedTopComponent(ModeImpl var1, TopComponent var2);

    public void userResizedMainWindow(Rectangle var1);

    public void userResizedEditorArea(Rectangle var1);

    public void userResizedModeBounds(ModeImpl var1, Rectangle var2);

    public void userChangedFrameStateMainWindow(int var1);

    public void userChangedFrameStateEditorArea(int var1);

    public void userChangedFrameStateMode(ModeImpl var1, int var2);

    public void userChangedSplit(ModelElement[] var1, double[] var2);

    public void userClosedTopComponent(ModeImpl var1, TopComponent var2);

    public void userClosedMode(ModeImpl var1);

    public void userResizedMainWindowBoundsSeparatedHelp(Rectangle var1);

    public void userResizedEditorAreaBoundsHelp(Rectangle var1);

    public void userResizedModeBoundsSeparatedHelp(ModeImpl var1, Rectangle var2);

    public void userDroppedTopComponents(ModeImpl var1, TopComponentDraggable var2);

    public void userDroppedTopComponents(ModeImpl var1, TopComponentDraggable var2, int var3);

    public void userDroppedTopComponents(ModeImpl var1, TopComponentDraggable var2, String var3);

    public void userDroppedTopComponentsIntoEmptyEditor(TopComponentDraggable var1);

    public void userDroppedTopComponentsAround(TopComponentDraggable var1, String var2);

    public void userDroppedTopComponentsAroundEditor(TopComponentDraggable var1, String var2);

    public void userDroppedTopComponentsIntoFreeArea(TopComponentDraggable var1, Rectangle var2);

    public void userUndockedTopComponent(TopComponent var1, ModeImpl var2);

    public void userDockedTopComponent(TopComponent var1, ModeImpl var2);

    public void userEnabledAutoHide(TopComponent var1, ModeImpl var2, String var3);

    public void userDisabledAutoHide(TopComponent var1, ModeImpl var2);

    public void userResizedSlidingMode(ModeImpl var1, Rectangle var2);
}

