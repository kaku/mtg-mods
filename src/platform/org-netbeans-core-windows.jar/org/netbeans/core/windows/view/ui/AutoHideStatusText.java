/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.StatusDisplayer
 */
package org.netbeans.core.windows.view.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.core.windows.EditorOnlyDisplayer;
import org.netbeans.core.windows.view.ui.slides.SlideBar;
import org.openide.awt.StatusDisplayer;

final class AutoHideStatusText
implements ChangeListener,
Runnable {
    private final JPanel panel = new JPanel(new BorderLayout());
    private final JLabel lblStatus = new JLabel();
    private String text;
    private final JPanel statusContainer;

    private AutoHideStatusText(JFrame frame, JPanel statusContainer) {
        this.statusContainer = statusContainer;
        Border outerBorder = UIManager.getBorder("Nb.ScrollPane.border");
        if (null == outerBorder) {
            outerBorder = BorderFactory.createEtchedBorder();
        }
        this.panel.setBorder(BorderFactory.createCompoundBorder(outerBorder, BorderFactory.createEmptyBorder(3, 3, 3, 3)));
        this.lblStatus.setName("AutoHideStatusTextLabel");
        this.panel.add((Component)this.lblStatus, "Center");
        frame.getLayeredPane().add((Component)this.panel, (Object)101);
        StatusDisplayer.getDefault().addChangeListener((ChangeListener)this);
        frame.addComponentListener(new ComponentAdapter(){

            @Override
            public void componentResized(ComponentEvent e) {
                AutoHideStatusText.this.run();
            }
        });
    }

    static void install(JFrame frame, JPanel statusContainer) {
        new AutoHideStatusText(frame, statusContainer);
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        this.text = StatusDisplayer.getDefault().getStatusText();
        String oldValue = this.lblStatus.getText();
        if (this.text == null ? oldValue == null : this.text.equals(oldValue)) {
            return;
        }
        if (SwingUtilities.isEventDispatchThread()) {
            this.run();
        } else {
            SwingUtilities.invokeLater(this);
        }
    }

    @Override
    public void run() {
        this.lblStatus.setText(this.text);
        if (EditorOnlyDisplayer.getInstance().isActive()) {
            return;
        }
        if (null == this.text || this.text.isEmpty()) {
            this.panel.setVisible(false);
            Container parent = this.panel.getParent();
            if (parent instanceof JLayeredPane) {
                JLayeredPane pane = (JLayeredPane)parent;
                pane.moveToBack(this.panel);
            }
        } else {
            JLayeredPane pane;
            int slideWidth;
            this.panel.setVisible(true);
            Container parent = this.panel.getParent();
            Dimension dim = this.panel.getPreferredSize();
            Rectangle rect = parent.getBounds();
            Component slideBar = this.findSlideBar();
            if (null != slideBar && (slideWidth = slideBar.getWidth()) > 0) {
                rect.x += slideWidth + 10;
            }
            this.panel.setBounds(rect.x - 1, rect.y + rect.height - dim.height + 1, dim.width, dim.height + 1);
            if (parent instanceof JLayeredPane && (pane = (JLayeredPane)parent).getComponentZOrder(this.panel) >= 0) {
                pane.moveToFront(this.panel);
            }
        }
    }

    private Component findSlideBar() {
        if (null == this.statusContainer) {
            return null;
        }
        for (Component c : this.statusContainer.getComponents()) {
            if (!(c instanceof SlideBar)) continue;
            return c;
        }
        return null;
    }

}

