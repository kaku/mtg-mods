/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view.ui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.view.ViewElement;

class MultiSplitCell {
    private ViewElement view;
    private double normalizedResizeWeight = 0.0;
    private double initialSplitWeight;
    private int requiredSize = -1;
    private boolean dirty = false;
    private boolean isHorizontalSplit;
    private static final int MINIMUM_POSSIBLE_SIZE = 10;

    MultiSplitCell(ViewElement view, double initialSplitWeight, boolean isHorizontalSplit) {
        this.view = view;
        this.initialSplitWeight = initialSplitWeight;
        this.isHorizontalSplit = isHorizontalSplit;
    }

    public boolean equals(Object o) {
        if (o instanceof MultiSplitCell) {
            MultiSplitCell cell = (MultiSplitCell)o;
            return this.getComponent().equals(cell.getComponent());
        }
        return super.equals(o);
    }

    boolean isDirty() {
        return this.dirty;
    }

    void setDirty(boolean isDirty) {
        this.dirty = isDirty;
    }

    void maybeResetToInitialSize(int newSize) {
        if (-1 == this.requiredSize) {
            this.requiredSize = this.getSize();
            if (this.requiredSize <= 0 || this.requiredSize >= newSize) {
                this.requiredSize = (int)((double)newSize * this.initialSplitWeight + 0.5);
            }
            this.requiredSize = Math.max(this.requiredSize, this.getMinimumSize());
            this.dirty = true;
        }
    }

    double getResizeWeight() {
        return this.view.getResizeWeight();
    }

    Component getComponent() {
        return this.view.getComponent();
    }

    int getMinimumSize() {
        int result = 10;
        if (Switches.isSplitterRespectMinimumSizeEnabled()) {
            result = this.isHorizontalSplit ? this.getComponent().getMinimumSize().width : this.getComponent().getMinimumSize().height;
        }
        if (result < 10) {
            result = 10;
        }
        return result;
    }

    int getRequiredSize() {
        if (-1 == this.requiredSize) {
            if (this.isHorizontalSplit) {
                return this.getComponent().getPreferredSize().width;
            }
            return this.getComponent().getPreferredSize().height;
        }
        return this.requiredSize;
    }

    void layout(int x, int y, int width, int height) {
        if (this.isHorizontalSplit) {
            this.dirty |= x != this.getLocation() || this.requiredSize != width;
            this.requiredSize = width;
        } else {
            this.dirty |= y != this.getLocation() || this.requiredSize != height;
            this.requiredSize = height;
        }
        this.getComponent().setBounds(x, y, width, height);
    }

    void setRequiredSize(int newRequiredSize) {
        this.dirty |= newRequiredSize != this.requiredSize;
        this.requiredSize = newRequiredSize;
    }

    int getLocation() {
        if (this.isHorizontalSplit) {
            return this.getComponent().getLocation().x;
        }
        return this.getComponent().getLocation().y;
    }

    int getSize() {
        if (this.isHorizontalSplit) {
            return this.getComponent().getSize().width;
        }
        return this.getComponent().getSize().height;
    }

    double getNormalizedResizeWeight() {
        return this.normalizedResizeWeight;
    }

    void setNormalizedResizeWeight(double newNormalizedResizeWeight) {
        this.normalizedResizeWeight = newNormalizedResizeWeight;
    }

    ViewElement getViewElement() {
        return this.view;
    }
}

