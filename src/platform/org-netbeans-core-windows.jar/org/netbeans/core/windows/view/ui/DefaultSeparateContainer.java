/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer
 *  org.netbeans.swing.tabcontrol.customtabs.Tabbed
 *  org.netbeans.swing.tabcontrol.customtabs.TabbedComponentFactory
 *  org.netbeans.swing.tabcontrol.customtabs.TabbedType
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view.ui;

import java.awt.AWTException;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Window;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowStateListener;
import java.text.MessageFormat;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JRootPane;
import javax.swing.RootPaneContainer;
import javax.swing.SwingUtilities;
import javax.swing.plaf.basic.BasicHTML;
import org.netbeans.core.windows.Constants;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.options.WinSysPrefs;
import org.netbeans.core.windows.view.Controller;
import org.netbeans.core.windows.view.ModeView;
import org.netbeans.core.windows.view.ViewElement;
import org.netbeans.core.windows.view.dnd.TopComponentDraggable;
import org.netbeans.core.windows.view.dnd.TopComponentDroppable;
import org.netbeans.core.windows.view.dnd.WindowDnDManager;
import org.netbeans.core.windows.view.dnd.ZOrderManager;
import org.netbeans.core.windows.view.ui.AbstractModeContainer;
import org.netbeans.core.windows.view.ui.MainWindow;
import org.netbeans.core.windows.view.ui.ModeComponent;
import org.netbeans.core.windows.view.ui.TabbedHandler;
import org.netbeans.core.windows.view.ui.WindowSnapper;
import org.netbeans.core.windows.view.ui.tabcontrol.TabbedAdapter;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.customtabs.Tabbed;
import org.netbeans.swing.tabcontrol.customtabs.TabbedComponentFactory;
import org.netbeans.swing.tabcontrol.customtabs.TabbedType;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

public final class DefaultSeparateContainer
extends AbstractModeContainer {
    private final ModeFrame modeFrame;
    private final ModeDialog modeDialog;

    public DefaultSeparateContainer(ModeView modeView, WindowDnDManager windowDnDManager, Rectangle bounds, int kind) {
        super(modeView, windowDnDManager, kind);
        this.modeFrame = new ModeFrame(this, modeView);
        MainWindow.initFrameIcons(this.modeFrame);
        this.modeDialog = null;
        Window w = this.getModeUIWindow();
        ((RootPaneContainer)((Object)w)).getContentPane().add(this.tabbedHandler.getComponent());
        w.setBounds(bounds);
    }

    @Override
    public void requestAttention(TopComponent tc) {
    }

    @Override
    public void cancelRequestAttention(TopComponent tc) {
    }

    @Override
    public void setAttentionHighlight(TopComponent tc, boolean highlight) {
        this.tabbedHandler.setAttentionHighlight(tc, highlight);
    }

    @Override
    public void makeBusy(TopComponent tc, boolean busy) {
        this.tabbedHandler.makeBusy(tc, busy);
    }

    @Override
    protected Component getModeComponent() {
        return this.getModeUIWindow();
    }

    @Override
    protected Tabbed createTabbed() {
        TabbedComponentFactory factory = (TabbedComponentFactory)Lookup.getDefault().lookup(TabbedComponentFactory.class);
        TabbedType type = this.getKind() == 1 ? TabbedType.EDITOR : TabbedType.VIEW;
        return factory.createTabbedComponent(type, (WinsysInfoForTabbedContainer)new TabbedAdapter.WinsysInfo(this.getKind()));
    }

    @Override
    protected void updateTitle(String title) {
        this.getModeUIBase().updateTitle(title);
    }

    @Override
    protected void updateActive(boolean active) {
        Window w = this.getModeUIWindow();
        if (active && w.isVisible() && !w.isActive()) {
            w.toFront();
        }
    }

    @Override
    public boolean isActive() {
        return this.getModeUIWindow().isActive();
    }

    @Override
    protected boolean isAttachingPossible() {
        return false;
    }

    @Override
    protected TopComponentDroppable getModeDroppable() {
        return this.getModeUIBase();
    }

    private Window getModeUIWindow() {
        return this.modeFrame != null ? this.modeFrame : this.modeDialog;
    }

    private ModeUIBase getModeUIBase() {
        return (ModeUIBase)((Object)this.getModeUIWindow());
    }

    private static class SharedModeUIBaseImpl
    implements SharedModeUIBase {
        private final AbstractModeContainer abstractModeContainer;
        private final ModeView modeView;
        private long frametimestamp = 0;
        private Window window;

        public SharedModeUIBaseImpl(AbstractModeContainer abstractModeContainer, ModeView view, Window window) {
            this.abstractModeContainer = abstractModeContainer;
            this.modeView = view;
            this.window = window;
            this.initWindow(window);
            this.attachListeners(window);
        }

        private void initWindow(Window w) {
            ((RootPaneContainer)((Object)w)).getRootPane().putClientProperty("SeparateWindow", Boolean.TRUE);
            ZOrderManager.getInstance().attachWindow((RootPaneContainer)((Object)w));
        }

        private void attachListeners(Window w) {
            w.addWindowListener(new WindowAdapter(){

                @Override
                public void windowClosing(WindowEvent evt) {
                    WindowManagerImpl wm = WindowManagerImpl.getInstance();
                    for (TopComponent tc : SharedModeUIBaseImpl.this.modeView.getTopComponents()) {
                        if (!Switches.isEditorTopComponentClosingEnabled() && wm.isEditorTopComponent(tc)) {
                            return;
                        }
                        if (!Switches.isViewTopComponentClosingEnabled() && !wm.isEditorTopComponent(tc)) {
                            return;
                        }
                        if (!Switches.isClosingEnabled(tc)) {
                            return;
                        }
                        if (tc.close()) continue;
                        return;
                    }
                    SharedModeUIBaseImpl.this.modeView.getController().userClosingMode(SharedModeUIBaseImpl.this.modeView);
                    ZOrderManager.getInstance().detachWindow((RootPaneContainer)((Object)SharedModeUIBaseImpl.this.window));
                }

                @Override
                public void windowClosed(WindowEvent evt) {
                    ZOrderManager.getInstance().detachWindow((RootPaneContainer)((Object)SharedModeUIBaseImpl.this.window));
                }

                @Override
                public void windowActivated(WindowEvent event) {
                    if (SharedModeUIBaseImpl.this.frametimestamp != 0 && System.currentTimeMillis() > SharedModeUIBaseImpl.this.frametimestamp + 500) {
                        SharedModeUIBaseImpl.this.modeView.getController().userActivatedModeWindow(SharedModeUIBaseImpl.this.modeView);
                    }
                    SharedModeUIBaseImpl.this.frametimestamp = System.currentTimeMillis();
                }

                @Override
                public void windowOpened(WindowEvent event) {
                    SharedModeUIBaseImpl.this.frametimestamp = System.currentTimeMillis();
                }
            });
            w.addComponentListener(new ComponentAdapter(){

                @Override
                public void componentResized(ComponentEvent evt) {
                    SharedModeUIBaseImpl.this.modeView.getController().userResizedModeBounds(SharedModeUIBaseImpl.this.modeView, SharedModeUIBaseImpl.this.window.getBounds());
                }

                @Override
                public void componentMoved(ComponentEvent evt) {
                    SharedModeUIBaseImpl.this.modeView.getController().userResizedModeBounds(SharedModeUIBaseImpl.this.modeView, SharedModeUIBaseImpl.this.window.getBounds());
                }
            });
            this.window.addWindowStateListener(new WindowStateListener(){

                @Override
                public void windowStateChanged(WindowEvent evt) {
                    if (!Constants.AUTO_ICONIFY) {
                        SharedModeUIBaseImpl.this.modeView.getController().userChangedFrameStateMode(SharedModeUIBaseImpl.this.modeView, evt.getNewState());
                    } else {
                        Component comp = SharedModeUIBaseImpl.this.modeView.getComponent();
                        if (comp instanceof Frame) {
                            long currentStamp = System.currentTimeMillis();
                            if (currentStamp > SharedModeUIBaseImpl.this.modeView.getUserStamp() + 500 && currentStamp > SharedModeUIBaseImpl.this.modeView.getMainWindowStamp() + 1000) {
                                SharedModeUIBaseImpl.this.modeView.getController().userChangedFrameStateMode(SharedModeUIBaseImpl.this.modeView, evt.getNewState());
                            } else {
                                SharedModeUIBaseImpl.this.modeView.setUserStamp(0);
                                SharedModeUIBaseImpl.this.modeView.setMainWindowStamp(0);
                                SharedModeUIBaseImpl.this.modeView.updateFrameState();
                            }
                            long stamp = System.currentTimeMillis();
                            SharedModeUIBaseImpl.this.modeView.setUserStamp(stamp);
                        }
                    }
                }
            });
        }

        public void setVisible(boolean visible) {
            this.frametimestamp = System.currentTimeMillis();
            this.window.setVisible(visible);
        }

        public void toFront() {
            this.frametimestamp = System.currentTimeMillis();
            this.window.toFront();
        }

        @Override
        public ModeView getModeView() {
            return this.abstractModeContainer.getModeView();
        }

        @Override
        public int getKind() {
            return this.abstractModeContainer.getKind();
        }

        @Override
        public Shape getIndicationForLocation(Point location) {
            return this.abstractModeContainer.getIndicationForLocation(location);
        }

        @Override
        public Object getConstraintForLocation(Point location) {
            return this.abstractModeContainer.getConstraintForLocation(location);
        }

        @Override
        public Component getDropComponent() {
            return this.abstractModeContainer.getDropComponent();
        }

        @Override
        public ViewElement getDropViewElement() {
            return this.abstractModeContainer.getDropModeView();
        }

        @Override
        public boolean canDrop(TopComponentDraggable transfer, Point location) {
            return this.abstractModeContainer.canDrop(transfer);
        }

        @Override
        public boolean supportsKind(TopComponentDraggable transfer) {
            return this.abstractModeContainer.canDrop(transfer);
        }

    }

    public static interface ModeUIBase
    extends ModeComponent,
    TopComponentDroppable {
        public void updateTitle(String var1);
    }

    public static interface SharedModeUIBase
    extends ModeComponent,
    TopComponentDroppable {
    }

    private static class ModeDialog
    extends JDialog
    implements ModeUIBase {
        private SharedModeUIBase modeBase;
        private WindowSnapper snapper;
        private boolean ignoreMovedEvents = false;

        public ModeDialog(Frame owner, AbstractModeContainer abstractModeContainer, ModeView view) {
            super(owner);
            this.enableEvents(16);
            this.modeBase = new SharedModeUIBaseImpl(abstractModeContainer, view, this);
            try {
                this.snapper = new WindowSnapper();
            }
            catch (AWTException e) {
                this.snapper = null;
                Logger.getLogger(ModeDialog.class.getName()).log(Level.INFO, null, e);
            }
            this.addComponentListener(new ComponentAdapter(){

                @Override
                public void componentMoved(ComponentEvent ce) {
                    if (ModeDialog.this.ignoreMovedEvents || null == ModeDialog.this.snapper || !WinSysPrefs.HANDLER.getBoolean("snapping", true)) {
                        return;
                    }
                    ModeDialog.this.snapWindow();
                    ModeDialog.this.snapper.cursorMoved();
                }
            });
            this.setDefaultCloseOperation(0);
        }

        private void snapWindow() {
            Rectangle myBounds = this.getBounds();
            WindowManagerImpl wm = WindowManagerImpl.getInstance();
            Set<? extends ModeImpl> modes = wm.getModes();
            for (ModeImpl m : modes) {
                Window w;
                Rectangle targetBounds;
                TopComponent tc;
                if (m.getState() != 1 || null == (tc = m.getSelectedTopComponent()) || (w = SwingUtilities.getWindowAncestor((Component)tc)) == this || !this.snapper.snapTo(myBounds, targetBounds = w.getBounds())) continue;
                return;
            }
            if (WinSysPrefs.HANDLER.getBoolean("snapping.screenedges", true)) {
                this.snapper.snapToScreenEdges(myBounds);
            }
        }

        @Override
        public ModeView getModeView() {
            return this.modeBase.getModeView();
        }

        @Override
        public int getKind() {
            return this.modeBase.getKind();
        }

        @Override
        public Shape getIndicationForLocation(Point location) {
            return this.modeBase.getIndicationForLocation(location);
        }

        @Override
        public Object getConstraintForLocation(Point location) {
            return this.modeBase.getConstraintForLocation(location);
        }

        @Override
        public Component getDropComponent() {
            return this.modeBase.getDropComponent();
        }

        @Override
        public ViewElement getDropViewElement() {
            return this.modeBase.getDropViewElement();
        }

        @Override
        public boolean canDrop(TopComponentDraggable transfer, Point location) {
            return this.modeBase.canDrop(transfer, location);
        }

        @Override
        public boolean supportsKind(TopComponentDraggable transfer) {
            return this.modeBase.supportsKind(transfer);
        }

        @Override
        public void updateTitle(String title) {
        }

        @Override
        public void setBounds(int x, int y, int w, int h) {
            this.ignoreMovedEvents = true;
            super.setBounds(x, y, w, h);
            this.ignoreMovedEvents = false;
        }

        @Override
        public void setBounds(Rectangle r) {
            this.ignoreMovedEvents = true;
            super.setBounds(r);
            this.ignoreMovedEvents = false;
        }

        @Override
        public void setLocation(Point p) {
            this.ignoreMovedEvents = true;
            super.setLocation(p);
            this.ignoreMovedEvents = false;
        }

        @Override
        public void setLocation(int x, int y) {
            this.ignoreMovedEvents = true;
            super.setLocation(x, y);
            this.ignoreMovedEvents = false;
        }

    }

    private static class ModeFrame
    extends JFrame
    implements ModeUIBase {
        private SharedModeUIBase modeBase;

        public ModeFrame(AbstractModeContainer abstractModeContainer, ModeView view) {
            this.enableEvents(16);
            this.modeBase = new SharedModeUIBaseImpl(abstractModeContainer, view, this);
            this.setDefaultCloseOperation(0);
        }

        @Override
        public ModeView getModeView() {
            return this.modeBase.getModeView();
        }

        @Override
        public int getKind() {
            return this.modeBase.getKind();
        }

        @Override
        public Shape getIndicationForLocation(Point location) {
            return this.modeBase.getIndicationForLocation(location);
        }

        @Override
        public Object getConstraintForLocation(Point location) {
            return this.modeBase.getConstraintForLocation(location);
        }

        @Override
        public Component getDropComponent() {
            return this.modeBase.getDropComponent();
        }

        @Override
        public ViewElement getDropViewElement() {
            return this.modeBase.getDropViewElement();
        }

        @Override
        public boolean canDrop(TopComponentDraggable transfer, Point location) {
            return this.modeBase.canDrop(transfer, location);
        }

        @Override
        public boolean supportsKind(TopComponentDraggable transfer) {
            return this.modeBase.supportsKind(transfer);
        }

        @Override
        public void updateTitle(String title) {
            if (BasicHTML.isHTMLString(title)) {
                char[] c = title.toCharArray();
                StringBuffer sb = new StringBuffer(title.length());
                boolean inTag = false;
                for (int i = 0; i < c.length; ++i) {
                    if (inTag && c[i] == '>') {
                        inTag = false;
                        continue;
                    }
                    if (!inTag && c[i] == '<') {
                        inTag = true;
                        continue;
                    }
                    if (inTag) continue;
                    sb.append(c[i]);
                }
                title = sb.toString().replace("&nbsp;", " ");
            }
            String completeTitle = MessageFormat.format(NbBundle.getMessage(DefaultSeparateContainer.class, (String)"CTL_SeparateEditorTitle"), title);
            this.setTitle(completeTitle);
        }
    }

}

