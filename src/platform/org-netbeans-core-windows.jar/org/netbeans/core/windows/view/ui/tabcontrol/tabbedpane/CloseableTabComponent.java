/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.customtabs.TabbedType
 *  org.openide.awt.CloseButtonFactory
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.windows.view.ui.tabcontrol.tabbedpane;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.netbeans.core.windows.view.ui.tabcontrol.tabbedpane.NBTabbedPane;
import org.netbeans.swing.tabcontrol.customtabs.TabbedType;
import org.openide.awt.CloseButtonFactory;
import org.openide.util.NbBundle;

public class CloseableTabComponent
extends JPanel {
    private final NBTabbedPane parent;
    private final JLabel lblTitle = new JLabel();
    private final JButton closeButton;

    public CloseableTabComponent(Icon icon, String title, boolean closeable, String tooltip, NBTabbedPane parent, MouseListener controller) {
        super(new BorderLayout(2, 0));
        this.lblTitle.setText(title);
        if (parent.getType() == TabbedType.EDITOR) {
            this.lblTitle.setIcon(icon);
        }
        this.add((Component)this.lblTitle, "Center");
        this.lblTitle.setToolTipText(tooltip);
        this.addMouseListener(controller);
        this.lblTitle.addMouseListener(controller);
        if (closeable) {
            this.closeButton = CloseButtonFactory.createBigCloseButton();
            this.add((Component)this.closeButton, "East");
            this.closeButton.addMouseListener(controller);
            if (parent.getType() == TabbedType.EDITOR) {
                this.closeButton.setToolTipText(NbBundle.getMessage(CloseableTabComponent.class, (String)"BtnClose_Tooltip"));
            }
        } else {
            this.closeButton = null;
        }
        this.parent = parent;
        this.setOpaque(false);
    }

    public void setIcon(Icon icon) {
        if (this.parent.getType() == TabbedType.EDITOR) {
            this.lblTitle.setIcon(icon);
        }
    }

    public void setTitle(String title) {
        this.lblTitle.setText(title);
    }

    public void setTooltip(String tooltip) {
        this.lblTitle.setToolTipText(tooltip);
    }

    boolean isInCloseButton(MouseEvent e) {
        return null != this.closeButton && e.getComponent() == this.closeButton;
    }
}

