/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.ToolbarPool
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.windows.view.ui.toolbars;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import org.openide.awt.ToolbarPool;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.NbBundle;

public class ResetToolbarsAction
extends AbstractAction {
    public ResetToolbarsAction() {
        super(NbBundle.getMessage(ResetToolbarsAction.class, (String)"CTL_ResetToolbarsAction"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String name = ToolbarPool.getDefault().getConfiguration();
        FileObject fo = FileUtil.getConfigFile((String)"Toolbars");
        try {
            fo.revert();
        }
        catch (IOException ex) {
            Logger.getLogger(ResetToolbarsAction.class.getName()).log(Level.FINE, null, ex);
        }
        ToolbarPool.getDefault().waitFinished();
        ToolbarPool.getDefault().setConfiguration(name);
    }
}

