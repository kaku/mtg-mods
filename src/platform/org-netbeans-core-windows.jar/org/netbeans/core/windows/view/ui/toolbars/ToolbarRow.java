/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view.ui.toolbars;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.netbeans.core.windows.view.ui.toolbars.ToolbarConstraints;
import org.netbeans.core.windows.view.ui.toolbars.ToolbarContainer;

class ToolbarRow
extends JPanel {
    private final Map<String, ToolbarConstraints> name2constraint = new HashMap<String, ToolbarConstraints>(20);
    private final List<ToolbarConstraints> constraints = new ArrayList<ToolbarConstraints>(20);
    private static final String FAKE_NAME = "__fake_drag_container__";
    private ToolbarConstraints dragConstraints;
    private Component dragContainer;
    private Point dragOriginalLocation;
    private JLabel dropReplacement;
    private ToolbarConstraints dropConstraints;
    private ToolbarContainer dropContainter;
    private static final boolean isMetalLaF = "Metal".equals(UIManager.getLookAndFeel().getID());
    private static final boolean isNimbusLaF = "Nimbus".equals(UIManager.getLookAndFeel().getID());
    private static final boolean isGTKLaF = "GTK".equals(UIManager.getLookAndFeel().getID());
    private static final boolean isAquaLaF = "Aqua".equals(UIManager.getLookAndFeel().getID());

    public ToolbarRow() {
        this.setLayout(new ToolbarLayout());
        this.setOpaque(false);
        this.addDropConstraints();
    }

    public void addConstraint(ToolbarConstraints tc) {
        ToolbarConstraints current = this.name2constraint.get(tc.getName());
        if (null != current) {
            this.constraints.remove(current);
            Logger.getLogger(ToolbarRow.class.getName()).log(Level.FINE, "Duplicate toolbar defintion " + tc.getName());
        }
        List<ToolbarConstraints> left = this.getConstraints(ToolbarConstraints.Align.left);
        List<ToolbarConstraints> right = this.getConstraints(ToolbarConstraints.Align.right);
        this.constraints.clear();
        this.constraints.addAll(left);
        if (tc.getAlign() == ToolbarConstraints.Align.left) {
            this.constraints.add(tc);
        }
        this.constraints.addAll(right);
        if (tc.getAlign() == ToolbarConstraints.Align.right) {
            this.constraints.add(tc);
        }
        this.name2constraint.put(tc.getName(), tc);
    }

    boolean removeConstraint(ToolbarConstraints tc) {
        if (null != this.name2constraint.get(tc.getName())) {
            this.name2constraint.remove(tc.getName());
            this.constraints.remove(tc);
            return true;
        }
        return false;
    }

    @Override
    public void removeAll() {
        super.removeAll();
        if (null != this.dropReplacement) {
            this.add(this.dropReplacement);
        }
    }

    @Override
    public boolean isVisible() {
        for (ToolbarConstraints tc : this.constraints) {
            if (!tc.isVisible()) continue;
            return true;
        }
        return false;
    }

    public boolean isEmpty() {
        return this.name2constraint.isEmpty() || null != this.dragContainer && this.name2constraint.size() == 1;
    }

    private void addDropConstraints() {
        this.dropConstraints = new ToolbarConstraints("__fake_drag_container__", ToolbarConstraints.Align.left, false, true);
        this.dropReplacement = new JLabel();
        this.dropReplacement.setName(this.dropConstraints.getName());
        this.add(this.dropReplacement);
        this.constraints.add(this.dropConstraints);
    }

    void dragStarted(ToolbarContainer container) {
        this.dragConstraints = this.findConstraints(container.getName());
        if (null != this.dragConstraints) {
            this.dragContainer = this.findComponent(this.dragConstraints.getName());
            this.dragConstraints.setVisible(false);
            this.dragOriginalLocation = new Point(this.dragContainer.getLocationOnScreen());
            container.setVisible(false);
            this.invalidate();
            this.revalidate();
            this.repaint();
        }
    }

    void showDropFeedback(ToolbarContainer container, Point screenLocation, Image dragImage) {
        int dropIndex;
        Component targetComp = null;
        Rectangle bounds = null;
        for (Component c : this.getComponents()) {
            if (!c.isVisible()) continue;
            bounds = c.getBounds();
            bounds.setLocation(c.getLocationOnScreen());
            if (!bounds.contains(screenLocation)) continue;
            targetComp = c;
            break;
        }
        this.dropReplacement.setPreferredSize(container.getPreferredSize());
        this.dropReplacement.setMinimumSize(container.getMinimumSize());
        if (this.dropContainter != container) {
            this.dropContainter = container;
            this.dropReplacement.setIcon(new ImageIcon(dragImage));
        }
        if (null != targetComp) {
            if (targetComp == this.dropReplacement) {
                return;
            }
            boolean dropAfter = bounds.x + bounds.width / 2 < screenLocation.x;
            ToolbarConstraints targetTc = this.findConstraints(targetComp.getName());
            this.dropConstraints.setAlign(targetTc.getAlign());
            dropIndex = this.constraints.indexOf(targetTc);
            if (dropAfter) {
                ++dropIndex;
            }
            if (dropIndex > this.constraints.indexOf(this.dropConstraints)) {
                --dropIndex;
            }
            if (this.isLastVisibleToolbar(targetTc) && this.isStretchLastToolbar() && bounds.x + bounds.width - bounds.width / 4 < screenLocation.x) {
                this.dropConstraints.setAlign(ToolbarConstraints.Align.right);
                dropIndex = this.constraints.size() + 1;
            }
            this.constraints.remove(this.dropConstraints);
            if (dropIndex <= this.constraints.size()) {
                this.constraints.add(dropIndex, this.dropConstraints);
            } else {
                this.constraints.add(this.dropConstraints);
            }
            this.dropConstraints.setVisible(true);
            this.dropReplacement.setVisible(true);
        } else {
            Rectangle freeAreaBounds = this.getFreeAreaBounds();
            if (freeAreaBounds.contains(screenLocation)) {
                boolean leftAlign = freeAreaBounds.x + freeAreaBounds.width / 2 >= screenLocation.x;
                this.constraints.remove(this.dropConstraints);
                dropIndex = -1;
                if (leftAlign) {
                    this.dropConstraints.setAlign(ToolbarConstraints.Align.left);
                    for (int i = 0; i < this.constraints.size(); ++i) {
                        ToolbarConstraints tc = this.constraints.get(i);
                        if (!tc.isVisible() || tc.getAlign() == ToolbarConstraints.Align.left) continue;
                        dropIndex = i;
                        break;
                    }
                } else {
                    this.dropConstraints.setAlign(ToolbarConstraints.Align.right);
                    for (int i = this.constraints.size() - 1; i >= 0; --i) {
                        ToolbarConstraints tc = this.constraints.get(i);
                        if (!tc.isVisible() || tc.getAlign() == ToolbarConstraints.Align.right) continue;
                        dropIndex = i;
                        break;
                    }
                    if (dropIndex < 0) {
                        dropIndex = 0;
                    }
                }
                if (dropIndex >= 0) {
                    this.constraints.add(dropIndex, this.dropConstraints);
                } else {
                    this.constraints.add(this.dropConstraints);
                }
                this.dropConstraints.setVisible(true);
                this.dropReplacement.setVisible(true);
            } else {
                this.dropConstraints.setVisible(false);
                this.dropReplacement.setVisible(false);
            }
        }
        this.invalidate();
        this.revalidate();
        this.repaint();
    }

    void hideDropFeedback() {
        this.dropConstraints.setVisible(false);
        this.dropReplacement.setVisible(false);
        this.dropContainter = null;
        this.invalidate();
        this.revalidate();
        this.repaint();
    }

    Point drop() {
        Point res = null;
        if (null == this.dropContainter) {
            return res;
        }
        if (this.dropReplacement.isShowing()) {
            res = this.dropReplacement.getLocationOnScreen();
        }
        if (null != this.dragConstraints) {
            this.add(this.dragContainer);
            this.constraints.remove(this.dragConstraints);
            this.dragConstraints.setVisible(true);
            this.dragConstraints.setAlign(this.dropConstraints.getAlign());
            this.constraints.add(this.constraints.indexOf(this.dropConstraints), this.dragConstraints);
        } else {
            ToolbarConstraints newConstraints = new ToolbarConstraints(this.dropContainter.getName(), this.dropConstraints.getAlign(), true, true);
            this.add(this.dropContainter);
            this.constraints.add(this.constraints.indexOf(this.dropConstraints), newConstraints);
            this.name2constraint.put(newConstraints.getName(), newConstraints);
        }
        this.dropConstraints.setVisible(false);
        this.dropReplacement.setVisible(false);
        this.invalidate();
        this.revalidate();
        this.repaint();
        this.dropContainter = null;
        this.dragConstraints = null;
        this.dragContainer = null;
        return res;
    }

    void dragSuccess() {
        if (null != this.dragConstraints) {
            Component c = this.findComponent(this.dragConstraints.getName());
            if (null != c) {
                this.remove(c);
            }
            this.constraints.remove(this.dragConstraints);
            this.name2constraint.remove(this.dragConstraints.getName());
            this.dragConstraints = null;
            this.dragContainer = null;
        }
    }

    Point dragAbort() {
        Point res = null;
        if (null != this.dragConstraints) {
            this.add(this.dragContainer);
            this.dragContainer.setVisible(true);
            this.dragConstraints.setVisible(true);
            this.invalidate();
            this.revalidate();
            this.repaint();
            res = this.dragOriginalLocation;
            this.dragConstraints = null;
            this.dragContainer = null;
        }
        return res;
    }

    Iterable<? extends ToolbarConstraints> getConstraints() {
        ArrayList<ToolbarConstraints> res = new ArrayList<ToolbarConstraints>(this.constraints.size());
        for (ToolbarConstraints tc : this.constraints) {
            if (null == this.name2constraint.get(tc.getName())) continue;
            res.add(tc);
        }
        return res;
    }

    int countVisibleToolbars() {
        int count = 0;
        for (ToolbarConstraints tc : this.name2constraint.values()) {
            if (!tc.isVisible()) continue;
            ++count;
        }
        return count;
    }

    private boolean isLastVisibleToolbar(ToolbarConstraints toolbarConstraints) {
        for (int i = this.constraints.size() - 1; i >= 0; --i) {
            ToolbarConstraints tc = this.constraints.get(i);
            if (!tc.isVisible() || "__fake_drag_container__".equals(tc.getName())) continue;
            return tc == toolbarConstraints;
        }
        return false;
    }

    private List<Component> getContainers(ToolbarConstraints.Align align) {
        ArrayList<Component> res = new ArrayList<Component>(this.getComponentCount());
        for (ToolbarConstraints tc : this.constraints) {
            Component c;
            if (!tc.isVisible() || tc.getAlign() != align || null == (c = this.findComponent(tc.getName()))) continue;
            res.add(c);
        }
        return res;
    }

    private ToolbarConstraints findConstraints(String name) {
        for (ToolbarConstraints tc : this.constraints) {
            if (!tc.getName().equals(name)) continue;
            return tc;
        }
        return null;
    }

    private Component findComponent(String name) {
        for (Component c : this.getComponents()) {
            if (!name.equals(c.getName())) continue;
            return c;
        }
        return null;
    }

    private List<ToolbarConstraints> getConstraints(ToolbarConstraints.Align align) {
        ArrayList<ToolbarConstraints> res = new ArrayList<ToolbarConstraints>(this.constraints.size());
        for (ToolbarConstraints tc : this.constraints) {
            if (tc.getAlign() != align) continue;
            res.add(tc);
        }
        return res;
    }

    Rectangle getFreeAreaBounds() {
        ToolbarConstraints tc;
        Component c;
        int i;
        int x1 = 0;
        int x2 = this.getWidth();
        for (i = this.constraints.size() - 1; i >= 0; --i) {
            tc = this.constraints.get(i);
            if (!tc.isVisible() || tc == this.dragConstraints || tc.getAlign() != ToolbarConstraints.Align.left || null == (c = this.findComponent(tc.getName()))) continue;
            x1 = c.getLocation().x + c.getWidth();
            break;
        }
        for (i = 0; i < this.constraints.size(); ++i) {
            tc = this.constraints.get(i);
            if (!tc.isVisible() || tc == this.dragConstraints || tc.getAlign() != ToolbarConstraints.Align.right || null == (c = this.findComponent(tc.getName()))) continue;
            x2 = c.getLocation().x;
            break;
        }
        Rectangle res = new Rectangle(x1, 0, x2 - x1, this.getHeight());
        Point location = res.getLocation();
        if (this.isShowing()) {
            SwingUtilities.convertPointToScreen(location, this);
            res.setLocation(location);
        }
        return res;
    }

    private boolean isStretchLastToolbar() {
        return isMetalLaF || isNimbusLaF || isGTKLaF || isAquaLaF;
    }

    private class ToolbarLayout
    implements LayoutManager {
        @Override
        public void addLayoutComponent(String name, Component comp) {
        }

        @Override
        public void removeLayoutComponent(Component comp) {
        }

        @Override
        public Dimension preferredLayoutSize(Container parent) {
            Dimension d = new Dimension(0, 0);
            d.height = this.getPreferredHeight();
            for (Component c : ToolbarRow.this.getComponents()) {
                if (!c.isVisible()) continue;
                d.width += c.getPreferredSize().width;
            }
            Insets borderInsets = parent.getInsets();
            if (null != borderInsets) {
                d.height += borderInsets.top;
                d.height += borderInsets.bottom;
            }
            return d;
        }

        @Override
        public Dimension minimumLayoutSize(Container parent) {
            Dimension d = new Dimension(0, 0);
            d.height = this.getMinimumHeight();
            for (Component c : ToolbarRow.this.getComponents()) {
                if (!c.isVisible()) continue;
                d.width += c.getMinimumSize().width;
            }
            Insets borderInsets = parent.getInsets();
            if (null != borderInsets) {
                d.height += borderInsets.top;
                d.height += borderInsets.bottom;
            }
            return d;
        }

        @Override
        public void layoutContainer(Container parent) {
            int w = parent.getWidth();
            int h = parent.getHeight();
            int top = 0;
            Insets borderInsets = parent.getInsets();
            if (null != borderInsets) {
                h -= borderInsets.top + borderInsets.bottom;
                top = borderInsets.top;
            }
            Dimension prefSize = this.preferredLayoutSize(parent);
            List leftBars = ToolbarRow.this.getContainers(ToolbarConstraints.Align.left);
            List rightBars = ToolbarRow.this.getContainers(ToolbarConstraints.Align.right);
            HashMap<Component, Integer> bar2width = new HashMap<Component, Integer>(leftBars.size() + rightBars.size());
            if (prefSize.width > w) {
                int barMinWidth;
                int availableToCut;
                int barPrefWidth;
                int toCut = prefSize.width - w;
                ArrayList reversed = new ArrayList(rightBars);
                Collections.reverse(reversed);
                for (Component c2 : reversed) {
                    barPrefWidth = c2.getPreferredSize().width;
                    barMinWidth = c2.getMinimumSize().width;
                    availableToCut = barPrefWidth - barMinWidth;
                    if (toCut <= availableToCut) {
                        bar2width.put(c2, barPrefWidth - toCut);
                        toCut = 0;
                        continue;
                    }
                    bar2width.put(c2, barMinWidth);
                    toCut -= availableToCut;
                }
                reversed = new ArrayList(leftBars);
                Collections.reverse(reversed);
                for (Component c2 : reversed) {
                    barPrefWidth = c2.getPreferredSize().width;
                    barMinWidth = c2.getMinimumSize().width;
                    availableToCut = barPrefWidth - barMinWidth;
                    if (toCut <= availableToCut) {
                        bar2width.put(c2, barPrefWidth - toCut);
                        toCut = 0;
                        continue;
                    }
                    bar2width.put(c2, barMinWidth);
                    toCut -= availableToCut;
                }
            } else {
                for (Component c3 : leftBars) {
                    bar2width.put(c3, c3.getPreferredSize().width);
                }
                for (Component c3 : rightBars) {
                    bar2width.put(c3, c3.getPreferredSize().width);
                }
            }
            int x = 0;
            for (Component c4 : leftBars) {
                int barWidth = (Integer)bar2width.get(c4);
                if (ToolbarRow.this.isStretchLastToolbar() && leftBars.indexOf(c4) == leftBars.size() - 1) {
                    int rightBarsWidth = 0;
                    for (Component rb : rightBars) {
                        rightBarsWidth += ((Integer)bar2width.get(rb)).intValue();
                    }
                    barWidth = w - x - rightBarsWidth;
                }
                c4.setBounds(x, top, barWidth, h);
                x += barWidth;
            }
            x = w;
            Collections.reverse(rightBars);
            for (Component c4 : rightBars) {
                int barWidth = (Integer)bar2width.get(c4);
                c4.setBounds(x -= barWidth, top, barWidth, h);
            }
        }

        private int getPreferredHeight() {
            int h = 0;
            for (Component c : ToolbarRow.this.getComponents()) {
                if (!c.isVisible()) continue;
                Dimension d = c.getPreferredSize();
                if (d.height <= h) continue;
                h = d.height;
            }
            return h;
        }

        private int getMinimumHeight() {
            int h = 0;
            for (Component c : ToolbarRow.this.getComponents()) {
                if (!c.isVisible()) continue;
                Dimension d = c.getMinimumSize();
                if (d.height <= h) continue;
                h = d.height;
            }
            return h;
        }
    }

}

