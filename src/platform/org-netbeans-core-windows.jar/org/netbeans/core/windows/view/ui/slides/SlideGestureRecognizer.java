/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.SlideBarDataModel
 */
package org.netbeans.core.windows.view.ui.slides;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.logging.Logger;
import javax.swing.AbstractButton;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import org.netbeans.core.windows.view.ui.slides.ResizeGestureRecognizer;
import org.netbeans.core.windows.view.ui.slides.SlideBar;
import org.netbeans.swing.tabcontrol.SlideBarDataModel;

final class SlideGestureRecognizer
implements ActionListener,
MouseListener,
MouseMotionListener {
    private SlideBar slideBar;
    private Component mouseInButton = null;
    private int curMouseLocX;
    private int curMouseLocY;
    private AutoSlideTrigger autoSlideTrigger;
    private ResizeGestureRecognizer resizer;
    private boolean pressingButton;
    private static final Logger LOG = Logger.getLogger(SlideGestureRecognizer.class.getName());

    SlideGestureRecognizer(SlideBar slideBar, ResizeGestureRecognizer resize) {
        this.autoSlideTrigger = new AutoSlideTrigger();
        this.pressingButton = false;
        this.slideBar = slideBar;
        this.resizer = resize;
    }

    public void attachButton(AbstractButton button) {
        button.addActionListener(this);
        button.addMouseListener(this);
        button.addMouseMotionListener(this);
    }

    public void detachButton(AbstractButton button) {
        button.removeActionListener(this);
        button.removeMouseListener(this);
        button.addMouseMotionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.slideBar.userClickedSlidingButton((Component)e.getSource());
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        if (this.autoSlideTrigger.isEnabled()) {
            this.curMouseLocX = e.getX();
            this.curMouseLocY = e.getY();
        }
        if (this.pressingButton && (e.getModifiersEx() & 1024) == 0) {
            this.pressingButton = false;
            this.autoSlideTrigger.activateAutoSlideInGesture();
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (!this.slideBar.isHoveringAllowed()) {
            return;
        }
        this.mouseInButton = (Component)e.getSource();
        this.curMouseLocX = e.getX();
        this.curMouseLocY = e.getY();
        this.pressingButton = false;
        if ((e.getModifiersEx() & 1024) == 1024) {
            this.pressingButton = true;
            return;
        }
        this.autoSlideTrigger.activateAutoSlideInGesture();
    }

    @Override
    public void mouseExited(MouseEvent e) {
        this.mouseInButton = null;
        this.pressingButton = false;
        this.autoSlideTrigger.deactivateAutoSlideInGesture(e);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        this.autoSlideTrigger.deactivateAutoSlideInGesture(e);
        this.handlePopupRequests(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        this.autoSlideTrigger.deactivateAutoSlideInGesture(e);
        this.handlePopupRequests(e);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getButton() == 2) {
            this.slideBar.userMiddleClickedSlidingButton(e.getComponent());
        }
    }

    private void handlePopupRequests(MouseEvent e) {
        if (e.getSource().equals(this.slideBar)) {
            return;
        }
        if (e.isPopupTrigger()) {
            this.slideBar.userTriggeredPopup(e, (Component)e.getSource());
        }
    }

    private final class AutoSlideTrigger
    implements ActionListener,
    AWTEventListener {
        private Timer slideInTimer;
        private Timer slideOutTimer;
        private int initialX;
        private int initialY;
        private boolean autoSlideActive;
        private Rectangle activeArea;

        AutoSlideTrigger() {
            this.autoSlideActive = false;
            this.slideInTimer = new Timer(200, this);
            this.slideInTimer.setRepeats(true);
            this.slideInTimer.setCoalesce(true);
        }

        public void activateAutoSlideInGesture() {
            this.initialX = SlideGestureRecognizer.this.curMouseLocX;
            this.initialY = SlideGestureRecognizer.this.curMouseLocY;
            this.slideInTimer.start();
        }

        public void deactivateAutoSlideInGesture(MouseEvent evt) {
            this.slideInTimer.stop();
            this.notifySlideOutTimer(evt);
        }

        public boolean isEnabled() {
            return this.autoSlideActive || this.slideInTimer.isRunning();
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            if (this.slideInTimer.equals(evt.getSource())) {
                this.slideInTimerReaction(evt);
            } else {
                this.slideOutTimerReaction(evt);
            }
        }

        private void slideInTimerReaction(ActionEvent evt) {
            if (this.isSlideInGesture()) {
                this.slideInTimer.stop();
                if (this.autoSlideActive) {
                    this.autoSlideOut();
                }
                this.autoSlideActive = true;
                if (SlideGestureRecognizer.this.slideBar.userTriggeredAutoSlideIn(SlideGestureRecognizer.this.mouseInButton)) {
                    Toolkit.getDefaultToolkit().addAWTEventListener(this, 32);
                } else {
                    this.autoSlideActive = false;
                }
            } else {
                this.initialX = SlideGestureRecognizer.this.curMouseLocX;
                this.initialY = SlideGestureRecognizer.this.curMouseLocY;
            }
        }

        private void slideOutTimerReaction(ActionEvent evt) {
            LOG.fine("slideOutTimerReaction entered, trying to auto slide out");
            this.slideOutTimer.stop();
            this.autoSlideOutIfNeeded();
        }

        @Override
        public void eventDispatched(AWTEvent event) {
            this.notifySlideOutTimer((MouseEvent)event);
        }

        private void autoSlideOutIfNeeded() {
            if (!this.autoSlideActive) {
                return;
            }
            if (SlideGestureRecognizer.this.slideBar.isActive()) {
                this.cleanup();
                return;
            }
            this.cleanup();
            this.autoSlideOut();
        }

        private void autoSlideOut() {
            SlideGestureRecognizer.this.slideBar.userTriggeredAutoSlideOut();
        }

        private void cleanup() {
            Toolkit.getDefaultToolkit().removeAWTEventListener(this);
            this.autoSlideActive = false;
            this.activeArea = null;
        }

        private boolean isSlideInGesture() {
            if (SlideGestureRecognizer.this.mouseInButton == null) {
                return false;
            }
            int diffX = Math.abs(this.initialX - SlideGestureRecognizer.this.curMouseLocX);
            int diffY = Math.abs(this.initialY - SlideGestureRecognizer.this.curMouseLocY);
            return diffX <= 2 && diffY <= 2;
        }

        private boolean isSlideOutGesture(MouseEvent evt) {
            if (SlideGestureRecognizer.this.resizer.isDragging()) {
                this.activeArea = null;
                return false;
            }
            if (this.activeArea == null) {
                this.activeArea = this.computeActiveArea();
                if (this.activeArea == null) {
                    return false;
                }
            }
            Point mouseLoc = evt.getPoint();
            if (!(evt.getSource() instanceof Component)) {
                return false;
            }
            SwingUtilities.convertPointToScreen(mouseLoc, (Component)evt.getSource());
            boolean isMouseOut = !this.activeArea.contains(mouseLoc);
            return isMouseOut;
        }

        private Rectangle computeActiveArea() {
            Component slidedComp = SlideGestureRecognizer.this.slideBar.getSlidedComp();
            if (slidedComp == null || !slidedComp.isShowing()) {
                return null;
            }
            Point slideBarLoc = SlideGestureRecognizer.this.slideBar.getLocationOnScreen();
            Rectangle actArea = new Rectangle(slideBarLoc.x - 1, slideBarLoc.y - 1, SlideGestureRecognizer.this.slideBar.getWidth() - 1, SlideGestureRecognizer.this.slideBar.getHeight() - 1);
            Point slidedCompLoc = slidedComp.getLocationOnScreen();
            int slidex = slidedCompLoc.x;
            int slidey = slidedCompLoc.y;
            int slideh = slidedComp.getHeight();
            int slidew = slidedComp.getWidth();
            int orientation = SlideGestureRecognizer.this.slideBar.getModel().getOrientation();
            if (orientation == 2) {
                slidew += 8;
            }
            if (orientation == 1) {
                slidew += 8;
                slidex -= 8;
            }
            if (orientation == 3) {
                slideh += 8;
                slidey -= 8;
            }
            if (orientation == 4) {
                slideh += 8;
            }
            actArea = SwingUtilities.computeUnion(slidex, slidey, slidew, slideh, actArea);
            return actArea;
        }

        private void notifySlideOutTimer(MouseEvent evt) {
            if (!this.autoSlideActive) {
                return;
            }
            if (!this.isSlideOutGesture(evt)) {
                if (this.slideOutTimer != null && this.slideOutTimer.isRunning()) {
                    this.slideOutTimer.stop();
                    LOG.fine("notifySlideOutTimer: slide out gesture not satisfied, stopping auto slide out");
                }
                return;
            }
            if (this.slideOutTimer == null) {
                this.slideOutTimer = new Timer(400, this);
                this.slideOutTimer.setRepeats(false);
                LOG.fine("notifySlideOutTimer: created slideOutTimer");
            }
            if (!this.slideOutTimer.isRunning()) {
                this.slideOutTimer.start();
                LOG.fine("notifySlideOutTimer: started slideoutTimer");
            }
        }
    }

}

