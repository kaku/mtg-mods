/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view.ui.slides;

import java.awt.Component;
import java.awt.event.MouseEvent;
import org.netbeans.core.windows.view.ui.slides.SlideController;

public interface SlideBarController
extends SlideController {
    public void userClickedSlidingButton(Component var1);

    public void userMiddleClickedSlidingButton(Component var1);

    public void userTriggeredPopup(MouseEvent var1, Component var2);

    public boolean userTriggeredAutoSlideIn(Component var1);

    public void userTriggeredAutoSlideOut();
}

