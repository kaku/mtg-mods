/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowStateListener;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.netbeans.core.windows.Constants;
import org.netbeans.core.windows.Debug;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.view.Controller;
import org.netbeans.core.windows.view.EditorAccessor;
import org.netbeans.core.windows.view.EditorView;
import org.netbeans.core.windows.view.ElementAccessor;
import org.netbeans.core.windows.view.ModeAccessor;
import org.netbeans.core.windows.view.ModeStructureAccessor;
import org.netbeans.core.windows.view.ModeView;
import org.netbeans.core.windows.view.SlidingAccessor;
import org.netbeans.core.windows.view.SlidingView;
import org.netbeans.core.windows.view.SplitAccessor;
import org.netbeans.core.windows.view.SplitView;
import org.netbeans.core.windows.view.ViewElement;
import org.netbeans.core.windows.view.WindowSystemAccessor;
import org.netbeans.core.windows.view.dnd.TopComponentDraggable;
import org.netbeans.core.windows.view.dnd.WindowDnDManager;
import org.netbeans.core.windows.view.ui.DesktopImpl;
import org.netbeans.core.windows.view.ui.EditorAreaFrame;
import org.netbeans.core.windows.view.ui.MainWindow;
import org.netbeans.core.windows.view.ui.slides.SlideOperation;
import org.openide.windows.TopComponent;

final class ViewHierarchy {
    private final Controller controller;
    private final WindowDnDManager windowDnDManager;
    private DesktopImpl desktop = null;
    private final Map<ModeView, ModeAccessor> separateModeViews = new HashMap<ModeView, ModeAccessor>(10);
    private final Map<SlidingView, SlidingAccessor> slidingModeViews = new HashMap<SlidingView, SlidingAccessor>(10);
    private EditorAreaFrame editorAreaFrame;
    private ModeView activeModeView;
    private ModeView maximizedModeView;
    private ViewElement currentSplitRoot;
    private WeakReference<ModeView> lastNonSlidingActive;
    private final Map<ElementAccessor, ViewElement> accessor2view = new HashMap<ElementAccessor, ViewElement>(10);
    private final Map<ViewElement, ElementAccessor> view2accessor = new HashMap<ViewElement, ElementAccessor>(10);
    private MainWindow mainWindow;
    private final MainWindowListener mainWindowListener;
    private ViewElement fakeSplitRoot;

    public ViewHierarchy(Controller controller, WindowDnDManager windowDnDManager) {
        this.controller = controller;
        this.windowDnDManager = windowDnDManager;
        this.mainWindowListener = new MainWindowListener(controller, this);
    }

    public boolean isDragInProgress() {
        return this.windowDnDManager.isDragging();
    }

    public MainWindow getMainWindow() {
        if (this.mainWindow == null) {
            JRootPane root;
            JFrame mainFrame = null;
            for (Frame f : Frame.getFrames()) {
                JFrame frame;
                if (!(f instanceof JFrame) || !"NbMainWindow".equals((frame = (JFrame)f).getName())) continue;
                mainFrame = frame;
                break;
            }
            if (null == mainFrame) {
                mainFrame = new JFrame();
                mainFrame.setName("NbMainWindow");
            }
            if ("Aqua".equals(UIManager.getLookAndFeel().getID()) && null == System.getProperty("apple.awt.brushMetalLook") && null != (root = mainFrame.getRootPane())) {
                root.putClientProperty("apple.awt.brushMetalLook", Boolean.TRUE);
            }
            Logger.getLogger(MainWindow.class.getName()).log(Level.FINE, "Installing MainWindow into " + mainFrame);
            this.mainWindow = MainWindow.install(mainFrame);
        }
        return this.mainWindow;
    }

    public void installMainWindowListeners() {
        this.mainWindow.getFrame().addComponentListener(this.mainWindowListener);
        this.mainWindow.getFrame().addWindowStateListener(this.mainWindowListener);
    }

    public void uninstallMainWindowListeners() {
        this.mainWindow.getFrame().removeComponentListener(this.mainWindowListener);
        this.mainWindow.getFrame().removeWindowStateListener(this.mainWindowListener);
    }

    public void updateViewHierarchy(ModeStructureAccessor modeStructureAccessor) {
        this.updateAccessors(modeStructureAccessor);
        this.currentSplitRoot = this.updateViewForAccessor(modeStructureAccessor.getSplitRootAccessor());
        if (null == this.currentSplitRoot && this.shouldUseFakeSplitRoot()) {
            this.currentSplitRoot = this.getFakeSplitRoot();
        }
        if (this.getDesktop().getSplitRoot() == null) {
            this.setSplitRootIntoDesktop(this.currentSplitRoot);
        }
        this.updateSeparateViews(modeStructureAccessor.getSeparateModeAccessors());
        this.updateSlidingViews(modeStructureAccessor.getSlidingModeAccessors());
    }

    public void updateAccessors(ModeStructureAccessor modeStructureAccessor) {
        HashMap<ElementAccessor, ViewElement> a2v = new HashMap<ElementAccessor, ViewElement>(this.accessor2view);
        this.accessor2view.clear();
        this.view2accessor.clear();
        Set<ElementAccessor> accessors = this.getAllAccessorsForTree(modeStructureAccessor.getSplitRootAccessor());
        accessors.addAll(Arrays.asList(modeStructureAccessor.getSeparateModeAccessors()));
        accessors.addAll(Arrays.asList(modeStructureAccessor.getSlidingModeAccessors()));
        for (ElementAccessor accessor : accessors) {
            ElementAccessor similar = this.findSimilarAccessor(accessor, a2v);
            if (similar == null) continue;
            ViewElement view = a2v.get(similar);
            this.accessor2view.put(accessor, view);
            this.view2accessor.put(view, accessor);
        }
    }

    private Set<ElementAccessor> getAllAccessorsForTree(ElementAccessor accessor) {
        HashSet<ElementAccessor> s = new HashSet<ElementAccessor>();
        if (accessor instanceof ModeAccessor) {
            s.add(accessor);
        } else if (accessor instanceof SplitAccessor) {
            SplitAccessor sa = (SplitAccessor)accessor;
            s.add(sa);
            ElementAccessor[] children = sa.getChildren();
            for (int i = 0; i < children.length; ++i) {
                s.addAll(this.getAllAccessorsForTree(children[i]));
            }
        } else if (accessor instanceof EditorAccessor) {
            EditorAccessor ea = (EditorAccessor)accessor;
            s.add(ea);
            s.addAll(this.getAllAccessorsForTree(ea.getEditorAreaAccessor()));
        }
        return s;
    }

    private ElementAccessor findSimilarAccessor(ElementAccessor accessor, Map a2v) {
        for (ElementAccessor next : a2v.keySet()) {
            if (!accessor.originatorEquals(next)) continue;
            return next;
        }
        return null;
    }

    private ViewElement updateViewForAccessor(ElementAccessor patternAccessor) {
        if (patternAccessor == null) {
            return null;
        }
        ViewElement view = this.accessor2view.get(patternAccessor);
        if (view != null) {
            if (patternAccessor instanceof SplitAccessor) {
                SplitAccessor sa = (SplitAccessor)patternAccessor;
                ElementAccessor[] childAccessors = sa.getChildren();
                ArrayList<ViewElement> childViews = new ArrayList<ViewElement>(childAccessors.length);
                for (int i = 0; i < childAccessors.length; ++i) {
                    childViews.add(this.updateViewForAccessor(childAccessors[i]));
                }
                double[] splitWeights = sa.getSplitWeights();
                ArrayList<Double> weights = new ArrayList<Double>(splitWeights.length);
                for (int i2 = 0; i2 < splitWeights.length; ++i2) {
                    weights.add(splitWeights[i2]);
                }
                SplitView sv = (SplitView)view;
                sv.setOrientation(sa.getOrientation());
                sv.setSplitWeights(weights);
                sv.setChildren(childViews);
                return sv;
            }
            if (patternAccessor instanceof EditorAccessor) {
                EditorAccessor ea = (EditorAccessor)patternAccessor;
                EditorView ev = (EditorView)view;
                ev.setEditorArea(this.updateViewForAccessor(ea.getEditorAreaAccessor()));
                return ev;
            }
            if (patternAccessor instanceof SlidingAccessor) {
                SlidingAccessor sa = (SlidingAccessor)patternAccessor;
                SlidingView sv = (SlidingView)view;
                sv.setTopComponents(sa.getOpenedTopComponents(), sa.getSelectedTopComponent());
                sv.setSlideBounds(sa.getBounds());
                sv.setSlideInSizes(sa.getSlideInSizes());
                return sv;
            }
            if (patternAccessor instanceof ModeAccessor) {
                ModeAccessor ma = (ModeAccessor)patternAccessor;
                ModeView mv = (ModeView)view;
                mv.setTopComponents(ma.getOpenedTopComponents(), ma.getSelectedTopComponent());
                if (ma.getState() == 1) {
                    mv.setFrameState(ma.getFrameState());
                }
                return mv;
            }
        } else {
            if (patternAccessor instanceof SplitAccessor) {
                SplitAccessor sa = (SplitAccessor)patternAccessor;
                ArrayList<Double> weights = new ArrayList<Double>(sa.getSplitWeights().length);
                for (int i = 0; i < sa.getSplitWeights().length; ++i) {
                    weights.add(sa.getSplitWeights()[i]);
                }
                ArrayList<ViewElement> children = new ArrayList<ViewElement>(sa.getChildren().length);
                for (int i3 = 0; i3 < sa.getChildren().length; ++i3) {
                    children.add(this.updateViewForAccessor(sa.getChildren()[i3]));
                }
                SplitView sv = new SplitView(this.controller, sa.getResizeWeight(), sa.getOrientation(), weights, children);
                this.accessor2view.put(patternAccessor, sv);
                this.view2accessor.put(sv, patternAccessor);
                return sv;
            }
            if (patternAccessor instanceof SlidingAccessor) {
                SlidingAccessor sa = (SlidingAccessor)patternAccessor;
                SlidingView sv = new SlidingView(this.controller, this.windowDnDManager, sa.getOpenedTopComponents(), sa.getSelectedTopComponent(), sa.getSide(), sa.getSlideInSizes());
                sv.setSlideBounds(sa.getBounds());
                this.accessor2view.put(patternAccessor, sv);
                this.view2accessor.put(sv, patternAccessor);
                return sv;
            }
            if (patternAccessor instanceof ModeAccessor) {
                ModeAccessor ma = (ModeAccessor)patternAccessor;
                ModeView mv = ma.getState() == 0 ? new ModeView(this.controller, this.windowDnDManager, ma.getResizeWeight(), ma.getKind(), ma.getOpenedTopComponents(), ma.getSelectedTopComponent()) : new ModeView(this.controller, this.windowDnDManager, ma.getBounds(), ma.getKind(), ma.getFrameState(), ma.getOpenedTopComponents(), ma.getSelectedTopComponent());
                this.accessor2view.put(patternAccessor, mv);
                this.view2accessor.put(mv, patternAccessor);
                return mv;
            }
            if (patternAccessor instanceof EditorAccessor) {
                EditorAccessor editorAccessor = (EditorAccessor)patternAccessor;
                EditorView ev = new EditorView(this.controller, this.windowDnDManager, editorAccessor.getResizeWeight(), this.updateViewForAccessor(editorAccessor.getEditorAreaAccessor()));
                this.accessor2view.put(patternAccessor, ev);
                this.view2accessor.put(ev, patternAccessor);
                return ev;
            }
        }
        throw new IllegalStateException("Unknown accessor type, accessor=" + patternAccessor);
    }

    private void updateSeparateViews(ModeAccessor[] separateModeAccessors) {
        HashMap<ModeView, ModeAccessor> newViews = new HashMap<ModeView, ModeAccessor>();
        for (int i = 0; i < separateModeAccessors.length; ++i) {
            ModeAccessor ma = separateModeAccessors[i];
            ModeView mv2 = (ModeView)this.updateViewForAccessor(ma);
            newViews.put(mv2, ma);
        }
        HashSet<ModeView> oldViews = new HashSet<ModeView>(this.separateModeViews.keySet());
        oldViews.removeAll(newViews.keySet());
        this.separateModeViews.clear();
        this.separateModeViews.putAll(newViews);
        if (WindowManagerImpl.getInstance().getMainWindow().isVisible()) {
            Component comp;
            for (ModeView mv2 : oldViews) {
                comp = mv2.getComponent();
                if (comp.isVisible()) {
                    comp.setVisible(false);
                }
                ((Window)comp).dispose();
            }
            for (ModeView mv2 : newViews.keySet()) {
                comp = mv2.getComponent();
                if (comp.isVisible()) continue;
                comp.setVisible(true);
            }
        }
    }

    private void updateSlidingViews(SlidingAccessor[] slidingModeAccessors) {
        HashMap<SlidingView, SlidingAccessor> newViews = new HashMap<SlidingView, SlidingAccessor>();
        for (int i = 0; i < slidingModeAccessors.length; ++i) {
            SlidingAccessor sa = slidingModeAccessors[i];
            SlidingView sv = (SlidingView)this.updateViewForAccessor(sa);
            newViews.put(sv, sa);
        }
        HashSet<SlidingView> oldViews = new HashSet<SlidingView>(this.slidingModeViews.keySet());
        oldViews.removeAll(newViews.keySet());
        HashSet addedViews = new HashSet(newViews.keySet());
        addedViews.removeAll(this.slidingModeViews.keySet());
        this.slidingModeViews.clear();
        this.slidingModeViews.putAll(newViews);
        for (SlidingView curSv2 : oldViews) {
            this.getDesktop().removeSlidingView(curSv2);
        }
        for (SlidingView curSv2 : addedViews) {
            this.getDesktop().addSlidingView(curSv2);
        }
        this.getDesktop().updateCorners();
    }

    public ModeView getModeViewForAccessor(ModeAccessor modeAccessor) {
        return (ModeView)this.accessor2view.get(modeAccessor);
    }

    public ElementAccessor getAccessorForView(ViewElement view) {
        return this.view2accessor.get(view);
    }

    public void activateMode(ModeAccessor activeModeAccessor) {
        ModeView activeModeV = this.getModeViewForAccessor(activeModeAccessor);
        this.activateModeView(activeModeV);
    }

    private void activateModeView(ModeView modeView) {
        this.setActiveModeView(modeView);
        if (modeView != null) {
            modeView.focusSelectedTopComponent();
            if (!(modeView instanceof SlidingView)) {
                this.lastNonSlidingActive = new WeakReference<ModeView>(modeView);
            }
        }
    }

    private void setActiveModeView(ModeView modeView) {
        if (modeView == this.activeModeView && this.activeModeView != null && this.activeModeView.isActive()) {
            return;
        }
        if (this.activeModeView != null && modeView != this.activeModeView) {
            this.activeModeView.setActive(false);
        }
        this.activeModeView = modeView;
        if (this.activeModeView != null) {
            this.activeModeView.setActive(true);
        }
    }

    public ModeView getActiveModeView() {
        return this.activeModeView;
    }

    ModeView getLastNonSlidingActiveModeView() {
        return this.lastNonSlidingActive == null ? null : this.lastNonSlidingActive.get();
    }

    public void setMaximizedModeView(ModeView modeView) {
        if (modeView == this.maximizedModeView) {
            return;
        }
        this.maximizedModeView = modeView;
    }

    public ModeView getMaximizedModeView() {
        return this.maximizedModeView;
    }

    public void removeModeView(ModeView modeView) {
        if (!this.view2accessor.containsKey(modeView)) {
            return;
        }
        ElementAccessor accessor = this.view2accessor.remove(modeView);
        this.accessor2view.remove(accessor);
        if (this.separateModeViews.keySet().contains(modeView)) {
            this.separateModeViews.keySet().remove(modeView);
            modeView.getComponent().setVisible(false);
            return;
        }
        this.setSplitRootIntoDesktop((SplitView)this.removeModeViewFromElement(this.getDesktop().getSplitRoot(), modeView));
    }

    public Set<Component> getModeComponents() {
        HashSet<Component> set = new HashSet<Component>();
        for (ViewElement next : this.view2accessor.keySet()) {
            if (!(next instanceof ModeView)) continue;
            ModeView modeView = (ModeView)next;
            set.add(modeView.getComponent());
        }
        return set;
    }

    public Component getSlidingModeComponent(String side) {
        for (SlidingView mod : this.slidingModeViews.keySet()) {
            if (!mod.getSide().equals(side)) continue;
            return mod.getComponent();
        }
        return null;
    }

    public Set<Component> getSeparateModeFrames() {
        HashSet<Component> s = new HashSet<Component>();
        for (ModeView modeView : this.separateModeViews.keySet()) {
            s.add(modeView.getComponent());
        }
        if (this.editorAreaFrame != null) {
            s.add(this.editorAreaFrame);
        }
        return s;
    }

    private ViewElement removeModeViewFromElement(ViewElement view, ModeView modeView) {
        if (view == modeView) {
            return null;
        }
        if (view instanceof SplitView) {
            SplitView sv = (SplitView)view;
            List<ViewElement> children = sv.getChildren();
            ArrayList<ViewElement> newChildren = new ArrayList<ViewElement>(children.size());
            ViewElement removedView = null;
            for (ViewElement child : children) {
                ViewElement newChild = this.removeModeViewFromElement(child, modeView);
                if (newChild != child) {
                    removedView = child;
                }
                if (null == newChild) continue;
                newChildren.add(newChild);
            }
            if (newChildren.size() == 0) {
                return (ViewElement)newChildren.get(0);
            }
            if (null != removedView) {
                sv.remove(removedView);
            }
            sv.setChildren(newChildren);
            return sv;
        }
        if (view instanceof EditorView) {
            EditorView ev = (EditorView)view;
            ev.setEditorArea(this.removeModeViewFromElement(ev.getEditorArea(), modeView));
            return ev;
        }
        return view;
    }

    private Component getDesktopComponent() {
        return this.currentSplitRoot == null ? null : this.getDesktop().getDesktopComponent();
    }

    public ViewElement getSplitRootElement() {
        return this.currentSplitRoot;
    }

    public void releaseAll() {
        this.setSplitRootIntoDesktop(null);
        this.separateModeViews.clear();
        this.activeModeView = null;
        this.accessor2view.clear();
    }

    public void setSplitModesVisible(boolean visible) {
        ViewHierarchy.setVisibleModeElement(this.getDesktop().getSplitRoot(), visible);
    }

    private static void setVisibleModeElement(ViewElement view, boolean visible) {
        if (view instanceof ModeView) {
            view.getComponent().setVisible(visible);
        } else if (view instanceof SplitView) {
            SplitView sv = (SplitView)view;
            List<ViewElement> children = sv.getChildren();
            for (ViewElement child : children) {
                ViewHierarchy.setVisibleModeElement(child, visible);
            }
        } else if (view instanceof EditorView) {
            ViewHierarchy.setVisibleModeElement(((EditorView)view).getEditorArea(), visible);
        }
    }

    public void setSeparateModesVisible(boolean visible) {
        if (this.editorAreaFrame != null && this.editorAreaFrame.isVisible() != visible) {
            this.editorAreaFrame.setVisible(visible);
        }
        for (ModeView mv : this.separateModeViews.keySet()) {
            if (mv.getComponent().isVisible() == visible) continue;
            mv.getComponent().setVisible(visible);
        }
    }

    public void updateEditorAreaFrameState(int frameState) {
        if (this.editorAreaFrame != null) {
            this.editorAreaFrame.setExtendedState(frameState);
        }
    }

    public void updateFrameStates() {
        for (ModeView mv : this.separateModeViews.keySet()) {
            mv.updateFrameState();
        }
    }

    public void updateMainWindowBounds(WindowSystemAccessor wsa) {
        JFrame frame = this.mainWindow.getFrame();
        if (wsa.getEditorAreaState() == 0) {
            frame.setBounds(wsa.getMainWindowBoundsJoined());
        } else {
            this.setMainWindowDesktop(null);
            frame.invalidate();
            frame.setBounds(wsa.getMainWindowBoundsSeparated());
        }
        frame.validate();
    }

    private void setMaximizedViewIntoDesktop(ViewElement elem) {
        boolean revalidate = elem.updateAWTHierarchy(this.getDesktop().getInnerPaneDimension());
        this.getDesktop().setMaximizedView(elem);
        if (revalidate) {
            this.getDesktop().getDesktopComponent().invalidate();
            ((JComponent)this.getDesktop().getDesktopComponent()).revalidate();
            this.getDesktop().getDesktopComponent().repaint();
        }
    }

    private void setSplitRootIntoDesktop(ViewElement root) {
        boolean revalidate = false;
        this.getDesktop().setSplitRoot(root);
        if (root != null) {
            Dimension dim = this.getDesktop().getInnerPaneDimension();
            revalidate = root.updateAWTHierarchy(dim);
        }
        if (revalidate) {
            this.getDesktop().getDesktopComponent().invalidate();
            ((JComponent)this.getDesktop().getDesktopComponent()).revalidate();
            this.getDesktop().getDesktopComponent().repaint();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void updateDesktop(WindowSystemAccessor wsa) {
        Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        List focusOwnerAWTHierarchyChain = focusOwner != null ? this.getComponentAWTHierarchyChain(focusOwner) : Collections.emptyList();
        try {
            if (wsa.getEditorAreaState() == 0 && this.maximizedModeView != null) {
                this.setMainWindowDesktop(this.getDesktopComponent());
                this.setMaximizedViewIntoDesktop(this.maximizedModeView);
                return;
            }
            int editorAreaState = wsa.getEditorAreaState();
            if (editorAreaState == 0) {
                if (this.editorAreaFrame != null) {
                    this.editorAreaFrame.setVisible(false);
                    this.editorAreaFrame = null;
                }
                this.setMainWindowDesktop(this.getDesktopComponent());
                this.setSplitRootIntoDesktop(this.getSplitRootElement());
            } else {
                boolean showEditorFrame = this.hasEditorAreaVisibleView();
                if (this.editorAreaFrame == null && showEditorFrame) {
                    this.editorAreaFrame = this.createEditorAreaFrame();
                    Rectangle editorAreaBounds = wsa.getEditorAreaBounds();
                    if (editorAreaBounds != null) {
                        this.editorAreaFrame.setBounds(editorAreaBounds);
                    }
                } else if (this.editorAreaFrame != null && !showEditorFrame) {
                    this.editorAreaFrame.setVisible(false);
                    this.editorAreaFrame = null;
                }
                this.setMainWindowDesktop(null);
                if (showEditorFrame) {
                    this.setSplitRootIntoDesktop(this.getSplitRootElement());
                    this.setEditorAreaDesktop(this.getDesktopComponent());
                    this.updateEditorAreaFrameState(wsa.getEditorAreaFrameState());
                }
            }
        }
        finally {
            if (focusOwner != null && !focusOwnerAWTHierarchyChain.equals(this.getComponentAWTHierarchyChain(focusOwner)) && SwingUtilities.getAncestorOfClass(Window.class, focusOwner) != null) {
                focusOwner.requestFocus();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void updateDesktop() {
        block8 : {
            Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
            List<Component> focusOwnerAWTHierarchyChain = focusOwner != null ? this.getComponentAWTHierarchyChain(focusOwner) : Collections.EMPTY_LIST;
            try {
                if (this.mainWindow.hasDesktop()) {
                    this.setMainWindowDesktop(this.getDesktopComponent());
                    if (this.maximizedModeView != null) {
                        this.setMaximizedViewIntoDesktop(this.maximizedModeView);
                    } else {
                        this.setSplitRootIntoDesktop(this.getSplitRootElement());
                    }
                    break block8;
                }
                boolean showEditorFrame = this.hasEditorAreaVisibleView();
                if (this.editorAreaFrame == null) break block8;
                if (showEditorFrame) {
                    this.editorAreaFrame.setDesktop(this.getDesktopComponent());
                    break block8;
                }
                this.editorAreaFrame.setVisible(false);
                this.editorAreaFrame = null;
            }
            finally {
                if (focusOwner != null && !focusOwnerAWTHierarchyChain.equals(this.getComponentAWTHierarchyChain(focusOwner)) && SwingUtilities.getAncestorOfClass(Window.class, focusOwner) != null) {
                    focusOwner.requestFocus();
                }
            }
        }
    }

    public void performSlideIn(SlideOperation operation) {
        this.getDesktop().performSlideIn(operation, this.getPureEditorAreaBounds());
    }

    public void performSlideOut(SlideOperation operation) {
        this.getDesktop().performSlideOut(operation, this.getPureEditorAreaBounds());
    }

    public void performSlideIntoDesktop(SlideOperation operation) {
        this.getDesktop().performSlideIntoDesktop(operation, this.getPureEditorAreaBounds());
    }

    public void performSlideIntoEdge(SlideOperation operation) {
        this.getDesktop().performSlideIntoEdge(operation, this.getPureEditorAreaBounds());
    }

    public void performSlideResize(SlideOperation operation) {
        this.getDesktop().performSlideResize(operation);
    }

    public void performSlideToggleMaximize(TopComponent tc, String side) {
        this.getDesktop().performSlideToggleMaximize(tc, side, this.getPureEditorAreaBounds());
    }

    private void setMainWindowDesktop(Component component) {
        this.setDesktop(component, true);
    }

    private void setEditorAreaDesktop(Component component) {
        this.setDesktop(component, false);
    }

    private void setDesktop(Component component, boolean toMainWindow) {
        if (toMainWindow) {
            this.mainWindow.setDesktop(component);
        } else {
            this.editorAreaFrame.setDesktop(component);
        }
    }

    private List<Component> getComponentAWTHierarchyChain(Component comp) {
        ArrayList<Component> l = new ArrayList<Component>();
        for (Component c = comp; c != null; c = c.getParent()) {
            l.add(c);
        }
        Collections.reverse(l);
        return l;
    }

    private boolean hasEditorAreaVisibleView() {
        EditorView view = this.findEditorAreaElement();
        return view != null ? view.getEditorArea() != null : false;
    }

    private EditorAreaFrame createEditorAreaFrame() {
        final EditorAreaFrame frame = new EditorAreaFrame();
        frame.addComponentListener(new ComponentAdapter(){

            @Override
            public void componentResized(ComponentEvent evt) {
                if (frame.getExtendedState() == 6) {
                    return;
                }
                ViewHierarchy.this.controller.userResizedEditorArea(frame.getBounds());
            }

            @Override
            public void componentMoved(ComponentEvent evt) {
                if (frame.getExtendedState() == 6) {
                    return;
                }
                ViewHierarchy.this.controller.userResizedEditorArea(frame.getBounds());
            }
        });
        frame.setWindowActivationListener(this.controller);
        frame.addWindowListener(new WindowAdapter(){

            @Override
            public void windowClosing(WindowEvent evt) {
                ViewHierarchy.this.closeEditorModes();
            }
        });
        frame.addWindowStateListener(new WindowStateListener(){

            @Override
            public void windowStateChanged(WindowEvent evt) {
                long currentStamp = System.currentTimeMillis();
                if (currentStamp > frame.getUserStamp() + 500 && currentStamp > frame.getMainWindowStamp() + 1000) {
                    ViewHierarchy.this.controller.userChangedFrameStateEditorArea(evt.getNewState());
                    long stamp = System.currentTimeMillis();
                    frame.setUserStamp(stamp);
                } else {
                    frame.setUserStamp(0);
                    frame.setMainWindowStamp(0);
                    frame.setExtendedState(evt.getOldState());
                }
            }
        });
        return frame;
    }

    private void closeEditorModes() {
        this.closeModeForView(this.findEditorAreaElement().getEditorArea());
    }

    private void closeModeForView(ViewElement view) {
        if (view instanceof ModeView) {
            this.controller.userClosingMode((ModeView)view);
        } else if (view instanceof SplitView) {
            SplitView sv = (SplitView)view;
            List<ViewElement> children = sv.getChildren();
            for (ViewElement child : children) {
                this.closeModeForView(child);
            }
        }
    }

    public void updateEditorAreaBounds(Rectangle bounds) {
        if (this.editorAreaFrame != null) {
            this.editorAreaFrame.setBounds(bounds);
        }
    }

    public Rectangle getPureEditorAreaBounds() {
        EditorView editorView = this.findEditorAreaElement();
        if (editorView == null) {
            return new Rectangle();
        }
        return editorView.getPureBounds();
    }

    private EditorView findEditorAreaElement() {
        return this.findEditorViewForElement(this.getSplitRootElement());
    }

    Component getEditorAreaComponent() {
        EditorView editor = this.findEditorAreaElement();
        if (null != editor) {
            return editor.getComponent();
        }
        return null;
    }

    private EditorView findEditorViewForElement(ViewElement view) {
        if (view instanceof EditorView) {
            return (EditorView)view;
        }
        if (view instanceof SplitView) {
            SplitView sv = (SplitView)view;
            List<ViewElement> children = sv.getChildren();
            for (ViewElement child : children) {
                EditorView ev = this.findEditorViewForElement(child);
                if (null == ev) continue;
                return ev;
            }
        }
        return null;
    }

    public void updateUI() {
        SwingUtilities.updateComponentTreeUI(this.mainWindow.getFrame());
        if (this.editorAreaFrame != null) {
            SwingUtilities.updateComponentTreeUI(this.editorAreaFrame);
        }
        for (ModeView mv : this.separateModeViews.keySet()) {
            SwingUtilities.updateComponentTreeUI(mv.getComponent());
        }
    }

    public Set<TopComponent> getShowingTopComponents() {
        HashSet<TopComponent> s = new HashSet<TopComponent>();
        for (ElementAccessor accessor2 : this.accessor2view.keySet()) {
            if (!(accessor2 instanceof ModeAccessor)) continue;
            s.add(((ModeAccessor)accessor2).getSelectedTopComponent());
        }
        for (ModeAccessor accessor : this.separateModeViews.values()) {
            s.add(accessor.getSelectedTopComponent());
        }
        return s;
    }

    public String toString() {
        return this.dumpElement(this.getDesktop().getSplitRoot(), 0) + "\nseparateViews=" + this.separateModeViews.keySet();
    }

    private String dumpElement(ViewElement view, int indent) {
        String indentString = ViewHierarchy.createIndentString(indent);
        StringBuffer sb = new StringBuffer();
        if (view instanceof ModeView) {
            sb.append(indentString + view + "->" + view.getComponent().getClass() + "@" + view.getComponent().hashCode());
        } else if (view instanceof EditorView) {
            sb.append(indentString + view);
            sb.append("\n" + this.dumpElement(((EditorView)view).getEditorArea(), ++indent));
        } else if (view instanceof SplitView) {
            sb.append(indentString + view + "->" + view.getComponent().getClass() + "@" + view.getComponent().hashCode());
            ++indent;
            List<ViewElement> children = ((SplitView)view).getChildren();
            for (ViewElement child : children) {
                sb.append("\n" + this.dumpElement(child, indent));
            }
        }
        return sb.toString();
    }

    private static String createIndentString(int indent) {
        StringBuffer sb = new StringBuffer(indent);
        for (int i = 0; i < indent; ++i) {
            sb.append("  ");
        }
        return sb.toString();
    }

    private String dumpAccessors() {
        StringBuffer sb = new StringBuffer();
        for (ElementAccessor accessor : this.accessor2view.keySet()) {
            sb.append("accessor=" + accessor + "\tview=" + this.accessor2view.get(accessor) + "\n");
        }
        return sb.toString();
    }

    private void changeStateOfSeparateViews(boolean iconify) {
        long mainStamp = System.currentTimeMillis();
        if (this.editorAreaFrame != null) {
            if (iconify && mainStamp < this.editorAreaFrame.getUserStamp() + 500) {
                int newState = this.editorAreaFrame.getExtendedState() & -2;
                this.controller.userChangedFrameStateEditorArea(newState);
                this.editorAreaFrame.setExtendedState(newState);
            }
            this.editorAreaFrame.setMainWindowStamp(mainStamp);
            this.editorAreaFrame.setVisible(!iconify);
        }
        for (ModeView mv : this.separateModeViews.keySet()) {
            Component comp = mv.getComponent();
            if (!(comp instanceof Frame)) continue;
            Frame fr = (Frame)comp;
            if (iconify && mainStamp < mv.getUserStamp() + 500) {
                int newState = fr.getExtendedState() & -2;
                this.controller.userChangedFrameStateMode(mv, newState);
                mv.setFrameState(newState);
            }
            mv.setMainWindowStamp(mainStamp);
            fr.setVisible(!iconify);
        }
    }

    void userStartedKeyboardDragAndDrop(TopComponentDraggable draggable) {
        this.windowDnDManager.startKeyboardDragAndDrop(draggable);
    }

    private static void debugLog(String message) {
        Debug.log(ViewHierarchy.class, message);
    }

    private boolean shouldUseFakeSplitRoot() {
        return Constants.SWITCH_HIDE_EMPTY_DOCUMENT_AREA;
    }

    private ViewElement getFakeSplitRoot() {
        if (null == this.fakeSplitRoot) {
            final JPanel panel = new JPanel();
            panel.setOpaque(false);
            this.fakeSplitRoot = new ViewElement(this.controller, 1.0){

                @Override
                public Component getComponent() {
                    return panel;
                }

                @Override
                public boolean updateAWTHierarchy(Dimension availableSpace) {
                    return false;
                }
            };
        }
        return this.fakeSplitRoot;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private DesktopImpl getDesktop() {
        ViewHierarchy viewHierarchy = this;
        synchronized (viewHierarchy) {
            if (null == this.desktop) {
                this.desktop = new DesktopImpl();
            }
        }
        return this.desktop;
    }

    private static class MainWindowListener
    extends ComponentAdapter
    implements WindowStateListener {
        private final Controller controller;
        private final ViewHierarchy hierarchy;

        public MainWindowListener(Controller controller, ViewHierarchy hierarchy) {
            this.controller = controller;
            this.hierarchy = hierarchy;
        }

        @Override
        public void componentResized(ComponentEvent evt) {
            this.controller.userResizedMainWindow(evt.getComponent().getBounds());
        }

        @Override
        public void componentMoved(ComponentEvent evt) {
            this.controller.userMovedMainWindow(evt.getComponent().getBounds());
        }

        @Override
        public void windowStateChanged(WindowEvent evt) {
            int oldState = evt.getOldState();
            int newState = evt.getNewState();
            this.controller.userChangedFrameStateMainWindow(newState);
            if (Constants.AUTO_ICONIFY) {
                if ((oldState & 1) == 0 && (newState & 1) == 1) {
                    this.hierarchy.changeStateOfSeparateViews(true);
                } else if ((oldState & 1) == 1 && (newState & 1) == 0) {
                    this.hierarchy.changeStateOfSeparateViews(false);
                }
            }
        }
    }

}

