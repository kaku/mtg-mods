/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.WindowSystem
 *  org.openide.util.Lookup
 */
package org.netbeans.core.windows.view.ui.tabcontrol;

import org.netbeans.core.WindowSystem;
import org.netbeans.core.windows.Switches;
import org.openide.util.Lookup;

public final class Utilities {
    private Utilities() {
    }

    public static void resetTabbedContainers() {
        WindowSystem ws = (WindowSystem)Lookup.getDefault().lookup(WindowSystem.class);
        ws.hide();
        ws.show();
    }

    public static boolean isEditorTopComponentClosingEnabled() {
        return Switches.isEditorTopComponentClosingEnabled();
    }
}

