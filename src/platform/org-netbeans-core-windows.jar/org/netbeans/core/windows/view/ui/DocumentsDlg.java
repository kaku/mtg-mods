/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.awt.Mnemonics
 *  org.openide.cookies.SaveCookie
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.view.ListView
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Array
 *  org.openide.nodes.Node
 *  org.openide.util.HelpCtx
 *  org.openide.util.HelpCtx$Provider
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.TopComponentTracker;
import org.netbeans.core.windows.WindowManagerImpl;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.awt.Mnemonics;
import org.openide.cookies.SaveCookie;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.ListView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public class DocumentsDlg
extends JPanel
implements PropertyChangeListener,
ExplorerManager.Provider,
HelpCtx.Provider {
    private static DocumentsDlg defaultInstance;
    private final ExplorerManager explorer = new ExplorerManager();
    private Dimension previousDialogSize;
    private ButtonGroup buttonGroup1;
    private JButton closeButton;
    private JTextArea descriptionArea;
    private JLabel descriptionLabel;
    private JLabel explorerLabel;
    private JPanel explorerPanel;
    private JButton jButtonActivate;
    private JButton jButtonClose;
    private JButton jButtonSave;
    private JScrollPane jScrollPane1;
    private JLabel lblOrderBy;
    private JRadioButton radioOrderByName;
    private JRadioButton radioOrderByUsage;
    private ListView listView;
    private static final Collator COLLATOR;

    private DocumentsDlg() {
        this.initComponents();
        Mnemonics.setLocalizedText((AbstractButton)this.jButtonActivate, (String)NbBundle.getMessage(DocumentsDlg.class, (String)"LBL_Activate"));
        Mnemonics.setLocalizedText((AbstractButton)this.jButtonClose, (String)NbBundle.getMessage(DocumentsDlg.class, (String)"LBL_CloseDocuments"));
        Mnemonics.setLocalizedText((AbstractButton)this.jButtonSave, (String)NbBundle.getMessage(DocumentsDlg.class, (String)"LBL_SaveDocuments"));
        Mnemonics.setLocalizedText((JLabel)this.explorerLabel, (String)NbBundle.getMessage(DocumentsDlg.class, (String)"LBL_Documents"));
        Mnemonics.setLocalizedText((JLabel)this.descriptionLabel, (String)NbBundle.getMessage(DocumentsDlg.class, (String)"LBL_Description"));
        Mnemonics.setLocalizedText((AbstractButton)this.closeButton, (String)NbBundle.getMessage(DocumentsDlg.class, (String)"LBL_Close"));
        this.explorerLabel.setLabelFor((Component)this.listView);
        this.descriptionLabel.setLabelFor(this.descriptionArea);
        this.jButtonActivate.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DocumentsDlg.class, (String)"ACSD_Activate"));
        this.jButtonClose.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DocumentsDlg.class, (String)"ACSD_CloseDocuments"));
        this.jButtonSave.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DocumentsDlg.class, (String)"ACSD_SaveDocuments"));
        this.closeButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DocumentsDlg.class, (String)"ACSD_Close"));
        this.descriptionArea.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DocumentsDlg.class, (String)"ACSD_DescriptionArea"));
        ItemListener orderingListener = new ItemListener(){

            @Override
            public void itemStateChanged(ItemEvent e) {
                DocumentsDlg.this.updateNodes();
            }
        };
        this.radioOrderByName.addItemListener(orderingListener);
        this.radioOrderByUsage.addItemListener(orderingListener);
    }

    private static DocumentsDlg getDefault() {
        if (defaultInstance == null) {
            defaultInstance = new DocumentsDlg();
        }
        return defaultInstance;
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(DocumentsDlg.class);
    }

    private void initComponents() {
        this.buttonGroup1 = new ButtonGroup();
        this.explorerLabel = new JLabel();
        this.descriptionLabel = new JLabel();
        this.explorerPanel = this.createListView();
        this.jScrollPane1 = new JScrollPane();
        this.descriptionArea = new JTextArea();
        this.jButtonActivate = new JButton();
        this.jButtonClose = new JButton();
        this.jButtonSave = new JButton();
        this.closeButton = new JButton();
        this.lblOrderBy = new JLabel();
        this.radioOrderByName = new JRadioButton();
        this.radioOrderByUsage = new JRadioButton();
        FormListener formListener = new FormListener();
        this.setBorder(BorderFactory.createEmptyBorder(12, 12, 0, 11));
        this.setLayout(new GridBagLayout());
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 16;
        gridBagConstraints.insets = new Insets(0, 0, 2, 0);
        this.add((Component)this.explorerLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.anchor = 16;
        gridBagConstraints.insets = new Insets(0, 0, 2, 0);
        this.add((Component)this.descriptionLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 7;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 0, 11, 11);
        this.add((Component)this.explorerPanel, gridBagConstraints);
        this.jScrollPane1.setPreferredSize(new Dimension(3, 60));
        this.descriptionArea.setEditable(false);
        this.jScrollPane1.setViewportView(this.descriptionArea);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 15;
        gridBagConstraints.insets = new Insets(0, 0, 11, 11);
        this.add((Component)this.jScrollPane1, gridBagConstraints);
        this.jButtonActivate.addActionListener(formListener);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 11;
        gridBagConstraints.insets = new Insets(0, 0, 5, 0);
        this.add((Component)this.jButtonActivate, gridBagConstraints);
        this.jButtonClose.addActionListener(formListener);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 11;
        gridBagConstraints.insets = new Insets(0, 0, 5, 0);
        this.add((Component)this.jButtonClose, gridBagConstraints);
        this.jButtonSave.addActionListener(formListener);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 11;
        gridBagConstraints.insets = new Insets(0, 0, 20, 0);
        this.add((Component)this.jButtonSave, gridBagConstraints);
        this.closeButton.addActionListener(formListener);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 15;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new Insets(0, 0, 11, 0);
        this.add((Component)this.closeButton, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.lblOrderBy, (String)NbBundle.getMessage(DocumentsDlg.class, (String)"Lbl_OrderBy"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = 17;
        this.add((Component)this.lblOrderBy, gridBagConstraints);
        this.buttonGroup1.add(this.radioOrderByName);
        this.radioOrderByName.setSelected(true);
        Mnemonics.setLocalizedText((AbstractButton)this.radioOrderByName, (String)NbBundle.getMessage(DocumentsDlg.class, (String)"Lbl_OrderByName"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 20, 0, 0);
        this.add((Component)this.radioOrderByName, gridBagConstraints);
        this.buttonGroup1.add(this.radioOrderByUsage);
        Mnemonics.setLocalizedText((AbstractButton)this.radioOrderByUsage, (String)NbBundle.getMessage(DocumentsDlg.class, (String)"Lbl_OrderByUsage"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 20, 10, 0);
        this.add((Component)this.radioOrderByUsage, gridBagConstraints);
    }

    private void closeButtonActionPerformed(ActionEvent evt) {
        this.closeDialog();
    }

    private void saveDocuments(ActionEvent evt) {
        Node[] selNodes = this.explorer.getSelectedNodes();
        if (selNodes.length == 0) {
            return;
        }
        for (int i = 0; i < selNodes.length; ++i) {
            TopComponent tc = ((TopComponentNode)selNodes[i]).getTopComponent();
            Lookup l = tc.getLookup();
            SaveCookie sc = (SaveCookie)l.lookup(SaveCookie.class);
            if (sc == null) continue;
            try {
                sc.save();
            }
            catch (IOException exc) {
                Logger.getAnonymousLogger().log(Level.WARNING, "[WinSys.DocumentsDlg.saveDocuments] Warning: Cannot save content of TopComponent: [" + WindowManagerImpl.getInstance().getTopComponentDisplayName(tc) + "]" + " [" + tc.getClass().getName() + "]", exc);
            }
            ((TopComponentNode)selNodes[i]).refresh();
        }
        this.jButtonSave.setEnabled(false);
    }

    private void closeDocuments(ActionEvent evt) {
        int i;
        Node[] selNodes = this.explorer.getSelectedNodes();
        if (selNodes.length == 0) {
            return;
        }
        int positionToSelectAfter = 0;
        Node[] children = this.explorer.getRootContext().getChildren().getNodes();
        for (i = 0; i < children.length; ++i) {
            if (children[i] != selNodes[0]) continue;
            positionToSelectAfter = Math.max(0, i - 1);
            break;
        }
        for (i = 0; i < selNodes.length; ++i) {
            TopComponent tc = ((TopComponentNode)selNodes[i]).getTopComponent();
            if (!Switches.isEditorTopComponentClosingEnabled() || !Switches.isClosingEnabled(tc)) continue;
            tc.close();
        }
        List<TopComponent> tcList = DocumentsDlg.getOpenedDocuments();
        ArrayList<TopComponentNode> tcNodes = new ArrayList<TopComponentNode>(tcList.size());
        for (TopComponent tc : tcList) {
            tcNodes.add(new TopComponentNode(tc));
        }
        if (tcList.isEmpty()) {
            this.closeDialog();
        } else {
            if (this.radioOrderByName.isSelected()) {
                Collections.sort(tcNodes);
            }
            Children.Array nodeArray = new Children.Array();
            nodeArray.add((Node[])tcNodes.toArray(new TopComponentNode[0]));
            AbstractNode root = new AbstractNode((Children)nodeArray);
            this.explorer.setRootContext((Node)root);
            try {
                this.explorer.setSelectedNodes(new Node[]{root.getChildren().getNodes()[positionToSelectAfter]});
            }
            catch (PropertyVetoException exc) {
                // empty catch block
            }
            this.listView.requestFocusInWindow();
        }
    }

    private void activate(ActionEvent evt) {
        Node[] selNodes = this.explorer.getSelectedNodes();
        if (selNodes.length == 0) {
            return;
        }
        this.closeDialog();
        final TopComponent tc = ((TopComponentNode)selNodes[0]).getTopComponent();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                WindowManagerImpl wm = WindowManagerImpl.getInstance();
                ModeImpl mode = (ModeImpl)wm.findMode(tc);
                if (mode != null && mode != wm.getCurrentMaximizedMode()) {
                    wm.switchMaximizedMode(null);
                }
                tc.requestActive();
            }
        });
    }

    private void closeDialog() {
        DocumentsDlg.getDefault().previousDialogSize = this.getSize();
        Window w = SwingUtilities.getWindowAncestor(this);
        w.setVisible(false);
        w.dispose();
    }

    @Override
    public void addNotify() {
        this.explorer.addPropertyChangeListener((PropertyChangeListener)this);
        this.jButtonActivate.setEnabled(false);
        this.jButtonClose.setEnabled(false);
        this.jButtonSave.setEnabled(false);
        super.addNotify();
    }

    @Override
    public void removeNotify() {
        Window dlg = SwingUtilities.getWindowAncestor(this);
        if (null != dlg) {
            DocumentsDlg.getDefault().previousDialogSize = dlg.getSize();
        }
        super.removeNotify();
        this.explorer.removePropertyChangeListener((PropertyChangeListener)this);
    }

    public static void showDocumentsDialog() {
        DocumentsDlg documentsPanel = DocumentsDlg.getDefault();
        DialogDescriptor dlgDesc = new DialogDescriptor((Object)documentsPanel, NbBundle.getMessage(DocumentsDlg.class, (String)"CTL_DocumentsTitle"), true, new Object[0], (Object)DocumentsDlg.getDefault().jButtonActivate, 0, null, null);
        dlgDesc.setHelpCtx(null);
        Dialog dlg = DialogDisplayer.getDefault().createDialog(dlgDesc);
        dlg.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DocumentsDlg.class, (String)"ACSD_DocumentsDialog"));
        if (dlg instanceof JDialog) {
            HelpCtx.setHelpIDString((JComponent)((JDialog)dlg).getRootPane(), (String)documentsPanel.getHelpCtx().getHelpID());
        }
        DocumentsDlg.getDefault().updateNodes();
        if (DocumentsDlg.getDefault().previousDialogSize != null) {
            dlg.setSize(DocumentsDlg.getDefault().previousDialogSize);
            dlg.setLocationRelativeTo(null);
        }
        dlg.setVisible(true);
        DocumentsDlg.getDefault().clearNodes();
    }

    public static boolean isEmpty() {
        for (ModeImpl elem : WindowManagerImpl.getInstance().getModes()) {
            ModeImpl mode = elem;
            if (mode.getKind() != 1 || mode.getOpenedTopComponents().isEmpty()) continue;
            return false;
        }
        return true;
    }

    private JPanel createListView() {
        JPanel panel = new JPanel();
        panel.setPreferredSize(new Dimension(375, 232));
        panel.setLayout(new BorderLayout());
        this.listView = new ListView();
        this.listView.setBorder((Border)UIManager.get("Nb.ScrollPane.border"));
        this.listView.setPopupAllowed(false);
        this.listView.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DocumentsDlg.class, (String)"ACSD_ListView"));
        this.listView.getInputMap(1).put(KeyStroke.getKeyStroke(127, 0), "closeSelected");
        this.listView.getActionMap().put("closeSelected", new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent e) {
                DocumentsDlg.this.closeDocuments(e);
            }
        });
        panel.add((Component)this.listView, "Center");
        return panel;
    }

    private void updateNodes() {
        List<TopComponent> tcList = DocumentsDlg.getOpenedDocuments();
        TopComponent activeTC = TopComponent.getRegistry().getActivated();
        Object[] tcNodes = new TopComponentNode[tcList.size()];
        Object toSelect = null;
        for (int i = 0; i < tcNodes.length; ++i) {
            TopComponent tc = tcList.get(i);
            tcNodes[i] = new TopComponentNode(tc);
            if (tc != activeTC) continue;
            toSelect = tcNodes[i];
        }
        if (this.radioOrderByName.isSelected()) {
            Arrays.sort(tcNodes);
        }
        Children.Array nodeArray = new Children.Array();
        nodeArray.add((Node[])tcNodes);
        AbstractNode root = new AbstractNode((Children)nodeArray);
        this.explorer.setRootContext((Node)root);
        this.listView.requestFocus();
        if (tcNodes.length > 0) {
            try {
                if (null == toSelect) {
                    toSelect = tcNodes[0];
                }
                this.explorer.setSelectedNodes(new Node[]{toSelect});
            }
            catch (PropertyVetoException exc) {
                // empty catch block
            }
        }
    }

    private void clearNodes() {
        this.explorer.setRootContext(Node.EMPTY);
    }

    private static List<TopComponent> getOpenedDocuments() {
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        TopComponent[] recentDocuments = wm.getRecentViewList();
        ArrayList<TopComponent> documents = new ArrayList<TopComponent>(recentDocuments.length);
        TopComponentTracker tcTracker = TopComponentTracker.getDefault();
        for (TopComponent tc : recentDocuments) {
            if (!tcTracker.isEditorTopComponent(tc)) continue;
            documents.add(tc);
        }
        return documents;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("selectedNodes".equals(evt.getPropertyName())) {
            Node[] selNodes = (Node[])evt.getNewValue();
            if (selNodes.length == 1) {
                this.jButtonActivate.setEnabled(true);
            } else {
                this.jButtonActivate.setEnabled(false);
            }
            if (selNodes.length > 0) {
                if (selNodes.length == 1) {
                    TopComponent tc = ((TopComponentNode)selNodes[0]).getTopComponent();
                    this.jButtonClose.setEnabled(Switches.isEditorTopComponentClosingEnabled() && Switches.isClosingEnabled(tc));
                } else {
                    this.jButtonClose.setEnabled(Switches.isEditorTopComponentClosingEnabled());
                }
            } else {
                this.jButtonClose.setEnabled(false);
            }
            boolean enableSave = false;
            for (int i = 0; i < selNodes.length; ++i) {
                TopComponent tc = ((TopComponentNode)selNodes[i]).getTopComponent();
                Lookup l = tc.getLookup();
                SaveCookie sc = (SaveCookie)l.lookup(SaveCookie.class);
                if (sc == null) continue;
                enableSave = true;
                break;
            }
            this.jButtonSave.setEnabled(enableSave);
            if (selNodes != null && selNodes.length == 1) {
                this.descriptionArea.setText(((TopComponentNode)selNodes[0]).getDescription());
            } else {
                this.descriptionArea.setText(null);
            }
        }
    }

    public ExplorerManager getExplorerManager() {
        return this.explorer;
    }

    static {
        COLLATOR = Collator.getInstance();
    }

    private class TopComponentNode
    extends AbstractNode
    implements Comparable<TopComponentNode>,
    Action,
    PropertyChangeListener {
        private TopComponent tc;

        public TopComponentNode(TopComponent tc) {
            super(Children.LEAF);
            this.tc = tc;
            tc.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)tc));
        }

        public String getName() {
            String result = this.tc.getName();
            if (null == result) {
                result = this.tc.getDisplayName();
            }
            if (null == result) {
                result = this.tc.toString();
            }
            return result;
        }

        public String getDisplayName() {
            String result = this.tc.getDisplayName();
            return result != null ? result : this.tc.getName();
        }

        public String getHtmlDisplayName() {
            return this.tc.getHtmlDisplayName();
        }

        public Image getIcon(int type) {
            Image image = this.tc.getIcon();
            return image == null ? ImageUtilities.loadImage((String)"org/openide/resources/actions/empty.gif") : image;
        }

        public String getDescription() {
            return this.tc.getToolTipText();
        }

        public String getShortDescription() {
            return this.tc.getToolTipText();
        }

        public TopComponent getTopComponent() {
            return this.tc;
        }

        void refresh() {
            this.fireNameChange(null, null);
        }

        @Override
        public int compareTo(TopComponentNode tcn) {
            String displayName1 = this.getDisplayName();
            String displayName2 = tcn.getDisplayName();
            if (displayName1 == null) {
                return displayName2 == null ? 0 : -1;
            }
            return displayName2 == null ? 1 : COLLATOR.compare(displayName1, displayName2);
        }

        public Action getPreferredAction() {
            return this;
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            DocumentsDlg.this.activate(evt);
        }

        @Override
        public boolean isEnabled() {
            return true;
        }

        @Override
        public void putValue(String key, Object value) {
        }

        @Override
        public void setEnabled(boolean b) {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            this.fireDisplayNameChange(null, null);
        }
    }

    private class FormListener
    implements ActionListener {
        FormListener() {
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            if (evt.getSource() == DocumentsDlg.this.jButtonActivate) {
                DocumentsDlg.this.activate(evt);
            } else if (evt.getSource() == DocumentsDlg.this.jButtonClose) {
                DocumentsDlg.this.closeDocuments(evt);
            } else if (evt.getSource() == DocumentsDlg.this.jButtonSave) {
                DocumentsDlg.this.saveDocuments(evt);
            } else if (evt.getSource() == DocumentsDlg.this.closeButton) {
                DocumentsDlg.this.closeButtonActionPerformed(evt);
            }
        }
    }

}

