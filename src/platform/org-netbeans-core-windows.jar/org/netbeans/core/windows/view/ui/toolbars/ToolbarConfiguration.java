/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.settings.Saver
 *  org.openide.awt.Actions
 *  org.openide.awt.Mnemonics
 *  org.openide.awt.Toolbar
 *  org.openide.awt.ToolbarPool
 *  org.openide.awt.ToolbarPool$Configuration
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.windows.view.ui.toolbars;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.netbeans.core.windows.view.ui.MainWindow;
import org.netbeans.core.windows.view.ui.toolbars.Bundle;
import org.netbeans.core.windows.view.ui.toolbars.ConfigureToolbarPanel;
import org.netbeans.core.windows.view.ui.toolbars.DnDSupport;
import org.netbeans.core.windows.view.ui.toolbars.ResetToolbarsAction;
import org.netbeans.core.windows.view.ui.toolbars.ToolbarConstraints;
import org.netbeans.core.windows.view.ui.toolbars.ToolbarContainer;
import org.netbeans.core.windows.view.ui.toolbars.ToolbarRow;
import org.netbeans.spi.settings.Saver;
import org.openide.awt.Actions;
import org.openide.awt.Mnemonics;
import org.openide.awt.Toolbar;
import org.openide.awt.ToolbarPool;
import org.openide.util.NbBundle;

public final class ToolbarConfiguration
implements ToolbarPool.Configuration {
    private final JPanel toolbarPanel;
    private static Map<String, ToolbarConfiguration> name2config = new HashMap<String, ToolbarConfiguration>(10);
    private static JMenu toolbarMenu;
    private final String configName;
    private final String configDisplayName;
    private final List<ToolbarRow> rows;
    private DnDSupport dndSupport;
    private Saver saver;
    private List<List<ToolbarConstraints>> snapshot;
    private static Boolean isXP;
    private static Color mid;
    private static final Border lowerBorder;
    private static final Border upperBorder;

    ToolbarConfiguration(String name, String displayName, List<ToolbarRow> rows) {
        this.configName = name;
        if (displayName.endsWith(".xml")) {
            displayName = displayName.substring(0, displayName.length() - ".xml".length());
        }
        this.configDisplayName = displayName;
        name2config.put(name, this);
        this.toolbarPanel = new JPanel(new GridLayout(0, 1)){

            @Override
            public boolean isOpaque() {
                if (null != UIManager.get("NbMainWindow.showCustomBackground")) {
                    return !UIManager.getBoolean("NbMainWindow.showCustomBackground");
                }
                return super.isOpaque();
            }
        };
        this.rows = new ArrayList<ToolbarRow>(rows);
    }

    private synchronized DnDSupport dndSupport() {
        if (this.dndSupport == null) {
            this.dndSupport = new DnDSupport(this);
        }
        return this.dndSupport;
    }

    public static final ToolbarConfiguration findConfiguration(String name) {
        return name2config.get(name);
    }

    private static final ToolbarPool getToolbarPool() {
        return ToolbarPool.getDefault();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void rebuildMenu() {
        Class<ToolbarConfiguration> class_ = ToolbarConfiguration.class;
        synchronized (ToolbarConfiguration.class) {
            if (toolbarMenu != null) {
                toolbarMenu.removeAll();
                ToolbarConfiguration.fillToolbarsMenu(toolbarMenu, false);
            }
            // ** MonitorExit[var0] (shouldn't be in output)
            return;
        }
    }

    private static void fillToolbarsMenu(JComponent menu, boolean isContextMenu) {
        ToolbarPool pool = ToolbarConfiguration.getToolbarPool();
        if (!pool.isFinished()) {
            JMenuItem mi = new JMenuItem();
            mi.setText(Bundle.MSG_ToolbarsInitializing());
            mi.setEnabled(false);
            menu.add(mi);
            return;
        }
        boolean fullScreen = MainWindow.getInstance().isFullScreenMode();
        ToolbarConfiguration conf = ToolbarConfiguration.findConfiguration(ToolbarPool.getDefault().getConfiguration());
        if (conf == null) {
            return;
        }
        Map<String, ToolbarConstraints> name2constr = conf.collectAllConstraints();
        Toolbar[] arr$ = pool.getToolbars();
        int len$ = arr$.length;
        for (int i$ = 0; i$ < len$; ++i$) {
            Toolbar tb;
            final Toolbar bar = tb = arr$[i$];
            final String tbName = tb.getName();
            ToolbarConstraints tc = name2constr.get(tbName);
            if (tc == null || tb == null) continue;
            JCheckBoxMenuItem mi = new JCheckBoxMenuItem(tb.getDisplayName(), tc.isVisible());
            mi.putClientProperty("ToolbarName", tbName);
            mi.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent ae) {
                    ToolbarConfiguration conf = ToolbarConfiguration.findConfiguration(ToolbarPool.getDefault().getConfiguration());
                    if (conf != null) {
                        ToolbarConstraints tc = conf.getConstraints(tbName);
                        conf.setToolbarVisible(bar, !tc.isVisible());
                    }
                }
            });
            mi.setEnabled(!fullScreen);
            menu.add(mi);
        }
        menu.add(new JPopupMenu.Separator());
        boolean smallToolbarIcons = ToolbarConfiguration.getToolbarPool().getPreferredIconSize() == 16;
        String stiName = NbBundle.getMessage(ToolbarConfiguration.class, (String)"CTL_SmallIcons");
        if (!stiName.isEmpty()) {
            JCheckBoxMenuItem cbmi = new JCheckBoxMenuItem(stiName, smallToolbarIcons);
            cbmi.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent ev) {
                    if (ev.getSource() instanceof JCheckBoxMenuItem) {
                        JCheckBoxMenuItem cb = (JCheckBoxMenuItem)ev.getSource();
                        boolean state = cb.getState();
                        if (state) {
                            ToolbarPool.getDefault().setPreferredIconSize(16);
                        } else {
                            ToolbarPool.getDefault().setPreferredIconSize(24);
                        }
                        String name = ToolbarPool.getDefault().getConfiguration();
                        ToolbarConfiguration tbConf = ToolbarConfiguration.findConfiguration(name);
                        if (tbConf != null) {
                            tbConf.refresh();
                        }
                    }
                }
            });
            cbmi.setEnabled(!fullScreen);
            menu.add(cbmi);
            menu.add(new JPopupMenu.Separator());
        }
        JMenuItem menuItem = new JMenuItem(new ResetToolbarsAction());
        menuItem.setEnabled(!fullScreen);
        menu.add(menuItem);
        menuItem = new JMenuItem(NbBundle.getMessage(ToolbarConfiguration.class, (String)"CTL_CustomizeToolbars"));
        menuItem.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent event) {
                ConfigureToolbarPanel.showConfigureDialog();
            }
        });
        menuItem.setEnabled(!fullScreen);
        menu.add(menuItem);
        for (Component c : menu instanceof JPopupMenu ? menu.getComponents() : ((JMenu)menu).getPopupMenu().getComponents()) {
            if (!(c instanceof AbstractButton)) continue;
            AbstractButton b = (AbstractButton)c;
            if (isContextMenu) {
                b.setText(Actions.cutAmpersand((String)b.getText()));
                continue;
            }
            Mnemonics.setLocalizedText((AbstractButton)b, (String)b.getText());
        }
    }

    void refresh() {
        this.toolbarPanel.removeAll();
        Toolbar[] tbs = ToolbarConfiguration.getToolbarPool().getToolbars();
        HashMap<String, Toolbar> bars = new HashMap<String, Toolbar>(tbs.length);
        boolean smallToolbarIcons = ToolbarConfiguration.getToolbarPool().getPreferredIconSize() == 16;
        for (int i = 0; i < tbs.length; ++i) {
            Toolbar tb = tbs[i];
            String name = tb.getName();
            ToolbarConstraints tc = this.getConstraints(name);
            Component[] comps = tb.getComponents();
            for (int j = 0; j < comps.length; ++j) {
                if (!(comps[j] instanceof JComponent)) continue;
                if (smallToolbarIcons) {
                    ((JComponent)comps[j]).putClientProperty("PreferredIconSize", null);
                    tb.putClientProperty((Object)"PreferredIconSize", (Object)null);
                    continue;
                }
                ((JComponent)comps[j]).putClientProperty("PreferredIconSize", 24);
                tb.putClientProperty((Object)"PreferredIconSize", (Object)24);
            }
            bars.put(name, tb);
        }
        this.removeEmptyRows();
        for (ToolbarRow row : this.rows) {
            row.removeAll();
            if (!row.isVisible()) continue;
            for (ToolbarConstraints tc : row.getConstraints()) {
                Toolbar tb;
                if (!tc.isVisible() || null == (tb = (Toolbar)bars.get(tc.getName()))) continue;
                ToolbarContainer container = new ToolbarContainer(tb, this.dndSupport(), tc.isDraggable());
                row.add(tc.getName(), container);
            }
            this.toolbarPanel.add(row);
        }
        this.adjustToolbarPanelBorder();
        ToolbarConfiguration.rebuildMenu();
        this.repaint();
    }

    ToolbarRow maybeAddEmptyRow(Point screenLocation) {
        if (this.rows.isEmpty()) {
            return null;
        }
        if (this.rows.size() > 0 && this.rows.get(this.rows.size() - 1).isEmpty()) {
            return null;
        }
        if (!this.toolbarPanel.isShowing()) {
            return null;
        }
        int rowHeight = this.rows.get(0).getHeight();
        int bottom = this.toolbarPanel.getLocationOnScreen().y + this.toolbarPanel.getHeight();
        if (screenLocation.y >= bottom && screenLocation.y <= bottom + rowHeight) {
            ToolbarRow row = new ToolbarRow();
            this.rows.add(row);
            this.toolbarPanel.add(row);
            this.repaint();
            return row;
        }
        return null;
    }

    boolean isLastRow(ToolbarRow row) {
        return this.rows.size() > 1 && this.rows.get(this.rows.size() - 1) == row;
    }

    void maybeRemoveLastRow() {
        ToolbarRow lastRow;
        if (this.rows.size() > 1 && (lastRow = this.rows.get(this.rows.size() - 1)).isEmpty()) {
            this.rows.remove(lastRow);
            this.toolbarPanel.remove(lastRow);
            this.repaint();
        }
    }

    void removeEmptyRows() {
        ArrayList<ToolbarRow> toRemove = new ArrayList<ToolbarRow>(this.rows.size());
        for (ToolbarRow r : this.rows) {
            if (!r.isEmpty()) continue;
            toRemove.add(r);
            this.toolbarPanel.remove(r);
        }
        this.rows.removeAll(toRemove);
        this.repaint();
    }

    void repaint() {
        this.toolbarPanel.invalidate();
        this.toolbarPanel.revalidate();
        this.toolbarPanel.repaint();
    }

    public Component activate() {
        this.refresh();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                ToolbarConfiguration.rebuildMenu();
            }
        });
        return this.toolbarPanel;
    }

    public String getName() {
        return this.configName;
    }

    public String getDisplayName() {
        return this.configDisplayName;
    }

    public JPopupMenu getContextMenu() {
        JPopupMenu menu = new JPopupMenu();
        ToolbarConfiguration.fillToolbarsMenu(menu, true);
        return menu;
    }

    public static JMenu getToolbarsMenu(JMenu menu) {
        ToolbarConfiguration.fillToolbarsMenu(menu, false);
        toolbarMenu = menu;
        return menu;
    }

    public void setToolbarVisible(Toolbar tb, boolean visible) {
        ToolbarConstraints tc = this.getConstraints(tb.getName());
        boolean isBarVisible = tc.isVisible();
        tc.setVisible(visible);
        if (visible != isBarVisible) {
            this.refresh();
            this.save();
        }
    }

    public boolean isToolbarVisible(Toolbar tb) {
        ToolbarConstraints tc = this.getConstraints(tb.getName());
        return tc.isVisible();
    }

    void setToolbarButtonDragAndDropAllowed(boolean buttonDndAllowed) {
        this.dndSupport().setButtonDragAndDropAllowed(buttonDndAllowed);
    }

    public boolean isToolbarConfigurationInProgress() {
        return this.dndSupport.isButtonDragAndDropAllowed();
    }

    private ToolbarConstraints getConstraints(String toolbarName) {
        ToolbarConstraints tc = this.collectAllConstraints().get(toolbarName);
        if (null == tc) {
            boolean isQuickSearch = "QuickSearch".equals(toolbarName);
            tc = new ToolbarConstraints(toolbarName, isQuickSearch ? ToolbarConstraints.Align.right : ToolbarConstraints.Align.left, true, true);
            ToolbarRow row = null;
            if (this.rows.isEmpty()) {
                row = new ToolbarRow();
                this.rows.add(row);
            } else {
                row = isQuickSearch ? this.rows.get(0) : this.rows.get(this.rows.size() - 1);
            }
            row.addConstraint(tc);
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    ToolbarConfiguration.this.refresh();
                }
            });
        }
        return tc;
    }

    ToolbarRow getToolbarRowAt(Point screenLocation) {
        Rectangle bounds = new Rectangle();
        for (ToolbarRow row : this.rows) {
            bounds = row.getBounds(bounds);
            if (!row.isShowing()) continue;
            bounds.setLocation(row.getLocationOnScreen());
            if (!bounds.contains(screenLocation)) continue;
            return row;
        }
        return null;
    }

    void save() {
        if (null == this.saver) {
            return;
        }
        try {
            this.createSnapshot();
            this.saver.requestSave();
        }
        catch (IOException ioE) {
            Logger.getLogger(ToolbarConfiguration.class.getName()).log(Level.INFO, "Error while saving toolbar configuration", ioE);
        }
    }

    void setSaverCallback(Saver s) {
        this.saver = s;
    }

    private void createSnapshot() {
        this.snapshot = new ArrayList<List<ToolbarConstraints>>(this.rows.size());
        for (ToolbarRow r : this.rows) {
            ArrayList<ToolbarConstraints> constraints = new ArrayList<ToolbarConstraints>(20);
            for (ToolbarConstraints tc : r.getConstraints()) {
                constraints.add(tc);
            }
            this.snapshot.add(constraints);
        }
    }

    List<? extends List<? extends ToolbarConstraints>> getSnapshot() {
        return this.snapshot;
    }

    private Map<String, ToolbarConstraints> collectAllConstraints() {
        HashMap<String, ToolbarConstraints> res = new HashMap<String, ToolbarConstraints>(20);
        for (ToolbarRow row : this.rows) {
            for (ToolbarConstraints tc : row.getConstraints()) {
                res.put(tc.getName(), tc);
            }
        }
        return res;
    }

    private static boolean isXPTheme() {
        if (isXP == null) {
            Boolean xp = (Boolean)Toolkit.getDefaultToolkit().getDesktopProperty("win.xpstyle.themeActive");
            isXP = Boolean.TRUE.equals(xp) ? Boolean.TRUE : Boolean.FALSE;
        }
        return isXP;
    }

    private static Color fetchColor(String key, Color fallback) {
        Color result = (Color)UIManager.get(key);
        if (result == null) {
            result = fallback;
        }
        return result;
    }

    private static boolean isWindows8() {
        String osName = System.getProperty("os.name");
        return osName.indexOf("Windows 8") >= 0 || osName.equals("Windows NT (unknown)") && "6.2".equals(System.getProperty("os.version"));
    }

    private void adjustToolbarPanelBorder() {
        if (this.toolbarPanel.getComponentCount() > 0) {
            Border b = UIManager.getBorder("Nb.MainWindow.Toolbar.Border");
            if (null != b) {
                this.toolbarPanel.setBorder(b);
                return;
            }
            if ("Windows".equals(UIManager.getLookAndFeel().getID())) {
                if (ToolbarConfiguration.isXPTheme()) {
                    if (ToolbarConfiguration.isWindows8()) {
                        this.toolbarPanel.setBorder(BorderFactory.createEmptyBorder());
                    } else {
                        this.toolbarPanel.setBorder(BorderFactory.createCompoundBorder(upperBorder, BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, ToolbarConfiguration.fetchColor("controlShadow", Color.DARK_GRAY)), BorderFactory.createMatteBorder(0, 0, 1, 0, mid))));
                    }
                } else {
                    this.toolbarPanel.setBorder(BorderFactory.createEtchedBorder());
                }
            } else if ("GTK".equals(UIManager.getLookAndFeel().getID())) {
                this.toolbarPanel.setBorder(BorderFactory.createEmptyBorder());
            }
        } else if ("GTK".equals(UIManager.getLookAndFeel().getID())) {
            this.toolbarPanel.setBorder(BorderFactory.createEmptyBorder());
        } else {
            this.toolbarPanel.setBorder(lowerBorder);
        }
    }

    static {
        isXP = null;
        Color lo = ToolbarConfiguration.fetchColor("controlShadow", Color.DARK_GRAY);
        Color hi = ToolbarConfiguration.fetchColor("control", Color.GRAY);
        int r = (lo.getRed() + hi.getRed()) / 2;
        int g = (lo.getGreen() + hi.getGreen()) / 2;
        int b = (lo.getBlue() + hi.getBlue()) / 2;
        mid = new Color(r, g, b);
        lowerBorder = BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, ToolbarConfiguration.fetchColor("controlShadow", Color.DARK_GRAY)), BorderFactory.createMatteBorder(0, 0, 1, 0, mid));
        upperBorder = BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, ToolbarConfiguration.fetchColor("controlShadow", Color.DARK_GRAY)), BorderFactory.createMatteBorder(1, 0, 0, 0, ToolbarConfiguration.fetchColor("controlLtHighlight", Color.WHITE)));
    }

}

