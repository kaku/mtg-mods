/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.StatusDisplayer
 */
package org.netbeans.core.windows.view.ui;

import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.awt.StatusDisplayer;

final class StatusLine
extends JLabel
implements ChangeListener,
Runnable {
    private StatusDisplayer d = StatusDisplayer.getDefault();

    @Override
    public void addNotify() {
        super.addNotify();
        this.run();
        this.d.addChangeListener((ChangeListener)this);
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.d.removeChangeListener((ChangeListener)this);
    }

    @Override
    public void updateUI() {
        super.updateUI();
        Font f = UIManager.getFont("controlFont");
        if (f == null) {
            f = UIManager.getFont("Tree.font");
        }
        if (f != null) {
            this.setFont(f);
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if (SwingUtilities.isEventDispatchThread()) {
            this.run();
        } else {
            SwingUtilities.invokeLater(this);
        }
    }

    @Override
    public void run() {
        String currentMsg = this.d.getStatusText();
        this.setText(currentMsg);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(100, super.getPreferredSize().height);
    }

    @Override
    public Dimension getMinimumSize() {
        return new Dimension(0, super.getMinimumSize().height);
    }
}

