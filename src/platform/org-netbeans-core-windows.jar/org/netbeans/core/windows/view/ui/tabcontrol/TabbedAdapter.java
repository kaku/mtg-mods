/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.ComponentConverter
 *  org.netbeans.swing.tabcontrol.TabDataModel
 *  org.netbeans.swing.tabcontrol.TabDisplayer
 *  org.netbeans.swing.tabcontrol.TabbedContainer
 *  org.netbeans.swing.tabcontrol.TabbedContainerUI
 *  org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer
 *  org.netbeans.swing.tabcontrol.customtabs.Tabbed
 *  org.netbeans.swing.tabcontrol.customtabs.Tabbed$Accessor
 *  org.netbeans.swing.tabcontrol.event.TabActionEvent
 *  org.netbeans.swing.tabcontrol.plaf.BusyTabsSupport
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view.ui.tabcontrol;

import java.awt.Component;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.SingleSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.view.ui.slides.SlideController;
import org.netbeans.core.windows.view.ui.tabcontrol.AbstractTabbedImpl;
import org.netbeans.swing.tabcontrol.ComponentConverter;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.TabbedContainer;
import org.netbeans.swing.tabcontrol.TabbedContainerUI;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbedContainer;
import org.netbeans.swing.tabcontrol.customtabs.Tabbed;
import org.netbeans.swing.tabcontrol.event.TabActionEvent;
import org.netbeans.swing.tabcontrol.plaf.BusyTabsSupport;
import org.openide.windows.TopComponent;

public class TabbedAdapter
extends TabbedContainer
implements Tabbed.Accessor,
SlideController {
    private final AbstractTabbedImpl tabbedImpl;

    public TabbedAdapter(int type, WinsysInfoForTabbedContainer winsysInfo) {
        super(null, type, winsysInfo);
        this.tabbedImpl = new AbstractTabbedImpl(){

            public Rectangle getTabBounds(int tabIndex) {
                return TabbedAdapter.this.getTabRect(tabIndex, new Rectangle());
            }

            public Rectangle getTabsArea() {
                return TabbedAdapter.this.getUI().getTabsArea();
            }

            public Component getComponent() {
                return TabbedAdapter.this;
            }

            public int getTabCount() {
                return TabbedAdapter.this.getTabCount();
            }

            public int indexOf(Component tc) {
                return TabbedAdapter.this.indexOf(tc);
            }

            public void setTitleAt(int index, String title) {
                TabbedAdapter.this.setTitleAt(index, title);
            }

            public void setIconAt(int index, Icon icon) {
                TabbedAdapter.this.setIconAt(index, icon);
            }

            public void setToolTipTextAt(int index, String toolTip) {
                TabbedAdapter.this.setToolTipTextAt(index, toolTip);
            }

            public void addActionListener(ActionListener al) {
                TabbedAdapter.this.addActionListener(al);
            }

            public void removeActionListener(ActionListener al) {
                TabbedAdapter.this.removeActionListener(al);
            }

            public void setActive(boolean active) {
                TabbedAdapter.this.setActive(active);
            }

            public int tabForCoordinate(Point p) {
                return TabbedAdapter.this.tabForCoordinate(p);
            }

            public Image createImageOfTab(int tabIndex) {
                return TabbedAdapter.this.createImageOfTab(tabIndex);
            }

            public boolean isTransparent() {
                return TabbedAdapter.this.isTransparent();
            }

            public void setTransparent(boolean transparent) {
                TabbedAdapter.this.setTransparent(transparent);
            }

            @Override
            protected TabDataModel getTabModel() {
                return TabbedAdapter.this.getModel();
            }

            @Override
            protected SingleSelectionModel getSelectionModel() {
                return TabbedAdapter.this.getSelectionModel();
            }

            @Override
            protected void requestAttention(int tabIndex) {
                TabbedAdapter.this.requestAttention(tabIndex);
            }

            @Override
            protected void cancelRequestAttention(int tabIndex) {
                TabbedAdapter.this.cancelRequestAttention(tabIndex);
            }

            @Override
            protected void setAttentionHighlight(int tabIndex, boolean highlight) {
                TabbedAdapter.this.setAttentionHighlight(tabIndex, highlight);
            }

            @Override
            protected int dropIndexOfPoint(Point location) {
                return TabbedAdapter.this.dropIndexOfPoint(location);
            }

            @Override
            protected ComponentConverter getComponentConverter() {
                return TabbedAdapter.this.getComponentConverter();
            }

            @Override
            protected Shape getDropIndication(TopComponent draggedTC, Point location) {
                return TabbedAdapter.this.getDropIndication((Object)draggedTC, location);
            }

            public void makeBusy(TopComponent tc, boolean busy) {
                int tabIndex = this.indexOf((Component)tc);
                BusyTabsSupport.getDefault().makeTabBusy((Tabbed)this, tabIndex, busy);
            }
        };
        this.getSelectionModel().addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent ce) {
                int idx = TabbedAdapter.this.getSelectionModel().getSelectedIndex();
                if (idx != -1) {
                    TabbedAdapter.this.tabbedImpl.fireStateChanged();
                }
            }
        });
    }

    public static boolean isInMaximizedMode(Component comp) {
        ModeImpl maxMode = WindowManagerImpl.getInstance().getCurrentMaximizedMode();
        if (maxMode == null) {
            return false;
        }
        return maxMode.containsTopComponent((TopComponent)comp);
    }

    @Override
    public void userToggledAutoHide(int tabIndex, boolean enabled) {
        this.postActionEvent(new TabActionEvent((Object)this, "enableAutoHide", tabIndex));
    }

    @Override
    public void userToggledTransparency(int tabIndex) {
        this.postActionEvent(new TabActionEvent((Object)this, "toggleTransparency", tabIndex));
    }

    public Tabbed getTabbed() {
        return this.tabbedImpl;
    }

    public void addNotify() {
        super.addNotify();
        BusyTabsSupport.getDefault().install(this.getTabbed(), this.getModel());
    }

    public void removeNotify() {
        super.removeNotify();
        BusyTabsSupport.getDefault().uninstall(this.getTabbed(), this.getModel());
    }

    public static class WinsysInfo
    extends WinsysInfoForTabbedContainer {
        private int containerType;

        public WinsysInfo(int containerType) {
            this.containerType = containerType;
        }

        public Object getOrientation(Component comp) {
            WindowManagerImpl wmi = WindowManagerImpl.getInstance();
            if (!wmi.isDocked((TopComponent)comp)) {
                return TabDisplayer.ORIENTATION_INVISIBLE;
            }
            String side = wmi.guessSlideSide((TopComponent)comp);
            Object result = null;
            result = side.equals("left") ? TabDisplayer.ORIENTATION_WEST : (side.equals("right") ? TabDisplayer.ORIENTATION_EAST : (side.equals("bottom") ? TabDisplayer.ORIENTATION_SOUTH : (side.equals("top") ? TabDisplayer.ORIENTATION_NORTH : TabDisplayer.ORIENTATION_CENTER)));
            return result;
        }

        public boolean inMaximizedMode(Component comp) {
            return TabbedAdapter.isInMaximizedMode(comp);
        }

        public boolean isTopComponentSlidingEnabled() {
            return Switches.isTopComponentSlidingEnabled();
        }

        public boolean isTopComponentClosingEnabled() {
            if (this.containerType == 1) {
                return Switches.isEditorTopComponentClosingEnabled();
            }
            return Switches.isViewTopComponentClosingEnabled();
        }

        public boolean isTopComponentMaximizationEnabled() {
            return Switches.isTopComponentMaximizationEnabled();
        }

        public boolean isTopComponentClosingEnabled(TopComponent tc) {
            return !Boolean.TRUE.equals(tc.getClientProperty((Object)"netbeans.winsys.tc.closing_disabled")) && this.isTopComponentClosingEnabled();
        }

        public boolean isTopComponentMaximizationEnabled(TopComponent tc) {
            return !Boolean.TRUE.equals(tc.getClientProperty((Object)"netbeans.winsys.tc.maximization_disabled")) && this.isTopComponentMaximizationEnabled();
        }

        public boolean isTopComponentSlidingEnabled(TopComponent tc) {
            return !Boolean.TRUE.equals(tc.getClientProperty((Object)"netbeans.winsys.tc.sliding_disabled")) && this.isTopComponentSlidingEnabled();
        }

        public boolean isModeSlidingEnabled() {
            return Switches.isModeSlidingEnabled();
        }

        public boolean isTopComponentBusy(TopComponent tc) {
            return WindowManagerImpl.getInstance().isTopComponentBusy(tc);
        }
    }

}

