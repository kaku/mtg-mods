/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view.dnd;

import java.awt.Component;
import java.awt.Container;
import java.awt.Rectangle;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.Constants;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.view.ui.ModeComponent;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public final class TopComponentDraggable {
    private final TopComponent tc;
    private final ModeImpl mode;

    public TopComponentDraggable(TopComponent tc) {
        this(tc, (ModeImpl)WindowManagerImpl.getInstance().findMode(tc));
    }

    TopComponentDraggable(TopComponent tc, ModeImpl mode) {
        assert (null != tc);
        assert (null != mode);
        this.tc = tc;
        this.mode = mode;
    }

    public TopComponentDraggable(ModeImpl mode) {
        this.tc = null;
        assert (null != mode);
        this.mode = mode;
    }

    public int getKind() {
        return this.mode.getKind();
    }

    public ModeImpl getMode() {
        return this.mode;
    }

    public boolean isAllowedToMoveAnywhere() {
        boolean res;
        boolean bl = res = Constants.SWITCH_MODE_ADD_NO_RESTRICT || Switches.isMixingOfEditorsAndViewsEnabled();
        if (this.isTopComponentTransfer()) {
            res |= WindowManagerImpl.getInstance().isTopComponentAllowedToMoveAnywhere(this.tc);
        }
        return res;
    }

    boolean isUndockingEnabled() {
        if (this.isModeTransfer()) {
            if (this.getKind() == 1) {
                return Switches.isEditorModeUndockingEnabled();
            }
            return Switches.isViewModeUndockingEnabled();
        }
        return Switches.isTopComponentUndockingEnabled() && Switches.isUndockingEnabled(this.tc);
    }

    boolean isSlidingEnabled() {
        if (this.isModeTransfer()) {
            return Switches.isModeSlidingEnabled();
        }
        return Switches.isTopComponentSlidingEnabled() && Switches.isSlidingEnabled(this.tc);
    }

    public boolean isModeTransfer() {
        return null == this.tc;
    }

    public TopComponent getTopComponent() {
        return this.tc;
    }

    public boolean isTopComponentTransfer() {
        return null != this.tc;
    }

    Rectangle getBounds() {
        TopComponent[] tcs;
        Container modeComp;
        Rectangle bounds = null;
        TopComponent modeTC = this.getTopComponent();
        if (null == modeTC && null == (modeTC = this.mode.getSelectedTopComponent()) && null != (tcs = this.mode.getTopComponents()) && tcs.length > 0) {
            modeTC = tcs[0];
        }
        if ((modeComp = SwingUtilities.getAncestorOfClass(ModeComponent.class, (Component)modeTC)) != null) {
            bounds = modeComp.getBounds();
        }
        return bounds;
    }
}

