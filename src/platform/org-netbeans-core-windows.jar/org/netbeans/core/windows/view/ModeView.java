/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.swing.JComponent;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.view.Controller;
import org.netbeans.core.windows.view.ModeContainer;
import org.netbeans.core.windows.view.ViewElement;
import org.netbeans.core.windows.view.dnd.WindowDnDManager;
import org.netbeans.core.windows.view.ui.DefaultSeparateContainer;
import org.netbeans.core.windows.view.ui.DefaultSplitContainer;
import org.openide.windows.TopComponent;

public class ModeView
extends ViewElement {
    protected ModeContainer container;
    private int frameState;
    private long timeStamp = 0;
    private long mainWindowStamp = 0;

    public ModeView(Controller controller, WindowDnDManager windowDnDManager, double resizeWeight, int kind, TopComponent[] topComponents, TopComponent selectedTopComponent) {
        super(controller, resizeWeight);
        this.container = new DefaultSplitContainer(this, windowDnDManager, kind);
        this.setTopComponents(topComponents, selectedTopComponent);
    }

    public ModeView(Controller controller, WindowDnDManager windowDnDManager, Rectangle bounds, int kind, int frameState, TopComponent[] topComponents, TopComponent selectedTopComponent) {
        super(controller, 0.0);
        this.frameState = frameState;
        this.container = new DefaultSeparateContainer(this, windowDnDManager, bounds, kind);
        this.setTopComponents(topComponents, selectedTopComponent);
    }

    protected ModeView(Controller controller) {
        super(controller, 0.0);
    }

    public void setFrameState(int frameState) {
        this.frameState = frameState;
        Component comp = this.container.getComponent();
        if (comp instanceof Frame) {
            this.timeStamp = (frameState & 1) == 1 ? System.currentTimeMillis() : 0;
        }
    }

    public void removeTopComponent(TopComponent tc) {
        if (!this.getTopComponents().contains((Object)tc)) {
            return;
        }
        this.container.removeTopComponent(tc);
    }

    public void setTopComponents(TopComponent[] tcs, TopComponent select) {
        this.container.setTopComponents(tcs, select);
    }

    public TopComponent getSelectedTopComponent() {
        return this.container.getSelectedTopComponent();
    }

    public void setActive(boolean active) {
        this.container.setActive(active);
    }

    public boolean isActive() {
        return this.container.isActive();
    }

    public List<TopComponent> getTopComponents() {
        return new ArrayList<TopComponent>(Arrays.asList(this.container.getTopComponents()));
    }

    public void focusSelectedTopComponent() {
        this.container.focusSelectedTopComponent();
    }

    @Override
    public Component getComponent() {
        return this.container.getComponent();
    }

    public void updateName(TopComponent tc) {
        this.container.updateName(tc);
    }

    public void updateToolTip(TopComponent tc) {
        this.container.updateToolTip(tc);
    }

    public void updateIcon(TopComponent tc) {
        this.container.updateIcon(tc);
    }

    public void requestAttention(TopComponent tc) {
        this.container.requestAttention(tc);
    }

    public void cancelRequestAttention(TopComponent tc) {
        this.container.cancelRequestAttention(tc);
    }

    public void setAttentionHighlight(TopComponent tc, boolean highlight) {
        this.container.setAttentionHighlight(tc, highlight);
    }

    public void makeBusy(TopComponent tc, boolean busy) {
        this.container.makeBusy(tc, busy);
    }

    public void updateFrameState() {
        Component comp = this.container.getComponent();
        if (comp instanceof Frame) {
            ((Frame)comp).setExtendedState(this.frameState);
        }
    }

    public void setUserStamp(long stamp) {
        this.timeStamp = stamp;
    }

    public long getUserStamp() {
        return this.timeStamp;
    }

    public void setMainWindowStamp(long stamp) {
        this.mainWindowStamp = stamp;
    }

    public long getMainWindowStamp() {
        return this.mainWindowStamp;
    }

    public String toString() {
        TopComponent selected;
        return Object.super.toString() + " [selected=" + ((selected = this.container.getSelectedTopComponent()) == null ? null : WindowManagerImpl.getInstance().getTopComponentDisplayName(selected)) + "]";
    }

    @Override
    public boolean updateAWTHierarchy(Dimension availableSpace) {
        Component comp = this.container.getComponent();
        boolean result = false;
        if (comp instanceof JComponent) {
            Dimension d = (Dimension)((JComponent)comp).getClientProperty("lastAvailableSpace");
            Dimension currDim = comp.getPreferredSize();
            if (!availableSpace.equals(d) || !availableSpace.equals(currDim)) {
                ((JComponent)comp).setPreferredSize(availableSpace);
                ((JComponent)comp).putClientProperty("lastAvailableSpace", availableSpace);
                result = true;
            }
        }
        return result;
    }
}

