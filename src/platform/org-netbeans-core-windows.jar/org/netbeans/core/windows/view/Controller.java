/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view;

import java.awt.Rectangle;
import org.netbeans.core.windows.view.ModeView;
import org.netbeans.core.windows.view.SplitView;
import org.netbeans.core.windows.view.ViewElement;
import org.netbeans.core.windows.view.dnd.TopComponentDraggable;
import org.netbeans.core.windows.view.ui.slides.SlideOperation;
import org.openide.windows.TopComponent;

public interface Controller {
    public void userActivatedModeView(ModeView var1);

    public void userActivatedModeWindow(ModeView var1);

    public void userActivatedEditorWindow();

    public void userSelectedTab(ModeView var1, TopComponent var2);

    public void userClosingMode(ModeView var1);

    public void userResizedMainWindow(Rectangle var1);

    public void userMovedMainWindow(Rectangle var1);

    public void userResizedEditorArea(Rectangle var1);

    public void userChangedFrameStateMainWindow(int var1);

    public void userChangedFrameStateEditorArea(int var1);

    public void userChangedFrameStateMode(ModeView var1, int var2);

    public void userResizedModeBounds(ModeView var1, Rectangle var2);

    public void userMovedSplit(SplitView var1, ViewElement[] var2, double[] var3);

    public void userClosedTopComponent(ModeView var1, TopComponent var2);

    public void userDroppedTopComponents(ModeView var1, TopComponentDraggable var2);

    public void userDroppedTopComponents(ModeView var1, TopComponentDraggable var2, int var3);

    public void userDroppedTopComponents(ModeView var1, TopComponentDraggable var2, String var3);

    public void userDroppedTopComponentsIntoEmptyEditor(TopComponentDraggable var1);

    public void userDroppedTopComponentsAround(TopComponentDraggable var1, String var2);

    public void userDroppedTopComponentsAroundEditor(TopComponentDraggable var1, String var2);

    public void userDroppedTopComponentsIntoFreeArea(TopComponentDraggable var1, Rectangle var2);

    public void userStartedKeyboardDragAndDrop(TopComponentDraggable var1);

    public void userEnabledAutoHide(ModeView var1, TopComponent var2);

    public void userDisabledAutoHide(ModeView var1, TopComponent var2);

    public void userTriggeredSlideIn(ModeView var1, SlideOperation var2);

    public void userTriggeredSlideOut(ModeView var1, SlideOperation var2);

    public void userTriggeredSlideIntoEdge(ModeView var1, SlideOperation var2);

    public void userTriggeredSlideIntoDesktop(ModeView var1, SlideOperation var2);

    public void userResizedSlidingWindow(ModeView var1, SlideOperation var2);
}

