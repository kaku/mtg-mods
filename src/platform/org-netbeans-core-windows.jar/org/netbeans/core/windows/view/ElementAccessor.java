/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view;

import org.netbeans.core.windows.ModeStructureSnapshot;
import org.netbeans.core.windows.model.ModelElement;

interface ElementAccessor {
    public ModelElement getOriginator();

    public ModeStructureSnapshot.ElementSnapshot getSnapshot();

    public boolean originatorEquals(ElementAccessor var1);
}

