/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view;

import java.awt.Rectangle;
import org.netbeans.core.windows.view.ModeAccessor;
import org.netbeans.core.windows.view.ModeStructureAccessor;
import org.netbeans.core.windows.view.ModeStructureAccessorImpl;
import org.netbeans.core.windows.view.WindowSystemAccessor;

final class WindowSystemAccessorImpl
implements WindowSystemAccessor {
    private Rectangle mainWindowBoundsJoined;
    private Rectangle mainWindowBoundsSeparated;
    private int mainWindowFrameStateJoined;
    private int mainWindowFrameStateSeparated;
    private String toolbarConfigurationName;
    private int editorAreaState;
    private int editorAreaFrameState;
    private Rectangle editorAreaBounds;
    private ModeAccessor activeMode;
    private ModeAccessor maximizedMode;
    private ModeStructureAccessor modeStructureAccessor;

    public void setMainWindowBoundsJoined(Rectangle mainWindowBoundsJoined) {
        this.mainWindowBoundsJoined = mainWindowBoundsJoined;
    }

    @Override
    public Rectangle getMainWindowBoundsJoined() {
        return this.mainWindowBoundsJoined;
    }

    public void setMainWindowBoundsSeparated(Rectangle mainWindowBoundsSeparated) {
        this.mainWindowBoundsSeparated = mainWindowBoundsSeparated;
    }

    @Override
    public Rectangle getMainWindowBoundsSeparated() {
        return this.mainWindowBoundsSeparated;
    }

    public void setMainWindowFrameStateJoined(int mainWindowFrameStateJoined) {
        this.mainWindowFrameStateJoined = mainWindowFrameStateJoined;
    }

    @Override
    public int getMainWindowFrameStateJoined() {
        return this.mainWindowFrameStateJoined;
    }

    public void setMainWindowFrameStateSeparated(int mainWindowFrameStateSeparated) {
        this.mainWindowFrameStateSeparated = mainWindowFrameStateSeparated;
    }

    @Override
    public int getMainWindowFrameStateSeparated() {
        return this.mainWindowFrameStateSeparated;
    }

    public void setEditorAreaBounds(Rectangle editorAreaBounds) {
        this.editorAreaBounds = editorAreaBounds;
    }

    @Override
    public Rectangle getEditorAreaBounds() {
        return this.editorAreaBounds;
    }

    public void setEditorAreaState(int editorAreaState) {
        this.editorAreaState = editorAreaState;
    }

    @Override
    public int getEditorAreaState() {
        return this.editorAreaState;
    }

    public void setEditorAreaFrameState(int editorAreaFrameState) {
        this.editorAreaFrameState = editorAreaFrameState;
    }

    @Override
    public int getEditorAreaFrameState() {
        return this.editorAreaFrameState;
    }

    public void setActiveModeAccessor(ModeAccessor activeMode) {
        this.activeMode = activeMode;
    }

    @Override
    public ModeAccessor getActiveModeAccessor() {
        return this.activeMode;
    }

    public void setMaximizedModeAccessor(ModeAccessor maximizedMode) {
        this.maximizedMode = maximizedMode;
    }

    @Override
    public ModeAccessor getMaximizedModeAccessor() {
        return this.maximizedMode;
    }

    public void setToolbarConfigurationName(String toolbarConfigurationName) {
        this.toolbarConfigurationName = toolbarConfigurationName;
    }

    @Override
    public String getToolbarConfigurationName() {
        return this.toolbarConfigurationName;
    }

    public void setModeStructureAccessor(ModeStructureAccessor modeStructureAccessor) {
        this.modeStructureAccessor = modeStructureAccessor;
    }

    @Override
    public ModeStructureAccessor getModeStructureAccessor() {
        return this.modeStructureAccessor;
    }

    @Override
    public ModeAccessor findModeAccessor(String modeName) {
        if (modeName == null) {
            return null;
        }
        if (this.modeStructureAccessor != null) {
            return ((ModeStructureAccessorImpl)this.modeStructureAccessor).findModeAccessor(modeName);
        }
        return null;
    }

    public String toString() {
        return super.toString() + "[modeStructure=" + this.modeStructureAccessor + ",\nactiveMode=" + this.activeMode + ",\nmaximizedMode=" + this.maximizedMode + "]";
    }
}

