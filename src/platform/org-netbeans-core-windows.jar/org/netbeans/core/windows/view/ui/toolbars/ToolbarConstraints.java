/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view.ui.toolbars;

class ToolbarConstraints {
    private final String name;
    private Align align;
    private boolean visible;
    private final boolean draggable;

    public ToolbarConstraints(String name, Align align, boolean visible, boolean draggable) {
        this.name = name;
        this.draggable = draggable;
        this.align = align;
        this.visible = visible;
    }

    public Align getAlign() {
        return this.align;
    }

    public void setAlign(Align align) {
        this.align = align;
    }

    public boolean isDraggable() {
        return this.draggable;
    }

    public boolean isVisible() {
        return this.visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getName() {
        return this.name;
    }

    static enum Align {
        left,
        right;
        

        private Align() {
        }

        static Align fromString(String s) {
            if ("right".equals(s)) {
                return right;
            }
            return left;
        }
    }

}

