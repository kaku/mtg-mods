/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view.ui.popupswitcher;

import java.util.ArrayList;
import javax.swing.Icon;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.view.ui.popupswitcher.Item;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

class Model
extends AbstractTableModel {
    private final Item[] documents;
    private final Item[] views;
    private int rowCount;
    private int colCount;
    private int documentCol = -1;
    private int viewCol = 0;
    private int initialColumn;
    private final boolean hasIcons;
    private int selCol = -1;
    private int selRow = -1;
    private Item selectedTopItem = null;
    private int extraRows = 0;

    Model(boolean documentsOnly) {
        this(Model.createItems(true), documentsOnly ? new Item[]{} : Model.createItems(false), Model.isEditorTCActive());
    }

    Model(Item[] documents, Item[] views, boolean startInDocumentColumn) {
        Icon icon;
        int i;
        boolean documentsHaveSubTabs = false;
        boolean viewsHaveSubTabs = false;
        boolean icons = false;
        this.documents = documents;
        for (i = 0; i < documents.length; ++i) {
            icon = documents[i].getIcon();
            icons |= null != icon && icon.getIconWidth() > 0;
            documentsHaveSubTabs |= documents[i].hasSubItems();
        }
        this.views = views;
        for (i = 0; i < views.length; ++i) {
            icon = views[i].getIcon();
            icons |= null != icon && icon.getIconWidth() > 0;
            viewsHaveSubTabs |= views[i].hasSubItems();
        }
        this.hasIcons = icons;
        this.rowCount = Math.max(views.length, documents.length);
        int columns = 0;
        if (documents.length > 0) {
            ++columns;
            this.documentCol = 0;
            ++this.viewCol;
            if (documentsHaveSubTabs) {
                ++columns;
                ++this.viewCol;
            }
        }
        if (views.length > 0) {
            ++columns;
            if (viewsHaveSubTabs) {
                ++columns;
            }
        } else {
            this.viewCol = -1;
        }
        this.colCount = columns;
        this.initialColumn = startInDocumentColumn ? this.documentCol : this.viewCol;
    }

    @Override
    public int getRowCount() {
        return this.rowCount + this.extraRows;
    }

    @Override
    public int getColumnCount() {
        return this.colCount;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (this.selCol >= 0 && columnIndex == this.selCol + 1) {
            if (rowIndex < this.selRow) {
                return null;
            }
            Item[] subItems = this.selectedTopItem.getActivatableSubItems();
            if (null == subItems || (rowIndex -= this.selRow) >= subItems.length) {
                return null;
            }
            return this.selectedTopItem.getActivatableSubItems()[rowIndex];
        }
        Item[] items = null;
        if (columnIndex == this.documentCol) {
            items = this.documents;
        } else if (columnIndex == this.viewCol) {
            items = this.views;
        }
        if (null == items || rowIndex >= items.length || rowIndex < 0) {
            return null;
        }
        return items[rowIndex];
    }

    int getInitialColumn() {
        return this.initialColumn;
    }

    int getRowCount(int col) {
        if (col != this.viewCol && col != this.documentCol) {
            throw new IllegalArgumentException();
        }
        if (col == this.viewCol) {
            return this.views.length;
        }
        return this.documents.length;
    }

    boolean hasIcons() {
        return this.hasIcons;
    }

    void setCurrentSelection(int rowIndex, int columnIndex) {
        if (rowIndex < 0 || columnIndex < 0) {
            this.showSubTabs(-1, -1);
        }
        if (columnIndex != this.viewCol && columnIndex != this.documentCol) {
            return;
        }
        this.showSubTabs(rowIndex, columnIndex);
    }

    private void showSubTabs(int row, int col) {
        this.selCol = col;
        this.selRow = row;
        int newRowCount = this.rowCount;
        this.selectedTopItem = null;
        if (this.selCol >= 0) {
            Item item = this.selectedTopItem = this.selCol == this.documentCol ? this.documents[this.selRow] : this.views[this.selRow];
            if (this.selectedTopItem.hasSubItems()) {
                newRowCount = Math.max(this.rowCount, this.selectedTopItem.getActivatableSubItems().length + this.selRow);
            } else {
                this.selCol = -1;
                this.selRow = -1;
                this.selectedTopItem = null;
            }
        }
        if (this.documentCol >= 0) {
            this.fireTableChanged(new TableModelEvent(this, 0, this.getRowCount(), this.documentCol + 1));
        }
        if (this.viewCol >= 0) {
            this.fireTableChanged(new TableModelEvent(this, 0, this.getRowCount(), this.viewCol + 1));
        }
        int rowDelta = newRowCount - this.getRowCount();
        this.extraRows = newRowCount - this.rowCount;
        if (rowDelta < 0) {
            this.fireTableRowsDeleted(this.rowCount, this.rowCount - rowDelta);
        } else if (rowDelta > 0) {
            this.fireTableRowsInserted(this.rowCount, this.rowCount + rowDelta);
        }
    }

    boolean isTopItemColumn(int col) {
        return col >= 0 && (col == this.viewCol || col == this.documentCol);
    }

    int getMaxRowCount() {
        int i;
        int maxRows = this.rowCount;
        for (i = 0; i < this.documents.length; ++i) {
            if (!this.documents[i].hasSubItems()) continue;
            maxRows = Math.max(maxRows, i + this.documents[i].getActivatableSubItems().length);
        }
        for (i = 0; i < this.views.length; ++i) {
            if (!this.views[i].hasSubItems()) continue;
            maxRows = Math.max(maxRows, i + this.views[i].getActivatableSubItems().length);
        }
        return maxRows;
    }

    private static Item[] createItems(boolean documentsOnly) {
        WindowManagerImpl wmi = WindowManagerImpl.getInstance();
        TopComponent[] windows = wmi.getRecentViewList();
        ArrayList<Item> items = new ArrayList<Item>(windows.length);
        for (TopComponent tc : windows) {
            boolean isEditor;
            if (tc == null) continue;
            ModeImpl mode = (ModeImpl)wmi.findMode(tc);
            boolean bl = isEditor = null != mode && mode.getKind() == 1;
            if (documentsOnly != isEditor) continue;
            items.add(Item.create(tc));
        }
        return items.toArray(new Item[items.size()]);
    }

    private static boolean isEditorTCActive() {
        ModeImpl mode;
        boolean res = true;
        TopComponent tc = TopComponent.getRegistry().getActivated();
        if (null != tc && null != (mode = (ModeImpl)WindowManagerImpl.getInstance().findMode(tc))) {
            res = mode.getKind() == 1;
        }
        return res;
    }
}

