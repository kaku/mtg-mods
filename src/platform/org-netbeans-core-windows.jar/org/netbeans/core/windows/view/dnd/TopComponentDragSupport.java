/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.customtabs.Tabbed
 *  org.netbeans.swing.tabcontrol.customtabs.Tabbed$Accessor
 *  org.openide.util.ImageUtilities
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Cloneable
 */
package org.netbeans.core.windows.view.dnd;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragGestureRecognizer;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceContext;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.dnd.DragSourceMotionListener;
import java.awt.dnd.InvalidDnDOperationException;
import java.awt.event.AWTEventListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.JButton;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.TreePath;
import org.netbeans.core.windows.Debug;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.options.WinSysPrefs;
import org.netbeans.core.windows.view.Controller;
import org.netbeans.core.windows.view.dnd.DragAndDropFeedbackVisualizer;
import org.netbeans.core.windows.view.dnd.TopComponentDraggable;
import org.netbeans.core.windows.view.dnd.TopComponentDroppable;
import org.netbeans.core.windows.view.dnd.WindowDnDManager;
import org.netbeans.swing.tabcontrol.customtabs.Tabbed;
import org.openide.util.ImageUtilities;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

final class TopComponentDragSupport
implements AWTEventListener,
DragSourceListener,
DragSourceMotionListener {
    public static final String MIME_TOP_COMPONENT = "application/x-java-jvm-local-objectref; class=org.openide.windows.TopComponent";
    public static final String MIME_TOP_COMPONENT_CLONEABLE = "application/x-java-jvm-local-objectref; class=org.openide.windows.TopComponent$Cloneable";
    public static final String MIME_TOP_COMPONENT_MODE = "application/x-java-jvm-local-objectref; class=org.netbeans.core.windows.ModeImpl";
    private static final int CURSOR_COPY = 0;
    private static final int CURSOR_COPY_NO = 1;
    private static final int CURSOR_MOVE = 2;
    private static final int CURSOR_MOVE_NO = 3;
    private static final int CURSOR_COPY_NO_MOVE = 4;
    private static final int CURSOR_MOVE_FREE = 5;
    private static final String NAME_CURSOR_COPY = "CursorTopComponentCopy";
    private static final String NAME_CURSOR_COPY_NO = "CursorTopComponentCopyNo";
    private static final String NAME_CURSOR_MOVE = "CursorTopComponentMove";
    private static final String NAME_CURSOR_MOVE_NO = "CursorTopComponentMoveNo";
    private static final String NAME_CURSOR_COPY_NO_MOVE = "CursorTopComponentCopyNoMove";
    private static final String NAME_CURSOR_MOVE_FREE = "CursorTopComponentMoveFree";
    private static final boolean DEBUG = Debug.isLoggable(TopComponentDragSupport.class);
    private final WindowDnDManager windowDnDManager;
    private Reference<DragSourceContext> dragContextWRef = new WeakReference<Object>(null);
    private boolean canCopy;
    private int hackUserDropAction;
    private boolean hackESC;
    private Point startingPoint;
    private Component startingComponent;
    private long startingTime;
    private DragAndDropFeedbackVisualizer visualizer;
    private boolean dropFailed = false;
    private AWTEventListener keyListener;

    TopComponentDragSupport(WindowDnDManager windowDnDManager) {
        this.keyListener = new AWTEventListener(){

            @Override
            public void eventDispatched(AWTEvent event) {
                KeyEvent keyevent = (KeyEvent)event;
                if ((keyevent.getID() == 401 || keyevent.getID() == 402) && keyevent.getKeyCode() == 27) {
                    TopComponentDragSupport.this.hackESC = true;
                }
            }
        };
        this.windowDnDManager = windowDnDManager;
    }

    public boolean isCopyOperationPossible() {
        return this.canCopy;
    }

    @Override
    public void eventDispatched(AWTEvent evt) {
        ModeImpl mode;
        Point startPoint;
        Tabbed tabbed;
        MouseEvent me = (MouseEvent)evt;
        if (!(evt.getSource() instanceof Component)) {
            return;
        }
        if (me.getID() == 501 && SwingUtilities.isLeftMouseButton(me)) {
            this.startingPoint = me.getPoint();
            this.startingComponent = me.getComponent();
            this.startingTime = me.getWhen();
        } else if (me.getID() == 502) {
            this.startingPoint = null;
            this.startingComponent = null;
        }
        if (me.isConsumed()) {
            return;
        }
        if (evt.getID() != 506) {
            return;
        }
        if (this.windowDnDManager.isDragging()) {
            return;
        }
        if (this.startingPoint == null) {
            return;
        }
        if (evt.getSource() instanceof JButton) {
            return;
        }
        if (!WindowDnDManager.isDnDEnabled()) {
            return;
        }
        Component srcComp = this.startingComponent;
        if (srcComp == null) {
            return;
        }
        Point point = new Point(this.startingPoint);
        Point currentPoint = me.getPoint();
        Component currentComponent = me.getComponent();
        if (currentComponent == null) {
            return;
        }
        currentPoint = SwingUtilities.convertPoint(currentComponent, currentPoint, srcComp);
        if (Math.abs(currentPoint.x - point.x) <= 10 && Math.abs(currentPoint.y - point.y) <= 10) {
            return;
        }
        if (me.getWhen() - this.startingTime <= 200) {
            return;
        }
        this.startingPoint = null;
        this.startingComponent = null;
        if (DEBUG) {
            TopComponentDragSupport.debugLog("");
            TopComponentDragSupport.debugLog("eventDispatched (MOUSE_DRAGGED)");
        }
        if (srcComp instanceof JTree && ((JTree)srcComp).getPathForLocation(me.getX(), me.getY()) != null) {
            return;
        }
        srcComp = SwingUtilities.getDeepestComponentAt(srcComp, point.x, point.y);
        boolean ctrlDown = me.isControlDown();
        Object tc = null;
        if (srcComp instanceof Tabbed.Accessor) {
            tabbed = ((Tabbed.Accessor)srcComp).getTabbed();
        } else {
            Tabbed.Accessor acc = (Tabbed.Accessor)SwingUtilities.getAncestorOfClass(Tabbed.Accessor.class, srcComp);
            Tabbed tabbed2 = tabbed = acc != null ? acc.getTabbed() : null;
        }
        if (tabbed == null) {
            return;
        }
        Dialog dlg = (Dialog)SwingUtilities.getAncestorOfClass(Dialog.class, tabbed.getComponent());
        if (dlg != null && dlg.isModal()) {
            return;
        }
        Point ppp = new Point(point);
        Point p = SwingUtilities.convertPoint(srcComp, ppp, tabbed.getComponent());
        TopComponentDraggable draggable = null;
        int tabIndex = tabbed.tabForCoordinate(p);
        Object object = tc = tabIndex != -1 ? tabbed.getTopComponentAt(tabIndex) : null;
        if (tc == null) {
            ModeImpl mode2;
            TopComponent[] tcs;
            Rectangle tabsArea = tabbed.getTabsArea();
            if (tabsArea.contains(p) && null != (tcs = tabbed.getTopComponents()) && tcs.length > 0 && null != (mode2 = (ModeImpl)WindowManagerImpl.getInstance().findMode(tcs[0])) && (mode2.getKind() == 1 && Switches.isEditorModeDragAndDropEnabled() || mode2.getKind() == 0 && Switches.isViewModeDragAndDropEnabled())) {
                draggable = new TopComponentDraggable(mode2);
            }
        } else if (Switches.isTopComponentDragAndDropEnabled() && Switches.isDraggingEnabled((TopComponent)tc) && null != (mode = (ModeImpl)WindowManagerImpl.getInstance().findMode((TopComponent)tc))) {
            draggable = new TopComponentDraggable((TopComponent)tc);
        }
        if (null == draggable) {
            return;
        }
        this.hackUserDropAction = ctrlDown ? 1 : 2;
        ArrayList<MouseEvent> list = new ArrayList<MouseEvent>();
        list.add(me);
        TopComponentDroppable startDroppable = (TopComponentDroppable)((Object)SwingUtilities.getAncestorOfClass(TopComponentDroppable.class, null == tc ? tabbed.getComponent() : tc));
        if (startDroppable == null && tc != null) {
            startDroppable = (TopComponentDroppable)((Object)SwingUtilities.getAncestorOfClass(TopComponentDroppable.class, tabbed.getComponent()));
        }
        if (startDroppable != null) {
            startPoint = point;
            Point pp = new Point(point);
            startPoint = SwingUtilities.convertPoint(srcComp, pp, (Component)((Object)startDroppable));
        } else {
            startPoint = null;
        }
        this.doStartDrag(srcComp, draggable, new DragGestureEvent(new FakeDragGestureRecognizer(this.windowDnDManager, me), this.hackUserDropAction, point, list), startDroppable, startPoint);
    }

    private void doStartDrag(Component startingComp, TopComponentDraggable transfer, DragGestureEvent evt, TopComponentDroppable startingDroppable, Point startingPoint) {
        block8 : {
            TopComponent tc;
            if (DEBUG) {
                TopComponentDragSupport.debugLog("");
                TopComponentDragSupport.debugLog("doStartDrag");
            }
            this.canCopy = (tc = transfer.getTopComponent()) instanceof TopComponent.Cloneable && !Boolean.TRUE.equals(tc.getClientProperty((Object)"netbeans.winsys.tc.draganddrop_copy_disabled"));
            this.windowDnDManager.dragStarting(startingDroppable, startingPoint, transfer);
            Cursor cursor = this.hackUserDropAction == 2 ? TopComponentDragSupport.getDragCursor(startingComp, 2) : (this.canCopy ? TopComponentDragSupport.getDragCursor(startingComp, 0) : TopComponentDragSupport.getDragCursor(startingComp, 4));
            this.addListening();
            this.hackESC = false;
            Tabbed tabbed = null;
            Tabbed.Accessor acc = (Tabbed.Accessor)SwingUtilities.getAncestorOfClass(Tabbed.Accessor.class, startingComp);
            tabbed = acc != null ? acc.getTabbed() : null;
            int tabIndex = -1;
            Image img = this.createDragImage();
            if (tabbed != null && transfer.isTopComponentTransfer() && WinSysPrefs.HANDLER.getBoolean("dnd.dragimage", Utilities.getOperatingSystem() != 8)) {
                tabIndex = tabbed.indexOf((Component)transfer.getTopComponent());
                this.visualizer = new DragAndDropFeedbackVisualizer(tabbed, tabIndex);
            }
            try {
                Transferable transferable2;
                Transferable transferable2;
                if (transfer.isTopComponentTransfer()) {
                    transferable2 = new TopComponentTransferable(transfer.getTopComponent());
                } else {
                    assert (transfer.isModeTransfer());
                    transferable2 = new TopComponentModeTransferable(transfer.getMode());
                }
                evt.startDrag(cursor, img, new Point(0, 0), transferable2, this);
                evt.getDragSource().addDragSourceMotionListener(this);
                if (null != this.visualizer) {
                    this.visualizer.start(evt);
                }
            }
            catch (InvalidDnDOperationException idoe) {
                Logger.getLogger(TopComponentDragSupport.class.getName()).log(Level.WARNING, null, idoe);
                this.removeListening();
                this.windowDnDManager.resetDragSource();
                if (null == this.visualizer) break block8;
                this.visualizer.dispose(false);
                this.visualizer = null;
            }
        }
    }

    private void addListening() {
        Toolkit.getDefaultToolkit().addAWTEventListener(this.keyListener, 8);
    }

    private void removeListening() {
        Toolkit.getDefaultToolkit().removeAWTEventListener(this.keyListener);
    }

    @Override
    public void dragEnter(DragSourceDragEvent evt) {
        if (DEBUG) {
            TopComponentDragSupport.debugLog("");
            TopComponentDragSupport.debugLog("dragEnter");
        }
        if (this.dragContextWRef.get() == null) {
            this.dragContextWRef = new WeakReference<DragSourceContext>(evt.getDragSourceContext());
        }
    }

    @Override
    public void dragOver(DragSourceDragEvent evt) {
        if (DEBUG) {
            TopComponentDragSupport.debugLog("");
            TopComponentDragSupport.debugLog("dragOver");
        }
    }

    @Override
    public void dragExit(DragSourceEvent evt) {
        if (DEBUG) {
            TopComponentDragSupport.debugLog("");
            TopComponentDragSupport.debugLog("dragExit");
        }
        if (this.dragContextWRef.get() == null) {
            this.dragContextWRef = new WeakReference<DragSourceContext>(evt.getDragSourceContext());
        }
    }

    @Override
    public void dropActionChanged(DragSourceDragEvent evt) {
        String name;
        int type;
        if (DEBUG) {
            TopComponentDragSupport.debugLog("");
            TopComponentDragSupport.debugLog("dropActionChanged");
        }
        if ((name = evt.getDragSourceContext().getCursor().getName()) == null) {
            return;
        }
        int userAction = evt.getUserAction();
        if (userAction == 0) {
            userAction = 2;
        }
        this.hackUserDropAction = userAction;
        if (("CursorTopComponentCopy".equals(name) || "CursorTopComponentCopyNoMove".equals(name)) && userAction == 2) {
            type = 2;
        } else if ("CursorTopComponentCopyNo".equals(name) && userAction == 2) {
            type = 3;
        } else if ("CursorTopComponentMove".equals(name) && userAction == 1) {
            type = 0;
        } else if ("CursorTopComponentMoveNo".equals(name) && userAction == 1) {
            type = 1;
        } else {
            return;
        }
        if (type == 0 && !this.canCopy) {
            type = 4;
        }
        if (TopComponentDragSupport.getDragCursorName(type).equals(evt.getDragSourceContext().getCursor().getName())) {
            return;
        }
        evt.getDragSourceContext().setCursor(TopComponentDragSupport.getDragCursor(evt.getDragSourceContext().getComponent(), type));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void dragDropEnd(final DragSourceDropEvent evt) {
        if (DEBUG) {
            TopComponentDragSupport.debugLog("");
            TopComponentDragSupport.debugLog("dragDropEnd");
        }
        try {
            if (this.checkDropSuccess(evt)) {
                this.windowDnDManager.dragFinished();
                this.removeListening();
                return;
            }
            final Set<Component> floatingFrames = this.windowDnDManager.getFloatingFrames();
            RequestProcessor.getDefault().post(new Runnable(){

                @Override
                public void run() {
                    SwingUtilities.invokeLater(TopComponentDragSupport.this.createDropIntoFreeAreaTask(evt, evt.getLocation(), floatingFrames));
                }
            }, 350);
        }
        finally {
            this.windowDnDManager.dragFinishedEx();
        }
    }

    private boolean checkDropSuccess(DragSourceDropEvent evt) {
        if (this.windowDnDManager.isDropSuccess()) {
            return true;
        }
        Point location = evt.getLocation();
        if (location == null) {
            return true;
        }
        if (WindowDnDManager.isInMainWindow(location) || this.windowDnDManager.isInFloatingFrame(location) || WindowDnDManager.isAroundCenterPanel(location)) {
            return false;
        }
        return false;
    }

    private Runnable createDropIntoFreeAreaTask(final DragSourceDropEvent evt, final Point location, final Set<Component> floatingFrames) {
        final int dropAction = this.hackUserDropAction;
        return new Runnable(){

            @Override
            public void run() {
                TopComponentDragSupport.this.removeListening();
                if (TopComponentDragSupport.this.hackESC) {
                    TopComponentDragSupport.this.windowDnDManager.dragFinished();
                    return;
                }
                TopComponentDraggable transfer = WindowDnDManager.extractTopComponentDraggable(dropAction == 1, evt.getDragSourceContext().getTransferable());
                if (transfer != null) {
                    boolean res = TopComponentDragSupport.this.windowDnDManager.tryPerformDrop(TopComponentDragSupport.this.windowDnDManager.getController(), floatingFrames, location, dropAction, evt.getDragSourceContext().getTransferable());
                }
                TopComponentDragSupport.this.windowDnDManager.dragFinished();
            }
        };
    }

    void setSuccessCursor(boolean freeArea, boolean mixedDragDrop) {
        int dropAction = this.hackUserDropAction;
        DragSourceContext ctx = this.dragContextWRef.get();
        if (ctx == null) {
            return;
        }
        if (null != this.visualizer) {
            this.visualizer.setDropFeedback(true, mixedDragDrop);
        }
        this.dropFailed = false;
    }

    void setUnsuccessCursor(boolean mixedDragDrop) {
        DragSourceContext ctx = this.dragContextWRef.get();
        if (ctx == null) {
            return;
        }
        if (null != this.visualizer) {
            this.visualizer.setDropFeedback(false, mixedDragDrop);
        }
        String name = ctx.getCursor().getName();
        this.dropFailed = true;
    }

    void dragFinished() {
        this.dragContextWRef = new WeakReference<Object>(null);
        if (null != this.visualizer) {
            this.visualizer.dispose(!this.dropFailed && !this.hackESC);
            this.dropFailed = false;
            this.visualizer = null;
        }
    }

    private static void debugLog(String message) {
        Debug.log(TopComponentDragSupport.class, message);
    }

    private static String getDragCursorName(int type) {
        if (type == 0) {
            return "CursorTopComponentCopy";
        }
        if (type == 1) {
            return "CursorTopComponentCopyNo";
        }
        if (type == 2) {
            return "CursorTopComponentMove";
        }
        if (type == 3) {
            return "CursorTopComponentMoveNo";
        }
        if (type == 4) {
            return "CursorTopComponentCopyNoMove";
        }
        if (type == 5) {
            return "CursorTopComponentMoveFree";
        }
        return null;
    }

    private static Cursor getDragCursor(Component comp, int type) {
        Image image = null;
        String name = null;
        if (type == 0) {
            image = ImageUtilities.loadImage((String)"org/netbeans/core/resources/topComponentDragCopy.gif");
            name = "CursorTopComponentCopy";
        } else if (type == 1) {
            image = ImageUtilities.loadImage((String)"org/netbeans/core/resources/topComponentDragCopyNo.gif");
            name = "CursorTopComponentCopyNo";
        } else if (type == 2) {
            image = ImageUtilities.loadImage((String)"org/netbeans/core/resources/topComponentDragMove.gif");
            name = "CursorTopComponentMove";
        } else if (type == 3) {
            image = ImageUtilities.loadImage((String)"org/netbeans/core/resources/topComponentDragMoveNo.gif");
            name = "CursorTopComponentMoveNo";
        } else if (type == 4) {
            image = ImageUtilities.loadImage((String)"org/netbeans/core/resources/topComponentDragCopyNo.gif");
            name = "CursorTopComponentCopyNoMove";
        } else if (type == 5) {
            image = ImageUtilities.loadImage((String)"org/netbeans/core/windows/resources/topComponentDragMoveFreeArea.gif");
            name = "CursorTopComponentMoveFree";
        } else {
            throw new IllegalArgumentException("Unknown cursor type=" + type);
        }
        return Utilities.createCustomCursor((Component)comp, (Image)image, (String)name);
    }

    @Override
    public void dragMouseMoved(DragSourceDragEvent dsde) {
        if (null != this.visualizer) {
            this.visualizer.update(dsde);
        }
    }

    private Image createDragImage() {
        GraphicsConfiguration config = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
        BufferedImage res = config.createCompatibleImage(1, 1);
        Graphics2D g = res.createGraphics();
        g.setColor(Color.white);
        g.fillRect(0, 0, 1, 1);
        return res;
    }

    private static class FakeDragGestureRecognizer
    extends DragGestureRecognizer {
        public FakeDragGestureRecognizer(WindowDnDManager windowDnDManager, MouseEvent evt) {
            super(windowDnDManager.getWindowDragSource(), (Component)evt.getSource(), 3, null);
            this.appendEvent(evt);
        }

        @Override
        public void registerListeners() {
        }

        @Override
        public void unregisterListeners() {
        }
    }

    private static class TopComponentModeTransferable
    implements Transferable {
        private WeakReference<ModeImpl> weakRef;

        public TopComponentModeTransferable(ModeImpl mode) {
            this.weakRef = new WeakReference<ModeImpl>(mode);
        }

        @Override
        public Object getTransferData(DataFlavor df) {
            if (this.isDataFlavorSupported(df)) {
                return this.weakRef.get();
            }
            return null;
        }

        @Override
        public DataFlavor[] getTransferDataFlavors() {
            try {
                return new DataFlavor[]{new DataFlavor("application/x-java-jvm-local-objectref; class=org.netbeans.core.windows.ModeImpl", null, ModeImpl.class.getClassLoader())};
            }
            catch (ClassNotFoundException ex) {
                Logger.getLogger(TopComponentDragSupport.class.getName()).log(Level.WARNING, ex.getMessage(), ex);
                return new DataFlavor[0];
            }
        }

        @Override
        public boolean isDataFlavorSupported(DataFlavor df) {
            if ("application/x-java-jvm-local-objectref; class=org.netbeans.core.windows.ModeImpl".equals(df.getMimeType())) {
                return true;
            }
            return false;
        }
    }

    private static class TopComponentTransferable
    implements Transferable {
        private WeakReference<TopComponent> weakTC;

        public TopComponentTransferable(TopComponent tc) {
            this.weakTC = new WeakReference<TopComponent>(tc);
        }

        @Override
        public Object getTransferData(DataFlavor df) {
            TopComponent tc = this.weakTC.get();
            if ("application/x-java-jvm-local-objectref; class=org.openide.windows.TopComponent".equals(df.getMimeType())) {
                return tc;
            }
            if ("application/x-java-jvm-local-objectref; class=org.openide.windows.TopComponent$Cloneable".equals(df.getMimeType()) && tc instanceof TopComponent.Cloneable) {
                return tc;
            }
            return null;
        }

        @Override
        public DataFlavor[] getTransferDataFlavors() {
            try {
                TopComponent tc = this.weakTC.get();
                if (tc instanceof TopComponent.Cloneable) {
                    return new DataFlavor[]{new DataFlavor("application/x-java-jvm-local-objectref; class=org.openide.windows.TopComponent", null, TopComponent.class.getClassLoader()), new DataFlavor("application/x-java-jvm-local-objectref; class=org.openide.windows.TopComponent$Cloneable", null, TopComponent.Cloneable.class.getClassLoader())};
                }
                return new DataFlavor[]{new DataFlavor("application/x-java-jvm-local-objectref; class=org.openide.windows.TopComponent", null, TopComponent.class.getClassLoader())};
            }
            catch (ClassNotFoundException ex) {
                Logger.getLogger(TopComponentDragSupport.class.getName()).log(Level.WARNING, ex.getMessage(), ex);
                return new DataFlavor[0];
            }
        }

        @Override
        public boolean isDataFlavorSupported(DataFlavor df) {
            TopComponent tc = this.weakTC.get();
            if ("application/x-java-jvm-local-objectref; class=org.openide.windows.TopComponent".equals(df.getMimeType())) {
                return true;
            }
            if ("application/x-java-jvm-local-objectref; class=org.openide.windows.TopComponent$Cloneable".equals(df.getMimeType()) && tc instanceof TopComponent.Cloneable) {
                return true;
            }
            return false;
        }
    }

}

