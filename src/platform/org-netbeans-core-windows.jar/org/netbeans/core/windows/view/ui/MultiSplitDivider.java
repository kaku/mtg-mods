/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view.ui;

import java.awt.Dimension;
import java.awt.IllegalComponentStateException;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Locale;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleRole;
import javax.accessibility.AccessibleState;
import javax.accessibility.AccessibleStateSet;
import javax.accessibility.AccessibleValue;
import org.netbeans.core.windows.view.ui.MultiSplitCell;
import org.netbeans.core.windows.view.ui.MultiSplitPane;

public class MultiSplitDivider
implements Accessible {
    MultiSplitPane splitPane;
    Rectangle rect = new Rectangle();
    MultiSplitCell first;
    MultiSplitCell second;
    Point currentDragLocation;
    int dragMin;
    int dragMax;
    int cursorPositionCompensation;
    private AccessibleContext accessibleContext;

    public MultiSplitDivider(MultiSplitPane parent, MultiSplitCell first, MultiSplitCell second) {
        assert (null != parent);
        assert (null != first);
        assert (null != second);
        this.splitPane = parent;
        this.first = first;
        this.second = second;
        this.reshape();
    }

    boolean isHorizontal() {
        return this.splitPane.isHorizontalSplit();
    }

    boolean isVertical() {
        return this.splitPane.isVerticalSplit();
    }

    int getDividerSize() {
        return this.splitPane.getDividerSize();
    }

    boolean containsPoint(Point p) {
        return this.rect.contains(p);
    }

    void startDragging(Point p) {
        this.currentDragLocation = new Point(this.rect.x, this.rect.y);
        this.cursorPositionCompensation = this.isHorizontal() ? p.x - this.rect.x : p.y - this.rect.y;
        this.initDragMinMax();
    }

    void dragTo(Point p) {
        if (this.isHorizontal()) {
            if (p.x < this.dragMin) {
                p.x = this.dragMin;
            }
            if (p.x > this.dragMax) {
                p.x = this.dragMax;
            }
        } else {
            if (p.y < this.dragMin) {
                p.y = this.dragMin;
            }
            if (p.y > this.dragMax) {
                p.y = this.dragMax;
            }
        }
        this.currentDragLocation = p;
        this.resize(p);
    }

    void resize(int delta) {
        Point p = this.rect.getLocation();
        if (this.isHorizontal()) {
            p.x += delta;
        } else {
            p.y += delta;
        }
        this.resize(p);
    }

    private void resize(Point p) {
        if (this.isHorizontal()) {
            p.x -= this.cursorPositionCompensation;
            if (p.x < this.dragMin) {
                p.x = this.dragMin;
            }
            if (p.x > this.dragMax) {
                p.x = this.dragMax;
            }
            if (p.x == this.rect.x) {
                return;
            }
        } else {
            p.y -= this.cursorPositionCompensation;
            if (p.y < this.dragMin) {
                p.y = this.dragMin;
            }
            if (p.y > this.dragMax) {
                p.y = this.dragMax;
            }
            if (p.y == this.rect.y) {
                return;
            }
        }
        int dividerSize = this.getDividerSize();
        if (this.isHorizontal()) {
            int delta = p.x - this.rect.x;
            int x = this.first.getLocation();
            int y = 0;
            int width = this.first.getSize() + delta;
            int height = this.rect.height;
            this.first.layout(x, y, width, height);
            x = this.second.getLocation() + delta;
            width = this.second.getSize() - delta;
            this.second.layout(x, y, width, height);
            this.rect.x = p.x;
        } else {
            int delta = p.y - this.rect.y;
            int x = 0;
            int y = this.first.getLocation();
            int width = this.rect.width;
            int height = this.first.getSize() + delta;
            this.first.layout(x, y, width, height);
            y = this.second.getLocation() + delta;
            height = this.second.getSize() - delta;
            this.second.layout(x, y, width, height);
            this.rect.y = p.y;
        }
        this.splitPane.splitterMoved();
    }

    void finishDraggingTo(Point p) {
        this.resize(p);
        this.currentDragLocation = null;
    }

    Point initDragMinMax() {
        int firstSize = this.first.getSize();
        int secondSize = this.second.getSize();
        int firstMinSize = this.first.getMinimumSize();
        int secondMinSize = this.second.getMinimumSize();
        if (this.isHorizontal()) {
            this.dragMin = this.rect.x;
            this.dragMax = this.rect.x;
        } else {
            this.dragMin = this.rect.y;
            this.dragMax = this.rect.y;
        }
        if (firstSize >= firstMinSize) {
            this.dragMin -= firstSize - firstMinSize;
        }
        if (secondSize >= secondMinSize) {
            this.dragMax += secondSize - secondMinSize;
        }
        return this.rect.getLocation();
    }

    void reshape() {
        Dimension d = this.splitPane.getSize();
        int location = this.second.getLocation();
        if (this.isHorizontal()) {
            this.rect.x = location - this.getDividerSize();
            this.rect.y = 0;
            this.rect.width = this.getDividerSize();
            this.rect.height = d.height;
        } else {
            this.rect.x = 0;
            this.rect.y = location - this.getDividerSize();
            this.rect.width = d.width;
            this.rect.height = this.getDividerSize();
        }
    }

    @Override
    public AccessibleContext getAccessibleContext() {
        if (null == this.accessibleContext) {
            this.accessibleContext = new AccessibleMultiSplitDivider();
        }
        return this.accessibleContext;
    }

    protected class AccessibleMultiSplitDivider
    extends AccessibleContext
    implements AccessibleValue {
        public AccessibleMultiSplitDivider() {
            this.setAccessibleParent(MultiSplitDivider.this.splitPane);
        }

        @Override
        public Accessible getAccessibleChild(int i) {
            return null;
        }

        @Override
        public int getAccessibleChildrenCount() {
            return 0;
        }

        @Override
        public int getAccessibleIndexInParent() {
            return MultiSplitDivider.this.splitPane.getDividerAccessibleIndex(MultiSplitDivider.this);
        }

        @Override
        public AccessibleRole getAccessibleRole() {
            return AccessibleRole.SPLIT_PANE;
        }

        @Override
        public AccessibleStateSet getAccessibleStateSet() {
            AccessibleStateSet stateSet = new AccessibleStateSet();
            if (MultiSplitDivider.this.isHorizontal()) {
                stateSet.add(AccessibleState.HORIZONTAL);
            } else {
                stateSet.add(AccessibleState.VERTICAL);
            }
            return stateSet;
        }

        @Override
        public Locale getLocale() throws IllegalComponentStateException {
            return Locale.getDefault();
        }

        @Override
        public boolean setCurrentAccessibleValue(Number n) {
            MultiSplitDivider.this.initDragMinMax();
            int value = n.intValue();
            if (value < MultiSplitDivider.this.dragMin || value > MultiSplitDivider.this.dragMax) {
                return false;
            }
            if (MultiSplitDivider.this.isHorizontal()) {
                MultiSplitDivider.this.finishDraggingTo(new Point(value, 0));
            } else {
                MultiSplitDivider.this.finishDraggingTo(new Point(0, value));
            }
            return true;
        }

        @Override
        public Number getMinimumAccessibleValue() {
            MultiSplitDivider.this.initDragMinMax();
            return MultiSplitDivider.this.dragMin;
        }

        @Override
        public Number getMaximumAccessibleValue() {
            MultiSplitDivider.this.initDragMinMax();
            return MultiSplitDivider.this.dragMax;
        }

        @Override
        public Number getCurrentAccessibleValue() {
            if (MultiSplitDivider.this.isHorizontal()) {
                return MultiSplitDivider.this.rect.x;
            }
            return MultiSplitDivider.this.rect.y;
        }

        @Override
        public AccessibleValue getAccessibleValue() {
            return this;
        }
    }

}

