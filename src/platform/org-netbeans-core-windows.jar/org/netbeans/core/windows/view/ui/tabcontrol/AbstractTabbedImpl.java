/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.ComponentConverter
 *  org.netbeans.swing.tabcontrol.TabData
 *  org.netbeans.swing.tabcontrol.TabDataModel
 *  org.netbeans.swing.tabcontrol.customtabs.Tabbed
 *  org.netbeans.swing.tabcontrol.plaf.EqualPolygon
 *  org.openide.util.ChangeSupport
 *  org.openide.util.WeakListeners
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view.ui.tabcontrol;

import java.awt.Component;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.SingleSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeListener;
import org.netbeans.core.windows.Debug;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.actions.ActionUtils;
import org.netbeans.core.windows.view.ui.tabcontrol.TabbedAdapter;
import org.netbeans.swing.tabcontrol.ComponentConverter;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.customtabs.Tabbed;
import org.netbeans.swing.tabcontrol.plaf.EqualPolygon;
import org.openide.util.ChangeSupport;
import org.openide.util.WeakListeners;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public abstract class AbstractTabbedImpl
extends Tabbed {
    private PropertyChangeListener tooltipListener;
    private PropertyChangeListener weakTooltipListener;
    private final ChangeSupport cs;
    private static final boolean DEBUG = Debug.isLoggable(TabbedAdapter.class);

    public AbstractTabbedImpl() {
        this.cs = new ChangeSupport((Object)this);
    }

    protected abstract TabDataModel getTabModel();

    protected abstract SingleSelectionModel getSelectionModel();

    public final void addTopComponent(String name, Icon icon, TopComponent tc, String toolTip) {
        this.insertComponent(name, icon, (Component)tc, toolTip, this.getTabCount());
    }

    public final TopComponent getTopComponentAt(int index) {
        if (index == -1 || index >= this.getTabModel().size()) {
            return null;
        }
        return (TopComponent)this.getTabModel().getTab(index).getComponent();
    }

    public final TopComponent getSelectedTopComponent() {
        int i = this.getSelectionModel().getSelectedIndex();
        return i == -1 ? null : this.getTopComponentAt(i);
    }

    public final void requestAttention(TopComponent tc) {
        int idx = this.indexOf((Component)tc);
        if (idx >= 0) {
            this.requestAttention(idx);
        } else {
            Logger.getAnonymousLogger().fine("RequestAttention on component unknown to container: " + (Object)tc);
        }
    }

    protected abstract void requestAttention(int var1);

    public final void cancelRequestAttention(TopComponent tc) {
        int idx = this.indexOf((Component)tc);
        if (idx < 0) {
            throw new IllegalArgumentException("TopComponent " + (Object)tc + " is not a child of this container");
        }
        this.cancelRequestAttention(idx);
    }

    protected abstract void cancelRequestAttention(int var1);

    public final void setAttentionHighlight(TopComponent tc, boolean highlight) {
        int idx = this.indexOf((Component)tc);
        if (idx < 0) {
            throw new IllegalArgumentException("TopComponent " + (Object)tc + " is not a child of this container");
        }
        this.setAttentionHighlight(idx, highlight);
    }

    protected abstract void setAttentionHighlight(int var1, boolean var2);

    public final void insertComponent(String name, Icon icon, Component comp, String toolTip, int position) {
        TabData td = new TabData((Object)comp, icon, name, toolTip);
        if (DEBUG) {
            Debug.log(AbstractTabbedImpl.class, "InsertTab: " + name + " hash:" + System.identityHashCode(comp));
        }
        this.getTabModel().addTab(position, td);
        comp.addPropertyChangeListener("ToolTipText", this.getTooltipListener(comp));
    }

    public final void setSelectedComponent(Component comp) {
        int i = this.indexOf(comp);
        if (i == -1 && null != comp) {
            throw new IllegalArgumentException("Component not a child of this control: " + comp);
        }
        this.getSelectionModel().setSelectedIndex(i);
    }

    public final TopComponent[] getTopComponents() {
        ComponentConverter cc = this.getComponentConverter();
        TabData[] td = this.getTabModel().getTabs().toArray((T[])new TabData[0]);
        TopComponent[] result = new TopComponent[this.getTabModel().size()];
        for (int i = 0; i < td.length; ++i) {
            result[i] = (TopComponent)cc.getComponent(td[i]);
        }
        return result;
    }

    public final void removeComponent(Component comp) {
        int i = this.indexOf(comp);
        this.getTabModel().removeTab(i);
        comp.removePropertyChangeListener("ToolTipText", this.getTooltipListener(comp));
        if (this.getTabModel().size() == 0) {
            ((JComponent)this.getComponent()).revalidate();
            ((JComponent)this.getComponent()).repaint();
        }
    }

    public final void setTopComponents(TopComponent[] tcs, TopComponent selected) {
        if (selected == null && tcs.length > 0) {
            selected = tcs[0];
            Logger.getLogger(TabbedAdapter.class.getName()).warning("Selected component is null although open components are " + Arrays.asList(tcs));
        }
        int sizeBefore = this.getTabModel().size();
        this.detachTooltipListeners(this.getTabModel().getTabs());
        TabData[] data = new TabData[tcs.length];
        int toSelect = -1;
        for (int i = 0; i < tcs.length; ++i) {
            TopComponent tc = tcs[i];
            Image icon = tc.getIcon();
            String displayName = WindowManagerImpl.getInstance().getTopComponentDisplayName(tc);
            data[i] = new TabData((Object)tc, (Icon)(icon == null ? null : new ImageIcon(icon)), displayName == null ? "" : displayName, tc.getToolTipText());
            if (selected == tcs[i]) {
                toSelect = i;
            }
            tc.addPropertyChangeListener("ToolTipText", this.getTooltipListener((Component)tc));
        }
        assert (selected != null && toSelect != -1);
        this.getTabModel().setTabs(data);
        if (toSelect != -1) {
            this.getSelectionModel().setSelectedIndex(toSelect);
        } else if (selected != null) {
            Logger.getAnonymousLogger().warning("Tried toset a selected component that was not in the array of open components.  ToSelect: " + (Object)selected + " components: " + Arrays.asList(tcs));
        }
        int sizeNow = this.getTabModel().size();
        if (sizeBefore != 0 && sizeNow == 0) {
            ((JComponent)this.getComponent()).revalidate();
            ((JComponent)this.getComponent()).repaint();
        }
    }

    public final Object getConstraintForLocation(Point location, boolean attachingPossible) {
        String s;
        int tab = this.tabForCoordinate(location);
        if (tab != -1) {
            int index = this.dropIndexOfPoint(location);
            return index < 0 ? null : Integer.valueOf(index);
        }
        if (attachingPossible && (s = this.getSideForLocation(location)) != null) {
            return s;
        }
        int index = this.dropIndexOfPoint(location);
        return index < 0 ? null : Integer.valueOf(index);
    }

    protected abstract int dropIndexOfPoint(Point var1);

    private String getSideForLocation(Point location) {
        Rectangle top;
        int LEFT_WIDTH;
        Rectangle bounds = this.getComponent().getBounds();
        bounds.setLocation(0, 0);
        int TOP_HEIGHT = 10;
        int BOTTOM_HEIGHT = (int)(0.25 * (double)bounds.height);
        int RIGHT_WIDTH = LEFT_WIDTH = Math.max(this.getComponent().getWidth() / 8, 40);
        if (DEBUG) {
            AbstractTabbedImpl.debugLog("");
            AbstractTabbedImpl.debugLog("TOP_HEIGHT    =10");
            AbstractTabbedImpl.debugLog("BOTTOM_HEIGHT =" + BOTTOM_HEIGHT);
            AbstractTabbedImpl.debugLog("LEFT_WIDTH    =" + LEFT_WIDTH);
            AbstractTabbedImpl.debugLog("RIGHT_WIDTH   =" + RIGHT_WIDTH);
        }
        if ((top = new Rectangle(0, 0, bounds.width, BOTTOM_HEIGHT)).contains(location)) {
            return "top";
        }
        EqualPolygon left = new EqualPolygon(new int[]{0, LEFT_WIDTH, LEFT_WIDTH, 0}, new int[]{10, 10, bounds.height - BOTTOM_HEIGHT, bounds.height}, 4);
        if (left.contains(location)) {
            return "left";
        }
        EqualPolygon right = new EqualPolygon(new int[]{bounds.width - RIGHT_WIDTH, bounds.width, bounds.width, bounds.width - RIGHT_WIDTH}, new int[]{10, 10, bounds.height, bounds.height - BOTTOM_HEIGHT}, 4);
        if (right.contains(location)) {
            return "right";
        }
        EqualPolygon bottom = new EqualPolygon(new int[]{LEFT_WIDTH, bounds.width - RIGHT_WIDTH, bounds.width, 0}, new int[]{bounds.height - BOTTOM_HEIGHT, bounds.height - BOTTOM_HEIGHT, bounds.height, bounds.height}, 4);
        if (bottom.contains(location)) {
            return "bottom";
        }
        return null;
    }

    public final Shape getIndicationForLocation(Point location, TopComponent startingTransfer, Point startingPoint, boolean attachingPossible) {
        Shape s;
        Rectangle rect = this.getComponent().getBounds();
        rect.setLocation(0, 0);
        TopComponent draggedTC = startingTransfer;
        int tab = this.tabForCoordinate(location);
        if (tab != -1 && (s = this.getDropIndication(draggedTC, location)) != null) {
            return s;
        }
        String side = attachingPossible ? this.getSideForLocation(location) : null;
        double ratio = 0.5;
        if (side == "top") {
            return new Rectangle(0, 0, rect.width, (int)((double)rect.height * ratio));
        }
        if (side == "left") {
            return new Rectangle(0, 0, (int)((double)rect.width * ratio), rect.height);
        }
        if (side == "right") {
            return new Rectangle(rect.width - (int)((double)rect.width * ratio), 0, (int)((double)rect.width * ratio), rect.height);
        }
        if (side == "bottom") {
            return new Rectangle(0, rect.height - (int)((double)rect.height * ratio), rect.width, (int)((double)rect.height * ratio));
        }
        Shape s2 = this.getDropIndication(draggedTC, location);
        if (s2 != null) {
            return s2;
        }
        if (null != startingTransfer && startingPoint != null && this.indexOf((Component)startingTransfer) != -1) {
            return this.getStartingIndication(startingPoint, location);
        }
        return rect;
    }

    private Shape getStartingIndication(Point startingPoint, Point location) {
        Rectangle rect = this.getComponent().getBounds();
        rect.setLocation(location.x - startingPoint.x, location.y - startingPoint.y);
        return rect;
    }

    public final Action[] getPopupActions(Action[] defaultActions, int tabIndex) {
        if (tabIndex < 0) {
            ModeImpl mode = this.getModeImpl();
            if (null != mode) {
                return ActionUtils.createDefaultPopupActions(mode);
            }
            return null;
        }
        return defaultActions;
    }

    private ModeImpl getModeImpl() {
        TopComponent[] topComponents = this.getTopComponents();
        if (topComponents.length < 1) {
            return null;
        }
        return (ModeImpl)WindowManagerImpl.getInstance().findMode(topComponents[0]);
    }

    public final void addChangeListener(ChangeListener listener) {
        this.cs.addChangeListener(listener);
    }

    public final void removeChangeListener(ChangeListener listener) {
        this.cs.removeChangeListener(listener);
    }

    public boolean isBusy(TopComponent tc) {
        return WindowManagerImpl.getInstance().isTopComponentBusy(tc);
    }

    protected abstract ComponentConverter getComponentConverter();

    PropertyChangeListener getTooltipListener(Component comp) {
        if (this.tooltipListener == null) {
            this.tooltipListener = new ToolTipListener();
            this.weakTooltipListener = WeakListeners.propertyChange((PropertyChangeListener)this.tooltipListener, (Object)comp);
        }
        return this.weakTooltipListener;
    }

    protected abstract Shape getDropIndication(TopComponent var1, Point var2);

    private void detachTooltipListeners(List tabs) {
        Iterator<E> iter = tabs.iterator();
        while (iter.hasNext()) {
            JComponent curComp = (JComponent)((TabData)iter.next()).getComponent();
            curComp.removePropertyChangeListener("ToolTipText", this.getTooltipListener(curComp));
        }
    }

    protected final void fireStateChanged() {
        if (!SwingUtilities.isEventDispatchThread()) {
            Logger.getAnonymousLogger().warning("All state changes to the tab component must happen on the event thread!");
            Exception e = new Exception();
            e.fillInStackTrace();
            Logger.getAnonymousLogger().warning(e.getStackTrace()[1].toString());
        }
        this.cs.fireChange();
    }

    private static void debugLog(String message) {
        Debug.log(TabbedAdapter.class, message);
    }

    private class ToolTipListener
    implements PropertyChangeListener {
        private ToolTipListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("ToolTipText".equals(evt.getPropertyName())) {
                List tabs = AbstractTabbedImpl.this.getTabModel().getTabs();
                int index = 0;
                Iterator iter = tabs.iterator();
                while (iter.hasNext()) {
                    JComponent curComp = (JComponent)((TabData)iter.next()).getComponent();
                    if (curComp == evt.getSource() && index < AbstractTabbedImpl.this.getTabCount()) {
                        AbstractTabbedImpl.this.setToolTipTextAt(index, (String)evt.getNewValue());
                        break;
                    }
                    ++index;
                }
            }
        }
    }

}

