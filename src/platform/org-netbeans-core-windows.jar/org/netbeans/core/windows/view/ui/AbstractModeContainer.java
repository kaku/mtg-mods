/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.customtabs.Tabbed
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view.ui;

import java.awt.Component;
import java.awt.Point;
import java.awt.Shape;
import java.awt.Window;
import java.util.Arrays;
import javax.swing.FocusManager;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.view.ModeContainer;
import org.netbeans.core.windows.view.ModeView;
import org.netbeans.core.windows.view.dnd.TopComponentDraggable;
import org.netbeans.core.windows.view.dnd.TopComponentDroppable;
import org.netbeans.core.windows.view.dnd.WindowDnDManager;
import org.netbeans.core.windows.view.ui.TabbedHandler;
import org.netbeans.swing.tabcontrol.customtabs.Tabbed;
import org.openide.windows.TopComponent;

public abstract class AbstractModeContainer
implements ModeContainer {
    protected final ModeView modeView;
    protected final TabbedHandler tabbedHandler;
    protected final WindowDnDManager windowDnDManager;
    private final int kind;

    public AbstractModeContainer(ModeView modeView, WindowDnDManager windowDnDManager, int kind) {
        this.modeView = modeView;
        this.windowDnDManager = windowDnDManager;
        this.kind = kind;
        this.tabbedHandler = new TabbedHandler(modeView, kind, this.createTabbed());
    }

    @Override
    public ModeView getModeView() {
        return this.modeView;
    }

    @Override
    public Component getComponent() {
        return this.getModeComponent();
    }

    protected abstract Component getModeComponent();

    protected abstract Tabbed createTabbed();

    @Override
    public void addTopComponent(TopComponent tc) {
        this.tabbedHandler.addTopComponent(tc, this.kind);
    }

    @Override
    public void removeTopComponent(TopComponent tc) {
        this.tabbedHandler.removeTopComponent(tc);
        TopComponent selected = this.tabbedHandler.getSelectedTopComponent();
        this.updateTitle(selected == null ? "" : WindowManagerImpl.getInstance().getTopComponentDisplayName(selected));
    }

    @Override
    public void setSelectedTopComponent(TopComponent tc) {
        this.tabbedHandler.setSelectedTopComponent(tc);
        this.updateTitle(WindowManagerImpl.getInstance().getTopComponentDisplayName(tc));
    }

    @Override
    public void setTopComponents(TopComponent[] tcs, TopComponent selected) {
        if (!Arrays.equals((Object[])tcs, (Object[])this.getTopComponents())) {
            this.tabbedHandler.setTopComponents(tcs, selected);
            this.updateTitle(WindowManagerImpl.getInstance().getTopComponentDisplayName(selected));
        } else {
            this.setSelectedTopComponent(selected);
        }
    }

    protected abstract void updateTitle(String var1);

    protected abstract void updateActive(boolean var1);

    @Override
    public TopComponent getSelectedTopComponent() {
        return this.tabbedHandler.getSelectedTopComponent();
    }

    @Override
    public void setActive(boolean active) {
        this.updateActive(active);
        TopComponent selected = this.tabbedHandler.getSelectedTopComponent();
        this.updateTitle(selected == null ? "" : WindowManagerImpl.getInstance().getTopComponentDisplayName(selected));
        this.tabbedHandler.setActive(active);
    }

    @Override
    public void focusSelectedTopComponent() {
        TopComponent selectedTopComponent = this.tabbedHandler.getSelectedTopComponent();
        if (selectedTopComponent == null) {
            return;
        }
        Window oldFocusedW = FocusManager.getCurrentManager().getFocusedWindow();
        Window newFocusedW = SwingUtilities.getWindowAncestor((Component)selectedTopComponent);
        if (newFocusedW != null) {
            if (newFocusedW.equals(oldFocusedW) || null == oldFocusedW) {
                selectedTopComponent.requestFocusInWindow();
            } else {
                newFocusedW.toFront();
                selectedTopComponent.requestFocus();
            }
        }
    }

    @Override
    public TopComponent[] getTopComponents() {
        return this.tabbedHandler.getTopComponents();
    }

    @Override
    public void updateName(TopComponent tc) {
        TopComponent selected = this.getSelectedTopComponent();
        if (tc == selected) {
            this.updateTitle(tc == null ? "" : WindowManagerImpl.getInstance().getTopComponentDisplayName(tc));
        }
        this.tabbedHandler.topComponentNameChanged(tc, this.kind);
    }

    @Override
    public void updateToolTip(TopComponent tc) {
        this.tabbedHandler.topComponentToolTipChanged(tc);
    }

    @Override
    public void updateIcon(TopComponent tc) {
        this.tabbedHandler.topComponentIconChanged(tc);
    }

    protected int getKind() {
        return this.kind;
    }

    protected Shape getIndicationForLocation(Point location) {
        return this.tabbedHandler.getIndicationForLocation(location, this.windowDnDManager.getStartingTransfer().getTopComponent(), this.windowDnDManager.getStartingPoint(), this.isAttachingPossible());
    }

    protected Object getConstraintForLocation(Point location) {
        return this.tabbedHandler.getConstraintForLocation(location, this.isAttachingPossible());
    }

    protected abstract boolean isAttachingPossible();

    protected ModeView getDropModeView() {
        return this.modeView;
    }

    protected Component getDropComponent() {
        return this.tabbedHandler.getComponent();
    }

    protected abstract TopComponentDroppable getModeDroppable();

    protected boolean canDrop(TopComponentDraggable transfer) {
        if (transfer.isAllowedToMoveAnywhere()) {
            return true;
        }
        boolean isNonEditor = transfer.getKind() == 0 || transfer.getKind() == 2;
        boolean thisIsNonEditor = this.kind == 0 || this.kind == 2;
        return isNonEditor == thisIsNonEditor;
    }
}

