/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view.ui.slides;

import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Rectangle;
import java.util.Set;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.view.ui.slides.SlideOperation;
import org.netbeans.core.windows.view.ui.slides.SlidingFx;
import org.openide.windows.TopComponent;

class SlideOperationImpl
implements SlideOperation,
ChangeListener {
    private final int type;
    private final Component component;
    private final SlidingFx effect;
    private final boolean requestsActivation;
    private final String side;
    protected Rectangle startBounds;
    protected Rectangle finishBounds;
    private JLayeredPane pane;
    private Integer layer;

    SlideOperationImpl(int type, Component component, int orientation, SlidingFx effect, boolean requestsActivation) {
        this(type, component, SlideOperationImpl.orientation2Side(orientation), effect, requestsActivation);
    }

    SlideOperationImpl(int type, Component component, String side, SlidingFx effect, boolean requestsActivation) {
        this.type = type;
        this.component = component;
        this.effect = effect;
        this.requestsActivation = requestsActivation;
        this.side = side;
    }

    @Override
    public void run(JLayeredPane pane, Integer layer) {
        if (this.effect != null && this.effect.shouldOperationWait()) {
            this.pane = pane;
            this.layer = layer;
            this.effect.setFinishListener(this);
            this.effect.showEffect(pane, layer, this);
        } else {
            if (this.effect != null) {
                this.effect.showEffect(pane, layer, this);
            }
            this.performOperation(pane, layer);
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        this.performOperation(this.pane, this.layer);
        this.pane = null;
        this.layer = null;
    }

    private void performOperation(JLayeredPane pane, Integer layer) {
        switch (this.type) {
            case 0: {
                this.component.setBounds(this.finishBounds);
                pane.add(this.component, layer);
                if (!this.isHeavyWeightShowing()) break;
                this.repaintLayeredPane();
                break;
            }
            case 1: {
                pane.remove(this.component);
                break;
            }
            case 4: {
                this.component.setBounds(this.finishBounds);
                ((JComponent)this.component).revalidate();
                if (!this.isHeavyWeightShowing()) break;
                this.repaintLayeredPane();
            }
        }
    }

    @Override
    public void setFinishBounds(Rectangle bounds) {
        this.finishBounds = bounds;
    }

    @Override
    public void setStartBounds(Rectangle bounds) {
        this.startBounds = bounds;
    }

    @Override
    public String getSide() {
        return this.side;
    }

    @Override
    public Component getComponent() {
        return this.component;
    }

    @Override
    public Rectangle getFinishBounds() {
        return this.finishBounds;
    }

    @Override
    public Rectangle getStartBounds() {
        return this.startBounds;
    }

    @Override
    public boolean requestsActivation() {
        return this.requestsActivation;
    }

    protected static String orientation2Side(int orientation) {
        String side = "left";
        if (orientation == 2) {
            side = "left";
        } else if (orientation == 1) {
            side = "right";
        } else if (orientation == 3) {
            side = "bottom";
        } else if (orientation == 4) {
            side = "top";
        }
        return side;
    }

    @Override
    public int getType() {
        return this.type;
    }

    @Override
    public void prepareEffect() {
        if (this.effect != null) {
            this.effect.prepareEffect(this);
        }
    }

    static int side2Orientation(String side) {
        int orientation = 2;
        if ("left".equals(side)) {
            orientation = 2;
        } else if ("right".equals(side)) {
            orientation = 1;
        } else if ("bottom".equals(side)) {
            orientation = 3;
        } else if ("top".equals(side)) {
            orientation = 4;
        }
        return orientation;
    }

    private void repaintLayeredPane() {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                JLayeredPane lp;
                Frame f = WindowManagerImpl.getInstance().getMainWindow();
                if (f instanceof JFrame && null != (lp = ((JFrame)f).getLayeredPane())) {
                    lp.invalidate();
                    lp.revalidate();
                    lp.repaint();
                }
            }
        });
    }

    private boolean isHeavyWeightShowing() {
        for (TopComponent tc : TopComponent.getRegistry().getOpened()) {
            if (!tc.isShowing() || !this.containsHeavyWeightChild((Container)tc)) continue;
            return true;
        }
        return false;
    }

    private boolean containsHeavyWeightChild(Container c) {
        if (!c.isLightweight()) {
            return true;
        }
        for (Component child : c.getComponents()) {
            if (null != child && !child.isLightweight()) {
                return true;
            }
            if (!(child instanceof Container) || !this.containsHeavyWeightChild((Container)child)) continue;
            return true;
        }
        return false;
    }

}

