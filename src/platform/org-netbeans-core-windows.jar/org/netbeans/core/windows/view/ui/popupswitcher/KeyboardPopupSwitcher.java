/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.Utilities
 *  org.openide.windows.Mode
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.windows.view.ui.popupswitcher;

import java.awt.AWTKeyStroke;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.io.PrintStream;
import java.lang.ref.WeakReference;
import java.util.Set;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JWindow;
import javax.swing.MenuSelectionManager;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.actions.RecentViewListAction;
import org.netbeans.core.windows.view.ui.popupswitcher.Model;
import org.netbeans.core.windows.view.ui.popupswitcher.PopupSwitcher;
import org.netbeans.core.windows.view.ui.popupswitcher.Table;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Utilities;
import org.openide.windows.Mode;
import org.openide.windows.WindowManager;

public final class KeyboardPopupSwitcher
implements WindowFocusListener {
    private static final int TIME_TO_SHOW = 200;
    private static KeyboardPopupSwitcher instance;
    private static JWindow popup;
    private static boolean shown;
    private static Timer invokerTimer;
    private static boolean invokerTimerRunning;
    private static int hits;
    private PopupSwitcher switcher;
    private Table table;
    private static int triggerKey;
    private static int reverseKey;
    private static int releaseKey;
    private static boolean defaultDocumentsOnly;
    private boolean fwd = true;
    private static final AWTKeyStroke CTRL_TAB;
    private static final AWTKeyStroke CTRL_SHIFT_TAB;
    private static WeakReference<Component> lastSource;

    public static boolean processShortcut(KeyEvent kev) {
        ModeImpl activeMode;
        WindowManagerImpl wmi = WindowManagerImpl.getInstance();
        if (!wmi.getMainWindow().isFocused() && !WindowManagerImpl.isSeparateWindow(KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusedWindow())) {
            return false;
        }
        if (Boolean.getBoolean("netbeans.winsys.ctrltab.editoronly") && !wmi.isEditorMode(activeMode = wmi.getActiveMode())) {
            return false;
        }
        return KeyboardPopupSwitcher.doProcessShortcut(kev);
    }

    static boolean doProcessShortcut(KeyEvent kev) {
        boolean isCtrlShiftTab;
        boolean isCtrlTab = kev.getKeyCode() == 9 && kev.getModifiers() == 2;
        boolean bl = isCtrlShiftTab = kev.getKeyCode() == 9 && kev.getModifiers() == 3;
        if (KeyboardPopupSwitcher.isShown()) {
            assert (instance != null);
            instance.processKeyEvent(kev);
            kev.consume();
            return true;
        }
        if (isCtrlTab || isCtrlShiftTab) {
            if (kev.getID() == 401) {
                lastSource = new WeakReference<Component>(kev.getComponent());
            }
            if (!Switches.isCtrlTabWindowSwitchingInJTableEnabled()) {
                JComponent jc;
                Component c = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
                if (c instanceof JEditorPane) {
                    if (Boolean.TRUE.equals(((JEditorPane)c).getClientProperty("nb.ctrltab.popupswitcher.disable"))) {
                        return false;
                    }
                } else if (c instanceof JComponent && !(c instanceof JEditorPane) && (jc = (JComponent)c).getFocusTraversalKeysEnabled()) {
                    Set<AWTKeyStroke> keys = jc.getFocusTraversalKeys(0);
                    if (keys.contains(CTRL_TAB) || keys.contains(CTRL_SHIFT_TAB)) {
                        return false;
                    }
                    keys = jc.getFocusTraversalKeys(0);
                    if (keys.contains(CTRL_TAB) || keys.contains(CTRL_SHIFT_TAB)) {
                        return false;
                    }
                }
            }
            if (!KeyboardPopupSwitcher.isAlive()) {
                Component currentSource;
                if (kev.getID() == 402 && null != (currentSource = kev.getComponent()) && null != lastSource && !currentSource.equals(lastSource.get())) {
                    return false;
                }
                Action rva = defaultDocumentsOnly ? RecentViewListAction.createDocumentsOnlyInstance() : new RecentViewListAction();
                rva.actionPerformed(new ActionEvent(kev.getSource(), 1001, "C-TAB", kev.getModifiers()));
                return true;
            }
            KeyboardPopupSwitcher.processInterruption(kev);
            kev.consume();
            return true;
        }
        if (kev.getKeyCode() == releaseKey && KeyboardPopupSwitcher.isAlive()) {
            KeyboardPopupSwitcher.processInterruption(kev);
            return true;
        }
        return false;
    }

    public static void showPopup(boolean documentsOnly, int releaseKey, int triggerKey, boolean forward) {
        if (invokerTimerRunning) {
            return;
        }
        KeyboardPopupSwitcher.releaseKey = releaseKey;
        KeyboardPopupSwitcher.triggerKey = triggerKey;
        invokerTimer = new Timer(200, new PopupInvoker(forward, documentsOnly));
        invokerTimer.setRepeats(false);
        invokerTimer.start();
        invokerTimerRunning = true;
    }

    static void showPopup(Model model, int releaseKey, int triggerKey, boolean forward) {
        KeyboardPopupSwitcher.releaseKey = releaseKey;
        KeyboardPopupSwitcher.triggerKey = triggerKey;
        instance = new KeyboardPopupSwitcher(model, forward);
        instance.showPopup();
        shown = true;
    }

    static void hidePopup() {
        KeyboardPopupSwitcher.cleanupInterrupter();
        if (null != instance) {
            instance.cancelSwitching();
        }
    }

    private static void cleanupInterrupter() {
        invokerTimerRunning = false;
        if (invokerTimer != null) {
            invokerTimer.stop();
        }
    }

    public static boolean isShown() {
        return shown;
    }

    private static boolean isAlive() {
        return invokerTimerRunning || shown;
    }

    private KeyboardPopupSwitcher(int hits, boolean forward, boolean documentsOnly) {
        this.fwd = forward;
        this.switcher = new PopupSwitcher(documentsOnly, hits, forward);
        this.table = this.switcher.getTable();
    }

    private KeyboardPopupSwitcher(Model model, boolean forward) {
        this.fwd = true;
        this.switcher = new PopupSwitcher(model, 0, forward);
        this.table = this.switcher.getTable();
    }

    private void showPopup() {
        if (!KeyboardPopupSwitcher.isShown()) {
            popup = new JWindow();
            popup.setAlwaysOnTop(true);
            popup.getContentPane().add(this.switcher);
            Dimension popupDim = this.switcher.getPreferredSize();
            Rectangle screen = Utilities.getUsableScreenBounds();
            int x = screen.x + (screen.width / 2 - popupDim.width / 2);
            int y = screen.y + (screen.height / 2 - popupDim.height / 2);
            popup.setLocation(x, y);
            popup.pack();
            MenuSelectionManager.defaultManager().addChangeListener(new ChangeListener(){

                @Override
                public void stateChanged(ChangeEvent e) {
                    MenuSelectionManager.defaultManager().removeChangeListener(this);
                    KeyboardPopupSwitcher.hidePopup();
                }
            });
            popup.setVisible(true);
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    WindowManager.getDefault().getMainWindow().addWindowFocusListener(KeyboardPopupSwitcher.this);
                }
            });
            shown = true;
        }
    }

    private static void processInterruption(KeyEvent kev) {
        int keyCode = kev.getKeyCode();
        if (keyCode == releaseKey && kev.getID() == 402) {
            KeyboardPopupSwitcher.cleanupInterrupter();
            hits = 0;
            RecentViewListAction rva = new RecentViewListAction();
            rva.actionPerformed(new ActionEvent(kev.getSource(), 1001, "immediately", kev.getModifiers()));
            kev.consume();
        } else if (keyCode == triggerKey && kev.getModifiers() == 2 && kev.getID() == 401) {
            kev.consume();
            KeyboardPopupSwitcher.cleanupInterrupter();
            if (null != instance) {
                instance.hideCurrentPopup();
            }
            System.err.println("showing popup with " + defaultDocumentsOnly);
            instance = new KeyboardPopupSwitcher(hits + 1, true, defaultDocumentsOnly);
            instance.showPopup();
        }
    }

    private void processKeyEvent(KeyEvent kev) {
        switch (kev.getID()) {
            case 401: {
                int code = kev.getKeyCode();
                if (code == reverseKey) {
                    this.fwd = false;
                } else if (code == triggerKey) {
                    if (this.fwd) {
                        this.table.nextRow();
                    } else {
                        this.table.previousRow();
                    }
                } else {
                    switch (code) {
                        case 38: {
                            this.table.previousRow();
                            break;
                        }
                        case 40: {
                            this.table.nextRow();
                            break;
                        }
                        case 37: {
                            this.table.previousColumn();
                            break;
                        }
                        case 39: {
                            this.table.nextColumn();
                        }
                    }
                }
                kev.consume();
                break;
            }
            case 402: {
                int code = kev.getKeyCode();
                if (code == reverseKey) {
                    this.fwd = true;
                    kev.consume();
                    break;
                }
                if (code == 27) {
                    this.cancelSwitching();
                    break;
                }
                if (code != releaseKey && code != 10) break;
                this.table.performSwitching();
                this.cancelSwitching();
            }
        }
    }

    private void cancelSwitching() {
        this.hideCurrentPopup();
        StatusDisplayer.getDefault().setStatusText("");
    }

    private synchronized void hideCurrentPopup() {
        if (popup != null) {
            SwingUtilities.invokeLater(new PopupHider(popup));
            popup = null;
        }
    }

    @Override
    public void windowGainedFocus(WindowEvent e) {
    }

    @Override
    public void windowLostFocus(WindowEvent e) {
        if (e.getOppositeWindow() != popup) {
            this.cancelSwitching();
        }
    }

    static {
        reverseKey = 16;
        defaultDocumentsOnly = Boolean.getBoolean("netbeans.winsys.ctrltab.documentsonly");
        CTRL_TAB = AWTKeyStroke.getAWTKeyStroke(9, 128);
        CTRL_SHIFT_TAB = AWTKeyStroke.getAWTKeyStroke(9, 192);
    }

    private class PopupHider
    implements Runnable {
        private JWindow toHide;

        public PopupHider(JWindow popup) {
            this.toHide = popup;
        }

        @Override
        public void run() {
            this.toHide.setAlwaysOnTop(false);
            this.toHide.setVisible(false);
            this.toHide.dispose();
            shown = false;
            hits = 0;
            WindowManager.getDefault().getMainWindow().removeWindowFocusListener(KeyboardPopupSwitcher.this);
        }
    }

    private static class PopupInvoker
    implements ActionListener {
        private boolean forward;
        private boolean documentsOnly;

        public PopupInvoker(boolean forward, boolean documentsOnly) {
            this.forward = forward;
            this.documentsOnly = documentsOnly;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (invokerTimerRunning) {
                KeyboardPopupSwitcher.cleanupInterrupter();
                if (null != instance) {
                    instance.hideCurrentPopup();
                }
                instance = new KeyboardPopupSwitcher(hits, this.forward, this.documentsOnly);
                instance.showPopup();
            }
        }
    }

}

