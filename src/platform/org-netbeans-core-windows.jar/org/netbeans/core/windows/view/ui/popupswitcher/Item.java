/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$SubComponent
 */
package org.netbeans.core.windows.view.ui.popupswitcher;

import java.awt.Image;
import javax.swing.Icon;
import org.openide.util.ImageUtilities;
import org.openide.windows.TopComponent;

abstract class Item {
    private final String displayName;
    private final String description;
    private final Icon icon;
    private final boolean active;

    protected Item(String displayName, String description, Icon icon, boolean active) {
        this.displayName = displayName;
        this.description = description;
        this.icon = icon;
        this.active = active;
    }

    public static Item create(TopComponent tc) {
        return new TopItem(tc);
    }

    public abstract void activate();

    public final String toString() {
        return this.displayName;
    }

    public final String getDescription() {
        return this.description;
    }

    public final String getDisplayName() {
        return this.displayName;
    }

    public final Icon getIcon() {
        return this.icon;
    }

    public abstract boolean hasSubItems();

    public abstract Item[] getActivatableSubItems();

    public abstract Item getActiveSubItem();

    public final boolean isActive() {
        return this.active;
    }

    public abstract boolean isTopItem();

    public abstract boolean isParentOf(Item var1);

    private static String extractDisplayName(TopComponent tc) {
        String name = tc.getShortName();
        if (name == null || name.isEmpty()) {
            name = tc.getHtmlDisplayName();
        }
        if (name == null || name.isEmpty()) {
            name = tc.getDisplayName();
        }
        if (name == null || name.isEmpty()) {
            name = tc.getName();
        }
        return name;
    }

    private static Icon extractIcon(TopComponent tc) {
        Image img = tc.getIcon();
        if (null != img) {
            return ImageUtilities.image2Icon((Image)img);
        }
        return null;
    }

    private static class SubItem
    extends Item {
        private final TopComponent.SubComponent subComponent;
        private final TopComponent parent;

        public SubItem(TopComponent parent, TopComponent.SubComponent subComponent) {
            super(subComponent.getDisplayName(), subComponent.getDescription(), null, subComponent.isActive());
            this.subComponent = subComponent;
            this.parent = parent;
        }

        @Override
        public void activate() {
            this.parent.requestActive();
            this.subComponent.activate();
        }

        @Override
        public boolean hasSubItems() {
            return false;
        }

        @Override
        public boolean isTopItem() {
            return false;
        }

        @Override
        public Item[] getActivatableSubItems() {
            return null;
        }

        @Override
        public Item getActiveSubItem() {
            return null;
        }

        @Override
        public boolean isParentOf(Item subItem) {
            return false;
        }
    }

    private static class TopItem
    extends Item {
        private final TopComponent tc;
        private final Item[] subItems;
        private final Item activeSubItem;

        public TopItem(TopComponent tc) {
            super(Item.extractDisplayName(tc), tc.getToolTipText(), Item.extractIcon(tc), false);
            this.tc = tc;
            Item[] subItems = null;
            SubItem activeSubItem = null;
            TopComponent.SubComponent[] subs = tc.getSubComponents();
            if (null != subs && subs.length > 0) {
                for (TopComponent.SubComponent sc : subs) {
                    if (!sc.isActive()) continue;
                    activeSubItem = new SubItem(tc, sc);
                    break;
                }
                subItems = new Item[null == activeSubItem ? subs.length : subs.length - 1];
                int index = 0;
                for (TopComponent.SubComponent sc2 : subs) {
                    if (sc2.isActive()) continue;
                    subItems[index++] = new SubItem(tc, sc2);
                }
            }
            this.subItems = subItems;
            this.activeSubItem = activeSubItem;
        }

        @Override
        public void activate() {
            this.tc.requestActive();
        }

        @Override
        public boolean hasSubItems() {
            return null != this.subItems;
        }

        @Override
        public Item[] getActivatableSubItems() {
            return this.subItems;
        }

        @Override
        public boolean isTopItem() {
            return true;
        }

        @Override
        public Item getActiveSubItem() {
            return this.activeSubItem;
        }

        @Override
        public boolean isParentOf(Item child) {
            if (null != this.subItems) {
                for (Item sub : this.subItems) {
                    if (sub != child) continue;
                    return true;
                }
            }
            return null != child && child == this.activeSubItem;
        }
    }

}

