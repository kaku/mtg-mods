/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.DefaultTabDataModel
 *  org.netbeans.swing.tabcontrol.SlideBarDataModel
 *  org.netbeans.swing.tabcontrol.SlideBarDataModel$Impl
 *  org.netbeans.swing.tabcontrol.TabData
 *  org.netbeans.swing.tabcontrol.TabDataModel
 *  org.netbeans.swing.tabcontrol.TabbedContainer
 *  org.netbeans.swing.tabcontrol.customtabs.Tabbed
 *  org.openide.util.ChangeSupport
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view.ui.slides;

import java.awt.Component;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.Action;
import javax.swing.DefaultSingleSelectionModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.SingleSelectionModel;
import javax.swing.event.ChangeListener;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.actions.ActionUtils;
import org.netbeans.core.windows.view.dnd.DragAndDropFeedbackVisualizer;
import org.netbeans.core.windows.view.ui.slides.SlideBar;
import org.netbeans.core.windows.view.ui.slides.SlideController;
import org.netbeans.swing.tabcontrol.DefaultTabDataModel;
import org.netbeans.swing.tabcontrol.SlideBarDataModel;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabbedContainer;
import org.netbeans.swing.tabcontrol.customtabs.Tabbed;
import org.openide.util.ChangeSupport;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;

public final class TabbedSlideAdapter
extends Tabbed {
    private TabDataModel dataModel;
    private SingleSelectionModel selModel;
    private SlideBar slideBar;
    private List<ActionListener> actionListeners;
    private final ChangeSupport cs;
    private final ModeImpl slidingMode;

    public TabbedSlideAdapter(String side) {
        this.cs = new ChangeSupport((Object)this);
        this.dataModel = new SlideBarDataModel.Impl();
        this.setSide(side);
        this.selModel = new DefaultSingleSelectionModel();
        this.slideBar = new SlideBar(this, (SlideBarDataModel)this.dataModel, this.selModel);
        this.slidingMode = this.findSlidingMode();
    }

    public void requestAttention(TopComponent tc) {
        this.slideBar.setBlinking(tc, true);
    }

    public void cancelRequestAttention(TopComponent tc) {
        this.slideBar.setBlinking(tc, false);
    }

    public void makeBusy(TopComponent tc, boolean busy) {
        this.slideBar.makeBusy(tc, busy);
    }

    public boolean isBusy(TopComponent tc) {
        return WindowManagerImpl.getInstance().isTopComponentBusy(tc);
    }

    private void setSide(String side) {
        int orientation = 2;
        if ("left".equals(side)) {
            orientation = 2;
        } else if ("right".equals(side)) {
            orientation = 1;
        } else if ("bottom".equals(side)) {
            orientation = 3;
        } else if ("top".equals(side)) {
            orientation = 4;
        }
        ((SlideBarDataModel)this.dataModel).setOrientation(orientation);
    }

    public final synchronized void addActionListener(ActionListener listener) {
        if (this.actionListeners == null) {
            this.actionListeners = new ArrayList<ActionListener>();
        }
        this.actionListeners.add(listener);
    }

    public final synchronized void removeActionListener(ActionListener listener) {
        if (this.actionListeners != null) {
            this.actionListeners.remove(listener);
            if (this.actionListeners.isEmpty()) {
                this.actionListeners = null;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void postActionEvent(ActionEvent event) {
        List<ActionListener> list;
        TabbedSlideAdapter tabbedSlideAdapter = this;
        synchronized (tabbedSlideAdapter) {
            if (this.actionListeners == null) {
                return;
            }
            list = Collections.unmodifiableList(this.actionListeners);
        }
        for (int i = 0; i < list.size(); ++i) {
            list.get(i).actionPerformed(event);
        }
    }

    public void addChangeListener(ChangeListener listener) {
        this.cs.addChangeListener(listener);
    }

    public void removeChangeListener(ChangeListener listener) {
        this.cs.removeChangeListener(listener);
    }

    final void postSelectionEvent() {
        this.cs.fireChange();
    }

    public void addPropertyChangeListener(String name, PropertyChangeListener listener) {
        this.slideBar.addPropertyChangeListener(name, listener);
    }

    public void removePropertyChangeListener(String name, PropertyChangeListener listener) {
        this.slideBar.removePropertyChangeListener(name, listener);
    }

    public void addTopComponent(String name, Icon icon, TopComponent tc, String toolTip) {
        this.dataModel.addTab(this.dataModel.size(), new TabData((Object)tc, icon, name, toolTip));
    }

    public TopComponent getSelectedTopComponent() {
        int index = this.selModel.getSelectedIndex();
        return index < 0 ? null : (TopComponent)this.dataModel.getTab(index).getComponent();
    }

    public TopComponent getTopComponentAt(int index) {
        return (TopComponent)this.dataModel.getTab(index).getComponent();
    }

    public TopComponent[] getTopComponents() {
        int size = this.dataModel.size();
        TopComponent[] result = new TopComponent[size];
        for (int i = 0; i < size; ++i) {
            result[i] = (TopComponent)this.dataModel.getTab(i).getComponent();
        }
        return result;
    }

    public void setActive(boolean active) {
        this.slideBar.setActive(active);
    }

    public void setIconAt(int index, Icon icon) {
        this.dataModel.setIcon(index, icon);
    }

    public void setTitleAt(int index, String title) {
        this.dataModel.setText(index, title);
    }

    public void setToolTipTextAt(int index, String toolTip) {
    }

    public void setTopComponents(TopComponent[] tcs, TopComponent selected) {
        TabData[] data = new TabData[tcs.length];
        int toSelect = -1;
        for (int i = 0; i < tcs.length; ++i) {
            TopComponent tc = tcs[i];
            Image icon = tc.getIcon();
            String displayName = WindowManagerImpl.getInstance().getTopComponentDisplayName(tc);
            data[i] = new TabData((Object)tc, (Icon)(icon == null ? null : new ImageIcon(icon)), displayName == null ? "" : displayName, tc.getToolTipText());
            if (selected != tcs[i]) continue;
            toSelect = i;
        }
        this.dataModel.setTabs(data);
        this.setSelectedComponent((Component)selected);
    }

    public int getTabCount() {
        return this.dataModel.size();
    }

    public int indexOf(Component tc) {
        int size = this.dataModel.size();
        for (int i = 0; i < size; ++i) {
            if (tc != this.dataModel.getTab(i).getComponent()) continue;
            return i;
        }
        return -1;
    }

    public void insertComponent(String name, Icon icon, Component comp, String toolTip, int position) {
        this.dataModel.addTab(position, new TabData((Object)comp, icon, name, toolTip));
    }

    public void removeComponent(Component comp) {
        int i = this.indexOf(comp);
        this.dataModel.removeTab(i);
    }

    public void setSelectedComponent(Component comp) {
        int newIndex = this.indexOf(comp);
        if (this.selModel.getSelectedIndex() != newIndex) {
            this.selModel.setSelectedIndex(newIndex);
        }
        if (comp instanceof TopComponent) {
            TopComponent tc = (TopComponent)comp;
            tc.cancelRequestAttention();
        }
    }

    public int tabForCoordinate(Point p) {
        return this.slideBar.tabForCoordinate(p.x, p.y);
    }

    public Component getComponent() {
        return this.slideBar;
    }

    public Object getConstraintForLocation(Point location, boolean attachingPossible) {
        int tab = this.slideBar.nextTabForCoordinate(location.x, location.y);
        return tab;
    }

    public Shape getIndicationForLocation(Point location, TopComponent startingTransfer, Point startingPoint, boolean attachingPossible) {
        int nextTab = this.slideBar.nextTabForCoordinate(location.x, location.y);
        SlideBarDataModel sbdm = (SlideBarDataModel)this.dataModel;
        if (this.getTabCount() != 0) {
            if (nextTab == 0) {
                Rectangle rect = this.getTabBounds(0);
                if (this.isHorizontal()) {
                    rect.x = 0;
                    rect.width /= 2;
                } else {
                    rect.y = 0;
                    rect.height /= 2;
                }
                return rect;
            }
            if (nextTab < this.getTabCount()) {
                Rectangle rect1 = this.getTabBounds(nextTab - 1);
                Rectangle rect2 = this.getTabBounds(nextTab);
                Rectangle result = new Rectangle();
                if (this.isHorizontal()) {
                    result.y = rect1.y;
                    result.height = rect1.height;
                    result.x = rect1.x + rect1.width / 2;
                    result.width = rect2.x + rect2.width / 2 - result.x;
                } else {
                    result.x = rect1.x;
                    result.width = rect1.width;
                    result.y = rect1.y + rect1.height / 2;
                    result.height = rect2.y + rect2.height / 2 - result.y;
                }
                return result;
            }
            if (nextTab == this.getTabCount()) {
                Rectangle rect = this.getTabBounds(this.getTabCount() - 1);
                if (this.isHorizontal()) {
                    rect.x += rect.width;
                } else {
                    rect.y += rect.height;
                }
                return rect;
            }
        }
        if (this.isHorizontal()) {
            return new Rectangle(10, 0, 50, 20);
        }
        return new Rectangle(0, 10, 20, 50);
    }

    public Image createImageOfTab(int tabIndex) {
        TabData dt = this.slideBar.getModel().getTab(tabIndex);
        if (dt.getComponent() instanceof TopComponent) {
            DefaultTabDataModel tempModel = new DefaultTabDataModel(new TabData[]{dt});
            TabbedContainer temp = new TabbedContainer((TabDataModel)tempModel, 0);
            temp.setSize(300, 300);
            return temp.createImageOfTab(0);
        }
        return null;
    }

    public DragAndDropFeedbackVisualizer getDragAndDropFeedbackVisualizer(int tabIndex) {
        this.slideBar.getSelectionModel().setSelectedIndex(tabIndex);
        return new DragAndDropFeedbackVisualizer(this, tabIndex);
    }

    public Action[] getPopupActions(Action[] defaultActions, int tabIndex) {
        boolean isMDI = WindowManagerImpl.getInstance().getEditorAreaState() == 0;
        Action[] result = new Action[defaultActions.length + (isMDI ? 1 : 0)];
        System.arraycopy(defaultActions, 0, result, 0, defaultActions.length);
        if (isMDI) {
            result[defaultActions.length] = new ActionUtils.ToggleWindowTransparencyAction(this.slideBar, tabIndex, this.slideBar.isSlidedTabTransparent() && tabIndex == this.slideBar.getSelectionModel().getSelectedIndex());
        }
        return result;
    }

    public Rectangle getTabBounds(int tabIndex) {
        return this.slideBar.getTabBounds(tabIndex);
    }

    public boolean isTransparent() {
        return false;
    }

    public void setTransparent(boolean transparent) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Rectangle getTabsArea() {
        Rectangle res = this.slideBar.getBounds();
        res.setLocation(0, 0);
        return res;
    }

    final ModeImpl getSlidingMode() {
        return this.slidingMode;
    }

    private ModeImpl findSlidingMode() {
        String modeName;
        switch (((SlideBarDataModel)this.dataModel).getOrientation()) {
            case 1: {
                modeName = "rightSlidingSide";
                break;
            }
            case 3: {
                modeName = "bottomSlidingSide";
                break;
            }
            case 4: {
                modeName = "topSlidingSide";
                break;
            }
            default: {
                modeName = "leftSlidingSide";
            }
        }
        return (ModeImpl)WindowManagerImpl.getInstance().findMode(modeName);
    }

    final boolean isHorizontal() {
        return ((SlideBarDataModel)this.dataModel).getOrientation() == 3 || ((SlideBarDataModel)this.dataModel).getOrientation() == 4;
    }
}

