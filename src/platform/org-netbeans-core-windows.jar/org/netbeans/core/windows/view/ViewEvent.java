/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view;

public class ViewEvent {
    private final Object source;
    private final int type;
    private final Object oldValue;
    private final Object newValue;

    public ViewEvent(Object source, int type, Object oldValue, Object newValue) {
        this.source = source;
        this.type = type;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    public Object getSource() {
        return this.source;
    }

    public int getType() {
        return this.type;
    }

    public Object getOldValue() {
        return this.oldValue;
    }

    public Object getNewValue() {
        return this.newValue;
    }

    public String toString() {
        StringBuffer buf = new StringBuffer(100);
        buf.append("ViewEvent:");
        String typeStr = "Unknown";
        switch (this.type) {
            case 9: {
                typeStr = "CHANGE_ACTIVE_MODE_CHANGED";
                break;
            }
            case 46: {
                typeStr = "CHANGE_DND_PERFORMED";
                break;
            }
            case 7: {
                typeStr = "CHANGE_EDITOR_AREA_BOUNDS_CHANGED";
                break;
            }
            case 8: {
                typeStr = ".CHANGE_EDITOR_AREA_CONSTRAINTS_CHANGED";
                break;
            }
            case 6: {
                typeStr = "CHANGE_EDITOR_AREA_FRAME_STATE_CHANGED";
                break;
            }
            case 5: {
                typeStr = "CHANGE_EDITOR_AREA_STATE_CHANGED";
                break;
            }
            case 1: {
                typeStr = "CHANGE_MAIN_WINDOW_BOUNDS_JOINED_CHANGED";
                break;
            }
            case 2: {
                typeStr = "CHANGE_MAIN_WINDOW_BOUNDS_SEPARATED_CHANGED";
                break;
            }
            case 3: {
                typeStr = "CHANGE_MAIN_WINDOW_FRAME_STATE_JOINED_CHANGED";
                break;
            }
            case 4: {
                typeStr = "CHANGE_MAIN_WINDOW_FRAME_STATE_SEPARATED_CHANGED";
                break;
            }
            case 11: {
                typeStr = "CHANGE_MAXIMIZED_MODE_CHANGED";
                break;
            }
            case 12: {
                typeStr = "CHANGE_MODE_ADDED";
                break;
            }
            case 45: {
                typeStr = "CHANGE_MODE_CLOSED";
                break;
            }
            case 20: {
                typeStr = "CHANGE_MODE_BOUNDS_CHANGED";
                break;
            }
            case 14: {
                typeStr = "CHANGE_MODE_CONSTRAINTS_CHANGED";
                break;
            }
            case 21: {
                typeStr = "CHANGE_MODE_FRAME_STATE_CHANGED";
                break;
            }
            case 13: {
                typeStr = "CHANGE_MODE_REMOVED";
                break;
            }
            case 22: {
                typeStr = "CHANGE_MODE_SELECTED_TOPCOMPONENT_CHANGED";
                break;
            }
            case 23: {
                typeStr = "CHANGE_MODE_TOPCOMPONENT_ADDED";
                break;
            }
            case 24: {
                typeStr = "CHANGE_MODE_TOPCOMPONENT_REMOVED";
                break;
            }
            case 10: {
                typeStr = "CHANGE_TOOLBAR_CONFIGURATION_CHANGED";
                break;
            }
            case 44: {
                typeStr = "CHANGE_TOPCOMPONENT_ACTIVATED";
                break;
            }
            case 42: {
                typeStr = "CHANGE_TOPCOMPONENT_ARRAY_ADDED";
                break;
            }
            case 43: {
                typeStr = "CHANGE_TOPCOMPONENT_ARRAY_REMOVED";
                break;
            }
            case 41: {
                typeStr = "CHANGE_TOPCOMPONENT_ATTACHED";
                break;
            }
            case 32: {
                typeStr = "CHANGE_TOPCOMPONENT_DISPLAY_NAME_ANNOTATION_CHANGED";
                break;
            }
            case 31: {
                typeStr = "CHANGE_TOPCOMPONENT_DISPLAY_NAME_CHANGED";
                break;
            }
            case 34: {
                typeStr = "CHANGE_TOPCOMPONENT_ICON_CHANGED";
                break;
            }
            case 33: {
                typeStr = "CHANGE_TOPCOMPONENT_TOOLTIP_CHANGED";
                break;
            }
            case 61: {
                typeStr = "CHANGE_UI_UPDATE";
                break;
            }
            case 0: {
                typeStr = "CHANGE_VISIBILITY_CHANGED";
                break;
            }
            case 63: {
                typeStr = "TOPCOMPONENT_REQUEST_ATTENTION";
                break;
            }
            case 64: {
                typeStr = "TOPCOMPONENT_CANCEL_REQUEST_ATTENTION";
                break;
            }
            case 66: {
                typeStr = "TOPCOMPONENT_ATTENTION_HIGHLIGHT_OFF";
                break;
            }
            case 65: {
                typeStr = "TOPCOMPONENT_ATTENTION_HIGHLIGHT_ON";
                break;
            }
            case 70: {
                typeStr = "TOPCOMPONENT_SHOW_BUSY";
                break;
            }
            case 71: {
                typeStr = "TOPCOMPONENT_HIDE_BUSY";
            }
        }
        buf.append(typeStr);
        buf.append("\nnewValue=");
        buf.append(this.newValue);
        buf.append("\noldValue=");
        buf.append(this.oldValue);
        return buf.toString();
    }
}

