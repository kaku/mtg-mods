/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view.ui.slides;

import javax.swing.JLayeredPane;
import javax.swing.event.ChangeListener;
import org.netbeans.core.windows.view.ui.slides.SlideOperation;
import org.netbeans.core.windows.view.ui.slides.SlidingFx;

public class DefaultSlidingFx
implements SlidingFx {
    @Override
    public void showEffect(JLayeredPane pane, Integer layer, SlideOperation operation) {
    }

    @Override
    public void prepareEffect(SlideOperation operation) {
    }

    @Override
    public void setFinishListener(ChangeListener finishL) {
    }

    @Override
    public boolean shouldOperationWait() {
        return false;
    }
}

