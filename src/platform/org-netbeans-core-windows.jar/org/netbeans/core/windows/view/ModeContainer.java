/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.view;

import java.awt.Component;
import org.netbeans.core.windows.view.ModeView;
import org.openide.windows.TopComponent;

public interface ModeContainer {
    public ModeView getModeView();

    public Component getComponent();

    public void addTopComponent(TopComponent var1);

    public void removeTopComponent(TopComponent var1);

    public void setSelectedTopComponent(TopComponent var1);

    public void setTopComponents(TopComponent[] var1, TopComponent var2);

    public TopComponent getSelectedTopComponent();

    public void setActive(boolean var1);

    public boolean isActive();

    public void focusSelectedTopComponent();

    public TopComponent[] getTopComponents();

    public void updateName(TopComponent var1);

    public void updateToolTip(TopComponent var1);

    public void updateIcon(TopComponent var1);

    public void requestAttention(TopComponent var1);

    public void cancelRequestAttention(TopComponent var1);

    public void setAttentionHighlight(TopComponent var1, boolean var2);

    public void makeBusy(TopComponent var1, boolean var2);
}

