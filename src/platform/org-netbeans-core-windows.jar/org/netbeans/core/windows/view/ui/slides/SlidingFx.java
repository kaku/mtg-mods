/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view.ui.slides;

import javax.swing.JLayeredPane;
import javax.swing.event.ChangeListener;
import org.netbeans.core.windows.view.ui.slides.SlideOperation;

public interface SlidingFx {
    public void prepareEffect(SlideOperation var1);

    public void showEffect(JLayeredPane var1, Integer var2, SlideOperation var3);

    public boolean shouldOperationWait();

    public void setFinishListener(ChangeListener var1);
}

