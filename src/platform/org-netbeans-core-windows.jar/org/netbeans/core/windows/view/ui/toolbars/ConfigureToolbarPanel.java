/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.Actions
 *  org.openide.awt.Mnemonics
 *  org.openide.awt.ToolbarPool
 *  org.openide.cookies.InstanceCookie
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFilter
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.FilterNode
 *  org.openide.nodes.FilterNode$Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.actions.CallbackSystemAction
 *  org.openide.util.datatransfer.ExTransferable
 *  org.openide.util.datatransfer.ExTransferable$Single
 */
package org.netbeans.core.windows.view.ui.toolbars;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import org.netbeans.core.windows.view.ui.toolbars.ActionsTree;
import org.netbeans.core.windows.view.ui.toolbars.ResetToolbarsAction;
import org.netbeans.core.windows.view.ui.toolbars.ToolbarConfiguration;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Actions;
import org.openide.awt.Mnemonics;
import org.openide.awt.ToolbarPool;
import org.openide.cookies.InstanceCookie;
import org.openide.explorer.ExplorerManager;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFilter;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.actions.CallbackSystemAction;
import org.openide.util.datatransfer.ExTransferable;

public class ConfigureToolbarPanel
extends JPanel
implements Runnable,
ExplorerManager.Provider {
    private static final Logger LOG = Logger.getLogger(ConfigureToolbarPanel.class.getName());
    private static WeakReference<Dialog> dialogRef;
    private Node root;
    private final ExplorerManager explorerManager = new ExplorerManager();
    private boolean firstTimeInit = true;
    private JButton btnNewToolbar;
    private JButton btnReset;
    private JCheckBox checkSmallIcons;
    private JLabel lblHint;
    private JPanel palettePanel;

    private ConfigureToolbarPanel() {
        this.initComponents();
        if (this.checkSmallIcons.getText().isEmpty()) {
            this.checkSmallIcons.setVisible(false);
        }
        this.setCursor(Cursor.getPredefinedCursor(3));
        FileObject paletteFolder = FileUtil.getConfigFile((String)"Actions");
        DataFolder df = DataFolder.findFolder((FileObject)paletteFolder);
        this.root = ConfigureToolbarPanel.createFolderActionNode(df);
        JLabel lblWait = new JLabel(ConfigureToolbarPanel.getBundleString("LBL_PleaseWait"));
        lblWait.setHorizontalAlignment(0);
        this.palettePanel.setPreferredSize(new Dimension(440, 350));
        this.palettePanel.add(lblWait);
        this.getAccessibleContext().setAccessibleDescription(ConfigureToolbarPanel.getBundleString("ACSD_ToolbarCustomizer"));
    }

    static FolderActionNode createFolderActionNode(DataFolder df) {
        return new FolderActionNode((Node)new AbstractNode(df.createNodeChildren((DataFilter)new ActionIconDataFilter())));
    }

    @Override
    public void run() {
        ActionsTree tree = new ActionsTree();
        tree.getAccessibleContext().setAccessibleDescription(ConfigureToolbarPanel.getBundleString("ACSD_ActionsTree"));
        tree.getAccessibleContext().setAccessibleName(ConfigureToolbarPanel.getBundleString("ACSN_ActionsTree"));
        this.palettePanel.removeAll();
        this.palettePanel.setBorder(BorderFactory.createEtchedBorder());
        this.palettePanel.add((Component)((Object)tree), "Center");
        this.lblHint.setLabelFor((Component)((Object)tree));
        this.invalidate();
        this.validate();
        this.repaint();
        this.setCursor(Cursor.getDefaultCursor());
        this.explorerManager.setRootContext(this.root);
        tree.expandAll();
    }

    public static void showConfigureDialog() {
        Dialog dialog = null;
        if (dialogRef != null) {
            dialog = dialogRef.get();
        }
        if (dialog == null) {
            JButton closeButton = new JButton();
            closeButton.getAccessibleContext().setAccessibleDescription(ConfigureToolbarPanel.getBundleString("ACSD_Close"));
            Mnemonics.setLocalizedText((AbstractButton)closeButton, (String)ConfigureToolbarPanel.getBundleString("CTL_Close"));
            DialogDescriptor dd = new DialogDescriptor((Object)new ConfigureToolbarPanel(), ConfigureToolbarPanel.getBundleString("CustomizerTitle"), false, new Object[]{closeButton}, (Object)closeButton, 0, new HelpCtx(ConfigureToolbarPanel.class), null);
            dialog = DialogDisplayer.getDefault().createDialog(dd);
            dialogRef = new WeakReference<Dialog>(dialog);
        }
        dialog.setVisible(true);
    }

    static final String getBundleString(String bundleStr) {
        return NbBundle.getMessage(ConfigureToolbarPanel.class, (String)bundleStr);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (this.firstTimeInit) {
            this.firstTimeInit = false;
            new RequestProcessor("ToolbarPanelConfigWarmUp").post(new Runnable(){

                @Override
                public void run() {
                    Node[] categories = ConfigureToolbarPanel.this.root.getChildren().getNodes(true);
                    for (int i = 0; i < categories.length; ++i) {
                        final Node category = categories[i];
                        SwingUtilities.invokeLater(new Runnable(){

                            @Override
                            public void run() {
                                category.getChildren().getNodes(true);
                            }
                        });
                    }
                    SwingUtilities.invokeLater(ConfigureToolbarPanel.this);
                }

            });
        }
    }

    private void initComponents() {
        this.lblHint = new JLabel();
        this.palettePanel = new JPanel();
        this.checkSmallIcons = new JCheckBox();
        this.btnNewToolbar = new JButton();
        this.btnReset = new JButton();
        FormListener formListener = new FormListener();
        this.setLayout(new GridBagLayout());
        this.setMinimumSize(new Dimension(453, 68));
        Mnemonics.setLocalizedText((JLabel)this.lblHint, (String)ConfigureToolbarPanel.getBundleString("CTL_TreeLabel"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 10, 1, 10);
        this.add((Component)this.lblHint, gridBagConstraints);
        this.palettePanel.setLayout(new BorderLayout());
        this.palettePanel.setBorder(BorderFactory.createEtchedBorder());
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(1, 10, 5, 10);
        this.add((Component)this.palettePanel, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.checkSmallIcons, (String)ConfigureToolbarPanel.getBundleString("CTL_SmallIcons"));
        this.checkSmallIcons.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.checkSmallIcons.setMargin(new Insets(0, 0, 0, 0));
        this.checkSmallIcons.setSelected(ToolbarPool.getDefault().getPreferredIconSize() == 16);
        this.checkSmallIcons.addActionListener(formListener);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(10, 10, 0, 10);
        this.add((Component)this.checkSmallIcons, gridBagConstraints);
        this.checkSmallIcons.getAccessibleContext().setAccessibleDescription(ConfigureToolbarPanel.getBundleString("ACSD_SmallIcons"));
        Mnemonics.setLocalizedText((AbstractButton)this.btnNewToolbar, (String)ConfigureToolbarPanel.getBundleString("CTL_NewToolbar"));
        this.btnNewToolbar.addActionListener(formListener);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(10, 10, 0, 10);
        this.add((Component)this.btnNewToolbar, gridBagConstraints);
        this.btnNewToolbar.getAccessibleContext().setAccessibleDescription(ConfigureToolbarPanel.getBundleString("ACSD_NewToolbar"));
        Mnemonics.setLocalizedText((AbstractButton)this.btnReset, (String)ConfigureToolbarPanel.getBundleString("CTL_ResetToolbarsButton"));
        this.btnReset.addActionListener(formListener);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(10, 10, 0, 10);
        this.add((Component)this.btnReset, gridBagConstraints);
    }

    private void resetToolbars(ActionEvent evt) {
        new ResetToolbarsAction().actionPerformed(evt);
    }

    private void newToolbar(ActionEvent evt) {
        JPanel panel = new JPanel(new BorderLayout(5, 5));
        JLabel lbl = new JLabel();
        Mnemonics.setLocalizedText((JLabel)lbl, (String)NbBundle.getMessage(ConfigureToolbarPanel.class, (String)"PROP_newToolbarLabel"));
        panel.add((Component)lbl, "West");
        JTextField inputField = new JTextField(NbBundle.getMessage(ConfigureToolbarPanel.class, (String)"PROP_newToolbar"));
        inputField.setColumns(25);
        panel.add((Component)inputField, "Center");
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        DialogDescriptor dd = new DialogDescriptor((Object)panel, NbBundle.getMessage(ConfigureToolbarPanel.class, (String)"PROP_newToolbarDialog"), true, 2, (Object)null, 0, new HelpCtx(ConfigureToolbarPanel.class), null);
        Dialog dlg = DialogDisplayer.getDefault().createDialog(dd);
        dlg.setVisible(true);
        if (dd.getValue() != NotifyDescriptor.OK_OPTION) {
            return;
        }
        String s = inputField.getText().trim();
        if (s.length() == 0) {
            return;
        }
        DataFolder folder = ToolbarPool.getDefault().getFolder();
        FileObject toolbars = folder.getPrimaryFile();
        try {
            FileObject newToolbar = toolbars.getFileObject(s);
            if (newToolbar == null) {
                DataObject[] oldKids = folder.getChildren();
                newToolbar = toolbars.createFolder(s);
                DataObject[] newKids = new DataObject[oldKids.length + 1];
                System.arraycopy(oldKids, 0, newKids, 0, oldKids.length);
                newKids[oldKids.length] = DataObject.find((FileObject)newToolbar);
                folder.setOrder(newKids);
                ToolbarPool.getDefault().waitFinished();
                ToolbarConfiguration.findConfiguration(ToolbarPool.getDefault().getConfiguration()).repaint();
            } else {
                NotifyDescriptor.Message msg = new NotifyDescriptor.Message((Object)NbBundle.getMessage(ConfigureToolbarPanel.class, (String)"MSG_ToolbarExists", (Object)s));
                DialogDisplayer.getDefault().notify((NotifyDescriptor)msg);
            }
        }
        catch (IOException e) {
            Exceptions.printStackTrace((Throwable)e);
        }
    }

    private void switchIconSize(ActionEvent evt) {
        boolean state = this.checkSmallIcons.isSelected();
        if (state) {
            ToolbarPool.getDefault().setPreferredIconSize(16);
        } else {
            ToolbarPool.getDefault().setPreferredIconSize(24);
        }
        String name = ToolbarPool.getDefault().getConfiguration();
        ToolbarConfiguration tbConf = ToolbarConfiguration.findConfiguration(name);
        if (tbConf != null) {
            tbConf.refresh();
        }
    }

    @Override
    public void addNotify() {
        super.addNotify();
        ToolbarPool pool = ToolbarPool.getDefault();
        this.checkSmallIcons.setSelected(pool.getPreferredIconSize() == 16);
        ToolbarConfiguration tc = ToolbarConfiguration.findConfiguration(pool.getConfiguration());
        if (null != tc) {
            tc.setToolbarButtonDragAndDropAllowed(true);
        }
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        ToolbarPool pool = ToolbarPool.getDefault();
        final ToolbarConfiguration tc = ToolbarConfiguration.findConfiguration(pool.getConfiguration());
        if (null != tc) {
            tc.setToolbarButtonDragAndDropAllowed(false);
        }
        DataFolder folder = pool.getFolder();
        DataObject[] children = folder.getChildren();
        for (int i = 0; i < children.length; ++i) {
            final DataFolder subFolder = (DataFolder)children[i].getCookie(DataFolder.class);
            if (null == subFolder || subFolder.getChildren().length != 0) continue;
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    try {
                        subFolder.delete();
                        ToolbarPool.getDefault().waitFinished();
                        if (null != tc) {
                            tc.removeEmptyRows();
                            tc.save();
                        }
                    }
                    catch (IOException e) {
                        LOG.log(Level.WARNING, null, e);
                    }
                }
            });
        }
    }

    public ExplorerManager getExplorerManager() {
        return this.explorerManager;
    }

    private static class ActionIconDataFilter
    implements DataFilter {
        private InstanceCookie instanceCookie;

        private ActionIconDataFilter() {
        }

        public boolean acceptDataObject(DataObject obj) {
            boolean a = this.doAcceptDataObject(obj);
            Object[] arrobject = new Object[2];
            arrobject[0] = Character.valueOf(a ? '+' : '-');
            arrobject[1] = obj.getPrimaryFile().getPath().replace("Actions/", "");
            LOG.log(Level.FINE, "{0} {1}", arrobject);
            return a;
        }

        private boolean doAcceptDataObject(DataObject obj) {
            this.instanceCookie = (InstanceCookie)obj.getCookie(InstanceCookie.class);
            if (null != this.instanceCookie) {
                block16 : {
                    try {
                        Object instance = this.instanceCookie.instanceCreate();
                        if (null == instance) break block16;
                        if (instance instanceof Action) {
                            Action action = (Action)instance;
                            boolean noIconBase = false;
                            try {
                                noIconBase = null == action.getValue("iconBase");
                            }
                            catch (AssertionError aE) {
                                LOG.log(Level.FINE, null, (Throwable)((Object)aE));
                            }
                            boolean smallIcon = false;
                            if (noIconBase) {
                                try {
                                    Object icon = action.getValue("SmallIcon");
                                    smallIcon = icon != null && icon != BlankAction.BLANK_ICON;
                                }
                                catch (AssertionError aE) {
                                    LOG.log(Level.FINE, null, (Throwable)((Object)aE));
                                }
                            }
                            Object allowedInToolbar = action.getValue("CanBePlacedOnMainToolbar");
                            if (noIconBase && !smallIcon || Boolean.FALSE.equals(allowedInToolbar)) {
                                return false;
                            }
                            break block16;
                        }
                        if (instance instanceof JSeparator) {
                            return false;
                        }
                    }
                    catch (AssertionError aE) {
                        LOG.log(Level.FINE, null, (Throwable)((Object)aE));
                        return false;
                    }
                    catch (Throwable e) {
                        LOG.log(Level.WARNING, null, e);
                    }
                }
                return true;
            }
            FileObject fo = obj.getPrimaryFile();
            if (fo.isFolder()) {
                boolean hasChildWithIcon = false;
                FileObject[] children = fo.getChildren();
                for (int i = 0; i < children.length; ++i) {
                    DataObject child = null;
                    try {
                        child = DataObject.find((FileObject)children[i]);
                    }
                    catch (DataObjectNotFoundException e) {
                        continue;
                    }
                    if (null == child || !this.acceptDataObject(child)) continue;
                    hasChildWithIcon = true;
                    break;
                }
                return hasChildWithIcon;
            }
            return true;
        }
    }

    private static class ItemActionNode
    extends FilterNode {
        private static DataFlavor nodeDataFlavor = new DataFlavor(Node.class, "Action Node");

        public ItemActionNode(Node original) {
            super(original, FilterNode.Children.LEAF);
        }

        public Transferable drag() throws IOException {
            return new ExTransferable.Single(nodeDataFlavor){

                public Object getData() {
                    return ItemActionNode.this;
                }
            };
        }

        public String getDisplayName() {
            return Actions.cutAmpersand((String)super.getDisplayName());
        }

        public Action[] getActions(boolean context) {
            return new Action[0];
        }

    }

    private static class BlankAction
    extends CallbackSystemAction {
        static final Icon BLANK_ICON = ((BlankAction)BlankAction.get(BlankAction.class)).getIcon();

        private BlankAction() {
        }

        public String getName() {
            return null;
        }

        public HelpCtx getHelpCtx() {
            return null;
        }
    }

    private static class FolderActionNode
    extends FilterNode {
        public FolderActionNode(Node original) {
            super(original, (Children)new MyChildren(original));
        }

        public String getDisplayName() {
            return Actions.cutAmpersand((String)super.getDisplayName());
        }

        public Transferable drag() throws IOException {
            return Node.EMPTY.drag();
        }

        public Transferable clipboardCut() throws IOException {
            return Node.EMPTY.clipboardCut();
        }

        public Transferable clipboardCopy() throws IOException {
            return Node.EMPTY.clipboardCopy();
        }

        public Action[] getActions(boolean context) {
            return new Action[0];
        }

        private static class MyChildren
        extends FilterNode.Children {
            public MyChildren(Node original) {
                super(original);
            }

            protected Node copyNode(Node node) {
                FileObject fo = (FileObject)node.getLookup().lookup(FileObject.class);
                if (null != fo && fo.isData()) {
                    return new ItemActionNode(node);
                }
                return new FolderActionNode(node);
            }
        }

    }

    private class FormListener
    implements ActionListener {
        FormListener() {
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            if (evt.getSource() == ConfigureToolbarPanel.this.checkSmallIcons) {
                ConfigureToolbarPanel.this.switchIconSize(evt);
            } else if (evt.getSource() == ConfigureToolbarPanel.this.btnNewToolbar) {
                ConfigureToolbarPanel.this.newToolbar(evt);
            } else if (evt.getSource() == ConfigureToolbarPanel.this.btnReset) {
                ConfigureToolbarPanel.this.resetToolbars(evt);
            }
        }
    }

}

