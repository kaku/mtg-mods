/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.view.ui.slides;

import java.awt.Component;
import org.netbeans.core.windows.view.ui.slides.ScaleFx;
import org.netbeans.core.windows.view.ui.slides.SlideOperation;
import org.netbeans.core.windows.view.ui.slides.SlideOperationImpl;
import org.netbeans.core.windows.view.ui.slides.SlidingFx;

public final class SlideOperationFactory {
    private static final SlidingFx slideInFx = new ScaleFx(0.1f, 0.9f, true);
    private static final SlidingFx slideOutFx = new ScaleFx(0.9f, 0.1f, false);
    private static final SlidingFx slideIntoEdgeFx = new ScaleFx(0.9f, 0.1f, false);
    private static final SlidingFx slideIntoDesktopFx = new ScaleFx(1.0f, 1.0f, true);
    static final boolean EFFECTS_ENABLED = Boolean.getBoolean("nb.winsys.sliding.effects");

    private SlideOperationFactory() {
    }

    public static SlideOperation createSlideIn(Component component, int orientation, boolean useEffect, boolean requestActivation) {
        SlideOperationImpl result = new SlideOperationImpl(0, component, orientation, useEffect && EFFECTS_ENABLED ? slideInFx : null, requestActivation);
        return result;
    }

    public static SlideOperation createSlideOut(Component component, int orientation, boolean useEffect, boolean requestActivation) {
        SlideOperationImpl result = new SlideOperationImpl(1, component, orientation, useEffect && EFFECTS_ENABLED ? slideOutFx : null, requestActivation);
        return result;
    }

    public static SlideOperation createSlideIntoEdge(Component component, String side, boolean useEffect) {
        SlideOperationImpl result = new SlideOperationImpl(2, component, side, useEffect && EFFECTS_ENABLED ? slideIntoEdgeFx : null, false);
        return result;
    }

    public static SlideOperation createSlideIntoDesktop(Component component, int orientation, boolean useEffect) {
        SlideOperationImpl result = new SlideOperationImpl(3, component, orientation, useEffect && EFFECTS_ENABLED ? slideIntoDesktopFx : null, false);
        return result;
    }

    public static SlideOperation createSlideResize(Component component, int orientation) {
        SlideOperationImpl result = new SlideOperationImpl(4, component, orientation, null, false);
        return result;
    }

    public static SlideOperation createSlideResize(Component component, String side) {
        return SlideOperationFactory.createSlideResize(component, SlideOperationImpl.side2Orientation(side));
    }
}

