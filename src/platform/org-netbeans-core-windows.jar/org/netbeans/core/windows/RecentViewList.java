/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.windows;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

final class RecentViewList
implements PropertyChangeListener {
    private List<String> tcIdList = new ArrayList<String>(20);
    private Map<String, Reference<TopComponent>> tcCache = new HashMap<String, Reference<TopComponent>>(20);

    public RecentViewList(WindowManager wm) {
        wm.getRegistry().addPropertyChangeListener((PropertyChangeListener)this);
    }

    public TopComponent[] getTopComponents() {
        ArrayList<TopComponent> tcList = new ArrayList<TopComponent>(this.tcIdList.size());
        WindowManager wm = WindowManager.getDefault();
        for (int i = 0; i < this.tcIdList.size(); ++i) {
            String tcId = this.tcIdList.get(i);
            TopComponent tc = null;
            Reference<TopComponent> ref = this.tcCache.get(tcId);
            if (null != ref) {
                tc = ref.get();
            }
            if (null == tc && null != (tc = wm.findTopComponent(tcId))) {
                this.tcCache.put(tcId, new WeakReference<TopComponent>(tc));
            }
            if (tc == null || !tc.isOpened()) continue;
            tcList.add(tc);
        }
        return tcList.toArray((T[])new TopComponent[tcList.size()]);
    }

    public String[] getTopComponentIDs() {
        return this.tcIdList.toArray(new String[this.tcIdList.size()]);
    }

    public void setTopComponents(String[] tcIDs) {
        this.tcIdList.clear();
        this.tcIdList.addAll(Arrays.asList(tcIDs));
        this.tcCache.clear();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        TopComponent tc;
        if ("activated".equals(evt.getPropertyName()) && (tc = (TopComponent)evt.getNewValue()) != null) {
            String tcId = WindowManager.getDefault().findTopComponentID(tc);
            this.tcIdList.remove(tcId);
            this.tcIdList.add(0, tcId);
            this.fillList(TopComponent.getRegistry().getOpened());
        }
    }

    private void fillList(Set<TopComponent> openedTCs) {
        this.tcCache.clear();
        WindowManager wm = WindowManager.getDefault();
        for (TopComponent curTC : openedTCs) {
            String id = wm.findTopComponentID(curTC);
            if (!this.tcIdList.contains(id)) {
                if (this.tcIdList.size() > 1) {
                    this.tcIdList.add(1, id);
                } else {
                    this.tcIdList.add(id);
                }
            }
            this.tcCache.put(id, new WeakReference<TopComponent>(curTC));
        }
    }
}

