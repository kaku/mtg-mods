/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows;

final class ViewRequest {
    public final Object source;
    public final int type;
    public final Object oldValue;
    public final Object newValue;

    public ViewRequest(Object source, int type, Object oldValue, Object newValue) {
        this.source = source;
        this.type = type;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    public String toString() {
        String tp;
        StringBuilder result = new StringBuilder();
        result.append("ViewRequest@");
        result.append(System.identityHashCode(this));
        result.append(" [TYPE=");
        switch (this.type) {
            case 9: {
                tp = "CHANGE_ACTIVE_MODE_CHANGED";
                break;
            }
            case 7: {
                tp = "CHANGE_EDITOR_AREA_BOUNDS_CHANGED";
                break;
            }
            case 8: {
                tp = "CHANGE_EDITOR_AREA_CONSTRAINTS_CHANGED";
                break;
            }
            case 5: {
                tp = "CHANGE_EDITOR_AREA_STATE_CHANGED";
                break;
            }
            case 6: {
                tp = "CHANGE_EDITOR_AREA_FRAME_STATE_CHANGED";
                break;
            }
            case 1: {
                tp = "CHANGE_MAIN_WINDOW_BOUNDS_JOINED_CHANGED";
                break;
            }
            case 2: {
                tp = "CHANGE_MAIN_WINDOW_BOUNDS_SEPARATED_CHANGED";
                break;
            }
            case 3: {
                tp = "CHANGE_MAIN_WINDOW_FRAME_STATE_JOINED_CHANGED";
                break;
            }
            case 4: {
                tp = "CHANGE_MAIN_WINDOW_FRAME_STATE_SEPARATED_CHANGED";
                break;
            }
            case 11: {
                tp = "CHANGE_MAXIMIZED_MODE_CHANGED";
                break;
            }
            case 22: {
                tp = "CHANGE_MODE_SELECTED_TOPCOMPONENT_CHANGED";
                break;
            }
            case 20: {
                tp = "CHANGE_MODE_BOUNDS_CHANGED";
                break;
            }
            case 14: {
                tp = "CHANGE_MODE_CONSTRAINTS_CHANGED";
                break;
            }
            case 21: {
                tp = "CHANGE_MODE_FRAME_STATE_CHANGED";
                break;
            }
            case 10: {
                tp = "CHANGE_TOOLBAR_CONFIGURATION_CHANGED";
                break;
            }
            case 34: {
                tp = "CHANGE_TOPCOMPONENT_ICON_CHANGED";
                break;
            }
            case 31: {
                tp = "CHANGE_TOPCOMPONENT_DISPLAY_NAME_CHANGED";
                break;
            }
            case 32: {
                tp = "CHANGE_TOPCOMPONENT_DISPLAY_NAME_ANNOTATION_CHANGED";
                break;
            }
            case 33: {
                tp = "CHANGE_TOPCOMPONENT_TOOLTIP_CHANGED";
                break;
            }
            case 44: {
                tp = "CHANGE_TOPCOMPONENT_ACTIVATED";
                break;
            }
            case 46: {
                tp = "CHANGE_DND_PERFORMED";
                break;
            }
            case 61: {
                tp = "CHANGE_UI_UPDATE";
                break;
            }
            case 63: {
                tp = "TOPCOMPONENT_REQUEST_ATTENTION";
                break;
            }
            case 64: {
                tp = "TOPCOMPONENT_CANCEL_REQUEST_ATTENTION";
                break;
            }
            case 65: {
                tp = "TOPCOMPONENT_ATTENTION_HIGHLIGHT_ON";
                break;
            }
            case 66: {
                tp = "TOPCOMPONENT_ATTENTION_HIGHLIGHT_OFF";
                break;
            }
            default: {
                tp = "UNKNOWN";
            }
        }
        result.append(tp).append("]  [oldValue:").append(this.oldValue).append("] [newValue:").append(this.newValue).append("] [source:").append(this.source).append(']');
        return result.toString();
    }
}

