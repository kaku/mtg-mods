/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.Workspace
 */
package org.netbeans.core.windows;

import java.awt.Image;
import java.awt.Rectangle;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.Central;
import org.netbeans.core.windows.Constants;
import org.netbeans.core.windows.Debug;
import org.netbeans.core.windows.PersistenceHandler;
import org.netbeans.core.windows.SplitConstraint;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.WindowManagerImpl;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.Workspace;

public final class ModeImpl
implements Mode {
    private static final String MODE_ANONYMOUS_NAME = "anonymousMode";
    private final PropertyChangeSupport changeSupport;
    private static final boolean DEBUG = Debug.isLoggable(ModeImpl.class);

    private ModeImpl(String name, int state, int kind, boolean permanent) {
        this.changeSupport = new PropertyChangeSupport(this);
        ModeImpl.getCentral().createModeModel(this, name, state, kind, permanent);
    }

    public static ModeImpl createModeImpl(String name, int state, int kind, boolean permanent) {
        return new ModeImpl(name, state, kind, permanent);
    }

    public String getName() {
        WindowManagerImpl.warnIfNotInEDT();
        return ModeImpl.getCentral().getModeName(this);
    }

    Collection<String> getOtherNames() {
        WindowManagerImpl.warnIfNotInEDT();
        return ModeImpl.getCentral().getModeOtherNames(this);
    }

    void addOtherName(String modeOtherName) {
        WindowManagerImpl.warnIfNotInEDT();
        ModeImpl.getCentral().addModeOtherName(this, modeOtherName);
    }

    public String getDisplayName() {
        WindowManagerImpl.warnIfNotInEDT();
        return this.getName();
    }

    public Image getIcon() {
        WindowManagerImpl.warnIfNotInEDT();
        return null;
    }

    public boolean canDock(TopComponent tc) {
        WindowManagerImpl.warnIfNotInEDT();
        return true;
    }

    public boolean dockInto(TopComponent tc) {
        WindowManagerImpl.warnIfNotInEDT();
        return this.dockIntoImpl(tc, true);
    }

    public void setBounds(Rectangle bounds) {
        WindowManagerImpl.warnIfNotInEDT();
        ModeImpl.getCentral().setModeBounds(this, bounds);
    }

    public Rectangle getBounds() {
        WindowManagerImpl.warnIfNotInEDT();
        return ModeImpl.getCentral().getModeBounds(this);
    }

    public Workspace getWorkspace() {
        WindowManagerImpl.warnIfNotInEDT();
        return WindowManagerImpl.getInstance();
    }

    public TopComponent[] getTopComponents() {
        WindowManagerImpl.warnIfNotInEDT();
        return ModeImpl.getCentral().getModeTopComponents(this).toArray((T[])new TopComponent[0]);
    }

    public void addPropertyChangeListener(PropertyChangeListener pchl) {
        this.changeSupport.addPropertyChangeListener(pchl);
    }

    public void removePropertyChangeListener(PropertyChangeListener pchl) {
        this.changeSupport.removePropertyChangeListener(pchl);
    }

    private boolean dockIntoImpl(TopComponent tc, boolean select) {
        if (DEBUG) {
            Debug.log(ModeImpl.class, "Docking tc=" + tc.getName() + " into mode=" + this);
            Debug.dumpStack(ModeImpl.class);
        }
        boolean opened = false;
        ModeImpl mode = (ModeImpl)WindowManagerImpl.getInstance().findMode(tc);
        if (mode != null && mode != this) {
            opened = tc.isOpened();
            mode.removeTopComponent(tc);
        }
        if (opened) {
            this.addOpenedTopComponent(tc);
        } else {
            this.addClosedTopComponent(tc);
        }
        return true;
    }

    public void close(TopComponent tc) {
        if (!this.getOpenedTopComponents().contains((Object)tc)) {
            return;
        }
        if (PersistenceHandler.isTopComponentPersistentWhenClosed(tc)) {
            this.addClosedTopComponent(tc);
        } else if (Boolean.TRUE.equals(tc.getClientProperty((Object)"KeepNonPersistentTCInModelWhenClosed"))) {
            this.addClosedTopComponent(tc);
        } else {
            this.removeTopComponent(tc);
        }
    }

    public List<TopComponent> getOpenedTopComponents() {
        return ModeImpl.getCentral().getModeOpenedTopComponents(this);
    }

    public void setSelectedTopComponent(TopComponent tc) {
        if (!this.getOpenedTopComponents().contains((Object)tc)) {
            return;
        }
        TopComponent old = this.getSelectedTopComponent();
        if (tc == old) {
            return;
        }
        ModeImpl.getCentral().setModeSelectedTopComponent(this, tc);
    }

    public TopComponent getSelectedTopComponent() {
        WindowManagerImpl.assertEventDispatchThread();
        return ModeImpl.getCentral().getModeSelectedTopComponent(this);
    }

    public void setPreviousSelectedTopComponentID(String tcId) {
        String old = this.getPreviousSelectedTopComponentID();
        if (null != tcId && tcId.equals(old)) {
            return;
        }
        ModeImpl.getCentral().setModePreviousSelectedTopComponentID(this, tcId);
    }

    public TopComponent getPreviousSelectedTopComponent() {
        String tcId = this.getPreviousSelectedTopComponentID();
        TopComponent res = null;
        if (null != tcId) {
            res = WindowManagerImpl.getInstance().findTopComponent(tcId);
        }
        WindowManagerImpl.assertEventDispatchThread();
        return res;
    }

    public String getPreviousSelectedTopComponentID() {
        WindowManagerImpl.assertEventDispatchThread();
        return ModeImpl.getCentral().getModePreviousSelectedTopComponentID(this);
    }

    public void addOpenedTopComponent(TopComponent tc) {
        ModeImpl.getCentral().addModeOpenedTopComponent(this, tc);
    }

    public void addOpenedTopComponentNoNotify(TopComponent tc) {
        ModeImpl.getCentral().addModeOpenedTopComponentNoNotify(this, tc);
    }

    public void addOpenedTopComponent(TopComponent tc, int index) {
        ModeImpl.getCentral().insertModeOpenedTopComponent(this, tc, index);
    }

    public void addClosedTopComponent(TopComponent tc) {
        ModeImpl.getCentral().addModeClosedTopComponent(this, tc);
    }

    public void addUnloadedTopComponent(String tcID) {
        this.addUnloadedTopComponent(tcID, -1);
    }

    public void addUnloadedTopComponent(String tcID, int index) {
        ModeImpl.getCentral().addModeUnloadedTopComponent(this, tcID, index);
    }

    public void setUnloadedSelectedTopComponent(String tcID) {
        ModeImpl.getCentral().setUnloadedSelectedTopComponent(this, tcID);
    }

    public void setUnloadedPreviousSelectedTopComponent(String tcID) {
        ModeImpl.getCentral().setUnloadedPreviousSelectedTopComponent(this, tcID);
    }

    public List<String> getOpenedTopComponentsIDs() {
        return ModeImpl.getCentral().getModeOpenedTopComponentsIDs(this);
    }

    public List<String> getClosedTopComponentsIDs() {
        return ModeImpl.getCentral().getModeClosedTopComponentsIDs(this);
    }

    public List<String> getTopComponentsIDs() {
        return ModeImpl.getCentral().getModeTopComponentsIDs(this);
    }

    public int getTopComponentTabPosition(TopComponent tc) {
        return ModeImpl.getCentral().getModeTopComponentTabPosition(this, tc);
    }

    public void setFrameState(int state) {
        ModeImpl.getCentral().setModeFrameState(this, state);
    }

    public int getFrameState() {
        return ModeImpl.getCentral().getModeFrameState(this);
    }

    public boolean isPermanent() {
        return ModeImpl.getCentral().isModePermanent(this);
    }

    public boolean isEmpty() {
        return ModeImpl.getCentral().isModeEmpty(this);
    }

    public boolean containsTopComponent(TopComponent tc) {
        return ModeImpl.getCentral().containsModeTopComponent(this, tc);
    }

    public int getState() {
        return ModeImpl.getCentral().getModeState(this);
    }

    public int getKind() {
        return ModeImpl.getCentral().getModeKind(this);
    }

    public String getSide() {
        return ModeImpl.getCentral().getModeSide(this);
    }

    public void setConstraints(SplitConstraint[] constraints) {
        WindowManagerImpl.getInstance().setModeConstraints(this, constraints);
    }

    public SplitConstraint[] getConstraints() {
        return WindowManagerImpl.getInstance().getModeConstraints(this);
    }

    public boolean isMinimized() {
        return ModeImpl.getCentral().isModeMinimized(this);
    }

    public void setMinimized(boolean minimized) {
        ModeImpl.getCentral().setModeMinimized(this, minimized);
    }

    public void removeTopComponent(TopComponent tc) {
        ModeImpl.getCentral().removeModeTopComponent(this, tc);
    }

    public void removeTopComponents(Set topComponentSet) {
        for (TopComponent tc : topComponentSet) {
            this.removeTopComponent(tc);
        }
    }

    public void removeClosedTopComponentID(String tcID) {
        ModeImpl.getCentral().removeModeClosedTopComponentID(this, tcID);
    }

    public boolean canContain(TopComponent tc) {
        int otherKind;
        if (Constants.SWITCH_MODE_ADD_NO_RESTRICT || WindowManagerImpl.getInstance().isTopComponentAllowedToMoveAnywhere(tc) || Switches.isMixingOfEditorsAndViewsEnabled()) {
            return true;
        }
        ModeImpl mode = (ModeImpl)WindowManagerImpl.getInstance().findMode(tc);
        if (mode == null) {
            return true;
        }
        int myKind = this.getKind();
        return myKind == (otherKind = mode.getKind()) || myKind != 1 && otherKind != 1;
    }

    void doFirePropertyChange(final String propName, final Object oldValue, final Object newValue) {
        if (SwingUtilities.isEventDispatchThread()) {
            this.changeSupport.firePropertyChange(propName, oldValue, newValue);
        } else {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    ModeImpl.this.changeSupport.firePropertyChange(propName, oldValue, newValue);
                }
            });
        }
    }

    public String toString() {
        return super.toString() + "[" + ModeImpl.getCentral().getModeName(this) + "]";
    }

    private static Central getCentral() {
        return WindowManagerImpl.getInstance().getCentral();
    }

    static String getUnusedModeName() {
        String result;
        String base = "anonymousMode";
        if (base.length() > 20) {
            base = base.substring(0, 20);
        }
        int modeNumber = 1;
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        while (wm.findMode(result = base + "_" + modeNumber) != null) {
            ++modeNumber;
        }
        return result;
    }

    public void setModeName(String text) {
        ModeImpl.getCentral().setModeName(this, text);
    }

}

