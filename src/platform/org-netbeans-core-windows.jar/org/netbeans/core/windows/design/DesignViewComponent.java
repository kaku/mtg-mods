/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Description
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.windows.design;

import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.netbeans.core.windows.ModeImpl;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

@TopComponent.Description(preferredID="DesignViewComponentTopComponent", iconBase="org/netbeans/core/windows/model/DesignView.png", persistenceType=1)
final class DesignViewComponent
extends TopComponent
implements DocumentListener {
    private JLabel jLabel1;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JTextField modeName;

    DesignViewComponent() {
        this.initComponents();
        this.setName(NbBundle.getMessage(DesignViewComponent.class, (String)"CTL_DesignViewComponentTopComponent"));
        this.setToolTipText(NbBundle.getMessage(DesignViewComponent.class, (String)"HINT_DesignViewComponentTopComponent"));
        this.putClientProperty((Object)"TopComponentAllowDockAnywhere", (Object)true);
        this.refresh();
        this.modeName.getDocument().addDocumentListener(this);
    }

    private void initComponents() {
        this.modeName = new JTextField();
        this.jPanel1 = new JPanel();
        this.jPanel2 = new JPanel();
        this.jLabel1 = new JLabel();
        this.modeName.setText(NbBundle.getMessage(DesignViewComponent.class, (String)"DesignViewComponent.modeName.text", (Object[])new Object[0]));
        this.modeName.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                DesignViewComponent.this.modeNameActionPerformed(evt);
            }
        });
        GroupLayout jPanel1Layout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 500, 32767));
        jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 88, 32767));
        GroupLayout jPanel2Layout = new GroupLayout(this.jPanel2);
        this.jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 500, 32767));
        jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 63, 32767));
        Mnemonics.setLocalizedText((JLabel)this.jLabel1, (String)NbBundle.getMessage(DesignViewComponent.class, (String)"DesignViewComponent.jLabel1.text", (Object[])new Object[0]));
        GroupLayout layout = new GroupLayout((Container)((Object)this));
        this.setLayout((LayoutManager)layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel1).addComponent(this.modeName, -1, 500, 32767).addComponent(this.jPanel1, -1, -1, 32767).addComponent(this.jPanel2, -1, -1, 32767)).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGap(9, 9, 9).addComponent(this.jPanel1, -1, -1, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel1).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.modeName, -2, -1, -2).addGap(34, 34, 34).addComponent(this.jPanel2, -1, -1, 32767).addContainerGap()));
    }

    private void refresh() {
        Mode mode = WindowManager.getDefault().findMode((TopComponent)this);
        if (mode != null) {
            if (!this.modeName.getText().equals(mode.getName())) {
                this.modeName.setText(mode.getName());
            }
            this.setName(mode.getName());
        }
    }

    private void modeNameActionPerformed(ActionEvent evt) {
        Mode mode = WindowManager.getDefault().findMode((TopComponent)this);
        if (mode instanceof ModeImpl) {
            ModeImpl mi = (ModeImpl)mode;
            mi.setModeName(this.modeName.getText());
        }
        for (TopComponent tc : mode.getTopComponents()) {
            if (!(tc instanceof DesignViewComponent)) continue;
            DesignViewComponent dvc = (DesignViewComponent)tc;
            dvc.refresh();
        }
    }

    public void componentOpened() {
        this.refresh();
    }

    protected void componentActivated() {
        this.refresh();
    }

    void writeProperties(Properties p) {
        p.setProperty("version", "1.0");
    }

    void readProperties(Properties p) {
        String version = p.getProperty("version");
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                DesignViewComponent.this.modeNameActionPerformed(null);
            }
        });
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                DesignViewComponent.this.modeNameActionPerformed(null);
            }
        });
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                DesignViewComponent.this.modeNameActionPerformed(null);
            }
        });
    }

}

