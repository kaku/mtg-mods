/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.windows.design;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.Set;
import org.netbeans.core.windows.design.DesignViewComponent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public final class DesignView
implements Runnable,
PropertyChangeListener {
    private static final DesignView INSTANCE = new DesignView();
    static int designModeCounter;

    private DesignView() {
    }

    public static void initialize() {
        INSTANCE.cleanToolbarsAndMenu();
        WindowManager.getDefault().invokeWhenUIReady((Runnable)INSTANCE);
        TopComponent.getRegistry().addPropertyChangeListener((PropertyChangeListener)INSTANCE);
    }

    private void cleanToolbarsAndMenu() {
        FileObject mb;
        FileObject ws;
        FileObject tb = FileUtil.getConfigFile((String)"Toolbars");
        if (tb != null) {
            for (FileObject fileObject : tb.getChildren()) {
                try {
                    fileObject.delete();
                    continue;
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            try {
                FileObject shadow = tb.createFolder("DesignView").createData("org-netbeans-core-windows-model-NewMode.shadow");
                shadow.setAttribute("originalFile", (Object)"Actions/System/org-netbeans-core-windows-model-NewMode.instance");
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        if ((mb = FileUtil.getConfigFile((String)"Menu")) != null) {
            for (FileObject fileObject : mb.getChildren()) {
                try {
                    fileObject.delete();
                    continue;
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }
        if ((ws = FileUtil.getConfigFile((String)"Windows2Local")) != null) {
            try {
                ws.delete();
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        WindowManager.getDefault().invokeWhenUIReady((Runnable)this);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("opened".equals(evt.getPropertyName())) {
            for (Mode m : WindowManager.getDefault().getModes()) {
                for (TopComponent topComponent : m.getTopComponents()) {
                    if (topComponent instanceof DesignViewComponent) continue;
                    topComponent.close();
                }
            }
        }
    }

    @Override
    public void run() {
        for (Mode m : WindowManager.getDefault().getModes()) {
            boolean found = false;
            for (TopComponent topComponent : m.getTopComponents()) {
                if (topComponent instanceof DesignViewComponent) {
                    found = true;
                    continue;
                }
                topComponent.close();
            }
            if (found) continue;
            DesignViewComponent mc = new DesignViewComponent();
            m.dockInto((TopComponent)mc);
            mc.open();
        }
    }

    public static ActionListener newModeAction() {
        return new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                DesignViewComponent dvc = new DesignViewComponent();
                dvc.open();
                dvc.requestAttention(true);
            }
        };
    }

}

