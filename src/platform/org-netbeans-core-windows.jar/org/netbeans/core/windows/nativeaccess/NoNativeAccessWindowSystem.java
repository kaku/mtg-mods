/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.nativeaccess;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.Shape;
import java.awt.Window;
import javax.swing.Icon;
import org.netbeans.core.windows.nativeaccess.NativeWindowSystem;

class NoNativeAccessWindowSystem
extends NativeWindowSystem {
    NoNativeAccessWindowSystem() {
    }

    @Override
    public boolean isWindowAlphaSupported() {
        return false;
    }

    @Override
    public void setWindowAlpha(Window w, float alpha) {
        GraphicsConfiguration gc = w.getGraphicsConfiguration();
        GraphicsDevice gd = gc.getDevice();
        if (gc.getDevice().getFullScreenWindow() == w) {
            return;
        }
        if (!gd.isWindowTranslucencySupported(GraphicsDevice.WindowTranslucency.TRANSLUCENT)) {
            return;
        }
        w.setOpacity(alpha);
    }

    @Override
    public void setWindowMask(Window w, Shape mask) {
        GraphicsConfiguration gc = w.getGraphicsConfiguration();
        GraphicsDevice gd = gc.getDevice();
        if (gc.getDevice().getFullScreenWindow() == w) {
            return;
        }
        if (!gd.isWindowTranslucencySupported(GraphicsDevice.WindowTranslucency.TRANSLUCENT)) {
            return;
        }
        w.setShape(mask);
    }

    @Override
    public void setWindowMask(Window w, Icon mask) {
    }

    @Override
    public boolean isUndecoratedWindowAlphaSupported() {
        return true;
    }
}

