/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.core.windows.nativeaccess;

import java.awt.Shape;
import java.awt.Window;
import javax.swing.Icon;
import org.netbeans.core.windows.nativeaccess.NoNativeAccessWindowSystem;
import org.openide.util.Lookup;

public abstract class NativeWindowSystem {
    private static NativeWindowSystem dummyInstance;

    public static final NativeWindowSystem getDefault() {
        NativeWindowSystem wmInstance = (NativeWindowSystem)Lookup.getDefault().lookup(NativeWindowSystem.class);
        return wmInstance != null ? wmInstance : NativeWindowSystem.getDummyInstance();
    }

    private static synchronized NativeWindowSystem getDummyInstance() {
        if (dummyInstance == null) {
            dummyInstance = new NoNativeAccessWindowSystem();
        }
        return dummyInstance;
    }

    public abstract boolean isWindowAlphaSupported();

    public abstract boolean isUndecoratedWindowAlphaSupported();

    public abstract void setWindowAlpha(Window var1, float var2);

    public abstract void setWindowMask(Window var1, Shape var2);

    public abstract void setWindowMask(Window var1, Icon var2);
}

