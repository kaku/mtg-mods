/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.windows.documentgroup;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CTL_CloseDocumentGroupAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_CloseDocumentGroupAction");
    }

    static String CTL_ManageGroupsAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_ManageGroupsAction");
    }

    static String CTL_NewGroupAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_NewGroupAction");
    }

    private void Bundle() {
    }
}

