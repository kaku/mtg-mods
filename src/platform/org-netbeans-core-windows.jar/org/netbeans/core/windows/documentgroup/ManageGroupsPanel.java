/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.awt.Mnemonics
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.windows.documentgroup;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.netbeans.core.windows.documentgroup.DocumentGroupImpl;
import org.netbeans.core.windows.documentgroup.GroupsManager;
import org.netbeans.core.windows.documentgroup.GroupsMenuAction;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.awt.Mnemonics;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

class ManageGroupsPanel
extends JPanel {
    private DialogDescriptor descriptor;
    private Dialog dialog;
    private final JButton btnSelect = new JButton(NbBundle.getMessage(ManageGroupsPanel.class, (String)"Btn_SELECT"));
    private JButton btnRemove;
    private JButton btnRemoveAll;
    private JList listGroups;
    private JScrollPane scrollGroups;

    public ManageGroupsPanel() {
        this.initComponents();
        this.fillGroups();
        this.btnSelect.setEnabled(false);
        this.listGroups.getSelectionModel().addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent e) {
                ManageGroupsPanel.this.enableButtons();
            }
        });
        this.btnSelect.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                DocumentGroupImpl group = (DocumentGroupImpl)ManageGroupsPanel.this.listGroups.getSelectedValue();
                if (null != group) {
                    GroupsManager.getDefault().openGroup(group);
                }
            }
        });
    }

    private void enableButtons() {
        this.btnRemoveAll.setEnabled(this.listGroups.getModel().getSize() > 0);
        int selIndex = this.listGroups.getSelectedIndex();
        if (null != this.descriptor) {
            this.descriptor.setValid(selIndex >= 0);
        }
        this.btnSelect.setEnabled(selIndex >= 0);
    }

    public void showDialog() {
        this.descriptor = new DialogDescriptor((Object)this, NbBundle.getMessage(ManageGroupsPanel.class, (String)"Dlg_DOCUMENT_GROUPS"), true, new Object[]{this.btnSelect, DialogDescriptor.CANCEL_OPTION}, (Object)this.btnSelect, 0, null, null);
        this.descriptor.setHelpCtx(new HelpCtx("org.netbeans.core.windows.documentgroup.ManageGroupsPanel"));
        this.descriptor.setValid(false);
        this.dialog = DialogDisplayer.getDefault().createDialog(this.descriptor);
        this.dialog.setVisible(true);
        GroupsMenuAction.refreshMenu();
    }

    private void initComponents() {
        this.scrollGroups = new JScrollPane();
        this.listGroups = new JList();
        this.btnRemove = new JButton();
        this.btnRemoveAll = new JButton();
        this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.listGroups.setSelectionMode(0);
        this.scrollGroups.setViewportView(this.listGroups);
        Mnemonics.setLocalizedText((AbstractButton)this.btnRemove, (String)NbBundle.getMessage(ManageGroupsPanel.class, (String)"ManageGroupsPanel.btnRemove.text"));
        this.btnRemove.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                ManageGroupsPanel.this.btnRemoveActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.btnRemoveAll, (String)NbBundle.getMessage(ManageGroupsPanel.class, (String)"ManageGroupsPanel.btnRemoveAll.text"));
        this.btnRemoveAll.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                ManageGroupsPanel.this.btnRemoveAllActionPerformed(evt);
            }
        });
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.scrollGroups, -1, 295, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.btnRemoveAll, -1, -1, 32767).addComponent(this.btnRemove, -2, 114, -2))));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.scrollGroups, -2, -1, -2).addGroup(layout.createSequentialGroup().addComponent(this.btnRemove).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnRemoveAll)));
    }

    private void btnRemoveAllActionPerformed(ActionEvent evt) {
        GroupsManager.getDefault().removeAllGroups();
        this.fillGroups();
        this.enableButtons();
    }

    private void btnRemoveActionPerformed(ActionEvent evt) {
        DocumentGroupImpl group = (DocumentGroupImpl)this.listGroups.getSelectedValue();
        if (null != group) {
            GroupsManager.getDefault().removeGroup(group);
            this.fillGroups();
            this.enableButtons();
        }
    }

    private void fillGroups() {
        List<DocumentGroupImpl> groups = GroupsManager.getDefault().getGroups();
        DefaultListModel<DocumentGroupImpl> model = new DefaultListModel<DocumentGroupImpl>();
        for (DocumentGroupImpl group : groups) {
            model.addElement(group);
        }
        this.listGroups.setModel(model);
    }

}

