/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.documentgroup;

import java.awt.Color;
import java.awt.Component;
import java.awt.Frame;
import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.netbeans.core.windows.WindowManagerImpl;

class PleaseWait
extends JPanel {
    private static final Color FILL_COLOR = new Color(0, 0, 0, 128);
    private Component oldGlass;

    public PleaseWait() {
        this.setOpaque(false);
    }

    public void install() {
        JFrame frame = (JFrame)WindowManagerImpl.getInstance().getMainWindow();
        this.oldGlass = frame.getGlassPane();
        if (this.oldGlass instanceof PleaseWait) {
            this.oldGlass = null;
        }
        frame.setGlassPane(this);
        this.setVisible(true);
        this.invalidate();
        this.revalidate();
        this.repaint();
    }

    public void uninstall() {
        this.setVisible(false);
        JFrame frame = (JFrame)WindowManagerImpl.getInstance().getMainWindow();
        frame.setGlassPane(this.oldGlass);
    }

    @Override
    public void paint(Graphics g) {
        g.setColor(FILL_COLOR);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
    }
}

