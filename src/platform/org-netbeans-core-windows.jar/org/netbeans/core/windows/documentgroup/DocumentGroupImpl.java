/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.documentgroup;

import java.text.Collator;
import javax.swing.JPanel;
import org.netbeans.core.windows.documentgroup.GroupsManager;

public final class DocumentGroupImpl
implements Comparable<DocumentGroupImpl> {
    private final String name;
    private final String displayName;

    DocumentGroupImpl(String name, String displayName) {
        this.name = name;
        this.displayName = displayName;
    }

    public boolean open() {
        return GroupsManager.getDefault().openGroup(this);
    }

    public boolean close() {
        return GroupsManager.getDefault().closeGroup(this);
    }

    public String toString() {
        return this.displayName;
    }

    String getName() {
        return this.name;
    }

    @Override
    public int compareTo(DocumentGroupImpl o) {
        Collator collator = Collator.getInstance();
        int res = collator.compare(this.displayName, o.displayName);
        if (0 == res) {
            res = collator.compare(this.name, o.name);
        }
        return res;
    }

    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        DocumentGroupImpl other = (DocumentGroupImpl)obj;
        if (this.name == null ? other.name != null : !this.name.equals(other.name)) {
            return false;
        }
        return true;
    }

    private static class WaitPane
    extends JPanel {
        public WaitPane() {
            this.setOpaque(false);
        }
    }

}

