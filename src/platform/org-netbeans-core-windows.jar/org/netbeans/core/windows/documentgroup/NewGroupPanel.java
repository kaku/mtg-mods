/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.awt.Mnemonics
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.documentgroup;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.documentgroup.GroupsManager;
import org.netbeans.core.windows.documentgroup.GroupsMenuAction;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.awt.Mnemonics;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

class NewGroupPanel
extends JPanel {
    private static final String DEFAULT_NAME = NbBundle.getMessage(NewGroupPanel.class, (String)"Txt_NEW_GROUP_NAME");
    private JCheckBox cbKeepOpenedDocuments;
    private JLabel lblName;
    private JTextField txtName;

    public NewGroupPanel() {
        this.initComponents();
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.txtName.selectAll();
        this.txtName.requestFocusInWindow();
    }

    void showDialog() {
        boolean hasOpenedDocuments;
        final DialogDescriptor dd = new DialogDescriptor((Object)this, NbBundle.getMessage(NewGroupPanel.class, (String)"Dlg_NEW_GROUP"), true, 2, (Object)null, null);
        dd.setHelpCtx(new HelpCtx("org.netbeans.core.windows.documentgroup.NewGroupAction"));
        boolean bl = hasOpenedDocuments = WindowManagerImpl.getInstance().getEditorTopComponents().length > 0;
        if (!hasOpenedDocuments) {
            this.cbKeepOpenedDocuments.setSelected(false);
            this.cbKeepOpenedDocuments.setEnabled(false);
        }
        this.txtName.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent e) {
                NewGroupPanel.this.validate(dd);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                NewGroupPanel.this.validate(dd);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                NewGroupPanel.this.validate(dd);
            }
        });
        this.txtName.setText(DEFAULT_NAME);
        Dialog dlg = DialogDisplayer.getDefault().createDialog(dd);
        dlg.setVisible(true);
        if (DialogDescriptor.OK_OPTION == dd.getValue()) {
            if (!this.cbKeepOpenedDocuments.isSelected() && !GroupsManager.closeAllDocuments()) {
                return;
            }
            String name = this.txtName.getText().trim();
            GroupsManager.getDefault().addGroup(name);
            GroupsMenuAction.refreshMenu();
        }
    }

    private void validate(DialogDescriptor dd) {
        String name = this.txtName.getText();
        if (null == name) {
            name = "";
        }
        dd.setValid(!(name = name.trim()).isEmpty() && !DEFAULT_NAME.equals(name));
    }

    private void initComponents() {
        this.lblName = new JLabel();
        this.txtName = new JTextField();
        this.cbKeepOpenedDocuments = new JCheckBox();
        this.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        Mnemonics.setLocalizedText((JLabel)this.lblName, (String)NbBundle.getMessage(NewGroupPanel.class, (String)"NewGroupPanel.lblName.text"));
        this.cbKeepOpenedDocuments.setSelected(true);
        Mnemonics.setLocalizedText((AbstractButton)this.cbKeepOpenedDocuments, (String)NbBundle.getMessage(NewGroupPanel.class, (String)"NewGroupPanel.cbKeepOpenedDocuments.text"));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.lblName).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.txtName)).addGroup(layout.createSequentialGroup().addComponent(this.cbKeepOpenedDocuments).addGap(0, 102, 32767))).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lblName).addComponent(this.txtName, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.cbKeepOpenedDocuments).addContainerGap(-1, 32767)));
    }

}

