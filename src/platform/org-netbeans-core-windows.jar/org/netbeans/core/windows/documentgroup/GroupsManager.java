/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.WindowSystem
 *  org.openide.cookies.InstanceCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataLoader
 *  org.openide.loaders.DataLoaderPool
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.InstanceDataObject
 *  org.openide.modules.Places
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.windows.documentgroup;

import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import org.netbeans.core.WindowSystem;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.PersistenceHandler;
import org.netbeans.core.windows.SplitConstraint;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.documentgroup.DocumentGroupImpl;
import org.netbeans.core.windows.persistence.ModeConfig;
import org.netbeans.core.windows.persistence.PersistenceManager;
import org.netbeans.core.windows.persistence.WindowManagerParser;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataLoader;
import org.openide.loaders.DataLoaderPool;
import org.openide.loaders.DataObject;
import org.openide.loaders.InstanceDataObject;
import org.openide.modules.Places;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class GroupsManager {
    private static GroupsManager theInstance;
    private static final String SEL_GROUP = "selectedDocumentGroup";
    private static final String DEFAULT_GROUP_NAME = "group";
    private static final String DISPLAY_NAME = "displayName";
    private static final String CONFIG = "config";
    private static final String SETTINGS = "settings";
    private static final String WSMODE = "wsmode";
    private static final String WINDOWS2_LOCAL = "Windows2Local";
    private static final String COMPONENTS = "Components";
    private static final String MODES = "Modes";
    private static final String DOCUMENT_GROUPS = "DocumentGroups";
    private static final String ID_SEPARATOR = ":";
    private static final String SELECTED_ID = "_selectedId";
    private static final Logger LOG;

    private GroupsManager() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static GroupsManager getDefault() {
        Class<GroupsManager> class_ = GroupsManager.class;
        synchronized (GroupsManager.class) {
            if (null == theInstance) {
                theInstance = new GroupsManager();
            }
            // ** MonitorExit[var0] (shouldn't be in output)
            return theInstance;
        }
    }

    void addGroup(String displayName) {
        Preferences prefs = GroupsManager.getPreferences();
        int groupIndex = this.getGroups().size();
        String groupName = "group" + groupIndex;
        try {
            while (prefs.nodeExists(groupName = "group" + ++groupIndex)) {
            }
        }
        catch (BackingStoreException ex) {
            LOG.log(Level.INFO, null, ex);
        }
        prefs.put("selectedDocumentGroup", groupName);
        prefs.node(groupName).put("displayName", displayName);
    }

    void removeAllGroups() {
        List<DocumentGroupImpl> groups = this.getGroups();
        for (DocumentGroupImpl group : groups) {
            this.removeGroup(group);
        }
    }

    DocumentGroupImpl getCurrentGroup() {
        String selGroupName = GroupsManager.getPreferences().get("selectedDocumentGroup", "");
        return selGroupName.isEmpty() ? null : this.createGroup(selGroupName);
    }

    private DocumentGroupImpl createGroup(String groupName) {
        Preferences prefs = GroupsManager.getPreferences().node(groupName);
        String displayName = prefs.get("displayName", groupName);
        return new DocumentGroupImpl(groupName, displayName);
    }

    List<DocumentGroupImpl> getGroups() {
        Preferences prefs = GroupsManager.getPreferences();
        try {
            String[] names = prefs.childrenNames();
            ArrayList<DocumentGroupImpl> res = new ArrayList<DocumentGroupImpl>(names.length);
            for (String name : names) {
                res.add(this.createGroup(name));
            }
            Collections.sort(res);
            return res;
        }
        catch (BackingStoreException e) {
            LOG.log(Level.INFO, null, e);
            return Collections.emptyList();
        }
    }

    static Preferences getPreferences() {
        return NbPreferences.forModule(DocumentGroupImpl.class).node("DocumentGroups");
    }

    boolean openGroup(DocumentGroupImpl group) {
        DocumentGroupImpl current = this.getCurrentGroup();
        if (null != current && !current.close()) {
            return false;
        }
        WindowManagerImpl wmi = WindowManagerImpl.getInstance();
        for (TopComponent tc : TopComponent.getRegistry().getOpened()) {
            if (!wmi.isEditorTopComponent(tc) || tc.close()) continue;
            return false;
        }
        ArrayList<ModeImpl> emptyModes = new ArrayList<ModeImpl>(10);
        for (ModeImpl mode2 : wmi.getModes()) {
            if (mode2.isPermanent() || mode2.getKind() != 1 || !mode2.getOpenedTopComponentsIDs().isEmpty()) continue;
            emptyModes.add(mode2);
        }
        for (ModeImpl mode : emptyModes) {
            wmi.removeMode(mode);
        }
        String name = group.getName();
        Preferences prefs = GroupsManager.getPreferences().node(name);
        File userDir = Places.getUserDirectory();
        File root = new File(new File(userDir, "config"), "DocumentGroups");
        File groupDir = new File(root, name);
        FileObject groupFO = FileUtil.toFileObject((File)groupDir);
        if (null != groupFO) {
            HashMap<ModeImpl, ModeConfig> mode2config = new HashMap<ModeImpl, ModeConfig>(10);
            HashMap<String, ModeImpl> tcid2mode = new HashMap<String, ModeImpl>(50);
            for (FileObject fo : groupFO.getChildren()) {
                if (!fo.isData() || !"wsmode".equals(fo.getExt())) continue;
                try {
                    String[] ids;
                    ModeConfig config = WindowManagerParser.loadModeConfigFrom(fo);
                    String idList = prefs.get(config.name, "");
                    if (idList.isEmpty()) continue;
                    ModeImpl mode3 = (ModeImpl)wmi.findMode(config.name);
                    if (null == mode3) {
                        mode3 = this.createMode(config);
                    } else {
                        mode3.setConstraints(config.constraints);
                    }
                    mode2config.put(mode3, config);
                    for (String id : ids = idList.split(":")) {
                        tcid2mode.put(id, mode3);
                        mode3.addUnloadedTopComponent(id);
                    }
                    continue;
                }
                catch (IOException ex) {
                    LOG.log(Level.INFO, null, ex);
                }
            }
            DataLoader settingsLoader = null;
            Enumeration loaders = DataLoaderPool.getDefault().producersOf(InstanceDataObject.class);
            while (loaders.hasMoreElements()) {
                settingsLoader = (DataLoader)loaders.nextElement();
            }
            HashMap<String, TopComponent> id2tc = new HashMap<String, TopComponent>(50);
            FileObject[] arr$ = groupFO.getChildren();
            int len$ = arr$.length;
            boolean i$ = false;
            while (++i$ < len$) {
                FileObject fo2 = arr$[i$];
                if (!fo2.isData() || !"settings".equals(fo2.getExt())) continue;
                try {
                    DataLoaderPool.setPreferredLoader((FileObject)fo2, (DataLoader)settingsLoader);
                    DataObject dob = DataObject.find((FileObject)fo2);
                    DataLoaderPool.setPreferredLoader((FileObject)fo2, (DataLoader)null);
                    InstanceCookie ic = (InstanceCookie)dob.getCookie(InstanceCookie.class);
                    if (null != ic) {
                        TopComponent tc2 = (TopComponent)ic.instanceCreate();
                        id2tc.put(fo2.getName(), tc2);
                        continue;
                    }
                    String excAnnotation = NbBundle.getMessage(PersistenceManager.class, (String)"EXC_BrokenTCSetting", (Object)fo2.getName());
                    LOG.log(Level.INFO, "[PersistenceManager.getTopComponentForID] Problem when deserializing TopComponent for tcID:'" + fo2.getName() + "'. Reason: " + excAnnotation);
                    continue;
                }
                catch (Exception ex) {
                    LOG.log(Level.INFO, null, ex);
                }
            }
            HashMap<String, String> oldId2newId = new HashMap<String, String>(id2tc.size());
            for (String oldId : id2tc.keySet()) {
                TopComponent tc3 = (TopComponent)id2tc.get(oldId);
                ModeImpl mode4 = (ModeImpl)tcid2mode.get(oldId);
                if (null != mode4) {
                    mode4.dockInto(tc3);
                }
                tc3.open();
                oldId2newId.put(oldId, wmi.findTopComponentID(tc3));
            }
            for (ModeImpl mode5 : wmi.getModes()) {
                TopComponent tc4;
                ModeConfig config = (ModeConfig)mode2config.get(mode5);
                if (null == config) continue;
                String selectedId = config.selectedTopComponentID;
                if (null != selectedId) {
                    selectedId = (String)oldId2newId.get(selectedId);
                }
                if (null == selectedId || !mode5.getOpenedTopComponentsIDs().contains(selectedId) || null == (tc4 = wmi.findTopComponent(selectedId))) continue;
                mode5.setSelectedTopComponent(tc4);
            }
        }
        GroupsManager.getPreferences().put("selectedDocumentGroup", group.getName());
        return true;
    }

    boolean closeGroup(DocumentGroupImpl group) {
        TopComponent welcomeTc = WindowManager.getDefault().findTopComponent("Welcome");
        boolean welcomeWasOpened = null != welcomeTc && welcomeTc.isOpened();
        ((WindowSystem)Lookup.getDefault().lookup(WindowSystem.class)).save();
        WindowManagerImpl wmi = WindowManagerImpl.getInstance();
        ArrayList<TopComponent> documents = new ArrayList<TopComponent>(TopComponent.getRegistry().getOpened().size());
        for (TopComponent tc : TopComponent.getRegistry().getOpened()) {
            if (!wmi.isEditorTopComponent(tc)) continue;
            documents.add(tc);
        }
        String name = group.getName();
        File userDir = Places.getUserDirectory();
        File root = new File(new File(userDir, "config"), "DocumentGroups");
        File groupDir = new File(root, name);
        GroupsManager.deleteAll(groupDir);
        groupDir.mkdirs();
        FileObject groupFO = FileUtil.toFileObject((File)groupDir);
        Preferences prefs = GroupsManager.getPreferences().node(name);
        try {
            prefs.clear();
        }
        catch (BackingStoreException ex) {
            LOG.log(Level.INFO, null, ex);
        }
        prefs.put("displayName", group.toString());
        File configRoot = new File(new File(Places.getUserDirectory(), "config"), "Windows2Local");
        File modesRoot = new File(configRoot, "Modes");
        for (ModeImpl mode : wmi.getModes()) {
            FileObject modeFO;
            String modeName;
            if (mode.getKind() != 1 || null == (modeFO = FileUtil.toFileObject((File)new File(modesRoot, (modeName = mode.getName()) + "." + "wsmode")))) continue;
            try {
                modeFO.copy(groupFO, modeName, "wsmode");
            }
            catch (IOException ex) {
                LOG.log(Level.INFO, null, ex);
                continue;
            }
            StringBuilder sb = new StringBuilder();
            for (String id : mode.getOpenedTopComponentsIDs()) {
                sb.append(id);
                sb.append(":");
            }
            prefs.put(modeName, sb.toString());
        }
        File componentRoot = new File(configRoot, "Components");
        for (TopComponent tc2 : documents) {
            String id = wmi.findTopComponentID(tc2);
            if (tc2.equals((Object)welcomeTc) && !welcomeWasOpened) continue;
            FileObject tcFO = FileUtil.toFileObject((File)new File(componentRoot, id + "." + "settings"));
            try {
                tcFO.copy(groupFO, id, "settings");
            }
            catch (IOException ex) {
                LOG.log(Level.INFO, null, ex);
            }
        }
        GroupsManager.getPreferences().put("selectedDocumentGroup", "");
        return true;
    }

    private ModeImpl createMode(ModeConfig config) {
        WindowManagerImpl wmi = WindowManagerImpl.getInstance();
        ModeImpl res = wmi.createMode(config.name, config.kind, config.state, false, config.constraints);
        Rectangle absBounds = config.bounds == null ? new Rectangle() : config.bounds;
        Rectangle relBounds = config.relativeBounds == null ? new Rectangle() : config.relativeBounds;
        Rectangle bounds = PersistenceHandler.computeBounds(false, false, absBounds.x, absBounds.y, absBounds.width, absBounds.height, (float)relBounds.x / 100.0f, (float)relBounds.y / 100.0f, (float)relBounds.width / 100.0f, (float)relBounds.height / 100.0f);
        res.setBounds(bounds);
        res.setFrameState(config.frameState);
        res.setMinimized(config.minimized);
        return res;
    }

    void removeGroup(DocumentGroupImpl group) {
        try {
            Preferences prefs = GroupsManager.getPreferences();
            if (group.getName().equals(prefs.get("selectedDocumentGroup", ""))) {
                prefs.put("selectedDocumentGroup", "");
            }
            prefs = prefs.node(group.getName());
            prefs.removeNode();
        }
        catch (Exception e) {
            LOG.log(Level.INFO, "Failed to remove document group '" + group.toString() + "'", e);
        }
    }

    private static void deleteAll(File dir) {
        File[] dirContents = dir.listFiles();
        if (null == dirContents) {
            return;
        }
        for (File f : dirContents) {
            if (f.isDirectory()) {
                GroupsManager.deleteAll(f);
            }
            f.delete();
        }
    }

    static boolean closeAllDocuments() {
        TopComponent[] tcs;
        for (TopComponent tc : tcs = WindowManagerImpl.getInstance().getEditorTopComponents()) {
            if (!Switches.isClosingEnabled(tc)) continue;
            tc.putClientProperty((Object)"inCloseAll", (Object)Boolean.TRUE);
            if (tc.close()) continue;
            return false;
        }
        return true;
    }

    static {
        LOG = Logger.getLogger(GroupsManager.class.getName());
    }
}

