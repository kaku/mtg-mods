/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 */
package org.netbeans.core.windows.documentgroup;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.documentgroup.DocumentGroupImpl;
import org.netbeans.core.windows.documentgroup.GroupsManager;
import org.netbeans.core.windows.documentgroup.ManageGroupsPanel;
import org.netbeans.core.windows.documentgroup.NewGroupPanel;
import org.netbeans.core.windows.documentgroup.PleaseWait;
import org.openide.util.NbBundle;
import org.openide.util.actions.Presenter;

public class GroupsMenuAction
extends AbstractAction
implements Presenter.Menu {
    private static final JMenu menu = new JMenu(NbBundle.getMessage(GroupsMenuAction.class, (String)"Menu_DOCUMENT_GROUPS"));

    private GroupsMenuAction() {
        super(NbBundle.getMessage(GroupsMenuAction.class, (String)"Menu_DOCUMENT_GROUPS"));
    }

    public static AbstractAction create() {
        return new GroupsMenuAction();
    }

    public JMenuItem getMenuPresenter() {
        if (EventQueue.isDispatchThread()) {
            GroupsMenuAction.fillMenu(menu);
        } else {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    GroupsMenuAction.fillMenu(menu);
                }
            });
        }
        return menu;
    }

    static void refreshMenu() {
        GroupsMenuAction.fillMenu(menu);
    }

    private static void fillMenu(JMenu menu) {
        menu.removeAll();
        GroupsManager gm = GroupsManager.getDefault();
        DocumentGroupImpl current = gm.getCurrentGroup();
        List<DocumentGroupImpl> groups = gm.getGroups();
        menu.add(new AbstractAction(NbBundle.getMessage(GroupsMenuAction.class, (String)"CTL_NewGroupAction")){

            @Override
            public void actionPerformed(ActionEvent e) {
                NewGroupPanel panel = new NewGroupPanel();
                panel.showDialog();
            }
        });
        if (!groups.isEmpty()) {
            menu.addSeparator();
            JRadioButtonMenuItem item = new JRadioButtonMenuItem(new AbstractAction(NbBundle.getMessage(GroupsMenuAction.class, (String)"CTL_CloseDocumentGroupAction")){

                @Override
                public void actionPerformed(ActionEvent e) {
                    GroupsMenuAction.closeGroup();
                }
            });
            item.setSelected(null == current);
            menu.add(item);
            for (DocumentGroupImpl group : groups) {
                item = new JRadioButtonMenuItem(new OpenGroupAction(group));
                item.setSelected(group.equals(current));
                menu.add(item);
            }
            menu.addSeparator();
            menu.add(new AbstractAction(NbBundle.getMessage(GroupsMenuAction.class, (String)"CTL_ManageGroupsAction")){

                @Override
                public void actionPerformed(ActionEvent e) {
                    ManageGroupsPanel panel = new ManageGroupsPanel();
                    panel.showDialog();
                }
            });
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }

    private static void closeGroup() {
        final DocumentGroupImpl selGroup = GroupsManager.getDefault().getCurrentGroup();
        if (null != selGroup) {
            final PleaseWait wait = new PleaseWait();
            wait.install();
            SwingUtilities.invokeLater(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    try {
                        if (selGroup.close()) {
                            GroupsManager.closeAllDocuments();
                        }
                        GroupsMenuAction.refreshMenu();
                    }
                    finally {
                        wait.uninstall();
                    }
                }
            });
        }
    }

    private static class OpenGroupAction
    extends AbstractAction {
        private final DocumentGroupImpl group;

        public OpenGroupAction(DocumentGroupImpl group) {
            super(group.toString());
            this.group = group;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            final PleaseWait wait = new PleaseWait();
            wait.install();
            SwingUtilities.invokeLater(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    try {
                        OpenGroupAction.this.group.open();
                        GroupsMenuAction.refreshMenu();
                    }
                    finally {
                        wait.uninstall();
                    }
                }
            });
        }

    }

}

