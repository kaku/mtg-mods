/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.prefs.Preferences;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.options.WinSysPrefs;
import org.openide.windows.TopComponent;

final class TopComponentSubModel {
    private final List<TopComponent> openedTopComponents = new ArrayList<TopComponent>(10);
    private final List<String> tcIDs = new ArrayList<String>(10);
    private final int kind;
    private String selectedTopComponentID;
    private String previousSelectedTopComponentID;
    private static final String IS_SLIDING = "isSliding";

    public TopComponentSubModel(int kind) {
        this.kind = kind;
    }

    public List<TopComponent> getTopComponents() {
        ArrayList<TopComponent> l = new ArrayList<TopComponent>(this.openedTopComponents);
        ArrayList<String> ids = new ArrayList<String>(this.tcIDs);
        ArrayList<TopComponent> ll = new ArrayList<TopComponent>(ids.size());
        Iterator<String> it = ids.iterator();
        while (it.hasNext()) {
            String tcID = it.next();
            TopComponent tc = TopComponentSubModel.getTopComponent(tcID);
            if (tc != null) {
                ll.add(tc);
                continue;
            }
            it.remove();
        }
        ll.removeAll(this.openedTopComponents);
        l.addAll(ll);
        return l;
    }

    public List<TopComponent> getOpenedTopComponents() {
        return new ArrayList<TopComponent>(this.openedTopComponents);
    }

    public boolean addOpenedTopComponent(TopComponent tc) {
        TopComponent persTC;
        if (this.openedTopComponents.contains((Object)tc)) {
            return false;
        }
        String tcID = TopComponentSubModel.getID(tc);
        int index = this.tcIDs.indexOf(tcID);
        int position = this.openedTopComponents.size();
        if (index >= 0) {
            for (TopComponent otc : this.openedTopComponents) {
                String otcID = TopComponentSubModel.getID(otc);
                int openedIndex = this.tcIDs.indexOf(otcID);
                if (openedIndex < index) continue;
                position = this.openedTopComponents.indexOf((Object)otc);
                break;
            }
        }
        if (this.kind == 1 && WinSysPrefs.HANDLER.getBoolean("editor.open.next.to.active", false) && this.selectedTopComponentID != null) {
            for (int i = 0; i < this.openedTopComponents.size(); ++i) {
                if (!this.selectedTopComponentID.equals(TopComponentSubModel.getID(this.openedTopComponents.get(i)))) continue;
                position = i + 1;
                break;
            }
        }
        if ((persTC = TopComponentSubModel.getTopComponent(tcID)) != tc) {
            String message = "Model in inconsistent state, generated TC ID=" + tcID + " for " + tc.getClass() + ":" + tc.hashCode() + " but" + " that ID is reserved for TC=" + persTC.getClass() + ":" + persTC.hashCode();
            assert (false);
        }
        this.openedTopComponents.add(position, tc);
        if (!this.tcIDs.contains(tcID)) {
            this.tcIDs.add(tcID);
        }
        if (this.selectedTopComponentID == null && !this.isNullSelectionAllowed()) {
            this.selectedTopComponentID = tcID;
        }
        if (this.kind == 2) {
            this.setSlidingProperty(tc);
        } else {
            this.clearSlidingProperty(tc);
        }
        return true;
    }

    public boolean insertOpenedTopComponent(TopComponent tc, int index) {
        if (index >= 0 && !this.openedTopComponents.isEmpty() && this.openedTopComponents.size() > index && this.openedTopComponents.get(index) == tc) {
            return false;
        }
        this.openedTopComponents.remove((Object)tc);
        int position = index;
        if (position < 0) {
            position = 0;
        } else if (position > this.openedTopComponents.size()) {
            position = this.openedTopComponents.size();
        }
        String tcID = TopComponentSubModel.getID(tc);
        this.tcIDs.remove(tcID);
        this.openedTopComponents.add(position, tc);
        if (position == 0) {
            this.tcIDs.add(0, tcID);
        } else {
            TopComponent previous = this.openedTopComponents.get(position - 1);
            int previousIndex = this.tcIDs.indexOf(TopComponentSubModel.getID(previous));
            this.tcIDs.add(previousIndex + 1, tcID);
        }
        if (this.selectedTopComponentID == null && !this.isNullSelectionAllowed()) {
            this.selectedTopComponentID = TopComponentSubModel.getID(tc);
        }
        if (this.kind == 2) {
            this.setSlidingProperty(tc);
        } else {
            this.clearSlidingProperty(tc);
        }
        return true;
    }

    public boolean addClosedTopComponent(TopComponent tc) {
        int index = this.openedTopComponents.indexOf((Object)tc);
        String tcID = TopComponentSubModel.getID(tc);
        if (!this.tcIDs.contains(tcID)) {
            this.tcIDs.add(tcID);
        }
        if (index != -1) {
            this.openedTopComponents.remove((Object)tc);
            if (this.selectedTopComponentID != null && this.selectedTopComponentID.equals(TopComponentSubModel.getID(tc))) {
                this.adjustSelectedTopComponent(index, null);
            }
        }
        if (this.kind == 2) {
            this.setSlidingProperty(tc);
        } else {
            this.clearSlidingProperty(tc);
        }
        return true;
    }

    public boolean addUnloadedTopComponent(String tcID, int index) {
        if (!this.tcIDs.contains(tcID)) {
            if (index >= 0 && index < this.tcIDs.size()) {
                this.tcIDs.add(index, tcID);
            } else {
                this.tcIDs.add(tcID);
            }
        }
        return true;
    }

    public boolean removeTopComponent(TopComponent tc, TopComponent recentTc) {
        boolean res;
        String tcID = TopComponentSubModel.getID(tc);
        if (this.openedTopComponents.contains((Object)tc)) {
            if (this.selectedTopComponentID != null && this.selectedTopComponentID.equals(tcID)) {
                int index = this.openedTopComponents.indexOf((Object)TopComponentSubModel.getTopComponent(this.selectedTopComponentID));
                this.openedTopComponents.remove((Object)tc);
                this.adjustSelectedTopComponent(index, recentTc);
            } else {
                this.openedTopComponents.remove((Object)tc);
            }
            this.tcIDs.remove(tcID);
            res = true;
        } else if (this.tcIDs.contains(tcID)) {
            this.tcIDs.remove(tcID);
            res = true;
        } else {
            res = false;
        }
        this.clearSlidingProperty(tc);
        return res;
    }

    public boolean containsTopComponent(TopComponent tc) {
        return this.openedTopComponents.contains((Object)tc) || this.tcIDs.contains(TopComponentSubModel.getID(tc));
    }

    public int getOpenedTopComponentTabPosition(TopComponent tc) {
        return this.openedTopComponents.indexOf((Object)tc);
    }

    public boolean isEmpty() {
        return this.tcIDs.isEmpty();
    }

    private void adjustSelectedTopComponent(int index, TopComponent recentTc) {
        if (this.openedTopComponents.isEmpty() || this.isNullSelectionAllowed()) {
            this.selectedTopComponentID = null;
            return;
        }
        if (null != recentTc && this.openedTopComponents.contains((Object)recentTc)) {
            this.selectedTopComponentID = TopComponentSubModel.getID(recentTc);
        } else {
            if (index > this.openedTopComponents.size() - 1) {
                index = this.openedTopComponents.size() - 1;
            }
            this.selectedTopComponentID = TopComponentSubModel.getID(this.openedTopComponents.get(index));
        }
    }

    private boolean isNullSelectionAllowed() {
        return this.kind == 2;
    }

    public void setSelectedTopComponent(TopComponent tc) {
        if (tc != null && !this.openedTopComponents.contains((Object)tc)) {
            return;
        }
        this.selectedTopComponentID = tc == null && this.isNullSelectionAllowed() ? null : TopComponentSubModel.getID(tc);
    }

    public void setPreviousSelectedTopComponentID(String tcId) {
        this.previousSelectedTopComponentID = tcId;
    }

    public void setUnloadedSelectedTopComponent(String tcID) {
        if (tcID != null && !this.getOpenedTopComponentsIDs().contains(tcID)) {
            return;
        }
        this.selectedTopComponentID = tcID;
    }

    public void setUnloadedPreviousSelectedTopComponent(String tcID) {
        this.previousSelectedTopComponentID = tcID;
    }

    public List<String> getOpenedTopComponentsIDs() {
        ArrayList<String> l = new ArrayList<String>(this.openedTopComponents.size());
        Iterator<TopComponent> it = this.openedTopComponents.iterator();
        while (it.hasNext()) {
            l.add(TopComponentSubModel.getID(it.next()));
        }
        return l;
    }

    public List<String> getClosedTopComponentsIDs() {
        ArrayList<String> closedIDs = new ArrayList<String>(this.tcIDs);
        closedIDs.removeAll(this.getOpenedTopComponentsIDs());
        return closedIDs;
    }

    public List<String> getTopComponentsIDs() {
        return new ArrayList<String>(this.tcIDs);
    }

    public void removeClosedTopComponentID(String tcID) {
        this.tcIDs.remove(tcID);
    }

    public TopComponent getSelectedTopComponent() {
        return TopComponentSubModel.getTopComponent(this.selectedTopComponentID);
    }

    public String getPreviousSelectedTopComponentID() {
        return this.previousSelectedTopComponentID;
    }

    private static TopComponent getTopComponent(String tcID) {
        return WindowManagerImpl.getInstance().getTopComponentForID(tcID);
    }

    private static String getID(TopComponent tc) {
        return WindowManagerImpl.getInstance().findTopComponentID(tc);
    }

    private void setSlidingProperty(TopComponent tc) {
        tc.putClientProperty((Object)"isSliding", (Object)Boolean.TRUE);
    }

    private void clearSlidingProperty(TopComponent tc) {
        tc.putClientProperty((Object)"isSliding", (Object)null);
    }

    void setOpenedTopComponents(List<TopComponent> opened) {
        this.openedTopComponents.clear();
        this.openedTopComponents.addAll(opened);
    }
}

