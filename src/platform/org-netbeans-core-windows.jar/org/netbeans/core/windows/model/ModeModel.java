/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.model;

import java.awt.Rectangle;
import java.util.Collection;
import java.util.List;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.SplitConstraint;
import org.openide.windows.TopComponent;

interface ModeModel {
    public void setName(String var1);

    public void setState(int var1);

    public void setBounds(Rectangle var1);

    public void setBoundsSeparatedHelp(Rectangle var1);

    public void setFrameState(int var1);

    public void setSelectedTopComponent(TopComponent var1);

    public void setPreviousSelectedTopComponentID(String var1);

    public void addOpenedTopComponent(TopComponent var1);

    public void insertOpenedTopComponent(TopComponent var1, int var2);

    public void addClosedTopComponent(TopComponent var1);

    public void addUnloadedTopComponent(String var1, int var2);

    public void setUnloadedSelectedTopComponent(String var1);

    public void setUnloadedPreviousSelectedTopComponent(String var1);

    public void removeTopComponent(TopComponent var1, TopComponent var2);

    public void removeClosedTopComponentID(String var1);

    public void setTopComponentPreviousMode(String var1, ModeImpl var2, int var3);

    public void setTopComponentPreviousConstraints(String var1, SplitConstraint[] var2);

    public String getName();

    public Rectangle getBounds();

    public Rectangle getBoundsSeparatedHelp();

    public int getState();

    public int getKind();

    public int getFrameState();

    public boolean isPermanent();

    public void makePermanent();

    public boolean isEmpty();

    public boolean containsTopComponent(TopComponent var1);

    public TopComponent getSelectedTopComponent();

    public String getPreviousSelectedTopComponentID();

    public List<TopComponent> getTopComponents();

    public List<TopComponent> getOpenedTopComponents();

    public List<String> getOpenedTopComponentsIDs();

    public List<String> getClosedTopComponentsIDs();

    public List<String> getTopComponentsIDs();

    public ModeImpl getTopComponentPreviousMode(String var1);

    public int getTopComponentPreviousIndex(String var1);

    public SplitConstraint[] getTopComponentPreviousConstraints(String var1);

    public int getOpenedTopComponentTabPosition(TopComponent var1);

    public boolean isMinimized();

    public void setMinimized(boolean var1);

    public Collection<String> getOtherNames();

    public void addOtherName(String var1);
}

