/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.model;

import java.util.HashMap;
import java.util.Map;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.SplitConstraint;

final class TopComponentContextSubModel {
    private final Map<String, Context> tcID2Contexts = new HashMap<String, Context>(10);

    public void setTopComponentPreviousConstraints(String tcID, SplitConstraint[] constraints) {
        Context context = this.tcID2Contexts.get(tcID);
        if (context == null) {
            context = new Context();
            this.tcID2Contexts.put(tcID, context);
        }
        context.constraints = constraints;
    }

    public void setTopComponentPreviousMode(String tcID, ModeImpl mode, int index) {
        Context context = this.tcID2Contexts.get(tcID);
        if (context == null) {
            context = new Context();
            this.tcID2Contexts.put(tcID, context);
        }
        context.mode = mode;
        context.index = index;
    }

    public SplitConstraint[] getTopComponentPreviousConstraints(String tcID) {
        Context context = this.tcID2Contexts.get(tcID);
        return context == null ? null : context.constraints;
    }

    public ModeImpl getTopComponentPreviousMode(String tcID) {
        Context context = this.tcID2Contexts.get(tcID);
        return context == null ? null : context.mode;
    }

    public int getTopComponentPreviousIndex(String tcID) {
        Context context = this.tcID2Contexts.get(tcID);
        return context == null ? -1 : context.index;
    }

    private static final class Context {
        ModeImpl mode;
        int index = -1;
        SplitConstraint[] constraints;

        private Context() {
        }
    }

}

