/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.model;

import java.util.Collection;
import java.util.Set;
import org.openide.windows.TopComponent;

interface TopComponentGroupModel {
    public String getName();

    public void open(Collection<TopComponent> var1, Collection<TopComponent> var2);

    public void close();

    public boolean isOpened();

    public Set<TopComponent> getTopComponents();

    public Set<TopComponent> getOpenedTopComponents();

    public Set<TopComponent> getOpenedBeforeTopComponents();

    public Set<TopComponent> getOpeningTopComponents();

    public Set<TopComponent> getClosingTopComponents();

    public boolean addUnloadedTopComponent(String var1);

    public boolean removeUnloadedTopComponent(String var1);

    public boolean addOpeningTopComponent(TopComponent var1);

    public boolean removeOpeningTopComponent(TopComponent var1);

    public boolean addUnloadedOpeningTopComponent(String var1);

    public boolean removeUnloadedOpeningTopComponent(String var1);

    public boolean addUnloadedClosingTopComponent(String var1);

    public boolean removeUnloadedClosingTopComponent(String var1);

    public boolean addUnloadedOpenedTopComponent(String var1);

    public Set<String> getTopComponentsIDs();

    public Set<String> getOpeningSetIDs();

    public Set<String> getClosingSetIDs();

    public Set<String> getOpenedTopComponentsIDs();
}

