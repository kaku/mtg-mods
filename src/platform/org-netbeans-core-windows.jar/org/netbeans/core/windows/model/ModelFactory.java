/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.model;

import org.netbeans.core.windows.model.DefaultModel;
import org.netbeans.core.windows.model.Model;

public final class ModelFactory {
    private ModelFactory() {
    }

    public static Model createWindowSystemModel() {
        return new DefaultModel();
    }
}

