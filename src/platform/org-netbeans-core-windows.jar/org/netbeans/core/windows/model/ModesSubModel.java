/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.model;

import java.util.AbstractMap;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.ModeStructureSnapshot;
import org.netbeans.core.windows.SplitConstraint;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.model.EditorSplitSubModel;
import org.netbeans.core.windows.model.Model;
import org.netbeans.core.windows.model.ModelElement;
import org.netbeans.core.windows.model.SplitSubModel;
import org.openide.windows.TopComponent;

final class ModesSubModel {
    private final Model parentModel;
    private final Set<ModeImpl> modes = new HashSet<ModeImpl>(10);
    private final EditorSplitSubModel editorSplitSubModel;
    private final HashMap<ModeImpl, String> slidingModes2Sides = new HashMap(5);
    private final HashMap<String, ModeImpl> slidingSides2Modes = new HashMap(5);
    private ModeImpl activeMode;
    private ModeImpl lastActiveEditorMode;
    private ModeImpl editorMaximizedMode;
    private ModeImpl viewMaximizedMode;
    private final Map<String, Integer> slideInSizes = new HashMap<String, Integer>(15);

    public ModesSubModel(Model parentModel) {
        this.parentModel = parentModel;
        this.editorSplitSubModel = new EditorSplitSubModel(parentModel, new SplitSubModel(parentModel));
    }

    public void setEditorAreaConstraints(SplitConstraint[] editorAreaConstraints) {
        this.editorSplitSubModel.setEditorNodeConstraints(editorAreaConstraints);
    }

    public SplitConstraint[] getModelElementConstraints(ModelElement element) {
        return this.editorSplitSubModel.getModelElementConstraints(element);
    }

    public SplitConstraint[] getEditorAreaConstraints() {
        return this.editorSplitSubModel.getEditorNodeConstraints();
    }

    public SplitConstraint[] getModeConstraints(ModeImpl mode) {
        if (mode.getKind() == 1) {
            return this.editorSplitSubModel.getEditorArea().getModeConstraints(mode);
        }
        return this.editorSplitSubModel.getModeConstraints(mode);
    }

    public String getSlideSideForMode(ModeImpl mode) {
        return this.editorSplitSubModel.getSlideSideForMode(mode);
    }

    public String getSlidingModeConstraints(ModeImpl mode) {
        return this.slidingModes2Sides.get(mode);
    }

    public ModeImpl getSlidingMode(String side) {
        return this.slidingSides2Modes.get(side);
    }

    public Set<ModeImpl> getSlidingModes() {
        return Collections.unmodifiableSet(this.slidingModes2Sides.keySet());
    }

    public boolean addMode(ModeImpl mode, SplitConstraint[] constraints) {
        if (this.modes.contains(mode)) {
            return false;
        }
        boolean result = mode.getKind() == 1 && mode.getState() == 0 ? this.editorSplitSubModel.getEditorArea().addMode(mode, constraints) : this.editorSplitSubModel.addMode(mode, constraints);
        if (result) {
            this.modes.add(mode);
        }
        return result;
    }

    public boolean addModeToSide(ModeImpl mode, ModeImpl attachMode, String side) {
        if (this.modes.contains(mode)) {
            return false;
        }
        boolean result = mode.getKind() == 1 ? this.editorSplitSubModel.getEditorArea().addModeToSide(mode, attachMode, side) : this.editorSplitSubModel.addModeToSide(mode, attachMode, side);
        if (result) {
            this.modes.add(mode);
        }
        return result;
    }

    public boolean addModeAround(ModeImpl mode, String side) {
        if (this.modes.contains(mode)) {
            return false;
        }
        boolean result = mode.getKind() == 1 ? false : this.editorSplitSubModel.addModeAround(mode, side);
        if (result) {
            this.modes.add(mode);
        }
        return result;
    }

    public boolean addModeAroundEditor(ModeImpl mode, String side) {
        if (this.modes.contains(mode)) {
            return false;
        }
        boolean result = mode.getKind() == 1 ? this.editorSplitSubModel.getEditorArea().addModeToSideRoot(mode, side) : this.editorSplitSubModel.addModeAroundEditor(mode, side);
        if (result) {
            this.modes.add(mode);
        }
        return result;
    }

    public boolean addModeSliding(ModeImpl mode, String side, Map<String, Integer> slideInSizes) {
        if (this.modes.contains(mode) || mode.getKind() != 2) {
            return false;
        }
        this.slidingModes2Sides.put(mode, side);
        this.slidingSides2Modes.put(side, mode);
        this.modes.add(mode);
        if (null != slideInSizes) {
            for (String tcId : slideInSizes.keySet()) {
                this.slideInSizes.put(side + tcId, slideInSizes.get(tcId));
            }
        }
        return true;
    }

    public Map<String, Integer> getSlideInSizes(String side) {
        HashMap<String, Integer> res = new HashMap<String, Integer>(5);
        for (String key : this.slideInSizes.keySet()) {
            if (!key.startsWith(side)) continue;
            String tcId = key.substring(side.length());
            Integer size = this.slideInSizes.get(key);
            res.put(tcId, size);
        }
        return res;
    }

    public Map<TopComponent, Integer> getSlideInSizes(ModeImpl mode) {
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        TopComponent[] tcs = mode.getTopComponents();
        HashMap<TopComponent, Integer> res = new HashMap<TopComponent, Integer>(tcs.length);
        for (TopComponent tc : tcs) {
            String tcId = wm.findTopComponentID(tc);
            Integer size = this.slideInSizes.get(mode.getSide() + tcId);
            if (null == size) continue;
            res.put(tc, size);
        }
        return res;
    }

    public void setSlideInSize(String side, TopComponent tc, int size) {
        if (null != tc && null != side) {
            String tcId = WindowManagerImpl.getInstance().findTopComponentID(tc);
            this.slideInSizes.put(side + tcId, new Integer(size));
        }
    }

    public boolean removeMode(ModeImpl mode) {
        int kind = mode.getKind();
        if (kind == 2) {
            return true;
        }
        this.modes.remove(mode);
        if (mode.equals(this.lastActiveEditorMode)) {
            this.lastActiveEditorMode = null;
        }
        if (mode.getKind() == 1) {
            return this.editorSplitSubModel.getEditorArea().removeMode(mode);
        }
        return this.editorSplitSubModel.removeMode(mode);
    }

    public boolean setActiveMode(ModeImpl activeMode) {
        if (activeMode == null || this.modes.contains(activeMode)) {
            this.activeMode = activeMode;
            if (!(activeMode == null || activeMode.getKind() != 1 || activeMode.getState() == 1 && Switches.isOpenNewEditorsDocked())) {
                this.lastActiveEditorMode = activeMode;
            }
            return true;
        }
        return false;
    }

    public ModeImpl getActiveMode() {
        return this.activeMode;
    }

    public ModeImpl getLastActiveEditorMode() {
        return this.lastActiveEditorMode;
    }

    public boolean setEditorMaximizedMode(ModeImpl maximizedMode) {
        if (maximizedMode == null || this.modes.contains(maximizedMode)) {
            this.editorMaximizedMode = maximizedMode;
            return true;
        }
        return false;
    }

    public ModeImpl getEditorMaximizedMode() {
        return this.editorMaximizedMode;
    }

    public boolean setViewMaximizedMode(ModeImpl maximizedMode) {
        if (maximizedMode == null || this.modes.contains(maximizedMode)) {
            this.viewMaximizedMode = maximizedMode;
            return true;
        }
        return false;
    }

    public ModeImpl getViewMaximizedMode() {
        return this.viewMaximizedMode;
    }

    public Set<ModeImpl> getModes() {
        return new HashSet<ModeImpl>(this.modes);
    }

    public void setSplitWeights(ModelElement[] snapshots, double[] splitWeights) {
        this.editorSplitSubModel.setSplitWeights(snapshots, splitWeights);
    }

    public String toString() {
        return this.getClass().getName() + "@" + Integer.toHexString(this.hashCode()) + "\n" + this.editorSplitSubModel;
    }

    public ModeStructureSnapshot.ElementSnapshot createSplitSnapshot() {
        return this.editorSplitSubModel.createSplitSnapshot();
    }

    public Set<ModeStructureSnapshot.ModeSnapshot> createSeparateModeSnapshots() {
        HashSet<ModeStructureSnapshot.ModeSnapshot> s = new HashSet<ModeStructureSnapshot.ModeSnapshot>();
        s.addAll(this.editorSplitSubModel.createSeparateSnapshots());
        return s;
    }

    public Set<ModeStructureSnapshot.SlidingModeSnapshot> createSlidingModeSnapshots() {
        HashSet<ModeStructureSnapshot.SlidingModeSnapshot> result = new HashSet<ModeStructureSnapshot.SlidingModeSnapshot>();
        for (Map.Entry<ModeImpl, String> curEntry : this.slidingModes2Sides.entrySet()) {
            final ModeImpl key = curEntry.getKey();
            AbstractMap<TopComponent, Integer> lazy = new AbstractMap<TopComponent, Integer>(){
                Map<TopComponent, Integer> delegate;

                @Override
                public Set<Map.Entry<TopComponent, Integer>> entrySet() {
                    if (this.delegate == null) {
                        this.delegate = ModesSubModel.this.getSlideInSizes(key);
                    }
                    return this.delegate.entrySet();
                }
            };
            result.add(new ModeStructureSnapshot.SlidingModeSnapshot(curEntry.getKey(), curEntry.getValue(), lazy));
        }
        return result;
    }

    public void dock(ModeImpl prevMode, ModeImpl floatingMode) {
        boolean editorMode;
        assert (this.modes.contains(prevMode));
        assert (this.modes.contains(floatingMode));
        SplitConstraint[] constraints = this.getModeConstraints(prevMode);
        boolean bl = editorMode = prevMode.getKind() == 1;
        if (editorMode) {
            this.editorSplitSubModel.getEditorArea().addMode(floatingMode, constraints);
            this.removeMode(prevMode);
            this.editorSplitSubModel.removeMode(floatingMode);
        } else {
            this.editorSplitSubModel.removeMode(floatingMode);
            this.editorSplitSubModel.addMode(floatingMode, constraints);
            this.removeMode(prevMode);
        }
    }

    public ModeImpl getModeForOriginator(ModelElement originator) {
        ModeImpl mode = this.editorSplitSubModel.getModeForOriginator(originator);
        if (this.modes.contains(mode)) {
            return mode;
        }
        return null;
    }

}

