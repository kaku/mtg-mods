/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.model.TopComponentGroupModel;
import org.openide.windows.TopComponent;

final class DefaultTopComponentGroupModel
implements TopComponentGroupModel {
    private final String name;
    private boolean opened;
    private final Set<String> topComponents = new HashSet<String>(3);
    private final Set<String> openedTopComponents = new HashSet<String>(3);
    private final Set<String> openedBeforeTopComponents = new HashSet<String>(3);
    private final Set<String> openingTopComponents = new HashSet<String>(3);
    private final Set<String> closingTopComponents = new HashSet<String>(3);
    private final Object LOCK_OPENED = new Object();
    private final Object LOCK_TOPCOMPONENTS = new Object();

    public DefaultTopComponentGroupModel(String name, boolean opened) {
        this.name = name;
        this.opened = opened;
    }

    @Override
    public String getName() {
        return this.name;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void open(Collection<TopComponent> openedTopComponents, Collection<TopComponent> openedBeforeTopComponents) {
        Object object = this.LOCK_OPENED;
        synchronized (object) {
            String tcID;
            this.opened = true;
            this.openedTopComponents.clear();
            for (TopComponent tc2 : openedTopComponents) {
                tcID = DefaultTopComponentGroupModel.getID(tc2);
                if (tcID == null) continue;
                this.openedTopComponents.add(tcID);
            }
            this.openedBeforeTopComponents.clear();
            for (TopComponent tc2 : openedBeforeTopComponents) {
                tcID = DefaultTopComponentGroupModel.getID(tc2);
                if (tcID == null) continue;
                this.openedBeforeTopComponents.add(tcID);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void close() {
        Object object = this.LOCK_OPENED;
        synchronized (object) {
            this.opened = false;
            this.openedTopComponents.clear();
            this.openedBeforeTopComponents.clear();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean isOpened() {
        Object object = this.LOCK_OPENED;
        synchronized (object) {
            return this.opened;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Set<TopComponent> getTopComponents() {
        HashSet<String> s;
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            s = new HashSet<String>(this.topComponents);
        }
        HashSet<TopComponent> result = new HashSet<TopComponent>(s.size());
        for (String tcId : s) {
            TopComponent tc = DefaultTopComponentGroupModel.getTopComponent(tcId);
            if (tc == null) continue;
            result.add(tc);
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Set<TopComponent> getOpenedTopComponents() {
        HashSet<String> s;
        Object object = this.LOCK_OPENED;
        synchronized (object) {
            s = new HashSet<String>(this.openedTopComponents);
        }
        HashSet<TopComponent> result = new HashSet<TopComponent>(s.size());
        for (String tcId : s) {
            TopComponent tc = DefaultTopComponentGroupModel.getTopComponent(tcId);
            if (tc == null) continue;
            result.add(tc);
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Set<TopComponent> getOpenedBeforeTopComponents() {
        HashSet<String> s;
        Object object = this.LOCK_OPENED;
        synchronized (object) {
            s = new HashSet<String>(this.openedBeforeTopComponents);
        }
        HashSet<TopComponent> result = new HashSet<TopComponent>(s.size());
        for (String tcId : s) {
            TopComponent tc = DefaultTopComponentGroupModel.getTopComponent(tcId);
            if (tc == null) continue;
            result.add(tc);
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Set<TopComponent> getOpeningTopComponents() {
        HashSet<String> s;
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            s = new HashSet<String>(this.openingTopComponents);
        }
        HashSet<TopComponent> result = new HashSet<TopComponent>(s.size());
        for (String tcId : s) {
            TopComponent tc = DefaultTopComponentGroupModel.getTopComponent(tcId);
            if (tc == null) continue;
            result.add(tc);
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Set<TopComponent> getClosingTopComponents() {
        HashSet<String> s;
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            s = new HashSet<String>(this.closingTopComponents);
        }
        HashSet<TopComponent> result = new HashSet<TopComponent>(s.size());
        for (String tcId : s) {
            TopComponent tc = DefaultTopComponentGroupModel.getTopComponent(tcId);
            if (tc == null) continue;
            result.add(tc);
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean addUnloadedTopComponent(String tcID) {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            return this.topComponents.add(tcID);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean removeUnloadedTopComponent(String tcID) {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            if (this.openingTopComponents.contains(tcID)) {
                this.openingTopComponents.remove(tcID);
            }
            if (this.closingTopComponents.contains(tcID)) {
                this.closingTopComponents.remove(tcID);
            }
            return this.topComponents.remove(tcID);
        }
    }

    @Override
    public boolean addOpeningTopComponent(TopComponent tc) {
        return this.addUnloadedOpeningTopComponent(DefaultTopComponentGroupModel.getID(tc));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean addUnloadedOpeningTopComponent(String tcID) {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            if (!this.topComponents.contains(tcID)) {
                this.topComponents.add(tcID);
            }
            return this.openingTopComponents.add(tcID);
        }
    }

    @Override
    public boolean removeOpeningTopComponent(TopComponent tc) {
        return this.removeUnloadedOpeningTopComponent(DefaultTopComponentGroupModel.getID(tc));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean removeUnloadedOpeningTopComponent(String tcID) {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            return this.openingTopComponents.remove(tcID);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean addUnloadedClosingTopComponent(String tcID) {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            if (!this.topComponents.contains(tcID)) {
                this.topComponents.add(tcID);
            }
            return this.closingTopComponents.add(tcID);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean removeUnloadedClosingTopComponent(String tcID) {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            return this.closingTopComponents.remove(tcID);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean addUnloadedOpenedTopComponent(String tcID) {
        Object object = this.LOCK_OPENED;
        synchronized (object) {
            if (!this.opened) {
                return false;
            }
            this.openedTopComponents.add(tcID);
        }
        return true;
    }

    private static TopComponent getTopComponent(String tcID) {
        return WindowManagerImpl.getInstance().getTopComponentForID(tcID);
    }

    private static String getID(TopComponent tc) {
        return WindowManagerImpl.getInstance().findTopComponentID(tc);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Set<String> getTopComponentsIDs() {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            return new HashSet<String>(this.topComponents);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Set<String> getOpeningSetIDs() {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            return new HashSet<String>(this.openingTopComponents);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Set<String> getClosingSetIDs() {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            return new HashSet<String>(this.closingTopComponents);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Set<String> getOpenedTopComponentsIDs() {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            return new HashSet<String>(this.openedTopComponents);
        }
    }
}

