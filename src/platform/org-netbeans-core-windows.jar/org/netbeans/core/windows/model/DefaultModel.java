/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.RetainLocation
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.model;

import java.awt.Rectangle;
import java.lang.annotation.Annotation;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.ModeStructureSnapshot;
import org.netbeans.core.windows.SplitConstraint;
import org.netbeans.core.windows.TopComponentGroupImpl;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.WindowSystemSnapshot;
import org.netbeans.core.windows.model.DefaultModeModel;
import org.netbeans.core.windows.model.DefaultTopComponentGroupModel;
import org.netbeans.core.windows.model.DockingStatus;
import org.netbeans.core.windows.model.ModeModel;
import org.netbeans.core.windows.model.Model;
import org.netbeans.core.windows.model.ModelElement;
import org.netbeans.core.windows.model.ModesSubModel;
import org.netbeans.core.windows.model.TopComponentGroupModel;
import org.openide.windows.RetainLocation;
import org.openide.windows.TopComponent;

final class DefaultModel
implements Model {
    private final Map<ModeImpl, ModeModel> mode2model = new WeakHashMap<ModeImpl, ModeModel>(10);
    private final Map<TopComponentGroupImpl, TopComponentGroupModel> group2model = new WeakHashMap<TopComponentGroupImpl, TopComponentGroupModel>(10);
    private boolean visible = false;
    private final Rectangle mainWindowBoundsJoined = new Rectangle();
    private final Rectangle mainWindowBoundsSeparated = new Rectangle();
    private final Rectangle mainWindowBoundsSeparatedHelp = new Rectangle();
    private int mainWindowFrameStateJoined = 0;
    private int mainWindowFrameStateSeparated = 0;
    private int editorAreaState = 0;
    private final Rectangle editorAreaBounds = new Rectangle();
    private final Rectangle editorAreaBoundsHelp = new Rectangle();
    private int editorAreaFrameState = 0;
    private String toolbarConfigName = "Standard";
    private DockingStatus maximizedDockingStatus;
    private DockingStatus defaultDockingStatus;
    private Set<String> slideInMaximizedTopComponents;
    private ModesSubModel modesSubModel;
    private final Set<TopComponentGroupImpl> topComponentGroups;
    private final Object LOCK_VISIBLE;
    private final Object LOCK_MAIN_WINDOW_BOUNDS_JOINED;
    private final Object LOCK_MAIN_WINDOW_BOUNDS_SEPARATED;
    private final Object LOCK_MAIN_WINDOW_BOUNDS_SEPARATED_HELP;
    private final Object LOCK_MAIN_WINDOW_FRAME_STATE_JOINED;
    private final Object LOCK_MAIN_WINDOW_FRAME_STATE_SEPARATED;
    private final Object LOCK_EDITOR_AREA_STATE;
    private final Object LOCK_EDITOR_AREA_FRAME_STATE;
    private final Object LOCK_EDITOR_AREA_BOUNDS;
    private final Object LOCK_EDITOR_AREA_BOUNDS_HELP;
    private final Object LOCK_TOOLBAR_CONFIG;
    private final Object LOCK_MODES;
    private final Object LOCK_TOPCOMPONENT_GROUPS;
    private final Object LOCK_PROJECT_NAME;
    private Reference<ModeImpl> lastActiveMode;

    public DefaultModel() {
        this.maximizedDockingStatus = new DockingStatus(this);
        this.defaultDockingStatus = new DefaultDockingStatus(this);
        this.slideInMaximizedTopComponents = new HashSet<String>(3);
        this.modesSubModel = new ModesSubModel(this);
        this.topComponentGroups = new HashSet<TopComponentGroupImpl>(5);
        this.LOCK_VISIBLE = new Object();
        this.LOCK_MAIN_WINDOW_BOUNDS_JOINED = new Object();
        this.LOCK_MAIN_WINDOW_BOUNDS_SEPARATED = new Object();
        this.LOCK_MAIN_WINDOW_BOUNDS_SEPARATED_HELP = new Object();
        this.LOCK_MAIN_WINDOW_FRAME_STATE_JOINED = new Object();
        this.LOCK_MAIN_WINDOW_FRAME_STATE_SEPARATED = new Object();
        this.LOCK_EDITOR_AREA_STATE = new Object();
        this.LOCK_EDITOR_AREA_FRAME_STATE = new Object();
        this.LOCK_EDITOR_AREA_BOUNDS = new Object();
        this.LOCK_EDITOR_AREA_BOUNDS_HELP = new Object();
        this.LOCK_TOOLBAR_CONFIG = new Object();
        this.LOCK_MODES = new Object();
        this.LOCK_TOPCOMPONENT_GROUPS = new Object();
        this.LOCK_PROJECT_NAME = new Object();
        this.lastActiveMode = null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setVisible(boolean visible) {
        Object object = this.LOCK_VISIBLE;
        synchronized (object) {
            this.visible = visible;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setMainWindowBoundsJoined(Rectangle mainWindowBoundsJoined) {
        if (mainWindowBoundsJoined == null) {
            return;
        }
        Object object = this.LOCK_MAIN_WINDOW_BOUNDS_JOINED;
        synchronized (object) {
            this.mainWindowBoundsJoined.setBounds(mainWindowBoundsJoined);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setMainWindowBoundsSeparated(Rectangle mainWindowBoundsSeparated) {
        if (mainWindowBoundsSeparated == null) {
            return;
        }
        Object object = this.LOCK_MAIN_WINDOW_BOUNDS_SEPARATED;
        synchronized (object) {
            this.mainWindowBoundsSeparated.setBounds(mainWindowBoundsSeparated);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setMainWindowFrameStateJoined(int frameState) {
        Object object = this.LOCK_MAIN_WINDOW_FRAME_STATE_JOINED;
        synchronized (object) {
            this.mainWindowFrameStateJoined = frameState;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setMainWindowFrameStateSeparated(int frameState) {
        Object object = this.LOCK_MAIN_WINDOW_FRAME_STATE_SEPARATED;
        synchronized (object) {
            this.mainWindowFrameStateSeparated = frameState;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setEditorAreaBounds(Rectangle editorAreaBounds) {
        if (editorAreaBounds == null) {
            return;
        }
        Object object = this.LOCK_EDITOR_AREA_BOUNDS;
        synchronized (object) {
            this.editorAreaBounds.setBounds(editorAreaBounds);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setEditorAreaState(int editorAreaState) {
        Object object = this.LOCK_EDITOR_AREA_STATE;
        synchronized (object) {
            this.editorAreaState = editorAreaState;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setEditorAreaFrameState(int frameState) {
        Object object = this.LOCK_EDITOR_AREA_FRAME_STATE;
        synchronized (object) {
            this.editorAreaFrameState = frameState;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setEditorAreaConstraints(SplitConstraint[] editorAreaConstraints) {
        Object object = this.LOCK_MODES;
        synchronized (object) {
            this.modesSubModel.setEditorAreaConstraints(editorAreaConstraints);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setModeConstraints(ModeImpl mode, SplitConstraint[] constraints) {
        Object object = this.LOCK_MODES;
        synchronized (object) {
            this.modesSubModel.removeMode(mode);
            this.modesSubModel.addMode(mode, constraints);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void addMode(ModeImpl mode, SplitConstraint[] constraints) {
        Object object = this.LOCK_MODES;
        synchronized (object) {
            this.modesSubModel.addMode(mode, constraints);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void addModeToSide(ModeImpl mode, ModeImpl attachMode, String side) {
        Object object = this.LOCK_MODES;
        synchronized (object) {
            this.modesSubModel.addModeToSide(mode, attachMode, side);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void addModeAround(ModeImpl mode, String side) {
        Object object = this.LOCK_MODES;
        synchronized (object) {
            this.modesSubModel.addModeAround(mode, side);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void addModeAroundEditor(ModeImpl mode, String side) {
        Object object = this.LOCK_MODES;
        synchronized (object) {
            this.modesSubModel.addModeAroundEditor(mode, side);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void addSlidingMode(ModeImpl mode, String side, Map<String, Integer> slideInSizes) {
        Object object = this.LOCK_MODES;
        synchronized (object) {
            this.modesSubModel.addModeSliding(mode, side, slideInSizes);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeMode(ModeImpl mode) {
        Object object = this.LOCK_MODES;
        synchronized (object) {
            this.modesSubModel.removeMode(mode);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setActiveMode(ModeImpl activeMode) {
        if (this.lastActiveMode != null && this.lastActiveMode.get() == activeMode) {
            return;
        }
        this.lastActiveMode = new WeakReference<ModeImpl>(activeMode);
        Object object = this.LOCK_MODES;
        synchronized (object) {
            boolean success = this.modesSubModel.setActiveMode(activeMode);
            if (success) {
                this.updateSlidingSelections(activeMode);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setEditorMaximizedMode(ModeImpl maximizedMode) {
        assert (null == maximizedMode || maximizedMode.getKind() == 1);
        Object object = this.LOCK_MODES;
        synchronized (object) {
            this.modesSubModel.setEditorMaximizedMode(maximizedMode);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setViewMaximizedMode(ModeImpl maximizedMode) {
        assert (null == maximizedMode || maximizedMode.getKind() == 0);
        Object object = this.LOCK_MODES;
        synchronized (object) {
            this.modesSubModel.setViewMaximizedMode(maximizedMode);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setToolbarConfigName(String toolbarConfigName) {
        Object object = this.LOCK_TOOLBAR_CONFIG;
        synchronized (object) {
            this.toolbarConfigName = toolbarConfigName;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void addTopComponentGroup(TopComponentGroupImpl tcGroup) {
        Object object = this.LOCK_TOPCOMPONENT_GROUPS;
        synchronized (object) {
            this.topComponentGroups.add(tcGroup);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeTopComponentGroup(TopComponentGroupImpl tcGroup) {
        Object object = this.LOCK_TOPCOMPONENT_GROUPS;
        synchronized (object) {
            this.topComponentGroups.remove(tcGroup);
        }
    }

    @Override
    public void reset() {
        this.mode2model.clear();
        this.group2model.clear();
        this.mainWindowFrameStateJoined = 0;
        this.mainWindowFrameStateSeparated = 0;
        this.editorAreaState = 0;
        this.editorAreaFrameState = 0;
        this.toolbarConfigName = "Standard";
        this.modesSubModel = new ModesSubModel(this);
        this.topComponentGroups.clear();
        this.maximizedDockingStatus.clear();
        this.defaultDockingStatus.clear();
        this.slideInMaximizedTopComponents.clear();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean isVisible() {
        Object object = this.LOCK_VISIBLE;
        synchronized (object) {
            return this.visible;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Rectangle getMainWindowBoundsJoined() {
        Object object = this.LOCK_MAIN_WINDOW_BOUNDS_JOINED;
        synchronized (object) {
            return (Rectangle)this.mainWindowBoundsJoined.clone();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Rectangle getMainWindowBoundsSeparated() {
        Object object = this.LOCK_MAIN_WINDOW_BOUNDS_SEPARATED;
        synchronized (object) {
            return (Rectangle)this.mainWindowBoundsSeparated.clone();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Rectangle getMainWindowBoundsSeparatedHelp() {
        Object object = this.LOCK_MAIN_WINDOW_BOUNDS_SEPARATED_HELP;
        synchronized (object) {
            return (Rectangle)this.mainWindowBoundsSeparatedHelp.clone();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int getMainWindowFrameStateJoined() {
        Object object = this.LOCK_MAIN_WINDOW_FRAME_STATE_JOINED;
        synchronized (object) {
            return this.mainWindowFrameStateJoined;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int getMainWindowFrameStateSeparated() {
        Object object = this.LOCK_MAIN_WINDOW_FRAME_STATE_SEPARATED;
        synchronized (object) {
            return this.mainWindowFrameStateSeparated;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int getEditorAreaState() {
        Object object = this.LOCK_EDITOR_AREA_STATE;
        synchronized (object) {
            return this.editorAreaState;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int getEditorAreaFrameState() {
        Object object = this.LOCK_EDITOR_AREA_FRAME_STATE;
        synchronized (object) {
            return this.editorAreaFrameState;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Rectangle getEditorAreaBounds() {
        Object object = this.LOCK_EDITOR_AREA_BOUNDS;
        synchronized (object) {
            return (Rectangle)this.editorAreaBounds.clone();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Rectangle getEditorAreaBoundsHelp() {
        Object object = this.LOCK_EDITOR_AREA_BOUNDS_HELP;
        synchronized (object) {
            return (Rectangle)this.editorAreaBoundsHelp.clone();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public SplitConstraint[] getEditorAreaConstraints() {
        Object object = this.LOCK_MODES;
        synchronized (object) {
            return this.modesSubModel.getEditorAreaConstraints();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Set<ModeImpl> getModes() {
        Object object = this.LOCK_MODES;
        synchronized (object) {
            return this.modesSubModel.getModes();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public SplitConstraint[] getModeConstraints(ModeImpl mode) {
        Object object = this.LOCK_MODES;
        synchronized (object) {
            return this.modesSubModel.getModeConstraints(mode);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public SplitConstraint[] getModelElementConstraints(ModelElement element) {
        Object object = this.LOCK_MODES;
        synchronized (object) {
            return this.modesSubModel.getModelElementConstraints(element);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public String getSlidingModeConstraints(ModeImpl mode) {
        Object object = this.LOCK_MODES;
        synchronized (object) {
            return this.modesSubModel.getSlidingModeConstraints(mode);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public ModeImpl getSlidingMode(String side) {
        Object object = this.LOCK_MODES;
        synchronized (object) {
            return this.modesSubModel.getSlidingMode(side);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public ModeImpl getActiveMode() {
        Object object = this.LOCK_MODES;
        synchronized (object) {
            return this.modesSubModel.getActiveMode();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public ModeImpl getLastActiveEditorMode() {
        Object object = this.LOCK_MODES;
        synchronized (object) {
            return this.modesSubModel.getLastActiveEditorMode();
        }
    }

    @Override
    public DockingStatus getDefaultDockingStatus() {
        return this.defaultDockingStatus;
    }

    @Override
    public DockingStatus getMaximizedDockingStatus() {
        return this.maximizedDockingStatus;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public ModeImpl getEditorMaximizedMode() {
        Object object = this.LOCK_MODES;
        synchronized (object) {
            return this.modesSubModel.getEditorMaximizedMode();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public ModeImpl getViewMaximizedMode() {
        Object object = this.LOCK_MODES;
        synchronized (object) {
            return this.modesSubModel.getViewMaximizedMode();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public String getSlideSideForMode(ModeImpl mode) {
        Object object = this.LOCK_MODES;
        synchronized (object) {
            return this.modesSubModel.getSlideSideForMode(mode);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public String getToolbarConfigName() {
        Object object = this.LOCK_TOOLBAR_CONFIG;
        synchronized (object) {
            return this.toolbarConfigName;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Map<String, Integer> getSlideInSizes(String side) {
        Object object = this.LOCK_MODES;
        synchronized (object) {
            return this.modesSubModel.getSlideInSizes(side);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setSlideInSize(String side, TopComponent tc, int size) {
        Object object = this.LOCK_MODES;
        synchronized (object) {
            this.modesSubModel.setSlideInSize(side, tc, size);
        }
    }

    @Override
    public boolean isTopComponentMaximizedWhenSlidedIn(String tcid) {
        return null != tcid && this.slideInMaximizedTopComponents.contains(tcid);
    }

    @Override
    public void setTopComponentMaximizedWhenSlidedIn(String tcid, boolean maximized) {
        if (null != tcid) {
            if (maximized) {
                this.slideInMaximizedTopComponents.add(tcid);
            } else {
                this.slideInMaximizedTopComponents.remove(tcid);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void createModeModel(ModeImpl mode, String name, int state, int kind, boolean permanent) {
        Map<ModeImpl, ModeModel> map = this.mode2model;
        synchronized (map) {
            DefaultModeModel mm = new DefaultModeModel(name, state, kind, permanent);
            this.mode2model.put(mode, mm);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private ModeModel getModelForMode(ModeImpl mode) {
        Map<ModeImpl, ModeModel> map = this.mode2model;
        synchronized (map) {
            return this.mode2model.get(mode);
        }
    }

    @Override
    public void setModeState(ModeImpl mode, int state) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            modeModel.setState(state);
        }
    }

    @Override
    public void setModeBounds(ModeImpl mode, Rectangle bounds) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            modeModel.setBounds(bounds);
        }
    }

    @Override
    public void setModeFrameState(ModeImpl mode, int frameState) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            modeModel.setFrameState(frameState);
        }
    }

    @Override
    public void setModeSelectedTopComponent(ModeImpl mode, TopComponent selected) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            modeModel.setSelectedTopComponent(selected);
        }
    }

    @Override
    public void setModePreviousSelectedTopComponentID(ModeImpl mode, String prevSelectedId) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            modeModel.setPreviousSelectedTopComponentID(prevSelectedId);
        }
    }

    @Override
    public void addModeOpenedTopComponent(ModeImpl mode, TopComponent tc) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            modeModel.addOpenedTopComponent(tc);
        }
    }

    @Override
    public void insertModeOpenedTopComponent(ModeImpl mode, TopComponent tc, int index) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            modeModel.insertOpenedTopComponent(tc, index);
        }
    }

    @Override
    public void addModeClosedTopComponent(ModeImpl mode, TopComponent tc) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            modeModel.addClosedTopComponent(tc);
        }
    }

    @Override
    public void addModeUnloadedTopComponent(ModeImpl mode, String tcID, int index) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            modeModel.addUnloadedTopComponent(tcID, index);
        }
    }

    @Override
    public void setModeUnloadedSelectedTopComponent(ModeImpl mode, String tcID) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            modeModel.setUnloadedSelectedTopComponent(tcID);
        }
    }

    @Override
    public void setModeUnloadedPreviousSelectedTopComponent(ModeImpl mode, String tcID) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            modeModel.setUnloadedPreviousSelectedTopComponent(tcID);
        }
    }

    @Override
    public void removeModeTopComponent(ModeImpl mode, TopComponent tc, TopComponent recentTc) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            modeModel.removeTopComponent(tc, recentTc);
        }
    }

    @Override
    public void removeModeClosedTopComponentID(ModeImpl mode, String tcID) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            modeModel.removeClosedTopComponentID(tcID);
        }
    }

    @Override
    public void setModeTopComponentPreviousConstraints(ModeImpl mode, String tcID, SplitConstraint[] constraints) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            modeModel.setTopComponentPreviousConstraints(tcID, constraints);
        }
    }

    @Override
    public void setModeTopComponentPreviousMode(ModeImpl mode, String tcID, ModeImpl previousMode, int prevIndex) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            modeModel.setTopComponentPreviousMode(tcID, previousMode, prevIndex);
        }
    }

    @Override
    public String getModeName(ModeImpl mode) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            return modeModel.getName();
        }
        return null;
    }

    @Override
    public final void setModeName(ModeImpl mode, String name) {
        ModeModel modeModel = this.getModelForMode(mode);
        modeModel.setName(name);
    }

    @Override
    public Rectangle getModeBounds(ModeImpl mode) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            return modeModel.getBounds();
        }
        return null;
    }

    @Override
    public Rectangle getModeBoundsSeparatedHelp(ModeImpl mode) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            return modeModel.getBoundsSeparatedHelp();
        }
        return null;
    }

    @Override
    public int getModeState(ModeImpl mode) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            return modeModel.getState();
        }
        return -1;
    }

    @Override
    public int getModeKind(ModeImpl mode) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            return modeModel.getKind();
        }
        return -1;
    }

    @Override
    public String getModeSide(ModeImpl mode) {
        String side = this.modesSubModel.getSlidingModeConstraints(mode);
        return side;
    }

    @Override
    public int getModeFrameState(ModeImpl mode) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            return modeModel.getFrameState();
        }
        return -1;
    }

    @Override
    public boolean isModePermanent(ModeImpl mode) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            boolean result = modeModel.isPermanent();
            if (!result) {
                for (TopComponent tc : mode.getTopComponents()) {
                    if (result |= tc.getClass().getAnnotation(RetainLocation.class) != null) break;
                }
            }
            return result;
        }
        return false;
    }

    @Override
    public void makeModePermanent(ModeImpl mode) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            modeModel.makePermanent();
        }
    }

    @Override
    public boolean isModeEmpty(ModeImpl mode) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            return modeModel.isEmpty();
        }
        return false;
    }

    @Override
    public boolean containsModeTopComponent(ModeImpl mode, TopComponent tc) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            return modeModel.containsTopComponent(tc);
        }
        return false;
    }

    @Override
    public TopComponent getModeSelectedTopComponent(ModeImpl mode) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            return modeModel.getSelectedTopComponent();
        }
        return null;
    }

    @Override
    public String getModePreviousSelectedTopComponentID(ModeImpl mode) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            return modeModel.getPreviousSelectedTopComponentID();
        }
        return null;
    }

    @Override
    public List<TopComponent> getModeTopComponents(ModeImpl mode) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            return modeModel.getTopComponents();
        }
        return Collections.emptyList();
    }

    @Override
    public List<TopComponent> getModeOpenedTopComponents(ModeImpl mode) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            return modeModel.getOpenedTopComponents();
        }
        return Collections.emptyList();
    }

    @Override
    public List<String> getModeOpenedTopComponentsIDs(ModeImpl mode) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            return modeModel.getOpenedTopComponentsIDs();
        }
        return Collections.emptyList();
    }

    @Override
    public int getModeOpenedTopComponentTabPosition(ModeImpl mode, TopComponent tc) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            return modeModel.getOpenedTopComponentTabPosition(tc);
        }
        return -1;
    }

    @Override
    public List<String> getModeClosedTopComponentsIDs(ModeImpl mode) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            return modeModel.getClosedTopComponentsIDs();
        }
        return Collections.emptyList();
    }

    @Override
    public List<String> getModeTopComponentsIDs(ModeImpl mode) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            return modeModel.getTopComponentsIDs();
        }
        return Collections.emptyList();
    }

    @Override
    public SplitConstraint[] getModeTopComponentPreviousConstraints(ModeImpl mode, String tcID) {
        ModeModel modeModel = this.getModelForMode(mode);
        return modeModel == null ? null : modeModel.getTopComponentPreviousConstraints(tcID);
    }

    @Override
    public ModeImpl getModeTopComponentPreviousMode(ModeImpl mode, String tcID) {
        ModeModel modeModel = this.getModelForMode(mode);
        return modeModel == null ? null : modeModel.getTopComponentPreviousMode(tcID);
    }

    @Override
    public int getModeTopComponentPreviousIndex(ModeImpl mode, String tcID) {
        ModeModel modeModel = this.getModelForMode(mode);
        return modeModel == null ? null : Integer.valueOf(modeModel.getTopComponentPreviousIndex(tcID));
    }

    @Override
    public boolean isModeMinimized(ModeImpl mode) {
        ModeModel modeModel = this.getModelForMode(mode);
        return modeModel == null ? false : modeModel.isMinimized();
    }

    @Override
    public void setModeMinimized(ModeImpl mode, boolean minimized) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (null != modeModel) {
            modeModel.setMinimized(minimized);
        }
    }

    @Override
    public Collection<String> getModeOtherNames(ModeImpl mode) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            return modeModel.getOtherNames();
        }
        return Collections.emptyList();
    }

    @Override
    public void addModeOtherName(ModeImpl mode, String otherModeName) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (null != modeModel) {
            modeModel.addOtherName(otherModeName);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void dockMode(ModeImpl prevMode, ModeImpl floatingMode) {
        ModeModel modeModel = this.getModelForMode(floatingMode);
        if (null != modeModel) {
            Object object = this.LOCK_MODES;
            synchronized (object) {
                this.modesSubModel.dock(prevMode, floatingMode);
            }
            modeModel.setState(0);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void createGroupModel(TopComponentGroupImpl tcGroup, String name, boolean opened) {
        Map<TopComponentGroupImpl, TopComponentGroupModel> map = this.group2model;
        synchronized (map) {
            DefaultTopComponentGroupModel tcgm = new DefaultTopComponentGroupModel(name, opened);
            this.group2model.put(tcGroup, tcgm);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private TopComponentGroupModel getModelForGroup(TopComponentGroupImpl tcGroup) {
        Map<TopComponentGroupImpl, TopComponentGroupModel> map = this.group2model;
        synchronized (map) {
            return this.group2model.get(tcGroup);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Set<TopComponentGroupImpl> getTopComponentGroups() {
        Object object = this.LOCK_TOPCOMPONENT_GROUPS;
        synchronized (object) {
            return new HashSet<TopComponentGroupImpl>(this.topComponentGroups);
        }
    }

    @Override
    public String getGroupName(TopComponentGroupImpl tcGroup) {
        TopComponentGroupModel groupModel = this.getModelForGroup(tcGroup);
        if (groupModel != null) {
            return groupModel.getName();
        }
        return null;
    }

    @Override
    public void openGroup(TopComponentGroupImpl tcGroup, Collection<TopComponent> openedTopComponents, Collection<TopComponent> openedBeforeTopComponents) {
        TopComponentGroupModel groupModel = this.getModelForGroup(tcGroup);
        if (groupModel != null) {
            groupModel.open(openedTopComponents, openedBeforeTopComponents);
        }
    }

    @Override
    public void closeGroup(TopComponentGroupImpl tcGroup) {
        TopComponentGroupModel groupModel = this.getModelForGroup(tcGroup);
        if (groupModel != null) {
            groupModel.close();
        }
    }

    @Override
    public boolean isGroupOpened(TopComponentGroupImpl tcGroup) {
        TopComponentGroupModel groupModel = this.getModelForGroup(tcGroup);
        if (groupModel != null) {
            return groupModel.isOpened();
        }
        return false;
    }

    @Override
    public Set<TopComponent> getGroupTopComponents(TopComponentGroupImpl tcGroup) {
        TopComponentGroupModel groupModel = this.getModelForGroup(tcGroup);
        if (groupModel != null) {
            return groupModel.getTopComponents();
        }
        return Collections.emptySet();
    }

    @Override
    public Set<TopComponent> getGroupOpenedTopComponents(TopComponentGroupImpl tcGroup) {
        TopComponentGroupModel groupModel = this.getModelForGroup(tcGroup);
        if (groupModel != null) {
            return groupModel.getOpenedTopComponents();
        }
        return Collections.emptySet();
    }

    @Override
    public Set<TopComponent> getGroupOpenedBeforeTopComponents(TopComponentGroupImpl tcGroup) {
        TopComponentGroupModel groupModel = this.getModelForGroup(tcGroup);
        if (groupModel != null) {
            return groupModel.getOpenedBeforeTopComponents();
        }
        return Collections.emptySet();
    }

    @Override
    public Set<TopComponent> getGroupOpeningTopComponents(TopComponentGroupImpl tcGroup) {
        TopComponentGroupModel groupModel = this.getModelForGroup(tcGroup);
        if (groupModel != null) {
            return groupModel.getOpeningTopComponents();
        }
        return Collections.emptySet();
    }

    @Override
    public Set<TopComponent> getGroupClosingTopComponents(TopComponentGroupImpl tcGroup) {
        TopComponentGroupModel groupModel = this.getModelForGroup(tcGroup);
        if (groupModel != null) {
            return groupModel.getClosingTopComponents();
        }
        return Collections.emptySet();
    }

    @Override
    public boolean addGroupUnloadedTopComponent(TopComponentGroupImpl tcGroup, String tcID) {
        TopComponentGroupModel groupModel = this.getModelForGroup(tcGroup);
        if (groupModel != null) {
            return groupModel.addUnloadedTopComponent(tcID);
        }
        return false;
    }

    @Override
    public boolean removeGroupUnloadedTopComponent(TopComponentGroupImpl tcGroup, String tcID) {
        TopComponentGroupModel groupModel = this.getModelForGroup(tcGroup);
        if (groupModel != null) {
            return groupModel.removeUnloadedTopComponent(tcID);
        }
        return false;
    }

    @Override
    public boolean addGroupOpeningTopComponent(TopComponentGroupImpl tcGroup, TopComponent tc) {
        TopComponentGroupModel groupModel = this.getModelForGroup(tcGroup);
        if (groupModel != null) {
            return groupModel.addOpeningTopComponent(tc);
        }
        return false;
    }

    @Override
    public boolean removeGroupOpeningTopComponent(TopComponentGroupImpl tcGroup, TopComponent tc) {
        TopComponentGroupModel groupModel = this.getModelForGroup(tcGroup);
        if (groupModel != null) {
            return groupModel.removeOpeningTopComponent(tc);
        }
        return false;
    }

    @Override
    public boolean addGroupUnloadedOpeningTopComponent(TopComponentGroupImpl tcGroup, String tcID) {
        TopComponentGroupModel groupModel = this.getModelForGroup(tcGroup);
        if (groupModel != null) {
            return groupModel.addUnloadedOpeningTopComponent(tcID);
        }
        return false;
    }

    @Override
    public boolean removeGroupUnloadedOpeningTopComponent(TopComponentGroupImpl tcGroup, String tcID) {
        TopComponentGroupModel groupModel = this.getModelForGroup(tcGroup);
        if (groupModel != null) {
            return groupModel.removeUnloadedOpeningTopComponent(tcID);
        }
        return false;
    }

    @Override
    public boolean addGroupUnloadedClosingTopComponent(TopComponentGroupImpl tcGroup, String tcID) {
        TopComponentGroupModel groupModel = this.getModelForGroup(tcGroup);
        if (groupModel != null) {
            return groupModel.addUnloadedClosingTopComponent(tcID);
        }
        return false;
    }

    @Override
    public boolean removeGroupUnloadedClosingTopComponent(TopComponentGroupImpl tcGroup, String tcID) {
        TopComponentGroupModel groupModel = this.getModelForGroup(tcGroup);
        if (groupModel != null) {
            return groupModel.removeUnloadedClosingTopComponent(tcID);
        }
        return false;
    }

    @Override
    public boolean addGroupUnloadedOpenedTopComponent(TopComponentGroupImpl tcGroup, String tcID) {
        TopComponentGroupModel groupModel = this.getModelForGroup(tcGroup);
        if (groupModel != null) {
            return groupModel.addUnloadedOpenedTopComponent(tcID);
        }
        return false;
    }

    @Override
    public Set<String> getGroupTopComponentsIDs(TopComponentGroupImpl tcGroup) {
        TopComponentGroupModel groupModel = this.getModelForGroup(tcGroup);
        if (groupModel != null) {
            return groupModel.getTopComponentsIDs();
        }
        return Collections.emptySet();
    }

    @Override
    public Set<String> getGroupOpeningSetIDs(TopComponentGroupImpl tcGroup) {
        TopComponentGroupModel groupModel = this.getModelForGroup(tcGroup);
        if (groupModel != null) {
            return groupModel.getOpeningSetIDs();
        }
        return Collections.emptySet();
    }

    @Override
    public Set<String> getGroupClosingSetIDs(TopComponentGroupImpl tcGroup) {
        TopComponentGroupModel groupModel = this.getModelForGroup(tcGroup);
        if (groupModel != null) {
            return groupModel.getClosingSetIDs();
        }
        return Collections.emptySet();
    }

    @Override
    public Set<String> getGroupOpenedTopComponentsIDs(TopComponentGroupImpl tcGroup) {
        TopComponentGroupModel groupModel = this.getModelForGroup(tcGroup);
        if (groupModel != null) {
            return groupModel.getOpenedTopComponentsIDs();
        }
        return Collections.emptySet();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setMainWindowBoundsUserSeparatedHelp(Rectangle bounds) {
        if (bounds == null) {
            return;
        }
        Object object = this.LOCK_MAIN_WINDOW_BOUNDS_SEPARATED_HELP;
        synchronized (object) {
            this.mainWindowBoundsSeparatedHelp.setBounds(bounds);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setEditorAreaBoundsUserHelp(Rectangle bounds) {
        if (bounds == null) {
            return;
        }
        Object object = this.LOCK_EDITOR_AREA_BOUNDS_HELP;
        synchronized (object) {
            this.editorAreaBoundsHelp.setBounds(bounds);
        }
    }

    @Override
    public void setModeBoundsSeparatedHelp(ModeImpl mode, Rectangle bounds) {
        ModeModel modeModel = this.getModelForMode(mode);
        if (modeModel != null) {
            modeModel.setBoundsSeparatedHelp(bounds);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setSplitWeights(ModelElement[] snapshots, double[] splitWeights) {
        Object object = this.LOCK_MODES;
        synchronized (object) {
            this.modesSubModel.setSplitWeights(snapshots, splitWeights);
        }
    }

    @Override
    public WindowSystemSnapshot createWindowSystemSnapshot() {
        WindowSystemSnapshot wsms = new WindowSystemSnapshot();
        ModeStructureSnapshot mss = this.createModeStructureSnapshot();
        wsms.setModeStructureSnapshot(mss);
        ModeImpl activeMode = this.getActiveMode();
        wsms.setActiveModeSnapshot(activeMode == null ? null : mss.findModeSnapshot(activeMode.getName()));
        ModeImpl maximizedMode = null != this.getViewMaximizedMode() ? this.getViewMaximizedMode() : null;
        wsms.setMaximizedModeSnapshot(maximizedMode == null ? null : mss.findModeSnapshot(maximizedMode.getName()));
        wsms.setMainWindowBoundsJoined(this.getMainWindowBoundsJoined());
        wsms.setMainWindowBoundsSeparated(this.getMainWindowBoundsSeparated());
        wsms.setEditorAreaBounds(this.getEditorAreaBounds());
        wsms.setEditorAreaState(this.getEditorAreaState());
        wsms.setEditorAreaFrameState(this.getEditorAreaFrameState());
        wsms.setMainWindowFrameStateJoined(this.getMainWindowFrameStateJoined());
        wsms.setMainWindowFrameStateSeparated(this.getMainWindowFrameStateSeparated());
        wsms.setToolbarConfigurationName(this.getToolbarConfigName());
        return wsms;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private ModeStructureSnapshot createModeStructureSnapshot() {
        ModeStructureSnapshot.ElementSnapshot splitRoot;
        Set<ModeStructureSnapshot.SlidingModeSnapshot> slidingModes;
        Set<ModeStructureSnapshot.ModeSnapshot> separateModes;
        Object object = this.LOCK_MODES;
        synchronized (object) {
            splitRoot = this.modesSubModel.createSplitSnapshot();
            separateModes = this.modesSubModel.createSeparateModeSnapshots();
            slidingModes = this.modesSubModel.createSlidingModeSnapshots();
        }
        ModeStructureSnapshot ms = new ModeStructureSnapshot(splitRoot, separateModes, slidingModes);
        return ms;
    }

    private static boolean validateAddingMode(ModeImpl mode) {
        if (mode == null) {
            Logger.getLogger(DefaultModel.class.getName()).log(Level.WARNING, null, new NullPointerException("Not allowed null mode"));
            return false;
        }
        return true;
    }

    private void updateSlidingSelections(ModeImpl curActive) {
        Set<ModeImpl> slidingModes = this.modesSubModel.getSlidingModes();
        ModeImpl curSliding2 = null;
        for (ModeImpl curSliding2 : slidingModes) {
            if (curSliding2.equals(curActive)) continue;
            this.setModeSelectedTopComponent(curSliding2, null);
        }
    }

    private static class DefaultDockingStatus
    extends DockingStatus {
        public DefaultDockingStatus(Model model) {
            super(model);
        }

        @Override
        public boolean shouldSlide(String tcID) {
            return null != tcID && this.slided.contains(tcID);
        }

        @Override
        public boolean shouldDock(String tcID) {
            return null != tcID && (this.docked.contains(tcID) || !this.docked.contains(tcID) && !this.slided.contains(tcID));
        }

        @Override
        public void mark() {
            super.mark();
            Set<ModeImpl> modes = this.model.getModes();
            for (ModeImpl modeImpl : modes) {
                if (modeImpl.getState() != 0) continue;
                String selTcId = null;
                TopComponent selTc = modeImpl.getSelectedTopComponent();
                if (null != selTc) {
                    selTcId = WindowManagerImpl.getInstance().findTopComponentID(selTc);
                }
                modeImpl.setPreviousSelectedTopComponentID(selTcId);
            }
        }
    }

}

