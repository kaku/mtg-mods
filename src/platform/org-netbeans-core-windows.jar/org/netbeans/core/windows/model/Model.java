/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.model;

import java.awt.Rectangle;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.SplitConstraint;
import org.netbeans.core.windows.TopComponentGroupImpl;
import org.netbeans.core.windows.WindowSystemSnapshot;
import org.netbeans.core.windows.model.DockingStatus;
import org.netbeans.core.windows.model.ModelElement;
import org.openide.windows.TopComponent;

public interface Model {
    public void setVisible(boolean var1);

    public void setMainWindowBoundsJoined(Rectangle var1);

    public void setMainWindowBoundsSeparated(Rectangle var1);

    public void setMainWindowFrameStateJoined(int var1);

    public void setMainWindowFrameStateSeparated(int var1);

    public void setEditorAreaState(int var1);

    public void setEditorAreaFrameState(int var1);

    public void setEditorAreaBounds(Rectangle var1);

    public void setEditorAreaConstraints(SplitConstraint[] var1);

    public void setToolbarConfigName(String var1);

    public void setActiveMode(ModeImpl var1);

    public void setEditorMaximizedMode(ModeImpl var1);

    public void setViewMaximizedMode(ModeImpl var1);

    public void addMode(ModeImpl var1, SplitConstraint[] var2);

    public void addModeToSide(ModeImpl var1, ModeImpl var2, String var3);

    public void addModeAround(ModeImpl var1, String var2);

    public void addModeAroundEditor(ModeImpl var1, String var2);

    public void removeMode(ModeImpl var1);

    public void setModeName(ModeImpl var1, String var2);

    public void setModeConstraints(ModeImpl var1, SplitConstraint[] var2);

    public void addTopComponentGroup(TopComponentGroupImpl var1);

    public void removeTopComponentGroup(TopComponentGroupImpl var1);

    public void addSlidingMode(ModeImpl var1, String var2, Map<String, Integer> var3);

    public void reset();

    public void setSlideInSize(String var1, TopComponent var2, int var3);

    public void setTopComponentMaximizedWhenSlidedIn(String var1, boolean var2);

    public boolean isVisible();

    public Rectangle getMainWindowBoundsJoined();

    public Rectangle getMainWindowBoundsSeparated();

    public int getMainWindowFrameStateJoined();

    public int getMainWindowFrameStateSeparated();

    public Rectangle getMainWindowBoundsSeparatedHelp();

    public int getEditorAreaState();

    public int getEditorAreaFrameState();

    public Rectangle getEditorAreaBounds();

    public Rectangle getEditorAreaBoundsHelp();

    public SplitConstraint[] getEditorAreaConstraints();

    public String getToolbarConfigName();

    public ModeImpl getActiveMode();

    public ModeImpl getLastActiveEditorMode();

    public ModeImpl getEditorMaximizedMode();

    public ModeImpl getViewMaximizedMode();

    public Set<ModeImpl> getModes();

    public SplitConstraint[] getModeConstraints(ModeImpl var1);

    public SplitConstraint[] getModelElementConstraints(ModelElement var1);

    public String getSlidingModeConstraints(ModeImpl var1);

    public ModeImpl getSlidingMode(String var1);

    public Map<String, Integer> getSlideInSizes(String var1);

    public DockingStatus getDefaultDockingStatus();

    public DockingStatus getMaximizedDockingStatus();

    public String getSlideSideForMode(ModeImpl var1);

    public boolean isTopComponentMaximizedWhenSlidedIn(String var1);

    public void setModeState(ModeImpl var1, int var2);

    public void setModeBounds(ModeImpl var1, Rectangle var2);

    public void setModeFrameState(ModeImpl var1, int var2);

    public void setModeSelectedTopComponent(ModeImpl var1, TopComponent var2);

    public void setModePreviousSelectedTopComponentID(ModeImpl var1, String var2);

    public void addModeOpenedTopComponent(ModeImpl var1, TopComponent var2);

    public void insertModeOpenedTopComponent(ModeImpl var1, TopComponent var2, int var3);

    public void addModeClosedTopComponent(ModeImpl var1, TopComponent var2);

    public void addModeUnloadedTopComponent(ModeImpl var1, String var2, int var3);

    public void setModeUnloadedSelectedTopComponent(ModeImpl var1, String var2);

    public void setModeUnloadedPreviousSelectedTopComponent(ModeImpl var1, String var2);

    public void removeModeTopComponent(ModeImpl var1, TopComponent var2, TopComponent var3);

    public void removeModeClosedTopComponentID(ModeImpl var1, String var2);

    public void setModeTopComponentPreviousMode(ModeImpl var1, String var2, ModeImpl var3, int var4);

    public void setModeTopComponentPreviousConstraints(ModeImpl var1, String var2, SplitConstraint[] var3);

    public String getModeName(ModeImpl var1);

    public Rectangle getModeBounds(ModeImpl var1);

    public Rectangle getModeBoundsSeparatedHelp(ModeImpl var1);

    public int getModeState(ModeImpl var1);

    public int getModeKind(ModeImpl var1);

    public String getModeSide(ModeImpl var1);

    public int getModeFrameState(ModeImpl var1);

    public boolean isModePermanent(ModeImpl var1);

    public void makeModePermanent(ModeImpl var1);

    public boolean isModeEmpty(ModeImpl var1);

    public boolean containsModeTopComponent(ModeImpl var1, TopComponent var2);

    public TopComponent getModeSelectedTopComponent(ModeImpl var1);

    public String getModePreviousSelectedTopComponentID(ModeImpl var1);

    public List<TopComponent> getModeTopComponents(ModeImpl var1);

    public List<TopComponent> getModeOpenedTopComponents(ModeImpl var1);

    public List<String> getModeOpenedTopComponentsIDs(ModeImpl var1);

    public List<String> getModeClosedTopComponentsIDs(ModeImpl var1);

    public List<String> getModeTopComponentsIDs(ModeImpl var1);

    public boolean isModeMinimized(ModeImpl var1);

    public void setModeMinimized(ModeImpl var1, boolean var2);

    public Collection<String> getModeOtherNames(ModeImpl var1);

    public void addModeOtherName(ModeImpl var1, String var2);

    public void dockMode(ModeImpl var1, ModeImpl var2);

    public ModeImpl getModeTopComponentPreviousMode(ModeImpl var1, String var2);

    public SplitConstraint[] getModeTopComponentPreviousConstraints(ModeImpl var1, String var2);

    public int getModeTopComponentPreviousIndex(ModeImpl var1, String var2);

    public int getModeOpenedTopComponentTabPosition(ModeImpl var1, TopComponent var2);

    public Set<TopComponentGroupImpl> getTopComponentGroups();

    public String getGroupName(TopComponentGroupImpl var1);

    public void openGroup(TopComponentGroupImpl var1, Collection<TopComponent> var2, Collection<TopComponent> var3);

    public void closeGroup(TopComponentGroupImpl var1);

    public boolean isGroupOpened(TopComponentGroupImpl var1);

    public Set<TopComponent> getGroupTopComponents(TopComponentGroupImpl var1);

    public Set<TopComponent> getGroupOpenedTopComponents(TopComponentGroupImpl var1);

    public Set<TopComponent> getGroupOpenedBeforeTopComponents(TopComponentGroupImpl var1);

    public Set<TopComponent> getGroupOpeningTopComponents(TopComponentGroupImpl var1);

    public Set<TopComponent> getGroupClosingTopComponents(TopComponentGroupImpl var1);

    public boolean addGroupUnloadedTopComponent(TopComponentGroupImpl var1, String var2);

    public boolean removeGroupUnloadedTopComponent(TopComponentGroupImpl var1, String var2);

    public boolean addGroupOpeningTopComponent(TopComponentGroupImpl var1, TopComponent var2);

    public boolean removeGroupOpeningTopComponent(TopComponentGroupImpl var1, TopComponent var2);

    public boolean addGroupUnloadedOpeningTopComponent(TopComponentGroupImpl var1, String var2);

    public boolean removeGroupUnloadedOpeningTopComponent(TopComponentGroupImpl var1, String var2);

    public boolean addGroupUnloadedClosingTopComponent(TopComponentGroupImpl var1, String var2);

    public boolean removeGroupUnloadedClosingTopComponent(TopComponentGroupImpl var1, String var2);

    public boolean addGroupUnloadedOpenedTopComponent(TopComponentGroupImpl var1, String var2);

    public Set<String> getGroupTopComponentsIDs(TopComponentGroupImpl var1);

    public Set<String> getGroupOpeningSetIDs(TopComponentGroupImpl var1);

    public Set<String> getGroupClosingSetIDs(TopComponentGroupImpl var1);

    public Set<String> getGroupOpenedTopComponentsIDs(TopComponentGroupImpl var1);

    public void createModeModel(ModeImpl var1, String var2, int var3, int var4, boolean var5);

    public void createGroupModel(TopComponentGroupImpl var1, String var2, boolean var3);

    public WindowSystemSnapshot createWindowSystemSnapshot();

    public void setMainWindowBoundsUserSeparatedHelp(Rectangle var1);

    public void setEditorAreaBoundsUserHelp(Rectangle var1);

    public void setModeBoundsSeparatedHelp(ModeImpl var1, Rectangle var2);

    public void setSplitWeights(ModelElement[] var1, double[] var2);
}

