/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.WeakSet
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.core.windows.Debug;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.ModeStructureSnapshot;
import org.netbeans.core.windows.SplitConstraint;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.model.EditorSplitSubModel;
import org.netbeans.core.windows.model.Model;
import org.netbeans.core.windows.model.ModelElement;
import org.openide.util.WeakSet;
import org.openide.windows.TopComponent;

class SplitSubModel {
    protected final Model parentModel;
    private final Set<ModeNode> nodes = new WeakSet(20);
    protected Node root;
    private static final boolean DEBUG = Debug.isLoggable(SplitSubModel.class);

    public SplitSubModel(Model parentModel) {
        this.parentModel = parentModel;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private ModeNode getModeNode(ModeImpl mode) {
        ModeNode res = null;
        Set<ModeNode> set = this.nodes;
        synchronized (set) {
            for (ModeNode node : this.nodes) {
                if (!node.getMode().equals(mode)) continue;
                res = node;
                break;
            }
            if (null == res) {
                res = new ModeNode(mode);
                this.nodes.add(res);
            }
            return res;
        }
    }

    public SplitConstraint[] getModelElementConstraints(ModelElement element) {
        if (element instanceof Node) {
            Node node = (Node)element;
            if (!this.isInTree(node)) {
                return null;
            }
            return node.getNodeConstraints();
        }
        return null;
    }

    public SplitConstraint[] getModeConstraints(ModeImpl mode) {
        ModeNode modeNode = this.getModeNode(mode);
        return modeNode.getNodeConstraints();
    }

    public String getSlideSideForMode(ModeImpl mode) {
        ModeNode modeNode = this.getModeNode(mode);
        return this.getPositionRelativeToEditor(modeNode, modeNode.getParent());
    }

    private String getPositionRelativeToEditor(Node node, SplitNode parent) {
        if (null == parent) {
            return "left";
        }
        Node editorNode = this.getEditorChildNode(parent);
        if (null != editorNode) {
            int orientation = parent.getOrientation();
            int nodeIndex = parent.getChildIndex(node);
            int editorIndex = parent.getChildIndex(editorNode);
            if (orientation == 0) {
                if (nodeIndex > editorIndex) {
                    return "bottom";
                }
                return "top";
            }
            if (nodeIndex < editorIndex) {
                return "left";
            }
            return "right";
        }
        return this.getPositionRelativeToEditor(parent, parent.getParent());
    }

    private Node getEditorChildNode(SplitNode split) {
        List<Node> children = split.getChildren();
        for (Node node : children) {
            if (!(node instanceof EditorSplitSubModel.EditorNode) && (!(node instanceof SplitNode) || null == this.getEditorChildNode((SplitNode)node))) continue;
            return node;
        }
        return null;
    }

    public boolean addMode(ModeImpl mode, SplitConstraint[] constraints) {
        if (mode == null || constraints == null) {
            Logger.getLogger(SplitSubModel.class.getName()).log(Level.WARNING, null, new IllegalArgumentException("Mode=" + mode + " constraints=" + Arrays.toString(constraints)));
            return false;
        }
        ModeNode modeNode = this.getModeNode(mode);
        if (DEBUG) {
            SplitSubModel.debugLog("");
            SplitSubModel.debugLog("");
            SplitSubModel.debugLog("==========================================");
            SplitSubModel.debugLog("Adding mode to tree=" + mode);
            SplitSubModel.debugLog("constraints=" + Arrays.asList(constraints));
            SplitSubModel.debugLog("modeNode=" + modeNode);
        }
        return this.addNodeToTree(modeNode, constraints);
    }

    public boolean addModeToSide(ModeImpl mode, ModeImpl attachMode, String side) {
        if (mode == null || mode.getState() == 1) {
            Logger.getLogger(SplitSubModel.class.getName()).log(Level.WARNING, null, new IllegalArgumentException("Mode=" + mode));
            return false;
        }
        ModeNode modeNode = this.getModeNode(mode);
        ModeNode attachModeNode = this.getModeNode(attachMode);
        if (DEBUG) {
            SplitSubModel.debugLog("");
            SplitSubModel.debugLog("");
            SplitSubModel.debugLog("==========================================");
            SplitSubModel.debugLog("Adding mode to between=" + mode);
            SplitSubModel.debugLog("attachMode=" + attachMode);
            SplitSubModel.debugLog("side=" + side);
        }
        return this.addNodeToTreeToSide(modeNode, attachModeNode, side);
    }

    public boolean addModeToSideRoot(ModeImpl mode, String side) {
        if (mode == null || mode.getState() == 1) {
            Logger.getLogger(SplitSubModel.class.getName()).log(Level.WARNING, null, new IllegalArgumentException("Mode=" + mode));
            return false;
        }
        ModeNode modeNode = this.getModeNode(mode);
        if (DEBUG) {
            SplitSubModel.debugLog("");
            SplitSubModel.debugLog("");
            SplitSubModel.debugLog("==========================================");
            SplitSubModel.debugLog("Adding mode to root's side=" + mode);
            SplitSubModel.debugLog("side=" + side);
        }
        return this.addNodeToTreeToSide(modeNode, this.root, side);
    }

    public boolean addModeAround(ModeImpl mode, String side) {
        if (mode == null || mode.getState() == 1) {
            Logger.getLogger(SplitSubModel.class.getName()).log(Level.WARNING, null, new IllegalArgumentException("Mode=" + mode));
            return false;
        }
        ModeNode modeNode = this.getModeNode(mode);
        if (DEBUG) {
            SplitSubModel.debugLog("");
            SplitSubModel.debugLog("");
            SplitSubModel.debugLog("==========================================");
            SplitSubModel.debugLog("Adding mode to around=" + mode);
            SplitSubModel.debugLog("side=" + side);
        }
        return this.addNodeToTreeAround(modeNode, side);
    }

    public boolean addModeAroundEditor(ModeImpl mode, String side) {
        if (mode == null || mode.getState() == 1) {
            Logger.getLogger(SplitSubModel.class.getName()).log(Level.WARNING, null, new IllegalArgumentException("Mode=" + mode));
            return false;
        }
        ModeNode modeNode = this.getModeNode(mode);
        if (DEBUG) {
            SplitSubModel.debugLog("");
            SplitSubModel.debugLog("");
            SplitSubModel.debugLog("==========================================");
            SplitSubModel.debugLog("Adding mode to around=" + mode);
            SplitSubModel.debugLog("side=" + side);
        }
        return this.addNodeToTreeAroundEditor(modeNode, side);
    }

    private boolean isInTree(Node descendant) {
        if (this.root == null) {
            return false;
        }
        if (descendant == this.root) {
            return true;
        }
        for (SplitNode parent = descendant.getParent(); parent != null; parent = parent.getParent()) {
            if (parent != this.root) continue;
            return true;
        }
        return false;
    }

    protected boolean addNodeToTree(Node addingNode, SplitConstraint[] constraints) {
        SplitNode splitNode;
        if (this.isInTree(addingNode)) {
            return false;
        }
        if (this.root == null) {
            if (constraints.length == 0) {
                this.root = addingNode;
                return true;
            }
            splitNode = new SplitNode(constraints[0].orientation);
            this.root = splitNode;
        } else if (this.root instanceof SplitNode) {
            splitNode = (SplitNode)this.root;
        } else {
            splitNode = new SplitNode(0);
            splitNode.setChildAt(-1, 0.5, this.root);
            this.root = splitNode;
        }
        for (int level = 0; level < constraints.length; ++level) {
            int orientation = constraints[level].orientation;
            if (orientation != splitNode.getOrientation()) {
                SplitNode newSplit = new SplitNode(orientation);
                if (splitNode == this.root) {
                    newSplit.setChildAt(-1, 0.5, splitNode);
                    this.root = newSplit;
                } else {
                    SplitNode parent = splitNode.getParent();
                    int oldIndex = parent.getChildIndex(splitNode);
                    double oldSplitWeight = parent.getChildSplitWeight(splitNode);
                    parent.removeChild(splitNode);
                    newSplit.setChildAt(-1, 0.5, splitNode);
                    parent.setChildAt(oldIndex, oldSplitWeight, newSplit);
                }
                splitNode = newSplit;
            }
            if (level >= constraints.length - 1) continue;
            int index = constraints[level].index;
            double splitWeight = constraints[level].splitWeight;
            Node child = splitNode.getChildAt(index);
            if (child instanceof SplitNode) {
                splitNode = (SplitNode)child;
                continue;
            }
            SplitNode newSplit = new SplitNode(constraints[level + 1].orientation);
            splitNode.setChildAt(index, splitWeight, newSplit);
            splitNode = newSplit;
        }
        if (constraints.length == 0) {
            splitNode.setChildAt(-1, 0.5, addingNode);
        } else {
            splitNode.setChildAt(constraints[constraints.length - 1].index, constraints[constraints.length - 1].splitWeight, addingNode);
        }
        this.verifyNode(this.root);
        return true;
    }

    private boolean addNodeToTreeToSide(Node addingNode, Node attachNode, String side) {
        if (this.isInTree(addingNode)) {
            return false;
        }
        if (!this.isInTree(attachNode)) {
            return false;
        }
        if (DEBUG) {
            SplitSubModel.debugLog("");
            SplitSubModel.debugLog("Inserting to side=" + side);
        }
        if (attachNode == this.root) {
            int addingIndex = side == "top" || side == "left" ? 0 : -1;
            int oldIndex = addingIndex == 0 ? -1 : 0;
            int orientation = side == "top" || side == "bottom" ? 0 : 1;
            SplitNode newSplit = new SplitNode(orientation);
            newSplit.setChildAt(addingIndex, 0.5, addingNode);
            newSplit.setChildAt(oldIndex, 0.5, attachNode);
            this.root = newSplit;
        } else {
            int orientation;
            SplitNode parent = attachNode.getParent();
            if (parent == null) {
                return false;
            }
            int attachIndex = parent.getChildIndex(attachNode);
            double attachWeight = parent.getChildSplitWeight(attachNode);
            int n = orientation = side == "top" || side == "bottom" ? 0 : 1;
            if (orientation == parent.getOrientation()) {
                if (side == "bottom" || side == "right") {
                    ++attachIndex;
                }
                if (parent.getChildren().size() == 1) {
                    parent.setChildSplitWeight(attachNode, 0.5);
                }
                parent.setChildAt(attachIndex, 0.5, addingNode);
            } else {
                SplitNode newSplit = new SplitNode(orientation);
                parent.removeChild(attachNode);
                int addingIndex = side == "top" || side == "left" ? 0 : -1;
                int oldIndex = addingIndex == 0 ? -1 : 0;
                newSplit.setChildAt(addingIndex, 0.5, addingNode);
                newSplit.setChildAt(oldIndex, 0.5, attachNode);
                parent.setChildAt(attachIndex, attachWeight, newSplit);
            }
        }
        return true;
    }

    private boolean addNodeToTreeAround(Node addingNode, String side) {
        SplitConstraint[] newConstraints;
        Node top = this.root;
        if (top instanceof SplitNode) {
            SplitNode parent = (SplitNode)top;
            if (parent.getOrientation() == 0 && (side == "top" || side == "bottom") || parent.getOrientation() == 1 && (side == "left" || side == "right")) {
                double splitWeights = 0.0;
                for (Node next : parent.getChildren()) {
                    splitWeights += parent.getChildSplitWeight(next);
                }
                double addingSplitWeight = splitWeights * 0.25;
                int index = side == "top" || side == "left" ? 0 : -1;
                parent.setChildAt(index, addingSplitWeight, addingNode);
                if (addingSplitWeight > 1.0) {
                    double ratio = 1.0 / addingSplitWeight;
                    parent.normalizeWeights(ratio);
                }
                return true;
            }
            int orientation = side == "top" || side == "bottom" ? 0 : 1;
            SplitNode newSplit = new SplitNode(orientation);
            int addingIndex = side == "top" || side == "left" ? 0 : -1;
            int oldIndex = addingIndex == 0 ? -1 : 0;
            newSplit.setChildAt(addingIndex, 0.25, addingNode);
            newSplit.setChildAt(oldIndex, 0.75, parent);
            this.root = newSplit;
            return true;
        }
        if (side == "top") {
            newConstraints = new SplitConstraint[]{new SplitConstraint(0, 0, 0.25)};
        } else if (side == "bottom") {
            newConstraints = new SplitConstraint[]{new SplitConstraint(0, -1, 0.25)};
        } else if (side == "left") {
            newConstraints = new SplitConstraint[]{new SplitConstraint(1, 0, 0.25)};
        } else if (side == "right") {
            newConstraints = new SplitConstraint[]{new SplitConstraint(1, -1, 0.25)};
        } else {
            return false;
        }
        return this.addNodeToTree(addingNode, newConstraints);
    }

    protected boolean addNodeToTreeAroundEditor(Node addingNode, String side) {
        return false;
    }

    public boolean removeMode(ModeImpl mode) {
        if (mode == null) {
            throw new NullPointerException("Cannot remove null mode!");
        }
        return this.removeNodeFromTree(this.getModeNode(mode));
    }

    protected boolean removeNodeFromTree(Node node) {
        if (!this.isInTree(node)) {
            return false;
        }
        SplitNode parent = node.getParent();
        if (parent == null && node != this.root) {
            return false;
        }
        if (node == this.root) {
            this.root = null;
        } else {
            parent.removeChild(node);
            List<Node> children = parent.getChildren();
            if (children.isEmpty()) {
                if (parent == this.root) {
                    this.root = null;
                } else {
                    SplitNode grandParent = parent.getParent();
                    grandParent.removeChild(parent);
                }
            } else if (children.size() == 1) {
                Node orphan = children.get(0);
                if (parent == this.root) {
                    orphan.setParent(null);
                    this.root = orphan;
                } else {
                    SplitNode grandParent = parent.getParent();
                    int index = grandParent.getChildIndex(parent);
                    double weight = grandParent.getChildSplitWeight(parent);
                    grandParent.removeChild(parent);
                    grandParent.setChildAt(index, weight, orphan);
                }
            }
        }
        this.verifyNode(this.root);
        return true;
    }

    private void verifyNode(Node node) {
        if (node instanceof SplitNode) {
            SplitNode splitNode = (SplitNode)node;
            for (Node child : splitNode.getChildren()) {
                if (child.getParent() != splitNode) {
                    Logger.getLogger(SplitSubModel.class.getName()).log(Level.WARNING, null, new IllegalStateException("Node->" + child + " has wrong parent->" + child.getParent() + " is has to be->" + splitNode + " \nModel: " + this.toString()));
                    child.setParent(splitNode);
                }
                this.verifyNode(child);
            }
        }
    }

    public void reset() {
        SplitSubModel.detachNodes(this.root);
        this.root = null;
    }

    private static void detachNodes(Node node) {
        if (node instanceof SplitNode) {
            SplitNode splitNode = (SplitNode)node;
            for (Node child : splitNode.getChildren()) {
                splitNode.removeChild(child);
                SplitSubModel.detachNodes(child);
            }
        }
    }

    public boolean setSplitWeights(ModelElement[] snapshots, double[] splitWeights) {
        if (0 == snapshots.length) {
            return false;
        }
        for (int i = 0; i < snapshots.length; ++i) {
            Node node = (Node)snapshots[i];
            if (null != node && null != node.getParent()) continue;
            return false;
        }
        Node firstNode = (Node)snapshots[0];
        SplitNode parent = firstNode.getParent();
        if (parent == null || !this.isInTree(parent)) {
            return false;
        }
        boolean res = true;
        for (int i2 = 0; i2 < snapshots.length; ++i2) {
            Node node = (Node)snapshots[i2];
            double weight = splitWeights[i2];
            SplitNode parentNode = node.getParent();
            if (null == parentNode || !this.isInTree(parentNode)) {
                res = false;
                continue;
            }
            parentNode.setChildSplitWeight(node, weight);
        }
        return res;
    }

    public ModeStructureSnapshot.ElementSnapshot createSplitSnapshot() {
        return this.root == null ? null : this.root.createSnapshot();
    }

    public Set<ModeStructureSnapshot.ModeSnapshot> createSeparateSnapshots() {
        return this.findSeparateModeSnapshots(this.root);
    }

    private Set<ModeStructureSnapshot.ModeSnapshot> findSeparateModeSnapshots(Node node) {
        HashSet<ModeStructureSnapshot.ModeSnapshot> s = new HashSet<ModeStructureSnapshot.ModeSnapshot>();
        if (node instanceof ModeNode) {
            ModeNode modeNode = (ModeNode)node;
            if (modeNode.isVisibleSeparate()) {
                s.add((ModeStructureSnapshot.ModeSnapshot)modeNode.createSnapshot());
            }
        } else if (node instanceof SplitNode) {
            SplitNode splitNode = (SplitNode)node;
            for (Node child : splitNode.getChildren()) {
                s.addAll(this.findSeparateModeSnapshots(child));
            }
        }
        return s;
    }

    public String toString() {
        return SplitSubModel.dumpNode(this.root, 0, null);
    }

    private static String dumpNode(Node node, int ind, String state) {
        ++ind;
        if (node == null) {
            return "NULL NODE\n";
        }
        StringBuffer buffer = new StringBuffer();
        if (state == null) {
            buffer.append("\n");
        }
        StringBuffer sb = SplitSubModel.getOffset(ind);
        if (node instanceof ModeNode) {
            buffer.append(sb);
            buffer.append("<mode-node");
            buffer.append(" [" + Integer.toHexString(System.identityHashCode(node)) + "]");
            buffer.append(" index=\"");
            if (node.getParent() != null) {
                buffer.append(node.getParent().getChildIndex(node));
            }
            buffer.append(" splitWeight=");
            if (node.getParent() != null) {
                buffer.append(node.getParent().getChildSplitWeight(node));
            }
            buffer.append("\"");
            buffer.append(" state=\"");
            buffer.append(state);
            buffer.append("\"");
            buffer.append(" name=\"" + ((ModeNode)node).getMode().getName() + "\"");
            buffer.append(" parent=");
            buffer.append(node.getParent() == null ? null : "[" + Integer.toHexString(node.getParent().hashCode()) + "]");
            buffer.append(" constraints='" + Arrays.asList(node.getNodeConstraints()) + "\"");
            buffer.append("</mode-node>\n");
        } else if (node instanceof SplitNode) {
            buffer.append(sb);
            buffer.append("<split-node");
            buffer.append(" [" + Integer.toHexString(System.identityHashCode(node)) + "]");
            buffer.append(" index=\"");
            if (node.getParent() != null) {
                buffer.append(node.getParent().getChildIndex(node));
            }
            buffer.append(" splitWeight=");
            if (node.getParent() != null) {
                buffer.append(node.getParent().getChildSplitWeight(node));
            }
            buffer.append("\"");
            SplitNode split = (SplitNode)node;
            buffer.append(" state=\"");
            buffer.append(state);
            buffer.append("\" orientation=\"");
            buffer.append(split.getOrientation());
            buffer.append("\">\n");
            int j = 0;
            for (Node child : split.getChildren()) {
                buffer.append(SplitSubModel.dumpNode(child, ind, "child[" + j + "]"));
                ++j;
            }
            buffer.append(sb);
            buffer.append("</split-node>\n");
        } else {
            buffer.append(sb);
            buffer.append("<editor-node");
            buffer.append(" [" + Integer.toHexString(System.identityHashCode(node)) + "]");
            buffer.append(" index=\"");
            if (node.getParent() != null) {
                buffer.append(node.getParent().getChildIndex(node));
            }
            buffer.append("\"");
            buffer.append(" splitWeight=");
            if (node.getParent() != null) {
                buffer.append(node.getParent().getChildSplitWeight(node));
            }
            buffer.append(" parent=");
            buffer.append(node.getParent() == null ? null : "[" + Integer.toHexString(node.getParent().hashCode()) + "]");
            buffer.append("</editor-node>\n");
        }
        return buffer.toString();
    }

    private static StringBuffer getOffset(int ind) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < ind - 1; ++i) {
            sb.append("\t");
        }
        return sb;
    }

    public ModeImpl getModeForOriginator(ModelElement originator) {
        if (originator instanceof ModeNode) {
            return ((ModeNode)originator).getMode();
        }
        return null;
    }

    private static void debugLog(String message) {
        Debug.log(SplitSubModel.class, message);
    }

    private static class ModeNode
    extends Node {
        private final ModeImpl mode;

        public ModeNode(ModeImpl mode) {
            this.mode = mode;
        }

        public ModeImpl getMode() {
            return this.mode;
        }

        @Override
        public boolean isVisibleInSplit() {
            WindowManagerImpl wm;
            if (this.mode.getOpenedTopComponents().isEmpty()) {
                return false;
            }
            if (this.mode.getState() == 1) {
                return false;
            }
            if (this.mode.getKind() == 1 && null != (wm = WindowManagerImpl.getInstance()).getEditorMaximizedMode() && wm.getEditorMaximizedMode() != this.mode) {
                return false;
            }
            return true;
        }

        public boolean isVisibleSeparate() {
            if (this.mode.getOpenedTopComponents().isEmpty()) {
                return false;
            }
            if (this.mode.getState() == 0) {
                return false;
            }
            return true;
        }

        @Override
        public double getResizeWeight() {
            return 0.0;
        }

        @Override
        public ModeStructureSnapshot.ElementSnapshot createSnapshot() {
            return new ModeStructureSnapshot.ModeSnapshot(this, null, this.mode, this.getResizeWeight());
        }
    }

    protected static class SplitNode
    extends Node {
        private final int orientation;
        private final TreeMap<Integer, Node> index2child = new TreeMap();
        private final Map<Node, Double> child2splitWeight = new HashMap<Node, Double>();

        public SplitNode(int orientation) {
            this.orientation = orientation;
        }

        @Override
        public String toString() {
            StringBuffer sb = new StringBuffer();
            sb.append(super.toString());
            for (Integer index : this.index2child.keySet()) {
                Node child = this.index2child.get(index);
                sb.append("child[" + index + "]=" + child.getClass() + "@" + Integer.toHexString(child.hashCode()));
            }
            return sb.toString();
        }

        public int getOrientation() {
            return this.orientation;
        }

        public void setChildAt(int index, double splitWeight, Node child) {
            if (index == -1) {
                index = this.index2child.isEmpty() ? 0 : this.index2child.lastKey() + 1;
            }
            Integer ind = index;
            Node oldChild = this.index2child.get(ind);
            int i = ind + 1;
            while (oldChild != null) {
                oldChild = this.index2child.put(i, oldChild);
                ++i;
            }
            this.index2child.put(ind, child);
            this.setChildSplitWeightImpl(child, splitWeight);
            child.setParent(this);
            this.verifyChildren();
        }

        public Node getChildAt(int index) {
            return this.index2child.get(index);
        }

        private void verifyChildren() {
            for (Node child : this.index2child.values()) {
                if (child.getParent() == this) continue;
                Logger.getLogger(SplitSubModel.class.getName()).log(Level.WARNING, null, new IllegalStateException("Node " + child + " is a child in split " + this + " but his parent is " + child.getParent() + ". Repairing"));
                child.setParent(this);
            }
        }

        public double getChildSplitWeight(Node child) {
            Double db = this.child2splitWeight.get(child);
            if (db != null) {
                return db;
            }
            return -1.0;
        }

        public void setChildSplitWeight(Node child, double weight) {
            if (child == null || !this.child2splitWeight.keySet().contains(child)) {
                return;
            }
            this.setChildSplitWeightImpl(child, weight);
        }

        private void setChildSplitWeightImpl(Node child, double weight) {
            this.child2splitWeight.put(child, weight);
        }

        private void normalizeWeights(double ratio) {
            for (Map.Entry<Node, Double> entry : this.child2splitWeight.entrySet()) {
                double w = entry.getValue();
                w = ratio * w;
                entry.setValue(w);
            }
        }

        public int getChildIndex(Node child) {
            for (Integer key : this.index2child.keySet()) {
                if (child != this.index2child.get(key)) continue;
                return key;
            }
            return -1;
        }

        public List<Node> getChildren() {
            return new ArrayList<Node>(this.index2child.values());
        }

        public List<Node> getVisibleChildren() {
            List<Node> l = this.getChildren();
            Iterator<Node> it = l.iterator();
            while (it.hasNext()) {
                Node node = it.next();
                if (node.hasVisibleDescendant()) continue;
                it.remove();
            }
            return l;
        }

        protected boolean removeChild(Node child) {
            boolean result = this.index2child.values().remove(child);
            this.child2splitWeight.remove(child);
            child.setParent(null);
            return result;
        }

        @Override
        public boolean isVisibleInSplit() {
            int count = 0;
            for (Node node : this.index2child.values()) {
                if (!node.hasVisibleDescendant() || ++count < 2) continue;
                return true;
            }
            return false;
        }

        @Override
        public boolean hasVisibleDescendant() {
            for (Node node : this.index2child.values()) {
                if (!node.hasVisibleDescendant()) continue;
                return true;
            }
            return false;
        }

        @Override
        public double getResizeWeight() {
            List<Node> children = this.getVisibleChildren();
            double max = 0.0;
            Iterator<Node> it = children.iterator();
            while (it.hasNext()) {
                double resizeWeight = it.next().getResizeWeight();
                max = Math.max(max, resizeWeight);
            }
            return max;
        }

        @Override
        public ModeStructureSnapshot.ElementSnapshot createSnapshot() {
            ArrayList<ModeStructureSnapshot.ElementSnapshot> childSnapshots = new ArrayList<ModeStructureSnapshot.ElementSnapshot>();
            HashMap<ModeStructureSnapshot.ElementSnapshot, Double> childSnapshot2splitWeight = new HashMap<ModeStructureSnapshot.ElementSnapshot, Double>();
            for (Node child : this.getChildren()) {
                ModeStructureSnapshot.ElementSnapshot childSnapshot = child.createSnapshot();
                childSnapshots.add(childSnapshot);
                childSnapshot2splitWeight.put(childSnapshot, this.child2splitWeight.get(child));
            }
            ModeStructureSnapshot.SplitSnapshot splitSnapshot = new ModeStructureSnapshot.SplitSnapshot(this, null, this.getOrientation(), childSnapshots, childSnapshot2splitWeight, this.getResizeWeight());
            for (ModeStructureSnapshot.ElementSnapshot snapshot : childSnapshots) {
                snapshot.setParent(splitSnapshot);
            }
            return splitSnapshot;
        }
    }

    protected static abstract class Node
    implements ModelElement {
        private SplitNode parent;

        public String toString() {
            return super.toString() + "[parent=" + (this.parent == null ? null : new StringBuilder().append(this.parent.getClass()).append("@").append(Integer.toHexString(this.parent.hashCode())).toString()) + "]";
        }

        public void setParent(SplitNode parent) {
            if (this.parent == parent) {
                return;
            }
            this.parent = parent;
        }

        public SplitNode getParent() {
            return this.parent;
        }

        public abstract double getResizeWeight();

        public SplitConstraint[] getNodeConstraints() {
            Node node = this;
            ArrayList<SplitConstraint> conList = new ArrayList<SplitConstraint>(5);
            do {
                SplitConstraint item;
                if ((item = Node.getConstraintForNode(node)) == null) continue;
                conList.add(item);
            } while ((node = node.getParent()) != null);
            Collections.reverse(conList);
            return conList.toArray(new SplitConstraint[0]);
        }

        private static SplitConstraint getConstraintForNode(Node node) {
            SplitNode parent = node.getParent();
            if (parent != null) {
                return new SplitConstraint(parent.getOrientation(), parent.getChildIndex(node), parent.getChildSplitWeight(node));
            }
            return null;
        }

        public boolean isVisibleInSplit() {
            return false;
        }

        public boolean hasVisibleDescendant() {
            return this.isVisibleInSplit();
        }

        public abstract ModeStructureSnapshot.ElementSnapshot createSnapshot();
    }

}

