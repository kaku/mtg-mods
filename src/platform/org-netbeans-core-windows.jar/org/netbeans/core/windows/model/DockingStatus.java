/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.model.Model;

public class DockingStatus {
    protected Model model;
    protected List<String> docked = new ArrayList<String>(10);
    protected List<String> slided = new ArrayList<String>(10);
    private boolean marked = false;

    DockingStatus(Model model) {
        this.model = model;
    }

    public void mark() {
        Set<ModeImpl> modes = this.model.getModes();
        for (ModeImpl modeImpl : modes) {
            if (modeImpl.getState() == 1) continue;
            List<String> views = this.model.getModeOpenedTopComponentsIDs(modeImpl);
            if (modeImpl.getKind() == 0) {
                this.docked.addAll(views);
                this.slided.removeAll(views);
                continue;
            }
            if (modeImpl.getKind() != 2) continue;
            this.docked.removeAll(views);
            this.slided.addAll(views);
        }
        this.marked = true;
    }

    public boolean shouldDock(String tcID) {
        return null != tcID && this.docked.contains(tcID) && this.marked;
    }

    public boolean shouldSlide(String tcID) {
        return null != tcID && (this.slided.contains(tcID) || !this.slided.contains(tcID) && !this.docked.contains(tcID));
    }

    public void addDocked(String tcID) {
        if (null != tcID) {
            this.docked.add(tcID);
            this.slided.remove(tcID);
        }
    }

    public void addSlided(String tcID) {
        if (null != tcID) {
            this.slided.add(tcID);
            this.docked.remove(tcID);
        }
    }

    public boolean isDocked(String tcID) {
        return null != tcID && this.docked.contains(tcID);
    }

    public boolean isSlided(String tcID) {
        return null != tcID && this.slided.contains(tcID);
    }

    void clear() {
        this.docked.clear();
        this.slided.clear();
    }
}

