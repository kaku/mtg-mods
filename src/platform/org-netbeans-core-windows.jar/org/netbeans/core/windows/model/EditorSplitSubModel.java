/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows.model;

import org.netbeans.core.windows.ModeStructureSnapshot;
import org.netbeans.core.windows.SplitConstraint;
import org.netbeans.core.windows.model.Model;
import org.netbeans.core.windows.model.ModelElement;
import org.netbeans.core.windows.model.SplitSubModel;

final class EditorSplitSubModel
extends SplitSubModel {
    private final EditorNode editorNode;

    public EditorSplitSubModel(Model parentModel, SplitSubModel editorArea) {
        super(parentModel);
        this.editorNode = new EditorNode(editorArea);
        this.addNodeToTree(this.editorNode, new SplitConstraint[0]);
    }

    @Override
    protected boolean removeNodeFromTree(SplitSubModel.Node node) {
        if (node == this.editorNode) {
            return false;
        }
        return super.removeNodeFromTree(node);
    }

    public boolean setEditorNodeConstraints(SplitConstraint[] editorNodeConstraints) {
        super.removeNodeFromTree(this.editorNode);
        return this.addNodeToTree(this.editorNode, editorNodeConstraints);
    }

    public SplitConstraint[] getEditorNodeConstraints() {
        return this.editorNode.getNodeConstraints();
    }

    public SplitSubModel getEditorArea() {
        return this.editorNode.getEditorArea();
    }

    @Override
    public boolean setSplitWeights(ModelElement[] snapshots, double[] splitWeights) {
        if (super.setSplitWeights(snapshots, splitWeights)) {
            return true;
        }
        return this.getEditorArea().setSplitWeights(snapshots, splitWeights);
    }

    @Override
    protected boolean addNodeToTreeAroundEditor(SplitSubModel.Node addingNode, String side) {
        double dropRatio = 0.25;
        EditorNode attachNode = this.editorNode;
        if (attachNode == this.root) {
            int addingIndex = side == "top" || side == "left" ? 0 : -1;
            int oldIndex = addingIndex == 0 ? -1 : 0;
            int orientation = side == "top" || side == "bottom" ? 0 : 1;
            SplitSubModel.SplitNode newSplit = new SplitSubModel.SplitNode(orientation);
            newSplit.setChildAt(addingIndex, dropRatio, addingNode);
            newSplit.setChildAt(oldIndex, 1.0 - dropRatio, attachNode);
            this.root = newSplit;
        } else {
            int orientation;
            SplitSubModel.SplitNode parent = attachNode.getParent();
            if (parent == null) {
                return false;
            }
            int attachIndex = parent.getChildIndex(attachNode);
            double attachWeight = parent.getChildSplitWeight(attachNode);
            int n = orientation = side == "top" || side == "bottom" ? 0 : 1;
            if (orientation == parent.getOrientation()) {
                if (side == "bottom" || side == "right") {
                    ++attachIndex;
                }
                parent.setChildAt(attachIndex, dropRatio, addingNode);
            } else {
                SplitSubModel.SplitNode newSplit = new SplitSubModel.SplitNode(orientation);
                parent.removeChild(attachNode);
                int addingIndex = side == "top" || side == "left" ? 0 : -1;
                int oldIndex = addingIndex == 0 ? -1 : 0;
                newSplit.setChildAt(addingIndex, dropRatio, addingNode);
                newSplit.setChildAt(oldIndex, 1.0 - dropRatio, attachNode);
                parent.setChildAt(attachIndex, attachWeight, newSplit);
            }
        }
        return true;
    }

    static class EditorNode
    extends SplitSubModel.Node {
        private final SplitSubModel editorArea;

        public EditorNode(SplitSubModel editorArea) {
            this.editorArea = editorArea;
        }

        @Override
        public boolean isVisibleInSplit() {
            return true;
        }

        public SplitSubModel getEditorArea() {
            return this.editorArea;
        }

        @Override
        public double getResizeWeight() {
            return 1.0;
        }

        @Override
        public ModeStructureSnapshot.ElementSnapshot createSnapshot() {
            return new ModeStructureSnapshot.EditorSnapshot(this, null, this.editorArea.createSplitSnapshot(), this.getResizeWeight());
        }
    }

}

