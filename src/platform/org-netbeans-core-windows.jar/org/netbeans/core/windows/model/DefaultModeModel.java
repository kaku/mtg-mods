/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows.model;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.SplitConstraint;
import org.netbeans.core.windows.Switches;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.model.ModeModel;
import org.netbeans.core.windows.model.TopComponentContextSubModel;
import org.netbeans.core.windows.model.TopComponentSubModel;
import org.openide.windows.TopComponent;

final class DefaultModeModel
implements ModeModel {
    private String name;
    private final Set<String> otherNames = new HashSet<String>(3);
    private final Rectangle bounds = new Rectangle();
    private final Rectangle boundsSeparetedHelp = new Rectangle();
    private int state;
    private final int kind;
    private int frameState;
    private boolean permanent;
    private boolean minimized;
    private final TopComponentSubModel topComponentSubModel;
    private TopComponentContextSubModel topComponentContextSubModel = null;
    private final Object LOCK_STATE = new Object();
    private final Object LOCK_BOUNDS = new Object();
    private final Object LOCK_BOUNDS_SEPARATED_HELP = new Object();
    private final Object LOCK_FRAMESTATE = new Object();
    private final Object LOCK_TOPCOMPONENTS = new Object();
    private final Object LOCK_TC_CONTEXTS = new Object();

    public DefaultModeModel(String name, int state, int kind, boolean permanent) {
        this.name = name;
        this.state = state;
        this.kind = kind;
        this.permanent = permanent;
        this.topComponentSubModel = new TopComponentSubModel(kind);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setState(int state) {
        Object object = this.LOCK_STATE;
        synchronized (object) {
            this.state = state;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeTopComponent(TopComponent tc, TopComponent recentTc) {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            this.topComponentSubModel.removeTopComponent(tc, recentTc);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeClosedTopComponentID(String tcID) {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            this.topComponentSubModel.removeClosedTopComponentID(tcID);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void addOpenedTopComponent(TopComponent tc) {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            this.topComponentSubModel.addOpenedTopComponent(tc);
            this.sortOpenedTopComponents();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void insertOpenedTopComponent(TopComponent tc, int index) {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            this.topComponentSubModel.insertOpenedTopComponent(tc, index);
            this.sortOpenedTopComponents();
        }
    }

    private void sortOpenedTopComponents() {
        if (this.getKind() != 2) {
            return;
        }
        if (!Switches.isModeSlidingEnabled()) {
            return;
        }
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        List<TopComponent> opened = this.topComponentSubModel.getOpenedTopComponents();
        final ArrayList<String> prevModes = new ArrayList<String>(opened.size());
        final HashMap<TopComponent, String> tc2modeName = new HashMap<TopComponent, String>(opened.size());
        for (TopComponent tc : opened) {
            ModeImpl prevMode;
            String tcId = wm.findTopComponentID(tc);
            if (null == tcId || null == (prevMode = this.getTopComponentPreviousMode(tcId))) continue;
            if (!prevModes.contains(prevMode.getName())) {
                prevModes.add(prevMode.getName());
            }
            tc2modeName.put(tc, prevMode.getName());
        }
        if (prevModes.isEmpty()) {
            return;
        }
        Collections.sort(opened, new Comparator<TopComponent>(){

            @Override
            public int compare(TopComponent o1, TopComponent o2) {
                String mode1 = (String)tc2modeName.get((Object)o1);
                String mode2 = (String)tc2modeName.get((Object)o2);
                if (null == mode1 && null != mode2) {
                    return 1;
                }
                if (null != mode1 && null == mode2) {
                    return -1;
                }
                return prevModes.indexOf(mode1) - prevModes.indexOf(mode2);
            }
        });
        this.topComponentSubModel.setOpenedTopComponents(opened);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void addClosedTopComponent(TopComponent tc) {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            this.topComponentSubModel.addClosedTopComponent(tc);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void addUnloadedTopComponent(String tcID, int index) {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            this.topComponentSubModel.addUnloadedTopComponent(tcID, index);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setUnloadedSelectedTopComponent(String tcID) {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            this.topComponentSubModel.setUnloadedSelectedTopComponent(tcID);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setUnloadedPreviousSelectedTopComponent(String tcID) {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            this.topComponentSubModel.setUnloadedPreviousSelectedTopComponent(tcID);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setSelectedTopComponent(TopComponent selected) {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            this.topComponentSubModel.setSelectedTopComponent(selected);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setPreviousSelectedTopComponentID(String prevSelectedId) {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            this.topComponentSubModel.setPreviousSelectedTopComponentID(prevSelectedId);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setFrameState(int frameState) {
        Object object = this.LOCK_FRAMESTATE;
        synchronized (object) {
            this.frameState = frameState;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setBounds(Rectangle bounds) {
        if (bounds == null) {
            return;
        }
        Object object = this.LOCK_BOUNDS;
        synchronized (object) {
            this.bounds.setBounds(bounds);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setBoundsSeparatedHelp(Rectangle boundsSeparatedHelp) {
        if (this.bounds == null) {
            return;
        }
        Object object = this.LOCK_BOUNDS_SEPARATED_HELP;
        synchronized (object) {
            this.boundsSeparetedHelp.setBounds(boundsSeparatedHelp);
        }
    }

    @Override
    public String getName() {
        return this.name;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Rectangle getBounds() {
        Object object = this.LOCK_BOUNDS;
        synchronized (object) {
            return (Rectangle)this.bounds.clone();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Rectangle getBoundsSeparatedHelp() {
        Object object = this.LOCK_BOUNDS_SEPARATED_HELP;
        synchronized (object) {
            return (Rectangle)this.boundsSeparetedHelp.clone();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int getState() {
        Object object = this.LOCK_STATE;
        synchronized (object) {
            return this.state;
        }
    }

    @Override
    public int getKind() {
        return this.kind;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int getFrameState() {
        Object object = this.LOCK_FRAMESTATE;
        synchronized (object) {
            return this.frameState;
        }
    }

    @Override
    public boolean isPermanent() {
        return this.permanent;
    }

    @Override
    public void makePermanent() {
        this.permanent = true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean isEmpty() {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            return this.topComponentSubModel.isEmpty();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean containsTopComponent(TopComponent tc) {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            return this.topComponentSubModel.containsTopComponent(tc);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public List<TopComponent> getTopComponents() {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            return this.topComponentSubModel.getTopComponents();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public TopComponent getSelectedTopComponent() {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            return this.topComponentSubModel.getSelectedTopComponent();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public String getPreviousSelectedTopComponentID() {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            return this.topComponentSubModel.getPreviousSelectedTopComponentID();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public List<TopComponent> getOpenedTopComponents() {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            return this.topComponentSubModel.getOpenedTopComponents();
        }
    }

    @Override
    public final void setName(String name) {
        this.name = name;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public List<String> getOpenedTopComponentsIDs() {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            return this.topComponentSubModel.getOpenedTopComponentsIDs();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public List<String> getClosedTopComponentsIDs() {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            return this.topComponentSubModel.getClosedTopComponentsIDs();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public List<String> getTopComponentsIDs() {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            return this.topComponentSubModel.getTopComponentsIDs();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int getOpenedTopComponentTabPosition(TopComponent tc) {
        Object object = this.LOCK_TOPCOMPONENTS;
        synchronized (object) {
            return this.topComponentSubModel.getOpenedTopComponentTabPosition(tc);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public SplitConstraint[] getTopComponentPreviousConstraints(String tcID) {
        Object object = this.LOCK_TC_CONTEXTS;
        synchronized (object) {
            return this.getContextSubModel().getTopComponentPreviousConstraints(tcID);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public ModeImpl getTopComponentPreviousMode(String tcID) {
        Object object = this.LOCK_TC_CONTEXTS;
        synchronized (object) {
            return this.getContextSubModel().getTopComponentPreviousMode(tcID);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int getTopComponentPreviousIndex(String tcID) {
        Object object = this.LOCK_TC_CONTEXTS;
        synchronized (object) {
            return this.getContextSubModel().getTopComponentPreviousIndex(tcID);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setTopComponentPreviousConstraints(String tcID, SplitConstraint[] constraints) {
        Object object = this.LOCK_TC_CONTEXTS;
        synchronized (object) {
            this.getContextSubModel().setTopComponentPreviousConstraints(tcID, constraints);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setTopComponentPreviousMode(String tcID, ModeImpl mode, int prevIndex) {
        Object object = this.LOCK_TC_CONTEXTS;
        synchronized (object) {
            this.getContextSubModel().setTopComponentPreviousMode(tcID, mode, prevIndex);
            this.sortOpenedTopComponents();
        }
    }

    private TopComponentContextSubModel getContextSubModel() {
        if (this.topComponentContextSubModel == null) {
            this.topComponentContextSubModel = new TopComponentContextSubModel();
        }
        return this.topComponentContextSubModel;
    }

    @Override
    public boolean isMinimized() {
        return this.minimized;
    }

    @Override
    public void setMinimized(boolean minimized) {
        this.minimized = minimized;
    }

    @Override
    public Collection<String> getOtherNames() {
        return Collections.unmodifiableSet(this.otherNames);
    }

    @Override
    public void addOtherName(String otherModeName) {
        this.otherNames.add(otherModeName);
    }

}

