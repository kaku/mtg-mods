/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows;

public enum WindowSystemEventType {
    beforeLoad,
    afterLoad,
    beforeSave,
    afterSave;
    

    private WindowSystemEventType() {
    }
}

