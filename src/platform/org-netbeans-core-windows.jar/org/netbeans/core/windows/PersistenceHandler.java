/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.ToolbarPool
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.Utilities
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.windows;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.core.windows.Central;
import org.netbeans.core.windows.ConfigFactory;
import org.netbeans.core.windows.Debug;
import org.netbeans.core.windows.LazyLoader;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.SplitConstraint;
import org.netbeans.core.windows.TopComponentGroupImpl;
import org.netbeans.core.windows.TopComponentTracker;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.persistence.GroupConfig;
import org.netbeans.core.windows.persistence.ModeConfig;
import org.netbeans.core.windows.persistence.PersistenceManager;
import org.netbeans.core.windows.persistence.PersistenceObserver;
import org.netbeans.core.windows.persistence.TCGroupConfig;
import org.netbeans.core.windows.persistence.TCRefConfig;
import org.netbeans.core.windows.persistence.WindowManagerConfig;
import org.openide.awt.ToolbarPool;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public final class PersistenceHandler
implements PersistenceObserver {
    private final Map<String, ModeImpl> name2mode = new WeakHashMap<String, ModeImpl>(10);
    private final Map<String, TopComponentGroupImpl> name2group = new WeakHashMap<String, TopComponentGroupImpl>(10);
    private static PersistenceHandler defaultInstance;
    private boolean loaded = false;
    private static final boolean DEBUG;
    private final LazyLoader lazyLoader = new LazyLoader();

    private PersistenceHandler() {
    }

    public void clear() {
        this.name2mode.clear();
        this.name2group.clear();
        TopComponentTracker.getDefault().clear();
    }

    public static synchronized PersistenceHandler getDefault() {
        if (defaultInstance == null) {
            defaultInstance = new PersistenceHandler();
        }
        return defaultInstance;
    }

    public static boolean isTopComponentPersistentWhenClosed(TopComponent tc) {
        return PersistenceManager.isTopComponentPersistentWhenClosed(tc);
    }

    boolean isLoaded() {
        return this.loaded;
    }

    public void load() {
        Rectangle separatedBounds;
        TopComponent active;
        if (DEBUG) {
            PersistenceHandler.debugLog("## PersistenceHandler.load");
        }
        TopComponentTracker.getDefault().load();
        WindowManagerConfig wmc = null;
        try {
            wmc = PersistenceManager.getDefault().loadWindowSystem();
        }
        catch (IOException exc) {
            Exceptions.attachLocalizedMessage((Throwable)exc, (String)"Cannot load window system persistent data, user directory content is broken. Resetting to default layout...");
            Logger.getLogger(PersistenceHandler.class.getName()).log(Level.WARNING, null, exc);
            try {
                FileObject rootFolder = PersistenceManager.getDefault().getRootLocalFolder();
                if (null != rootFolder) {
                    rootFolder.delete();
                    wmc = PersistenceManager.getDefault().loadWindowSystem();
                } else {
                    Logger.getLogger(PersistenceHandler.class.getName()).log(Level.WARNING, "Cannot even get access to local winsys configuration, using internally predefined configuration.");
                    wmc = ConfigFactory.createDefaultConfig();
                }
            }
            catch (IOException ioE) {
                Exceptions.attachLocalizedMessage((Throwable)ioE, (String)"Cannot load even default layout, using internally predefined configuration.");
                Logger.getLogger(PersistenceHandler.class.getName()).log(Level.WARNING, null, ioE);
                wmc = ConfigFactory.createDefaultConfig();
            }
        }
        ToolbarPool.getDefault().setPreferredIconSize(wmc.preferredToolbarIconSize);
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        if (wmc.tcIdViewList.length > 0) {
            wm.setRecentViewList(wmc.tcIdViewList);
        } else {
            ArrayList<String> idList = new ArrayList<String>();
            for (int i = 0; i < wmc.modes.length; ++i) {
                ModeConfig mc = wmc.modes[i];
                for (int j = 0; j < mc.tcRefConfigs.length; ++j) {
                    if (!mc.tcRefConfigs[j].opened) continue;
                    idList.add(mc.tcRefConfigs[j].tc_id);
                }
            }
            wm.setRecentViewList(idList.toArray(new String[idList.size()]));
        }
        wm.setEditorAreaConstraints(wmc.editorAreaConstraints);
        wm.setEditorAreaState(wmc.editorAreaState);
        ModeImpl activeMode = null;
        ModeImpl editorMaximizedMode = null;
        ModeImpl viewMaximizedMode = null;
        HashMap<ModeImpl, ModeConfig> mode2config = new HashMap<ModeImpl, ModeConfig>();
        for (int i = 0; i < wmc.modes.length; ++i) {
            ModeConfig mc = wmc.modes[i];
            ModeImpl mode = this.getModeFromConfig(mc);
            mode2config.put(mode, mc);
            if (mc.name.equals(wmc.activeModeName)) {
                activeMode = mode;
            }
            if (mc.name.equals(wmc.editorMaximizedModeName)) {
                editorMaximizedMode = mode;
                continue;
            }
            if (!mc.name.equals(wmc.viewMaximizedModeName)) continue;
            viewMaximizedMode = mode;
        }
        TopComponentTracker tcTracker = TopComponentTracker.getDefault();
        TopComponent activeTopComponentOverride = null;
        block7 : for (ModeImpl mode : mode2config.keySet()) {
            ModeConfig mc = (ModeConfig)mode2config.get(mode);
            this.initModeFromConfig(mode, mc, false);
            this.initPreviousModes(mode, mc, mode2config);
            if (mc.selectedTopComponentID != null) {
                mode.setUnloadedSelectedTopComponent(mc.selectedTopComponentID);
            }
            if (mc.previousSelectedTopComponentID != null) {
                mode.setUnloadedPreviousSelectedTopComponent(mc.previousSelectedTopComponentID);
            }
            for (String tcId : mode.getTopComponentsIDs()) {
                tcTracker.add(tcId, mode);
            }
            for (TopComponent tc : mode.getOpenedTopComponents()) {
                Object val = tc.getClientProperty((Object)"netbeans.winsys.tc.activate_at_startup");
                if (null == val || !(val instanceof Boolean) || !((Boolean)val).booleanValue()) continue;
                activeTopComponentOverride = tc;
                continue block7;
            }
        }
        for (int i2 = 0; i2 < wmc.groups.length; ++i2) {
            GroupConfig groupCfg = wmc.groups[i2];
            this.createTopComponentGroupFromConfig(groupCfg);
        }
        if (activeMode != null && (active = activeMode.getSelectedTopComponent()) != null) {
            WindowManagerImpl.getInstance().specialPersistenceCompShow(active);
        }
        wm.setActiveMode(activeMode);
        wm.setEditorMaximizedMode(editorMaximizedMode);
        wm.setViewMaximizedMode(viewMaximizedMode);
        Rectangle joinedBounds = PersistenceHandler.computeBounds(wmc.centeredHorizontallyJoined, wmc.centeredVerticallyJoined, wmc.xJoined, wmc.yJoined, wmc.widthJoined, wmc.heightJoined, wmc.relativeXJoined, wmc.relativeYJoined, wmc.relativeWidthJoined, wmc.relativeHeightJoined);
        if (joinedBounds != null) {
            wm.setMainWindowBoundsJoined(joinedBounds);
        }
        if ((separatedBounds = PersistenceHandler.computeBounds(wmc.centeredHorizontallySeparated, wmc.centeredVerticallySeparated, wmc.xSeparated, wmc.ySeparated, wmc.widthSeparated, wmc.heightSeparated, wmc.relativeXSeparated, wmc.relativeYSeparated, wmc.relativeWidthSeparated, wmc.relativeHeightSeparated)) != null) {
            wm.setMainWindowBoundsSeparated(separatedBounds);
        }
        wm.setMainWindowFrameStateJoined(wmc.mainWindowFrameStateJoined);
        wm.setMainWindowFrameStateSeparated(wmc.mainWindowFrameStateSeparated);
        Rectangle absBounds = wmc.editorAreaBounds == null ? new Rectangle() : wmc.editorAreaBounds;
        Rectangle relBounds = wmc.editorAreaRelativeBounds == null ? new Rectangle() : wmc.editorAreaRelativeBounds;
        Rectangle bounds = PersistenceHandler.computeBounds(false, false, absBounds.x, absBounds.y, absBounds.width, absBounds.height, (float)relBounds.x / 100.0f, (float)relBounds.y / 100.0f, (float)relBounds.width / 100.0f, (float)relBounds.height / 100.0f);
        wm.setEditorAreaBounds(bounds);
        wm.setEditorAreaFrameState(wmc.editorAreaFrameState);
        wm.setToolbarConfigName(wmc.toolbarConfiguration);
        if (null != activeTopComponentOverride) {
            activeTopComponentOverride.requestActive();
        }
        this.loaded = true;
    }

    public synchronized void save() {
        if (!this.loaded) {
            return;
        }
        if (DEBUG) {
            PersistenceHandler.debugLog("## PersistenceHandler.save");
        }
        TopComponentTracker.getDefault().save();
        ToolbarPool.getDefault().waitFinished();
        WindowManagerConfig wmc = this.getConfig();
        PersistenceManager.getDefault().saveWindowSystem(wmc);
    }

    private ModeImpl getModeFromConfig(ModeConfig mc) {
        ModeImpl mode;
        if (DEBUG) {
            PersistenceHandler.debugLog("Getting mode name=" + mc.name);
        }
        if ((mode = (ModeImpl)WindowManagerImpl.getInstance().findMode(mc.name)) == null) {
            mode = this.createModeFromConfig(mc);
        }
        return mode;
    }

    private ModeImpl createModeFromConfig(ModeConfig mc) {
        if (DEBUG) {
            PersistenceHandler.debugLog("");
            PersistenceHandler.debugLog("Creating mode name=\"" + mc.name + "\"");
        }
        ModeImpl mode = mc.kind == 2 ? WindowManagerImpl.getInstance().createSlidingMode(mc.name, mc.permanent, mc.side, mc.slideInSizes) : WindowManagerImpl.getInstance().createMode(mc.name, mc.kind, mc.state, mc.permanent, mc.constraints);
        this.name2mode.put(mc.name, mode);
        if (mc.minimized) {
            mode.setMinimized(mc.minimized);
        }
        if (null != mc.otherNames) {
            for (String s : mc.otherNames) {
                mode.addOtherName(s);
            }
        }
        return mode;
    }

    private void initPreviousModes(ModeImpl mode, ModeConfig mc, Map modes) {
        for (int j = 0; j < mc.tcRefConfigs.length; ++j) {
            TCRefConfig tcRefConfig = mc.tcRefConfigs[j];
            if (DEBUG) {
                PersistenceHandler.debugLog("\tTopComponent[" + j + "] id=\"" + tcRefConfig.tc_id + "\", \topened=" + tcRefConfig.opened);
            }
            if (tcRefConfig.previousMode == null) continue;
            Iterator it = modes.keySet().iterator();
            ModeImpl previous = null;
            while (it.hasNext()) {
                ModeImpl md = (ModeImpl)it.next();
                if (!tcRefConfig.previousMode.equals(md.getName()) && !md.getOtherNames().contains(tcRefConfig.previousMode)) continue;
                previous = md;
                break;
            }
            if (previous != null) {
                WindowManagerImpl.getInstance().setPreviousModeForTopComponent(tcRefConfig.tc_id, mode, previous, tcRefConfig.previousIndex);
                continue;
            }
            Logger.getLogger(PersistenceHandler.class.getName()).log(Level.INFO, null, new NullPointerException("Cannot find previous mode named '" + tcRefConfig.previousMode + "'"));
        }
    }

    private void initPreviousMode(ModeImpl mode, TCRefConfig tcRefConfig) {
        if (DEBUG) {
            PersistenceHandler.debugLog("\tTopComponent id=\"" + tcRefConfig.tc_id + "\", \topened=" + tcRefConfig.opened);
        }
        if (tcRefConfig.previousMode == null) {
            return;
        }
        Set<? extends ModeImpl> modes = WindowManagerImpl.getInstance().getModes();
        Iterator<? extends ModeImpl> it = modes.iterator();
        ModeImpl previous = null;
        while (it.hasNext()) {
            ModeImpl md = it.next();
            if (!tcRefConfig.previousMode.equals(md.getName()) && !md.getOtherNames().contains(tcRefConfig.previousMode)) continue;
            previous = md;
            break;
        }
        if (previous != null) {
            WindowManagerImpl.getInstance().setPreviousModeForTopComponent(tcRefConfig.tc_id, mode, previous, tcRefConfig.previousIndex);
        } else {
            Logger.getLogger(PersistenceHandler.class.getName()).log(Level.INFO, null, new NullPointerException("Cannot find previous mode named '" + tcRefConfig.previousMode + "'"));
        }
    }

    private ModeImpl initModeFromConfig(ModeImpl mode, ModeConfig mc, boolean initPrevModes) {
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        if (null != mc.otherNames) {
            for (String s : mc.otherNames) {
                mode.addOtherName(s);
            }
        }
        for (int j = 0; j < mc.tcRefConfigs.length; ++j) {
            TCRefConfig tcRefConfig = mc.tcRefConfigs[j];
            if (DEBUG) {
                PersistenceHandler.debugLog("\tTopComponent[" + j + "] id=\"" + tcRefConfig.tc_id + "\", \topened=" + tcRefConfig.opened);
            }
            if (tcRefConfig.opened) {
                this.lazyLoader.lazyLoad(mode, mc.selectedTopComponentID, tcRefConfig, j);
            } else {
                mode.addUnloadedTopComponent(tcRefConfig.tc_id);
            }
            wm.setTopComponentDockedInMaximizedMode(tcRefConfig.tc_id, tcRefConfig.dockedInMaximizedMode);
            wm.setTopComponentSlidedInDefaultMode(tcRefConfig.tc_id, !tcRefConfig.dockedInDefaultMode);
            wm.setTopComponentMaximizedWhenSlidedIn(tcRefConfig.tc_id, tcRefConfig.slidedInMaximized);
            if (!initPrevModes) continue;
            this.initPreviousMode(mode, tcRefConfig);
        }
        Rectangle absBounds = mc.bounds == null ? new Rectangle() : mc.bounds;
        Rectangle relBounds = mc.relativeBounds == null ? new Rectangle() : mc.relativeBounds;
        Rectangle bounds = PersistenceHandler.computeBounds(false, false, absBounds.x, absBounds.y, absBounds.width, absBounds.height, (float)relBounds.x / 100.0f, (float)relBounds.y / 100.0f, (float)relBounds.width / 100.0f, (float)relBounds.height / 100.0f);
        mode.setBounds(bounds);
        mode.setFrameState(mc.frameState);
        mode.setMinimized(mc.minimized);
        return mode;
    }

    TopComponent getTopComponentForID(String tc_id, boolean deserialize) {
        if (tc_id == null || "".equals(tc_id)) {
            return null;
        }
        TopComponent tc = PersistenceManager.getDefault().getTopComponentForID(tc_id, deserialize);
        return tc;
    }

    private TopComponentGroupImpl createTopComponentGroupFromConfig(GroupConfig groupCfg) {
        if (DEBUG) {
            PersistenceHandler.debugLog("");
            PersistenceHandler.debugLog("Creating group name=\"" + groupCfg.name + "\" \t[opened=" + groupCfg.opened + "]");
        }
        TopComponentGroupImpl tcGroup = new TopComponentGroupImpl(groupCfg.name, groupCfg.opened);
        this.name2group.put(groupCfg.name, tcGroup);
        for (int j = 0; j < groupCfg.tcGroupConfigs.length; ++j) {
            TCGroupConfig tcGroupCfg = groupCfg.tcGroupConfigs[j];
            if (DEBUG) {
                PersistenceHandler.debugLog("\tTopComponent[" + j + "] id=\"" + tcGroupCfg.tc_id + "\", \topen=" + tcGroupCfg.open + ", \tclose=" + tcGroupCfg.close + ", \twasOpened=" + tcGroupCfg.wasOpened);
            }
            tcGroup.addUnloadedTopComponent(tcGroupCfg.tc_id);
            if (tcGroupCfg.open) {
                tcGroup.addUnloadedOpeningTopComponent(tcGroupCfg.tc_id);
            }
            if (tcGroupCfg.close) {
                tcGroup.addUnloadedClosingTopComponent(tcGroupCfg.tc_id);
            }
            if (!groupCfg.opened || !tcGroupCfg.wasOpened) continue;
            tcGroup.addGroupUnloadedOpenedTopComponent(tcGroupCfg.tc_id);
        }
        WindowManagerImpl.getInstance().addTopComponentGroup(tcGroup);
        return tcGroup;
    }

    private WindowManagerConfig getConfig() {
        WindowManagerConfig wmc = new WindowManagerConfig();
        wmc.preferredToolbarIconSize = ToolbarPool.getDefault().getPreferredIconSize();
        WindowManagerImpl wmi = WindowManagerImpl.getInstance();
        Rectangle joinedBounds = wmi.getMainWindowBoundsJoined();
        if (DEBUG) {
            PersistenceHandler.debugLog("joinedBouds=" + joinedBounds);
        }
        wmc.xJoined = joinedBounds.x;
        wmc.yJoined = joinedBounds.y;
        wmc.widthJoined = joinedBounds.width;
        wmc.heightJoined = joinedBounds.height;
        Rectangle separatedBounds = wmi.getMainWindowBoundsSeparated();
        if (DEBUG) {
            PersistenceHandler.debugLog("separatedBounds=" + separatedBounds);
        }
        wmc.xSeparated = separatedBounds.x;
        wmc.ySeparated = separatedBounds.y;
        wmc.widthSeparated = separatedBounds.width;
        wmc.heightSeparated = separatedBounds.height;
        wmc.mainWindowFrameStateJoined = Utilities.isMac() ? wmi.getMainWindow().getExtendedState() : wmi.getMainWindowFrameStateJoined();
        if (wmc.mainWindowFrameStateJoined == 1) {
            wmc.mainWindowFrameStateJoined = 0;
        }
        if (DEBUG) {
            PersistenceHandler.debugLog("mainWindowFrameStateJoined=" + wmc.mainWindowFrameStateJoined);
        }
        wmc.mainWindowFrameStateSeparated = wmi.getMainWindowFrameStateSeparated();
        if (wmc.mainWindowFrameStateSeparated == 1) {
            wmc.mainWindowFrameStateSeparated = 0;
        }
        if (DEBUG) {
            PersistenceHandler.debugLog("mainWindowFrameStateSeparated=" + wmc.mainWindowFrameStateSeparated);
        }
        wmc.editorAreaState = wmi.getEditorAreaState();
        if (DEBUG) {
            PersistenceHandler.debugLog("editorAreaState=" + wmc.editorAreaState);
        }
        wmc.editorAreaBounds = wmi.getEditorAreaBounds();
        if (DEBUG) {
            PersistenceHandler.debugLog("editorAreaBounds=" + wmc.editorAreaBounds);
        }
        wmc.editorAreaConstraints = wmi.getEditorAreaConstraints();
        if (DEBUG) {
            PersistenceHandler.debugLog("editorAreaConstraints=" + Arrays.toString(wmc.editorAreaConstraints));
        }
        wmc.editorAreaFrameState = wmi.getEditorAreaFrameState();
        if (DEBUG) {
            PersistenceHandler.debugLog("editorAreaFrameState=" + wmc.editorAreaFrameState);
        }
        wmc.screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        ModeImpl mo = wmi.getActiveMode();
        if (DEBUG) {
            PersistenceHandler.debugLog("active mode=" + mo);
        }
        if (mo != null) {
            wmc.activeModeName = mo.getName();
        }
        mo = wmi.getEditorMaximizedMode();
        if (DEBUG) {
            PersistenceHandler.debugLog("editor maximized mode=" + mo);
        }
        if (mo != null) {
            wmc.editorMaximizedModeName = mo.getName();
        }
        mo = wmi.getViewMaximizedMode();
        if (DEBUG) {
            PersistenceHandler.debugLog("view maximized mode=" + mo);
        }
        if (mo != null) {
            wmc.viewMaximizedModeName = mo.getName();
        }
        wmc.toolbarConfiguration = wmi.getToolbarConfigName();
        if (DEBUG) {
            PersistenceHandler.debugLog("toolbarConfiguration=" + wmc.toolbarConfiguration);
        }
        Set<? extends ModeImpl> modeSet = wmi.getModes();
        ArrayList<ModeConfig> modeConfigs = new ArrayList<ModeConfig>(modeSet.size());
        for (ModeImpl modeImpl : modeSet) {
            ModeConfig mc = this.getConfigFromMode(modeImpl);
            if (mc.tcRefConfigs.length == 0 && !mc.permanent && wmi.getCentral().doCheckSlidingModes(modeImpl)) continue;
            modeConfigs.add(mc);
        }
        wmc.modes = modeConfigs.toArray(new ModeConfig[0]);
        Set<TopComponentGroupImpl> tcGroups = wmi.getTopComponentGroups();
        ArrayList<GroupConfig> groupConfigs = new ArrayList<GroupConfig>(tcGroups.size());
        Iterator<TopComponentGroupImpl> it = tcGroups.iterator();
        while (it.hasNext()) {
            groupConfigs.add(this.getConfigFromGroup(it.next()));
        }
        wmc.groups = groupConfigs.toArray(new GroupConfig[0]);
        PersistenceManager pm = PersistenceManager.getDefault();
        TopComponent[] tcs = wmi.getRecentViewList();
        ArrayList<String> tcIdList = new ArrayList<String>(tcs.length);
        for (int i = 0; i < tcs.length; ++i) {
            if (!pm.isTopComponentPersistent(tcs[i])) continue;
            String tc_id = WindowManager.getDefault().findTopComponentID(tcs[i]);
            tc_id = PersistenceManager.escapeTcId4XmlContent(tc_id);
            tcIdList.add(tc_id);
        }
        wmc.tcIdViewList = tcIdList.toArray(new String[tcIdList.size()]);
        return wmc;
    }

    private ModeConfig getConfigFromMode(ModeImpl mode) {
        TopComponent selectedTC;
        String prevSelectedTCID;
        Object relBounds;
        PersistenceManager pm = PersistenceManager.getDefault();
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        ModeConfig modeCfg = new ModeConfig();
        modeCfg.name = mode.getName();
        if (DEBUG) {
            PersistenceHandler.debugLog("");
            PersistenceHandler.debugLog("mode name=" + modeCfg.name);
        }
        modeCfg.state = mode.getState();
        if (DEBUG) {
            PersistenceHandler.debugLog("mode state=" + modeCfg.state);
        }
        modeCfg.kind = mode.getKind();
        if (DEBUG) {
            PersistenceHandler.debugLog("mode kind=" + modeCfg.kind);
        }
        if (wm instanceof WindowManagerImpl) {
            modeCfg.side = wm.getCentral().getModeSide(mode);
            if (null != modeCfg.side) {
                modeCfg.slideInSizes = wm.getCentral().getSlideInSizes(modeCfg.side);
            }
        }
        if (DEBUG) {
            PersistenceHandler.debugLog("mode side=" + modeCfg.side);
        }
        modeCfg.constraints = mode.getConstraints();
        if (DEBUG) {
            PersistenceHandler.debugLog("mode constraints=" + Arrays.toString(modeCfg.constraints));
        }
        if ((relBounds = null) != null) {
            modeCfg.relativeBounds = relBounds;
        } else {
            modeCfg.bounds = mode.getBounds();
            if (DEBUG) {
                PersistenceHandler.debugLog("mode bounds=" + modeCfg.bounds);
            }
        }
        modeCfg.frameState = mode.getFrameState();
        if (DEBUG) {
            PersistenceHandler.debugLog("mode frame state=" + modeCfg.frameState);
        }
        if (modeCfg.frameState == 6 && Utilities.isWindows()) {
            if (modeCfg.bounds.x < 0) {
                modeCfg.bounds.x += 4;
                modeCfg.bounds.width -= 4;
            }
            if (modeCfg.bounds.y < 0) {
                modeCfg.bounds.y += 4;
                modeCfg.bounds.height -= 4;
            }
        }
        if ((selectedTC = mode.getSelectedTopComponent()) != null && pm.isTopComponentPersistent(selectedTC)) {
            String tc_id = wm.findTopComponentID(selectedTC);
            if (DEBUG) {
                PersistenceHandler.debugLog("selected tc=" + selectedTC.getName());
            }
            modeCfg.selectedTopComponentID = tc_id;
        }
        modeCfg.permanent = mode.isPermanent();
        if (DEBUG) {
            PersistenceHandler.debugLog("mode permanent=" + modeCfg.permanent);
        }
        if ((prevSelectedTCID = mode.getPreviousSelectedTopComponentID()) != null) {
            if (DEBUG) {
                PersistenceHandler.debugLog("previous selected tc id=" + prevSelectedTCID);
            }
            modeCfg.previousSelectedTopComponentID = prevSelectedTCID;
        }
        modeCfg.minimized = mode.isMinimized();
        modeCfg.otherNames = mode.getOtherNames();
        ArrayList<TCRefConfig> tcRefCfgList = new ArrayList<TCRefConfig>();
        List<String> openedTcIDs = mode.getOpenedTopComponentsIDs();
        for (String tcID : mode.getTopComponentsIDs()) {
            boolean opened = openedTcIDs.contains(tcID);
            if (opened ? pm.isTopComponentNonPersistentForID(tcID) : pm.isTopComponentNonPersistentForID(tcID) || pm.isTopComponentPersistentOnlyOpenedForID(tcID)) continue;
            String modeName = null;
            int prevIndex = -1;
            ModeImpl prev = wm.getPreviousModeForTopComponent(tcID, mode);
            if (prev != null) {
                modeName = prev.getName();
                prevIndex = wm.getPreviousIndexForTopComponent(tcID, mode);
            }
            if (DEBUG) {
                PersistenceHandler.debugLog("tc ID=" + tcID + " opened=" + opened);
            }
            TCRefConfig tcRefCfg = new TCRefConfig();
            tcRefCfg.tc_id = tcID;
            tcRefCfg.opened = opened;
            tcRefCfg.previousMode = modeName;
            tcRefCfg.previousIndex = prevIndex;
            tcRefCfg.dockedInMaximizedMode = wm.isTopComponentDockedInMaximizedMode(tcID);
            tcRefCfg.dockedInDefaultMode = !wm.isTopComponentSlidedInDefaultMode(tcID);
            tcRefCfg.slidedInMaximized = wm.isTopComponentMaximizedWhenSlidedIn(tcID);
            tcRefCfgList.add(tcRefCfg);
        }
        modeCfg.tcRefConfigs = tcRefCfgList.toArray(new TCRefConfig[tcRefCfgList.size()]);
        return modeCfg;
    }

    private GroupConfig getConfigFromGroup(TopComponentGroupImpl tcGroup) {
        GroupConfig groupCfg = new GroupConfig();
        groupCfg.name = tcGroup.getName();
        groupCfg.opened = tcGroup.isOpened();
        if (DEBUG) {
            PersistenceHandler.debugLog("");
            PersistenceHandler.debugLog("group name=" + groupCfg.name);
        }
        Set<String> openSet = tcGroup.getOpeningSetIDs();
        Set<String> closeSet = tcGroup.getClosingSetIDs();
        Set<String> wasOpenedSet = tcGroup.getGroupOpenedTopComponentsIDs();
        HashMap<String, TCGroupConfig> tcGroupCfgMap = new HashMap<String, TCGroupConfig>();
        for (String tcID : tcGroup.getTopComponentsIDs()) {
            TCGroupConfig tcGroupCfg;
            if (tcGroupCfgMap.containsKey(tcID)) {
                tcGroupCfg = (TCGroupConfig)tcGroupCfgMap.get(tcID);
            } else {
                tcGroupCfg = new TCGroupConfig();
                tcGroupCfg.tc_id = tcID;
                tcGroupCfgMap.put(tcID, tcGroupCfg);
            }
            tcGroupCfg.open = openSet.contains(tcID);
            tcGroupCfg.close = closeSet.contains(tcID);
            if (groupCfg.opened) {
                tcGroupCfg.wasOpened = wasOpenedSet.contains(tcID);
            }
            if (!DEBUG) continue;
            PersistenceHandler.debugLog("tc id=" + tcGroupCfg.tc_id + ", open=" + tcGroupCfg.open + ", close=" + tcGroupCfg.close + ", wasOpened=" + tcGroupCfg.wasOpened);
        }
        groupCfg.tcGroupConfigs = tcGroupCfgMap.values().toArray(new TCGroupConfig[0]);
        return groupCfg;
    }

    @Override
    public synchronized void modeConfigAdded(ModeConfig modeConfig) {
        if (DEBUG) {
            PersistenceHandler.debugLog("WMI.modeConfigAdded mo:" + modeConfig.name);
        }
        ModeImpl mode = this.getModeFromConfig(modeConfig);
        this.initModeFromConfig(mode, modeConfig, true);
    }

    @Override
    public synchronized void modeConfigRemoved(String modeName) {
        ModeImpl mode;
        if (DEBUG) {
            PersistenceHandler.debugLog("WMI.modeConfigRemoved mo:" + modeName);
        }
        if ((mode = this.name2mode.remove(modeName)) != null) {
            WindowManagerImpl.getInstance().removeMode(mode);
        } else {
            Logger.getLogger(PersistenceHandler.class.getName()).log(Level.WARNING, null, new NullPointerException("Mode for name=" + modeName + " was not created"));
        }
    }

    @Override
    public synchronized void topComponentRefConfigAdded(String modeName, TCRefConfig tcRefConfig, String[] tcRefNames) {
        TopComponent tc;
        if (DEBUG) {
            PersistenceHandler.debugLog("WMI.topComponentRefConfigAdded mo:" + modeName + " tcRef:" + tcRefConfig.tc_id);
        }
        WindowManagerImpl wm = WindowManagerImpl.getInstance();
        wm.setTopComponentDockedInMaximizedMode(tcRefConfig.tc_id, tcRefConfig.dockedInMaximizedMode);
        wm.setTopComponentSlidedInDefaultMode(tcRefConfig.tc_id, !tcRefConfig.dockedInDefaultMode);
        wm.setTopComponentMaximizedWhenSlidedIn(tcRefConfig.tc_id, tcRefConfig.slidedInMaximized);
        ModeImpl mode = this.name2mode.get(modeName);
        if (null == mode) {
            for (ModeImpl m : this.name2mode.values()) {
                if (!m.getOtherNames().contains(modeName)) continue;
                mode = m;
                break;
            }
        }
        if (mode != null) {
            this.initPreviousMode(mode, tcRefConfig);
        }
        if ((tc = this.getTopComponentForID(tcRefConfig.tc_id, true)) != null && mode != null) {
            if (tcRefConfig.opened) {
                mode.addOpenedTopComponent(tc);
            } else {
                mode.addClosedTopComponent(tc);
            }
        }
    }

    @Override
    public synchronized void topComponentRefConfigRemoved(String tc_id) {
        ModeImpl mode;
        WindowManagerImpl wm;
        if (DEBUG) {
            PersistenceHandler.debugLog("WMI.topComponentRefConfigRemoved tcRef:" + tc_id);
        }
        if ((mode = (wm = WindowManagerImpl.getInstance()).findModeForOpenedID(tc_id)) != null) {
            TopComponent tc = this.getTopComponentForID(tc_id, true);
            if (tc != null) {
                mode.removeTopComponent(tc);
            }
        } else {
            mode = wm.findModeForClosedID(tc_id);
            if (mode != null) {
                mode.removeClosedTopComponentID(tc_id);
            }
        }
    }

    @Override
    public synchronized void groupConfigAdded(GroupConfig groupConfig) {
        if (DEBUG) {
            PersistenceHandler.debugLog("WMI.groupConfigAdded group:" + groupConfig.name);
        }
        this.createTopComponentGroupFromConfig(groupConfig);
    }

    @Override
    public synchronized void groupConfigRemoved(String groupName) {
        TopComponentGroupImpl group;
        if (DEBUG) {
            PersistenceHandler.debugLog("WMI.groupConfigRemoved group:" + groupName);
        }
        if ((group = this.name2group.remove(groupName)) != null) {
            WindowManagerImpl.getInstance().removeTopComponentGroup(group);
        } else {
            Logger.getLogger(PersistenceHandler.class.getName()).log(Level.WARNING, null, new NullPointerException("Null group for name=" + groupName));
        }
    }

    @Override
    public synchronized void topComponentGroupConfigAdded(String groupName, TCGroupConfig tcGroupConfig) {
        TopComponentGroupImpl group;
        if (DEBUG) {
            PersistenceHandler.debugLog("WMI.topComponentGroupConfigAdded group:" + groupName + " tcGroup:" + tcGroupConfig.tc_id);
        }
        if ((group = this.name2group.get(groupName)) != null) {
            group.addUnloadedTopComponent(tcGroupConfig.tc_id);
            if (tcGroupConfig.open) {
                group.addUnloadedOpeningTopComponent(tcGroupConfig.tc_id);
            }
            if (tcGroupConfig.close) {
                group.addUnloadedClosingTopComponent(tcGroupConfig.tc_id);
            }
        }
    }

    @Override
    public synchronized void topComponentGroupConfigRemoved(String groupName, String tc_id) {
        TopComponentGroupImpl group;
        if (DEBUG) {
            PersistenceHandler.debugLog("WMI.topComponentGroupConfigRemoved group:" + groupName + " tcGroup:" + tc_id);
        }
        if ((group = this.name2group.get(groupName)) != null) {
            group.removeUnloadedTopComponent(tc_id);
        }
    }

    private static String dumpConfig(WindowManagerConfig wmc) {
        int i;
        int k;
        StringBuffer buffer = new StringBuffer();
        buffer.append("\n-- wmc: [" + Integer.toHexString(System.identityHashCode(wmc)) + "]");
        buffer.append("\n-- JOINED --");
        buffer.append("\n-- x: " + wmc.xJoined);
        buffer.append("\n-- y: " + wmc.yJoined);
        buffer.append("\n--  width: " + wmc.widthJoined);
        buffer.append("\n-- height: " + wmc.heightJoined);
        buffer.append("\n--  relativeX: " + wmc.relativeXJoined);
        buffer.append("\n--  relativeY: " + wmc.relativeYJoined);
        buffer.append("\n--  relativeWidth: " + wmc.relativeWidthJoined);
        buffer.append("\n-- relativeHeight: " + wmc.relativeHeightJoined);
        buffer.append("\n-- centeredHorizontally: " + wmc.centeredHorizontallyJoined);
        buffer.append("\n--   centeredVertically: " + wmc.centeredVerticallyJoined);
        buffer.append("\n--    maximizeIfWidthBelowJoined: " + wmc.maximizeIfWidthBelowJoined);
        buffer.append("\n--   maximizeIfHeightBelowJoined: " + wmc.maximizeIfHeightBelowJoined);
        buffer.append("\n-- SEPARATED --");
        buffer.append("\n-- x: " + wmc.xSeparated);
        buffer.append("\n-- y: " + wmc.ySeparated);
        buffer.append("\n--  width: " + wmc.widthSeparated);
        buffer.append("\n-- height: " + wmc.heightSeparated);
        buffer.append("\n--  relativeX: " + wmc.relativeXSeparated);
        buffer.append("\n--  relativeY: " + wmc.relativeYSeparated);
        buffer.append("\n--  relativeWidth: " + wmc.relativeWidthSeparated);
        buffer.append("\n-- relativeHeight: " + wmc.relativeHeightSeparated);
        buffer.append("\n-- centeredHorizontally: " + wmc.centeredHorizontallySeparated);
        buffer.append("\n--   centeredVertically: " + wmc.centeredVerticallySeparated);
        buffer.append("\n-- editorAreaState: " + wmc.editorAreaState);
        if (wmc.editorAreaConstraints != null) {
            for (i = 0; i < wmc.editorAreaConstraints.length; ++i) {
                buffer.append("\n-- co[" + i + "]: " + wmc.editorAreaConstraints[i]);
            }
        }
        buffer.append("\n--         editorAreaBounds: " + wmc.editorAreaBounds);
        buffer.append("\n-- editorAreaRelativeBounds: " + wmc.editorAreaRelativeBounds);
        buffer.append("\n--     screenSize: " + wmc.screenSize);
        buffer.append("\n--    activeModeName: " + wmc.activeModeName);
        buffer.append("\n-- editorMaximizedModeName: " + wmc.editorMaximizedModeName);
        buffer.append("\n-- viewMaximizedModeName: " + wmc.viewMaximizedModeName);
        buffer.append("\n--     toolbarconfig: " + wmc.toolbarConfiguration);
        buffer.append("\n-- modes: " + Arrays.toString(wmc.modes) + " size " + (wmc.modes == null ? -1 : wmc.modes.length));
        for (i = 0; i < wmc.modes.length; ++i) {
            ModeConfig mc = wmc.modes[i];
            buffer.append("\n-- --");
            buffer.append("\n-- -- mode[" + i + "]: " + mc.name);
            buffer.append("\n-- -- state: " + mc.state + " " + (mc.state == 0 ? "joined" : "separated"));
            if (mc.constraints != null) {
                buffer.append("\n-- -- constraints.sz: " + mc.constraints.length);
                for (int j = 0; j < mc.constraints.length; ++j) {
                    buffer.append("\n-- -- co[" + j + "]: " + mc.constraints[j]);
                }
            }
            buffer.append("\n-- -- kind: " + mc.kind + " ");
            if (mc.kind == 1) {
                buffer.append("editor");
            } else if (mc.kind == 0) {
                buffer.append("view");
            } else if (mc.kind == 2) {
                buffer.append("sliding");
            } else {
                buffer.append("unknown");
            }
            buffer.append("\n-- --         bounds: " + mc.bounds);
            buffer.append("\n-- -- relativeBounds: " + mc.relativeBounds);
            buffer.append("\n-- --          state: " + mc.frameState);
            buffer.append("\n-- -- active-tc: " + mc.selectedTopComponentID);
            buffer.append("\n-- -- permanent: " + mc.permanent);
            if (mc.tcRefConfigs == null) continue;
            for (k = 0; k < mc.tcRefConfigs.length; ++k) {
                TCRefConfig tcRefCfg = mc.tcRefConfigs[k];
                buffer.append("\n++ ++ ++ tcRef[" + k + "]: " + tcRefCfg.tc_id);
                buffer.append("\n++ ++ ++   opened: " + tcRefCfg.opened);
            }
        }
        buffer.append("\n-- groups: " + Arrays.toString(wmc.groups) + " size " + (wmc.groups == null ? -1 : wmc.groups.length));
        for (i = 0; i < wmc.groups.length; ++i) {
            GroupConfig sc = wmc.groups[i];
            buffer.append("\n-- --");
            buffer.append("\n-- -- group[" + i + "]: " + sc.name);
            if (sc.tcGroupConfigs == null) continue;
            for (k = 0; k < sc.tcGroupConfigs.length; ++k) {
                TCGroupConfig tcGroupCfg = sc.tcGroupConfigs[k];
                buffer.append("\n++ ++ ++ tcGroup[" + k + "]: " + tcGroupCfg.tc_id);
                buffer.append("\n++ ++ ++   open: " + tcGroupCfg.open);
                buffer.append("\n++ ++ ++  close: " + tcGroupCfg.close);
            }
        }
        return buffer.toString();
    }

    private static void debugLog(String message) {
        Debug.log(PersistenceHandler.class, message);
    }

    private static boolean isOutOfScreen(int x, int y) {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] gs = ge.getScreenDevices();
        for (int j = 0; j < gs.length; ++j) {
            Rectangle bounds;
            GraphicsDevice gd = gs[j];
            if (gd.getType() != 0 || !(bounds = gd.getDefaultConfiguration().getBounds()).contains(x, y)) continue;
            return false;
        }
        return true;
    }

    public static Rectangle computeBounds(boolean centeredHorizontaly, boolean centeredVerticaly, int x, int y, int width, int height, float relativeX, float relativeY, float relativeWidth, float relativeHeight) {
        Rectangle bounds;
        Rectangle screen;
        if (width > 0 && height > 0) {
            bounds = new Rectangle(x, y, width, height);
            screen = Utilities.getUsableScreenBounds();
            int xlimit = screen.x + screen.width - 20;
            int ylimit = screen.y + screen.height - 20;
            if (PersistenceHandler.isOutOfScreen(bounds.x, bounds.y)) {
                while (bounds.x > xlimit) {
                    bounds.x = Math.max(bounds.x - screen.width, screen.x);
                }
                while (bounds.y > ylimit) {
                    bounds.y = Math.max(bounds.y - ylimit, screen.y);
                }
            }
        } else if (relativeWidth > 0.0f && relativeHeight > 0.0f) {
            screen = Utilities.getUsableScreenBounds();
            bounds = new Rectangle((int)((float)screen.width * relativeX), (int)((float)screen.height * relativeY), (int)((float)screen.width * relativeWidth), (int)((float)screen.height * relativeHeight));
        } else {
            return null;
        }
        if (centeredHorizontaly || centeredVerticaly) {
            Rectangle centered = Utilities.findCenterBounds((Dimension)new Dimension(bounds.width, bounds.height));
            if (centeredHorizontaly) {
                bounds.x = centered.x;
            }
            if (centeredVerticaly) {
                bounds.y = centered.y;
            }
        }
        return bounds;
    }

    void finishLazyLoading() {
        this.lazyLoader.loadAllNow();
    }

    static {
        DEBUG = Debug.isLoggable(PersistenceHandler.class);
    }
}

