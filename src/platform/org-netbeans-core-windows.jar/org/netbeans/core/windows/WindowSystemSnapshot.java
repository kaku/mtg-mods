/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows;

import java.awt.Rectangle;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.ModeStructureSnapshot;

public class WindowSystemSnapshot {
    private Rectangle mainWindowBoundsJoined;
    private Rectangle mainWindowBoundsSeparated;
    private int mainWindowFrameStateJoined;
    private int mainWindowFrameStateSeparated;
    private String toolbarConfigurationName;
    private int editorAreaState;
    private int editorAreaFrameState;
    private Rectangle editorAreaBounds;
    private ModeStructureSnapshot.ModeSnapshot activeMode;
    private ModeStructureSnapshot.ModeSnapshot maximizedMode;
    private ModeStructureSnapshot modeStructureSnapshot;
    private String projectName;

    public void setMainWindowBoundsJoined(Rectangle mainWindowBoundsJoined) {
        this.mainWindowBoundsJoined = mainWindowBoundsJoined;
    }

    public Rectangle getMainWindowBoundsJoined() {
        return this.mainWindowBoundsJoined;
    }

    public void setMainWindowBoundsSeparated(Rectangle mainWindowBoundsSeparated) {
        this.mainWindowBoundsSeparated = mainWindowBoundsSeparated;
    }

    public Rectangle getMainWindowBoundsSeparated() {
        return this.mainWindowBoundsSeparated;
    }

    public void setMainWindowFrameStateJoined(int mainWindowFrameStateJoined) {
        this.mainWindowFrameStateJoined = mainWindowFrameStateJoined;
    }

    public int getMainWindowFrameStateJoined() {
        return this.mainWindowFrameStateJoined;
    }

    public void setMainWindowFrameStateSeparated(int mainWindowFrameStateSeparated) {
        this.mainWindowFrameStateSeparated = mainWindowFrameStateSeparated;
    }

    public int getMainWindowFrameStateSeparated() {
        return this.mainWindowFrameStateSeparated;
    }

    public void setEditorAreaBounds(Rectangle editorAreaBounds) {
        this.editorAreaBounds = editorAreaBounds;
    }

    public Rectangle getEditorAreaBounds() {
        return this.editorAreaBounds;
    }

    public void setEditorAreaState(int editorAreaState) {
        this.editorAreaState = editorAreaState;
    }

    public int getEditorAreaState() {
        return this.editorAreaState;
    }

    public void setEditorAreaFrameState(int editorAreaFrameState) {
        this.editorAreaFrameState = editorAreaFrameState;
    }

    public int getEditorAreaFrameState() {
        return this.editorAreaFrameState;
    }

    public void setActiveModeSnapshot(ModeStructureSnapshot.ModeSnapshot activeMode) {
        this.activeMode = activeMode;
    }

    public ModeStructureSnapshot.ModeSnapshot getActiveModeSnapshot() {
        return this.activeMode;
    }

    public void setMaximizedModeSnapshot(ModeStructureSnapshot.ModeSnapshot maximizedMode) {
        this.maximizedMode = maximizedMode;
    }

    public ModeStructureSnapshot.ModeSnapshot getMaximizedModeSnapshot() {
        return this.maximizedMode;
    }

    public void setToolbarConfigurationName(String toolbarConfigurationName) {
        this.toolbarConfigurationName = toolbarConfigurationName;
    }

    public String getToolbarConfigurationName() {
        return this.toolbarConfigurationName;
    }

    public void setModeStructureSnapshot(ModeStructureSnapshot modeStructureSnapshot) {
        this.modeStructureSnapshot = modeStructureSnapshot;
    }

    public ModeStructureSnapshot getModeStructureSnapshot() {
        return this.modeStructureSnapshot;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectName() {
        return this.projectName;
    }

    public ModeStructureSnapshot.ModeSnapshot findModeSnapshot(ModeImpl mode) {
        if (mode == null) {
            return null;
        }
        if (this.modeStructureSnapshot != null) {
            return this.modeStructureSnapshot.findModeSnapshot(mode.getName());
        }
        return null;
    }

    public String toString() {
        return super.toString() + "[modeStructure=" + this.modeStructureSnapshot + ",\nactiveMode=" + this.activeMode + ",\nmaximizedMode=" + this.maximizedMode + "]";
    }
}

