/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.NbKeymap
 *  org.netbeans.core.NbLifecycleManager
 *  org.openide.actions.ActionManager
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 */
package org.netbeans.core.windows;

import java.awt.AWTKeyStroke;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.KeyEventDispatcher;
import java.awt.KeyEventPostProcessor;
import java.awt.KeyboardFocusManager;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Set;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import javax.swing.MenuElement;
import javax.swing.MenuSelectionManager;
import javax.swing.SwingUtilities;
import javax.swing.text.Keymap;
import org.netbeans.core.NbKeymap;
import org.netbeans.core.NbLifecycleManager;
import org.netbeans.core.windows.WindowManagerImpl;
import org.netbeans.core.windows.view.ui.popupswitcher.KeyboardPopupSwitcher;
import org.openide.actions.ActionManager;
import org.openide.util.Lookup;
import org.openide.util.Utilities;

final class ShortcutAndMenuKeyEventProcessor
implements KeyEventDispatcher,
KeyEventPostProcessor {
    private static ShortcutAndMenuKeyEventProcessor defaultInstance;
    private static boolean installed;
    private static Set<AWTKeyStroke> defaultForward;
    private static Set<AWTKeyStroke> defaultBackward;
    private static final Logger log;
    private boolean wasPopupDisplayed;
    private int lastModifiers;
    private char lastKeyChar;
    private boolean lastSampled = false;
    private boolean skipNextTyped = false;

    private ShortcutAndMenuKeyEventProcessor() {
    }

    private static synchronized ShortcutAndMenuKeyEventProcessor getDefault() {
        if (defaultInstance == null) {
            defaultInstance = new ShortcutAndMenuKeyEventProcessor();
        }
        return defaultInstance;
    }

    public static synchronized void install() {
        if (installed) {
            return;
        }
        ShortcutAndMenuKeyEventProcessor instance = ShortcutAndMenuKeyEventProcessor.getDefault();
        KeyboardFocusManager keyboardFocusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        keyboardFocusManager.addKeyEventDispatcher(instance);
        keyboardFocusManager.addKeyEventPostProcessor(instance);
        defaultForward = keyboardFocusManager.getDefaultFocusTraversalKeys(0);
        defaultBackward = keyboardFocusManager.getDefaultFocusTraversalKeys(1);
        keyboardFocusManager.setDefaultFocusTraversalKeys(0, Collections.singleton(AWTKeyStroke.getAWTKeyStroke(9, 0)));
        keyboardFocusManager.setDefaultFocusTraversalKeys(1, Collections.singleton(AWTKeyStroke.getAWTKeyStroke(9, 64)));
    }

    public static synchronized void uninstall() {
        if (!installed) {
            return;
        }
        ShortcutAndMenuKeyEventProcessor instance = ShortcutAndMenuKeyEventProcessor.getDefault();
        KeyboardFocusManager keyboardFocusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        keyboardFocusManager.removeKeyEventDispatcher(instance);
        keyboardFocusManager.removeKeyEventPostProcessor(instance);
        keyboardFocusManager.setDefaultFocusTraversalKeys(0, defaultForward);
        keyboardFocusManager.setDefaultFocusTraversalKeys(1, defaultBackward);
        defaultBackward = null;
        defaultForward = null;
    }

    @Override
    public boolean postProcessKeyEvent(KeyEvent ev) {
        if (ev.isConsumed()) {
            return false;
        }
        if (this.processShortcut(ev)) {
            return true;
        }
        Window w = SwingUtilities.windowForComponent(ev.getComponent());
        if (w instanceof Dialog && !WindowManagerImpl.isSeparateWindow(w)) {
            return false;
        }
        JFrame mw = (JFrame)WindowManagerImpl.getInstance().getMainWindow();
        if (w == mw) {
            return false;
        }
        JMenuBar mb = mw.getJMenuBar();
        if (mb == null) {
            return false;
        }
        boolean pressed = ev.getID() == 401;
        boolean res = ShortcutAndMenuKeyEventProcessor.invokeProcessKeyBindingsForAllComponents(ev, mw, pressed);
        if (res) {
            ev.consume();
        }
        return res;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent ev) {
        MenuElement[] arr;
        log.fine("dispatchKeyEvent ev: " + ev.paramString() + " source:" + ev.getSource().getClass().getName());
        if (NbKeymap.getContext().length != 0) {
            if (ev.getID() != 401) {
                ev.consume();
                return true;
            }
            this.skipNextTyped = true;
            Component comp = ev.getComponent();
            if (!(comp instanceof JComponent) || ((JComponent)comp).getClientProperty("context-api-aware") == null) {
                this.processShortcut(ev);
                return true;
            }
        }
        if (ev.getID() == 401 && ev.getModifiers() == 3 && (ev.getKeyCode() == 19 || ev.getKeyCode() == 3)) {
            Object source = ev.getSource();
            if (source instanceof Component) {
                Component focused = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
                System.err.println("*** ShortcutAndMenuKeyEventProcessor: current focus owner = " + focused);
            }
            ev.consume();
            return true;
        }
        if (ev.getID() == 400 && this.skipNextTyped) {
            ev.consume();
            this.skipNextTyped = false;
            return true;
        }
        if (ev.getID() == 402) {
            this.skipNextTyped = false;
        }
        if (ev.getID() == 401) {
            this.lastKeyChar = ev.getKeyChar();
            this.lastModifiers = ev.getModifiers();
            this.lastSampled = true;
        }
        if ((arr = MenuSelectionManager.defaultManager().getSelectedPath()) == null || arr.length == 0) {
            this.wasPopupDisplayed = false;
            return KeyboardPopupSwitcher.processShortcut(ev);
        }
        if (!this.wasPopupDisplayed && this.lastSampled && ev.getID() == 400 && this.lastModifiers == 8 && ev.getModifiers() == 8 && this.lastKeyChar == ev.getKeyChar()) {
            this.wasPopupDisplayed = true;
            ev.consume();
            return true;
        }
        this.wasPopupDisplayed = true;
        MenuSelectionManager.defaultManager().processKeyEvent(ev);
        return ev.isConsumed();
    }

    private boolean processShortcut(KeyEvent ev) {
        if (NbLifecycleManager.isExiting()) {
            ev.consume();
            return true;
        }
        KeyStroke ks = KeyStroke.getKeyStrokeForEvent(ev);
        Window w = SwingUtilities.windowForComponent(ev.getComponent());
        if (w instanceof JFrame && ((JFrame)w).getRootPane().getClientProperty("netbeans.helpframe") != null) {
            return true;
        }
        if (w instanceof Dialog && !WindowManagerImpl.isSeparateWindow(w) && !ShortcutAndMenuKeyEventProcessor.isTransmodalAction(ks)) {
            return false;
        }
        ActionEvent aev = new ActionEvent(ev.getSource(), 1001, Utilities.keyToString((KeyStroke)ks));
        Keymap root = (Keymap)Lookup.getDefault().lookup(Keymap.class);
        Action a = root.getAction(ks);
        if (a != null && a.isEnabled()) {
            ActionManager am = (ActionManager)Lookup.getDefault().lookup(ActionManager.class);
            am.invokeAction(a, aev);
            ev.consume();
            return true;
        }
        return false;
    }

    private static boolean invokeProcessKeyBindingsForAllComponents(KeyEvent e, Container container, boolean pressed) {
        try {
            Method m = JComponent.class.getDeclaredMethod("processKeyBindingsForAllComponents", KeyEvent.class, Container.class, Boolean.TYPE);
            if (m == null) {
                return false;
            }
            m.setAccessible(true);
            Object[] arrobject = new Object[3];
            arrobject[0] = e;
            arrobject[1] = container;
            arrobject[2] = pressed ? Boolean.TRUE : Boolean.FALSE;
            Boolean b = (Boolean)m.invoke(null, arrobject);
            return b;
        }
        catch (Exception ex) {
            return false;
        }
    }

    private static boolean isTransmodalAction(KeyStroke key) {
        Keymap root = (Keymap)Lookup.getDefault().lookup(Keymap.class);
        Action a = root.getAction(key);
        if (a == null) {
            return false;
        }
        Object val = a.getValue("OpenIDE-Transmodal-Action");
        return val != null && val.equals(Boolean.TRUE);
    }

    static {
        installed = false;
        log = Logger.getLogger("org.netbeans.core.windows.ShortcutAndMenuKeyEventProcessor");
    }
}

