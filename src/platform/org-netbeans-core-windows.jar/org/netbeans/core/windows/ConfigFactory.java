/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.ArrayList;
import org.netbeans.core.windows.SplitConstraint;
import org.netbeans.core.windows.persistence.GroupConfig;
import org.netbeans.core.windows.persistence.ModeConfig;
import org.netbeans.core.windows.persistence.TCRefConfig;
import org.netbeans.core.windows.persistence.WindowManagerConfig;

abstract class ConfigFactory {
    private static final int VERTICAL = 0;
    private static final int HORIZONTAL = 1;

    private ConfigFactory() {
    }

    public static WindowManagerConfig createDefaultConfig() {
        WindowManagerConfig wmc = new WindowManagerConfig();
        wmc.xJoined = 100;
        wmc.yJoined = 100;
        wmc.widthJoined = 800;
        wmc.heightJoined = 600;
        wmc.relativeXJoined = -1.0f;
        wmc.relativeYJoined = -1.0f;
        wmc.relativeWidthJoined = -1.0f;
        wmc.relativeHeightJoined = -1.0f;
        wmc.centeredHorizontallyJoined = false;
        wmc.centeredVerticallyJoined = false;
        wmc.maximizeIfWidthBelowJoined = -1;
        wmc.maximizeIfHeightBelowJoined = -1;
        wmc.editorAreaState = 0;
        wmc.editorAreaConstraints = ConfigFactory.createDefaultEditorAreaConstraints();
        wmc.editorAreaBounds = null;
        wmc.editorAreaRelativeBounds = null;
        wmc.screenSize = new Dimension(1024, 750);
        wmc.activeModeName = "editor";
        wmc.editorMaximizedModeName = "";
        wmc.viewMaximizedModeName = "";
        wmc.toolbarConfiguration = "Standard";
        wmc.preferredToolbarIconSize = 24;
        wmc.modes = ConfigFactory.createDefaultModeConfigs();
        wmc.groups = ConfigFactory.createDefaultGroupConfigs();
        return wmc;
    }

    private static SplitConstraint[] createDefaultEditorAreaConstraints() {
        return new SplitConstraint[]{new SplitConstraint(1, 1, 0.75), new SplitConstraint(0, 0, 0.75), new SplitConstraint(1, 0, 0.75)};
    }

    private static ModeConfig[] createDefaultModeConfigs() {
        ArrayList<ModeConfig> l = new ArrayList<ModeConfig>();
        l.add(ConfigFactory.createDefaultExplorerModeConfig());
        l.add(ConfigFactory.createDefaultPropertiesModeConfig());
        l.add(ConfigFactory.createDefaultEditorModeConfig());
        l.add(ConfigFactory.createDefaultOutputModeConfig());
        l.add(ConfigFactory.createDefaultFormModeConfig());
        return l.toArray(new ModeConfig[0]);
    }

    private static ModeConfig createDefaultExplorerModeConfig() {
        ModeConfig mc = new ModeConfig();
        mc.name = "explorer";
        mc.bounds = null;
        mc.relativeBounds = null;
        mc.frameState = -1;
        mc.state = 0;
        mc.kind = 0;
        mc.constraints = ConfigFactory.createDefaultExplorerConstraints();
        mc.selectedTopComponentID = "runtime";
        mc.permanent = true;
        mc.tcRefConfigs = ConfigFactory.createDefaultExplorerTCRefConfigs();
        return mc;
    }

    private static SplitConstraint[] createDefaultExplorerConstraints() {
        return new SplitConstraint[]{new SplitConstraint(1, 0, 0.3), new SplitConstraint(0, 0, 0.7)};
    }

    private static TCRefConfig[] createDefaultExplorerTCRefConfigs() {
        ArrayList<TCRefConfig> tcRefConfigs = new ArrayList<TCRefConfig>();
        tcRefConfigs.add(ConfigFactory.createDefaultRuntimeTCRefConfig());
        return tcRefConfigs.toArray(new TCRefConfig[0]);
    }

    private static TCRefConfig createDefaultRuntimeTCRefConfig() {
        TCRefConfig tcrc = new TCRefConfig();
        tcrc.tc_id = "runtime";
        tcrc.opened = true;
        return tcrc;
    }

    private static ModeConfig createDefaultPropertiesModeConfig() {
        ModeConfig mc = new ModeConfig();
        mc.name = "properties";
        mc.bounds = null;
        mc.relativeBounds = null;
        mc.frameState = -1;
        mc.state = 0;
        mc.kind = 0;
        mc.constraints = ConfigFactory.createDefaultPropertiesConstraints();
        mc.selectedTopComponentID = "properties";
        mc.permanent = true;
        mc.tcRefConfigs = ConfigFactory.createDefaultPropertiesTCRefConfigs();
        return mc;
    }

    private static SplitConstraint[] createDefaultPropertiesConstraints() {
        return new SplitConstraint[]{new SplitConstraint(1, 0, 0.3), new SplitConstraint(0, 1, 0.3)};
    }

    private static TCRefConfig[] createDefaultPropertiesTCRefConfigs() {
        ArrayList<TCRefConfig> tcRefConfigs = new ArrayList<TCRefConfig>();
        tcRefConfigs.add(ConfigFactory.createDefaultPropertiesTCRefConfig());
        return tcRefConfigs.toArray(new TCRefConfig[0]);
    }

    private static TCRefConfig createDefaultPropertiesTCRefConfig() {
        TCRefConfig tcrc = new TCRefConfig();
        tcrc.tc_id = "properties";
        tcrc.opened = true;
        return tcrc;
    }

    private static ModeConfig createDefaultEditorModeConfig() {
        ModeConfig mc = new ModeConfig();
        mc.name = "editor";
        mc.bounds = null;
        mc.relativeBounds = null;
        mc.frameState = -1;
        mc.state = 0;
        mc.kind = 1;
        mc.constraints = ConfigFactory.createDefaultEditorConstraints();
        mc.selectedTopComponentID = null;
        mc.permanent = true;
        mc.tcRefConfigs = ConfigFactory.createDefaultEditorTCRefConfigs();
        return mc;
    }

    private static SplitConstraint[] createDefaultEditorConstraints() {
        return new SplitConstraint[0];
    }

    private static TCRefConfig[] createDefaultEditorTCRefConfigs() {
        ArrayList<TCRefConfig> tcRefConfigs = new ArrayList<TCRefConfig>();
        tcRefConfigs.add(ConfigFactory.createDefaultWelcomeTCRefConfig());
        return tcRefConfigs.toArray(new TCRefConfig[0]);
    }

    private static TCRefConfig createDefaultWelcomeTCRefConfig() {
        TCRefConfig tcrc = new TCRefConfig();
        tcrc.tc_id = "Welcome";
        tcrc.opened = true;
        return tcrc;
    }

    private static ModeConfig createDefaultOutputModeConfig() {
        ModeConfig mc = new ModeConfig();
        mc.name = "output";
        mc.bounds = null;
        mc.relativeBounds = null;
        mc.frameState = -1;
        mc.state = 0;
        mc.kind = 0;
        mc.constraints = ConfigFactory.createDefaultOutputConstraints();
        mc.selectedTopComponentID = null;
        mc.permanent = true;
        mc.tcRefConfigs = ConfigFactory.createDefaultOutputTCRefConfigs();
        return mc;
    }

    private static SplitConstraint[] createDefaultOutputConstraints() {
        return new SplitConstraint[]{new SplitConstraint(1, 1, 0.7), new SplitConstraint(0, 1, 0.2), new SplitConstraint(1, 0, 0.8)};
    }

    private static TCRefConfig[] createDefaultOutputTCRefConfigs() {
        ArrayList<TCRefConfig> tcRefConfigs = new ArrayList<TCRefConfig>();
        tcRefConfigs.add(ConfigFactory.createDefaultOutputTCRefConfig());
        return tcRefConfigs.toArray(new TCRefConfig[0]);
    }

    private static TCRefConfig createDefaultOutputTCRefConfig() {
        TCRefConfig tcrc = new TCRefConfig();
        tcrc.tc_id = "output";
        tcrc.opened = true;
        return tcrc;
    }

    private static ModeConfig createDefaultFormModeConfig() {
        ModeConfig mc = new ModeConfig();
        mc.name = "Form";
        mc.bounds = null;
        mc.relativeBounds = null;
        mc.frameState = -1;
        mc.state = 0;
        mc.kind = 0;
        mc.constraints = ConfigFactory.createDefaultFormConstraints();
        mc.selectedTopComponentID = "ComponentInspector";
        mc.permanent = true;
        mc.tcRefConfigs = ConfigFactory.createDefaultFormTCRefConfigs();
        return mc;
    }

    private static SplitConstraint[] createDefaultFormConstraints() {
        return new SplitConstraint[]{new SplitConstraint(1, 1, 0.7), new SplitConstraint(0, 0, 0.8), new SplitConstraint(1, 1, 0.5)};
    }

    private static TCRefConfig[] createDefaultFormTCRefConfigs() {
        ArrayList<TCRefConfig> tcRefConfigs = new ArrayList<TCRefConfig>();
        tcRefConfigs.add(ConfigFactory.createDefaultComponentInspectorTCRefConfig());
        tcRefConfigs.add(ConfigFactory.createDefaultComponentPaletteTCRefConfig());
        return tcRefConfigs.toArray(new TCRefConfig[0]);
    }

    private static TCRefConfig createDefaultComponentInspectorTCRefConfig() {
        TCRefConfig tcrc = new TCRefConfig();
        tcrc.tc_id = "ComponentInspector";
        tcrc.opened = true;
        return tcrc;
    }

    private static TCRefConfig createDefaultComponentPaletteTCRefConfig() {
        TCRefConfig tcrc = new TCRefConfig();
        tcrc.tc_id = "CommonPalette";
        tcrc.opened = true;
        tcrc.dockedInMaximizedMode = true;
        return tcrc;
    }

    private static GroupConfig[] createDefaultGroupConfigs() {
        ArrayList l = new ArrayList();
        return l.toArray(new GroupConfig[0]);
    }
}

