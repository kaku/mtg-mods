/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.windows;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class Debug {
    private Debug() {
    }

    public static boolean isLoggable(Class clazz) {
        return Logger.getLogger(clazz.getName()).isLoggable(Level.FINE);
    }

    public static void log(Class clazz, String message) {
        Logger.getLogger(clazz.getName()).fine(message);
    }

    public static void dumpStack(Class clazz) {
        if (Logger.getLogger(clazz.getName()).isLoggable(Level.FINE)) {
            StringWriter sw = new StringWriter();
            new Throwable().printStackTrace(new PrintWriter(sw));
            Debug.log(clazz, sw.getBuffer().toString());
        }
    }
}

