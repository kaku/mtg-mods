/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponentGroup
 */
package org.netbeans.core.windows;

import java.util.Set;
import org.netbeans.core.windows.Central;
import org.netbeans.core.windows.WindowManagerImpl;
import org.openide.windows.TopComponent;
import org.openide.windows.TopComponentGroup;

public class TopComponentGroupImpl
implements TopComponentGroup {
    public TopComponentGroupImpl(String name) {
        this(name, false);
    }

    public TopComponentGroupImpl(String name, boolean opened) {
        this.getCentral().createGroupModel(this, name, opened);
    }

    public void open() {
        WindowManagerImpl.assertEventDispatchThread();
        this.getCentral().openGroup(this);
    }

    public void close() {
        WindowManagerImpl.assertEventDispatchThread();
        this.getCentral().closeGroup(this);
    }

    public Set<TopComponent> getTopComponents() {
        return this.getCentral().getGroupTopComponents(this);
    }

    public String getName() {
        return this.getCentral().getGroupName(this);
    }

    public boolean isOpened() {
        return this.getCentral().isGroupOpened(this);
    }

    public Set<TopComponent> getOpeningSet() {
        return this.getCentral().getGroupOpeningTopComponents(this);
    }

    public Set getClosingSet() {
        return this.getCentral().getGroupClosingTopComponents(this);
    }

    public boolean addUnloadedTopComponent(String tcID) {
        return this.getCentral().addGroupUnloadedTopComponent(this, tcID);
    }

    public boolean removeUnloadedTopComponent(String tcID) {
        return this.getCentral().removeGroupUnloadedTopComponent(this, tcID);
    }

    public boolean addUnloadedOpeningTopComponent(String tcID) {
        return this.getCentral().addGroupUnloadedOpeningTopComponent(this, tcID);
    }

    public boolean removeUnloadedOpeningTopComponent(String tcID) {
        return this.getCentral().removeGroupUnloadedOpeningTopComponent(this, tcID);
    }

    public boolean addUnloadedClosingTopComponent(String tcID) {
        return this.getCentral().addGroupUnloadedClosingTopComponent(this, tcID);
    }

    public boolean removeUnloadedClosingTopComponent(String tcID) {
        return this.getCentral().removeGroupUnloadedClosingTopComponent(this, tcID);
    }

    public boolean addGroupUnloadedOpenedTopComponent(String tcID) {
        return this.getCentral().addGroupUnloadedOpenedTopComponent(this, tcID);
    }

    public Set getGroupOpenedTopComponents() {
        return this.getCentral().getGroupOpenedTopComponents(this);
    }

    public Set<String> getTopComponentsIDs() {
        return this.getCentral().getGroupTopComponentsIDs(this);
    }

    public Set<String> getOpeningSetIDs() {
        return this.getCentral().getGroupOpeningSetIDs(this);
    }

    public Set<String> getClosingSetIDs() {
        return this.getCentral().getGroupClosingSetIDs(this);
    }

    public Set<String> getGroupOpenedTopComponentsIDs() {
        return this.getCentral().getGroupOpenedTopComponentsIDs(this);
    }

    private Central getCentral() {
        return WindowManagerImpl.getInstance().getCentral();
    }

    public String toString() {
        StringBuffer buff = new StringBuffer();
        for (TopComponent tc : this.getTopComponents()) {
            buff.append("\n\t" + tc.getClass().getName() + "@" + Integer.toHexString(tc.hashCode()) + "[name=" + tc.getName() + ", openFlag=" + this.getOpeningSet().contains((Object)tc) + ", closeFlag=" + this.getClosingSet().contains((Object)tc) + "]");
        }
        return super.toString() + "[topComponents=[" + buff.toString() + "\n]]";
    }
}

