/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.windows;

import java.awt.Component;
import java.awt.Frame;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.Central;
import org.netbeans.core.windows.Debug;
import org.netbeans.core.windows.ModeImpl;
import org.netbeans.core.windows.ModeStructureSnapshot;
import org.netbeans.core.windows.ViewRequest;
import org.netbeans.core.windows.WindowSystemSnapshot;
import org.netbeans.core.windows.view.View;
import org.netbeans.core.windows.view.ViewEvent;
import org.netbeans.core.windows.view.ViewFactory;
import org.netbeans.core.windows.view.dnd.TopComponentDraggable;
import org.openide.windows.TopComponent;

class ViewRequestor {
    private final Central central;
    private final View view;
    private final List<ViewRequest> requests = new ArrayList<ViewRequest>(10);
    private WindowSystemSnapshot snapshot;
    private boolean reentryFlag = false;
    private static final boolean DEBUG = Debug.isLoggable(ViewRequestor.class);

    public ViewRequestor(Central central) {
        this.central = central;
        this.view = ViewFactory.createWindowSystemView(central);
    }

    public boolean isDragInProgress() {
        return this.view.isDragInProgress();
    }

    public Frame getMainWindow() {
        return this.view.getMainWindow();
    }

    public Component getEditorAreaComponent() {
        return this.view.getEditorAreaComponent();
    }

    public String guessSlideSide(TopComponent tc) {
        return this.view.guessSlideSide(tc);
    }

    public void scheduleRequest(ViewRequest request) {
        if (request.type == 0) {
            this.postVisibilityRequest(request);
            return;
        }
        this.coallesceRequest(request);
        this.postRequest();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void coallesceRequest(ViewRequest request) {
        Object source = request.source;
        int type = request.type;
        boolean doCoallesce = type == 9 || type == 7 || type == 8 || type == 5 || type == 6 || type == 1 || type == 2 || type == 3 || type == 4 || type == 11 || type == 22 || type == 20 || type == 14 || type == 21 || type == 10 || type == 34 || type == 31 || type == 32 || type == 33 || type == 44 || type == 46 || type == 61 || type == 64 || type == 63 || type == 66 || type == 65;
        List<ViewRequest> list = this.requests;
        synchronized (list) {
            Object oldValue = null;
            if (doCoallesce) {
                Iterator<ViewRequest> it = this.requests.iterator();
                while (it.hasNext()) {
                    ViewRequest r = it.next();
                    if (source != r.source || type != r.type) continue;
                    it.remove();
                    oldValue = r.oldValue;
                    break;
                }
            }
            if (oldValue != null) {
                this.requests.add(new ViewRequest(request.source, request.type, oldValue, request.newValue));
            } else {
                this.requests.add(request);
            }
        }
    }

    private void postRequest() {
        if (SwingUtilities.isEventDispatchThread()) {
            this.processRequest();
        } else {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    ViewRequestor.this.processRequest();
                }
            });
        }
    }

    private void postVisibilityRequest(final ViewRequest visibilityRequest) {
        if (SwingUtilities.isEventDispatchThread()) {
            this.processVisibilityRequest(visibilityRequest);
        } else {
            final long time = System.currentTimeMillis();
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    if (DEBUG) {
                        ViewRequestor.debugLog("Rescheduling request into AWT took=" + (System.currentTimeMillis() - time) + " ms");
                    }
                    ViewRequestor.this.processVisibilityRequest(visibilityRequest);
                }
            });
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void processRequest() {
        ViewRequest[] rs;
        if (this.reentryFlag) {
            return;
        }
        if (this.snapshot == null) {
            return;
        }
        List<ViewRequest> list = this.requests;
        synchronized (list) {
            if (this.requests.isEmpty()) {
                return;
            }
            rs = this.requests.toArray(new ViewRequest[0]);
            this.requests.clear();
        }
        ArrayList<ViewEvent> viewEvents = new ArrayList<ViewEvent>();
        this.updateSnapshot(rs);
        for (int i = 0; i < rs.length; ++i) {
            ViewRequest r = rs[i];
            if (DEBUG) {
                ViewRequestor.debugLog("Creating a view event for " + r);
            }
            viewEvents.add(this.getViewEvent(r));
        }
        this.dispatchRequest(viewEvents.toArray(new ViewEvent[0]), this.snapshot);
    }

    private void processVisibilityRequest(ViewRequest visibilityRequest) {
        this.snapshot = (Boolean)visibilityRequest.newValue != false ? this.central.createWindowSystemSnapshot() : null;
        this.dispatchRequest(new ViewEvent[]{new ViewEvent(visibilityRequest.source, visibilityRequest.type, visibilityRequest.oldValue, visibilityRequest.newValue)}, this.snapshot);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void dispatchRequest(ViewEvent[] viewEvents, WindowSystemSnapshot snapshot) {
        try {
            this.reentryFlag = true;
            this.view.changeGUI(viewEvents, snapshot);
        }
        finally {
            this.reentryFlag = false;
            this.processRequest();
        }
    }

    private void updateSnapshot(ViewRequest[] requests) {
        long time = System.currentTimeMillis();
        WindowSystemSnapshot currentSnapshot = this.central.createWindowSystemSnapshot();
        this.snapshot.setMainWindowBoundsJoined(currentSnapshot.getMainWindowBoundsJoined());
        this.snapshot.setMainWindowBoundsSeparated(currentSnapshot.getMainWindowBoundsSeparated());
        this.snapshot.setMainWindowFrameStateJoined(currentSnapshot.getMainWindowFrameStateJoined());
        this.snapshot.setMainWindowFrameStateSeparated(currentSnapshot.getMainWindowFrameStateSeparated());
        this.snapshot.setEditorAreaState(currentSnapshot.getEditorAreaState());
        this.snapshot.setEditorAreaFrameState(currentSnapshot.getEditorAreaFrameState());
        this.snapshot.setEditorAreaBounds(currentSnapshot.getEditorAreaBounds());
        this.snapshot.setActiveModeSnapshot(currentSnapshot.getActiveModeSnapshot());
        this.snapshot.setMaximizedModeSnapshot(currentSnapshot.getMaximizedModeSnapshot());
        this.snapshot.setModeStructureSnapshot(currentSnapshot.getModeStructureSnapshot());
        this.snapshot.setToolbarConfigurationName(currentSnapshot.getToolbarConfigurationName());
        this.snapshot.setProjectName(currentSnapshot.getProjectName());
        if (DEBUG) {
            ViewRequestor.debugLog("Updating winsys snapshot took=" + (System.currentTimeMillis() - time) + " ms");
            ViewRequestor.debugLog(this.snapshot.toString());
        }
    }

    private ViewEvent getViewEvent(ViewRequest request) {
        Object source = request.source;
        int type = request.type;
        Object oldValue = request.oldValue;
        Object newValue = request.newValue;
        if (type == 22) {
            return new ViewEvent(((ModeImpl)source).getName(), type, oldValue, newValue);
        }
        if (type == 23) {
            return new ViewEvent(((ModeImpl)source).getName(), type, oldValue, newValue);
        }
        if (type == 24) {
            return new ViewEvent(((ModeImpl)source).getName(), type, oldValue, newValue);
        }
        if (type == 20) {
            return new ViewEvent(((ModeImpl)source).getName(), type, oldValue, newValue);
        }
        if (type == 21) {
            return new ViewEvent(((ModeImpl)source).getName(), type, oldValue, newValue);
        }
        if (type == 34) {
            return new ViewEvent(((ModeImpl)source).getName(), type, oldValue, newValue);
        }
        if (type == 31) {
            return new ViewEvent(((ModeImpl)source).getName(), type, oldValue, newValue);
        }
        if (type == 32) {
            return new ViewEvent(((ModeImpl)source).getName(), type, oldValue, newValue);
        }
        if (type == 33) {
            return new ViewEvent(((ModeImpl)source).getName(), type, oldValue, newValue);
        }
        if (type == 44) {
            return new ViewEvent(((ModeImpl)source).getName(), type, oldValue, newValue);
        }
        if (type == 45) {
            return new ViewEvent(((ModeImpl)source).getName(), type, oldValue, newValue);
        }
        return new ViewEvent(source, type, oldValue, newValue);
    }

    private static void debugLog(String message) {
        Debug.log(ViewRequestor.class, message);
    }

    void userStartedKeyboardDragAndDrop(TopComponentDraggable draggable) {
        this.view.userStartedKeyboardDragAndDrop(draggable);
    }

}

