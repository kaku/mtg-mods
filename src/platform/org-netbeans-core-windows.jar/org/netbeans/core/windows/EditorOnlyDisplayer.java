/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 */
package org.netbeans.core.windows;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.LayoutManager;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import org.netbeans.core.windows.TopComponentTracker;
import org.netbeans.core.windows.WindowManagerImpl;
import org.openide.filesystems.FileUtil;
import org.openide.util.actions.Presenter;
import org.openide.windows.TopComponent;

public class EditorOnlyDisplayer {
    private static EditorOnlyDisplayer theInstance;
    private final PropertyChangeListener registryListener;
    private Container originalContentPane = null;
    private boolean originalShowEditorToolbar = true;

    private EditorOnlyDisplayer() {
        this.registryListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                EditorOnlyDisplayer.this.onRegistryChange(evt);
            }
        };
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static EditorOnlyDisplayer getInstance() {
        Class<EditorOnlyDisplayer> class_ = EditorOnlyDisplayer.class;
        synchronized (EditorOnlyDisplayer.class) {
            if (null == theInstance) {
                theInstance = new EditorOnlyDisplayer();
            }
            // ** MonitorExit[var0] (shouldn't be in output)
            return theInstance;
        }
    }

    public boolean isActive() {
        return null != this.originalContentPane;
    }

    public void setActive(boolean activate) {
        if (activate == this.isActive()) {
            return;
        }
        if (this.isActive()) {
            this.cancel(true);
        } else {
            this.activate();
        }
    }

    private void onRegistryChange(PropertyChangeEvent evt) {
        if ("activated".equals(evt.getPropertyName())) {
            Window activeWindow;
            TopComponent tc = TopComponent.getRegistry().getActivated();
            if (null != tc && null != (activeWindow = SwingUtilities.getWindowAncestor((Component)tc)) && !activeWindow.equals(WindowManagerImpl.getInstance().getMainWindow())) {
                return;
            }
            if (this.switchCurrentEditor()) {
                return;
            }
            this.cancel(true);
        }
    }

    private boolean switchCurrentEditor() {
        final TopComponent tc = TopComponent.getRegistry().getActivated();
        if (null == tc || !TopComponentTracker.getDefault().isEditorTopComponent(tc)) {
            return false;
        }
        WindowManagerImpl wmi = WindowManagerImpl.getInstance();
        JFrame mainWnd = (JFrame)wmi.getMainWindow();
        if (SwingUtilities.isDescendingFrom((Component)tc, mainWnd.getContentPane())) {
            return true;
        }
        JPanel panel = new JPanel(new BorderLayout());
        panel.add((Component)tc, "Center");
        try {
            mainWnd.setContentPane(panel);
        }
        catch (IndexOutOfBoundsException e) {
            Logger.getLogger(EditorOnlyDisplayer.class.getName()).log(Level.INFO, "Error while switching current editor.", e);
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    EditorOnlyDisplayer.this.cancel(false);
                }
            });
        }
        mainWnd.invalidate();
        mainWnd.revalidate();
        mainWnd.repaint();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                tc.requestFocusInWindow();
            }
        });
        return true;
    }

    public void cancel(boolean restoreFocus) {
        if (!this.isActive()) {
            return;
        }
        TopComponent.getRegistry().removePropertyChangeListener(this.registryListener);
        JFrame frame = (JFrame)WindowManagerImpl.getInstance().getMainWindow();
        frame.setContentPane(this.originalContentPane);
        this.originalContentPane = null;
        EditorOnlyDisplayer.setShowEditorToolbar(this.originalShowEditorToolbar);
        if (restoreFocus) {
            this.restoreFocus();
        }
    }

    private void restoreFocus() {
        final TopComponent tc = TopComponent.getRegistry().getActivated();
        if (null != tc) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    tc.requestFocusInWindow();
                }
            });
        }
    }

    private void activate() {
        assert (null == this.originalContentPane);
        final TopComponent tc = TopComponent.getRegistry().getActivated();
        if (null == tc || !TopComponentTracker.getDefault().isEditorTopComponent(tc)) {
            return;
        }
        WindowManagerImpl wmi = WindowManagerImpl.getInstance();
        JFrame mainWnd = (JFrame)wmi.getMainWindow();
        this.originalContentPane = mainWnd.getContentPane();
        JPanel panel = new JPanel(new BorderLayout());
        panel.add((Component)tc, "Center");
        mainWnd.setContentPane(panel);
        mainWnd.invalidate();
        mainWnd.revalidate();
        mainWnd.repaint();
        wmi.getRegistry().addPropertyChangeListener(this.registryListener);
        this.originalShowEditorToolbar = EditorOnlyDisplayer.setShowEditorToolbar(false);
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                tc.requestFocusInWindow();
            }
        });
    }

    private static boolean setShowEditorToolbar(boolean show) {
        JMenuItem menuItem;
        boolean res = true;
        Action toggleEditorToolbar = (Action)FileUtil.getConfigObject((String)"Editors/Actions/toggle-toolbar.instance", Action.class);
        if (null != toggleEditorToolbar && toggleEditorToolbar instanceof Presenter.Menu && (menuItem = ((Presenter.Menu)toggleEditorToolbar).getMenuPresenter()) instanceof JCheckBoxMenuItem) {
            JCheckBoxMenuItem checkBoxMenu = (JCheckBoxMenuItem)menuItem;
            res = checkBoxMenu.isSelected();
            if (checkBoxMenu.isSelected() != show) {
                try {
                    toggleEditorToolbar.actionPerformed(new ActionEvent(menuItem, 0, ""));
                }
                catch (Exception ex) {
                    Logger.getLogger(EditorOnlyDisplayer.class.getName()).log(Level.FINE, null, ex);
                }
            }
        }
        return res;
    }

}

