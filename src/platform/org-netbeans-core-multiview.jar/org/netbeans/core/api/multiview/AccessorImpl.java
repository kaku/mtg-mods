/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.api.multiview;

import org.netbeans.core.api.multiview.MultiViewHandler;
import org.netbeans.core.api.multiview.MultiViewPerspective;
import org.netbeans.core.multiview.Accessor;
import org.netbeans.core.multiview.MultiViewHandlerDelegate;
import org.netbeans.core.spi.multiview.MultiViewDescription;

class AccessorImpl
extends Accessor {
    private AccessorImpl() {
    }

    static void createAccesor() {
        if (DEFAULT == null) {
            DEFAULT = new AccessorImpl();
        }
    }

    @Override
    public MultiViewPerspective createPerspective(MultiViewDescription desc) {
        return new MultiViewPerspective(desc);
    }

    @Override
    public MultiViewHandler createHandler(MultiViewHandlerDelegate delegate) {
        return new MultiViewHandler(delegate);
    }

    @Override
    public MultiViewDescription extractDescription(MultiViewPerspective perspective) {
        return perspective.getDescription();
    }
}

