/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.api.multiview;

import org.netbeans.core.api.multiview.AccessorImpl;
import org.netbeans.core.api.multiview.MultiViewPerspective;
import org.netbeans.core.multiview.MultiViewHandlerDelegate;
import org.netbeans.core.spi.multiview.MultiViewDescription;

public final class MultiViewHandler {
    private MultiViewHandlerDelegate del;

    MultiViewHandler(MultiViewHandlerDelegate delegate) {
        this.del = delegate;
    }

    public MultiViewPerspective[] getPerspectives() {
        return this.del.getDescriptions();
    }

    public MultiViewPerspective getSelectedPerspective() {
        return this.del.getSelectedDescription();
    }

    public void requestActive(MultiViewPerspective desc) {
        this.del.requestActive(desc);
    }

    public void requestVisible(MultiViewPerspective desc) {
        this.del.requestVisible(desc);
    }

    public void addMultiViewDescription(MultiViewDescription descr, int position) {
        this.del.addMultiViewDescription(descr, position);
    }

    public void removeMultiViewDescription(MultiViewDescription descr) {
        this.del.removeMultiViewDescription(descr);
    }

    static {
        AccessorImpl.createAccesor();
    }
}

