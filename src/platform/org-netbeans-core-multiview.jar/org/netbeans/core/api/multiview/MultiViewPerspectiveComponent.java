/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.core.api.multiview;

import javax.swing.Action;
import javax.swing.JComponent;
import org.netbeans.core.api.multiview.AccessorImpl;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.openide.util.Lookup;

final class MultiViewPerspectiveComponent {
    private MultiViewElement element;

    MultiViewPerspectiveComponent(MultiViewElement elem) {
        this.element = elem;
    }

    MultiViewElement getElement() {
        return this.element;
    }

    public JComponent getVisualRepresentation() {
        return this.element.getVisualRepresentation();
    }

    public JComponent getToolbarRepresentation() {
        return this.element.getToolbarRepresentation();
    }

    public Action[] getActions() {
        return this.element.getActions();
    }

    public Lookup getLookup() {
        return this.element.getLookup();
    }

    static {
        AccessorImpl.createAccesor();
    }
}

