/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.windows.CloneableTopComponent
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.api.multiview;

import java.io.Serializable;
import org.netbeans.core.api.multiview.MultiViewHandler;
import org.netbeans.core.multiview.MultiViewCloneableTopComponent;
import org.netbeans.core.multiview.MultiViewHandlerDelegate;
import org.netbeans.core.multiview.MultiViewTopComponent;
import org.openide.util.Lookup;
import org.openide.windows.CloneableTopComponent;
import org.openide.windows.TopComponent;

public final class MultiViews {
    private MultiViews() {
    }

    public static MultiViewHandler findMultiViewHandler(TopComponent tc) {
        if (tc != null) {
            if (tc instanceof MultiViewTopComponent) {
                return new MultiViewHandler(((MultiViewTopComponent)tc).getMultiViewHandlerDelegate());
            }
            if (tc instanceof MultiViewCloneableTopComponent) {
                return new MultiViewHandler(((MultiViewCloneableTopComponent)tc).getMultiViewHandlerDelegate());
            }
        }
        return null;
    }

    public static <T extends Serializable,  extends Lookup.Provider> TopComponent createMultiView(String mimeType, T context) {
        MultiViewTopComponent tc = new MultiViewTopComponent();
        tc.setMimeLookup(mimeType, context);
        return tc;
    }

    public static <T extends Serializable,  extends Lookup.Provider> CloneableTopComponent createCloneableMultiView(String mimeType, T context) {
        MultiViewCloneableTopComponent tc = new MultiViewCloneableTopComponent();
        tc.setMimeLookup(mimeType, context);
        return tc;
    }
}

