/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.openide.util.HelpCtx
 */
package org.netbeans.core.api.multiview;

import java.awt.Image;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.core.api.multiview.AccessorImpl;
import org.netbeans.core.spi.multiview.MultiViewDescription;
import org.openide.util.HelpCtx;

public final class MultiViewPerspective {
    private MultiViewDescription description;

    MultiViewPerspective(MultiViewDescription desc) {
        this.description = desc;
    }

    MultiViewDescription getDescription() {
        return this.description;
    }

    public int getPersistenceType() {
        return this.description.getPersistenceType();
    }

    public String getDisplayName() {
        return this.description.getDisplayName();
    }

    @CheckForNull
    public Image getIcon() {
        return this.description.getIcon();
    }

    public HelpCtx getHelpCtx() {
        return this.description.getHelpCtx();
    }

    public String preferredID() {
        return this.description.preferredID();
    }

    static {
        AccessorImpl.createAccesor();
    }
}

