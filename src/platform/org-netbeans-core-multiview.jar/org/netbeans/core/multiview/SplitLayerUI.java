/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.multiview;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.plaf.LayerUI;
import org.netbeans.core.multiview.SplitAction;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

class SplitLayerUI
extends LayerUI<JPanel> {
    private final JComponent splitDragger;
    private Point lastLocation;
    private boolean horizontalSplit = true;
    private final int splitterWidth;
    private final JComponent content;
    private boolean isDragging = false;
    private final AWTEventListener awtListener;
    private static final Color FILL_COLOR = new Color(0, 0, 0, 128);

    public SplitLayerUI(final JComponent content) {
        this.content = content;
        int width = new JSplitPane().getDividerSize();
        this.splitterWidth = Math.max(5, width);
        this.splitDragger = new JLabel(ImageUtilities.loadImageIcon((String)"org/netbeans/core/multiview/resources/splitview.png", (boolean)true));
        this.splitDragger.setToolTipText(NbBundle.getMessage(SplitLayerUI.class, (String)"Hint_SplitView"));
        this.splitDragger.addMouseMotionListener(new MouseAdapter(){

            @Override
            public void mouseDragged(MouseEvent e) {
                if (!SplitLayerUI.this.isDragging && e.getSource() == SplitLayerUI.this.splitDragger) {
                    Rectangle bounds = SplitLayerUI.this.splitDragger.getBounds();
                    bounds.setLocation(SplitLayerUI.this.splitDragger.getLocationOnScreen());
                    if (bounds.contains(e.getLocationOnScreen())) {
                        SplitLayerUI.this.isDragging = true;
                        Toolkit.getDefaultToolkit().addAWTEventListener(SplitLayerUI.this.awtListener, 24);
                    }
                }
                if (SplitLayerUI.this.isDragging) {
                    SplitLayerUI.this.update(e.getLocationOnScreen());
                }
            }
        });
        this.awtListener = new AWTEventListener(){

            @Override
            public void eventDispatched(AWTEvent event) {
                if (event.getID() == 502) {
                    final int splitLocation = SplitLayerUI.this.horizontalSplit ? SplitLayerUI.access$500((SplitLayerUI)SplitLayerUI.this).x : SplitLayerUI.access$500((SplitLayerUI)SplitLayerUI.this).y;
                    final int orientation = SplitLayerUI.this.horizontalSplit ? 1 : 0;
                    SplitLayerUI.this.cancelDragging();
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            TopComponent tc = (TopComponent)SwingUtilities.getAncestorOfClass(TopComponent.class, content);
                            SplitAction.splitWindow(tc, orientation, splitLocation);
                        }
                    });
                } else if (event.getID() == 401 || event.getID() == 402) {
                    SplitLayerUI.this.cancelDragging();
                }
            }

        };
    }

    private void cancelDragging() {
        Toolkit.getDefaultToolkit().removeAWTEventListener(this.awtListener);
        this.isDragging = false;
        this.lastLocation = null;
        this.content.repaint();
    }

    JComponent getSplitDragger() {
        return this.splitDragger;
    }

    private void update(Point locationOnScreen) {
        if (null != locationOnScreen) {
            SwingUtilities.convertPointFromScreen(locationOnScreen, this.content);
            this.lastLocation = locationOnScreen;
            this.horizontalSplit = this.calculateOrientation();
            this.lastLocation.x = Math.max(0, this.lastLocation.x);
            this.lastLocation.y = Math.max(0, this.lastLocation.y);
            this.lastLocation.x = Math.min(this.content.getWidth() - this.splitterWidth, this.lastLocation.x);
            this.lastLocation.y = Math.min(this.content.getHeight() - this.splitterWidth, this.lastLocation.y);
            this.content.repaint();
        } else {
            this.lastLocation = null;
        }
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        LayerUI.super.paint(g, c);
        if (null != this.lastLocation && this.isDragging) {
            Rectangle rect = new Rectangle();
            if (this.horizontalSplit) {
                rect.width = this.splitterWidth;
                rect.height = c.getHeight();
                rect.x = this.lastLocation.x;
            } else {
                rect.width = c.getWidth();
                rect.height = this.splitterWidth;
                rect.y = this.lastLocation.y;
            }
            g.setColor(FILL_COLOR);
            g.fillRect(rect.x, rect.y, rect.width, rect.height);
        }
    }

    private boolean calculateOrientation() {
        int verticalDistance = this.lastLocation.y;
        int horizontalDistance = this.content.getWidth() - this.lastLocation.x;
        return verticalDistance < horizontalDistance;
    }

    static /* synthetic */ Point access$500(SplitLayerUI x0) {
        return x0.lastLocation;
    }

}

