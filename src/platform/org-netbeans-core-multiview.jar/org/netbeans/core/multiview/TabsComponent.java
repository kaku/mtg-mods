/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Actions
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.multiview;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JLayer;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;
import javax.swing.plaf.LayerUI;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import org.netbeans.core.multiview.ContextAwareDescription;
import org.netbeans.core.multiview.MultiViewModel;
import org.netbeans.core.multiview.MultiViewPeer;
import org.netbeans.core.multiview.SplitAction;
import org.netbeans.core.multiview.SplitLayerUI;
import org.netbeans.core.spi.multiview.MultiViewDescription;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.openide.awt.Actions;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

class TabsComponent
extends JPanel {
    private JComponent EMPTY;
    private static final String TOOLBAR_MARKER = "MultiViewPanel";
    MultiViewModel model;
    private MouseListener buttonMouseListener = null;
    private JComponent toolbarPanel;
    private JComponent toolbarPanelSplit;
    final JPanel componentPanel = new JPanel();
    JPanel componentPanelSplit;
    private CardLayout cardLayout;
    private CardLayout cardLayoutSplit;
    private Set<MultiViewElement> alreadyAddedElements;
    private Set<MultiViewElement> alreadyAddedElementsSplit;
    private JToolBar bar = new JToolBar();
    private JToolBar barSplit;
    private JSplitPane splitPane;
    private AWTEventListener awtEventListener;
    private boolean isTopLeft = true;
    private JPanel topLeftComponent;
    private JPanel bottomRightComponent;
    private MultiViewDescription[] topBottomDescriptions = null;
    private PropertyChangeListener splitterPropertyChangeListener;
    private boolean removeSplit = false;
    private boolean hiddenTriggeredByMultiViewButton = false;
    private SplitLayerUI layerUI;
    private static final boolean AQUA = "Aqua".equals(UIManager.getLookAndFeel().getID());
    private boolean toolbarVisible = true;
    private PropertyChangeListener _lookAndFeelListener;
    private Border buttonBorder = null;

    public TabsComponent(boolean toolVis) {
        Border b = (Border)UIManager.get("Nb.Editor.Toolbar.border");
        this.bar.setBorder(new MatteBorder(0, 0, 0, 2, UIManager.getLookAndFeelDefaults().getColor("Separator.foreground")));
        this.bar.setFloatable(false);
        this.bar.setFocusable(true);
        if ("Windows".equals(UIManager.getLookAndFeel().getID()) && !TabsComponent.isXPTheme()) {
            this.bar.setRollover(true);
        } else if (AQUA) {
            this.bar.setBackground(UIManager.getColor("NbExplorerView.background"));
        }
        this.updateLAF();
        this.setLayout(new BorderLayout());
        this.bar.setMinimumSize(new Dimension(10, 10));
        this.bar.setFloatable(false);
        this.bar.setOrientation(1);
        this.add((Component)this.bar, "West");
        this.startToggling();
        this.setToolbarBarVisible(toolVis);
    }

    private void updateLAF() {
        Color bg = UIManager.getColor("Nb.Editor.Toolbar.background");
        if (bg != null && this.bar != null) {
            this.bar.setBackground(bg);
        }
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._lookAndFeelListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                TabsComponent.this.updateLAF();
            }
        };
        UIManager.addPropertyChangeListener(this._lookAndFeelListener);
    }

    @Override
    public void removeNotify() {
        UIManager.removePropertyChangeListener(this._lookAndFeelListener);
        this._lookAndFeelListener = null;
        super.removeNotify();
    }

    public void setModel(MultiViewModel model) {
        if (this.model != null) {
            this.bar.removeAll();
        }
        this.model = model;
        this.cardLayout = new CardLayout();
        this.componentPanel.removeAll();
        this.componentPanel.setLayout(this.cardLayout);
        if (SplitAction.isSplitingEnabled() && model.canSplit()) {
            this.layerUI = new SplitLayerUI(this.componentPanel);
            this.add(new JLayer<JPanel>(this.componentPanel, this.layerUI), "Center");
        } else {
            this.layerUI = null;
            this.add((Component)this.componentPanel, "Center");
        }
        this.alreadyAddedElements = new HashSet<MultiViewElement>();
        MultiViewDescription[] descs = model.getDescriptions();
        MultiViewDescription def = model.getActiveDescription();
        AbstractButton active = null;
        int prefHeight = -1;
        int prefWidth = -1;
        for (int i = 0; i < descs.length; ++i) {
            boolean shouldCreateToggleButton = true;
            if (descs[i] instanceof ContextAwareDescription) {
                boolean bl = shouldCreateToggleButton = !((ContextAwareDescription)descs[i]).isSplitDescription();
            }
            if (!shouldCreateToggleButton) continue;
            JToggleButton button = this.createButton(descs[i]);
            model.getButtonGroup().add(button);
            GridBagConstraints cons = new GridBagConstraints();
            cons.anchor = 17;
            prefHeight = Math.max(button.getPreferredSize().height, prefHeight);
            prefWidth = Math.max(button.getPreferredSize().width, prefWidth);
            if (descs[i] != model.getActiveDescription()) continue;
            active = button;
        }
        Enumeration<AbstractButton> en = model.getButtonGroup().getElements();
        while (en.hasMoreElements()) {
            JToggleButton but = (JToggleButton)en.nextElement();
            but.setPreferredSize(new Dimension(prefWidth + 10, prefHeight));
            but.setMinimumSize(new Dimension(prefWidth + 10, prefHeight));
        }
        if (active != null) {
            active.setSelected(true);
        }
        this.toolbarPanel = this.getEmptyInnerToolBar();
        GridBagConstraints cons = new GridBagConstraints();
        cons.anchor = 13;
        cons.fill = 1;
        cons.gridwidth = 0;
        cons.weightx = 1.0;
        this.bar.add(this.toolbarPanel);
    }

    MultiViewDescription getTopComponentDescription() {
        return this.topBottomDescriptions == null ? this.model.getActiveDescription() : this.topBottomDescriptions[0];
    }

    MultiViewDescription getBottomComponentDescription() {
        return this.topBottomDescriptions == null ? this.model.getActiveDescription() : this.topBottomDescriptions[1];
    }

    void peerClearSplit(int splitElementToActivate) {
        MultiViewDescription toDeactivate;
        MultiViewDescription activeDescription = null;
        if (splitElementToActivate < 0 || splitElementToActivate >= this.topBottomDescriptions.length) {
            activeDescription = this.model.getActiveDescription();
            if (null == activeDescription) {
                activeDescription = this.topBottomDescriptions[0];
            }
        } else {
            activeDescription = this.topBottomDescriptions[splitElementToActivate];
        }
        MultiViewDescription multiViewDescription = toDeactivate = activeDescription == this.topBottomDescriptions[0] ? this.topBottomDescriptions[1] : this.topBottomDescriptions[0];
        if (activeDescription == this.topBottomDescriptions[1]) {
            MultiViewDescription[] descriptions = this.model.getDescriptions();
            for (int i = 1; i < descriptions.length; ++i) {
                if (descriptions[i] != activeDescription) continue;
                activeDescription = descriptions[i - 1];
                break;
            }
        }
        Toolkit.getDefaultToolkit().removeAWTEventListener(this.getAWTEventListener());
        this.splitPane.removePropertyChangeListener(this.splitterPropertyChangeListener);
        this.removeAll();
        this.splitPane = null;
        this.topBottomDescriptions = null;
        this.isTopLeft = true;
        this.topLeftComponent = null;
        this.bottomRightComponent = null;
        this.alreadyAddedElementsSplit = null;
        this.awtEventListener = null;
        this.barSplit = null;
        this.cardLayoutSplit = null;
        this.componentPanelSplit = null;
        this.toolbarPanelSplit = null;
        this.splitterPropertyChangeListener = null;
        this.add((Component)this.bar, "North");
        if (null != this.layerUI) {
            this.add(new JLayer<JPanel>(this.componentPanel, this.layerUI), "Center");
        } else {
            this.add((Component)this.componentPanel, "Center");
        }
        MultiViewElement mve = this.model.getElementForDescription(toDeactivate);
        mve.componentDeactivated();
        mve.componentHidden();
        this.model.setActiveDescription(activeDescription);
        this.syncButtonsWithModel();
    }

    private void setupSplit() {
        this.topLeftComponent = new JPanel(new BorderLayout());
        this.topLeftComponent.add((Component)this.bar, "North");
        this.topLeftComponent.add((Component)this.componentPanel, "Center");
        this.bottomRightComponent = new JPanel();
        this.barSplit = new JToolBar();
        Border b = (Border)UIManager.get("Nb.Editor.Toolbar.border");
        this.barSplit.setBorder(b);
        this.barSplit.setFloatable(false);
        this.barSplit.setFocusable(true);
        if ("Windows".equals(UIManager.getLookAndFeel().getID()) && !TabsComponent.isXPTheme()) {
            this.barSplit.setRollover(true);
        } else if (AQUA) {
            this.barSplit.setBackground(UIManager.getColor("NbExplorerView.background"));
        }
        this.bottomRightComponent.setLayout(new BorderLayout());
        this.bottomRightComponent.add((Component)this.barSplit, "North");
        this.startTogglingSplit();
        this.setToolbarBarVisibleSplit(this.bar.isVisible());
        this.componentPanelSplit = new JPanel();
        this.cardLayoutSplit = new CardLayout();
        this.componentPanelSplit.setLayout(this.cardLayoutSplit);
        this.bottomRightComponent.add((Component)this.componentPanelSplit, "Center");
        this.alreadyAddedElementsSplit = new HashSet<MultiViewElement>();
        MultiViewDescription[] descs = this.model.getDescriptions();
        GridBagLayout grid = new GridBagLayout();
        this.barSplit.setLayout(grid);
        int prefHeight = -1;
        int prefWidth = -1;
        for (int i = 0; i < descs.length; ++i) {
            if (!(descs[i] instanceof ContextAwareDescription) || !((ContextAwareDescription)descs[i]).isSplitDescription()) continue;
            JToggleButton button = this.createButton(descs[i]);
            this.model.getButtonGroupSplit().add(button);
            GridBagConstraints cons = new GridBagConstraints();
            cons.anchor = 17;
            prefHeight = Math.max(button.getPreferredSize().height, prefHeight);
            this.barSplit.add((Component)button, cons);
            prefWidth = Math.max(button.getPreferredSize().width, prefWidth);
        }
        Enumeration<AbstractButton> en = this.model.getButtonGroupSplit().getElements();
        while (en.hasMoreElements()) {
            JToggleButton but = (JToggleButton)en.nextElement();
            Insets ins = but.getBorder().getBorderInsets(but);
            but.setPreferredSize(new Dimension(prefWidth + 10, prefHeight));
            but.setMinimumSize(new Dimension(prefWidth + 10, prefHeight));
        }
        this.toolbarPanelSplit = this.getEmptyInnerToolBar();
        GridBagConstraints cons = new GridBagConstraints();
        cons.anchor = 13;
        cons.fill = 1;
        cons.gridwidth = 0;
        cons.weightx = 1.0;
        this.barSplit.add((Component)this.toolbarPanelSplit, cons);
    }

    void peerSplitComponent(int orientation, MultiViewPeer mvPeer, MultiViewDescription defaultDesc, MultiViewDescription defaultDescClone, int splitPosition) {
        MultiViewDescription[] descriptions = this.model.getDescriptions();
        MultiViewDescription activeDescription = this.model.getActiveDescription();
        if (this.splitPane == null) {
            this.splitPane = new JSplitPane();
            this.topBottomDescriptions = new MultiViewDescription[2];
            if (defaultDesc != null && defaultDescClone != null) {
                this.topBottomDescriptions[0] = defaultDesc;
                this.topBottomDescriptions[1] = defaultDescClone;
            } else {
                int activeDescIndex = 0;
                for (int i = 0; i < descriptions.length; ++i) {
                    MultiViewDescription multiViewDescription = descriptions[i];
                    if (!multiViewDescription.getDisplayName().equals(activeDescription.getDisplayName())) continue;
                    activeDescIndex = i;
                    break;
                }
                this.topBottomDescriptions[0] = descriptions[activeDescIndex];
                this.topBottomDescriptions[1] = descriptions[activeDescIndex + 1];
            }
            this.setupSplit();
            this.splitPane.setOneTouchExpandable(false);
            this.splitPane.setContinuousLayout(true);
            this.splitPane.setResizeWeight(0.5);
            this.splitPane.setBorder(BorderFactory.createEmptyBorder());
            this.removeAll();
            Toolkit.getDefaultToolkit().addAWTEventListener(this.getAWTEventListener(), 48);
            this.add((Component)this.splitPane, "Center");
            MultiViewDescription bottomDescription = this.topBottomDescriptions[1];
            this.isTopLeft = false;
            this.model.setActiveDescription(bottomDescription);
            this.syncSplitButtonsWithModel();
            MultiViewDescription topDescription = this.topBottomDescriptions[0];
            this.isTopLeft = true;
            this.model.setActiveDescription(topDescription);
            this.syncSplitButtonsWithModel();
        } else {
            this.topLeftComponent = (JPanel)this.splitPane.getTopComponent();
            this.bottomRightComponent = (JPanel)this.splitPane.getBottomComponent();
        }
        if (orientation != this.splitPane.getOrientation()) {
            this.splitPane.removePropertyChangeListener(this.splitterPropertyChangeListener);
            this.splitterPropertyChangeListener = null;
        }
        this.bar.remove(this.layerUI.getSplitDragger());
        this.splitPane.setOrientation(orientation);
        this.splitPane.setTopComponent(this.topLeftComponent);
        this.splitPane.setBottomComponent(this.bottomRightComponent);
        this.splitPane.addPropertyChangeListener("dividerLocation", this.getSplitterPropertyChangeListener(orientation));
        this.topLeftComponent.setMinimumSize(new Dimension(0, 0));
        this.bottomRightComponent.setMinimumSize(new Dimension(0, 0));
        if (splitPosition > 0) {
            this.splitPane.setDividerLocation(splitPosition);
        }
    }

    private void syncSplitButtonsWithModel() {
        this.model.setFreezeTabButtons(true);
        Enumeration<AbstractButton> en = this.model.getButtonGroupSplit().getElements();
        while (en.hasMoreElements()) {
            JToggleButton but = (JToggleButton)en.nextElement();
            TabsButtonModel buttonModel = (TabsButtonModel)but.getModel();
            MultiViewDescription buttonsDescription = buttonModel.getButtonsDescription();
            if (buttonsDescription != (this.isTopLeft ? this.topBottomDescriptions[0] : this.topBottomDescriptions[1])) continue;
            but.setSelected(true);
        }
        this.model.setFreezeTabButtons(false);
    }

    private void syncButtonsWithModel() {
        this.model.setFreezeTabButtons(true);
        Enumeration<AbstractButton> en = this.model.getButtonGroup().getElements();
        while (en.hasMoreElements()) {
            JToggleButton but = (JToggleButton)en.nextElement();
            TabsButtonModel buttonModel = (TabsButtonModel)but.getModel();
            MultiViewDescription buttonsDescription = buttonModel.getButtonsDescription();
            if (buttonsDescription != this.model.getActiveDescription()) continue;
            but.setSelected(true);
        }
        this.model.setFreezeTabButtons(false);
    }

    private PropertyChangeListener getSplitterPropertyChangeListener(final int orientation) {
        if (this.splitterPropertyChangeListener == null) {
            this.splitterPropertyChangeListener = new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent pce) {
                    if (TabsComponent.this.splitPane != null) {
                        int splitSize;
                        int bottomMinSize;
                        int topMinSize;
                        int current = Integer.parseInt(pce.getNewValue().toString());
                        int dividerSize = TabsComponent.this.splitPane.getDividerSize();
                        if (orientation == 0) {
                            splitSize = TabsComponent.this.splitPane.getHeight();
                            topMinSize = (int)TabsComponent.this.topLeftComponent.getMinimumSize().getHeight();
                            bottomMinSize = (int)TabsComponent.this.bottomRightComponent.getMinimumSize().getHeight();
                        } else {
                            splitSize = TabsComponent.this.splitPane.getWidth();
                            topMinSize = (int)TabsComponent.this.topLeftComponent.getMinimumSize().getWidth();
                            bottomMinSize = (int)TabsComponent.this.bottomRightComponent.getMinimumSize().getWidth();
                        }
                        int min = topMinSize;
                        int max = splitSize - bottomMinSize - dividerSize;
                        TabsComponent.this.removeSplit = splitSize > 0 && (current <= min || current >= max);
                    }
                }
            };
        }
        return this.splitterPropertyChangeListener;
    }

    private AWTEventListener getAWTEventListener() {
        if (this.awtEventListener == null) {
            this.awtEventListener = new AWTEventListener(){

                @Override
                public void eventDispatched(AWTEvent event) {
                    MouseEvent e = (MouseEvent)event;
                    if (TabsComponent.this.splitPane != null && e.getID() == 501) {
                        Component component;
                        MultiViewDescription activeDescription = null;
                        Point locationOnScreen = e.getLocationOnScreen();
                        SwingUtilities.convertPointFromScreen(locationOnScreen, TabsComponent.this.splitPane);
                        for (component = e.getComponent(); component != null && component != TabsComponent.this.splitPane; component = component.getParent()) {
                        }
                        if (component == TabsComponent.this.splitPane && TabsComponent.this.splitPane.getTopComponent().getBounds().contains(locationOnScreen)) {
                            TabsComponent.this.isTopLeft = true;
                            activeDescription = TabsComponent.this.topBottomDescriptions[0];
                        } else if (component == TabsComponent.this.splitPane && TabsComponent.this.splitPane.getBottomComponent().getBounds().contains(locationOnScreen)) {
                            TabsComponent.this.isTopLeft = false;
                            activeDescription = TabsComponent.this.topBottomDescriptions[1];
                        }
                        if (!(activeDescription == null || TabsComponent.this.model.getActiveDescription().equals(activeDescription) && ((ContextAwareDescription)TabsComponent.this.model.getActiveDescription()).isSplitDescription() == ((ContextAwareDescription)activeDescription).isSplitDescription())) {
                            TabsComponent.this.model.setActiveDescription(activeDescription);
                        }
                    } else if (TabsComponent.this.splitPane != null && e.getID() == 502 && e.getButton() == 1 && TabsComponent.this.removeSplit && e.getComponent() instanceof BasicSplitPaneDivider) {
                        TabsComponent.this.removeSplit = false;
                        TopComponent tc = (TopComponent)SwingUtilities.getAncestorOfClass(TopComponent.class, TabsComponent.this.splitPane);
                        if (null != tc) {
                            int toActivate = 0;
                            if (TabsComponent.this.splitPane.getDividerLocation() < 10) {
                                toActivate = 1;
                            }
                            SplitAction.clearSplit(tc, toActivate);
                        }
                    }
                }
            };
        }
        return this.awtEventListener;
    }

    boolean isHiddenTriggeredByMultiViewButton() {
        return this.hiddenTriggeredByMultiViewButton;
    }

    boolean isShowing(MultiViewDescription descr) {
        if (descr == null) {
            return false;
        }
        if (this.splitPane == null) {
            return this.model.getActiveDescription() == descr;
        }
        return this.isTopLeft ? this.topBottomDescriptions[1] == descr : this.topBottomDescriptions[0] == descr;
    }

    void switchToCard(MultiViewElement elem, String id, boolean isSplitElement) {
        if (isSplitElement) {
            this.switchToCardSplit(elem, id);
            return;
        }
        if (!this.alreadyAddedElements.contains(elem)) {
            this.componentPanel.add((Component)elem.getVisualRepresentation(), id);
            this.alreadyAddedElements.add(elem);
        }
        this.cardLayout.show(this.componentPanel, id);
    }

    private void switchToCardSplit(MultiViewElement elem, String id) {
        if (!this.alreadyAddedElementsSplit.contains(elem)) {
            this.componentPanelSplit.add((Component)elem.getVisualRepresentation(), id);
            this.alreadyAddedElementsSplit.add(elem);
        }
        this.cardLayoutSplit.show(this.componentPanelSplit, id);
    }

    void peerComponentClosed() {
        if (this.componentPanel != null) {
            this.componentPanel.removeAll();
        }
        if (this.alreadyAddedElements != null) {
            this.alreadyAddedElements.clear();
        }
        if (this.componentPanelSplit != null) {
            this.componentPanelSplit.removeAll();
        }
        if (this.alreadyAddedElementsSplit != null) {
            this.alreadyAddedElementsSplit.clear();
        }
    }

    void changeActiveManually(MultiViewDescription desc) {
        Enumeration<AbstractButton> en = this.model.getButtonGroup().getElements();
        MultiViewDescription activeDescription = this.model.getActiveDescription();
        if (activeDescription instanceof ContextAwareDescription && ((ContextAwareDescription)activeDescription).isSplitDescription()) {
            en = this.model.getButtonGroupSplit().getElements();
        }
        while (en.hasMoreElements()) {
            TabsButtonModel btnmodel;
            JToggleButton obj = (JToggleButton)en.nextElement();
            if (!(obj.getModel() instanceof TabsButtonModel) || !(btnmodel = (TabsButtonModel)obj.getModel()).getButtonsDescription().getDisplayName().equals(desc.getDisplayName())) continue;
            if (this.splitPane != null) {
                this.hiddenTriggeredByMultiViewButton = true;
                if (((ContextAwareDescription)activeDescription).isSplitDescription()) {
                    this.model.getButtonGroupSplit().setSelected(obj.getModel(), true);
                    this.isTopLeft = false;
                    this.topBottomDescriptions[1] = btnmodel.getButtonsDescription();
                } else {
                    this.model.getButtonGroup().setSelected(obj.getModel(), true);
                    this.isTopLeft = true;
                    this.topBottomDescriptions[0] = btnmodel.getButtonsDescription();
                }
                this.model.fireActivateCurrent();
                this.hiddenTriggeredByMultiViewButton = false;
                break;
            }
            obj.setSelected(true);
            MultiViewElement elem = this.model.getElementForDescription(desc);
            elem.getVisualRepresentation().requestFocus();
            break;
        }
    }

    void changeVisibleManually(MultiViewDescription desc) {
        Enumeration<AbstractButton> en = this.model.getButtonGroup().getElements();
        MultiViewDescription activeDescription = this.model.getActiveDescription();
        if (activeDescription instanceof ContextAwareDescription && ((ContextAwareDescription)activeDescription).isSplitDescription()) {
            en = this.model.getButtonGroupSplit().getElements();
            desc = this.model.getActiveDescription();
        }
        while (en.hasMoreElements()) {
            TabsButtonModel btnmodel;
            JToggleButton obj = (JToggleButton)en.nextElement();
            if (!(obj.getModel() instanceof TabsButtonModel) || !(btnmodel = (TabsButtonModel)obj.getModel()).getButtonsDescription().equals(desc)) continue;
            obj.setSelected(true);
            break;
        }
    }

    private JToggleButton createButton(MultiViewDescription description) {
        JToggleButton button = new JToggleButton();
        Mnemonics.setLocalizedText((AbstractButton)button, (String)Actions.cutAmpersand((String)description.getDisplayName()));
        button.setModel(new TabsButtonModel(description));
        button.setRolloverEnabled(true);
        Border b = this.getButtonBorder();
        if (b != null) {
            button.setBorder(b);
        }
        if (AQUA) {
            button.putClientProperty("JButton.buttonType", "square");
            button.putClientProperty("JComponent.sizeVariant", "small");
        }
        if (this.buttonMouseListener == null) {
            this.buttonMouseListener = new ButtonMouseListener();
        }
        button.addMouseListener(this.buttonMouseListener);
        button.setToolTipText(NbBundle.getMessage(TabsComponent.class, (String)"TabButton.tool_tip", (Object)button.getText()));
        button.setFocusable(true);
        button.setFocusPainted(true);
        return button;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void setInnerToolBar(JComponent innerbar, boolean isSplitElement) {
        if (isSplitElement) {
            this.setInnerToolBarSplit(innerbar);
            return;
        }
        Object object = this.getTreeLock();
        synchronized (object) {
            if (this.toolbarPanel != null) {
                this.bar.remove(this.toolbarPanel);
            }
            if (innerbar == null) {
                innerbar = this.getEmptyInnerToolBar();
            }
            innerbar.putClientProperty("MultiViewPanel", "X");
            if (!AQUA) {
                innerbar.setBorder(null);
            } else {
                innerbar.setBorder(BorderFactory.createEmptyBorder(2, 0, 2, 0));
            }
            this.toolbarPanel = innerbar;
            GridBagConstraints cons = new GridBagConstraints();
            cons.anchor = 13;
            cons.fill = 1;
            cons.weightx = 1.0;
            this.toolbarPanel.setMinimumSize(new Dimension(10, 10));
            this.bar.add(this.toolbarPanel);
            if (SplitAction.isSplitingEnabled() && null == this.splitPane && null != this.layerUI) {
                cons = new GridBagConstraints();
                cons.anchor = 13;
                cons.fill = 0;
                cons.insets = new Insets(0, 5, 0, 2);
                this.bar.add(this.layerUI.getSplitDragger());
            }
            this.bar.revalidate();
            this.bar.repaint();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void setInnerToolBarSplit(JComponent innerbar) {
        Object object = this.getTreeLock();
        synchronized (object) {
            if (this.toolbarPanelSplit != null) {
                this.barSplit.remove(this.toolbarPanelSplit);
            }
            if (innerbar == null) {
                innerbar = this.getEmptyInnerToolBar();
            }
            innerbar.setOpaque(false);
            innerbar.putClientProperty("MultiViewPanel", "X");
            if (!AQUA) {
                innerbar.setBorder(null);
            } else {
                innerbar.setBorder(BorderFactory.createEmptyBorder(2, 0, 2, 0));
            }
            this.toolbarPanelSplit = innerbar;
            GridBagConstraints cons = new GridBagConstraints();
            cons.anchor = 13;
            cons.fill = 1;
            cons.gridwidth = 0;
            cons.weightx = 1.0;
            this.toolbarPanelSplit.setMinimumSize(new Dimension(10, 10));
            this.barSplit.add((Component)this.toolbarPanelSplit, cons);
            this.barSplit.revalidate();
            this.barSplit.repaint();
        }
    }

    void setToolbarBarVisible(boolean visible) {
        if (this.toolbarVisible == visible) {
            return;
        }
        this.toolbarVisible = visible;
        this.bar.setVisible(visible);
        if (null != this.barSplit) {
            this.barSplit.setVisible(visible);
        }
    }

    void setToolbarBarVisibleSplit(boolean visible) {
        if (this.toolbarVisible == visible) {
            return;
        }
        this.toolbarVisible = visible;
        this.barSplit.setVisible(visible);
    }

    JComponent getEmptyInnerToolBar() {
        if (this.EMPTY == null) {
            this.EMPTY = new JPanel();
            this.EMPTY.setLayout(new BoxLayout(this.EMPTY, 1));
        }
        return this.EMPTY;
    }

    void requestFocusForSelectedButton() {
        this.bar.setFocusable(true);
        Enumeration<AbstractButton> en = this.model.getButtonGroup().getElements();
        while (en.hasMoreElements()) {
            JToggleButton but = (JToggleButton)en.nextElement();
            if (!this.model.getButtonGroup().isSelected(but.getModel())) continue;
            but.requestFocus();
            return;
        }
        throw new IllegalStateException("How come none of the buttons is selected?");
    }

    void requestFocusForPane() {
        this.bar.setFocusable(false);
        this.componentPanel.requestFocus();
    }

    private Border getButtonBorder() {
        if (this.buttonBorder == null) {
            this.buttonBorder = UIManager.getBorder("nb.tabbutton.border");
        }
        return this.buttonBorder;
    }

    public static boolean isXPTheme() {
        Boolean isXP = (Boolean)Toolkit.getDefaultToolkit().getDesktopProperty("win.xpstyle.themeActive");
        return isXP == null ? false : isXP;
    }

    void startToggling() {
        ActionMap map = this.bar.getActionMap();
        AbstractAction act = new TogglesGoEastAction();
        map.put("navigateRight", act);
        InputMap input = this.bar.getInputMap(1);
        act = new TogglesGoWestAction();
        map.put("navigateLeft", act);
        act = new TogglesGoDownAction();
        map.put("TogglesGoDown", act);
        map.put("navigateUp", act);
        KeyStroke stroke = KeyStroke.getKeyStroke("ESCAPE");
        input.put(stroke, "TogglesGoDown");
    }

    void startTogglingSplit() {
        ActionMap map = this.barSplit.getActionMap();
        AbstractAction act = new TogglesGoEastAction();
        map.put("navigateRight", act);
        InputMap input = this.barSplit.getInputMap(1);
        act = new TogglesGoWestAction();
        map.put("navigateLeft", act);
        act = new TogglesGoDownAction();
        map.put("TogglesGoDown", act);
        map.put("navigateUp", act);
        KeyStroke stroke = KeyStroke.getKeyStroke("ESCAPE");
        input.put(stroke, "TogglesGoDown");
    }

    class ButtonMouseListener
    extends MouseAdapter {
        ButtonMouseListener() {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            AbstractButton b = (AbstractButton)e.getComponent();
            b.getModel().setRollover(true);
        }

        @Override
        public void mouseExited(MouseEvent e) {
            AbstractButton b = (AbstractButton)e.getComponent();
            b.getModel().setRollover(false);
        }

        @Override
        public void mousePressed(MouseEvent e) {
            e.consume();
            AbstractButton b = (AbstractButton)e.getComponent();
            MultiViewModel model = TabsComponent.this.model;
            if (model != null) {
                if (TabsComponent.this.splitPane != null) {
                    ButtonModel buttonModel = b.getModel();
                    if (buttonModel instanceof TabsButtonModel) {
                        MultiViewDescription buttonsDescription = ((TabsButtonModel)buttonModel).getButtonsDescription();
                        TabsComponent.this.hiddenTriggeredByMultiViewButton = true;
                        if (((ContextAwareDescription)buttonsDescription).isSplitDescription()) {
                            model.getButtonGroupSplit().setSelected(b.getModel(), true);
                            TabsComponent.this.isTopLeft = false;
                            TabsComponent.access$600((TabsComponent)TabsComponent.this)[1] = buttonsDescription;
                        } else {
                            model.getButtonGroup().setSelected(b.getModel(), true);
                            TabsComponent.this.isTopLeft = true;
                            TabsComponent.access$600((TabsComponent)TabsComponent.this)[0] = buttonsDescription;
                        }
                        model.fireActivateCurrent();
                        TabsComponent.this.hiddenTriggeredByMultiViewButton = false;
                    }
                    return;
                }
                model.getButtonGroup().setSelected(b.getModel(), true);
                model.fireActivateCurrent();
            }
        }
    }

    static class TabsButtonModel
    extends JToggleButton.ToggleButtonModel {
        private MultiViewDescription desc;

        public TabsButtonModel(MultiViewDescription description) {
            this.desc = description;
        }

        public MultiViewDescription getButtonsDescription() {
            return this.desc;
        }
    }

    private class TogglesGoDownAction
    extends AbstractAction {
        private TogglesGoDownAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            TabsComponent.this.changeActiveManually(TabsComponent.this.model.getActiveDescription());
            TabsComponent.this.model.getActiveElement().getVisualRepresentation().requestFocusInWindow();
        }
    }

    private class TogglesGoEastAction
    extends AbstractAction {
        private TogglesGoEastAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            MultiViewDescription[] descs = TabsComponent.this.model.getDescriptions();
            MultiViewDescription active = TabsComponent.this.model.getActiveDescription();
            for (int i = 0; i < descs.length; ++i) {
                if (descs[i] != active) continue;
                int next = i + 1;
                if (next >= descs.length) {
                    next = 0;
                }
                TabsComponent.this.changeVisibleManually(descs[next]);
                TabsComponent.this.requestFocusForSelectedButton();
            }
        }
    }

    private class TogglesGoWestAction
    extends AbstractAction {
        private TogglesGoWestAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            MultiViewDescription[] descs = TabsComponent.this.model.getDescriptions();
            MultiViewDescription active = TabsComponent.this.model.getActiveDescription();
            for (int i = 0; i < descs.length; ++i) {
                if (descs[i] != active) continue;
                int next = i - 1;
                if (next < 0) {
                    next = descs.length - 1;
                }
                TabsComponent.this.changeVisibleManually(descs[next]);
                TabsComponent.this.requestFocusForSelectedButton();
            }
        }
    }

}

