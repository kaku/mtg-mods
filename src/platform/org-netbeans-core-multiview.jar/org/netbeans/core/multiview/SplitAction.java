/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.DynamicMenuContent
 *  org.openide.awt.Mnemonics
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.multiview;

import java.awt.event.ActionEvent;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;
import org.netbeans.core.multiview.Bundle;
import org.netbeans.core.multiview.Splitable;
import org.openide.awt.DynamicMenuContent;
import org.openide.awt.Mnemonics;
import org.openide.util.actions.Presenter;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class SplitAction
extends AbstractAction
implements Presenter.Menu,
Presenter.Popup {
    boolean useSplitName = false;

    public SplitAction() {
        super(Bundle.CTL_SplitDocumentAction());
    }

    public SplitAction(boolean useSplitName) {
        super(Bundle.CTL_SplitDocumentAction());
        this.useSplitName = useSplitName;
    }

    static Action createSplitAction(Map map) {
        if (!SplitAction.isSplitingEnabled()) {
            return null;
        }
        Object nameObj = map.get("displayName");
        if (nameObj == null) {
            return null;
        }
        return new SplitAction(nameObj.toString().equals(Bundle.CTL_SplitAction()));
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        assert (false);
    }

    public JMenuItem getMenuPresenter() {
        return this.getSplitMenuItem();
    }

    public JMenuItem getPopupPresenter() {
        return this.getSplitMenuItem();
    }

    private JMenuItem getSplitMenuItem() {
        if (!SplitAction.isSplitingEnabled()) {
            return null;
        }
        UpdatingMenu menu = new UpdatingMenu();
        String label = this.useSplitName ? Bundle.CTL_SplitAction() : Bundle.CTL_SplitDocumentAction();
        Mnemonics.setLocalizedText((AbstractButton)menu, (String)label);
        return menu;
    }

    static boolean isSplitingEnabled() {
        boolean splitingEnabled = "true".equals(Bundle.MultiViewElement_Spliting_Enabled());
        return splitingEnabled;
    }

    static void splitWindow(TopComponent tc, int orientation) {
        SplitAction.splitWindow(tc, orientation, -1);
    }

    static void splitWindow(TopComponent tc, int orientation, int splitLocation) {
        if (tc instanceof Splitable) {
            TopComponent split = ((Splitable)tc).splitComponent(orientation, splitLocation);
            split.open();
            split.requestActive();
            split.invalidate();
            split.revalidate();
            split.repaint();
            split.requestFocusInWindow();
        }
    }

    static void clearSplit(TopComponent tc, int elementToActivate) {
        if (tc instanceof Splitable) {
            TopComponent original = ((Splitable)tc).clearSplit(elementToActivate);
            original.open();
            original.requestActive();
            original.invalidate();
            original.revalidate();
            original.repaint();
            original.requestFocusInWindow();
        }
    }

    private static class ClearSplitAction
    extends AbstractAction {
        private final TopComponent tc;

        public ClearSplitAction(TopComponent tc) {
            this.tc = tc;
            this.putValue("Name", Bundle.LBL_ClearSplitAction());
            this.putValue("_nb_action_id_", Bundle.LBL_ValueClearSplit());
            if (tc instanceof Splitable) {
                this.setEnabled(((Splitable)tc).getSplitOrientation() != -1);
            } else {
                this.setEnabled(false);
            }
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            SplitAction.clearSplit(this.tc, -1);
        }
    }

    private static class SplitDocumentAction
    extends AbstractAction {
        private final TopComponent tc;
        private final int orientation;

        public SplitDocumentAction(TopComponent tc, int orientation) {
            this.tc = tc;
            this.orientation = orientation;
            this.putValue("Name", orientation == 0 ? Bundle.LBL_SplitDocumentActionVertical() : Bundle.LBL_SplitDocumentActionHorizontal());
            this.putValue("_nb_action_id_", orientation == 0 ? Bundle.LBL_ValueSplitVertical() : Bundle.LBL_ValueSplitHorizontal());
            if (tc instanceof Splitable) {
                int split = ((Splitable)tc).getSplitOrientation();
                this.setEnabled(split == -1 || split != orientation);
            } else {
                this.setEnabled(false);
            }
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            SplitAction.splitWindow(this.tc, this.orientation);
        }
    }

    private static final class UpdatingMenu
    extends JMenu
    implements DynamicMenuContent {
        private UpdatingMenu() {
        }

        public JComponent[] synchMenuPresenters(JComponent[] items) {
            return this.getMenuPresenters();
        }

        public JComponent[] getMenuPresenters() {
            assert (SwingUtilities.isEventDispatchThread());
            this.removeAll();
            TopComponent tc = WindowManager.getDefault().getRegistry().getActivated();
            if (tc != null) {
                this.setEnabled(true);
                if (tc instanceof Splitable && ((Splitable)tc).canSplit()) {
                    JMenuItem item = new JMenuItem(new SplitDocumentAction(tc, 0));
                    Mnemonics.setLocalizedText((AbstractButton)item, (String)item.getText());
                    this.add(item);
                    item = new JMenuItem(new SplitDocumentAction(tc, 1));
                    Mnemonics.setLocalizedText((AbstractButton)item, (String)item.getText());
                    this.add(item);
                    item = new JMenuItem(new ClearSplitAction(tc));
                    Mnemonics.setLocalizedText((AbstractButton)item, (String)item.getText());
                    this.add(item);
                } else {
                    this.setEnabled(false);
                }
            } else {
                this.setEnabled(false);
            }
            return new JComponent[]{this};
        }
    }

}

