/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Actions
 *  org.openide.awt.UndoRedo
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$SubComponent
 */
package org.netbeans.core.multiview;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import javax.swing.Action;
import org.netbeans.core.api.multiview.MultiViewPerspective;
import org.netbeans.core.multiview.Accessor;
import org.netbeans.core.multiview.ContextAwareDescription;
import org.netbeans.core.multiview.EditorsAction;
import org.netbeans.core.multiview.MultiViewElementCallbackDelegate;
import org.netbeans.core.multiview.MultiViewHandlerDelegate;
import org.netbeans.core.multiview.MultiViewModel;
import org.netbeans.core.multiview.MultiViewPeer;
import org.netbeans.core.multiview.MultiViewTopComponentLookup;
import org.netbeans.core.multiview.SpiAccessor;
import org.netbeans.core.multiview.SplitAction;
import org.netbeans.core.multiview.Splitable;
import org.netbeans.core.multiview.TabsComponent;
import org.netbeans.core.spi.multiview.CloseOperationHandler;
import org.netbeans.core.spi.multiview.MultiViewDescription;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.MultiViewElementCallback;
import org.openide.awt.Actions;
import org.openide.awt.UndoRedo;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;

public final class MultiViewTopComponent
extends TopComponent
implements MultiViewModel.ActionRequestObserverFactory,
Splitable {
    MultiViewPeer peer;
    private Action[] superActions4Tests = null;

    public MultiViewTopComponent() {
        this.peer = new MultiViewPeer(this, this);
        this.peer.initComponents();
        this.associateLookup(this.peer.getLookup());
        this.setName("");
        this.setFocusCycleRoot(false);
    }

    public <T extends Serializable,  extends Lookup.Provider> void setMimeLookup(String mimeType, T context) {
        this.peer.setMimeLookup(mimeType, (Lookup.Provider)context);
    }

    public void setMultiViewDescriptions(MultiViewDescription[] descriptions, MultiViewDescription defaultDesc) {
        this.peer.setMultiViewDescriptions(descriptions, defaultDesc);
    }

    public void setCloseOperationHandler(CloseOperationHandler handler) {
        this.peer.setCloseOperationHandler(handler);
    }

    private void setDeserializedMultiViewDescriptions(MultiViewDescription[] descriptions, MultiViewDescription defaultDesc, Map existingElements) {
        this.peer.setDeserializedMultiViewDescriptions(-1, descriptions, defaultDesc, null, existingElements);
    }

    MultiViewModel getModel() {
        return this.peer.getModel();
    }

    public Lookup getLookup() {
        this.peer.assignLookup((MultiViewTopComponentLookup)super.getLookup());
        return super.getLookup();
    }

    protected void componentClosed() {
        super.componentClosed();
        this.peer.peerComponentClosed();
    }

    protected void componentShowing() {
        super.componentShowing();
        this.peer.peerComponentShowing();
    }

    protected void componentHidden() {
        super.componentHidden();
        this.peer.peerComponentHidden();
    }

    protected void componentDeactivated() {
        super.componentDeactivated();
        this.peer.peerComponentDeactivated();
    }

    protected void componentActivated() {
        super.componentActivated();
        this.peer.peerComponentActivated();
    }

    protected void componentOpened() {
        super.componentOpened();
        this.peer.peerComponentOpened();
    }

    public Action[] getActions() {
        Action[] superActions = this.superActions4Tests == null ? super.getActions() : this.superActions4Tests;
        ArrayList<Action> acts = new ArrayList<Action>(Arrays.asList(this.peer.peerGetActions(superActions)));
        if (!acts.isEmpty()) {
            acts.add(null);
        }
        acts.add(new EditorsAction());
        if (this.canSplit()) {
            acts.add(new SplitAction(true));
        }
        return acts.toArray(new Action[acts.size()]);
    }

    @Override
    public boolean canSplit() {
        return null != this.peer.model && this.peer.model.canSplit();
    }

    void setSuperActions(Action[] acts) {
        this.superActions4Tests = acts;
    }

    public MultiViewHandlerDelegate getMultiViewHandlerDelegate() {
        return this.peer.getMultiViewHandlerDelegate();
    }

    public int getPersistenceType() {
        return this.peer.getPersistenceType();
    }

    protected String preferredID() {
        return this.peer.preferredID();
    }

    public void writeExternal(ObjectOutput out) throws IOException {
        super.writeExternal(out);
        this.peer.peerWriteExternal(out);
    }

    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        super.readExternal(in);
        this.peer.peerReadExternal(in);
    }

    Action[] getDefaultTCActions() {
        return super.getActions();
    }

    @Override
    public MultiViewElementCallback createElementCallback(MultiViewDescription desc) {
        return SpiAccessor.DEFAULT.createCallback(new ActReqObserver(desc));
    }

    public HelpCtx getHelpCtx() {
        return this.peer.getHelpCtx();
    }

    public String toString() {
        return "MultiViewTopComponent[name=" + this.getDisplayName() + ", peer=" + this.peer + "]";
    }

    public UndoRedo getUndoRedo() {
        UndoRedo retValue = this.peer.peerGetUndoRedo();
        if (retValue == null) {
            retValue = super.getUndoRedo();
        }
        return retValue;
    }

    public boolean canClose() {
        return this.peer.canClose();
    }

    public boolean requestFocusInWindow() {
        return this.peer.requestFocusInWindow();
    }

    public void requestFocus() {
        this.peer.requestFocus();
    }

    public TopComponent.SubComponent[] getSubComponents() {
        return MultiViewTopComponent.getSubComponents(this.peer);
    }

    static TopComponent.SubComponent[] getSubComponents(final MultiViewPeer peer) {
        MultiViewModel model = peer.getModel();
        MultiViewPerspective[] perspectives = model.getPerspectives();
        ArrayList<TopComponent.SubComponent> res = new ArrayList<TopComponent.SubComponent>(perspectives.length);
        for (int i = 0; i < perspectives.length; ++i) {
            ContextAwareDescription contextDescr;
            final MultiViewPerspective mvp = perspectives[i];
            MultiViewDescription descr = Accessor.DEFAULT.extractDescription(mvp);
            if (descr instanceof ContextAwareDescription && (contextDescr = (ContextAwareDescription)descr).isSplitDescription() && !peer.tabs.isShowing(descr)) continue;
            Lookup lookup = descr == null ? peer.getLookup() : (model.getElementForDescription(descr, false) == null ? peer.getLookup() : model.getElementForDescription(descr, false).getLookup());
            boolean showing = peer.tabs.isShowing(descr);
            res.add(new TopComponent.SubComponent(Actions.cutAmpersand((String)mvp.getDisplayName()), null, new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    peer.getMultiViewHandlerDelegate().requestActive(mvp);
                }
            }, mvp == model.getSelectedPerspective(), lookup, showing));
        }
        return res.toArray((T[])new TopComponent.SubComponent[res.size()]);
    }

    @Override
    public TopComponent splitComponent(int orientation, int splitPosition) {
        this.peer.peerSplitComponent(orientation, splitPosition);
        return this;
    }

    @Override
    public TopComponent clearSplit(int splitElementToActivate) {
        this.peer.peerClearSplit(splitElementToActivate);
        return this;
    }

    @Override
    public int getSplitOrientation() {
        return this.peer.getSplitOrientation();
    }

    class ActReqObserver
    implements Serializable,
    MultiViewElementCallbackDelegate {
        private static final long serialVersionUID = -3126744916624172415L;
        private MultiViewDescription description;

        ActReqObserver(MultiViewDescription desc) {
            this.description = desc;
        }

        @Override
        public void requestActive() {
            boolean activated = MultiViewTopComponent.this.peer.isActivated();
            if (!activated) {
                MultiViewTopComponent.this.requestActive();
            }
            if (MultiViewTopComponent.this.peer.model.getActiveDescription() != this.description) {
                MultiViewTopComponent.this.peer.tabs.changeActiveManually(this.description);
                if (activated) {
                    MultiViewTopComponent.this.peer.model.getActiveElement().componentActivated();
                }
            }
        }

        @Override
        public void requestVisible() {
            MultiViewTopComponent.this.peer.tabs.changeVisibleManually(this.description);
        }

        @Override
        public Action[] createDefaultActions() {
            return MultiViewTopComponent.this.getDefaultTCActions();
        }

        @Override
        public void updateTitle(String title) {
            MultiViewTopComponent.this.setDisplayName(title);
        }

        public Object writeReplace() throws ObjectStreamException {
            return null;
        }

        public Object readResolve() throws ObjectStreamException {
            return null;
        }

        @Override
        public boolean isSelectedElement() {
            return this.description.equals(MultiViewTopComponent.this.peer.model.getActiveDescription());
        }

        @Override
        public TopComponent getTopComponent() {
            return MultiViewTopComponent.this;
        }
    }

}

