/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.multiview;

import org.openide.windows.TopComponent;

public interface Splitable {
    public TopComponent splitComponent(int var1, int var2);

    public TopComponent clearSplit(int var1);

    public int getSplitOrientation();

    public boolean canSplit();
}

