/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.multiview;

import org.netbeans.core.api.multiview.MultiViewPerspective;
import org.netbeans.core.spi.multiview.MultiViewDescription;

public interface MultiViewHandlerDelegate {
    public MultiViewPerspective[] getDescriptions();

    public MultiViewPerspective getSelectedDescription();

    public void requestActive(MultiViewPerspective var1);

    public void requestVisible(MultiViewPerspective var1);

    public void addMultiViewDescription(MultiViewDescription var1, int var2);

    public void removeMultiViewDescription(MultiViewDescription var1);
}

