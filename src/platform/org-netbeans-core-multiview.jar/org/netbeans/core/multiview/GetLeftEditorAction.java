/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.multiview;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.netbeans.core.api.multiview.MultiViewHandler;
import org.netbeans.core.api.multiview.MultiViewPerspective;
import org.netbeans.core.api.multiview.MultiViews;
import org.netbeans.core.multiview.Accessor;
import org.netbeans.core.multiview.ContextAwareDescription;
import org.netbeans.core.spi.multiview.MultiViewDescription;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class GetLeftEditorAction
extends AbstractAction {
    public GetLeftEditorAction() {
        this.putValue("Name", NbBundle.getMessage(GetLeftEditorAction.class, (String)"GetLeftEditorAction.name"));
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        WindowManager wm = WindowManager.getDefault();
        MultiViewHandler handler = MultiViews.findMultiViewHandler(wm.getRegistry().getActivated());
        if (handler != null) {
            MultiViewPerspective pers = handler.getSelectedPerspective();
            MultiViewPerspective[] all = handler.getPerspectives();
            for (int i = 0; i < all.length; ++i) {
                if (!pers.getDisplayName().equals(all[i].getDisplayName())) continue;
                int newIndex = i != 0 ? i - 1 : all.length - 1;
                MultiViewDescription selectedDescr = Accessor.DEFAULT.extractDescription(pers);
                if (selectedDescr instanceof ContextAwareDescription) {
                    newIndex = ((ContextAwareDescription)selectedDescr).isSplitDescription() ? (i > 1 ? i - 2 : all.length - 1) : (i != 0 ? i - 2 : all.length - 2);
                }
                handler.requestActive(all[newIndex]);
                break;
            }
        } else {
            Utilities.disabledActionBeep();
        }
    }
}

