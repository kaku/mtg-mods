/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Actions
 *  org.openide.awt.DynamicMenuContent
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 *  org.openide.windows.WindowManager
 */
package org.netbeans.core.multiview;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.SwingUtilities;
import org.netbeans.core.api.multiview.MultiViewHandler;
import org.netbeans.core.api.multiview.MultiViewPerspective;
import org.netbeans.core.api.multiview.MultiViews;
import org.netbeans.core.multiview.Accessor;
import org.netbeans.core.multiview.ContextAwareDescription;
import org.netbeans.core.spi.multiview.MultiViewDescription;
import org.openide.awt.Actions;
import org.openide.awt.DynamicMenuContent;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;
import org.openide.util.actions.Presenter;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class EditorsAction
extends AbstractAction
implements Presenter.Menu,
Presenter.Popup {
    public EditorsAction() {
        super(NbBundle.getMessage(EditorsAction.class, (String)"CTL_EditorsAction"));
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        assert (false);
    }

    public JMenuItem getMenuPresenter() {
        UpdatingMenu menu = new UpdatingMenu();
        String label = NbBundle.getMessage(EditorsAction.class, (String)"CTL_EditorsAction");
        Mnemonics.setLocalizedText((AbstractButton)menu, (String)label);
        return menu;
    }

    public JMenuItem getPopupPresenter() {
        UpdatingMenu menu = new UpdatingMenu();
        String label = NbBundle.getMessage(EditorsAction.class, (String)"CTL_EditorsAction");
        Actions.setMenuText((AbstractButton)menu, (String)label, (boolean)false);
        return menu;
    }

    private static final class UpdatingMenu
    extends JMenu
    implements DynamicMenuContent {
        private UpdatingMenu() {
        }

        public JComponent[] synchMenuPresenters(JComponent[] items) {
            return this.getMenuPresenters();
        }

        public JComponent[] getMenuPresenters() {
            assert (SwingUtilities.isEventDispatchThread());
            this.removeAll();
            final TopComponent tc = WindowManager.getDefault().getRegistry().getActivated();
            if (tc != null) {
                this.setEnabled(true);
                MultiViewHandler handler = MultiViews.findMultiViewHandler(tc);
                if (handler != null) {
                    ButtonGroup group = new ButtonGroup();
                    MultiViewPerspective[] pers = handler.getPerspectives();
                    final String[] names = new String[pers.length];
                    for (int i = 0; i < pers.length; ++i) {
                        MultiViewPerspective thisPers = pers[i];
                        JRadioButtonMenuItem item = new JRadioButtonMenuItem();
                        names[i] = thisPers.getDisplayName();
                        Mnemonics.setLocalizedText((AbstractButton)item, (String)thisPers.getDisplayName());
                        item.setActionCommand(thisPers.getDisplayName());
                        item.addActionListener(new ActionListener(){

                            @Override
                            public void actionPerformed(ActionEvent event) {
                                MultiViewHandler handler = MultiViews.findMultiViewHandler(tc);
                                if (handler == null) {
                                    return;
                                }
                                MultiViewPerspective thisPers = null;
                                MultiViewPerspective[] pers = handler.getPerspectives();
                                assert (pers.length == names.length);
                                for (int i = 0; i < pers.length; ++i) {
                                    if (!event.getActionCommand().equals(names[i])) continue;
                                    thisPers = pers[i];
                                    break;
                                }
                                if (thisPers != null) {
                                    handler.requestActive(thisPers);
                                }
                            }
                        });
                        if (thisPers.getDisplayName().equals(handler.getSelectedPerspective().getDisplayName())) {
                            item.setSelected(true);
                        }
                        boolean isSplitDescription = false;
                        MultiViewDescription desc = Accessor.DEFAULT.extractDescription(thisPers);
                        if (desc instanceof ContextAwareDescription) {
                            isSplitDescription = ((ContextAwareDescription)desc).isSplitDescription();
                        }
                        if (isSplitDescription) continue;
                        group.add(item);
                        this.add(item);
                    }
                } else {
                    this.setEnabled(false);
                }
            } else {
                this.setEnabled(false);
            }
            return new JComponent[]{this};
        }

    }

}

