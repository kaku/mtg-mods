/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.multiview;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CTL_EmptyViewName() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_EmptyViewName");
    }

    static String CTL_SplitAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_SplitAction");
    }

    static String CTL_SplitDocumentAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_SplitDocumentAction");
    }

    static String ERR_EmptyView(Object arg0) {
        return NbBundle.getMessage(Bundle.class, (String)"ERR_EmptyView", (Object)arg0);
    }

    static String LBL_ClearAllSplitsDialogMessage() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_ClearAllSplitsDialogMessage");
    }

    static String LBL_ClearAllSplitsDialogTitle() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_ClearAllSplitsDialogTitle");
    }

    static String LBL_ClearSplitAction() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_ClearSplitAction");
    }

    static String LBL_SplitDocumentActionHorizontal() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_SplitDocumentActionHorizontal");
    }

    static String LBL_SplitDocumentActionVertical() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_SplitDocumentActionVertical");
    }

    static String LBL_ValueClearSplit() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_ValueClearSplit");
    }

    static String LBL_ValueSplitHorizontal() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_ValueSplitHorizontal");
    }

    static String LBL_ValueSplitVertical() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_ValueSplitVertical");
    }

    static String MultiViewElement_Spliting_Enabled() {
        return NbBundle.getMessage(Bundle.class, (String)"MultiViewElement.Spliting.Enabled");
    }

    private void Bundle() {
    }
}

