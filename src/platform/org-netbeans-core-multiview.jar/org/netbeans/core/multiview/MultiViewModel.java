/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.multiview;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import org.netbeans.core.api.multiview.MultiViewPerspective;
import org.netbeans.core.multiview.Accessor;
import org.netbeans.core.multiview.ContextAwareDescription;
import org.netbeans.core.multiview.TabsComponent;
import org.netbeans.core.spi.multiview.MultiViewDescription;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.MultiViewElementCallback;

class MultiViewModel {
    private MultiViewDescription currentEditor;
    private Map<MultiViewDescription, MultiViewElement> nestedElements;
    private Map<MultiViewElement, MultiViewElementCallback> nestedCallbacks;
    private Map<MultiViewDescription, MultiViewPerspective> nestedPerspectives;
    private MultiViewDescription[] descriptions;
    private ButtonGroup group;
    private ButtonGroup groupSplit;
    private Collection<MultiViewElement> shownElements;
    private ArrayList<ElementSelectionListener> listeners;
    private ActionRequestObserverFactory observerFactory;
    private boolean freezeButtons = false;

    MultiViewModel(MultiViewDescription[] descs, MultiViewDescription defaultDescr, ActionRequestObserverFactory factory) {
        this(descs, defaultDescr, factory, Collections.emptyMap());
    }

    MultiViewModel(MultiViewDescription[] descs, MultiViewDescription defaultDescr, ActionRequestObserverFactory factory, Map<MultiViewDescription, MultiViewElement> existingElements) {
        this.observerFactory = factory;
        this.nestedElements = new HashMap<MultiViewDescription, MultiViewElement>();
        this.nestedPerspectives = new HashMap<MultiViewDescription, MultiViewPerspective>();
        this.nestedCallbacks = new HashMap<MultiViewElement, MultiViewElementCallback>();
        this.shownElements = new HashSet<MultiViewElement>(descs.length + 3);
        this.descriptions = descs;
        for (int i = 0; i < this.descriptions.length; ++i) {
            MultiViewElement element = existingElements.get(this.descriptions[i]);
            this.nestedElements.put(this.descriptions[i], element);
            this.nestedPerspectives.put(this.descriptions[i], Accessor.DEFAULT.createPerspective(this.descriptions[i]));
            if (element == null) continue;
            MultiViewElementCallback call = factory.createElementCallback(this.descriptions[i]);
            this.nestedCallbacks.put(element, call);
            element.setMultiViewCallback(call);
        }
        this.currentEditor = defaultDescr == null || !this.nestedElements.containsKey(defaultDescr) ? this.descriptions[0] : defaultDescr;
        this.group = new BtnGroup();
        this.groupSplit = new BtnGroup();
    }

    void setActiveDescription(MultiViewDescription description) {
        if (this.currentEditor == description) {
            return;
        }
        MultiViewDescription old = this.currentEditor;
        this.currentEditor = description;
        this.fireSelectionChanged(old, description);
    }

    MultiViewDescription getActiveDescription() {
        return this.currentEditor;
    }

    MultiViewElement getActiveElement() {
        return this.getActiveElement(true);
    }

    MultiViewElement getActiveElement(boolean createIfNotCreatedYet) {
        return this.getElementForDescription(this.currentEditor, createIfNotCreatedYet);
    }

    synchronized Collection getCreatedElements() {
        ArrayList<MultiViewElement> col = new ArrayList<MultiViewElement>(this.nestedElements.size());
        for (Map.Entry<MultiViewDescription, MultiViewElement> entry : this.nestedElements.entrySet()) {
            if (entry.getValue() == null) continue;
            col.add(entry.getValue());
        }
        return col;
    }

    synchronized Map<MultiViewDescription, MultiViewElement> getCreatedElementsMap() {
        return new HashMap<MultiViewDescription, MultiViewElement>(this.nestedElements);
    }

    boolean wasShownBefore(MultiViewElement element) {
        return this.shownElements.contains(element);
    }

    void markAsShown(MultiViewElement element) {
        this.shownElements.add(element);
    }

    void markAsHidden(MultiViewElement element) {
        this.shownElements.remove(element);
    }

    MultiViewDescription[] getDescriptions() {
        return this.descriptions;
    }

    MultiViewPerspective[] getPerspectives() {
        MultiViewPerspective[] toReturn = new MultiViewPerspective[this.descriptions.length];
        for (int i = 0; i < this.descriptions.length; ++i) {
            toReturn[i] = this.nestedPerspectives.get(this.descriptions[i]);
        }
        return toReturn;
    }

    MultiViewPerspective getSelectedPerspective() {
        return this.nestedPerspectives.get(this.getActiveDescription());
    }

    ButtonGroup getButtonGroup() {
        return this.group;
    }

    ButtonGroup getButtonGroupSplit() {
        return this.groupSplit;
    }

    MultiViewElement getElementForDescription(MultiViewDescription description) {
        return this.getElementForDescription(description, true);
    }

    synchronized MultiViewElement getElementForDescription(MultiViewDescription description, boolean create) {
        MultiViewElement element = this.nestedElements.get(description);
        if (element == null && create) {
            element = description.createElement();
            MultiViewElementCallback call = this.observerFactory.createElementCallback(description);
            this.nestedCallbacks.put(element, call);
            element.setMultiViewCallback(call);
            this.nestedElements.put(description, element);
        }
        return element;
    }

    synchronized MultiViewElementCallback getCallbackForElement(MultiViewElement elem) {
        return this.nestedCallbacks.get(elem);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void addElementSelectionListener(ElementSelectionListener listener) {
        if (this.listeners == null) {
            this.listeners = new ArrayList();
        }
        ArrayList<ElementSelectionListener> arrayList = this.listeners;
        synchronized (arrayList) {
            this.listeners.add(listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void removeElementSelectionListener(ElementSelectionListener listener) {
        if (this.listeners == null) {
            this.listeners = new ArrayList();
        }
        ArrayList<ElementSelectionListener> arrayList = this.listeners;
        synchronized (arrayList) {
            this.listeners.remove(listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fireSelectionChanged(MultiViewDescription oldOne, MultiViewDescription newOne) {
        if (this.listeners != null) {
            ArrayList<ElementSelectionListener> arrayList = this.listeners;
            synchronized (arrayList) {
                for (ElementSelectionListener list : this.listeners) {
                    list.selectionChanged(oldOne, newOne);
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void fireActivateCurrent() {
        if (this.listeners != null) {
            ArrayList<ElementSelectionListener> arrayList = this.listeners;
            synchronized (arrayList) {
                for (ElementSelectionListener list : this.listeners) {
                    list.selectionActivatedByButton();
                }
            }
        }
    }

    public String toString() {
        return "current=" + this.currentEditor;
    }

    void setFreezeTabButtons(boolean freeze) {
        this.freezeButtons = freeze;
    }

    boolean canSplit() {
        for (MultiViewDescription mvd : this.getDescriptions()) {
            if (mvd instanceof ContextAwareDescription) continue;
            return false;
        }
        return true;
    }

    private class BtnGroup
    extends ButtonGroup {
        private BtnGroup() {
        }

        @Override
        public void setSelected(ButtonModel m, boolean b) {
            super.setSelected(m, b);
            if (this.getSelection() instanceof TabsComponent.TabsButtonModel && !MultiViewModel.this.freezeButtons) {
                TabsComponent.TabsButtonModel mod = (TabsComponent.TabsButtonModel)m;
                MultiViewDescription desc = mod.getButtonsDescription();
                MultiViewModel.this.setActiveDescription(desc);
            }
        }
    }

    static interface ActionRequestObserverFactory {
        public MultiViewElementCallback createElementCallback(MultiViewDescription var1);
    }

    static interface ElementSelectionListener {
        public void selectionChanged(MultiViewDescription var1, MultiViewDescription var2);

        public void selectionActivatedByButton();
    }

}

