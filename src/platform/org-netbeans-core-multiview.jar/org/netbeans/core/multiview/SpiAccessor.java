/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.multiview;

import org.netbeans.core.multiview.MultiViewElementCallbackDelegate;
import org.netbeans.core.spi.multiview.CloseOperationHandler;
import org.netbeans.core.spi.multiview.MultiViewElementCallback;

public abstract class SpiAccessor {
    protected static SpiAccessor DEFAULT = null;

    public abstract MultiViewElementCallback createCallback(MultiViewElementCallbackDelegate var1);

    public abstract CloseOperationHandler createDefaultCloseHandler();

    public abstract boolean shouldCheckCanCloseAgain(CloseOperationHandler var1);

    static {
        Class<MultiViewElementCallback> c = MultiViewElementCallback.class;
        try {
            Class.forName(c.getName(), true, c.getClassLoader());
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

