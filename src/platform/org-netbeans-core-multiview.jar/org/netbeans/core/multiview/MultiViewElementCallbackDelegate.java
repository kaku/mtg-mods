/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.multiview;

import javax.swing.Action;
import org.openide.windows.TopComponent;

public interface MultiViewElementCallbackDelegate {
    public void requestActive();

    public void requestVisible();

    public Action[] createDefaultActions();

    public void updateTitle(String var1);

    public boolean isSelectedElement();

    public TopComponent getTopComponent();
}

