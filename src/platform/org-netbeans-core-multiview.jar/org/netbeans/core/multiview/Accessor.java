/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.multiview;

import org.netbeans.core.api.multiview.MultiViewHandler;
import org.netbeans.core.api.multiview.MultiViewPerspective;
import org.netbeans.core.multiview.MultiViewHandlerDelegate;
import org.netbeans.core.spi.multiview.MultiViewDescription;

public abstract class Accessor {
    protected static Accessor DEFAULT = null;

    public abstract MultiViewPerspective createPerspective(MultiViewDescription var1);

    public abstract MultiViewHandler createHandler(MultiViewHandlerDelegate var1);

    public abstract MultiViewDescription extractDescription(MultiViewPerspective var1);

    static {
        Class<MultiViewPerspective> c = MultiViewPerspective.class;
        try {
            Class.forName(c.getName(), true, c.getClassLoader());
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

