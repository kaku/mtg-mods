/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.UndoRedo
 *  org.openide.awt.UndoRedo$Manager
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package org.netbeans.core.multiview;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Image;
import java.awt.LayoutManager;
import javax.swing.Action;
import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import org.netbeans.core.multiview.Bundle;
import org.netbeans.core.spi.multiview.CloseOperationState;
import org.netbeans.core.spi.multiview.MultiViewDescription;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.MultiViewElementCallback;
import org.openide.awt.UndoRedo;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

final class EmptyViewDescription
extends JPanel
implements MultiViewDescription,
MultiViewElement {
    private JToolBar toolbar;
    private UndoRedo.Manager undoRedo;
    private JScrollPane jScrollPane1;
    private JTextArea warning;

    public EmptyViewDescription(String mime) {
        this.initComponents();
        this.warning.setText(Bundle.ERR_EmptyView(mime));
    }

    private void initComponents() {
        this.jScrollPane1 = new JScrollPane();
        this.warning = new JTextArea();
        this.warning.setEditable(false);
        this.warning.setColumns(20);
        this.warning.setForeground(new Color(0, 0, 0));
        this.warning.setLineWrap(true);
        this.warning.setRows(5);
        this.warning.setAutoscrolls(false);
        this.warning.setBorder(null);
        this.warning.setEnabled(false);
        this.warning.setOpaque(false);
        this.jScrollPane1.setViewportView(this.warning);
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jScrollPane1, -1, 376, 32767).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jScrollPane1, -1, 276, 32767).addContainerGap()));
    }

    @Override
    public int getPersistenceType() {
        return 2;
    }

    @Override
    public String getDisplayName() {
        return Bundle.CTL_EmptyViewName();
    }

    @Override
    public Image getIcon() {
        return null;
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    public String preferredID() {
        return "empty";
    }

    @Override
    public MultiViewElement createElement() {
        return this;
    }

    @Override
    public JComponent getVisualRepresentation() {
        return this;
    }

    @Override
    public JComponent getToolbarRepresentation() {
        if (this.toolbar == null) {
            this.toolbar = new JToolBar();
        }
        return this.toolbar;
    }

    @Override
    public Action[] getActions() {
        return new Action[0];
    }

    @Override
    public Lookup getLookup() {
        return Lookup.EMPTY;
    }

    @Override
    public void componentOpened() {
    }

    @Override
    public void componentClosed() {
    }

    @Override
    public void componentShowing() {
    }

    @Override
    public void componentHidden() {
    }

    @Override
    public void componentActivated() {
    }

    @Override
    public void componentDeactivated() {
    }

    @Override
    public UndoRedo getUndoRedo() {
        if (this.undoRedo == null) {
            this.undoRedo = new UndoRedo.Manager();
        }
        return this.undoRedo;
    }

    @Override
    public void setMultiViewCallback(MultiViewElementCallback callback) {
    }

    @Override
    public CloseOperationState canCloseElement() {
        return CloseOperationState.STATE_OK;
    }
}

