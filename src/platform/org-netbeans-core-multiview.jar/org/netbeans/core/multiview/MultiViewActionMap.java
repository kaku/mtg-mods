/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.multiview;

import java.awt.Component;
import java.awt.Container;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JComponent;
import org.openide.windows.TopComponent;

final class MultiViewActionMap
extends ActionMap {
    private ActionMap delegate;
    private ActionMap topComponentMap;
    private TopComponent component;
    private boolean preventRecursive = false;
    private Object LOCK = new Object();

    public MultiViewActionMap(TopComponent tc, ActionMap tcMap) {
        this.topComponentMap = tcMap;
        this.component = tc;
    }

    public void setDelegateMap(ActionMap map) {
        this.delegate = map;
    }

    @Override
    public int size() {
        return this.keys().length;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Action get(Object key) {
        Component owner;
        Action found;
        Object a;
        ActionMap m = this.topComponentMap;
        if (m != null && (a = m.get(key)) != null) {
            return a;
        }
        m = this.delegate;
        if (m != null) {
            a = this.LOCK;
            synchronized (a) {
                Action a2;
                if (this.preventRecursive) {
                    this.preventRecursive = false;
                    return null;
                }
                this.preventRecursive = true;
                try {
                    a2 = m.get(key);
                }
                finally {
                    this.preventRecursive = false;
                }
                if (a2 != null) {
                    return a2;
                }
            }
        }
        found = null;
        try {
            this.preventRecursive = true;
            for (owner = java.awt.KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner(); owner != null && owner != this.component; owner = owner.getParent()) {
                if (found != null || !(owner instanceof JComponent) || (m = ((JComponent)owner).getActionMap()) == null) continue;
                if (!(m instanceof MultiViewActionMap) || !((MultiViewActionMap)m).preventRecursive) {
                    found = m.get(key);
                    continue;
                }
                break;
            }
        }
        finally {
            this.preventRecursive = false;
        }
        return owner == this.component ? found : null;
    }

    @Override
    public Object[] allKeys() {
        return this.keys(true);
    }

    @Override
    public Object[] keys() {
        return this.keys(false);
    }

    private Object[] keys(boolean all) {
        HashSet<Object> keys = new HashSet<Object>();
        if (this.delegate != null) {
            Object[] delegateKeys = all ? this.delegate.allKeys() : this.delegate.keys();
            if (null != delegateKeys) {
                keys.addAll(Arrays.asList(delegateKeys));
            }
        }
        if (this.topComponentMap != null) {
            List<Object> l = all ? Arrays.asList(this.topComponentMap.allKeys()) : Arrays.asList(this.topComponentMap.keys());
            keys.addAll(l);
        }
        return keys.toArray();
    }

    @Override
    public void remove(Object key) {
        this.topComponentMap.remove(key);
    }

    @Override
    public void setParent(ActionMap map) {
        this.topComponentMap.setParent(map);
    }

    @Override
    public void clear() {
        this.topComponentMap.clear();
    }

    @Override
    public void put(Object key, Action action) {
        this.topComponentMap.put(key, action);
    }

    @Override
    public ActionMap getParent() {
        return this.topComponentMap.getParent();
    }
}

