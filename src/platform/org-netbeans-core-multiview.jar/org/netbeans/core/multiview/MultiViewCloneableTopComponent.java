/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.UndoRedo
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.CloneableEditorSupport$Pane
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.windows.CloneableTopComponent
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$SubComponent
 */
package org.netbeans.core.multiview;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import org.netbeans.core.multiview.ContextAwareDescription;
import org.netbeans.core.multiview.EditorsAction;
import org.netbeans.core.multiview.MultiViewElementCallbackDelegate;
import org.netbeans.core.multiview.MultiViewHandlerDelegate;
import org.netbeans.core.multiview.MultiViewModel;
import org.netbeans.core.multiview.MultiViewPeer;
import org.netbeans.core.multiview.MultiViewTopComponent;
import org.netbeans.core.multiview.MultiViewTopComponentLookup;
import org.netbeans.core.multiview.SourceCheckDescription;
import org.netbeans.core.multiview.SpiAccessor;
import org.netbeans.core.multiview.SplitAction;
import org.netbeans.core.multiview.Splitable;
import org.netbeans.core.multiview.TabsComponent;
import org.netbeans.core.spi.multiview.CloseOperationHandler;
import org.netbeans.core.spi.multiview.MultiViewDescription;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.MultiViewElementCallback;
import org.netbeans.core.spi.multiview.SourceViewMarker;
import org.openide.awt.UndoRedo;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.windows.CloneableTopComponent;
import org.openide.windows.TopComponent;

public final class MultiViewCloneableTopComponent
extends CloneableTopComponent
implements MultiViewModel.ActionRequestObserverFactory,
CloneableEditorSupport.Pane,
Splitable {
    MultiViewPeer peer;

    public MultiViewCloneableTopComponent() {
        this.peer = new MultiViewPeer((TopComponent)this, this);
        this.peer.initComponents();
        this.setFocusCycleRoot(false);
        this.setName("");
        this.associateLookup(this.peer.getLookup());
    }

    public <T extends Serializable,  extends Lookup.Provider> void setMimeLookup(String mimeType, T context) {
        this.peer.setMimeLookup(mimeType, (Lookup.Provider)context);
    }

    public void setMultiViewDescriptions(MultiViewDescription[] descriptions, MultiViewDescription defaultDesc) {
        this.peer.setMultiViewDescriptions(descriptions, defaultDesc);
    }

    public void setCloseOperationHandler(CloseOperationHandler handler) {
        this.peer.setCloseOperationHandler(handler);
    }

    private void setDeserializedMultiViewDescriptions(MultiViewDescription[] descriptions, MultiViewDescription defaultDesc, Map existingElements) {
        this.peer.setDeserializedMultiViewDescriptions(-1, descriptions, defaultDesc, null, existingElements);
    }

    MultiViewModel getModel() {
        return this.peer.getModel();
    }

    public Lookup getLookup() {
        this.peer.assignLookup((MultiViewTopComponentLookup)super.getLookup());
        return super.getLookup();
    }

    protected void componentClosed() {
        super.componentClosed();
        this.peer.peerComponentClosed();
    }

    protected void componentShowing() {
        super.componentShowing();
        this.peer.peerComponentShowing();
    }

    protected void componentHidden() {
        super.componentHidden();
        this.peer.peerComponentHidden();
    }

    protected void componentDeactivated() {
        super.componentDeactivated();
        this.peer.peerComponentDeactivated();
    }

    protected void componentActivated() {
        super.componentActivated();
        this.peer.peerComponentActivated();
    }

    protected void componentOpened() {
        super.componentOpened();
        this.peer.peerComponentOpened();
    }

    public boolean requestFocusInWindow() {
        return this.peer.requestFocusInWindow();
    }

    public void requestFocus() {
        this.peer.requestFocus();
    }

    public Action[] getActions() {
        Action[] superActions = super.getActions();
        ArrayList<Action> acts = new ArrayList<Action>(Arrays.asList(this.peer.peerGetActions(superActions)));
        if (!acts.isEmpty()) {
            acts.add(null);
        }
        acts.add(new EditorsAction());
        if (this.canSplit()) {
            acts.add(new SplitAction(true));
        }
        return acts.toArray(new Action[acts.size()]);
    }

    @Override
    public boolean canSplit() {
        return null != this.peer.model && this.peer.model.canSplit();
    }

    public MultiViewHandlerDelegate getMultiViewHandlerDelegate() {
        return this.peer.getMultiViewHandlerDelegate();
    }

    public int getPersistenceType() {
        return this.peer.getPersistenceType();
    }

    protected String preferredID() {
        return this.peer.preferredID();
    }

    protected CloneableTopComponent createClonedObject() {
        MultiViewCloneableTopComponent tc = new MultiViewCloneableTopComponent();
        tc.setMultiViewDescriptions(this.peer.model.getDescriptions(), this.getActiveDescription());
        tc.setCloseOperationHandler(this.peer.closeHandler);
        tc.peer.copyMimeContext(this.peer);
        if (this.getSplitOrientation() != -1) {
            tc.splitComponent(this.getSplitOrientation(), -1);
            tc.updateName();
        }
        return tc;
    }

    private MultiViewDescription getActiveDescription() {
        int splitOrientation = this.getSplitOrientation();
        MultiViewDescription activeDescription = this.peer.model.getActiveDescription();
        if (splitOrientation != -1) {
            boolean isSplitDescription = false;
            if (activeDescription instanceof ContextAwareDescription) {
                isSplitDescription = ((ContextAwareDescription)activeDescription).isSplitDescription();
            }
            if (isSplitDescription) {
                for (MultiViewDescription descr : this.peer.model.getDescriptions()) {
                    if (!descr.getDisplayName().equals(activeDescription.getDisplayName())) continue;
                    activeDescription = descr;
                    break;
                }
            }
        }
        return activeDescription;
    }

    @Override
    public TopComponent splitComponent(int orientation, int splitPosition) {
        this.peer.peerSplitComponent(orientation, splitPosition);
        return this;
    }

    @Override
    public TopComponent clearSplit(int splitElementToActivate) {
        this.peer.peerClearSplit(splitElementToActivate);
        return this;
    }

    @Override
    public int getSplitOrientation() {
        return this.peer.getSplitOrientation();
    }

    public void writeExternal(ObjectOutput out) throws IOException {
        super.writeExternal(out);
        this.peer.peerWriteExternal(out);
    }

    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        super.readExternal(in);
        this.peer.peerReadExternal(in);
    }

    Action[] getDefaultTCActions() {
        return super.getActions();
    }

    @Override
    public MultiViewElementCallback createElementCallback(MultiViewDescription desc) {
        return SpiAccessor.DEFAULT.createCallback(new ActReqObserver(desc));
    }

    public CloneableTopComponent getComponent() {
        return this;
    }

    public JEditorPane getEditorPane() {
        if (this.peer == null || this.peer.model == null) {
            return null;
        }
        MultiViewElement paneEl = this.findPaneElement();
        if (paneEl != null) {
            CloneableEditorSupport.Pane pane = (CloneableEditorSupport.Pane)paneEl.getVisualRepresentation();
            return pane.getEditorPane();
        }
        return null;
    }

    private MultiViewElement findPaneElement() {
        MultiViewDescription[] descs;
        MultiViewElement el2 = this.peer.model.getActiveElement(false);
        if (el2 != null && el2.getVisualRepresentation() instanceof CloneableEditorSupport.Pane) {
            return el2;
        }
        Collection col = this.peer.model.getCreatedElements();
        for (MultiViewElement el2 : col) {
            if (!(el2.getVisualRepresentation() instanceof CloneableEditorSupport.Pane)) continue;
            return el2;
        }
        for (MultiViewDescription desc : descs = this.peer.model.getDescriptions()) {
            if (!MultiViewCloneableTopComponent.isSourceView(desc)) continue;
            el2 = this.peer.model.getElementForDescription(desc);
            if (el2.getVisualRepresentation() instanceof CloneableEditorSupport.Pane) {
                return el2;
            }
            Logger.getLogger(this.getClass().getName()).info("MultiViewDescription " + desc.getDisplayName() + "(" + desc.getClass() + ") claimed to contain sources, but it's MutliViewElement.getVisualRepresentation() didn't return a valid CloeanbleEditorSupport.Pane instance.");
        }
        return null;
    }

    public HelpCtx getHelpCtx() {
        return this.peer.getHelpCtx();
    }

    public String toString() {
        return "MVCTC[name=" + this.getDisplayName() + ", peer=" + this.peer + "]";
    }

    public UndoRedo getUndoRedo() {
        UndoRedo retValue = this.peer.peerGetUndoRedo();
        if (retValue == null) {
            retValue = super.getUndoRedo();
        }
        return retValue;
    }

    protected boolean closeLast() {
        return this.peer.canClose();
    }

    public void updateName() {
        if (this.peer != null) {
            if (SwingUtilities.isEventDispatchThread()) {
                this.peer.updateName();
            } else {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        MultiViewCloneableTopComponent.this.peer.updateName();
                    }
                });
            }
        }
    }

    public void ensureVisible() {
        MultiViewElement paneEl = this.findPaneElement();
        if (paneEl != null) {
            this.open();
            MultiViewElementCallback call = this.peer.getModel().getCallbackForElement(paneEl);
            call.requestVisible();
        }
    }

    static boolean isSourceView(MultiViewDescription desc) {
        if (desc instanceof SourceViewMarker) {
            return true;
        }
        if (desc instanceof SourceCheckDescription) {
            return ((SourceCheckDescription)desc).isSourceView();
        }
        return false;
    }

    public TopComponent.SubComponent[] getSubComponents() {
        return MultiViewTopComponent.getSubComponents(this.peer);
    }

    class ActReqObserver
    implements Serializable,
    MultiViewElementCallbackDelegate {
        private static final long serialVersionUID = -3126744916624172415L;
        private MultiViewDescription description;

        ActReqObserver(MultiViewDescription desc) {
            this.description = desc;
        }

        @Override
        public void requestActive() {
            boolean activated = MultiViewCloneableTopComponent.this.peer.isActivated();
            if (!activated) {
                MultiViewCloneableTopComponent.this.requestActive();
            }
            if (MultiViewCloneableTopComponent.this.peer.model.getActiveDescription() != this.description) {
                if (activated) {
                    MultiViewCloneableTopComponent.this.peer.model.getActiveElement().componentDeactivated();
                }
                MultiViewCloneableTopComponent.this.peer.tabs.changeActiveManually(this.description);
                if (activated) {
                    MultiViewCloneableTopComponent.this.peer.model.getActiveElement().componentActivated();
                }
            }
        }

        @Override
        public void requestVisible() {
            MultiViewCloneableTopComponent.this.peer.tabs.changeVisibleManually(this.description);
        }

        @Override
        public Action[] createDefaultActions() {
            return MultiViewCloneableTopComponent.this.getDefaultTCActions();
        }

        @Override
        public void updateTitle(String title) {
            MultiViewCloneableTopComponent.this.setDisplayName(title);
        }

        public Object writeReplace() throws ObjectStreamException {
            return null;
        }

        public Object readResolve() throws ObjectStreamException {
            return null;
        }

        @Override
        public boolean isSelectedElement() {
            return this.description.equals(MultiViewCloneableTopComponent.this.peer.model.getActiveDescription());
        }

        @Override
        public TopComponent getTopComponent() {
            return MultiViewCloneableTopComponent.this;
        }
    }

}

