/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.CloneableEditorSupport$Pane
 *  org.openide.util.Lookup
 */
package org.netbeans.core.multiview;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.FileObject;
import org.netbeans.core.multiview.ContextAwareDescription;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.MultiViewFactory;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.Lookup;

@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
public class MultiViewProcessor
extends LayerGeneratingProcessor {
    public Set<String> getSupportedAnnotationTypes() {
        return new HashSet<String>(Arrays.asList(MultiViewElement.Registration.class.getCanonicalName()));
    }

    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws LayerGenerationException {
        if (roundEnv.processingOver()) {
            return false;
        }
        TypeMirror pane = null;
        TypeElement typeElement = this.processingEnv.getElementUtils().getTypeElement(CloneableEditorSupport.Pane.class.getCanonicalName());
        if (typeElement != null) {
            pane = typeElement.asType();
        }
        for (Element e : roundEnv.getElementsAnnotatedWith(MultiViewElement.Registration.class)) {
            MultiViewElement.Registration mvr = e.getAnnotation(MultiViewElement.Registration.class);
            if (mvr.mimeType().length == 0) {
                throw new LayerGenerationException("You must specify mimeType", e, this.processingEnv, (Annotation)mvr, "mimeType");
            }
            TypeMirror[] exprType = new TypeMirror[1];
            String[] binAndMethodNames = this.findDefinition(e, exprType, mvr);
            String fileBaseName = binAndMethodNames[0].replace('.', '-');
            if (binAndMethodNames[1] != null) {
                fileBaseName = fileBaseName + "-" + binAndMethodNames[1];
            }
            for (String type : mvr.mimeType()) {
                LayerBuilder builder = this.layer(new Element[]{e});
                LayerBuilder.File f = builder.file("Editors/" + (type.equals("") ? "" : new StringBuilder().append(type).append('/').toString()) + "MultiView/" + fileBaseName + ".instance");
                f.methodvalue("instanceCreate", MultiViewFactory.class.getName(), "createMultiViewDescription");
                f.stringvalue("instanceClass", ContextAwareDescription.class.getName());
                f.stringvalue("class", binAndMethodNames[0]);
                f.bundlevalue("displayName", mvr.displayName(), (Annotation)mvr, "displayName");
                if (!mvr.iconBase().isEmpty()) {
                    builder.validateResource(mvr.iconBase(), e, (Annotation)mvr, "iconBase", true);
                    f.stringvalue("iconBase", mvr.iconBase());
                }
                f.stringvalue("preferredID", mvr.preferredID());
                f.intvalue("persistenceType", mvr.persistenceType());
                f.position(mvr.position());
                if (binAndMethodNames[1] != null) {
                    f.stringvalue("method", binAndMethodNames[1]);
                }
                if (pane != null && this.processingEnv.getTypeUtils().isAssignable(exprType[0], pane)) {
                    f.boolvalue("sourceview", true);
                }
                f.write();
            }
        }
        return true;
    }

    private String[] findDefinition(Element e, TypeMirror[] type, MultiViewElement.Registration mvr) throws LayerGenerationException {
        TypeElement lkpElem = this.processingEnv.getElementUtils().getTypeElement(Lookup.class.getCanonicalName());
        TypeMirror lkp = lkpElem == null ? null : lkpElem.asType();
        TypeMirror mve = this.processingEnv.getElementUtils().getTypeElement(MultiViewElement.class.getName()).asType();
        if (e.getKind() == ElementKind.CLASS) {
            TypeElement clazz = (TypeElement)e;
            if (!this.processingEnv.getTypeUtils().isAssignable(clazz.asType(), mve)) {
                throw new LayerGenerationException("Not assignable to " + mve, e, this.processingEnv, (Annotation)mvr);
            }
            boolean constructorCount = false;
            for (ExecutableElement constructor : ElementFilter.constructorsIn(clazz.getEnclosedElements())) {
                List<? extends VariableElement> params;
                if (!constructor.getModifiers().contains((Object)Modifier.PUBLIC) || (params = constructor.getParameters()).size() > 1) continue;
                for (VariableElement param : params) {
                    if (!param.asType().equals(lkp)) break;
                }
            }
            if (!clazz.getModifiers().contains((Object)Modifier.PUBLIC)) {
                throw new LayerGenerationException("Class must be public", e, this.processingEnv, (Annotation)mvr);
            }
            type[0] = e.asType();
            return new String[]{this.processingEnv.getElementUtils().getBinaryName(clazz).toString(), null};
        }
        ExecutableElement meth = (ExecutableElement)e;
        if (!this.processingEnv.getTypeUtils().isAssignable(meth.getReturnType(), mve)) {
            throw new LayerGenerationException("Not assignable to " + mve, e, this.processingEnv, (Annotation)mvr);
        }
        if (!meth.getModifiers().contains((Object)Modifier.PUBLIC)) {
            throw new LayerGenerationException("Method must be public", e, this.processingEnv, (Annotation)mvr);
        }
        if (!meth.getModifiers().contains((Object)Modifier.STATIC)) {
            throw new LayerGenerationException("Method must be static", e, this.processingEnv, (Annotation)mvr);
        }
        List<? extends VariableElement> params = meth.getParameters();
        if (params.size() > 1) {
            throw new LayerGenerationException("Method must take at most one parameter", e, this.processingEnv, (Annotation)mvr);
        }
        for (VariableElement param : params) {
            if (param.asType().equals(lkp)) continue;
            throw new LayerGenerationException("Method parameter may only be Lookup", e, this.processingEnv, (Annotation)mvr);
        }
        if (!meth.getEnclosingElement().getModifiers().contains((Object)Modifier.PUBLIC)) {
            throw new LayerGenerationException("Class must be public", e, this.processingEnv, (Annotation)mvr);
        }
        type[0] = meth.getReturnType();
        return new String[]{this.processingEnv.getElementUtils().getBinaryName((TypeElement)meth.getEnclosingElement()).toString(), meth.getSimpleName().toString()};
    }
}

