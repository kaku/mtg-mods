/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.openide.awt.UndoRedo
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.CloneableEditorSupport$Pane
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.NbPreferences
 *  org.openide.util.WeakListeners
 *  org.openide.windows.CloneableTopComponent
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Cloneable
 */
package org.netbeans.core.multiview;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.OptionalDataException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.core.api.multiview.MultiViewPerspective;
import org.netbeans.core.multiview.Accessor;
import org.netbeans.core.multiview.ContextAwareDescription;
import org.netbeans.core.multiview.EmptyViewDescription;
import org.netbeans.core.multiview.GetLeftEditorAction;
import org.netbeans.core.multiview.GetRightEditorAction;
import org.netbeans.core.multiview.MultiViewActionMap;
import org.netbeans.core.multiview.MultiViewCloneableTopComponent;
import org.netbeans.core.multiview.MultiViewHandlerDelegate;
import org.netbeans.core.multiview.MultiViewModel;
import org.netbeans.core.multiview.MultiViewTopComponent;
import org.netbeans.core.multiview.MultiViewTopComponentLookup;
import org.netbeans.core.multiview.SpiAccessor;
import org.netbeans.core.multiview.Splitable;
import org.netbeans.core.multiview.TabsComponent;
import org.netbeans.core.spi.multiview.CloseOperationHandler;
import org.netbeans.core.spi.multiview.CloseOperationState;
import org.netbeans.core.spi.multiview.MultiViewDescription;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.openide.awt.UndoRedo;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;
import org.openide.util.WeakListeners;
import org.openide.windows.CloneableTopComponent;
import org.openide.windows.TopComponent;

public final class MultiViewPeer
implements PropertyChangeListener {
    static final String MULTIVIEW_ID = "MultiView-";
    private static final String TOOLBAR_VISIBLE_PROP = "toolbarVisible";
    private static final Preferences editorSettingsPreferences;
    private Lookup.Provider context;
    private String mimeType;
    MultiViewModel model;
    TabsComponent tabs;
    SelectionListener selListener;
    CloseOperationHandler closeHandler;
    transient MultiViewTopComponentLookup lookup;
    TopComponent peer;
    private MultiViewModel.ActionRequestObserverFactory factory;
    private MultiViewActionMap delegatingMap;
    private boolean activated = false;
    private final PreferenceChangeListener editorSettingsListener;
    private final PropertyChangeListener propListener;
    private DelegateUndoRedo delegateUndoRedo;
    private int splitOrientation;
    private int initialSplitOrientation;
    private MultiViewDescription initialSplitDescription;

    MultiViewPeer(TopComponent pr, MultiViewModel.ActionRequestObserverFactory fact) {
        this.editorSettingsListener = new PreferenceChangeListenerImpl();
        this.splitOrientation = -1;
        this.initialSplitOrientation = -1;
        this.selListener = new SelectionListener();
        this.peer = pr;
        this.factory = fact;
        this.delegateUndoRedo = new DelegateUndoRedo();
        this.propListener = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)null);
    }

    void copyMimeContext(MultiViewPeer other) {
        this.context = other.context;
        this.mimeType = other.mimeType;
    }

    public void setMimeLookup(String mimeType, Lookup.Provider context) {
        this.context = context;
        this.mimeType = mimeType;
        ArrayList<MultiViewDescription> arr = new ArrayList<MultiViewDescription>();
        Lookup lkp = MimeLookup.getLookup((String)mimeType);
        Iterator i$ = lkp.lookupAll(ContextAwareDescription.class).iterator();
        while (i$.hasNext()) {
            ContextAwareDescription d = (ContextAwareDescription)i$.next();
            d = d.createContextAwareDescription(context.getLookup(), false);
            arr.add(d);
            d = d.createContextAwareDescription(context.getLookup(), true);
            arr.add(d);
        }
        if (arr.isEmpty()) {
            arr.add(new EmptyViewDescription(mimeType));
        }
        if (this.model != null) {
            this.model.removeElementSelectionListener(this.selListener);
        }
        this.model = new MultiViewModel(arr.toArray(new MultiViewDescription[0]), (MultiViewDescription)arr.get(0), this.factory);
        this.model.addElementSelectionListener(this.selListener);
        this.tabs.setModel(this.model);
        CloseOperationHandler h = (CloseOperationHandler)lkp.lookup(CloseOperationHandler.class);
        if (h == null) {
            h = SpiAccessor.DEFAULT.createDefaultCloseHandler();
        }
        this.closeHandler = h;
    }

    public void setMultiViewDescriptions(MultiViewDescription[] descriptions, MultiViewDescription defaultDesc) {
        assert (this.context == null);
        this._setMultiViewDescriptions(descriptions, defaultDesc);
    }

    private void _setMultiViewDescriptions(MultiViewDescription[] descriptions, MultiViewDescription defaultDesc) {
        Map createdElements = Collections.emptyMap();
        if (this.model != null) {
            this.model.removeElementSelectionListener(this.selListener);
            createdElements = this.model.getCreatedElementsMap();
        }
        this.model = new MultiViewModel(descriptions, defaultDesc, this.factory, createdElements);
        this.model.addElementSelectionListener(this.selListener);
        this.tabs.setModel(this.model);
    }

    public void setCloseOperationHandler(CloseOperationHandler handler) {
        assert (this.context == null);
        this.closeHandler = handler;
    }

    void setDeserializedMultiViewDescriptions(int splitOrientation, MultiViewDescription[] descriptions, MultiViewDescription defaultDesc, MultiViewDescription defaultDescSplit, Map<MultiViewDescription, MultiViewElement> existingElements) {
        if (this.model != null) {
            this.model.removeElementSelectionListener(this.selListener);
        }
        if (splitOrientation != -1) {
            defaultDescSplit = defaultDescSplit.getDisplayName().startsWith("&Design") ? descriptions[1] : defaultDescSplit;
        }
        this.model = new MultiViewModel(descriptions, defaultDesc, this.factory, existingElements);
        this.model.addElementSelectionListener(this.selListener);
        this.tabs.setModel(this.model);
        this.initialSplitOrientation = splitOrientation;
        this.initialSplitDescription = defaultDescSplit;
    }

    MultiViewModel getModel() {
        return this.model;
    }

    void initComponents() {
        this.initActionMap();
        this.peer.setLayout((LayoutManager)new BorderLayout());
        this.tabs = new TabsComponent(this.isToolbarVisible());
        this.peer.add((Component)this.tabs);
        ActionMap map = this.peer.getActionMap();
        AccessTogglesAction act = new AccessTogglesAction();
        map.put("NextViewAction", new GetRightEditorAction());
        map.put("PreviousViewAction", new GetLeftEditorAction());
        map.put("accesstoggles", act);
        InputMap input = this.peer.getInputMap(1);
        KeyStroke stroke = KeyStroke.getKeyStroke("control F10");
        input.put(stroke, "accesstoggles");
        input = this.peer.getInputMap(0);
        input.put(stroke, "accesstoggles");
        this.peer.putClientProperty((Object)"MultiViewBorderHack.topOffset", (Object)new Integer(this.tabs.getPreferredSize().height - 1));
    }

    private void assignLookup(MultiViewElement el, MultiViewTopComponentLookup lkp) {
        Lookup elementLookup = el.getLookup();
        assert (null != elementLookup);
        lkp.setElementLookup(elementLookup);
    }

    private void assignLookup(MultiViewElement el) {
        this.assignLookup(el, (MultiViewTopComponentLookup)this.peer.getLookup());
    }

    final void assignLookup(MultiViewTopComponentLookup lkp) {
        if (lkp.isInitialized()) {
            return;
        }
        MultiViewElement el = this.getModel().getActiveElement();
        if (el != null) {
            this.assignLookup(el, lkp);
        }
    }

    private void initActionMap() {
        this.delegatingMap = new MultiViewActionMap(this.peer, new ActionMap());
        if (this.peer instanceof TopComponent.Cloneable) {
            this.delegatingMap.put("cloneWindow", new AbstractAction(){

                @Override
                public void actionPerformed(ActionEvent evt) {
                    TopComponent cloned = ((TopComponent.Cloneable)MultiViewPeer.this.peer).cloneComponent();
                    cloned.open();
                    cloned.requestActive();
                }
            });
        }
        if (this.peer instanceof MultiViewTopComponent || this.peer instanceof MultiViewCloneableTopComponent) {
            this.delegatingMap.put("splitWindowHorizantally", new AbstractAction(){

                @Override
                public void actionPerformed(ActionEvent evt) {
                    if (MultiViewPeer.this.peer instanceof Splitable) {
                        TopComponent split = ((Splitable)MultiViewPeer.this.peer).splitComponent(1, -1);
                        split.open();
                        split.requestActive();
                    }
                }
            });
            this.delegatingMap.put("splitWindowVertically", new AbstractAction(){

                @Override
                public void actionPerformed(ActionEvent evt) {
                    if (MultiViewPeer.this.peer instanceof Splitable) {
                        TopComponent split = ((Splitable)MultiViewPeer.this.peer).splitComponent(0, -1);
                        split.open();
                        split.requestActive();
                    }
                }
            });
            this.delegatingMap.put("clearSplit", new AbstractAction(){

                @Override
                public void actionPerformed(ActionEvent evt) {
                    if (MultiViewPeer.this.peer instanceof Splitable) {
                        TopComponent original = ((Splitable)MultiViewPeer.this.peer).clearSplit(-1);
                        original.open();
                        original.requestActive();
                    }
                }
            });
        }
        this.delegatingMap.put("closeWindow", new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                MultiViewPeer.this.peer.close();
            }
        });
        this.peer.setActionMap((ActionMap)this.delegatingMap);
    }

    void peerComponentClosed() {
        for (MultiViewElement el : this.model.getCreatedElements()) {
            this.model.markAsHidden(el);
            el.componentClosed();
        }
        this.tabs.peerComponentClosed();
    }

    void peerComponentShowing() {
        MultiViewElement el = this.model.getActiveElement();
        el.componentShowing();
        this.delegatingMap.setDelegateMap(el.getVisualRepresentation().getActionMap());
        this.assignLookup(el);
        JComponent jc = el.getToolbarRepresentation();
        assert (jc != null);
        jc.setOpaque(false);
        boolean isSplitDescription = false;
        MultiViewDescription desc = this.model.getActiveDescription();
        if (desc instanceof ContextAwareDescription) {
            isSplitDescription = ((ContextAwareDescription)desc).isSplitDescription();
        }
        this.tabs.setInnerToolBar(jc, isSplitDescription);
        this.tabs.setToolbarBarVisible(this.isToolbarVisible());
        if (editorSettingsPreferences != null) {
            editorSettingsPreferences.addPreferenceChangeListener(this.editorSettingsListener);
        }
        if (this.initialSplitOrientation != -1) {
            this.splitOrientation = this.initialSplitOrientation;
            this.tabs.peerSplitComponent(this.splitOrientation, this, this.getModel().getActiveDescription(), this.initialSplitDescription, -1);
            this.initialSplitDescription = null;
            this.initialSplitOrientation = -1;
        }
    }

    void peerComponentHidden() {
        this.model.getActiveElement().componentHidden();
        if (editorSettingsPreferences != null) {
            editorSettingsPreferences.removePreferenceChangeListener(this.editorSettingsListener);
        }
    }

    void peerComponentDeactivated() {
        this.activated = false;
        this.model.getActiveElement().componentDeactivated();
    }

    boolean isActivated() {
        return this.activated;
    }

    void peerComponentActivated() {
        this.activated = true;
        this.model.getActiveElement().componentActivated();
    }

    void peerComponentOpened() {
        this.showCurrentElement(true);
        this.tabs.setToolbarBarVisible(this.isToolbarVisible());
        this.addPropertyChangeListeners();
    }

    private void addPropertyChangeListeners() {
        if (null != this.model) {
            for (MultiViewDescription mvd : this.model.getDescriptions()) {
                CloneableTopComponent tc;
                CloneableEditorSupport.Pane pane;
                MultiViewElement el;
                if (mvd instanceof ContextAwareDescription && ((ContextAwareDescription)mvd).isSplitDescription() || (el = this.model.getElementForDescription(mvd, false)) == null || !(el.getVisualRepresentation() instanceof CloneableEditorSupport.Pane) || Arrays.asList((tc = (pane = (CloneableEditorSupport.Pane)el.getVisualRepresentation()).getComponent()).getPropertyChangeListeners()).contains(this.propListener)) continue;
                tc.addPropertyChangeListener(this.propListener);
            }
        }
    }

    void peerSplitComponent(int orientation, int splitPosition) {
        this.splitOrientation = orientation;
        this.tabs.peerSplitComponent(orientation, this, null, null, splitPosition);
    }

    void peerClearSplit(int elementToActivate) {
        this.tabs.peerClearSplit(elementToActivate);
        this.showCurrentElement();
        this.model.fireActivateCurrent();
        this.splitOrientation = -1;
    }

    int getSplitOrientation() {
        return this.splitOrientation;
    }

    boolean requestFocusInWindow() {
        if (this.model == null) {
            return false;
        }
        return this.model.getActiveElement().getVisualRepresentation().requestFocusInWindow();
    }

    void requestFocus() {
        if (this.model != null) {
            this.model.getActiveElement().getVisualRepresentation().requestFocus();
        }
    }

    void hideElement(MultiViewDescription desc) {
        if (desc != null && this.splitOrientation != -1) {
            MultiViewDescription topDesc = this.tabs.getTopComponentDescription();
            MultiViewDescription bottomDesc = this.tabs.getBottomComponentDescription();
            if (!(!this.tabs.isHiddenTriggeredByMultiViewButton() || topDesc.getDisplayName().equals(desc.getDisplayName()) && bottomDesc.getDisplayName().equals(desc.getDisplayName()))) {
                MultiViewElement el = this.model.getElementForDescription(desc);
                el.componentHidden();
            }
            return;
        }
        if (desc != null) {
            MultiViewElement el = this.model.getElementForDescription(desc);
            el.componentHidden();
        }
    }

    void showCurrentElement() {
        this.showCurrentElement(false);
    }

    private void showCurrentElement(boolean calledFromComponentOpened) {
        MultiViewElement el = this.model.getActiveElement();
        MultiViewDescription desc = this.model.getActiveDescription();
        boolean isSplitDescription = false;
        if (desc instanceof ContextAwareDescription) {
            isSplitDescription = ((ContextAwareDescription)desc).isSplitDescription();
        }
        this.tabs.switchToCard(el, desc.getDisplayName(), isSplitDescription);
        if (null == this.peer.getIcon()) {
            MultiViewDescription[] descriptions;
            Image icon = desc.getIcon();
            if (null == icon && null != (descriptions = this.model.getDescriptions()) && descriptions.length > 0) {
                icon = descriptions[0].getIcon();
            }
            this.peer.setIcon(icon);
        }
        if ((this.peer.isOpened() || calledFromComponentOpened) && !this.model.wasShownBefore(el)) {
            el.componentOpened();
            this.model.markAsShown(el);
        }
        if (!calledFromComponentOpened) {
            if (this.peer.isVisible()) {
                el.componentShowing();
            }
            this.delegatingMap.setDelegateMap(el.getVisualRepresentation().getActionMap());
            this.assignLookup(el);
            if (this.peer.isVisible()) {
                this.tabs.setInnerToolBar(el.getToolbarRepresentation(), isSplitDescription);
                this.tabs.setToolbarBarVisible(this.isToolbarVisible());
            }
        }
    }

    Action[] peerGetActions(Action[] superActs) {
        Action[] acts = this.model.getActiveElement().getActions();
        Action[] superActions = new Action[superActs.length];
        System.arraycopy(superActs, 0, superActions, 0, superActs.length);
        block0 : for (int i = 0; i < acts.length; ++i) {
            Action act = acts[i];
            for (int j = 0; j < superActions.length; ++j) {
                Action superact = superActions[j];
                if (act == null && superact == null) continue block0;
                if (superact == null || act == null || !superact.getClass().equals(act.getClass())) continue;
                acts[i] = superActions[j];
                superActions[j] = null;
                continue block0;
            }
        }
        return acts;
    }

    MultiViewHandlerDelegate getMultiViewHandlerDelegate() {
        return new MVTCHandler();
    }

    int getPersistenceType() {
        int type = 2;
        if (null != this.model) {
            MultiViewDescription[] descs = this.model.getDescriptions();
            for (int i = 0; i < descs.length; ++i) {
                if (this.context == null && !(descs[i] instanceof Serializable)) {
                    Logger.getLogger(MultiViewTopComponent.class.getName()).warning("The MultiviewDescription instance " + descs[i].getClass() + " is not serializable. Cannot persist TopComponent.");
                    type = 2;
                    break;
                }
                if (descs[i].getPersistenceType() == 0) {
                    type = descs[i].getPersistenceType();
                }
                if (descs[i].getPersistenceType() != 1 || type == 0) continue;
                type = descs[i].getPersistenceType();
            }
        }
        return type;
    }

    String preferredID() {
        StringBuffer retValue = new StringBuffer("MultiView-");
        assert (this.model != null);
        MultiViewDescription[] descs = this.model.getDescriptions();
        for (int i = 0; i < descs.length; ++i) {
            retValue.append(descs[i].preferredID());
            retValue.append("|");
        }
        return retValue.toString();
    }

    void peerWriteExternal(ObjectOutput out) throws IOException {
        boolean fromMime;
        if (this.context != null) {
            out.writeObject(this.mimeType);
            out.writeObject((Object)this.context);
            fromMime = true;
        } else {
            if (this.closeHandler != null) {
                if (this.closeHandler instanceof Serializable) {
                    out.writeObject(this.closeHandler);
                } else {
                    Logger.getAnonymousLogger().info("The CloseOperationHandler isn not serializable. MultiView component id=" + this.preferredID());
                }
            }
            fromMime = false;
        }
        MultiViewDescription[] descs = this.model.getDescriptions();
        MultiViewDescription curr = this.tabs.getTopComponentDescription();
        MultiViewDescription currSplit = this.tabs.getBottomComponentDescription();
        int currIndex = 0;
        int currIndexSplit = 0;
        for (int i = 0; i < descs.length; ++i) {
            MultiViewElement elem;
            if (descs[i] instanceof RuntimeMultiViewDescription) continue;
            if (!fromMime) {
                out.writeObject(descs[i]);
            } else {
                out.writeObject(descs[i].preferredID());
            }
            if (descs[i].getPersistenceType() != 2 && (elem = this.model.getElementForDescription(descs[i], false)) != null && elem instanceof Serializable) {
                out.writeObject(elem);
            }
            if (descs[i] == curr) {
                currIndex = i;
            }
            if (descs[i] != currSplit) continue;
            currIndexSplit = i;
        }
        out.writeObject(new Integer(currIndex));
        out.writeObject(new Integer(currIndexSplit));
        out.writeObject(new Integer(this.splitOrientation));
        String htmlDisplayName = this.peer.getHtmlDisplayName();
        if (null != htmlDisplayName) {
            out.writeObject(htmlDisplayName);
        }
    }

    void peerReadExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        HashMap<MultiViewDescription, MultiViewElement> map;
        int currentSplit;
        CloseOperationHandler close;
        ArrayList<MultiViewDescription> descList;
        int splitOrient;
        int current;
        descList = new ArrayList<MultiViewDescription>();
        map = new HashMap<MultiViewDescription, MultiViewElement>();
        current = 0;
        currentSplit = 0;
        splitOrient = -1;
        close = null;
        try {
            Integer integ;
            int counting = 0;
            int intCounting = 0;
            MultiViewDescription lastDescription = null;
            do {
                Object obj;
                if ((obj = in.readObject()) instanceof String && counting++ == 0) {
                    Lookup.Provider lp = (Lookup.Provider)in.readObject();
                    this.setMimeLookup((String)obj, lp);
                    descList.addAll(Arrays.asList(this.model.getDescriptions()));
                    continue;
                }
                if (obj instanceof MultiViewDescription) {
                    lastDescription = (MultiViewDescription)obj;
                    descList.add(lastDescription);
                } else if (obj instanceof String) {
                    boolean match = false;
                    for (MultiViewDescription md : descList) {
                        if (!md.preferredID().equals(obj)) continue;
                        lastDescription = md;
                        match = true;
                        break;
                    }
                    if (!match) {
                        throw new IOException("Cannot find multiview description for id \"" + obj + "\". Maybe some module(s) is not installed or activated.");
                    }
                } else if (obj instanceof MultiViewElement) {
                    assert (lastDescription != null);
                    map.put(lastDescription, (MultiViewElement)obj);
                    lastDescription = null;
                } else if (obj instanceof Integer) {
                    integ = (Integer)obj;
                    if (intCounting == 0) {
                        ++intCounting;
                        current = integ;
                    } else if (intCounting == 1) {
                        ++intCounting;
                        currentSplit = integ;
                    } else if (intCounting == 2) break;
                }
                if (!(obj instanceof CloseOperationHandler)) continue;
                close = (CloseOperationHandler)obj;
            } while (true);
            splitOrient = integ;
            try {
                Object htmlDisplayName = in.readObject();
                if (htmlDisplayName instanceof String) {
                    this.peer.setHtmlDisplayName((String)htmlDisplayName);
                }
            }
            catch (OptionalDataException odE) {
                if (!odE.eof) {
                    throw odE;
                }
            }
        }
        catch (IOException exc) {
            if (this.context == null) {
                if (close == null) {
                    close = SpiAccessor.DEFAULT.createDefaultCloseHandler();
                }
                this.setCloseOperationHandler(close);
            }
            if (descList.size() > 0) {
                MultiViewDescription[] descs = new MultiViewDescription[descList.size()];
                descs = descList.toArray(descs);
                MultiViewDescription currDesc = descs[0];
                MultiViewDescription currDescSplit = descs[1];
                map.clear();
                this.setDeserializedMultiViewDescriptions(1, descs, currDesc, currDescSplit, map);
            }
            throw exc;
        }
        if (this.context == null) {
            if (close == null) {
                close = SpiAccessor.DEFAULT.createDefaultCloseHandler();
            }
            this.setCloseOperationHandler(close);
        }
        MultiViewDescription[] descs = new MultiViewDescription[descList.size()];
        descs = descList.toArray(descs);
        MultiViewDescription currDesc = descs[current];
        MultiViewDescription currDescSplit = descs[currentSplit];
        this.setDeserializedMultiViewDescriptions(splitOrient, descs, currDesc, currDescSplit, map);
    }

    private Action[] getDefaultTCActions() {
        if (this.peer instanceof MultiViewTopComponent) {
            return ((MultiViewTopComponent)this.peer).getDefaultTCActions();
        }
        return new Action[0];
    }

    JEditorPane getEditorPane() {
        MultiViewElement el;
        if (this.model != null && (el = this.model.getActiveElement()) != null && el.getVisualRepresentation() instanceof CloneableEditorSupport.Pane) {
            CloneableEditorSupport.Pane pane = (CloneableEditorSupport.Pane)el.getVisualRepresentation();
            return pane.getEditorPane();
        }
        return null;
    }

    HelpCtx getHelpCtx() {
        return this.model.getActiveDescription().getHelpCtx();
    }

    UndoRedo peerGetUndoRedo() {
        return this.delegateUndoRedo;
    }

    private UndoRedo privateGetUndoRedo() {
        return this.model.getActiveElement().getUndoRedo() != null ? this.model.getActiveElement().getUndoRedo() : UndoRedo.NONE;
    }

    boolean canClose() {
        Collection col = this.model.getCreatedElements();
        Iterator it = col.iterator();
        ArrayList<CloseOperationState> badOnes = new ArrayList<CloseOperationState>();
        while (it.hasNext()) {
            MultiViewElement el = (MultiViewElement)it.next();
            CloseOperationState state = el.canCloseElement();
            if (state.canClose()) continue;
            badOnes.add(state);
        }
        if (badOnes.size() > 0) {
            CloseOperationState[] states = new CloseOperationState[badOnes.size()];
            boolean res = this.closeHandler.resolveCloseOperation(states = badOnes.toArray(states));
            if (res && SpiAccessor.DEFAULT.shouldCheckCanCloseAgain(this.closeHandler)) {
                col = this.model.getCreatedElements();
                for (MultiViewElement el : col) {
                    CloseOperationState state = el.canCloseElement();
                    if (state.canClose()) continue;
                    res = false;
                    break;
                }
            }
            return res;
        }
        return true;
    }

    public void updateName() {
        if (this.model != null) {
            for (MultiViewDescription mvd : this.model.getDescriptions()) {
                MultiViewElement el;
                if (mvd instanceof ContextAwareDescription && ((ContextAwareDescription)mvd).isSplitDescription() || (el = this.model.getElementForDescription(mvd, MultiViewCloneableTopComponent.isSourceView(mvd))) == null || !(el.getVisualRepresentation() instanceof CloneableEditorSupport.Pane)) continue;
                CloneableEditorSupport.Pane pane = (CloneableEditorSupport.Pane)el.getVisualRepresentation();
                pane.updateName();
                CloneableTopComponent tc = pane.getComponent();
                this.peer.setDisplayName(tc.getDisplayName());
                this.peer.setIcon(tc.getIcon());
                if (Arrays.asList(tc.getPropertyChangeListeners()).contains(this.propListener)) continue;
                tc.addPropertyChangeListener(this.propListener);
            }
        }
    }

    public Lookup getLookup() {
        if (this.lookup == null) {
            this.lookup = new MultiViewTopComponentLookup(this.delegatingMap);
        }
        return this.lookup;
    }

    private boolean isToolbarVisible() {
        return editorSettingsPreferences == null || editorSettingsPreferences.getBoolean("toolbarVisible", true);
    }

    public String toString() {
        return "[model=" + this.model + "]";
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("icon".equals(evt.getPropertyName()) || "name".equals(evt.getPropertyName()) || "displayName".equals(evt.getPropertyName()) || "htmlDisplayName".equals(evt.getPropertyName())) {
            this.updateName();
        }
    }

    static {
        String n;
        Preferences p = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
        if (p == null && (n = System.getProperty("test.multiview.toolbar.settings")) != null) {
            p = NbPreferences.root().node(n);
        }
        editorSettingsPreferences = p;
    }

    private class PreferenceChangeListenerImpl
    implements PreferenceChangeListener,
    Runnable {
        @Override
        public void preferenceChange(PreferenceChangeEvent evt) {
            if ("toolbarVisible".equals(evt.getKey())) {
                EventQueue.invokeLater(this);
            }
        }

        @Override
        public void run() {
            MultiViewPeer.this.tabs.setToolbarBarVisible(MultiViewPeer.this.isToolbarVisible());
        }
    }

    private class DelegateUndoRedo
    implements UndoRedo {
        private List listeners;

        private DelegateUndoRedo() {
            this.listeners = new ArrayList();
        }

        public boolean canUndo() {
            return MultiViewPeer.this.privateGetUndoRedo().canUndo();
        }

        public boolean canRedo() {
            return MultiViewPeer.this.privateGetUndoRedo().canRedo();
        }

        public void undo() throws CannotUndoException {
            MultiViewPeer.this.privateGetUndoRedo().undo();
        }

        public void redo() throws CannotRedoException {
            MultiViewPeer.this.privateGetUndoRedo().redo();
        }

        public void addChangeListener(ChangeListener l) {
            this.listeners.add(l);
            MultiViewPeer.this.privateGetUndoRedo().addChangeListener(l);
        }

        public void removeChangeListener(ChangeListener l) {
            this.listeners.remove(l);
            MultiViewPeer.this.privateGetUndoRedo().removeChangeListener(l);
        }

        public String getUndoPresentationName() {
            return MultiViewPeer.this.privateGetUndoRedo().getUndoPresentationName();
        }

        public String getRedoPresentationName() {
            return MultiViewPeer.this.privateGetUndoRedo().getRedoPresentationName();
        }

        private void fireElementChange() {
            for (ChangeListener elem : new ArrayList(this.listeners)) {
                ChangeEvent event = new ChangeEvent(this);
                elem.stateChanged(event);
            }
        }

        void updateListeners(MultiViewElement old, MultiViewElement fresh) {
            for (ChangeListener elem : this.listeners) {
                if (old.getUndoRedo() != null) {
                    old.getUndoRedo().removeChangeListener(elem);
                }
                if (fresh.getUndoRedo() == null) continue;
                fresh.getUndoRedo().addChangeListener(elem);
            }
            this.fireElementChange();
        }
    }

    private class AccessTogglesAction
    extends AbstractAction {
        AccessTogglesAction() {
            this.putValue("AcceleratorKey", KeyStroke.getKeyStroke("control F10"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            MultiViewPeer.this.tabs.requestFocusForSelectedButton();
        }
    }

    private static class RuntimeMultiViewDescription
    implements ContextAwareDescription {
        private final MultiViewDescription delegate;
        private final boolean split;

        public RuntimeMultiViewDescription(MultiViewDescription delegate, boolean split) {
            this.delegate = delegate;
            this.split = split;
        }

        @Override
        public ContextAwareDescription createContextAwareDescription(Lookup context, boolean isSplitDescription) {
            return new RuntimeMultiViewDescription(this.delegate, isSplitDescription);
        }

        @Override
        public boolean isSplitDescription() {
            return this.split;
        }

        @Override
        public int getPersistenceType() {
            return this.delegate.getPersistenceType();
        }

        @Override
        public String getDisplayName() {
            return this.delegate.getDisplayName();
        }

        @Override
        public Image getIcon() {
            return this.delegate.getIcon();
        }

        @Override
        public HelpCtx getHelpCtx() {
            return this.delegate.getHelpCtx();
        }

        @Override
        public String preferredID() {
            return this.delegate.preferredID();
        }

        @Override
        public MultiViewElement createElement() {
            return this.delegate.createElement();
        }
    }

    private class MVTCHandler
    implements MultiViewHandlerDelegate {
        private MultiViewPerspective[] perspectives;

        private MVTCHandler() {
            this.perspectives = null;
        }

        @Override
        public MultiViewPerspective[] getDescriptions() {
            return MultiViewPeer.this.model.getPerspectives();
        }

        @Override
        public MultiViewPerspective getSelectedDescription() {
            return MultiViewPeer.this.model.getSelectedPerspective();
        }

        @Override
        public void requestActive(MultiViewPerspective pers) {
            MultiViewDescription desc = Accessor.DEFAULT.extractDescription(pers);
            if (MultiViewPeer.this.model.getActiveDescription() != desc) {
                MultiViewPeer.this.tabs.changeActiveManually(desc);
                MultiViewPeer.this.model.getActiveElement().componentActivated();
            }
        }

        @Override
        public void requestVisible(MultiViewPerspective pers) {
            MultiViewDescription desc = Accessor.DEFAULT.extractDescription(pers);
            MultiViewPeer.this.tabs.changeVisibleManually(desc);
        }

        @Override
        public void addMultiViewDescription(MultiViewDescription descr, int position) {
            if (-1 != MultiViewPeer.this.splitOrientation) {
                MultiViewPeer.this.peerClearSplit(0);
            }
            MultiViewDescription[] oldDesc = MultiViewPeer.this.model.getDescriptions();
            if (position < 0 || position >= oldDesc.length / 2) {
                position = oldDesc.length / 2;
            }
            RuntimeMultiViewDescription wrapper = new RuntimeMultiViewDescription(descr, false);
            RuntimeMultiViewDescription splitWrapper = new RuntimeMultiViewDescription(descr, true);
            MultiViewDescription[] newDesc = new MultiViewDescription[oldDesc.length + 2];
            int index = 0;
            for (int i = 0; i < newDesc.length / 2; ++i) {
                if (i == position) {
                    newDesc[2 * i] = wrapper;
                    newDesc[2 * i + 1] = splitWrapper;
                    continue;
                }
                newDesc[2 * i] = oldDesc[index++];
                newDesc[2 * i + 1] = oldDesc[index++];
            }
            MultiViewPeer.this._setMultiViewDescriptions(newDesc, null);
            MultiViewPeer.this.tabs.changeActiveManually(wrapper);
        }

        @Override
        public void removeMultiViewDescription(MultiViewDescription descr) {
            MultiViewDescription[] oldDesc = MultiViewPeer.this.model.getDescriptions();
            int position = -1;
            for (int i = 0; i < oldDesc.length / 2; ++i) {
                RuntimeMultiViewDescription runtimeDesc;
                if (!(oldDesc[2 * i] instanceof RuntimeMultiViewDescription) || !(runtimeDesc = (RuntimeMultiViewDescription)oldDesc[2 * i]).delegate.equals(descr)) continue;
                position = i;
                break;
            }
            if (position < 0) {
                return;
            }
            if (-1 != MultiViewPeer.this.splitOrientation) {
                MultiViewPeer.this.peerClearSplit(0);
            }
            MultiViewDescription[] newDesc = new MultiViewDescription[oldDesc.length - 2];
            int index = 0;
            for (int i2 = 0; i2 < oldDesc.length / 2; ++i2) {
                if (i2 == position) continue;
                newDesc[index++] = oldDesc[2 * i2];
                newDesc[index++] = oldDesc[2 * i2 + 1];
            }
            MultiViewPeer.this._setMultiViewDescriptions(newDesc, null);
            MultiViewPeer.this.tabs.changeActiveManually(newDesc[0]);
            MultiViewPeer.this.model.setActiveDescription(newDesc[0]);
            MultiViewPeer.this.showCurrentElement();
        }
    }

    private class SelectionListener
    implements MultiViewModel.ElementSelectionListener {
        private SelectionListener() {
        }

        @Override
        public void selectionChanged(MultiViewDescription oldOne, MultiViewDescription newOne) {
            if (MultiViewPeer.this.isActivated()) {
                MultiViewElement el = MultiViewPeer.this.model.getElementForDescription(oldOne);
                el.componentDeactivated();
            }
            MultiViewPeer.this.hideElement(oldOne);
            MultiViewPeer.this.showCurrentElement();
            MultiViewPeer.this.delegateUndoRedo.updateListeners(MultiViewPeer.this.model.getElementForDescription(oldOne), MultiViewPeer.this.model.getElementForDescription(newOne));
        }

        @Override
        public void selectionActivatedByButton() {
            MultiViewElement elem = MultiViewPeer.this.model.getActiveElement();
            elem.getVisualRepresentation().requestFocus();
            elem.componentActivated();
        }
    }

}

