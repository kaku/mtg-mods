/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 *  org.openide.util.Lookup
 */
package org.netbeans.core.multiview;

import org.netbeans.core.spi.multiview.MultiViewDescription;
import org.netbeans.spi.editor.mimelookup.MimeLocation;
import org.openide.util.Lookup;

@MimeLocation(subfolderName="MultiView")
public interface ContextAwareDescription
extends MultiViewDescription {
    public ContextAwareDescription createContextAwareDescription(Lookup var1, boolean var2);

    public boolean isSplitDescription();
}

