/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.multiview;

import org.netbeans.core.spi.multiview.MultiViewDescription;

public interface SourceCheckDescription
extends MultiViewDescription {
    public boolean isSourceView();
}

