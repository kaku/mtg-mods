/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.core.multiview;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.Action;
import javax.swing.ActionMap;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

class MultiViewTopComponentLookup
extends Lookup {
    private MyProxyLookup proxy;
    private InitialProxyLookup initial;

    public MultiViewTopComponentLookup(ActionMap initialObject) {
        this.initial = new InitialProxyLookup(initialObject);
        this.proxy = new MyProxyLookup((Lookup)this.initial);
    }

    public void setElementLookup(Lookup look) {
        this.proxy.setElementLookup(look);
        this.initial.refreshLookup();
    }

    public Lookup.Item lookupItem(Lookup.Template template) {
        if (template.getType() == ActionMap.class || template.getId() != null && template.getId().equals("javax.swing.ActionMap")) {
            return this.initial.lookupItem(template);
        }
        Lookup.Item retValue = super.lookupItem(template);
        return retValue;
    }

    public Object lookup(Class clazz) {
        if (clazz == ActionMap.class) {
            return this.initial.lookup(clazz);
        }
        Object retValue = this.proxy.lookup(clazz);
        return retValue;
    }

    public Lookup.Result lookup(Lookup.Template template) {
        if (template.getType() == ActionMap.class || template.getId() != null && template.getId().equals("javax.swing.ActionMap")) {
            return this.initial.lookup(template);
        }
        Lookup.Result retValue = this.proxy.lookup(template);
        retValue = new ExclusionResult(retValue);
        return retValue;
    }

    boolean isInitialized() {
        return this.proxy.isInitialized();
    }

    static class LookupProxyActionMap
    extends ActionMap {
        private ActionMap map;

        public LookupProxyActionMap(ActionMap original) {
            this.map = original;
        }

        @Override
        public void setParent(ActionMap map) {
            this.map.setParent(map);
        }

        @Override
        public ActionMap getParent() {
            return this.map.getParent();
        }

        @Override
        public void put(Object key, Action action) {
            this.map.put(key, action);
        }

        @Override
        public Action get(Object key) {
            return this.map.get(key);
        }

        @Override
        public void remove(Object key) {
            this.map.remove(key);
        }

        @Override
        public void clear() {
            this.map.clear();
        }

        @Override
        public Object[] keys() {
            return this.map.keys();
        }

        @Override
        public int size() {
            return this.map.size();
        }

        @Override
        public Object[] allKeys() {
            return this.map.allKeys();
        }
    }

    static class InitialProxyLookup
    extends ProxyLookup {
        private ActionMap initObject;

        public InitialProxyLookup(ActionMap obj) {
            super(new Lookup[]{Lookups.fixed((Object[])new Object[]{new LookupProxyActionMap(obj)})});
            this.initObject = obj;
        }

        public void refreshLookup() {
            this.setLookups(new Lookup[]{Lookups.fixed((Object[])new Object[]{new LookupProxyActionMap(this.initObject)})});
        }
    }

    private static class MyProxyLookup
    extends ProxyLookup {
        private Lookup initialLookup;

        public MyProxyLookup(Lookup initial) {
            super(new Lookup[]{initial});
            this.initialLookup = initial;
        }

        public void setElementLookup(Lookup look) {
            Lookup[] arr = this.getLookups();
            if (arr.length == 2 && look == arr[1]) {
                return;
            }
            this.setLookups(new Lookup[]{this.initialLookup, look});
        }

        private boolean isInitialized() {
            return this.getLookups().length == 2;
        }
    }

    private static final class ExclusionResult
    extends Lookup.Result
    implements LookupListener {
        private final Lookup.Result delegate;
        private final List listeners = new ArrayList();
        private Collection lastResults;

        public ExclusionResult(Lookup.Result delegate) {
            this.delegate = delegate;
        }

        public Collection allInstances() {
            HashSet s = new HashSet(this.delegate.allInstances());
            return s;
        }

        public Set allClasses() {
            return this.delegate.allClasses();
        }

        public Collection allItems() {
            HashSet s = new HashSet(this.delegate.allItems());
            Iterator it = s.iterator();
            HashSet<Object> instances = new HashSet<Object>();
            while (it.hasNext()) {
                Lookup.Item i = (Lookup.Item)it.next();
                if (instances.contains(i.getInstance())) {
                    it.remove();
                    continue;
                }
                instances.add(i.getInstance());
            }
            return s;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void addLookupListener(LookupListener l) {
            List list = this.listeners;
            synchronized (list) {
                if (this.listeners.isEmpty()) {
                    if (this.lastResults == null) {
                        this.lastResults = this.allInstances();
                    }
                    this.delegate.addLookupListener((LookupListener)this);
                }
                this.listeners.add(l);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void removeLookupListener(LookupListener l) {
            List list = this.listeners;
            synchronized (list) {
                this.listeners.remove((Object)l);
                if (this.listeners.isEmpty()) {
                    this.delegate.removeLookupListener((LookupListener)this);
                    this.lastResults = null;
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void resultChanged(LookupEvent ev) {
            LookupListener[] ls;
            List list = this.listeners;
            synchronized (list) {
                boolean equal;
                Collection current = this.allInstances();
                boolean bl = equal = this.lastResults != null && current != null && current.containsAll(this.lastResults) && this.lastResults.containsAll(current);
                if (equal) {
                    return;
                }
                this.lastResults = current;
            }
            LookupEvent ev2 = new LookupEvent((Lookup.Result)this);
            List equal = this.listeners;
            synchronized (equal) {
                ls = this.listeners.toArray((T[])new LookupListener[this.listeners.size()]);
            }
            for (int i = 0; i < ls.length; ++i) {
                ls[i].resultChanged(ev2);
            }
        }
    }

}

