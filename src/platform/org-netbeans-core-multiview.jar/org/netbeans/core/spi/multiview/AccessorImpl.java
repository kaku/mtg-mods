/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.spi.multiview;

import org.netbeans.core.multiview.MultiViewElementCallbackDelegate;
import org.netbeans.core.multiview.SpiAccessor;
import org.netbeans.core.spi.multiview.CloseOperationHandler;
import org.netbeans.core.spi.multiview.MultiViewElementCallback;
import org.netbeans.core.spi.multiview.MultiViewFactory;

class AccessorImpl
extends SpiAccessor {
    private AccessorImpl() {
    }

    static void createAccesor() {
        if (DEFAULT == null) {
            DEFAULT = new AccessorImpl();
        }
    }

    @Override
    public MultiViewElementCallback createCallback(MultiViewElementCallbackDelegate delegate) {
        return new MultiViewElementCallback(delegate);
    }

    @Override
    public CloseOperationHandler createDefaultCloseHandler() {
        return MultiViewFactory.createDefaultCloseOpHandler();
    }

    @Override
    public boolean shouldCheckCanCloseAgain(CloseOperationHandler closeHandler) {
        if (closeHandler instanceof MultiViewFactory.DefaultCloseHandler) {
            return ((MultiViewFactory.DefaultCloseHandler)closeHandler).shouldCheckCanCloseAgain();
        }
        return false;
    }
}

