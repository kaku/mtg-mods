/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.actions.Savable
 *  org.openide.nodes.Node
 *  org.openide.text.CloneableEditor
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.NbDocument
 *  org.openide.text.NbDocument$CustomToolbar
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.windows.CloneableTopComponent
 *  org.openide.windows.CloneableTopComponent$Ref
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.spi.multiview.text;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.text.Document;
import org.netbeans.api.actions.Savable;
import org.netbeans.core.spi.multiview.CloseOperationState;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.MultiViewElementCallback;
import org.netbeans.core.spi.multiview.MultiViewFactory;
import org.netbeans.core.spi.multiview.text.Bundle;
import org.openide.nodes.Node;
import org.openide.text.CloneableEditor;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.NbDocument;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.windows.CloneableTopComponent;
import org.openide.windows.TopComponent;

class MultiViewCloneableEditor
extends CloneableEditor
implements MultiViewElement {
    private static final long serialVersionUID = -3126744316644172415L;
    private transient MultiViewElementCallback multiViewObserver;
    private transient JPanel bar;

    public MultiViewCloneableEditor() {
    }

    public MultiViewCloneableEditor(CloneableEditorSupport support) {
        super(support, true);
        this.initializeBySupport();
    }

    @Override
    public JComponent getToolbarRepresentation() {
        if (this.bar == null) {
            this.bar = new JPanel();
            this.bar.setLayout(new BorderLayout());
            this.fillInBar();
        }
        return this.bar;
    }

    @Override
    public JComponent getVisualRepresentation() {
        return this;
    }

    @Override
    public final void setMultiViewCallback(MultiViewElementCallback callback) {
        this.multiViewObserver = callback;
    }

    protected final MultiViewElementCallback getElementObserver() {
        return this.multiViewObserver;
    }

    @Override
    public void componentActivated() {
        super.componentActivated();
    }

    @Override
    public void componentClosed() {
        super.componentClosed();
    }

    @Override
    public void componentDeactivated() {
        super.componentDeactivated();
    }

    @Override
    public void componentHidden() {
        super.componentHidden();
    }

    @Override
    public void componentOpened() {
        super.componentOpened();
    }

    @Override
    public void componentShowing() {
        this.updateDisplayText();
        super.componentShowing();
    }

    @Override
    public Action[] getActions() {
        return super.getActions();
    }

    @Override
    public Lookup getLookup() {
        if (this.multiViewObserver == null) {
            return this.getLookupSuper();
        }
        return this.multiViewObserver.getTopComponent().getLookup();
    }

    public String preferredID() {
        return super.preferredID();
    }

    public void requestVisible() {
        if (this.multiViewObserver != null) {
            this.multiViewObserver.requestVisible();
        } else {
            super.requestVisible();
        }
    }

    public void requestActive() {
        if (this.multiViewObserver != null) {
            this.multiViewObserver.requestActive();
        } else {
            super.requestActive();
        }
    }

    public void updateName() {
        super.updateName();
        this.updateDisplayText();
    }

    private void updateDisplayText() {
        if (this.multiViewObserver != null) {
            TopComponent tc = this.multiViewObserver.getTopComponent();
            tc.setHtmlDisplayName(this.getHtmlDisplayName());
            tc.setDisplayName(this.getDisplayName());
            tc.setName(this.getName());
            tc.setToolTipText(this.getToolTipText());
            tc.setIcon(this.getIcon());
        }
    }

    public void open() {
        if (this.multiViewObserver != null) {
            this.multiViewObserver.requestVisible();
        } else {
            super.open();
        }
    }

    protected boolean closeLast() {
        return true;
    }

    @Override
    public CloseOperationState canCloseElement() {
        Savable sav;
        final CloneableEditorSupport sup = (CloneableEditorSupport)this.getLookup().lookup(CloneableEditorSupport.class);
        Enumeration en = this.getReference().getComponents();
        if (en.hasMoreElements()) {
            en.nextElement();
            if (en.hasMoreElements()) {
                return CloseOperationState.STATE_OK;
            }
        }
        if ((sav = (Savable)this.getLookup().lookup(Savable.class)) != null) {
            AbstractAction save;
            block9 : {
                save = new AbstractAction(){

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        try {
                            sup.saveDocument();
                        }
                        catch (IOException ex) {
                            Exceptions.printStackTrace((Throwable)ex);
                        }
                    }
                };
                try {
                    if (sav.getClass().getMethod("toString", new Class[0]).getDeclaringClass() != Object.class) {
                        save.putValue("LongDescription", Bundle.MSG_SaveModified((Object)sav));
                    } else {
                        Logger.getLogger(MultiViewCloneableEditor.class.getName()).log(Level.WARNING, "Need to override toString() to contain the file name in o.n.api.action.Savable {0} with lookup {1}", new Object[]{sav.getClass(), this.getLookup().lookupAll(Object.class)});
                        Node n = (Node)this.getLookup().lookup(Node.class);
                        if (n != null) {
                            save.putValue("LongDescription", Bundle.MSG_SaveModified(n.getDisplayName()));
                        } else {
                            save.putValue("LongDescription", Bundle.MSG_SaveModified_no_name());
                        }
                    }
                }
                catch (NoSuchMethodException x) {
                    if ($assertionsDisabled) break block9;
                    throw new AssertionError(x);
                }
            }
            return MultiViewFactory.createUnsafeCloseState("editor", save, null);
        }
        return CloseOperationState.STATE_OK;
    }

    Lookup getLookupSuper() {
        return super.getLookup();
    }

    public void revalidate() {
        super.revalidate();
        this.fillInBar();
    }

    private void fillInBar() {
        Document doc;
        NbDocument.CustomToolbar custom;
        JToolBar content;
        if (this.bar != null && this.bar.getComponentCount() == 0 && this.pane != null && (doc = this.pane.getDocument()) instanceof NbDocument.CustomToolbar && (content = (custom = (NbDocument.CustomToolbar)doc).createToolbar(this.pane)) != null) {
            this.bar.add((Component)content, "Center");
        }
    }

}

