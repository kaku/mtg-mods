/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.awt.UndoRedo
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.windows.CloneableTopComponent
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.spi.multiview;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JPanel;
import org.netbeans.core.multiview.ContextAwareDescription;
import org.netbeans.core.multiview.MultiViewCloneableTopComponent;
import org.netbeans.core.multiview.MultiViewTopComponent;
import org.netbeans.core.multiview.SourceCheckDescription;
import org.netbeans.core.spi.multiview.Bundle;
import org.netbeans.core.spi.multiview.CloseOperationHandler;
import org.netbeans.core.spi.multiview.CloseOperationState;
import org.netbeans.core.spi.multiview.MultiViewDescription;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.MultiViewElementCallback;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.UndoRedo;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.windows.CloneableTopComponent;
import org.openide.windows.TopComponent;

public final class MultiViewFactory {
    public static final MultiViewElement BLANK_ELEMENT = new Blank();
    public static final Action NOOP_CLOSE_ACTION = new NoopAction();

    private MultiViewFactory() {
    }

    public static TopComponent createMultiView(MultiViewDescription[] descriptions, MultiViewDescription defaultDesc) {
        return MultiViewFactory.createMultiView(descriptions, defaultDesc, MultiViewFactory.createDefaultCloseOpHandler());
    }

    public static TopComponent createMultiView(MultiViewDescription[] descriptions, MultiViewDescription defaultDesc, CloseOperationHandler closeHandler) {
        if (descriptions == null) {
            return null;
        }
        if (closeHandler == null) {
            closeHandler = MultiViewFactory.createDefaultCloseOpHandler();
        }
        MultiViewTopComponent tc = new MultiViewTopComponent();
        tc.setMultiViewDescriptions(descriptions, defaultDesc);
        tc.setCloseOperationHandler(closeHandler);
        return tc;
    }

    public static CloneableTopComponent createCloneableMultiView(MultiViewDescription[] descriptions, MultiViewDescription defaultDesc) {
        return MultiViewFactory.createCloneableMultiView(descriptions, defaultDesc, MultiViewFactory.createDefaultCloseOpHandler());
    }

    public static CloneableTopComponent createCloneableMultiView(MultiViewDescription[] descriptions, MultiViewDescription defaultDesc, CloseOperationHandler closeHandler) {
        if (descriptions == null) {
            return null;
        }
        if (closeHandler == null) {
            closeHandler = MultiViewFactory.createDefaultCloseOpHandler();
        }
        MultiViewCloneableTopComponent tc = new MultiViewCloneableTopComponent();
        tc.setMultiViewDescriptions(descriptions, defaultDesc);
        tc.setCloseOperationHandler(closeHandler);
        return tc;
    }

    static CloseOperationState createSafeCloseState() {
        return new CloseOperationState(true, "ID_CLOSE_OK", NOOP_CLOSE_ACTION, NOOP_CLOSE_ACTION);
    }

    public static CloseOperationState createUnsafeCloseState(String warningId, Action proceedAction, Action discardAction) {
        return new CloseOperationState(false, warningId == null ? "" : warningId, proceedAction == null ? NOOP_CLOSE_ACTION : proceedAction, discardAction == null ? NOOP_CLOSE_ACTION : discardAction);
    }

    static CloseOperationHandler createDefaultCloseOpHandler() {
        return new DefaultCloseHandler();
    }

    static MultiViewDescription createMultiViewDescription(Map map) {
        return new MapMVD(map, null, false);
    }

    private static final class MapMVD
    implements MultiViewDescription,
    ContextAwareDescription,
    SourceCheckDescription {
        private final Map map;
        private final Lookup context;
        private boolean isSplitDescription;

        public MapMVD(Map map, Lookup context, boolean isSplitDescription) {
            this.map = map;
            this.context = context;
            this.isSplitDescription = isSplitDescription;
        }

        private <T> T get(String attr, Class<T> type) {
            Object obj = this.map.get(attr);
            if (obj == null) {
                throw new NullPointerException(attr + " attribute not specified for " + this.map.get("class"));
            }
            if (type.isInstance(obj)) {
                return type.cast(obj);
            }
            throw new IllegalArgumentException(attr + " not of type " + type + " but " + obj + " for " + this.map.get("class"));
        }

        @Override
        public int getPersistenceType() {
            if (!this.map.containsKey("persistenceType")) {
                return 2;
            }
            return this.get("persistenceType", Integer.class);
        }

        @Override
        public String getDisplayName() {
            return this.get("displayName", String.class);
        }

        @Override
        public Image getIcon() {
            if (!this.map.containsKey("iconBase")) {
                return null;
            }
            String base = this.get("iconBase", String.class);
            return ImageUtilities.loadImage((String)base, (boolean)true);
        }

        @Override
        public HelpCtx getHelpCtx() {
            return HelpCtx.DEFAULT_HELP;
        }

        @Override
        public String preferredID() {
            return this.get("preferredID", String.class);
        }

        @Override
        public MultiViewElement createElement() {
            String name = this.get("class", String.class);
            String method = (String)this.map.get("method");
            Exception first = null;
            try {
                ClassLoader cl = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
                if (cl == null) {
                    cl = Thread.currentThread().getContextClassLoader();
                }
                if (cl == null) {
                    cl = MultiViewFactory.class.getClassLoader();
                }
                Class clazz = Class.forName(name, true, cl);
                if (method == null) {
                    try {
                        Constructor lookupC = clazz.getConstructor(Lookup.class);
                        return (MultiViewElement)lookupC.newInstance(new Object[]{this.context});
                    }
                    catch (Exception ex) {
                        first = ex;
                        Constructor defC = clazz.getConstructor(new Class[0]);
                        return (MultiViewElement)defC.newInstance(new Object[0]);
                    }
                }
                try {
                    Method m = clazz.getMethod(method, Lookup.class);
                    return (MultiViewElement)m.invoke(null, new Object[]{this.context});
                }
                catch (NoSuchMethodException ex) {
                    first = ex;
                    Method m = clazz.getMethod(method, new Class[0]);
                    return (MultiViewElement)m.invoke(null, new Object[0]);
                }
            }
            catch (Exception ex) {
                IllegalStateException ise;
                Throwable t = ise = new IllegalStateException("Cannot instantiate " + name, ex);
                while (t.getCause() != null) {
                    t = t.getCause();
                }
                t.initCause(first);
                throw ise;
            }
        }

        @Override
        public ContextAwareDescription createContextAwareDescription(Lookup context, boolean isSplitDescription) {
            return new MapMVD(this.map, context, isSplitDescription);
        }

        @Override
        public boolean isSourceView() {
            return Boolean.TRUE.equals(this.map.get("sourceview"));
        }

        @Override
        public boolean isSplitDescription() {
            return this.isSplitDescription;
        }
    }

    private static final class NoopAction
    extends AbstractAction {
        private NoopAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
        }
    }

    static final class DefaultCloseHandler
    implements CloseOperationHandler,
    Serializable {
        private static final long serialVersionUID = -3126744916624172427L;
        private boolean checkCanCloseAgain = false;

        DefaultCloseHandler() {
        }

        @Override
        public boolean resolveCloseOperation(CloseOperationState[] elements) {
            this.checkCanCloseAgain = false;
            if (elements != null) {
                boolean canBeClosed = true;
                LinkedHashMap<String, CloseOperationState> badOnes = new LinkedHashMap<String, CloseOperationState>();
                for (int i = 0; i < elements.length; ++i) {
                    if (elements[i].canClose()) continue;
                    badOnes.put(elements[i].getCloseWarningID(), elements[i]);
                    canBeClosed = false;
                }
                if (!canBeClosed) {
                    NotifyDescriptor.Confirmation desc = new NotifyDescriptor.Confirmation(this.createPanel(badOnes), 1);
                    Object[] choose = new Object[]{Bundle.CTL_Save(), Bundle.CTL_Discard(), NotifyDescriptor.CANCEL_OPTION};
                    desc.setOptions(choose);
                    Object retVal = DialogDisplayer.getDefault().notify((NotifyDescriptor)desc);
                    if (retVal == choose[0]) {
                        Iterator<CloseOperationState> it = badOnes.values().iterator();
                        while (it.hasNext()) {
                            Action act = it.next().getProceedAction();
                            if (act == null) continue;
                            act.actionPerformed(new ActionEvent(this, 1001, "proceed"));
                        }
                        this.checkCanCloseAgain = true;
                    } else if (retVal == choose[1]) {
                        Iterator<CloseOperationState> it = badOnes.values().iterator();
                        while (it.hasNext()) {
                            Action act = it.next().getDiscardAction();
                            if (act == null) continue;
                            act.actionPerformed(new ActionEvent(this, 1001, "discard"));
                        }
                    } else {
                        return false;
                    }
                }
            }
            return true;
        }

        boolean shouldCheckCanCloseAgain() {
            return this.checkCanCloseAgain;
        }

        private Object createPanel(Map<String, CloseOperationState> elems) {
            if (elems.size() == 1) {
                return this.findDescription(elems.values().iterator().next());
            }
            StringBuilder sb = new StringBuilder();
            for (CloseOperationState state : elems.values()) {
                if (sb.length() > 0) {
                    sb.append(" ");
                }
                sb.append(this.findDescription(state));
            }
            return sb;
        }

        private Object findDescription(CloseOperationState e) {
            Action a = e.getProceedAction();
            Object msg = a.getValue("LongDescription");
            if (msg == null) {
                msg = a.getValue("ShortDescription");
            }
            if (msg == null) {
                msg = a.getValue("Name");
            }
            if (msg == null) {
                msg = e.getCloseWarningID();
            }
            return msg;
        }
    }

    private static final class Blank
    implements MultiViewElement,
    Serializable {
        private JPanel panel = new JPanel();
        private JPanel bar = new JPanel();

        Blank() {
        }

        @Override
        public void componentActivated() {
        }

        @Override
        public void componentClosed() {
        }

        @Override
        public void componentDeactivated() {
        }

        @Override
        public void componentHidden() {
        }

        @Override
        public void componentOpened() {
        }

        @Override
        public void componentShowing() {
        }

        @Override
        public Action[] getActions() {
            return new Action[0];
        }

        @Override
        public Lookup getLookup() {
            return Lookup.EMPTY;
        }

        @Override
        public JComponent getToolbarRepresentation() {
            return this.bar;
        }

        @Override
        public JComponent getVisualRepresentation() {
            return this.panel;
        }

        @Override
        public void setMultiViewCallback(MultiViewElementCallback callback) {
        }

        @Override
        public UndoRedo getUndoRedo() {
            return null;
        }

        @Override
        public CloseOperationState canCloseElement() {
            return CloseOperationState.STATE_OK;
        }
    }

}

