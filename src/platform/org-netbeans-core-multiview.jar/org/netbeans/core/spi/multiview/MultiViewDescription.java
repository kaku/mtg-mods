/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 *  org.openide.util.HelpCtx
 */
package org.netbeans.core.spi.multiview;

import java.awt.Image;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.spi.editor.mimelookup.MimeLocation;
import org.openide.util.HelpCtx;

@MimeLocation(subfolderName="MultiView")
public interface MultiViewDescription {
    public int getPersistenceType();

    public String getDisplayName();

    @CheckForNull
    public Image getIcon();

    public HelpCtx getHelpCtx();

    public String preferredID();

    public MultiViewElement createElement();
}

