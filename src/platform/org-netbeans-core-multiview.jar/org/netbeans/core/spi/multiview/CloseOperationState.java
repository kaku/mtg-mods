/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.spi.multiview;

import javax.swing.Action;
import org.netbeans.core.spi.multiview.MultiViewFactory;

public final class CloseOperationState {
    public static final CloseOperationState STATE_OK = MultiViewFactory.createSafeCloseState();
    private boolean canClose;
    private String id;
    private Action proceedAction;
    private Action discardAction;

    CloseOperationState(boolean close, String warningId, Action proceed, Action discard) {
        this.canClose = close;
        this.proceedAction = proceed;
        this.discardAction = discard;
        this.id = warningId;
    }

    public boolean canClose() {
        return this.canClose;
    }

    public String getCloseWarningID() {
        return this.id;
    }

    public Action getProceedAction() {
        return this.proceedAction;
    }

    public Action getDiscardAction() {
        return this.discardAction;
    }
}

