/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.core.spi.multiview;

import javax.swing.Action;
import org.netbeans.core.multiview.MultiViewElementCallbackDelegate;
import org.netbeans.core.spi.multiview.AccessorImpl;
import org.openide.windows.TopComponent;

public final class MultiViewElementCallback {
    private MultiViewElementCallbackDelegate delegate;

    MultiViewElementCallback(MultiViewElementCallbackDelegate del) {
        this.delegate = del;
    }

    public void requestActive() {
        this.delegate.requestActive();
    }

    public void requestVisible() {
        this.delegate.requestVisible();
    }

    public Action[] createDefaultActions() {
        return this.delegate.createDefaultActions();
    }

    public void updateTitle(String title) {
        this.delegate.updateTitle(title);
    }

    public boolean isSelectedElement() {
        return this.delegate.isSelectedElement();
    }

    public TopComponent getTopComponent() {
        return this.delegate.getTopComponent();
    }

    static {
        AccessorImpl.createAccesor();
    }
}

