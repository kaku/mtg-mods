/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.spi.multiview;

import org.netbeans.core.spi.multiview.CloseOperationState;

public interface CloseOperationHandler {
    public boolean resolveCloseOperation(CloseOperationState[] var1);
}

