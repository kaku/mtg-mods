/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.spi.multiview;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CTL_Discard() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_Discard");
    }

    static String CTL_Save() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_Save");
    }

    private void Bundle() {
    }
}

