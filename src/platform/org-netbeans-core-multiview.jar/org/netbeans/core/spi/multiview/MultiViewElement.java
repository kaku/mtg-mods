/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.UndoRedo
 *  org.openide.util.Lookup
 */
package org.netbeans.core.spi.multiview;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.swing.Action;
import javax.swing.JComponent;
import org.netbeans.core.spi.multiview.CloseOperationState;
import org.netbeans.core.spi.multiview.MultiViewElementCallback;
import org.openide.awt.UndoRedo;
import org.openide.util.Lookup;

public interface MultiViewElement {
    public JComponent getVisualRepresentation();

    public JComponent getToolbarRepresentation();

    public Action[] getActions();

    public Lookup getLookup();

    public void componentOpened();

    public void componentClosed();

    public void componentShowing();

    public void componentHidden();

    public void componentActivated();

    public void componentDeactivated();

    public UndoRedo getUndoRedo();

    public void setMultiViewCallback(MultiViewElementCallback var1);

    public CloseOperationState canCloseElement();

    @Target(value={ElementType.TYPE, ElementType.METHOD})
    @Retention(value=RetentionPolicy.SOURCE)
    public static @interface Registration {
        public String[] mimeType();

        public int persistenceType();

        public String displayName();

        public String iconBase() default "";

        public String preferredID();

        public int position() default Integer.MAX_VALUE;
    }

}

