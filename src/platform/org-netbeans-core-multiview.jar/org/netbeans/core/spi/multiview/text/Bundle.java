/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.spi.multiview.text;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String MSG_SaveModified(Object file_name) {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_SaveModified", (Object)file_name);
    }

    static String MSG_SaveModified_no_name() {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_SaveModified_no_name");
    }

    private void Bundle() {
    }
}

