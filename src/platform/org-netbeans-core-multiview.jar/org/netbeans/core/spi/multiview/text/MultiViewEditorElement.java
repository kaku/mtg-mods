/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.UndoRedo
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.CloneableEditorSupport$Pane
 *  org.openide.util.Lookup
 *  org.openide.windows.CloneableTopComponent
 */
package org.netbeans.core.spi.multiview.text;

import java.io.Serializable;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import org.netbeans.core.spi.multiview.CloseOperationState;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.MultiViewElementCallback;
import org.netbeans.core.spi.multiview.text.MultiViewCloneableEditor;
import org.openide.awt.UndoRedo;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.Lookup;
import org.openide.windows.CloneableTopComponent;

public class MultiViewEditorElement
implements MultiViewElement,
CloneableEditorSupport.Pane,
Serializable {
    static final long serialVersionUID = 430840231840923L;
    private MultiViewCloneableEditor editor;

    public MultiViewEditorElement(Lookup lookup) {
        CloneableEditorSupport sup = (CloneableEditorSupport)lookup.lookup(CloneableEditorSupport.class);
        if (sup == null) {
            throw new IllegalArgumentException("We expect CloneableEditorSupport in " + (Object)lookup);
        }
        this.editor = new MultiViewCloneableEditor(sup);
    }

    @Override
    public JComponent getVisualRepresentation() {
        return this.editor.getVisualRepresentation();
    }

    @Override
    public JComponent getToolbarRepresentation() {
        return this.editor.getToolbarRepresentation();
    }

    @Override
    public Action[] getActions() {
        return this.editor.getActions();
    }

    @Override
    public Lookup getLookup() {
        return this.editor.getLookupSuper();
    }

    @Override
    public void componentOpened() {
        this.editor.componentOpened();
    }

    @Override
    public void componentClosed() {
        this.editor.componentClosed();
    }

    @Override
    public void componentShowing() {
        this.editor.componentShowing();
    }

    @Override
    public void componentHidden() {
        this.editor.componentHidden();
    }

    @Override
    public void componentActivated() {
        this.editor.componentActivated();
    }

    @Override
    public void componentDeactivated() {
        this.editor.componentDeactivated();
    }

    @Override
    public UndoRedo getUndoRedo() {
        return this.editor.getUndoRedo();
    }

    @Override
    public void setMultiViewCallback(MultiViewElementCallback callback) {
        this.editor.setMultiViewCallback(callback);
    }

    @Override
    public CloseOperationState canCloseElement() {
        return this.editor.canCloseElement();
    }

    public JEditorPane getEditorPane() {
        return this.editor.getEditorPane();
    }

    public CloneableTopComponent getComponent() {
        return this.editor.getComponent();
    }

    public void updateName() {
        this.editor.updateName();
    }

    public void ensureVisible() {
        this.editor.ensureVisible();
    }
}

