/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.masterfs.providers.Notifier
 */
package org.netbeans.modules.masterfs.watcher.nio2;

import java.io.IOException;
import java.nio.file.ClosedWatchServiceException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.Watchable;
import java.util.List;
import org.netbeans.modules.masterfs.providers.Notifier;

public class NioNotifier
extends Notifier<WatchKey> {
    private final WatchService watcher = FileSystems.getDefault().newWatchService();

    protected WatchKey addWatch(String pathStr) throws IOException {
        Path path = Paths.get(pathStr, new String[0]);
        try {
            WatchKey key = path.register(this.watcher, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);
            return key;
        }
        catch (ClosedWatchServiceException ex) {
            throw new IOException(ex);
        }
    }

    protected void removeWatch(WatchKey key) throws IOException {
        try {
            key.cancel();
        }
        catch (ClosedWatchServiceException ex) {
            throw new IOException(ex);
        }
    }

    protected String nextEvent() throws IOException, InterruptedException {
        WatchKey key;
        try {
            key = this.watcher.take();
        }
        catch (ClosedWatchServiceException cwse) {
            InterruptedException ie = new InterruptedException();
            throw (InterruptedException)ie.initCause(cwse);
        }
        Path dir = (Path)key.watchable();
        String res = dir.toAbsolutePath().toString();
        for (WatchEvent event : key.pollEvents()) {
            if (event.kind() != StandardWatchEventKinds.OVERFLOW) continue;
            res = null;
        }
        key.reset();
        return res;
    }

    protected void start() throws IOException {
    }

    protected void stop() throws IOException {
        this.watcher.close();
    }
}

