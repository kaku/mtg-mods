/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.winxp;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.UIManager;
import javax.swing.border.AbstractBorder;

class StatusLineBorder
extends AbstractBorder {
    public static final int LEFT = 1;
    public static final int TOP = 2;
    public static final int RIGHT = 4;
    private Insets insets;
    private int type;

    public StatusLineBorder(int type) {
        this.type = type;
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
        g.translate(x, y);
        Color borderC = UIManager.getColor("Nb.ScrollPane.Border.color");
        g.setColor(borderC);
        if ((this.type & 2) != 0) {
            g.drawLine(0, 0, w - 1, 0);
        }
        if ((this.type & 1) != 0) {
            g.drawLine(0, 0, 0, h - 1);
        }
        if ((this.type & 4) != 0) {
            g.drawLine(w - 1, 0, w - 1, h - 1);
        }
        g.translate(- x, - y);
    }

    @Override
    public Insets getBorderInsets(Component c) {
        if (this.insets == null) {
            this.insets = new Insets((this.type & 2) != 0 ? 1 : 0, (this.type & 1) != 0 ? 1 : 0, 0, (this.type & 4) != 0 ? 1 : 0);
        }
        return this.insets;
    }
}

