/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.winxp;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Insets;
import javax.swing.BorderFactory;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.plaf.ColorUIResource;
import org.netbeans.swing.plaf.LFCustoms;
import org.netbeans.swing.plaf.util.GuaranteedValue;
import org.netbeans.swing.plaf.util.UIBootstrapValue;
import org.netbeans.swing.plaf.util.UIUtils;
import org.netbeans.swing.plaf.winxp.EditorToolbarBorder;
import org.netbeans.swing.plaf.winxp.StatusLineBorder;

public final class XPLFCustoms
extends LFCustoms {
    private static final String TAB_FOCUS_FILL_DARK = "tab_focus_fill_dark";
    private static final String TAB_FOCUS_FILL_BRIGHT = "tab_focus_fill_bright";
    private static final String TAB_UNSEL_FILL_DARK = "tab_unsel_fill_dark";
    private static final String TAB_UNSEL_FILL_BRIGHT = "tab_unsel_fill_bright";
    private static final String TAB_SEL_FILL = "tab_sel_fill";
    private static final String TAB_SEL_FILL_BRIGHT = "tab_sel_fill_bright";
    private static final String TAB_SEL_FILL_DARK = "tab_sel_fill_dark";
    private static final String TAB_BORDER = "tab_border";
    private static final String TAB_BOTTOM_BORDER = "tab_bottom_border";
    private static final String TAB_SEL_BORDER = "tab_sel_border";
    private static final String TAB_HIGHLIGHT_HEADER = "tab_highlight_header";
    private static final String TAB_HIGHLIGHT_HEADER_FILL = "tab_highlight_header_fill";
    private static final String STANDARD_BORDER = "standard_border";
    private static final String TAB_CLOSE_BUTTON = "close_button";
    private static final String TAB_CLOSE_BUTTON_HIGHLIGHT = "close_button_highlight";
    private static final String TAB_CLOSE_BUTTON_BORDER_FOCUS = "close_button_border_focus";
    private static final String TAB_CLOSE_BUTTON_BORDER_SELECTED = "close_button_border_selected";
    private static final String TAB_CLOSE_BUTTON_BORDER_UNSEL = "close_button_border_unsel";
    private static final String TAB_SEL_BOTTOM_BORDER = "tab_sel_bottom_border";
    static final String SCROLLPANE_BORDER_COLOR = "scrollpane_border";

    @Override
    public Object[] createLookAndFeelCustomizationKeysAndValues() {
        int fontsize = 11;
        Integer in = (Integer)UIManager.get("customFontSize");
        if (in != null) {
            fontsize = in;
        }
        Object[] result = new Object[]{"TextArea.font", new GuaranteedValue("Label.font", (Object)new Font("Dialog", 0, fontsize)), "Nb.Editor.ErrorStripe.ScrollBar.Insets", new Insets(17, 0, 17, 0)};
        return result;
    }

    @Override
    public Object[] createApplicationSpecificKeysAndValues() {
        XPEditorColorings editorTabsUI = new XPEditorColorings("org.netbeans.swing.tabcontrol.plaf.WinXPEditorTabDisplayerUI");
        UIDefaults.LazyValue viewTabsUI = editorTabsUI.createShared("org.netbeans.swing.tabcontrol.plaf.WinXPViewTabDisplayerUI");
        Image explorerIcon = UIUtils.loadImage("org/netbeans/swing/plaf/resources/xp-explorer-folder.gif");
        XPPropertySheetColorings propertySheetValues = new XPPropertySheetColorings();
        Object[] uiDefaults = new Object[]{"EditorTabDisplayerUI", editorTabsUI, "ViewTabDisplayerUI", viewTabsUI, "Nb.Desktop.background", new Color(226, 223, 214), "scrollpane_border", new Color(127, 157, 185), "Nb.Desktop.border", new EmptyBorder(6, 5, 4, 6), "Nb.ScrollPane.border", UIManager.get("ScrollPane.border"), "Nb.Explorer.Status.border", new StatusLineBorder(2), "Nb.Explorer.Folder.icon", explorerIcon, "Nb.Explorer.Folder.openedIcon", explorerIcon, "Nb.Editor.Status.leftBorder", new StatusLineBorder(6), "Nb.Editor.Status.rightBorder", new StatusLineBorder(3), "Nb.Editor.Status.innerBorder", new StatusLineBorder(7), "Nb.Editor.Status.onlyOneBorder", new StatusLineBorder(2), "Nb.Editor.Toolbar.border", new EditorToolbarBorder(), "nb.output.selectionBackground", new Color(164, 180, 255), "nb.propertysheet", propertySheetValues, "nb_workplace_fill", new Color(226, 223, 214), "nb.desktop.splitpane.border", BorderFactory.createEmptyBorder(4, 0, 0, 0), "SlidingButtonUI", "org.netbeans.swing.tabcontrol.plaf.WinXPSlidingButtonUI", "nbProgressBar.Foreground", new Color(49, 106, 197), "nbProgressBar.Background", Color.WHITE, "nbProgressBar.popupDynaText.foreground", new Color(141, 136, 122), "nbProgressBar.popupText.background", new Color(249, 249, 249), "nbProgressBar.popupText.foreground", UIManager.getColor("TextField.foreground"), "nbProgressBar.popupText.selectBackground", UIManager.getColor("List.selectionBackground"), "nbProgressBar.popupText.selectForeground", UIManager.getColor("List.selectionForeground"), "nb.progress.cancel.icon", UIUtils.loadImage("org/netbeans/swing/plaf/resources/cancel_task_win_xp.png"), "NbSlideBar.GroupSeparator.Gap.Before", 8, "NbSlideBar.GroupSeparator.Gap.After", 2, "NbSlideBar.RestoreButton.Gap", 1, "Nb.browser.picker.background.light", new Color(255, 255, 255), "Nb.browser.picker.foreground.light", new Color(130, 130, 130)};
        XPLFCustoms.convert("TextField.background");
        XPLFCustoms.convert("TextField.inactiveBackground");
        XPLFCustoms.convert("TextField.disabledBackground");
        return UIUtils.addInputMapsWithoutCtrlPageUpAndCtrlPageDown(uiDefaults);
    }

    private static void convert(String key) {
        Color c = UIManager.getColor(key);
        if (c != null && !(c instanceof ColorUIResource)) {
            UIManager.put(key, new ColorUIResource(c));
        }
    }

    @Override
    protected Object[] additionalKeys() {
        int i;
        Object[] kv = new XPEditorColorings("").createKeysAndValues();
        Object[] kv2 = new XPPropertySheetColorings().createKeysAndValues();
        Object[] result = new Object[kv.length / 2 + kv2.length / 2];
        int ct = 0;
        for (i = 0; i < kv.length; i += 2) {
            result[ct] = kv[i];
            ++ct;
        }
        for (i = 0; i < kv2.length; i += 2) {
            result[ct] = kv2[i];
            ++ct;
        }
        return result;
    }

    private class XPPropertySheetColorings
    extends UIBootstrapValue.Lazy {
        public XPPropertySheetColorings() {
            super("propertySheet");
        }

        @Override
        public Object[] createKeysAndValues() {
            return new Object[]{"PropSheet.setBackground", new Color(49, 106, 197), "PropSheet.selectionForeground", Color.WHITE, "PropSheet.setBackground", new Color(212, 208, 200), "PropSheet.setForeground", Color.BLACK, "PropSheet.selectedSetBackground", new Color(49, 106, 197), "PropSheet.selectedSetForeground", Color.WHITE, "PropSheet.disabledForeground", new Color(161, 161, 146), "PropSheet.customButtonForeground", Color.BLACK};
        }
    }

    private class XPEditorColorings
    extends UIBootstrapValue.Lazy {
        public XPEditorColorings(String name) {
            super(name);
        }

        @Override
        public Object[] createKeysAndValues() {
            return new Object[]{"tab_focus_fill_dark", new Color(210, 220, 243), "tab_focus_fill_bright", new Color(238, 242, 253), "tab_unsel_fill_dark", new Color(236, 235, 229), "tab_unsel_fill_bright", new Color(252, 251, 246), "tab_sel_fill", Color.white, "tab_sel_fill_bright", Color.white, "tab_sel_fill_dark", new Color(243, 241, 224), "tab_border", new Color(145, 167, 180), "tab_bottom_border", new Color(127, 157, 185), "tab_sel_border", new Color(145, 155, 156), "tab_highlight_header", new Color(230, 139, 44), "tab_highlight_header_fill", new Color(255, 199, 60), "standard_border", new Color(127, 157, 185), "close_button", Color.black, "close_button_highlight", new Color(172, 57, 28), "close_button_border_focus", new Color(181, 201, 243), "close_button_border_selected", new Color(203, 202, 187), "close_button_border_unsel", new Color(200, 201, 192), "tab_sel_bottom_border", new Color(238, 235, 218), "TabbedContainer.editor.outerBorder", BorderFactory.createEmptyBorder(), "TabbedContainer.editor.contentBorder", new MatteBorder(0, 1, 1, 1, new Color(127, 157, 185)), "TabbedContainer.editor.tabsBorder", BorderFactory.createEmptyBorder(), "TabbedContainer.view.outerBorder", BorderFactory.createEmptyBorder(), "TabbedContainer.view.contentBorder", new MatteBorder(0, 1, 1, 1, new Color(127, 157, 185)), "TabbedContainer.view.tabsBorder", BorderFactory.createEmptyBorder()};
        }
    }

}

