/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.util;

import java.awt.Color;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.util.HashSet;
import java.util.Map;
import java.util.StringTokenizer;
import javax.swing.LookAndFeel;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.DimensionUIResource;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.InsetsUIResource;
import javax.swing.plaf.metal.DefaultMetalTheme;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.AttributeList;
import org.xml.sax.DocumentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderAdapter;

public class NbTheme
extends DefaultMetalTheme
implements DocumentHandler {
    public static final String THEMEFILE_NAME = "themes.xml";
    private static final String THEMESET_TAG = "themeset";
    private static final String ACTIVE_ATTR = "active";
    private static final String THEME_TAG = "theme";
    private static final String BOOL_TAG = "boolean";
    private static final String DIM_TAG = "dimension";
    private static final String FONT_TAG = "font";
    private static final String INSETS_TAG = "insets";
    private static final String ETCHEDBORDER_TAG = "etchedborder";
    private static final String EMPTYBORDER_TAG = "emptyborder";
    private static final String BEVELBORDER_TAG = "bevelborder";
    private static final String LINEBORDER_TAG = "lineborder";
    private static final String COLOR_ATTR = "color";
    private static final String KEY_ATTR = "key";
    private static final String METRIC_TAG = "metric";
    private static final String STRING_TAG = "string";
    private static final String NAME_ATTR = "name";
    private static final String FONTSTYLE_ATTR = "style";
    private static final String FONTSIZE_ATTR = "size";
    private static final String VALUE_ATTR = "value";
    private static final String WIDTH_ATTR = "width";
    private static final String HEIGHT_ATTR = "height";
    private static final String RED_ATTR = "r";
    private static final String GREEN_ATTR = "g";
    private static final String BLUE_ATTR = "b";
    private static final String LEFT_ATTR = "left";
    private static final String TOP_ATTR = "top";
    private static final String RIGHT_ATTR = "right";
    private static final String BOTTOM_ATTR = "bottom";
    private static final String TYPE_ATTR = "type";
    private static final String REFERENCE_ATTR = "reference";
    private static final String FONTSTYLE_BOLD = "bold";
    private static final String FONTSTYLE_ITALIC = "italic";
    private static final String TYPE_LOWERED = "lowered";
    private static final String CONTROLFONT = "controlFont";
    private static final String SYSTEMFONT = "systemFont";
    private static final String USERFONT = "userFont";
    private static final String MENUFONT = "menuFont";
    private static final String WINDOWTITLEFONT = "windowTitleFont";
    private static final String SUBFONT = "subFont";
    private static final String PRIMARY1 = "primary1";
    private static final String PRIMARY2 = "primary2";
    private static final String PRIMARY3 = "primary3";
    private static final String SECONDARY1 = "secondary1";
    private static final String SECONDARY2 = "secondary2";
    private static final String SECONDARY3 = "secondary3";
    private static final String WHITE = "white";
    private static final String BLACK = "black";
    private HashSet<String> activeThemes = null;
    private boolean inActiveTheme = false;
    private URL themeURL = null;
    private UIDefaults defaults;
    Locator locator = null;

    @Override
    public String getName() {
        return "NetBeans XML Theme";
    }

    public NbTheme(URL themeURL, LookAndFeel lf) {
        this.themeURL = themeURL;
        this.defaults = lf.getDefaults();
        this.parseTheme();
        UIManager.getDefaults().putAll(this.defaults);
    }

    void initThemeDefaults() {
        this.defaults.put("primary1", new ColorUIResource(102, 102, 153));
        this.defaults.put("primary2", new ColorUIResource(153, 153, 204));
        this.defaults.put("primary3", new ColorUIResource(204, 204, 255));
        this.defaults.put("secondary1", new ColorUIResource(102, 102, 102));
        this.defaults.put("secondary2", new ColorUIResource(153, 153, 153));
        this.defaults.put("secondary3", new ColorUIResource(204, 204, 204));
        this.defaults.put("white", new ColorUIResource(255, 255, 255));
        this.defaults.put("black", new ColorUIResource(0, 0, 0));
    }

    private void parseTheme() {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setValidating(false);
            factory.setNamespaceAware(false);
            XMLReaderAdapter p = new XMLReaderAdapter(factory.newSAXParser().getXMLReader());
            p.setDocumentHandler(this);
            String externalForm = this.themeURL.toExternalForm();
            InputSource is = new InputSource(externalForm);
            p.parse(is);
            this.activeThemes = null;
            this.locator = null;
        }
        catch (IOException ie) {
            System.err.println("IO exception reading theme file");
        }
        catch (SAXException se) {
            System.err.println("Error parsing theme file " + (this.locator != null ? new StringBuilder().append("line ").append(this.locator.getLineNumber()).toString() : ""));
        }
        catch (ParserConfigurationException e) {
            System.err.println("Couldn't create XML parser for theme file");
        }
    }

    @Override
    public void endDocument() throws SAXException {
    }

    @Override
    public void startDocument() throws SAXException {
    }

    @Override
    public void startElement(String p1, AttributeList atts) throws SAXException {
        if (p1.equals("themeset")) {
            String themes = atts.getValue("active");
            if (themes != null) {
                StringTokenizer tok = new StringTokenizer(themes, ",");
                this.activeThemes = new HashSet(tok.countTokens() + 1);
                while (tok.hasMoreTokens()) {
                    this.activeThemes.add(tok.nextToken().trim());
                }
            }
        } else if (p1.equals("theme") && this.activeThemes != null) {
            String themeName = atts.getValue("name");
            this.inActiveTheme = this.activeThemes.contains(themeName);
        } else if (this.inActiveTheme) {
            if (this.handleReference(atts)) {
                return;
            }
            if (p1.equals("color")) {
                this.handleColor(atts);
                return;
            }
            if (p1.equals("font")) {
                this.handleFont(atts);
                return;
            }
            if (p1.equals("emptyborder")) {
                this.handleEmptyBorder(atts);
                return;
            }
            if (p1.equals("metric")) {
                this.handleMetric(atts);
                return;
            }
            if (p1.equals("string")) {
                this.handleString(atts);
                return;
            }
            if (p1.equals("insets")) {
                this.handleInsets(atts);
                return;
            }
            if (p1.equals("boolean")) {
                this.handleBool(atts);
                return;
            }
            if (p1.equals("dimension")) {
                this.handleDim(atts);
                return;
            }
            if (p1.equals("etchedborder")) {
                this.handleEtchedBorder(atts);
                return;
            }
            if (p1.equals("lineborder")) {
                this.handleLineBorder(atts);
                return;
            }
            if (p1.equals("bevelborder")) {
                this.handleBevelBorder(atts);
                return;
            }
            System.err.println("UNRECOGNIZED THEME ENTRY " + p1 + "\" " + atts.toString());
        }
    }

    private boolean handleReference(AttributeList atts) throws SAXException {
        Object res;
        String key = atts.getValue("key");
        String reference = atts.getValue("reference");
        if (reference != null && (res = this.defaults.get(reference)) != null) {
            this.defaults.put(key, res);
            return true;
        }
        return false;
    }

    private final void handleFont(AttributeList atts) throws SAXException {
        String key = atts.getValue("key");
        String fontname = atts.getValue("name");
        String fontstylename = atts.getValue("style");
        int fontsize = this.intFromAttr(atts, "size");
        int fontstyle = 0;
        if (fontstylename.equals("bold")) {
            fontstyle = 1;
        } else if (fontstylename.equals("italic")) {
            fontstyle = 2;
        }
        FontUIResource resource = new FontUIResource(fontname, fontstyle, fontsize);
        this.defaults.put(key, resource);
    }

    private final void handleColor(AttributeList atts) throws SAXException {
        int r = this.intFromAttr(atts, "r");
        int g = this.intFromAttr(atts, "g");
        int b = this.intFromAttr(atts, "b");
        String key = atts.getValue("key");
        ColorUIResource resource = new ColorUIResource(r, g, b);
        this.defaults.put(key, resource);
    }

    private final void handleMetric(AttributeList atts) throws SAXException {
        String key = atts.getValue("key");
        Integer resource = Integer.valueOf(atts.getValue("value"));
        this.defaults.put(key, resource);
    }

    private final void handleString(AttributeList atts) throws SAXException {
        String key = atts.getValue("key");
        String resource = atts.getValue("value");
        this.defaults.put(key, resource);
    }

    private final void handleBool(AttributeList atts) throws SAXException {
        String key = atts.getValue("key");
        Boolean resource = Boolean.valueOf(key);
        this.defaults.put(key, resource);
    }

    private final void handleDim(AttributeList atts) throws SAXException {
        String key = atts.getValue("key");
        int width = this.intFromAttr(atts, "width");
        int height = this.intFromAttr(atts, "height");
        DimensionUIResource resource = new DimensionUIResource(width, height);
        this.defaults.put(key, resource);
    }

    private final void handleInsets(AttributeList atts) throws SAXException {
        String key = atts.getValue("key");
        int top = this.intFromAttr(atts, "top");
        int left = this.intFromAttr(atts, "left");
        int bottom = this.intFromAttr(atts, "bottom");
        int right = this.intFromAttr(atts, "right");
        InsetsUIResource resource = new InsetsUIResource(top, left, bottom, right);
        this.defaults.put(key, resource);
    }

    private final void handleEtchedBorder(AttributeList atts) {
        String key = atts.getValue("key");
        int i = 1;
        String type = atts.getValue("type");
        if (type != null) {
            i = type.equals("lowered") ? 1 : 0;
        }
        BorderUIResource.EtchedBorderUIResource resource = new BorderUIResource.EtchedBorderUIResource(i);
        this.defaults.put(key, resource);
    }

    private final void handleBevelBorder(AttributeList atts) {
        String key = atts.getValue("key");
        int i = 1;
        String type = atts.getValue("type");
        if (type != null) {
            i = type.equals("lowered") ? 1 : 0;
        }
        BorderUIResource.BevelBorderUIResource resource = new BorderUIResource.BevelBorderUIResource(i);
        this.defaults.put(key, resource);
    }

    private final void handleEmptyBorder(AttributeList atts) throws SAXException {
        String key = atts.getValue("key");
        int top = this.intFromAttr(atts, "top");
        int left = this.intFromAttr(atts, "left");
        int bottom = this.intFromAttr(atts, "bottom");
        int right = this.intFromAttr(atts, "right");
        BorderUIResource.EmptyBorderUIResource resource = new BorderUIResource.EmptyBorderUIResource(top, left, bottom, right);
        this.defaults.put(key, resource);
    }

    private final void handleLineBorder(AttributeList atts) throws SAXException {
        String key = atts.getValue("key");
        int r = this.intFromAttr(atts, "r");
        int g = this.intFromAttr(atts, "g");
        int b = this.intFromAttr(atts, "b");
        int width = 1;
        if (atts.getValue("width") != null) {
            width = this.intFromAttr(atts, "width");
        }
        Color c = new Color(r, g, b);
        BorderUIResource.LineBorderUIResource resource = new BorderUIResource.LineBorderUIResource(c);
        this.defaults.put(key, resource);
    }

    private final int intFromAttr(AttributeList atts, String key) throws SAXException {
        try {
            return Integer.valueOf(atts.getValue(key));
        }
        catch (NumberFormatException nfe) {
            throw new SAXException(atts.getValue(key) + " is not an integer");
        }
    }

    @Override
    public void characters(char[] p1, int p2, int p3) throws SAXException {
    }

    @Override
    public void setDocumentLocator(Locator locator) {
        this.locator = locator;
    }

    @Override
    public void endElement(String p1) throws SAXException {
        if (p1.equals("theme")) {
            this.inActiveTheme = false;
        }
    }

    @Override
    public void ignorableWhitespace(char[] p1, int p2, int p3) throws SAXException {
    }

    @Override
    public void processingInstruction(String p1, String p2) throws SAXException {
    }

    private final ColorUIResource getColor(String key) {
        return (ColorUIResource)this.defaults.get(key);
    }

    private final FontUIResource getFont(String key) {
        return (FontUIResource)this.defaults.get(key);
    }

    @Override
    public FontUIResource getControlTextFont() {
        return this.getFont("controlFont");
    }

    @Override
    public FontUIResource getSystemTextFont() {
        return this.getFont("systemFont");
    }

    @Override
    public FontUIResource getUserTextFont() {
        return this.getFont("userFont");
    }

    @Override
    public FontUIResource getMenuTextFont() {
        return this.getFont("menuFont");
    }

    @Override
    public FontUIResource getWindowTitleFont() {
        return this.getFont("windowTitleFont");
    }

    @Override
    public FontUIResource getSubTextFont() {
        return this.getFont("subFont");
    }

    @Override
    protected ColorUIResource getPrimary1() {
        return this.getColor("primary1");
    }

    @Override
    protected ColorUIResource getPrimary2() {
        return this.getColor("primary2");
    }

    @Override
    protected ColorUIResource getPrimary3() {
        return this.getColor("primary3");
    }

    @Override
    protected ColorUIResource getSecondary1() {
        return this.getColor("secondary1");
    }

    @Override
    protected ColorUIResource getSecondary2() {
        return this.getColor("secondary2");
    }

    @Override
    protected ColorUIResource getSecondary3() {
        return this.getColor("secondary3");
    }

    @Override
    protected ColorUIResource getWhite() {
        return this.getColor("white");
    }

    @Override
    protected ColorUIResource getBlack() {
        return this.getColor("black");
    }
}

