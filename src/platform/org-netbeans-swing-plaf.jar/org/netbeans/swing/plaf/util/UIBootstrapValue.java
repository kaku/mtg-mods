/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.util;

import javax.swing.UIDefaults;
import javax.swing.UIManager;

public class UIBootstrapValue
implements UIDefaults.LazyValue {
    private boolean installed = false;
    private final String uiClassName;
    protected Object[] defaults;

    public UIBootstrapValue(String uiClassName, Object[] defaults) {
        this.defaults = defaults;
        this.uiClassName = uiClassName;
    }

    @Override
    public Object createValue(UIDefaults uidefaults) {
        if (!this.installed) {
            this.installKeysAndValues(uidefaults);
        }
        return this.uiClassName;
    }

    private void installKeysAndValues(UIDefaults ui) {
        ui.putDefaults(this.getKeysAndValues());
        this.installed = true;
    }

    public Object[] getKeysAndValues() {
        return this.defaults;
    }

    public void uninstall() {
        if (this.defaults == null) {
            return;
        }
        for (int i = 0; i < this.defaults.length; i += 2) {
            UIManager.put(this.defaults[i], null);
        }
        this.defaults = null;
    }

    public String toString() {
        return this.getClass() + " for " + this.uiClassName;
    }

    public UIDefaults.LazyValue createShared(String uiClassName) {
        return new Meta(uiClassName);
    }

    public static abstract class Lazy
    extends UIBootstrapValue
    implements UIDefaults.LazyValue {
        public Lazy(String uiClassName) {
            super(uiClassName, null);
        }

        @Override
        public Object[] getKeysAndValues() {
            if (this.defaults == null) {
                this.defaults = this.createKeysAndValues();
            }
            return this.defaults;
        }

        public abstract Object[] createKeysAndValues();
    }

    private class Meta
    implements UIDefaults.LazyValue {
        private String name;

        public Meta(String uiClassName) {
            this.name = uiClassName;
        }

        @Override
        public Object createValue(UIDefaults uidefaults) {
            if (!UIBootstrapValue.this.installed) {
                UIBootstrapValue.this.installKeysAndValues(uidefaults);
            }
            return this.name;
        }

        public String toString() {
            return "Meta-" + super.toString() + " for " + UIBootstrapValue.this.uiClassName;
        }
    }

}

