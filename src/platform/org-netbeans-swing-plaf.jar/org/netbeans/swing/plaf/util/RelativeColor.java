/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.util;

import java.awt.Color;
import java.io.PrintStream;
import javax.swing.UIDefaults;
import javax.swing.UIManager;

public class RelativeColor
implements UIDefaults.LazyValue {
    private Color value = null;
    private Color fallback = null;
    private Color targetColor = null;
    private String targetColorKey = null;
    private Color baseColor = null;
    private String baseColorKey = null;
    private Color mustContrastColor = null;
    private String mustContrastColorKey = null;
    private Color actualColor = null;
    private String actualColorKey = null;

    public RelativeColor(Object base, Object target, Object actual, Object mustContrast) {
        if (base == null || target == null || actual == null) {
            throw new NullPointerException("Null argument(s): " + base + ',' + target + ',' + actual + ',' + mustContrast);
        }
        if (base instanceof String) {
            this.baseColorKey = (String)base;
        } else {
            this.baseColor = (Color)base;
        }
        if (target instanceof String) {
            this.targetColorKey = (String)target;
        } else {
            this.targetColor = (Color)target;
        }
        if (actual instanceof String) {
            this.actualColorKey = (String)actual;
        } else {
            this.actualColor = (Color)actual;
        }
        if (mustContrast != null) {
            if (mustContrast instanceof String) {
                this.mustContrastColorKey = (String)mustContrast;
            } else {
                this.mustContrastColor = (Color)mustContrast;
            }
        }
    }

    public RelativeColor(Color base, Color target, Object actual) {
        this(base, target, actual, null);
    }

    public void clear() {
        this.value = null;
        if (this.actualColorKey != null) {
            this.actualColor = null;
        }
        if (this.targetColorKey != null) {
            this.targetColor = null;
        }
        if (this.mustContrastColorKey != null) {
            this.mustContrastColor = null;
        }
        if (this.baseColorKey != null) {
            this.baseColor = null;
        }
    }

    @Override
    public Object createValue(UIDefaults table) {
        Color base;
        if (this.value != null) {
            return this.value;
        }
        Color actual = this.getActualColor();
        this.value = actual.equals(base = this.getBaseColor()) ? this.getTargetColor() : RelativeColor.deriveColor(base, actual, this.getTargetColor());
        if (this.hasMustContrastColor()) {
            this.value = RelativeColor.ensureContrast(this.value, this.getMustContrastColor());
        }
        return this.value;
    }

    public Color getColor() {
        return (Color)this.createValue(null);
    }

    private Color getTargetColor() {
        if (this.checkState(this.targetColor, this.targetColorKey)) {
            this.targetColor = this.fetchColor(this.targetColorKey);
        }
        return this.targetColor;
    }

    private Color getBaseColor() {
        if (this.checkState(this.baseColor, this.baseColorKey)) {
            this.baseColor = this.fetchColor(this.baseColorKey);
        }
        return this.baseColor;
    }

    private Color getMustContrastColor() {
        if (this.checkState(this.mustContrastColor, this.mustContrastColorKey)) {
            this.mustContrastColor = this.fetchColor(this.mustContrastColorKey);
        }
        return this.mustContrastColor;
    }

    private Color getActualColor() {
        if (this.checkState(this.actualColor, this.actualColorKey)) {
            this.actualColor = this.fetchColor(this.actualColorKey);
        }
        return this.actualColor;
    }

    private boolean hasMustContrastColor() {
        return this.mustContrastColor != null || this.mustContrastColorKey != null;
    }

    private boolean checkState(Color color, String key) {
        if (color == null && key == null) {
            throw new NullPointerException("Both color and key are null for " + this);
        }
        return color == null;
    }

    private Color fetchColor(String key) {
        Color result = UIManager.getColor(key);
        if (result == null) {
            result = this.fallback;
        }
        return result;
    }

    static Color deriveColor(Color base, Color actual, Color target) {
        float[] baseHSB = Color.RGBtoHSB(base.getRed(), base.getGreen(), base.getBlue(), null);
        float[] targHSB = Color.RGBtoHSB(target.getRed(), target.getGreen(), target.getBlue(), null);
        float[] actualHSB = Color.RGBtoHSB(actual.getRed(), actual.getGreen(), actual.getBlue(), null);
        float[] resultHSB = new float[3];
        float[] finalHSB = new float[3];
        float[] diff = RelativeColor.percentageDiff(actualHSB, baseHSB);
        resultHSB[0] = actualHSB[0] + diff[0] * (targHSB[0] - baseHSB[0]);
        resultHSB[1] = actualHSB[1] + diff[1] * (targHSB[1] - baseHSB[1]);
        resultHSB[2] = actualHSB[2] + diff[2] * (targHSB[2] - baseHSB[2]);
        finalHSB[0] = RelativeColor.saturate(resultHSB[0]);
        finalHSB[1] = RelativeColor.saturate(resultHSB[1]);
        finalHSB[2] = RelativeColor.saturate(resultHSB[2]);
        if ((double)targHSB[1] > 0.1 && (double)resultHSB[1] <= 0.1) {
            resultHSB[1] = resultHSB[2] * 0.25f;
            resultHSB[2] = resultHSB[2] - resultHSB[2] * 0.25f;
        }
        Color result = new Color(Color.HSBtoRGB(finalHSB[0], finalHSB[1], finalHSB[2]));
        return result;
    }

    private static float[] percentageDiff(float[] a, float[] b) {
        float[] result = new float[3];
        for (int i = 0; i < 3; ++i) {
            result[i] = 1.0f - Math.abs(a[i] - b[i]);
            if (result[i] != 0.0f) continue;
            result[i] = 1.0f - a[i];
        }
        return result;
    }

    private static final void out(String nm, float[] f) {
        StringBuffer sb = new StringBuffer(nm);
        sb.append(": ");
        for (int i = 0; i < f.length; ++i) {
            sb.append(Math.round(f[i] * 100.0f));
            if (i == f.length - 1) continue;
            sb.append(',');
            sb.append(' ');
        }
        System.err.println(sb.toString());
    }

    private static float saturate(float f) {
        return Math.max(0.0f, Math.min(1.0f, f));
    }

    static Color ensureContrast(Color target, Color contrast) {
        float[] contHSB = Color.RGBtoHSB(contrast.getRed(), contrast.getGreen(), contrast.getBlue(), null);
        float[] targHSB = Color.RGBtoHSB(target.getRed(), target.getGreen(), target.getBlue(), null);
        float[] resultHSB = new float[3];
        System.arraycopy(targHSB, 0, resultHSB, 0, 3);
        float satDiff = Math.abs(targHSB[1] - contHSB[1]);
        float briDiff = Math.abs(targHSB[2] - contHSB[2]);
        if ((double)targHSB[1] > 0.6 && (double)resultHSB[1] > 0.6 || briDiff < 0.45f && satDiff < 0.4f) {
            float[] arrf = resultHSB;
            arrf[1] = arrf[1] / 3.0f;
            satDiff = Math.abs(targHSB[1] - contHSB[1]);
        }
        if ((double)briDiff < 0.3 || (double)satDiff < 0.3 && (double)briDiff < 0.5) {
            float dir = 1.5f * (0.5f - contHSB[2]);
            resultHSB[2] = RelativeColor.saturate(resultHSB[2] + dir);
        }
        Color result = new Color(Color.HSBtoRGB(resultHSB[0], resultHSB[1], resultHSB[2]));
        return result;
    }
}

