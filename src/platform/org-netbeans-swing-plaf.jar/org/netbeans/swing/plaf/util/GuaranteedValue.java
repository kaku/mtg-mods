/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.util;

import java.awt.Color;
import java.awt.Font;
import javax.swing.UIDefaults;
import javax.swing.UIManager;

public class GuaranteedValue
implements UIDefaults.LazyValue {
    private Object value;

    public GuaranteedValue(String key, Object fallback) {
        if (key == null || fallback == null) {
            throw new NullPointerException("Null parameters: " + key + ',' + fallback);
        }
        this.value = UIManager.get(key);
        if (this.value == null) {
            this.value = fallback;
        }
    }

    public GuaranteedValue(String[] keys, Object fallback) {
        if (keys == null || fallback == null) {
            throw new NullPointerException("Null parameters: " + keys + ',' + fallback);
        }
        for (int i = 0; i < keys.length; ++i) {
            this.value = UIManager.get(keys[i]);
            if (this.value != null) break;
        }
        if (this.value == null) {
            this.value = fallback;
        }
    }

    @Override
    public Object createValue(UIDefaults table) {
        return this.value;
    }

    public Color getColor() {
        Object o = this.createValue(null);
        if (o instanceof Color) {
            return (Color)o;
        }
        return null;
    }

    public Font getFont() {
        Object o = this.createValue(null);
        if (o instanceof Font) {
            return (Font)o;
        }
        return null;
    }
}

