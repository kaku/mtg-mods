/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.winclassic;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.UIManager;
import javax.swing.border.Border;

public class WinClassicTabBorder
implements Border {
    private static final Insets insets = new Insets(1, 1, 0, 1);

    @Override
    public Insets getBorderInsets(Component c) {
        return insets;
    }

    @Override
    public boolean isBorderOpaque() {
        return true;
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        g.translate(x, y);
        g.setColor(UIManager.getColor("InternalFrame.borderShadow"));
        g.drawLine(0, 0, width - 2, 0);
        g.drawLine(0, 1, 0, height - 1);
        g.setColor(UIManager.getColor("InternalFrame.borderHighlight"));
        g.drawLine(width - 1, 0, width - 1, height - 1);
        g.translate(- x, - y);
    }
}

