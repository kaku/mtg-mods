/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.winclassic;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.border.Border;

public class WinClassicCompBorder
implements Border {
    private static final Insets insets = new Insets(0, 2, 2, 2);

    @Override
    public Insets getBorderInsets(Component c) {
        return insets;
    }

    @Override
    public boolean isBorderOpaque() {
        return true;
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        int topOffset = 0;
        if (c instanceof JComponent) {
            JComponent jc = (JComponent)c;
            Integer in = (Integer)jc.getClientProperty("MultiViewBorderHack.topOffset");
            topOffset = in == null ? topOffset : in;
        }
        g.translate(x, y);
        g.setColor(UIManager.getColor("InternalFrame.borderShadow"));
        g.drawLine(0, 0, 0, height - 1);
        if (topOffset != 0) {
            g.drawLine(1, topOffset - 1, 1, topOffset);
        }
        g.setColor(UIManager.getColor("InternalFrame.borderDarkShadow"));
        g.drawLine(1, topOffset, 1, height - 2);
        g.setColor(UIManager.getColor("InternalFrame.borderHighlight"));
        g.drawLine(1, height - 1, width - 1, height - 1);
        g.drawLine(width - 1, height - 2, width - 1, 0);
        g.setColor(UIManager.getColor("InternalFrame.borderLight"));
        g.drawLine(2, height - 2, width - 2, height - 2);
        g.drawLine(width - 2, height - 3, width - 2, 0);
        g.translate(- x, - y);
    }
}

