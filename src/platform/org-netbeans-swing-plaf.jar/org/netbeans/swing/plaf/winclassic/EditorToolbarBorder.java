/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.winclassic;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.UIManager;
import javax.swing.border.AbstractBorder;

class EditorToolbarBorder
extends AbstractBorder {
    private static final Insets insets = new Insets(1, 0, 2, 0);

    EditorToolbarBorder() {
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
        g.setColor(UIManager.getColor("InternalFrame.borderShadow"));
        g.drawLine(x, y + h - 2, x + w - 1, y + h - 2);
        g.setColor(UIManager.getColor("InternalFrame.borderDarkShadow"));
        g.drawLine(x, y + h - 1, x + w - 1, y + h - 1);
    }

    @Override
    public Insets getBorderInsets(Component c) {
        return insets;
    }
}

