/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.winclassic;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import org.netbeans.swing.plaf.LFCustoms;
import org.netbeans.swing.plaf.util.GuaranteedValue;
import org.netbeans.swing.plaf.util.RelativeColor;
import org.netbeans.swing.plaf.util.UIBootstrapValue;
import org.netbeans.swing.plaf.util.UIUtils;
import org.netbeans.swing.plaf.winclassic.EditorToolbarBorder;
import org.netbeans.swing.plaf.winclassic.StatusLineBorder;
import org.netbeans.swing.plaf.winclassic.WinClassicCompBorder;
import org.netbeans.swing.plaf.winclassic.WinClassicTabBorder;

public final class WindowsLFCustoms
extends LFCustoms {
    @Override
    public Object[] createLookAndFeelCustomizationKeysAndValues() {
        int fontsize = 11;
        Integer in = (Integer)UIManager.get("customFontSize");
        if (in != null) {
            fontsize = in;
        }
        return new Object[]{"EditorPane.selectionBackground", new Color(157, 157, 255), "TextArea.font", new GuaranteedValue("Label.font", (Object)new Font("Dialog", 0, fontsize)), "Nb.Editor.ErrorStripe.ScrollBar.Insets", new Insets(17, 0, 17, 0)};
    }

    @Override
    public Object[] createApplicationSpecificKeysAndValues() {
        WinClassicPropertySheetColorings propertySheetColorings = new WinClassicPropertySheetColorings();
        Object[] result = new Object[]{"Nb.Desktop.border", new EmptyBorder(4, 2, 1, 2), "Nb.ScrollPane.border", UIManager.get("ScrollPane.border"), "Nb.Explorer.Status.border", new StatusLineBorder(2), "Nb.Explorer.Folder.icon", UIUtils.loadImage("org/netbeans/swing/plaf/resources/win-explorer-folder.gif"), "Nb.Explorer.Folder.openedIcon", UIUtils.loadImage("org/netbeans/swing/plaf/resources/win-explorer-opened-folder.gif"), "Nb.Editor.Status.leftBorder", new StatusLineBorder(6), "Nb.Editor.Status.rightBorder", new StatusLineBorder(3), "Nb.Editor.Status.innerBorder", new StatusLineBorder(7), "Nb.Editor.Toolbar.border", new EditorToolbarBorder(), "Nb.Editor.Status.onlyOneBorder", new StatusLineBorder(2), "nb.propertysheet", propertySheetColorings, "TabbedContainer.editor.contentBorder", new WinClassicCompBorder(), "TabbedContainer.editor.tabsBorder", new WinClassicTabBorder(), "TabbedContainer.view.contentBorder", new WinClassicCompBorder(), "TabbedContainer.view.tabsBorder", new WinClassicTabBorder(), "nb.desktop.splitpane.border", BorderFactory.createEmptyBorder(4, 2, 1, 2), "EditorTabDisplayerUI", "org.netbeans.swing.tabcontrol.plaf.WinClassicEditorTabDisplayerUI", "SlidingButtonUI", "org.netbeans.swing.tabcontrol.plaf.WindowsSlidingButtonUI", "ViewTabDisplayerUI", "org.netbeans.swing.tabcontrol.plaf.WinClassicViewTabDisplayerUI", "Nb.BusyIcon.Height", 14, "nb.progress.cancel.icon", UIUtils.loadImage("org/netbeans/swing/plaf/resources/cancel_task_win_classic.png"), "tab_unsel_fill", UIUtils.adjustColor(new GuaranteedValue("InternalFrame.inactiveTitleGradient", (Object)Color.GRAY).getColor(), -12, -15, -22), "tab_sel_fill", new GuaranteedValue("text", (Object)Color.WHITE), "tab_bottom_border", UIUtils.adjustColor(new GuaranteedValue("InternalFrame.borderShadow", (Object)Color.GRAY).getColor(), 20, 17, 12), "winclassic_tab_sel_gradient", new RelativeColor(new Color(7, 28, 95), new Color(152, 177, 208), "InternalFrame.activeTitleBackground"), "nbProgressBar.Foreground", new Color(49, 106, 197), "nbProgressBar.Background", Color.WHITE, "nbProgressBar.popupDynaText.foreground", new Color(141, 136, 122), "nbProgressBar.popupText.background", new Color(249, 249, 249), "nbProgressBar.popupText.foreground", UIManager.getColor("TextField.foreground"), "nbProgressBar.popupText.selectBackground", UIManager.getColor("List.selectionBackground"), "nbProgressBar.popupText.selectForeground", UIManager.getColor("List.selectionForeground"), "NbSlideBar.GroupSeparator.Gap.Before", 9, "NbSlideBar.GroupSeparator.Gap.After", 1, "NbSlideBar.RestoreButton.Gap", 1, "Nb.browser.picker.background.light", new Color(255, 255, 255), "Nb.browser.picker.foreground.light", new Color(130, 130, 130)};
        return UIUtils.addInputMapsWithoutCtrlPageUpAndCtrlPageDown(result);
    }

    @Override
    public Object[] createGuaranteedKeysAndValues() {
        return new Object[]{"InternalFrame.activeTitleBackground", new GuaranteedValue("InternalFrame.activeTitleBackground", (Object)Color.BLUE), "InternalFrame.borderShadow", new GuaranteedValue("InternalFrame.borderShadow", (Object)Color.gray), "InternalFrame.borderHighlight", new GuaranteedValue("InternalFrame.borderHighlight", (Object)Color.white), "InternalFrame.borderDarkShadow", new GuaranteedValue("InternalFrame.borderDarkShadow", (Object)Color.darkGray), "InternalFrame.borderLight", new GuaranteedValue("InternalFrame.borderLight", (Object)Color.lightGray), "TabbedPane.background", new GuaranteedValue("TabbedPane.background", (Object)Color.LIGHT_GRAY), "TabbedPane.focus", new GuaranteedValue("TabbedPane.focus", (Object)Color.GRAY), "TabbedPane.highlight", new GuaranteedValue("TabbedPane.highlight", (Object)Color.WHITE), "Button.dashedRectGapX", new GuaranteedValue("Button.dashedRectGapX", (Object)5), "Button.dashedRectGapY", new GuaranteedValue("Button.dashedRectGapY", (Object)4), "Button.dashedRectGapWidth", new GuaranteedValue("Button.dashedRectGapWidth", (Object)10), "Button.dashedRectGapHeight", new GuaranteedValue("Button.dashedRectGapHeight", (Object)8), "Tree.expandedIcon", new TreeIcon(false), "Tree.collapsedIcon", new TreeIcon(true)};
    }

    @Override
    protected Object[] additionalKeys() {
        Object[] kv = new WinClassicPropertySheetColorings().createKeysAndValues();
        Object[] result = new Object[kv.length / 2];
        int ct = 0;
        for (int i = 0; i < kv.length; i += 2) {
            result[ct] = kv[i];
            ++ct;
        }
        return result;
    }

    private class WinClassicPropertySheetColorings
    extends UIBootstrapValue.Lazy {
        public WinClassicPropertySheetColorings() {
            super(null);
        }

        @Override
        public Object[] createKeysAndValues() {
            return new Object[]{"PropSheet.selectionBackground", new Color(10, 36, 106), "PropSheet.selectionForeground", Color.WHITE, "PropSheet.setBackground", new Color(237, 233, 225), "PropSheet.setForeground", Color.BLACK, "PropSheet.selectedSetBackground", new Color(10, 36, 106), "PropSheet.selectedSetForeground", Color.WHITE, "PropSheet.disabledForeground", new Color(128, 128, 128), "netbeans.ps.buttonColor", UIManager.getColor("control")};
        }
    }

    private static class TreeIcon
    implements Icon {
        private static final int HALF_SIZE = 4;
        private static final int SIZE = 9;
        private boolean collapsed;

        public TreeIcon(boolean collapsed) {
            this.collapsed = collapsed;
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            g.setColor(Color.WHITE);
            g.fillRect(x, y, 8, 8);
            g.setColor(Color.GRAY);
            g.drawRect(x, y, 8, 8);
            g.setColor(Color.BLACK);
            g.drawLine(x + 2, y + 4, x + 6, y + 4);
            if (this.collapsed) {
                g.drawLine(x + 4, y + 2, x + 4, y + 6);
            }
        }

        @Override
        public int getIconWidth() {
            return 9;
        }

        @Override
        public int getIconHeight() {
            return 9;
        }
    }

}

