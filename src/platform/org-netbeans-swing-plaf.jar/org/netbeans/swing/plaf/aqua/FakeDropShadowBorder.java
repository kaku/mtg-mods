/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.aqua;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.swing.border.Border;

public class FakeDropShadowBorder
implements Border {
    private static final int TOP = 8;
    private static final int BOTTOM = 8;
    private static final int LEFT = 8;
    private static final int RIGHT = 8;
    private final Insets insets;
    private static final String upLeft = "border_top_left.png";
    private static final String downRight = "border_bottom_right.png";
    private static final String downLeft = "border_bottom_left.png";
    private static final String upRight = "border_top_right.png";
    private static final String bottom = "border_bottom.png";
    private static final String leftEdge = "border_left.png";
    private static final String rightEdge = "border_right.png";
    private static final String top = "border_top.png";
    private static Map<String, BufferedImage> imgs = new HashMap<String, BufferedImage>();

    private FakeDropShadowBorder(Insets insets) {
        this.insets = insets;
    }

    public static Border createDefault() {
        return new FakeDropShadowBorder(new Insets(8, 8, 8, 8));
    }

    public static Border createLeftBorder() {
        return new FakeDropShadowBorder(new Insets(0, 0, 8, 8));
    }

    public static Border createRightBorder() {
        return new FakeDropShadowBorder(new Insets(0, 8, 8, 0));
    }

    public static Border createBottomBorder() {
        return new FakeDropShadowBorder(new Insets(8, 8, 0, 8));
    }

    public static Border createTopBorder() {
        return new FakeDropShadowBorder(new Insets(0, 8, 8, 8));
    }

    @Override
    public Insets getBorderInsets(Component c) {
        return new Insets(this.insets.top, this.insets.left, this.insets.bottom, this.insets.right);
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
        Graphics2D gg = (Graphics2D)g;
        BufferedImage b = null;
        if (this.insets.top > 0 && this.insets.left > 0) {
            b = FakeDropShadowBorder.getImage("border_top_left.png");
            gg.drawImage(b, x, y, null);
        }
        if (this.insets.right > 0 && this.insets.bottom > 0) {
            b = FakeDropShadowBorder.getImage("border_bottom_right.png");
            int xPos = x + w - b.getWidth();
            if (this.insets.right == 0) {
                xPos += 8;
            }
            gg.drawImage(b, xPos, y + h - b.getHeight(), null);
        }
        if (this.insets.top > 0 && this.insets.right > 0) {
            b = FakeDropShadowBorder.getImage("border_top_right.png");
            gg.drawImage(b, x + w - b.getWidth(), y, null);
        }
        if (this.insets.left > 0 && this.insets.bottom > 0) {
            b = FakeDropShadowBorder.getImage("border_bottom_left.png");
            if (this.insets.left == 0) {
                x -= 8;
            }
            gg.drawImage(b, x, y + h - b.getHeight(), null);
        }
        if (this.insets.left > 0) {
            b = FakeDropShadowBorder.getImage("border_left.png");
            gg.drawImage(b, x, y + this.insets.top, b.getWidth(), h - this.insets.top - this.insets.bottom, null);
        }
        if (this.insets.right > 0) {
            b = FakeDropShadowBorder.getImage("border_right.png");
            gg.drawImage(b, x + w - b.getWidth(), y + this.insets.top, b.getWidth(), h - this.insets.top - this.insets.bottom, null);
        }
        if (this.insets.bottom > 0) {
            b = FakeDropShadowBorder.getImage("border_bottom.png");
            gg.drawImage(b, x + this.insets.left, y + h - b.getHeight(), x + w - this.insets.left - this.insets.right, b.getHeight(), null);
        }
        if (this.insets.top > 0) {
            b = FakeDropShadowBorder.getImage("border_top.png");
            gg.drawImage(b, x + this.insets.left, y, x + w - this.insets.left - this.insets.right, b.getHeight(), null);
        }
    }

    @Override
    public boolean isBorderOpaque() {
        return false;
    }

    private static BufferedImage getImage(String s) {
        BufferedImage result = imgs.get(s);
        if (result == null) {
            Exception e1 = null;
            try {
                result = ImageIO.read(FakeDropShadowBorder.class.getResourceAsStream(s));
            }
            catch (Exception e) {
                result = new BufferedImage(1, 1, 2);
                e1 = e;
            }
            imgs.put(s, result);
            if (e1 != null) {
                throw new IllegalStateException(e1);
            }
        }
        return result;
    }
}

