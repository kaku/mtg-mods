/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.aqua;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;
import javax.swing.plaf.FontUIResource;
import org.netbeans.swing.plaf.LFCustoms;
import org.netbeans.swing.plaf.aqua.FakeDropShadowBorder;
import org.netbeans.swing.plaf.util.GuaranteedValue;
import org.netbeans.swing.plaf.util.UIUtils;

public final class AquaLFCustoms
extends LFCustoms {
    @Override
    public Object[] createLookAndFeelCustomizationKeysAndValues() {
        Object[] result;
        Integer cus = (Integer)UIManager.get("customFontSize");
        if (cus != null) {
            int uiFontSize = cus;
            Font controlFont = new GuaranteedValue(new String[]{"controlFont", "Tree.font", "Label.font"}, (Object)new FontUIResource("Dialog", 0, uiFontSize)).getFont();
            result = new Object[]{"Button.font", controlFont, "Tree.font", controlFont, "ToggleButton.font", controlFont, "Menu.font", controlFont, "MenuBar.font", controlFont, "MenuItem.font", controlFont, "CheckBoxMenuItem.font", controlFont, "RadioButtonMenuItem.font", controlFont, "PopupMenu.font", controlFont, "List.font", controlFont, "Label.font", controlFont, "ComboBox.font", controlFont, "PopupMenuSeparatorUI", "org.netbeans.swing.plaf.aqua.AquaSeparatorUI", "SeparatorUI", "org.netbeans.swing.plaf.aqua.AquaSeparatorUI", "PopupMenu.border", BorderFactory.createEmptyBorder(4, 0, 4, 0), "SlidingButtonUI", "org.netbeans.swing.tabcontrol.plaf.AquaSlidingButtonUI", "Nb.Editor.ErrorStripe.ScrollBar.Insets", new Insets(18, 0, 18, 0)};
        } else {
            result = new Object[]{"controlFont", new GuaranteedValue(new String[]{"Label.font", "Tree.font"}, (Object)new FontUIResource("Dialog", 0, 14)).getFont(), "PopupMenuSeparatorUI", "org.netbeans.swing.plaf.aqua.AquaSeparatorUI", "SeparatorUI", "org.netbeans.swing.plaf.aqua.AquaSeparatorUI", "PopupMenu.border", BorderFactory.createEmptyBorder(4, 0, 4, 0), "SlidingButtonUI", "org.netbeans.swing.tabcontrol.plaf.AquaSlidingButtonUI", "Nb.Editor.ErrorStripe.ScrollBar.Insets", new Insets(18, 0, 18, 0)};
        }
        return result;
    }

    @Override
    public Object[] createApplicationSpecificKeysAndValues() {
        MatteBorder topOnly = BorderFactory.createMatteBorder(1, 0, 0, 0, UIManager.getColor("controlShadow").brighter());
        Border empty = BorderFactory.createEmptyBorder();
        Image explorerIcon = null;
        Icon treeIcon = UIManager.getIcon("Tree.closedIcon");
        explorerIcon = null != treeIcon ? AquaLFCustoms.icon2Image(treeIcon) : UIUtils.loadImage("org/netbeans/swing/plaf/resources/osx-folder.png");
        Object[] result = new Object[]{"Nb.Toolbar.ui", "org.netbeans.swing.plaf.aqua.PlainAquaToolbarUI", "Nb.Desktop.background", new Color(226, 223, 214), "Nb.ScrollPane.Border.color", new Color(127, 157, 185), "Nb.Explorer.Folder.icon", explorerIcon, "Nb.Explorer.Folder.openedIcon", explorerIcon, "Nb.Desktop.border", empty, "Nb.ScrollPane.border", UIManager.get("ScrollPane.border"), "Nb.Explorer.Status.border", topOnly, "Nb.Editor.Status.leftBorder", topOnly, "Nb.Editor.Status.rightBorder", topOnly, "Nb.Editor.Status.innerBorder", topOnly, "Nb.Editor.Status.onlyOneBorder", topOnly, "Nb.Editor.Toolbar.border", BorderFactory.createMatteBorder(0, 0, 1, 0, new Color(101, 101, 101)), "TabbedContainer.editor.outerBorder", BorderFactory.createEmptyBorder(), "TabbedContainer.view.outerBorder", BorderFactory.createEmptyBorder(), "EditorTabDisplayerUI", "org.netbeans.swing.tabcontrol.plaf.AquaEditorTabDisplayerUI", "ViewTabDisplayerUI", "org.netbeans.swing.tabcontrol.plaf.AquaViewTabDisplayerUI", "IndexButtonUI", "org.netbeans.swing.tabcontrol.plaf.SlidingTabDisplayerButtonUI$Aqua", "NbTabControl.focusedTabBackground", new Color(135, 189, 255), "NbTabControl.selectedTabBrighterBackground", new Color(252, 252, 252), "NbTabControl.selectedTabDarkerBackground", new Color(226, 226, 226), "NbTabControl.mouseoverTabBrighterBackground", new Color(194, 194, 194), "NbTabControl.mouseoverTabDarkerBackground", new Color(156, 156, 156), "NbTabControl.inactiveTabBrighterBackground", new Color(220, 220, 200), "NbTabControl.inactiveTabDarkerBackground", new Color(178, 178, 178), "NbTabControl.borderColor", new Color(49, 49, 49), "NbTabControl.borderShadowColor", new Color(178, 178, 178), "NbTabControl.borderDarkShadowColor", new Color(121, 121, 121), "NbTabControl.editorTabBackground", new Color(101, 101, 101), "NbTabControl.editorBorderShadowColor", new Color(121, 121, 121), "nb.explorer.ministatusbar.border", BorderFactory.createEmptyBorder(), "floatingBorder", FakeDropShadowBorder.createDefault(), "floatingBorder-left", FakeDropShadowBorder.createLeftBorder(), "floatingBorder-right", FakeDropShadowBorder.createRightBorder(), "floatingBorder-bottom", FakeDropShadowBorder.createBottomBorder(), "floatingBorder-top", FakeDropShadowBorder.createTopBorder(), "TabRenderer.selectedActivatedForeground", new GuaranteedValue("textText", (Object)Color.BLACK), "NbSplitPane.dividerSize", new Integer(3), "NbSplitPane.background", new Color(101, 101, 101), "nb.desktop.splitpane.border", BorderFactory.createMatteBorder(1, 0, 0, 0, new Color(49, 49, 49)), "nb.desktop.view.insets", new Insets(0, 0, 0, 0), "Nb.SplitPane.dividerSize.vertical", new Integer(3), "Nb.SplitPane.dividerSize.horizontal", new Integer(3), "NbExplorerView.quicksearch.border", new Color(64, 64, 64), "NbExplorerView.quicksearch.background.top", new Color(197, 197, 197), "NbExplorerView.quicksearch.background.bottom", new Color(150, 150, 150), "NbExplorerView.background", new Color(226, 226, 226), "NbEditorStatusBar.background", new Color(226, 226, 226), "NbEditorGlyphGutter.background", new Color(255, 255, 255), "NbSlideBar.rollover", new Color(135, 135, 135), "NbBrushedMetal.darkShadow", new Color(49, 49, 49), "NbBrushedMetal.lightShadow", new Color(178, 178, 178), "PropSheet.selectedSetBackground", new Color(181, 213, 255), "PropSheet.selectionBackground", new Color(181, 213, 255), "PropSheet.setBackground", new Color(226, 226, 226), "PropSheet.selectionForeground", UIManager.getColor("Table.foreground"), "Tree.paintLines", Boolean.FALSE, "nbProgressBar.Foreground", new Color(49, 106, 197), "nbProgressBar.Background", Color.WHITE, "nbProgressBar.popupDynaText.foreground", new Color(141, 136, 122), "nbProgressBar.popupText.background", new Color(249, 249, 249), "nbProgressBar.popupText.foreground", UIManager.getColor("TextField.foreground"), "nbProgressBar.popupText.selectBackground", UIManager.getColor("List.selectionBackground"), "nbProgressBar.popupText.selectForeground", UIManager.getColor("List.selectionForeground"), "nb.progress.cancel.icon", UIUtils.loadImage("org/netbeans/swing/plaf/resources/cancel_task_linux_mac.png"), "NbSlideBar.GroupSeparator.Gap.Before", 1, "NbSlideBar.GroupSeparator.Gap.After", 1, "NbSlideBar.RestoreButton.Gap", 3, "Nb.MainWindow.Toolbar.Border", BorderFactory.createEmptyBorder(0, 0, 0, 3), "Nb.MenuBar.VerticalAlign", Boolean.FALSE, "Nb.SplitPaneUI.clean", "org.netbeans.swing.plaf.aqua.CleanSplitPaneUI", "Nb.browser.picker.background.light", new Color(233, 239, 248), "Nb.browser.picker.foreground.light", new Color(130, 130, 130)};
        return result;
    }

    private static final Image icon2Image(Icon icon) {
        BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), 2);
        Graphics g = image.getGraphics();
        icon.paintIcon(new JLabel(), g, 0, 0);
        g.dispose();
        return image;
    }
}

