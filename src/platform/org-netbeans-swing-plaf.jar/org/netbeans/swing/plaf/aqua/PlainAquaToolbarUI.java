/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.aqua;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.ButtonUI;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicToolBarUI;
import org.netbeans.swing.plaf.aqua.AquaToolBarButtonUI;

public class PlainAquaToolbarUI
extends BasicToolBarUI
implements ContainerListener {
    private static final ButtonUI mainButtonui = new AquaToolBarButtonUI(true);
    private static final ButtonUI buttonui = new AquaToolBarButtonUI(false);

    public static ComponentUI createUI(JComponent c) {
        return new PlainAquaToolbarUI();
    }

    @Override
    public void installUI(JComponent c) {
        super.installUI(c);
        c.addContainerListener(this);
        boolean isEditorToolbar = "editorToolbar".equals(c.getName());
        c.setBackground(UIManager.getColor("NbExplorerView.background"));
        c.setOpaque(true);
        this.installButtonUIs(c, isEditorToolbar);
    }

    @Override
    public void uninstallUI(JComponent c) {
        super.uninstallUI(c);
        c.setBorder(null);
        c.removeContainerListener(this);
    }

    @Override
    public void setFloating(boolean b, Point p) {
    }

    private void installButtonUI(Component c, boolean isEditorToolbar) {
        if (c instanceof AbstractButton) {
            ((AbstractButton)c).setUI(isEditorToolbar ? buttonui : mainButtonui);
        }
        if (c instanceof JComponent) {
            ((JComponent)c).setOpaque(false);
        }
    }

    private void installButtonUIs(Container parent, boolean isEditorToolbar) {
        Component[] c = parent.getComponents();
        for (int i = 0; i < c.length; ++i) {
            this.installButtonUI(c[i], isEditorToolbar);
        }
    }

    @Override
    public void componentAdded(ContainerEvent e) {
        Container c = (Container)e.getSource();
        boolean isEditorToolbar = "editorToolbar".equals(c.getName());
        this.installButtonUI(e.getChild(), isEditorToolbar);
        if (isEditorToolbar) {
            Dimension min = new Dimension(32, 34);
            ((JComponent)e.getContainer()).setPreferredSize(min);
        }
    }

    @Override
    public void componentRemoved(ContainerEvent e) {
    }
}

