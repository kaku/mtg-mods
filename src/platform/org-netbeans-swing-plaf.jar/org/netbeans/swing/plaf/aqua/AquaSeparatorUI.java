/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.aqua;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JComponent;
import javax.swing.JSeparator;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.SeparatorUI;

public class AquaSeparatorUI
extends SeparatorUI {
    private static final Color lineColorHorizontal = new Color(215, 215, 215);
    private static final Color lineColorVertical = new Color(128, 128, 128);
    private static ComponentUI separatorui = new AquaSeparatorUI();

    public static ComponentUI createUI(JComponent c) {
        return separatorui;
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        Dimension s = c.getSize();
        if (((JSeparator)c).getOrientation() == 0) {
            g.setColor(lineColorHorizontal);
            g.drawLine(1, 5, s.width - 2, 5);
        } else {
            g.setColor(lineColorVertical);
            g.drawLine(0, 1, 0, s.height - 2);
        }
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        if (((JSeparator)c).getOrientation() == 0) {
            return new Dimension(0, 12);
        }
        return new Dimension(1, 11);
    }

    @Override
    public Dimension getMinimumSize(JComponent c) {
        return null;
    }

    @Override
    public Dimension getMaximumSize(JComponent c) {
        return null;
    }
}

