/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.aqua;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicButtonListener;
import javax.swing.plaf.basic.BasicButtonUI;

class AquaToolBarButtonUI
extends BasicButtonUI
implements ChangeListener {
    private final boolean isMainToolbarButtonUI;
    private static BasicButtonListener listener = new BasicButtonListener(null);
    private final Rectangle scratch = new Rectangle();
    private FontMetrics fm = null;
    private static final int minButtonSize = 16;

    public AquaToolBarButtonUI(boolean isMainToolbar) {
        this.isMainToolbarButtonUI = isMainToolbar;
    }

    @Override
    public void installUI(JComponent c) {
        AbstractButton b = (AbstractButton)c;
        b.addMouseListener(listener);
        b.addChangeListener(this);
        b.setContentAreaFilled(false);
        b.setOpaque(false);
        b.setFocusable(false);
        b.setBorderPainted(true);
        if (this.isMainToolbarButtonUI) {
            b.setBorder(BorderFactory.createEmptyBorder(4, 6, 4, 6));
        } else {
            b.setBorder(BorderFactory.createEmptyBorder(2, 6, 2, 6));
        }
        b.setRolloverEnabled(this.isMainToolbarButtonUI);
    }

    @Override
    public void uninstallUI(JComponent c) {
        c.removeMouseListener(listener);
        if (c instanceof AbstractButton) {
            ((AbstractButton)c).removeChangeListener(this);
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        ((AbstractButton)e.getSource()).repaint();
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        Rectangle r = c.getBounds(this.scratch);
        AbstractButton b = (AbstractButton)c;
        r.x = 0;
        r.y = 0;
        Paint temp = ((Graphics2D)g).getPaint();
        this.paintBackground((Graphics2D)g, b, r);
        this.paintIcon(g, b, r);
        this.paintText(g, b, r);
        ((Graphics2D)g).setPaint(temp);
    }

    private void paintText(Graphics g, AbstractButton b, Rectangle r) {
        String s = b.getText();
        if (s == null || s.length() == 0) {
            return;
        }
        g.setColor(b.getForeground());
        Font f = b.getFont();
        if (b.isSelected()) {
            f = new Font(f.getName(), 1, f.getSize());
        }
        g.setFont(f);
        if (g instanceof Graphics2D) {
            ((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }
        FontMetrics fontMetrics = g.getFontMetrics();
        if (this.fm == null) {
            this.fm = fontMetrics;
        }
        int x = 0;
        Icon ic = b.getIcon();
        if (ic != null) {
            x = ic.getIconWidth() + b.getIconTextGap() + 2;
        } else {
            int w = fontMetrics.stringWidth(s);
            if (w <= r.width) {
                x = r.width / 2 - w / 2;
            }
        }
        int h = fontMetrics.getHeight();
        int y = fontMetrics.getMaxAscent();
        if (h <= r.height) {
            y += r.height / 2 - h / 2;
        }
        g.drawString(s, x, y);
    }

    private void paintBackground(Graphics2D g, AbstractButton b, Rectangle r) {
        if (!b.isSelected() && !b.getModel().isPressed()) {
            return;
        }
        Color c = this.isMainToolbarButtonUI ? UIManager.getColor("NbBrushedMetal.lightShadow") : b.getParent().getBackground();
        Color darker = this.makeDarker(c);
        Paint p = g.getPaint();
        g.setPaint(new GradientPaint(r.x, r.y, c, r.x, r.height / 2, darker));
        g.fillRect(r.x, r.y, r.width, r.height / 2);
        g.setPaint(new GradientPaint(r.x, r.y + r.height / 2 - 1, darker, r.x, r.height, c));
        g.fillRect(r.x, r.y + r.height / 2 - 1, r.width, r.height);
        Color evenDarker = this.makeDarker(darker);
        g.setPaint(new GradientPaint(r.x, r.y, darker, r.x, r.height / 2, evenDarker));
        g.fillRect(r.x, r.y, 1, r.height / 2);
        g.fillRect(r.x + r.width - 1, r.y, 1, r.height / 2);
        g.setPaint(new GradientPaint(r.x, r.y + r.height / 2 - 1, evenDarker, r.x, r.height, darker));
        g.fillRect(r.x, r.y + r.height / 2 - 1, 1, r.height);
        g.fillRect(r.x + r.width - 1, r.y + r.height / 2 - 1, 1, r.height);
        g.setPaint(new GradientPaint(r.x, r.y, c, r.x, r.height / 2, evenDarker));
        g.fillRect(r.x + 1, r.y, 1, r.height / 2);
        g.fillRect(r.x + r.width - 2, r.y, 1, r.height / 2);
        g.setPaint(new GradientPaint(r.x, r.y + r.height / 2 - 1, evenDarker, r.x, r.height, c));
        g.fillRect(r.x + 1, r.y + r.height / 2 - 1, 1, r.height);
        g.fillRect(r.x + r.width - 2, r.y + r.height / 2 - 1, 1, r.height);
    }

    private Color makeDarker(Color c) {
        int factor = 30;
        return new Color(Math.max(c.getRed() - factor, 0), Math.max(c.getGreen() - factor, 0), Math.max(c.getRed() - factor, 0));
    }

    private void paintIcon(Graphics g, AbstractButton b, Rectangle r) {
        boolean noText;
        Icon ic = this.getIconForState(b);
        boolean bl = noText = b.getText() == null || b.getText().length() == 0;
        if (ic != null) {
            int iconX = 0;
            int iconY = 0;
            int iconW = ic.getIconWidth();
            int iconH = ic.getIconHeight();
            if (iconW <= r.width && noText) {
                iconX = r.width / 2 - iconW / 2;
            }
            if (iconH <= r.height) {
                iconY = r.height / 2 - iconH / 2;
            }
            Graphics2D g2d = (Graphics2D)g;
            ic.paintIcon(b, g, iconX, iconY);
            if (this.isMainToolbarButtonUI && b.getModel().isRollover() && !b.getModel().isPressed()) {
                Composite composite = g2d.getComposite();
                g2d.setComposite(AlphaComposite.getInstance(8, 0.2f));
                ic.paintIcon(b, g, iconX, iconY);
                g2d.setComposite(composite);
            }
        }
    }

    private Icon getIconForState(AbstractButton b) {
        ButtonModel mdl = b.getModel();
        Icon result = null;
        if (!b.isEnabled()) {
            Icon icon = result = mdl.isSelected() ? b.getDisabledSelectedIcon() : b.getDisabledIcon();
            if (result == null && mdl.isSelected()) {
                result = b.getDisabledIcon();
            }
        } else {
            if (mdl.isArmed() && !mdl.isPressed() && (result = mdl.isSelected() ? b.getRolloverSelectedIcon() : b.getRolloverIcon()) == null & mdl.isSelected()) {
                result = b.getRolloverIcon();
            }
            if (mdl.isPressed()) {
                result = b.getPressedIcon();
            } else if (mdl.isSelected()) {
                result = b.getSelectedIcon();
            }
        }
        if (result == null) {
            result = b.getIcon();
        }
        return result;
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        Insets in;
        Dimension result;
        AbstractButton b = (AbstractButton)c;
        boolean noText = b.getText() == null || b.getText().length() == 0;
        Icon ic = this.getIconForState((AbstractButton)c);
        int w = 16;
        Dimension dimension = ic == null ? new Dimension(noText ? 32 : 0, 16) : (result = new Dimension(Math.max(w, ic.getIconWidth() + 1), Math.max(16, ic.getIconHeight() + 1)));
        if (!noText) {
            FontMetrics fm = this.fm;
            if (fm == null && c.getGraphicsConfiguration() != null) {
                fm = c.getGraphicsConfiguration().createCompatibleImage(1, 1).getGraphics().getFontMetrics(c.getFont());
            }
            if (fm == null) {
                fm = new BufferedImage(1, 1, 1).getGraphics().getFontMetrics(c.getFont());
            }
            result.width += fm.stringWidth(b.getText());
        }
        if (null != (in = b.getInsets())) {
            result.width += in.left + in.right;
            result.height += in.top + in.bottom;
        }
        return result;
    }
}

