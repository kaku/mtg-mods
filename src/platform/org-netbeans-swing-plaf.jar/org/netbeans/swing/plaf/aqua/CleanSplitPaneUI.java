/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.aqua;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;

public class CleanSplitPaneUI
extends BasicSplitPaneUI {
    @Override
    protected void installDefaults() {
        super.installDefaults();
        this.divider.setBorder(new SplitBorder());
    }

    public static ComponentUI createUI(JComponent x) {
        return new CleanSplitPaneUI();
    }

    private static class SplitBorder
    implements Border {
        private Color bkColor = UIManager.getColor("NbSplitPane.background");

        private SplitBorder() {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return new Insets(0, 0, 0, 0);
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            g.setColor(this.bkColor);
            g.fillRect(x, y, width, height);
        }
    }

}

