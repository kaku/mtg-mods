/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.nimbus;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.UIManager;
import javax.swing.border.AbstractBorder;

class NimbusScrollPaneBorder
extends AbstractBorder {
    private static final Insets insets = new Insets(1, 1, 2, 2);

    NimbusScrollPaneBorder() {
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
        g.translate(x, y);
        Color color = UIManager.getColor("controlShadow");
        g.setColor(color == null ? Color.darkGray : color);
        g.drawRect(0, 0, w - 2, h - 2);
        color = UIManager.getColor("controlHighlight");
        g.setColor(color == null ? Color.white : color);
        g.drawLine(w - 1, 1, w - 1, h - 1);
        g.drawLine(1, h - 1, w - 1, h - 1);
        g.translate(- x, - y);
    }

    @Override
    public Insets getBorderInsets(Component c) {
        return insets;
    }
}

