/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.nimbus;

import java.awt.Color;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;
import org.netbeans.swing.plaf.LFCustoms;
import org.netbeans.swing.plaf.util.UIUtils;

public final class NimbusLFCustoms
extends LFCustoms {
    @Override
    public Object[] createLookAndFeelCustomizationKeysAndValues() {
        int fontsize = 11;
        Integer in = (Integer)UIManager.get("customFontSize");
        if (in != null) {
            fontsize = in;
        }
        Font controlFont = new Font("Dialog", 0, fontsize);
        Object[] result = new Object[]{"JXDateTimePicker.arrowIcon", UIUtils.loadImage("org/netbeans/swing/plaf/resources/nimbus_expander.png")};
        return result;
    }

    @Override
    public Object[] createApplicationSpecificKeysAndValues() {
        Object[] result = new Object[]{"EditorTabDisplayerUI", "org.netbeans.swing.tabcontrol.plaf.NimbusEditorTabDisplayerUI", "ViewTabDisplayerUI", "org.netbeans.swing.tabcontrol.plaf.NimbusViewTabDisplayerUI", "IndexButtonUI", "org.netbeans.swing.tabcontrol.plaf.SlidingTabDisplayerButtonUI", "SlidingButtonUI", "org.netbeans.swing.tabcontrol.plaf.NimbusSlidingButtonUI", "Nb.ScrollPane.border", new JScrollPane().getViewportBorder(), "TabbedContainer.editor.outerBorder", BorderFactory.createEmptyBorder(), "TabbedContainer.editor.contentBorder", new MatteBorder(0, 1, 1, 1, UIManager.getColor("nimbusBorder")), "TabbedContainer.editor.tabsBorder", BorderFactory.createEmptyBorder(), "TabbedContainer.view.outerBorder", BorderFactory.createEmptyBorder(), "TabbedContainer.view.contentBorder", new MatteBorder(0, 1, 1, 1, UIManager.getColor("nimbusBorder")), "TabbedContainer.view.tabsBorder", BorderFactory.createEmptyBorder(), "NbSlideBar.GroupSeparator.Gap.Before", 12, "NbSlideBar.GroupSeparator.Gap.After", 4, "NbSlideBar.RestoreButton.Gap", 8, "Nb.EmptyEditorArea.border", BorderFactory.createEtchedBorder(1), "Nb.browser.picker.background.light", new Color(249, 249, 249), "Nb.browser.picker.foreground.light", new Color(130, 130, 130)};
        return UIUtils.addInputMapsWithoutCtrlPageUpAndCtrlPageDown(result);
    }
}

