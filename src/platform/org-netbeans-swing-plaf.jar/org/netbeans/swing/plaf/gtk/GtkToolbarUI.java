/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.gtk;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Point;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.ButtonUI;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicToolBarUI;
import org.netbeans.swing.plaf.gtk.GtkToolBarButtonUI;

public class GtkToolbarUI
extends BasicToolBarUI
implements ContainerListener {
    private static final ButtonUI buttonui = new GtkToolBarButtonUI();

    private GtkToolbarUI() {
    }

    public static ComponentUI createUI(JComponent c) {
        return new GtkToolbarUI();
    }

    @Override
    public void installUI(JComponent c) {
        super.installUI(c);
        c.setOpaque(false);
        c.addContainerListener(this);
        this.installButtonUIs(c);
    }

    @Override
    public void uninstallUI(JComponent c) {
        super.uninstallUI(c);
        c.setBorder(null);
        c.removeContainerListener(this);
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        GradientPaint gp = new GradientPaint(0.0f, 0.0f, UIManager.getColor("controlHighlight"), 0.0f, c.getHeight(), UIManager.getColor("control"));
        ((Graphics2D)g).setPaint(gp);
        Insets ins = c.getInsets();
        g.fillRect(ins.left, ins.top, c.getWidth() - (ins.left + ins.top), c.getHeight() - (ins.top + ins.bottom));
    }

    @Override
    protected Border createRolloverBorder() {
        return BorderFactory.createEmptyBorder(2, 2, 2, 2);
    }

    @Override
    protected Border createNonRolloverBorder() {
        return this.createRolloverBorder();
    }

    private Border createNonRolloverToggleBorder() {
        return this.createRolloverBorder();
    }

    @Override
    protected void setBorderToRollover(Component c) {
        if (c instanceof AbstractButton) {
            ((AbstractButton)c).setBorderPainted(false);
            ((AbstractButton)c).setBorder(BorderFactory.createEmptyBorder());
            ((AbstractButton)c).setContentAreaFilled(false);
            ((AbstractButton)c).setOpaque(false);
        }
        if (c instanceof JComponent) {
            ((JComponent)c).setOpaque(false);
        }
    }

    @Override
    protected void setBorderToNormal(Component c) {
        if (c instanceof AbstractButton) {
            ((AbstractButton)c).setBorderPainted(false);
            ((AbstractButton)c).setContentAreaFilled(false);
            ((AbstractButton)c).setOpaque(false);
        }
        if (c instanceof JComponent) {
            ((JComponent)c).setOpaque(false);
        }
    }

    @Override
    public void setFloating(boolean b, Point p) {
    }

    private void installButtonUI(Component c) {
        if (c instanceof AbstractButton) {
            ((AbstractButton)c).setUI(buttonui);
        }
        if (c instanceof JComponent) {
            ((JComponent)c).setOpaque(false);
        }
    }

    private void installButtonUIs(Container parent) {
        Component[] c = parent.getComponents();
        for (int i = 0; i < c.length; ++i) {
            this.installButtonUI(c[i]);
        }
    }

    @Override
    public void componentAdded(ContainerEvent e) {
        this.installButtonUI(e.getChild());
    }

    @Override
    public void componentRemoved(ContainerEvent e) {
    }
}

