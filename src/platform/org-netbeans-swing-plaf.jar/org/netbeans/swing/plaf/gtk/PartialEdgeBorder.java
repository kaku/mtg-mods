/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.gtk;

import java.awt.Color;
import java.awt.Component;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.netbeans.swing.plaf.gtk.AdaptiveMatteBorder;
import org.netbeans.swing.plaf.util.UIUtils;

public class PartialEdgeBorder
implements Border {
    private Insets ins;

    public PartialEdgeBorder(int width) {
        this.ins = new Insets(0, 0, 0, width);
    }

    @Override
    public Insets getBorderInsets(Component c) {
        return this.ins;
    }

    @Override
    public boolean isBorderOpaque() {
        return false;
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        Color ctrl = UIManager.getColor("control");
        Color base = UIManager.getColor("controlShadow");
        GradientPaint gp = UIUtils.getGradientPaint(x + width - this.ins.right, y + height / 2, ctrl, x + width - this.ins.right, y + height, base, false);
        ((Graphics2D)g).setPaint(gp);
        g.drawLine(x + width - this.ins.right, y + height / 2, x + width - this.ins.right, y + height);
        for (int i = 1; i < this.ins.right - 1; ++i) {
            Color curr = AdaptiveMatteBorder.colorTowards(base, ctrl, this.ins.right, i + 1);
            int xpos = x + width - this.ins.right + i;
            int ypos = y + height / 3 + i * 2;
            gp = UIUtils.getGradientPaint(xpos, ypos, ctrl, xpos, y + height, curr, false);
            ((Graphics2D)g).setPaint(gp);
            g.drawLine(xpos, ypos, xpos, y + height);
        }
    }
}

