/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.gtk;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JScrollPane;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.metal.MetalIconFactory;
import org.netbeans.swing.plaf.LFCustoms;
import org.netbeans.swing.plaf.gtk.InsetBorder;
import org.netbeans.swing.plaf.gtk.ThemeValue;
import org.netbeans.swing.plaf.util.UIUtils;

public class GtkLFCustoms
extends LFCustoms {
    private Object light = new ThemeValue(ThemeValue.REGION_PANEL, ThemeValue.WHITE, Color.GRAY);
    private static Object control = new ThemeValue(ThemeValue.REGION_PANEL, ThemeValue.MID, Color.GRAY);
    private Object controlFont = new ThemeValue(ThemeValue.REGION_TAB, new FontUIResource("Dialog", 0, 11));

    @Override
    public Object[] createApplicationSpecificKeysAndValues() {
        Color borderColor;
        Object tabBg;
        ThemeValue selBg = ThemeValue.functioning() ? new ThemeValue(ThemeValue.REGION_BUTTON, ThemeValue.DARK, Color.CYAN) : Color.CYAN;
        ThemeValue selFg = ThemeValue.functioning() ? new ThemeValue(ThemeValue.REGION_BUTTON, ThemeValue.TEXT_FOREGROUND, Color.BLACK) : Color.BLACK;
        Object bg = ThemeValue.functioning() ? ThemeValue.TEXT_BACKGROUND : Color.WHITE;
        Color fb = new Color(144, 144, 255);
        Object object = tabBg = ThemeValue.functioning() ? new ThemeValue(ThemeValue.REGION_INTFRAME, ThemeValue.DARK, fb) : fb;
        if (!ThemeValue.functioning()) {
            Integer i = (Integer)UIManager.get("customFontSize");
            int sz = 11;
            if (i != null) {
                sz = i;
            }
            this.controlFont = new Font("Dialog", 0, sz);
        }
        if ((borderColor = (Color)UIManager.get("InternalFrame.borderShadow")) == null) {
            borderColor = new Color(144, 150, 162);
        }
        Object[] arrobject = new Object[74];
        arrobject[0] = "PropSheet.selectionBackground";
        arrobject[1] = selBg;
        arrobject[2] = "PropSheet.selectionForeground";
        arrobject[3] = selFg;
        arrobject[4] = "PropSheet.selectedSetBackground";
        arrobject[5] = selBg;
        arrobject[6] = "PropSheet.selectedSetForeground";
        arrobject[7] = selFg;
        arrobject[8] = "netbeans.ps.buttonColor";
        arrobject[9] = selFg;
        arrobject[10] = "PropSheet.setBackground";
        arrobject[11] = ThemeValue.functioning() ? control : Color.CYAN;
        arrobject[12] = "PropSheet.disabledForeground";
        arrobject[13] = new Color(161, 161, 146);
        arrobject[14] = "Table.selectionBackground";
        arrobject[15] = selBg;
        arrobject[16] = "Table.selectionForeground";
        arrobject[17] = selFg;
        arrobject[18] = "netbeans.ps.background";
        arrobject[19] = bg;
        arrobject[20] = "window";
        arrobject[21] = this.light;
        arrobject[22] = "TabbedContainer.view.outerBorder";
        arrobject[23] = BorderFactory.createEmptyBorder();
        arrobject[24] = "TabbedContainer.view.tabsBorder";
        arrobject[25] = BorderFactory.createEmptyBorder();
        arrobject[26] = "TabbedContainer.view.contentBorder";
        arrobject[27] = BorderFactory.createMatteBorder(0, 1, 1, 1, borderColor);
        arrobject[28] = "TabbedContainer.editor.outerBorder";
        arrobject[29] = BorderFactory.createEmptyBorder();
        arrobject[30] = "TabbedContainer.editor.contentBorder";
        arrobject[31] = BorderFactory.createMatteBorder(0, 1, 1, 1, borderColor);
        arrobject[32] = "TabbedContainer.editor.tabsBorder";
        arrobject[33] = BorderFactory.createEmptyBorder();
        arrobject[34] = "Nb.Editor.Status.leftBorder";
        arrobject[35] = new InsetBorder(false, true);
        arrobject[36] = "Nb.Editor.Status.rightBorder";
        arrobject[37] = new InsetBorder(false, false);
        arrobject[38] = "Nb.Editor.Status.onlyOneBorder";
        arrobject[39] = new InsetBorder(false, false);
        arrobject[40] = "Nb.Editor.Status.innerBorder";
        arrobject[41] = new InsetBorder(false, true);
        arrobject[42] = "nb.output.background";
        arrobject[43] = control;
        arrobject[44] = "nb.hyperlink.foreground";
        arrobject[45] = selFg;
        arrobject[46] = "nb.output.selectionBackground";
        arrobject[47] = selBg;
        arrobject[48] = "controlFont";
        arrobject[49] = this.controlFont;
        arrobject[50] = "EditorTabDisplayerUI";
        arrobject[51] = "org.netbeans.swing.tabcontrol.plaf.GtkEditorTabDisplayerUI";
        arrobject[52] = "ViewTabDisplayerUI";
        arrobject[53] = "org.netbeans.swing.tabcontrol.plaf.GtkViewTabDisplayerUI";
        arrobject[54] = "IndexButtonUI";
        arrobject[55] = "org.netbeans.swing.tabcontrol.plaf.SlidingTabDisplayerButtonUI";
        arrobject[56] = "SlidingButtonUI";
        arrobject[57] = "org.netbeans.swing.tabcontrol.plaf.GtkSlidingButtonUI";
        arrobject[58] = "Nb.Desktop.background";
        arrobject[59] = ThemeValue.functioning() ? new ThemeValue(ThemeValue.REGION_BUTTON, ThemeValue.LIGHT, Color.GRAY) : Color.GRAY;
        arrobject[60] = "nb.explorer.ministatusbar.border";
        arrobject[61] = BorderFactory.createEmptyBorder();
        arrobject[62] = "nb.progress.cancel.icon";
        arrobject[63] = UIUtils.loadImage("org/netbeans/swing/plaf/resources/cancel_task_linux_mac.png");
        arrobject[64] = "winclassic_tab_sel_gradient";
        arrobject[65] = tabBg;
        arrobject[66] = "Nb.ScrollPane.border";
        arrobject[67] = new JScrollPane().getViewportBorder();
        arrobject[68] = "NbSlideBar.GroupSeparator.Gap.Before";
        arrobject[69] = 7;
        arrobject[70] = "NbSlideBar.GroupSeparator.Gap.After";
        arrobject[71] = 2;
        arrobject[72] = "NbSlideBar.RestoreButton.Gap";
        arrobject[73] = 5;
        Object[] result = arrobject;
        return UIUtils.addInputMapsWithoutCtrlPageUpAndCtrlPageDown(result);
    }

    @Override
    public Object[] createLookAndFeelCustomizationKeysAndValues() {
        if (ThemeValue.functioning()) {
            return new Object[]{"control", control, "controlHighlight", new ThemeValue(ThemeValue.REGION_PANEL, ThemeValue.LIGHT, Color.LIGHT_GRAY), "controlShadow", new ThemeValue(ThemeValue.REGION_PANEL, ThemeValue.DARK, Color.DARK_GRAY), "controlDkShadow", new ThemeValue(ThemeValue.REGION_PANEL, ThemeValue.BLACK, Color.BLACK), "controlLtHighlight", new ThemeValue(ThemeValue.REGION_PANEL, ThemeValue.WHITE, Color.WHITE), "textText", new ThemeValue(ThemeValue.REGION_PANEL, ThemeValue.TEXT_FOREGROUND, Color.BLACK), "text", new ThemeValue(ThemeValue.REGION_PANEL, ThemeValue.TEXT_BACKGROUND, Color.GRAY), "tab_unsel_fill", control, "SplitPane.dividerSize", new Integer(2), "systemFont", this.controlFont, "userFont", this.controlFont, "menuFont", this.controlFont, "List.font", this.controlFont, "Label.font", this.controlFont, "Panel.font", this.controlFont, "FileChooserUI", "javax.swing.plaf.metal.MetalFileChooserUI", "FileView.computerIcon", MetalIconFactory.getTreeComputerIcon(), "FileView.hardDriveIcon", MetalIconFactory.getTreeHardDriveIcon(), "FileView.floppyDriveIcon", MetalIconFactory.getTreeFloppyDriveIcon(), "FileChooser.newFolderIcon", MetalIconFactory.getFileChooserNewFolderIcon(), "FileChooser.upFolderIcon", MetalIconFactory.getFileChooserUpFolderIcon(), "FileChooser.homeFolderIcon", MetalIconFactory.getFileChooserHomeFolderIcon(), "FileChooser.detailsViewIcon", MetalIconFactory.getFileChooserDetailViewIcon(), "FileChooser.listViewIcon", MetalIconFactory.getFileChooserListViewIcon(), "FileChooser.usesSingleFilePane", Boolean.TRUE, "FileChooser.ancestorInputMap", new UIDefaults.LazyInputMap(new Object[]{"ESCAPE", "cancelSelection", "F2", "editFileName", "F5", "refresh", "BACK_SPACE", "Go Up", "ENTER", "approveSelection", "ctrl ENTER", "approveSelection"}), "Tree.gtk_expandedIcon", new GTKExpandedIcon(), "Tree.gtk_collapsedIcon", new GTKCollapsedIcon()};
        }
        Object[] result = new Object[]{"Nb.Toolbar.ui", new UIDefaults.ProxyLazyValue("org.netbeans.swing.plaf.gtk.GtkToolbarUI"), "Tree.gtk_expandedIcon", new GTKExpandedIcon(), "Tree.gtk_collapsedIcon", new GTKCollapsedIcon()};
        return result;
    }

    private static final class GTKExpandedIcon
    extends GTKIcon {
        private GTKExpandedIcon() {
            super();
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            int up;
            int i;
            g.translate(x, y);
            int size = Math.min(this.getIconWidth(), this.getIconHeight());
            int mid = size / 2;
            int height = size / 2 + 1;
            int thick = Math.max(1, size / 7);
            int j = size / 2 - height / 2 - 1;
            g.setColor((Color)UIManager.get("Button.background"));
            for (i = height - 1; i > 0; --i) {
                g.drawLine(mid - i + 1, j, mid + i - 1, j);
                ++j;
            }
            g.setColor((Color)UIManager.get("Button.foreground"));
            j = size / 2 - height / 2 - 1;
            int down = thick - 1;
            for (up = 0; up < thick; ++up) {
                g.drawLine(0 - down, j + up, size + down, j + up);
                --down;
            }
            ++j;
            for (i = height - 1; i > 0; --i) {
                for (up = 0; up < thick; ++up) {
                    g.drawLine(mid - i + 1 - up, j, mid - i + 1 - up, j);
                    g.drawLine(mid + i - 1 + up, j, mid + i - 1 + up, j);
                }
                ++j;
            }
            if (thick > 1) {
                for (up = thick - 2; up >= 0; --up) {
                    g.drawLine(mid - up, j, mid + up, j);
                    ++j;
                }
            }
            g.translate(- x, - y);
        }
    }

    private static final class GTKCollapsedIcon
    extends GTKIcon {
        private GTKCollapsedIcon() {
            super();
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            int up;
            int j;
            g.translate(x, y);
            int size = Math.min(this.getIconWidth(), this.getIconHeight());
            int mid = size / 2;
            int height = size / 2 + 1;
            int thick = Math.max(1, size / 7);
            int i = size / 2 - height / 2 - 1;
            g.setColor((Color)UIManager.get("Button.background"));
            for (j = height - 1; j > 0; --j) {
                g.drawLine(i, mid - j + 1, i, mid + j - 1);
                ++i;
            }
            g.setColor((Color)UIManager.get("Button.foreground"));
            i = size / 2 - height / 2 - 1;
            int down = thick - 1;
            for (up = 0; up < thick; ++up) {
                g.drawLine(i + up, 0 - down, i + up, size + down);
                --down;
            }
            ++i;
            for (j = height - 1; j > 0; --j) {
                for (up = 0; up < thick; ++up) {
                    g.drawLine(i, mid - j + 1 - up, i, mid - j + 1 - up);
                    g.drawLine(i, mid + j - 1 + up, i, mid + j - 1 + up);
                }
                ++i;
            }
            if (thick > 1) {
                for (up = thick - 2; up >= 0; --up) {
                    g.drawLine(i, mid - up, i, mid + up);
                    ++i;
                }
            }
            g.translate(- x, - y);
        }
    }

    private static abstract class GTKIcon
    implements Icon {
        private static final int SIZE = 11;

        private GTKIcon() {
        }

        @Override
        public int getIconWidth() {
            return 11;
        }

        @Override
        public int getIconHeight() {
            return 11;
        }
    }

}

