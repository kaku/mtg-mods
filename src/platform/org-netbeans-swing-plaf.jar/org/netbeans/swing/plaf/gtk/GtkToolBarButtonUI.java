/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.gtk;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.event.MouseListener;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ButtonUI;
import javax.swing.plaf.basic.BasicButtonListener;

class GtkToolBarButtonUI
extends ButtonUI
implements ChangeListener {
    private static BasicButtonListener listener = new BasicButtonListener(null);
    private final Rectangle scratch = new Rectangle();
    private static final int minButtonSize = 32;

    @Override
    public void installUI(JComponent c) {
        AbstractButton b = (AbstractButton)c;
        b.addMouseListener(listener);
        b.addChangeListener(this);
        b.setContentAreaFilled(false);
        b.setOpaque(false);
        b.setFocusable(false);
        b.setBorderPainted(false);
        b.setBorder(BorderFactory.createEmptyBorder());
        b.putClientProperty("hideActionText", Boolean.TRUE);
    }

    @Override
    public void uninstallUI(JComponent c) {
        c.removeMouseListener(listener);
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        ((AbstractButton)e.getSource()).repaint();
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        Rectangle r = c.getBounds(this.scratch);
        AbstractButton b = (AbstractButton)c;
        r.x = 0;
        r.y = 0;
        Paint temp = ((Graphics2D)g).getPaint();
        this.paintBackground((Graphics2D)g, b, r);
        this.paintIcon(g, b, r);
        ((Graphics2D)g).setPaint(temp);
    }

    private void paintBackground(Graphics2D g, AbstractButton b, Rectangle r) {
        if (b.isEnabled()) {
            if (b.getModel().isPressed()) {
                this.compositeColor(g, r, Color.BLUE, 0.3f);
            } else if (b.getModel().isSelected()) {
                this.compositeColor(g, r, new Color(0, 120, 255), 0.2f);
            }
        }
    }

    private void compositeColor(Graphics2D g, Rectangle r, Color c, float alpha) {
        g.setColor(c);
        Composite comp = g.getComposite();
        g.setComposite(AlphaComposite.getInstance(3, alpha));
        g.fillRect(r.x, r.y, r.width, r.height);
        g.setComposite(comp);
    }

    private static boolean isFirst(AbstractButton b) {
        if (b.getParent() != null && b.getParent().getComponentCount() > 1) {
            return b == b.getParent().getComponent(1);
        }
        return false;
    }

    private void paintIcon(Graphics g, AbstractButton b, Rectangle r) {
        Icon ic = this.getIconForState(b);
        if (ic != null) {
            int iconX = 0;
            int iconY = 0;
            int iconW = ic.getIconWidth();
            int iconH = ic.getIconHeight();
            if (iconW <= r.width) {
                iconX = r.width / 2 - iconW / 2;
            }
            if (iconH <= r.height) {
                iconY = r.height / 2 - iconH / 2;
            }
            ic.paintIcon(b, g, iconX, iconY);
        }
    }

    private Icon getIconForState(AbstractButton b) {
        ButtonModel mdl = b.getModel();
        Icon result = null;
        if (!b.isEnabled()) {
            Icon icon = result = mdl.isSelected() ? b.getDisabledSelectedIcon() : b.getDisabledIcon();
            if (result == null && mdl.isSelected()) {
                result = b.getDisabledIcon();
            }
        } else {
            if (mdl.isArmed() && !mdl.isPressed() && (result = mdl.isSelected() ? b.getRolloverSelectedIcon() : b.getRolloverIcon()) == null & mdl.isSelected()) {
                result = b.getRolloverIcon();
            }
            if (mdl.isPressed()) {
                result = b.getPressedIcon();
            } else if (mdl.isSelected()) {
                result = b.getSelectedIcon();
            }
        }
        if (result == null) {
            result = b.getIcon();
        }
        return result;
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        if (c instanceof AbstractButton) {
            Icon ic = this.getIconForState((AbstractButton)c);
            int minSize = GtkToolBarButtonUI.isFirst((AbstractButton)c) ? 0 : 32;
            Dimension result = ic != null ? new Dimension(Math.max(minSize, ic.getIconWidth() + 1), Math.max(32, ic.getIconHeight() + 1)) : new Dimension(32, 32);
            result.width += 4;
            return result;
        }
        if (c.getLayout() != null) {
            return c.getLayout().preferredLayoutSize(c);
        }
        return null;
    }
}

