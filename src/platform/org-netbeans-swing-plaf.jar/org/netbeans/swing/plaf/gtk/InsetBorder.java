/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.gtk;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.UIManager;
import javax.swing.border.Border;

public class InsetBorder
implements Border {
    private boolean left;
    private boolean right;

    public InsetBorder(boolean left, boolean right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public Insets getBorderInsets(Component c) {
        return new Insets(2, this.left ? 6 : 2, 0, this.right ? 6 : 2);
    }

    @Override
    public boolean isBorderOpaque() {
        return false;
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        int h = c.getHeight();
        Color col = g.getColor();
        g.setColor(UIManager.getColor("controlShadow"));
        if (this.left) {
            g.drawLine(x + 3, y, x + 3, y + h - 1);
        }
        if (this.right) {
            g.drawLine(x + width - 3, y, x + width - 3, y + h - 1);
        }
        g.drawLine(x, y, x + width - 1, y);
    }
}

