/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.gtk;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Toolkit;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashSet;
import javax.swing.ButtonModel;
import javax.swing.CellRendererPane;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.plaf.ButtonUI;
import org.netbeans.swing.plaf.util.UIUtils;

final class ThemeValue
implements UIDefaults.ActiveValue {
    private final Object fallback;
    private final Object aRegion;
    private Object aColorType = null;
    private boolean darken = false;
    private Object value = null;
    private static Boolean functioning = null;
    private int ct = 0;
    private static boolean log = Boolean.getBoolean("themeValue.log");
    private static JButton dummyButton = null;
    private static Class<?> synthLookAndFeel = null;
    private static Class<?> gtkLookAndFeel = null;
    private static Class<?> colorType = null;
    private static Class<?> region = null;
    private static Class<?> synthStyle = null;
    private static Class<?> synthContext = null;
    private static Class<?> gtkColorType = null;
    private static Class<?> synthUI = null;
    private static Constructor synthContextConstructor;
    private static Method synthStyle_getColorForState;
    private static Method synthStyle_getFontForState;
    private static Method synthLookAndFeel_getStyle;
    private static Method synthContext_getContext;
    private static Method synthUI_getContext;
    static Object REGION_BUTTON;
    static Object REGION_PANEL;
    static Object REGION_SCROLLBAR_THUMB;
    static Object REGION_TAB;
    static Object REGION_INTFRAME;
    static Object LIGHT;
    static Object DARK;
    static Object BLACK;
    static Object WHITE;
    static Object MID;
    static Object TEXT_FOREGROUND;
    static Object TEXT_BACKGROUND;
    static Object FOCUS;
    private static HashSet<ThemeValue> instances;

    public ThemeValue(Object region, Object colorType, Object fallback) {
        this.fallback = fallback;
        this.aRegion = region;
        this.aColorType = colorType;
        ThemeValue.register(this);
    }

    public ThemeValue(Object region, Object colorType, Object fallback, boolean darken) {
        this.fallback = fallback;
        this.aRegion = region;
        this.aColorType = colorType;
        this.darken = darken;
        ThemeValue.register(this);
    }

    public ThemeValue(Object region, Font fallback) {
        this.fallback = fallback;
        this.aRegion = region;
        ThemeValue.register(this);
    }

    @Override
    public Object createValue(UIDefaults table) {
        if (this.value == null) {
            if (!ThemeValue.functioning()) {
                this.value = this.fallback;
            } else if (this.fallback instanceof Font) {
                Font val = this.getFont();
                if (this.ct++ < 4) {
                    return val;
                }
                this.value = val;
            } else {
                this.value = this.getColor();
            }
        }
        return this.value != null ? this.value : this.fallback;
    }

    void clear() {
        this.value = null;
    }

    public Font getFont() {
        block4 : {
            Object style = ThemeValue.getSynthStyle(this.aRegion);
            if (Boolean.TRUE.equals(functioning)) {
                try {
                    Font result = (Font)synthStyle_getFontForState.invoke(style, ThemeValue.getSynthContext());
                    if (result == null) {
                        result = (Font)this.fallback;
                    }
                    return result;
                }
                catch (Exception e) {
                    functioning = Boolean.FALSE;
                    if (!log) break block4;
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public Color getColor() {
        block5 : {
            Object style = ThemeValue.getSynthStyle(this.aRegion);
            if (Boolean.TRUE.equals(functioning)) {
                try {
                    Color result = (Color)synthStyle_getColorForState.invoke(style, ThemeValue.getSynthContext(), this.aColorType);
                    if (result == null) {
                        result = (Color)this.fallback;
                    }
                    if (this.darken) {
                        result = result.darker();
                    }
                    return result;
                }
                catch (Exception e) {
                    functioning = Boolean.FALSE;
                    if (!log) break block5;
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public static boolean functioning() {
        if (functioning == null) {
            ThemeValue.checkFunctioning();
        }
        return functioning;
    }

    private static void checkFunctioning() {
        functioning = Boolean.FALSE;
        try {
            gtkLookAndFeel = UIUtils.classForName("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
            synthLookAndFeel = UIUtils.classForName("javax.swing.plaf.synth.SynthLookAndFeel");
            region = UIUtils.classForName("javax.swing.plaf.synth.Region");
            synthStyle = UIUtils.classForName("javax.swing.plaf.synth.SynthStyle");
            synthContext = UIUtils.classForName("javax.swing.plaf.synth.SynthContext");
            colorType = UIUtils.classForName("javax.swing.plaf.synth.ColorType");
            gtkColorType = UIUtils.classForName("com.sun.java.swing.plaf.gtk.GTKColorType");
            try {
                synthUI = UIUtils.classForName("javax.swing.plaf.synth.SynthUI");
            }
            catch (ClassNotFoundException x) {
                synthUI = UIUtils.classForName("sun.swing.plaf.synth.SynthUI");
            }
            synthContextConstructor = synthContext.getDeclaredConstructor(JComponent.class, region, synthStyle, Integer.TYPE);
            synthContextConstructor.setAccessible(true);
            synthStyle_getColorForState = synthStyle.getDeclaredMethod("getColorForState", synthContext, colorType);
            synthStyle_getColorForState.setAccessible(true);
            synthStyle_getFontForState = synthStyle.getDeclaredMethod("getFontForState", synthContext);
            synthStyle_getFontForState.setAccessible(true);
            LIGHT = ThemeValue.valueOfField(gtkColorType, "LIGHT");
            DARK = ThemeValue.valueOfField(gtkColorType, "DARK");
            MID = ThemeValue.valueOfField(gtkColorType, "MID");
            BLACK = ThemeValue.valueOfField(gtkColorType, "BLACK");
            WHITE = ThemeValue.valueOfField(gtkColorType, "WHITE");
            TEXT_FOREGROUND = ThemeValue.valueOfField(colorType, "TEXT_FOREGROUND");
            TEXT_BACKGROUND = ThemeValue.valueOfField(colorType, "TEXT_BACKGROUND");
            FOCUS = ThemeValue.valueOfField(colorType, "FOCUS");
            synthContext_getContext = synthContext.getDeclaredMethod("getContext", Class.class, JComponent.class, region, synthStyle, Integer.TYPE);
            synthContext_getContext.setAccessible(true);
            synthLookAndFeel_getStyle = synthLookAndFeel.getDeclaredMethod("getStyle", JComponent.class, region);
            synthLookAndFeel_getStyle.setAccessible(true);
            REGION_BUTTON = ThemeValue.valueOfField(region, "BUTTON");
            REGION_PANEL = ThemeValue.valueOfField(region, "PANEL");
            REGION_SCROLLBAR_THUMB = ThemeValue.valueOfField(region, "SCROLL_BAR_THUMB");
            REGION_TAB = ThemeValue.valueOfField(region, "TABBED_PANE_TAB");
            REGION_INTFRAME = ThemeValue.valueOfField(region, "INTERNAL_FRAME_TITLE_PANE");
            synthUI_getContext = synthUI.getDeclaredMethod("getContext", JComponent.class);
            functioning = Boolean.TRUE;
        }
        catch (Exception e) {
            System.err.println("Cannot initialize GTK colors - using hardcoded defaults: " + e);
            if (log) {
                e.printStackTrace();
            }
            return;
        }
    }

    private static JButton getDummyButton() {
        if (dummyButton == null) {
            dummyButton = new JButton();
            CellRendererPane crp = new CellRendererPane();
            crp.add(dummyButton);
        }
        ButtonModel mdl = dummyButton.getModel();
        return dummyButton;
    }

    private static Object getSynthContext() {
        try {
            JButton dummyButton = ThemeValue.getDummyButton();
            if (synthUI.isAssignableFrom(dummyButton.getUI().getClass())) {
                return synthUI_getContext.invoke(dummyButton.getUI(), dummyButton);
            }
            throw new IllegalStateException("I don't have a SynthButtonUI to play with");
        }
        catch (Exception e) {
            functioning = Boolean.FALSE;
            if (log) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private static Object getSynthStyle(Object region) {
        try {
            return synthLookAndFeel_getStyle.invoke(null, ThemeValue.getDummyButton(), region);
        }
        catch (Exception e) {
            functioning = Boolean.FALSE;
            if (log) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private static Object valueOfField(Class clazz, String field) throws NoSuchFieldException, IllegalAccessException {
        Field f = clazz.getDeclaredField(field);
        f.setAccessible(true);
        return f.get(null);
    }

    private static synchronized void register(ThemeValue value) {
        if (instances == null) {
            instances = new HashSet();
            ThemeValue.registerPcl();
        }
        instances.add(value);
    }

    private static void registerPcl() {
        Listener l = new Listener();
        UIManager.addPropertyChangeListener(l);
        Toolkit.getDefaultToolkit().addPropertyChangeListener("gnome.Gtk/FontName", l);
        Toolkit.getDefaultToolkit().addPropertyChangeListener("gnome.Xft/DPI", l);
        Toolkit.getDefaultToolkit().addPropertyChangeListener("gnome.Net/ThemeName", l);
    }

    static {
        synthStyle_getColorForState = null;
        synthStyle_getFontForState = null;
        synthLookAndFeel_getStyle = null;
        synthContext_getContext = null;
        synthUI_getContext = null;
        REGION_BUTTON = null;
        REGION_PANEL = null;
        REGION_SCROLLBAR_THUMB = null;
        REGION_TAB = null;
        REGION_INTFRAME = null;
        LIGHT = null;
        DARK = null;
        BLACK = null;
        WHITE = null;
        MID = null;
        TEXT_FOREGROUND = null;
        TEXT_BACKGROUND = null;
        FOCUS = null;
        instances = null;
        ThemeValue.functioning();
    }

    private static class Listener
    implements PropertyChangeListener {
        private Listener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent pce) {
            if (pce.getSource() instanceof UIManager && "lookAndFeel".equals(pce.getPropertyName())) {
                String s = UIManager.getLookAndFeel().getClass().getName();
                if (s.indexOf("gtk") < 0) {
                    UIManager.removePropertyChangeListener(this);
                    Toolkit.getDefaultToolkit().removePropertyChangeListener("gnome.Gtk/FontName", this);
                    Toolkit.getDefaultToolkit().removePropertyChangeListener("gnome.Xft/DPI", this);
                    Toolkit.getDefaultToolkit().removePropertyChangeListener("gnome.Net/ThemeName", this);
                }
            } else {
                for (ThemeValue tv : instances) {
                    tv.clear();
                }
            }
        }
    }

}

