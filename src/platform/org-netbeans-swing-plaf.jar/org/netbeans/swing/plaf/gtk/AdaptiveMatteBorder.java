/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.gtk;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.border.Border;

public class AdaptiveMatteBorder
implements Border {
    private Insets insets;
    private int shadowDepth;
    private boolean topLeftInsets;
    private static final float[] comps = new float[4];
    private static final float[] targs = new float[4];

    public AdaptiveMatteBorder(boolean t, boolean l, boolean b, boolean r, int shadowDepth, boolean topLeftInsets) {
        this.insets = new Insets(t ? (topLeftInsets ? shadowDepth + 1 : 1) : 0, l ? (topLeftInsets ? shadowDepth + 1 : 1) : 0, b ? 1 + shadowDepth : shadowDepth, r ? 1 + shadowDepth : shadowDepth);
        this.shadowDepth = shadowDepth;
        this.topLeftInsets = topLeftInsets;
    }

    public AdaptiveMatteBorder(boolean t, boolean l, boolean b, boolean r, int shadowDepth) {
        this(t, l, b, r, shadowDepth, false);
    }

    private Insets maybeOmitInsets(Insets ins, Component c) {
        if (this.shadowDepth <= 0 || !this.topLeftInsets) {
            return ins;
        }
        Insets result = new Insets(ins.top, ins.left, ins.right, ins.bottom);
        if (this.topLeftInsets) {
            Point p = c.getLocation();
            if (p.x > 10) {
                result.left = 1;
            }
            if (p.y > 10) {
                result.top = 1;
            }
        }
        return result;
    }

    @Override
    public Insets getBorderInsets(Component c) {
        return this.maybeOmitInsets(this.insets, c);
    }

    @Override
    public boolean isBorderOpaque() {
        return false;
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
        Color color = g.getColor();
        Insets ins = this.getBorderInsets(c);
        Point p = c.getLocation();
        g.setColor(UIManager.getColor("controlShadow"));
        w -= this.shadowDepth;
        h -= this.shadowDepth;
        if (this.topLeftInsets) {
            if (p.y <= 10) {
                y += this.shadowDepth;
                h -= this.shadowDepth;
            }
            if (p.x <= 10) {
                x += this.shadowDepth;
                w -= this.shadowDepth;
            }
        }
        if (ins.top > 0) {
            g.fillRect(x, y, w, 1);
        }
        if (ins.left > 0) {
            g.fillRect(x, y, 1, h);
        }
        if (ins.right > 0) {
            g.fillRect(x + w - 1, y, 1, h);
        }
        if (ins.bottom > 0) {
            g.fillRect(x, y + h - 1, w, 1);
        }
        boolean isViewTab = AdaptiveMatteBorder.isViewTab(c);
        if (this.shadowDepth > 1) {
            boolean clipTouchesBottom;
            Rectangle clip = g.getClipBounds();
            boolean clipTouchesRight = clip.x + clip.width >= x + w;
            boolean bl = clipTouchesBottom = clip.y + clip.height >= y + h;
            if (clipTouchesBottom || clipTouchesRight) {
                Color ctrl = UIManager.getColor("control");
                Color base = UIManager.getColor("controlShadow");
                for (int i = 1; i < this.shadowDepth; ++i) {
                    Color curr = AdaptiveMatteBorder.colorTowards(base, ctrl, this.shadowDepth, i + 1);
                    g.setColor(curr);
                    if (clipTouchesRight && ins.right > 0) {
                        g.fillRect(x + w - 1 + i, y + (isViewTab ? 0 : i), 1, h);
                    }
                    if (!clipTouchesBottom || ins.bottom <= 0) continue;
                    g.fillRect(x + i, y + h - 1 + i, w - 1, 1);
                }
            }
        }
        g.setColor(color);
    }

    static boolean isViewTab(Component c) {
        JComponent jc;
        Object o;
        if (c.getParent() instanceof JComponent && (o = (jc = (JComponent)c.getParent()).getClientProperty("viewType")) != null && o instanceof Integer) {
            return (Integer)o == 0;
        }
        return false;
    }

    static final Color colorTowards(Color base, Color target, float steps, float step) {
        base.getColorComponents(comps);
        target.getColorComponents(targs);
        AdaptiveMatteBorder.comps[3] = 1.0f;
        float factor = step / steps;
        for (int i = 0; i < 3; ++i) {
            AdaptiveMatteBorder.comps[i] = AdaptiveMatteBorder.saturate(comps[i] - factor * (comps[i] - targs[i]));
        }
        Color result = new Color(comps[0], comps[1], comps[2], comps[3]);
        return result;
    }

    private static final float saturate(float f) {
        float orig = f;
        if (f > 1.0f) {
            f = 1.0f;
        }
        if (f < 0.0f) {
            f = 0.0f;
        }
        return f;
    }
}

