/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf;

import java.awt.Toolkit;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.PrintStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.plaf.metal.MetalLookAndFeel;
import org.netbeans.swing.plaf.AllLFCustoms;
import org.netbeans.swing.plaf.LFCustoms;
import org.netbeans.swing.plaf.aqua.AquaLFCustoms;
import org.netbeans.swing.plaf.gtk.GtkLFCustoms;
import org.netbeans.swing.plaf.metal.MetalLFCustoms;
import org.netbeans.swing.plaf.nimbus.NimbusLFCustoms;
import org.netbeans.swing.plaf.util.NbTheme;
import org.netbeans.swing.plaf.util.RelativeColor;
import org.netbeans.swing.plaf.util.UIBootstrapValue;
import org.netbeans.swing.plaf.util.UIUtils;
import org.netbeans.swing.plaf.winclassic.WindowsLFCustoms;
import org.netbeans.swing.plaf.windows8.Windows8LFCustoms;
import org.netbeans.swing.plaf.winvista.VistaLFCustoms;
import org.netbeans.swing.plaf.winxp.XPLFCustoms;

public final class Startup {
    private static final String FORCED_CUSTOMS = System.getProperty("nb.forceui");
    private static final boolean NO_CUSTOMIZATIONS = Boolean.getBoolean("netbeans.plaf.disable.ui.customizations");
    private static final String NIMBUS = "Nimbus";
    private static Startup instance = null;
    private LFCustoms curCustoms = null;
    private LFCustoms globalCustoms = null;
    private static URL themeURL = null;
    private static Class uiClass = null;
    private static ResourceBundle bundle;
    private boolean installed = false;
    private static ClassLoader loader;
    private LFListener listener = null;

    private Startup() {
        this.initialize();
    }

    private void initialize() {
        LookAndFeel lf = this.getLookAndFeel();
        boolean forceLaf = false;
        if (lf instanceof MetalLookAndFeel) {
            forceLaf = this.installTheme(lf);
        }
        try {
            if (lf != UIManager.getLookAndFeel() || forceLaf) {
                UIManager.setLookAndFeel(lf);
            }
        }
        catch (Exception e) {
            System.err.println("Could not install look and feel " + lf);
        }
    }

    private LookAndFeel getLookAndFeel() {
        if (uiClass != null && uiClass.getName().contains("Nimbus")) {
            Logger.getLogger(this.getClass().getName()).warning("L&F Warning: Nimbus L&F is not supported L&F yet and system may exhibit various drawing problems. Please use for experimental purposes only.");
        }
        if (uiClass == null) {
            ResourceBundle b = bundle != null ? bundle : ResourceBundle.getBundle("org.netbeans.swing.plaf.Bundle");
            String uiClassName = b.getString("LookAndFeelClassName");
            if ("default".equals(uiClassName)) {
                uiClassName = Startup.defaultLaF();
            }
            try {
                uiClass = UIUtils.classForName(uiClassName);
            }
            catch (ClassNotFoundException e) {
                System.err.println("Custom UI class " + uiClassName + " not on classpath.");
                try {
                    uiClass = UIUtils.classForName("javax.swing.plaf.metal.MetalLookAndFeel");
                }
                catch (Exception newEx) {
                    newEx.printStackTrace();
                }
            }
            catch (Exception e) {
                System.err.println("While loading: " + uiClassName);
                e.printStackTrace();
            }
        }
        LookAndFeel result = null;
        if (uiClass != null) {
            try {
                LookAndFeel lf = UIManager.getLookAndFeel();
                result = uiClass != lf.getClass() ? (LookAndFeel)uiClass.newInstance() : UIManager.getLookAndFeel();
            }
            catch (Exception e) {
                System.err.println("Cannot load custom UI class " + uiClass);
                e.printStackTrace();
                result = UIManager.getLookAndFeel();
            }
        }
        return result;
    }

    private static String defaultLaF() {
        String uiClassName;
        if (Startup.isWindows()) {
            uiClassName = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
        } else if (Startup.isMac()) {
            uiClassName = "apple.laf.AquaLookAndFeel";
        } else if (Startup.shouldUseMetal()) {
            uiClassName = "javax.swing.plaf.metal.MetalLookAndFeel";
        } else {
            uiClassName = UIManager.getSystemLookAndFeelClassName();
            String javaVersion = System.getProperty("java.version");
            if ("1.6.0_01".compareTo(javaVersion) > 0 && System.getProperty("java.vm.name") != null && System.getProperty("java.vm.name").indexOf("OpenJDK") < 0) {
                if (uiClassName.indexOf("gtk") >= 0 && !Boolean.getBoolean("useGtk")) {
                    uiClassName = "javax.swing.plaf.metal.MetalLookAndFeel";
                }
            } else if (uiClassName.indexOf("gtk") >= 0 && System.getProperty("useGtk") != null && !Boolean.getBoolean("useGtk")) {
                uiClassName = "javax.swing.plaf.metal.MetalLookAndFeel";
            }
            if (uiClassName.contains("Nimbus")) {
                uiClassName = "javax.swing.plaf.metal.MetalLookAndFeel";
            }
        }
        return uiClassName;
    }

    private boolean installTheme(LookAndFeel lf) {
        boolean themeInstalled = false;
        if (themeURL != null) {
            themeInstalled = true;
            NbTheme nbTheme = new NbTheme(themeURL, lf);
            MetalLookAndFeel.setCurrentTheme(nbTheme);
        }
        return themeInstalled;
    }

    private void install() {
        if (this.installed) {
            return;
        }
        if (this.globalCustoms == null) {
            this.globalCustoms = new AllLFCustoms();
            this.installLFCustoms(this.globalCustoms);
        }
        this.installPerLFDefaults();
        this.installTheme(UIManager.getLookAndFeel());
        this.runPostInstall();
        this.attachListener();
    }

    private void installPerLFDefaults() {
        boolean isLFChange = this.curCustoms != null;
        this.curCustoms = this.findCustoms();
        if (this.curCustoms != null) {
            Integer in = (Integer)UIManager.get("customFontSize");
            if (in == null && UIManager.getLookAndFeel().getClass() == MetalLookAndFeel.class) {
                in = new Integer(11);
            }
            if (in != null && !UIUtils.isGtkLF()) {
                AllLFCustoms.initCustomFontSize(in);
            }
            this.installLFCustoms(this.curCustoms);
            if (isLFChange) {
                this.loadAllLazyValues(this.curCustoms);
            }
            this.curCustoms.disposeValues();
        }
    }

    private void loadAllLazyValues(LFCustoms customs) {
        if (this.globalCustoms != null) {
            this.loadLazy(this.globalCustoms.getApplicationSpecificKeysAndValues());
            this.loadLazy(this.globalCustoms.getGuaranteedKeysAndValues());
            this.loadLazy(this.globalCustoms.getLookAndFeelCustomizationKeysAndValues());
        }
        this.loadLazy(customs.getApplicationSpecificKeysAndValues());
        this.loadLazy(customs.getGuaranteedKeysAndValues());
        this.loadLazy(customs.getLookAndFeelCustomizationKeysAndValues());
    }

    private void loadLazy(Object[] o) {
        if (o.length > 0) {
            UIDefaults uidefaults = UIManager.getDefaults();
            for (int i = 1; i < o.length; i += 2) {
                if (o[i] instanceof UIBootstrapValue.Lazy) {
                    ((UIBootstrapValue.Lazy)o[i]).createValue(uidefaults);
                }
                if (!(o[i] instanceof RelativeColor)) continue;
                ((RelativeColor)o[i]).clear();
            }
        }
    }

    private void uninstallPerLFDefaults() {
        assert (this.globalCustoms != null);
        if (this.curCustoms != null) {
            HashSet<Object> keep = new HashSet<Object>(Arrays.asList(this.globalCustoms.allKeys()));
            Object[] arr = this.curCustoms.allKeys();
            for (int i = 0; i < arr.length; ++i) {
                Object key = arr[i];
                if (keep.contains(key)) continue;
                UIManager.put(key, null);
            }
        }
    }

    private void attachListener() {
        assert (this.listener == null);
        this.listener = new LFListener();
        UIManager.addPropertyChangeListener(this.listener);
        Toolkit.getDefaultToolkit().addPropertyChangeListener("win.xpstyle.themeActive", this.listener);
    }

    private void installLFCustoms(LFCustoms customs) {
        UIDefaults defaults = UIManager.getDefaults();
        defaults.put("ClassLoader", new CLValue());
        defaults.putDefaults(customs.getGuaranteedKeysAndValues());
        defaults.putDefaults(customs.getApplicationSpecificKeysAndValues());
        if (!NO_CUSTOMIZATIONS) {
            defaults.putDefaults(customs.getLookAndFeelCustomizationKeysAndValues());
        }
    }

    private void runPostInstall() {
        final Object postInit = UIManager.get("nb.laf.postinstall.callable");
        if (postInit instanceof Callable) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    try {
                        ((Callable)postInit).call();
                    }
                    catch (Exception ex) {
                        Logger.getLogger(Startup.class.getName()).log(Level.INFO, null, ex);
                    }
                }
            });
        }
    }

    public static void setClassLoader(ClassLoader loader) {
        Startup.loader = loader;
    }

    private LFCustoms findCustoms() {
        ResourceBundle b = bundle != null ? bundle : ResourceBundle.getBundle("org.netbeans.swing.plaf.Bundle");
        String uiClassName = b.getString("LookAndFeelCustomsClassName");
        if ("default".equals(uiClassName)) {
            return this.findDefaultCustoms();
        }
        try {
            Class klazz = UIUtils.classForName(uiClassName);
            Object inst = klazz.newInstance();
            if (inst instanceof LFCustoms) {
                return (LFCustoms)inst;
            }
        }
        catch (ClassNotFoundException e) {
            System.err.println("LF Customs " + uiClassName + " not on classpath.");
        }
        catch (Exception e) {
            System.err.println("While loading: " + uiClassName);
            e.printStackTrace();
        }
        return null;
    }

    private LFCustoms findDefaultCustoms() {
        if (FORCED_CUSTOMS != null) {
            System.err.println("Using explicitly set UI customizations: " + FORCED_CUSTOMS);
            if ("Windows8".equals(FORCED_CUSTOMS)) {
                return new Windows8LFCustoms();
            }
            if ("Vista".equals(FORCED_CUSTOMS)) {
                return new VistaLFCustoms();
            }
            if ("XP".equals(FORCED_CUSTOMS)) {
                return new XPLFCustoms();
            }
            if ("Aqua".equals(FORCED_CUSTOMS)) {
                return new AquaLFCustoms();
            }
            if ("Metal".equals(FORCED_CUSTOMS)) {
                return new MetalLFCustoms();
            }
            if ("Windows".equals(FORCED_CUSTOMS)) {
                return new WindowsLFCustoms();
            }
            if ("GTK".equals(FORCED_CUSTOMS)) {
                return new GtkLFCustoms();
            }
            try {
                return (LFCustoms)UIUtils.classForName(FORCED_CUSTOMS).newInstance();
            }
            catch (Exception e) {
                System.err.println("UI customizations class not found: " + FORCED_CUSTOMS);
            }
        }
        StringBuffer buf = new StringBuffer(40);
        buf.append("Nb.");
        buf.append(UIManager.getLookAndFeel().getID());
        if (UIUtils.isXPLF()) {
            if (Startup.isWindows8()) {
                buf.append("Windows8LFCustoms");
            } else if (Startup.isWindowsVista() || Startup.isWindows7() || Startup.isWindows8()) {
                buf.append("VistaLFCustoms");
            } else {
                buf.append("XPLFCustoms");
            }
        } else {
            buf.append("LFCustoms");
        }
        LFCustoms result = null;
        try {
            result = (LFCustoms)UIManager.get(buf.toString());
        }
        catch (ClassCastException cce) {
            // empty catch block
        }
        if (result == null) {
            String[] knownLFs = new String[]{"Metal", "Windows", "Aqua", "GTK", "Nimbus"};
            switch (Arrays.asList(knownLFs).indexOf(UIManager.getLookAndFeel().getID())) {
                case 1: {
                    if (UIUtils.isXPLF()) {
                        if (Startup.isWindows8()) {
                            result = new Windows8LFCustoms();
                            break;
                        }
                        if (Startup.isWindowsVista() || Startup.isWindows7()) {
                            result = new VistaLFCustoms();
                            break;
                        }
                        result = new XPLFCustoms();
                        break;
                    }
                    result = new WindowsLFCustoms();
                    break;
                }
                case 0: {
                    result = new MetalLFCustoms();
                    break;
                }
                case 2: {
                    result = new AquaLFCustoms();
                    break;
                }
                case 3: {
                    result = new GtkLFCustoms();
                    break;
                }
                case 4: {
                    result = new NimbusLFCustoms();
                    break;
                }
                default: {
                    if (UIUtils.isXPLF()) {
                        if (Startup.isWindows8()) {
                            result = new Windows8LFCustoms();
                            break;
                        }
                        if (Startup.isWindowsVista() || Startup.isWindows7()) {
                            result = new VistaLFCustoms();
                            break;
                        }
                        result = new XPLFCustoms();
                        break;
                    }
                    result = UIManager.getLookAndFeel() instanceof MetalLookAndFeel ? new MetalLFCustoms() : new WindowsLFCustoms();
                }
            }
        }
        return result;
    }

    public static void run(Class uiClass, int uiFontSize, URL themeURL) {
        Startup.run(uiClass, uiFontSize, themeURL, null);
    }

    public static void run(Class uiClass, int uiFontSize, URL themeURL, ResourceBundle rb) {
        if (instance == null) {
            if (uiFontSize > 0) {
                Integer customFontSize = new Integer(uiFontSize);
                UIManager.put("customFontSize", customFontSize);
            }
            Startup.uiClass = uiClass;
            Startup.themeURL = themeURL;
            bundle = rb;
            instance = new Startup();
            instance.install();
        }
    }

    private static boolean isWindows() {
        String osName = System.getProperty("os.name");
        return osName.startsWith("Windows");
    }

    private static boolean isWindowsVista() {
        String osName = System.getProperty("os.name");
        return osName.indexOf("Vista") >= 0 || osName.equals("Windows NT (unknown)") && "6.0".equals(System.getProperty("os.version"));
    }

    private static boolean isWindows7() {
        String osName = System.getProperty("os.name");
        return osName.indexOf("Windows 7") >= 0 || osName.equals("Windows NT (unknown)") && "6.1".equals(System.getProperty("os.version"));
    }

    private static boolean isWindows8() {
        String osName = System.getProperty("os.name");
        return osName.indexOf("Windows 8") >= 0 || osName.equals("Windows NT (unknown)") && "6.2".equals(System.getProperty("os.version"));
    }

    private static boolean isMac() {
        String osName = System.getProperty("os.name");
        boolean result = osName.startsWith("Darwin") || "Mac OS X".equals(osName);
        return result;
    }

    private static boolean isSolaris10() {
        String osName = System.getProperty("os.name");
        String osVersion = System.getProperty("os.version");
        boolean result = osName.startsWith("SunOS") && "5.10".equals(osVersion);
        return result;
    }

    private static boolean shouldUseMetal() {
        String osName = System.getProperty("os.name");
        boolean result = !"Solaris".equals(osName) && !osName.startsWith("SunOS") && !osName.endsWith("Linux") || UIManager.getSystemLookAndFeelClassName().indexOf("Motif") > -1 || Startup.isSolaris10();
        return result;
    }

    private class LFListener
    implements PropertyChangeListener {
        private LFListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent pcl) {
            if ("lookAndFeel".equals(pcl.getPropertyName()) || "win.xpstyle.themeActive".equals(pcl.getPropertyName())) {
                Startup.this.uninstallPerLFDefaults();
                Startup.this.installPerLFDefaults();
            }
        }
    }

    private static final class CLValue
    implements UIDefaults.ActiveValue {
        private CLValue() {
        }

        @Override
        public ClassLoader createValue(UIDefaults defs) {
            return loader != null ? loader : Thread.currentThread().getContextClassLoader();
        }
    }

}

