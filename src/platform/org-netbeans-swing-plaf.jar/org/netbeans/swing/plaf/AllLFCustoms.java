/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf;

import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;
import java.util.HashMap;
import java.util.Map;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.metal.MetalLookAndFeel;
import org.netbeans.swing.plaf.LFCustoms;
import org.netbeans.swing.plaf.util.GuaranteedValue;

final class AllLFCustoms
extends LFCustoms {
    private static final boolean isMetal = null != UIManager.getLookAndFeel() && UIManager.getLookAndFeel().getClass() == MetalLookAndFeel.class;
    private static final boolean isWindows = null != UIManager.getLookAndFeel() && "Windows".equals(UIManager.getLookAndFeel().getID());

    AllLFCustoms() {
    }

    @Override
    public Object[] createApplicationSpecificKeysAndValues() {
        Object[] uiDefaults = new Object[]{"TabRenderer.selectedActivatedBackground", new GuaranteedValue(new String[]{"Table.selectionBackground", "info"}, (Object)Color.BLUE.brighter()), "TabRenderer.selectedActivatedForeground", new GuaranteedValue("Table.selectionForeground", (Object)Color.WHITE), "TabRenderer.selectedForeground", new GuaranteedValue("textText", (Object)Color.BLACK), "TabbedContainerUI", "org.netbeans.swing.tabcontrol.plaf.DefaultTabbedContainerUI", "SlidingTabDisplayerUI", "org.netbeans.swing.tabcontrol.plaf.BasicSlidingTabDisplayerUI", "IndexButtonUI", "org.netbeans.swing.tabcontrol.plaf.SlidingTabDisplayerButtonUI", "SlidingButtonUI", "org.netbeans.swing.tabcontrol.SlidingButtonUI", "Nb.ScrollPane.Border.color", new Color(127, 157, 185), "Nb.Editor.ErrorStripe.ScrollBar.Insets", new Insets(0, 0, 0, 0), "Nb.SplitPane.dividerSize.vertical", new Integer(4), "Nb.SplitPane.dividerSize.horizontal", new Integer(4)};
        return uiDefaults;
    }

    @Override
    public Object[] createGuaranteedKeysAndValues() {
        boolean hasCustomFontSize;
        ColorUIResource errorColor = new ColorUIResource(255, 0, 0);
        ColorUIResource warningColor = new ColorUIResource(51, 51, 51);
        int fontsize = 11;
        Integer in = (Integer)UIManager.get("customFontSize");
        boolean bl = hasCustomFontSize = in != null;
        if (hasCustomFontSize) {
            fontsize = in;
        }
        Object[] uiDefaults = new Object[]{"control", new GuaranteedValue("control", (Object)Color.LIGHT_GRAY), "controlShadow", new GuaranteedValue("controlShadow", (Object)Color.GRAY), "controlDkShadow", new GuaranteedValue("controlDkShadow", (Object)Color.DARK_GRAY), "textText", new GuaranteedValue("textText", (Object)Color.BLACK), "controlFont", new GuaranteedValue("controlFont", (Object)new Font("Dialog", 0, fontsize)), "nbDefaultFontSize", new Integer(11), "nb.errorForeground", new GuaranteedValue("nb.errorForeground", (Object)errorColor), "nb.warningForeground", new GuaranteedValue("nb.warningForeground", (Object)warningColor)};
        return uiDefaults;
    }

    public static void initCustomFontSize(int uiFontSize) {
        FontUIResource nbDialogPlain = new FontUIResource("Dialog", 0, uiFontSize);
        FontUIResource nbDialogBold = new FontUIResource("Dialog", 1, uiFontSize);
        FontUIResource nbSerifPlain = new FontUIResource("Serif", 0, uiFontSize);
        FontUIResource nbSansSerifPlain = new FontUIResource("SansSerif", 0, uiFontSize);
        FontUIResource nbMonospacedPlain = new FontUIResource("Monospaced", 0, uiFontSize);
        HashMap<Font, Font> fontTranslation = new HashMap<Font, Font>(5);
        if ("Nimbus".equals(UIManager.getLookAndFeel().getID())) {
            AllLFCustoms.switchFont("defaultFont", fontTranslation, uiFontSize, nbDialogPlain);
        }
        AllLFCustoms.switchFont("controlFont", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("Button.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("ToggleButton.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("RadioButton.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("CheckBox.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("ColorChooser.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("ComboBox.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("Label.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("List.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("FileChooser.listFont", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("MenuBar.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("MenuItem.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("MenuItem.acceleratorFont", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("RadioButtonMenuItem.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("CheckBoxMenuItem.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("Menu.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("PopupMenu.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("OptionPane.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("OptionPane.messageFont", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("Panel.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("ProgressBar.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("ScrollPane.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("Viewport.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("TabbedPane.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("Table.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("TableHeader.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("TextField.font", fontTranslation, uiFontSize, nbSansSerifPlain);
        AllLFCustoms.switchFont("PasswordField.font", fontTranslation, uiFontSize, nbMonospacedPlain);
        AllLFCustoms.switchFont("TextArea.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("TextPane.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("EditorPane.font", fontTranslation, uiFontSize, nbSerifPlain);
        AllLFCustoms.switchFont("TitledBorder.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("ToolBar.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("ToolTip.font", fontTranslation, uiFontSize, nbSansSerifPlain);
        AllLFCustoms.switchFont("Tree.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("InternalFrame.titleFont", fontTranslation, uiFontSize, nbDialogBold);
        AllLFCustoms.switchFont("windowTitleFont", fontTranslation, uiFontSize, nbDialogBold);
        AllLFCustoms.switchFont("Spinner.font", fontTranslation, uiFontSize, nbDialogPlain);
        AllLFCustoms.switchFont("FormattedTextField.font", fontTranslation, uiFontSize, nbDialogPlain);
    }

    private static void switchFont(String uiKey, Map<Font, Font> fontTranslation, int uiFontSize, Font defaultFont) {
        Font newFont;
        Font oldFont = UIManager.getFont(uiKey);
        Font font = newFont = null == oldFont || isMetal ? defaultFont : fontTranslation.get(oldFont);
        if (null == newFont) {
            newFont = isWindows ? oldFont.deriveFont((float)uiFontSize) : new FontUIResource(oldFont.getFontName(), oldFont.getStyle(), uiFontSize);
            fontTranslation.put(oldFont, newFont);
        }
        UIManager.put(uiKey, newFont);
    }
}

