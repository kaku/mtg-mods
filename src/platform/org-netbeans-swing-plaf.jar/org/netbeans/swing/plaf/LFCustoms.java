/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf;

import java.awt.Color;
import java.util.logging.Logger;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.text.AttributeSet;
import javax.swing.text.StyleConstants;

public abstract class LFCustoms {
    private Object[] lfKeysAndValues = null;
    private Object[] appKeysAndValues = null;
    private Object[] guaranteedKeysAndValues = null;
    protected static final String WORKPLACE_FILL = "nb_workplace_fill";
    private static String textFgColorHTML = "";
    private static Color textFgColor = null;
    private static final int shiftValue = 64;
    protected static final String CUSTOM_FONT_SIZE = "customFontSize";
    protected static final String DEFAULT_FONT_SIZE = "nbDefaultFontSize";
    protected static final String EDITOR_STATUS_LEFT_BORDER = "Nb.Editor.Status.leftBorder";
    protected static final String EDITOR_STATUS_INNER_BORDER = "Nb.Editor.Status.innerBorder";
    protected static final String EDITOR_STATUS_RIGHT_BORDER = "Nb.Editor.Status.rightBorder";
    protected static final String EDITOR_STATUS_ONLYONEBORDER = "Nb.Editor.Status.onlyOneBorder";
    protected static final String EDITOR_TOOLBAR_BORDER = "Nb.Editor.Toolbar.border";
    protected static final String EDITOR_ERRORSTRIPE_SCROLLBAR_INSETS = "Nb.Editor.ErrorStripe.ScrollBar.Insets";
    protected static final String EXPLORER_STATUS_BORDER = "Nb.Explorer.Status.border";
    protected static final String EXPLORER_FOLDER_ICON = "Nb.Explorer.Folder.icon";
    protected static final String EXPLORER_FOLDER_OPENED_ICON = "Nb.Explorer.Folder.openedIcon";
    protected static final String DESKTOP_BORDER = "Nb.Desktop.border";
    public static final String SCROLLPANE_BORDER = "Nb.ScrollPane.border";
    protected static final String TOOLBAR_UI = "Nb.Toolbar.ui";
    protected static final String DESKTOP_BACKGROUND = "Nb.Desktop.background";
    public static final String SCROLLPANE_BORDER_COLOR = "Nb.ScrollPane.Border.color";
    protected static final String OUTPUT_SELECTION_BACKGROUND = "nb.output.selectionBackground";
    protected static final String OUTPUT_HYPERLINK_FOREGROUND = "nb.hyperlink.foreground";
    protected static final String OUTPUT_BACKGROUND = "nb.output.background";
    protected static final String OUTPUT_FOREGROUND = "nb.output.foreground";
    protected static final String PROPSHEET_ALTERNATE_ROW_COLOR = "Tree.altbackground";
    protected static final String PROPSHEET_SET_BACKGROUND = "PropSheet.setBackground";
    protected static final String PROPSHEET_SELECTED_SET_BACKGROUND = "PropSheet.selectedSetBackground";
    protected static final String PROPSHEET_SET_FOREGROUND = "PropSheet.setForeground";
    protected static final String PROPSHEET_SELECTED_SET_FOREGROUND = "PropSheet.selectedSetForeground";
    protected static final String PROPSHEET_DISABLED_FOREGROUND = "PropSheet.disabledForeground";
    protected static final String PROPSHEET_SELECTION_BACKGROUND = "PropSheet.selectionBackground";
    protected static final String PROPSHEET_SELECTION_FOREGROUND = "PropSheet.selectionForeground";
    protected static final String PROPSHEET_BUTTON_FOREGROUND = "PropSheet.customButtonForeground";
    protected static final String PROPSHEET_BUTTON_COLOR = "netbeans.ps.buttonColor";
    protected static final String PROPSHEET_BACKGROUND = "netbeans.ps.background";
    protected static final String PROPSHEET_ICON_MARGIN = "netbeans.ps.iconmargin";
    protected static final String PROPSHEET_ROWHEIGHT = "netbeans.ps.rowheight";
    protected static final String ERROR_FOREGROUND = "nb.errorForeground";
    protected static final String WARNING_FOREGROUND = "nb.warningForeground";
    protected static final String EDITOR_TABBED_CONTAINER_UI = "TabbedContainerUI";
    protected static final String EDITOR_TAB_DISPLAYER_UI = "EditorTabDisplayerUI";
    protected static final String VIEW_TAB_DISPLAYER_UI = "ViewTabDisplayerUI";
    protected static final String SLIDING_TAB_DISPLAYER_UI = "SlidingTabDisplayerUI";
    protected static final String SLIDING_TAB_BUTTON_UI = "IndexButtonUI";
    protected static final String SLIDING_BUTTON_UI = "SlidingButtonUI";
    protected static final String EDITOR_TAB_CONTENT_BORDER = "TabbedContainer.editor.contentBorder";
    protected static final String EDITOR_TAB_TABS_BORDER = "TabbedContainer.editor.tabsBorder";
    protected static final String EDITOR_TAB_OUTER_BORDER = "TabbedContainer.editor.outerBorder";
    protected static final String VIEW_TAB_CONTENT_BORDER = "TabbedContainer.view.contentBorder";
    protected static final String VIEW_TAB_TABS_BORDER = "TabbedContainer.view.tabsBorder";
    protected static final String VIEW_TAB_OUTER_BORDER = "TabbedContainer.view.outerBorder";
    protected static final String SLIDING_TAB_CONTENT_BORDER = "TabbedContainer.sliding.contentBorder";
    protected static final String SLIDING_TAB_TABS_BORDER = "TabbedContainer.sliding.tabsBorder";
    protected static final String SLIDING_TAB_OUTER_BORDER = "TabbedContainer.sliding.outerBorder";
    protected static final String TAB_ACTIVE_SELECTION_BACKGROUND = "TabRenderer.selectedActivatedBackground";
    protected static final String TAB_ACTIVE_SELECTION_FOREGROUND = "TabRenderer.selectedActivatedForeground";
    protected static final String TAB_SELECTION_FOREGROUND = "TabRenderer.selectedForeground";
    protected static final String TAB_SELECTION_BACKGROUND = "TabRenderer.selectedBackground";
    protected static final String EXPLORER_MINISTATUSBAR_BORDER = "nb.explorer.ministatusbar.border";
    protected static final String DESKTOP_SPLITPANE_BORDER = "nb.desktop.splitpane.border";
    protected static final String PROPERTYSHEET_BOOTSTRAP = "nb.propertysheet";
    protected static final String SPLIT_PANE_DIVIDER_SIZE_VERTICAL = "Nb.SplitPane.dividerSize.vertical";
    protected static final String SPLIT_PANE_DIVIDER_SIZE_HORIZONTAL = "Nb.SplitPane.dividerSize.horizontal";
    public static final String CONTROLFONT = "controlFont";
    public static final String SYSTEMFONT = "systemFont";
    public static final String USERFONT = "userFont";
    public static final String MENUFONT = "menuFont";
    public static final String WINDOWTITLEFONT = "windowTitleFont";
    public static final String SUBFONT = "subFont";
    public static final String LISTFONT = "List.font";
    public static final String TREEFONT = "Tree.font";
    public static final String PANELFONT = "Panel.font";
    public static final String SPINNERFONT = "Spinner.font";
    public static final String PROGRESS_CANCEL_BUTTON_ICON = "nb.progress.cancel.icon";
    public static final String PROGRESS_CANCEL_BUTTON_ROLLOVER_ICON = "nb.progress.cancel.icon.mouseover";
    public static final String PROGRESS_CANCEL_BUTTON_PRESSED_ICON = "nb.progress.cancel.icon.pressed";

    public static String getHexString(int color) {
        String result = Integer.toHexString(color).toUpperCase();
        if (result.length() == 1) {
            return "" + '0' + result;
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static String getTextFgColorHTML() {
        Class<LFCustoms> class_ = LFCustoms.class;
        synchronized (LFCustoms.class) {
            if (textFgColorHTML.isEmpty()) {
                Object o = UIManager.getLookAndFeel().getDefaults().get("windowText");
                if (null == o) {
                    o = UIManager.getLookAndFeel().getDefaults().get("Tree.foreground");
                }
                if (o instanceof Color) {
                    Color resource = (Color)o;
                    textFgColorHTML = "<font color=#" + LFCustoms.getHexString(resource.getRed()) + LFCustoms.getHexString(resource.getGreen()) + LFCustoms.getHexString(resource.getBlue()) + ">";
                } else {
                    textFgColorHTML = "<font color=#000000>";
                    Logger.getLogger(LFCustoms.class.getName()).warning("BUG: getTextFgColorHTML: color isn't available");
                }
            }
            // ** MonitorExit[var0] (shouldn't be in output)
            return textFgColorHTML;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static Color getTextFgColor() {
        Class<LFCustoms> class_ = LFCustoms.class;
        synchronized (LFCustoms.class) {
            if (textFgColor == null) {
                Object o = UIManager.getLookAndFeel().getDefaults().get("windowText");
                if (null == o) {
                    o = UIManager.getLookAndFeel().getDefaults().get("Tree.foreground");
                }
                if (o instanceof Color) {
                    textFgColor = (Color)o;
                } else {
                    textFgColor = Color.BLACK;
                    Logger.getLogger(LFCustoms.class.getName()).warning("BUG: getTextFgColor: color isn't available");
                }
            }
            // ** MonitorExit[var0] (shouldn't be in output)
            return textFgColor;
        }
    }

    private static int brighter(int color) {
        return Math.min(color + 64, 255);
    }

    private static int darker(int color) {
        return Math.max(color - 64, 0);
    }

    public static Color shiftColor(Color color) {
        Color textColor = LFCustoms.getTextFgColor();
        if (textColor.getRed() > 127 || textColor.getGreen() > 127 || textColor.getBlue() > 127) {
            return new Color(LFCustoms.brighter(color.getRed()), LFCustoms.brighter(color.getGreen()), LFCustoms.brighter(color.getBlue()));
        }
        return new Color(LFCustoms.darker(color.getRed()), LFCustoms.darker(color.getGreen()), LFCustoms.darker(color.getBlue()));
    }

    public static Color getForeground(AttributeSet a) {
        Color fg = (Color)a.getAttribute(StyleConstants.Foreground);
        if (fg == null) {
            fg = LFCustoms.getTextFgColor();
        }
        return fg;
    }

    Object[] getLookAndFeelCustomizationKeysAndValues() {
        if (this.lfKeysAndValues == null) {
            this.lfKeysAndValues = this.createLookAndFeelCustomizationKeysAndValues();
        }
        return this.lfKeysAndValues;
    }

    Object[] getApplicationSpecificKeysAndValues() {
        if (this.appKeysAndValues == null) {
            this.appKeysAndValues = this.createApplicationSpecificKeysAndValues();
        }
        return this.appKeysAndValues;
    }

    Object[] getGuaranteedKeysAndValues() {
        if (this.guaranteedKeysAndValues == null) {
            this.guaranteedKeysAndValues = this.createGuaranteedKeysAndValues();
        }
        return this.guaranteedKeysAndValues;
    }

    Object[] allKeys() {
        int size;
        int i;
        Object[] additional = this.additionalKeys();
        int n = size = additional == null ? 0 : additional.length;
        if (this.appKeysAndValues != null) {
            size += this.appKeysAndValues.length / 2;
        }
        if (this.guaranteedKeysAndValues != null) {
            size += this.guaranteedKeysAndValues.length / 2;
        }
        if (this.lfKeysAndValues != null) {
            size += this.lfKeysAndValues.length / 2;
        }
        Object[] result = new Object[size];
        int ct = 0;
        if (this.lfKeysAndValues != null) {
            for (i = 0; i < this.lfKeysAndValues.length; i += 2) {
                result[ct++] = this.lfKeysAndValues[i];
            }
        }
        if (this.guaranteedKeysAndValues != null) {
            for (i = 0; i < this.guaranteedKeysAndValues.length; i += 2) {
                result[ct++] = this.guaranteedKeysAndValues[i];
            }
        }
        if (this.appKeysAndValues != null) {
            for (i = 0; i < this.appKeysAndValues.length; i += 2) {
                result[ct++] = this.appKeysAndValues[i];
            }
        }
        if (additional != null) {
            for (i = 0; i < additional.length; ++i) {
                result[ct++] = additional[i];
            }
        }
        return result;
    }

    protected Object[] additionalKeys() {
        return null;
    }

    void disposeValues() {
        if (this.lfKeysAndValues != null) {
            this.disposeValues(this.lfKeysAndValues);
        }
        this.disposeValues(this.appKeysAndValues);
        this.disposeValues(this.guaranteedKeysAndValues);
    }

    private void disposeValues(Object[] arr) {
        for (int i = 1; i < arr.length; i += 2) {
            arr[i] = null;
        }
    }

    public Object[] createLookAndFeelCustomizationKeysAndValues() {
        return new Object[0];
    }

    public Object[] createApplicationSpecificKeysAndValues() {
        return new Object[0];
    }

    public Object[] createGuaranteedKeysAndValues() {
        return new Object[0];
    }
}

