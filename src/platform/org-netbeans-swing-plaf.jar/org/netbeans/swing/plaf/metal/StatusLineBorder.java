/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.metal;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.UIManager;
import javax.swing.border.AbstractBorder;

class StatusLineBorder
extends AbstractBorder {
    public static final int LEFT = 1;
    public static final int TOP = 2;
    public static final int RIGHT = 4;
    private Insets insets;
    private int type;

    public StatusLineBorder(int type) {
        this.type = type;
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
        g.translate(x, y);
        Color shadowC = UIManager.getColor("controlShadow");
        Color highlightC = UIManager.getColor("controlHighlight");
        Color middleC = UIManager.getColor("control");
        if ((this.type & 2) != 0) {
            g.setColor(shadowC);
            g.drawLine(0, 0, w - 1, 0);
            g.drawLine(0, 3, w - 1, 3);
            g.setColor(highlightC);
            g.drawLine(0, 1, w - 1, 1);
            g.setColor(middleC);
            g.drawLine(0, 2, w - 1, 2);
        }
        if ((this.type & 1) != 0) {
            g.setColor(middleC);
            g.drawLine(0, 2, 0, h - 1);
            g.setColor(shadowC);
            g.drawLine(1, 3, 1, h - 1);
        }
        if ((this.type & 4) != 0) {
            g.setColor(shadowC);
            g.drawLine(w - 2, 3, w - 2, h - 1);
            g.setColor(highlightC);
            g.drawLine(w - 1, 4, w - 1, h - 1);
            g.setColor(middleC);
            g.drawLine(w - 1, 3, w - 1, 3);
        }
        g.translate(- x, - y);
    }

    @Override
    public Insets getBorderInsets(Component c) {
        if (this.insets == null) {
            this.insets = this.getBorderInsets(c, new Insets(0, 0, 0, 0));
        }
        return this.insets;
    }

    @Override
    public Insets getBorderInsets(Component c, Insets insets) {
        insets.left = (this.type & 1) != 0 ? 2 : 0;
        insets.top = (this.type & 2) != 0 ? 4 : 0;
        insets.right = (this.type & 4) != 0 ? 2 : 0;
        insets.bottom = 0;
        return insets;
    }
}

