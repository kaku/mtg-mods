/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.swing.plaf.metal;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import javax.swing.BorderFactory;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import org.netbeans.swing.plaf.LFCustoms;
import org.netbeans.swing.plaf.metal.EditorToolbarBorder;
import org.netbeans.swing.plaf.metal.MetalScrollPaneBorder;
import org.netbeans.swing.plaf.metal.StatusLineBorder;
import org.netbeans.swing.plaf.util.GuaranteedValue;
import org.netbeans.swing.plaf.util.UIBootstrapValue;
import org.netbeans.swing.plaf.util.UIUtils;

public final class MetalLFCustoms
extends LFCustoms {
    @Override
    public Object[] createLookAndFeelCustomizationKeysAndValues() {
        int fontsize = 11;
        Integer in = (Integer)UIManager.get("customFontSize");
        if (in != null) {
            fontsize = in;
        }
        Font controlFont = new Font("Dialog", 0, fontsize);
        Object[] result = new Object[]{"controlFont", controlFont, "systemFont", controlFont, "userFont", controlFont, "menuFont", controlFont, "windowTitleFont", controlFont, "List.font", controlFont, "Tree.font", controlFont, "Panel.font", controlFont, "subFont", new Font("Dialog", 0, Math.min(fontsize - 1, 6)), "textInactiveText", Color.GRAY, "Spinner.font", controlFont, "Nb.Editor.ErrorStripe.ScrollBar.Insets", new Insets(16, 0, 16, 0), "NbSlideBar.GroupSeparator.Gap.Before", 15, "NbSlideBar.GroupSeparator.Gap.After", 5, "NbSlideBar.RestoreButton.Gap", 10};
        return result;
    }

    @Override
    public Object[] createApplicationSpecificKeysAndValues() {
        Border outerBorder = BorderFactory.createLineBorder(UIManager.getColor("controlShadow"));
        MetalPropertySheetColorings propertySheetColorings = new MetalPropertySheetColorings();
        Color unfocusedSelBg = UIManager.getColor("controlShadow");
        if (!Color.WHITE.equals(unfocusedSelBg.brighter())) {
            unfocusedSelBg = unfocusedSelBg.brighter();
        }
        Object[] result = new Object[]{"Nb.Desktop.border", new EmptyBorder(1, 1, 1, 1), "Nb.ScrollPane.border", new MetalScrollPaneBorder(), "Nb.Explorer.Status.border", new StatusLineBorder(2), "Nb.Editor.Status.leftBorder", new StatusLineBorder(6), "Nb.Editor.Status.rightBorder", new StatusLineBorder(3), "Nb.Editor.Status.innerBorder", new StatusLineBorder(7), "Nb.Editor.Status.onlyOneBorder", new StatusLineBorder(2), "Nb.Editor.Toolbar.border", new EditorToolbarBorder(), "nb.propertysheet", propertySheetColorings, "EditorTabDisplayerUI", "org.netbeans.swing.tabcontrol.plaf.MetalEditorTabDisplayerUI", "ViewTabDisplayerUI", "org.netbeans.swing.tabcontrol.plaf.MetalViewTabDisplayerUI", "SlidingButtonUI", "org.netbeans.swing.tabcontrol.plaf.MetalSlidingButtonUI", "TabbedContainer.editor.outerBorder", outerBorder, "TabbedContainer.view.outerBorder", outerBorder, "Nb.browser.picker.background.light", new Color(249, 249, 249), "Nb.browser.picker.foreground.light", new Color(130, 130, 130), "nb.explorer.ministatusbar.border", BorderFactory.createMatteBorder(1, 0, 0, 0, UIManager.getColor("controlShadow")), "nb.explorer.unfocusedSelBg", unfocusedSelBg, "nb.progress.cancel.icon", MetalLFCustoms.filterImage(UIUtils.loadImage("org/netbeans/swing/plaf/resources/cancel_task_linux_mac.png")), "nbProgressBar.popupDynaText.foreground", new Color(115, 115, 115), "nbProgressBar.popupText.foreground", UIManager.getColor("TextField.foreground"), "nbProgressBar.popupText.selectBackground", UIManager.getColor("List.selectionBackground"), "nbProgressBar.popupText.selectForeground", UIManager.getColor("List.selectionForeground"), "Nb.browser.picker.background.light", new Color(249, 249, 249), "Nb.browser.picker.foreground.light", new Color(130, 130, 130)};
        return UIUtils.addInputMapsWithoutCtrlPageUpAndCtrlPageDown(result);
    }

    private static Image filterImage(Image image) {
        if (null == image) {
            return image;
        }
        Object obj = UIManager.get("nb.imageicon.filter");
        if (obj instanceof RGBImageFilter && null != image) {
            RGBImageFilter imageIconFilter = (RGBImageFilter)obj;
            image = Toolkit.getDefaultToolkit().createImage(new FilteredImageSource(image.getSource(), imageIconFilter));
        }
        return image;
    }

    private class MetalPropertySheetColorings
    extends UIBootstrapValue.Lazy {
        public MetalPropertySheetColorings() {
            super(null);
        }

        @Override
        public Object[] createKeysAndValues() {
            return new Object[]{"PropSheet.selectionBackground", new GuaranteedValue("PropSheet.selectionBackground", (Object)new Color(204, 204, 255)), "PropSheet.selectionForeground", new GuaranteedValue("PropSheet.selectionForeground", (Object)Color.BLACK), "PropSheet.setBackground", new GuaranteedValue("PropSheet.setBackground", (Object)new Color(224, 224, 224)), "PropSheet.setForeground", new GuaranteedValue("PropSheet.setForeground", (Object)Color.BLACK), "PropSheet.selectedSetBackground", new GuaranteedValue("PropSheet.selectedSetBackground", (Object)new Color(204, 204, 255)), "PropSheet.selectedSetForeground", new GuaranteedValue("PropSheet.selectedSetForeground", (Object)Color.BLACK), "PropSheet.disabledForeground", new Color(153, 153, 153)};
        }
    }

}

