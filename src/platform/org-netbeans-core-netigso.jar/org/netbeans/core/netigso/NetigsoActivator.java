/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.osgi.framework.Bundle
 */
package org.netbeans.core.netigso;

import java.util.HashMap;
import org.netbeans.core.netigso.Netigso;
import org.osgi.framework.Bundle;

final class NetigsoActivator
extends HashMap<Bundle, ClassLoader> {
    private final Netigso netigso;

    public NetigsoActivator(Netigso netigso) {
        this.netigso = netigso;
    }

    @Override
    public ClassLoader get(Object o) {
        if (o instanceof Bundle) {
            String loc = ((Bundle)o).getLocation();
            String pref = "netigso://";
            if (loc != null && loc.startsWith("netigso://")) {
                String cnb = loc.substring("netigso://".length());
                return this.netigso.findClassLoader(cnb);
            }
        }
        return null;
    }
}

