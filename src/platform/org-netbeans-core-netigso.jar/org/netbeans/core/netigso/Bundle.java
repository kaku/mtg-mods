/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.netigso;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String DEFAULT_BUNDLE_START_LEVEL() {
        return NbBundle.getMessage(Bundle.class, (String)"DEFAULT_BUNDLE_START_LEVEL");
    }

    static String FIND_COVERED_PKGS() {
        return NbBundle.getMessage(Bundle.class, (String)"FIND_COVERED_PKGS");
    }

    static String FRAMEWORK_START_LEVEL() {
        return NbBundle.getMessage(Bundle.class, (String)"FRAMEWORK_START_LEVEL");
    }

    static String MODULE_START_LEVEL() {
        return NbBundle.getMessage(Bundle.class, (String)"MODULE_START_LEVEL");
    }

    private void Bundle() {
    }
}

