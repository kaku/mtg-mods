/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.startup.MainLookup
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 *  org.osgi.framework.Bundle
 *  org.osgi.framework.BundleContext
 *  org.osgi.framework.BundleEvent
 *  org.osgi.framework.BundleListener
 *  org.osgi.framework.ServiceEvent
 *  org.osgi.framework.ServiceListener
 *  org.osgi.framework.ServiceReference
 *  org.osgi.framework.SynchronousBundleListener
 *  org.osgi.framework.Version
 *  org.osgi.framework.launch.Framework
 */
package org.netbeans.core.netigso;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.core.netigso.Netigso;
import org.netbeans.core.startup.MainLookup;
import org.openide.util.lookup.InstanceContent;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.BundleListener;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.SynchronousBundleListener;
import org.osgi.framework.Version;
import org.osgi.framework.launch.Framework;

final class NetigsoServices
implements SynchronousBundleListener,
ServiceListener,
InstanceContent.Convertor<ServiceReference, Object> {
    private final Netigso netigso;

    NetigsoServices(Netigso netigso, Framework f) {
        this.netigso = netigso;
        for (ServiceReference ref : f.getRegisteredServices()) {
            MainLookup.register((Object)ref, (InstanceContent.Convertor)this);
        }
        f.getBundleContext().addServiceListener((ServiceListener)this);
        f.getBundleContext().addBundleListener((BundleListener)this);
    }

    public void serviceChanged(ServiceEvent ev) {
        ServiceReference ref = ev.getServiceReference();
        if (ev.getType() == 1) {
            MainLookup.register((Object)ref, (InstanceContent.Convertor)this);
        }
        if (ev.getType() == 4) {
            MainLookup.unregister((Object)ref, (InstanceContent.Convertor)this);
        }
    }

    public Object convert(ServiceReference obj) {
        Bundle bundle = obj.getBundle();
        if (bundle != null) {
            return bundle.getBundleContext().getService(obj);
        }
        return null;
    }

    public Class<? extends Object> type(ServiceReference obj) {
        Bundle bundle;
        String[] arr = (String[])obj.getProperty("objectClass");
        if (arr.length > 0 && (bundle = obj.getBundle()) != null) {
            try {
                return bundle.loadClass(arr[0]);
            }
            catch (ClassNotFoundException ex) {
                Netigso.LOG.log(Level.INFO, "Cannot load service class", arr[0]);
            }
        }
        return Object.class;
    }

    public String id(ServiceReference obj) {
        Long id = (Long)obj.getProperty("service.id");
        return "OSGiService[" + id + "]";
    }

    public String displayName(ServiceReference obj) {
        return (String)obj.getProperty("service.description");
    }

    public void bundleChanged(BundleEvent be) {
        if (be.getBundle().getLocation().startsWith("netigso://")) {
            return;
        }
        this.netigso.notifyBundleChange(be.getBundle().getSymbolicName(), be.getBundle().getVersion(), be.getType());
    }
}

