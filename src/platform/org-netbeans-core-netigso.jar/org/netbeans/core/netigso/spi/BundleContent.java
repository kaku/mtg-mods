/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.netigso.spi;

import java.io.IOException;

public interface BundleContent {
    public byte[] resource(String var1) throws IOException;
}

