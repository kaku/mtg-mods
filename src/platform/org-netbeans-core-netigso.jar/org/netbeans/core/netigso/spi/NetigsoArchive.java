/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.ArchiveResources
 */
package org.netbeans.core.netigso.spi;

import java.io.IOException;
import java.security.ProtectionDomain;
import org.netbeans.ArchiveResources;
import org.netbeans.core.netigso.Netigso;
import org.netbeans.core.netigso.NetigsoArchiveFactory;
import org.netbeans.core.netigso.spi.BundleContent;

public final class NetigsoArchive {
    private final Netigso netigso;
    private final long bundleId;
    private final ArchiveResources content;

    NetigsoArchive(Netigso n, long id, final BundleContent content) {
        this.netigso = n;
        this.bundleId = id;
        this.content = new ArchiveResources(){

            public byte[] resource(String name) throws IOException {
                return content == null ? null : content.resource(name);
            }

            public String getIdentifier() {
                return "netigso://" + NetigsoArchive.this.bundleId + "!/";
            }
        };
    }

    public NetigsoArchive forBundle(long bundleId, BundleContent content) {
        return new NetigsoArchive(this.netigso, bundleId, content);
    }

    public byte[] fromArchive(String resource) throws IOException {
        return this.netigso.fromArchive(this.bundleId, resource, this.content);
    }

    public boolean isActive() {
        return this.netigso.isArchiveActive();
    }

    public final byte[] patchByteCode(ClassLoader l, String className, ProtectionDomain pd, byte[] arr) {
        return this.netigso.patchBC(l, className, pd, arr);
    }

    static {
        NetigsoArchiveFactory f = new NetigsoArchiveFactory(){

            @Override
            protected NetigsoArchive create(Netigso n) {
                return new NetigsoArchive(n, 0, null);
            }
        };
    }

}

