/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package org.netbeans.core.netigso;

import org.netbeans.core.netigso.Netigso;
import org.netbeans.core.netigso.spi.NetigsoArchive;
import org.openide.util.Exceptions;

public abstract class NetigsoArchiveFactory {
    static NetigsoArchiveFactory DEFAULT;

    protected NetigsoArchiveFactory() {
        DEFAULT = this;
    }

    protected abstract NetigsoArchive create(Netigso var1);

    static {
        try {
            Class.forName(NetigsoArchive.class.getName(), true, NetigsoArchive.class.getClassLoader());
        }
        catch (ClassNotFoundException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }
}

