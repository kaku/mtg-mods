/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.ArchiveResources
 *  org.netbeans.Module
 *  org.netbeans.Module$PackageExport
 *  org.netbeans.ModuleManager
 *  org.netbeans.NetigsoFramework
 *  org.netbeans.ProxyClassLoader
 *  org.netbeans.Stamps
 *  org.netbeans.Stamps$Updater
 *  org.netbeans.core.startup.Main
 *  org.openide.modules.ModuleInfo
 *  org.openide.modules.Places
 *  org.openide.modules.SpecificationVersion
 *  org.openide.util.Enumerations
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.osgi.framework.Bundle
 *  org.osgi.framework.BundleContext
 *  org.osgi.framework.BundleException
 *  org.osgi.framework.FrameworkEvent
 *  org.osgi.framework.ServiceReference
 *  org.osgi.framework.Version
 *  org.osgi.framework.launch.Framework
 *  org.osgi.framework.launch.FrameworkFactory
 *  org.osgi.service.packageadmin.PackageAdmin
 *  org.osgi.service.packageadmin.RequiredBundle
 *  org.osgi.service.startlevel.StartLevel
 */
package org.netbeans.core.netigso;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.security.ProtectionDomain;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.ArchiveResources;
import org.netbeans.Module;
import org.netbeans.ModuleManager;
import org.netbeans.NetigsoFramework;
import org.netbeans.ProxyClassLoader;
import org.netbeans.Stamps;
import org.netbeans.core.netigso.Bundle;
import org.netbeans.core.netigso.NetigsoActivator;
import org.netbeans.core.netigso.NetigsoArchiveFactory;
import org.netbeans.core.netigso.NetigsoLoader;
import org.netbeans.core.netigso.NetigsoServices;
import org.netbeans.core.netigso.spi.NetigsoArchive;
import org.netbeans.core.startup.Main;
import org.openide.modules.ModuleInfo;
import org.openide.modules.Places;
import org.openide.modules.SpecificationVersion;
import org.openide.util.Enumerations;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.FrameworkEvent;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.Version;
import org.osgi.framework.launch.Framework;
import org.osgi.framework.launch.FrameworkFactory;
import org.osgi.service.packageadmin.PackageAdmin;
import org.osgi.service.packageadmin.RequiredBundle;
import org.osgi.service.startlevel.StartLevel;

public final class Netigso
extends NetigsoFramework
implements Cloneable,
Stamps.Updater {
    static final Logger LOG = Logger.getLogger(Netigso.class.getName());
    private static final AtomicBoolean SELF_QUERY = new AtomicBoolean();
    private static final String[] EMPTY = new String[0];
    private Framework framework;
    private ClassLoader frameworkLoader;
    private NetigsoActivator activator;
    private Integer defaultStartLevel;
    private String defaultCoveredPkgs;
    private final Map<String, String[]> registered = new HashMap<String, String[]>();
    private static final RequestProcessor RP = new RequestProcessor("Netigso Events");

    protected NetigsoFramework clone() {
        return new Netigso();
    }

    Framework getFramework() {
        return this.framework;
    }

    protected ClassLoader findFrameworkClassLoader() {
        ClassLoader l = this.frameworkLoader;
        if (l != null) {
            return l;
        }
        Framework f = this.framework;
        if (f != null) {
            this.frameworkLoader = f.getClass().getClassLoader();
            return this.frameworkLoader;
        }
        return this.getClass().getClassLoader();
    }

    protected void prepare(Lookup lkp, Collection<? extends Module> preregister) {
        if (this.framework == null) {
            FrameworkFactory frameworkFactory;
            this.readBundles();
            HashMap<String, Object> configMap = new HashMap<String, Object>();
            this.injectSystemProperties(configMap);
            String cache = this.getNetigsoCache().getPath();
            configMap.put("org.osgi.framework.storage", cache);
            this.activator = new NetigsoActivator(this);
            configMap.put("netigso.archive", NetigsoArchiveFactory.DEFAULT.create(this));
            if (!configMap.containsKey("felix.log.level")) {
                configMap.put("felix.log.level", "4");
            }
            configMap.put("felix.bootdelegation.classloaders", this.activator);
            String startLevel = Bundle.FRAMEWORK_START_LEVEL();
            if (!startLevel.isEmpty()) {
                configMap.put("org.osgi.framework.startlevel.beginning", startLevel);
            }
            if ((frameworkFactory = (FrameworkFactory)lkp.lookup(FrameworkFactory.class)) == null) {
                throw new IllegalStateException("Cannot find OSGi framework implementation. Is org.netbeans.libs.felix module or similar enabled?");
            }
            this.framework = frameworkFactory.newFramework(configMap);
            try {
                this.framework.init();
                NetigsoServices ns = new NetigsoServices(this, this.framework);
            }
            catch (BundleException ex) {
                LOG.log(Level.SEVERE, "Cannot start OSGi framework", (Throwable)ex);
            }
            LOG.finer("OSGi Container initialized");
        }
        for (Module mi : preregister) {
            try {
                this.fakeOneModule(mi, null);
            }
            catch (IOException ex) {
                LOG.log(Level.WARNING, "Cannot fake " + mi.getCodeName(), ex);
            }
        }
    }

    protected Set<String> start(Collection<? extends Module> allModules) {
        return Netigso.toActivate(this.framework, allModules);
    }

    protected void start() {
        try {
            this.framework.start();
        }
        catch (BundleException ex) {
            LOG.log(Level.WARNING, "Cannot start Container" + (Object)this.framework, (Throwable)ex);
        }
    }

    private void injectSystemProperties(Map configProps) {
        Enumeration e = System.getProperties().propertyNames();
        while (e.hasMoreElements()) {
            String key = e.nextElement().toString();
            if (!key.startsWith("felix.") && !key.startsWith("org.osgi.framework.")) continue;
            configProps.put(key, System.getProperty(key));
        }
    }

    private static Set<String> toActivate(Framework f, Collection<? extends Module> allModules) {
        ServiceReference sr = f.getBundleContext().getServiceReference("org.osgi.service.packageadmin.PackageAdmin");
        if (sr == null) {
            return null;
        }
        PackageAdmin pkgAdm = (PackageAdmin)f.getBundleContext().getService(sr);
        if (pkgAdm == null) {
            return null;
        }
        HashSet<String> allCnbs = new HashSet<String>(allModules.size() * 2);
        for (ModuleInfo m : allModules) {
            allCnbs.add(m.getCodeNameBase());
        }
        HashSet<String> needEnablement = new HashSet<String>();
        for (org.osgi.framework.Bundle b : f.getBundleContext().getBundles()) {
            RequiredBundle[] arr;
            String loc = b.getLocation();
            if (!loc.startsWith("netigso://") || (arr = pkgAdm.getRequiredBundles(loc = loc.substring("netigso://".length()))) == null) continue;
            for (RequiredBundle rb : arr) {
                for (org.osgi.framework.Bundle n : rb.getRequiringBundles()) {
                    if (!allCnbs.contains(n.getSymbolicName().replace('-', '_'))) continue;
                    needEnablement.add(loc);
                }
            }
        }
        return needEnablement;
    }

    protected void shutdown() {
        try {
            if (this.framework != null) {
                this.framework.stop();
                this.framework.waitForStop(10000);
            }
            this.framework = null;
            this.frameworkLoader = null;
        }
        catch (InterruptedException ex) {
            LOG.log(Level.WARNING, "Wait for shutdown failed" + (Object)this.framework, ex);
        }
        catch (BundleException ex) {
            LOG.log(Level.WARNING, "Cannot start Container" + (Object)this.framework, (Throwable)ex);
        }
    }

    protected int defaultStartLevel() {
        if (this.defaultStartLevel == null) {
            this.defaultStartLevel = Integer.parseInt(Bundle.DEFAULT_BUNDLE_START_LEVEL());
        }
        return this.defaultStartLevel;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected Set<String> createLoader(ModuleInfo m, ProxyClassLoader pcl, File jar) throws IOException {
        try {
            assert (this.registered.containsKey(m.getCodeNameBase()));
            org.osgi.framework.Bundle b = this.findBundle(m.getCodeNameBase());
            if (b == null) {
                for (org.osgi.framework.Bundle bb : this.framework.getBundleContext().getBundles()) {
                    LOG.log(Level.FINE, "Bundle {0}: {1}", new Object[]{bb.getBundleId(), bb.getSymbolicName()});
                }
                throw new IOException("Not found bundle:" + m.getCodeNameBase());
            }
            NetigsoLoader l = new NetigsoLoader(b, m, jar);
            HashSet<String> pkgs = new HashSet<String>();
            String[] knownPkgs = this.registered.get(m.getCodeNameBase());
            Object exported = b.getHeaders("").get("Export-Package");
            if (knownPkgs == EMPTY) {
                try {
                    SELF_QUERY.set(true);
                    if (this.findCoveredPkgs(exported)) {
                        Enumeration en = b.findEntries("", null, true);
                        if (en == null) {
                            LOG.log(Level.INFO, "Bundle {0}: {1} is empty", new Object[]{b.getBundleId(), b.getSymbolicName()});
                        } else {
                            while (en.hasMoreElements()) {
                                URL url = (URL)en.nextElement();
                                if (url.getFile().startsWith("/META-INF")) {
                                    pkgs.add(url.getFile().substring(9));
                                    continue;
                                }
                                pkgs.add(url.getFile().substring(1).replaceFirst("/[^/]*$", "").replace('/', '.'));
                            }
                        }
                    }
                    if (exported instanceof String) {
                        for (String p : exported.toString().split(",")) {
                            int semic = p.indexOf(59);
                            if (semic >= 0) {
                                p = p.substring(0, semic);
                            }
                            pkgs.add(p);
                        }
                    }
                }
                finally {
                    SELF_QUERY.set(false);
                }
                this.registered.put(m.getCodeNameBase(), pkgs.toArray(new String[0]));
                Stamps.getModulesJARs().scheduleSave((Stamps.Updater)this, "netigso-bundles", false);
            } else {
                pkgs.addAll(Arrays.asList(knownPkgs));
            }
            pcl.append(new ClassLoader[]{l});
            try {
                String msl = Bundle.MODULE_START_LEVEL();
                boolean start = true;
                if (!msl.isEmpty()) {
                    int level;
                    int moduleStartLevel = Integer.parseInt(msl);
                    start = moduleStartLevel >= (level = this.getBundleStartLevel(b, this.framework.getBundleContext()));
                }
                LOG.log(Level.FINE, "Starting bundle {0}: {1}", new Object[]{m.getCodeNameBase(), start});
                if (start) {
                    b.start();
                    if (this.findCoveredPkgs(exported) && !Netigso.isResolved(b) && Netigso.isRealBundle(b)) {
                        throw new IOException("Cannot start " + m.getCodeName() + " state remains INSTALLED after start()");
                    }
                }
            }
            catch (BundleException possible) {
                if (Netigso.isRealBundle(b)) {
                    throw possible;
                }
                LOG.log(Level.FINE, "Not starting fragment {0}", m.getCodeNameBase());
            }
            return pkgs;
        }
        catch (BundleException ex) {
            throw new IOException("Cannot start " + jar, (Throwable)ex);
        }
    }

    private static boolean isResolved(org.osgi.framework.Bundle b) {
        if (b.getState() == 2) {
            b.findEntries("META-INF", "MANIFEST.MF", false);
        }
        return b.getState() != 2;
    }

    private static boolean isRealBundle(org.osgi.framework.Bundle b) {
        return b.getHeaders("").get("Fragment-Host") == null;
    }

    protected void stopLoader(ModuleInfo m, ClassLoader loader) {
        NetigsoLoader nl = (NetigsoLoader)((Object)loader);
        org.osgi.framework.Bundle b = nl.bundle;
        try {
            assert (b != null);
            try {
                LOG.log(Level.FINE, "Stopping bundle {0}", m.getCodeNameBase());
                b.stop();
            }
            catch (BundleException possible) {
                if (Netigso.isRealBundle(b)) {
                    throw possible;
                }
                LOG.log(Level.FINE, "Not stopping fragment {0}", m.getCodeNameBase());
            }
        }
        catch (BundleException ex) {
            throw new IllegalStateException((Throwable)ex);
        }
    }

    protected Enumeration<URL> findResources(Module m, String resName) {
        org.osgi.framework.Bundle b = this.findBundle(m.getCodeNameBase());
        URL u = b.getEntry(resName);
        return u == null ? Enumerations.empty() : Enumerations.singleton((Object)u);
    }

    protected void reload(Module m) throws IOException {
        try {
            org.osgi.framework.Bundle b = this.findBundle(m.getCodeNameBase());
            b.stop();
            this.fakeOneModule(m, b);
        }
        catch (BundleException ex) {
            throw new IOException((Throwable)ex);
        }
    }

    final void notifyBundleChange(final String symbolicName, final Version version, final int action) {
        final Exception stack = LOG.isLoggable(Level.FINER) ? new Exception("StackTrace") : null;
        Runnable doLog = new Runnable(){

            @Override
            public void run() {
                if (Netigso.this.isEnabled(symbolicName)) {
                    return;
                }
                Mutex mutex = Main.getModuleSystem().getManager().mutex();
                if (!mutex.isReadAccess()) {
                    mutex.postReadRequest((Runnable)this);
                    return;
                }
                String type = "" + action;
                Level notify = Level.INFO;
                switch (action) {
                    case 1: {
                        return;
                    }
                    case 32: {
                        type = "resolved";
                        break;
                    }
                    case 2: {
                        type = "started";
                        break;
                    }
                    case 4: {
                        type = "stopped";
                        break;
                    }
                    case 16: {
                        return;
                    }
                    case 512: {
                        type = "lazy";
                        notify = Level.FINEST;
                        break;
                    }
                    case 128: {
                        type = "starting";
                        notify = Level.FINEST;
                    }
                }
                Netigso.LOG.log(notify, "bundle {0}@{2} {1}", new Object[]{symbolicName, type, version});
                if (stack != null) {
                    Netigso.LOG.log(Level.FINER, null, stack);
                }
            }
        };
        RP.post(doLog);
    }

    private File getNetigsoCache() throws IllegalStateException {
        return Places.getCacheSubdirectory((String)"netigso");
    }

    private void deleteRec(File dir) {
        File[] arr = dir.listFiles();
        if (arr != null) {
            for (File f : arr) {
                this.deleteRec(f);
            }
        }
        dir.delete();
    }

    private void fakeOneModule(Module m, org.osgi.framework.Bundle original) throws IOException {
        String cnb = m.getCodeNameBase();
        if (this.registered.get(cnb) != null && original == null) {
            return;
        }
        this.registered.put(cnb, EMPTY);
        try {
            String symbolicName = (String)m.getAttribute("Bundle-SymbolicName");
            if (!"org.netbeans.core.osgi".equals(symbolicName)) {
                if (symbolicName != null) {
                    if (original != null) {
                        LOG.log(Level.FINE, "Updating bundle {0}", original.getLocation());
                        FileInputStream is = new FileInputStream(m.getJarFile());
                        original.update((InputStream)is);
                        is.close();
                        org.osgi.framework.Bundle b = original;
                    } else {
                        BundleContext bc = this.framework.getBundleContext();
                        File jar = m.getJarFile();
                        String loc = m.isReloadable() ? Netigso.toURI(jar) : "reference:" + Netigso.toURI(jar);
                        LOG.log(Level.FINE, "Installing bundle {0}", loc);
                        org.osgi.framework.Bundle b = bc.installBundle(loc);
                        int startLevel = m.getStartLevel();
                        if (startLevel == -1) {
                            startLevel = this.defaultStartLevel();
                        }
                        if (startLevel > 0) {
                            this.setBundleStartLevel(bc, b, startLevel);
                        }
                    }
                } else {
                    InputStream is = Netigso.fakeBundle(m);
                    if (is != null) {
                        if (original != null) {
                            original.update(is);
                            org.osgi.framework.Bundle b = original;
                        } else {
                            assert (this.framework != null);
                            BundleContext bc = this.framework.getBundleContext();
                            assert (bc != null);
                            org.osgi.framework.Bundle b = bc.installBundle("netigso://" + cnb, is);
                        }
                        is.close();
                    }
                }
            }
            Stamps.getModulesJARs().scheduleSave((Stamps.Updater)this, "netigso-bundles", false);
        }
        catch (BundleException ex) {
            throw new IOException((Throwable)ex);
        }
    }

    private void setFrameworkStartLevel(BundleContext bc, int startLevel) {
        ServiceReference sr = bc.getServiceReference("org.osgi.service.startlevel.StartLevel");
        StartLevel level = null;
        if (sr != null && (level = (StartLevel)bc.getService(sr)) != null) {
            level.setStartLevel(startLevel);
            return;
        }
        LOG.log(Level.WARNING, "Cannot set framewok startLevel to {1} reference: {2} level {3}", new Object[]{null, startLevel, sr, level});
    }

    private void setBundleStartLevel(BundleContext bc, org.osgi.framework.Bundle b, int startLevel) {
        ServiceReference sr = bc.getServiceReference("org.osgi.service.startlevel.StartLevel");
        StartLevel level = null;
        if (sr != null && (level = (StartLevel)bc.getService(sr)) != null) {
            level.setBundleStartLevel(b, startLevel);
            return;
        }
        LOG.log(Level.WARNING, "Cannot set startLevel for {0} to {1} reference: {2} level {3}", new Object[]{b.getSymbolicName(), startLevel, sr, level});
    }

    private int getBundleStartLevel(org.osgi.framework.Bundle b, BundleContext bc) {
        ServiceReference sr = bc.getServiceReference("org.osgi.service.startlevel.StartLevel");
        StartLevel level = null;
        if (sr != null && (level = (StartLevel)bc.getService(sr)) != null) {
            return level.getBundleStartLevel(b);
        }
        return 0;
    }

    private static String threeDotsWithMajor(String version, String withMajor) {
        int indx = withMajor.indexOf(47);
        int major = 0;
        if (indx > 0) {
            major = Integer.parseInt(withMajor.substring(indx + 1));
        }
        String[] segments = (version + ".0.0.0").split("\\.");
        assert (segments.length >= 3 && segments[0].length() > 0);
        return "" + (Integer.parseInt(segments[0]) + major * 100) + "." + segments[1] + "." + segments[2];
    }

    private static InputStream fakeBundle(Module m) throws IOException {
        String netigsoExp = (String)m.getAttribute("Netigso-Export-Package");
        String exp = (String)m.getAttribute("OpenIDE-Module-Public-Packages");
        if (netigsoExp == null && ("-".equals(exp) || m.getAttribute("OpenIDE-Module-Friends") != null)) {
            return null;
        }
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        Manifest man = new Manifest();
        man.getMainAttributes().putValue("Manifest-Version", "1.0");
        man.getMainAttributes().putValue("Bundle-ManifestVersion", "2");
        man.getMainAttributes().putValue("Bundle-SymbolicName", m.getCodeNameBase());
        if (m.getSpecificationVersion() != null) {
            String spec = Netigso.threeDotsWithMajor(m.getSpecificationVersion().toString(), m.getCodeName());
            man.getMainAttributes().putValue("Bundle-Version", spec.toString());
        }
        if (netigsoExp != null) {
            man.getMainAttributes().putValue("Export-Package", netigsoExp);
        } else if (exp != null) {
            man.getMainAttributes().putValue("Export-Package", Netigso.substitutePkg(m));
        } else {
            man.getMainAttributes().putValue("Export-Package", m.getCodeNameBase());
        }
        JarOutputStream jos = new JarOutputStream(os, man);
        jos.close();
        return new ByteArrayInputStream(os.toByteArray());
    }

    private void readBundles() {
        assert (this.registered.isEmpty());
        try {
            InputStream is = Stamps.getModulesJARs().asStream("netigso-bundles");
            if (is == null) {
                File f;
                try {
                    f = this.getNetigsoCache();
                }
                catch (IllegalStateException ex) {
                    return;
                }
                this.deleteRec(f);
                return;
            }
            Properties p = new Properties();
            p.load(is);
            is.close();
            for (Map.Entry entry : p.entrySet()) {
                String k = (String)entry.getKey();
                String v = (String)entry.getValue();
                this.registered.put(k, v.split(","));
                LOG.log(Level.FINE, "readBundle: {0}", k);
            }
        }
        catch (IOException ex) {
            LOG.log(Level.WARNING, "Cannot read cache", ex);
        }
    }

    public void flushCaches(DataOutputStream os) throws IOException {
        Properties p = new Properties();
        for (Map.Entry<String, String[]> entry : this.registered.entrySet()) {
            StringBuilder sb = new StringBuilder();
            String sep = "";
            for (String s : entry.getValue()) {
                sb.append(sep);
                sb.append(s);
                sep = ",";
            }
            p.setProperty(entry.getKey(), sb.toString());
        }
        p.store(os, null);
    }

    public void cacheReady() {
    }

    private org.osgi.framework.Bundle findBundle(String codeNameBase) {
        for (org.osgi.framework.Bundle bb : this.framework.getBundleContext().getBundles()) {
            String bbName = bb.getSymbolicName().replace('-', '_');
            if (!bbName.equals(codeNameBase)) continue;
            return bb;
        }
        return null;
    }

    public byte[] fromArchive(long bundleId, String resource, ArchiveResources ar) throws IOException {
        if (SELF_QUERY.get()) {
            return ar.resource(resource);
        }
        return this.fromArchive(ar, resource);
    }

    public boolean isArchiveActive() {
        return !SELF_QUERY.get();
    }

    private static String toURI(File file) {
        class VFile
        extends File {
            public VFile() {
                super(File.this.getPath());
            }

            @Override
            public boolean isDirectory() {
                return false;
            }

            @Override
            public File getAbsoluteFile() {
                return this;
            }
        }
        return Utilities.toURI((File)file.new VFile()).toString();
    }

    private boolean findCoveredPkgs(Object exportedPackages) {
        if (this.defaultCoveredPkgs == null) {
            this.defaultCoveredPkgs = Bundle.FIND_COVERED_PKGS();
        }
        if ("exportedIfPresent".equals(this.defaultCoveredPkgs)) {
            return exportedPackages == null;
        }
        return "findEntries".equals(this.defaultCoveredPkgs);
    }

    final ClassLoader findClassLoader(String cnb) {
        return this.createClassLoader(cnb);
    }

    private boolean isEnabled(String cnd) {
        Module m = this.findModule(cnd);
        return m != null && m.isEnabled();
    }

    private static String substitutePkg(Module m) {
        StringBuilder exported = new StringBuilder();
        String sep = "";
        Module.PackageExport[] pblk = m.getPublicPackages();
        if (pblk == null) {
            pblk = new Module.PackageExport[]{new Module.PackageExport("", true)};
        }
        for (Module.PackageExport packageExport : pblk) {
            Set<String> pkgs = packageExport.recursive ? Netigso.findRecursivePkgs(m, packageExport) : Collections.singleton(packageExport.pkg);
            Iterator<String> i$ = pkgs.iterator();
            while (i$.hasNext()) {
                String p = i$.next();
                if (p.endsWith("/")) {
                    p = p.substring(0, p.length() - 1);
                }
                exported.append(sep).append(p.replace('/', '.'));
                sep = ",";
            }
        }
        if (exported.length() == 0) {
            exported.append(m.getCodeNameBase());
        }
        return exported.toString();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static Set<String> findRecursivePkgs(Module m, Module.PackageExport packageExport) {
        HashSet<String> pkgs = new HashSet<String>();
        for (File f : m.getAllJars()) {
            JarFile jf = null;
            try {
                jf = new JarFile(f);
                Enumeration<JarEntry> en = jf.entries();
                while (en.hasMoreElements()) {
                    String pkg;
                    String entryName;
                    int lastSlash;
                    JarEntry e = en.nextElement();
                    if (e.isDirectory() || (lastSlash = (entryName = e.getName()).lastIndexOf(47)) == -1 || !(pkg = entryName.substring(0, lastSlash + 1)).startsWith(packageExport.pkg)) continue;
                    pkgs.add(pkg);
                }
                continue;
            }
            catch (IOException ex) {
                LOG.log(Level.INFO, "Can't process " + f, ex);
                continue;
            }
            finally {
                try {
                    jf.close();
                    continue;
                }
                catch (IOException ex) {
                    LOG.log(Level.INFO, "Can't close " + f, ex);
                }
                continue;
            }
        }
        return pkgs;
    }

    public final byte[] patchBC(ClassLoader l, String className, ProtectionDomain pd, byte[] arr) {
        return this.patchByteCode(l, className, pd, arr);
    }

}

