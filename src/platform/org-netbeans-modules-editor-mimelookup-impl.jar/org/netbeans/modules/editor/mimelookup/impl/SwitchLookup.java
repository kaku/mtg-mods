/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.spi.editor.mimelookup.InstanceProvider
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.modules.editor.mimelookup.impl;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.mimelookup.impl.FolderPathLookup;
import org.netbeans.modules.editor.mimelookup.impl.InstanceProviderLookup;
import org.netbeans.spi.editor.mimelookup.InstanceProvider;
import org.netbeans.spi.editor.mimelookup.MimeLocation;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.ProxyLookup;

public class SwitchLookup
extends Lookup {
    private static final Logger LOG = Logger.getLogger(SwitchLookup.class.getName());
    static final String ROOT_FOLDER = "Editors";
    private MimePath mimePath;
    private final String LOCK = new String("SwitchLookup.LOCK");
    private Map<Class<?>, UpdatableProxyLookup> classLookups = new HashMap();
    private Map<List<String>, Lookup> pathsLookups = new HashMap<List<String>, Lookup>();

    public SwitchLookup(MimePath mimePath) {
        this.mimePath = mimePath;
    }

    public <T> Lookup.Result<T> lookup(Lookup.Template<T> template) {
        return this.findLookup(template.getType()).lookup(template);
    }

    public <T> T lookup(Class<T> clazz) {
        return (T)this.findLookup(clazz).lookup(clazz);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Lookup findLookup(Class<?> clazz) {
        String string = this.LOCK;
        synchronized (string) {
            UpdatableProxyLookup lookup = this.classLookups.get(clazz);
            if (lookup == null) {
                Lookup innerLookup = this.createLookup(clazz);
                lookup = new UpdatableProxyLookup(innerLookup);
                this.classLookups.put(clazz, lookup);
            }
            return lookup;
        }
    }

    private Lookup createLookup(Class<?> forClass) {
        Lookup lookup;
        MimeLocation loc = forClass.getAnnotation(MimeLocation.class);
        if (loc == null) {
            loc = new MimeLocation(){

                public String subfolderName() {
                    return null;
                }

                public Class<? extends InstanceProvider> instanceProviderClass() {
                    return null;
                }

                public Class<? extends Annotation> annotationType() {
                    return MimeLocation.class;
                }
            };
        }
        List<String> paths = SwitchLookup.computePaths(this.mimePath, "Editors", loc.subfolderName());
        if (loc.instanceProviderClass() != null && loc.instanceProviderClass() != InstanceProvider.class) {
            try {
                lookup = this.getLookupForProvider(paths, (InstanceProvider)loc.instanceProviderClass().newInstance());
            }
            catch (InstantiationException ex) {
                Exceptions.printStackTrace((Throwable)ex);
                lookup = Lookup.EMPTY;
            }
            catch (IllegalAccessException ex) {
                Exceptions.printStackTrace((Throwable)ex);
                lookup = Lookup.EMPTY;
            }
        } else {
            lookup = this.getLookupForPaths(paths);
        }
        return lookup;
    }

    private Lookup getLookupForPaths(List<String> paths) {
        Object lookup = this.pathsLookups.get(paths);
        if (lookup == null) {
            lookup = new FolderPathLookup(paths.toArray(new String[paths.size()]));
            this.pathsLookups.put(paths, (Lookup)lookup);
        }
        return lookup;
    }

    private Lookup getLookupForProvider(List<String> paths, InstanceProvider instanceProvider) {
        return new InstanceProviderLookup(paths.toArray(new String[paths.size()]), instanceProvider);
    }

    private static List<String> computePaths(MimePath mimePath, String prefixPath, String suffixPath) {
        try {
            Method m = MimePath.class.getDeclaredMethod("getInheritedPaths", String.class, String.class);
            m.setAccessible(true);
            List paths = (List)m.invoke((Object)mimePath, prefixPath, suffixPath);
            return paths;
        }
        catch (Exception e) {
            LOG.log(Level.WARNING, "Can't call org.netbeans.api.editor.mimelookup.MimePath.getInheritedPaths method.", e);
            StringBuilder sb = new StringBuilder();
            if (prefixPath != null && prefixPath.length() > 0) {
                sb.append(prefixPath);
            }
            if (mimePath.size() > 0) {
                if (sb.length() > 0) {
                    sb.append('/');
                }
                sb.append(mimePath.getPath());
            }
            if (suffixPath != null && suffixPath.length() > 0) {
                if (sb.length() > 0) {
                    sb.append('/');
                }
                sb.append(suffixPath);
            }
            return Collections.singletonList(sb.toString());
        }
    }

    private static final class UpdatableProxyLookup
    extends ProxyLookup {
        public UpdatableProxyLookup() {
        }

        public /* varargs */ UpdatableProxyLookup(Lookup ... lookups) {
            super(lookups);
        }

        public /* varargs */ void setLookupsEx(Lookup ... lookups) {
            this.setLookups(lookups);
        }
    }

}

