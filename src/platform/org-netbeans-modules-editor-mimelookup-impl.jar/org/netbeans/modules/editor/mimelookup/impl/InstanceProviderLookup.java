/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.mimelookup.InstanceProvider
 *  org.openide.filesystems.FileObject
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 */
package org.netbeans.modules.editor.mimelookup.impl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.netbeans.modules.editor.mimelookup.impl.CompoundFolderChildren;
import org.netbeans.spi.editor.mimelookup.InstanceProvider;
import org.openide.filesystems.FileObject;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public final class InstanceProviderLookup
extends AbstractLookup {
    private InstanceContent content;
    private InstanceProvider<?> instanceProvider;
    private CompoundFolderChildren children;
    private PCL listener;

    public InstanceProviderLookup(String[] paths, InstanceProvider instanceProvider) {
        this(paths, instanceProvider, new InstanceContent());
    }

    private InstanceProviderLookup(String[] paths, InstanceProvider instanceProvider, InstanceContent content) {
        super((AbstractLookup.Content)content);
        this.listener = new PCL();
        this.content = content;
        this.instanceProvider = instanceProvider;
        this.children = new CompoundFolderChildren(paths, true);
        this.children.addPropertyChangeListener(this.listener);
    }

    protected void initialize() {
        this.rebuild();
    }

    private void rebuild() {
        List<FileObject> files = this.children.getChildren();
        Object instance = this.instanceProvider.createInstance(files);
        this.content.set(Collections.singleton(instance), null);
    }

    private class PCL
    implements PropertyChangeListener {
        private PCL() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            InstanceProviderLookup.this.rebuild();
        }
    }

}

