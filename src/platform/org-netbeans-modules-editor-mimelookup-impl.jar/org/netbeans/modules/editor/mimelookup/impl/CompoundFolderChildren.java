/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.MultiFileSystem
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.editor.mimelookup.impl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.MultiFileSystem;
import org.openide.util.Exceptions;

public final class CompoundFolderChildren
implements FileChangeListener {
    public static final String PROP_CHILDREN = "FolderChildren.PROP_CHILDREN";
    private static final String HIDDEN_ATTR_NAME = "hidden";
    private static final Logger LOG = Logger.getLogger(CompoundFolderChildren.class.getName());
    private final String LOCK = new String("CompoundFolderChildren.LOCK");
    private final List<String> prefixes;
    private final boolean includeSubfolders;
    private List<FileObject> children;
    private FileObject mergedLayers;
    private final FileChangeListener weakFCL;
    private PropertyChangeSupport pcs;
    private final Runnable rebuildRunnable;
    private final Runnable propChangeRunnable;
    private long rebuildCnt;

    public CompoundFolderChildren(String[] paths) {
        this(paths, true);
    }

    public CompoundFolderChildren(String[] paths, boolean includeSubfolders) {
        this.weakFCL = FileUtil.weakFileChangeListener((FileChangeListener)this, (Object)null);
        this.pcs = new PropertyChangeSupport(this);
        this.rebuildRunnable = new Runnable(){

            @Override
            public void run() {
                CompoundFolderChildren.this.rebuild();
            }
        };
        this.propChangeRunnable = new Runnable(){

            @Override
            public void run() {
                CompoundFolderChildren.this.pcs.firePropertyChange("FolderChildren.PROP_CHILDREN", null, null);
            }
        };
        this.rebuildCnt = 0;
        this.prefixes = new ArrayList<String>();
        for (String path : paths) {
            this.prefixes.add(path.endsWith("/") ? path : path + "/");
        }
        this.includeSubfolders = includeSubfolders;
        this.rebuild();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public List<FileObject> getChildren() {
        String string = this.LOCK;
        synchronized (string) {
            return this.children;
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void rebuild() {
        PropertyChangeEvent event = null;
        String string = this.LOCK;
        synchronized (string) {
            ArrayList<FileObject> folders = new ArrayList<FileObject>(this.prefixes.size());
            ArrayList<> layers = new ArrayList<>(this.prefixes.size());
            for (final String prefix : this.prefixes) {
                FileObject parent;
                FileObject layer = FileUtil.getConfigFile((String)prefix);
                if (layer != null && layer.isFolder()) {
                    folders.add(layer);
                    try {
                        layers.add(()new MultiFileSystem(new FileSystem[]{layer.getFileSystem()}){

                            protected FileObject findResourceOn(FileSystem fs, String res) {
                                return fs.findResource(prefix + res);
                            }
                        });
                    }
                    catch (FileStateInvalidException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                    continue;
                }
                String parentPath = prefix;
                do {
                    assert (parentPath.length() > 0);
                } while ((parent = FileUtil.getConfigFile((String)(parentPath = parentPath.substring(0, Math.max(0, parentPath.lastIndexOf(47)))))) == null);
                parent.removeFileChangeListener(this.weakFCL);
                parent.addFileChangeListener(this.weakFCL);
            }
            this.mergedLayers = new MultiFileSystem(layers.toArray((T[])new FileSystem[layers.size()])).getRoot();
            this.mergedLayers.addFileChangeListener((FileChangeListener)this);
            ArrayList<FileObject> unsorted = new ArrayList<FileObject>();
            for (FileObject f : this.mergedLayers.getChildren()) {
                if (!this.includeSubfolders && !f.isData() || Boolean.TRUE.equals(f.getAttribute("hidden"))) continue;
                f.addFileChangeListener((FileChangeListener)this);
                unsorted.add(f);
            }
            ArrayList<FileObject> sorted = new ArrayList<FileObject>(unsorted.size());
            for (FileObject merged : FileUtil.getOrder(unsorted, (boolean)true)) {
                FileObject folder;
                String name = merged.getNameExt();
                FileObject original = null;
                Iterator i$ = folders.iterator();
                while (i$.hasNext() && (original = (folder = (FileObject)i$.next()).getFileObject(name)) == null) {
                }
                assert (original != null);
                sorted.add(original);
            }
            if (this.children != null && !sorted.equals(this.children)) {
                event = new PropertyChangeEvent(this, "FolderChildren.PROP_CHILDREN", this.children, sorted);
            }
            this.children = sorted;
            if (LOG.isLoggable(Level.FINE)) {
                ++this.rebuildCnt;
                LOG.log(Level.FINE, "{0} rebuilt {1} times", new Object[]{this, this.rebuildCnt});
            }
        }
        if (event != null) {
            this.pcs.firePropertyChange(event);
        }
    }

    public void fileFolderCreated(FileEvent fe) {
        fe.runWhenDeliveryOver(this.rebuildRunnable);
    }

    public void fileDataCreated(FileEvent fe) {
        fe.runWhenDeliveryOver(this.rebuildRunnable);
    }

    public void fileChanged(FileEvent fe) {
        fe.runWhenDeliveryOver(this.propChangeRunnable);
    }

    public void fileDeleted(FileEvent fe) {
        fe.runWhenDeliveryOver(this.rebuildRunnable);
    }

    public void fileRenamed(FileRenameEvent fe) {
        fe.runWhenDeliveryOver(this.rebuildRunnable);
    }

    public void fileAttributeChanged(FileAttributeEvent fe) {
        fe.runWhenDeliveryOver(this.rebuildRunnable);
    }

}

