/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.InstanceCookie$Of
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.loaders.InstanceDataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.AbstractLookup$Pair
 *  org.openide.util.lookup.InstanceContent
 */
package org.netbeans.modules.editor.mimelookup.impl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.editor.mimelookup.impl.CompoundFolderChildren;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.loaders.InstanceDataObject;
import org.openide.nodes.Node;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public final class FolderPathLookup
extends AbstractLookup {
    private static final Logger LOG = Logger.getLogger(FolderPathLookup.class.getName());
    private InstanceContent content;
    private CompoundFolderChildren children;
    private PCL listener;

    public FolderPathLookup(String[] paths) {
        this(paths, new InstanceContent());
    }

    private FolderPathLookup(String[] paths, InstanceContent content) {
        super((AbstractLookup.Content)content);
        this.listener = new PCL();
        this.content = content;
        this.children = new CompoundFolderChildren(paths, false);
        this.children.addPropertyChangeListener(this.listener);
        this.rebuild();
    }

    private void rebuild() {
        ArrayList<ICItem> instanceFiles = new ArrayList<ICItem>();
        for (FileObject file : this.children.getChildren()) {
            if (!file.isValid()) continue;
            try {
                DataObject d = DataObject.find((FileObject)file);
                InstanceCookie instanceCookie = (InstanceCookie)d.getCookie(InstanceCookie.class);
                if (instanceCookie == null) continue;
                instanceFiles.add(new ICItem(d, instanceCookie));
            }
            catch (Exception e) {
                LOG.log(Level.WARNING, "Can't create DataObject", e);
            }
        }
        this.content.setPairs(instanceFiles);
    }

    private static final class ICItem
    extends AbstractLookup.Pair {
        static final long serialVersionUID = 10;
        static final ThreadLocal<ICItem> DANGEROUS = new ThreadLocal();
        private static final Logger ERR = Logger.getLogger(ICItem.class.getName());
        private FileObject fo;
        private transient InstanceCookie ic;
        private transient DataObject dataObject;
        private transient Reference<Object> ref;

        public ICItem(DataObject obj, InstanceCookie ic) {
            this.ic = ic;
            this.dataObject = obj;
            this.fo = obj.getPrimaryFile();
            if (ERR.isLoggable(Level.FINE)) {
                ERR.fine("New ICItem: " + (Object)obj);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void init() {
            if (this.ic != null) {
                return;
            }
            ICItem prev = DANGEROUS.get();
            try {
                DANGEROUS.set(this);
                if (this.dataObject == null) {
                    try {
                        this.dataObject = DataObject.find((FileObject)this.fo);
                    }
                    catch (DataObjectNotFoundException donfe) {
                        this.ic = new BrokenInstance("No DataObject for " + this.fo.getPath(), (Exception)donfe);
                        DANGEROUS.set(prev);
                        return;
                    }
                }
                this.ic = (InstanceCookie)this.dataObject.getCookie(InstanceCookie.class);
                if (this.ic == null) {
                    this.ic = new BrokenInstance("No cookie for " + this.fo.getPath(), null);
                }
            }
            finally {
                DANGEROUS.set(prev);
            }
        }

        protected boolean instanceOf(Class clazz) {
            this.init();
            if (ERR.isLoggable(Level.FINE)) {
                ERR.fine("instanceOf: " + clazz.getName() + " obj: " + (Object)this.dataObject);
            }
            if (this.ic instanceof InstanceCookie.Of) {
                InstanceCookie.Of of = (InstanceCookie.Of)this.ic;
                boolean res = of.instanceOf(clazz);
                if (ERR.isLoggable(Level.FINE)) {
                    ERR.fine("  of: " + res);
                }
                return res;
            }
            try {
                boolean res = clazz.isAssignableFrom(this.ic.instanceClass());
                if (ERR.isLoggable(Level.FINE)) {
                    ERR.fine("  plain: " + res);
                }
                return res;
            }
            catch (ClassNotFoundException ex) {
                ICItem.exception(ex, this.fo);
            }
            catch (IOException ex) {
                ICItem.exception(ex, this.fo);
            }
            return false;
        }

        public Object getInstance() {
            this.init();
            try {
                Object obj = this.ic.instanceCreate();
                if (ERR.isLoggable(Level.FINE)) {
                    ERR.fine("  getInstance: " + obj + " for " + (Object)this.dataObject);
                }
                this.ref = new WeakReference<Object>(obj);
                return obj;
            }
            catch (ClassNotFoundException ex) {
                ICItem.exception(ex, this.fo);
            }
            catch (IOException ex) {
                ICItem.exception(ex, this.fo);
            }
            return null;
        }

        public int hashCode() {
            this.init();
            return System.identityHashCode((Object)this.ic);
        }

        public boolean equals(Object obj) {
            if (obj instanceof ICItem) {
                ICItem i = (ICItem)((Object)obj);
                i.init();
                this.init();
                return this.ic == i.ic;
            }
            return false;
        }

        public String getId() {
            this.init();
            if (this.dataObject == null) {
                return "<broken: " + this.fo.getPath() + ">";
            }
            return this.dataObject.getName();
        }

        public String getDisplayName() {
            this.init();
            if (this.dataObject == null) {
                return "<broken: " + this.fo.getPath() + ">";
            }
            return this.dataObject.getNodeDelegate().getDisplayName();
        }

        protected boolean creatorOf(Object obj) {
            Reference<Object> w = this.ref;
            if (w != null && w.get() == obj) {
                return true;
            }
            if (this.dataObject instanceof InstanceDataObject) {
                try {
                    Method m = InstanceDataObject.class.getDeclaredMethod("creatorOf", Object.class);
                    return (Boolean)m.invoke((Object)this.dataObject, obj);
                }
                catch (Exception e) {
                    // empty catch block
                }
            }
            return false;
        }

        public Class getType() {
            this.init();
            try {
                return this.ic.instanceClass();
            }
            catch (IOException ex) {
            }
            catch (ClassNotFoundException ex) {
                // empty catch block
            }
            return Object.class;
        }

        private static void exception(Exception e, FileObject fo) {
            LOG.log(Level.INFO, "Bad file: " + (Object)fo, e);
        }

        private static final class BrokenInstance
        implements InstanceCookie.Of {
            private final String message;
            private final Exception ex;

            public BrokenInstance(String message, Exception ex) {
                this.message = message;
                this.ex = ex;
            }

            public String instanceName() {
                return "java.lang.Object";
            }

            private ClassNotFoundException die() {
                if (this.ex != null) {
                    return new ClassNotFoundException(this.message, this.ex);
                }
                return new ClassNotFoundException(this.message);
            }

            public Class instanceClass() throws IOException, ClassNotFoundException {
                throw this.die();
            }

            public Object instanceCreate() throws IOException, ClassNotFoundException {
                throw this.die();
            }

            public boolean instanceOf(Class type) {
                return false;
            }
        }

    }

    private class PCL
    implements PropertyChangeListener {
        private PCL() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            FolderPathLookup.this.rebuild();
        }
    }

}

