/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.spi.editor.mimelookup.MimeDataProvider
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.editor.mimelookup.impl;

import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.mimelookup.impl.SwitchLookup;
import org.netbeans.spi.editor.mimelookup.MimeDataProvider;
import org.openide.util.Lookup;

public class DefaultMimeDataProvider
implements MimeDataProvider {
    public Lookup getLookup(MimePath mimePath) {
        return new SwitchLookup(mimePath);
    }
}

