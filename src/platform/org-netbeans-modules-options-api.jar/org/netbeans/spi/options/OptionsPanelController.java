/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package org.netbeans.spi.options;

import java.beans.PropertyChangeListener;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;
import javax.swing.JComponent;
import org.netbeans.modules.options.OptionsPanelControllerAccessor;
import org.netbeans.modules.options.advanced.AdvancedPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public abstract class OptionsPanelController {
    public static final String PROP_VALID = "valid";
    public static final String PROP_CHANGED = "changed";
    public static final String PROP_HELP_CTX = "helpCtx";

    @Deprecated
    public static final OptionsPanelController createAdvanced(String subpath) {
        return new AdvancedPanelController(subpath);
    }

    public abstract void update();

    public abstract void applyChanges();

    public abstract void cancel();

    public abstract boolean isValid();

    public abstract boolean isChanged();

    public Lookup getLookup() {
        return Lookup.EMPTY;
    }

    public void handleSuccessfulSearch(String searchText, List<String> matchedKeywords) {
    }

    public abstract JComponent getComponent(Lookup var1);

    protected void setCurrentSubcategory(String subpath) {
    }

    public final void setSubcategory(String subpath) {
        this.setCurrentSubcategory(subpath);
    }

    public abstract HelpCtx getHelpCtx();

    public abstract void addPropertyChangeListener(PropertyChangeListener var1);

    public abstract void removePropertyChangeListener(PropertyChangeListener var1);

    static {
        OptionsPanelControllerAccessor.DEFAULT = new OptionsPanelControllerAccessor(){

            @Override
            public void setCurrentSubcategory(OptionsPanelController controller, String subpath) {
                controller.setCurrentSubcategory(subpath);
            }
        };
    }

    @Target(value={ElementType.TYPE})
    @Retention(value=RetentionPolicy.SOURCE)
    public static @interface Keywords {
        public String[] keywords();

        public String location();

        public String tabTitle() default "";
    }

    @Target(value={ElementType.TYPE})
    @Retention(value=RetentionPolicy.SOURCE)
    public static @interface KeywordsRegistration {
        public Keywords[] value();
    }

    @Target(value={ElementType.PACKAGE})
    @Retention(value=RetentionPolicy.SOURCE)
    public static @interface ContainerRegistration {
        public String id();

        public String categoryName();

        public String iconBase();

        public String keywords() default "";

        public String keywordsCategory() default "";

        public int position() default Integer.MAX_VALUE;
    }

    @Target(value={ElementType.TYPE, ElementType.METHOD})
    @Retention(value=RetentionPolicy.SOURCE)
    public static @interface SubRegistration {
        public String id() default "";

        public String location() default "Advanced";

        public String displayName();

        public String keywords() default "";

        public String keywordsCategory() default "";

        public int position() default Integer.MAX_VALUE;
    }

    @Target(value={ElementType.TYPE, ElementType.METHOD})
    @Retention(value=RetentionPolicy.SOURCE)
    public static @interface TopLevelRegistration {
        public String id() default "";

        public String categoryName();

        public String iconBase();

        public String keywords() default "";

        public String keywordsCategory() default "";

        public int position() default Integer.MAX_VALUE;
    }

}

