/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.options;

import java.util.Map;
import java.util.concurrent.Callable;
import org.netbeans.modules.options.AdvancedOptionImpl;
import org.netbeans.spi.options.OptionsPanelController;

public abstract class AdvancedOption {
    private static final String DISPLAYNAME = "displayName";
    private static final String TOOLTIP = "toolTip";
    private static final String KEYWORDS = "keywords";
    private static final String CONTROLLER = "controller";
    private static final String KEYWORDS_CATEGORY = "keywordsCategory";

    @Deprecated
    protected AdvancedOption() {
    }

    public abstract String getDisplayName();

    public abstract String getTooltip();

    public abstract OptionsPanelController create();

    static AdvancedOption createSubCategory(final Map attrs) {
        final String displayName = (String)attrs.get("displayName");
        String tooltip = (String)attrs.get("toolTip");
        String keywords = (String)attrs.get("keywords");
        String keywordsCategory = (String)attrs.get("keywordsCategory");
        return new AdvancedOptionImpl(new Callable<OptionsPanelController>(){

            @Override
            public OptionsPanelController call() throws Exception {
                Object o = attrs.get("controller");
                if (o instanceof OptionsPanelController) {
                    return (OptionsPanelController)o;
                }
                throw new Exception("got no controller from " + displayName + ": " + o);
            }
        }, displayName, tooltip, keywords, keywordsCategory);
    }

}

