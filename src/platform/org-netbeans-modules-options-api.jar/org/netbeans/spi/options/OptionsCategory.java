/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 */
package org.netbeans.spi.options;

import java.util.Map;
import java.util.concurrent.Callable;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.netbeans.modules.options.OptionsCategoryImpl;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.ImageUtilities;

public abstract class OptionsCategory {
    private static final String TITLE = "title";
    private static final String CATEGORY_NAME = "categoryName";
    private static final String ICON = "iconBase";
    private static final String CONTROLLER = "controller";
    private static final String KEYWORDS = "keywords";
    private static final String KEYWORDS_CATEGORY = "keywordsCategory";
    private static final String ADVANCED_OPTIONS_FOLDER = "advancedOptionsFolder";

    public String getIconBase() {
        return null;
    }

    public Icon getIcon() {
        ImageIcon res = ImageUtilities.loadImageIcon((String)(this.getIconBase() + ".png"), (boolean)true);
        if (res == null) {
            res = ImageUtilities.loadImageIcon((String)(this.getIconBase() + ".gif"), (boolean)true);
        }
        return res;
    }

    public abstract String getCategoryName();

    public abstract String getTitle();

    public abstract OptionsPanelController create();

    static OptionsCategory createCategory(final Map attrs) {
        final String title = (String)attrs.get("title");
        String categoryName = (String)attrs.get("categoryName");
        String iconBase = (String)attrs.get("iconBase");
        String keywords = (String)attrs.get("keywords");
        String keywordsCategory = (String)attrs.get("keywordsCategory");
        String advancedOptionsCategory = (String)attrs.get("advancedOptionsFolder");
        return new OptionsCategoryImpl(title, categoryName, iconBase, new Callable<OptionsPanelController>(){

            @Override
            public OptionsPanelController call() throws Exception {
                Object o = attrs.get("controller");
                if (o instanceof OptionsPanelController) {
                    return (OptionsPanelController)o;
                }
                throw new Exception("got no controller from " + title + ": " + o);
            }
        }, keywords, keywordsCategory, advancedOptionsCategory);
    }

}

