/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressUtils
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 *  org.openide.util.NbBundle
 *  org.openide.windows.WindowManager
 */
package org.netbeans.api.options;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Frame;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;
import javax.swing.JFrame;
import org.netbeans.api.options.Bundle;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.modules.options.CategoryModel;
import org.netbeans.modules.options.OptionsDisplayerImpl;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

public final class OptionsDisplayer {
    private static final OptionsDisplayer INSTANCE = new OptionsDisplayer();
    private final OptionsDisplayerImpl impl = new OptionsDisplayerImpl(false);
    private static Logger log = Logger.getLogger(OptionsDisplayer.class.getName());
    public static final String ADVANCED = "Advanced";
    public static final String KEYMAPS = "Keymaps";
    public static final String FONTSANDCOLORS = "FontsAndColors";
    public static final String EDITOR = "Editor";
    public static final String GENERAL = "General";
    private String currentCategoryID = null;
    private AtomicBoolean operationCancelled;
    private CategoryModel categoryModel;

    private OptionsDisplayer() {
    }

    public static OptionsDisplayer getDefault() {
        return INSTANCE;
    }

    public boolean open() {
        OptionsDisplayer.showWaitCursor();
        if (this.categoryModel == null || this.operationCancelled == null || this.operationCancelled.get()) {
            if (this.operationCancelled == null) {
                this.operationCancelled = new AtomicBoolean();
            }
            if (this.operationCancelled.get()) {
                this.currentCategoryID = null;
                this.operationCancelled.set(false);
            }
            ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

                @Override
                public void run() {
                    OptionsDisplayer.this.categoryModel = CategoryModel.getInstance();
                    OptionsDisplayer.this.currentCategoryID = OptionsDisplayer.this.categoryModel.getCurrentCategoryID();
                }
            }, (String)Bundle.CTL_Loading_Options_Waiting(), (AtomicBoolean)this.operationCancelled, (boolean)false, (int)0, (int)3000);
            return this.open(this.currentCategoryID);
        }
        return this.open(this.categoryModel.getCurrentCategoryID());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean open(String path) {
        log.fine("Open Options Dialog: " + path);
        OptionsDisplayer.showWaitCursor();
        try {
            if (path != null && (this.categoryModel == null || this.operationCancelled == null || this.operationCancelled.get())) {
                if (this.operationCancelled == null) {
                    this.operationCancelled = new AtomicBoolean();
                }
                if (this.operationCancelled.get()) {
                    this.operationCancelled.set(false);
                }
                ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

                    @Override
                    public void run() {
                        OptionsDisplayer.this.categoryModel = CategoryModel.getInstance();
                        OptionsDisplayer.this.categoryModel.getCategoryIDs();
                    }
                }, (String)Bundle.CTL_Loading_Options_Waiting(), (AtomicBoolean)this.operationCancelled, (boolean)false, (int)0, (int)3000);
                if (this.operationCancelled.get()) {
                    boolean bl = true;
                    return bl;
                }
            }
            boolean bl = this.openImpl(path);
            return bl;
        }
        finally {
            OptionsDisplayer.hideWaitCursor();
        }
    }

    public boolean open(boolean isModal) {
        this.impl.setIsModal(isModal);
        return this.open();
    }

    public boolean open(String path, boolean isModal) {
        this.impl.setIsModal(isModal);
        return this.open(path);
    }

    private boolean openImpl(String path) {
        if (path == null) {
            log.warning("Category to open is null.");
            return false;
        }
        final String categoryId = path.indexOf(47) == -1 ? path : path.substring(0, path.indexOf(47));
        final String subpath = path.indexOf(47) == -1 ? null : path.substring(path.indexOf(47) + 1);
        Boolean retval = (Boolean)Mutex.EVENT.readAccess((Mutex.Action)new Mutex.Action<Boolean>(){

            public Boolean run() {
                boolean retvalForRun;
                Boolean r = OptionsDisplayer.this.impl.isOpen();
                boolean bl = retvalForRun = r == false;
                if (retvalForRun) {
                    retvalForRun = Arrays.asList(OptionsDisplayer.this.categoryModel.getCategoryIDs()).contains(categoryId);
                    if (!retvalForRun) {
                        log.warning("Unknown categoryId: " + categoryId);
                    }
                } else {
                    log.warning("Options Dialog is opened");
                }
                if (retvalForRun) {
                    log.fine("openImpl:impl.showOptionsDialog(" + categoryId + ", " + subpath + ")");
                    OptionsDisplayer.this.impl.showOptionsDialog(categoryId, subpath, OptionsDisplayer.this.categoryModel);
                }
                log.fine("openImpl return " + Boolean.valueOf(retvalForRun));
                OptionsDisplayer.this.categoryModel = null;
                OptionsDisplayer.this.operationCancelled = null;
                return retvalForRun;
            }
        });
        return retval;
    }

    private static void showWaitCursor() {
        Mutex.EVENT.readAccess(new Runnable(){

            @Override
            public void run() {
                JFrame mainWindow = (JFrame)WindowManager.getDefault().getMainWindow();
                mainWindow.getGlassPane().setCursor(Cursor.getPredefinedCursor(3));
                mainWindow.getGlassPane().setVisible(true);
                StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(OptionsDisplayerImpl.class, (String)"CTL_Loading_Options"));
            }
        });
    }

    private static void hideWaitCursor() {
        Mutex.EVENT.readAccess(new Runnable(){

            @Override
            public void run() {
                StatusDisplayer.getDefault().setStatusText("");
                JFrame mainWindow = (JFrame)WindowManager.getDefault().getMainWindow();
                mainWindow.getGlassPane().setVisible(false);
                mainWindow.getGlassPane().setCursor(null);
            }
        });
    }

}

