/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.options.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Shape;
import java.awt.geom.Line2D;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.border.Border;

public class VariableBorder
implements Border {
    private Color topColor;
    private Color leftColor;
    private Color bottomColor;
    private Color rightColor;

    public VariableBorder(Color topColor, Color leftColor, Color bottomColor, Color rightColor) {
        this.topColor = topColor;
        this.leftColor = leftColor;
        this.bottomColor = bottomColor;
        this.rightColor = rightColor;
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        Line2D.Double s;
        Graphics2D g2d = (Graphics2D)g;
        if (this.topColor != null) {
            s = new Line2D.Double(x, y, x + width, y);
            g2d.setColor(this.topColor);
            g2d.fill(s);
        }
        if (this.leftColor != null) {
            s = new Line2D.Double(x, y, x, y + height);
            g2d.setColor(this.leftColor);
            g2d.fill(s);
        }
        if (this.bottomColor != null) {
            s = new Line2D.Double(x, y + height - 1, x + width, y + height - 1);
            g2d.setColor(this.bottomColor);
            g2d.fill(s);
        }
        if (this.rightColor != null) {
            s = new Line2D.Double(x + width - 1, y, x + width - 1, y + height);
            g2d.setColor(this.rightColor);
            g2d.fill(s);
        }
    }

    @Override
    public Insets getBorderInsets(Component c) {
        Insets i = new Insets(0, 0, 0, 0);
        if (this.topColor != null) {
            i.top = 1;
        }
        if (this.leftColor != null) {
            i.left = 1;
        }
        if (this.bottomColor != null) {
            i.bottom = 1;
        }
        if (this.rightColor != null) {
            i.right = 1;
        }
        if (c instanceof JToolBar) {
            Insets toolBarInsets = ((JToolBar)c).getMargin();
            i.top += toolBarInsets.top;
            i.left += toolBarInsets.left;
            i.right += toolBarInsets.right;
            i.bottom += toolBarInsets.bottom;
        }
        if (c instanceof JToggleButton) {
            Insets buttonInsets = ((JToggleButton)c).getMargin();
            i.top += buttonInsets.top;
            i.left += buttonInsets.left;
            i.right += buttonInsets.right;
            i.bottom += buttonInsets.bottom;
        }
        return i;
    }

    @Override
    public boolean isBorderOpaque() {
        return false;
    }
}

