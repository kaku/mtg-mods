/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.options.ui;

import java.util.List;
import javax.swing.JComponent;

public abstract class TabbedPanelModel {
    public abstract List<String> getCategories();

    public abstract String getToolTip(String var1);

    public abstract JComponent getPanel(String var1);
}

