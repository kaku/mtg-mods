/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.options.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.border.Border;

public class DashedBorder
implements Border {
    private Insets insets = new Insets(1, 1, 1, 1);

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        Color old = g.getColor();
        g.setColor(Color.black);
        for (int vx = x; vx < x + width; vx += 2) {
            g.fillRect(vx, y, 1, 1);
            g.fillRect(vx, y + height - 1, 1, 1);
        }
        for (int vy = y; vy < y + height; vy += 2) {
            g.fillRect(x, vy, 1, 1);
            g.fillRect(x + width - 1, vy, 1, 1);
        }
        g.setColor(old);
    }

    @Override
    public Insets getBorderInsets(Component c) {
        return this.insets;
    }

    @Override
    public boolean isBorderOpaque() {
        return true;
    }
}

