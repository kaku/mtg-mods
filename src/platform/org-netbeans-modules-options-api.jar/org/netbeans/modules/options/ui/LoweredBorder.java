/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.options.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.border.AbstractBorder;

public class LoweredBorder
extends AbstractBorder {
    private Color darker = LoweredBorder.getLabelBackgroundColor().darker().darker();
    private Color brighter = LoweredBorder.getLabelBackgroundColor().brighter().brighter();

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
        Color oldColor = g.getColor();
        g.translate(x, y);
        g.setColor(this.darker);
        g.drawLine(0, 0, 0, h - 1);
        g.drawLine(1, 0, w - 1, 0);
        g.setColor(this.brighter);
        g.drawLine(1, h - 1, w - 1, h - 1);
        g.drawLine(w - 1, 1, w - 1, h - 2);
        g.translate(- x, - y);
        g.setColor(oldColor);
    }

    @Override
    public Insets getBorderInsets(Component c) {
        return new Insets(1, 1, 1, 1);
    }

    @Override
    public boolean isBorderOpaque() {
        return true;
    }

    private static Color getLabelBackgroundColor() {
        Color retval = new JLabel().getBackground();
        return retval != null ? retval : UIManager.getDefaults().getColor("Label.background");
    }
}

