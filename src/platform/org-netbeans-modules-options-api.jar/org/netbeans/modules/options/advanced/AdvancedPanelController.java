/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.options.advanced;

import java.beans.PropertyChangeListener;
import java.util.List;
import javax.swing.JComponent;
import org.netbeans.modules.options.advanced.AdvancedPanel;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public final class AdvancedPanelController
extends OptionsPanelController {
    private String subpath = null;
    private AdvancedPanel advancedPanel;

    public AdvancedPanelController(String subpath) {
        this.subpath = subpath;
    }

    @Override
    public void update() {
        this.getAdvancedPanel().update();
    }

    @Override
    public void applyChanges() {
        this.getAdvancedPanel().applyChanges();
    }

    @Override
    public void cancel() {
        this.getAdvancedPanel().cancel();
    }

    @Override
    public boolean isValid() {
        return this.getAdvancedPanel().dataValid();
    }

    @Override
    public boolean isChanged() {
        return this.getAdvancedPanel().isChanged();
    }

    @Override
    public Lookup getLookup() {
        return this.getAdvancedPanel().getLookup();
    }

    @Override
    public JComponent getComponent(Lookup masterLookup) {
        AdvancedPanel p = this.getAdvancedPanel();
        p.init();
        return p;
    }

    @Override
    public void setCurrentSubcategory(String subpath) {
        this.getAdvancedPanel().setCurrentSubcategory(subpath);
    }

    @Override
    public HelpCtx getHelpCtx() {
        return this.getAdvancedPanel().getHelpCtx();
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.getAdvancedPanel().addPropertyChangeListener(l);
        this.getAdvancedPanel().addModelPropertyChangeListener(l);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.getAdvancedPanel().removePropertyChangeListener(l);
        this.getAdvancedPanel().removeModelPropertyChangeListener(l);
    }

    @Override
    public void handleSuccessfulSearch(String searchText, List<String> matchedKeywords) {
        this.getAdvancedPanel().handleSearch(searchText, matchedKeywords);
    }

    private synchronized AdvancedPanel getAdvancedPanel() {
        if (this.advancedPanel == null) {
            this.advancedPanel = new AdvancedPanel(this.subpath);
        }
        return this.advancedPanel;
    }
}

