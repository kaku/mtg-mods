/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.advanced;

import javax.swing.Icon;
import org.netbeans.modules.options.advanced.AdvancedPanelController;
import org.netbeans.spi.options.OptionsCategory;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public final class Advanced
extends OptionsCategory {
    private OptionsPanelController controller;
    private static Icon icon;

    private static String loc(String key) {
        return NbBundle.getMessage(Advanced.class, (String)key);
    }

    @Override
    public Icon getIcon() {
        if (icon == null) {
            icon = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/options/resources/advanced.png", (boolean)false);
        }
        return icon;
    }

    @Override
    public String getCategoryName() {
        return Advanced.loc("CTL_Advanced_Options");
    }

    @Override
    public String getTitle() {
        return Advanced.loc("CTL_Advanced_Options_Title");
    }

    public String getDescription() {
        return Advanced.loc("CTL_Advanced_Options_Description");
    }

    @Override
    public OptionsPanelController create() {
        if (this.controller == null) {
            this.controller = new AdvancedPanelController("Advanced");
        }
        return this.controller;
    }
}

