/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.advanced;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.options.OptionsPanelControllerAccessor;
import org.netbeans.modules.options.advanced.Model;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;

public final class AdvancedPanel
extends JPanel {
    JTabbedPane tabbedPanel;
    private static final Logger LOGGER = Logger.getLogger(AdvancedPanel.class.getName());
    private LookupListener listener;
    private Model model;
    private String subpath;
    private ChangeListener changeListener;

    AdvancedPanel(String subpath) {
        this.listener = new LookupListenerImpl();
        this.changeListener = new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                AdvancedPanel.this.handleTabSwitched(null, null);
            }
        };
        this.subpath = subpath;
        this.model = new Model(subpath, this.listener);
    }

    public void update() {
        int idx = this.tabbedPanel.getSelectedIndex();
        if (idx != -1) {
            String category = this.tabbedPanel.getTitleAt(idx);
            this.model.update(category);
        }
    }

    public void applyChanges() {
        this.model.applyChanges();
    }

    public void cancel() {
        this.model.cancel();
    }

    public HelpCtx getHelpCtx() {
        return this.model.getHelpCtx(this.tabbedPanel != null ? (JComponent)this.tabbedPanel.getSelectedComponent() : null);
    }

    public boolean dataValid() {
        return this.model.isValid();
    }

    public boolean isChanged() {
        return this.model.isChanged();
    }

    public void addModelPropertyChangeListener(PropertyChangeListener listener) {
        this.model.addPropertyChangeListener(listener);
    }

    public void removeModelPropertyChangeListener(PropertyChangeListener listener) {
        this.model.removePropertyChangeListener(listener);
    }

    public Lookup getLookup() {
        return this.model.getLookup();
    }

    void init() {
        this.tabbedPanel = new JTabbedPane();
        this.tabbedPanel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(AdvancedPanel.class, (String)"AdvancedPanel.tabbedPanel.AD"));
        this.setLayout(new BorderLayout());
        this.removeAll();
        this.add((Component)this.tabbedPanel, "Center");
        this.initTabbedPane();
    }

    private void initTabbedPane() {
        this.tabbedPanel.removeChangeListener(this.changeListener);
        this.tabbedPanel.removeAll();
        List<String> categories = this.model.getCategories();
        this.tabbedPanel.setVisible(categories.size() > 0);
        for (String category : categories) {
            this.tabbedPanel.addTab(category, new JLabel(category));
        }
        this.tabbedPanel.addChangeListener(this.changeListener);
        this.handleTabSwitched(null, null);
    }

    public void setCurrentSubcategory(String path) {
        String currentDisplayName;
        String subcategoryID = path.indexOf(47) == -1 ? path : path.substring(0, path.indexOf(47));
        String subcategorySubpath = path.indexOf(47) == -1 ? null : path.substring(path.indexOf(47) + 1);
        LOGGER.fine("Set current subcategory: " + path);
        if (!this.model.getIDs().contains(subcategoryID)) {
            LOGGER.warning("Subcategory " + subcategoryID + " not found.");
            return;
        }
        String newDisplayName = this.model.getDisplayName(subcategoryID);
        if (!newDisplayName.equals(currentDisplayName = this.getSelectedDisplayName())) {
            for (int i = 0; i < this.tabbedPanel.getComponentCount(); ++i) {
                if (!this.tabbedPanel.getTitleAt(i).equals(newDisplayName)) continue;
                this.tabbedPanel.setSelectedIndex(i);
                break;
            }
        }
        if (subcategorySubpath != null) {
            OptionsPanelControllerAccessor.getDefault().setCurrentSubcategory(this.model.getController(subcategoryID), subcategorySubpath);
        }
    }

    private String getSelectedDisplayName() {
        String categoryDisplayName = null;
        int selectedIndex = this.tabbedPanel.getSelectedIndex();
        if (selectedIndex != -1) {
            categoryDisplayName = this.tabbedPanel.getTitleAt(selectedIndex);
        }
        return categoryDisplayName;
    }

    private void handleTabSwitched(String searchText, List<String> matchedKeywords) {
        int selectedIndex;
        int n = selectedIndex = this.tabbedPanel.getSelectedIndex() >= 0 ? this.tabbedPanel.getSelectedIndex() : -1;
        if (selectedIndex != -1) {
            String category = this.tabbedPanel.getTitleAt(selectedIndex);
            if (this.tabbedPanel.getSelectedComponent() instanceof JLabel) {
                JComponent panel = this.model.getPanel(category);
                if (null == panel.getBorder()) {
                    panel.setBorder(BorderFactory.createEmptyBorder(11, 11, 11, 11));
                }
                JScrollPane scroll = new JScrollPane(panel);
                scroll.setOpaque(false);
                scroll.getViewport().setOpaque(false);
                scroll.setBorder(BorderFactory.createEmptyBorder());
                scroll.getVerticalScrollBar().setUnitIncrement(16);
                scroll.getHorizontalScrollBar().setUnitIncrement(16);
                this.tabbedPanel.setComponentAt(this.tabbedPanel.getSelectedIndex(), scroll);
            }
            this.model.update(category);
            if (searchText != null && matchedKeywords != null) {
                OptionsPanelController controller = this.model.getController(this.model.getID(category));
                if (controller == null) {
                    LOGGER.log(Level.WARNING, "No controller found for category: {0}", category);
                } else {
                    controller.handleSuccessfulSearch(searchText, matchedKeywords);
                }
            }
            this.firePropertyChange("helpCtx", null, null);
        }
    }

    void handleSearch(String searchText, List<String> matchedKeywords) {
        this.handleTabSwitched(searchText, matchedKeywords);
    }

    private class LookupListenerImpl
    implements LookupListener {
        private LookupListenerImpl() {
        }

        public void resultChanged(LookupEvent ev) {
            AdvancedPanel.this.model = new Model(AdvancedPanel.this.subpath, AdvancedPanel.this.listener);
            if (SwingUtilities.isEventDispatchThread()) {
                AdvancedPanel.this.initTabbedPane();
            } else {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        AdvancedPanel.this.initTabbedPane();
                    }
                });
            }
        }

    }

}

