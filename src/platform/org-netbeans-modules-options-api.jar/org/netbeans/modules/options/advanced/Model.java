/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupListener
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.modules.options.advanced;

import java.awt.Component;
import java.awt.Dimension;
import java.beans.PropertyChangeListener;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import org.netbeans.modules.options.OptionsPanelControllerAccessor;
import org.netbeans.modules.options.ui.TabbedPanelModel;
import org.netbeans.spi.options.AdvancedOption;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.LookupListener;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

public final class Model
extends TabbedPanelModel {
    private Map<String, String> idToCategory = new HashMap<String, String>();
    private Map<String, AdvancedOption> categoryToOption = new LinkedHashMap<String, AdvancedOption>();
    private Map<String, JComponent> categoryToPanel = new HashMap<String, JComponent>();
    private Map<String, OptionsPanelController> categoryToController = new HashMap<String, OptionsPanelController>();
    private Lookup masterLookup;
    private LookupListener lkpListener;
    private Lookup.Result<AdvancedOption> lkpResult;
    private String subpath;
    private PropertyChangeListener propertyChangeListener;
    private boolean initialized = false;

    public Model(String subpath, LookupListener listener) {
        this.subpath = subpath;
        this.lkpListener = listener;
    }

    @Override
    public List<String> getCategories() {
        this.init();
        ArrayList<String> l = new ArrayList<String>(this.categoryToOption.keySet());
        if ("Advanced".equals(this.subpath)) {
            Collections.sort(l, Collator.getInstance());
        }
        return l;
    }

    public List<String> getIDs() {
        this.init();
        return new ArrayList<String>(this.idToCategory.keySet());
    }

    public String getID(String category) {
        this.init();
        for (Map.Entry<String, String> entrySet : this.idToCategory.entrySet()) {
            if (!entrySet.getValue().equals(category)) continue;
            return entrySet.getKey();
        }
        return null;
    }

    @Override
    public String getToolTip(String category) {
        AdvancedOption option = this.categoryToOption.get(category);
        return option.getTooltip();
    }

    public String getDisplayName(String categoryID) {
        AdvancedOption option = this.categoryToOption.get(this.idToCategory.get(categoryID));
        if (option == null) {
            Logger.getLogger(Model.class.getName()).info("No category found for ID: " + categoryID);
            return "";
        }
        return option.getDisplayName();
    }

    public OptionsPanelController getController(String categoryID) {
        return this.categoryToController.get(this.getDisplayName(categoryID));
    }

    @Override
    public JComponent getPanel(String category) {
        this.init();
        JComponent panel = this.categoryToPanel.get(category);
        if (panel != null) {
            return panel;
        }
        AdvancedOption option = this.categoryToOption.get(category);
        OptionsPanelController controller = this.categoryToController.get(category);
        if (controller == null) {
            controller = new DelegatingController(option.create());
            this.categoryToController.put(category, controller);
        }
        controller.addPropertyChangeListener(this.propertyChangeListener);
        panel = controller.getComponent(this.masterLookup);
        this.categoryToPanel.put(category, panel);
        Border b = panel.getBorder();
        b = b != null ? new CompoundBorder(new EmptyBorder(6, 16, 6, 6), b) : new EmptyBorder(6, 16, 6, 6);
        panel.setBorder(b);
        panel.setMaximumSize(panel.getPreferredSize());
        return panel;
    }

    void update(String category) {
        OptionsPanelController controller = this.categoryToController.get(category);
        if (controller != null) {
            controller.update();
        }
    }

    void applyChanges() {
        Iterator<OptionsPanelController> it = this.categoryToController.values().iterator();
        while (it.hasNext()) {
            it.next().applyChanges();
        }
    }

    void cancel() {
        Iterator<OptionsPanelController> it = this.categoryToController.values().iterator();
        while (it.hasNext()) {
            it.next().cancel();
        }
    }

    boolean isValid() {
        for (OptionsPanelController controller : this.categoryToController.values()) {
            if (controller.isValid() || !controller.isChanged()) continue;
            return false;
        }
        return true;
    }

    boolean isChanged() {
        Iterator<OptionsPanelController> it = this.categoryToController.values().iterator();
        while (it.hasNext()) {
            if (!it.next().isChanged()) continue;
            return true;
        }
        return false;
    }

    void addPropertyChangeListener(PropertyChangeListener listener) {
        this.propertyChangeListener = listener;
        for (OptionsPanelController controller : this.categoryToController.values()) {
            controller.addPropertyChangeListener(listener);
        }
    }

    void removePropertyChangeListener(PropertyChangeListener listener) {
        for (OptionsPanelController controller : this.categoryToController.values()) {
            controller.removePropertyChangeListener(listener);
        }
    }

    Lookup getLookup() {
        ArrayList<Lookup> lookups = new ArrayList<Lookup>();
        Iterator<OptionsPanelController> it = this.categoryToController.values().iterator();
        while (it.hasNext()) {
            lookups.add(it.next().getLookup());
        }
        return new ProxyLookup(lookups.toArray((T[])new Lookup[lookups.size()]));
    }

    HelpCtx getHelpCtx(JComponent panel) {
        Component view;
        if (panel instanceof JScrollPane && (view = ((JScrollPane)panel).getViewport().getView()) instanceof JComponent) {
            panel = (JComponent)view;
        }
        for (String category : this.categoryToPanel.keySet()) {
            OptionsPanelController controller;
            if (panel != null && panel != this.categoryToPanel.get(category) || (controller = this.categoryToController.get(category)) == null) continue;
            return controller.getHelpCtx();
        }
        return new HelpCtx("netbeans.optionsDialog.advanced");
    }

    private void init() {
        if (this.initialized) {
            return;
        }
        this.initialized = true;
        String path = "OptionsDialog/" + this.subpath;
        Lookup lookup = Lookups.forPath((String)path);
        this.lkpResult = lookup.lookup(new Lookup.Template(AdvancedOption.class));
        for (Lookup.Item item : this.lkpResult.allItems()) {
            if (!item.getId().substring(0, item.getId().lastIndexOf(47)).equals(path)) continue;
            AdvancedOption option = (AdvancedOption)item.getInstance();
            String displayName = option.getDisplayName();
            if (displayName != null) {
                this.categoryToOption.put(option.getDisplayName(), option);
                this.idToCategory.put(item.getId().substring(path.length() + 1), ((AdvancedOption)item.getInstance()).getDisplayName());
                continue;
            }
            assert (false);
        }
        this.lkpResult.addLookupListener(this.lkpListener);
        this.lkpListener = null;
    }

    void setLoookup(Lookup masterLookup) {
        this.masterLookup = masterLookup;
    }

    private static final class DelegatingController
    extends OptionsPanelController {
        private OptionsPanelController delegate;
        private boolean isUpdated;

        private DelegatingController(OptionsPanelController delegate) {
            this.delegate = delegate;
        }

        @Override
        public void update() {
            if (!this.isUpdated) {
                this.isUpdated = true;
                this.delegate.update();
            }
        }

        @Override
        public void applyChanges() {
            this.isUpdated = false;
            this.delegate.applyChanges();
        }

        @Override
        public void cancel() {
            this.isUpdated = false;
            this.delegate.cancel();
        }

        @Override
        public boolean isValid() {
            return this.delegate.isValid();
        }

        @Override
        public boolean isChanged() {
            return this.delegate.isChanged();
        }

        @Override
        public JComponent getComponent(Lookup masterLookup) {
            return this.delegate.getComponent(masterLookup);
        }

        @Override
        public void setCurrentSubcategory(String subpath) {
            OptionsPanelControllerAccessor.getDefault().setCurrentSubcategory(this.delegate, subpath);
        }

        @Override
        public HelpCtx getHelpCtx() {
            return this.delegate.getHelpCtx();
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener l) {
            this.delegate.addPropertyChangeListener(l);
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener l) {
            this.delegate.removePropertyChangeListener(l);
        }

        @Override
        public void handleSuccessfulSearch(String searchText, List<String> matchedKeywords) {
            this.delegate.handleSuccessfulSearch(searchText, matchedKeywords);
        }
    }

}

