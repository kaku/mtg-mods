/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 *  org.openide.util.NbBundle
 *  org.openide.util.NbBundle$Messages
 */
package org.netbeans.modules.options;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.tools.FileObject;
import org.netbeans.spi.options.AdvancedOption;
import org.netbeans.spi.options.OptionsCategory;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;
import org.openide.util.NbBundle;

@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
public class OptionsPanelControllerProcessor
extends LayerGeneratingProcessor {
    private Element originatingElement;

    public Set<String> getSupportedAnnotationTypes() {
        return new HashSet<String>(Arrays.asList(OptionsPanelController.TopLevelRegistration.class.getCanonicalName(), OptionsPanelController.ContainerRegistration.class.getCanonicalName(), OptionsPanelController.SubRegistration.class.getCanonicalName(), OptionsPanelController.KeywordsRegistration.class.getCanonicalName(), OptionsPanelController.Keywords.class.getCanonicalName()));
    }

    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws LayerGenerationException {
        LayerBuilder builder;
        LayerBuilder.File file;
        if (roundEnv.processingOver()) {
            return false;
        }
        for (Element e2222 : roundEnv.getElementsAnnotatedWith(OptionsPanelController.TopLevelRegistration.class)) {
            OptionsPanelController.TopLevelRegistration r = e2222.getAnnotation(OptionsPanelController.TopLevelRegistration.class);
            if (r == null) continue;
            builder = this.layer(new Element[]{e2222});
            file = builder.instanceFile("OptionsDialog", r.id().length() > 0 ? r.id() : null, (Annotation)r, null).methodvalue("instanceCreate", OptionsCategory.class.getName(), "createCategory").instanceAttribute("controller", OptionsPanelController.class).bundlevalue("categoryName", r.categoryName()).position(r.position());
            this.iconBase(e2222, r.iconBase(), r, file, builder);
            this.keywords(e2222, r.keywords(), r.keywordsCategory(), r, file);
            file.write();
        }
        for (Element e : roundEnv.getElementsAnnotatedWith(OptionsPanelController.SubRegistration.class)) {
            OptionsPanelController.SubRegistration r = e.getAnnotation(OptionsPanelController.SubRegistration.class);
            if (r.position() != Integer.MAX_VALUE && r.location().equals("Advanced")) {
                throw new LayerGenerationException("position ignored for Advanced subpanels", e, this.processingEnv, (Annotation)r, "position");
            }
            LayerBuilder.File file2 = this.layer(new Element[]{e}).instanceFile("OptionsDialog/" + r.location(), r.id().length() > 0 ? r.id() : null, (Annotation)r, null).methodvalue("instanceCreate", AdvancedOption.class.getName(), "createSubCategory").instanceAttribute("controller", OptionsPanelController.class).bundlevalue("displayName", r.displayName()).position(r.position());
            this.keywords(e, r.keywords(), r.keywordsCategory(), r, file2);
            file2.write();
        }
        for (Element e2 : roundEnv.getElementsAnnotatedWith(OptionsPanelController.Keywords.class)) {
            this.handleElement(e2, e2.getAnnotation(OptionsPanelController.Keywords.class), "");
        }
        for (Element e3 : roundEnv.getElementsAnnotatedWith(OptionsPanelController.KeywordsRegistration.class)) {
            OptionsPanelController.KeywordsRegistration r = e3.getAnnotation(OptionsPanelController.KeywordsRegistration.class);
            OptionsPanelController.Keywords[] panels = r.value();
            for (int i = 0; i < panels.length; ++i) {
                this.handleElement(e3, panels[i], Integer.toString(- i + 1));
            }
        }
        for (Element e4 : roundEnv.getElementsAnnotatedWith(OptionsPanelController.ContainerRegistration.class)) {
            OptionsPanelController.ContainerRegistration r = e4.getAnnotation(OptionsPanelController.ContainerRegistration.class);
            builder = this.layer(new Element[]{e4});
            file = builder.file("OptionsDialog/" + r.id() + ".instance").methodvalue("instanceCreate", OptionsCategory.class.getName(), "createCategory").stringvalue("advancedOptionsFolder", "OptionsDialog/" + r.id()).bundlevalue("categoryName", r.categoryName()).position(r.position());
            this.iconBase(e4, r.iconBase(), r, file, builder);
            this.keywords(e4, r.keywords(), r.keywordsCategory(), r, file);
            file.write();
            this.layer(new Element[]{e4}).folder("OptionsDialog/" + r.id()).position(0).write();
        }
        return true;
    }

    private void handleElement(Element e, OptionsPanelController.Keywords annotation, String name) throws LayerGenerationException {
        this.originatingElement = e;
        if (!annotation.location().equals("General") && !annotation.location().equals("Keymaps") && annotation.tabTitle().trim().isEmpty()) {
            throw new LayerGenerationException("Must specify tabTitle", e, this.processingEnv, (Annotation)annotation, "tabTitle");
        }
        LayerBuilder.File file = this.layer(new Element[]{e}).file("OptionsDialog/Keywords/".concat(e.asType().toString()).concat(name)).stringvalue("location", annotation.location()).bundlevalue("tabTitle", annotation.tabTitle(), (Annotation)annotation, "tabTitle");
        String[] keywords = annotation.keywords();
        for (int j = 0; j < keywords.length; ++j) {
            file = file.bundlevalue("keywords-".concat(Integer.toString(j + 1)), keywords[j], (Annotation)annotation, "keywords");
        }
        file.write();
    }

    private String getBundleValue(String label, Annotation annotation, String annotationMethod) throws LayerGenerationException {
        String javaIdentifier = "(?:\\p{javaJavaIdentifierStart}\\p{javaJavaIdentifierPart}*)";
        Matcher m = Pattern.compile("((?:" + javaIdentifier + "\\.)+[^\\s.#]+)?#(\\S*)").matcher(label);
        if (m.matches()) {
            String bundle = m.group(1);
            String key = m.group(2);
            if (bundle == null) {
                Element referenceElement;
                for (referenceElement = this.originatingElement; referenceElement != null && referenceElement.getKind() != ElementKind.PACKAGE; referenceElement = referenceElement.getEnclosingElement()) {
                }
                if (referenceElement == null) {
                    throw new LayerGenerationException("No reference element to determine package in '" + label + "'", this.originatingElement);
                }
                bundle = ((PackageElement)referenceElement).getQualifiedName() + ".Bundle";
            }
            return this.verifyBundleValue(bundle, key, m.group(1) == null, annotation, annotationMethod);
        }
        return label;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private String verifyBundleValue(String bundle, String key, boolean samePackage, Annotation annotation, String annotationMethod) throws LayerGenerationException {
        String arr$;
        if (this.processingEnv == null) {
            return "";
        }
        if (samePackage) {
            for (Element e = this.originatingElement; e != null; e = e.getEnclosingElement()) {
                NbBundle.Messages m = e.getAnnotation(NbBundle.Messages.class);
                if (m == null) continue;
                for (String kv : m.value()) {
                    if (!kv.startsWith(key + "=")) continue;
                    return bundle.concat("#").concat(key);
                }
            }
        }
        InputStream is = this.layer(new Element[]{this.originatingElement}).validateResource(bundle.replace('.', '/') + ".properties", this.originatingElement, null, null, false).openInputStream();
        try {
            Properties p = new Properties();
            p.load(is);
            if (p.getProperty(key) == null) {
                throw new LayerGenerationException("No key '" + key + "' found in " + bundle, this.originatingElement, this.processingEnv, annotation, annotationMethod);
            }
            arr$ = bundle.concat("#").concat(key);
        }
        catch (Throwable var12_13) {
            try {
                is.close();
                throw var12_13;
            }
            catch (IOException x) {
                throw new LayerGenerationException("Could not open " + bundle + ": " + x, this.originatingElement, this.processingEnv, annotation, annotationMethod);
            }
        }
        is.close();
        return arr$;
    }

    private void iconBase(Element e, String iconBase, Annotation r, LayerBuilder.File file, LayerBuilder builder) throws LayerGenerationException {
        builder.validateResource(iconBase, e, r, "iconBase", true);
        file.stringvalue("iconBase", iconBase);
    }

    private void keywords(Element e, String keywords, String keywordsCategory, Annotation r, LayerBuilder.File file) throws LayerGenerationException {
        if (keywords.length() > 0) {
            if (keywordsCategory.length() == 0) {
                throw new LayerGenerationException("Must specify both keywords and keywordsCategory", e, this.processingEnv, r, "keywordsCategory");
            }
            file.bundlevalue("keywords", keywords, r, "keywords").bundlevalue("keywordsCategory", keywordsCategory, r, "keywordsCategory");
        } else if (keywordsCategory.length() > 0) {
            throw new LayerGenerationException("Must specify both keywords and keywordsCategory", e, this.processingEnv, r, "keywords");
        }
    }
}

