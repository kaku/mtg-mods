/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.options;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.Callable;
import org.netbeans.modules.options.TabbedController;
import org.netbeans.spi.options.AdvancedOption;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.Exceptions;

public class AdvancedOptionImpl
extends AdvancedOption {
    private String displayName;
    private String tooltip;
    private String keywords;
    private Callable<OptionsPanelController> controller;
    private String keywordsCategory;

    public AdvancedOptionImpl(Callable<OptionsPanelController> controller, String displayName, String tooltip, String keywords, String keywordsCategory) {
        this.controller = controller;
        this.displayName = displayName;
        this.tooltip = tooltip;
        this.keywords = keywords;
        this.keywordsCategory = keywordsCategory;
    }

    @Override
    public String getDisplayName() {
        return this.displayName;
    }

    @Override
    public String getTooltip() {
        return this.tooltip;
    }

    public Set<String> getKeywordsByCategory() {
        if (this.keywords != null) {
            return Collections.singleton(this.keywords);
        }
        return Collections.emptySet();
    }

    @Override
    public OptionsPanelController create() {
        try {
            return this.controller.call();
        }
        catch (Exception x) {
            Exceptions.printStackTrace((Throwable)x);
            return new TabbedController("<error>");
        }
    }
}

