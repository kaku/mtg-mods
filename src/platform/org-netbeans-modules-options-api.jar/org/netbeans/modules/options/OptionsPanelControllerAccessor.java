/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.options;

import org.netbeans.spi.options.OptionsPanelController;

public abstract class OptionsPanelControllerAccessor {
    public static OptionsPanelControllerAccessor DEFAULT;

    public static OptionsPanelControllerAccessor getDefault() {
        block4 : {
            if (DEFAULT != null) {
                return DEFAULT;
            }
            Class<OptionsPanelController> c = OptionsPanelController.class;
            try {
                Class.forName(c.getName(), true, c.getClassLoader());
            }
            catch (ClassNotFoundException ex) {
                if ($assertionsDisabled) break block4;
                throw new AssertionError(ex);
            }
        }
        assert (DEFAULT != null);
        return DEFAULT;
    }

    public abstract void setCurrentSubcategory(OptionsPanelController var1, String var2);
}

