/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressUtils
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.awt.Mnemonics
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 *  org.openide.util.TaskListener
 *  org.openide.util.Utilities
 *  org.openide.util.actions.CallableSystemAction
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.WindowManager
 */
package org.netbeans.modules.options;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.netbeans.api.options.OptionsDisplayer;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.modules.options.Bundle;
import org.netbeans.modules.options.CategoryModel;
import org.netbeans.modules.options.OptionsPanel;
import org.netbeans.modules.options.classic.OptionsAction;
import org.netbeans.modules.options.export.OptionsChooserPanel;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.TaskListener;
import org.openide.util.Utilities;
import org.openide.util.actions.CallableSystemAction;
import org.openide.util.actions.SystemAction;
import org.openide.windows.WindowManager;

public class OptionsDisplayerImpl {
    private static Dialog dialog;
    private static WeakReference<DialogDescriptor> descriptorRef;
    private static String title;
    private static Logger log;
    private FileChangeListener fcl;
    private boolean modal;
    static final LookupListener lookupListener;
    private JButton bOK;
    private JButton bAPPLY;
    private JButton bClassic;
    private JButton btnExport;
    private JButton btnImport;
    private static final RequestProcessor RP;
    private static final int DELAY = 500;
    private boolean savingInProgress = false;

    public OptionsDisplayerImpl(boolean modal) {
        this.modal = modal;
        this.fcl = new DefaultFSListener();
        try {
            FileUtil.getConfigRoot().getFileSystem().addFileChangeListener(this.fcl);
        }
        catch (FileStateInvalidException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    public void setIsModal(boolean isModal) {
        this.modal = isModal;
    }

    public boolean isOpen() {
        return dialog != null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void selectCategory(String path) {
        DialogDescriptor descriptor = null;
        LookupListener lookupListener = OptionsDisplayerImpl.lookupListener;
        synchronized (lookupListener) {
            descriptor = descriptorRef.get();
        }
        if (descriptor != null) {
            OptionsPanel optionsPanel = (OptionsPanel)descriptor.getMessage();
            String categoryId = path.indexOf(47) == -1 ? path : path.substring(0, path.indexOf(47));
            String subpath = path.indexOf(47) == -1 ? null : path.substring(path.indexOf(47) + 1);
            optionsPanel.initCurrentCategory(categoryId, subpath);
        }
        dialog.toFront();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void showOptionsDialog(String categoryID, String subpath, CategoryModel categoryInstance) {
        log.fine("showOptionsDialog(" + categoryID + ", " + subpath + ")");
        if (this.isOpen()) {
            dialog.setVisible(true);
            dialog.toFront();
            log.fine("Front Options Dialog");
            return;
        }
        DialogDescriptor descriptor = null;
        LookupListener lookupListener = OptionsDisplayerImpl.lookupListener;
        synchronized (lookupListener) {
            descriptor = descriptorRef.get();
        }
        OptionsPanel optionsPanel = null;
        if (descriptor == null) {
            optionsPanel = categoryID == null ? new OptionsPanel(categoryInstance) : new OptionsPanel(categoryID, categoryInstance);
            this.bOK = (JButton)OptionsDisplayerImpl.loc(new JButton(), "CTL_OK");
            this.bOK.getAccessibleContext().setAccessibleDescription(OptionsDisplayerImpl.loc("ACS_OKButton"));
            this.bAPPLY = (JButton)OptionsDisplayerImpl.loc(new JButton(), "CTL_APPLY");
            this.bAPPLY.getAccessibleContext().setAccessibleDescription(OptionsDisplayerImpl.loc("ACS_APPLYButton"));
            this.bAPPLY.setEnabled(false);
            this.bClassic = (JButton)OptionsDisplayerImpl.loc(new JButton(), "CTL_Classic");
            this.bClassic.getAccessibleContext().setAccessibleDescription(OptionsDisplayerImpl.loc("ACS_ClassicButton"));
            this.btnExport = (JButton)OptionsDisplayerImpl.loc(new JButton(), "CTL_Export");
            this.btnExport.getAccessibleContext().setAccessibleDescription(OptionsDisplayerImpl.loc("ACS_Export"));
            this.btnImport = (JButton)OptionsDisplayerImpl.loc(new JButton(), "CTL_Import");
            this.btnImport.getAccessibleContext().setAccessibleDescription(OptionsDisplayerImpl.loc("ACS_Import"));
            this.updateButtons();
            boolean isMac = Utilities.isMac();
            Object[] options = new Object[3];
            options[0] = isMac ? DialogDescriptor.CANCEL_OPTION : this.bOK;
            options[1] = this.bAPPLY;
            options[2] = isMac ? this.bOK : DialogDescriptor.CANCEL_OPTION;
            descriptor = new DialogDescriptor((Object)optionsPanel, title, this.modal, options, DialogDescriptor.OK_OPTION, 0, null, null, false);
            JPanel additionalOptionspanel = new JPanel(new FlowLayout(1, 5, 0));
            additionalOptionspanel.add(this.bClassic);
            this.setUpButtonListeners(optionsPanel);
            descriptor.setAdditionalOptions(new Object[]{additionalOptionspanel});
            descriptor.setHelpCtx(optionsPanel.getHelpCtx());
            OptionsPanelListener listener = new OptionsPanelListener(descriptor, optionsPanel, this.bOK, this.bAPPLY);
            descriptor.setClosingOptions(new Object[]{DialogDescriptor.CANCEL_OPTION, this.bOK});
            descriptor.setButtonListener((ActionListener)listener);
            optionsPanel.addPropertyChangeListener(listener);
            LookupListener lookupListener2 = OptionsDisplayerImpl.lookupListener;
            synchronized (lookupListener2) {
                descriptorRef = new WeakReference<DialogDescriptor>(descriptor);
            }
            log.fine("Create new Options Dialog");
        } else {
            optionsPanel = (OptionsPanel)descriptor.getMessage();
            optionsPanel.setCategoryInstance(categoryInstance);
            optionsPanel.update();
            log.fine("Reopen Options Dialog");
        }
        descriptor.setBackground(UIManager.getLookAndFeelDefaults().getColor("Panel.background"));
        Dialog tmpDialog = DialogDisplayer.getDefault().createDialog(descriptor, WindowManager.getDefault().getMainWindow());
        log.fine("Options Dialog created; descriptor.title = " + descriptor.getTitle() + "; descriptor.message = " + descriptor.getMessage());
        optionsPanel.initCurrentCategory(categoryID, subpath);
        tmpDialog.addWindowListener(new MyWindowListener(optionsPanel, tmpDialog));
        Point userLocation = this.getUserLocation(optionsPanel);
        if (userLocation != null) {
            tmpDialog.setLocation(userLocation);
            log.fine("userLocation is set to " + userLocation);
        }
        log.fine("setting Options Dialog visible");
        tmpDialog.setVisible(true);
        dialog = tmpDialog;
        this.setUpApplyChecker(optionsPanel);
    }

    private void setUpApplyChecker(final OptionsPanel optsPanel) {
        final RequestProcessor.Task applyChecker = RP.post(new Runnable(){

            @Override
            public void run() {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        if (!OptionsDisplayerImpl.this.savingInProgress) {
                            OptionsDisplayerImpl.this.bAPPLY.setEnabled(optsPanel.isChanged() && optsPanel.dataValid());
                        }
                    }
                });
            }

        });
        applyChecker.addTaskListener(new TaskListener(){

            public void taskFinished(Task task) {
                if (dialog != null) {
                    applyChecker.schedule(500);
                }
            }
        });
    }

    private void setUpButtonListeners(OptionsPanel optionsPanel) {
        this.btnExport.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                OptionsChooserPanel.showExportDialog();
            }
        });
        this.btnImport.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                OptionsChooserPanel.showImportDialog();
            }
        });
        final OptionsPanel finalOptionsPanel = optionsPanel;
        this.bClassic.addActionListener(new ActionListener(){

            /*
             * Enabled aggressive block sorting
             * Enabled unnecessary exception pruning
             * Enabled aggressive exception aggregation
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                log.fine("Options Dialog - Classic pressed.");
                Dialog d = dialog;
                dialog = null;
                if (finalOptionsPanel.isChanged()) {
                    NotifyDescriptor.Confirmation confirmationDescriptor = new NotifyDescriptor.Confirmation((Object)OptionsDisplayerImpl.loc("CTL_Some_values_changed"), 1, 3);
                    Object result = DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmationDescriptor);
                    if (result == NotifyDescriptor.YES_OPTION) {
                        finalOptionsPanel.save();
                        d.dispose();
                    } else {
                        if (result != NotifyDescriptor.NO_OPTION) {
                            dialog = d;
                            return;
                        }
                        finalOptionsPanel.cancel();
                        d.dispose();
                    }
                } else {
                    d.dispose();
                    finalOptionsPanel.cancel();
                }
                try {
                    CallableSystemAction a = (CallableSystemAction)SystemAction.get(OptionsAction.class);
                    a.putValue("additionalActionName", (Object)OptionsDisplayerImpl.loc("CTL_Modern"));
                    a.putValue("optionsDialogTitle", (Object)OptionsDisplayerImpl.loc("CTL_Classic_Title"));
                    a.putValue("additionalActionListener", (Object)new OpenOptionsListener());
                    a.performAction();
                    return;
                }
                catch (Exception ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        });
    }

    private void updateButtons() {
        if (this.bClassic != null) {
            this.bClassic.setVisible(this.advancedOptionsNotEmpty());
        }
        boolean optionsExportNotEmpty = this.optionsExportNotEmpty();
        if (this.btnExport != null) {
            this.btnExport.setVisible(optionsExportNotEmpty);
        }
        if (this.btnImport != null) {
            this.btnImport.setVisible(optionsExportNotEmpty);
        }
    }

    private boolean advancedOptionsNotEmpty() {
        FileObject servicesFO = FileUtil.getConfigFile((String)"UI/Services");
        if (servicesFO != null) {
            FileObject[] advancedOptions;
            for (FileObject advancedOption : advancedOptions = servicesFO.getChildren()) {
                Object hidden = advancedOption.getAttribute("hidden");
                if (hidden != null && ((Boolean)hidden).booleanValue()) continue;
                return true;
            }
        }
        return false;
    }

    private boolean optionsExportNotEmpty() {
        FileObject optionsExportFO = FileUtil.getConfigFile((String)"OptionsExport");
        if (optionsExportFO != null) {
            FileObject[] categories;
            for (FileObject category : categories = optionsExportFO.getChildren()) {
                FileObject[] items;
                Object hiddenCategory = category.getAttribute("hidden");
                if (hiddenCategory != null && ((Boolean)hiddenCategory).booleanValue()) continue;
                for (FileObject item : items = category.getChildren()) {
                    Object hiddenItem = item.getAttribute("hidden");
                    if (hiddenItem != null && ((Boolean)hiddenItem).booleanValue()) continue;
                    return true;
                }
            }
        }
        return false;
    }

    private Point getUserLocation(OptionsPanel optionsPanel) {
        GraphicsDevice[] screenDevices;
        for (GraphicsDevice device : screenDevices = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()) {
            Point userLocation = this.getUserLocation(device.getDefaultConfiguration(), optionsPanel);
            if (userLocation == null) continue;
            return userLocation;
        }
        return null;
    }

    private Point getUserLocation(GraphicsConfiguration gconf, OptionsPanel optionsPanel) {
        Rectangle screenBounds = Utilities.getUsableScreenBounds((GraphicsConfiguration)gconf);
        int x = NbPreferences.forModule(OptionsDisplayerImpl.class).getInt("OptionsX", Integer.MAX_VALUE);
        int y = NbPreferences.forModule(OptionsDisplayerImpl.class).getInt("OptionsY", Integer.MAX_VALUE);
        Dimension userSize = optionsPanel.getUserSize();
        if ((double)x > (double)screenBounds.x + screenBounds.getWidth() || (double)y > (double)screenBounds.y + screenBounds.getHeight() || (double)(x + userSize.width) > (double)screenBounds.x + screenBounds.getWidth() || (double)(y + userSize.height) > (double)screenBounds.y + screenBounds.getHeight() || x < screenBounds.x && screenBounds.x >= 0 || x > screenBounds.x && screenBounds.x < 0 || y < screenBounds.y && screenBounds.y >= 0 || y > screenBounds.y && screenBounds.y < 0) {
            return null;
        }
        return new Point(x, y);
    }

    private static String loc(String key) {
        return NbBundle.getMessage(OptionsDisplayerImpl.class, (String)key);
    }

    private static Component loc(Component c, String key) {
        if (c instanceof AbstractButton) {
            Mnemonics.setLocalizedText((AbstractButton)((AbstractButton)c), (String)OptionsDisplayerImpl.loc(key));
        } else {
            Mnemonics.setLocalizedText((JLabel)((JLabel)c), (String)OptionsDisplayerImpl.loc(key));
        }
        return c;
    }

    static {
        descriptorRef = new WeakReference<Object>(null);
        title = OptionsDisplayerImpl.loc("CTL_Options_Dialog_Title");
        log = Logger.getLogger(OptionsDisplayerImpl.class.getName());
        lookupListener = new LookupListenerImpl();
        RP = new RequestProcessor(OptionsDisplayerImpl.class.getName(), 1, true);
    }

    private class DefaultFSListener
    implements FileChangeListener {
        private DefaultFSListener() {
        }

        public void fileRenamed(FileRenameEvent fe) {
            OptionsDisplayerImpl.this.updateButtons();
        }

        public void fileChanged(FileEvent fe) {
            OptionsDisplayerImpl.this.updateButtons();
        }

        public void fileFolderCreated(FileEvent fe) {
            OptionsDisplayerImpl.this.updateButtons();
        }

        public void fileDataCreated(FileEvent fe) {
            OptionsDisplayerImpl.this.updateButtons();
        }

        public void fileDeleted(FileEvent fe) {
            OptionsDisplayerImpl.this.updateButtons();
        }

        public void fileAttributeChanged(FileAttributeEvent fe) {
            OptionsDisplayerImpl.this.updateButtons();
        }
    }

    private static class LookupListenerImpl
    implements LookupListener {
        private LookupListenerImpl() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void resultChanged(LookupEvent ev) {
            LookupListener lookupListener = OptionsDisplayerImpl.lookupListener;
            synchronized (lookupListener) {
                descriptorRef = new WeakReference<Object>(null);
                if (dialog != null) {
                    Mutex.EVENT.readAccess(new Runnable(){

                        @Override
                        public void run() {
                            if (dialog != null) {
                                log.log(Level.FINE, "Options Dialog - closing dialog when categories change.");
                                dialog.setVisible(false);
                                dialog = null;
                            }
                        }
                    });
                }
            }
        }

    }

    class OpenOptionsListener
    implements ActionListener {
        OpenOptionsListener() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            RequestProcessor.getDefault().post(new Runnable(){

                @Override
                public void run() {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            log.fine("Options Dialog - Back to modern.");
                            OptionsDisplayer.getDefault().open();
                        }
                    });
                }

            });
        }

    }

    private class MyWindowListener
    implements WindowListener {
        private OptionsPanel optionsPanel;
        private Dialog originalDialog;

        MyWindowListener(OptionsPanel optionsPanel, Dialog tmpDialog) {
            this.optionsPanel = optionsPanel;
            this.originalDialog = tmpDialog;
        }

        @Override
        public void windowClosing(WindowEvent e) {
            if (dialog == null) {
                return;
            }
            log.fine("Options Dialog - windowClosing ");
            this.optionsPanel.cancel();
            OptionsDisplayerImpl.this.bOK.setEnabled(true);
            OptionsDisplayerImpl.this.bAPPLY.setEnabled(false);
            if (this.originalDialog == dialog) {
                dialog = null;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void windowClosed(WindowEvent e) {
            this.optionsPanel.storeUserSize();
            NbPreferences.forModule(OptionsDisplayerImpl.class).putInt("OptionsX", this.originalDialog.getX());
            NbPreferences.forModule(OptionsDisplayerImpl.class).putInt("OptionsY", this.originalDialog.getY());
            try {
                FileUtil.getConfigRoot().getFileSystem().removeFileChangeListener(OptionsDisplayerImpl.this.fcl);
            }
            catch (FileStateInvalidException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            if (this.optionsPanel.needsReinit()) {
                LookupListener ex = OptionsDisplayerImpl.lookupListener;
                synchronized (ex) {
                    descriptorRef = new WeakReference<Object>(null);
                }
            }
            if (this.originalDialog == dialog) {
                dialog = null;
            }
            log.fine("Options Dialog - windowClosed");
        }

        @Override
        public void windowDeactivated(WindowEvent e) {
        }

        @Override
        public void windowOpened(WindowEvent e) {
        }

        @Override
        public void windowIconified(WindowEvent e) {
        }

        @Override
        public void windowDeiconified(WindowEvent e) {
        }

        @Override
        public void windowActivated(WindowEvent e) {
        }
    }

    private class OptionsPanelListener
    implements PropertyChangeListener,
    ActionListener {
        private DialogDescriptor descriptor;
        private OptionsPanel optionsPanel;
        private JButton bOK;
        private JButton bAPPLY;
        private HelpCtx helpCtx;

        OptionsPanelListener(DialogDescriptor descriptor, OptionsPanel optionsPanel, JButton bOK, JButton bAPPLY) {
            this.helpCtx = HelpCtx.DEFAULT_HELP;
            this.descriptor = descriptor;
            this.optionsPanel = optionsPanel;
            this.bOK = bOK;
            this.bAPPLY = bAPPLY;
        }

        @Override
        public void propertyChange(PropertyChangeEvent ev) {
            if (ev.getPropertyName().equals("buranhelpCtx")) {
                AtomicBoolean helpCtxLoadingCancelled = new AtomicBoolean(false);
                ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

                    @Override
                    public void run() {
                        OptionsPanelListener.this.helpCtx = OptionsPanelListener.this.optionsPanel.getHelpCtx();
                    }
                }, (String)Bundle.Loading_HelpCtx_Lengthy_Operation(), (AtomicBoolean)helpCtxLoadingCancelled, (boolean)false, (int)50, (int)5000);
                if (helpCtxLoadingCancelled.get()) {
                    log.fine("Options Dialog - HelpCtx loading cancelled by user.");
                }
                this.descriptor.setHelpCtx(this.helpCtx);
            } else if (ev.getPropertyName().equals("buranvalid")) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        OptionsPanelListener.this.bOK.setEnabled(OptionsPanelListener.this.optionsPanel.dataValid());
                        OptionsPanelListener.this.bAPPLY.setEnabled(OptionsPanelListener.this.optionsPanel.isChanged() && OptionsPanelListener.this.optionsPanel.dataValid());
                    }
                });
            }
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (!OptionsDisplayerImpl.this.isOpen()) {
                return;
            }
            if (e.getSource() == this.bOK) {
                log.fine("Options Dialog - Ok pressed.");
                Dialog d = dialog;
                dialog = null;
                this.saveOptionsOffEDT(true);
                d.dispose();
            } else if (e.getSource() == this.bAPPLY) {
                log.fine("Options Dialog - Apply pressed.");
                this.saveOptionsOffEDT(false);
                this.bAPPLY.setEnabled(false);
            } else if (e.getSource() == DialogDescriptor.CANCEL_OPTION || e.getSource() == DialogDescriptor.CLOSED_OPTION) {
                log.fine("Options Dialog - Cancel pressed.");
                Dialog d = dialog;
                dialog = null;
                this.optionsPanel.cancel();
                this.bOK.setEnabled(true);
                this.bAPPLY.setEnabled(false);
                d.dispose();
            }
        }

        private void saveOptionsOffEDT(final boolean okPressed) {
            OptionsDisplayerImpl.this.savingInProgress = true;
            JPanel content = new JPanel();
            content.add(new JLabel(Bundle.Saving_Options_Lengthy_Operation()));
            ProgressUtils.runOffEventThreadWithCustomDialogContent((Runnable)new Runnable(){

                @Override
                public void run() {
                    if (okPressed) {
                        OptionsPanelListener.this.optionsPanel.save();
                    } else {
                        OptionsPanelListener.this.optionsPanel.save(true);
                    }
                }
            }, (String)Bundle.Saving_Options_Lengthy_Operation_Title(), (JPanel)content, (int)50, (int)5000);
            OptionsDisplayerImpl.this.savingInProgress = false;
        }

    }

}

