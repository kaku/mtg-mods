/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.modules.options;

import java.awt.Component;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JViewport;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.options.OptionsPanelControllerAccessor;
import org.netbeans.spi.options.AdvancedOption;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

public class TabbedController
extends OptionsPanelController {
    private static final Logger LOGGER = Logger.getLogger(TabbedController.class.getName());
    private final String tabFolder;
    private Lookup.Result<AdvancedOption> options;
    private Map<String, String> id2tabTitle;
    private Map<String, OptionsPanelController> tabTitle2controller;
    private final Map<String, AdvancedOption> tabTitle2Option;
    private Lookup masterLookup;
    private final LookupListener lookupListener;
    private JTabbedPane pane;
    private final PropertyChangeSupport pcs;
    private final ChangeListener tabbedPaneChangeListener;

    public TabbedController(String tabFolder) {
        this.lookupListener = new LookupListener(){

            public void resultChanged(LookupEvent ev) {
                TabbedController.this.readPanels();
                Mutex.EVENT.readAccess(new Runnable(){

                    @Override
                    public void run() {
                        TabbedController.this.initTabbedPane();
                    }
                });
            }

        };
        this.pcs = new PropertyChangeSupport(this);
        this.tabbedPaneChangeListener = new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                TabbedController.this.handleTabSwitched(null, null);
            }
        };
        this.tabFolder = tabFolder;
        this.tabTitle2Option = Collections.synchronizedMap(new LinkedHashMap());
        this.readPanels();
        this.options.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this.lookupListener, this.options));
    }

    @Override
    public void update() {
        for (OptionsPanelController c : this.getControllers()) {
            c.update();
        }
    }

    @Override
    public void applyChanges() {
        for (OptionsPanelController c : this.getControllers()) {
            c.applyChanges();
        }
    }

    @Override
    public void cancel() {
        for (OptionsPanelController c : this.getControllers()) {
            c.cancel();
        }
    }

    @Override
    public boolean isValid() {
        for (OptionsPanelController c : this.getControllers()) {
            if (c.isValid() || !c.isChanged()) continue;
            return false;
        }
        return true;
    }

    @Override
    public boolean isChanged() {
        for (OptionsPanelController c : this.getControllers()) {
            if (!c.isChanged()) continue;
            return true;
        }
        return false;
    }

    @Override
    public JComponent getComponent(Lookup masterLookup) {
        if (this.pane == null) {
            this.pane = new JTabbedPane();
            this.pane.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(TabbedController.class, (String)"TabbedController.pane.AD"));
            this.masterLookup = masterLookup;
            this.initTabbedPane();
        }
        return this.pane;
    }

    @Override
    public void handleSuccessfulSearch(String searchText, List<String> matchedKeywords) {
        this.handleTabSwitched(searchText, matchedKeywords);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void initTabbedPane() {
        if (this.pane != null) {
            this.pane.removeChangeListener(this.tabbedPaneChangeListener);
            this.pane.removeAll();
            Set<String> keySet = this.tabTitle2Option.keySet();
            Map<String, AdvancedOption> map = this.tabTitle2Option;
            synchronized (map) {
                for (String tabTitle : keySet) {
                    this.pane.addTab(tabTitle, new JLabel(tabTitle));
                }
            }
            this.pane.addChangeListener(this.tabbedPaneChangeListener);
            this.handleTabSwitched(null, null);
        }
    }

    private void handleTabSwitched(String searchText, List<String> matchedKeywords) {
        int selectedIndex = this.pane.getSelectedIndex();
        if (selectedIndex != -1) {
            String tabTitle = this.pane.getTitleAt(selectedIndex);
            OptionsPanelController controller = this.tabTitle2controller.get(tabTitle);
            if (this.pane.getSelectedComponent() instanceof JLabel) {
                JComponent comp;
                if (controller == null) {
                    AdvancedOption advancedOption = this.tabTitle2Option.get(tabTitle);
                    if (advancedOption == null) {
                        LOGGER.log(Level.INFO, "AdvancedOption for {0} is not present.", tabTitle);
                        return;
                    }
                    controller = advancedOption.create();
                    this.tabTitle2controller.put(tabTitle, controller);
                    comp = controller.getComponent(this.masterLookup);
                    for (PropertyChangeListener pcl : this.pcs.getPropertyChangeListeners()) {
                        controller.addPropertyChangeListener(pcl);
                    }
                } else {
                    comp = controller.getComponent(this.masterLookup);
                }
                if (null == comp.getBorder()) {
                    comp.setBorder(BorderFactory.createEmptyBorder(11, 11, 11, 11));
                }
                JScrollPane scroll = new JScrollPane(comp);
                scroll.setBorder(BorderFactory.createEmptyBorder());
                scroll.setOpaque(false);
                scroll.getViewport().setOpaque(false);
                scroll.getVerticalScrollBar().setUnitIncrement(16);
                scroll.getHorizontalScrollBar().setUnitIncrement(16);
                this.pane.setComponentAt(selectedIndex, scroll);
                controller.update();
                controller.isValid();
            }
            if (searchText != null && matchedKeywords != null) {
                controller.handleSuccessfulSearch(searchText, matchedKeywords);
            }
            this.pcs.firePropertyChange("helpCtx", null, null);
        }
    }

    @Override
    public HelpCtx getHelpCtx() {
        if (this.pane != null && this.pane.getSelectedIndex() != -1) {
            return this.getHelpCtx(this.pane.getTitleAt(this.pane.getSelectedIndex()));
        }
        return null;
    }

    private HelpCtx getHelpCtx(String tabTitle) {
        OptionsPanelController controller = this.tabTitle2controller.get(tabTitle);
        if (controller != null) {
            return controller.getHelpCtx();
        }
        return new HelpCtx("netbeans.optionsDialog.java");
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
        for (OptionsPanelController c : this.getControllers()) {
            c.addPropertyChangeListener(l);
        }
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
        for (OptionsPanelController c : this.getControllers()) {
            c.removePropertyChangeListener(l);
        }
    }

    @Override
    protected void setCurrentSubcategory(String path) {
        String currentTabTitle;
        String subcategoryID = path.indexOf(47) == -1 ? path : path.substring(0, path.indexOf(47));
        String subcategorySubpath = path.indexOf(47) == -1 ? null : path.substring(path.indexOf(47) + 1);
        LOGGER.fine("Set current subcategory: " + path);
        if (!this.id2tabTitle.containsKey(subcategoryID)) {
            LOGGER.warning("Subcategory " + subcategoryID + " not found.");
            return;
        }
        String newTabTitle = this.id2tabTitle.get(subcategoryID);
        String string = currentTabTitle = this.pane.getSelectedIndex() != -1 ? this.pane.getTitleAt(this.pane.getSelectedIndex()) : null;
        if (!newTabTitle.equals(currentTabTitle)) {
            for (int i = 0; i < this.pane.getTabCount(); ++i) {
                if (!this.pane.getTitleAt(i).equals(newTabTitle)) continue;
                this.pane.setSelectedIndex(i);
                break;
            }
        }
        if (subcategorySubpath != null) {
            OptionsPanelControllerAccessor.getDefault().setCurrentSubcategory(this.tabTitle2controller.get(newTabTitle), subcategorySubpath);
        }
    }

    @Override
    public Lookup getLookup() {
        ArrayList<Lookup> lookups = new ArrayList<Lookup>();
        for (OptionsPanelController controller : this.getControllers()) {
            Lookup lookup = controller.getLookup();
            if (lookup != null && lookup != Lookup.EMPTY) {
                lookups.add(lookup);
            }
            if (lookup != null) continue;
            LOGGER.log(Level.WARNING, "{0}.getLookup() should never return null. Please, see Bug #194736.", controller.getClass().getName());
            throw new NullPointerException(controller.getClass().getName() + ".getLookup() should never return null. Please, see Bug #194736.");
        }
        if (lookups.isEmpty()) {
            return Lookup.EMPTY;
        }
        return new ProxyLookup(lookups.toArray((T[])new Lookup[lookups.size()]));
    }

    private Collection<OptionsPanelController> getControllers() {
        return this.tabTitle2controller.values();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void readPanels() {
        Lookup lookup = Lookups.forPath((String)this.tabFolder);
        this.options = lookup.lookup(new Lookup.Template(AdvancedOption.class));
        this.tabTitle2controller = new HashMap<String, OptionsPanelController>();
        this.id2tabTitle = new HashMap<String, String>();
        Map<String, AdvancedOption> map = this.tabTitle2Option;
        synchronized (map) {
            for (Lookup.Item item : this.options.allItems()) {
                AdvancedOption option = (AdvancedOption)item.getInstance();
                String displayName = option.getDisplayName();
                if (displayName != null) {
                    this.tabTitle2Option.put(displayName, option);
                    String id = item.getId().substring(item.getId().lastIndexOf(47) + 1);
                    this.id2tabTitle.put(id, displayName);
                    continue;
                }
                LOGGER.log(Level.WARNING, "Display name not defined: {0}", item.toString());
            }
        }
    }

}

