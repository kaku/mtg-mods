/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CategoryButton_AccessibleDescription(Object name_of_the_category_button) {
        return NbBundle.getMessage(Bundle.class, (String)"CategoryButton_AccessibleDescription", (Object)name_of_the_category_button);
    }

    static String Filter_Textfield_Hint(Object shortcut_to_access_the_search_text_field) {
        return NbBundle.getMessage(Bundle.class, (String)"Filter_Textfield_Hint", (Object)shortcut_to_access_the_search_text_field);
    }

    static String Filter_Textfield_Tooltip() {
        return NbBundle.getMessage(Bundle.class, (String)"Filter_Textfield_Tooltip");
    }

    static String Loading_HelpCtx_Lengthy_Operation() {
        return NbBundle.getMessage(Bundle.class, (String)"Loading_HelpCtx_Lengthy_Operation");
    }

    static String Saving_Options_Lengthy_Operation() {
        return NbBundle.getMessage(Bundle.class, (String)"Saving_Options_Lengthy_Operation");
    }

    static String Saving_Options_Lengthy_Operation_Title() {
        return NbBundle.getMessage(Bundle.class, (String)"Saving_Options_Lengthy_Operation_Title");
    }

    private void Bundle() {
    }
}

