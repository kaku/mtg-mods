/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.quicksearch.SearchProvider
 *  org.netbeans.spi.quicksearch.SearchRequest
 *  org.netbeans.spi.quicksearch.SearchResponse
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Result
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.options;

import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.options.OptionsDisplayer;
import org.netbeans.modules.options.AdvancedOptionImpl;
import org.netbeans.modules.options.CategoryModel;
import org.netbeans.modules.options.OptionsCategoryImpl;
import org.netbeans.modules.options.OptionsDisplayerImpl;
import org.netbeans.spi.options.AdvancedOption;
import org.netbeans.spi.options.OptionsCategory;
import org.netbeans.spi.quicksearch.SearchProvider;
import org.netbeans.spi.quicksearch.SearchRequest;
import org.netbeans.spi.quicksearch.SearchResponse;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

public class QuickSearchProvider
implements SearchProvider {
    private HashMap<String, Set<String>> path2keywords = new HashMap();

    public void evaluate(SearchRequest request, SearchResponse response) {
        this.readKeywordsFromNewAnnotation();
        for (Lookup.Item<OptionsCategory> entry : this.getODCategories()) {
            for (Map.Entry<String, Set<String>> kw : this.getKeywords(entry).entrySet()) {
                for (String keywords : kw.getValue()) {
                    for (String keyword : Arrays.asList(keywords.split(","))) {
                        if (keyword.toLowerCase().indexOf(request.getText().toLowerCase()) <= -1) continue;
                        String path = entry.getId().substring(entry.getId().indexOf("/") + 1);
                        if (kw.getKey().contains("/")) {
                            path = path + kw.getKey().substring(kw.getKey().indexOf("/"));
                        }
                        if (response.addResult((Runnable)new OpenOption(path), keyword)) continue;
                        return;
                    }
                }
            }
        }
    }

    private Map<String, Set<String>> getKeywords(Lookup.Item<OptionsCategory> it) {
        OptionsCategory category = (OptionsCategory)it.getInstance();
        String categoryID = it.getId();
        HashMap<String, Set<String>> kws = new HashMap<String, Set<String>>();
        if (category != null && category instanceof OptionsCategoryImpl) {
            Set<String> categoryKeywords = ((OptionsCategoryImpl)category).getKeywordsByCategory();
            String categoryPath = categoryID.substring(categoryID.indexOf(47) + 1);
            HashMap<String, Set<String>> mergedMap = new HashMap<String, Set<String>>();
            mergedMap.put(categoryPath, this.mergeKeywords(categoryPath, categoryKeywords));
            kws.putAll(mergedMap);
        }
        Lookup lkp = Lookups.forPath((String)categoryID);
        Lookup.Result lkpResult = lkp.lookupResult(AdvancedOption.class);
        for (Lookup.Item item : lkpResult.allItems()) {
            AdvancedOption option;
            if (!item.getId().substring(0, item.getId().lastIndexOf(47)).equals(categoryID) || !((option = (AdvancedOption)item.getInstance()) instanceof AdvancedOptionImpl)) continue;
            String subCategoryID = item.getId();
            Set<String> subCategoryKeywords = ((AdvancedOptionImpl)option).getKeywordsByCategory();
            String subCategoryPath = subCategoryID.substring(subCategoryID.indexOf(47) + 1);
            HashMap<String, Set<String>> mergedMap = new HashMap<String, Set<String>>();
            String optionPath = subCategoryPath.substring(0, subCategoryPath.indexOf("/") + 1).concat(((AdvancedOptionImpl)option).getDisplayName());
            mergedMap.put(subCategoryPath, this.mergeKeywords(optionPath, subCategoryKeywords));
            kws.putAll(mergedMap);
        }
        return kws;
    }

    private void readKeywordsFromNewAnnotation() {
        FileObject keywordsFOs = FileUtil.getConfigRoot().getFileObject(CategoryModel.OD_LAYER_KEYWORDS_FOLDER_NAME);
        for (FileObject keywordsFO : keywordsFOs.getChildren()) {
            String location = keywordsFO.getAttribute("location").toString();
            String tabTitle = keywordsFO.getAttribute("tabTitle").toString();
            HashSet<String> keywords = new HashSet<String>();
            Enumeration attributes = keywordsFO.getAttributes();
            while (attributes.hasMoreElements()) {
                String attribute = (String)attributes.nextElement();
                if (!attribute.startsWith("keywords")) continue;
                String word = keywordsFO.getAttribute(attribute).toString();
                for (String s : word.split(",")) {
                    keywords.add(s.trim());
                }
            }
            String path = location.concat("/").concat(tabTitle);
            Set<String> keywordsFromPath = this.path2keywords.get(path);
            if (keywordsFromPath != null) {
                for (String keyword : keywordsFromPath) {
                    if (keywords.contains(keyword)) continue;
                    keywords.add(keyword);
                }
            }
            this.path2keywords.put(path, keywords);
        }
    }

    private Set<String> mergeKeywords(String path, Set<String> initialKeywords) {
        Set<String> mergedKeywords = this.path2keywords.get(path);
        if (mergedKeywords == null) {
            return initialKeywords;
        }
        HashSet<String> toAdd = new HashSet<String>();
        for (String keyword : initialKeywords) {
            for (String s : keyword.split(",")) {
                if (mergedKeywords.contains(s)) continue;
                toAdd.add(s);
            }
        }
        mergedKeywords.addAll(toAdd);
        this.path2keywords.put(path, mergedKeywords);
        return mergedKeywords;
    }

    private Iterable<? extends Lookup.Item<OptionsCategory>> getODCategories() {
        Lookup lookup = Lookups.forPath((String)"OptionsDialog");
        Lookup.Result result = lookup.lookupResult(OptionsCategory.class);
        return result.allItems();
    }

    private class OpenOption
    implements Runnable {
        private String path;

        OpenOption(String path) {
            this.path = path;
        }

        @Override
        public void run() {
            if (!OptionsDisplayer.getDefault().open(this.path)) {
                OptionsDisplayerImpl.selectCategory(this.path);
            }
        }
    }

}

