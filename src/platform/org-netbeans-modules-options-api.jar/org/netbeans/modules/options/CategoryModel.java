/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.modules.options;

import java.beans.PropertyChangeListener;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.JComponent;
import org.netbeans.modules.options.OptionsDisplayerImpl;
import org.netbeans.modules.options.OptionsPanelControllerAccessor;
import org.netbeans.spi.options.OptionsCategory;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.RequestProcessor;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

public final class CategoryModel
implements LookupListener {
    private static final RequestProcessor RP = new RequestProcessor("org.netbeans.modules.options.CategoryModel");
    private static Reference<CategoryModel> INSTANCE = new WeakReference<Object>(null);
    private static String currentCategoryID = null;
    private String highlitedCategoryID = null;
    private boolean categoriesValid = true;
    private final Map<String, Category> id2Category = Collections.synchronizedMap(new LinkedHashMap());
    private MasterLookup masterLookup;
    static final String OD_LAYER_FOLDER_NAME = "OptionsDialog";
    static final String OD_LAYER_KEYWORDS_FOLDER_NAME = "OptionsDialog".concat("/Keywords");
    private Lookup.Result<OptionsCategory> result;
    private final RequestProcessor.Task masterLookupTask;
    private final RequestProcessor.Task categoryTask;

    Set<Map.Entry<String, Category>> getCategories() {
        return this.id2Category.entrySet();
    }

    private CategoryModel() {
        this.masterLookupTask = RP.create(new Runnable(){

            @Override
            public void run() {
                String[] categoryIDs = CategoryModel.this.getCategoryIDs();
                ArrayList<Lookup> all = new ArrayList<Lookup>();
                for (int i = 0; i < categoryIDs.length; ++i) {
                    Category item = CategoryModel.this.getCategory(categoryIDs[i]);
                    Lookup lkp = item.getLookup();
                    assert (lkp != null);
                    if (lkp == Lookup.EMPTY) continue;
                    all.add(lkp);
                }
                CategoryModel.this.getMasterLookup().setLookups(all);
                CategoryModel.this.addLookupListener();
            }
        }, true);
        this.categoryTask = RP.create(new Runnable(){

            @Override
            public void run() {
                Map all = CategoryModel.this.loadOptionsCategories();
                LinkedHashMap<String, Category> temp = new LinkedHashMap<String, Category>();
                for (Map.Entry entry : all.entrySet()) {
                    OptionsCategory oc = (OptionsCategory)entry.getValue();
                    String id = (String)entry.getKey();
                    Category cat = new Category(id, oc);
                    temp.put(cat.getID(), cat);
                }
                CategoryModel.this.id2Category.clear();
                CategoryModel.this.id2Category.putAll(temp);
                CategoryModel.this.masterLookupTask.schedule(0);
            }
        }, true);
        if (INSTANCE.get() == null && this.categoryTask.isFinished()) {
            this.categoryTask.schedule(0);
        }
    }

    public static synchronized CategoryModel getInstance() {
        CategoryModel retval = INSTANCE.get();
        if (retval == null) {
            retval = new CategoryModel();
            INSTANCE = new WeakReference<CategoryModel>(retval);
        }
        return retval;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    boolean needsReinit() {
        Class<CategoryModel> class_ = CategoryModel.class;
        synchronized (CategoryModel.class) {
            // ** MonitorExit[var1_1] (shouldn't be in output)
            return !this.categoriesValid;
        }
    }

    boolean isInitialized() {
        return this.categoryTask.isFinished();
    }

    boolean isLookupInitialized() {
        return this.masterLookupTask.isFinished();
    }

    void waitForInitialization() {
        this.categoryTask.waitFinished();
    }

    public String getCurrentCategoryID() {
        return this.verifyCategoryID(currentCategoryID);
    }

    public void setCurrentCategoryID(String categoryID) {
        currentCategoryID = this.verifyCategoryID(categoryID);
    }

    String getHighlitedCategoryID() {
        return this.verifyCategoryID(this.highlitedCategoryID);
    }

    private String verifyCategoryID(String categoryID) {
        String[] categoryIDs;
        String retval;
        String string = retval = this.findCurrentCategoryID(categoryID) != -1 ? categoryID : null;
        if (retval == null && (categoryIDs = this.getCategoryIDs()).length > 0) {
            retval = categoryID = categoryIDs[0];
        }
        return retval;
    }

    private int findCurrentCategoryID(String categoryID) {
        return categoryID == null ? -1 : Arrays.asList(this.getCategoryIDs()).indexOf(categoryID);
    }

    public String[] getCategoryIDs() {
        this.categoryTask.waitFinished();
        Set<String> keys = this.id2Category.keySet();
        return keys.toArray(new String[keys.size()]);
    }

    Category getCurrent() {
        String categoryID = this.getCurrentCategoryID();
        return categoryID == null ? null : this.getCategory(categoryID);
    }

    void setCurrent(Category item) {
        item.setCurrent();
    }

    void setHighlited(Category item, boolean highlited) {
        item.setHighlited(highlited);
    }

    HelpCtx getHelpCtx() {
        Category category = this.getCurrent();
        return category == null ? null : category.getHelpCtx();
    }

    void update(PropertyChangeListener l, boolean force) {
        String[] categoryIDs = this.getCategoryIDs();
        for (int i = 0; i < categoryIDs.length; ++i) {
            Category item = this.getCategory(categoryIDs[i]);
            item.update(l, force);
        }
    }

    void save() {
        String[] categoryIDs = this.getCategoryIDs();
        for (int i = 0; i < categoryIDs.length; ++i) {
            Category item = this.getCategory(categoryIDs[i]);
            item.applyChanges();
        }
    }

    void cancel() {
        String[] categoryIDs = this.getCategoryIDs();
        for (int i = 0; i < categoryIDs.length; ++i) {
            Category item = this.getCategory(categoryIDs[i]);
            item.cancel();
        }
    }

    boolean dataValid() {
        boolean retval = true;
        String[] categoryIDs = this.getCategoryIDs();
        for (int i = 0; retval && i < categoryIDs.length; ++i) {
            Category item = this.getCategory(categoryIDs[i]);
            retval = item.isValid();
        }
        return retval;
    }

    boolean isChanged() {
        boolean retval = false;
        String[] categoryIDs = this.getCategoryIDs();
        for (int i = 0; !retval && i < categoryIDs.length; ++i) {
            Category item = this.getCategory(categoryIDs[i]);
            retval = item.isChanged();
        }
        return retval;
    }

    Category getNextCategory() {
        int idx = this.findCurrentCategoryID(this.getCurrentCategoryID());
        String[] categoryIDs = this.getCategoryIDs();
        String nextId = "";
        nextId = idx >= 0 && idx < categoryIDs.length && categoryIDs.length > 0 ? (idx + 1 < categoryIDs.length ? categoryIDs[idx + 1] : categoryIDs[0]) : null;
        return nextId != null ? this.getCategory(nextId) : null;
    }

    Category getPreviousCategory() {
        int idx = this.findCurrentCategoryID(this.getCurrentCategoryID());
        String[] categoryIDs = this.getCategoryIDs();
        String previousId = "";
        previousId = idx >= 0 && idx < categoryIDs.length && categoryIDs.length > 0 ? (idx - 1 >= 0 ? categoryIDs[idx - 1] : categoryIDs[categoryIDs.length - 1]) : null;
        return previousId != null ? this.getCategory(previousId) : null;
    }

    Category getCategory(String categoryID) {
        this.categoryTask.waitFinished();
        return this.id2Category.get(categoryID);
    }

    private MasterLookup getMasterLookup() {
        if (this.masterLookup == null) {
            this.masterLookup = new MasterLookup();
        }
        return this.masterLookup;
    }

    private Map<String, OptionsCategory> loadOptionsCategories() {
        Lookup lookup = Lookups.forPath((String)"OptionsDialog");
        if (this.result != null) {
            this.result.removeLookupListener((LookupListener)this);
        }
        this.result = lookup.lookup(new Lookup.Template(OptionsCategory.class));
        LinkedHashMap<String, Object> m = new LinkedHashMap<String, Object>();
        for (Lookup.Item item : this.result.allItems()) {
            m.put(item.getId().substring("OptionsDialog".length() + 1), item.getInstance());
        }
        return Collections.unmodifiableMap(m);
    }

    private void addLookupListener() {
        this.result.addLookupListener((LookupListener)this);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void resultChanged(LookupEvent ev) {
        Class<CategoryModel> class_ = CategoryModel.class;
        synchronized (CategoryModel.class) {
            this.categoriesValid = false;
            OptionsDisplayerImpl.lookupListener.resultChanged(ev);
            INSTANCE = new WeakReference<Object>(null);
            // ** MonitorExit[var2_2] (shouldn't be in output)
            return;
        }
    }

    private class MasterLookup
    extends ProxyLookup {
        private MasterLookup() {
        }

        private void setLookups(List<Lookup> lookups) {
            this.setLookups(lookups.toArray((T[])new Lookup[lookups.size()]));
        }

        protected void beforeLookup(Lookup.Template template) {
            super.beforeLookup(template);
            CategoryModel.this.masterLookupTask.waitFinished();
        }
    }

    final class Category {
        private OptionsCategory category;
        private OptionsPanelController controller;
        private boolean isUpdated;
        private JComponent component;
        private Lookup lookup;
        private final String id;

        private Category(String id, OptionsCategory category) {
            this.category = category;
            this.id = id;
        }

        boolean isCurrent() {
            return this.getID().equals(CategoryModel.this.getCurrentCategoryID());
        }

        boolean isHighlited() {
            return this.getID().equals(CategoryModel.this.getHighlitedCategoryID());
        }

        private void setCurrent() {
            CategoryModel.this.setCurrentCategoryID(this.getID());
        }

        public void setCurrentSubcategory(String subpath) {
            OptionsPanelControllerAccessor.getDefault().setCurrentSubcategory(this.create(), subpath);
        }

        private void setHighlited(boolean highlited) {
            if (highlited) {
                CategoryModel.this.highlitedCategoryID = this.getID();
            } else {
                CategoryModel.this.highlitedCategoryID = currentCategoryID;
            }
        }

        public Icon getIcon() {
            return this.category.getIcon();
        }

        public String getID() {
            return this.id;
        }

        public String getCategoryName() {
            return this.category.getCategoryName();
        }

        public void handleSuccessfulSearchInController(String searchText, List<String> matchedKeywords) {
            this.create().handleSuccessfulSearch(searchText, matchedKeywords);
        }

        private synchronized OptionsPanelController create() {
            if (this.controller == null) {
                this.controller = this.category.create();
            }
            return this.controller;
        }

        final void update(PropertyChangeListener l, boolean forceUpdate) {
            if (!this.isUpdated && !forceUpdate || this.isUpdated && forceUpdate) {
                this.isUpdated = true;
                this.getComponent();
                this.create().update();
                if (l != null) {
                    this.create().addPropertyChangeListener(l);
                }
            }
        }

        private void applyChanges() {
            if (this.isUpdated) {
                this.create().applyChanges();
            }
            this.isUpdated = false;
        }

        private void cancel() {
            if (this.isUpdated) {
                this.create().cancel();
            }
            this.isUpdated = false;
        }

        private boolean isValid() {
            boolean retval = true;
            if (this.isUpdated) {
                retval = this.create().isValid();
            }
            return retval;
        }

        private boolean isChanged() {
            boolean retval = false;
            if (this.isUpdated) {
                retval = this.create().isChanged();
            }
            return retval;
        }

        public JComponent getComponent() {
            if (this.component == null) {
                this.component = this.create().getComponent((Lookup)CategoryModel.this.getMasterLookup());
            }
            return this.component;
        }

        private HelpCtx getHelpCtx() {
            return this.create().getHelpCtx();
        }

        private Lookup getLookup() {
            if (this.lookup == null) {
                this.lookup = this.create().getLookup();
            }
            return this.lookup;
        }
    }

}

