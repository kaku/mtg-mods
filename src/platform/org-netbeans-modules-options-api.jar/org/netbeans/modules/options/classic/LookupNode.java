/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.actions.FileSystemAction
 *  org.openide.actions.MoveDownAction
 *  org.openide.actions.MoveUpAction
 *  org.openide.actions.NewTemplateAction
 *  org.openide.actions.NewTemplateAction$Cookie
 *  org.openide.actions.PasteAction
 *  org.openide.actions.PropertiesAction
 *  org.openide.actions.ReorderAction
 *  org.openide.actions.ToolsAction
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataFolder$FolderNode
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataShadow
 *  org.openide.loaders.TemplateWizard
 *  org.openide.loaders.XMLDataObject
 *  org.openide.nodes.Children
 *  org.openide.nodes.CookieSet
 *  org.openide.nodes.FilterNode
 *  org.openide.nodes.FilterNode$Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.nodes.Sheet
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.SystemAction
 */
package org.netbeans.modules.options.classic;

import java.io.IOException;
import java.util.Enumeration;
import javax.swing.Action;
import org.openide.actions.FileSystemAction;
import org.openide.actions.MoveDownAction;
import org.openide.actions.MoveUpAction;
import org.openide.actions.NewTemplateAction;
import org.openide.actions.PasteAction;
import org.openide.actions.PropertiesAction;
import org.openide.actions.ReorderAction;
import org.openide.actions.ToolsAction;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataShadow;
import org.openide.loaders.TemplateWizard;
import org.openide.loaders.XMLDataObject;
import org.openide.nodes.Children;
import org.openide.nodes.CookieSet;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.HelpCtx;
import org.openide.util.actions.SystemAction;

public class LookupNode
extends DataFolder.FolderNode
implements NewTemplateAction.Cookie {
    private static final String EA_HIDDEN = "hidden";
    private static final String EA_HELPCTX = "helpID";
    private static final HelpCtx INSTANCE_DEFAULT_HELP = new HelpCtx("org.openide.loaders.InstanceDataObject");
    private static final HelpCtx FOLDER_DEFAULT_HELP = new HelpCtx("org.openide.loaders.DataFolder");
    private static final String PREFIX_SETTING_CATEGORIES = "UI";

    public LookupNode(DataFolder folder) {
        DataFolder dataFolder = folder;
        dataFolder.getClass();
        super(dataFolder, (Children)new Ch(folder));
        this.getCookieSet().add((Node.Cookie)this);
    }

    private boolean isUISettingCategoryNode() {
        DataFolder df = (DataFolder)super.getCookie(DataFolder.class);
        if (df != null) {
            String name = df.getPrimaryFile().getPath();
            return name.startsWith("UI");
        }
        return false;
    }

    public HelpCtx getHelpCtx() {
        Object o = this.getDataObject().getPrimaryFile().getAttribute("helpID");
        if (o != null) {
            return new HelpCtx(o.toString());
        }
        HelpCtx ctx = this.getDataObject().getHelpCtx();
        if (ctx != null && ctx != HelpCtx.DEFAULT_HELP && !FOLDER_DEFAULT_HELP.equals((Object)ctx)) {
            return ctx;
        }
        Node n = this.getParentNode();
        if (n != null) {
            ctx = n.getHelpCtx();
        }
        return ctx;
    }

    public final Action[] getActions(boolean context) {
        if (this.isUISettingCategoryNode()) {
            return new Action[0];
        }
        return new Action[]{SystemAction.get(FileSystemAction.class), null, SystemAction.get(PasteAction.class), null, SystemAction.get(MoveUpAction.class), SystemAction.get(MoveDownAction.class), SystemAction.get(ReorderAction.class), null, SystemAction.get(NewTemplateAction.class), null, SystemAction.get(ToolsAction.class), SystemAction.get(PropertiesAction.class)};
    }

    public final <T extends Node.Cookie> T getCookie(Class<T> type) {
        if (this.isUISettingCategoryNode()) {
            return null;
        }
        return (T)super.getCookie(type);
    }

    public final TemplateWizard getTemplateWizard() {
        TemplateWizard templateWizard = this.createWizard();
        templateWizard.setTemplatesFolder(LookupNode.findFolder(this.root(), this.findName(), true));
        templateWizard.setTargetFolder(LookupNode.findFolder(this.root(), this.findName(), false));
        return templateWizard;
    }

    protected TemplateWizard createWizard() {
        return new TemplateWizard();
    }

    protected LookupNode createChild(DataFolder folder) {
        return new LookupNode(folder);
    }

    protected Node createChild(Node node) {
        return node.cloneNode();
    }

    protected String root() {
        return "Services";
    }

    private static String prefTemplates(String root) {
        return "Templates/" + root;
    }

    private static String prefObjects(String root) {
        return root;
    }

    private String findName() {
        DataFolder df = this.getCookie(DataFolder.class);
        if (df == null) {
            return "";
        }
        String name = df.getPrimaryFile().getPath();
        if (name.startsWith(LookupNode.prefObjects(this.root()))) {
            name = name.substring(LookupNode.prefObjects(this.root()).length());
        }
        return name;
    }

    static DataFolder findFolder(String root, String name, boolean template) {
        try {
            name = template ? "" + '/' + LookupNode.prefTemplates(root) + name : "" + '/' + LookupNode.prefObjects(root) + name;
            FileObject fo = FileUtil.getConfigFile((String)name);
            if (fo == null && template) {
                name = LookupNode.prefTemplates(root);
            }
            if (fo == null) {
                fo = FileUtil.createFolder((FileObject)FileUtil.getConfigRoot(), (String)name);
            }
            return DataFolder.findFolder((FileObject)fo);
        }
        catch (IOException ex) {
            throw (IllegalStateException)new IllegalStateException(ex.toString()).initCause(ex);
        }
    }

    public final void refreshKey(Node node) {
        ((Ch)this.getChildren()).refreshKey0(node);
    }

    public boolean canDestroy() {
        return false;
    }

    public boolean canCut() {
        return false;
    }

    public boolean canCopy() {
        return false;
    }

    protected Sheet createSheet() {
        return new Sheet();
    }

    public boolean canRename() {
        return false;
    }

    public Node cloneNode() {
        return new LookupNode((DataFolder)super.getCookie(DataFolder.class));
    }

    private static final class Ch
    extends FilterNode.Children {
        public Ch(DataFolder folder) {
            super(folder.getNodeDelegate());
        }

        void refreshKey0(Node node) {
            this.refreshKey((Object)node);
        }

        protected Node[] createNodes(Node node) {
            DataObject obj = (DataObject)node.getCookie(DataObject.class);
            if (obj != null && Boolean.TRUE.equals(obj.getPrimaryFile().getAttribute("hidden"))) {
                return new Node[0];
            }
            LookupNode parent = (LookupNode)this.getNode();
            if (obj != null) {
                if (obj instanceof DataFolder && node.equals((Object)obj.getNodeDelegate())) {
                    node = parent.createChild((DataFolder)obj);
                    return new Node[]{node};
                }
                if (obj instanceof DataShadow) {
                    DataObject orig = ((DataShadow)obj).getOriginal();
                    FileObject fo = orig.getPrimaryFile();
                    if (fo.isFolder() && !fo.getChildren(false).hasMoreElements()) {
                        return null;
                    }
                    if (orig instanceof DataFolder) {
                        return new Node[]{parent.createChild((DataFolder)orig)};
                    }
                    obj = orig;
                    node = orig.getNodeDelegate();
                }
                node = new Leaf((Node)node, obj, (Node)parent);
            }
            node = parent.createChild((Node)node);
            return new Node[]{node};
        }
    }

    private static final class Leaf
    extends FilterNode {
        DataObject data;
        Node parent;

        Leaf(Node node, DataObject data, Node parent) {
            super(node, data instanceof XMLDataObject || node.isLeaf() ? FilterNode.Children.LEAF : new FilterNode.Children(node));
            this.data = data;
            this.parent = parent;
        }

        public boolean equals(Object o) {
            return this == o || this.getOriginal().equals(o) || o != null && o.equals((Object)this.getOriginal());
        }

        public int hashCode() {
            return this.getOriginal().hashCode();
        }

        public HelpCtx getHelpCtx() {
            Object o = this.data.getPrimaryFile().getAttribute("helpID");
            if (o != null) {
                return new HelpCtx(o.toString());
            }
            HelpCtx ctx = this.getOriginal().getHelpCtx();
            if (ctx != null && ctx != HelpCtx.DEFAULT_HELP && !INSTANCE_DEFAULT_HELP.equals((Object)ctx)) {
                return ctx;
            }
            Node n = this.getParentNode();
            if (n == null) {
                n = this.parent;
            }
            if (n != null) {
                ctx = n.getHelpCtx();
            }
            return ctx;
        }

        public Action getPreferredAction() {
            return null;
        }
    }

}

