/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.beaninfo.editors.ListImageEditor
 *  org.openide.explorer.propertysheet.PropertyEnv
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.classic;

import java.beans.FeatureDescriptor;
import java.lang.reflect.InvocationTargetException;
import org.netbeans.beaninfo.editors.ListImageEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

class FileStateEditor
extends ListImageEditor {
    private String action_define = NbBundle.getMessage(FileStateEditor.class, (String)"LBL_action_define");
    private String action_revert = NbBundle.getMessage(FileStateEditor.class, (String)"LBL_action_revert");
    private String action_delete = NbBundle.getMessage(FileStateEditor.class, (String)"LBL_action_delete");
    private Node.Property<?> prop = null;

    public void attachEnv(PropertyEnv env) {
        super.attachEnv(env);
        try {
            this.prop = (Node.Property)env.getFeatureDescriptor();
        }
        catch (ClassCastException cce) {
            ClassCastException cce2 = new ClassCastException("Expected a Node.Property but got a " + env.getFeatureDescriptor() + " descriptor " + env.getFeatureDescriptor().getClass().getName());
            throw cce2;
        }
    }

    public String getAsText() {
        return null;
    }

    public void setAsText(String str) throws IllegalArgumentException {
        try {
            Integer value = null;
            if (this.action_define.equals(str)) {
                value = 1;
            }
            if (this.action_revert.equals(str)) {
                value = 2;
            }
            if (this.action_delete.equals(str)) {
                value = 3;
            }
            if (value != null) {
                this.doSetValue(this.prop, value);
                super.setValue((Object)value);
            }
        }
        catch (IllegalAccessException e) {
        }
        catch (InvocationTargetException e) {
            // empty catch block
        }
    }

    private <T> void doSetValue(Node.Property<T> prop, Object value) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        prop.setValue(prop.getValueType().cast(value));
    }

    public String[] getTags() {
        Integer val = (Integer)this.getValue();
        if ("Modules-Layer".equals(this.prop.getName())) {
            return new String[]{this.action_revert};
        }
        if (val != null && val == 1) {
            return new String[]{this.action_define, this.action_revert, this.action_delete};
        }
        return new String[]{this.action_define};
    }
}

