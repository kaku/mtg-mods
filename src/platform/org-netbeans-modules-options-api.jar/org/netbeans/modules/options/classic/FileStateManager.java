/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.startup.layers.SessionManager
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.options.classic;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import org.netbeans.core.startup.layers.SessionManager;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.util.WeakListeners;

final class FileStateManager {
    public static final int LAYER_SESSION = 1;
    public static final int LAYER_MODULES = 2;
    public static final int FSTATE_DEFINED = 0;
    public static final int FSTATE_IGNORED = 1;
    public static final int FSTATE_INHERITED = 2;
    public static final int FSTATE_UNDEFINED = 3;
    private static FileStateManager manager = null;
    private WeakHashMap<FileObject, FileInfo> info = new WeakHashMap();
    private static final int LAYERS_COUNT = 3;
    private FileSystem[] layers = new FileSystem[3];
    private HashMap<FileStatusListener, LinkedList<FileObject>> listeners = new HashMap(10);
    private PropertyChangeListener propL = null;

    public static synchronized FileStateManager getDefault() {
        if (manager == null) {
            manager = new FileStateManager();
        }
        return manager;
    }

    private FileStateManager() {
        this.getLayers();
        this.propL = new PropL();
        SessionManager.getDefault().addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.propL, (Object)SessionManager.getDefault()));
    }

    public void define(final FileObject mfo, int layer, boolean revert) throws IOException {
        if (0 == this.getFileState(mfo, layer)) {
            return;
        }
        FileSystem fsLayer = this.getLayer(layer);
        if (fsLayer == null) {
            throw new IllegalArgumentException("Invalid layer " + layer);
        }
        FileObject fo = fsLayer.findResource(mfo.getPath());
        if (fo != null && !revert) {
            this.deleteImpl(mfo, fsLayer);
            fo = null;
        }
        if (fo == null) {
            String parent = mfo.getParent().getPath();
            final FileObject fparent = FileUtil.createFolder((FileObject)fsLayer.getRoot(), (String)parent);
            fparent.getFileSystem().runAtomicAction(new FileSystem.AtomicAction(){

                public void run() throws IOException {
                    mfo.copy(fparent, mfo.getName(), mfo.getExt());
                }
            });
        }
        for (int i = 0; i < layer; ++i) {
            FileSystem fsl = this.getLayer(i);
            if (fsl == null) continue;
            this.deleteImpl(mfo, fsl);
        }
    }

    public void delete(FileObject mfo, int layer) throws IOException {
        FileSystem fsLayer = this.getLayer(layer);
        if (fsLayer == null) {
            throw new IllegalArgumentException("Invalid layer " + layer);
        }
        this.deleteImpl(mfo, fsLayer);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public int getFileState(FileObject mfo, int layer) {
        FileSystem fs = null;
        FileInfo finf = null;
        try {
            fs = mfo.getFileSystem();
        }
        catch (FileStateInvalidException e2) {
            // empty catch block
        }
        if (fs == null || !fs.isDefault()) {
            throw new IllegalArgumentException("FileObject has to be from DefaultFileSystem - " + (Object)mfo);
        }
        WeakHashMap<FileObject, FileInfo> e2 = this.info;
        synchronized (e2) {
            finf = this.info.get((Object)mfo);
            if (null == finf) {
                finf = new FileInfo(mfo);
                this.info.put(mfo, finf);
            }
        }
        return finf.getState(layer);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final void addFileStatusListener(FileStatusListener l, FileObject mfo) {
        HashMap<FileStatusListener, LinkedList<FileObject>> hashMap = this.listeners;
        synchronized (hashMap) {
            LinkedList lst = null;
            if (!this.listeners.containsKey(l)) {
                lst = new LinkedList();
                this.listeners.put(l, lst);
            } else {
                lst = this.listeners.get(l);
            }
            if (!lst.contains((Object)mfo)) {
                lst.add(mfo);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final void removeFileStatusListener(FileStatusListener l, FileObject mfo) {
        HashMap<FileStatusListener, LinkedList<FileObject>> hashMap = this.listeners;
        synchronized (hashMap) {
            if (mfo == null) {
                this.listeners.remove(l);
            } else {
                LinkedList<FileObject> lst = this.listeners.get(l);
                if (lst != null) {
                    lst.remove((Object)mfo);
                    if (lst.isEmpty()) {
                        this.listeners.remove(l);
                    }
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fireFileStatusChanged(FileObject mfo) {
        HashMap h = null;
        HashMap<FileStatusListener, LinkedList<FileObject>> hashMap = this.listeners;
        synchronized (hashMap) {
            h = (HashMap)this.listeners.clone();
        }
        for (Map.Entry entry : h.entrySet()) {
            FileStatusListener l = (FileStatusListener)entry.getKey();
            LinkedList lst = (LinkedList)entry.getValue();
            if (!lst.contains((Object)mfo)) continue;
            l.fileStatusChanged(mfo);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void deleteImpl(FileObject mfo, FileSystem fsLayer) throws IOException {
        FileObject fo = fsLayer.findResource(mfo.getPath());
        if (fo != null) {
            FileLock lock = null;
            try {
                lock = fo.lock();
                fo.delete(lock);
            }
            finally {
                if (lock != null) {
                    lock.releaseLock();
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void discard(FileObject mfo) {
        WeakHashMap<FileObject, FileInfo> weakHashMap = this.info;
        synchronized (weakHashMap) {
            this.info.remove((Object)mfo);
        }
    }

    private void getLayers() {
        this.layers[1] = SessionManager.getDefault().getLayer("session");
        this.layers[2] = SessionManager.getDefault().getLayer("install");
    }

    private FileSystem getLayer(int layer) {
        return this.layers[layer];
    }

    private class FileInfo
    extends FileChangeAdapter {
        private WeakReference<FileObject> file;
        private int[] state;
        private final Object LOCK;
        private FileObject[] notifiers;
        private FileChangeListener[] weakL;

        public FileInfo(FileObject mfo) {
            int i;
            this.file = null;
            this.state = new int[3];
            this.LOCK = new Object();
            this.notifiers = new FileObject[3];
            this.weakL = new FileChangeListener[3];
            this.file = new WeakReference<FileObject>(mfo);
            for (i = 0; i < 3; ++i) {
                this.state[i] = this.getStateImpl(mfo, i);
            }
            for (i = 0; i < 3; ++i) {
                this.attachNotifier(mfo, i);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void invalidate() {
            this.detachAllNotifiers();
            Object object = this.LOCK;
            synchronized (object) {
                for (int i = 0; i < 3; ++i) {
                    this.state[i] = 3;
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public int getState(int layer) {
            Object object = this.LOCK;
            synchronized (object) {
                return this.state[layer];
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void rescan(FileObject mfo) {
            boolean changed = false;
            Object object = this.LOCK;
            synchronized (object) {
                for (int i = 0; i < 3; ++i) {
                    int ns = this.getStateImpl(mfo, i);
                    if (this.state[i] == ns) continue;
                    this.state[i] = ns;
                    changed = true;
                }
            }
            if (changed) {
                FileStateManager.this.fireFileStatusChanged(mfo);
            }
        }

        private int getStateImpl(FileObject mfo, int layer) {
            int i;
            boolean above = false;
            boolean below = false;
            for (i = 0; i < layer; ++i) {
                if (!this.isOnLayer(mfo, i)) continue;
                above = true;
                break;
            }
            for (i = layer + 1; i < 3; ++i) {
                if (!this.isOnLayer(mfo, i)) continue;
                below = true;
                break;
            }
            if (this.isOnLayer(mfo, layer)) {
                return above ? 1 : 0;
            }
            return below && !above ? 2 : 3;
        }

        private boolean isOnLayer(FileObject mfo, int layer) {
            FileSystem fsLayer = FileStateManager.this.getLayer(layer);
            return fsLayer == null ? false : null != fsLayer.findResource(mfo.getPath());
        }

        private synchronized boolean attachNotifier(FileObject mfo, int layer) {
            FileSystem fsLayer = FileStateManager.this.getLayer(layer);
            String fn = mfo.getPath();
            FileObject fo = null;
            boolean isDelegate = true;
            if (fsLayer == null) {
                return false;
            }
            while (fn.length() > 0 && null == (fo = fsLayer.findResource(fn))) {
                int pos = fn.lastIndexOf(47);
                isDelegate = false;
                if (-1 == pos) break;
                fn = fn.substring(0, pos);
            }
            if (fo == null) {
                fo = fsLayer.getRoot();
            }
            if (fo != this.notifiers[layer]) {
                if (this.notifiers[layer] != null) {
                    this.notifiers[layer].removeFileChangeListener(this.weakL[layer]);
                }
                this.weakL[layer] = FileUtil.weakFileChangeListener((FileChangeListener)this, (Object)fo);
                fo.addFileChangeListener(this.weakL[layer]);
                this.notifiers[layer] = fo;
            }
            return isDelegate;
        }

        private synchronized void detachAllNotifiers() {
            for (int i = 0; i < 3; ++i) {
                if (this.notifiers[i] == null) continue;
                this.notifiers[i].removeFileChangeListener(this.weakL[i]);
                this.notifiers[i] = null;
                this.weakL[i] = null;
            }
        }

        private int layerOfFile(FileObject fo) {
            try {
                FileSystem fs = fo.getFileSystem();
                for (int i = 0; i < 3; ++i) {
                    if (!fs.equals((Object)FileStateManager.this.getLayer(i))) continue;
                    return i;
                }
            }
            catch (FileStateInvalidException e) {
                throw (IllegalStateException)new IllegalStateException("Invalid file - " + (Object)fo).initCause((Throwable)e);
            }
            return -1;
        }

        public void fileRenamed(FileRenameEvent fe) {
            FileObject mfo = this.file.get();
            if (mfo != null && mfo.isValid()) {
                FileStateManager.this.discard(mfo);
                FileStateManager.this.fireFileStatusChanged(mfo);
            } else {
                this.detachAllNotifiers();
            }
        }

        public void fileDataCreated(FileEvent fe) {
            FileObject mfo = this.file.get();
            if (mfo != null && mfo.isValid()) {
                String mfoname;
                String created = fe.getFile().getPath();
                if (created.equals(mfoname = mfo.getPath())) {
                    int layer = this.layerOfFile(fe.getFile());
                    if (-1 != layer) {
                        this.attachNotifier(mfo, layer);
                    }
                    this.rescan(mfo);
                }
            } else {
                this.detachAllNotifiers();
            }
        }

        public void fileFolderCreated(FileEvent fe) {
            FileObject mfo = this.file.get();
            if (mfo != null && mfo.isValid()) {
                int layer;
                String created = fe.getFile().getPath();
                String mfoname = mfo.getPath();
                if (mfoname.startsWith(created) && -1 != (layer = this.layerOfFile(fe.getFile())) && this.attachNotifier(mfo, layer)) {
                    this.rescan(mfo);
                }
            } else {
                this.detachAllNotifiers();
            }
        }

        public void fileDeleted(FileEvent fe) {
            FileObject mfo = this.file.get();
            if (mfo != null && mfo.isValid()) {
                String mfoname;
                String deleted = fe.getFile().getPath();
                if (deleted.equals(mfoname = mfo.getPath())) {
                    int layer = this.layerOfFile(fe.getFile());
                    if (-1 != layer) {
                        this.attachNotifier(mfo, layer);
                    }
                    this.rescan(mfo);
                }
            } else {
                this.detachAllNotifiers();
            }
        }
    }

    public static interface FileStatusListener {
        public void fileStatusChanged(FileObject var1);
    }

    private class PropL
    implements PropertyChangeListener {
        PropL() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("session_open".equals(evt.getPropertyName())) {
                FileObject[] mfos = null;
                WeakHashMap weakHashMap = FileStateManager.this.info;
                synchronized (weakHashMap) {
                    mfos = FileStateManager.this.info.keySet().toArray((T[])new FileObject[FileStateManager.this.info.size()]);
                    for (int i = 0; i < mfos.length; ++i) {
                        FileInfo finf = (FileInfo)((Object)FileStateManager.this.info.get((Object)mfos[i]));
                        if (finf == null) continue;
                        finf.invalidate();
                    }
                    FileStateManager.this.info.clear();
                    FileStateManager.this.getLayers();
                }
                for (int i = 0; i < mfos.length; ++i) {
                    FileStateManager.this.fireFileStatusChanged(mfos[i]);
                }
            }
        }
    }

}

