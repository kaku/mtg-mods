/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.startup.layers.SessionManager
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.awt.Mnemonics
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.InstanceCookie$Of
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerUtils
 *  org.openide.explorer.propertysheet.PropertySheetView
 *  org.openide.explorer.view.NodeTableModel
 *  org.openide.explorer.view.TreeTableView
 *  org.openide.explorer.view.TreeView
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataShadow
 *  org.openide.nodes.Children
 *  org.openide.nodes.FilterNode
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.nodes.Node$Handle
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.PropertySupport
 *  org.openide.nodes.PropertySupport$ReadOnly
 *  org.openide.util.HelpCtx
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.CallableSystemAction
 */
package org.netbeans.modules.options.classic;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.JTableHeader;
import org.netbeans.core.startup.layers.SessionManager;
import org.netbeans.modules.options.classic.InitPanel;
import org.netbeans.modules.options.classic.NbMainExplorer;
import org.netbeans.modules.options.classic.NbPlaces;
import org.netbeans.modules.options.classic.SettingChildren;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.awt.Mnemonics;
import org.openide.cookies.InstanceCookie;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.propertysheet.PropertySheetView;
import org.openide.explorer.view.NodeTableModel;
import org.openide.explorer.view.TreeTableView;
import org.openide.explorer.view.TreeView;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataShadow;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.util.HelpCtx;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.util.actions.CallableSystemAction;

public class OptionsAction
extends CallableSystemAction {
    private static final String HELP_ID = "org.netbeans.core.actions.OptionsAction";
    private Reference<Dialog> dialogWRef = new WeakReference<Object>(null);

    public OptionsAction() {
        this.putValue("noIconInMenu", (Object)Boolean.TRUE);
    }

    public void performAction() {
        final OptionsPanel optionPanel = OptionsPanel.singleton();
        Mutex.EVENT.readAccess(new Runnable(){

            @Override
            public void run() {
                Dialog dialog = (Dialog)OptionsAction.this.dialogWRef.get();
                if (dialog == null || !dialog.isShowing()) {
                    JButton closeButton = new JButton();
                    Mnemonics.setLocalizedText((AbstractButton)closeButton, (String)NbBundle.getMessage(OptionsAction.class, (String)"CTL_close_button"));
                    closeButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(OptionsAction.class, (String)"ACSD_close_button"));
                    String title = (String)OptionsAction.this.getValue("optionsDialogTitle");
                    DialogDescriptor dd = new DialogDescriptor((Object)InitPanel.getDefault(optionPanel), title == null ? optionPanel.getName() : title, false, new Object[]{closeButton}, (Object)closeButton, 0, OptionsAction.this.getHelpCtx(), null);
                    String name = (String)OptionsAction.this.getValue("additionalActionName");
                    if (name != null) {
                        ActionListener actionListener = (ActionListener)OptionsAction.this.getValue("additionalActionListener");
                        JButton additionalButton = new JButton();
                        Mnemonics.setLocalizedText((AbstractButton)additionalButton, (String)name);
                        additionalButton.addActionListener(new ActionListener(){

                            @Override
                            public void actionPerformed(ActionEvent e) {
                                Dialog dialog = (Dialog)OptionsAction.this.dialogWRef.get();
                                dialog.setVisible(false);
                            }
                        });
                        additionalButton.addActionListener(actionListener);
                        dd.setAdditionalOptions(new Object[]{additionalButton});
                    }
                    optionPanel.setDialogDescriptor(dd);
                    dialog = DialogDisplayer.getDefault().createDialog(dd);
                    dialog.setVisible(true);
                    OptionsAction.this.dialogWRef = new WeakReference<Dialog>(dialog);
                } else {
                    dialog.toFront();
                }
            }

        });
    }

    protected boolean asynchronous() {
        return false;
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx("org.netbeans.core.actions.OptionsAction");
    }

    public String getName() {
        return NbBundle.getBundle(OptionsAction.class).getString("Options");
    }

    public static final class OptionsPanel
    extends NbMainExplorer.ExplorerTab
    implements PropertyChangeListener {
        public static final String MODE_NAME = "options";
        private static OptionsPanel singleton;
        private static String TEMPLATES_DISPLAY_NAME;
        private Collection<Node> toExpand;
        private transient boolean expanded;
        private transient Node rootNode;
        private transient Reference<DialogDescriptor> descriptorRef = new WeakReference<Object>(null);
        private transient JSplitPane split = null;

        private OptionsPanel() {
            this.validateRootContext();
            this.getExplorerManager().addPropertyChangeListener((PropertyChangeListener)this);
        }

        protected String preferredID() {
            return "options";
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            DialogDescriptor dd;
            if ("selectedNodes".equals(evt.getPropertyName()) && (dd = this.descriptorRef.get()) != null) {
                dd.setHelpCtx(this.getHelpCtx());
            }
        }

        public void setDialogDescriptor(DialogDescriptor dd) {
            this.descriptorRef = new WeakReference<DialogDescriptor>(dd);
        }

        public HelpCtx getHelpCtx() {
            HelpCtx defaultHelp = new HelpCtx("org.netbeans.core.actions.OptionsAction");
            HelpCtx help = ExplorerUtils.getHelpCtx((Node[])this.getExplorerManager().getSelectedNodes(), (HelpCtx)defaultHelp);
            if (!defaultHelp.equals((Object)help)) {
                Node node = this.getExplorerManager().getSelectedNodes()[0];
                HelpCtx readHelpId = this.getHelpId(node);
                if (readHelpId != null) {
                    return readHelpId;
                }
                while (node != null && !TEMPLATES_DISPLAY_NAME.equals(node.getDisplayName())) {
                    readHelpId = this.getHelpId(node);
                    if (readHelpId != null) {
                        return readHelpId;
                    }
                    node = node.getParentNode();
                }
                if (node != null && TEMPLATES_DISPLAY_NAME.equals(node.getDisplayName())) {
                    return new HelpCtx("org.netbeans.core.actions.OptionsAction$TemplatesSubnode");
                }
            }
            return help;
        }

        private HelpCtx getHelpId(Node node) {
            Object o;
            DataObject dataObj = (DataObject)node.getCookie(DataObject.class);
            if (dataObj != null && (o = dataObj.getPrimaryFile().getAttribute("helpID")) != null) {
                return new HelpCtx(o.toString());
            }
            return null;
        }

        static OptionsPanel singleton() {
            if (singleton == null) {
                singleton = new OptionsPanel();
            }
            return singleton;
        }

        @Override
        protected TreeView initGui() {
            TTW retVal = new TTW();
            this.split = new JSplitPane(1);
            PropertySheetView propertyView = new PropertySheetView();
            this.split.setLeftComponent((Component)((Object)retVal));
            this.split.setRightComponent((Component)propertyView);
            this.split.setBorder((Border)UIManager.get("Nb.ScrollPane.border"));
            this.setLayout(new GridBagLayout());
            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.fill = 1;
            gridBagConstraints.weightx = 1.0;
            gridBagConstraints.weighty = 1.0;
            gridBagConstraints.gridwidth = 2;
            this.add((Component)this.split, gridBagConstraints);
            return retVal;
        }

        @Override
        public Dimension getPreferredSize() {
            int fontsize;
            Dimension result = super.getPreferredSize();
            Font treeFont = UIManager.getFont("Tree.font");
            int n = fontsize = treeFont != null ? treeFont.getSize() : 11;
            if (fontsize > 11) {
                int factor = fontsize - 11;
                result.height += 15 * factor;
                result.width += 50 * factor;
                Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
                if (result.height > screen.height) {
                    result.height = screen.height - 30;
                }
                if (result.width > screen.width) {
                    result.width = screen.width - 30;
                }
            } else {
                result.width += 20;
                result.height += 20;
            }
            return result;
        }

        @Override
        protected void updateTitle() {
        }

        boolean isPrepared() {
            return this.toExpand != null;
        }

        public void prepareNodes() {
            if (this.toExpand == null) {
                ArrayList<Node> arr = new ArrayList<Node>(101);
                OptionsPanel.expandNodes(this.getRootContext(), 2, arr);
                this.toExpand = arr;
            }
        }

        @Override
        public void addNotify() {
            super.addNotify();
            if (!this.expanded) {
                ((TTW)this.view).expandTheseNodes(this.toExpand, this.getExplorerManager().getRootContext());
                this.expanded = true;
            }
            this.split.setDividerLocation(this.getPreferredSize().width / 2);
        }

        protected void validateRootContext() {
            Node n = this.initRC();
            this.setRootContext(n);
        }

        private synchronized Node initRC() {
            if (this.rootNode == null) {
                this.rootNode = new OptionsFilterNode();
            }
            return this.rootNode;
        }

        private static void expandNodes(Node n, int depth, Collection<Node> list) {
            if (depth == 0) {
                return;
            }
            DataObject obj = (DataObject)n.getCookie(DataObject.class);
            if (obj instanceof DataShadow) {
                obj = ((DataShadow)obj).getOriginal();
            }
            if (obj != null) {
                if (!obj.getPrimaryFile().getPath().startsWith("UI/Services")) {
                    return;
                }
                InstanceCookie ic = (InstanceCookie)obj.getCookie(InstanceCookie.class);
                if (ic != null && ic instanceof InstanceCookie.Of && ((InstanceCookie.Of)ic).instanceOf(Node.class)) {
                    return;
                }
            }
            if (!list.contains((Object)n)) {
                list.add(n);
            }
            Node[] arr = n.getChildren().getNodes(true);
            for (int i = 0; i < arr.length; ++i) {
                Node p = arr[i];
                OptionsPanel.expandNodes(p, depth - 1, list);
            }
        }

        static {
            TEMPLATES_DISPLAY_NAME = NbBundle.getBundle(OptionsAction.class).getString("CTL_Templates_name");
        }

        private static class OptionsFilterNode
        extends FilterNode {
            public OptionsFilterNode() {
                super(NbPlaces.getDefault().session(), (Children)new SettingChildren(NbPlaces.getDefault().session()));
            }

            public HelpCtx getHelpCtx() {
                return new HelpCtx(OptionsFilterNode.class);
            }

            public Node.Handle getHandle() {
                return new H();
            }

            private static class H
            implements Node.Handle {
                private static final long serialVersionUID = -5158460093499159177L;

                H() {
                }

                public Node getNode() throws IOException {
                    return new OptionsFilterNode();
                }
            }

        }

        private static class TTW
        extends TreeTableView
        implements MouseListener,
        PropertyChangeListener,
        ActionListener {
            private final Node.Property indicator = new IndicatorProperty();
            private final Node.Property session = new SettingChildren.FileStateProperty("Session-Layer");
            private final Node.Property modules = new SettingChildren.FileStateProperty("Modules-Layer");
            private Node.Property[] active_set = null;
            PropertyChangeListener weakL = null;

            public TTW() {
                super((NodeTableModel)new NTM());
                this.refreshColumns(true);
                this.addMouseListener((MouseListener)this);
                this.weakL = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)SessionManager.getDefault());
                SessionManager.getDefault().addPropertyChangeListener(this.weakL);
                this.registerKeyboardAction((ActionListener)this, KeyStroke.getKeyStroke('+'), 1);
                this.getAccessibleContext().setAccessibleName(NbBundle.getBundle(OptionsAction.class).getString("ACSN_optionsTree"));
                this.getAccessibleContext().setAccessibleDescription(NbBundle.getBundle(OptionsAction.class).getString("ACSD_optionsTree"));
            }

            @Override
            public void mouseExited(MouseEvent evt) {
            }

            @Override
            public void mouseReleased(MouseEvent evt) {
            }

            @Override
            public void mousePressed(MouseEvent evt) {
            }

            @Override
            public void mouseClicked(MouseEvent evt) {
                JTableHeader h;
                Component c = evt.getComponent();
                if (c instanceof JTableHeader && 1 == (h = (JTableHeader)c).columnAtPoint(evt.getPoint())) {
                    this.refreshColumns(true);
                }
            }

            @Override
            public void mouseEntered(MouseEvent evt) {
            }

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if ("session_open".equals(evt.getPropertyName())) {
                    this.refreshColumns(false);
                }
            }

            private void refreshColumns(boolean changeSets) {
                int length;
                Node.Property[] new_set = this.active_set;
                int n = length = this.active_set == null ? 0 : this.active_set.length;
                if (changeSets && length == 1 || !changeSets && length > 1) {
                    new_set = new Node.Property[]{this.indicator, this.session, this.modules};
                    this.indicator.setDisplayName(NbBundle.getMessage(OptionsAction.class, (String)"LBL_IndicatorProperty_Name_Expanded"));
                    this.indicator.setShortDescription(NbBundle.getMessage(OptionsAction.class, (String)"LBL_IndicatorProperty_Description_Expanded"));
                } else if (changeSets) {
                    new_set = new Node.Property[]{this.indicator};
                    this.indicator.setDisplayName(NbBundle.getMessage(OptionsAction.class, (String)"LBL_IndicatorProperty_Name"));
                    this.indicator.setShortDescription(NbBundle.getMessage(OptionsAction.class, (String)"LBL_IndicatorProperty_Description"));
                }
                if (this.active_set != new_set) {
                    final Node.Property[] set = new_set;
                    if (SwingUtilities.isEventDispatchThread()) {
                        this.setNewSet(set);
                    } else {
                        SwingUtilities.invokeLater(new Runnable(){

                            @Override
                            public void run() {
                                TTW.this.setNewSet(set);
                            }
                        });
                    }
                    this.active_set = new_set;
                }
            }

            private void setNewSet(Node.Property[] set) {
                this.setProperties(set);
                this.setTreePreferredWidth(set.length == 1 ? 480 : 300);
                this.setTableColumnPreferredWidth(0, 20);
                for (int i = 1; i < set.length; ++i) {
                    this.setTableColumnPreferredWidth(i, 60);
                }
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                this.refreshColumns(true);
            }

            public void expandTheseNodes(Collection<Node> paths, Node root) {
                Iterator<Node> it = paths.iterator();
                Node first = null;
                while (it.hasNext()) {
                    Node n = it.next();
                    if (first == null) {
                        first = n;
                    }
                    this.expandNode(n);
                }
                if (first != null) {
                    this.collapseNode(first);
                    this.expandNode(first);
                }
                this.tree.scrollRowToVisible(0);
            }

            private static final class IndicatorProperty
            extends PropertySupport.ReadOnly<String> {
                public IndicatorProperty() {
                    super("indicator", String.class, "", "");
                }

                public String getValue() {
                    return "";
                }
            }

        }

        private static class NTM
        extends NodeTableModel {
            protected Node.Property getPropertyFor(Node node, Node.Property prop) {
                Object value = node.getValue(prop.getName());
                if (value instanceof Node.Property) {
                    return (Node.Property)value;
                }
                return null;
            }
        }

    }

}

