/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.nodes.Node
 *  org.openide.util.ChangeSupport
 */
package org.netbeans.modules.options.classic;

import java.io.IOException;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.options.classic.EnvironmentNode;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.nodes.Node;
import org.openide.util.ChangeSupport;

public final class NbPlaces {
    private final ChangeSupport cs;
    private static NbPlaces DEFAULT;

    private NbPlaces() {
        this.cs = new ChangeSupport((Object)this);
    }

    public static synchronized NbPlaces getDefault() {
        if (DEFAULT == null) {
            DEFAULT = new NbPlaces();
        }
        return DEFAULT;
    }

    public void addChangeListener(ChangeListener l) {
        this.cs.addChangeListener(l);
    }

    public void removeChangeListener(ChangeListener l) {
        this.cs.removeChangeListener(l);
    }

    void fireChange() {
        this.cs.fireChange();
    }

    public Node session() {
        return EnvironmentNode.find("session");
    }

    public static DataFolder findSessionFolder(String name) {
        try {
            FileObject fo = FileUtil.getConfigFile((String)name);
            if (fo == null) {
                fo = FileUtil.createFolder((FileObject)FileUtil.getConfigRoot(), (String)name);
            }
            return DataFolder.findFolder((FileObject)fo);
        }
        catch (IOException ex) {
            throw (IllegalStateException)new IllegalStateException("Folder not found and cannot be created: " + name).initCause(ex);
        }
    }
}

