/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.ExplorerUtils
 */
package org.netbeans.modules.options.classic;

import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JPanel;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;

public class ExplorerPanel
extends JPanel
implements ExplorerManager.Provider {
    private ExplorerManager manager = new ExplorerManager();

    public ExplorerPanel() {
        ActionMap map = this.getActionMap();
        map.put("copy-to-clipboard", ExplorerUtils.actionCopy((ExplorerManager)this.manager));
        map.put("cut-to-clipboard", ExplorerUtils.actionCut((ExplorerManager)this.manager));
        map.put("paste-from-clipboard", ExplorerUtils.actionPaste((ExplorerManager)this.manager));
        map.put("delete", ExplorerUtils.actionDelete((ExplorerManager)this.manager, (boolean)true));
    }

    public ExplorerManager getExplorerManager() {
        return this.manager;
    }

    @Override
    public void addNotify() {
        super.addNotify();
        ExplorerUtils.activateActions((ExplorerManager)this.manager, (boolean)true);
    }

    @Override
    public void removeNotify() {
        ExplorerUtils.activateActions((ExplorerManager)this.manager, (boolean)false);
        super.removeNotify();
    }
}

