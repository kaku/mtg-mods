/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.AsyncGUIJob
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.options.classic;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import org.netbeans.modules.options.classic.OptionsAction;
import org.openide.util.AsyncGUIJob;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class InitPanel
extends JPanel
implements AsyncGUIJob {
    private JLabel initComponent;
    private OptionsAction.OptionsPanel oPanel;
    private static InitPanel defInstance;

    static InitPanel getDefault(OptionsAction.OptionsPanel oPanel) {
        if (defInstance == null) {
            defInstance = new InitPanel(oPanel);
        }
        return defInstance;
    }

    private InitPanel(OptionsAction.OptionsPanel oPanel) {
        this.oPanel = oPanel;
        this.initComponents();
    }

    protected void initComponents() {
        if (!this.oPanel.isPrepared()) {
            this.initComponent = new JLabel(NbBundle.getMessage(InitPanel.class, (String)"LBL_computing"));
            this.initComponent.setPreferredSize(new Dimension(850, 450));
            Color c = UIManager.getColor("Tree.background");
            if (c == null) {
                c = Color.WHITE;
            }
            this.initComponent.setBackground(c);
            this.initComponent.setHorizontalAlignment(0);
            this.initComponent.setOpaque(true);
            CardLayout card = new CardLayout();
            this.setLayout(card);
            this.add((Component)this.initComponent, "init");
            card.show(this, "init");
            Utilities.attachInitJob((Component)this, (AsyncGUIJob)this);
        } else {
            this.finished();
        }
    }

    public void construct() {
        this.oPanel.prepareNodes();
    }

    public void finished() {
        this.add((Component)this.oPanel, "ready");
        CardLayout card = (CardLayout)this.getLayout();
        card.show(this, "ready");
        this.oPanel.requestFocus();
    }
}

