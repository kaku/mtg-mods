/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.actions.ToolsAction
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.InstanceCookie$Of
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataShadow
 *  org.openide.nodes.Children
 *  org.openide.nodes.FilterNode
 *  org.openide.nodes.FilterNode$Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.nodes.Node$Handle
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.PropertySupport
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.SystemAction
 */
package org.netbeans.modules.options.classic;

import java.awt.Image;
import java.beans.PropertyEditor;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import javax.swing.Action;
import org.netbeans.modules.options.classic.FileStateEditor;
import org.netbeans.modules.options.classic.FileStateManager;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.actions.ToolsAction;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataShadow;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;

public final class SettingChildren
extends FilterNode.Children {
    public static final String PROP_LAYER_SESSION = "Session-Layer";
    public static final String PROP_LAYER_MODULES = "Modules-Layer";

    public SettingChildren(Node original) {
        super(original);
    }

    protected Node copyNode(Node node) {
        boolean filter = false;
        try {
            InstanceCookie.Of inst;
            DataObject d = (DataObject)node.getCookie(DataObject.class);
            if (d != null && (inst = (InstanceCookie.Of)d.getCookie(InstanceCookie.Of.class)) != null && (inst.instanceOf(Node.class) || inst.instanceOf(Node.Handle.class))) {
                d = null;
            }
            DataFolder folder = (DataFolder)node.getCookie(DataFolder.class);
            FileSystem fs = d == null || folder != null ? null : d.getPrimaryFile().getFileSystem();
            filter = fs == null ? false : fs.isDefault();
        }
        catch (FileStateInvalidException e) {
            // empty catch block
        }
        return filter ? new SettingFilterNode(node) : (node.isLeaf() ? node.cloneNode() : new TrivialFilterNode(node));
    }

    private static Action[] removeActions(Action[] allActions, Action[] toDeleteActions) {
        Action[] retVal = allActions;
        ArrayList<Action> actions = new ArrayList<Action>(Arrays.asList(allActions));
        for (int i = 0; i < toDeleteActions.length; ++i) {
            Action a = toDeleteActions[i];
            if (!actions.contains(a)) continue;
            actions.remove(a);
            retVal = actions.toArray(new Action[actions.size()]);
        }
        return retVal;
    }

    private static final class SettingFilterNode
    extends FilterNode {
        private FSL weakL = null;

        public SettingFilterNode(Node original) {
            super(original);
            this.disableDelegation(1536);
            FileObject pf = ((DataObject)this.getCookie(DataObject.class)).getPrimaryFile();
            this.weakL = new FSL(this);
            FileStateManager.getDefault().addFileStatusListener(this.weakL, pf);
            this.specialProp((Node.Property)new FileStateProperty(pf, 1, "Session-Layer", false));
            this.specialProp((Node.Property)new FileStateProperty(pf, 2, "Modules-Layer", false));
        }

        public String getDisplayName() {
            DataShadow dsh;
            Node origNode;
            String retVal = null;
            DataObject dobj = (DataObject)this.getCookie(DataObject.class);
            if (dobj != null && dobj instanceof DataShadow && (origNode = (dsh = (DataShadow)dobj).getOriginal().getNodeDelegate()) != null) {
                retVal = origNode.getDisplayName();
            }
            return retVal != null ? retVal : super.getDisplayName();
        }

        private void specialProp(Node.Property p) {
            this.setValue(p.getName(), (Object)p);
        }

        public boolean equals(Object o) {
            return this == o || this.getOriginal().equals(o) || o != null && o.equals((Object)this.getOriginal());
        }

        public int hashCode() {
            return this.getOriginal().hashCode();
        }

        public Action[] getActions(boolean context) {
            return SettingChildren.removeActions(super.getActions(context), new Action[]{SystemAction.get(ToolsAction.class)});
        }

        private static class FSL
        implements FileStateManager.FileStatusListener {
            WeakReference<SettingFilterNode> node = null;

            public FSL(SettingFilterNode sfn) {
                this.node = new WeakReference<SettingFilterNode>(sfn);
            }

            @Override
            public void fileStatusChanged(FileObject mfo) {
                SettingFilterNode n = this.node.get();
                if (n == null) {
                    FileStateManager.getDefault().removeFileStatusListener(this, null);
                    return;
                }
                n.firePropertyChange("Session-Layer", null, null);
                n.firePropertyChange("Modules-Layer", null, null);
            }
        }

    }

    public static class FileStateProperty
    extends PropertySupport<Integer> {
        static final int ACTION_DEFINE = 1;
        static final int ACTION_REVERT = 2;
        static final int ACTION_DELETE = 3;
        private FileObject primaryFile = null;
        private int layer;

        public FileStateProperty(String name) {
            this(null, 0, name, true);
        }

        public FileStateProperty(FileObject primaryFile, int layer, String name, boolean readonly) {
            super(name, Integer.class, NbBundle.getMessage(FileStateProperty.class, (String)("LBL_FSP_" + name)), NbBundle.getMessage(FileStateProperty.class, (String)("LBL_FSP_Desc_" + name)), true, !readonly);
            this.primaryFile = primaryFile;
            this.layer = layer;
            this.setValue("values", (Object)new Integer[]{0, 1, 2, 3});
            this.setValue("images", (Object)new Image[]{ImageUtilities.loadImage((String)"org/netbeans/core/resources/setting-defined.gif"), ImageUtilities.loadImage((String)"org/netbeans/core/resources/setting-ignored.gif"), ImageUtilities.loadImage((String)"org/netbeans/core/resources/setting-inherited.gif"), ImageUtilities.loadImage((String)"org/openide/resources/actions/empty.gif")});
        }

        public boolean canWrite() {
            if (!PropertySupport.super.canWrite()) {
                return false;
            }
            Integer val = null;
            try {
                val = this.getValue();
            }
            catch (Exception e) {
                // empty catch block
            }
            return val != null && val != 0 && (this.layer != 2 || val != 3);
        }

        public Integer getValue() throws IllegalAccessException, InvocationTargetException {
            return FileStateManager.getDefault().getFileState(this.primaryFile, this.layer);
        }

        public void setValue(Integer val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            FileStateManager fsm = FileStateManager.getDefault();
            int action = val;
            try {
                switch (action) {
                    case 1: 
                    case 2: {
                        boolean go = true;
                        for (int i = 0; i < this.layer; ++i) {
                            int state = fsm.getFileState(this.primaryFile, i);
                            if (state != 0) continue;
                            NotifyDescriptor.Confirmation nd = new NotifyDescriptor.Confirmation((Object)NbBundle.getMessage(SettingChildren.class, (String)"MSG_ask_remove_above_defined_files"), 0);
                            Object answer = DialogDisplayer.getDefault().notify((NotifyDescriptor)nd);
                            if (!answer.equals(NotifyDescriptor.NO_OPTION)) break;
                            go = false;
                            break;
                        }
                        if (go) {
                            fsm.define(this.primaryFile, this.layer, action == 2);
                            break;
                        }
                        break;
                    }
                    case 3: {
                        fsm.delete(this.primaryFile, this.layer);
                        break;
                    }
                    default: {
                        throw new IllegalArgumentException("Required file state change isn't allowed. Action=" + action);
                    }
                }
            }
            catch (IOException e) {
                Exceptions.printStackTrace((Throwable)e);
            }
        }

        public PropertyEditor getPropertyEditor() {
            return new FileStateEditor();
        }

        public String getShortDescription() {
            Integer val = null;
            String s = null;
            if (this.primaryFile != null) {
                try {
                    val = this.getValue();
                }
                catch (Exception e) {
                    // empty catch block
                }
                switch (val == null ? -1 : val) {
                    case 0: {
                        s = NbBundle.getMessage(SettingChildren.class, (String)"LBL_fstate_defined");
                        break;
                    }
                    case 1: {
                        s = NbBundle.getMessage(SettingChildren.class, (String)"LBL_fstate_ignored");
                        break;
                    }
                    case 2: {
                        s = NbBundle.getMessage(SettingChildren.class, (String)"LBL_fstate_inherited");
                        break;
                    }
                    case 3: {
                        s = NbBundle.getMessage(SettingChildren.class, (String)"LBL_fstate_undefined");
                    }
                }
            } else {
                s = PropertySupport.super.getShortDescription();
            }
            return s == null || s.length() == 0 ? null : s;
        }
    }

    private static final class TrivialFilterNode
    extends FilterNode {
        public TrivialFilterNode(Node n) {
            super(n, (Children)new SettingChildren(n));
        }

        public boolean equals(Object o) {
            return this == o || this.getOriginal().equals(o) || o != null && o.equals((Object)this.getOriginal());
        }

        public int hashCode() {
            return this.getOriginal().hashCode();
        }

        public Action[] getActions(boolean context) {
            return SettingChildren.removeActions(super.getActions(context), new Action[]{SystemAction.get(ToolsAction.class)});
        }

        public String getHtmlDisplayName() {
            return null;
        }
    }

}

