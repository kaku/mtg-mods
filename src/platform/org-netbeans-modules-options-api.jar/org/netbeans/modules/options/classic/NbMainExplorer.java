/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerUtils
 *  org.openide.explorer.view.BeanTreeView
 *  org.openide.explorer.view.TreeView
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeListener
 *  org.openide.util.HelpCtx
 *  org.openide.util.HelpCtx$Provider
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 */
package org.netbeans.modules.options.classic;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.beans.PropertyChangeListener;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.Action;
import javax.swing.ActionMap;
import org.netbeans.modules.options.classic.EnvironmentNode;
import org.netbeans.modules.options.classic.ExplorerPanel;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.BeanTreeView;
import org.openide.explorer.view.TreeView;
import org.openide.nodes.Node;
import org.openide.nodes.NodeListener;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

public final class NbMainExplorer {
    static final long serialVersionUID = 6021472310669753679L;
    public static final int MIN_HEIGHT = 150;
    public static final int DEFAULT_WIDTH = 350;
    private static NbMainExplorer explorer;

    public static NbMainExplorer getExplorer() {
        if (explorer == null) {
            explorer = new NbMainExplorer();
        }
        return explorer;
    }

    public static class MainTab
    extends ExplorerTab
    implements HelpCtx.Provider {
        private static MainTab DEFAULT;

        public static synchronized MainTab getDefaultMainTab() {
            if (DEFAULT == null) {
                DEFAULT = new MainTab();
            }
            return DEFAULT;
        }

        public HelpCtx getHelpCtx() {
            return ExplorerUtils.getHelpCtx((Node[])this.getExplorerManager().getSelectedNodes(), (HelpCtx)new HelpCtx(EnvironmentNode.class));
        }

        @Override
        protected void updateTitle() {
        }
    }

    public static class ExplorerTab
    extends ExplorerPanel {
        static final long serialVersionUID = -8202452314155464024L;
        private static final String PROP_CONFIRM_DELETE = "confirmDelete";
        protected TreeView view;
        private PropertyChangeListener weakRcL;
        private NodeListener weakNRcL;
        private NodeListener rcListener;
        private boolean valid = true;
        private boolean rootVis = true;

        public ExplorerTab() {
            this.getActionMap().put("delete", ExplorerUtils.actionDelete((ExplorerManager)this.getExplorerManager(), (boolean)ExplorerTab.getConfirmDelete()));
            ExplorerTab.getPreferences().addPreferenceChangeListener(new PreferenceChangeListener(){

                @Override
                public void preferenceChange(PreferenceChangeEvent evt) {
                    if ("confirmDelete".equals(evt.getKey())) {
                        ExplorerTab.this.getActionMap().put("delete", ExplorerUtils.actionDelete((ExplorerManager)ExplorerTab.this.getExplorerManager(), (boolean)ExplorerTab.getConfirmDelete()));
                    }
                }
            });
        }

        private static Preferences getPreferences() {
            return NbPreferences.root().node("/org/netbeans/core");
        }

        private static boolean getConfirmDelete() {
            return ExplorerTab.getPreferences().getBoolean("confirmDelete", true);
        }

        @Override
        public void addNotify() {
            super.addNotify();
            if (this.view == null) {
                this.view = this.initGui();
                this.view.setRootVisible(this.rootVis);
                this.view.getAccessibleContext().setAccessibleName(NbBundle.getBundle(NbMainExplorer.class).getString("ACSN_ExplorerBeanTree"));
                this.view.getAccessibleContext().setAccessibleDescription(NbBundle.getBundle(NbMainExplorer.class).getString("ACSD_ExplorerBeanTree"));
            }
        }

        @Override
        public void requestFocus() {
            super.requestFocus();
            if (this.view != null) {
                this.view.requestFocus();
            }
        }

        @Override
        public boolean requestFocusInWindow() {
            super.requestFocusInWindow();
            if (this.view != null) {
                return this.view.requestFocusInWindow();
            }
            return false;
        }

        protected TreeView initGui() {
            BeanTreeView v = new BeanTreeView();
            v.setDragSource(true);
            this.setLayout(new BorderLayout());
            this.add((Component)v);
            return v;
        }

        public void setRootContext(Node rc) {
            Node oldRC = this.getExplorerManager().getRootContext();
            if (this.weakRcL != null) {
                oldRC.removePropertyChangeListener(this.weakRcL);
            }
            if (this.weakNRcL != null) {
                oldRC.removeNodeListener(this.weakNRcL);
            }
            this.getExplorerManager().setRootContext(rc);
        }

        public void setRootContext(Node rc, boolean rootVisible) {
            this.rootVis = rootVisible;
            if (this.view != null) {
                this.view.setRootVisible(rootVisible);
            }
            this.setRootContext(rc);
        }

        public Node getRootContext() {
            return this.getExplorerManager().getRootContext();
        }

        protected void updateTitle() {
            this.setName(this.getExplorerManager().getRootContext().getDisplayName());
        }

        @Override
        public void setName(String name) {
            super.setName(name);
            if (this.view != null) {
                this.view.getAccessibleContext().setAccessibleName(name);
            }
        }

        @Override
        public void setToolTipText(String text) {
            super.setToolTipText(text);
            if (this.view != null) {
                this.view.getAccessibleContext().setAccessibleDescription(text);
            }
        }

    }

}

