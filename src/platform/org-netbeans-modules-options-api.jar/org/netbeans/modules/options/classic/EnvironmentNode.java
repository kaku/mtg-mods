/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.actions.PropertiesAction
 *  org.openide.actions.ToolsAction
 *  org.openide.loaders.DataFolder
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Handle
 *  org.openide.util.HelpCtx
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 *  org.openide.util.actions.SystemAction
 */
package org.netbeans.modules.options.classic;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import org.netbeans.modules.options.classic.LookupNode;
import org.netbeans.modules.options.classic.NbPlaces;
import org.openide.actions.PropertiesAction;
import org.openide.actions.ToolsAction;
import org.openide.loaders.DataFolder;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Mutex;
import org.openide.util.actions.SystemAction;

final class EnvironmentNode {
    static final long serialVersionUID = 4782447107972624693L;
    private String filter;
    private static HashMap<String, Node> types = new HashMap(11);
    private static final Object lock = new Object();
    public static final String TYPE_SESSION = "session";

    EnvironmentNode() {
    }

    public static Node find(final String name) {
        Node retValue = (Node)Children.MUTEX.readAccess((Mutex.Action)new Mutex.Action<Node>(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            public Node run() {
                Object object = lock;
                synchronized (object) {
                    Object n = (Node)types.get(name);
                    if (n == null) {
                        DataFolder folder = null;
                        assert ("session".equals(name));
                        folder = NbPlaces.findSessionFolder("UI/Services");
                        n = new PersistentLookupNode(name, folder);
                        types.put(name, n);
                    }
                    return n;
                }
            }
        });
        if (retValue != null) {
            return retValue;
        }
        throw new IllegalStateException();
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(EnvironmentNode.class);
    }

    public SystemAction[] createActions() {
        return new SystemAction[]{SystemAction.get(ToolsAction.class), SystemAction.get(PropertiesAction.class)};
    }

    static final class EnvironmentHandle
    implements Node.Handle {
        static final long serialVersionUID = -850350968366553370L;
        private String filter;

        public EnvironmentHandle(String filter) {
            this.filter = filter;
        }

        public Node getNode() {
            String f = this.filter;
            if (f == null) {
                f = "session";
            }
            return EnvironmentNode.find(f);
        }
    }

    private static final class PersistentLookupNode
    extends LookupNode
    implements PropertyChangeListener {
        private String filter;

        public PersistentLookupNode(String filter, DataFolder folder) {
            super(folder);
            this.filter = filter;
        }

        public Node.Handle getHandle() {
            return new EnvironmentHandle(this.filter);
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("children".equals(evt.getPropertyName())) {
                NbPlaces.getDefault().fireChange();
            }
        }
    }

}

