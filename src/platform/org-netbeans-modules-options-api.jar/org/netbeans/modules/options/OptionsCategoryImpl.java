/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 */
package org.netbeans.modules.options;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.Callable;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.netbeans.modules.options.TabbedController;
import org.netbeans.spi.options.OptionsCategory;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;

public class OptionsCategoryImpl
extends OptionsCategory {
    private String title;
    private String categoryName;
    private String iconBase;
    private ImageIcon icon;
    private Callable<OptionsPanelController> controller;
    private String keywords;
    private String keywordsCategory;
    private String advancedOptionsFolder;

    public OptionsCategoryImpl(String title, String categoryName, String iconBase, Callable<OptionsPanelController> controller, String keywords, String keywordsCategory, String advancedOptionsFolder) {
        this.title = title;
        this.categoryName = categoryName;
        this.iconBase = iconBase;
        this.controller = controller;
        this.advancedOptionsFolder = advancedOptionsFolder;
        this.keywords = keywords;
        this.keywordsCategory = keywordsCategory;
    }

    @Override
    public Icon getIcon() {
        if (this.icon == null) {
            ImageIcon res = ImageUtilities.loadImageIcon((String)this.iconBase, (boolean)true);
            if (res != null) {
                return res;
            }
            res = ImageUtilities.loadImageIcon((String)(this.iconBase + ".png"), (boolean)true);
            if (res != null) {
                return res;
            }
            res = ImageUtilities.loadImageIcon((String)(this.iconBase + ".gif"), (boolean)true);
            return res;
        }
        return this.icon;
    }

    @Override
    public String getCategoryName() {
        return this.categoryName;
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public OptionsPanelController create() {
        if (this.advancedOptionsFolder != null) {
            return new TabbedController(this.advancedOptionsFolder);
        }
        try {
            return this.controller.call();
        }
        catch (Exception x) {
            Exceptions.printStackTrace((Throwable)x);
            return new TabbedController("<error>");
        }
    }

    final Set<String> getKeywordsByCategory() {
        if (this.keywords != null) {
            return Collections.singleton(this.keywords);
        }
        return Collections.emptySet();
    }
}

