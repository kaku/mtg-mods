/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.EditableProperties
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.options.export;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.SyncFailedException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;
import org.netbeans.modules.options.export.OptionsChooserPanel;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.EditableProperties;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public final class OptionsExportModel {
    private static final Logger LOGGER = Logger.getLogger(OptionsExportModel.class.getName());
    private static final String OPTIONS_EXPORT_FOLDER = "OptionsExport";
    private static final String GROUP_PATTERN = "([^/]*)";
    private File source;
    private List<Category> categories;
    List<String> relativePaths;
    private ZipOutputStream zipOutputStream;
    private File targetUserdir;
    private Set<String> includePatterns;
    private Set<String> excludePatterns;
    private EditableProperties currentProperties;
    private static final List<String> IGNORED_FOLDERS = Arrays.asList("var/cache");
    private final String PASSWORDS_PATTERN = "config/Preferences/org/netbeans/modules/keyring.*";
    static final String ENABLED_ITEMS_INFO = "enabledItems.info";
    static final String BUILD_INFO = "build.info";

    public OptionsExportModel(File source) {
        this.source = source;
    }

    ArrayList<String> getEnabledItemsDuringExport(File importSource) {
        File[] children;
        ArrayList<String> enabledItems = null;
        if (importSource.isFile()) {
            try {
                ZipFile zipFile = new ZipFile(importSource);
                Enumeration<? extends ZipEntry> entries = zipFile.entries();
                while (entries.hasMoreElements()) {
                    String strLine;
                    ZipEntry zipEntry = entries.nextElement();
                    if (!zipEntry.getName().equals("enabledItems.info")) continue;
                    enabledItems = new ArrayList();
                    InputStream stream = zipFile.getInputStream(zipEntry);
                    BufferedReader br = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8));
                    while ((strLine = br.readLine()) != null) {
                        enabledItems.add(strLine);
                    }
                }
            }
            catch (ZipException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        } else if (importSource.isDirectory() && (children = importSource.listFiles(new FileFilter(){

            @Override
            public boolean accept(File pathname) {
                return pathname.getName().equals("enabledItems.info");
            }
        })).length == 1) {
            enabledItems = new ArrayList<String>();
            try {
                String strLine;
                BufferedReader br = Files.newBufferedReader(Paths.get(Utilities.toURI((File)children[0])), StandardCharsets.UTF_8);
                while ((strLine = br.readLine()) != null) {
                    enabledItems.add(strLine);
                }
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return enabledItems;
    }

    double getBuildNumberDuringExport(File importSource) {
        File[] children;
        String buildNumber = null;
        if (importSource.isFile()) {
            try {
                ZipFile zipFile = new ZipFile(importSource);
                Enumeration<? extends ZipEntry> entries = zipFile.entries();
                while (entries.hasMoreElements()) {
                    String strLine;
                    ZipEntry zipEntry = entries.nextElement();
                    if (!zipEntry.getName().equals("build.info")) continue;
                    InputStream stream = zipFile.getInputStream(zipEntry);
                    BufferedReader br = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8));
                    while ((strLine = br.readLine()) != null) {
                        if (!strLine.startsWith("ProductVersion=")) continue;
                        buildNumber = this.getBuildNumber(strLine.substring(strLine.indexOf(" (Build ") + 8, strLine.length() - 1));
                    }
                }
            }
            catch (ZipException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        } else if (importSource.isDirectory() && (children = importSource.listFiles(new FileFilter(){

            @Override
            public boolean accept(File pathname) {
                return pathname.getName().equals("build.info");
            }
        })).length == 1) {
            try {
                String strLine;
                BufferedReader br = Files.newBufferedReader(Paths.get(Utilities.toURI((File)children[0])), StandardCharsets.UTF_8);
                while ((strLine = br.readLine()) != null) {
                    if (!strLine.startsWith("ProductVersion=")) continue;
                    buildNumber = this.getBuildNumber(strLine.substring(strLine.indexOf(" (Build ") + 8, strLine.length() - 1));
                }
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return buildNumber == null ? -1.0 : Double.parseDouble(buildNumber);
    }

    String getBuildNumber(String build) {
        if (build.contains("-")) {
            build = build.substring(0, build.indexOf("-")).concat("2359");
        }
        return build;
    }

    List<Category> getCategories() {
        if (this.categories == null) {
            this.loadCategories();
        }
        return this.categories;
    }

    State getState() {
        int enabled = 0;
        int disabled = 0;
        int applicableCount = 0;
        for (Category category : this.getCategories()) {
            if (!category.isApplicable()) continue;
            ++applicableCount;
            if (category.getState() == State.ENABLED) {
                ++enabled;
                continue;
            }
            if (category.getState() != State.DISABLED) continue;
            ++disabled;
        }
        if (enabled == applicableCount) {
            return State.ENABLED;
        }
        if (disabled == applicableCount) {
            return State.DISABLED;
        }
        return State.PARTIAL;
    }

    void setState(State state) {
        String passwords = NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.export.passwords.category.displayName");
        for (Category category : this.getCategories()) {
            if (!category.isApplicable()) continue;
            if (state.equals((Object)State.ENABLED)) {
                if (category.getDisplayName() == null || category.getDisplayName().equals(passwords)) continue;
                category.setState(state);
                continue;
            }
            category.setState(state);
        }
    }

    void doImport(File targetUserdir) throws IOException {
        LOGGER.fine("Copying from: " + this.source + "\n    to: " + targetUserdir);
        this.targetUserdir = targetUserdir;
        this.copyFiles();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void doExport(File targetZipFile, ArrayList<String> enabledItems) {
        try {
            OptionsExportModel.ensureParent(targetZipFile);
            this.zipOutputStream = new ZipOutputStream(OptionsExportModel.createOutputStream(targetZipFile));
            this.copyFiles();
            this.createEnabledItemsInfo(this.zipOutputStream, enabledItems);
            OptionsExportModel.createProductInfo(this.zipOutputStream);
            this.zipOutputStream.close();
        }
        catch (IOException ex) {
            Exceptions.attachLocalizedMessage((Throwable)ex, (String)NbBundle.getMessage(OptionsExportModel.class, (String)"OptionsExportModel.export.zip.error", (Object)targetZipFile));
            Exceptions.printStackTrace((Throwable)ex);
        }
        finally {
            if (this.zipOutputStream != null) {
                try {
                    this.zipOutputStream.close();
                }
                catch (IOException ex) {}
            }
        }
    }

    private void createEnabledItemsInfo(ZipOutputStream out, ArrayList<String> enabledItems) throws IOException {
        out.putNextEntry(new ZipEntry("enabledItems.info"));
        if (!enabledItems.isEmpty()) {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter((OutputStream)out, StandardCharsets.UTF_8));
            for (String item : enabledItems) {
                writer.append(item).append('\n');
            }
            writer.flush();
        }
        out.closeEntry();
    }

    static Set<String> parsePattern(String pattern) {
        HashSet<String> patterns = new HashSet<String>();
        if (pattern.contains("#")) {
            StringBuilder partPattern = new StringBuilder();
            ParserState state = ParserState.START;
            int blockLevel = 0;
            block6 : for (int i = 0; i < pattern.length(); ++i) {
                char c = pattern.charAt(i);
                switch (state) {
                    case START: {
                        if (c == '#') {
                            state = ParserState.IN_KEY_PATTERN;
                            partPattern.append(c);
                            continue block6;
                        }
                        if (c == '(') {
                            state = ParserState.IN_BLOCK;
                            ++blockLevel;
                            partPattern.append(c);
                            continue block6;
                        }
                        if (c == '|') {
                            patterns.add(partPattern.toString());
                            partPattern = new StringBuilder();
                            continue block6;
                        }
                        partPattern.append(c);
                        continue block6;
                    }
                    case IN_KEY_PATTERN: {
                        if (c == '#') {
                            state = ParserState.AFTER_KEY_PATTERN;
                            continue block6;
                        }
                        partPattern.append(c);
                        continue block6;
                    }
                    case AFTER_KEY_PATTERN: {
                        if (c == '|') {
                            state = ParserState.START;
                            patterns.add(partPattern.toString());
                            partPattern = new StringBuilder();
                            continue block6;
                        }
                        assert (false);
                        continue block6;
                    }
                    case IN_BLOCK: {
                        partPattern.append(c);
                        if (c != ')' || --blockLevel != 0) continue block6;
                        state = ParserState.START;
                    }
                }
            }
            patterns.add(partPattern.toString());
        } else {
            patterns.add(pattern);
        }
        return patterns;
    }

    private Set<String> getIncludePatterns() {
        if (this.includePatterns == null) {
            this.includePatterns = new HashSet<String>();
            for (Category category : this.getCategories()) {
                for (Item item : category.getItems()) {
                    String include;
                    if (!item.isEnabled() || (include = item.getInclude()) == null || include.length() <= 0) continue;
                    this.includePatterns.addAll(OptionsExportModel.parsePattern(include));
                }
            }
        }
        return this.includePatterns;
    }

    private Set<String> getExcludePatterns() {
        if (this.excludePatterns == null) {
            this.excludePatterns = new HashSet<String>();
            String passwords = NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.export.passwords.displayName");
            for (Category category : this.getCategories()) {
                for (Item item : category.getItems()) {
                    if (item.isEnabled()) {
                        String exclude = item.getExclude();
                        if (exclude == null || exclude.length() <= 0) continue;
                        this.excludePatterns.addAll(OptionsExportModel.parsePattern(exclude));
                        continue;
                    }
                    if (!item.getDisplayName().equals(passwords)) continue;
                    this.excludePatterns.add("config/Preferences/org/netbeans/modules/keyring.*");
                }
            }
        }
        return this.excludePatterns;
    }

    public String toString() {
        return this.getClass().getName() + " source=" + this.source;
    }

    private void loadCategories() {
        FileObject[] categoryFOs = FileUtil.getConfigFile((String)"OptionsExport").getChildren();
        List sortedCats = FileUtil.getOrder(Arrays.asList(categoryFOs), (boolean)false);
        this.categories = new ArrayList<Category>(sortedCats.size());
        for (FileObject curFO : sortedCats) {
            String displayName = (String)curFO.getAttribute("displayName");
            this.categories.add(new Category(curFO, displayName));
        }
    }

    private List<String> getApplicablePaths(Set<String> includePatterns, Set<String> excludePatterns) {
        ArrayList<String> applicablePaths = new ArrayList<String>();
        for (String relativePath : this.getRelativePaths()) {
            if (!OptionsExportModel.matches(relativePath, includePatterns, excludePatterns)) continue;
            applicablePaths.add(relativePath);
        }
        return applicablePaths;
    }

    private void copyFiles() throws IOException {
        if (this.source.isFile()) {
            try {
                this.copyZipFile();
            }
            catch (IOException ex) {
                Exceptions.attachLocalizedMessage((Throwable)ex, (String)NbBundle.getMessage(OptionsExportModel.class, (String)"OptionsExportModel.invalid.zipfile", (Object)this.source));
                Exceptions.printStackTrace((Throwable)ex);
            }
        } else {
            this.copyFolder(this.source);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void copyZipFile() throws IOException {
        ZipFile zipFile = new ZipFile(this.source);
        try {
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry zipEntry = entries.nextElement();
                if (zipEntry.isDirectory()) continue;
                this.copyFile(zipEntry.getName());
            }
        }
        finally {
            if (zipFile != null) {
                zipFile.close();
            }
        }
    }

    private void copyFolder(File file) throws IOException {
        String relativePath = OptionsExportModel.getRelativePath(this.source, file);
        if (IGNORED_FOLDERS.contains(relativePath)) {
            return;
        }
        File[] children = file.listFiles();
        if (children == null) {
            return;
        }
        for (File child : children) {
            if (child.isDirectory()) {
                this.copyFolder(child);
                continue;
            }
            this.copyFile(OptionsExportModel.getRelativePath(this.source, child));
        }
    }

    private List<String> getRelativePaths() {
        if (this.relativePaths == null) {
            if (this.source.isFile()) {
                try {
                    this.relativePaths = OptionsExportModel.listZipFile(this.source);
                }
                catch (IOException ex) {
                    Exceptions.attachLocalizedMessage((Throwable)ex, (String)NbBundle.getMessage(OptionsExportModel.class, (String)"OptionsExportModel.invalid.zipfile", (Object)this.source));
                    Exceptions.printStackTrace((Throwable)ex);
                    this.relativePaths = Collections.emptyList();
                }
            } else {
                this.relativePaths = OptionsExportModel.getRelativePaths(this.source);
            }
            LOGGER.fine("relativePaths=" + this.relativePaths);
        }
        return this.relativePaths;
    }

    static List<String> getRelativePaths(File sourceRoot) {
        return OptionsExportModel.getRelativePaths(sourceRoot, sourceRoot);
    }

    private static List<String> getRelativePaths(File root, File file) {
        String relativePath = OptionsExportModel.getRelativePath(root, file);
        ArrayList<String> result = new ArrayList<String>();
        if (file.isDirectory()) {
            if (IGNORED_FOLDERS.contains(relativePath)) {
                return result;
            }
            File[] children = file.listFiles();
            if (children == null) {
                return Collections.emptyList();
            }
            for (File child : children) {
                result.addAll(OptionsExportModel.getRelativePaths(root, child));
            }
        } else {
            result.add(relativePath);
        }
        return result;
    }

    private static String getRelativePath(File root, File file) {
        String result = file.getAbsolutePath().substring(root.getAbsolutePath().length());
        if ((result = result.replace('\\', '/')).startsWith("/") && !result.startsWith("//")) {
            result = result.substring(1);
        }
        return result;
    }

    private static boolean matches(String relativePath, Set<String> includePatterns, Set<String> excludePatterns) {
        boolean include = false;
        for (String pattern2 : includePatterns) {
            if (!OptionsExportModel.matches(relativePath, pattern2)) continue;
            include = true;
            break;
        }
        if (include) {
            for (String pattern2 : excludePatterns) {
                if (pattern2.contains("#") || !OptionsExportModel.matches(relativePath, pattern2)) continue;
                return false;
            }
        }
        return include;
    }

    private static boolean matches(String relativePath, String pattern) {
        if (pattern.contains("#")) {
            pattern = pattern.split("#", 2)[0];
        }
        return relativePath.matches(pattern);
    }

    private Set<String> matchingKeys(String relativePath, String propertiesPattern) throws IOException {
        HashSet<String> matchingKeys = new HashSet<String>();
        String[] patterns = propertiesPattern.split("#", 2);
        String filePattern = patterns[0];
        String keyPattern = patterns[1];
        if (relativePath.matches(filePattern)) {
            if (this.currentProperties == null) {
                this.currentProperties = this.getProperties(relativePath);
            }
            for (String key : this.currentProperties.keySet()) {
                if (!key.matches(keyPattern)) continue;
                matchingKeys.add(key);
            }
        }
        return matchingKeys;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void copyFile(String relativePath) throws IOException {
        this.currentProperties = null;
        boolean includeFile = false;
        HashSet<String> includeKeys = new HashSet<String>();
        HashSet<String> excludeKeys = new HashSet<String>();
        for (String pattern2 : this.getIncludePatterns()) {
            if (pattern2.contains("#")) {
                includeKeys.addAll(this.matchingKeys(relativePath, pattern2));
                continue;
            }
            if (!relativePath.matches(pattern2)) continue;
            includeFile = true;
            includeKeys.clear();
            break;
        }
        if (includeFile || !includeKeys.isEmpty()) {
            for (String pattern2 : this.getExcludePatterns()) {
                if (pattern2.contains("#")) {
                    excludeKeys.addAll(this.matchingKeys(relativePath, pattern2));
                    continue;
                }
                if (!relativePath.matches(pattern2)) continue;
                includeFile = false;
                includeKeys.clear();
                break;
            }
        }
        LOGGER.log(Level.FINEST, "{0}, includeFile={1}, includeKeys={2}, excludeKeys={3}", new Object[]{relativePath, includeFile, includeKeys, excludeKeys});
        if (!includeFile && includeKeys.isEmpty()) {
            return;
        }
        if (this.zipOutputStream != null) {
            LOGGER.log(Level.FINE, "Adding to zip: {0}", relativePath);
            this.zipOutputStream.putNextEntry(new ZipEntry(relativePath));
            this.copyFileOrProperties(relativePath, includeKeys, excludeKeys, this.zipOutputStream);
            this.zipOutputStream.closeEntry();
        } else {
            OutputStream out = null;
            File targetFile = new File(this.targetUserdir, relativePath);
            LOGGER.log(Level.FINE, "Path: {0}", relativePath);
            if (includeKeys.isEmpty() && excludeKeys.isEmpty()) {
                try {
                    out = OptionsExportModel.createOutputStream(targetFile);
                    this.copyFile(relativePath, out);
                }
                finally {
                    if (out != null) {
                        out.close();
                    }
                }
            }
            this.mergeProperties(relativePath, includeKeys, excludeKeys);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void mergeProperties(String relativePath, Set<String> includeKeys, Set<String> excludeKeys) throws IOException {
        EditableProperties targetProperties;
        File targetFile;
        if (!includeKeys.isEmpty()) {
            this.currentProperties.keySet().retainAll(includeKeys);
        }
        this.currentProperties.keySet().removeAll(excludeKeys);
        LOGGER.log(Level.FINE, "  Keys merged with existing properties: {0}", this.currentProperties.keySet());
        if (this.currentProperties.isEmpty()) {
            return;
        }
        targetProperties = new EditableProperties(false);
        InputStream in = null;
        targetFile = new File(this.targetUserdir, relativePath);
        try {
            if (targetFile.exists()) {
                in = new FileInputStream(targetFile);
                targetProperties.load(in);
            }
        }
        finally {
            if (in != null) {
                in.close();
            }
        }
        for (Map.Entry entry : this.currentProperties.entrySet()) {
            targetProperties.put((String)entry.getKey(), (String)entry.getValue());
        }
        OutputStream out = null;
        try {
            out = OptionsExportModel.createOutputStream(targetFile);
            targetProperties.store(out);
        }
        finally {
            if (out != null) {
                out.close();
            }
        }
    }

    private void copyFileOrProperties(String relativePath, Set<String> includeKeys, Set<String> excludeKeys, OutputStream out) throws IOException {
        if (includeKeys.isEmpty() && excludeKeys.isEmpty()) {
            this.copyFile(relativePath, out);
        } else {
            if (!includeKeys.isEmpty()) {
                this.currentProperties.keySet().retainAll(includeKeys);
            }
            this.currentProperties.keySet().removeAll(excludeKeys);
            LOGGER.log(Level.FINE, "  Only keys: {0}", this.currentProperties.keySet());
            this.currentProperties.store(out);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private EditableProperties getProperties(String relativePath) throws IOException {
        EditableProperties properties = new EditableProperties(false);
        InputStream in = null;
        try {
            in = this.getInputStream(relativePath);
            properties.load(in);
        }
        finally {
            if (in != null) {
                in.close();
            }
        }
        return properties;
    }

    private InputStream getInputStream(String relativePath) throws IOException {
        if (this.source.isFile()) {
            ZipFile zipFile = new ZipFile(this.source);
            ZipEntry zipEntry = zipFile.getEntry(relativePath);
            return zipFile.getInputStream(zipEntry);
        }
        return new FileInputStream(new File(this.source, relativePath));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void copyFile(String relativePath, OutputStream out) throws IOException {
        InputStream in = null;
        try {
            in = this.getInputStream(relativePath);
            FileUtil.copy((InputStream)in, (OutputStream)out);
        }
        finally {
            if (in != null) {
                in.close();
            }
        }
    }

    private static void ensureParent(File file) throws IOException {
        File parent = file.getParentFile();
        if (parent != null && !parent.exists() && !parent.mkdirs()) {
            throw new IOException("Cannot create folder: " + parent.getAbsolutePath());
        }
    }

    static List<String> listZipFile(File file) throws IOException {
        ArrayList<String> relativePaths = new ArrayList<String>();
        ZipFile zipFile = new ZipFile(file);
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            ZipEntry zipEntry = entries.nextElement();
            if (zipEntry.isDirectory()) continue;
            relativePaths.add(zipEntry.getName());
        }
        return relativePaths;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void createZipFile(File targetFile, File sourceDir, List<String> relativePaths) throws IOException {
        OptionsExportModel.ensureParent(targetFile);
        ZipOutputStream out = null;
        try {
            out = new ZipOutputStream(OptionsExportModel.createOutputStream(targetFile));
            for (String relativePath : relativePaths) {
                LOGGER.finest("Adding to zip: " + relativePath);
                out.putNextEntry(new ZipEntry(relativePath));
                FileInputStream in = null;
                try {
                    in = new FileInputStream(new File(sourceDir, relativePath));
                    FileUtil.copy((InputStream)in, (OutputStream)out);
                }
                finally {
                    if (in != null) {
                        in.close();
                    }
                }
                out.closeEntry();
            }
            OptionsExportModel.createProductInfo(out);
            out.close();
        }
        finally {
            if (out != null) {
                out.close();
            }
        }
    }

    private static void createProductInfo(ZipOutputStream out) throws IOException {
        String productVersion = MessageFormat.format(NbBundle.getBundle((String)"org.netbeans.core.startup.Bundle").getString("currentVersion"), System.getProperty("netbeans.buildnumber"));
        String os = System.getProperty("os.name", "unknown") + ", " + System.getProperty("os.version", "unknown") + ", " + System.getProperty("os.arch", "unknown");
        String java = System.getProperty("java.version", "unknown") + ", " + System.getProperty("java.vm.name", "unknown") + ", " + System.getProperty("java.vm.version", "");
        out.putNextEntry(new ZipEntry("build.info"));
        PrintWriter writer = new PrintWriter(out);
        writer.println("ProductVersion=" + productVersion);
        writer.println("OS=" + os);
        writer.println("Java=" + java);
        writer.println("Userdir=" + System.getProperty("netbeans.user"));
        writer.flush();
        out.closeEntry();
    }

    private static OutputStream createOutputStream(File file) throws IOException {
        if (OptionsExportModel.containsConfig(file)) {
            String rootPath;
            file = file.getCanonicalFile();
            File root = FileUtil.toFile((FileObject)FileUtil.getConfigRoot());
            String filePath = file.getPath();
            if (filePath.startsWith(rootPath = root.getPath())) {
                String res = filePath.substring(rootPath.length()).replace(File.separatorChar, '/');
                try {
                    FileObject fo = FileUtil.createData((FileObject)FileUtil.getConfigRoot(), (String)res);
                    if (fo != null) {
                        return fo.getOutputStream();
                    }
                }
                catch (SyncFailedException ex) {
                    LOGGER.log(Level.INFO, "File already exists: {0}", filePath);
                }
                catch (IOException ex) {
                    LOGGER.log(Level.INFO, "IOException while getting output stream: {0}", filePath);
                }
            }
        }
        OptionsExportModel.ensureParent(file);
        return new FileOutputStream(file);
    }

    private static boolean containsConfig(File file) {
        while (file != null) {
            if (file.getName().equals("config")) {
                return true;
            }
            file = file.getParentFile();
        }
        return false;
    }

    class Category {
        private static final String INCLUDE = "include";
        private static final String EXCLUDE = "exclude";
        private static final String DISPLAY_NAME = "displayName";
        private FileObject categoryFO;
        private String displayName;
        private List<Item> items;
        private State state;

        public Category(FileObject fo, String displayName) {
            this.state = State.DISABLED;
            this.categoryFO = fo;
            this.displayName = displayName;
        }

        private void addItem(String displayName, String includes, String excludes) {
            this.items.add(new Item(displayName, includes, excludes));
        }

        private void resolveGroups(String dispName, String include, String exclude) {
            LOGGER.fine("resolveGroups include=" + include);
            List applicablePaths = OptionsExportModel.this.getApplicablePaths(Collections.singleton(include), Collections.singleton(exclude));
            HashSet<String> groups = new HashSet<String>();
            Pattern p = Pattern.compile(include);
            for (String path : applicablePaths) {
                String group;
                Matcher m = p.matcher(path);
                m.matches();
                if (m.groupCount() != 1 || (group = m.group(1)) == null) continue;
                groups.add(group);
            }
            LOGGER.fine("GROUPS=" + groups);
            Iterator i$ = groups.iterator();
            while (i$.hasNext()) {
                String group;
                String newDisplayName = group = (String)i$.next();
                if (dispName.contains("{")) {
                    newDisplayName = MessageFormat.format(dispName, group);
                }
                this.addItem(newDisplayName, include.replace("([^/]*)", group), exclude);
            }
        }

        public List<Item> getItems() {
            if (this.items == null) {
                this.items = Collections.synchronizedList(new ArrayList());
                FileObject[] itemsFOs = this.categoryFO.getChildren();
                List sortedItems = FileUtil.getOrder(Arrays.asList(itemsFOs), (boolean)false);
                for (FileObject itemFO : itemsFOs = sortedItems.toArray((T[])new FileObject[sortedItems.size()])) {
                    String exclude;
                    String dispName = (String)itemFO.getAttribute("displayName");
                    assert (dispName != null);
                    String include = (String)itemFO.getAttribute("include");
                    if (include == null) {
                        include = "";
                    }
                    if ((exclude = (String)itemFO.getAttribute("exclude")) == null) {
                        exclude = "";
                    }
                    if (include.contains("([^/]*)")) {
                        this.resolveGroups(dispName, include, exclude);
                        continue;
                    }
                    this.addItem(dispName, include, exclude);
                }
            }
            return this.items;
        }

        public String getName() {
            return this.categoryFO.getNameExt();
        }

        public String getDisplayName() {
            return this.displayName;
        }

        public void setState(State state) {
            this.state = state;
            this.updateItems(state);
        }

        public State getState() {
            return this.state;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public boolean isApplicable() {
            if (this.items == null) {
                return false;
            }
            List<Item> list = this.items;
            synchronized (list) {
                for (Item item : this.items) {
                    if (!item.isApplicable()) continue;
                    return true;
                }
            }
            return false;
        }

        public String toString() {
            return this.getDisplayName() + ", state=" + (Object)((Object)this.getState());
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void updateItems(State state) {
            List<Item> list = this.items;
            synchronized (list) {
                for (Item item : this.items) {
                    if (state == State.PARTIAL || !item.isApplicable()) continue;
                    item.setEnabled(state.toBoolean());
                }
            }
        }
    }

    static enum State {
        ENABLED(Boolean.TRUE),
        DISABLED(Boolean.FALSE),
        PARTIAL(null);
        
        private final Boolean bool;

        private State(Boolean bool) {
            this.bool = bool;
        }

        public Boolean toBoolean() {
            return this.bool;
        }

        public static State valueOf(Boolean bool) {
            if (bool == null) {
                return PARTIAL;
            }
            return bool != false ? ENABLED : DISABLED;
        }
    }

    class Item {
        private String displayName;
        private String include;
        private String exclude;
        private boolean enabled;
        private boolean applicable;
        private boolean applicableInitialized;

        public Item(String displayName, String include, String exclude) {
            this.enabled = false;
            this.applicable = false;
            this.applicableInitialized = false;
            this.displayName = displayName;
            this.include = include;
            this.exclude = exclude;
            assert (this.assertIgnoredFolders(include));
        }

        public String getDisplayName() {
            return this.displayName;
        }

        public String getInclude() {
            return this.include;
        }

        public String getExclude() {
            return this.exclude;
        }

        public boolean isApplicable() {
            if (!this.applicableInitialized) {
                List applicablePaths = OptionsExportModel.this.getApplicablePaths(Collections.singleton(this.include), Collections.singleton(this.exclude));
                LOGGER.fine("    applicablePaths=" + applicablePaths);
                this.applicable = !applicablePaths.isEmpty();
                this.applicableInitialized = true;
            }
            return this.applicable;
        }

        public boolean isEnabled() {
            return this.enabled;
        }

        public void setEnabled(boolean newState) {
            if (this.enabled != newState) {
                this.enabled = newState;
                OptionsExportModel.this.includePatterns = null;
                OptionsExportModel.this.excludePatterns = null;
            }
        }

        public String toString() {
            return this.getDisplayName() + ", enabled=" + this.isEnabled();
        }

        private boolean assertIgnoredFolders(String pattern) {
            boolean result = true;
            for (String folder : IGNORED_FOLDERS) {
                assert (result = !pattern.contains(folder));
            }
            return result;
        }
    }

    private static enum ParserState {
        START,
        IN_KEY_PATTERN,
        AFTER_KEY_PATTERN,
        IN_BLOCK;
        

        private ParserState() {
        }
    }

}

