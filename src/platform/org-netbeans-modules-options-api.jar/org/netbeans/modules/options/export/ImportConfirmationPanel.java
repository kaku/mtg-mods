/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.export;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import javax.accessibility.AccessibleContext;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.NbBundle;

public class ImportConfirmationPanel
extends JPanel {
    private boolean confirmed = false;
    private JLabel lblMessage;

    public ImportConfirmationPanel() {
        this.initComponents();
    }

    void showConfirmation() {
        DialogDescriptor dd = new DialogDescriptor((Object)this, NbBundle.getMessage(ImportConfirmationPanel.class, (String)"ImportConfirmationPanel.title"), true, 0, DialogDescriptor.YES_OPTION, null);
        dd.setMessageType(2);
        DialogDisplayer.getDefault().createDialog(dd).setVisible(true);
        this.confirmed = DialogDescriptor.OK_OPTION.equals(dd.getValue());
    }

    boolean confirmed() {
        return this.confirmed;
    }

    private void initComponents() {
        this.lblMessage = new JLabel();
        this.setMinimumSize(new Dimension(450, 132));
        this.setPreferredSize(new Dimension(450, 132));
        this.setLayout(new BorderLayout());
        this.lblMessage.setText(NbBundle.getMessage(ImportConfirmationPanel.class, (String)"ImportConfirmationPanel.lblMessage.text"));
        this.lblMessage.setMaximumSize(new Dimension(900, 66));
        this.lblMessage.setMinimumSize(new Dimension(450, 66));
        this.lblMessage.setPreferredSize(new Dimension(450, 66));
        this.add((Component)this.lblMessage, "Center");
        this.lblMessage.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ImportConfirmationPanel.class, (String)"ImportConfirmationPanel.lblMessage.text"));
        this.lblMessage.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ImportConfirmationPanel.class, (String)"ImportConfirmationPanel.lblMessage.AD"));
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ImportConfirmationPanel.class, (String)"ImportConfirmationPanel.AN"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ImportConfirmationPanel.class, (String)"ImportConfirmationPanel.AD"));
    }
}

