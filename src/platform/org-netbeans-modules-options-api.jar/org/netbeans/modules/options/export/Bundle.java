/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.export;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String Export_Notification_DetailsText(Object path_where_the_exported_options_are_saved) {
        return NbBundle.getMessage(Bundle.class, (String)"Export_Notification_DetailsText", (Object)path_where_the_exported_options_are_saved);
    }

    static String OPT_RestartAfterImport() {
        return NbBundle.getMessage(Bundle.class, (String)"OPT_RestartAfterImport");
    }

    static String ProgressHandle_Export_DisplayName() {
        return NbBundle.getMessage(Bundle.class, (String)"ProgressHandle_Export_DisplayName");
    }

    private void Bundle() {
    }
}

