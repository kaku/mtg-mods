/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.netbeans.swing.outline.CheckRenderDataProvider
 *  org.netbeans.swing.outline.DefaultOutlineModel
 *  org.netbeans.swing.outline.Outline
 *  org.netbeans.swing.outline.RenderDataProvider
 *  org.netbeans.swing.outline.RowModel
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.LifecycleManager
 *  org.openide.NotificationLineSupport
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.awt.Actions
 *  org.openide.awt.Mnemonics
 *  org.openide.awt.Notification
 *  org.openide.awt.NotificationDisplayer
 *  org.openide.filesystems.FileChooserBuilder
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileUtil
 *  org.openide.modules.Places
 *  org.openide.util.Cancellable
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 *  org.openide.util.TaskListener
 */
package org.netbeans.modules.options.export;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;
import javax.swing.text.Document;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.modules.options.export.Bundle;
import org.netbeans.modules.options.export.ExportConfirmationPanel;
import org.netbeans.modules.options.export.ImportConfirmationPanel;
import org.netbeans.modules.options.export.OptionsExportModel;
import org.netbeans.swing.outline.CheckRenderDataProvider;
import org.netbeans.swing.outline.DefaultOutlineModel;
import org.netbeans.swing.outline.Outline;
import org.netbeans.swing.outline.RenderDataProvider;
import org.netbeans.swing.outline.RowModel;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.LifecycleManager;
import org.openide.NotificationLineSupport;
import org.openide.NotifyDescriptor;
import org.openide.awt.Actions;
import org.openide.awt.Mnemonics;
import org.openide.awt.Notification;
import org.openide.awt.NotificationDisplayer;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.modules.Places;
import org.openide.util.Cancellable;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.TaskListener;

public final class OptionsChooserPanel
extends JPanel {
    private static final Logger LOGGER = Logger.getLogger(OptionsChooserPanel.class.getName());
    private static final Icon NODE_ICON = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/options/export/defaultNode.gif", (boolean)true);
    private static final Color DISABLED_COLOR = UIManager.getColor("Label.disabledForeground");
    private DialogDescriptor dialogDescriptor;
    private PanelType panelType;
    private OptionsExportModel optionsExportModel;
    private static TreeModel treeModel;
    private static OptionsTreeDataProvider treeDataProvider;
    private static RequestProcessor.Task exportTask;
    private static final Icon OPTIONS_ICON;
    private JButton btnBrowse;
    private JLabel lblFile;
    private JLabel lblHint;
    private JScrollPane scrollPaneOptions;
    private JTextField txtFile;

    private OptionsChooserPanel() {
        this.initComponents();
        Mnemonics.setLocalizedText((AbstractButton)this.btnBrowse, (String)NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.btnBrowse"));
        Mnemonics.setLocalizedText((JLabel)this.lblFile, (String)NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.lblFile.text"));
        Mnemonics.setLocalizedText((JLabel)this.lblHint, (String)NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.lblHint.text"));
    }

    private void setOptionsExportModel(OptionsExportModel optionsExportModel) {
        this.optionsExportModel = optionsExportModel;
    }

    private OptionsExportModel getOptionsExportModel() {
        return this.optionsExportModel;
    }

    private static String getDefaultUserdirRoot() {
        String defaultUserdirRoot = System.getProperty("netbeans.default_userdir_root");
        if (defaultUserdirRoot == null) {
            defaultUserdirRoot = System.getProperty("user.home");
        }
        return defaultUserdirRoot;
    }

    public static void showExportDialog() {
        if (exportTask != null && !exportTask.isFinished()) {
            return;
        }
        LOGGER.fine("showExportDialog");
        File sourceUserdir = Places.getUserDirectory();
        final OptionsChooserPanel optionsChooserPanel = new OptionsChooserPanel();
        optionsChooserPanel.panelType = PanelType.EXPORT;
        optionsChooserPanel.setOptionsExportModel(new OptionsExportModel(sourceUserdir));
        optionsChooserPanel.loadOptions();
        optionsChooserPanel.txtFile.setText(OptionsChooserPanel.getDefaultUserdirRoot().concat(File.separator));
        optionsChooserPanel.txtFile.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent e) {
                optionsChooserPanel.dialogDescriptor.setValid(optionsChooserPanel.isPanelValid());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                optionsChooserPanel.dialogDescriptor.setValid(optionsChooserPanel.isPanelValid());
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                optionsChooserPanel.dialogDescriptor.setValid(optionsChooserPanel.isPanelValid());
            }
        });
        DialogDescriptor dd = new DialogDescriptor((Object)optionsChooserPanel, NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.export.title"), true, new Object[]{DialogDescriptor.OK_OPTION, DialogDescriptor.CANCEL_OPTION}, DialogDescriptor.OK_OPTION, 0, null, null);
        dd.createNotificationLineSupport();
        dd.setValid(false);
        ExportConfirmationPanel exportConfirmationPanel = null;
        if (!ExportConfirmationPanel.getSkipOption()) {
            final ExportConfirmationPanel finalExportConfirmationPanel = exportConfirmationPanel = new ExportConfirmationPanel();
            dd.setButtonListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    if (e.getSource() == DialogDescriptor.OK_OPTION) {
                        String passwords = NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.export.passwords.displayName");
                        Enumeration dfs = ((DefaultMutableTreeNode)treeModel.getRoot()).depthFirstEnumeration();
                        while (dfs.hasMoreElements()) {
                            Object nodeObj = dfs.nextElement();
                            DefaultMutableTreeNode node = (DefaultMutableTreeNode)nodeObj;
                            Object userObject = node.getUserObject();
                            if (!(userObject instanceof OptionsExportModel.Item) || !((OptionsExportModel.Item)userObject).getDisplayName().equals(passwords) || !treeDataProvider.isSelected(nodeObj).booleanValue()) continue;
                            finalExportConfirmationPanel.showConfirmation();
                        }
                    }
                }
            });
        }
        optionsChooserPanel.setDialogDescriptor(dd);
        DialogDisplayer.getDefault().createDialog(dd).setVisible(true);
        if (DialogDescriptor.OK_OPTION.equals(dd.getValue())) {
            String selectedFilePath;
            if (exportConfirmationPanel != null && !exportConfirmationPanel.confirmed()) {
                LOGGER.fine("Export canceled.");
                return;
            }
            Action save = Actions.forID((String)"Window", (String)"org.netbeans.core.windows.actions.SaveWindowsAction");
            if (save != null) {
                save.actionPerformed(new ActionEvent(optionsChooserPanel, 0, ""));
            }
            if ((selectedFilePath = optionsChooserPanel.getSelectedFilePath()).endsWith("/")) {
                selectedFilePath = selectedFilePath.substring(0, selectedFilePath.lastIndexOf("/"));
                String zipName = selectedFilePath.substring(selectedFilePath.lastIndexOf("/") + 1);
                selectedFilePath = selectedFilePath.concat("/").concat(zipName).concat(".zip");
            }
            if (!selectedFilePath.endsWith(".zip")) {
                selectedFilePath = selectedFilePath.concat(".zip");
            }
            final String targetPath = selectedFilePath;
            RequestProcessor RP = new RequestProcessor("OptionsChooserPanel Export", 1);
            Runnable runnable = new Runnable(){

                @Override
                public void run() {
                    Enumeration dfs = ((DefaultMutableTreeNode)treeModel.getRoot()).depthFirstEnumeration();
                    ArrayList<String> enabledItems = new ArrayList<String>();
                    while (dfs.hasMoreElements()) {
                        OptionsExportModel.Category category;
                        Object userObject = ((DefaultMutableTreeNode)dfs.nextElement()).getUserObject();
                        if (!(userObject instanceof OptionsExportModel.Category) || (category = (OptionsExportModel.Category)userObject).getState().equals((Object)OptionsExportModel.State.DISABLED)) continue;
                        List<OptionsExportModel.Item> items = ((OptionsExportModel.Category)userObject).getItems();
                        for (OptionsExportModel.Item item : items) {
                            if (!item.isEnabled()) continue;
                            enabledItems.add(category.getDisplayName().concat(item.getDisplayName()));
                        }
                    }
                    optionsChooserPanel.getOptionsExportModel().doExport(new File(targetPath), enabledItems);
                    NotificationDisplayer.getDefault().notify(NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.export.status.text"), OPTIONS_ICON, Bundle.Export_Notification_DetailsText(targetPath), null);
                    LOGGER.fine("Export finished.");
                }
            };
            exportTask = RP.create(runnable);
            final ProgressHandle ph = ProgressHandleFactory.createHandle((String)Bundle.ProgressHandle_Export_DisplayName(), (Cancellable)exportTask);
            exportTask.addTaskListener(new TaskListener(){

                public void taskFinished(Task task) {
                    ph.finish();
                }
            });
            ph.start();
            exportTask.schedule(0);
        }
    }

    public static void showImportDialog() {
        LOGGER.fine("showImportDialog");
        OptionsChooserPanel optionsChooserPanel = new OptionsChooserPanel();
        optionsChooserPanel.txtFile.setEditable(false);
        Mnemonics.setLocalizedText((JLabel)optionsChooserPanel.lblFile, (String)NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.import.lblFile.text"));
        Mnemonics.setLocalizedText((JLabel)optionsChooserPanel.lblHint, (String)NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.import.lblHint.text"));
        optionsChooserPanel.panelType = PanelType.IMPORT;
        DialogDescriptor dd = new DialogDescriptor((Object)optionsChooserPanel, NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.import.title"), true, new Object[]{DialogDescriptor.OK_OPTION, DialogDescriptor.CANCEL_OPTION}, DialogDescriptor.OK_OPTION, 0, null, null);
        dd.createNotificationLineSupport();
        dd.setValid(false);
        final boolean willRestart = "true".equals(Bundle.OPT_RestartAfterImport());
        final ImportConfirmationPanel confirmationPanel = new ImportConfirmationPanel();
        dd.setButtonListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                if (willRestart && e.getSource() == DialogDescriptor.OK_OPTION) {
                    confirmationPanel.showConfirmation();
                }
            }
        });
        optionsChooserPanel.setDialogDescriptor(dd);
        DialogDisplayer.getDefault().createDialog(dd).setVisible(true);
        boolean ok = DialogDescriptor.OK_OPTION.equals(dd.getValue());
        if (willRestart && !confirmationPanel.confirmed()) {
            LOGGER.fine("Import canceled.");
            ok = false;
        }
        if (ok) {
            File targetUserdir = Places.getUserDirectory();
            try {
                optionsChooserPanel.getOptionsExportModel().doImport(targetUserdir);
            }
            catch (IOException ioe) {
                Exceptions.attachLocalizedMessage((Throwable)ioe, (String)NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.import.error"));
                LOGGER.log(Level.SEVERE, ioe.getMessage(), ioe);
                return;
            }
            LOGGER.fine("Import finished.");
            if (willRestart) {
                LifecycleManager.getDefault().markForRestart();
                LifecycleManager.getDefault().exit();
            }
            try {
                FileUtil.getConfigRoot().getFileSystem().refresh(true);
            }
            catch (FileStateInvalidException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            Action reload = Actions.forID((String)"Window", (String)"org.netbeans.core.windows.actions.ReloadWindowsAction");
            if (reload != null) {
                reload.actionPerformed(new ActionEvent(optionsChooserPanel, 0, ""));
            }
        }
    }

    private void loadOptions() {
        assert (SwingUtilities.isEventDispatchThread());
        JLabel loadingLabel = new JLabel(NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.loading"));
        loadingLabel.setHorizontalAlignment(0);
        this.scrollPaneOptions.setViewportView(loadingLabel);
        Thread loadingThread = new Thread("Export/import options loading"){

            @Override
            public void run() {
                OptionsExportModel model = OptionsChooserPanel.this.getOptionsExportModel();
                LOGGER.fine("Loading started: " + OptionsChooserPanel.this.getOptionsExportModel());
                final TreeModel treeModel = OptionsChooserPanel.this.createOptionsTreeModel();
                LOGGER.fine("Loading finished: " + OptionsChooserPanel.this.getOptionsExportModel());
                if (model == OptionsChooserPanel.this.getOptionsExportModel()) {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            Object root;
                            LOGGER.fine("Changing options.");
                            OptionsChooserPanel.this.scrollPaneOptions.setViewportView((Component)OptionsChooserPanel.this.getOutline(treeModel));
                            if (OptionsChooserPanel.this.panelType == PanelType.IMPORT && (root = treeModel.getRoot()) != null) {
                                treeDataProvider.setSelected(root, Boolean.TRUE);
                            }
                            OptionsChooserPanel.this.dialogDescriptor.setValid(OptionsChooserPanel.this.isPanelValid());
                        }
                    });
                }
            }

        };
        loadingThread.start();
    }

    private Outline getOutline(TreeModel treeModel) {
        Outline outline = new Outline();
        outline.setModel((TableModel)DefaultOutlineModel.createOutlineModel((TreeModel)treeModel, (RowModel)new OptionsRowModel(), (boolean)true, (String)NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.outline.header.tree")));
        treeDataProvider = new OptionsTreeDataProvider();
        outline.setRenderDataProvider((RenderDataProvider)treeDataProvider);
        outline.getTableHeader().setReorderingAllowed(false);
        outline.setColumnHidingAllowed(false);
        outline.getAccessibleContext().setAccessibleName(NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.outline.AN"));
        outline.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.outline.AD"));
        this.lblHint.setLabelFor((Component)outline);
        final Outline out = outline;
        outline.addKeyListener((KeyListener)new KeyAdapter(){

            @Override
            public void keyPressed(KeyEvent evt) {
                if (evt.getKeyCode() == 32) {
                    int[] rows;
                    for (int row : rows = out.getSelectedRows()) {
                        if (row < 0) continue;
                        Object node = out.getValueAt(row, 0);
                        Boolean isSelected = treeDataProvider.isSelected(node);
                        if (isSelected == null) {
                            treeDataProvider.setSelected(node, Boolean.FALSE);
                            continue;
                        }
                        if (!treeDataProvider.isCheckEnabled(node)) continue;
                        treeDataProvider.setSelected(node, isSelected == false);
                    }
                }
            }
        });
        return outline;
    }

    private TreeModel createOptionsTreeModel() {
        double currentBuildNumber;
        LOGGER.fine("getOptionsTreeModel - " + this.getOptionsExportModel());
        String allLabel = NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.outline.all");
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(allLabel);
        ArrayList enabledItems = new ArrayList();
        double buildNumberDuringExport = 0.0;
        String nbBuildNumber = System.getProperty("netbeans.buildnumber");
        try {
            currentBuildNumber = Double.parseDouble(this.getOptionsExportModel().getBuildNumber(nbBuildNumber));
        }
        catch (NumberFormatException nfe) {
            LOGGER.log(Level.INFO, "Could not parse netbeans.buildnumber: {0}", nbBuildNumber);
            currentBuildNumber = 2.01403101706E11;
        }
        if (this.panelType == PanelType.IMPORT) {
            enabledItems = this.getOptionsExportModel().getEnabledItemsDuringExport(new File(this.txtFile.getText()));
            buildNumberDuringExport = this.getOptionsExportModel().getBuildNumberDuringExport(new File(this.txtFile.getText()));
        }
        for (OptionsExportModel.Category category : this.getOptionsExportModel().getCategories()) {
            LOGGER.fine("category=" + category);
            DefaultMutableTreeNode categoryNode = new DefaultMutableTreeNode(category);
            List<OptionsExportModel.Item> items = category.getItems();
            for (OptionsExportModel.Item item : items) {
                LOGGER.fine("    item=" + item);
                if (this.panelType != PanelType.EXPORT && !item.isApplicable()) continue;
                if (this.panelType == PanelType.IMPORT) {
                    if (enabledItems != null && !enabledItems.contains(category.getDisplayName().concat(item.getDisplayName())) && (!category.getDisplayName().equals("Projects") || !enabledItems.contains("GeneralAll Other Unspecified") || buildNumberDuringExport < 2.01310111528E11 || currentBuildNumber < 2.01403101706E11)) continue;
                    categoryNode.add(new DefaultMutableTreeNode(item));
                    continue;
                }
                categoryNode.add(new DefaultMutableTreeNode(item));
            }
            if (categoryNode.getChildCount() == 0) continue;
            rootNode.add(categoryNode);
            OptionsChooserPanel.updateCategoryNode(categoryNode);
        }
        if (rootNode.getChildCount() == 0) {
            rootNode = null;
        }
        treeModel = new DefaultTreeModel(rootNode);
        return treeModel;
    }

    private String getSelectedFilePath() {
        return this.txtFile.getText();
    }

    private void setDialogDescriptor(DialogDescriptor dd) {
        this.dialogDescriptor = dd;
    }

    /*
     * Enabled aggressive block sorting
     */
    private boolean isPanelValid() {
        File parent;
        if (this.panelType == PanelType.IMPORT) {
            if (this.txtFile.getText().length() == 0) {
                this.dialogDescriptor.getNotificationLineSupport().setWarningMessage(NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.import.file.warning"));
                return false;
            }
            if (this.getOptionsExportModel().getState() == OptionsExportModel.State.DISABLED) {
                this.dialogDescriptor.getNotificationLineSupport().setWarningMessage(NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.import.nooption.warning"));
                return false;
            }
            this.dialogDescriptor.getNotificationLineSupport().clearMessages();
            return true;
        }
        if (this.txtFile.getText().length() == 0) {
            this.dialogDescriptor.getNotificationLineSupport().setWarningMessage(NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.file.warning"));
            return false;
        }
        if (this.getOptionsExportModel().getState() == OptionsExportModel.State.DISABLED) {
            this.dialogDescriptor.getNotificationLineSupport().setWarningMessage(NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.nooption.warning"));
            return false;
        }
        String text = this.txtFile.getText();
        File file = parent = text.endsWith("/") ? new File(text) : new File(text).getParentFile();
        if (parent == null) {
            this.dialogDescriptor.getNotificationLineSupport().setWarningMessage(NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.noparent.warning"));
            return false;
        }
        if (parent.canWrite()) {
            this.dialogDescriptor.getNotificationLineSupport().clearMessages();
            return true;
        }
        this.dialogDescriptor.getNotificationLineSupport().setWarningMessage(NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.nowrite.warning"));
        return false;
    }

    private void initComponents() {
        this.lblHint = new JLabel();
        this.scrollPaneOptions = new JScrollPane();
        this.lblFile = new JLabel();
        this.txtFile = new JTextField();
        this.btnBrowse = new JButton();
        this.lblHint.setText(NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.lblHint.text"));
        this.lblFile.setLabelFor(this.txtFile);
        this.lblFile.setText(NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.lblFile.text"));
        this.btnBrowse.setText(NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.btnBrowse"));
        this.btnBrowse.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                OptionsChooserPanel.this.btnBrowseActionPerformed(evt);
            }
        });
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.scrollPaneOptions, -1, 406, 32767).addGroup(layout.createSequentialGroup().addComponent(this.lblFile).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.txtFile, -1, 163, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnBrowse)).addComponent(this.lblHint, -1, 406, 32767)).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lblFile).addComponent(this.txtFile, -2, -1, -2).addComponent(this.btnBrowse)).addGap(18, 18, 18).addComponent(this.lblHint).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.scrollPaneOptions, -1, 236, 32767).addContainerGap()));
        this.txtFile.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.txtFile.AD"));
        this.btnBrowse.getAccessibleContext().setAccessibleName(NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.btnBrowse.AN"));
        this.btnBrowse.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.btnBrowse.AD"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.AD"));
    }

    private void btnBrowseActionPerformed(ActionEvent evt) {
        FileChooserBuilder fileChooserBuilder = new FileChooserBuilder(OptionsChooserPanel.class);
        String defaultUserdirRoot = OptionsChooserPanel.getDefaultUserdirRoot();
        fileChooserBuilder.setDefaultWorkingDirectory(new File(defaultUserdirRoot));
        fileChooserBuilder.setFileFilter((FileFilter)new FileNameExtensionFilter("*.zip", "zip"));
        fileChooserBuilder.setAcceptAllFileFilterUsed(false);
        String approveText = NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.file.chooser.approve");
        fileChooserBuilder.setApproveText(approveText);
        if (this.panelType == PanelType.IMPORT) {
            fileChooserBuilder.setTitle(NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.import.file.chooser.title"));
            File selectedFile = fileChooserBuilder.showOpenDialog();
            if (selectedFile != null) {
                if (selectedFile.isDirectory() && !new File(selectedFile, "config").exists()) {
                    String message = NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.import.invalid.userdir", (Object)selectedFile);
                    NotifyDescriptor.Confirmation nd = new NotifyDescriptor.Confirmation((Object)message, NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.import.invalid.userdir.title"), 0);
                    DialogDisplayer.getDefault().notify((NotifyDescriptor)nd);
                    if (!NotifyDescriptor.YES_OPTION.equals(nd.getValue())) {
                        return;
                    }
                }
                this.txtFile.setText(selectedFile.getAbsolutePath());
                this.setOptionsExportModel(new OptionsExportModel(selectedFile));
                this.loadOptions();
            }
        } else {
            fileChooserBuilder.setTitle(NbBundle.getMessage(OptionsChooserPanel.class, (String)"OptionsChooserPanel.file.chooser.title"));
            File selectedFile = fileChooserBuilder.showSaveDialog();
            if (selectedFile != null) {
                String selectedFileName = selectedFile.getAbsolutePath();
                if (!selectedFileName.endsWith(".zip")) {
                    selectedFileName = selectedFileName + ".zip";
                }
                this.txtFile.setText(selectedFileName);
                this.dialogDescriptor.setValid(this.isPanelValid());
            }
        }
    }

    private static void updateCategoryNode(DefaultMutableTreeNode categoryNode) {
        int enabledCount = 0;
        int applicableCount = 0;
        for (int i = 0; i < categoryNode.getChildCount(); ++i) {
            Object userObject = ((DefaultMutableTreeNode)categoryNode.getChildAt(i)).getUserObject();
            OptionsExportModel.Item item = (OptionsExportModel.Item)userObject;
            if (!item.isApplicable()) continue;
            ++applicableCount;
            if (!item.isEnabled()) continue;
            ++enabledCount;
        }
        Object userObject = categoryNode.getUserObject();
        OptionsExportModel.Category category = (OptionsExportModel.Category)userObject;
        if (enabledCount == 0) {
            category.setState(OptionsExportModel.State.DISABLED);
        } else if (enabledCount == applicableCount) {
            category.setState(OptionsExportModel.State.ENABLED);
        } else {
            category.setState(OptionsExportModel.State.PARTIAL);
        }
    }

    static {
        OPTIONS_ICON = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/options/export/options.png", (boolean)true);
    }

    private static class FileNameExtensionFilter
    extends FileFilter {
        private final String description;
        private final String lowerCaseExtension;

        public FileNameExtensionFilter(String description, String extension) {
            assert (extension != null);
            this.description = description;
            this.lowerCaseExtension = extension.toLowerCase();
        }

        @Override
        public boolean accept(File f) {
            if (f != null) {
                String desiredExtension;
                if (f.isDirectory()) {
                    return true;
                }
                String fileName = f.getName();
                int i = fileName.lastIndexOf(46);
                if (i > 0 && i < fileName.length() - 1 && (desiredExtension = fileName.substring(i + 1).toLowerCase()).equals(this.lowerCaseExtension)) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public String getDescription() {
            return this.description;
        }
    }

    private class OptionsTreeDataProvider
    implements CheckRenderDataProvider {
        private OptionsTreeDataProvider() {
        }

        public Color getBackground(Object node) {
            return null;
        }

        public String getDisplayName(Object node) {
            if (node == null) {
                return null;
            }
            Object userObject = ((DefaultMutableTreeNode)node).getUserObject();
            if (userObject instanceof OptionsExportModel.Category) {
                return ((OptionsExportModel.Category)userObject).getDisplayName();
            }
            if (userObject instanceof OptionsExportModel.Item) {
                return ((OptionsExportModel.Item)userObject).getDisplayName();
            }
            return node.toString();
        }

        public Color getForeground(Object node) {
            if (node == null) {
                return null;
            }
            Object userObject = ((DefaultMutableTreeNode)node).getUserObject();
            if (userObject instanceof OptionsExportModel.Category ? !((OptionsExportModel.Category)userObject).isApplicable() : userObject instanceof OptionsExportModel.Item && !((OptionsExportModel.Item)userObject).isApplicable()) {
                return DISABLED_COLOR;
            }
            return null;
        }

        public Icon getIcon(Object o) {
            return NODE_ICON;
        }

        public String getTooltipText(Object o) {
            return null;
        }

        public boolean isHtmlDisplayName(Object o) {
            return false;
        }

        public boolean isCheckable(Object node) {
            return true;
        }

        public boolean isCheckEnabled(Object node) {
            if (node == null) {
                return true;
            }
            Object userObject = ((DefaultMutableTreeNode)node).getUserObject();
            if (userObject instanceof OptionsExportModel.Category ? !((OptionsExportModel.Category)userObject).isApplicable() : userObject instanceof OptionsExportModel.Item && !((OptionsExportModel.Item)userObject).isApplicable()) {
                return false;
            }
            return true;
        }

        public Boolean isSelected(Object node) {
            if (node == null) {
                return false;
            }
            if (((DefaultMutableTreeNode)node).isRoot()) {
                return OptionsChooserPanel.this.getOptionsExportModel().getState().toBoolean();
            }
            Object userObject = ((DefaultMutableTreeNode)node).getUserObject();
            if (userObject instanceof OptionsExportModel.Category) {
                return ((OptionsExportModel.Category)userObject).getState().toBoolean();
            }
            if (userObject instanceof OptionsExportModel.Item) {
                return ((OptionsExportModel.Item)userObject).isEnabled();
            }
            assert (false);
            return false;
        }

        public void setSelected(Object node, Boolean selected) {
            Object userObject = ((DefaultMutableTreeNode)node).getUserObject();
            if (((DefaultMutableTreeNode)node).isRoot()) {
                OptionsChooserPanel.this.getOptionsExportModel().setState(OptionsExportModel.State.valueOf(selected));
            } else if (userObject instanceof OptionsExportModel.Category) {
                ((OptionsExportModel.Category)userObject).setState(OptionsExportModel.State.valueOf(selected));
            } else if (userObject instanceof OptionsExportModel.Item) {
                ((OptionsExportModel.Item)userObject).setEnabled(selected);
                TreeNode parent = ((TreeNode)node).getParent();
                OptionsChooserPanel.updateCategoryNode((DefaultMutableTreeNode)parent);
            }
            ((DefaultTreeModel)treeModel).nodeChanged((TreeNode)node);
            OptionsChooserPanel.this.dialogDescriptor.setValid(OptionsChooserPanel.this.isPanelValid());
            OptionsChooserPanel.this.scrollPaneOptions.repaint();
        }
    }

    private class OptionsRowModel
    implements RowModel {
        private OptionsRowModel() {
        }

        public Class getColumnClass(int column) {
            return null;
        }

        public int getColumnCount() {
            return 0;
        }

        public String getColumnName(int column) {
            return null;
        }

        public Object getValueFor(Object node, int column) {
            return null;
        }

        public boolean isCellEditable(Object node, int column) {
            return false;
        }

        public void setValueFor(Object node, int column, Object value) {
        }
    }

    private static enum PanelType {
        EXPORT,
        IMPORT;
        

        private PanelType() {
        }
    }

}

