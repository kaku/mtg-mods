/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 */
package org.netbeans.modules.options.export;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

public class ExportConfirmationPanel
extends JPanel {
    private static final String PROP_SKIP_PASSWORDS_WARNING = "SkipPasswordsWarning";
    private boolean confirmed = true;
    private JCheckBox cbSkip;
    private JLabel lblMessage;

    public ExportConfirmationPanel() {
        this.initComponents();
        Mnemonics.setLocalizedText((AbstractButton)this.cbSkip, (String)NbBundle.getMessage(ExportConfirmationPanel.class, (String)"ExportConfirmationPanel.cbSkip.text"));
        String message = NbBundle.getMessage(ExportConfirmationPanel.class, (String)"ExportConfirmationPanel.lblMessage.text");
        this.lblMessage.setText("<html>" + message + "</html>");
        this.cbSkip.setSelected(ExportConfirmationPanel.getSkipOption());
    }

    void showConfirmation() {
        DialogDescriptor dd = new DialogDescriptor((Object)this, NbBundle.getMessage(ExportConfirmationPanel.class, (String)"ExportConfirmationPanel.title"), true, 0, DialogDescriptor.YES_OPTION, null);
        dd.setMessageType(2);
        DialogDisplayer.getDefault().createDialog(dd).setVisible(true);
        if (DialogDescriptor.OK_OPTION.equals(dd.getValue())) {
            this.confirmed = true;
            this.storeSkipOption();
        } else {
            this.confirmed = false;
        }
    }

    boolean confirmed() {
        return this.confirmed;
    }

    boolean skipNextTime() {
        return this.cbSkip.isSelected();
    }

    private void storeSkipOption() {
        NbPreferences.forModule(ExportConfirmationPanel.class).putBoolean("SkipPasswordsWarning", this.skipNextTime());
    }

    static boolean getSkipOption() {
        return NbPreferences.forModule(ExportConfirmationPanel.class).getBoolean("SkipPasswordsWarning", false);
    }

    private void initComponents() {
        this.cbSkip = new JCheckBox();
        this.lblMessage = new JLabel();
        this.setMinimumSize(new Dimension(450, 112));
        this.setPreferredSize(new Dimension(450, 112));
        this.setLayout(new BorderLayout());
        this.cbSkip.setText(NbBundle.getMessage(ExportConfirmationPanel.class, (String)"ExportConfirmationPanel.cbSkip.text"));
        this.cbSkip.setMaximumSize(new Dimension(900, 56));
        this.cbSkip.setMinimumSize(new Dimension(450, 56));
        this.cbSkip.setPreferredSize(new Dimension(450, 56));
        this.add((Component)this.cbSkip, "South");
        this.cbSkip.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ExportConfirmationPanel.class, (String)"ExportConfirmationPanel.cbSkip.AN"));
        this.cbSkip.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ExportConfirmationPanel.class, (String)"ExportConfirmationPanel.cbSkip.AD"));
        this.lblMessage.setText(NbBundle.getMessage(ExportConfirmationPanel.class, (String)"ExportConfirmationPanel.lblMessage.text"));
        this.lblMessage.setMaximumSize(new Dimension(900, 56));
        this.lblMessage.setMinimumSize(new Dimension(450, 56));
        this.lblMessage.setPreferredSize(new Dimension(450, 56));
        this.add((Component)this.lblMessage, "North");
        this.lblMessage.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ExportConfirmationPanel.class, (String)"ExportConfirmationPanel.lblMessage.text"));
        this.lblMessage.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ExportConfirmationPanel.class, (String)"ExportConfirmationPanel.lblMessage.AD"));
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ExportConfirmationPanel.class, (String)"ExportConfirmationPanel.AN"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ExportConfirmationPanel.class, (String)"ExportConfirmationPanel.AD"));
    }
}

