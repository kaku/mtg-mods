/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.options;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.netbeans.api.options.OptionsDisplayer;

public class OptionsWindowAction
implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent evt) {
        OptionsDisplayer.getDefault().open();
    }
}

