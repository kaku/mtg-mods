/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.awt.QuickSearch
 *  org.openide.awt.QuickSearch$Callback
 *  org.openide.awt.StatusDisplayer
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.HelpCtx
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.windows.WindowManager
 */
package org.netbeans.modules.options;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.RoundRectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.options.Bundle;
import org.netbeans.modules.options.CategoryModel;
import org.netbeans.modules.options.OptionsDisplayerImpl;
import org.netbeans.modules.options.advanced.AdvancedPanel;
import org.netbeans.modules.options.ui.VariableBorder;
import org.openide.awt.Mnemonics;
import org.openide.awt.QuickSearch;
import org.openide.awt.StatusDisplayer;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.HelpCtx;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.windows.WindowManager;

public class OptionsPanel
extends JPanel {
    private JPanel pCategories;
    private JPanel pCategories2;
    private JScrollPane categoriesScrollPane;
    private JPanel pOptions;
    private JPanel quickSearch;
    private Color origForeground;
    private String hintText;
    private JTextComponent searchTC;
    private String text2search = "";
    private boolean clearSearch = false;
    private CardLayout cLayout;
    private int selectedTabIndex = -1;
    private HashMap<String, JTabbedPane> categoryid2tabbedpane = new HashMap();
    private HashMap<String, ArrayList<String>> categoryid2words = new HashMap();
    private HashMap<String, HashMap<Integer, TabInfo>> categoryid2tabs = new HashMap();
    private ArrayList<String> disabledCategories = new ArrayList();
    private ArrayList<FileObject> advancedFOs = new ArrayList();
    private HashMap<String, Integer> dublicateKeywordsFOs = new HashMap();
    private HashMap<FileObject, Integer> fo2index = new HashMap();
    private Map<String, CategoryButton> buttons = new LinkedHashMap<String, CategoryButton>();
    private final boolean isMac = UIManager.getLookAndFeel().getID().equals("Aqua");
    private static final boolean isNimbus = UIManager.getLookAndFeel().getID().equals("Nimbus");
    private static final boolean isMetal = UIManager.getLookAndFeel() instanceof MetalLookAndFeel;
    private final boolean isGTK = UIManager.getLookAndFeel().getID().equals("GTK");
    private Color selected = this.isMac ? new Color(221, 221, 221) : OptionsPanel.getSelectionBackground();
    private Color selectedB = this.isMac ? new Color(183, 183, 183) : new Color(149, 106, 197);
    private Color highlighted = this.isMac ? new Color(221, 221, 221) : OptionsPanel.getHighlightBackground();
    private Color highlightedB = new Color(152, 180, 226);
    private Color iconViewBorder = new Color(127, 157, 185);
    private ControllerListener controllerListener;
    private final Color borderMac;
    private final Font labelFontMac;
    private CategoryModel categoryModel;
    private boolean applyPressed;
    private static final int BORDER_WIDTH = 4;
    private static final Border selBorder = new CompoundBorder(new CompoundBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4), new NimbusBorder()), BorderFactory.createEmptyBorder(4, 4, 4, 4));
    private static final Border normalBorder = BorderFactory.createEmptyBorder(9, 9, 11, 11);
    private static final short STATUS_NORMAL = 0;
    private static final short STATUS_SELECTED = 1;
    private static final short STATUS_HIGHLIGHTED = 2;
    private static final Color COL_GRADIENT1 = new Color(244, 245, 249);
    private static final Color COL_GRADIENT2 = new Color(163, 184, 203);
    private static final Color COL_GRADIENT3 = new Color(206, 227, 246);
    private static final Color COL_OVER_GRADIENT1 = new Color(244, 245, 249, 128);
    private static final Color COL_OVER_GRADIENT2 = new Color(163, 184, 203, 128);
    private static final Color COL_OVER_GRADIENT3 = new Color(206, 227, 246, 128);
    private static final boolean isDefaultTabBackground = Color.white.equals(OptionsPanel.getTabPanelBackground());

    private static String loc(String key) {
        return NbBundle.getMessage(OptionsPanel.class, (String)key);
    }

    public OptionsPanel(CategoryModel categoryModel) {
        this(null, categoryModel);
    }

    public OptionsPanel(String categoryID, CategoryModel categoryModel) {
        this.controllerListener = new ControllerListener();
        this.borderMac = new Color(141, 141, 141);
        this.labelFontMac = new Font("Lucida Grande", 0, 10);
        this.applyPressed = false;
        this.categoryModel = categoryModel;
        this.initUI(categoryID);
        if (this.getActionMap().get("SEARCH_OPTIONS") == null) {
            InputMap inputMap = this.getInputMap(2);
            if (Utilities.isMac()) {
                inputMap.put(KeyStroke.getKeyStroke(70, 4), "SEARCH_OPTIONS");
                this.hintText = Bundle.Filter_Textfield_Hint("\u2318+F");
            } else {
                inputMap.put(KeyStroke.getKeyStroke(70, 2), "SEARCH_OPTIONS");
                this.hintText = Bundle.Filter_Textfield_Hint("Ctrl+F");
            }
            this.getActionMap().put("SEARCH_OPTIONS", new SearchAction());
        }
    }

    private String getCategoryID(String categoryID) {
        return categoryID == null ? this.categoryModel.getCurrentCategoryID() : categoryID;
    }

    void initCurrentCategory(final String categoryID, final String subpath) {
        boolean isGeneralPanel = "General".equals(this.getCategoryID(categoryID));
        if (this.categoryModel.isLookupInitialized() || isGeneralPanel) {
            this.setCurrentCategory(this.categoryModel.getCategory(this.getCategoryID(categoryID)), subpath);
            this.initActions();
        } else {
            RequestProcessor.getDefault().post(new Runnable(){

                @Override
                public void run() {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            Frame[] all = Frame.getFrames();
                            if (all == null || all.length == 0) {
                                return;
                            }
                            Frame frame = all[0];
                            Cursor cursor = frame.getCursor();
                            frame.setCursor(Cursor.getPredefinedCursor(3));
                            OptionsPanel.this.setCursor(Cursor.getPredefinedCursor(3));
                            OptionsPanel.this.setCurrentCategory(OptionsPanel.this.categoryModel.getCategory(OptionsPanel.this.getCategoryID(categoryID)), subpath);
                            OptionsPanel.this.initActions();
                            frame.setCursor(cursor);
                            OptionsPanel.this.setCursor(cursor);
                        }
                    });
                }

            }, 500);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void setCurrentCategory(CategoryModel.Category category, String subpath) {
        this.setCursor(Cursor.getPredefinedCursor(3));
        try {
            if (category == null) {
                JPanel component = new JPanel(new BorderLayout());
                JLabel label = new JLabel(OptionsPanel.loc("CTL_Options_Search_Nothing_Found"));
                label.setHorizontalAlignment(0);
                label.setHorizontalTextPosition(0);
                component.add((Component)label, "Center");
                component.setSize(this.pOptions.getSize());
                component.setPreferredSize(this.pOptions.getPreferredSize());
                Dimension size = component.getSize();
                if (component.getParent() == null || !this.pOptions.equals(component.getParent())) {
                    this.pOptions.add((Component)component, label.getText());
                }
                this.cLayout.show(this.pOptions, label.getText());
                this.checkSize(size);
                this.firePropertyChange("buranhelpCtx", null, null);
            } else {
                CategoryModel.Category oldCategory = this.categoryModel.getCurrent();
                if (oldCategory != null) {
                    this.buttons.get(oldCategory.getID()).setNormal();
                }
                if (category != null) {
                    this.buttons.get(category.getID()).setSelected();
                }
                this.categoryModel.setCurrent(category);
                JComponent component = category.getComponent();
                category.update(this.controllerListener, false);
                Dimension size = component.getSize();
                if (component.getParent() == null || !this.pOptions.equals(component.getParent())) {
                    this.pOptions.add((Component)component, category.getCategoryName());
                }
                this.cLayout.show(this.pOptions, category.getCategoryName());
                this.checkSize(size);
                this.firePropertyChange("buranhelpCtx", null, null);
                if (subpath != null) {
                    category.setCurrentSubcategory(subpath);
                }
            }
        }
        finally {
            this.setCursor(null);
        }
    }

    public void setCategoryInstance(CategoryModel categoryInstance) {
        this.categoryModel = categoryInstance;
    }

    HelpCtx getHelpCtx() {
        if (this.categoryModel == null) {
            return null;
        }
        return this.categoryModel.getHelpCtx();
    }

    void update() {
        if (this.categoryModel == null) {
            return;
        }
        this.categoryModel.update(this.controllerListener, true);
    }

    void save(boolean applyButtonPressed) {
        if (this.categoryModel == null) {
            return;
        }
        this.applyPressed = applyButtonPressed;
        this.save();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                if (OptionsPanel.this.applyPressed) {
                    OptionsPanel.this.categoryModel.update(OptionsPanel.this.controllerListener, false);
                }
                OptionsPanel.this.applyPressed = false;
            }
        });
    }

    void save() {
        if (this.categoryModel == null) {
            return;
        }
        this.categoryModel.save();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                if (OptionsPanel.this.categoryModel == null) {
                    return;
                }
                OptionsPanel.this.clearSearchField();
                if (!OptionsPanel.this.applyPressed) {
                    OptionsPanel.this.categoryModel = null;
                }
            }
        });
    }

    void cancel() {
        if (this.categoryModel == null) {
            return;
        }
        this.clearSearchField();
        this.categoryModel.cancel();
        this.categoryModel = null;
    }

    boolean dataValid() {
        if (this.categoryModel == null) {
            return false;
        }
        return this.categoryModel.dataValid();
    }

    boolean isChanged() {
        if (this.categoryModel == null) {
            return false;
        }
        return this.categoryModel.isChanged();
    }

    boolean needsReinit() {
        if (this.categoryModel == null) {
            return false;
        }
        return this.categoryModel.needsReinit();
    }

    private void initUI(String categoryName) {
        this.getAccessibleContext().setAccessibleDescription(OptionsPanel.loc("ACS_OptionsPanel"));
        this.pOptions = new JPanel();
        this.cLayout = new CardLayout();
        this.pOptions.setLayout(this.cLayout);
        this.pOptions.setPreferredSize(this.getUserSize());
        JLabel label = new JLabel(OptionsPanel.loc("CTL_Loading_Options"));
        label.setHorizontalAlignment(0);
        this.pOptions.add((Component)label, label.getText());
        this.pCategories2 = new JPanel(new GridBagLayout());
        this.pCategories2.setBackground(OptionsPanel.getTabPanelBackground());
        this.pCategories2.setBorder(null);
        this.addCategoryButtons();
        this.quickSearch = new JPanel(new FlowLayout(2, 0, 0));
        this.quickSearch.setBackground(OptionsPanel.getTabPanelBackground());
        QuickSearch qs = QuickSearch.attach((JComponent)this.quickSearch, (Object)null, (QuickSearch.Callback)new OptionsQSCallback());
        qs.setAlwaysShown(true);
        JComponent searchPanel = (JComponent)this.quickSearch.getComponent(0);
        searchPanel.setToolTipText(Bundle.Filter_Textfield_Tooltip());
        this.searchTC = (JTextComponent)searchPanel.getComponent(searchPanel.getComponentCount() - 1);
        this.searchTC.setToolTipText(Bundle.Filter_Textfield_Tooltip());
        this.searchTC.addFocusListener(new FocusListener(){

            @Override
            public void focusGained(FocusEvent e) {
                OptionsPanel.this.showHint(false);
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (OptionsPanel.this.text2search.trim().isEmpty()) {
                    OptionsPanel.this.showHint(true);
                } else {
                    OptionsPanel.this.showHint(false);
                }
                if (e.getOppositeComponent() != null && e.getOppositeComponent().equals(OptionsPanel.this.quickSearch) && !OptionsPanel.this.clearSearch) {
                    OptionsPanel.this.searchTC.requestFocusInWindow();
                } else {
                    OptionsPanel.this.clearSearch = false;
                    if (e.getOppositeComponent() != null && e.getOppositeComponent().equals(OptionsPanel.this.quickSearch)) {
                        OptionsPanel.this.pOptions.requestFocusInWindow();
                    }
                }
            }
        });
        this.showHint(true);
        this.pCategories = new JPanel(new BorderLayout());
        this.pCategories.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.lightGray));
        this.pCategories.setBackground(OptionsPanel.getTabPanelBackground());
        this.categoriesScrollPane = new JScrollPane(this.pCategories2, 21, 30);
        this.categoriesScrollPane.setBorder(null);
        this.categoriesScrollPane.getHorizontalScrollBar().setUnitIncrement(16);
        this.pCategories.add("Center", this.categoriesScrollPane);
        this.pCategories.add("East", this.quickSearch);
        this.pCategories.setPreferredSize(new Dimension(this.pCategories.getPreferredSize().width, this.pCategories.getPreferredSize().height + this.categoriesScrollPane.getHorizontalScrollBar().getPreferredSize().height));
        this.setLayout(new BorderLayout(10, 10));
        this.pOptions.setBorder(new CompoundBorder(new VariableBorder(null, null, this.borderMac, null), BorderFactory.createEmptyBorder(0, 5, 5, 5)));
        this.add((Component)this.pCategories, "North");
        this.add((Component)this.pOptions, "Center");
        categoryName = this.getCategoryID(categoryName);
        if (categoryName != null) {
            CategoryModel.Category c = this.categoryModel.getCategory(this.getCategoryID(categoryName));
            CategoryButton b = this.buttons.get(categoryName);
            if (b != null) {
                b.setSelected();
            }
        }
        Boolean alwaysShow = UIManager.getLookAndFeelDefaults().getBoolean("OptionsPanel.showSearchPanel");
        qs.setAlwaysShown(alwaysShow.booleanValue());
    }

    private void clearSearchField() {
        this.searchTC.setText("");
        this.clearAllinQS();
    }

    private void showHint(boolean showHint) {
        if (this.origForeground == null) {
            this.origForeground = this.searchTC.getForeground();
        }
        if (showHint) {
            this.searchTC.setForeground(this.searchTC.getDisabledTextColor());
            this.searchTC.setText(this.hintText);
        } else {
            this.searchTC.setForeground(this.origForeground);
            this.searchTC.setText(this.text2search);
        }
    }

    private void computeOptionsWords() {
        Set<Map.Entry<String, CategoryModel.Category>> categories = this.categoryModel.getCategories();
        this.categoryid2tabs = new HashMap();
        for (Map.Entry<String, CategoryModel.Category> set : categories) {
            JComponent jcomp = set.getValue().getComponent();
            String id = set.getValue().getID();
            if (jcomp instanceof JTabbedPane) {
                this.categoryid2tabbedpane.put(id, (JTabbedPane)jcomp);
                continue;
            }
            if (jcomp instanceof AdvancedPanel) {
                this.categoryid2tabbedpane.put(id, (JTabbedPane)jcomp.getComponent(0));
                continue;
            }
            if (!(jcomp instanceof Container)) continue;
            this.handleAllComponents(jcomp, id, null, -1);
        }
        FileObject keywordsFOs = FileUtil.getConfigRoot().getFileObject(CategoryModel.OD_LAYER_KEYWORDS_FOLDER_NAME);
        for (FileObject keywordsFO : keywordsFOs.getChildren()) {
            this.handlePanel(keywordsFO);
        }
    }

    private void handlePanel(FileObject keywordsFO) {
        HashMap<Integer, TabInfo> categoryTabs;
        String location = keywordsFO.getAttribute("location").toString();
        String tabTitle = keywordsFO.getAttribute("tabTitle").toString();
        JTabbedPane pane = this.categoryid2tabbedpane.get(location);
        int tabIndex = pane == null ? -1 : pane.indexOfTab(tabTitle);
        HashSet<String> keywords = new HashSet<String>();
        keywords.add(location.toUpperCase());
        Enumeration attributes = keywordsFO.getAttributes();
        while (attributes.hasMoreElements()) {
            String attribute = (String)attributes.nextElement();
            if (!attribute.startsWith("keywords")) continue;
            String word = keywordsFO.getAttribute(attribute).toString();
            keywords.add(word.toUpperCase());
        }
        ArrayList words = this.categoryid2words.get(location);
        if (words == null) {
            words = new ArrayList();
        }
        HashSet<String> newWords = new HashSet<String>();
        for (String keyword : keywords) {
            if (words.contains(keyword)) continue;
            newWords.add(keyword);
        }
        words.addAll(newWords);
        this.categoryid2words.put(location, words);
        if (!this.categoryid2tabs.containsKey(location)) {
            this.categoryid2tabs.put(location, new HashMap());
        }
        TabInfo tabInfo = !(categoryTabs = this.categoryid2tabs.get(location)).containsKey(tabIndex) ? new TabInfo() : categoryTabs.get(tabIndex);
        tabInfo.addWords(keywords);
        categoryTabs.put(tabIndex, tabInfo);
        this.categoryid2tabs.put(location, categoryTabs);
    }

    private void handleAllComponents(Container container, String categoryID, JTabbedPane tabbedPane, int index) {
        Component[] components = container.getComponents();
        for (int i = 0; i < components.length; ++i) {
            Component component = components[i];
            if (component instanceof JTabbedPane) {
                if (this.categoryid2tabbedpane.get(categoryID) != null) continue;
                this.categoryid2tabbedpane.put(categoryID, (JTabbedPane)component);
                continue;
            }
            this.handleAllComponents((Container)component, categoryID, tabbedPane, index);
        }
    }

    private void clearAllinQS() {
        this.clearSearch = true;
        for (String id : this.categoryModel.getCategoryIDs()) {
            JTabbedPane pane = this.categoryid2tabbedpane.get(id);
            if (this.categoryid2tabs.get(id) != null && pane != null) {
                for (int i = 0; i < pane.getTabCount(); ++i) {
                    pane.setEnabledAt(i, true);
                }
            }
            this.buttons.get(id).setEnabled(true);
        }
        this.setCurrentCategory(this.categoryModel.getCurrent(), null);
        this.disabledCategories.clear();
        this.categoryModel.getCurrent().handleSuccessfulSearchInController(null, null);
    }

    private void initActions() {
        if (this.getActionMap().get("PREVIOUS") == null) {
            InputMap inputMap = this.getInputMap(2);
            inputMap.put(KeyStroke.getKeyStroke(37, 0), "PREVIOUS");
            this.getActionMap().put("PREVIOUS", new PreviousAction());
            inputMap.put(KeyStroke.getKeyStroke(39, 0), "NEXT");
            this.getActionMap().put("NEXT", new NextAction());
        }
    }

    private void addCategoryButtons() {
        Iterator<CategoryButton> it = this.buttons.values().iterator();
        while (it.hasNext()) {
            this.removeButton(it.next());
        }
        this.pCategories2.removeAll();
        this.buttons = new LinkedHashMap<String, CategoryButton>();
        String[] names = this.categoryModel.getCategoryIDs();
        for (int i = 0; i < names.length; ++i) {
            CategoryModel.Category category = this.categoryModel.getCategory(names[i]);
            this.addButton(category);
        }
        this.addFakeButton();
    }

    private CategoryButton addButton(CategoryModel.Category category) {
        JLabel button;
        int index = this.buttons.size();
        JLabel jLabel = button = isNimbus || this.isGTK ? new NimbusCategoryButton(category) : new CategoryButton(category);
        if (!this.isMac) {
            KeyStroke keyStroke = KeyStroke.getKeyStroke(button.getDisplayedMnemonic(), 8);
            this.getInputMap(2).put(keyStroke, button);
            this.getActionMap().put(button, new SelectAction(category));
        }
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = 18;
        gbc.fill = 1;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.gridx = index;
        gbc.gridy = 0;
        this.pCategories2.add((Component)button, gbc);
        this.buttons.put(category.getID(), (CategoryButton)button);
        return button;
    }

    private void removeButton(CategoryButton button) {
        KeyStroke keyStroke = KeyStroke.getKeyStroke(button.getDisplayedMnemonic(), 8);
        this.getInputMap(2).remove(keyStroke);
        this.getActionMap().remove(button);
    }

    private void addFakeButton() {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = 1;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.gridy = 0;
        this.pCategories2.add((Component)new JLabel(""), gbc);
    }

    private Dimension getInitSize() {
        return new Dimension(750, 500);
    }

    Dimension getUserSize() {
        int w = NbPreferences.forModule(OptionsPanel.class).getInt("OptionsWidth", this.getInitSize().width);
        int h = NbPreferences.forModule(OptionsPanel.class).getInt("OptionsHeight", this.getInitSize().height);
        return new Dimension(w, h);
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension d = super.getPreferredSize();
        Rectangle screenBounds = Utilities.getUsableScreenBounds();
        return new Dimension(Math.min(d.width, screenBounds.width - 101), Math.min(d.height, screenBounds.height - 101));
    }

    void storeUserSize() {
        Dimension d = this.pOptions.getSize();
        NbPreferences.forModule(OptionsPanel.class).putInt("OptionsWidth", d.width);
        NbPreferences.forModule(OptionsPanel.class).putInt("OptionsHeight", d.height);
        this.pOptions.setPreferredSize(d);
    }

    private boolean checkSize(Dimension componentSize) {
        boolean retval = false;
        Dimension prefSize = this.pOptions.getPreferredSize();
        Dimension userSize = this.getUserSize();
        componentSize = new Dimension(Math.max(componentSize.width, userSize.width), Math.max(componentSize.height, userSize.height));
        if (prefSize.width < componentSize.width || prefSize.height < componentSize.height) {
            Dimension newSize = new Dimension(Math.max(prefSize.width, componentSize.width), Math.max(prefSize.height, componentSize.height));
            this.pOptions.setPreferredSize(newSize);
            Window w = (Window)SwingUtilities.getAncestorOfClass(Window.class, this);
            this.invalidate();
            if (w != null) {
                w.pack();
            }
            retval = true;
        }
        return retval;
    }

    private static Color getTabPanelBackground() {
        if (isMetal || isNimbus) {
            Color res = UIManager.getColor("Tree.background");
            if (null == res) {
                res = Color.white;
            }
            return new Color(res.getRGB());
        }
        return Color.white;
    }

    private static Color getTabPanelForeground() {
        if (isMetal || isNimbus) {
            Color res = UIManager.getColor("Tree.foreground");
            if (null == res) {
                res = Color.black;
            }
            return new Color(res.getRGB());
        }
        return Color.black;
    }

    private static Color getSelectionBackground() {
        if ((isMetal || isNimbus) && !Color.white.equals(OptionsPanel.getTabPanelBackground())) {
            Color res = UIManager.getColor("Tree.selectionBackground");
            if (null == res) {
                res = Color.blue;
            }
            return new Color(res.getRGB());
        }
        return new Color(193, 210, 238);
    }

    private static Color getHighlightBackground() {
        if ((isMetal || isNimbus) && !Color.white.equals(OptionsPanel.getTabPanelBackground())) {
            Color res = UIManager.getColor("Tree.selectionBackground");
            if (null == res) {
                res = Color.blue;
            }
            return new Color(res.getRGB());
        }
        return new Color(224, 232, 246);
    }

    private boolean doPreviousNextAction(String[] ids, int start, int end, boolean nextAction) {
        if (nextAction) {
            for (int i = start; i < end; ++i) {
                String id = ids[i];
                if (!this.buttons.get(id).isEnabled()) continue;
                this.setCurrentCategory(this.categoryModel.getCategory(id), null);
                return false;
            }
        } else {
            for (int i = start; i > end; --i) {
                String id = ids[i];
                if (!this.buttons.get(id).isEnabled()) continue;
                this.setCurrentCategory(this.categoryModel.getCategory(id), null);
                return false;
            }
        }
        return true;
    }

    private static class NimbusBorder
    implements Border {
        private static final Color COLOR_BORDER = new Color(72, 93, 112, 255);
        private static final Color COLOR_SHADOW1 = new Color(72, 93, 112, 100);
        private static final Color COLOR_SHADOW2 = new Color(72, 93, 112, 60);

        private NimbusBorder() {
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            Graphics2D g2d = (Graphics2D)g;
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            Area rect = new Area(new RoundRectangle2D.Float(x, y, width - 3, height - 2, 4.0f, 4.0f));
            g2d.setColor(COLOR_BORDER);
            g2d.draw(rect);
            Area shadow = new Area(rect);
            AffineTransform tx = new AffineTransform();
            tx.translate(1.0, 1.0);
            shadow.transform(tx);
            shadow.subtract(rect);
            g2d.setColor(COLOR_SHADOW1);
            g2d.draw(shadow);
            shadow = new Area(rect);
            tx = new AffineTransform();
            tx.translate(2.0, 2.0);
            shadow.transform(tx);
            shadow.subtract(rect);
            g2d.setColor(COLOR_SHADOW2);
            g2d.draw(shadow);
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return new Insets(1, 1, 3, 3);
        }

        @Override
        public boolean isBorderOpaque() {
            return false;
        }
    }

    private class NimbusCategoryButton
    extends CategoryButton {
        private short status;

        public NimbusCategoryButton(CategoryModel.Category category) {
            super(category);
            this.status = 0;
            this.setOpaque(false);
            this.setBorder(normalBorder);
        }

        @Override
        protected void paintChildren(Graphics g) {
            super.paintChildren(g);
        }

        @Override
        protected void paintComponent(Graphics g) {
            if (isDefaultTabBackground && (this.status == 1 || this.status == 2)) {
                Insets in = this.getInsets();
                in.top -= 4;
                in.left -= 4;
                in.bottom -= 4;
                in.right -= 4;
                Graphics2D g2d = (Graphics2D)g.create();
                int width = this.getWidth() - in.left - in.right + 1;
                int height = this.getHeight() - in.top - in.bottom + 1;
                int topGradient = (int)(0.7 * (double)height);
                int bottomGradient = height - topGradient;
                Color c1 = this.status == 2 ? COL_OVER_GRADIENT1 : COL_GRADIENT1;
                Color c2 = this.status == 2 ? COL_OVER_GRADIENT2 : COL_GRADIENT2;
                Color c3 = this.status == 2 ? COL_OVER_GRADIENT3 : COL_GRADIENT3;
                g2d.setPaint(new GradientPaint(in.left, in.top, c1, in.left, in.top + topGradient, c2));
                g2d.fillRect(in.left, in.top, width, topGradient);
                g2d.setPaint(new GradientPaint(in.left, in.top + topGradient, c2, in.left, in.top + topGradient + bottomGradient, c3));
                g2d.fillRect(in.left, in.top + topGradient, width, bottomGradient);
                g2d.dispose();
            }
            super.paintComponent(g);
        }

        @Override
        void setHighlighted() {
            super.setHighlighted();
            this.status = 2;
            this.setBorder(selBorder);
            this.repaint();
        }

        @Override
        void setNormal() {
            this.setBorder(normalBorder);
            this.status = 0;
            this.repaint();
        }

        @Override
        void setSelected() {
            this.setBorder(selBorder);
            this.status = 1;
            this.repaint();
        }
    }

    class CategoryButton
    extends JLabel
    implements MouseListener {
        private final CategoryModel.Category category;

        CategoryButton(CategoryModel.Category category) {
            super(category.getIcon());
            this.category = category;
            Mnemonics.setLocalizedText((JLabel)this, (String)category.getCategoryName());
            this.getAccessibleContext().setAccessibleName(category.getCategoryName());
            this.getAccessibleContext().setAccessibleDescription(Bundle.CategoryButton_AccessibleDescription(category.getCategoryName()));
            this.setDisplayedMnemonic(0);
            this.setOpaque(true);
            this.setVerticalTextPosition(3);
            this.setHorizontalTextPosition(0);
            this.setHorizontalAlignment(0);
            this.addMouseListener(this);
            this.setFocusable(false);
            this.setFocusTraversalKeysEnabled(false);
            this.setForeground(OptionsPanel.getTabPanelForeground());
            if (OptionsPanel.this.isMac) {
                this.setFont(OptionsPanel.this.labelFontMac);
                this.setIconTextGap(2);
            }
            this.setNormal();
        }

        void setNormal() {
            if (OptionsPanel.this.isMac) {
                this.setBorder(new EmptyBorder(5, 6, 3, 6));
            } else {
                this.setBorder(new EmptyBorder(2, 4, 2, 4));
            }
            this.setBackground(OptionsPanel.getTabPanelBackground());
        }

        void setSelected() {
            if (OptionsPanel.this.isMac) {
                this.setBorder(new CompoundBorder(new VariableBorder(null, OptionsPanel.this.selectedB, null, OptionsPanel.this.selectedB), BorderFactory.createEmptyBorder(5, 5, 3, 5)));
            } else {
                this.setBorder(new CompoundBorder(new CompoundBorder(new LineBorder(OptionsPanel.getTabPanelBackground()), new LineBorder(OptionsPanel.this.selectedB)), new EmptyBorder(0, 2, 0, 2)));
            }
            this.setBackground(OptionsPanel.this.selected);
        }

        void setHighlighted() {
            if (!OptionsPanel.this.isMac) {
                this.setBorder(new CompoundBorder(new CompoundBorder(new LineBorder(OptionsPanel.getTabPanelBackground()), new LineBorder(OptionsPanel.this.highlightedB)), new EmptyBorder(0, 2, 0, 2)));
                this.setBackground(OptionsPanel.this.highlighted);
            }
            if (!this.category.isHighlited()) {
                CategoryButton b;
                if (OptionsPanel.this.categoryModel.getHighlitedCategoryID() != null && (b = (CategoryButton)OptionsPanel.this.buttons.get(OptionsPanel.this.categoryModel.getHighlitedCategoryID())) != null && !b.category.isCurrent()) {
                    b.setNormal();
                }
                OptionsPanel.this.categoryModel.setHighlited(this.category, true);
            }
        }

        @Override
        public void mouseClicked(MouseEvent e) {
        }

        @Override
        public void mousePressed(MouseEvent e) {
            if (((CategoryButton)OptionsPanel.this.buttons.get(this.category.getID())).isEnabled() && !OptionsPanel.this.isMac && OptionsPanel.this.categoryModel.getCurrent() != null) {
                this.setSelected();
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if (((CategoryButton)OptionsPanel.this.buttons.get(this.category.getID())).isEnabled() && !this.category.isCurrent() && this.category.isHighlited() && OptionsPanel.this.categoryModel.getCurrent() != null) {
                OptionsPanel.this.setCurrentCategory(this.category, null);
            }
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            CategoryButton button = (CategoryButton)OptionsPanel.this.buttons.get(this.category.getID());
            if (button != null && button.isEnabled() && !this.category.isCurrent() && OptionsPanel.this.categoryModel != null && OptionsPanel.this.categoryModel.getCurrent() != null) {
                this.setHighlighted();
            } else if (OptionsPanel.this.categoryModel != null) {
                OptionsPanel.this.categoryModel.setHighlited(OptionsPanel.this.categoryModel.getCategory(OptionsPanel.this.categoryModel.getHighlitedCategoryID()), false);
            }
        }

        @Override
        public void mouseExited(MouseEvent e) {
            if (!this.category.isCurrent() && !OptionsPanel.this.isMac && OptionsPanel.this.categoryModel.getCurrent() != null) {
                this.setNormal();
            }
        }
    }

    class ControllerListener
    implements PropertyChangeListener {
        ControllerListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            OptionsPanel.this.firePropertyChange("buran" + evt.getPropertyName(), null, null);
        }
    }

    private class NextAction
    extends AbstractAction {
        private NextAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            CategoryModel.Category next = OptionsPanel.this.categoryModel.getNextCategory();
            if (((CategoryButton)OptionsPanel.this.buttons.get(next.getID())).isEnabled()) {
                OptionsPanel.this.setCurrentCategory(next, null);
            } else {
                String currentID = OptionsPanel.this.categoryModel.getCurrentCategoryID();
                String[] ids = OptionsPanel.this.categoryModel.getCategoryIDs();
                int idx = Arrays.asList(ids).indexOf(currentID);
                if (idx + 1 < ids.length) {
                    if (OptionsPanel.this.doPreviousNextAction(ids, idx + 1, ids.length, true)) {
                        OptionsPanel.this.doPreviousNextAction(ids, 0, idx, true);
                    }
                } else {
                    OptionsPanel.this.doPreviousNextAction(ids, 0, idx, true);
                }
            }
        }
    }

    private class PreviousAction
    extends AbstractAction {
        private PreviousAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            CategoryModel.Category previous = OptionsPanel.this.categoryModel.getPreviousCategory();
            if (((CategoryButton)OptionsPanel.this.buttons.get(previous.getID())).isEnabled()) {
                OptionsPanel.this.setCurrentCategory(previous, null);
            } else {
                String currentID = OptionsPanel.this.categoryModel.getCurrentCategoryID();
                String[] ids = OptionsPanel.this.categoryModel.getCategoryIDs();
                int idx = Arrays.asList(ids).indexOf(currentID);
                if (idx - 1 > -1) {
                    if (OptionsPanel.this.doPreviousNextAction(ids, idx - 1, -1, false)) {
                        OptionsPanel.this.doPreviousNextAction(ids, ids.length - 1, idx, false);
                    }
                } else {
                    OptionsPanel.this.doPreviousNextAction(ids, ids.length - 1, idx, false);
                }
            }
        }
    }

    private class SearchAction
    extends AbstractAction {
        private SearchAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            OptionsPanel.this.showHint(false);
            OptionsPanel.this.searchTC.requestFocusInWindow();
        }
    }

    private class SelectCurrentAction
    extends AbstractAction {
        private SelectCurrentAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            CategoryModel.Category highlightedB = OptionsPanel.this.categoryModel.getCategory(OptionsPanel.this.categoryModel.getHighlitedCategoryID());
            if (highlightedB != null) {
                OptionsPanel.this.setCurrentCategory(highlightedB, null);
            }
        }
    }

    private class SelectAction
    extends AbstractAction {
        private CategoryModel.Category category;

        SelectAction(CategoryModel.Category category) {
            this.category = category;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            OptionsPanel.this.setCurrentCategory(this.category, null);
        }
    }

    final class OptionsQSCallback
    implements QuickSearch.Callback {
        private boolean initialized;

        OptionsQSCallback() {
            this.initialized = false;
        }

        public void quickSearchUpdate(String searchText) {
            if (!searchText.equalsIgnoreCase(OptionsPanel.this.hintText)) {
                OptionsPanel.this.text2search = searchText.trim();
            }
        }

        private void showWaitCursor() {
            Mutex.EVENT.readAccess(new Runnable(){

                @Override
                public void run() {
                    JFrame mainWindow = (JFrame)WindowManager.getDefault().getMainWindow();
                    mainWindow.getGlassPane().setCursor(Cursor.getPredefinedCursor(3));
                    mainWindow.getGlassPane().setVisible(true);
                    StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(OptionsDisplayerImpl.class, (String)"CTL_Searching_Options"));
                }
            });
        }

        private void hideWaitCursor() {
            Mutex.EVENT.readAccess(new Runnable(){

                @Override
                public void run() {
                    StatusDisplayer.getDefault().setStatusText("");
                    JFrame mainWindow = (JFrame)WindowManager.getDefault().getMainWindow();
                    mainWindow.getGlassPane().setVisible(false);
                    mainWindow.getGlassPane().setCursor(null);
                }
            });
        }

        private int getNextEnabledTabIndex(JTabbedPane pane, int currentIndex) {
            int i;
            for (i = currentIndex + 1; i < pane.getTabCount(); ++i) {
                if (!pane.isEnabledAt(i)) continue;
                return i;
            }
            for (i = 0; i < currentIndex; ++i) {
                if (!pane.isEnabledAt(i)) continue;
                return i;
            }
            return -1;
        }

        private boolean containsAllSearchWords(ArrayList<String> keywords, Collection<String> stWords) {
            Iterator<String> e = stWords.iterator();
            while (e.hasNext()) {
                if (this.containsSearchWord(keywords, e.next())) continue;
                return false;
            }
            return true;
        }

        private boolean containsSearchWord(ArrayList<String> keywords, String stWord) {
            Iterator<String> e = keywords.iterator();
            while (e.hasNext()) {
                if (!e.next().contains(stWord)) continue;
                return true;
            }
            return false;
        }

        private ArrayList<String> getAllMatchedKeywords(ArrayList<String> keywords, Collection<String> stWords) {
            ArrayList<String> allMatched = new ArrayList<String>();
            Iterator<String> e = stWords.iterator();
            while (e.hasNext()) {
                allMatched.addAll(this.getMatchedKeywords(keywords, e.next(), allMatched));
            }
            return allMatched;
        }

        private ArrayList<String> getMatchedKeywords(ArrayList<String> keywords, String stWord, ArrayList<String> allMatched) {
            ArrayList<String> matched = new ArrayList<String>();
            for (String next : keywords) {
                for (String s : next.split(",")) {
                    if (!(s = s.trim()).contains(stWord) || allMatched.contains(s) || matched.contains(s)) continue;
                    matched.add(s);
                }
            }
            return matched;
        }

        private void handleSearch(String searchText) {
            List<String> stWords = Arrays.asList(searchText.toUpperCase().split(" "));
            String exactCategory = null;
            String exactTabTitle = null;
            int exactTabIndex = -1;
            for (String id : OptionsPanel.this.categoryModel.getCategoryIDs()) {
                exactTabIndex = -1;
                ArrayList entry = (ArrayList)OptionsPanel.this.categoryid2words.get(id);
                if (entry != null) {
                    boolean found = this.containsAllSearchWords(entry, stWords);
                    for (String stWord : stWords) {
                        if (!id.toUpperCase().contains(stWord)) continue;
                        exactCategory = id;
                    }
                    if (found) {
                        ArrayList<String> matchedKeywords;
                        OptionsPanel.this.disabledCategories.remove(id);
                        ((CategoryButton)OptionsPanel.this.buttons.get(id)).setEnabled(true);
                        JTabbedPane pane = (JTabbedPane)OptionsPanel.this.categoryid2tabbedpane.get(id);
                        if (OptionsPanel.this.categoryid2tabs.get(id) != null) {
                            HashMap tabsInfo = (HashMap)OptionsPanel.this.categoryid2tabs.get(id);
                            boolean foundInNoTab = true;
                            for (Integer tabIndex : tabsInfo.keySet()) {
                                if (tabIndex != -1) {
                                    ArrayList<String> tabWords = ((TabInfo)tabsInfo.get(tabIndex)).getWords();
                                    boolean foundInTab = false;
                                    if (this.containsAllSearchWords(tabWords, stWords)) {
                                        foundInTab = true;
                                        foundInNoTab = false;
                                        exactTabIndex = tabIndex;
                                        if (exactCategory == null && exactTabTitle == null || exactCategory != null && exactCategory.equals(id)) {
                                            OptionsPanel.this.setCurrentCategory(OptionsPanel.this.categoryModel.getCategory(id), null);
                                        }
                                    }
                                    if (foundInTab) {
                                        for (String stWord2 : stWords) {
                                            if (!pane.getTitleAt(tabIndex).toUpperCase().contains(stWord2)) continue;
                                            exactTabTitle = pane.getTitleAt(tabIndex);
                                        }
                                        pane.setEnabledAt(tabIndex, true);
                                        if (exactTabIndex == tabIndex && (exactTabTitle == null || exactTabTitle != null && pane.getTitleAt(tabIndex).equals(exactTabTitle))) {
                                            pane.setSelectedIndex(tabIndex);
                                        }
                                        matchedKeywords = this.getAllMatchedKeywords(tabWords, stWords);
                                        OptionsPanel.this.categoryModel.getCurrent().handleSuccessfulSearchInController(searchText, matchedKeywords);
                                        continue;
                                    }
                                    pane.setEnabledAt(tabIndex, false);
                                    if (exactTabIndex != -1) continue;
                                    pane.setSelectedIndex(this.getNextEnabledTabIndex(pane, tabIndex));
                                    continue;
                                }
                                if (exactCategory == null && exactTabTitle == null || exactCategory != null && exactCategory.equals(id)) {
                                    OptionsPanel.this.setCurrentCategory(OptionsPanel.this.categoryModel.getCategory(id), null);
                                }
                                if (tabsInfo.size() != 1) continue;
                                foundInNoTab = false;
                                matchedKeywords = this.getAllMatchedKeywords(entry, stWords);
                                OptionsPanel.this.categoryModel.getCurrent().handleSuccessfulSearchInController(searchText, matchedKeywords);
                            }
                            if (!foundInNoTab) continue;
                            this.handleNotFound(id, exactCategory, exactTabTitle);
                            continue;
                        }
                        if (exactCategory == null && exactTabTitle == null || exactCategory != null && exactCategory.equals(id)) {
                            OptionsPanel.this.setCurrentCategory(OptionsPanel.this.categoryModel.getCategory(id), null);
                        }
                        matchedKeywords = this.getAllMatchedKeywords(entry, stWords);
                        OptionsPanel.this.categoryModel.getCurrent().handleSuccessfulSearchInController(searchText, matchedKeywords);
                        continue;
                    }
                    this.handleNotFound(id, exactCategory, exactTabTitle);
                    continue;
                }
                this.handleNotFound(id, exactCategory, exactTabTitle);
            }
        }

        private void handleNotFound(String id, String exactCategory, String exactTabTitle) {
            if (!OptionsPanel.this.disabledCategories.contains(id)) {
                OptionsPanel.this.disabledCategories.add(id);
            }
            JTabbedPane pane = (JTabbedPane)OptionsPanel.this.categoryid2tabbedpane.get(id);
            if (OptionsPanel.this.categoryid2tabs.get(id) != null && pane != null) {
                for (int i = 0; i < pane.getTabCount(); ++i) {
                    pane.setEnabledAt(i, false);
                }
            }
            ((CategoryButton)OptionsPanel.this.buttons.get(id)).setEnabled(false);
            if (OptionsPanel.this.disabledCategories.size() == OptionsPanel.this.buttons.size()) {
                OptionsPanel.this.setCurrentCategory(null, null);
            } else {
                for (String id3 : OptionsPanel.this.categoryModel.getCategoryIDs()) {
                    if (!((CategoryButton)OptionsPanel.this.buttons.get(id3)).isEnabled() || (exactCategory == null || !exactCategory.equals(id3)) && (exactCategory != null || exactTabTitle != null)) continue;
                    OptionsPanel.this.setCurrentCategory(OptionsPanel.this.categoryModel.getCategory(id3), null);
                    break;
                }
            }
        }

        public void showNextSelection(boolean forward) {
        }

        public String findMaxPrefix(String prefix) {
            return prefix;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void quickSearchConfirmed() {
            block5 : {
                if (OptionsPanel.this.text2search.length() == 0) {
                    OptionsPanel.this.clearAllinQS();
                    OptionsPanel.this.showHint(true);
                    return;
                }
                this.showWaitCursor();
                try {
                    if (!this.initialized) {
                        final String sText = OptionsPanel.this.text2search;
                        SwingUtilities.invokeLater(new Runnable(){

                            @Override
                            public void run() {
                                OptionsPanel.this.computeOptionsWords();
                                OptionsQSCallback.this.initialized = true;
                                OptionsQSCallback.this.handleSearch(sText);
                                OptionsPanel.this.showHint(false);
                            }
                        });
                        break block5;
                    }
                    this.handleSearch(OptionsPanel.this.text2search);
                    OptionsPanel.this.showHint(false);
                }
                finally {
                    this.hideWaitCursor();
                }
            }
        }

        public void quickSearchCanceled() {
            OptionsPanel.this.clearAllinQS();
            OptionsPanel.this.showHint(true);
        }

    }

    private class TabInfo {
        private ArrayList<String> words;

        public TabInfo() {
            this.words = new ArrayList();
        }

        public ArrayList<String> getWords() {
            return this.words;
        }

        public void addWord(String word) {
            this.words.add(word.toUpperCase());
        }

        public void addWords(Set<String> words) {
            for (String word : words) {
                this.addWord(word);
            }
        }
    }

}

