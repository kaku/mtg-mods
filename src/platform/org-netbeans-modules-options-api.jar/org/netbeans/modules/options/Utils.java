/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.options;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Lookup;

public class Utils {
    private static Map<String, FileObject> filesCache = new HashMap<String, FileObject>();
    public static final int ScrollBarUnitIncrement = 16;

    public static FileObject getFileObject(String name, String ext, boolean create) throws IOException {
        FileObject fileObject;
        FileObject r = filesCache.get(name + '.' + ext);
        if (r != null) {
            return r;
        }
        FileObject optionsFolder = FileUtil.getConfigFile((String)"Options");
        if (optionsFolder == null) {
            if (create) {
                optionsFolder = FileUtil.getConfigRoot().createFolder("Options");
            } else {
                return null;
            }
        }
        if ((fileObject = optionsFolder.getFileObject(name, ext)) == null) {
            if (create) {
                fileObject = optionsFolder.createData(name, ext);
            } else {
                return null;
            }
        }
        filesCache.put(name + '.' + ext, fileObject);
        return fileObject;
    }

    public static Enumeration getInputStreams(String name, String ext) throws IOException {
        ClassLoader classLoader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
        return classLoader.getResources("META-INF/options/" + name + "." + ext);
    }
}

