/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.output2;

import java.io.IOException;
import java.nio.ByteBuffer;
import org.netbeans.core.output2.BufferResource;

interface Storage {
    public BufferResource<ByteBuffer> getReadBuffer(int var1, int var2) throws IOException;

    public ByteBuffer getWriteBuffer(int var1) throws IOException;

    public int write(ByteBuffer var1) throws IOException;

    public void removeBytesFromEnd(int var1) throws IOException;

    public void dispose();

    public int size();

    public void flush() throws IOException;

    public void close() throws IOException;

    public boolean isClosed();

    public void shiftStart(int var1);
}

