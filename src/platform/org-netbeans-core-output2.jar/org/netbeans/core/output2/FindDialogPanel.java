/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.awt.Mnemonics
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 */
package org.netbeans.core.output2;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.util.Vector;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.ComboBoxEditor;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.netbeans.core.output2.Controller;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

class FindDialogPanel
extends JPanel {
    static final long serialVersionUID = 5048678953767663114L;
    private static final String KEY_REGEXP = "regExp";
    private static final String KEY_MATCHCASE = "matchCase";
    private static Reference<FindDialogPanel> panel = null;
    private JButton acceptButton;
    private static Vector<Object> history = new Vector();
    private JCheckBox chbMatchCase;
    private JCheckBox chbRegExp;
    protected JComboBox findWhat;
    protected JLabel findWhatLabel;
    private static String result;
    private static boolean regExp;
    private static boolean matchCase;

    FindDialogPanel() {
        regExp = NbPreferences.forModule(Controller.class).getBoolean("regExp", false);
        matchCase = NbPreferences.forModule(Controller.class).getBoolean("matchCase", false);
        this.initComponents();
        this.acceptButton = new JButton();
        Mnemonics.setLocalizedText((AbstractButton)this.chbRegExp, (String)NbBundle.getMessage(FindDialogPanel.class, (String)"LBL_Use_RegExp"));
        Mnemonics.setLocalizedText((AbstractButton)this.chbMatchCase, (String)NbBundle.getMessage(FindDialogPanel.class, (String)"LBL_Match_Case"));
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FindDialogPanel.class, (String)"ACSN_Find"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FindDialogPanel.class, (String)"ACSD_Find"));
        this.findWhat.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FindDialogPanel.class, (String)"ACSD_Find_What"));
        this.acceptButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FindDialogPanel.class, (String)"ACSD_FindBTN"));
        this.findWhat.setModel(new DefaultComboBoxModel<Object>(history));
        this.findWhat.getEditor().addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                FindDialogPanel.this.acceptButton.doClick();
            }
        });
        this.findWhatLabel.setFocusable(false);
    }

    public static FindDialogPanel getPanel() {
        FindDialogPanel result = null;
        if (panel != null) {
            result = panel.get();
        }
        if (result == null) {
            result = new FindDialogPanel();
            panel = new SoftReference<FindDialogPanel>(result);
        }
        return result;
    }

    void setFindText(String text) {
        int end = text.indexOf("\n");
        String txt = text;
        if (end > -1) {
            txt = text.substring(0, end);
        }
        if (!txt.equals(this.findWhat.getSelectedItem())) {
            this.findWhat.insertItemAt(txt, 0);
            this.findWhat.setSelectedIndex(0);
        }
        this.selectText();
    }

    private void selectText() {
        Component comp = this.findWhat.getEditor().getEditorComponent();
        if (comp instanceof JTextField) {
            JTextField fld = (JTextField)comp;
            fld.setSelectionStart(0);
            fld.setSelectionEnd(fld.getText().length());
        }
    }

    private void initComponents() {
        this.findWhatLabel = new JLabel();
        this.findWhat = new JComboBox();
        this.chbRegExp = new JCheckBox();
        this.chbMatchCase = new JCheckBox();
        this.setLayout(new GridBagLayout());
        this.findWhatLabel.setLabelFor(this.findWhat);
        this.findWhatLabel.setText(NbBundle.getMessage(FindDialogPanel.class, (String)"LBL_Find_What"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(5, 12, 5, 5);
        this.add((Component)this.findWhatLabel, gridBagConstraints);
        this.findWhat.setEditable(true);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 50;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(5, 12, 5, 5);
        this.add((Component)this.findWhat, gridBagConstraints);
        this.chbRegExp.setSelected(FindDialogPanel.regExp());
        this.chbRegExp.setText(NbBundle.getMessage(FindDialogPanel.class, (String)"LBL_Use_RegExp"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(5, 12, 5, 5);
        this.add((Component)this.chbRegExp, gridBagConstraints);
        this.chbMatchCase.setSelected(FindDialogPanel.matchCase());
        this.chbMatchCase.setText(NbBundle.getMessage(FindDialogPanel.class, (String)"LBL_Match_Case"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(5, 12, 5, 5);
        this.add((Component)this.chbMatchCase, gridBagConstraints);
    }

    private String getPattern() {
        return (String)this.findWhat.getSelectedItem();
    }

    private void updateHistory() {
        Object pattern = this.findWhat.getEditor().getItem();
        history.add(0, pattern);
        for (int i = FindDialogPanel.history.size() - 1; i > 0; --i) {
            if (!history.get(i).equals(pattern)) continue;
            history.remove(i);
            break;
        }
    }

    static String getResult(String selection, String dlgTitle, String comboLabel, String buttonText) {
        FindDialogPanel findPanel = FindDialogPanel.getPanel();
        Mnemonics.setLocalizedText((AbstractButton)findPanel.acceptButton, (String)NbBundle.getMessage(FindDialogPanel.class, (String)buttonText));
        Mnemonics.setLocalizedText((JLabel)findPanel.findWhatLabel, (String)NbBundle.getMessage(FindDialogPanel.class, (String)comboLabel));
        if (selection != null) {
            findPanel.setFindText(selection);
        }
        findPanel.selectText();
        DialogDescriptor dd = new DialogDescriptor((Object)findPanel, NbBundle.getMessage(FindDialogPanel.class, (String)dlgTitle), true, new Object[]{findPanel.acceptButton, DialogDescriptor.CANCEL_OPTION}, (Object)findPanel.acceptButton, 1, null, null);
        Object res = DialogDisplayer.getDefault().notify((NotifyDescriptor)dd);
        if (res.equals(findPanel.acceptButton)) {
            findPanel.updateHistory();
            regExp = findPanel.chbRegExp.getModel().isSelected();
            matchCase = findPanel.chbMatchCase.getModel().isSelected();
            NbPreferences.forModule(FindDialogPanel.class).putBoolean("regExp", regExp);
            NbPreferences.forModule(FindDialogPanel.class).putBoolean("matchCase", matchCase);
            result = findPanel.getPattern();
            return result;
        }
        result = null;
        return null;
    }

    static String result() {
        return result;
    }

    static boolean regExp() {
        return regExp;
    }

    static boolean matchCase() {
        return matchCase;
    }

}

