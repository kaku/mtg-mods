/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.lookup.Lookups
 *  org.openide.windows.IOColorLines
 *  org.openide.windows.IOColorPrint
 *  org.openide.windows.IOColors
 *  org.openide.windows.IOColors$OutputType
 *  org.openide.windows.IOContainer
 *  org.openide.windows.IOFolding
 *  org.openide.windows.IOFolding$FoldHandleDefinition
 *  org.openide.windows.IOPosition
 *  org.openide.windows.IOPosition$Position
 *  org.openide.windows.IOSelect
 *  org.openide.windows.IOSelect$AdditionalOperation
 *  org.openide.windows.IOTab
 *  org.openide.windows.InputOutput
 *  org.openide.windows.OutputListener
 *  org.openide.windows.OutputWriter
 */
package org.netbeans.core.output2;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.io.IOException;
import java.io.Reader;
import java.util.Set;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.SwingUtilities;
import org.netbeans.core.output2.AbstractLines;
import org.netbeans.core.output2.Controller;
import org.netbeans.core.output2.ErrWriter;
import org.netbeans.core.output2.IOEvent;
import org.netbeans.core.output2.Lines;
import org.netbeans.core.output2.NbIOProvider;
import org.netbeans.core.output2.NbWriter;
import org.netbeans.core.output2.OutWriter;
import org.netbeans.core.output2.OutputKind;
import org.netbeans.core.output2.options.OutputOptions;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.windows.IOColorLines;
import org.openide.windows.IOColorPrint;
import org.openide.windows.IOColors;
import org.openide.windows.IOContainer;
import org.openide.windows.IOFolding;
import org.openide.windows.IOPosition;
import org.openide.windows.IOSelect;
import org.openide.windows.IOTab;
import org.openide.windows.InputOutput;
import org.openide.windows.OutputListener;
import org.openide.windows.OutputWriter;

class NbIO
implements InputOutput,
Lookup.Provider {
    private Boolean focusTaken = null;
    private volatile boolean closed = false;
    private final String name;
    private OutputOptions options = OutputOptions.getDefault().makeCopy();
    private Action[] actions;
    private NbWriter out = null;
    private IOContainer ioContainer;
    private Lookup lookup;
    private IOTabImpl ioTab;
    private IOColorsImpl ioColors;
    private IOFoldingImpl.NbIoFoldHandleDefinition currentFold = null;
    private boolean streamClosed = false;
    private IOReader in = null;

    NbIO(String name, Action[] toolbarActions, IOContainer ioContainer) {
        this(name);
        this.actions = toolbarActions;
        this.ioContainer = ioContainer != null ? ioContainer : IOContainer.getDefault();
    }

    NbIO(String name) {
        this.name = name;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void closeInputOutput() {
        NbWriter currentOut;
        if (Controller.LOG) {
            Controller.log("CLOSE INPUT OUTPUT CALLED FOR " + this);
        }
        NbIO nbIO = this;
        synchronized (nbIO) {
            currentOut = this.out;
        }
        if (currentOut != null) {
            if (Controller.LOG) {
                Controller.log(" - Its output is non null, calling close() on " + (Object)((Object)currentOut));
            }
            currentOut.close();
        }
        NbIO.post(this, 7, true);
    }

    String getName() {
        return this.name;
    }

    IOContainer getIOContainer() {
        return this.ioContainer;
    }

    public OutputWriter getErr() {
        return ((NbWriter)this.getOut()).getErr();
    }

    synchronized NbWriter writer() {
        return this.out;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void dispose() {
        if (Controller.LOG) {
            Controller.log(this + ": IO " + this.getName() + " is being disposed");
        }
        OutWriter currentOut = null;
        IOReader currentIn = null;
        NbIO nbIO = this;
        synchronized (nbIO) {
            if (this.out != null) {
                if (Controller.LOG) {
                    Controller.log(this + ": Still has an OutWriter.  Disposing it");
                }
                currentOut = this.out();
                this.out = null;
                if (this.in != null) {
                    currentIn = this.in;
                    this.in = null;
                }
                this.focusTaken = null;
            }
        }
        if (currentOut != null) {
            currentOut.dispose();
        }
        if (currentIn != null) {
            currentIn.eof();
        }
        NbIOProvider.dispose(this);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public OutputWriter getOut() {
        NbIO nbIO = this;
        synchronized (nbIO) {
            if (this.out == null) {
                OutWriter realout = new OutWriter(this);
                this.out = new NbWriter(realout, this);
            }
            return this.out;
        }
    }

    synchronized OutWriter out() {
        return this.out == null ? null : this.out.out();
    }

    private OutWriter outOrException() {
        OutWriter outWriter = this.out();
        if (outWriter == null) {
            throw new IllegalStateException("No OutWriter available");
        }
        return outWriter;
    }

    void setClosed(boolean val) {
        this.closed = val;
    }

    public boolean isClosed() {
        return this.closed;
    }

    public boolean isErrSeparated() {
        return false;
    }

    public boolean isFocusTaken() {
        return Boolean.TRUE.equals(this.focusTaken);
    }

    synchronized boolean isStreamClosed() {
        return this.out == null ? true : this.streamClosed;
    }

    public void select() {
        if (Controller.LOG) {
            Controller.log(this + ": select");
        }
        NbIO.post(this, 6, true);
    }

    public void setErrSeparated(boolean value) {
    }

    public void setErrVisible(boolean value) {
    }

    public void setFocusTaken(boolean value) {
        this.focusTaken = value ? Boolean.TRUE : Boolean.FALSE;
        NbIO.post(this, 5, value);
    }

    public void setInputVisible(boolean value) {
        if (Controller.LOG) {
            Controller.log(this + ": SetInputVisible");
        }
        NbIO.post(this, 2, value);
    }

    public void setOutputVisible(boolean value) {
    }

    public void setStreamClosed(boolean value) {
        if (this.streamClosed != value) {
            this.streamClosed = value;
            NbIO.post(this, 8, value);
        }
    }

    public void setToolbarActions(Action[] a) {
        this.actions = a;
        NbIO.post(this, 10, a);
    }

    Action[] getToolbarActions() {
        return this.actions;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void reset() {
        IOReader currentIn;
        if (Controller.LOG) {
            Controller.log(this + ": reset");
        }
        this.closed = false;
        this.streamClosed = false;
        NbIO nbIO = this;
        synchronized (nbIO) {
            currentIn = this.in;
        }
        if (currentIn != null) {
            currentIn.eof();
            currentIn.reuse();
        }
        NbIO.post(this, 9, true);
    }

    private static void post(NbIO io, int command, boolean val) {
        IOEvent evt = new IOEvent(io, command, val);
        NbIO.post(evt);
    }

    private static void post(NbIO io, int command, Object data) {
        IOEvent evt = new IOEvent(io, command, data);
        NbIO.post(evt);
    }

    static void post(IOEvent evt) {
        if (SwingUtilities.isEventDispatchThread()) {
            if (Controller.LOG) {
                Controller.log("Synchronously dispatching " + evt + " from call on EQ");
            }
            evt.dispatch();
        } else {
            if (Controller.LOG) {
                Controller.log("Asynchronously posting " + evt + " to EQ");
            }
            EventQueue eq = Toolkit.getDefaultToolkit().getSystemEventQueue();
            eq.postEvent(evt);
        }
    }

    public String toString() {
        return "NbIO@" + System.identityHashCode(this) + " " + this.getName();
    }

    synchronized IOReader in() {
        return this.in;
    }

    public synchronized Reader getIn() {
        if (this.in == null) {
            this.in = new IOReader();
        }
        return this.in;
    }

    public Reader flushReader() {
        return this.getIn();
    }

    public synchronized Lookup getLookup() {
        if (this.lookup == null) {
            this.ioTab = new IOTabImpl();
            this.ioColors = new IOColorsImpl();
            this.lookup = Lookups.fixed((Object[])new Object[]{this.ioTab, this.ioColors, new IOPositionImpl(), new IOColorLinesImpl(), new IOColorPrintImpl(), new IOSelectImpl(), new IOFoldingImpl(), this.options});
        }
        return this.lookup;
    }

    Icon getIcon() {
        return this.ioTab != null ? this.ioTab.getIcon() : null;
    }

    String getToolTipText() {
        return this.ioTab != null ? this.ioTab.getToolTipText() : null;
    }

    Color getColor(IOColors.OutputType type) {
        return this.ioColors != null ? this.ioColors.getColor(type) : AbstractLines.getDefColors()[type.ordinal()];
    }

    private int getLastLineNumber() {
        return Math.max(0, this.out().getLines().getLineCount() - 2);
    }

    void setOptions(OutputOptions options) {
        this.options.assign(options);
    }

    OutputOptions getOptions() {
        return this.options;
    }

    private class IOFoldingImpl
    extends IOFolding {
        private IOFoldingImpl() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected IOFolding.FoldHandleDefinition startFold(boolean expanded) {
            OutWriter outWriter = NbIO.this.outOrException();
            synchronized (outWriter) {
                if (NbIO.this.currentFold != null) {
                    throw new IllegalStateException("The last fold hasn't been finished yet");
                }
                return new NbIoFoldHandleDefinition(null, NbIO.this.getLastLineNumber(), expanded);
            }
        }

        class NbIoFoldHandleDefinition
        extends IOFolding.FoldHandleDefinition {
            private final NbIoFoldHandleDefinition parent;
            private final int start;
            private int end;
            private NbIoFoldHandleDefinition nested;

            public NbIoFoldHandleDefinition(NbIoFoldHandleDefinition parent, int start, boolean expanded) {
                this.end = -1;
                this.nested = null;
                this.parent = parent;
                this.start = start;
                this.setCurrentFoldStart(start);
                this.setExpanded(expanded, false);
            }

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            public void finish() {
                OutWriter outWriter = NbIO.this.outOrException();
                synchronized (outWriter) {
                    if (this.nested != null) {
                        throw new IllegalStateException("Nested fold hasn't been finished.");
                    }
                    if (this.end != -1) {
                        throw new IllegalStateException("Fold has been already finished.");
                    }
                    if (this.parent == null) {
                        NbIO.this.currentFold = null;
                        this.setCurrentFoldStart(-1);
                    } else {
                        this.parent.nested = null;
                        this.setCurrentFoldStart(this.parent.start);
                    }
                    this.end = NbIO.this.getLastLineNumber();
                }
            }

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            public IOFolding.FoldHandleDefinition startFold(boolean expanded) {
                OutWriter outWriter = NbIO.this.outOrException();
                synchronized (outWriter) {
                    NbIoFoldHandleDefinition def;
                    if (this.end != -1) {
                        throw new IllegalStateException("The fold has been alredy finished.");
                    }
                    if (this.nested != null) {
                        throw new IllegalStateException("An unfinished nested fold exists.");
                    }
                    this.nested = def = new NbIoFoldHandleDefinition(this, NbIO.this.getLastLineNumber(), expanded);
                    return def;
                }
            }

            public final void setExpanded(boolean expanded) {
                this.setExpanded(expanded, true);
            }

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            private void setExpanded(boolean expanded, boolean expandParents) {
                OutWriter outWriter = NbIO.this.outOrException();
                synchronized (outWriter) {
                    if (expanded) {
                        if (expandParents) {
                            this.getLines().showFoldAndParentFolds(this.start);
                        } else {
                            this.getLines().showFold(this.start);
                        }
                    } else {
                        this.getLines().hideFold(this.start);
                    }
                }
            }

            private void setCurrentFoldStart(int foldStartIndex) {
                this.getLines().setCurrentFoldStart(foldStartIndex);
            }

            private AbstractLines getLines() {
                return (AbstractLines)NbIO.this.out().getLines();
            }
        }

    }

    private class IOColorsImpl
    extends IOColors {
        Color[] clrs;

        private IOColorsImpl() {
            this.clrs = new Color[IOColors.OutputType.values().length];
        }

        protected Color getColor(IOColors.OutputType type) {
            return this.clrs[type.ordinal()] != null ? this.clrs[type.ordinal()] : NbIO.this.options.getColorForType(type);
        }

        protected void setColor(IOColors.OutputType type, Color color) {
            this.clrs[type.ordinal()] = color;
            NbIO.post(NbIO.this, 14, (Object)type);
        }
    }

    private class IOSelectImpl
    extends IOSelect {
        private IOSelectImpl() {
        }

        protected void select(Set<IOSelect.AdditionalOperation> extraOps) {
            if (Controller.LOG) {
                Controller.log((Object)((Object)this) + ": IOSelect.select");
            }
            NbIO.post(NbIO.this, 15, extraOps);
        }
    }

    private class IOColorPrintImpl
    extends IOColorPrint {
        private IOColorPrintImpl() {
        }

        protected void print(CharSequence text, OutputListener listener, boolean important, Color color) throws IOException {
            OutWriter out = NbIO.this.out();
            if (out != null) {
                out.print(text, listener, important, color, null, OutputKind.OUT, false);
            }
        }
    }

    private class IOColorLinesImpl
    extends IOColorLines {
        private IOColorLinesImpl() {
        }

        protected void println(CharSequence text, OutputListener listener, boolean important, Color color) throws IOException {
            OutWriter out = NbIO.this.out();
            if (out != null) {
                out.print(text, listener, important, color, null, OutputKind.OUT, true);
            }
        }
    }

    private class PositionImpl
    implements IOPosition.Position {
        private int pos;

        public PositionImpl(int pos) {
            this.pos = pos;
        }

        public void scrollTo() {
            NbIO.post(NbIO.this, 13, new Integer(this.pos));
        }
    }

    private class IOPositionImpl
    extends IOPosition {
        private IOPositionImpl() {
        }

        protected IOPosition.Position currentPosition() {
            OutWriter out = NbIO.this.out();
            int size = 0;
            if (out != null) {
                size = out.getLines().getCharCount();
            }
            return new PositionImpl(size);
        }
    }

    private class IOTabImpl
    extends IOTab {
        Icon icon;
        String toolTip;

        private IOTabImpl() {
        }

        protected Icon getIcon() {
            return this.icon;
        }

        protected String getToolTipText() {
            return this.toolTip;
        }

        protected void setIcon(Icon icon) {
            this.icon = icon;
            NbIO.post(NbIO.this, 11, this.icon);
        }

        protected void setToolTipText(String text) {
            this.toolTip = text;
            NbIO.post(NbIO.this, 12, this.toolTip);
        }
    }

    class IOReader
    extends Reader {
        private boolean pristine;
        private boolean inputClosed;

        IOReader() {
            super(new StringBuffer());
            this.pristine = true;
            this.inputClosed = false;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void reuse() {
            this.pristine = true;
            Object object = this.lock;
            synchronized (object) {
                this.inputClosed = false;
            }
        }

        private StringBuffer buffer() {
            return (StringBuffer)this.lock;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void pushText(String txt) {
            if (Controller.LOG) {
                Controller.log(NbIO.this + ": Input text: " + txt);
            }
            Object object = this.lock;
            synchronized (object) {
                this.buffer().append(txt);
                this.lock.notifyAll();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void eof() {
            Object object = this.lock;
            synchronized (object) {
                try {
                    this.close();
                }
                catch (IOException ioe) {
                    Exceptions.printStackTrace((Throwable)ioe);
                }
            }
        }

        private void checkPristine() throws IOException {
            if (SwingUtilities.isEventDispatchThread()) {
                throw new IOException("Cannot call read() from the event thread, it will deadlock");
            }
            if (this.pristine) {
                NbIO.this.setInputVisible(true);
                this.pristine = false;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public int read(char[] cbuf, int off, int len) throws IOException {
            if (Controller.LOG) {
                Controller.log(NbIO.this + ":Input read: " + off + " len " + len);
            }
            this.checkPristine();
            Object object = this.lock;
            synchronized (object) {
                while (!this.inputClosed && this.buffer().length() == 0) {
                    try {
                        this.lock.wait();
                        continue;
                    }
                    catch (InterruptedException e) {
                        throw (IOException)new IOException("Interrupted: " + e.getMessage()).initCause(e);
                    }
                }
                if (this.inputClosed) {
                    this.reuse();
                    return -1;
                }
                int realLen = Math.min(this.buffer().length(), len);
                this.buffer().getChars(0, realLen, cbuf, off);
                this.buffer().delete(0, realLen);
                return realLen;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public int read() throws IOException {
            if (Controller.LOG) {
                Controller.log(NbIO.this + ": Input read one char");
            }
            this.checkPristine();
            Object object = this.lock;
            synchronized (object) {
                while (!this.inputClosed && this.buffer().length() == 0) {
                    try {
                        this.lock.wait();
                        continue;
                    }
                    catch (InterruptedException e) {
                        throw (IOException)new IOException("Interrupted: " + e.getMessage()).initCause(e);
                    }
                }
                if (this.inputClosed) {
                    this.reuse();
                    return -1;
                }
                char i = this.buffer().charAt(0);
                this.buffer().deleteCharAt(0);
                return i;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public boolean ready() throws IOException {
            Object object = this.lock;
            synchronized (object) {
                if (this.inputClosed) {
                    this.reuse();
                    return false;
                }
                return this.buffer().length() > 0;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public long skip(long n) throws IOException {
            if (Controller.LOG) {
                Controller.log(NbIO.this + ": Input skip " + n);
            }
            this.checkPristine();
            Object object = this.lock;
            synchronized (object) {
                while (!this.inputClosed && this.buffer().length() == 0) {
                    try {
                        this.lock.wait();
                        continue;
                    }
                    catch (InterruptedException e) {
                        throw (IOException)new IOException("Interrupted: " + e.getMessage()).initCause(e);
                    }
                }
                if (this.inputClosed) {
                    this.reuse();
                    return 0;
                }
                int realLen = Math.min(this.buffer().length(), (int)n);
                this.buffer().delete(0, realLen);
                return realLen;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void close() throws IOException {
            if (Controller.LOG) {
                Controller.log(NbIO.this + ": Input close");
            }
            NbIO.this.setInputVisible(false);
            Object object = this.lock;
            synchronized (object) {
                this.inputClosed = true;
                this.buffer().setLength(0);
                this.lock.notifyAll();
            }
        }

        public boolean isClosed() {
            return this.inputClosed;
        }
    }

}

