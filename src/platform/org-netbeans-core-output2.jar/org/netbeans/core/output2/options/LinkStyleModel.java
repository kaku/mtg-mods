/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.output2.options;

import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;
import org.netbeans.core.output2.options.Bundle;
import org.netbeans.core.output2.options.OutputOptions;

public class LinkStyleModel
implements ComboBoxModel {
    private LinkStyleItem none;
    private LinkStyleItem underline;
    private LinkStyleItem selectedItem;

    public LinkStyleModel() {
        this.none = new LinkStyleItem(OutputOptions.LinkStyle.NONE, Bundle.LBL_None());
        this.selectedItem = this.underline = new LinkStyleItem(OutputOptions.LinkStyle.UNDERLINE, Bundle.LBL_Underline());
    }

    @Override
    public void setSelectedItem(Object anItem) {
        if (!(anItem instanceof LinkStyleItem)) {
            throw new IllegalArgumentException();
        }
        this.selectedItem = (LinkStyleItem)anItem;
    }

    @Override
    public Object getSelectedItem() {
        return this.selectedItem;
    }

    @Override
    public int getSize() {
        return 2;
    }

    @Override
    public Object getElementAt(int index) {
        switch (index) {
            case 0: {
                return this.underline;
            }
            case 1: {
                return this.none;
            }
        }
        throw new IndexOutOfBoundsException();
    }

    @Override
    public void addListDataListener(ListDataListener l) {
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
    }

    OutputOptions.LinkStyle getLinkStyle() {
        return this.selectedItem.getLinkStyle();
    }

    void setLinkStyle(OutputOptions.LinkStyle style) {
        if (style != this.selectedItem.getLinkStyle()) {
            switch (style) {
                case NONE: {
                    this.selectedItem = this.none;
                    break;
                }
                case UNDERLINE: {
                    this.selectedItem = this.underline;
                    break;
                }
                default: {
                    throw new IllegalArgumentException();
                }
            }
        }
    }

    private LinkStyleItem linkStyleItemFor(OutputOptions.LinkStyle linkStyle) {
        switch (linkStyle) {
            case NONE: {
                return this.none;
            }
            case UNDERLINE: {
                return this.underline;
            }
        }
        throw new IllegalArgumentException();
    }

    public Object itemFor(OutputOptions.LinkStyle linkStyle) {
        return this.linkStyleItemFor(linkStyle);
    }

    private class LinkStyleItem {
        private OutputOptions.LinkStyle linkStyle;
        private String displayName;

        public LinkStyleItem(OutputOptions.LinkStyle linkStyle, String displayName) {
            this.linkStyle = linkStyle;
            this.displayName = displayName;
        }

        public OutputOptions.LinkStyle getLinkStyle() {
            return this.linkStyle;
        }

        public String toString() {
            return this.displayName;
        }
    }

}

