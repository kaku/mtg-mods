/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.plaf.LFCustoms
 *  org.openide.util.NbPreferences
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.windows.IOColors
 *  org.openide.windows.IOColors$OutputType
 */
package org.netbeans.core.output2.options;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.swing.UIManager;
import org.netbeans.core.output2.Controller;
import org.netbeans.swing.plaf.LFCustoms;
import org.openide.util.NbPreferences;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;
import org.openide.windows.IOColors;

public class OutputOptions {
    private static OutputOptions DEFAULT = null;
    private boolean initialized = false;
    private static final Logger LOG = Logger.getLogger(OutputOptions.class.getName());
    private static AtomicBoolean saveScheduled = new AtomicBoolean(false);
    private static final String PREFIX = "output.settings.";
    public static final String PROP_FONT = "font";
    private static final String PROP_FONT_FAMILY = "font.family";
    private static final String PROP_FONT_SIZE = "font.size";
    private static final String PROP_FONT_STYLE = "font.style";
    public static final String PROP_COLOR_STANDARD = "color.standard";
    public static final String PROP_COLOR_ERROR = "color.error";
    public static final String PROP_COLOR_INPUT = "color.input";
    public static final String PROP_COLOR_LINK = "color.link";
    public static final String PROP_COLOR_LINK_IMPORTANT = "color.link.important";
    public static final String PROP_COLOR_BACKGROUND = "color.backgorund";
    public static final String PROP_COLOR_WARNING = "color.warning";
    public static final String PROP_COLOR_FAILURE = "color.failure";
    public static final String PROP_COLOR_SUCCESS = "color.success";
    public static final String PROP_COLOR_DEBUG = "color.debug";
    public static final String PROP_STYLE_LINK = "style.link";
    public static final String PROP_FONT_SIZE_WRAP = "font.size.wrap";
    static final String PROP_INITIALIZED = "initialized";
    private static final int MIN_FONT_SIZE = 3;
    private static final int MAX_FONT_SIZE = 72;
    private static Font defaultFont = null;
    private Font font = null;
    private Font fontWrapped = null;
    private Color colorStandard;
    private Color colorError;
    private Color colorInput;
    private Color colorLink;
    private Color colorLinkImportant;
    private Color colorBackground;
    private Color colorWarning;
    private Color colorFailure;
    private Color colorSuccess;
    private Color colorDebug;
    private LinkStyle linkStyle = LinkStyle.UNDERLINE;
    private PropertyChangeSupport pcs;
    private boolean defaultFontType;

    private OutputOptions(boolean initFromDisk) {
        this.pcs = new PropertyChangeSupport(this);
        this.defaultFontType = false;
        this.resetToDefault();
        if (!initFromDisk) {
            return;
        }
        RequestProcessor.getDefault().post(new Runnable(){

            @Override
            public void run() {
                OutputOptions.this.loadFrom(NbPreferences.forModule(Controller.class));
            }
        });
    }

    final void resetToDefault() {
        this.setDefaultFont();
        this.setDefaultColors();
        this.setLinkStyle(LinkStyle.UNDERLINE);
    }

    boolean isChanged() {
        Preferences preferences = NbPreferences.forModule(Controller.class);
        if (!this.getFont().getFamily().equals(preferences.get("output.settings.font.family", OutputOptions.getDefaultFont().getFamily()))) {
            return true;
        }
        if (this.getFont().getSize() != preferences.getInt("output.settings.font.size", OutputOptions.getDefaultFont().getSize())) {
            return true;
        }
        if (this.getFont().getStyle() != preferences.getInt("output.settings.font.style", OutputOptions.getDefaultFont().getStyle())) {
            return true;
        }
        if (!this.getLinkStyle().name().equals(preferences.get("output.settings.style.link", "UNDERLINE"))) {
            return true;
        }
        if (this.getColorStandard().getRGB() != preferences.getInt("output.settings.color.standard", OutputOptions.getDefaultColorStandard().getRGB())) {
            return true;
        }
        if (this.getColorError().getRGB() != preferences.getInt("output.settings.color.error", OutputOptions.getDefaultColorError().getRGB())) {
            return true;
        }
        if (this.getColorInput().getRGB() != preferences.getInt("output.settings.color.input", OutputOptions.getDefaultColorInput().getRGB())) {
            return true;
        }
        if (this.getColorBackground().getRGB() != preferences.getInt("output.settings.color.backgorund", OutputOptions.getDefaultColorBackground().getRGB())) {
            return true;
        }
        if (this.getColorLink().getRGB() != preferences.getInt("output.settings.color.link", OutputOptions.getDefaultColorLink().getRGB())) {
            return true;
        }
        if (this.getColorLinkImportant().getRGB() != preferences.getInt("output.settings.color.link.important", OutputOptions.getDefaultColorLinkImportant().getRGB())) {
            return true;
        }
        if (this.getColorDebug().getRGB() != preferences.getInt("output.settings.color.debug", OutputOptions.getDefaultColorDebug().getRGB())) {
            return true;
        }
        if (this.getColorWarning().getRGB() != preferences.getInt("output.settings.color.warning", OutputOptions.getDefaultColorWarning().getRGB())) {
            return true;
        }
        if (this.getColorFailure().getRGB() != preferences.getInt("output.settings.color.failure", OutputOptions.getDefaultColorFailure().getRGB())) {
            return true;
        }
        return this.getColorSuccess().getRGB() != preferences.getInt("output.settings.color.success", OutputOptions.getDefaultColorSuccess().getRGB());
    }

    public void loadFrom(Preferences preferences) {
        assert (!EventQueue.isDispatchThread());
        final OutputOptions diskData = new OutputOptions(false);
        String fontFamily = preferences.get("output.settings.font.family", OutputOptions.getDefaultFont().getFamily());
        int fontSize = preferences.getInt("output.settings.font.size", OutputOptions.getDefaultFont().getSize());
        int fontStyle = preferences.getInt("output.settings.font.style", OutputOptions.getDefaultFont().getStyle());
        diskData.setFont(new Font(fontFamily, fontStyle, fontSize));
        int fontSizeWrapped = preferences.getInt("output.settings.font.size.wrap", OutputOptions.getDefaultFont().getSize());
        diskData.setFontForWrappedMode(OutputOptions.getDefaultFont().deriveFont((float)fontSizeWrapped));
        this.loadColors(preferences, diskData);
        String linkStyleStr = preferences.get("output.settings.style.link", "UNDERLINE");
        try {
            diskData.setLinkStyle(LinkStyle.valueOf(linkStyleStr));
        }
        catch (Exception e) {
            LOG.log(Level.INFO, "Invalid link style {0}", linkStyleStr);
        }
        EventQueue.invokeLater(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                OutputOptions.this.assign(diskData);
                OutputOptions outputOptions = OutputOptions.this;
                synchronized (outputOptions) {
                    OutputOptions.this.initialized = true;
                }
                OutputOptions.this.pcs.firePropertyChange("initialized", false, true);
            }
        });
    }

    private void loadColors(Preferences preferences, OutputOptions diskData) {
        int rgbStandard = preferences.getInt("output.settings.color.standard", OutputOptions.getDefaultColorStandard().getRGB());
        diskData.setColorStandard(new Color(rgbStandard));
        int rgbError = preferences.getInt("output.settings.color.error", OutputOptions.getDefaultColorError().getRGB());
        diskData.setColorError(new Color(rgbError));
        int rgbInput = preferences.getInt("output.settings.color.input", OutputOptions.getDefaultColorInput().getRGB());
        diskData.setColorInput(new Color(rgbInput));
        int rgbBackground = preferences.getInt("output.settings.color.backgorund", OutputOptions.getDefaultColorBackground().getRGB());
        diskData.setColorBackground(new Color(rgbBackground));
        int rgbLink = preferences.getInt("output.settings.color.link", OutputOptions.getDefaultColorLink().getRGB());
        diskData.setColorLink(new Color(rgbLink));
        int rgbLinkImportant = preferences.getInt("output.settings.color.link.important", OutputOptions.getDefaultColorLinkImportant().getRGB());
        diskData.setColorLinkImportant(new Color(rgbLinkImportant));
        int rgbDebug = preferences.getInt("output.settings.color.debug", OutputOptions.getDefaultColorDebug().getRGB());
        diskData.setColorDebug(new Color(rgbDebug));
        int rgbWarning = preferences.getInt("output.settings.color.warning", OutputOptions.getDefaultColorWarning().getRGB());
        diskData.setColorWarning(new Color(rgbWarning));
        int rgbFailure = preferences.getInt("output.settings.color.failure", OutputOptions.getDefaultColorFailure().getRGB());
        diskData.setColorFailure(new Color(rgbFailure));
        int rgbSuccess = preferences.getInt("output.settings.color.success", OutputOptions.getDefaultColorSuccess().getRGB());
        diskData.setColorSuccess(new Color(rgbSuccess));
    }

    public void saveTo(Preferences preferences) {
        assert (!EventQueue.isDispatchThread());
        this.saveColorsTo(preferences);
        this.saveFontsTo(preferences);
        try {
            preferences.flush();
        }
        catch (BackingStoreException ex) {
            LOG.log(Level.INFO, null, ex);
        }
    }

    private void saveColorsTo(Preferences p) {
        this.saveIfNotDefault(p, "color.standard", this.getColorStandard(), OutputOptions.getDefaultColorStandard());
        this.saveIfNotDefault(p, "color.error", this.getColorError(), OutputOptions.getDefaultColorError());
        this.saveIfNotDefault(p, "color.input", this.getColorInput(), OutputOptions.getDefaultColorInput());
        this.saveIfNotDefault(p, "color.backgorund", this.getColorBackground(), OutputOptions.getDefaultColorBackground());
        this.saveIfNotDefault(p, "color.link", this.getColorLink(), OutputOptions.getDefaultColorLink());
        this.saveIfNotDefault(p, "color.warning", this.getColorWarning(), OutputOptions.getDefaultColorWarning());
        this.saveIfNotDefault(p, "color.failure", this.getColorFailure(), OutputOptions.getDefaultColorFailure());
        this.saveIfNotDefault(p, "color.success", this.getColorSuccess(), OutputOptions.getDefaultColorSuccess());
        this.saveIfNotDefault(p, "color.debug", this.getColorDebug(), OutputOptions.getDefaultColorDebug());
        this.saveIfNotDefault(p, "color.link.important", this.getColorLinkImportant(), OutputOptions.getDefaultColorLinkImportant());
    }

    private void saveIfNotDefault(Preferences preferences, String key, Color value, Color dflt) {
        if (value == null || dflt.getRGB() == value.getRGB()) {
            preferences.remove("output.settings." + key);
        } else {
            preferences.putInt("output.settings." + key, value.getRGB());
        }
    }

    private void saveFontsTo(Preferences preferences) {
        preferences.putInt("output.settings.font.size", this.getFont().getSize());
        preferences.putInt("output.settings.font.style", this.getFont().getStyle());
        preferences.putInt("output.settings.font.size.wrap", this.getFontForWrappedMode().getSize());
        preferences.put("output.settings.font.family", this.getFont().getFamily());
        preferences.put("output.settings.style.link", this.getLinkStyle().name());
    }

    private void setDefaultColors() {
        this.setColorStandard(OutputOptions.getDefaultColorStandard());
        this.setColorError(OutputOptions.getDefaultColorError());
        this.setColorInput(OutputOptions.getDefaultColorInput());
        this.setColorLink(OutputOptions.getDefaultColorLink());
        this.setColorLinkImportant(OutputOptions.getDefaultColorLinkImportant());
        this.setColorBackground(OutputOptions.getDefaultColorBackground());
        this.setColorWarning(OutputOptions.getDefaultColorWarning());
        this.setColorFailure(OutputOptions.getDefaultColorFailure());
        this.setColorSuccess(OutputOptions.getDefaultColorSuccess());
        this.setColorDebug(OutputOptions.getDefaultColorDebug());
    }

    private void setDefaultFont() {
        this.setFont(OutputOptions.getDefaultFont());
        this.setFontForWrappedMode(OutputOptions.getDefaultFont());
    }

    public static Font getDefaultFont() {
        if (defaultFont == null) {
            Font f;
            int size = UIManager.getInt("uiFontSize");
            if (size < 3) {
                size = UIManager.getInt("customFontSize");
            }
            if (size < 3 && (f = (Font)UIManager.get("controlFont")) != null) {
                size = f.getSize();
            }
            if (size < 3) {
                size = 11;
            }
            defaultFont = new Font("Monospaced", 0, size);
        }
        return defaultFont;
    }

    public Font getFont() {
        return this.font;
    }

    public Font getFontForWrappedMode() {
        return this.fontWrapped;
    }

    public Font getFont(boolean wrapped) {
        return wrapped ? this.getFontForWrappedMode() : this.getFont();
    }

    public Color getColorStandard() {
        return this.colorStandard;
    }

    public Color getColorError() {
        return this.colorError;
    }

    public Color getColorInput() {
        return this.colorInput;
    }

    public Color getColorLink() {
        return this.colorLink;
    }

    public Color getColorLinkImportant() {
        return this.colorLinkImportant;
    }

    public Color getColorBackground() {
        return this.colorBackground;
    }

    public Color getColorWarning() {
        return this.colorWarning;
    }

    public Color getColorFailure() {
        return this.colorFailure;
    }

    public Color getColorSuccess() {
        return this.colorSuccess;
    }

    public Color getColorDebug() {
        return this.colorDebug;
    }

    public LinkStyle getLinkStyle() {
        return this.linkStyle;
    }

    public void setFont(Font font) {
        Font fontToSet = this.checkFontToSet(font);
        if (!fontToSet.equals(this.font)) {
            Font oldFont = this.font;
            this.font = fontToSet;
            this.defaultFontType = this.checkDefaultFontType();
            this.pcs.firePropertyChange("font", oldFont, fontToSet);
        }
    }

    private void setFontForWrappedMode(Font font) {
        Font fontToSet = this.checkFontToSet(font);
        if (!fontToSet.equals(this.fontWrapped)) {
            int oldFontSize = this.fontWrapped != null ? this.fontWrapped.getSize() : 0;
            this.fontWrapped = fontToSet;
            this.pcs.firePropertyChange("font.size.wrap", oldFontSize, fontToSet.getSize());
        }
    }

    private Font checkFontToSet(Font font) {
        Font checkedFont;
        Font font2 = checkedFont = font == null ? OutputOptions.getDefaultFont() : font;
        if (checkedFont.getSize() < 3) {
            checkedFont = checkedFont.deriveFont(3.0f);
        } else if (checkedFont.getSize() > 72) {
            checkedFont = checkedFont.deriveFont(72.0f);
        }
        return checkedFont;
    }

    public void setFontSize(boolean wrapped, int fontSize) {
        if (this.getFont() != null && (!wrapped || this.isDefaultFontType()) && fontSize != this.getFont().getSize()) {
            this.setFont(this.getFont().deriveFont((float)fontSize));
        }
        if (this.getFontForWrappedMode() != null && (wrapped || this.isDefaultFontType())) {
            this.setFontForWrappedMode(this.getFontForWrappedMode().deriveFont((float)fontSize));
        }
    }

    public boolean isDefaultFontType() {
        return this.defaultFontType;
    }

    private boolean checkDefaultFontType() {
        Font defFont = OutputOptions.getDefaultFont();
        return defFont.getName().equals(this.font.getName()) && defFont.getStyle() == this.font.getStyle();
    }

    public void setColorStandard(Color colorStandard) {
        Parameters.notNull((CharSequence)"colorStandard", (Object)colorStandard);
        if (!colorStandard.equals(this.colorStandard)) {
            Color oldColorStandard = this.colorStandard;
            this.colorStandard = colorStandard;
            this.pcs.firePropertyChange("color.standard", oldColorStandard, colorStandard);
        }
    }

    public void setColorError(Color colorError) {
        Parameters.notNull((CharSequence)"colorError", (Object)colorError);
        if (!colorError.equals(this.colorError)) {
            Color oldColorError = this.colorError;
            this.colorError = colorError;
            this.pcs.firePropertyChange("color.error", oldColorError, colorError);
        }
    }

    public void setColorInput(Color colorInput) {
        Parameters.notNull((CharSequence)"colorError", (Object)colorInput);
        if (!colorInput.equals(this.colorInput)) {
            Color oldColorInput = this.colorInput;
            this.colorInput = colorInput;
            this.pcs.firePropertyChange("color.input", oldColorInput, colorInput);
        }
    }

    public void setColorLink(Color colorLink) {
        Parameters.notNull((CharSequence)"colorLink", (Object)colorLink);
        if (!colorLink.equals(this.colorLink)) {
            Color oldColorLink = this.colorLink;
            this.colorLink = colorLink;
            this.pcs.firePropertyChange("color.link", oldColorLink, colorLink);
        }
    }

    public void setColorLinkImportant(Color colorLinkImportant) {
        Parameters.notNull((CharSequence)"colorLinkImportant", (Object)colorLinkImportant);
        if (!colorLinkImportant.equals(this.colorLinkImportant)) {
            Color oldColorLinkImportant = this.colorLinkImportant;
            this.colorLinkImportant = colorLinkImportant;
            this.pcs.firePropertyChange("color.link.important", oldColorLinkImportant, colorLinkImportant);
        }
    }

    public void setColorBackground(Color colorBackground) {
        Parameters.notNull((CharSequence)"colorBackground", (Object)colorBackground);
        if (!colorBackground.equals(this.colorBackground)) {
            Color oldColorBackground = this.colorBackground;
            this.colorBackground = colorBackground;
            this.pcs.firePropertyChange("color.backgorund", oldColorBackground, colorBackground);
        }
    }

    public void setColorWarning(Color colorWarning) {
        Parameters.notNull((CharSequence)"colorWarning", (Object)colorWarning);
        if (!colorWarning.equals(this.colorWarning)) {
            Color oldColorWarning = this.colorWarning;
            this.colorWarning = colorWarning;
            this.pcs.firePropertyChange("color.warning", oldColorWarning, colorWarning);
        }
    }

    public void setColorFailure(Color colorFailure) {
        Parameters.notNull((CharSequence)"colorFailure", (Object)colorFailure);
        if (!colorFailure.equals(this.colorFailure)) {
            Color oldColorFailure = this.colorFailure;
            this.colorFailure = colorFailure;
            this.pcs.firePropertyChange("color.failure", oldColorFailure, colorFailure);
        }
    }

    public void setColorSuccess(Color colorSuccess) {
        Parameters.notNull((CharSequence)"colorSuccess", (Object)colorSuccess);
        if (!colorSuccess.equals(this.colorSuccess)) {
            Color oldColorSuccess = this.colorSuccess;
            this.colorSuccess = colorSuccess;
            this.pcs.firePropertyChange("color.success", oldColorSuccess, colorSuccess);
        }
    }

    public void setColorDebug(Color colorDebug) {
        Parameters.notNull((CharSequence)"colorDebug", (Object)colorDebug);
        if (!colorDebug.equals(this.colorDebug)) {
            Color oldColorDebug = this.colorDebug;
            this.colorDebug = colorDebug;
            this.pcs.firePropertyChange("color.debug", oldColorDebug, colorDebug);
        }
    }

    public void setLinkStyle(LinkStyle linkStyle) {
        Parameters.notNull((CharSequence)"linkStyle", (Object)((Object)linkStyle));
        if (!linkStyle.equals((Object)this.linkStyle)) {
            LinkStyle oldLinkStyle = this.linkStyle;
            this.linkStyle = linkStyle;
            this.pcs.firePropertyChange("style.link", (Object)oldLinkStyle, (Object)linkStyle);
        }
    }

    public static synchronized OutputOptions getDefault() {
        if (DEFAULT == null) {
            DEFAULT = new OutputOptions(true);
        }
        return DEFAULT;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.removePropertyChangeListener(listener);
    }

    public synchronized OutputOptions makeCopy() {
        final OutputOptions copy = new OutputOptions(false);
        copy.font = this.font;
        copy.fontWrapped = this.fontWrapped;
        copy.colorStandard = this.colorStandard;
        copy.colorError = this.colorError;
        copy.colorInput = this.colorInput;
        copy.colorBackground = this.colorBackground;
        copy.colorLink = this.colorLink;
        copy.colorLinkImportant = this.colorLinkImportant;
        copy.colorWarning = this.colorWarning;
        copy.colorFailure = this.colorFailure;
        copy.colorSuccess = this.colorSuccess;
        copy.colorDebug = this.colorDebug;
        copy.initialized = this.initialized;
        copy.linkStyle = this.linkStyle;
        if (!this.initialized) {
            PropertyChangeListener l = new PropertyChangeListener(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if (evt.getPropertyName().equals("initialized")) {
                        copy.assign(OutputOptions.this);
                        OutputOptions outputOptions = copy;
                        synchronized (outputOptions) {
                            copy.initialized = true;
                        }
                        copy.pcs.firePropertyChange("initialized", false, true);
                        OutputOptions.this.removePropertyChangeListener(this);
                    }
                }
            };
            this.addPropertyChangeListener(l);
        } else {
            copy.initialized = true;
        }
        return copy;
    }

    public synchronized boolean isInitialized() {
        return this.initialized;
    }

    public void assign(OutputOptions outputOptions) {
        this.setFont(outputOptions.getFont());
        this.setFontForWrappedMode(outputOptions.getFontForWrappedMode());
        this.setColorStandard(outputOptions.getColorStandard());
        this.setColorError(outputOptions.getColorError());
        this.setColorInput(outputOptions.getColorInput());
        this.setColorLink(outputOptions.getColorLink());
        this.setColorLinkImportant(outputOptions.getColorLinkImportant());
        this.setColorBackground(outputOptions.getColorBackground());
        this.setColorDebug(outputOptions.getColorDebug());
        this.setColorWarning(outputOptions.getColorWarning());
        this.setColorFailure(outputOptions.getColorFailure());
        this.setColorSuccess(outputOptions.getColorSuccess());
        this.setLinkStyle(outputOptions.getLinkStyle());
    }

    static Color getDefaultColorStandard() {
        Color out = UIManager.getColor("nb.output.foreground");
        if (out == null && (out = UIManager.getColor("TextField.foreground")) == null) {
            out = Color.BLACK;
        }
        return out;
    }

    static Color getDefaultColorBackground() {
        Color back = UIManager.getColor("nb.output.backgorund");
        if (back == null) {
            back = UIManager.getColor("TextField.background");
            if (back == null) {
                back = Color.WHITE;
            } else if ("Nimbus".equals(UIManager.getLookAndFeel().getName())) {
                back = new Color(back.getRGB());
            }
        }
        return back;
    }

    static Color getDefaultColorError() {
        Color err = UIManager.getColor("nb.output.err.foreground");
        if (err == null) {
            err = LFCustoms.shiftColor((Color)Color.red);
        }
        return err;
    }

    static Color getDefaultColorInput() {
        Color input = UIManager.getColor("nb.output.input");
        if (input == null) {
            input = OutputOptions.getDefaultColorStandard();
        }
        return input;
    }

    static Color getDefaultColorLink() {
        Color hyperlink = UIManager.getColor("nb.output.link.foreground");
        if (hyperlink == null) {
            hyperlink = LFCustoms.shiftColor((Color)Color.blue);
        }
        return hyperlink;
    }

    static Color getDefaultColorLinkImportant() {
        Color hyperlinkImp = UIManager.getColor("nb.output.link.foreground.important");
        if (hyperlinkImp == null) {
            return OutputOptions.getDefaultColorLink();
        }
        return hyperlinkImp;
    }

    static Color getDefaultColorWarning() {
        Color c = UIManager.getColor("nb.output.warning.foreground");
        if (c == null) {
            c = OutputOptions.ensureContrastingColor(Color.ORANGE, OutputOptions.getDefaultColorBackground());
        }
        return c;
    }

    static Color getDefaultColorFailure() {
        Color c = UIManager.getColor("nb.output.failure.foreground");
        if (c == null) {
            c = OutputOptions.ensureContrastingColor(Color.RED, OutputOptions.getDefaultColorBackground());
        }
        return c;
    }

    static Color getDefaultColorSuccess() {
        Color c = UIManager.getColor("nb.output.success.foreground");
        if (c == null) {
            c = OutputOptions.ensureContrastingColor(Color.GREEN.darker().darker(), OutputOptions.getDefaultColorBackground());
        }
        return c;
    }

    static Color getDefaultColorDebug() {
        Color c = UIManager.getColor("nb.output.debug.foreground");
        if (c == null) {
            c = OutputOptions.ensureContrastingColor(Color.GRAY, OutputOptions.getDefaultColorBackground());
        }
        return c;
    }

    static Color ensureContrastingColor(Color fg, Color bg) {
        if (bg == null) {
            if (OutputOptions.isNimbus()) {
                bg = UIManager.getColor("Tree.background");
                if (null == bg) {
                    bg = Color.WHITE;
                }
            } else {
                bg = UIManager.getColor("text");
                if (bg == null) {
                    bg = Color.WHITE;
                }
            }
        }
        if (fg == null) {
            if (OutputOptions.isNimbus()) {
                fg = UIManager.getColor("Tree.foreground");
                if (null == fg) {
                    fg = Color.BLACK;
                }
            } else {
                fg = UIManager.getColor("textText");
                if (fg == null) {
                    fg = Color.BLACK;
                }
            }
        }
        if (Color.BLACK.equals(fg) && Color.WHITE.equals(fg)) {
            return fg;
        }
        boolean replace = fg.equals(bg);
        int dif = 0;
        if (!replace) {
            dif = OutputOptions.difference(fg, bg);
            boolean bl = replace = dif < 60;
        }
        if (replace) {
            int lum = OutputOptions.luminance(bg);
            boolean darker = lum >= 128;
            fg = darker ? fg.darker() : fg.brighter();
        }
        return fg;
    }

    private static int difference(Color a, Color b) {
        return Math.abs(OutputOptions.luminance(a) - OutputOptions.luminance(b));
    }

    private static int luminance(Color c) {
        return (299 * c.getRed() + 587 * c.getGreen() + 114 * c.getBlue()) / 1000;
    }

    static boolean isNimbus() {
        return "Nimbus".equals(UIManager.getLookAndFeel().getID());
    }

    public Color getColorForType(IOColors.OutputType type) {
        switch (type) {
            case OUTPUT: {
                return this.getColorStandard();
            }
            case ERROR: {
                return this.getColorError();
            }
            case INPUT: {
                return this.getColorInput();
            }
            case HYPERLINK: {
                return this.getColorLink();
            }
            case HYPERLINK_IMPORTANT: {
                return this.getColorLinkImportant();
            }
            case LOG_DEBUG: {
                return this.getColorDebug();
            }
            case LOG_WARNING: {
                return this.getColorWarning();
            }
            case LOG_FAILURE: {
                return this.getColorFailure();
            }
            case LOG_SUCCESS: {
                return this.getColorSuccess();
            }
        }
        return this.getColorStandard();
    }

    public static void storeDefault() {
        if (saveScheduled.compareAndSet(false, true)) {
            RequestProcessor.getDefault().post(new Runnable(){

                @Override
                public void run() {
                    OutputOptions.getDefault().saveTo(NbPreferences.forModule(Controller.class));
                    saveScheduled.set(false);
                }
            }, 100);
        }
    }

    public static enum LinkStyle {
        NONE,
        UNDERLINE;
        

        private LinkStyle() {
        }
    }

}

