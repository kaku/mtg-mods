/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.IOContainer
 *  org.openide.windows.IOContainer$CallBacks
 *  org.openide.windows.IOContainer$Provider
 */
package org.netbeans.core.output2.options;

import java.awt.Component;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JPanel;
import org.openide.windows.IOContainer;

public class PreviewIOProvider
implements IOContainer.Provider {
    JPanel panel;

    public PreviewIOProvider(JPanel panel) {
        this.panel = panel;
    }

    public void open() {
    }

    public void requestActive() {
        this.panel.requestFocusInWindow();
    }

    public void requestVisible() {
        this.panel.requestFocusInWindow();
    }

    public boolean isActivated() {
        return this.panel.hasFocus();
    }

    public void add(JComponent comp, IOContainer.CallBacks cb) {
        this.panel.add(comp);
    }

    public void remove(JComponent comp) {
        this.panel.remove(comp);
    }

    public void select(JComponent comp) {
    }

    public JComponent getSelected() {
        return (JComponent)this.panel.getComponent(0);
    }

    public void setTitle(JComponent comp, String name) {
    }

    public void setToolTipText(JComponent comp, String text) {
    }

    public void setIcon(JComponent comp, Icon icon) {
    }

    public void setToolbarActions(JComponent comp, Action[] toolbarActions) {
    }

    public boolean isCloseable(JComponent comp) {
        return false;
    }
}

