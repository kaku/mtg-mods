/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.awt.ColorComboBox
 *  org.openide.awt.Mnemonics
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.NbBundle
 *  org.openide.windows.IOColorPrint
 *  org.openide.windows.IOContainer
 *  org.openide.windows.IOContainer$Provider
 *  org.openide.windows.InputOutput
 *  org.openide.windows.OutputEvent
 *  org.openide.windows.OutputListener
 *  org.openide.windows.OutputWriter
 */
package org.netbeans.core.output2.options;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.beans.PropertyEditorManager;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.core.output2.Controller;
import org.netbeans.core.output2.NbIOProvider;
import org.netbeans.core.output2.options.LinkStyleModel;
import org.netbeans.core.output2.options.OutputOptions;
import org.netbeans.core.output2.options.OutputSettingsOptionsPanelController;
import org.netbeans.core.output2.options.PreviewIOProvider;
import org.netbeans.core.output2.ui.AbstractOutputPane;
import org.netbeans.core.output2.ui.AbstractOutputTab;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.awt.ColorComboBox;
import org.openide.awt.Mnemonics;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.IOColorPrint;
import org.openide.windows.IOContainer;
import org.openide.windows.InputOutput;
import org.openide.windows.OutputEvent;
import org.openide.windows.OutputListener;
import org.openide.windows.OutputWriter;

public final class OutputSettingsPanel
extends JPanel {
    private OutputOptions outputOptions;
    private InputOutput previewInputOutput = null;
    private final OutputSettingsOptionsPanelController controller;
    private LinkStyleModel linkStyleModel = new LinkStyleModel();
    private JButton btnRestore;
    private JButton btnSelectFont;
    private JComboBox cmbBackgroundColor;
    private JComboBox cmbDebugColor;
    private JComboBox cmbErrorColor;
    private JComboBox cmbFailureColor;
    private JComboBox cmbImportantLinkColor;
    private JComboBox cmbInputColor;
    private JComboBox cmbLinkColor;
    private JComboBox cmbLinkStyle;
    private JComboBox cmbStandardColor;
    private JComboBox cmbSuccessColor;
    private JComboBox cmbWarningColor;
    private JTextField fldFontFamily;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JPanel jPanel2;
    private JLabel lblBackgroundColor;
    private JLabel lblErrorColor;
    private JLabel lblFontFamily;
    private JLabel lblFontSize;
    private JLabel lblInputColor;
    private JLabel lblLinkColor;
    private JLabel lblLinkStyle;
    private JLabel lblStandardColor;
    private JLabel lblTitle;
    private JLabel lblUnwrappedOnly;
    private JPanel previewPanel;
    private JSpinner spnFontSize;

    OutputSettingsPanel(OutputSettingsOptionsPanelController controller) {
        this.controller = controller;
        this.initComponents();
    }

    private void initComponents() {
        this.lblTitle = new JLabel();
        this.jPanel2 = new JPanel();
        this.lblFontFamily = new JLabel();
        this.lblStandardColor = new JLabel();
        this.lblErrorColor = new JLabel();
        this.lblBackgroundColor = new JLabel();
        this.lblLinkColor = new JLabel();
        this.cmbLinkColor = new ColorComboBox();
        this.cmbBackgroundColor = new ColorComboBox();
        this.cmbErrorColor = new ColorComboBox();
        this.cmbStandardColor = new ColorComboBox();
        this.lblFontSize = new JLabel();
        this.spnFontSize = new JSpinner();
        this.btnSelectFont = new JButton();
        this.cmbLinkStyle = new JComboBox();
        this.lblLinkStyle = new JLabel();
        this.fldFontFamily = new JTextField();
        this.cmbImportantLinkColor = new ColorComboBox();
        this.jLabel1 = new JLabel();
        this.lblUnwrappedOnly = new JLabel();
        this.lblInputColor = new JLabel();
        this.cmbInputColor = new ColorComboBox();
        this.jLabel2 = new JLabel();
        this.jLabel3 = new JLabel();
        this.jLabel4 = new JLabel();
        this.jLabel5 = new JLabel();
        this.cmbDebugColor = new ColorComboBox();
        this.cmbWarningColor = new ColorComboBox();
        this.cmbFailureColor = new ColorComboBox();
        this.cmbSuccessColor = new ColorComboBox();
        this.previewPanel = new JPanel();
        this.btnRestore = new JButton();
        Mnemonics.setLocalizedText((JLabel)this.lblTitle, (String)NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.lblTitle.text"));
        this.lblFontFamily.setLabelFor(this.btnSelectFont);
        Mnemonics.setLocalizedText((JLabel)this.lblFontFamily, (String)NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.lblFontFamily.text"));
        this.lblStandardColor.setLabelFor(this.cmbStandardColor);
        Mnemonics.setLocalizedText((JLabel)this.lblStandardColor, (String)NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.lblStandardColor.text"));
        this.lblErrorColor.setLabelFor(this.cmbErrorColor);
        Mnemonics.setLocalizedText((JLabel)this.lblErrorColor, (String)NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.lblErrorColor.text"));
        this.lblBackgroundColor.setLabelFor(this.cmbBackgroundColor);
        Mnemonics.setLocalizedText((JLabel)this.lblBackgroundColor, (String)NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.lblBackgroundColor.text"));
        this.lblLinkColor.setLabelFor(this.cmbLinkColor);
        Mnemonics.setLocalizedText((JLabel)this.lblLinkColor, (String)NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.lblLinkColor.text"));
        this.cmbLinkColor.setToolTipText(NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.cmbLinkColor.toolTipText"));
        this.cmbLinkColor.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                OutputSettingsPanel.this.cmbLinkColorActionPerformed(evt);
            }
        });
        this.cmbBackgroundColor.setToolTipText(NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.cmbBackgroundColor.toolTipText"));
        this.cmbBackgroundColor.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                OutputSettingsPanel.this.cmbBackgroundColorActionPerformed(evt);
            }
        });
        this.cmbErrorColor.setToolTipText(NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.cmbErrorColor.toolTipText"));
        this.cmbErrorColor.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                OutputSettingsPanel.this.cmbErrorColorActionPerformed(evt);
            }
        });
        this.cmbStandardColor.setToolTipText(NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.cmbStandardColor.toolTipText"));
        this.cmbStandardColor.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                OutputSettingsPanel.this.cmbStandardColorActionPerformed(evt);
            }
        });
        this.lblFontSize.setLabelFor(this.spnFontSize);
        Mnemonics.setLocalizedText((JLabel)this.lblFontSize, (String)NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.lblFontSize.text"));
        this.spnFontSize.setToolTipText(NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.spnFontSize.toolTipText"));
        this.spnFontSize.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent evt) {
                OutputSettingsPanel.this.spnFontSizeStateChanged(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.btnSelectFont, (String)NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.btnSelectFont.text"));
        this.btnSelectFont.setToolTipText(NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.btnSelectFont.toolTipText"));
        this.btnSelectFont.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                OutputSettingsPanel.this.btnSelectFontActionPerformed(evt);
            }
        });
        this.cmbLinkStyle.setModel(this.linkStyleModel);
        this.cmbLinkStyle.setSelectedIndex(0);
        this.cmbLinkStyle.setToolTipText(NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.cmbLinkStyle.toolTipText"));
        this.cmbLinkStyle.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                OutputSettingsPanel.this.cmbLinkStyleActionPerformed(evt);
            }
        });
        this.lblLinkStyle.setLabelFor(this.cmbLinkStyle);
        Mnemonics.setLocalizedText((JLabel)this.lblLinkStyle, (String)NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.lblLinkStyle.text"));
        this.fldFontFamily.setEditable(false);
        this.fldFontFamily.setText(NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.fldFontFamily.text"));
        this.cmbImportantLinkColor.setToolTipText(NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.cmbImportantLinkColor.toolTipText"));
        this.cmbImportantLinkColor.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                OutputSettingsPanel.this.cmbImportantLinkColorActionPerformed(evt);
            }
        });
        this.jLabel1.setLabelFor(this.cmbImportantLinkColor);
        Mnemonics.setLocalizedText((JLabel)this.jLabel1, (String)NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.jLabel1.text"));
        Mnemonics.setLocalizedText((JLabel)this.lblUnwrappedOnly, (String)NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.lblUnwrappedOnly.text"));
        Mnemonics.setLocalizedText((JLabel)this.lblInputColor, (String)NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.lblInputColor.text"));
        this.cmbInputColor.setToolTipText(NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.cmbInputColor.toolTipText"));
        this.cmbInputColor.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                OutputSettingsPanel.this.cmbInputColorActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((JLabel)this.jLabel2, (String)NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.jLabel2.text"));
        Mnemonics.setLocalizedText((JLabel)this.jLabel3, (String)NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.jLabel3.text"));
        Mnemonics.setLocalizedText((JLabel)this.jLabel4, (String)NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.jLabel4.text"));
        Mnemonics.setLocalizedText((JLabel)this.jLabel5, (String)NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.jLabel5.text"));
        this.cmbDebugColor.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                OutputSettingsPanel.this.cmbDebugColorActionPerformed(evt);
            }
        });
        this.cmbWarningColor.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                OutputSettingsPanel.this.cmbWarningColorActionPerformed(evt);
            }
        });
        this.cmbFailureColor.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                OutputSettingsPanel.this.cmbFailureColorActionPerformed(evt);
            }
        });
        this.cmbSuccessColor.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                OutputSettingsPanel.this.cmbSuccessColorActionPerformed(evt);
            }
        });
        GroupLayout jPanel2Layout = new GroupLayout(this.jPanel2);
        this.jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel2Layout.createSequentialGroup().addContainerGap().addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.lblErrorColor).addComponent(this.lblStandardColor).addComponent(this.lblLinkColor).addComponent(this.lblFontFamily).addComponent(this.lblBackgroundColor).addComponent(this.lblFontSize).addComponent(this.jLabel1).addComponent(this.lblLinkStyle).addComponent(this.lblInputColor)).addGap(18, 18, 18).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel2Layout.createSequentialGroup().addComponent(this.fldFontFamily).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnSelectFont)).addComponent(this.lblUnwrappedOnly, -1, -1, 32767).addGroup(jPanel2Layout.createSequentialGroup().addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false).addComponent(this.cmbLinkStyle, GroupLayout.Alignment.LEADING, 0, -1, 32767).addComponent(this.cmbImportantLinkColor, GroupLayout.Alignment.LEADING, 0, -1, 32767).addComponent(this.cmbLinkColor, GroupLayout.Alignment.LEADING, 0, -1, 32767).addComponent(this.cmbInputColor, GroupLayout.Alignment.LEADING, 0, -1, 32767).addComponent(this.cmbErrorColor, GroupLayout.Alignment.LEADING, 0, -1, 32767).addComponent(this.cmbStandardColor, GroupLayout.Alignment.LEADING, 0, -1, 32767).addComponent(this.spnFontSize, GroupLayout.Alignment.LEADING, -2, 71, -2).addComponent(this.cmbBackgroundColor, GroupLayout.Alignment.LEADING, 0, -1, 32767)).addGap(18, 18, 18).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel2).addComponent(this.jLabel3).addComponent(this.jLabel4).addComponent(this.jLabel5)).addGap(21, 21, 21).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false).addComponent(this.cmbFailureColor, GroupLayout.Alignment.LEADING, 0, -1, 32767).addComponent(this.cmbWarningColor, GroupLayout.Alignment.LEADING, 0, -1, 32767).addComponent(this.cmbDebugColor, 0, 238, 32767).addComponent(this.cmbSuccessColor, 0, -1, 32767)).addGap(0, 0, 32767))).addGap(1, 1, 1)));
        jPanel2Layout.linkSize(0, this.cmbBackgroundColor, this.cmbDebugColor);
        jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel2Layout.createSequentialGroup().addContainerGap().addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lblFontFamily).addComponent(this.btnSelectFont).addComponent(this.fldFontFamily, -2, -1, -2)).addGap(1, 1, 1).addComponent(this.lblUnwrappedOnly).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lblFontSize).addComponent(this.spnFontSize, -2, -1, -2)).addGap(18, 18, 18).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lblBackgroundColor).addComponent(this.cmbBackgroundColor, -2, -1, -2).addComponent(this.jLabel2).addComponent(this.cmbDebugColor, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.cmbStandardColor, -2, -1, -2).addComponent(this.lblStandardColor).addComponent(this.jLabel3).addComponent(this.cmbWarningColor, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.cmbErrorColor, -2, -1, -2).addComponent(this.lblErrorColor).addComponent(this.jLabel4).addComponent(this.cmbFailureColor, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lblInputColor).addComponent(this.cmbInputColor, -2, -1, -2).addComponent(this.jLabel5).addComponent(this.cmbSuccessColor, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lblLinkColor).addComponent(this.cmbLinkColor, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.cmbImportantLinkColor, -2, -1, -2).addComponent(this.jLabel1)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.cmbLinkStyle, -2, -1, -2).addComponent(this.lblLinkStyle)).addContainerGap(-1, 32767)));
        this.previewPanel.setBorder(BorderFactory.createBevelBorder(1));
        this.previewPanel.setLayout(new BoxLayout(this.previewPanel, 2));
        Mnemonics.setLocalizedText((AbstractButton)this.btnRestore, (String)NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.btnRestore.text"));
        this.btnRestore.setToolTipText(NbBundle.getMessage(OutputSettingsPanel.class, (String)"OutputSettingsPanel.btnRestore.toolTipText"));
        this.btnRestore.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                OutputSettingsPanel.this.btnRestoreActionPerformed(evt);
            }
        });
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.previewPanel, GroupLayout.Alignment.TRAILING, -1, -1, 32767).addComponent(this.jPanel2, -1, -1, 32767).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addComponent(this.lblTitle).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.btnRestore)))));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lblTitle).addComponent(this.btnRestore)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jPanel2, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.previewPanel, -1, 51, 32767).addContainerGap()));
    }

    private void btnSelectFontActionPerformed(ActionEvent evt) {
        PropertyEditor pe = PropertyEditorManager.findEditor(Font.class);
        if (pe != null) {
            pe.setValue(this.outputOptions.getFont());
            DialogDescriptor dd = new DialogDescriptor((Object)pe.getCustomEditor(), NbBundle.getMessage(Controller.class, (String)"LBL_Font_Chooser_Title"));
            String defaultFont = NbBundle.getMessage(Controller.class, (String)"BTN_Defaul_Font");
            dd.setOptions(new Object[]{DialogDescriptor.OK_OPTION, defaultFont, DialogDescriptor.CANCEL_OPTION});
            DialogDisplayer.getDefault().createDialog(dd).setVisible(true);
            if (dd.getValue() == DialogDescriptor.OK_OPTION) {
                Font f = (Font)pe.getValue();
                this.outputOptions.setFont(f);
            } else if (dd.getValue() == defaultFont) {
                this.outputOptions.setFont(null);
            }
            this.updateFontField();
        }
    }

    private void cmbStandardColorActionPerformed(ActionEvent evt) {
        Color std = ((ColorComboBox)this.cmbStandardColor).getSelectedColor();
        if (std != null) {
            this.outputOptions.setColorStandard(std);
        }
    }

    private void spnFontSizeStateChanged(ChangeEvent evt) {
        int fontSize = (Integer)this.spnFontSize.getValue();
        this.outputOptions.setFont(this.outputOptions.getFont().deriveFont((float)fontSize));
        this.updateFontField();
    }

    private void cmbBackgroundColorActionPerformed(ActionEvent evt) {
        Color bg = ((ColorComboBox)this.cmbBackgroundColor).getSelectedColor();
        if (bg != null) {
            this.outputOptions.setColorBackground(bg);
        }
    }

    private void cmbErrorColorActionPerformed(ActionEvent evt) {
        Color err = ((ColorComboBox)this.cmbErrorColor).getSelectedColor();
        if (err != null) {
            this.outputOptions.setColorError(err);
        }
    }

    private void cmbLinkColorActionPerformed(ActionEvent evt) {
        Color link = ((ColorComboBox)this.cmbLinkColor).getSelectedColor();
        if (link != null) {
            this.outputOptions.setColorLink(link);
        }
    }

    private void cmbImportantLinkColorActionPerformed(ActionEvent evt) {
        Color iLink = ((ColorComboBox)this.cmbImportantLinkColor).getSelectedColor();
        if (iLink != null) {
            this.outputOptions.setColorLinkImportant(iLink);
        }
    }

    private void btnRestoreActionPerformed(ActionEvent evt) {
        this.outputOptions.resetToDefault();
        this.updateControlsByModel();
    }

    private void cmbLinkStyleActionPerformed(ActionEvent evt) {
        this.outputOptions.setLinkStyle(this.linkStyleModel.getLinkStyle());
    }

    private void cmbInputColorActionPerformed(ActionEvent evt) {
        Color input = ((ColorComboBox)this.cmbInputColor).getSelectedColor();
        if (input != null) {
            this.outputOptions.setColorInput(input);
        }
    }

    private void cmbDebugColorActionPerformed(ActionEvent evt) {
        Color debug = ((ColorComboBox)this.cmbDebugColor).getSelectedColor();
        if (debug != null) {
            this.outputOptions.setColorDebug(debug);
        }
    }

    private void cmbWarningColorActionPerformed(ActionEvent evt) {
        Color warning = ((ColorComboBox)this.cmbWarningColor).getSelectedColor();
        if (warning != null) {
            this.outputOptions.setColorWarning(warning);
        }
    }

    private void cmbFailureColorActionPerformed(ActionEvent evt) {
        Color failure = ((ColorComboBox)this.cmbFailureColor).getSelectedColor();
        if (failure != null) {
            this.outputOptions.setColorFailure(failure);
        }
    }

    private void cmbSuccessColorActionPerformed(ActionEvent evt) {
        Color success = ((ColorComboBox)this.cmbSuccessColor).getSelectedColor();
        if (success != null) {
            this.outputOptions.setColorSuccess(success);
        }
    }

    void load() {
        if (this.previewInputOutput == null) {
            this.initPreview();
        }
        this.updateControlsByModel();
    }

    private void selectColor(JComboBox combo, Color color) {
        ((ColorComboBox)combo).setSelectedColor(color);
    }

    private void updateFontField() {
        Font f = this.outputOptions.getFont();
        this.fldFontFamily.setText(f.getFamily() + " " + f.getSize());
    }

    void store() {
        Controller.getDefault().updateOptions(this.outputOptions);
        OutputOptions.getDefault().assign(this.outputOptions);
        OutputOptions.storeDefault();
    }

    void cancel() {
        if (this.previewInputOutput != null) {
            this.previewInputOutput.closeInputOutput();
        }
    }

    boolean valid() {
        return true;
    }

    private void initPreview() {
        this.previewInputOutput = this.initPreviewInputOutput();
        this.outputOptions = (OutputOptions)((Lookup.Provider)this.previewInputOutput).getLookup().lookup(OutputOptions.class);
        final Reader in = this.previewInputOutput.getIn();
        this.previewInputOutput.setInputVisible(true);
        this.previewInputOutput.getOut().println("Standard Output");
        this.previewInputOutput.getErr().println("Error Output");
        OutputListenerImpl ol = new OutputListenerImpl();
        try {
            IOColorPrint.print((InputOutput)this.previewInputOutput, (CharSequence)"Standard Link", (OutputListener)ol, (boolean)false, (Color)null);
            this.previewInputOutput.getOut().println();
            IOColorPrint.print((InputOutput)this.previewInputOutput, (CharSequence)"Important Link", (OutputListener)ol, (boolean)true, (Color)null);
            this.previewInputOutput.getOut().println();
        }
        catch (IOException ex) {
            ex.printStackTrace((PrintWriter)this.previewInputOutput.getErr());
        }
        this.previewInputOutput.getOut().close();
        this.previewInputOutput.getErr().close();
        this.outputOptions.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (!evt.getPropertyName().equals("initialized") && OutputSettingsPanel.this.outputOptions.isInitialized()) {
                    OutputSettingsPanel.this.controller.changed(OutputSettingsPanel.this.outputOptions.isChanged());
                }
                OutputSettingsPanel.this.updateControlsByModel();
            }
        });
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                Component component = OutputSettingsPanel.this.previewPanel.getComponent(0);
                if (component instanceof AbstractOutputTab) {
                    ((AbstractOutputTab)component).inputSent("Input from keyboard");
                }
                try {
                    in.close();
                }
                catch (IOException ex) {
                    // empty catch block
                }
            }
        });
    }

    private InputOutput initPreviewInputOutput() throws NullPointerException {
        IOContainer ioContainer = IOContainer.create((IOContainer.Provider)new PreviewIOProvider(this.previewPanel));
        InputOutput io = NbIOProvider.getDefault().getIO("Preview", false, new Action[0], ioContainer);
        Component component = this.previewPanel.getComponent(0);
        if (component instanceof AbstractOutputTab) {
            ((AbstractOutputTab)component).getOutputPane().setWrapped(false);
        }
        return io;
    }

    private void updateControlsByModel() {
        this.updateFontField();
        this.spnFontSize.setValue(this.outputOptions.getFont().getSize());
        this.selectColor(this.cmbStandardColor, this.outputOptions.getColorStandard());
        this.selectColor(this.cmbErrorColor, this.outputOptions.getColorError());
        this.selectColor(this.cmbInputColor, this.outputOptions.getColorInput());
        this.selectColor(this.cmbBackgroundColor, this.outputOptions.getColorBackground());
        this.selectColor(this.cmbLinkColor, this.outputOptions.getColorLink());
        this.selectColor(this.cmbDebugColor, this.outputOptions.getColorDebug());
        this.selectColor(this.cmbWarningColor, this.outputOptions.getColorWarning());
        this.selectColor(this.cmbFailureColor, this.outputOptions.getColorFailure());
        this.selectColor(this.cmbSuccessColor, this.outputOptions.getColorSuccess());
        this.selectColor(this.cmbImportantLinkColor, this.outputOptions.getColorLinkImportant());
        this.cmbLinkStyle.setSelectedItem(this.linkStyleModel.itemFor(this.outputOptions.getLinkStyle()));
        this.cmbLinkStyle.repaint();
    }

    private static class OutputListenerImpl
    implements OutputListener {
        public void outputLineSelected(OutputEvent ev) {
        }

        public void outputLineAction(OutputEvent ev) {
        }

        public void outputLineCleared(OutputEvent ev) {
        }
    }

}

