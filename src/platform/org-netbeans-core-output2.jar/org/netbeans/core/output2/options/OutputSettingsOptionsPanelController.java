/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package org.netbeans.core.output2.options;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JComponent;
import org.netbeans.core.output2.options.OutputSettingsPanel;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public final class OutputSettingsOptionsPanelController
extends OptionsPanelController {
    private OutputSettingsPanel panel;
    private final PropertyChangeSupport pcs;
    private boolean changed;
    private boolean initialized;

    public OutputSettingsOptionsPanelController() {
        this.pcs = new PropertyChangeSupport((Object)this);
        this.initialized = false;
    }

    public void update() {
        if (!this.initialized) {
            this.getPanel().load();
            this.initialized = true;
        }
        this.changed = false;
    }

    public void applyChanges() {
        this.getPanel().store();
        this.changed = false;
    }

    public void cancel() {
    }

    public boolean isValid() {
        return this.getPanel().valid();
    }

    public boolean isChanged() {
        return this.changed;
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx("netbeans.optionsDialog.advanced.output");
    }

    public JComponent getComponent(Lookup masterLookup) {
        return this.getPanel();
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    private OutputSettingsPanel getPanel() {
        if (this.panel == null) {
            this.panel = new OutputSettingsPanel(this);
        }
        return this.panel;
    }

    void changed(boolean isChanged) {
        if (!this.changed) {
            this.pcs.firePropertyChange("changed", false, true);
        }
        this.changed = isChanged;
        this.pcs.firePropertyChange("valid", null, null);
    }
}

