/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.core.output2.options;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String AdvancedOption_DisplayName_OutputSettings() {
        return NbBundle.getMessage(Bundle.class, (String)"AdvancedOption_DisplayName_OutputSettings");
    }

    static String AdvancedOption_Keywords_OutputSettings() {
        return NbBundle.getMessage(Bundle.class, (String)"AdvancedOption_Keywords_OutputSettings");
    }

    static String KW_Background() {
        return NbBundle.getMessage(Bundle.class, (String)"KW_Background");
    }

    static String KW_Color() {
        return NbBundle.getMessage(Bundle.class, (String)"KW_Color");
    }

    static String KW_Font() {
        return NbBundle.getMessage(Bundle.class, (String)"KW_Font");
    }

    static String KW_Output_Window() {
        return NbBundle.getMessage(Bundle.class, (String)"KW_Output_Window");
    }

    static String KW_Underline() {
        return NbBundle.getMessage(Bundle.class, (String)"KW_Underline");
    }

    static String LBL_Description() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_Description");
    }

    static String LBL_None() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_None");
    }

    static String LBL_Underline() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_Underline");
    }

    private void Bundle() {
    }
}

