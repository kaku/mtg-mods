/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package org.netbeans.core.output2;

import java.util.Arrays;
import org.openide.util.Exceptions;

final class IntMap {
    private int[] keys = new int[]{Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE};
    private Object[] vals = new Object[4];
    private int last = -1;

    IntMap() {
    }

    public int first() {
        return this.isEmpty() ? -1 : this.keys[0];
    }

    public int nearest(int line, boolean backward) {
        if (this.isEmpty()) {
            return -1;
        }
        if (this.last == 0) {
            return this.keys[this.last];
        }
        if (line < this.keys[0]) {
            return backward ? this.keys[this.last] : this.keys[0];
        }
        if (line > this.keys[this.last]) {
            return backward ? this.keys[this.last] : this.keys[0];
        }
        int idx = Arrays.binarySearch(this.keys, line);
        if (idx < 0) {
            if ((idx = - idx + (backward ? -2 : -1)) > this.last) {
                idx = backward ? this.last : 0;
            } else if (idx < 0) {
                idx = backward ? this.last : 0;
            }
        }
        return this.keys[idx];
    }

    public int[] getKeys() {
        if (this.last == -1) {
            return new int[0];
        }
        if (this.last == this.keys.length - 1) {
            this.growArrays();
        }
        int[] result = new int[this.last + 1];
        try {
            System.arraycopy(this.keys, 0, result, 0, this.last + 1);
            return result;
        }
        catch (ArrayIndexOutOfBoundsException aioobe) {
            ArrayIndexOutOfBoundsException e = new ArrayIndexOutOfBoundsException("AIOOBE in IntMap.getKeys() - last = " + this.last + " keys: " + IntMap.i2s(this.keys) + " vals: " + Arrays.asList(this.vals) + " result length " + result.length);
            Exceptions.printStackTrace((Throwable)e);
            return new int[0];
        }
    }

    private static String i2s(int[] arr) {
        StringBuffer sb = new StringBuffer(arr.length * 3);
        sb.append('[');
        for (int i = 0; i < arr.length; ++i) {
            if (arr[i] == Integer.MAX_VALUE) continue;
            sb.append(arr[i]);
            sb.append(',');
        }
        sb.append(']');
        return sb.toString();
    }

    public Object get(int key) {
        int idx = Arrays.binarySearch(this.keys, key);
        if (idx > -1 && idx <= this.last) {
            return this.vals[idx];
        }
        return null;
    }

    public void put(int key, Object val) {
        if (this.last >= 0) {
            if (this.keys[this.last] == key && this.vals[this.last] == val) {
                return;
            }
            assert (key > this.keys[this.last]);
        }
        if (this.last == this.keys.length - 1) {
            this.growArrays();
        }
        ++this.last;
        this.keys[this.last] = key;
        this.vals[this.last] = val;
    }

    private void growArrays() {
        int newSize = this.keys.length * 2;
        int[] newKeys = new int[newSize];
        Object[] newVals = new Object[newSize];
        Arrays.fill(newKeys, Integer.MAX_VALUE);
        System.arraycopy(this.keys, 0, newKeys, 0, this.keys.length);
        System.arraycopy(this.vals, 0, newVals, 0, this.vals.length);
        this.keys = newKeys;
        this.vals = newVals;
    }

    public int nextEntry(int entry) {
        int idx;
        int result = -1;
        if (!this.isEmpty() && (idx = Arrays.binarySearch(this.keys, entry)) >= 0) {
            result = idx == this.keys.length - 1 ? this.keys[0] : this.keys[idx + 1];
        }
        return result;
    }

    public int prevEntry(int entry) {
        int idx;
        int result = -1;
        if (!this.isEmpty() && (idx = Arrays.binarySearch(this.keys, entry)) >= 0) {
            result = idx == -1 ? this.keys[this.keys.length - 1] : this.keys[idx - 1];
        }
        return result;
    }

    public boolean isEmpty() {
        return this.last == -1;
    }

    public int size() {
        return this.last + 1;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer("IntMap@").append(System.identityHashCode(this));
        for (int i = 0; i < this.size(); ++i) {
            sb.append("[");
            sb.append(this.keys[i]);
            sb.append(":");
            sb.append(this.vals[i]);
            sb.append("]");
        }
        if (this.size() == 0) {
            sb.append("empty");
        }
        return sb.toString();
    }

    public void decrementKeys(int decrement) {
        if (decrement < 0) {
            throw new IllegalArgumentException();
        }
        int shift = Arrays.binarySearch(this.keys, decrement);
        if (shift < 0) {
            shift = - shift - 1;
        }
        for (int i = shift; i <= this.last; ++i) {
            this.keys[i - shift] = this.keys[i] - decrement;
            this.vals[i - shift] = this.vals[i];
        }
        Arrays.fill(this.keys, this.last - shift + 1, this.last + 1, Integer.MAX_VALUE);
        this.last -= shift;
    }
}

