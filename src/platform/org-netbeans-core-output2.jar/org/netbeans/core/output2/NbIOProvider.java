/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.windows.IOContainer
 *  org.openide.windows.IOProvider
 *  org.openide.windows.InputOutput
 *  org.openide.windows.OutputWriter
 */
package org.netbeans.core.output2;

import java.io.IOException;
import java.util.WeakHashMap;
import javax.swing.Action;
import org.netbeans.core.output2.Controller;
import org.netbeans.core.output2.IOEvent;
import org.netbeans.core.output2.NbIO;
import org.netbeans.core.output2.NbWriter;
import org.netbeans.core.output2.PairMap;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.windows.IOContainer;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;
import org.openide.windows.OutputWriter;

public final class NbIOProvider
extends IOProvider {
    private static final WeakHashMap<IOContainer, PairMap> containerPairMaps = new WeakHashMap();
    private static final String STDOUT = NbBundle.getMessage(NbIOProvider.class, (String)"LBL_STDOUT");
    private static final String NAME = "output2";

    public OutputWriter getStdOut() {
        if (Controller.LOG) {
            Controller.log("NbIOProvider.getStdOut");
        }
        NbIO stdout = (NbIO)this.getIO(STDOUT, false);
        NbWriter out = stdout.writer();
        NbIO.post(new IOEvent(stdout, 0, true));
        if (out != null && out.isClosed()) {
            try {
                out.reset();
                out = (NbWriter)stdout.getOut();
            }
            catch (IOException e) {
                Exceptions.printStackTrace((Throwable)e);
                stdout = (NbIO)this.getIO(STDOUT, true);
                out = (NbWriter)stdout.getOut();
            }
        } else {
            out = (NbWriter)stdout.getOut();
        }
        return out;
    }

    public InputOutput getIO(String name, boolean newIO) {
        return this.getIO(name, newIO, new Action[0], null);
    }

    public InputOutput getIO(String name, Action[] toolbarActions) {
        return this.getIO(name, true, toolbarActions, null);
    }

    public InputOutput getIO(String name, Action[] additionalActions, IOContainer ioContainer) {
        return this.getIO(name, true, additionalActions, ioContainer);
    }

    public String getName() {
        return "output2";
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public InputOutput getIO(String name, boolean newIO, Action[] toolbarActions, IOContainer ioContainer) {
        PairMap namesToIos;
        NbIO result;
        if (Controller.LOG) {
            Controller.log("GETIO: " + name + " new:" + newIO);
        }
        IOContainer realIoContainer = ioContainer == null ? IOContainer.getDefault() : ioContainer;
        WeakHashMap<IOContainer, PairMap> weakHashMap = containerPairMaps;
        synchronized (weakHashMap) {
            namesToIos = containerPairMaps.get((Object)realIoContainer);
            result = namesToIos != null ? namesToIos.get(name) : null;
        }
        if (result == null || newIO) {
            result = new NbIO(name, toolbarActions, realIoContainer);
            weakHashMap = containerPairMaps;
            synchronized (weakHashMap) {
                namesToIos = containerPairMaps.get((Object)realIoContainer);
                if (namesToIos == null) {
                    namesToIos = new PairMap();
                    containerPairMaps.put(realIoContainer, namesToIos);
                }
                namesToIos.add(name, result);
            }
            NbIO.post(new IOEvent(result, 0, newIO));
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void dispose(NbIO io) {
        IOContainer ioContainer = io.getIOContainer();
        if (ioContainer == null) {
            ioContainer = IOContainer.getDefault();
        }
        WeakHashMap<IOContainer, PairMap> weakHashMap = containerPairMaps;
        synchronized (weakHashMap) {
            PairMap namesToIos = containerPairMaps.get((Object)ioContainer);
            if (namesToIos != null) {
                namesToIos.remove(io);
                if (namesToIos.isEmpty()) {
                    containerPairMaps.remove((Object)ioContainer);
                }
            }
        }
    }
}

