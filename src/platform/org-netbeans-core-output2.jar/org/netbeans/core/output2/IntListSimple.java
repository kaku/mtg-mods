/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.output2;

import java.util.Arrays;

final class IntListSimple {
    private int[] array;
    private int used = 0;

    IntListSimple(int capacity) {
        this.array = new int[capacity];
    }

    public synchronized void add(int value) {
        if (this.used >= this.array.length) {
            this.growArray();
        }
        this.array[this.used++] = value;
    }

    public synchronized int get(int index) {
        if (index >= this.used) {
            throw new ArrayIndexOutOfBoundsException("List contains " + this.used + " items, but tried to fetch item " + index);
        }
        return this.array[index];
    }

    public synchronized int size() {
        return this.used;
    }

    public void set(int index, int value) {
        if (index >= this.used) {
            throw new IndexOutOfBoundsException();
        }
        this.array[index] = value;
    }

    public void shorten(int newSize) {
        if (newSize > this.used) {
            throw new IllegalArgumentException();
        }
        this.used = newSize;
    }

    private void growArray() {
        int[] old = this.array;
        this.array = new int[Math.round(this.array.length * 2)];
        System.arraycopy(old, 0, this.array, 0, old.length);
    }

    public String toString() {
        StringBuilder result = new StringBuilder("IntListSimple [");
        for (int i = 0; i < this.used; ++i) {
            result.append(i);
            result.append(':');
            result.append(this.array[i]);
            if (i == this.used - 1) continue;
            result.append(',');
        }
        result.append(']');
        return result.toString();
    }

    public synchronized void compact(int shift, int decrement) {
        if (shift < 0 || shift > this.used) {
            throw new IllegalArgumentException();
        }
        for (int i = shift; i < this.used; ++i) {
            this.array[i - shift] = this.array[i] - decrement;
        }
        Arrays.fill(this.array, this.used - shift, this.used, Integer.MAX_VALUE);
        this.used -= shift;
    }
}

