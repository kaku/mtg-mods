/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.output2;

import java.awt.AWTEvent;
import java.awt.ActiveEvent;
import org.netbeans.core.output2.Controller;
import org.netbeans.core.output2.NbIO;

final class IOEvent
extends AWTEvent
implements ActiveEvent {
    static final int IO_EVENT_MASK = 983040;
    static final int CMD_CREATE = 0;
    static final int CMD_OUTPUT_VISIBLE = 1;
    static final int CMD_INPUT_VISIBLE = 2;
    static final int CMD_ERR_VISIBLE = 3;
    static final int CMD_ERR_SEPARATED = 4;
    static final int CMD_FOCUS_TAKEN = 5;
    static final int CMD_SELECT = 6;
    static final int CMD_CLOSE = 7;
    static final int CMD_STREAM_CLOSED = 8;
    static final int CMD_RESET = 9;
    static final int CMD_SET_TOOLBAR_ACTIONS = 10;
    static final int CMD_SET_ICON = 11;
    static final int CMD_SET_TOOLTIP = 12;
    static final int CMD_SCROLL = 13;
    static final int CMD_DEF_COLORS = 14;
    static final int CMD_FINE_SELECT = 15;
    private static final int CMD_LAST = 16;
    private static final String[] CMDS = new String[]{"CREATE", "OUTPUT_VISIBLE", "INPUT_VISIBLE", "ERR_VISIBLE", "ERR_SEPARATED", "FOCUS_TAKEN", "SELECT", "CLOSE", "STREAM_CLOSED", "RESET", "SET_TOOLBAR_ACTIONS", "CMD_SET_ICON", "CMD_SET_TOOLTIP", "CMD_SCROLL", "CMD_DEF_COLORS", "CMD_FINE_SELECT"};
    private boolean value = false;
    private Object data = null;
    static int pendingCount = 0;

    IOEvent(NbIO source, int command, boolean value) {
        super(source == null ? new Object() : source, command + 983040);
        assert (command >= 0 && command < 16);
        this.consumed = false;
        this.value = value;
        ++pendingCount;
    }

    IOEvent(NbIO source, int command, Object data) {
        this(source, command, false);
        this.data = data;
    }

    public int getCommand() {
        return this.getID() - 983040;
    }

    public NbIO getIO() {
        return this.getSource() instanceof NbIO ? (NbIO)this.getSource() : null;
    }

    public boolean getValue() {
        return this.value;
    }

    public Object getData() {
        return this.data;
    }

    @Override
    public boolean isConsumed() {
        return this.consumed;
    }

    @Override
    public void consume() {
        this.consumed = true;
    }

    @Override
    public String toString() {
        return "IOEvent@" + System.identityHashCode(this) + "-" + IOEvent.cmdToString(this.getCommand()) + " on " + this.getIO() + " value= " + this.getValue() + " data=" + this.getData();
    }

    @Override
    public void dispatch() {
        Controller.getDefault().eventDispatched(this);
        --pendingCount;
    }

    public static String cmdToString(int cmd) {
        return CMDS[cmd];
    }
}

