/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package org.netbeans.core.output2;

import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.TextAction;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import org.netbeans.core.output2.ExtPlainView;
import org.netbeans.core.output2.WrappedTextView;
import org.openide.util.Exceptions;

final class OutputEditorKit
extends DefaultEditorKit
implements ViewFactory,
ChangeListener {
    private final boolean wrapped;
    private final JTextComponent comp;
    private static final Action[] actions = OutputEditorKit.prepareActions();
    private final PropertyChangeListener propertyChangeListener;
    private WrappedTextView lastWrappedView = null;
    private int lastMark = -1;
    private int lastDot = -1;
    private static final Rectangle scratch = new Rectangle();

    OutputEditorKit(boolean wrapped, JTextComponent comp, PropertyChangeListener propertyChangeListener) {
        this.comp = comp;
        this.wrapped = wrapped;
        this.propertyChangeListener = propertyChangeListener;
    }

    @Override
    public Action[] getActions() {
        return actions;
    }

    public WrappedTextView view() {
        return this.lastWrappedView;
    }

    @Override
    public View create(Element element) {
        View result = this.wrapped ? new WrappedTextView(element, this.comp, this.propertyChangeListener) : new ExtPlainView(element);
        this.lastWrappedView = this.wrapped ? result : null;
        return result;
    }

    @Override
    public ViewFactory getViewFactory() {
        return this;
    }

    public boolean isWrapped() {
        return this.wrapped;
    }

    @Override
    public void install(JEditorPane c) {
        super.install(c);
        if (this.wrapped) {
            c.getCaret().addChangeListener(this);
        }
    }

    @Override
    public void deinstall(JEditorPane c) {
        super.deinstall(c);
        if (this.wrapped) {
            c.getCaret().removeChangeListener(this);
        }
    }

    @Override
    public void stateChanged(ChangeEvent ce) {
        boolean hadSelection;
        int dot;
        int mark = this.comp.getSelectionStart();
        boolean hasSelection = mark != (dot = this.comp.getSelectionEnd());
        boolean bl = hadSelection = this.lastMark != this.lastDot;
        if (this.lastMark != mark || this.lastDot != dot) {
            int begin = Math.min(mark, dot);
            int end = Math.max(mark, dot);
            int oldBegin = Math.min(this.lastMark, this.lastDot);
            int oldEnd = Math.max(this.lastMark, this.lastDot);
            if (hadSelection && hasSelection) {
                if (begin != oldBegin) {
                    int startChar = Math.min(begin, oldBegin);
                    int endChar = Math.max(begin, oldBegin);
                    this.repaintRange(startChar, endChar);
                } else {
                    int startChar = Math.min(end, oldEnd);
                    int endChar = Math.max(end, oldEnd);
                    this.repaintRange(startChar, endChar);
                }
            } else if (hadSelection && !hasSelection) {
                this.repaintRange(oldBegin, oldEnd);
            }
        }
        this.lastMark = mark;
        this.lastDot = dot;
    }

    private void repaintRange(int start, int end) {
        try {
            Rectangle r = (Rectangle)this.view().modelToView(end, scratch, Position.Bias.Forward);
            int y1 = r.y + r.height;
            r = (Rectangle)this.view().modelToView(start, scratch, Position.Bias.Forward);
            r.x = 0;
            r.width = this.comp.getWidth();
            r.height = y1 - r.y;
            this.comp.repaint(r);
        }
        catch (BadLocationException e) {
            this.comp.repaint();
            Exceptions.printStackTrace((Throwable)e);
        }
    }

    static Action[] prepareActions() {
        DefaultEditorKit dek = new DefaultEditorKit();
        Action[] defActions = dek.getActions();
        Action[] newActions = new Action[defActions.length];
        for (int i = 0; i < defActions.length; ++i) {
            Object actionName = defActions[i].getValue("Name");
            newActions[i] = actionName.equals("caret-begin-line") ? new OutputBeginLineAction("caret-begin-line", false) : (actionName.equals("selection-begin-line") ? new OutputBeginLineAction("selection-begin-line", true) : (actionName.equals("caret-end-line") ? new OutputEndLineAction("caret-end-line", false) : (actionName.equals("selection-end-line") ? new OutputEndLineAction("selection-end-line", true) : defActions[i])));
        }
        return newActions;
    }

    static class OutputEndLineAction
    extends TextAction {
        private boolean select;

        OutputEndLineAction(String nm, boolean select) {
            super(nm);
            this.select = select;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            JTextComponent target = this.getTextComponent(e);
            if (target != null) {
                Document doc = target.getDocument();
                Element map = doc.getDefaultRootElement();
                int offs = target.getCaretPosition();
                int lineIndex = map.getElementIndex(offs);
                int lineEnd = map.getElement(lineIndex).getEndOffset() - 1;
                if (this.select) {
                    target.moveCaretPosition(lineEnd);
                } else {
                    target.setCaretPosition(lineEnd);
                }
            }
        }
    }

    static class OutputBeginLineAction
    extends TextAction {
        private boolean select;

        OutputBeginLineAction(String nm, boolean select) {
            super(nm);
            this.select = select;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            JTextComponent target = this.getTextComponent(e);
            if (target != null) {
                Document doc = target.getDocument();
                Element map = doc.getDefaultRootElement();
                int offs = target.getCaretPosition();
                int lineIndex = map.getElementIndex(offs);
                int lineStart = map.getElement(lineIndex).getStartOffset();
                if (this.select) {
                    target.moveCaretPosition(lineStart);
                } else {
                    target.setCaretPosition(lineStart);
                }
            }
        }
    }

}

