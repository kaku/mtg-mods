/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.output2;

import java.io.IOException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import org.netbeans.core.output2.BufferResource;
import org.netbeans.core.output2.FileMapStorage;
import org.netbeans.core.output2.Storage;

class HeapStorage
implements Storage {
    private boolean closed = true;
    private byte[] bytes = new byte[2048];
    private int size = 0;

    HeapStorage() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Storage toFileMapStorage() throws IOException {
        FileMapStorage result = new FileMapStorage();
        BufferResource<ByteBuffer> br = this.getReadBuffer(0, this.size);
        try {
            result.write(br.getBuffer());
            FileMapStorage fileMapStorage = result;
            return fileMapStorage;
        }
        finally {
            if (br != null) {
                br.releaseBuffer();
            }
        }
    }

    @Override
    public BufferResource<ByteBuffer> getReadBuffer(int start, int length) throws IOException {
        return new HeapBufferResource(ByteBuffer.wrap(this.bytes, start, Math.min(length, this.bytes.length - start)));
    }

    @Override
    public ByteBuffer getWriteBuffer(int length) throws IOException {
        return ByteBuffer.allocate(length);
    }

    @Override
    public synchronized int write(ByteBuffer buf) throws IOException {
        this.closed = false;
        int oldSize = this.size;
        this.size += buf.limit();
        if (this.size > this.bytes.length) {
            byte[] oldBytes = this.bytes;
            this.bytes = new byte[Math.max(oldSize * 2, buf.limit() * 2 + oldSize)];
            System.arraycopy(oldBytes, 0, this.bytes, 0, oldSize);
        }
        buf.flip();
        buf.get(this.bytes, oldSize, buf.limit());
        return oldSize;
    }

    @Override
    public void removeBytesFromEnd(int length) throws IOException {
        this.size -= length;
    }

    @Override
    public synchronized void dispose() {
        this.bytes = new byte[0];
        this.size = 0;
    }

    @Override
    public synchronized int size() {
        return this.size;
    }

    @Override
    public void flush() throws IOException {
    }

    @Override
    public void close() throws IOException {
        this.closed = true;
    }

    @Override
    public boolean isClosed() {
        return this.closed;
    }

    @Override
    public synchronized void shiftStart(int byteOffset) {
        this.size -= byteOffset;
        System.arraycopy(this.bytes, byteOffset, this.bytes, 0, this.size);
    }

    private class HeapBufferResource
    implements BufferResource<ByteBuffer> {
        private ByteBuffer buffer;

        public HeapBufferResource(ByteBuffer buffer) {
            this.buffer = buffer;
        }

        @Override
        public ByteBuffer getBuffer() {
            return this.buffer;
        }

        @Override
        public void releaseBuffer() {
            this.buffer = null;
        }
    }

}

