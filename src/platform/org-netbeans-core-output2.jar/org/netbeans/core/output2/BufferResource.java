/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.output2;

import java.nio.Buffer;

interface BufferResource<T extends Buffer> {
    public T getBuffer();

    public void releaseBuffer();
}

