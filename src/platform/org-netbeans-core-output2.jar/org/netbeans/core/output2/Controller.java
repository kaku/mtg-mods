/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.windows.IOColors
 *  org.openide.windows.IOColors$OutputType
 *  org.openide.windows.IOContainer
 *  org.openide.windows.IOContainer$CallBacks
 *  org.openide.windows.IOSelect
 *  org.openide.windows.IOSelect$AdditionalOperation
 *  org.openide.windows.InputOutput
 *  org.openide.windows.OutputEvent
 *  org.openide.windows.OutputWriter
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.core.output2;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.io.CharConversionException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.text.Document;
import org.netbeans.core.output2.IOEvent;
import org.netbeans.core.output2.Lines;
import org.netbeans.core.output2.NbIO;
import org.netbeans.core.output2.NbWriter;
import org.netbeans.core.output2.OutWriter;
import org.netbeans.core.output2.OutputDocument;
import org.netbeans.core.output2.OutputTab;
import org.netbeans.core.output2.options.OutputOptions;
import org.netbeans.core.output2.ui.AbstractOutputPane;
import org.openide.util.Exceptions;
import org.openide.windows.IOColors;
import org.openide.windows.IOContainer;
import org.openide.windows.IOSelect;
import org.openide.windows.InputOutput;
import org.openide.windows.OutputEvent;
import org.openide.windows.OutputWriter;
import org.openide.xml.XMLUtil;

public class Controller {
    static Controller controller;
    private Map<NbIO, WeakReference<OutputTab>> ioToTab = new WeakHashMap<NbIO, WeakReference<OutputTab>>();
    private static final boolean DONT_USE_HTML;
    private CoalescedNameUpdater nameUpdater = null;
    public static final boolean LOG;
    public static final boolean VERBOSE;
    static final boolean logStdOut;
    private static OutputStream logStream;

    public static Controller getDefault() {
        if (controller == null) {
            controller = new Controller();
        }
        return controller;
    }

    Controller() {
    }

    void eventDispatched(IOEvent ioe) {
        if (LOG) {
            Controller.log("Event received: " + ioe);
        }
        NbIO io = ioe.getIO();
        int command = ioe.getCommand();
        boolean value = ioe.getValue();
        Object data = ioe.getData();
        OutputTab comp = this.findTabForIo(io);
        if (LOG) {
            Controller.log("Passing command to controller " + ioe);
        }
        this.performCommand(comp, io, command, value, data);
        ioe.consume();
    }

    private OutputTab createOutputTab(NbIO io) {
        if (LOG) {
            Controller.log("Create component for nbio " + io);
        }
        OutputTab result = new OutputTab(io);
        result.setName(io.getName() + " ");
        Action[] a = io.getToolbarActions();
        if (a != null) {
            result.setToolbarActions(a);
        }
        if (LOG) {
            Controller.log("Adding new tab " + result);
        }
        this.ioToTab.put(io, new WeakReference<OutputTab>(result));
        IOContainer ioContainer = io.getIOContainer();
        ioContainer.add((JComponent)result, (IOContainer.CallBacks)result);
        ioContainer.setToolbarActions((JComponent)result, a);
        if (io.getIcon() != null) {
            ioContainer.setIcon((JComponent)result, io.getIcon());
        }
        if (io.getToolTipText() != null) {
            ioContainer.setToolTipText((JComponent)result, io.getToolTipText());
        }
        io.setClosed(false);
        OutputWriter out = io.getOut();
        if (out instanceof NbWriter) {
            ((NbWriter)out).out().setDisposeOnClose(false);
        }
        for (OutputTab tab : this.getAllTabs()) {
            this.updateName(tab);
        }
        return result;
    }

    void removeTab(NbIO io) {
        OutputTab tab;
        WeakReference<OutputTab> tabReference = this.ioToTab.remove(io);
        if (tabReference != null && (tab = tabReference.get()) != null) {
            this.removeFromUpdater(tab);
        }
    }

    void changeFontSizeBy(int change, boolean monospaced) {
        Font origFont = OutputOptions.getDefault().getFont(monospaced);
        int fontSize = origFont.getSize() + change;
        OutputOptions.getDefault().setFontSize(monospaced, fontSize);
        for (OutputTab tab : this.getAllTabs()) {
            OutputOptions opts = tab.getIO().getOptions();
            opts.setFontSize(monospaced, fontSize);
        }
        OutputOptions.storeDefault();
    }

    public void updateOptions(OutputOptions options) {
        options.setFontSize(true, options.getFont(false).getSize());
        for (OutputTab ot : this.getAllTabs()) {
            if (!ot.getIO().getIOContainer().equals((Object)IOContainer.getDefault())) continue;
            ot.getIO().getOptions().assign(options);
        }
    }

    void removeFromUpdater(OutputTab tab) {
        if (this.nameUpdater != null) {
            this.nameUpdater.remove(tab);
        }
    }

    void updateName(OutputTab tab) {
        if (this.nameUpdater == null) {
            if (LOG) {
                Controller.log("Update name for " + tab.getIO() + " dispatching a name updater");
            }
            this.nameUpdater = new CoalescedNameUpdater();
            SwingUtilities.invokeLater(this.nameUpdater);
        }
        this.nameUpdater.add(tab);
    }

    private static boolean htmlTabsBroken() {
        String version = System.getProperty("java.version");
        for (int i = 14; i < 18; ++i) {
            if (!version.startsWith("1.6.0_" + i)) continue;
            return true;
        }
        if (version.startsWith("1.6.0") && "Aqua".equals(UIManager.getLookAndFeel().getID())) {
            return true;
        }
        return false;
    }

    private static boolean isInSlidingMode(OutputTab tab) {
        for (Container p = tab; p != null; p = p.getParent()) {
            Object sliding;
            JComponent jp;
            if (!(p instanceof JComponent) || (sliding = (jp = (JComponent)p).getClientProperty("isSliding")) == null) continue;
            if (sliding.equals(Boolean.TRUE)) {
                return true;
            }
            return false;
        }
        return false;
    }

    void performCommand(OutputTab tab, NbIO io, int command, boolean value, Object data) {
        if (LOG) {
            Controller.log("PERFORMING: " + IOEvent.cmdToString(command) + " value=" + value + " on " + io + " tob " + tab);
        }
        IOContainer ioContainer = io.getIOContainer();
        switch (command) {
            case 0: {
                this.createOutputTab(io);
                break;
            }
            case 2: {
                if (value && tab == null) {
                    tab = this.createOutputTab(io);
                }
                if (tab == null) break;
                tab.setInputVisible(value);
                break;
            }
            case 6: {
                if (tab == null) {
                    tab = this.createOutputTab(io);
                }
                if (io.isFocusTaken() || Controller.isInSlidingMode(tab)) {
                    ioContainer.requestActive();
                }
                ioContainer.open();
                ioContainer.requestVisible();
                ioContainer.select((JComponent)tab);
                break;
            }
            case 15: {
                if (tab == null) {
                    tab = this.createOutputTab(io);
                }
                assert (data == null || data instanceof Set);
                Set extraOps = (Set)data;
                if (extraOps != null) {
                    if (io.isFocusTaken() && extraOps.contains((Object)IOSelect.AdditionalOperation.REQUEST_ACTIVE)) {
                        ioContainer.requestActive();
                    }
                    if (extraOps.contains((Object)IOSelect.AdditionalOperation.OPEN)) {
                        ioContainer.open();
                    }
                    if (extraOps.contains((Object)IOSelect.AdditionalOperation.REQUEST_VISIBLE)) {
                        ioContainer.requestVisible();
                    }
                }
                ioContainer.select((JComponent)tab);
                break;
            }
            case 10: {
                if (tab == null) break;
                Action[] a = (Action[])data;
                tab.setToolbarActions(a);
                tab.getIO().getIOContainer().setToolbarActions((JComponent)tab, a);
                break;
            }
            case 7: {
                if (tab != null) {
                    ioContainer.remove((JComponent)tab);
                }
                io.dispose();
                break;
            }
            case 8: {
                if (value) {
                    if (tab == null) {
                        if (io.out() == null) break;
                        io.out().dispose();
                        break;
                    }
                    this.updateName(tab);
                    if (tab.getIO().out() != null && tab.getIO().out().getLines().firstListenerLine() == -1) {
                        tab.getOutputPane().ensureCaretPosition();
                    }
                    if (tab != ioContainer.getSelected()) break;
                    tab.updateActions();
                    break;
                }
                if (tab == null || tab.getParent() == null) break;
                this.updateName(tab);
                break;
            }
            case 9: {
                if (tab == null) {
                    if (LOG) {
                        Controller.log("Got a reset on an io with no tab.  Creating a tab.");
                    }
                    this.createOutputTab(io);
                    ioContainer.requestVisible();
                    return;
                }
                if (LOG) {
                    Controller.log("Setting io " + io + " on tab " + tab);
                }
                tab.reset();
                this.updateName(tab);
                if (!LOG) break;
                Controller.log("Reset on " + tab + " tab displayable " + tab.isDisplayable() + " io " + io + " io.out " + io.out());
                break;
            }
            case 11: {
                if (tab == null) break;
                tab.getIO().getIOContainer().setIcon((JComponent)tab, (Icon)data);
                break;
            }
            case 12: {
                if (tab == null) break;
                tab.getIO().getIOContainer().setToolTipText((JComponent)tab, (String)data);
                break;
            }
            case 13: {
                if (tab == null) break;
                tab.getOutputPane().scrollTo((Integer)data);
                break;
            }
            case 14: {
                Lines lines;
                Document doc;
                if (tab == null || (doc = tab.getOutputPane().getDocument()) == null || !(doc instanceof OutputDocument) || (lines = ((OutputDocument)doc).getLines()) == null) break;
                IOColors.OutputType type = (IOColors.OutputType)data;
                lines.setDefColor(type, io.getColor(type));
                tab.getOutputPane().repaint();
            }
        }
    }

    public static void log(String s) {
        s = Long.toString(System.currentTimeMillis()) + ":" + s + "(" + Thread.currentThread() + ")  ";
        if (logStdOut) {
            System.out.println(s);
            return;
        }
        OutputStream os = Controller.getLogStream();
        byte[] b = new byte[s.length() + 1];
        char[] c = s.toCharArray();
        for (int i = 0; i < c.length; ++i) {
            b[i] = (byte)c[i];
        }
        b[b.length - 1] = 10;
        try {
            os.write(b);
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println(s);
        }
        try {
            os.flush();
        }
        catch (Exception e) {
            // empty catch block
        }
    }

    public static void logStack() {
        if (logStdOut) {
            new Exception().printStackTrace();
            return;
        }
        Exception e = new Exception();
        e.fillInStackTrace();
        StackTraceElement[] ste = e.getStackTrace();
        for (int i = 1; i < Math.min(22, ste.length); ++i) {
            Controller.log("   *   " + ste[i]);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private static OutputStream getLogStream() {
        if (logStream != null) return logStream;
        String spec = System.getProperty("java.io.tmpdir") + File.separator + "outlog.txt";
        Class<Controller> class_ = Controller.class;
        synchronized (Controller.class) {
            try {
                File f = new File(spec);
                if (f.exists()) {
                    f.delete();
                }
                f.createNewFile();
                logStream = new FileOutputStream(f);
            }
            catch (Exception e) {
                e.printStackTrace();
                logStream = System.err;
            }
            return logStream;
        }
    }

    private synchronized OutputTab findTabForIo(NbIO io) {
        OutputTab result;
        WeakReference<OutputTab> tabReference = this.ioToTab.get(io);
        OutputTab outputTab = result = tabReference == null ? null : tabReference.get();
        if (result == null && LOG) {
            Controller.log("Tab for IO " + io.getName() + " was not found.");
        }
        return result;
    }

    private synchronized List<OutputTab> getAllTabs() {
        LinkedList<OutputTab> tabs = new LinkedList<OutputTab>();
        for (WeakReference<OutputTab> tabReference : this.ioToTab.values()) {
            OutputTab outputTab;
            if (tabReference == null || (outputTab = tabReference.get()) == null) continue;
            tabs.add(outputTab);
        }
        return tabs;
    }

    static {
        DONT_USE_HTML = Controller.htmlTabsBroken();
        LOG = Boolean.getBoolean("nb.output.log") || Boolean.getBoolean("nb.output.log.verbose");
        VERBOSE = Boolean.getBoolean("nb.output.log.verbose");
        logStdOut = Boolean.getBoolean("nb.output.log.stdout");
        logStream = null;
    }

    static class ControllerOutputEvent
    extends OutputEvent {
        private int line;
        private OutWriter out;

        ControllerOutputEvent(NbIO io, int line) {
            super((InputOutput)io);
            this.out = io.out();
            this.line = line;
        }

        ControllerOutputEvent(NbIO io, OutWriter out, int line) {
            this(io, line);
            this.out = out;
        }

        void setLine(int line) {
            this.line = line;
        }

        public String getLine() {
            NbIO io = (NbIO)this.getSource();
            OutWriter out = io.out();
            try {
                if (out != null) {
                    String s = out.getLines().getLine(this.line);
                    if (s.endsWith("\n")) {
                        s = s.substring(0, s.length() - 1);
                    }
                    if (s.endsWith("\r")) {
                        s = s.substring(0, s.length() - 1);
                    }
                    return s;
                }
            }
            catch (IOException ioe) {
                IOException nue = new IOException("Could not fetch line " + this.line + " on " + io.getName());
                nue.initCause(ioe);
                Exceptions.printStackTrace((Throwable)ioe);
            }
            return null;
        }
    }

    class CoalescedNameUpdater
    implements Runnable {
        private Set<OutputTab> components;

        CoalescedNameUpdater() {
            this.components = new HashSet<OutputTab>();
        }

        public void add(OutputTab tab) {
            this.components.add(tab);
        }

        public void remove(OutputTab tab) {
            this.components.remove(tab);
        }

        boolean contains(OutputTab tab) {
            return this.components.contains(tab);
        }

        @Override
        public void run() {
            LinkedList<OutputTab> toRemove = null;
            for (OutputTab t : this.components) {
                String name;
                String escaped;
                NbIO io = t.getIO();
                if (!Controller.this.ioToTab.containsKey(io)) {
                    if (toRemove == null) {
                        toRemove = new LinkedList<OutputTab>();
                    }
                    toRemove.add(t);
                    continue;
                }
                if (Controller.LOG) {
                    Controller.log("Update name for " + io.getName() + " stream " + "closed is " + io.isStreamClosed());
                }
                try {
                    escaped = XMLUtil.toAttributeValue((String)io.getName());
                }
                catch (CharConversionException e) {
                    escaped = io.getName();
                }
                String string = io.isStreamClosed() ? io.getName() + " " : (name = DONT_USE_HTML ? io.getName() + " * " : "<html><b>" + escaped + " </b>&nbsp;</html>");
                if (Controller.LOG) {
                    Controller.log("  set name to " + name);
                }
                io.getIOContainer().setTitle((JComponent)t, name.replace("&apos;", "'"));
            }
            if (toRemove != null) {
                this.components.removeAll(toRemove);
            }
            Controller.this.nameUpdater = null;
        }
    }

}

