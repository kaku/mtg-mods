/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.output2.ui;

import java.awt.Component;
import java.awt.Insets;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleRole;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.KeyStroke;
import javax.swing.text.Document;
import org.netbeans.core.output2.Controller;
import org.netbeans.core.output2.OutputDocument;
import org.netbeans.core.output2.ui.AbstractOutputPane;
import org.netbeans.core.output2.ui.WeakAction;

public abstract class AbstractOutputTab
extends JComponent
implements Accessible {
    private boolean inputVisible = false;
    private AbstractOutputPane outputPane;
    private Action[] actions = new Action[0];
    protected static final String ACCELERATORS_KEY = "ACCELERATORS_KEY";
    private Component toFocus;

    public AbstractOutputTab() {
        this.outputPane = this.createOutputPane();
        this.add(this.outputPane);
        this.setFocusable(false);
    }

    public void setDocument(Document doc) {
        this.outputPane.setDocument(doc);
        if (doc instanceof OutputDocument) {
            ((OutputDocument)doc).setPane(this.outputPane);
        }
    }

    public AbstractOutputPane setOutputPane(AbstractOutputPane pane) {
        AbstractOutputPane old = this.outputPane;
        this.remove(this.outputPane);
        this.outputPane = pane;
        this.add(this.outputPane);
        return old;
    }

    @Override
    public AccessibleContext getAccessibleContext() {
        if (this.accessibleContext == null) {
            this.accessibleContext = new JComponent.AccessibleJComponent(){

                @Override
                public AccessibleRole getAccessibleRole() {
                    return AccessibleRole.PANEL;
                }

                @Override
                public String getAccessibleName() {
                    if (this.accessibleName != null) {
                        return this.accessibleName;
                    }
                    return AbstractOutputTab.this.getName();
                }
            };
        }
        return this.accessibleContext;
    }

    public void setToFocus(Component foc) {
        this.toFocus = foc;
    }

    @Override
    public void requestFocus() {
        if (this.toFocus != null) {
            this.toFocus.requestFocus();
            this.toFocus = null;
            return;
        }
        this.outputPane.requestFocus();
    }

    @Override
    public boolean requestFocusInWindow() {
        return this.getOutputPane().requestFocusInWindow();
    }

    protected abstract AbstractOutputPane createOutputPane();

    public abstract void inputSent(String var1);

    public final AbstractOutputPane getOutputPane() {
        return this.outputPane;
    }

    public final void setToolbarActions(Action[] a) {
        if (a == null || a.length == 0) {
            this.actions = new Action[0];
            return;
        }
        this.actions = new Action[a.length];
        for (int i = 0; i < a.length; ++i) {
            this.actions[i] = a[i];
            this.installKeyboardAction(this.actions[i]);
        }
    }

    public Action[] getToolbarActions() {
        return this.actions;
    }

    public void installKeyboardAction(Action a) {
        if (!(a instanceof WeakAction)) {
            a = new WeakAction(a);
        }
        KeyStroke[] accels = null;
        Object o = a.getValue("ACCELERATORS_KEY");
        if (o instanceof KeyStroke[]) {
            accels = (KeyStroke[])o;
        }
        String name = (String)a.getValue("Name");
        if (accels != null) {
            for (KeyStroke accel : accels) {
                if (Controller.LOG) {
                    Controller.log("Installed action " + name + " on " + accel);
                }
                JEditorPane c = this.getOutputPane().textView;
                c.getInputMap().put(accel, name);
                c.getActionMap().put(name, a);
                this.getInputMap(1).put(accel, name);
                this.getActionMap().put(name, a);
            }
        }
    }

    public final boolean isInputVisible() {
        return this.inputVisible;
    }

    public void setInputVisible(boolean val) {
        if (val == this.isInputVisible()) {
            return;
        }
        this.inputVisible = val;
        this.outputPane.textView.setEditable(val);
        this.validate();
        this.getOutputPane().ensureCaretPosition();
    }

    protected abstract void inputEof();

    @Override
    public void doLayout() {
        Insets ins = this.getInsets();
        int left = ins.left;
        int bottom = this.getHeight() - ins.bottom;
        AbstractOutputPane main = this.outputPane;
        if (main != null) {
            main.setBounds(left, ins.top, this.getWidth() - (left + ins.right), bottom - ins.top);
        }
    }

    public abstract void hasSelectionChanged(boolean var1);

    void notifyInputFocusGained() {
        this.getOutputPane().lockScroll();
        this.getOutputPane().ensureCaretPosition();
    }

}

