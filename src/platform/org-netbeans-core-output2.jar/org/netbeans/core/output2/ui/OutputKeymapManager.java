/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.options.keymap.api.ShortcutAction
 *  org.netbeans.core.options.keymap.spi.KeymapManager
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.core.output2.ui;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.core.options.keymap.api.ShortcutAction;
import org.netbeans.core.options.keymap.spi.KeymapManager;
import org.netbeans.core.output2.NbIOProvider;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class OutputKeymapManager
extends KeymapManager {
    private static final Logger LOG = Logger.getLogger(OutputKeymapManager.class.getName());
    private static final String CATEGORY_NAME = NbBundle.getMessage(NbIOProvider.class, (String)"OpenIDE-Module-Name");
    public static final String CLEAR_ACTION_ID = "output-window-clear";
    public static final String FILTER_ACTION_ID = "output-window-filter";
    public static final String LARGER_FONT_ACTION_ID = "output-window-larger-font";
    public static final String SMALLER_FONT_ACTION_ID = "output-window-smaller-font";
    public static final String CLOSE_ACTION_ID = "output-window-close";
    public static final String OUTPUT_SETTINGS_ACTION_ID = "output-window-settings";
    public static final String SAVE_AS_ACTION_ID = "output-window-save-as";
    public static final String WRAP_ACTION_ID = "output-window-wrap";
    public static final String STORAGE_DIR = "org-netbeans-core-output2/actions/";
    public static final String SHORTCUT_PREFIX = "sc";
    private final OutWinShortCutAction wrap;
    private final OutWinShortCutAction clear;
    private final OutWinShortCutAction filter;
    private final OutWinShortCutAction largerFont;
    private final OutWinShortCutAction smallerFont;
    private final OutWinShortCutAction closeWindow;
    private final OutWinShortCutAction fontType;
    private final OutWinShortCutAction saveAs;
    Map<String, Map<ShortcutAction, Set<String>>> keymaps;
    Map<ShortcutAction, Set<String>> defaultKeymap;
    private final Set<OutWinShortCutAction> allActions;
    Map<String, Set<ShortcutAction>> actions;

    public OutputKeymapManager() {
        super("OutputWindowKeymapManager");
        this.wrap = new OutWinShortCutAction("output-window-wrap", "ACTION_WRAP");
        this.clear = new OutWinShortCutAction("output-window-clear", "ACTION_CLEAR");
        this.filter = new OutWinShortCutAction("output-window-filter", "ACTION_FILTER");
        this.largerFont = new OutWinShortCutAction("output-window-larger-font", "ACTION_LARGER_FONT");
        this.smallerFont = new OutWinShortCutAction("output-window-smaller-font", "ACTION_SMALLER_FONT");
        this.closeWindow = new OutWinShortCutAction("output-window-close", "ACTION_CLOSE");
        this.fontType = new OutWinShortCutAction("output-window-settings", "ACTION_SETTINGS");
        this.saveAs = new OutWinShortCutAction("output-window-save-as", "ACTION_SAVEAS");
        this.keymaps = new HashMap<String, Map<ShortcutAction, Set<String>>>();
        this.defaultKeymap = new HashMap<ShortcutAction, Set<String>>();
        this.allActions = new HashSet<OutWinShortCutAction>();
        this.actions = new HashMap<String, Set<ShortcutAction>>();
        this.actions = new HashMap<String, Set<ShortcutAction>>();
        Collections.addAll(this.allActions, this.wrap, this.clear, this.filter, this.largerFont, this.smallerFont, this.closeWindow, this.fontType, this.saveAs);
        HashSet<OutWinShortCutAction> set = new HashSet<OutWinShortCutAction>();
        set.addAll(this.allActions);
        this.actions.put(CATEGORY_NAME, set);
        this.fillDefaultKeyMap();
        this.loadShortCuts();
    }

    private void fillDefaultKeyMap() {
        Iterator<OutWinShortCutAction> i$ = this.allActions.iterator();
        while (i$.hasNext()) {
            OutWinShortCutAction a;
            String dflt = (a = i$.next()).getDefaultShortcut();
            this.defaultKeymap.put(a, dflt != null && !dflt.isEmpty() ? Collections.singleton(dflt) : Collections.emptySet());
        }
    }

    public Map<String, Set<ShortcutAction>> getActions() {
        return this.actions;
    }

    public void refreshActions() {
    }

    public Map<ShortcutAction, Set<String>> getKeymap(String profileName) {
        Map<ShortcutAction, Set<String>> km = this.keymaps.get(profileName);
        if (km == null) {
            km = new HashMap<ShortcutAction, Set<String>>(this.defaultKeymap);
            this.keymaps.put(profileName, km);
        }
        return km;
    }

    public Map<ShortcutAction, Set<String>> getDefaultKeymap(String profileName) {
        return this.defaultKeymap;
    }

    public void saveKeymap(String profileName, Map<ShortcutAction, Set<String>> actionToShortcuts) {
        HashMap<OutWinShortCutAction, Set<String>> newShortcuts = new HashMap<OutWinShortCutAction, Set<String>>();
        this.keymaps.put(profileName, newShortcuts);
        for (OutWinShortCutAction owsa : this.allActions) {
            Set shortcuts = actionToShortcuts.get(owsa);
            if (shortcuts == null) {
                shortcuts = Collections.emptySet();
            }
            newShortcuts.put(owsa, shortcuts);
        }
        this.storeShortCuts(profileName);
    }

    public List<String> getProfiles() {
        return null;
    }

    public String getCurrentProfile() {
        return null;
    }

    public void setCurrentProfile(String profileName) {
    }

    public void deleteProfile(String profileName) {
    }

    public boolean isCustomProfile(String profileName) {
        return false;
    }

    private void storeShortCuts(String profileName) {
        FileObject root = FileUtil.getConfigRoot();
        try {
            FileObject actionsDir = FileUtil.createFolder((FileObject)root, (String)("org-netbeans-core-output2/actions/" + profileName));
            for (OutWinShortCutAction a : this.allActions) {
                FileObject data = actionsDir.getFileObject(a.getId());
                if (data == null) {
                    data = actionsDir.createData(a.getId());
                } else if (data.isFolder()) {
                    throw new IOException((Object)data + " is a folder.");
                }
                Enumeration atts = data.getAttributes();
                while (atts.hasMoreElements()) {
                    String attName = (String)atts.nextElement();
                    data.setAttribute(attName, (Object)null);
                }
                int index = 1;
                if (this.keymaps.get(profileName).get(a) == null) continue;
                for (String shortCut : this.keymaps.get(profileName).get(a)) {
                    data.setAttribute("sc" + index++, (Object)shortCut);
                }
            }
        }
        catch (IOException e) {
            LOG.log(Level.WARNING, "Cannot create folder", e);
        }
    }

    private void loadShortCuts() {
        FileObject root = FileUtil.getConfigRoot();
        FileObject actionsDir = root.getFileObject("org-netbeans-core-output2/actions/");
        if (actionsDir == null) {
            return;
        }
        for (FileObject profileDir : actionsDir.getChildren()) {
            if (!profileDir.isFolder()) continue;
            HashMap<OutWinShortCutAction, Set> keymap = new HashMap<OutWinShortCutAction, Set>();
            this.keymaps.put(profileDir.getName(), keymap);
            for (OutWinShortCutAction a : this.allActions) {
                FileObject actionFile = profileDir.getFileObject(a.getId());
                if (actionFile == null || !actionFile.isData()) {
                    keymap.put(a, Collections.emptySet());
                    continue;
                }
                Enumeration atts = actionFile.getAttributes();
                HashSet<String> strokes = new HashSet<String>();
                while (atts.hasMoreElements()) {
                    String att = (String)atts.nextElement();
                    if (!att.startsWith("sc")) continue;
                    strokes.add((String)actionFile.getAttribute(att));
                }
                keymap.put(a, strokes);
            }
        }
    }

    private class OutWinShortCutAction
    implements ShortcutAction {
        private String id;
        private String bundleKey;
        private String displayName;
        private String defaultShortcut;

        public OutWinShortCutAction(String id, String bundleKey) {
            String nbKeys;
            this.id = id;
            this.bundleKey = bundleKey;
            this.displayName = NbBundle.getMessage(NbIOProvider.class, (String)bundleKey);
            String nbKeysBundleKey = Utilities.isMac() ? bundleKey + ".accel.mac" : bundleKey + ".accel";
            this.defaultShortcut = nbKeys = NbBundle.getMessage(NbIOProvider.class, (String)nbKeysBundleKey);
        }

        public String getId() {
            return this.id;
        }

        public String getBundleKey() {
            return this.bundleKey;
        }

        public String getDisplayName() {
            return this.displayName;
        }

        public String getDefaultShortcut() {
            return this.defaultShortcut;
        }

        public String getDelegatingActionId() {
            return null;
        }

        public ShortcutAction getKeymapManagerInstance(String keymapManagerName) {
            return null;
        }
    }

}

