/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.WeakListeners
 */
package org.netbeans.core.output2.ui;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Action;
import javax.swing.Icon;
import org.openide.util.WeakListeners;

class WeakAction
implements Action,
PropertyChangeListener {
    private Reference<Action> original;
    private Icon icon;
    private List<PropertyChangeListener> listeners = new ArrayList<PropertyChangeListener>();
    private String name = null;
    private boolean wasEnabled = true;
    private boolean hadOriginal = true;

    public WeakAction(Action original) {
        this.wasEnabled = original.isEnabled();
        this.icon = (Icon)original.getValue("SmallIcon");
        this.name = (String)original.getValue("Name");
        this.original = new WeakReference<Action>(original);
        original.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)original));
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Action orig = this.getOriginal();
        if (orig != null) {
            orig.actionPerformed(actionEvent);
        }
    }

    @Override
    public synchronized void addPropertyChangeListener(PropertyChangeListener pce) {
        this.listeners.add(pce);
    }

    @Override
    public Object getValue(String str) {
        if ("SmallIcon".equals(str)) {
            return this.icon;
        }
        Action orig = this.getOriginal();
        if (orig != null) {
            return orig.getValue(str);
        }
        if ("Name".equals(str)) {
            return this.name;
        }
        return null;
    }

    @Override
    public boolean isEnabled() {
        Action orig = this.getOriginal();
        if (orig != null) {
            this.wasEnabled = orig.isEnabled();
            return this.wasEnabled;
        }
        return false;
    }

    @Override
    public void putValue(String str, Object obj) {
        if ("SmallIcon".equals(str)) {
            this.icon = (Icon)obj;
        } else {
            Action orig = this.getOriginal();
            if (orig != null) {
                orig.putValue(str, obj);
            }
        }
    }

    @Override
    public synchronized void removePropertyChangeListener(PropertyChangeListener pce) {
        this.listeners.remove(pce);
    }

    @Override
    public void setEnabled(boolean val) {
        Action orig = this.getOriginal();
        if (orig != null) {
            orig.setEnabled(val);
        }
    }

    private Action getOriginal() {
        Action result = this.original.get();
        if (result == null && this.hadOriginal && this.wasEnabled) {
            this.hadOriginal = false;
            this.firePropertyChange("enabled", Boolean.TRUE, Boolean.FALSE);
        }
        return result;
    }

    private synchronized void firePropertyChange(String nm, Object old, Object nue) {
        PropertyChangeEvent pce = new PropertyChangeEvent(this, nm, old, nue);
        for (PropertyChangeListener pcl : this.listeners) {
            pcl.propertyChange(pce);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent pce) {
        this.firePropertyChange(pce.getPropertyName(), pce.getOldValue(), pce.getNewValue());
        if ("enabled".equals(pce.getPropertyName())) {
            this.wasEnabled = Boolean.TRUE.equals(pce.getNewValue());
        }
    }
}

