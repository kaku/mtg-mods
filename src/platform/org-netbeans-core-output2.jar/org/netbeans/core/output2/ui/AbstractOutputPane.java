/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.windows.OutputListener
 */
package org.netbeans.core.output2.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import javax.swing.BorderFactory;
import javax.swing.BoundedRangeModel;
import javax.swing.JEditorPane;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.TextUI;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.DefaultCaret;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.Element;
import javax.swing.text.Highlighter;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;
import javax.swing.text.PlainDocument;
import javax.swing.text.Position;
import org.netbeans.core.output2.FoldingSideBar;
import org.netbeans.core.output2.Lines;
import org.netbeans.core.output2.OutputDocument;
import org.netbeans.core.output2.options.OutputOptions;
import org.netbeans.core.output2.ui.AbstractOutputTab;
import org.openide.util.Exceptions;
import org.openide.windows.OutputListener;

public abstract class AbstractOutputPane
extends JScrollPane
implements DocumentListener,
MouseListener,
MouseMotionListener,
KeyListener,
ChangeListener,
MouseWheelListener,
Runnable {
    private boolean locked = true;
    private int fontHeight = -1;
    private int fontWidth = -1;
    protected JEditorPane textView;
    private FoldingSideBar foldingSideBar;
    int lastCaretLine = 0;
    int caretBlinkRate = 500;
    boolean hadSelection = false;
    boolean recentlyReset = false;
    private boolean enqueued = false;
    private int lastLength = -1;
    private boolean inSendCaretToLine = false;
    private int lineToScroll = -1;
    private int lastPressedPos = -1;

    public AbstractOutputPane() {
        this.textView = this.createTextView();
        this.init();
    }

    public void doUpdateCaret() {
        Caret car = this.textView.getCaret();
        if (car instanceof DefaultCaret) {
            ((DefaultCaret)car).setUpdatePolicy(2);
        }
    }

    public void dontUpdateCaret() {
        Caret car = this.textView.getCaret();
        if (car instanceof DefaultCaret) {
            ((DefaultCaret)car).setUpdatePolicy(1);
        }
    }

    @Override
    public void requestFocus() {
        this.textView.requestFocus();
    }

    @Override
    public boolean requestFocusInWindow() {
        return this.textView.requestFocusInWindow();
    }

    public Font getViewFont() {
        return this.textView.getFont();
    }

    public void setViewFont(Font f) {
        this.textView.setFont(f);
        this.updateFont(this.getGraphics());
    }

    protected abstract JEditorPane createTextView();

    protected void documentChanged() {
        this.lastLength = -1;
        if (this.lineToScroll != -1) {
            if (this.scrollToLine(this.lineToScroll)) {
                this.lineToScroll = -1;
            }
        } else {
            this.ensureCaretPosition();
        }
        if (this.recentlyReset && this.isShowing()) {
            this.recentlyReset = false;
        }
        if (this.locked) {
            this.resetCursor();
        }
        if (this.isWrapped()) {
            this.getViewport().revalidate();
            this.getViewport().repaint();
        }
    }

    public abstract boolean isWrapped();

    public abstract void setWrapped(boolean var1);

    public boolean hasSelection() {
        return this.textView.getSelectionStart() != this.textView.getSelectionEnd();
    }

    public boolean isScrollLocked() {
        return this.locked;
    }

    public final void ensureCaretPosition() {
        if (!this.enqueued) {
            this.enqueued = true;
            SwingUtilities.invokeLater(this);
        }
    }

    @Override
    public void run() {
        this.enqueued = false;
        if (this.locked) {
            this.getVerticalScrollBar().setValue(this.getVerticalScrollBar().getModel().getMaximum());
            this.getHorizontalScrollBar().setValue(this.getHorizontalScrollBar().getModel().getMinimum());
        }
        this.ensureCaretAtVisiblePosition();
    }

    private void ensureCaretAtVisiblePosition() {
        assert (EventQueue.isDispatchThread());
        Lines lines = this.getLines();
        if (lines != null) {
            int caretLine;
            int origCaretLine = caretLine = lines.getLineAt(this.getCaretPos());
            while (caretLine >= 0 && !lines.isVisible(caretLine)) {
                caretLine = lines.getParentFoldStart(caretLine);
            }
            if (caretLine != origCaretLine && caretLine >= 0) {
                this.getCaret().setDot(lines.getLineStart(caretLine));
            }
        }
    }

    public int getSelectionStart() {
        return this.textView.getSelectionStart();
    }

    public int getSelectionEnd() {
        return this.textView.getSelectionEnd();
    }

    public String getSelectedText() {
        int start = this.getSelectionStart();
        int end = this.getSelectionEnd();
        String str = null;
        if (start > 0 && end > start) {
            try {
                str = this.getDocument().getText(start, end - start);
            }
            catch (BadLocationException ex) {
                ex.printStackTrace();
            }
        }
        return str;
    }

    public void setSelection(int start, int end) {
        int rend;
        int rstart = Math.min(start, end);
        if (rstart == (rend = Math.max(start, end))) {
            this.getCaret().setDot(rstart);
        } else {
            this.textView.setSelectionStart(rstart);
            this.textView.setSelectionEnd(rend);
        }
    }

    public void selectAll() {
        this.unlockScroll();
        this.getCaret().setVisible(true);
        this.textView.setSelectionStart(0);
        this.textView.setSelectionEnd(this.getLength());
    }

    public boolean isAllSelected() {
        return this.textView.getSelectionStart() == 0 && this.textView.getSelectionEnd() == this.getLength();
    }

    protected void init() {
        this.foldingSideBar = new FoldingSideBar(this.textView, this);
        this.setRowHeaderView(this.foldingSideBar);
        this.setViewportView(this.textView);
        this.textView.setEditable(false);
        this.textView.addMouseListener(this);
        this.textView.addMouseMotionListener(this);
        this.textView.addKeyListener(this);
        this.textView.addMouseWheelListener(this);
        OCaret oc = new OCaret();
        oc.setUpdatePolicy(1);
        this.textView.setCaret(oc);
        this.getCaret().setSelectionVisible(true);
        this.getVerticalScrollBar().getModel().addChangeListener(this);
        this.getVerticalScrollBar().addMouseMotionListener(this);
        this.getViewport().addMouseListener(this);
        this.getVerticalScrollBar().addMouseListener(this);
        this.setHorizontalScrollBarPolicy(30);
        this.setVerticalScrollBarPolicy(20);
        this.addMouseListener(this);
        this.getCaret().addChangeListener(this);
        this.textView.setFont(OutputOptions.getDefault().getFont(this.isWrapped()));
        this.setBorder(BorderFactory.createEmptyBorder());
        this.setViewportBorder(BorderFactory.createEmptyBorder());
        Color c = UIManager.getColor("nb.output.selectionBackground");
        if (c != null) {
            this.textView.setSelectionColor(c);
        }
    }

    public final Document getDocument() {
        return this.textView.getDocument();
    }

    public final JTextComponent getTextView() {
        return this.textView;
    }

    public final FoldingSideBar getFoldingSideBar() {
        return this.foldingSideBar;
    }

    public final void copy() {
        if (this.getCaret().getDot() != this.getCaret().getMark()) {
            this.textView.copy();
        } else {
            Toolkit.getDefaultToolkit().beep();
        }
    }

    public final void paste() {
        this.textView.paste();
    }

    protected void setDocument(Document doc) {
        if (this.hasSelection()) {
            this.hasSelectionChanged(false);
        }
        this.hadSelection = false;
        this.lastCaretLine = 0;
        this.lastLength = -1;
        this.lineToScroll = -1;
        Document old = this.textView.getDocument();
        old.removeDocumentListener(this);
        if (doc != null) {
            this.textView.setDocument(doc);
            doc.addDocumentListener(this);
            this.lockScroll();
            this.recentlyReset = true;
        } else {
            this.textView.setDocument(new PlainDocument());
            this.textView.setEditorKit(new DefaultEditorKit());
        }
    }

    protected void setEditorKit(EditorKit kit) {
        Document doc = this.textView.getDocument();
        this.textView.setEditorKit(kit);
        this.textView.setDocument(doc);
        this.updateKeyBindings();
    }

    protected final void updateKeyBindings() {
        Keymap keymap = this.textView.getKeymap();
        keymap.removeKeyStrokeBinding(KeyStroke.getKeyStroke(27, 0));
    }

    protected EditorKit getEditorKit() {
        return this.textView.getEditorKit();
    }

    public final int getLineCount() {
        return this.textView.getDocument().getDefaultRootElement().getElementCount();
    }

    public final int getLength() {
        if (this.lastLength == -1) {
            this.lastLength = this.textView.getDocument().getLength();
        }
        return this.lastLength;
    }

    public void scrollTo(int pos) {
        this.getCaret().setDot(pos);
        try {
            Rectangle rect = this.textView.modelToView(pos);
            if (rect != null) {
                int spaceAround = (this.textView.getVisibleRect().height - rect.height) / 2;
                Rectangle centeredRect = new Rectangle(rect.x, Math.max(0, rect.y - spaceAround + rect.height), rect.width, spaceAround * 2 + rect.height);
                this.textView.scrollRectToVisible(centeredRect);
            }
            this.locked = false;
        }
        catch (BadLocationException ex) {
            // empty catch block
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final void sendCaretToPos(int startPos, int endPos, boolean select) {
        block7 : {
            this.inSendCaretToLine = true;
            try {
                this.getCaret().setVisible(true);
                this.getCaret().setSelectionVisible(true);
                if (select) {
                    this.scrollTo(endPos);
                    this.getCaret().setDot(endPos);
                    this.getCaret().moveDot(startPos);
                    this.textView.repaint();
                    break block7;
                }
                this.getCaret().setDot(startPos);
            }
            catch (Error sie) {
                if (sie.getClass().getName().equals("javax.swing.text.StateInvariantError")) {
                    Exceptions.attachMessage((Throwable)sie, (String)("sendCaretToPos(" + startPos + ", " + endPos + ", " + select + "), caret = " + this.getCaret() + ", highlighter = " + this.textView.getHighlighter() + ", document length = " + this.textView.getDocument().getLength()));
                }
                Exceptions.printStackTrace((Throwable)sie);
            }
            finally {
                this.locked = false;
                this.inSendCaretToLine = false;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final boolean sendCaretToLine(int idx, boolean select) {
        int lastLine = this.getLineCount() - 1;
        if (idx > lastLine) {
            idx = lastLine;
        }
        this.inSendCaretToLine = true;
        try {
            this.getCaret().setVisible(true);
            this.getCaret().setSelectionVisible(true);
            Element el = this.textView.getDocument().getDefaultRootElement().getElement(idx);
            int position = el.getStartOffset();
            if (select) {
                this.getCaret().setDot(el.getEndOffset() - 1);
                this.getCaret().moveDot(position);
                this.textView.repaint();
            } else {
                this.getCaret().setDot(position);
            }
            if (!this.scrollToLine(idx + 3) && this.isScrollLocked()) {
                this.lineToScroll = idx + 3;
            }
        }
        catch (Error sie) {
            if (sie.getClass().getName().equals("javax.swing.text.StateInvariantError")) {
                Exceptions.attachMessage((Throwable)sie, (String)("sendCaretToLine(" + idx + ", " + select + "), caret = " + this.getCaret() + ", highlighter = " + this.textView.getHighlighter() + ", document length = " + this.textView.getDocument().getLength()));
            }
            Exceptions.printStackTrace((Throwable)sie);
        }
        finally {
            this.locked = false;
            this.inSendCaretToLine = false;
        }
        return true;
    }

    boolean scrollToLine(int line) {
        int lineIdx = Math.min(this.getLineCount() - 1, line);
        Rectangle rect = null;
        try {
            rect = this.textView.modelToView(this.textView.getDocument().getDefaultRootElement().getElement(lineIdx).getStartOffset());
        }
        catch (BadLocationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        if (rect == null) {
            return false;
        }
        boolean oldLocked = this.locked;
        this.textView.scrollRectToVisible(rect);
        this.locked = oldLocked;
        Rectangle visRect = this.textView.getVisibleRect();
        return line == lineIdx && visRect.y + visRect.height == rect.y + rect.height;
    }

    boolean scrollToPos(int pos) {
        Rectangle rect = null;
        try {
            rect = this.textView.modelToView(pos);
        }
        catch (BadLocationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        if (rect == null) {
            return false;
        }
        boolean oldLocked = this.locked;
        this.textView.scrollRectToVisible(rect);
        this.locked = oldLocked;
        return true;
    }

    public final void lockScroll() {
        if (!this.locked) {
            this.locked = true;
        }
    }

    public final void unlockScroll() {
        if (this.locked) {
            this.locked = false;
        }
        this.lineToScroll = -1;
    }

    protected abstract void caretPosChanged(int var1);

    protected abstract void lineClicked(int var1, int var2);

    protected abstract void enterPressed();

    protected abstract void postPopupMenu(Point var1, Component var2);

    public final int getCaretLine() {
        int result = 0;
        int charPos = this.getCaret().getDot();
        if (charPos > 0) {
            result = this.textView.getDocument().getDefaultRootElement().getElementIndex(charPos);
        }
        return result;
    }

    public final boolean isLineSelected(int idx) {
        Element line = this.textView.getDocument().getDefaultRootElement().getElement(idx);
        return line.getStartOffset() == this.getSelectionStart() && line.getEndOffset() - 1 == this.getSelectionEnd();
    }

    public final int getCaretPos() {
        return this.getCaret().getDot();
    }

    @Override
    public final void paint(Graphics g) {
        if (this.fontHeight == -1) {
            this.updateFont(g);
        }
        super.paint(g);
    }

    void updateFont(Graphics g) {
        if (g == null) {
            this.fontWidth = -1;
            this.fontHeight = -1;
            return;
        }
        this.fontHeight = g.getFontMetrics(this.textView.getFont()).getHeight();
        this.fontWidth = g.getFontMetrics(this.textView.getFont()).charWidth('m');
        this.getVerticalScrollBar().setUnitIncrement(this.fontHeight);
        this.getHorizontalScrollBar().setUnitIncrement(this.fontWidth);
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if (e.getSource() == this.getVerticalScrollBar().getModel()) {
            BoundedRangeModel mdl;
            if (!this.locked && (mdl = this.getVerticalScrollBar().getModel()).getValue() + mdl.getExtent() == mdl.getMaximum()) {
                this.lockScroll();
            }
        } else {
            boolean hasSelection;
            if (!this.locked) {
                this.maybeSendCaretEnteredLine();
            }
            boolean bl = hasSelection = this.textView.getSelectionStart() != this.textView.getSelectionEnd();
            if (hasSelection != this.hadSelection) {
                this.hadSelection = hasSelection;
                this.hasSelectionChanged(hasSelection);
            }
        }
    }

    private void maybeSendCaretEnteredLine() {
        if (EventQueue.getCurrentEvent() instanceof MouseEvent) {
            return;
        }
        if (!this.locked && !this.inSendCaretToLine) {
            boolean sel;
            boolean bl = sel = this.textView.getSelectionStart() != this.textView.getSelectionEnd();
            if (!sel) {
                this.caretPosChanged(this.getCaretPos());
            }
            if (sel != this.hadSelection) {
                this.hadSelection = sel;
                this.hasSelectionChanged(sel);
            }
        }
    }

    private void hasSelectionChanged(boolean sel) {
        AbstractOutputTab parent = (AbstractOutputTab)this.getParent();
        if (parent != null) {
            parent.hasSelectionChanged(sel);
        }
    }

    @Override
    public final void changedUpdate(DocumentEvent e) {
        e.getLength();
        this.documentChanged();
        if (e.getOffset() + e.getLength() >= this.getCaretPos() && (this.locked || !(e instanceof OutputDocument.DO))) {
            OutputDocument doc = (OutputDocument)e.getDocument();
            if (!(e instanceof OutputDocument.DO) && this.getCaretPos() >= doc.getOutputLength()) {
                return;
            }
            this.getCaret().setDot(e.getOffset() + e.getLength());
        }
    }

    @Override
    public final void insertUpdate(DocumentEvent e) {
        e.getLength();
        this.documentChanged();
        if (e.getOffset() + e.getLength() >= this.getCaretPos() && (this.locked || !(e instanceof OutputDocument.DO))) {
            OutputDocument doc = (OutputDocument)e.getDocument();
            if (!(e instanceof OutputDocument.DO) && this.getCaretPos() >= doc.getOutputLength()) {
                return;
            }
            this.getCaret().setDot(e.getOffset() + e.getLength());
        }
    }

    @Override
    public final void removeUpdate(DocumentEvent e) {
        e.getLength();
        this.documentChanged();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.isAltDown() || e.isAltGraphDown() || e.isControlDown() && SwingUtilities.isMiddleMouseButton(e)) {
            int currentSize = this.getViewFont().getSize();
            int defaultSize = OutputOptions.getDefaultFont().getSize();
            this.changeFontSizeBy(defaultSize - currentSize);
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
        this.resetCursor();
    }

    boolean isOnHyperlink(Point p) {
        Document doc = this.getDocument();
        if (doc instanceof OutputDocument) {
            int pos = this.textView.viewToModel(p);
            if (pos >= this.getLength()) {
                return false;
            }
            int line = this.getDocument().getDefaultRootElement().getElementIndex(pos);
            int lineStart = this.getDocument().getDefaultRootElement().getElement(line).getStartOffset();
            int lineLength = this.getDocument().getDefaultRootElement().getElement(line).getEndOffset() - lineStart;
            try {
                boolean onLine;
                Rectangle r = this.textView.modelToView(lineStart + lineLength - 1);
                boolean bl = onLine = p.x <= r.x + r.width || this.isWrapped() && p.y < r.y;
                if (onLine) {
                    return ((OutputDocument)doc).getLines().getListener(pos, null) != null;
                }
            }
            catch (BadLocationException ex) {
                // empty catch block
            }
        }
        return false;
    }

    private void resetCursor() {
        Cursor txtCursor = Cursor.getPredefinedCursor(2);
        if (this.textView.getCursor() != txtCursor) {
            this.textView.setCursor(txtCursor);
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        if (e.getSource() == this.textView) {
            if (this.isOnHyperlink(e.getPoint())) {
                Cursor hand = Cursor.getPredefinedCursor(12);
                if (this.textView.getCursor() != hand) {
                    this.textView.setCursor(hand);
                }
            } else {
                this.resetCursor();
            }
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        int y;
        if (e.getSource() == this.getVerticalScrollBar() && (y = e.getY()) > this.getVerticalScrollBar().getHeight()) {
            this.lockScroll();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getSource() == this.textView && SwingUtilities.isLeftMouseButton(e)) {
            this.lastPressedPos = this.textView.viewToModel(e.getPoint());
        }
        if (this.locked && !e.isPopupTrigger()) {
            Element el = this.getDocument().getDefaultRootElement().getElement(this.getLineCount() - 1);
            this.getCaret().setDot(el.getStartOffset());
            this.unlockScroll();
            if (e.getSource() == this.textView) {
                this.getCaret().setDot(this.textView.viewToModel(e.getPoint()));
            }
        }
        if (e.isPopupTrigger()) {
            Point p = SwingUtilities.convertPoint((Component)e.getSource(), e.getPoint(), this);
            this.postPopupMenu(p, this);
        }
    }

    @Override
    public final void mouseReleased(MouseEvent e) {
        if (e.getSource() == this.textView && SwingUtilities.isLeftMouseButton(e)) {
            int line;
            int pos = this.textView.viewToModel(e.getPoint());
            if (pos != -1 && pos == this.lastPressedPos && (line = this.textView.getDocument().getDefaultRootElement().getElementIndex(pos)) >= 0) {
                this.lineClicked(line, pos);
                e.consume();
            }
            this.lastPressedPos = -1;
        }
        if (e.isPopupTrigger()) {
            Point p = SwingUtilities.convertPoint((Component)e.getSource(), e.getPoint(), this);
            this.postPopupMenu(p, this);
        }
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        switch (keyEvent.getKeyCode()) {
            case 35: {
                if (!keyEvent.isControlDown()) break;
                this.lockScroll();
                break;
            }
            case 33: 
            case 34: 
            case 36: 
            case 37: 
            case 38: 
            case 39: 
            case 40: {
                this.unlockScroll();
                break;
            }
            case 10: {
                this.enterPressed();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {
    }

    protected abstract void changeFontSizeBy(int var1);

    @Override
    public final void mouseWheelMoved(MouseWheelEvent e) {
        if (e.isAltDown() || e.isAltGraphDown() || e.isControlDown()) {
            int change = - e.getWheelRotation();
            this.changeFontSizeBy(change);
            e.consume();
            return;
        }
        if (e.isShiftDown()) {
            BoundedRangeModel sbmodel = this.getHorizontalScrollBar().getModel();
            int currPosition = sbmodel.getValue();
            int newPosition = Math.max(0, Math.min(sbmodel.getMaximum(), currPosition + e.getUnitsToScroll() * this.fontWidth));
            sbmodel.setValue(newPosition);
            return;
        }
        BoundedRangeModel sbmodel = this.getVerticalScrollBar().getModel();
        int max = sbmodel.getMaximum();
        int range = sbmodel.getExtent();
        int currPosition = sbmodel.getValue();
        if (e.getSource() == this.textView) {
            int newPosition = Math.max(0, Math.min(sbmodel.getMaximum(), currPosition + e.getUnitsToScroll() * this.fontHeight));
            sbmodel.setValue(newPosition);
            if (newPosition + range >= max) {
                this.lockScroll();
                return;
            }
        }
        this.unlockScroll();
    }

    Caret getCaret() {
        return this.textView.getCaret();
    }

    public void collapseFold() {
        Lines l = this.getLines();
        if (l != null) {
            l.hideFold(l.getFoldStart(l.visibleToRealLine(this.getCaretLine())));
        }
    }

    public void expandFold() {
        Lines l = this.getLines();
        if (l != null) {
            l.showFold(l.getFoldStart(l.visibleToRealLine(this.getCaretLine())));
        }
    }

    public void collapseAllFolds() {
        Lines l = this.getLines();
        if (l != null) {
            l.hideAllFolds();
        }
    }

    public void expandAllFolds() {
        Lines l = this.getLines();
        if (l != null) {
            l.showAllFolds();
        }
    }

    public void collapseFoldTree() {
        Lines l = this.getLines();
        if (l != null) {
            l.hideFoldTree(l.getFoldStart(l.visibleToRealLine(this.getCaretLine())));
        }
    }

    public void expandFoldTree() {
        Lines l = this.getLines();
        if (l != null) {
            l.showFoldTree(l.getFoldStart(l.visibleToRealLine(this.getCaretLine())));
        }
    }

    private Lines getLines() {
        Document d = this.getDocument();
        if (d instanceof OutputDocument) {
            return ((OutputDocument)d).getLines();
        }
        return null;
    }

    private class OCaret
    extends DefaultCaret {
        private OCaret() {
        }

        @Override
        public void paint(Graphics g) {
            JEditorPane component = AbstractOutputPane.this.textView;
            if (this.isVisible() && this.y >= 0) {
                try {
                    TextUI mapper = component.getUI();
                    Rectangle r = mapper.modelToView(component, this.getDot(), Position.Bias.Forward);
                    if (r == null || r.width == 0 && r.height == 0) {
                        return;
                    }
                    if (this.width > 0 && this.height > 0 && !this._contains(r.x, r.y, r.width, r.height)) {
                        Rectangle clip = g.getClipBounds();
                        if (clip != null && !clip.contains(this)) {
                            this.repaint();
                        }
                        this.damage(r);
                    }
                    g.setColor(component.getCaretColor());
                    g.drawLine(r.x, r.y, r.x, r.y + r.height - 1);
                    g.drawLine(r.x + 1, r.y, r.x + 1, r.y + r.height - 1);
                }
                catch (BadLocationException e) {
                    // empty catch block
                }
            }
        }

        private boolean _contains(int X, int Y, int W, int H) {
            int w = this.width;
            int h = this.height;
            if ((w | h | W | H) < 0) {
                return false;
            }
            int x = this.x;
            int y = this.y;
            if (X < x || Y < y) {
                return false;
            }
            if (W > 0) {
                w += x;
                if ((W += X) <= X) {
                    if (w >= x || W > w) {
                        return false;
                    }
                } else if (w >= x && W > w) {
                    return true;
                }
            } else if (x + w < X) {
                return false;
            }
            if (H > 0) {
                h += y;
                if ((H += Y) <= Y ? h >= y || H > h : h >= y && H > h) {
                    return false;
                }
            } else if (y + h < Y) {
                return false;
            }
            return true;
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if (!e.isConsumed()) {
                super.mouseReleased(e);
            }
        }

        @Override
        public void focusGained(FocusEvent e) {
            AbstractOutputPane.this.getCaret().setBlinkRate(AbstractOutputPane.this.caretBlinkRate);
            AbstractOutputPane.this.getCaret().setVisible(true);
        }

        @Override
        public void focusLost(FocusEvent e) {
            AbstractOutputPane.this.getCaret().setVisible(false);
        }

        @Override
        public void setSelectionVisible(boolean vis) {
            if (vis) {
                super.setSelectionVisible(vis);
            }
        }
    }

}

