/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.OutputListener
 *  org.openide.windows.OutputWriter
 */
package org.netbeans.core.output2;

import java.awt.Color;
import java.io.IOException;
import java.io.Writer;
import java.util.logging.Logger;
import org.netbeans.core.output2.NbWriter;
import org.netbeans.core.output2.OutWriter;
import org.netbeans.core.output2.OutputKind;
import org.openide.windows.OutputListener;
import org.openide.windows.OutputWriter;

class ErrWriter
extends OutputWriter {
    private OutWriter wrapped;
    private final NbWriter parent;
    boolean closed = true;

    ErrWriter(OutWriter wrapped, NbWriter parent) {
        super((Writer)new OutWriter.DummyWriter());
        this.wrapped = wrapped;
        this.parent = parent;
    }

    synchronized void setWrapped(OutWriter wrapped) {
        this.wrapped = wrapped;
        this.closed = true;
    }

    public void println(String s, OutputListener l) throws IOException {
        this.println(s, l, false);
    }

    public void println(String s, OutputListener l, boolean important) throws IOException {
        this.closed = false;
        this.wrapped.print(s, l, important, null, null, OutputKind.ERR, true);
    }

    public void reset() throws IOException {
        Logger.getAnonymousLogger().warning("Do not call reset() on the error io, only on the output IO.  Reset on the error io does nothing.");
        this.closed = false;
    }

    public void close() {
        if (!this.closed) {
            this.closed = true;
            this.parent.notifyErrClosed();
        }
    }

    boolean isClosed() {
        return this.closed;
    }

    public void flush() {
        this.wrapped.flush();
    }

    public boolean checkError() {
        return this.wrapped.checkError();
    }

    public void write(int c) {
        this.print(String.valueOf(c), false);
    }

    public void write(char[] buf, int off, int len) {
        this.print(new OutWriter.CharArrayWrapper(buf, off, len), false);
    }

    public void write(String s, int off, int len) {
        this.print(s.substring(off, off + len), false);
    }

    public void println(boolean x) {
        this.print(x ? "true" : "false", true);
    }

    public void println(int i) {
        this.print(String.valueOf(i), true);
    }

    public void println(char c) {
        this.print(String.valueOf(c), true);
    }

    public void println(long l) {
        this.print(String.valueOf(l), true);
    }

    public void println(float x) {
        this.print(String.valueOf(x), true);
    }

    public void println(double x) {
        this.print(String.valueOf(x), true);
    }

    public void println(char[] buf) {
        this.print(new OutWriter.CharArrayWrapper(buf), true);
    }

    public void println(String s) {
        this.print(s, true);
    }

    public void println(Object x) {
        this.print(String.valueOf(x), true);
    }

    public void print(char[] buf) {
        this.print(new OutWriter.CharArrayWrapper(buf), false);
    }

    public void print(Object obj) {
        this.print(String.valueOf(obj), false);
    }

    public void print(char c) {
        this.print(String.valueOf(c), false);
    }

    public void print(int i) {
        this.print(String.valueOf(i), false);
    }

    public void print(String s) {
        this.print(s, false);
    }

    public void print(boolean b) {
        this.print(b ? "true" : "false", false);
    }

    public void println() {
        this.closed = false;
        this.wrapped.println();
    }

    private void print(CharSequence s, boolean addLineSep) {
        this.closed = false;
        this.wrapped.print(s, null, false, null, null, OutputKind.ERR, addLineSep);
    }
}

