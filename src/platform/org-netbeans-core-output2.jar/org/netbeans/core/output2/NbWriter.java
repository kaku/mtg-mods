/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.windows.OutputListener
 *  org.openide.windows.OutputWriter
 */
package org.netbeans.core.output2;

import java.io.IOException;
import java.io.Writer;
import org.netbeans.core.output2.Controller;
import org.netbeans.core.output2.ErrWriter;
import org.netbeans.core.output2.NbIO;
import org.netbeans.core.output2.OutWriter;
import org.openide.util.Exceptions;
import org.openide.windows.OutputListener;
import org.openide.windows.OutputWriter;

class NbWriter
extends OutputWriter {
    private final NbIO owner;
    private ErrWriter err = null;

    public NbWriter(OutWriter real, NbIO owner) {
        super((Writer)real);
        this.owner = owner;
    }

    public void println(String s, OutputListener l) throws IOException {
        ((OutWriter)this.out).println(s, l);
    }

    public void println(String s, OutputListener l, boolean important) throws IOException {
        ((OutWriter)this.out).println(s, l, important);
    }

    public synchronized void reset() throws IOException {
        if (!this.out().isDisposed() && this.out().isEmpty()) {
            return;
        }
        if (this.out != null) {
            if (Controller.LOG) {
                Controller.log("Disposing old OutWriter");
            }
            this.out().dispose();
        }
        if (Controller.LOG) {
            Controller.log("NbWriter.reset() replacing old OutWriter");
        }
        this.out = new OutWriter(this.owner);
        this.lock = this.out;
        if (this.err != null) {
            this.err.setWrapped((OutWriter)this.out);
        }
        this.owner.reset();
    }

    OutWriter out() {
        return (OutWriter)this.out;
    }

    ErrWriter err() {
        return this.err;
    }

    public synchronized ErrWriter getErr() {
        if (this.err == null) {
            this.err = new ErrWriter((OutWriter)this.out, this);
        }
        return this.err;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void close() {
        boolean isClosed;
        boolean wasClosed = this.isClosed();
        if (Controller.LOG) {
            Controller.log("NbWriter.close wasClosed=" + wasClosed + " out is " + this.out + " out is closed " + ((OutWriter)this.out).isClosed());
        }
        if (!wasClosed || !((OutWriter)this.out).isClosed()) {
            Object object = this.lock;
            synchronized (object) {
                try {
                    if (Controller.LOG) {
                        Controller.log("Now closing OutWriter");
                    }
                    this.out.close();
                    if (this.err != null) {
                        this.err.close();
                    }
                }
                catch (IOException ioe) {
                    Exceptions.printStackTrace((Throwable)ioe);
                }
            }
        }
        if (wasClosed != (isClosed = this.isClosed())) {
            if (Controller.LOG) {
                Controller.log("Setting streamClosed on InputOutput to " + isClosed);
            }
            this.owner.setStreamClosed(isClosed);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean isClosed() {
        OutWriter ow;
        OutWriter outWriter = ow = (OutWriter)this.out;
        synchronized (outWriter) {
            boolean result = ow.isClosed();
            if (result && this.err != null && !ow.checkError()) {
                result &= this.err.isClosed();
            }
            return result;
        }
    }

    public void notifyErrClosed() {
        if (this.isClosed()) {
            if (Controller.LOG) {
                Controller.log("NbWriter.notifyErrClosed - error stream has been closed");
            }
            this.owner.setStreamClosed(this.isClosed());
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void println(String s) {
        Object object = this.lock;
        synchronized (object) {
            ((OutWriter)this.out).println(s);
        }
    }
}

