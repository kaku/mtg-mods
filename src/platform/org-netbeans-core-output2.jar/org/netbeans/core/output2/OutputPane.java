/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package org.netbeans.core.output2;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BoundedRangeModel;
import javax.swing.InputMap;
import javax.swing.JEditorPane;
import javax.swing.JScrollBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.DocumentListener;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.PlainDocument;
import org.netbeans.core.output2.Controller;
import org.netbeans.core.output2.FoldingSideBar;
import org.netbeans.core.output2.Lines;
import org.netbeans.core.output2.OutputDocument;
import org.netbeans.core.output2.OutputEditorKit;
import org.netbeans.core.output2.OutputTab;
import org.netbeans.core.output2.options.OutputOptions;
import org.netbeans.core.output2.ui.AbstractOutputPane;
import org.openide.util.NbPreferences;

class OutputPane
extends AbstractOutputPane {
    OutputTab parent;
    PropertyChangeListener editorKitListener;

    public OutputPane(OutputTab parent) {
        this.editorKitListener = new OutputEditorKitListener();
        this.parent = parent;
    }

    @Override
    protected void documentChanged() {
        super.documentChanged();
        this.findOutputTab().documentChanged(this);
    }

    @Override
    protected void caretPosChanged(int pos) {
        this.findOutputTab().caretPosChanged(pos);
    }

    @Override
    protected void lineClicked(int line, int pos) {
        if (this.getDocument() instanceof OutputDocument) {
            this.findOutputTab().lineClicked(line, pos);
        }
    }

    @Override
    protected void enterPressed() {
        Caret caret = this.textView.getCaret();
        this.findOutputTab().enterPressed(caret.getMark(), caret.getDot());
    }

    @Override
    protected void postPopupMenu(Point p, Component src) {
        if (src.isShowing()) {
            this.findOutputTab().postPopupMenu(p, src);
        }
    }

    @Override
    public void mouseMoved(MouseEvent evt) {
        Document doc = this.getDocument();
        if (doc instanceof OutputDocument && ((OutputDocument)doc).getLines().hasListeners()) {
            super.mouseMoved(evt);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        super.mousePressed(e);
        if (!e.isPopupTrigger()) {
            this.findOutputTab().setToFocus((Component)e.getSource());
            this.findOutputTab().requestActive();
        }
    }

    private OutputTab findOutputTab() {
        return this.parent;
    }

    @Override
    protected void setDocument(Document doc) {
        if (doc == null) {
            Document d = this.getDocument();
            if (d != null) {
                d.removeDocumentListener(this);
            }
            this.textView.setDocument(new PlainDocument());
            return;
        }
        this.textView.setEditorKit(new OutputEditorKit(this.isWrapped(), this.textView, this.editorKitListener));
        super.setDocument(doc);
        this.updateKeyBindings();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setWrapped(boolean val) {
        if (val != this.isWrapped() || !(this.getEditorKit() instanceof OutputEditorKit)) {
            NbPreferences.forModule(OutputPane.class).putBoolean("wrap", val);
            this.textView.setFont(OutputOptions.getDefault().getFont(val));
            final int pos = this.textView.getCaret().getDot();
            Cursor cursor = this.textView.getCursor();
            try {
                this.textView.setCursor(Cursor.getPredefinedCursor(3));
                this.setEditorKit(new OutputEditorKit(val, this.textView, this.editorKitListener));
            }
            finally {
                this.textView.setCursor(cursor);
            }
            SwingUtilities.invokeLater(new Runnable(){
                private boolean first;

                @Override
                public void run() {
                    if (this.first) {
                        this.first = false;
                        SwingUtilities.invokeLater(this);
                        return;
                    }
                    OutputPane.this.textView.getCaret().setDot(pos);
                }
            });
            if (this.getDocument() instanceof OutputDocument && ((OutputDocument)this.getDocument()).getLines().isGrowing()) {
                this.lockScroll();
            }
            if (!val) {
                this.getHorizontalScrollBar().setValue(this.getHorizontalScrollBar().getModel().getMinimum());
            }
            this.validate();
            this.getFoldingSideBar().setWrapped(val);
        }
    }

    @Override
    public boolean isWrapped() {
        if (this.getEditorKit() instanceof OutputEditorKit) {
            return this.getEditorKit() instanceof OutputEditorKit && ((OutputEditorKit)this.getEditorKit()).isWrapped();
        }
        return NbPreferences.forModule(OutputPane.class).getBoolean("wrap", false);
    }

    @Override
    protected JEditorPane createTextView() {
        JEditorPane result = new JEditorPane();
        if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
            result.setBackground(UIManager.getColor("NbExplorerView.background"));
        } else if ("GTK".equals(UIManager.getLookAndFeel().getID())) {
            result.setBackground(UIManager.getColor("text"));
        }
        result.setDisabledTextColor(result.getBackground());
        InputMap map = result.getInputMap();
        MyInputMap myMap = new MyInputMap();
        myMap.setParent(map);
        result.setInputMap(0, myMap);
        AbstractAction act = new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                OutputDocument od = (OutputDocument)((JEditorPane)arg0.getSource()).getDocument();
                OutputPane.this.findOutputTab().inputSent(od.sendLine());
            }
        };
        result.getActionMap().put("SENDLINE", act);
        act = new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                OutputDocument od = (OutputDocument)((JEditorPane)arg0.getSource()).getDocument();
                OutputPane.this.findOutputTab().inputSent(od.sendLine());
                OutputPane.this.findOutputTab().inputEof();
            }
        };
        result.getActionMap().put("EOF", act);
        result.setDragEnabled(false);
        return result;
    }

    @Override
    protected void changeFontSizeBy(int change) {
        Controller.getDefault().changeFontSizeBy(change, this.isWrapped());
    }

    private class OutputEditorKitListener
    implements PropertyChangeListener {
        private OutputEditorKitListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("charsPerLine".equals(evt.getPropertyName())) {
                OutputPane.this.getFoldingSideBar().setCharsPerLine((Integer)evt.getNewValue());
            }
        }
    }

    protected class MyInputMap
    extends InputMap {
        @Override
        public Object get(KeyStroke keyStroke) {
            KeyStroke stroke = KeyStroke.getKeyStroke("control shift O");
            if (keyStroke.equals(stroke)) {
                return null;
            }
            stroke = KeyStroke.getKeyStroke(8, 2);
            if (keyStroke.equals(stroke)) {
                return null;
            }
            stroke = KeyStroke.getKeyStroke(10, 0);
            if (keyStroke.equals(stroke) && OutputPane.this.findOutputTab().isInputVisible()) {
                return "SENDLINE";
            }
            stroke = KeyStroke.getKeyStroke(68, 2);
            if (keyStroke.equals(stroke)) {
                return "EOF";
            }
            Object retValue = super.get(keyStroke);
            return retValue;
        }
    }

}

