/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Pair
 *  org.openide.windows.IOColors
 *  org.openide.windows.IOColors$OutputType
 *  org.openide.windows.OutputListener
 */
package org.netbeans.core.output2;

import java.awt.Color;
import java.io.IOException;
import java.util.Collection;
import javax.swing.event.ChangeListener;
import org.netbeans.core.output2.LineInfo;
import org.openide.util.Pair;
import org.openide.windows.IOColors;
import org.openide.windows.OutputListener;

public interface Lines {
    public int[] getLinesWithListeners();

    public int length(int var1);

    public int lengthWithTabs(int var1);

    public int getLineStart(int var1);

    public int getLineAt(int var1);

    public int getLineCount();

    public Collection<OutputListener> getListenersForLine(int var1);

    public LineInfo getLineInfo(int var1);

    public void setDefColor(IOColors.OutputType var1, Color var2);

    public Color getDefColor(IOColors.OutputType var1);

    public int firstListenerLine();

    public int firstImportantListenerLine();

    public boolean isImportantLine(int var1);

    public OutputListener nearestListener(int var1, boolean var2, int[] var3);

    public int getLongestLineLength();

    public int getLogicalLineCountAbove(int var1, int var2);

    public int getLogicalLineCountIfWrappedAt(int var1);

    public int getNumPhysicalChars(int var1, int var2, int[] var3);

    public int getNumLogicalChars(int var1, int var2);

    public boolean isLineStart(int var1);

    public String getLine(int var1) throws IOException;

    public boolean hasListeners();

    public OutputListener getListener(int var1, int[] var2);

    public boolean isListener(int var1, int var2);

    public int getCharCount();

    public String getText(int var1, int var2);

    public char[] getText(int var1, int var2, char[] var3);

    public void toPhysicalLineIndex(int[] var1, int var2);

    public void saveAs(String var1) throws IOException;

    public int[] find(int var1, String var2, boolean var3, boolean var4);

    public int[] rfind(int var1, String var2, boolean var3, boolean var4);

    public Object readLock();

    public void addChangeListener(ChangeListener var1);

    public void removeChangeListener(ChangeListener var1);

    public boolean checkDirty(boolean var1);

    public boolean isGrowing();

    public void showFold(int var1);

    public void hideFold(int var1);

    public void showFoldAndParentFolds(int var1);

    public void showFoldsForLine(int var1);

    public void showAllFolds();

    public void hideAllFolds();

    public void showFoldTree(int var1);

    public void hideFoldTree(int var1);

    public boolean isVisible(int var1);

    public int getVisibleLineCount();

    public int realToVisibleLine(int var1);

    public int visibleToRealLine(int var1);

    public int getFoldStart(int var1);

    public int getParentFoldStart(int var1);

    public Pair<Integer, Integer> removeCharsFromLastLine(int var1);
}

