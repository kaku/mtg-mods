/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.IOColors
 *  org.openide.windows.IOColors$OutputType
 *  org.openide.windows.OutputListener
 */
package org.netbeans.core.output2;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import org.netbeans.core.output2.Lines;
import org.netbeans.core.output2.OutputKind;
import org.openide.windows.IOColors;
import org.openide.windows.OutputListener;

public class LineInfo {
    List<Segment> segments = new CopyOnWriteArrayList<Segment>();
    final Lines parent;

    LineInfo(Lines parent) {
        this.parent = parent;
    }

    LineInfo(Lines parent, int end) {
        this(parent, end, OutputKind.OUT, null, null, null, false);
    }

    LineInfo(Lines parent, int end, OutputKind outKind, OutputListener l, Color c, Color b, boolean important) {
        this.parent = parent;
        this.addSegment(end, outKind, l, c, b, important);
    }

    int getEnd() {
        return this.segments.isEmpty() ? 0 : this.segments.get(this.segments.size() - 1).getEnd();
    }

    void addSegment(int end, OutputKind outKind, OutputListener l, Color c, Color b, boolean important) {
        boolean isColor;
        Segment s = null;
        if (!this.segments.isEmpty() && (s = this.segments.get(this.segments.size() - 1)).getKind() == outKind && s.getListener() == l && this.hasColors(s, c, b)) {
            s.end = end;
            return;
        }
        boolean bl = isColor = c != null || b != null;
        s = l != null ? (isColor ? new ColorListenerSegment(end, l, important, c, b) : new ListenerSegment(end, l, important)) : (isColor ? new ColorSegment(end, outKind, c, b) : new Segment(end, outKind));
        this.segments.add(s);
    }

    private boolean hasColors(Segment s, Color c, Color b) {
        return this.hasForeground(s, c) && this.hasBackground(s, b);
    }

    private boolean hasForeground(Segment s, Color c) {
        return s.getCustomColor() == c || c != null && c.equals(s.getCustomColor());
    }

    private boolean hasBackground(Segment s, Color b) {
        return s.getCustomBackground() == b || b != null && b.equals(s.getCustomBackground());
    }

    OutputListener getListenerAfter(int pos, int[] range) {
        int start = 0;
        for (Segment s : this.segments) {
            if (s.getEnd() < pos) continue;
            if (s.getListener() != null) {
                if (range != null) {
                    range[0] = start;
                    range[1] = s.getEnd();
                }
                return s.getListener();
            }
            start = s.getEnd();
        }
        return null;
    }

    OutputListener getListenerBefore(int pos, int[] range) {
        for (int i = this.segments.size() - 1; i >= 0; --i) {
            int startPos;
            int n = startPos = i == 0 ? 0 : this.segments.get(i - 1).getEnd();
            if (startPos > pos || this.segments.get(i).getListener() == null) continue;
            if (range != null) {
                range[0] = startPos;
                range[1] = this.segments.get(i).getEnd();
            }
            return this.segments.get(i).getListener();
        }
        return null;
    }

    OutputListener getFirstListener(int[] range) {
        int pos = 0;
        for (Segment s : this.segments) {
            if (s.getListener() != null) {
                if (range != null) {
                    range[0] = pos;
                    range[1] = s.getEnd();
                }
                return s.getListener();
            }
            pos = s.getEnd();
        }
        return null;
    }

    OutputListener getLastListener(int[] range) {
        for (int i = this.segments.size() - 1; i >= 0; --i) {
            Segment s = this.segments.get(i);
            if (s.getListener() == null) continue;
            if (range != null) {
                range[0] = i == 0 ? 0 : this.segments.get(i - 1).getEnd();
                range[1] = s.getEnd();
            }
            return s.getListener();
        }
        return null;
    }

    Collection<Segment> getLineSegments() {
        return this.segments;
    }

    Collection<OutputListener> getListeners() {
        ArrayList<OutputListener> ol = new ArrayList<OutputListener>();
        for (Segment s : this.segments) {
            OutputListener l = s.getListener();
            if (l == null) continue;
            ol.add(l);
        }
        return ol;
    }

    private class ColorListenerSegment
    extends ListenerSegment {
        final Color color;
        final Color background;

        public ColorListenerSegment(int end, OutputListener l, boolean important, Color color, Color background) {
            super(end, l, important);
            this.color = color == null ? super.getColor() : color;
            this.background = background;
        }

        @Override
        Color getColor() {
            return this.color;
        }

        @Override
        Color getCustomColor() {
            return this.color;
        }

        @Override
        Color getCustomBackground() {
            return this.background;
        }
    }

    private class ListenerSegment
    extends Segment {
        final OutputListener listener;
        final boolean important;

        public ListenerSegment(int end, OutputListener l, boolean important) {
            super(end, OutputKind.OUT);
            this.listener = l;
            this.important = important;
        }

        @Override
        OutputListener getListener() {
            return this.listener;
        }

        @Override
        Color getColor() {
            return LineInfo.this.parent.getDefColor(this.important ? IOColors.OutputType.HYPERLINK_IMPORTANT : IOColors.OutputType.HYPERLINK);
        }
    }

    private class ColorSegment
    extends Segment {
        final Color color;
        final Color background;

        public ColorSegment(int end, OutputKind outputKind, Color color, Color background) {
            super(end, outputKind);
            this.color = color == null ? super.getColor() : color;
            this.background = background;
        }

        @Override
        Color getColor() {
            return this.color;
        }

        @Override
        Color getCustomColor() {
            return this.color;
        }

        @Override
        Color getCustomBackground() {
            return this.background;
        }
    }

    public class Segment {
        private int end;
        private OutputKind outputKind;

        Segment(int end, OutputKind outputKind) {
            this.end = end;
            this.outputKind = outputKind;
        }

        int getEnd() {
            return this.end;
        }

        OutputListener getListener() {
            return null;
        }

        OutputKind getKind() {
            return this.outputKind;
        }

        Color getColor() {
            IOColors.OutputType type;
            switch (this.outputKind) {
                case OUT: {
                    type = IOColors.OutputType.OUTPUT;
                    break;
                }
                case ERR: {
                    type = IOColors.OutputType.ERROR;
                    break;
                }
                case IN: {
                    type = IOColors.OutputType.INPUT;
                    break;
                }
                default: {
                    type = IOColors.OutputType.OUTPUT;
                }
            }
            return LineInfo.this.parent.getDefColor(type);
        }

        Color getCustomColor() {
            return null;
        }

        Color getCustomBackground() {
            return null;
        }
    }

}

