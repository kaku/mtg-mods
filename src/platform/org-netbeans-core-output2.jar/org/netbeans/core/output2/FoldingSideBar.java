/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.output2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.core.output2.AbstractLines;
import org.netbeans.core.output2.IntListSimple;
import org.netbeans.core.output2.Lines;
import org.netbeans.core.output2.OutputDocument;
import org.netbeans.core.output2.ui.AbstractOutputPane;

public class FoldingSideBar
extends JComponent {
    private static final Logger LOG = Logger.getLogger(FoldingSideBar.class.getName());
    private final int BAR_WIDTH = 15;
    private final JEditorPane textView;
    private AbstractLines lines;
    private int charsPerLine = 80;
    private boolean wrapped;
    private int activeFold = -1;

    public FoldingSideBar(JEditorPane textView, AbstractOutputPane outputPane) {
        this.textView = textView;
        this.lines = this.getLines();
        textView.addPropertyChangeListener("document", new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                FoldingSideBar.this.lines = FoldingSideBar.this.getLines();
            }
        });
        textView.addComponentListener(new ComponentAdapter(){

            @Override
            public void componentResized(ComponentEvent e) {
                FoldingSideBar.this.setPreferredSize(new Dimension(15, FoldingSideBar.this.textView.getHeight()));
                FoldingSideBar.this.repaint();
            }
        });
        this.setMinimumSize(new Dimension(15, 0));
        this.setPreferredSize(new Dimension(15, textView.getHeight()));
        this.setMaximumSize(new Dimension(15, Integer.MAX_VALUE));
        this.wrapped = outputPane.isWrapped();
        this.addMouseListener(new FoldingMouseListener());
        this.addMouseMotionListener(new FoldingMouseListener());
    }

    private AbstractLines getLines() {
        Document doc = this.textView.getDocument();
        if (doc instanceof OutputDocument) {
            Lines l = ((OutputDocument)doc).getLines();
            if (l instanceof AbstractLines) {
                return (AbstractLines)l;
            }
            return null;
        }
        return null;
    }

    @Override
    public void paintComponent(Graphics g) {
        Rectangle cp = g.getClipBounds();
        g.setColor(this.getBackground());
        g.fillRect(cp.x, cp.y, cp.width, cp.height);
        if (this.lines == null) {
            return;
        }
        g.setColor(this.getForeground());
        FontMetrics fontMetrics = this.textView.getFontMetrics(this.textView.getFont());
        int lineHeight = fontMetrics.getHeight();
        int descent = fontMetrics.getDescent();
        int offset = 0;
        try {
            Rectangle modelToView = this.textView.modelToView(0);
            offset = modelToView == null ? 0 : modelToView.y;
        }
        catch (BadLocationException ex) {
            LOG.log(Level.INFO, null, ex);
        }
        offset += lineHeight - fontMetrics.getAscent();
        int size = this.lines.getLineCount();
        int logLine = 0;
        int firstVisibleLine = Math.max(0, this.getLineAtPosition(cp.y) - 1);
        int lastVisibleLine = this.getLastVisibleLine(cp, size);
        for (int i = firstVisibleLine; i < lastVisibleLine; ++i) {
            if (!this.lines.isVisible(i)) continue;
            int nextLogLine = this.findLogicalLineIndex(this.findNextVisibleLine(i), size);
            this.drawLineGraphics(g, i, logLine, nextLogLine, offset, lineHeight, descent);
            logLine = nextLogLine;
        }
    }

    private int getLastVisibleLine(Rectangle cp, int realLineSize) {
        int lineAtClipBoundsEnd = this.getLineAtPosition(cp.y + cp.height);
        return lineAtClipBoundsEnd < 0 ? realLineSize - 1 : Math.min(realLineSize - 1, lineAtClipBoundsEnd + 1);
    }

    private int getLineAtPosition(int y) {
        int physicalLine;
        FontMetrics fontMetrics = this.textView.getFontMetrics(this.textView.getFont());
        int lineHeight = fontMetrics.getHeight();
        int offset = 0;
        try {
            Rectangle modelToView = this.textView.modelToView(0);
            offset = modelToView == null ? 0 : modelToView.y;
        }
        catch (BadLocationException ex) {
            LOG.log(Level.INFO, null, ex);
        }
        int logicalLine = (y - (offset += lineHeight - fontMetrics.getAscent())) / lineHeight;
        if (this.wrapped) {
            int[] info = new int[]{logicalLine, 0, 0};
            this.lines.toPhysicalLineIndex(info, this.charsPerLine);
            physicalLine = info[0];
        } else {
            physicalLine = logicalLine < this.lines.getVisibleLineCount() ? this.lines.visibleToRealLine(logicalLine) : -1;
        }
        return physicalLine;
    }

    private void drawLineGraphics(Graphics g, int line, int logLine, int nextLogLine, int offset, int lineHeight, int descent) {
        int nextOffset;
        int currOffset;
        try {
            currOffset = this.lines.getFoldOffsets().get(line);
            nextOffset = line + 1 < this.lines.getFoldOffsets().size() ? this.lines.getFoldOffsets().get(line + 1) : 0;
        }
        catch (IndexOutOfBoundsException ioobe) {
            LOG.log(Level.FINE, null, ioobe);
            return;
        }
        int startY = logLine * lineHeight + offset;
        int endY = nextLogLine * lineHeight + offset;
        if (nextOffset == 1) {
            this.drawButton(g, startY, endY, line);
        } else if (currOffset != 0 && currOffset + 1 == nextOffset) {
            if (this.isActive(line)) {
                g.drawLine(6, startY, 6, endY);
            }
            g.drawLine(7, startY, 7, endY);
        } else if (currOffset > 0 && nextOffset == 0) {
            this.drawFoldEnd(g, line, startY, FoldingSideBar.lineMid(endY, lineHeight, descent));
        } else if (currOffset > 0 && nextOffset > 0) {
            this.drawNestedFoldEnd(g, line, startY, endY, FoldingSideBar.lineMid(endY, lineHeight, descent));
        }
    }

    private static int lineMid(int lineEndY, int lineHeight, int descent) {
        return lineEndY - lineHeight / 2 - descent;
    }

    private void drawButton(Graphics g, int lineStartY, int lineEndY, int line) {
        boolean collapsed = !this.lines.isVisible(line + 1);
        g.drawRect(2, lineStartY, 10, 10);
        g.drawLine(5, lineStartY + 5, 9, lineStartY + 5);
        if (collapsed) {
            g.drawLine(7, lineStartY + 3, 7, lineStartY + 7);
        }
        if (lineEndY > lineStartY + 10 && (!collapsed || this.isLastVisibleLineInFold(line))) {
            g.drawLine(7, lineStartY + 10, 7, lineEndY);
            if (this.isActive(line)) {
                g.drawLine(6, lineStartY + 10, 6, lineEndY);
            }
        }
    }

    private boolean isLastVisibleLineInFold(int line) {
        if (this.lines.getFoldOffsets().get(line) > 0) {
            int visibleLine = this.lines.realToVisibleLine(line);
            int nextVisibleRealIndex = this.lines.visibleToRealLine(visibleLine + 1);
            if (nextVisibleRealIndex >= this.lines.getFoldOffsets().size()) {
                return true;
            }
            return this.lines.getFoldOffsets().get(nextVisibleRealIndex) > 0;
        }
        return false;
    }

    private void drawNestedFoldEnd(Graphics g, int lineIndex, int lineStartY, int lineEndY, int lineMid) {
        g.drawLine(7, lineStartY, 7, lineEndY);
        g.drawLine(7, lineMid, 11, lineMid);
        if (this.isActive(lineIndex)) {
            g.drawLine(6, lineStartY, 6, lineMid);
            g.drawLine(7, lineMid - 1, 11, lineMid - 1);
            if (this.isActive(this.findNextVisibleLine(lineIndex))) {
                g.drawLine(6, lineMid, 6, lineEndY);
            }
        }
    }

    private void drawFoldEnd(Graphics g, int lineIndex, int lineStartY, int lineMid) {
        g.drawLine(7, lineStartY, 7, lineMid);
        g.drawLine(7, lineMid, 11, lineMid);
        if (this.isActive(lineIndex)) {
            g.drawLine(6, lineStartY, 6, lineMid);
            g.drawLine(7, lineMid - 1, 11, lineMid - 1);
        }
    }

    private int findLogicalLineIndex(int physicalLineIndex, int size) {
        if (this.wrapped) {
            if (physicalLineIndex < size) {
                return this.lines.getLogicalLineCountAbove(physicalLineIndex, this.charsPerLine);
            }
            return this.lines.getLogicalLineCountIfWrappedAt(this.charsPerLine);
        }
        return this.lines.realToVisibleLine(physicalLineIndex);
    }

    private int findNextVisibleLine(int physicalLine) {
        int visibleLineIndex = this.lines.realToVisibleLine(physicalLine);
        if (visibleLineIndex < 0) {
            return this.lines.getVisibleLineCount() - 1;
        }
        if (visibleLineIndex + 1 < this.lines.getVisibleLineCount()) {
            return this.lines.visibleToRealLine(visibleLineIndex + 1);
        }
        return this.lines.getVisibleLineCount() - 1;
    }

    public void setWrapped(boolean wrapped) {
        this.wrapped = wrapped;
        this.repaint();
    }

    public void setCharsPerLine(int charsPerLine) {
        this.charsPerLine = charsPerLine;
        this.repaint();
    }

    private boolean isActive(int line) {
        int parent;
        int foldOffset;
        for (parent = line; parent != this.activeFold && parent >= 0 && (foldOffset = this.lines.getFoldOffsets().get(parent)) != 0; parent -= foldOffset) {
        }
        return parent == this.activeFold;
    }

    private class FoldingMouseListener
    extends MouseAdapter {
        private FoldingMouseListener() {
        }

        @Override
        public void mouseExited(MouseEvent e) {
            FoldingSideBar.this.activeFold = -1;
            FoldingSideBar.this.repaint();
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            if (FoldingSideBar.this.lines == null) {
                return;
            }
            int physicalRealLine = this.getLineForEvent(e);
            this.updateActiveFold(physicalRealLine);
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (FoldingSideBar.this.lines == null) {
                return;
            }
            int physicalRealLine = this.getLineForEvent(e);
            this.updateActiveFold(physicalRealLine);
            if (FoldingSideBar.this.activeFold == physicalRealLine) {
                if (FoldingSideBar.this.lines.isVisible(physicalRealLine + 1)) {
                    FoldingSideBar.this.lines.hideFold(physicalRealLine);
                } else {
                    FoldingSideBar.this.lines.showFold(physicalRealLine);
                }
            }
        }

        private void updateActiveFold(int physicalLine) {
            int origActiveFold = FoldingSideBar.this.activeFold;
            if (physicalLine < 0) {
                FoldingSideBar.this.activeFold = -1;
            } else if (physicalLine + 1 < FoldingSideBar.this.lines.getFoldOffsets().size() && FoldingSideBar.this.lines.getFoldOffsets().get(physicalLine + 1) == 1) {
                FoldingSideBar.this.activeFold = physicalLine;
            } else if (physicalLine < FoldingSideBar.this.lines.getFoldOffsets().size()) {
                FoldingSideBar.this.activeFold = physicalLine - FoldingSideBar.this.lines.getFoldOffsets().get(physicalLine);
            } else {
                FoldingSideBar.this.activeFold = -1;
            }
            if (FoldingSideBar.this.activeFold != origActiveFold) {
                FoldingSideBar.this.repaint();
            }
        }

        private int getLineForEvent(MouseEvent e) {
            return FoldingSideBar.this.getLineAtPosition(e.getY());
        }
    }

}

