/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.Pair
 *  org.openide.util.Utilities
 *  org.openide.windows.OutputEvent
 *  org.openide.windows.OutputListener
 */
package org.netbeans.core.output2;

import java.awt.Color;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.AsynchronousCloseException;
import java.nio.channels.ClosedByInterruptException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.core.output2.AbstractLines;
import org.netbeans.core.output2.Controller;
import org.netbeans.core.output2.ErrWriter;
import org.netbeans.core.output2.FileMapStorage;
import org.netbeans.core.output2.HeapStorage;
import org.netbeans.core.output2.LineInfo;
import org.netbeans.core.output2.Lines;
import org.netbeans.core.output2.NbIO;
import org.netbeans.core.output2.NbWriter;
import org.netbeans.core.output2.OutputKind;
import org.netbeans.core.output2.Storage;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.Pair;
import org.openide.util.Utilities;
import org.openide.windows.OutputEvent;
import org.openide.windows.OutputListener;

class OutWriter
extends PrintWriter {
    private static final Logger LOG = Logger.getLogger(OutWriter.class.getName());
    private boolean trouble = false;
    private NbIO owner;
    private boolean disposed = false;
    private boolean disposeOnClose = false;
    private static final boolean USE_HEAP_STORAGE = Boolean.getBoolean("nb.output.heap") || Utilities.getOperatingSystem() == 4 || Utilities.getOperatingSystem() == 2;
    static final String LINE_SEPARATOR = "\n";
    private Storage storage;
    private AbstractLines lines;
    static boolean lowDiskSpace = false;
    private int lineStart;
    private int lineLength;
    private int lineCharLengthWithTabs;
    private int errorCount;
    private boolean closed;
    private static final int WRITE_BUFF_SIZE = 16384;
    private Color ansiColor;
    private Color ansiBackground;
    private int ansiColorCode;
    private int ansiBackgroundCode;
    private boolean ansiBright;
    private boolean ansiFaint;
    private static final Pattern ANSI_CSI = Pattern.compile("\u001b\\[(\\d+(;\\d+)*)?(\\p{Alpha})");
    private static final Color[] COLORS = new Color[]{null, new Color(205, 0, 0), new Color(0, 205, 0), new Color(205, 205, 0), new Color(0, 0, 238), new Color(205, 0, 205), new Color(0, 205, 205), new Color(229, 229, 229), new Color(127, 127, 127), new Color(255, 0, 0), new Color(0, 255, 0), new Color(255, 255, 0), new Color(92, 92, 255), new Color(255, 0, 255), new Color(0, 255, 255), new Color(255, 255, 255)};

    OutWriter(NbIO owner) {
        this();
        this.owner = owner;
    }

    OutWriter() {
        super(new DummyWriter());
        this.lines = new LinesImpl();
        this.errorCount = 0;
        this.closed = false;
        this.ansiBackgroundCode = 9;
        this.lineStart = -1;
        this.lineLength = 0;
        this.lineCharLengthWithTabs = 0;
    }

    Storage getStorage() {
        if (this.disposed) {
            throw new IllegalStateException("Output file has been disposed!");
        }
        if (this.storage == null) {
            this.storage = USE_HEAP_STORAGE || lowDiskSpace ? new HeapStorage() : new FileMapStorage();
        }
        return this.storage;
    }

    boolean hasStorage() {
        return this.storage != null;
    }

    boolean isDisposed() {
        return this.disposed;
    }

    boolean isEmpty() {
        return this.storage == null ? true : this.storage.size() == 0;
    }

    public String toString() {
        return "OutWriter@" + System.identityHashCode(this) + " for " + this.owner + " closed ";
    }

    private void handleException(Exception e) {
        this.setError();
        if (Controller.LOG) {
            StackTraceElement[] el = e.getStackTrace();
            Controller.log("EXCEPTION: " + e.getClass() + e.getMessage());
            for (int i = 1; i < el.length; ++i) {
                Controller.log(el[i].toString());
            }
        }
        if (this.errorCount++ < 3) {
            Exceptions.printStackTrace((Throwable)e);
        }
    }

    private synchronized void write(ByteBuffer bb, int lineCharLengthWithTabs, boolean completeLine) {
        if (this.checkError()) {
            return;
        }
        this.closed = false;
        int start = -1;
        try {
            start = this.getStorage().write(bb);
        }
        catch (ClosedByInterruptException cbiex) {
            this.onWriteException();
        }
        catch (AsynchronousCloseException ace) {
            Exceptions.printStackTrace((Throwable)ace);
            this.onWriteException();
        }
        catch (IOException ioe) {
            if (ioe.getMessage().indexOf("There is not enough space on the disk") != -1) {
                lowDiskSpace = true;
                String msg = NbBundle.getMessage(OutWriter.class, (String)"MSG_DiskSpace", (Object)this.storage);
                Exceptions.attachLocalizedMessage((Throwable)ioe, (String)msg);
                Exceptions.printStackTrace((Throwable)ioe);
                this.setError();
                this.storage.dispose();
            }
            Exceptions.printStackTrace((Throwable)ioe);
            this.onWriteException();
        }
        if (this.checkError()) {
            return;
        }
        int length = bb.limit();
        this.lineLength += length;
        this.lineCharLengthWithTabs += lineCharLengthWithTabs;
        if (start >= 0 && this.lineStart == -1) {
            this.lineStart = start;
        }
        this.lines.lineUpdated(this.lineStart, this.lineLength, this.lineCharLengthWithTabs, completeLine);
        if (completeLine) {
            this.lineStart = -1;
            this.lineLength = 0;
            this.lineCharLengthWithTabs = 0;
        }
        if (this.owner != null && this.owner.isStreamClosed()) {
            this.owner.setStreamClosed(false);
            this.lines.fire();
        }
    }

    void onWriteException() {
        ErrWriter err;
        this.trouble = true;
        if (Controller.LOG) {
            Controller.log(this + " Close due to termination");
        }
        if ((err = this.owner.writer().err()) != null) {
            err.closed = true;
        }
        this.owner.setStreamClosed(true);
        this.close();
    }

    public synchronized void dispose() {
        if (this.disposed) {
            return;
        }
        if (Controller.LOG) {
            Controller.log(this + ": OutWriter.dispose - owner is " + (this.owner == null ? "null" : this.owner.getName()));
        }
        this.clearListeners();
        if (this.storage != null) {
            this.lines.onDispose(this.storage.size());
            this.storage.dispose();
            this.storage = null;
        }
        if (Controller.LOG) {
            Controller.log(this + ": Setting owner to null, trouble to true, dirty to false.  This OutWriter is officially dead.");
        }
        this.owner = null;
        this.disposed = true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void clearListeners() {
        if (Controller.LOG) {
            Controller.log(this + ": Sending outputLineCleared to all listeners");
        }
        if (this.owner == null) {
            return;
        }
        OutWriter outWriter = this;
        synchronized (outWriter) {
            if (this.lines.hasListeners()) {
                int[] listenerLines = this.lines.getLinesWithListeners();
                Controller.ControllerOutputEvent e = new Controller.ControllerOutputEvent(this.owner, 0);
                for (int i = 0; i < listenerLines.length; ++i) {
                    Collection<OutputListener> ols = this.lines.getListenersForLine(listenerLines[i]);
                    e.setLine(listenerLines[i]);
                    for (OutputListener ol : ols) {
                        ol.outputLineCleared((OutputEvent)e);
                    }
                }
            } else if (Controller.LOG) {
                Controller.log(this + ": No listeners to clear");
            }
        }
    }

    public synchronized boolean isClosed() {
        if (this.checkError() || this.disposed || this.storage != null && this.storage.isClosed()) {
            return true;
        }
        return this.closed;
    }

    public Lines getLines() {
        return this.lines;
    }

    @Override
    public synchronized void close() {
        this.closed = true;
        try {
            if (this.storage != null) {
                this.storage.close();
            }
            this.lines.fire();
            if (this.isDisposeOnClose() && !this.isDisposed()) {
                this.dispose();
            }
        }
        catch (IOException ioe) {
            this.onWriteException();
        }
    }

    boolean isDisposeOnClose() {
        return this.disposeOnClose;
    }

    void setDisposeOnClose(boolean disposeOnClose) {
        this.disposeOnClose = disposeOnClose;
    }

    @Override
    public synchronized void println(String s) {
        this.doWrite(s, 0, s.length());
        this.println();
    }

    @Override
    public synchronized void flush() {
        if (this.checkError()) {
            return;
        }
        try {
            this.getStorage().flush();
            this.lines.fire();
        }
        catch (IOException e) {
            this.onWriteException();
        }
    }

    @Override
    public boolean checkError() {
        return this.disposed || this.trouble;
    }

    @Override
    public synchronized void write(int c) {
        this.doWrite(new String(new char[]{(char)c}), 0, 1);
        this.checkLimits();
    }

    private void checkLimits() {
        int shift = this.lines.checkLimits();
        if (this.lineStart > 0) {
            this.lineStart -= shift;
        }
    }

    @Override
    public synchronized void write(char[] data, int off, int len) {
        this.doWrite(new CharArrayWrapper(data), off, len);
        this.checkLimits();
    }

    private synchronized void doWrite(CharSequence s, int off, int len) {
        if (this.checkError() || len == 0) {
            return;
        }
        if (this.printANSI(s.subSequence(off, off + len), false, OutputKind.OUT, false)) {
            return;
        }
        int lineCLVT = 0;
        try {
            boolean written = false;
            int lastChar = 0;
            ByteBuffer byteBuff = this.getStorage().getWriteBuffer(32768);
            CharBuffer charBuff = byteBuff.asCharBuffer();
            int charOffset = AbstractLines.toCharIndex(this.getStorage().size());
            int skipped = 0;
            int tabLength = 0;
            for (int i = off; i < off + len; ++i) {
                if (charBuff.position() + 1 >= 16384) {
                    this.write((ByteBuffer)byteBuff.position(charBuff.position() * 2), lineCLVT, false);
                    written = true;
                }
                if (written) {
                    byteBuff = this.getStorage().getWriteBuffer(32768);
                    charBuff = byteBuff.asCharBuffer();
                    lineCLVT = 0;
                    written = false;
                }
                char c = s.charAt(i);
                if (lastChar == 13 && c != '\n') {
                    char charP1;
                    int p;
                    for (p = charBuff.position(); p > 0 && (charP1 = charBuff.get(p - 1)) != '\n'; --p) {
                        if (charP1 == '\t') {
                            lineCLVT -= this.lines.removeLastTab();
                        }
                        ++skipped;
                        charBuff.position(p - 1);
                    }
                    if (p == 0) {
                        this.clearLine();
                    }
                }
                if (c == '\t') {
                    charBuff.put(c);
                    tabLength = 8 - (this.lineCharLengthWithTabs + lineCLVT) % 8;
                    LOG.log(Level.FINEST, "Going to add tab: charOffset = {0}, i = {1}, off = {2},tabLength = {3}, tabIndex = {4}, skipped = {5}", new Object[]{charOffset, i, off, tabLength, charOffset + (i - off), skipped});
                    this.lines.addTabAt(charOffset + (i - off) - skipped, tabLength);
                    lineCLVT += tabLength;
                } else if (c == '\b') {
                    int skip = this.handleBackspace(charBuff);
                    if (skip == -2) {
                        lineCLVT -= this.lines.removeLastTab();
                    } else if (skip == 2) {
                        --lineCLVT;
                    } else if (skip == 1) {
                        Pair<Integer, Integer> removedInfo = this.lines.removeCharsFromLastLine(1);
                        this.storage.removeBytesFromEnd((Integer)removedInfo.first() * 2);
                        this.lineLength -= (Integer)removedInfo.first() * 2;
                        this.lineCharLengthWithTabs -= ((Integer)removedInfo.second()).intValue();
                    }
                    skipped += Math.abs(skip);
                } else if (c == '\n') {
                    charBuff.put('\n');
                    int pos = charBuff.position() * 2;
                    ByteBuffer bf = (ByteBuffer)byteBuff.position(pos);
                    this.write(bf, lineCLVT, true);
                    written = true;
                } else if (c != '\r') {
                    charBuff.put(c);
                    ++lineCLVT;
                } else {
                    assert (c == '\r');
                    ++skipped;
                }
                lastChar = c;
            }
            if (!written) {
                this.write((ByteBuffer)byteBuff.position(charBuff.position() * 2), lineCLVT, false);
            }
        }
        catch (IOException ioe) {
            this.onWriteException();
        }
        this.lines.delayedFire();
    }

    private int handleBackspace(CharBuffer charBuff) {
        if (charBuff.position() > 0) {
            char deletedChar = charBuff.get(charBuff.position() - 1);
            charBuff.position(charBuff.position() - 1);
            return deletedChar == '\t' ? -2 : 2;
        }
        return 1;
    }

    @Override
    public synchronized void write(char[] data) {
        this.doWrite(new CharArrayWrapper(data), 0, data.length);
        this.checkLimits();
    }

    @Override
    public synchronized void println() {
        this.printLineEnd();
        this.checkLimits();
    }

    private void printLineEnd() {
        this.doWrite("\n", 0, 1);
    }

    @Override
    public synchronized void write(String s, int off, int len) {
        this.doWrite(s, off, len);
        this.checkLimits();
    }

    @Override
    public synchronized void write(String s) {
        this.doWrite(s, 0, s.length());
        this.checkLimits();
    }

    public synchronized void println(String s, OutputListener l) {
        this.println(s, l, false);
    }

    public synchronized void println(String s, OutputListener l, boolean important) {
        this.print(s, l, important, null, null, OutputKind.OUT, true);
    }

    synchronized void print(CharSequence s, OutputListener l, boolean important, Color c, Color b, OutputKind outKind, boolean addLS) {
        if (c == null) {
            if (l == null && this.printANSI(s, important, outKind, addLS)) {
                return;
            }
            c = this.ansiColor;
        }
        int lastLine = this.lines.getLineCount() - 1;
        int lastPos = this.lines.getCharCount();
        this.doWrite(s, 0, s.length());
        if (addLS) {
            this.printLineEnd();
        }
        this.lines.updateLinesInfo(s, lastLine, lastPos, l, important, outKind, c, b);
        this.checkLimits();
    }

    private boolean printANSI(CharSequence s, boolean important, OutputKind outKind, boolean addLS) {
        int len = s.length();
        boolean hasEscape = false;
        for (int i = 0; i < len - 1; ++i) {
            if (s.charAt(i) != '\u001b' || s.charAt(i + 1) != '[') continue;
            hasEscape = true;
            break;
        }
        if (!hasEscape) {
            return false;
        }
        Matcher m = ANSI_CSI.matcher(s);
        int text = 0;
        while (m.find()) {
            int esc = m.start();
            if (esc > text) {
                this.print(s.subSequence(text, esc), null, important, this.ansiColor, this.ansiBackground, outKind, false);
            }
            text = m.end();
            if ("K".equals(m.group(3)) && "2".equals(m.group(1))) {
                this.clearLine();
                continue;
            }
            if (!"m".equals(m.group(3))) continue;
            String paramsS = m.group(1);
            if (Controller.VERBOSE) {
                Controller.log("ANSI CSI+SGR: " + paramsS);
            }
            if (paramsS == null) {
                this.ansiColorCode = 0;
                this.ansiBackgroundCode = 9;
                this.ansiBright = false;
                this.ansiFaint = false;
            } else {
                for (String param : paramsS.split(";")) {
                    int code = Integer.parseInt(param);
                    if (code == 0) {
                        this.ansiColorCode = 0;
                        this.ansiBackgroundCode = 9;
                        this.ansiBright = false;
                        this.ansiFaint = false;
                        continue;
                    }
                    if (code == 1) {
                        this.ansiBright = true;
                        this.ansiFaint = false;
                        continue;
                    }
                    if (code == 2) {
                        this.ansiBright = false;
                        this.ansiFaint = true;
                        continue;
                    }
                    if (code == 21) {
                        this.ansiBright = false;
                        continue;
                    }
                    if (code == 22) {
                        this.ansiBright = false;
                        this.ansiFaint = false;
                        continue;
                    }
                    if (code >= 30 && code <= 37) {
                        this.ansiColorCode = code - 30;
                        continue;
                    }
                    if (code == 39) {
                        this.ansiColorCode = 0;
                        continue;
                    }
                    if (code >= 40 && code <= 47) {
                        this.ansiBackgroundCode = code - 40;
                        continue;
                    }
                    if (code != 49) continue;
                    this.ansiBackgroundCode = 9;
                }
            }
            assert (this.ansiColorCode >= 0 && this.ansiColorCode <= 7);
            assert (this.ansiBackgroundCode >= 0 && this.ansiBackgroundCode <= 9);
            assert (!this.ansiBright || !this.ansiFaint);
            Color setColor = COLORS[this.ansiColorCode + (this.ansiBright ? 8 : 0)];
            this.ansiBackground = this.ansiBackgroundCode == 9 ? null : COLORS[this.ansiBackgroundCode];
            this.ansiColor = OutWriter.fixTextColor(setColor, this.ansiBackground);
            if (!this.ansiFaint || this.ansiColor == null) continue;
            this.ansiColor = this.ansiColor.darker();
        }
        if (text == 0) {
            return false;
        }
        if (text < len) {
            this.print(s.subSequence(text, len), null, important, this.ansiColor, this.ansiBackground, outKind, addLS);
        } else if (addLS) {
            this.printLineEnd();
        }
        return true;
    }

    private void clearLine() {
        Pair<Integer, Integer> r = this.lines.removeCharsFromLastLine(-1);
        try {
            this.getStorage().removeBytesFromEnd((Integer)r.first() * 2);
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        this.lineLength -= (Integer)r.first() * 2;
        this.lineCharLengthWithTabs -= ((Integer)r.second()).intValue();
    }

    synchronized void print(CharSequence s, LineInfo info, boolean important) {
        int line = this.lines.getLineCount() - 1;
        this.doWrite(s, 0, s.length());
        if (info != null) {
            this.lines.addLineInfo(line, info, important);
        }
        this.checkLimits();
    }

    private static Color fixTextColor(Color textColor, Color background) {
        if (background == null && textColor != null && textColor.equals(Color.WHITE)) {
            return Color.BLACK;
        }
        if (textColor != null && background != null && OutWriter.colorDiff(textColor, background) < 10) {
            if (OutWriter.colorDiff(textColor, Color.WHITE) > OutWriter.colorDiff(textColor, Color.BLACK)) {
                return Color.WHITE;
            }
            return Color.BLACK;
        }
        return textColor;
    }

    private static int colorDiff(Color c1, Color c2) {
        int redDiff = Math.abs(c1.getRed() - c2.getRed());
        int greenDiff = Math.abs(c1.getGreen() - c2.getGreen());
        int blueDiff = Math.abs(c1.getBlue() - c2.getBlue());
        return Math.max(redDiff, Math.max(greenDiff, blueDiff));
    }

    private class LinesImpl
    extends AbstractLines {
        LinesImpl() {
        }

        @Override
        protected Storage getStorage() {
            return OutWriter.this.getStorage();
        }

        @Override
        protected boolean isDisposed() {
            return OutWriter.this.disposed;
        }

        @Override
        public Object readLock() {
            return OutWriter.this;
        }

        @Override
        public boolean isGrowing() {
            return !OutWriter.this.isClosed();
        }

        @Override
        protected void handleException(Exception e) {
            OutWriter.this.handleException(e);
        }
    }

    static class DummyWriter
    extends Writer {
        DummyWriter() {
            super(new Object());
        }

        @Override
        public void close() throws IOException {
        }

        @Override
        public void flush() throws IOException {
        }

        @Override
        public void write(char[] cbuf, int off, int len) throws IOException {
        }
    }

    static class CharArrayWrapper
    implements CharSequence {
        private char[] arr;
        private int off;
        private int len;

        public CharArrayWrapper(char[] arr) {
            this(arr, 0, arr.length);
        }

        public CharArrayWrapper(char[] arr, int off, int len) {
            this.arr = arr;
            this.off = off;
            this.len = len;
        }

        @Override
        public char charAt(int index) {
            return this.arr[this.off + index];
        }

        @Override
        public int length() {
            return this.len;
        }

        @Override
        public CharSequence subSequence(int start, int end) {
            return new CharArrayWrapper(this.arr, this.off + start, end - start);
        }

        @Override
        public String toString() {
            return new String(this.arr, this.off, this.len);
        }
    }

}

