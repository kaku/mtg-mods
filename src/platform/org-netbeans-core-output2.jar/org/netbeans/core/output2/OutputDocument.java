/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package org.netbeans.core.output2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.Segment;
import javax.swing.text.SimpleAttributeSet;
import org.netbeans.core.output2.AbstractLines;
import org.netbeans.core.output2.Controller;
import org.netbeans.core.output2.FoldingSideBar;
import org.netbeans.core.output2.Lines;
import org.netbeans.core.output2.OutWriter;
import org.netbeans.core.output2.ui.AbstractOutputPane;
import org.openide.util.Exceptions;

public class OutputDocument
implements Document,
Element,
ChangeListener {
    private static final Logger LOG = Logger.getLogger(OutputDocument.class.getName());
    private List<DocumentListener> dlisteners = new ArrayList<DocumentListener>();
    private volatile Timer timer = null;
    private OutWriter writer;
    private StringBuffer inBuffer;
    private AbstractOutputPane pane;
    private int lastInputOff = -1;
    private char[] reusableSubrange = new char[256];
    private volatile DO lastEvent = null;
    private int lastFiredLineCount = 0;
    private int lastFiredLength = 0;
    private int lastVisibleLineCount = 0;

    OutputDocument(OutWriter writer) {
        if (Controller.LOG) {
            Controller.log("Creating a Document for " + writer);
        }
        this.writer = writer;
        this.getLines().addChangeListener(this);
        this.inBuffer = new StringBuffer();
    }

    OutWriter getWriter() {
        return this.writer;
    }

    public int getOutputLength() {
        return this.getLines().getCharCount();
    }

    public void setPane(AbstractOutputPane pane) {
        this.pane = pane;
    }

    public void dispose() {
        if (Controller.LOG) {
            Controller.log("Disposing document and backing storage for " + this.getLines().readLock());
        }
        this.disposeQuietly();
        this.writer.dispose();
        this.writer = null;
    }

    public void disposeQuietly() {
        if (this.timer != null) {
            this.timer.stop();
            this.timer = null;
        }
        this.dlisteners.clear();
        this.lastEvent = null;
        this.getLines().removeChangeListener(this);
    }

    @Override
    public synchronized void addDocumentListener(DocumentListener documentListener) {
        this.dlisteners.add(documentListener);
        this.lastEvent = null;
    }

    @Override
    public void addUndoableEditListener(UndoableEditListener l) {
    }

    @Override
    public Position createPosition(int offset) throws BadLocationException {
        if (offset < 0 || offset > this.getLines().getCharCount() + this.inBuffer.length()) {
            throw new BadLocationException("Bad position", offset);
        }
        return new ODPosition(offset);
    }

    @Override
    public Element getDefaultRootElement() {
        return this;
    }

    @Override
    public Position getEndPosition() {
        return new ODEndPosition();
    }

    @Override
    public int getLength() {
        return this.getLines().getCharCount() + this.inBuffer.length();
    }

    @Override
    public Object getProperty(Object obj) {
        return null;
    }

    @Override
    public Element[] getRootElements() {
        return new Element[]{this};
    }

    @Override
    public Position getStartPosition() {
        return new ODStartPosition();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public String getText(int offset, int length) throws BadLocationException {
        String result;
        if (length == 0) {
            return "";
        }
        Object object = this.getLines().readLock();
        synchronized (object) {
            if (offset < 0 || offset + length > this.getLines().getCharCount() + this.inBuffer.length() || length < 0) {
                throw new BadLocationException("Bad: " + offset + "," + length + " (" + this.getLines().getCharCount() + ", " + this.inBuffer.length() + ")", offset);
            }
            int linesOffset = Math.min(this.getLines().getCharCount(), offset);
            int linesEnd = Math.min(this.getLines().getCharCount(), offset + length);
            result = this.getLines().getText(linesOffset, linesEnd);
            if (offset + length > this.getLines().getCharCount()) {
                int inEnd = offset + length - this.getLines().getCharCount();
                result = result + this.inBuffer.substring(0, inEnd);
            }
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void getText(int offset, int length, Segment txt) throws BadLocationException {
        if (length < 0) {
            txt.array = new char[0];
            txt.offset = 0;
            txt.count = 0;
            return;
        }
        if (offset < 0) {
            throw new BadLocationException("Negative offset", offset);
        }
        if (this.getLines().getLineCount() == -1) {
            txt.array = new char[]{'\n'};
            txt.offset = 0;
            txt.count = 1;
            return;
        }
        if (length > this.reusableSubrange.length) {
            this.reusableSubrange = new char[length];
        }
        try {
            Object object = this.getLines().readLock();
            synchronized (object) {
                int charCount = this.getLines().getCharCount();
                if (charCount < 0) {
                    txt.array = new char[0];
                    txt.offset = 0;
                    txt.count = 0;
                    return;
                }
                int linesOffset = Math.min(charCount, offset);
                int linesEnd = Math.min(charCount, offset + length);
                char[] chars = this.getLines().getText(linesOffset, linesEnd, this.reusableSubrange);
                if (offset + length > charCount) {
                    int inEnd = offset - charCount + length;
                    int inStart = Math.max(0, offset - charCount);
                    this.inBuffer.getChars(Math.min(inStart, this.inBuffer.length()), Math.min(inEnd, this.inBuffer.length()), chars, linesEnd - linesOffset);
                }
                txt.array = chars;
                txt.offset = 0;
                txt.count = Math.min(length, chars.length);
            }
        }
        catch (OutOfMemoryError error) {
            OutWriter.lowDiskSpace = true;
            this.writer.dispose();
            Logger.getAnonymousLogger().log(Level.WARNING, "OOME while reading output.  Cleaning up.", error);
            System.gc();
        }
    }

    @Override
    public void insertString(int offset, String str, AttributeSet attributeSet) throws BadLocationException {
        int off;
        int docLen = this.getLength();
        int inputOff = docLen - this.inBuffer.length();
        if (inputOff != this.lastInputOff) {
            this.lastInputOff = inputOff;
            off = docLen;
        } else {
            off = offset < inputOff ? inputOff : offset;
        }
        this.inBuffer.insert(off - inputOff, str);
        final int len = str.length();
        DocumentEvent ev = new DocumentEvent(){

            @Override
            public int getOffset() {
                return off;
            }

            @Override
            public int getLength() {
                return len;
            }

            @Override
            public Document getDocument() {
                return OutputDocument.this;
            }

            @Override
            public DocumentEvent.EventType getType() {
                return DocumentEvent.EventType.INSERT;
            }

            @Override
            public DocumentEvent.ElementChange getChange(Element arg0) {
                return null;
            }
        };
        if (this.getLines() instanceof AbstractLines) {
            AbstractLines lines = (AbstractLines)this.getLines();
            int start = lines.getLineStart(lines.getLineCount() - 1);
            int length = this.getLength() - start;
            lines.lineUpdated(2 * start, 2 * length, length, false);
        }
        this.fireDocumentEvent(ev);
    }

    public String sendLine() {
        final int off = this.getLength() - this.inBuffer.length();
        final int len = this.inBuffer.length();
        String toReturn = this.inBuffer.toString();
        this.inBuffer = new StringBuffer();
        DocumentEvent ev = new DocumentEvent(){

            @Override
            public int getOffset() {
                return off;
            }

            @Override
            public int getLength() {
                return len;
            }

            @Override
            public Document getDocument() {
                return OutputDocument.this;
            }

            @Override
            public DocumentEvent.EventType getType() {
                return DocumentEvent.EventType.REMOVE;
            }

            @Override
            public DocumentEvent.ElementChange getChange(Element arg0) {
                return null;
            }
        };
        if (this.getLines() instanceof AbstractLines) {
            AbstractLines lines = (AbstractLines)this.getLines();
            int start = lines.getLineStart(lines.getLineCount() - 1);
            lines.lineUpdated(2 * start, 0, 0, false);
        }
        this.fireDocumentEvent(ev);
        return toReturn;
    }

    @Override
    public void putProperty(Object obj, Object obj1) {
    }

    @Override
    public void remove(int offset, int length) throws BadLocationException {
        int startOff = this.getLength() - this.inBuffer.length();
        final int off = Math.max(startOff, offset);
        final int len = Math.min(length, this.inBuffer.length());
        if (startOff != this.lastInputOff) {
            this.lastInputOff = startOff;
            this.inBuffer.delete(this.inBuffer.length() - len, this.inBuffer.length());
        } else if (off - startOff + len <= this.getLength()) {
            this.inBuffer.delete(off - startOff, off - startOff + len);
            DocumentEvent ev = new DocumentEvent(){

                @Override
                public int getOffset() {
                    return off;
                }

                @Override
                public int getLength() {
                    return len;
                }

                @Override
                public Document getDocument() {
                    return OutputDocument.this;
                }

                @Override
                public DocumentEvent.EventType getType() {
                    return DocumentEvent.EventType.REMOVE;
                }

                @Override
                public DocumentEvent.ElementChange getChange(Element arg0) {
                    return null;
                }
            };
            if (this.getLines() instanceof AbstractLines) {
                AbstractLines lines = (AbstractLines)this.getLines();
                int start = lines.getLineStart(lines.getLineCount() - 1);
                int l = this.getLength() - start;
                lines.lineUpdated(2 * start, 2 * l, l, false);
            }
            this.fireDocumentEvent(ev);
        }
    }

    @Override
    public synchronized void removeDocumentListener(DocumentListener documentListener) {
        this.dlisteners.remove(documentListener);
        this.lastEvent = null;
        if (this.dlisteners.isEmpty() && this.timer != null) {
            this.timer.stop();
            this.timer = null;
        }
    }

    public Lines getLines() {
        return this.writer != null ? this.writer.getLines() : null;
    }

    public int getLineStart(int line) {
        return this.getLines().getLineCount() > 0 ? this.getLines().getLineStart(line) : 0;
    }

    public int getLineEnd(int lineIndex) {
        if (this.getLines().getLineCount() == 0) {
            return 0;
        }
        int endOffset = lineIndex >= this.getLines().getLineCount() - 1 ? this.getLines().getCharCount() + this.inBuffer.length() : this.getLines().getLineStart(lineIndex + 1) - 1;
        return endOffset;
    }

    @Override
    public void removeUndoableEditListener(UndoableEditListener undoableEditListener) {
    }

    @Override
    public void render(Runnable runnable) {
        this.getElementCount();
        runnable.run();
    }

    @Override
    public AttributeSet getAttributes() {
        return SimpleAttributeSet.EMPTY;
    }

    @Override
    public Document getDocument() {
        return this;
    }

    @Override
    public Element getElement(int index) {
        int realIndex = this.getLines().visibleToRealLine(index);
        return new ODElement(realIndex);
    }

    @Override
    public int getElementCount() {
        return Math.max(1, this.getLines().getVisibleLineCount());
    }

    @Override
    public int getElementIndex(int offset) {
        int realLine = this.getLines().getLineAt(offset);
        if (realLine < 0) {
            return realLine;
        }
        return this.getLines().realToVisibleLine(realLine);
    }

    @Override
    public int getEndOffset() {
        return this.getLength() + 1;
    }

    @Override
    public String getName() {
        return "foo";
    }

    @Override
    public Element getParentElement() {
        return null;
    }

    @Override
    public int getStartOffset() {
        return 0;
    }

    @Override
    public boolean isLeaf() {
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void stateChanged(ChangeEvent changeEvent) {
        assert (SwingUtilities.isEventDispatchThread());
        if (Controller.VERBOSE) {
            Controller.log(changeEvent != null ? "Document got change event from writer" : "Document timer polling");
        }
        if (this.dlisteners.isEmpty()) {
            if (Controller.VERBOSE) {
                Controller.log("listeners empty, not firing");
            }
            return;
        }
        Lines lines = this.getLines();
        if (lines.checkDirty(true)) {
            boolean lastLineChanged;
            int size;
            int lineCount;
            int visibleLineCount;
            if (this.lastEvent != null && !this.lastEvent.isConsumed()) {
                if (Controller.VERBOSE) {
                    Controller.log("Last event not consumed, not firing");
                }
                return;
            }
            Object object = lines.readLock();
            synchronized (object) {
                int lastFiredLineEnd;
                lineCount = lines.getLineCount();
                visibleLineCount = lines.getVisibleLineCount();
                size = lines.getCharCount() + this.inBuffer.length();
                if (size == this.lastFiredLength && visibleLineCount == this.lastVisibleLineCount) {
                    if (Controller.VERBOSE) {
                        Controller.log("Size is same " + size + " - not firing");
                    }
                    return;
                }
                boolean bl = lastLineChanged = this.lastFiredLineCount == lineCount;
                if (this.lastFiredLineCount > 0 && lineCount > this.lastFiredLineCount && (lastFiredLineEnd = lines.getLineStart(this.lastFiredLineCount)) > this.lastFiredLength) {
                    lastLineChanged = true;
                }
            }
            this.lastEvent = this.lastFiredLineCount == lineCount && this.lastVisibleLineCount != visibleLineCount ? new DO(0) : new DO(lastLineChanged ? this.lastFiredLineCount - 1 : this.lastFiredLineCount);
            this.lastFiredLineCount = lineCount;
            this.lastVisibleLineCount = visibleLineCount;
            this.lastFiredLength = size;
            if (Controller.VERBOSE) {
                Controller.log("Firing document event on EQ with start index " + this.lastEvent.first);
            }
            this.fireDocumentEvent(this.lastEvent);
            if (this.pane != null) {
                this.pane.getFoldingSideBar().repaint();
            }
        } else if (Controller.VERBOSE) {
            Controller.log("Writer says it is not dirty, firing no change");
        }
    }

    private void fireDocumentEvent(DocumentEvent de) {
        for (DocumentListener dl : new ArrayList<DocumentListener>(this.dlisteners)) {
            if (!(de instanceof DO) && this.pane != null) {
                this.pane.doUpdateCaret();
            }
            if (de.getType() == DocumentEvent.EventType.REMOVE) {
                dl.removeUpdate(de);
            } else if (de.getType() == DocumentEvent.EventType.CHANGE) {
                dl.changedUpdate(de);
            } else {
                dl.insertUpdate(de);
            }
            if (de instanceof DO || this.pane == null) continue;
            this.pane.dontUpdateCaret();
        }
    }

    public String toString() {
        return "OD@" + System.identityHashCode(this) + " for " + this.getLines().readLock();
    }

    static /* synthetic */ OutWriter access$200(OutputDocument x0) {
        return x0.writer;
    }

    public class DO
    implements DocumentEvent,
    DocumentEvent.ElementChange {
        private int offset;
        private int length;
        private int lineCount;
        private boolean consumed;
        private int first;

        DO(int start) {
            this.offset = -1;
            this.length = -1;
            this.lineCount = -1;
            this.consumed = false;
            this.first = -1;
            this.first = start;
            if (start < 0) {
                throw new IllegalArgumentException("Illogical start: " + start);
            }
        }

        private void calc() {
            assert (SwingUtilities.isEventDispatchThread());
            if (!this.consumed) {
                this.consumed = true;
                OutputDocument.this.lastFiredLineCount = OutputDocument.this.getLines().getLineCount();
                OutputDocument.this.lastFiredLength = OutputDocument.this.getLines().getCharCount() + OutputDocument.this.inBuffer.length();
                if (this.first < OutputDocument.this.lastFiredLineCount) {
                    this.offset = OutputDocument.this.getLines().getLineStart(this.first);
                    this.lineCount = OutputDocument.this.lastFiredLineCount - this.first;
                    this.length = OutputDocument.this.lastFiredLength - this.offset;
                } else {
                    this.lineCount = 0;
                    this.length = 0;
                    this.offset = 0;
                }
            }
        }

        public boolean isConsumed() {
            return this.consumed;
        }

        public String toString() {
            boolean wasConsumed = this.isConsumed();
            this.calc();
            return "Event: first=" + this.first + " linecount=" + this.lineCount + " offset=" + this.offset + " length=" + this.length + " consumed=" + wasConsumed;
        }

        @Override
        public DocumentEvent.ElementChange getChange(Element element) {
            if (element == OutputDocument.this) {
                return this;
            }
            return null;
        }

        @Override
        public Document getDocument() {
            return OutputDocument.this;
        }

        @Override
        public int getLength() {
            this.calc();
            return this.length;
        }

        @Override
        public int getOffset() {
            this.calc();
            return this.offset;
        }

        @Override
        public DocumentEvent.EventType getType() {
            return this.first == 0 ? DocumentEvent.EventType.CHANGE : DocumentEvent.EventType.INSERT;
        }

        @Override
        public Element[] getChildrenAdded() {
            this.calc();
            if (this.first + this.lineCount > OutputDocument.this.getLines().getLineCount()) {
                LOG.log(Level.INFO, "Document line count: {0}, OD line count: {1}", new Object[]{OutputDocument.this.getLines().getLineCount(), this.first + this.lineCount});
                return new Element[0];
            }
            Element[] e = new Element[this.lineCount];
            for (int i = 0; i < this.lineCount; ++i) {
                e[i] = new ODElement(this.first + i);
            }
            return e;
        }

        @Override
        public Element[] getChildrenRemoved() {
            return new Element[0];
        }

        @Override
        public Element getElement() {
            return OutputDocument.this;
        }

        @Override
        public int getIndex() {
            this.calc();
            return this.first;
        }
    }

    final class ODElement
    implements Element {
        private int lineIndex;
        private int startOffset;
        private int endOffset;

        ODElement(int lineIndex) {
            this.startOffset = -1;
            this.endOffset = -1;
            this.lineIndex = lineIndex;
        }

        public int hashCode() {
            return this.lineIndex;
        }

        public boolean equals(Object o) {
            return o instanceof ODElement && ((ODElement)o).lineIndex == this.lineIndex && ((ODElement)o).getDocument() == this.getDocument();
        }

        @Override
        public AttributeSet getAttributes() {
            return SimpleAttributeSet.EMPTY;
        }

        @Override
        public Document getDocument() {
            return OutputDocument.this;
        }

        @Override
        public Element getElement(int param) {
            return null;
        }

        @Override
        public int getElementCount() {
            return 0;
        }

        @Override
        public int getElementIndex(int param) {
            return -1;
        }

        @Override
        public int getEndOffset() {
            this.calc();
            return this.endOffset;
        }

        @Override
        public String getName() {
            return null;
        }

        @Override
        public Element getParentElement() {
            return OutputDocument.this;
        }

        @Override
        public int getStartOffset() {
            this.calc();
            return this.startOffset;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void calc() {
            Object object = OutputDocument.this.getLines().readLock();
            synchronized (object) {
                if (this.lineIndex < 0) {
                    return;
                }
                if (this.startOffset == -1) {
                    if (this.lineIndex >= OutputDocument.this.getLines().getLineCount()) {
                        this.startOffset = 0;
                        this.endOffset = 0;
                        return;
                    }
                    this.startOffset = OutputDocument.this.getLines().getLineStart(this.lineIndex);
                    this.endOffset = this.lineIndex >= OutputDocument.this.getLines().getLineCount() - 1 ? OutputDocument.this.getLines().getCharCount() + OutputDocument.this.inBuffer.length() + 1 : OutputDocument.this.getLines().getLineStart(this.lineIndex + 1);
                    assert (this.endOffset >= this.startOffset);
                } else if (this.lineIndex >= OutputDocument.this.getLines().getLineCount() - 1) {
                    this.endOffset = OutputDocument.this.getLines().getCharCount() + OutputDocument.this.inBuffer.length() + 1;
                }
            }
        }

        @Override
        public boolean isLeaf() {
            return true;
        }

        public String toString() {
            try {
                return OutputDocument.this.getText(this.getStartOffset(), this.getEndOffset() - this.getStartOffset());
            }
            catch (BadLocationException ble) {
                Exceptions.printStackTrace((Throwable)ble);
                return "";
            }
        }
    }

    final class ODStartPosition
    implements Position {
        ODStartPosition() {
        }

        @Override
        public int getOffset() {
            return 0;
        }

        private Document doc() {
            return OutputDocument.this;
        }

        public boolean equals(Object o) {
            return o instanceof ODStartPosition && ((ODStartPosition)o).doc() == this.doc();
        }

        public int hashCode() {
            return 2190481;
        }
    }

    final class ODEndPosition
    implements Position {
        ODEndPosition() {
        }

        @Override
        public int getOffset() {
            return OutputDocument.this.getLines().getCharCount() + OutputDocument.this.inBuffer.length();
        }

        private Document doc() {
            return OutputDocument.this;
        }

        public boolean equals(Object o) {
            return o instanceof ODEndPosition && ((ODEndPosition)o).doc() == this.doc();
        }

        public int hashCode() {
            return -2390481;
        }
    }

    static final class ODPosition
    implements Position {
        private int offset;

        ODPosition(int offset) {
            this.offset = offset;
        }

        @Override
        public int getOffset() {
            return this.offset;
        }

        public int hashCode() {
            return this.offset * 11;
        }

        public boolean equals(Object o) {
            return o instanceof ODPosition && ((ODPosition)o).getOffset() == this.offset;
        }
    }

}

