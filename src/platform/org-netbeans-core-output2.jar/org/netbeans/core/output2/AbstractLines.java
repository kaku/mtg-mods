/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.Mutex
 *  org.openide.util.Pair
 *  org.openide.windows.IOColors
 *  org.openide.windows.IOColors$OutputType
 *  org.openide.windows.OutputListener
 */
package org.netbeans.core.output2;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.core.output2.BufferResource;
import org.netbeans.core.output2.Controller;
import org.netbeans.core.output2.IntList;
import org.netbeans.core.output2.IntListSimple;
import org.netbeans.core.output2.IntMap;
import org.netbeans.core.output2.LineInfo;
import org.netbeans.core.output2.Lines;
import org.netbeans.core.output2.OutputKind;
import org.netbeans.core.output2.OutputLimits;
import org.netbeans.core.output2.SparseIntList;
import org.netbeans.core.output2.Storage;
import org.netbeans.core.output2.options.OutputOptions;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Mutex;
import org.openide.util.Pair;
import org.openide.windows.IOColors;
import org.openide.windows.OutputListener;

abstract class AbstractLines
implements Lines,
Runnable,
ActionListener {
    private static final Logger LOG = Logger.getLogger(AbstractLines.class.getName());
    private OutputLimits outputLimits = OutputLimits.getDefault();
    IntList lineStartList;
    IntListSimple lineCharLengthListWithTabs;
    IntMap lineWithListenerToInfo;
    IntMap linesToInfos;
    private int longestLineLen = 0;
    private int knownCharsPerLine = -1;
    private SparseIntList knownLogicalLineCounts = null;
    private IntList tabCharOffsets = new IntList(128);
    private IntListSimple tabLengthSums = new IntListSimple(128);
    private final IntListSimple foldOffsets = new IntListSimple(16);
    private final IntListSimple visibleList = new IntListSimple(128);
    private final IntListSimple visibleToRealLine = new IntListSimple(128);
    private final IntListSimple realToVisibleLine = new IntListSimple(128);
    private int hiddenLines = 0;
    private int currentFoldStart = -1;
    private int lastStorageSize = -1;
    private ChangeListener listener = null;
    private Timer timer = null;
    private final AtomicBoolean newEvent = new AtomicBoolean(false);
    private boolean dirty;
    private IntList importantLines = new IntList(16);
    private boolean lastLineFinished = true;
    private int lastLineLength = -1;
    private int lastCharLengthWithTabs = -1;
    private static Color[] DEF_COLORS = null;
    Color[] curDefColors;
    private static final int MAX_FIND_SIZE = 16384;
    private Pattern pattern;

    AbstractLines() {
        if (Controller.LOG) {
            Controller.log("Creating a new AbstractLines");
        }
        this.init();
    }

    protected abstract Storage getStorage();

    protected abstract boolean isDisposed();

    protected abstract void handleException(Exception var1);

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public char[] getText(int start, int end, char[] chars) {
        if (chars == null) {
            chars = new char[end - start];
        }
        if (end < start || start < 0) {
            throw new IllegalArgumentException("Illogical text range from " + start + " to " + end);
        }
        if (end - start > chars.length) {
            throw new IllegalArgumentException("Array size is too small");
        }
        Object object = this.readLock();
        synchronized (object) {
            char[] arrc;
            block15 : {
                if (this.isDisposed()) {
                    for (int i = 0; i < end - start; ++i) {
                        chars[i] = '\u0000';
                    }
                    return chars;
                }
                int fileStart = AbstractLines.toByteIndex(start);
                int byteCount = AbstractLines.toByteIndex(end - start);
                BufferResource<ByteBuffer> br = null;
                try {
                    br = this.getStorage().getReadBuffer(fileStart, byteCount);
                    CharBuffer chb = br.getBuffer().asCharBuffer();
                    int len = Math.min(end - start, chb.remaining());
                    chb.get(chars, 0, len);
                    arrc = chars;
                    if (br == null) break block15;
                    br.releaseBuffer();
                }
                catch (Exception e) {
                    char[] len;
                    block16 : {
                        try {
                            this.handleException(e);
                            len = new char[]{};
                            if (br == null) break block16;
                            br.releaseBuffer();
                        }
                        catch (Throwable var11_14) {
                            if (br != null) {
                                br.releaseBuffer();
                            }
                            throw var11_14;
                        }
                    }
                    return len;
                }
            }
            return arrc;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private BufferResource<CharBuffer> getCharBuffer(int start, int len) {
        if (len < 0 || start < 0) {
            throw new IllegalArgumentException("Illogical text range from " + start + " to " + (start + len));
        }
        Object object = this.readLock();
        synchronized (object) {
            if (this.isDisposed()) {
                return null;
            }
            int fileStart = AbstractLines.toByteIndex(start);
            int byteCount = AbstractLines.toByteIndex(len);
            int available = this.getStorage().size();
            if (available < fileStart + byteCount) {
                throw new ArrayIndexOutOfBoundsException("Bytes from " + fileStart + " to " + (fileStart + byteCount) + " requested, " + "but storage is only " + available + " bytes long");
            }
            BufferResource<ByteBuffer> readBuffer = null;
            try {
                readBuffer = this.getStorage().getReadBuffer(fileStart, byteCount);
                return new CharBufferResource(readBuffer);
            }
            catch (Exception e) {
                if (readBuffer != null) {
                    readBuffer.releaseBuffer();
                }
                return null;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public String getText(int start, int end) {
        BufferResource<CharBuffer> br = this.getCharBuffer(start, end - start);
        try {
            String s;
            CharBuffer cb = br == null ? null : br.getBuffer();
            String string = s = cb != null ? cb.toString() : new String(new char[end - start]);
            return string;
        }
        finally {
            if (br != null) {
                br.releaseBuffer();
            }
        }
    }

    void onDispose(int lastStorageSize) {
        this.lastStorageSize = lastStorageSize;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    int getByteSize() {
        Object object = this.readLock();
        synchronized (object) {
            if (this.lastStorageSize >= 0) {
                return this.lastStorageSize;
            }
            // MONITOREXIT [0, 1] lbl7 : MonitorExitStatement: MONITOREXIT : var1_1
            Storage storage = this.getStorage();
            return storage == null ? 0 : storage.size();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void addChangeListener(ChangeListener cl) {
        this.listener = cl;
        Object object = this.readLock();
        synchronized (object) {
            if (this.getLineCount() > 0) {
                this.fire();
            }
        }
    }

    @Override
    public void removeChangeListener(ChangeListener cl) {
        if (this.listener == cl) {
            this.listener = null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        this.newEvent.set(false);
        this.fire();
        AtomicBoolean atomicBoolean = this.newEvent;
        synchronized (atomicBoolean) {
            if (!this.newEvent.get()) {
                this.timer.stop();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void delayedFire() {
        this.newEvent.set(true);
        if (this.listener == null) {
            return;
        }
        if (this.timer == null) {
            this.timer = new Timer(200, this);
        }
        AtomicBoolean atomicBoolean = this.newEvent;
        synchronized (atomicBoolean) {
            if (this.newEvent.get() && !this.timer.isRunning()) {
                this.timer.start();
            }
        }
    }

    public void fire() {
        if (Controller.LOG) {
            Controller.log(this + ": Writer firing " + this.getStorage().size() + " bytes written");
        }
        if (this.listener != null) {
            Mutex.EVENT.readAccess((Runnable)this);
        }
    }

    @Override
    public void run() {
        if (this.listener != null) {
            this.listener.stateChanged(new ChangeEvent(this));
        }
    }

    @Override
    public boolean hasListeners() {
        return this.firstListenerLine() != -1;
    }

    @Override
    public OutputListener getListener(int pos, int[] range) {
        int line = this.getLineAt(pos);
        int lineStart = this.getLineStart(line);
        pos -= lineStart;
        LineInfo info = (LineInfo)this.lineWithListenerToInfo.get(line);
        if (info == null) {
            return null;
        }
        int start = 0;
        for (LineInfo.Segment seg : info.getLineSegments()) {
            if (pos < seg.getEnd()) {
                if (seg.getListener() != null) {
                    if (range != null) {
                        range[0] = lineStart + start;
                        range[1] = lineStart + seg.getEnd();
                    }
                    return seg.getListener();
                }
                return null;
            }
            start = seg.getEnd();
        }
        return null;
    }

    @Override
    public boolean isListener(int start, int end) {
        int[] range = new int[2];
        OutputListener l = this.getListener(start, range);
        return l == null ? false : range[0] == start && range[1] == end;
    }

    private void init() {
        this.knownLogicalLineCounts = null;
        this.lineStartList = new IntList(128);
        this.lineStartList.add(0);
        this.lineCharLengthListWithTabs = new IntListSimple(100);
        this.linesToInfos = new IntMap();
        this.lineWithListenerToInfo = new IntMap();
        this.longestLineLen = 0;
        this.listener = null;
        this.dirty = false;
        this.curDefColors = (Color[])AbstractLines.getDefColors().clone();
    }

    @Override
    public boolean checkDirty(boolean clear) {
        if (this.isDisposed()) {
            return false;
        }
        boolean wasDirty = this.dirty;
        if (clear) {
            this.dirty = false;
        }
        return wasDirty;
    }

    @Override
    public int[] getLinesWithListeners() {
        return this.lineWithListenerToInfo.getKeys();
    }

    @Override
    public int getCharCount() {
        return AbstractLines.toCharIndex(this.getByteSize());
    }

    @Override
    public String getLine(int idx) throws IOException {
        int lineStart = this.getCharLineStart(idx);
        int lineEnd = AbstractLines.toCharIndex(idx < this.lineStartList.size() - 1 ? this.lineStartList.get(idx + 1) : this.getByteSize());
        return this.getText(lineStart, lineEnd);
    }

    private int getByteLineLength(int idx) {
        if (idx == this.lineStartList.size() - 1) {
            return Math.max(0, AbstractLines.toByteIndex(this.lastLineLength));
        }
        int lineStart = this.getByteLineStart(idx);
        int lineEnd = idx < this.lineStartList.size() - 1 ? this.lineStartList.get(idx + 1) - 2 * "\n".length() : this.getByteSize();
        return lineEnd - lineStart;
    }

    @Override
    public boolean isLineStart(int chpos) {
        int bpos = AbstractLines.toByteIndex(chpos);
        return this.lineStartList.contains(bpos) || bpos == 0 || bpos == this.getByteSize() && this.lastLineFinished;
    }

    @Override
    public int length(int idx) {
        return AbstractLines.toCharIndex(this.getByteLineLength(idx));
    }

    @Override
    public int lengthWithTabs(int idx) {
        if (idx == this.lineCharLengthListWithTabs.size()) {
            return Math.max(0, this.lastCharLengthWithTabs);
        }
        return this.lineCharLengthListWithTabs.get(idx);
    }

    @Override
    public int getLineStart(int line) {
        return this.getCharLineStart(line);
    }

    private int getByteLineStart(int line) {
        if (line == this.lineStartList.size() && this.lastLineFinished) {
            return this.getByteSize();
        }
        return this.lineStartList.get(line);
    }

    private int getCharLineStart(int line) {
        return AbstractLines.toCharIndex(this.getByteLineStart(line));
    }

    @Override
    public int getLineAt(int position) {
        int bytePos = AbstractLines.toByteIndex(position);
        if (bytePos >= this.getByteSize()) {
            return this.getLineCount() - 1;
        }
        return this.lineStartList.findNearest(bytePos);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int getLineCount() {
        Object object = this.readLock();
        synchronized (object) {
            return this.lineStartList.size();
        }
    }

    @Override
    public Collection<OutputListener> getListenersForLine(int line) {
        LineInfo info = (LineInfo)this.lineWithListenerToInfo.get(line);
        if (info == null) {
            return Collections.emptyList();
        }
        return info.getListeners();
    }

    @Override
    public int firstListenerLine() {
        if (this.isDisposed()) {
            return -1;
        }
        return this.lineWithListenerToInfo.isEmpty() ? -1 : this.lineWithListenerToInfo.first();
    }

    @Override
    public OutputListener nearestListener(int pos, boolean backward, int[] range) {
        int posLine = this.getLineAt(pos);
        int line = this.lineWithListenerToInfo.nearest(posLine, backward);
        if (line < 0) {
            return null;
        }
        LineInfo info = (LineInfo)this.lineWithListenerToInfo.get(line);
        int lineStart = this.getLineStart(line);
        OutputListener l = null;
        int[] lpos = new int[2];
        if (posLine == line) {
            if (backward) {
                info.getFirstListener(lpos);
                if (lpos[0] + lineStart > pos) {
                    line = this.lineWithListenerToInfo.nearest(line - 1, backward);
                    info = (LineInfo)this.lineWithListenerToInfo.get(line);
                    lineStart = this.getLineStart(line);
                    l = info.getLastListener(lpos);
                }
            } else {
                info.getLastListener(lpos);
                if (lpos[1] + lineStart <= pos) {
                    line = this.lineWithListenerToInfo.nearest(line + 1, backward);
                    info = (LineInfo)this.lineWithListenerToInfo.get(line);
                    lineStart = this.getLineStart(line);
                    l = info.getFirstListener(lpos);
                }
            }
        } else {
            pos = lineStart;
            OutputListener outputListener = l = backward ? info.getLastListener(lpos) : info.getFirstListener(lpos);
        }
        if (l == null) {
            OutputListener outputListener = l = backward ? info.getListenerBefore(pos - lineStart, lpos) : info.getListenerAfter(pos - lineStart, lpos);
        }
        if (l != null) {
            range[0] = lpos[0] + lineStart;
            range[1] = lpos[1] + lineStart;
        }
        return l;
    }

    @Override
    public int getLongestLineLength() {
        return this.longestLineLen;
    }

    @Override
    public void toPhysicalLineIndex(int[] info, int charsPerLine) {
        int logicalLineIdx = info[0];
        if (logicalLineIdx <= 0) {
            info[0] = 0;
            info[1] = 0;
            info[2] = AbstractLines.lengthToLineCount(this.lengthWithTabs(0), charsPerLine);
            return;
        }
        if (charsPerLine >= this.longestLineLen || this.getLineCount() < 1) {
            info[0] = this.visibleToRealLine(logicalLineIdx);
            info[1] = 0;
            info[2] = 1;
            return;
        }
        int physLineIdx = Math.min(this.findPhysicalLine(logicalLineIdx, charsPerLine), this.getLineCount() - 1);
        int linesAbove = this.getLogicalLineCountAbove(physLineIdx, charsPerLine);
        int len = this.lengthWithTabs(physLineIdx);
        int wrapCount = AbstractLines.lengthToLineCount(len, charsPerLine);
        info[0] = physLineIdx;
        info[1] = logicalLineIdx - linesAbove;
        info[2] = wrapCount;
    }

    private int findPhysicalLine(int logicalLineIdx, int charsPerLine) {
        if (logicalLineIdx == 0) {
            return 0;
        }
        if (charsPerLine != this.knownCharsPerLine || this.knownLogicalLineCounts == null) {
            this.calcLogicalLineCount(charsPerLine);
        }
        return this.knownLogicalLineCounts.getNextKey(logicalLineIdx);
    }

    @Override
    public int getLogicalLineCountAbove(int line, int charsPerLine) {
        if (line == 0) {
            return 0;
        }
        if (charsPerLine >= this.longestLineLen) {
            return this.realToVisibleLine(line);
        }
        if (charsPerLine != this.knownCharsPerLine || this.knownLogicalLineCounts == null) {
            this.calcLogicalLineCount(charsPerLine);
        }
        return this.knownLogicalLineCounts.get(line - 1);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int getLogicalLineCountIfWrappedAt(int charsPerLine) {
        if (charsPerLine >= this.longestLineLen) {
            return this.getVisibleLineCount();
        }
        int lineCount = this.getLineCount();
        if (charsPerLine == 0 || lineCount == 0) {
            return 0;
        }
        Object object = this.readLock();
        synchronized (object) {
            if (charsPerLine != this.knownCharsPerLine || this.knownLogicalLineCounts == null) {
                this.calcLogicalLineCount(charsPerLine);
            }
            return this.knownLogicalLineCounts.get(lineCount - 1);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int getNumPhysicalChars(int offset, int logicalLength, int[] tabShiftPtr) {
        Object object = this.readLock();
        synchronized (object) {
            int i;
            int tabSum2;
            int tabIndex1 = this.tabCharOffsets.findNearest(offset);
            int tabSum1 = tabIndex1 >= 0 ? (this.tabCharOffsets.get(tabIndex1) < offset ? this.tabLengthSums.get(tabIndex1) : (tabIndex1 > 0 ? this.tabLengthSums.get(tabIndex1 - 1) : 0)) : 0;
            int tabIndex2 = this.tabCharOffsets.findNearest(offset + logicalLength);
            if (tabIndex2 < 0) {
                return logicalLength;
            }
            int n = tabSum2 = tabIndex2 >= 0 ? this.tabLengthSums.get(tabIndex2) : 0;
            if (tabSum1 == tabSum2) {
                return logicalLength;
            }
            int realLength = logicalLength + tabSum1 - tabSum2;
            if (realLength < 0) {
                realLength = 0;
            }
            if ((tabIndex2 = this.tabCharOffsets.findNearest(i = offset + realLength)) < 0) {
                tabIndex2 = -1;
                tabSum2 = 0;
            } else {
                tabSum2 = this.tabLengthSums.get(tabIndex2);
            }
            int n2 = this.tabCharOffsets.size();
            ++tabIndex2;
            while (tabIndex2 < n2) {
                int nextTabOffset;
                if (realLength + tabSum2 - tabSum1 < logicalLength) {
                    nextTabOffset = this.tabCharOffsets.get(tabIndex2);
                    if (nextTabOffset - offset + (tabSum2 = this.tabLengthSums.get(tabIndex2)) - tabSum1 > logicalLength) {
                        tabSum2 = tabIndex2 > 0 ? this.tabLengthSums.get(tabIndex2 - 1) : 0;
                        realLength = logicalLength + tabSum1 - tabSum2;
                        if (realLength <= nextTabOffset - offset) break;
                        realLength = nextTabOffset - offset;
                        break;
                    }
                } else {
                    if (tabShiftPtr == null) break;
                    tabShiftPtr[0] = realLength + tabSum2 - tabSum1 - logicalLength;
                    break;
                }
                realLength = nextTabOffset - offset;
                ++tabIndex2;
            }
            if (realLength < 0) {
                realLength = 0;
            }
            return realLength;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int getNumLogicalChars(int offset, int physicalLength) {
        Object object = this.readLock();
        synchronized (object) {
            int tabIndex1 = this.tabCharOffsets.findNearest(offset);
            int tabSum1 = tabIndex1 >= 0 && this.tabCharOffsets.get(tabIndex1) < offset ? this.tabLengthSums.get(tabIndex1) : (tabIndex1 > 0 ? this.tabLengthSums.get(tabIndex1 - 1) : 0);
            int tabIndex2 = this.tabCharOffsets.findNearest(offset + physicalLength - 1);
            if (tabIndex2 < 0) {
                return physicalLength;
            }
            int tabSum2 = this.tabLengthSums.get(tabIndex2);
            return physicalLength + tabSum2 - tabSum1;
        }
    }

    private void registerLineWithListener(int line, LineInfo info, boolean important) {
        this.lineWithListenerToInfo.put(line, info);
        if (important) {
            this.importantLines.add(line);
        }
    }

    @Override
    public int firstImportantListenerLine() {
        return this.importantLines.size() == 0 ? -1 : this.importantLines.get(0);
    }

    @Override
    public boolean isImportantLine(int line) {
        return this.importantLines.contains(line);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void calcLogicalLineCount(int width) {
        Object object = this.readLock();
        synchronized (object) {
            int lineCount = this.getLineCount();
            this.knownLogicalLineCounts = new SparseIntList(30);
            int val = 0;
            for (int i = 0; i < lineCount; ++i) {
                if (!this.isVisible(i)) {
                    this.knownLogicalLineCounts.add(i, val);
                    continue;
                }
                int len = this.lengthWithTabs(i);
                if (len > width) {
                    this.knownLogicalLineCounts.add(i, val += AbstractLines.lengthToLineCount(len, width));
                    continue;
                }
                ++val;
            }
            this.knownCharsPerLine = width;
        }
    }

    static int lengthToLineCount(int len, int charsPerLine) {
        return len > charsPerLine ? (charsPerLine == 0 ? len : (len + charsPerLine - 1) / charsPerLine) : 1;
    }

    void markDirty() {
        this.dirty = true;
    }

    boolean isLastLineFinished() {
        return this.lastLineFinished;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void updateLastLine(int lineIdx, int lineLength) {
        Object object = this.readLock();
        synchronized (object) {
            boolean alreadyAdded;
            this.longestLineLen = Math.max(this.longestLineLen, lineLength);
            if (this.knownLogicalLineCounts == null) {
                return;
            }
            if (!(this.currentFoldStart < 0 || this.visibleList.get(this.currentFoldStart) != 0 && this.isVisible(this.currentFoldStart))) {
                if (this.knownLogicalLineCounts.lastIndex() != lineIdx) {
                    this.knownLogicalLineCounts.add(lineIdx, this.knownLogicalLineCounts.get(lineIdx - 1));
                }
                return;
            }
            boolean bl = alreadyAdded = this.knownLogicalLineCounts.lastIndex() == lineIdx;
            if (alreadyAdded) {
                assert (!this.lastLineFinished);
                if (lineLength <= this.knownCharsPerLine) {
                    this.knownLogicalLineCounts.removeLast();
                } else {
                    int aboveLineCount = this.knownLogicalLineCounts.lastAdded() - AbstractLines.lengthToLineCount(this.lastCharLengthWithTabs, this.knownCharsPerLine) + AbstractLines.lengthToLineCount(lineLength, this.knownCharsPerLine);
                    this.knownLogicalLineCounts.updateLast(lineIdx, aboveLineCount);
                }
            } else {
                if (lineLength <= this.knownCharsPerLine) {
                    return;
                }
                int aboveLineCount = this.knownLogicalLineCounts.lastIndex() != -1 ? lineIdx - (this.knownLogicalLineCounts.lastIndex() + 1) + this.knownLogicalLineCounts.lastAdded() : Math.max(0, lineIdx - 1);
                this.knownLogicalLineCounts.add(lineIdx, aboveLineCount += AbstractLines.lengthToLineCount(lineLength, this.knownCharsPerLine));
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void lineUpdated(int lineStart, int lineLength, int charLengthWithTabs, boolean isFinished) {
        Object object = this.readLock();
        synchronized (object) {
            int charLineLength = AbstractLines.toCharIndex(lineLength);
            if (isFinished) {
                --charLineLength;
            }
            int lineIndex = this.lineStartList.size() - 1;
            this.updateLastLine(lineIndex, charLengthWithTabs);
            if (isFinished) {
                this.lineStartList.add(lineStart + lineLength);
                this.updateFolds(lineIndex);
                this.lineCharLengthListWithTabs.add(charLengthWithTabs);
            }
            this.lastLineFinished = isFinished;
            this.lastLineLength = isFinished ? -1 : charLineLength;
            this.lastCharLengthWithTabs = isFinished ? -1 : charLengthWithTabs;
        }
        this.markDirty();
    }

    private void updateFolds(int lineIndex) {
        if (this.currentFoldStart >= this.visibleList.size()) {
            LOG.log(Level.FINE, "currentFoldStart = {0}, visibleList.size() = {1}: Forgetting current fold.", new Object[]{this.currentFoldStart, this.visibleList.size()});
            this.currentFoldStart = -1;
        }
        if (this.currentFoldStart == -1) {
            this.foldOffsets.add(0);
        } else {
            this.foldOffsets.add(lineIndex - this.currentFoldStart);
        }
        if (!(this.currentFoldStart == -1 || this.visibleList.get(this.currentFoldStart) != 0 && this.isVisible(this.currentFoldStart))) {
            ++this.hiddenLines;
            this.realToVisibleLine.add(-1);
        } else {
            this.visibleToRealLine.add(lineIndex);
            this.realToVisibleLine.add(lineIndex - this.hiddenLines);
        }
        this.visibleList.add(1);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void setCurrentFoldStart(int foldStart) {
        Object object = this.readLock();
        synchronized (object) {
            if (foldStart > -1 && foldStart + 1 < this.foldOffsets.size() && this.foldOffsets.get(foldStart + 1) != 1) {
                LOG.log(Level.FINE, "Ignoring currentFoldStart at {0}, because foldOffset at {1} is {2}", new Object[]{foldStart, foldStart + 1, this.foldOffsets.get(foldStart + 1)});
                this.currentFoldStart = -1;
            } else if (foldStart >= this.foldOffsets.size()) {
                LOG.log(Level.FINE, "Ignoring currentFoldStart at {0}, foldOffsets.size() is only {1}", new Object[]{foldStart, this.foldOffsets.size()});
                this.currentFoldStart = -1;
            } else {
                this.currentFoldStart = foldStart;
            }
        }
    }

    IntListSimple getFoldOffsets() {
        return this.foldOffsets;
    }

    static int toByteIndex(int charIndex) {
        return charIndex << 1;
    }

    static int toCharIndex(int byteIndex) {
        assert (byteIndex % 2 == 0);
        return byteIndex >> 1;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void saveAs(String path) throws IOException {
        FileChannel ch;
        Storage storage = this.getStorage();
        if (storage == null) {
            throw new IOException("Data has already been disposed");
        }
        FileOutputStream fos = new FileOutputStream(path);
        try {
            String encoding = System.getProperty("file.encoding");
            if (encoding == null) {
                encoding = "UTF-8";
            }
            Charset charset = Charset.forName(encoding);
            CharsetEncoder encoder = charset.newEncoder();
            String ls = System.getProperty("line.separator");
            ch = fos.getChannel();
            ByteBuffer lsbb = encoder.encode(CharBuffer.wrap(ls));
            for (int i = 0; i < this.getLineCount(); ++i) {
                int lineStart = this.getCharLineStart(i);
                int lineLength = this.length(i);
                BufferResource<CharBuffer> br = this.getCharBuffer(lineStart, lineLength);
                try {
                    CharBuffer cb = br.getBuffer();
                    ByteBuffer bb = encoder.encode(cb);
                    ch.write(bb);
                    if (i == this.getLineCount() - 1) continue;
                    lsbb.rewind();
                    ch.write(lsbb);
                }
                finally {
                    if (br != null) {
                        br.releaseBuffer();
                    }
                }
            }
        }
        catch (Throwable var17_17) {
            fos.close();
            FileUtil.refreshFor((File[])new File[]{new File(path)});
            throw var17_17;
        }
        ch.close();
        fos.close();
        FileUtil.refreshFor((File[])new File[]{new File(path)});
    }

    static Color[] getDefColors() {
        if (DEF_COLORS != null) {
            return DEF_COLORS;
        }
        DEF_COLORS = new Color[]{OutputOptions.getDefault().getColorStandard(), OutputOptions.getDefault().getColorError(), OutputOptions.getDefault().getColorLink(), OutputOptions.getDefault().getColorLinkImportant(), OutputOptions.getDefault().getColorInput()};
        return DEF_COLORS;
    }

    @Override
    public void setDefColor(IOColors.OutputType type, Color color) {
        this.curDefColors[type.ordinal()] = color;
    }

    @Override
    public Color getDefColor(IOColors.OutputType type) {
        return this.curDefColors[type.ordinal()];
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public LineInfo getLineInfo(int line) {
        Object object = this.readLock();
        synchronized (object) {
            LineInfo info = (LineInfo)this.linesToInfos.get(line);
            if (info != null) {
                int lineLength = this.length(line);
                if (lineLength > info.getEnd()) {
                    info.addSegment(lineLength, OutputKind.IN, null, null, null, false);
                }
                return info;
            }
            if (line == this.getLineCount() - 1) {
                LineInfo li = new LineInfo(this);
                try {
                    li.addSegment(this.getLine(line).length(), OutputKind.OUT, null, null, null, false);
                }
                catch (IOException e) {
                    LOG.log(Level.INFO, null, e);
                }
                li.addSegment(this.length(line), OutputKind.IN, null, null, null, false);
                return li;
            }
            return new LineInfo(this, this.length(line));
        }
    }

    public LineInfo getExistingLineInfo(int line) {
        return (LineInfo)this.linesToInfos.get(line);
    }

    private boolean regExpChanged(String pattern, boolean matchCase) {
        return this.pattern != null && (!this.pattern.toString().equals(pattern) || this.pattern.flags() == 2 == matchCase);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int[] find(int start, String pattern, boolean regExp, boolean matchCase) {
        Storage storage = this.getStorage();
        if (storage == null) {
            return null;
        }
        if (regExp && this.regExpChanged(pattern, matchCase)) {
            this.pattern = null;
        }
        if (!regExp && !matchCase) {
            pattern = pattern.toLowerCase();
        }
        do {
            BufferResource<ByteBuffer> br = null;
            int size = this.getCharCount() - start;
            if (size > 16384) {
                int l = this.getLineAt(start + 16384);
                size = this.getLineStart(l) + this.length(l) - start;
            } else if (size <= 0) break;
            CharBuffer buff = null;
            try {
                try {
                    br = storage.getReadBuffer(AbstractLines.toByteIndex(start), AbstractLines.toByteIndex(size));
                    buff = br.getBuffer().asCharBuffer();
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
                if (buff == null) break;
                if (regExp) {
                    Matcher matcher;
                    if (this.pattern == null) {
                        Pattern pattern2 = this.pattern = matchCase ? Pattern.compile(pattern) : Pattern.compile(pattern, 2);
                    }
                    if ((matcher = this.pattern.matcher(buff)).find()) {
                        int[] arrn = new int[]{start + matcher.start(), start + matcher.end()};
                        return arrn;
                    }
                } else {
                    int idx;
                    int n = idx = matchCase ? buff.toString().indexOf(pattern) : buff.toString().toLowerCase().indexOf(pattern);
                    if (idx != -1) {
                        int[] arrn = new int[]{start + idx, start + idx + pattern.length()};
                        return arrn;
                    }
                }
                start += buff.length();
                continue;
            }
            finally {
                if (br == null) continue;
                br.releaseBuffer();
                continue;
            }
            break;
        } while (true);
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int[] rfind(int start, String pattern, boolean regExp, boolean matchCase) {
        Storage storage = this.getStorage();
        if (storage == null) {
            return null;
        }
        if (regExp && this.regExpChanged(pattern, matchCase)) {
            this.pattern = null;
        }
        if (!regExp && !matchCase) {
            pattern = pattern.toLowerCase();
        }
        do {
            int end = start;
            start = end - 16384;
            if (start < 0) {
                start = 0;
            } else {
                int l = this.getLineAt(start);
                start = this.getLineStart(l);
            }
            if (start == end) break;
            BufferResource<ByteBuffer> br = null;
            try {
                CharBuffer buff = null;
                try {
                    br = storage.getReadBuffer(AbstractLines.toByteIndex(start), AbstractLines.toByteIndex(end - start));
                    buff = br.getBuffer().asCharBuffer();
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
                if (buff == null) break;
                if (regExp) {
                    if (this.pattern == null) {
                        this.pattern = matchCase ? Pattern.compile(pattern) : Pattern.compile(pattern, 2);
                    }
                    Matcher matcher = this.pattern.matcher(buff);
                    int mStart = -1;
                    int mEnd = -1;
                    while (matcher.find()) {
                        mStart = matcher.start();
                        mEnd = matcher.end();
                    }
                    if (mStart == -1) continue;
                    int[] arrn = new int[]{start + mStart, start + mEnd};
                    return arrn;
                }
                int idx = matchCase ? buff.toString().lastIndexOf(pattern) : buff.toString().toLowerCase().lastIndexOf(pattern);
                if (idx == -1) continue;
                int[] mStart = new int[]{start + idx, start + idx + pattern.length()};
                return mStart;
            }
            finally {
                if (br == null) continue;
                br.releaseBuffer();
                continue;
            }
            break;
        } while (true);
        return null;
    }

    public String toString() {
        return this.lineStartList.toString();
    }

    private int addSegment(CharSequence s, int offset, int lineIdx, int pos, OutputListener l, boolean important, OutputKind outKind, Color c, Color b) {
        int len = this.length(lineIdx);
        if (len > 0) {
            LineInfo info = (LineInfo)this.linesToInfos.get(lineIdx);
            if (info == null) {
                info = new LineInfo(this);
                this.linesToInfos.put(lineIdx, info);
            }
            int curEnd = info.getEnd();
            if (pos > 0 && pos != curEnd) {
                info.addSegment(pos, OutputKind.OUT, null, null, null, false);
                curEnd = pos;
            }
            if (l != null) {
                int trailingCnt;
                int endPos = Math.min(curEnd + s.length() - offset, len);
                int strlen = Math.min(s.length(), offset + len);
                if (s.charAt(strlen - 1) == '\n') {
                    --strlen;
                }
                if (s.charAt(strlen - 1) == '\r') {
                    --strlen;
                }
                int leadingCnt = 0;
                while (leadingCnt + offset < strlen && Character.isWhitespace(s.charAt(offset + leadingCnt))) {
                    ++leadingCnt;
                }
                if (leadingCnt != strlen) {
                    for (trailingCnt = 0; trailingCnt < strlen && Character.isWhitespace(s.charAt(strlen - trailingCnt - 1)); ++trailingCnt) {
                    }
                }
                if (leadingCnt > 0) {
                    info.addSegment(curEnd + leadingCnt, OutputKind.OUT, null, null, null, false);
                }
                info.addSegment(endPos - trailingCnt, outKind, l, c, b, important);
                if (trailingCnt > 0) {
                    info.addSegment(endPos, OutputKind.OUT, null, null, null, false);
                }
                this.registerLineWithListener(lineIdx, info, important);
            } else {
                info.addSegment(len, outKind, l, c, b, important);
                if (important) {
                    this.importantLines.add(lineIdx);
                }
            }
        }
        return len;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void updateLinesInfo(CharSequence s, int startLine, int startPos, OutputListener l, boolean important, OutputKind outKind, Color c, Color b) {
        int offset = 0;
        Object object = this.readLock();
        synchronized (object) {
            int startLinePos = startPos - this.getLineStart(startLine);
            for (int i = startLine; i < this.getLineCount(); ++i) {
                offset += this.addSegment(s, offset, i, startLinePos, l, important, outKind, c, b) + 1;
                startLinePos = 0;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void addLineInfo(int idx, LineInfo info, boolean important) {
        Object object = this.readLock();
        synchronized (object) {
            this.linesToInfos.put(idx, info);
            if (!info.getListeners().isEmpty()) {
                this.registerLineWithListener(idx, info, important);
            }
        }
    }

    private int getTabLength(int i) {
        if (i == 0) {
            return this.tabLengthSums.get(0);
        }
        return this.tabLengthSums.get(i) - this.tabLengthSums.get(i - 1) + 1;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    int checkLimits() {
        Object object = this.readLock();
        synchronized (object) {
            if (this.getLineCount() >= this.outputLimits.getMaxLines() || this.getCharCount() >= this.outputLimits.getMaxChars() || this.linesToInfos.size() >= 524288) {
                return this.removeOldLines();
            }
            return 0;
        }
    }

    private int removeOldLines() {
        int newFirstLine = Math.min(this.outputLimits.getRemoveLines(), this.lineStartList.size() / 2);
        int firstByteOffset = this.lineStartList.get(newFirstLine);
        this.lineStartList.compact(newFirstLine, firstByteOffset);
        this.lineCharLengthListWithTabs.compact(newFirstLine, 0);
        this.lineWithListenerToInfo.decrementKeys(newFirstLine);
        this.linesToInfos.decrementKeys(newFirstLine);
        int firstCharOffset = AbstractLines.toCharIndex(firstByteOffset);
        int firstTabIndex = this.tabCharOffsets.findNearest(firstCharOffset);
        this.tabCharOffsets.compact(Math.max(0, firstTabIndex), firstCharOffset);
        if (firstTabIndex > 0) {
            this.tabLengthSums.compact(firstTabIndex, this.tabLengthSums.get(firstTabIndex - 1));
        }
        int firstImportantLine = this.importantLines.findNearest(newFirstLine);
        this.importantLines.compact(Math.max(0, firstImportantLine), newFirstLine);
        this.knownLogicalLineCounts = null;
        this.foldOffsets.compact(newFirstLine, 0);
        for (int foIndex = 0; foIndex < this.foldOffsets.size() && this.foldOffsets.get(foIndex) != 0; ++foIndex) {
            this.foldOffsets.set(foIndex, 0);
        }
        this.visibleList.compact(newFirstLine, 0);
        this.visibleList.set(0, 1);
        this.realToVisibleLine.compact(newFirstLine, 0);
        this.currentFoldStart = Math.max(-1, this.currentFoldStart - newFirstLine);
        this.recomputeRealToVisibleLine();
        this.updateVisibleToRealLines(0);
        this.getStorage().shiftStart(firstByteOffset);
        this.fire();
        return firstByteOffset;
    }

    private void recomputeRealToVisibleLine() {
        int currentVisibleLine = 0;
        int currentHiddenLineCount = 0;
        int hiddenFoldStart = -1;
        for (int i = 0; i < this.realToVisibleLine.size(); ++i) {
            if (hiddenFoldStart == -1 && this.visibleList.get(i) == 0) {
                hiddenFoldStart = i;
                this.realToVisibleLine.set(i, currentVisibleLine++);
                continue;
            }
            if (hiddenFoldStart > -1 && this.foldOffsets.get(i) > 0 && this.foldOffsets.get(i) <= i - hiddenFoldStart) {
                this.realToVisibleLine.set(i, -1);
                ++currentHiddenLineCount;
                continue;
            }
            this.realToVisibleLine.set(i, currentVisibleLine++);
            hiddenFoldStart = -1;
        }
        this.hiddenLines = currentHiddenLineCount;
    }

    void setOutputLimits(OutputLimits outputLimits) {
        this.outputLimits = outputLimits;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void addTabAt(int i, int tabLength) {
        --tabLength;
        Object object = this.readLock();
        synchronized (object) {
            LOG.log(Level.FINEST, "addTabAt: i = {0}", i);
            this.tabCharOffsets.add(i);
            int n = this.tabLengthSums.size();
            if (n > 0) {
                tabLength += this.tabLengthSums.get(n - 1);
            }
            this.tabLengthSums.add(tabLength);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    int removeLastTab() {
        Object object = this.readLock();
        synchronized (object) {
            LOG.log(Level.FINEST, "removeLastTabAt");
            int size = this.tabLengthSums.size();
            if (size == 0) {
                return 0;
            }
            this.tabCharOffsets.shorten(this.tabCharOffsets.size() - 1);
            int tabLen = size > 1 ? this.tabLengthSums.get(size - 1) - this.tabLengthSums.get(size - 2) : this.tabLengthSums.get(0);
            this.tabLengthSums.shorten(size - 1);
            return tabLen;
        }
    }

    private boolean isFoldStartValid(int foldStartIndex) {
        return foldStartIndex >= 0 && foldStartIndex < this.foldOffsets.size();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void showFold(int foldStartIndex) {
        Object object = this.readLock();
        synchronized (object) {
            if (this.isFoldStartValid(foldStartIndex)) {
                this.setFoldExpanded(foldStartIndex, true);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void hideFold(int foldStartIndex) {
        Object object = this.readLock();
        synchronized (object) {
            if (this.isFoldStartValid(foldStartIndex)) {
                this.setFoldExpanded(foldStartIndex, false);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void setFoldExpanded(int foldStartIndex, boolean expanded) {
        Object object = this.readLock();
        synchronized (object) {
            if (this.visibleList.get(foldStartIndex) == (expanded ? 1 : 0)) {
                return;
            }
            this.visibleList.set(foldStartIndex, expanded ? 1 : 0);
            if (!this.isVisible(foldStartIndex)) {
                return;
            }
            int len = this.foldLength(foldStartIndex);
            if (len > 0) {
                int changed = this.updateRealToVisibleIndexesInFold(foldStartIndex, len, expanded);
                for (int i = foldStartIndex + len + 1; i < this.realToVisibleLine.size(); ++i) {
                    int currentValue = this.realToVisibleLine.get(i);
                    if (currentValue == -1) continue;
                    this.realToVisibleLine.set(i, currentValue + (expanded ? changed : - changed));
                }
                this.hiddenLines += expanded ? - changed : changed;
                this.updateVisibleToRealLines(foldStartIndex);
                this.foldVisibilityUpdated();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void hideAllFolds() {
        Object object = this.readLock();
        synchronized (object) {
            int currentVisibleLine = 0;
            int currentHiddenLineCount = 0;
            for (int i = 0; i < this.foldOffsets.size(); ++i) {
                boolean isFoldStart;
                boolean bl = isFoldStart = i + 1 < this.foldOffsets.size() && this.foldOffsets.get(i + 1) == 1;
                if (isFoldStart) {
                    this.visibleList.set(i, 0);
                }
                if (this.foldOffsets.get(i) > 0) {
                    this.realToVisibleLine.set(i, -1);
                    ++currentHiddenLineCount;
                    continue;
                }
                this.realToVisibleLine.set(i, currentVisibleLine);
                ++currentVisibleLine;
            }
            this.hiddenLines = currentHiddenLineCount;
            this.updateVisibleToRealLines(0);
            this.foldVisibilityUpdated();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void showAllFolds() {
        Object object = this.readLock();
        synchronized (object) {
            for (int i = 0; i < this.foldOffsets.size(); ++i) {
                boolean isFoldStart;
                boolean bl = isFoldStart = i + 1 < this.foldOffsets.size() && this.foldOffsets.get(i + 1) == 1;
                if (isFoldStart) {
                    this.visibleList.set(i, 1);
                }
                this.realToVisibleLine.set(i, i);
            }
            this.hiddenLines = 0;
            this.updateVisibleToRealLines(0);
            this.foldVisibilityUpdated();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void showFoldTree(int foldStartIndex) {
        Object object = this.readLock();
        synchronized (object) {
            if (this.isFoldStartValid(foldStartIndex)) {
                this.setFoldTreeExpanded(foldStartIndex, true);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void showFoldAndParentFolds(int foldStartIndex) {
        Object object = this.readLock();
        synchronized (object) {
            int parentFoldStart;
            int foldOffset = this.getFoldOffsets().get(foldStartIndex);
            if (foldOffset > 0 && (parentFoldStart = foldStartIndex - foldOffset) >= 0) {
                this.showFoldAndParentFolds(parentFoldStart);
            }
            this.showFold(foldStartIndex);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void showFoldsForLine(int realLineIndex) {
        Object object = this.readLock();
        synchronized (object) {
            int foldStart = this.getFoldStart(realLineIndex);
            this.showFoldAndParentFolds(foldStart);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void hideFoldTree(int foldStartIndex) {
        Object object = this.readLock();
        synchronized (object) {
            if (this.isFoldStartValid(foldStartIndex)) {
                this.setFoldTreeExpanded(foldStartIndex, false);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void setFoldTreeExpanded(int foldStartIndex, boolean expanded) {
        int visibleValue = expanded ? 1 : 0;
        Object object = this.readLock();
        synchronized (object) {
            assert (this.isVisible(foldStartIndex));
            this.visibleList.set(foldStartIndex, visibleValue);
            int visibleFoldStartIndex = this.realToVisibleLine.get(foldStartIndex);
            int delta = 0;
            int lastUpdatedIndex = 2147483646;
            int i = foldStartIndex + 1;
            while (i < this.foldOffsets.size()) {
                int offset = i - foldStartIndex;
                if (this.foldOffsets.get(i) == 0 || this.foldOffsets.get(i) > offset) break;
                if (i + 1 < this.foldOffsets.size() && this.foldOffsets.get(i + 1) == 1) {
                    this.visibleList.set(i, visibleValue);
                }
                int visibleIndex = this.realToVisibleLine.get(i);
                if (expanded && visibleIndex < 0 || !expanded && visibleIndex >= 0) {
                    delta += expanded ? 1 : -1;
                }
                this.realToVisibleLine.set(i, expanded ? visibleFoldStartIndex + offset : -1);
                lastUpdatedIndex = i++;
            }
            for (i = lastUpdatedIndex + 1; i < this.realToVisibleLine.size(); ++i) {
                this.realToVisibleLine.set(i, this.realToVisibleLine.get(i) + delta);
            }
            this.hiddenLines -= delta;
            this.updateVisibleToRealLines(foldStartIndex);
            this.foldVisibilityUpdated();
        }
    }

    private void foldVisibilityUpdated() {
        if (this.knownCharsPerLine > 0) {
            this.calcLogicalLineCount(this.knownCharsPerLine);
        }
        this.markDirty();
        this.delayedFire();
    }

    private int updateRealToVisibleIndexesInFold(int foldStart, int len, boolean expanded) {
        int changed = 0;
        int nestedHidden = -1;
        for (int i = 0; i < len; ++i) {
            int lineIndex = foldStart + i + 1;
            int foldOffset = this.foldOffsets.get(lineIndex);
            if (i > 0 && foldOffset == 1 && this.visibleList.get(lineIndex - 1) == 0 && nestedHidden == -1) {
                nestedHidden = lineIndex - 1;
                continue;
            }
            if (foldOffset <= i + 1 && (nestedHidden == -1 || foldOffset > lineIndex - nestedHidden)) {
                assert (!expanded || this.realToVisibleLine.get(lineIndex) == -1);
                assert (expanded || this.realToVisibleLine.get(lineIndex) != -1);
                nestedHidden = -1;
                this.realToVisibleLine.set(lineIndex, expanded ? this.realToVisibleLine.get(foldStart) + ++changed : -1);
                continue;
            }
            if (foldOffset <= len + 1 && nestedHidden != -1) {
                assert (foldOffset <= lineIndex - nestedHidden);
                continue;
            }
            assert (false);
        }
        return changed;
    }

    private void updateVisibleToRealLines(int fromRealIndex) {
        for (int i = fromRealIndex; i < this.getLineCount() - 1; ++i) {
            int visibleLine = this.realToVisibleLine.get(i);
            if (visibleLine == -1) continue;
            if (visibleLine >= this.visibleToRealLine.size()) {
                this.visibleToRealLine.add(i);
                continue;
            }
            this.visibleToRealLine.set(visibleLine, i);
        }
        if (this.visibleToRealLine.size() > this.realToVisibleLine.size() - this.hiddenLines) {
            this.visibleToRealLine.shorten(this.realToVisibleLine.size() - this.hiddenLines);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int visibleToRealLine(int visibleLineIndex) {
        Object object = this.readLock();
        synchronized (object) {
            if (visibleLineIndex >= this.visibleToRealLine.size()) {
                return visibleLineIndex + this.hiddenLines;
            }
            if (visibleLineIndex < 0) {
                return visibleLineIndex;
            }
            return this.visibleToRealLine.get(visibleLineIndex);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int realToVisibleLine(int realLineIndex) {
        Object object = this.readLock();
        synchronized (object) {
            if (realLineIndex >= this.realToVisibleLine.size()) {
                return realLineIndex - this.hiddenLines;
            }
            return this.realToVisibleLine.get(realLineIndex);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean isVisible(int lineIndex) {
        Object object = this.readLock();
        synchronized (object) {
            if (lineIndex >= this.foldOffsets.size()) {
                return true;
            }
            int fo = this.foldOffsets.get(lineIndex);
            if (fo == 0) {
                return true;
            }
            for (int parentFold = lineIndex - this.foldOffsets.get((int)lineIndex); parentFold >= 0; parentFold -= fo) {
                if (this.visibleList.get(parentFold) == 0) {
                    return false;
                }
                fo = this.foldOffsets.get(parentFold);
                if (fo == 0) break;
            }
            return true;
        }
    }

    int foldLength(int foldStart) {
        int i;
        for (i = foldStart + 1; i < this.foldOffsets.size() && this.foldOffsets.get(i) > 0 && this.foldOffsets.get(i) <= i - foldStart; ++i) {
        }
        return i - foldStart - 1;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int getVisibleLineCount() {
        Object object = this.readLock();
        synchronized (object) {
            return this.getLineCount() - this.hiddenLines;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int getFoldStart(int realLineIndex) {
        Object object = this.readLock();
        synchronized (object) {
            if (realLineIndex + 1 < this.foldOffsets.size() && this.foldOffsets.get(realLineIndex + 1) == 1) {
                return realLineIndex;
            }
            if (realLineIndex < 0 || realLineIndex >= this.foldOffsets.size()) {
                return Math.max(0, realLineIndex);
            }
            return realLineIndex - this.foldOffsets.get(realLineIndex);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int getParentFoldStart(int realLineIndex) {
        Object object = this.readLock();
        synchronized (object) {
            if (realLineIndex < 0 || realLineIndex >= this.foldOffsets.size()) {
                return -1;
            }
            int offset = this.foldOffsets.get(realLineIndex);
            if (offset > 0) {
                // MONITOREXIT [0, 1, 3] lbl9 : MonitorExitStatement: MONITOREXIT : var2_2
                int result = realLineIndex - offset;
                return result >= 0 ? result : -1;
            }
            return -1;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Pair<Integer, Integer> removeCharsFromLastLine(int length) {
        Object object = this.readLock();
        synchronized (object) {
            String lastLine;
            if (this.lastLineFinished) {
                return Pair.of((Object)0, (Object)0);
            }
            try {
                lastLine = this.getLine(this.getLineCount() - 1);
            }
            catch (IOException e) {
                LOG.log(Level.INFO, null, e);
                return Pair.of((Object)0, (Object)0);
            }
            if (lastLine.isEmpty()) {
                return Pair.of((Object)0, (Object)0);
            }
            int removed = length < 0 ? lastLine.length() : Math.max(length, lastLine.length());
            int tabs = 0;
            for (int i = 0; i < removed; ++i) {
                if (lastLine.charAt(lastLine.length() - 1 - i) != '\t') continue;
                tabs += this.removeLastTab();
            }
            this.lastLineLength -= removed;
            return Pair.of((Object)removed, (Object)tabs);
        }
    }

    private class CharBufferResource
    implements BufferResource<CharBuffer> {
        private BufferResource<ByteBuffer> parentResource;
        private CharBuffer cb;

        public CharBufferResource(BufferResource<ByteBuffer> parentResource) {
            this.parentResource = parentResource;
            this.cb = parentResource.getBuffer().asCharBuffer();
        }

        @Override
        public CharBuffer getBuffer() {
            return this.cb;
        }

        @Override
        public void releaseBuffer() {
            this.cb = null;
            this.parentResource.releaseBuffer();
        }
    }

}

