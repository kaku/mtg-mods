/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.output2;

import java.util.Arrays;

final class SparseIntList {
    private int[] keys;
    private int[] values;
    private int used = 0;
    private int lastAdded = Integer.MIN_VALUE;
    private int lastIndex = Integer.MIN_VALUE;
    private int lastGetIndex = -1;
    private int lastGetResult;
    private int lastGetNextKeyValue = -1;
    private int lastGetNextKeyResult;

    SparseIntList(int capacity) {
        this.allocArrays(capacity);
    }

    public synchronized void add(int idx, int value) {
        if (value < this.lastAdded) {
            throw new IllegalArgumentException("Contents must be presorted - added value " + value + " is less than preceding " + "value " + this.lastAdded);
        }
        if (idx <= this.lastIndex) {
            throw new IllegalArgumentException("Contents must be presorted - added index " + idx + " is less than preceding " + "index " + this.lastIndex);
        }
        if (this.used >= this.keys.length) {
            this.growArrays();
        }
        this.values[this.used] = value;
        this.keys[this.used++] = idx;
        this.lastAdded = value;
        this.lastIndex = idx;
        this.lastGetResult = -1;
        this.lastGetIndex = -1;
        this.lastGetNextKeyResult = -1;
        this.lastGetNextKeyValue = -1;
    }

    public synchronized void updateLast(int idx, int value) {
        if (this.lastIndex != idx) {
            throw new IllegalArgumentException("Last index: " + this.lastIndex + " idx: " + idx);
        }
        this.values[this.used - 1] = value;
        this.lastAdded = value;
        this.lastGetResult = -1;
        this.lastGetIndex = -1;
        this.lastGetNextKeyResult = -1;
        this.lastGetNextKeyValue = -1;
    }

    public synchronized void removeLast() {
        if (this.used < 1) {
            throw new IllegalStateException("Cannot remove last, list is empty");
        }
        --this.used;
        if (this.used > 0) {
            this.lastAdded = this.values[this.used - 1];
            this.lastIndex = this.keys[this.used - 1];
        } else {
            this.lastIndex = Integer.MIN_VALUE;
            this.lastAdded = Integer.MIN_VALUE;
        }
        this.lastGetResult = -1;
        this.lastGetIndex = -1;
        this.lastGetNextKeyResult = -1;
        this.lastGetNextKeyValue = -1;
    }

    int lastAdded() {
        return this.lastAdded;
    }

    int lastIndex() {
        return this.lastIndex;
    }

    private void allocArrays(int size) {
        this.keys = new int[size];
        this.values = new int[size];
        Arrays.fill(this.keys, Integer.MAX_VALUE);
        Arrays.fill(this.values, Integer.MAX_VALUE);
    }

    public synchronized int get(int index) {
        if (index < 0) {
            return 0;
        }
        if (this.used == 0 || this.used > 0 && index < this.keys[0]) {
            return index + 1;
        }
        if (index == this.lastGetIndex) {
            return this.lastGetResult;
        }
        this.lastGetIndex = index;
        int pos = Arrays.binarySearch(this.keys, index);
        if (pos >= 0) {
            this.lastGetResult = this.values[pos];
            return this.lastGetResult;
        }
        pos = - pos - 2;
        this.lastGetResult = this.values[pos] + index - this.keys[pos];
        return this.lastGetResult;
    }

    public synchronized int getNextKey(int val) {
        if (val < 0) {
            return 0;
        }
        if (this.used == 0) {
            return val;
        }
        if (this.used > 0 && val < this.values[0]) {
            return val < this.keys[0] ? val : this.keys[0];
        }
        if (val == this.lastGetNextKeyValue) {
            return this.lastGetNextKeyResult;
        }
        this.lastGetNextKeyValue = val;
        int pos = Arrays.binarySearch(this.values, val);
        if (pos < 0) {
            pos = - pos - 1;
            int key = val - this.values[pos - 1] + this.keys[pos - 1] + 1;
            this.lastGetNextKeyResult = pos < this.used ? Math.min(key, this.keys[pos]) : key;
            return this.lastGetNextKeyResult;
        }
        while (pos >= 0 && pos + 1 < this.values.length && this.values[pos] == this.values[pos + 1]) {
            ++pos;
        }
        this.lastGetNextKeyResult = this.keys[pos] + 1;
        return this.lastGetNextKeyResult;
    }

    public synchronized int getKey(int val) {
        if (val < 0) {
            return 0;
        }
        if (this.used == 0) {
            return val - 1;
        }
        if (this.used > 0 && val < this.values[0]) {
            return val < this.keys[0] ? val - 1 : this.keys[0];
        }
        int pos = Arrays.binarySearch(this.values, val);
        if (pos < 0) {
            pos = - pos - 1;
            int key = val - this.values[pos - 1] + this.keys[pos - 1];
            return pos < this.used ? Math.min(key, this.keys[pos]) : key;
        }
        return this.keys[pos];
    }

    private void growArrays() {
        int[] oldkeys = this.keys;
        int[] oldvals = this.values;
        this.allocArrays(Math.round((float)this.keys.length * 1.5f));
        System.arraycopy(oldkeys, 0, this.keys, 0, oldkeys.length);
        System.arraycopy(oldvals, 0, this.values, 0, oldvals.length);
    }

    public String toString() {
        StringBuffer result = new StringBuffer("SparseIntList [");
        result.append("used=");
        result.append(this.used);
        result.append(" capacity=");
        result.append(this.keys.length);
        result.append(" keyValuePairs:");
        for (int i = 0; i < this.used; ++i) {
            result.append(this.keys[i]);
            result.append(':');
            result.append(this.values[i]);
            if (i == this.used - 1) continue;
            result.append(',');
        }
        result.append(']');
        return result.toString();
    }
}

