/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.output2;

public class OutputLimits {
    private final int maxLines;
    private final int maxChars;
    private final int removeLines;
    private static final OutputLimits DEFAULT = new OutputLimits();

    public OutputLimits(int maxLines, int maxChars, int removeLines) {
        this.maxLines = maxLines;
        this.maxChars = maxChars;
        this.removeLines = removeLines;
    }

    private OutputLimits() {
        this(4194304, 1073741823, 2097152);
    }

    public int getMaxLines() {
        return this.maxLines;
    }

    public int getMaxChars() {
        return this.maxChars;
    }

    public int getRemoveLines() {
        return this.removeLines;
    }

    public static OutputLimits getDefault() {
        return DEFAULT;
    }
}

