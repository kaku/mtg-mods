/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Parameters
 */
package org.netbeans.core.output2;

import java.lang.ref.WeakReference;
import java.util.Arrays;
import org.netbeans.core.output2.NbIO;
import org.openide.util.Parameters;

class PairMap {
    private String[] keys = new String[10];
    Object[] vals = new Object[10];
    int last = -1;
    private boolean weak = false;
    private boolean pruning = false;

    PairMap() {
    }

    public synchronized void clear() {
        this.keys = new String[10];
        this.vals = new Object[10];
        this.last = -1;
    }

    public synchronized int size() {
        if (this.weak) {
            return this.prune();
        }
        return this.last + 1;
    }

    public synchronized boolean isEmpty() {
        return this.size() == 0;
    }

    public synchronized void setWeak(boolean val) {
        if (this.weak != val) {
            this.weak = val;
            for (int i = 0; i <= this.last; ++i) {
                this.vals[i] = this.weak ? new WeakReference<Object>(this.vals[i]) : ((WeakReference)this.vals[i]).get();
            }
            if (!this.weak) {
                this.prune();
            }
        }
    }

    public synchronized void add(String key, NbIO value) {
        Parameters.notNull((CharSequence)"key", (Object)key);
        if (this.last == this.keys.length - 1) {
            this.growArrays();
        }
        ++this.last;
        this.keys[this.last] = key;
        this.vals[this.last] = value;
    }

    public synchronized NbIO get(String key, boolean mustBeClosed) {
        if (this.last < 0) {
            return null;
        }
        boolean foundNull = false;
        for (int i = this.last; i >= 0; --i) {
            if (!this.keys[i].equals(key)) continue;
            NbIO io = this.getValue(i);
            foundNull |= io == null;
            if (io == null || mustBeClosed && (!mustBeClosed || !io.isStreamClosed())) continue;
            return io;
        }
        if (foundNull) {
            this.prune();
        }
        return null;
    }

    public boolean containsValue(NbIO io) {
        if (this.last < 0) {
            return false;
        }
        for (int i = this.last; i >= 0; --i) {
            if (this.getValue(i) != io) continue;
            return true;
        }
        return false;
    }

    public synchronized NbIO get(String key) {
        return this.get(key, false);
    }

    public synchronized String remove(NbIO io) {
        int idx = this.indexOfVal(io);
        if (idx == -1) {
            return null;
        }
        String result = this.keys[idx];
        this.removeIndex(idx);
        return result;
    }

    public synchronized NbIO remove(String key) {
        int idx = this.indexOfKey(key);
        if (idx == -1) {
            return null;
        }
        NbIO result = this.getValue(idx);
        this.removeIndex(idx);
        return result;
    }

    private NbIO getValue(int idx) {
        NbIO result;
        if (idx > this.last) {
            throw new ArrayIndexOutOfBoundsException("Tried to fetch item " + idx + " but map only contains " + (this.last + 1) + " elements");
        }
        Object o = this.vals[idx];
        if (this.weak) {
            result = (NbIO)((WeakReference)this.vals[idx]).get();
            if (result == null && !this.pruning) {
                this.removeIndex(idx);
            }
        } else {
            result = (NbIO)o;
        }
        return result;
    }

    public void setValue(int idx, NbIO value) {
        this.vals[idx] = this.weak ? new WeakReference<NbIO>((NbIO)value) : value;
    }

    private void removeIndex(int idx) {
        if (idx < 0 || idx > this.last) {
            throw new ArrayIndexOutOfBoundsException("Trying to remove element " + idx + " but map only contains " + (this.last + 1) + " elements");
        }
        if (idx == this.last) {
            this.keys[idx] = null;
            this.vals[idx] = null;
        } else {
            this.keys[idx] = this.keys[this.last];
            this.vals[idx] = this.vals[this.last];
            this.vals[this.last] = null;
            this.keys[this.last] = null;
        }
        --this.last;
    }

    private int indexOfKey(String key) {
        for (int i = this.last; i >= 0; --i) {
            if (!this.keys[i].equals(key)) continue;
            return i;
        }
        return -1;
    }

    private int indexOfVal(NbIO val) {
        for (int i = this.last; i >= 0; --i) {
            if (this.vals[i] != val) continue;
            return i;
        }
        return -1;
    }

    private void growArrays() {
        String[] newKeys = new String[this.keys.length * 2];
        NbIO[] newVals = new NbIO[this.vals.length * 2];
        System.arraycopy(this.keys, 0, newKeys, 0, this.last + 1);
        System.arraycopy(this.vals, 0, newVals, 0, this.last + 1);
        this.keys = newKeys;
        this.vals = newVals;
    }

    private int prune() {
        int i;
        int oldSize;
        this.pruning = true;
        int result = oldSize = this.last + 1;
        int[] removes = new int[oldSize];
        Arrays.fill(removes, -1);
        for (i = this.last; i >= 0; --i) {
            if (this.getValue(i) != null) continue;
            removes[i] = i;
            --result;
        }
        if (result != oldSize) {
            for (i = removes.length - 1; i >= 0; --i) {
                if (removes[i] == -1) continue;
                this.removeIndex(removes[i]);
            }
        }
        this.pruning = false;
        return result;
    }
}

