/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.windows.OutputListener
 */
package org.netbeans.core.output2;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Toolkit;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Highlighter;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.Segment;
import javax.swing.text.TabExpander;
import javax.swing.text.Utilities;
import javax.swing.text.View;
import org.netbeans.core.output2.ExtPlainView;
import org.netbeans.core.output2.LineInfo;
import org.netbeans.core.output2.Lines;
import org.netbeans.core.output2.OutputDocument;
import org.openide.util.Exceptions;
import org.openide.windows.OutputListener;

public class WrappedTextView
extends View
implements TabExpander {
    static final int TAB_SIZE = 8;
    private JTextComponent comp;
    private int charsPerLine = 80;
    private int fontDescent = 4;
    private static final Segment SEGMENT = new Segment();
    private int width = 0;
    private boolean changed = true;
    private int charWidth = 12;
    private int charHeight = 7;
    static final int[] ln = new int[3];
    private boolean aa = false;
    private static final boolean antialias = Boolean.getBoolean("swing.aatext") || "Aqua".equals(UIManager.getLookAndFeel().getID());
    static final Color arrowColor = new Color(80, 162, 80);
    private static Map<RenderingHints.Key, Object> hintsMap = null;
    int tabSize;
    int tabBase;
    private int tabOffsetX = 0;
    private final PropertyChangeListener propertyChangeListener;
    float viewWidth = -1.0f;

    static Map<RenderingHints.Key, Object> getHints() {
        if (hintsMap == null && (WrappedTextView.hintsMap = (Map)Toolkit.getDefaultToolkit().getDesktopProperty("awt.font.desktophints")) == null) {
            hintsMap = new HashMap<RenderingHints.Key, Object>();
            if (antialias) {
                hintsMap.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                hintsMap.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            }
        }
        return hintsMap;
    }

    public WrappedTextView(Element elem, JTextComponent comp, PropertyChangeListener propertyChangeListener1) {
        super(elem);
        this.comp = comp;
        this.propertyChangeListener = propertyChangeListener1;
    }

    @Override
    public float getPreferredSpan(int axis) {
        OutputDocument doc = this.odoc();
        float result = 0.0f;
        if (doc != null) {
            this.updateWidth();
            switch (axis) {
                case 0: {
                    result = this.charsPerLine;
                    break;
                }
                case 1: {
                    result = doc.getLines().getLogicalLineCountIfWrappedAt(this.charsPerLine) * this.charHeight + this.fontDescent;
                    break;
                }
                default: {
                    throw new IllegalArgumentException(Integer.toString(axis));
                }
            }
        }
        return result;
    }

    @Override
    public float getMinimumSpan(int axis) {
        return this.getPreferredSpan(axis);
    }

    @Override
    public float getMaximumSpan(int axis) {
        return this.getPreferredSpan(axis);
    }

    @Override
    public void setSize(float width, float height) {
        super.setSize(width, height);
        if (this.viewWidth != width) {
            this.viewWidth = width;
            this.updateMetrics();
        }
    }

    private int getTabSize() {
        return 8;
    }

    @Override
    public float nextTabStop(float x, int tabOffset) {
        if (this.tabSize == 0) {
            return x;
        }
        int ntabs = ((int)x - WrappedTextView.margin() + this.tabOffsetX) / this.tabSize;
        return WrappedTextView.margin() + (ntabs + 1) * this.tabSize - this.tabOffsetX;
    }

    void updateMetrics() {
        Font font = this.comp.getFont();
        FontMetrics fm = this.comp.getFontMetrics(font);
        this.charWidth = fm.charWidth('m');
        this.charHeight = fm.getHeight();
        this.fontDescent = fm.getMaxDescent();
        Graphics2D g2d = (Graphics2D)this.comp.getGraphics();
        if (g2d != null) {
            this.aa = g2d.getRenderingHint(RenderingHints.KEY_ANTIALIASING) == RenderingHints.VALUE_ANTIALIAS_ON;
        }
        this.tabSize = this.getTabSize() * this.charWidth;
        this.updateWidth();
    }

    private OutputDocument odoc() {
        Document doc = this.comp.getDocument();
        if (doc instanceof OutputDocument) {
            return (OutputDocument)doc;
        }
        return null;
    }

    private void updateWidth() {
        int oldCharPerWidth = this.charsPerLine;
        if (this.comp.getParent() instanceof JViewport) {
            JViewport jv = (JViewport)this.comp.getParent();
            this.width = jv.getExtentSize().width - (this.aa ? 18 : 17);
        } else {
            this.width = this.comp.getWidth() - (this.aa ? 18 : 17);
        }
        if (this.width < 0) {
            this.width = 0;
        }
        this.charsPerLine = this.width / this.charWidth;
        if (this.charsPerLine != oldCharPerWidth) {
            this.propertyChangeListener.propertyChange(new PropertyChangeEvent(this, "charsPerLine", oldCharPerWidth, this.charsPerLine));
        }
    }

    private static int margin() {
        return 9;
    }

    @Override
    public void paint(Graphics g, Shape allocation) {
        ((Graphics2D)g).addRenderingHints(WrappedTextView.getHints());
        this.comp.getHighlighter().paint(g);
        this.tabBase = ((Rectangle)allocation).x + WrappedTextView.margin();
        OutputDocument doc = this.odoc();
        if (doc != null) {
            Rectangle clip = g.getClipBounds();
            clip.y = Math.max(0, clip.y - this.charHeight);
            clip.height += this.charHeight * 2;
            int lineCount = doc.getElementCount();
            if (lineCount == 0) {
                return;
            }
            WrappedTextView.ln[0] = clip.y / this.charHeight;
            Lines lines = doc.getLines();
            lines.toPhysicalLineIndex(ln, this.charsPerLine);
            int firstline = ln[0];
            g.setColor(this.comp.getForeground());
            Segment seg = SwingUtilities.isEventDispatchThread() ? SEGMENT : new Segment();
            int selStart = this.comp.getSelectionStart();
            int selEnd = this.comp.getSelectionEnd();
            int y = clip.y - clip.y % this.charHeight + this.charHeight;
            int maxVisibleChars = (clip.height + this.charHeight - 1) / this.charHeight * this.charsPerLine;
            try {
                for (int i = firstline; i < lines.getLineCount(); ++i) {
                    int logLineOffset;
                    if (y > clip.y + clip.height) {
                        return;
                    }
                    int visibleLine = lines.realToVisibleLine(i);
                    if (visibleLine < 0) continue;
                    int lineStart = doc.getLineStart(i);
                    int lineEnd = doc.getLineEnd(i);
                    int length = lineEnd - lineStart;
                    if (length == 0) {
                        y += this.charHeight;
                        continue;
                    }
                    length = lines.lengthWithTabs(i);
                    LineInfo info = lines.getLineInfo(i);
                    int logicalLines = length <= this.charsPerLine ? 1 : (this.charsPerLine == 0 ? length : (length + this.charsPerLine - 1) / this.charsPerLine);
                    int currLogicalLine = i == firstline && logicalLines > 0 && ln[1] > 0 ? ln[1] : 0;
                    int charpos = 0;
                    int charsWithTabs = 0;
                    int arrowDrawn = currLogicalLine - 1;
                    int x = 0;
                    int remainCharsOnLogicalLine = this.charsPerLine;
                    if (currLogicalLine > 0) {
                        logLineOffset = currLogicalLine * this.charsPerLine;
                        int[] tabShiftPtr = new int[]{0};
                        logLineOffset = lines.getNumPhysicalChars(lineStart, logLineOffset, tabShiftPtr);
                        lineStart += logLineOffset;
                        if (tabShiftPtr[0] > 0) {
                            remainCharsOnLogicalLine -= tabShiftPtr[0];
                            x = tabShiftPtr[0] * this.charWidth;
                            charsWithTabs += tabShiftPtr[0];
                        }
                    } else {
                        logLineOffset = 0;
                    }
                    length = Math.min(maxVisibleChars, length - logLineOffset);
                    int sourceLength = Math.min(maxVisibleChars, lineEnd - lineStart);
                    doc.getText(lineStart, sourceLength, seg);
                    this.tabOffsetX = this.charWidth * currLogicalLine * this.charsPerLine;
                    block3 : for (LineInfo.Segment ls : info.getLineSegments()) {
                        if (ls.getEnd() < logLineOffset) continue;
                        g.setColor(ls.getColor());
                        int shift = 0;
                        while (charpos < ls.getEnd() - logLineOffset && currLogicalLine < logicalLines) {
                            int lenToDraw;
                            int charsToDraw = lenToDraw = Math.min(remainCharsOnLogicalLine, ls.getEnd() - logLineOffset - charpos);
                            if (lenToDraw > 0) {
                                charsToDraw = this.getCharsForLengthWithTabs(seg.array, charpos, currLogicalLine * this.charsPerLine + shift, lenToDraw, remainCharsOnLogicalLine);
                                if (currLogicalLine != logicalLines - 1 && arrowDrawn != currLogicalLine) {
                                    arrowDrawn = currLogicalLine;
                                    this.drawArrow(g, y, currLogicalLine == logicalLines - 2);
                                }
                                Color bg = ls.getCustomBackground();
                                this.drawText(seg, g, x, y, lineStart, charpos, selStart, charsToDraw, selEnd, bg);
                                if (ls.getListener() != null) {
                                    this.underline(g, seg, charpos, charsToDraw, x, y);
                                }
                            }
                            lenToDraw = this.getCharLengthWithTabs(seg.array, charpos, currLogicalLine * this.charsPerLine + shift, charsToDraw);
                            charpos += charsToDraw;
                            if (charsToDraw == 0) continue block3;
                            charsWithTabs += lenToDraw;
                            remainCharsOnLogicalLine -= lenToDraw;
                            x += lenToDraw * this.charWidth;
                            shift += lenToDraw;
                            while (remainCharsOnLogicalLine <= 0) {
                                int a;
                                int b;
                                int realPos;
                                shift = - remainCharsOnLogicalLine;
                                remainCharsOnLogicalLine += this.charsPerLine;
                                ++currLogicalLine;
                                x = shift * this.charWidth;
                                this.tabOffsetX += this.charWidth * this.charsPerLine;
                                if ((y += this.charHeight) > clip.y + clip.height) {
                                    return;
                                }
                                if (shift <= 0 || selStart == selEnd || (a = Math.max(selStart, realPos = lineStart + charpos)) >= (b = Math.min(selEnd, realPos + charsToDraw))) continue;
                                this.drawSelection(g, 0, x, y);
                            }
                        }
                    }
                    if (this.charsPerLine != 0 && charsWithTabs % this.charsPerLine == 0) continue;
                    y += this.charHeight;
                }
                this.tabOffsetX = 0;
            }
            catch (BadLocationException e) {
                Exceptions.printStackTrace((Throwable)e);
            }
        }
    }

    private void drawText(Segment seg, Graphics g, int x, int y, int lineStart, int charpos, int selStart, int lenToDraw, int selEnd, Color bg) {
        int a;
        int realPos;
        int b;
        Color clr = g.getColor();
        if (selStart != selEnd && (a = Math.max(selStart, realPos = lineStart + charpos)) < (b = Math.min(selEnd, realPos + lenToDraw))) {
            realPos = this.odoc().getLines().getNumLogicalChars(lineStart, realPos - lineStart) + lineStart;
            a = this.odoc().getLines().getNumLogicalChars(lineStart, a - lineStart) + lineStart;
            b = this.odoc().getLines().getNumLogicalChars(lineStart, b - lineStart) + lineStart;
            int start = x + WrappedTextView.margin() + (a - realPos) * this.charWidth;
            int len = (b - a) * this.charWidth;
            int w = this.charsPerLine * this.charWidth;
            if (start - WrappedTextView.margin() + len > w) {
                len = w - start + WrappedTextView.margin();
            }
            g.setColor(this.comp.getSelectionColor());
            g.fillRect(start, y + this.fontDescent - this.charHeight, len, this.charHeight);
            g.setColor(clr);
        }
        int count = seg.count;
        int offset = seg.offset;
        seg.count = lenToDraw;
        seg.offset = charpos;
        this.drawTextBackground(g, clr, bg, selStart != selEnd, seg, x, y, charpos);
        Utilities.drawTabbedText(seg, WrappedTextView.margin() + x, y, g, this, charpos);
        seg.count = count;
        seg.offset = offset;
    }

    private void drawTextBackground(Graphics g, Color fg, Color bg, boolean selection, Segment seg, int x, int y, int charpos) {
        if (bg != null && !selection) {
            int w = Utilities.getTabbedTextWidth(seg, g.getFontMetrics(), x, this, charpos);
            int h = g.getFontMetrics().getHeight();
            g.setColor(bg);
            g.fillRect(x + WrappedTextView.margin(), y - h + g.getFontMetrics().getDescent(), w, h);
        }
        g.setColor(fg);
    }

    private void drawSelection(Graphics g, int x1, int x2, int y) {
        Color c = g.getColor();
        g.setColor(this.comp.getSelectionColor());
        g.fillRect(x1 + WrappedTextView.margin(), y + this.fontDescent - this.charHeight, x2 - x1, this.charHeight);
        g.setColor(c);
    }

    private void underline(Graphics g, Segment seg, int charpos, int lenToDraw, int x, int y) {
        if (!ExtPlainView.isLinkUndeliningEnabled(this)) {
            return;
        }
        int underlineStart = WrappedTextView.margin() + x;
        FontMetrics fm = g.getFontMetrics();
        int underlineEnd = underlineStart + fm.charsWidth(seg.array, charpos, lenToDraw);
        int underlineShift = fm.getDescent() - 1;
        g.drawLine(underlineStart, y + underlineShift, underlineEnd, y + underlineShift);
    }

    private void drawArrow(Graphics g, int y, boolean drawHead) {
        int rpos;
        Color c = g.getColor();
        g.setColor(WrappedTextView.arrowColor());
        int w = this.width + 15;
        y += 2;
        int n = rpos = this.aa ? 8 : 4;
        if (this.aa) {
            g.drawArc(w - rpos, y - this.charHeight / 2, rpos + 1, this.charHeight, 265, 185);
            ++w;
        } else {
            g.drawLine(w - rpos, y - this.charHeight / 2, w, y - this.charHeight / 2);
            g.drawLine(w, y - this.charHeight / 2 + 1, w, y + this.charHeight / 2 - 1);
            g.drawLine(w - rpos, y + this.charHeight / 2, w, y + this.charHeight / 2);
        }
        if (drawHead) {
            rpos = this.aa ? 7 : 8;
            int[] xpoints = new int[]{w - rpos, w - rpos + 5, w - rpos + 5};
            int[] ypoints = new int[]{y + this.charHeight / 2, y + this.charHeight / 2 - 5, y + this.charHeight / 2 + 5};
            g.fillPolygon(xpoints, ypoints, 3);
        }
        g.setColor(WrappedTextView.arrowColor());
        g.drawLine(1, y - this.charHeight / 2, 5, y - this.charHeight / 2);
        g.drawLine(1, y - this.charHeight / 2, 1, y + this.charHeight / 2);
        g.drawLine(1, y + this.charHeight / 2, 5, y + this.charHeight / 2);
        g.setColor(c);
    }

    private static Color arrowColor() {
        return arrowColor;
    }

    @Override
    public Shape modelToView(int pos, Shape a, Position.Bias b) throws BadLocationException {
        Rectangle result = new Rectangle();
        result.setBounds(0, 0, this.charWidth, this.charHeight);
        OutputDocument od = this.odoc();
        if (od != null) {
            int line = Math.max(0, od.getLines().getLineAt(pos));
            int start = od.getLineStart(line);
            int column = pos - start;
            column = od.getLines().getNumLogicalChars(start, column);
            int row = od.getLines().getLogicalLineCountAbove(line, this.charsPerLine);
            int end = this.getLineEnd(line, od.getLines());
            int len = od.getLines().getNumLogicalChars(start, end - start);
            if (column >= this.charsPerLine && this.charsPerLine != 0) {
                row += column % this.charsPerLine == 0 && column == len ? column / this.charsPerLine - 1 : column / this.charsPerLine;
                column = column % this.charsPerLine == 0 && column == len ? this.charsPerLine : column % this.charsPerLine;
            }
            result.y = row * this.charHeight + this.fontDescent;
            result.x = WrappedTextView.margin() + column * this.charWidth;
        }
        return result;
    }

    @Override
    public int viewToModel(float x, float y, Shape a, Position.Bias[] biasReturn) {
        OutputDocument od = this.odoc();
        if (od != null) {
            int ix = Math.max((int)x - WrappedTextView.margin(), 0);
            int iy = (int)y - this.fontDescent;
            WrappedTextView.ln[0] = iy / this.charHeight;
            od.getLines().toPhysicalLineIndex(ln, this.charsPerLine);
            int logicalLine = ln[0];
            int wraps = ln[2] - 1;
            int totalLines = od.getLines().getLineCount();
            if (totalLines == 0) {
                return 0;
            }
            if (logicalLine >= totalLines) {
                return od.getLength();
            }
            int lineStart = od.getLineStart(logicalLine);
            int lineLength = od.getLines().lengthWithTabs(logicalLine);
            int lineEnd = lineStart + lineLength;
            int column = ix / this.charWidth;
            if (column > lineLength) {
                column = lineLength;
            }
            int result = wraps > 0 ? Math.min(lineEnd, lineStart + ln[1] * this.charsPerLine + column) : Math.min(lineStart + column, lineEnd);
            Lines lines = od.getLines();
            result = lines.getNumPhysicalChars(lineStart, result - lineStart, null) + lineStart;
            result = Math.min(od.getLength(), result);
            return result;
        }
        return 0;
    }

    private int getCharLengthWithTabs(char[] array, int charpos, int tabLineOffset, int lenToDraw) {
        int n = Math.min(array.length, charpos + lenToDraw);
        int tabExpand = 0;
        for (int i = charpos; i < n; ++i) {
            if ('\t' != array[i]) continue;
            int numSpaces = 8 - (i - charpos + tabLineOffset + tabExpand) % 8;
            tabExpand += numSpaces - 1;
            lenToDraw += numSpaces - 1;
        }
        return lenToDraw;
    }

    private int getCharsForLengthWithTabs(char[] array, int charpos, int tabLineOffset, int lenToDraw, int length) {
        int i;
        int n = Math.min(array.length, charpos + lenToDraw);
        int lengthWithTab = 0;
        int tabExpand = 0;
        for (i = charpos; i < n && lengthWithTab < length; ++i) {
            if ('\t' == array[i]) {
                int numSpaces = 8 - (i - charpos + tabLineOffset + tabExpand) % 8;
                tabExpand += numSpaces - 1;
                lengthWithTab += numSpaces;
                continue;
            }
            ++lengthWithTab;
        }
        if (lengthWithTab > length && i > charpos + 1 && array[i - 1] != '\t') {
            --i;
        }
        return i - charpos;
    }

    @Override
    public int getNextVisualPositionFrom(int pos, Position.Bias b, Shape a, int direction, Position.Bias[] biasRet) throws BadLocationException {
        Element elem = this.getElement();
        if (pos == -1) {
            pos = direction == 5 || direction == 3 ? this.getStartOffset() : this.getEndOffset() - 1;
        }
        Lines lines = this.odoc().getLines();
        switch (direction) {
            case 1: {
                PositionInfo pi = this.getPositionInfo(pos);
                if (pi.lineIndex <= 0 && pi.innerRowIndex <= 0) break;
                if (pi.innerRowIndex > 0) {
                    return this.jumpInLine(lines, pi, direction);
                }
                return this.jumpToLine(lines, pi, direction);
            }
            case 5: {
                PositionInfo pi = this.getPositionInfo(pos);
                if (pi.innerRowIndex < pi.innerRowsCount - 1) {
                    return this.jumpInLine(lines, pi, direction);
                }
                int visibleLineIndex = lines.realToVisibleLine(pi.lineIndex);
                if (visibleLineIndex >= elem.getElementCount() - 1) break;
                return this.jumpToLine(lines, pi, direction);
            }
            case 7: {
                int origLineIndex = lines.getLineAt(pos);
                pos = Math.max(0, pos - 1);
                int lineIndex = lines.getLineAt(pos);
                if (origLineIndex == lineIndex) break;
                int origVisibleLine = lines.realToVisibleLine(origLineIndex);
                pos = elem.getElement(origVisibleLine - 1).getEndOffset() - 1;
                break;
            }
            case 3: {
                int origLineIndex = lines.getLineAt(pos);
                pos = Math.min(pos + 1, elem.getEndOffset() - 1);
                int lineIndex = lines.getLineAt(pos);
                if (origLineIndex == lineIndex) break;
                int origVisibleLine = lines.realToVisibleLine(origLineIndex);
                pos = elem.getElement(origVisibleLine + 1).getStartOffset();
                break;
            }
            default: {
                throw new IllegalArgumentException("Bad direction");
            }
        }
        return pos;
    }

    private PositionInfo getLineInfo(int lineIndex) {
        Lines lines = this.odoc().getLines();
        int lineStart = lines.getLineStart(lineIndex);
        return this.getPositionInfo(lines, lineIndex, lineStart, lineStart);
    }

    private PositionInfo getPositionInfo(int offset) {
        Lines lines = this.odoc().getLines();
        int lineIndex = lines.getLineAt(offset);
        int lineStart = lines.getLineStart(lineIndex);
        return this.getPositionInfo(lines, lineIndex, lineStart, offset);
    }

    private PositionInfo getPositionInfo(Lines lines, int lineIndex, int lineStart, int offset) {
        PositionInfo pi = new PositionInfo();
        pi.offset = offset;
        pi.lineIndex = lineIndex;
        pi.lineStart = lineStart;
        pi.lineEnd = this.getLineEnd(pi.lineIndex, lines);
        int lineLen = pi.lineEnd - pi.lineStart;
        int column = offset - pi.lineStart;
        int logicalColumn = lines.getNumLogicalChars(pi.lineStart, column);
        pi.logicalLineLength = lines.getNumLogicalChars(pi.lineStart, lineLen);
        int innerRowsCount = pi.logicalLineLength / this.charsPerLine + (pi.logicalLineLength % this.charsPerLine > 0 ? 1 : 0);
        pi.innerRowsCount = Math.max(1, innerRowsCount);
        pi.innerRowIndex = logicalColumn / this.charsPerLine - (pi.logicalLineLength > 0 && logicalColumn == pi.logicalLineLength && logicalColumn % this.charsPerLine == 0 ? 1 : 0);
        pi.innerColumn = lineLen > 0 && pi.lineEnd == offset && pi.logicalLineLength % this.charsPerLine == 0 ? this.charsPerLine - (pi.innerRowsCount > 1 ? 1 : 0) : lines.getNumLogicalChars(pi.lineStart, offset - pi.lineStart) % this.charsPerLine;
        return pi;
    }

    private int jumpToLine(Lines lines, PositionInfo pi, int direction) {
        assert (direction == 1 || direction == 5);
        int newRealLine = this.findNearestVisibleLine(lines, pi.lineIndex, direction);
        if (newRealLine < 0) {
            return pi.offset;
        }
        PositionInfo targetLine = this.getLineInfo(newRealLine);
        int newInnerRow = direction == 1 ? targetLine.innerRowsCount - 1 : 0;
        int logicalLineStart = targetLine.lineStart + lines.getNumPhysicalChars(targetLine.lineStart, newInnerRow * this.charsPerLine, null);
        int physicalColumn = lines.getNumPhysicalChars(logicalLineStart, pi.innerColumn, null);
        int physicalPos = this.fixPhysicalPosition(lines, logicalLineStart + physicalColumn, newInnerRow, targetLine.lineStart);
        return Math.min(physicalPos, targetLine.lineEnd);
    }

    private int findNearestVisibleLine(Lines lines, int realLineIndex, int direction) {
        assert (direction == 5 || direction == 1);
        int inc = direction == 5 ? 1 : -1;
        int visibleLine = lines.realToVisibleLine(realLineIndex);
        if (visibleLine < 0) {
            for (int i = realLineIndex + inc; i >= 0 && i < lines.getLineCount(); i += inc) {
                if (lines.realToVisibleLine(i) < 0) continue;
                return i;
            }
            return -1;
        }
        return lines.visibleToRealLine(visibleLine + inc);
    }

    private int jumpInLine(Lines lines, PositionInfo pi, int direction) {
        assert (direction == 1 || direction == 5);
        assert (pi.innerRowIndex > 0 || direction == 5);
        assert (pi.innerRowIndex + 1 < pi.innerRowsCount || direction == 1);
        int newRow = pi.innerRowIndex + (direction == 5 ? 1 : -1);
        int newLogicalColumn = newRow * this.charsPerLine + pi.innerColumn;
        int newPos = pi.lineStart + lines.getNumPhysicalChars(pi.lineStart, newLogicalColumn, null);
        newPos = this.fixPhysicalPosition(lines, newPos, newRow, pi.lineStart);
        return Math.min(pi.lineEnd, Math.max(pi.lineStart, newPos));
    }

    private int fixPhysicalPosition(Lines lines, int pos, int newRow, int lineStart) {
        int newLogicalLineStart = newRow * this.charsPerLine;
        int computedLogicalColumn = lines.getNumLogicalChars(lineStart, pos - lineStart);
        if (computedLogicalColumn < newLogicalLineStart) {
            return pos + 1;
        }
        return pos;
    }

    private int getLineEnd(int realLineIndex, Lines lines) {
        return realLineIndex + 1 < lines.getLineCount() ? lines.getLineStart(realLineIndex + 1) - 1 : lines.getCharCount();
    }

    private static final class PositionInfo {
        public int offset;
        public int lineIndex;
        public int lineStart;
        public int lineEnd;
        public int logicalLineLength;
        public int innerRowsCount;
        public int innerRowIndex;
        public int innerColumn;

        private PositionInfo() {
        }
    }

}

