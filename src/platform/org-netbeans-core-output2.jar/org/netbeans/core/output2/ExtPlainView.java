/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.windows.OutputListener
 */
package org.netbeans.core.output2;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Toolkit;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.PlainView;
import javax.swing.text.Position;
import javax.swing.text.Segment;
import javax.swing.text.Utilities;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import org.netbeans.core.output2.LineInfo;
import org.netbeans.core.output2.Lines;
import org.netbeans.core.output2.NbIO;
import org.netbeans.core.output2.OutputDocument;
import org.netbeans.core.output2.OutputTab;
import org.netbeans.core.output2.options.OutputOptions;
import org.openide.util.NbBundle;
import org.openide.windows.OutputListener;

class ExtPlainView
extends PlainView {
    private final Segment SEGMENT = new Segment();
    private static final int MAX_LINE_LENGTH = 4096;
    private static final String LINE_TOO_LONG_MSG = NbBundle.getMessage(ExtPlainView.class, (String)"MSG_LINE_TOO_LONG");
    private static final boolean antialias = Boolean.getBoolean("swing.aatext") || "Aqua".equals(UIManager.getLookAndFeel().getID());
    private static Map<RenderingHints.Key, Object> hintsMap = null;
    Font font;
    int tabBase;
    int longestLineLength = -1;
    Element longestLine;

    static Map<RenderingHints.Key, Object> getHints() {
        if (hintsMap == null && (ExtPlainView.hintsMap = (Map)Toolkit.getDefaultToolkit().getDesktopProperty("awt.font.desktophints")) == null) {
            hintsMap = new HashMap<RenderingHints.Key, Object>();
            if (antialias) {
                hintsMap.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            }
        }
        return hintsMap;
    }

    ExtPlainView(Element elem) {
        super(elem);
    }

    @Override
    public void paint(Graphics g, Shape allocation) {
        ((Graphics2D)g).addRenderingHints(ExtPlainView.getHints());
        super.paint(g, allocation);
    }

    Segment getSegment() {
        return SwingUtilities.isEventDispatchThread() ? this.SEGMENT : new Segment();
    }

    private int drawText(Graphics g, int x, int y, int p0, int p1, boolean selected) throws BadLocationException {
        Document doc = this.getDocument();
        if (doc instanceof OutputDocument) {
            Segment s = this.getSegment();
            if (!this.getText(p0, p1 - p0, s)) {
                return x;
            }
            int end = p0 + s.count;
            Lines lines = ((OutputDocument)doc).getLines();
            int line = lines.getLineAt(p0);
            int lineOffset = lines.getLineStart(line);
            LineInfo info = lines.getLineInfo(line);
            for (LineInfo.Segment ls : info.getLineSegments()) {
                if (lineOffset + ls.getEnd() <= p0) continue;
                s.count = Math.min(lineOffset + ls.getEnd() - p0, end - p0);
                if (s.count == 0) {
                    return x;
                }
                Color bg = ls.getCustomBackground();
                if (bg != null && !selected) {
                    int w = Utilities.getTabbedTextWidth(s, this.metrics, x, this, p0);
                    int h = this.metrics.getHeight();
                    g.setColor(bg);
                    g.fillRect(x, y - h + this.metrics.getDescent(), w, h);
                }
                g.setColor(ls.getColor());
                int nx = Utilities.drawTabbedText(s, x, y, g, this, p0);
                if (ls.getListener() != null) {
                    this.underline(g, s, x, p0, y);
                }
                x = nx;
                p0 += s.count;
                s.offset += s.count;
            }
            return x;
        }
        return super.drawUnselectedText(g, x, y, p0, p1);
    }

    @Override
    protected int drawSelectedText(Graphics g, int x, int y, int p0, int p1) throws BadLocationException {
        return this.drawText(g, x, y, p0, p1, true);
    }

    @Override
    protected int drawUnselectedText(Graphics g, int x, int y, int p0, int p1) throws BadLocationException {
        return this.drawText(g, x, y, p0, p1, false);
    }

    boolean getText(int offset, int length, Segment txt) throws BadLocationException {
        Document doc = this.getDocument();
        Element elem = this.getElement();
        int lineIndex = elem.getElementIndex(offset);
        Element lineElem = elem.getElement(lineIndex);
        int lineStart = lineElem.getStartOffset();
        int lineLength = lineElem.getEndOffset() - 1 - lineStart;
        int newLen = Math.min(length, lineStart + 4096 - offset);
        if (newLen <= 0) {
            txt.count = 0;
            return false;
        }
        doc.getText(offset, newLen, txt);
        if (lineLength > 4096 && offset + length > lineStart + 4096 - LINE_TOO_LONG_MSG.length()) {
            int diff = offset - (lineStart + 4096 - LINE_TOO_LONG_MSG.length());
            int strPos = diff < 0 ? 0 : diff;
            int txtPos = diff < 0 ? - diff : 0;
            for (int i = strPos; i < LINE_TOO_LONG_MSG.length() && txtPos + i - strPos < txt.array.length; ++i) {
                txt.array[txtPos + i - strPos] = LINE_TOO_LONG_MSG.charAt(i);
            }
        }
        return true;
    }

    private void underline(Graphics g, Segment s, int x, int p0, int y) {
        if (!ExtPlainView.isLinkUndeliningEnabled(this)) {
            return;
        }
        int textLen = Utilities.getTabbedTextWidth(s, this.metrics, this.tabBase, this, p0);
        int underlineShift = g.getFontMetrics().getDescent() - 1;
        g.drawLine(x, y + underlineShift, x + textLen, y + underlineShift);
    }

    @Override
    public Shape modelToView(int pos, Shape a, Position.Bias b) throws BadLocationException {
        Element map = this.getElement();
        int lineIndex = map.getElementIndex(pos);
        Rectangle lineArea = this.lineToRect(a, lineIndex);
        this.tabBase = lineArea.x;
        Element line = map.getElement(lineIndex);
        int lineStart = line.getStartOffset();
        Segment s = this.getSegment();
        this.getText(lineStart, pos - lineStart, s);
        int xOffs = Utilities.getTabbedTextWidth(s, this.metrics, this.tabBase, this, lineStart);
        lineArea.x += xOffs;
        lineArea.width = 1;
        lineArea.height = this.metrics.getHeight();
        return lineArea;
    }

    @Override
    public int viewToModel(float fx, float fy, Shape a, Position.Bias[] bias) {
        Document doc = this.getDocument();
        if (!(doc instanceof Document)) {
            return super.viewToModel(fx, fy, a, bias);
        }
        Rectangle alloc = a.getBounds();
        int x = (int)fx;
        int y = (int)fy;
        if (y < alloc.y) {
            return this.getStartOffset();
        }
        if (y > alloc.y + alloc.height) {
            return this.getEndOffset() - 1;
        }
        Element map = doc.getDefaultRootElement();
        int lineIndex = Math.abs((y - alloc.y) / this.metrics.getHeight());
        if (lineIndex >= map.getElementCount()) {
            return this.getEndOffset() - 1;
        }
        Element line = map.getElement(lineIndex);
        if (x < alloc.x) {
            return line.getStartOffset();
        }
        if (x > alloc.x + alloc.width) {
            return line.getEndOffset() - 1;
        }
        try {
            int p0 = line.getStartOffset();
            int p1 = line.getEndOffset() - 1;
            Segment s = this.getSegment();
            this.getText(p0, p1 - p0, s);
            this.tabBase = alloc.x;
            int offs = p0 + Utilities.getTabbedTextOffset(s, this.metrics, this.tabBase, x, this, p0);
            return offs;
        }
        catch (BadLocationException e) {
            return -1;
        }
    }

    @Override
    public float getPreferredSpan(int axis) {
        if (axis == 1) {
            return super.getPreferredSpan(axis);
        }
        if (this.longestLineLength == -1) {
            this.calcLongestLineLength();
        }
        return this.longestLineLength + 1;
    }

    private void calcLongestLineLength() {
        Container c = this.getContainer();
        this.font = c.getFont();
        this.metrics = c.getFontMetrics(this.font);
        Element lines = this.getElement();
        int n = lines.getElementCount();
        this.longestLineLength = 0;
        for (int i = 0; i < n; ++i) {
            Element line = lines.getElement(i);
            int w = this.getLineWidth(line);
            if (w <= this.longestLineLength) continue;
            this.longestLineLength = w;
            this.longestLine = line;
        }
    }

    protected int getLineWidth(Element line) {
        int w;
        int p0 = line.getStartOffset();
        int p1 = line.getEndOffset() - 1;
        Segment s = this.getSegment();
        try {
            this.getText(p0, p1 - p0, s);
            w = Utilities.getTabbedTextWidth(s, this.metrics, this.tabBase, this, p0);
        }
        catch (BadLocationException e) {
            w = 0;
        }
        return w;
    }

    @Override
    protected void updateDamage(DocumentEvent changes, Shape a, ViewFactory f) {
        Element[] removed;
        Document doc = this.getDocument();
        if (!(doc instanceof Document)) {
            super.updateDamage(changes, a, f);
            return;
        }
        if (this.longestLineLength == -1) {
            this.calcLongestLineLength();
        }
        Container host = this.getContainer();
        this.updateMetrics();
        Element elem = this.getElement();
        DocumentEvent.ElementChange ec = changes.getChange(elem);
        Element[] added = ec != null ? ec.getChildrenAdded() : null;
        Element[] arrelement = removed = ec != null ? ec.getChildrenRemoved() : null;
        if (added != null && added.length > 0 || removed != null && removed.length > 0) {
            int i;
            if (added != null) {
                for (i = 0; i < added.length; ++i) {
                    int w = this.getLineWidth(added[i]);
                    if (w <= this.longestLineLength) continue;
                    this.longestLineLength = w;
                    this.longestLine = added[i];
                }
            }
            if (removed != null) {
                for (i = 0; i < removed.length; ++i) {
                    if (removed[i] != this.longestLine) continue;
                    this.calcLongestLineLength();
                    break;
                }
            }
            this.preferenceChanged(null, true, true);
            host.repaint();
        } else {
            Element map = this.getElement();
            int line = map.getElementIndex(changes.getOffset());
            this.damageLineRange(line, line, a, host);
            if (changes.getType() == DocumentEvent.EventType.INSERT) {
                Element e = map.getElement(line);
                int lineLen = this.getLineWidth(e);
                if (e == this.longestLine) {
                    this.preferenceChanged(null, true, false);
                } else if (lineLen > this.longestLineLength) {
                    this.longestLineLength = lineLen;
                    this.longestLine = e;
                    this.preferenceChanged(null, true, false);
                }
            } else if (changes.getType() == DocumentEvent.EventType.REMOVE && map.getElement(line) == this.longestLine) {
                this.calcLongestLineLength();
                this.preferenceChanged(null, true, false);
            }
        }
    }

    @Override
    public float getMaximumSpan(int axis) {
        return this.getPreferredSpan(axis);
    }

    @Override
    public float getMinimumSpan(int axis) {
        return this.getPreferredSpan(axis);
    }

    @Override
    public int getNextVisualPositionFrom(int pos, Position.Bias b, Shape a, int direction, Position.Bias[] biasRet) throws BadLocationException {
        Element elem = this.getElement();
        if (pos == -1) {
            pos = direction == 5 || direction == 3 ? this.getStartOffset() : this.getEndOffset() - 1;
        }
        switch (direction) {
            case 1: {
                int lineIndex = elem.getElementIndex(pos);
                int lineStart = elem.getElement(lineIndex).getStartOffset();
                if (lineIndex <= 0) break;
                int linePos = pos - lineStart;
                Element el = elem.getElement(lineIndex - 1);
                pos = el.getStartOffset();
                pos += Math.min(Math.min(4096, el.getEndOffset() - pos - 1), linePos);
                break;
            }
            case 5: {
                int lineIndex = elem.getElementIndex(pos);
                int lineStart = elem.getElement(lineIndex).getStartOffset();
                if (lineIndex >= elem.getElementCount() - 1) break;
                int linePos = pos - lineStart;
                Element el = elem.getElement(lineIndex + 1);
                pos = el.getStartOffset();
                pos += Math.min(Math.min(4096, el.getEndOffset() - pos - 1), linePos);
                break;
            }
            case 7: {
                pos = Math.max(0, pos - 1);
                int lineIndex = elem.getElementIndex(pos);
                int lineStart = elem.getElement(lineIndex).getStartOffset();
                if (pos - lineStart <= 4096) break;
                pos = lineStart + 4096;
                break;
            }
            case 3: {
                pos = Math.min(pos + 1, elem.getEndOffset() - 1);
                int lineIndex = elem.getElementIndex(pos);
                int lineStart = elem.getElement(lineIndex).getStartOffset();
                if (pos - lineStart <= 4096) break;
                pos = elem.getElementCount() > lineIndex + 1 ? elem.getElement(lineIndex + 1).getStartOffset() : lineStart + 4096;
                break;
            }
            default: {
                throw new IllegalArgumentException("Bad direction: " + direction);
            }
        }
        return pos;
    }

    static boolean isLinkUndeliningEnabled(View v) {
        OutputTab outputTab;
        OutputOptions.LinkStyle linkStyle;
        OutputTab tab;
        Container pane = v.getContainer();
        if (pane != null && (tab = (OutputTab)SwingUtilities.getAncestorOfClass(OutputTab.class, pane)) != null && (linkStyle = (outputTab = tab).getIO().getOptions().getLinkStyle()) == OutputOptions.LinkStyle.NONE) {
            return false;
        }
        return true;
    }
}

