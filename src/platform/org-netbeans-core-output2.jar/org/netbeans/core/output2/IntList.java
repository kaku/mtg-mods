/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.core.output2;

import java.util.Arrays;

final class IntList {
    private int[] array;
    private int used = 0;
    private int lastAdded = Integer.MIN_VALUE;

    IntList(int capacity) {
        this.array = this.allocArray(capacity);
    }

    public synchronized void add(int value) {
        if (this.used > 0 && this.array[this.used - 1] == value) {
            return;
        }
        if (value < this.lastAdded) {
            throw new IllegalArgumentException("Contents must be presorted - added value " + value + " is less than preceding " + "value " + this.lastAdded);
        }
        if (this.used >= this.array.length) {
            this.growArray();
        }
        this.array[this.used++] = value;
        this.lastAdded = value;
    }

    private int[] allocArray(int size) {
        int[] result = new int[size];
        Arrays.fill(result, Integer.MAX_VALUE);
        return result;
    }

    public synchronized int get(int index) {
        if (index >= this.used) {
            throw new ArrayIndexOutOfBoundsException("List contains " + this.used + " items, but tried to fetch item " + index);
        }
        return this.array[index];
    }

    public boolean contains(int val) {
        return Arrays.binarySearch(this.array, val) >= 0;
    }

    public int findNearest(int val) {
        if (this.size() == 0) {
            return -1;
        }
        int pos = Arrays.binarySearch(this.array, val);
        if (pos < 0) {
            pos = - pos - 2;
        }
        return pos;
    }

    public int indexOf(int val) {
        int result = Arrays.binarySearch(this.array, val);
        if (result < 0) {
            result = -1;
        }
        if (result >= this.used) {
            result = -1;
        }
        return result;
    }

    public synchronized int size() {
        return this.used;
    }

    private void growArray() {
        int[] old = this.array;
        this.array = this.allocArray(Math.round(this.array.length * 2));
        System.arraycopy(old, 0, this.array, 0, old.length);
    }

    public String toString() {
        StringBuffer result = new StringBuffer("IntList [");
        for (int i = 0; i < this.used; ++i) {
            result.append(i);
            result.append(':');
            result.append(this.array[i]);
            if (i == this.used - 1) continue;
            result.append(',');
        }
        result.append(']');
        return result.toString();
    }

    public synchronized void compact(int shift, int decrement) {
        if (shift < 0 || shift > this.used) {
            throw new IllegalArgumentException();
        }
        for (int i = shift; i < this.used; ++i) {
            this.array[i - shift] = this.array[i] - decrement;
        }
        Arrays.fill(this.array, this.used - shift, this.used, Integer.MAX_VALUE);
        if (this.used > 0) {
            this.used -= shift;
            this.lastAdded = this.used == 0 ? Integer.MIN_VALUE : this.lastAdded - decrement;
        }
    }

    public synchronized void shorten(int newSize) {
        if (newSize > this.used || newSize < 0) {
            throw new IllegalArgumentException();
        }
        if (newSize < this.used) {
            this.lastAdded = newSize == 0 ? Integer.MIN_VALUE : this.array[newSize - 1];
            Arrays.fill(this.array, newSize, this.used, Integer.MAX_VALUE);
            this.used = newSize;
        }
    }
}

