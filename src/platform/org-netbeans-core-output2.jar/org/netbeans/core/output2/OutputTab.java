/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.options.OptionsDisplayer
 *  org.netbeans.core.options.keymap.api.KeyStrokeUtils
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.actions.FindAction
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.WeakListeners
 *  org.openide.windows.IOColors
 *  org.openide.windows.IOColors$OutputType
 *  org.openide.windows.IOContainer
 *  org.openide.windows.IOContainer$CallBacks
 *  org.openide.windows.OutputEvent
 *  org.openide.windows.OutputListener
 *  org.openide.windows.OutputWriter
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.core.output2;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.EventQueue;
import java.awt.FileDialog;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.CharConversionException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.RootPaneContainer;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.event.SwingPropertyChangeSupport;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.options.OptionsDisplayer;
import org.netbeans.core.options.keymap.api.KeyStrokeUtils;
import org.netbeans.core.output2.AbstractLines;
import org.netbeans.core.output2.Bundle;
import org.netbeans.core.output2.Controller;
import org.netbeans.core.output2.FindDialogPanel;
import org.netbeans.core.output2.FoldingSideBar;
import org.netbeans.core.output2.LineInfo;
import org.netbeans.core.output2.Lines;
import org.netbeans.core.output2.NbIO;
import org.netbeans.core.output2.NbIOProvider;
import org.netbeans.core.output2.NbWriter;
import org.netbeans.core.output2.OutWriter;
import org.netbeans.core.output2.OutputDocument;
import org.netbeans.core.output2.OutputKind;
import org.netbeans.core.output2.OutputPane;
import org.netbeans.core.output2.options.OutputOptions;
import org.netbeans.core.output2.ui.AbstractOutputPane;
import org.netbeans.core.output2.ui.AbstractOutputTab;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.actions.FindAction;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;
import org.openide.windows.IOColors;
import org.openide.windows.IOContainer;
import org.openide.windows.OutputEvent;
import org.openide.windows.OutputListener;
import org.openide.windows.OutputWriter;
import org.openide.xml.XMLUtil;

final class OutputTab
extends AbstractOutputTab
implements IOContainer.CallBacks {
    private static final RequestProcessor RP = new RequestProcessor("OutputTab");
    private final NbIO io;
    private OutWriter outWriter;
    private PropertyChangeListener optionsListener;
    private volatile boolean actionsLoaded = false;
    private boolean hasOutputListeners = false;
    boolean ignoreCaretChanges = false;
    int[] lastCaretListenerRange;
    private static String lastDir = null;
    FilteredOutput filtOut;
    AbstractOutputPane origPane;
    private static final ACTION[] popupItems = new ACTION[]{ACTION.COPY, ACTION.PASTE, null, ACTION.FIND, ACTION.FIND_NEXT, ACTION.FIND_PREVIOUS, ACTION.FILTER, null, ACTION.WRAP, ACTION.LARGER_FONT, ACTION.SMALLER_FONT, ACTION.SETTINGS, null, ACTION.SAVEAS, ACTION.CLEAR, ACTION.CLOSE};
    private static final ACTION[] popUpFoldItems = new ACTION[]{ACTION.COLLAPSE_FOLD, ACTION.EXPAND_FOLD, null, ACTION.COLLAPSE_ALL, ACTION.EXPAND_ALL, null, ACTION.COLLAPSE_TREE, ACTION.EXPAND_TREE};
    private static final ACTION[] actionsToInstall = new ACTION[]{ACTION.COPY, ACTION.SELECT_ALL, ACTION.FIND, ACTION.FIND_NEXT, ACTION.FIND_PREVIOUS, ACTION.WRAP, ACTION.LARGER_FONT, ACTION.SMALLER_FONT, ACTION.SAVEAS, ACTION.CLOSE, ACTION.COPY, ACTION.NAVTOLINE, ACTION.POSTMENU, ACTION.CLEAR, ACTION.FILTER, ACTION.COLLAPSE_FOLD, ACTION.EXPAND_FOLD, ACTION.COLLAPSE_ALL, ACTION.EXPAND_ALL, ACTION.COLLAPSE_TREE, ACTION.EXPAND_TREE};
    private final Map<ACTION, TabAction> actions = new EnumMap<ACTION, TabAction>(ACTION.class);

    OutputTab(NbIO io) {
        this.io = io;
        if (Controller.LOG) {
            Controller.log("Created an output component for " + io);
        }
        this.outWriter = ((NbWriter)io.getOut()).out();
        OutputDocument doc = new OutputDocument(this.outWriter);
        this.setDocument(doc);
        this.applyOptions();
        this.initOptionsListener();
        this.loadAndInitActions();
    }

    private void applyOptions() {
        Lines lines = this.getDocumentLines();
        if (lines != null) {
            OutputOptions opts = this.io.getOptions();
            lines.setDefColor(IOColors.OutputType.OUTPUT, opts.getColorStandard());
            lines.setDefColor(IOColors.OutputType.ERROR, opts.getColorError());
            lines.setDefColor(IOColors.OutputType.INPUT, opts.getColorInput());
            lines.setDefColor(IOColors.OutputType.HYPERLINK, opts.getColorLink());
            lines.setDefColor(IOColors.OutputType.HYPERLINK_IMPORTANT, opts.getColorLinkImportant());
            Color bg = this.io.getOptions().getColorBackground();
            this.getOutputPane().getFoldingSideBar().setForeground(opts.getColorStandard());
            this.setTextViewBackground(this.getOutputPane().getTextView(), bg);
            this.getOutputPane().setViewFont(this.io.getOptions().getFont(this.getOutputPane().isWrapped()));
        }
    }

    private void setTextViewBackground(JTextComponent textView, Color bg) {
        this.getOutputPane().getTextView().setBackground(bg);
        this.getOutputPane().getFoldingSideBar().setBackground(bg);
        if ("Nimbus".equals(UIManager.getLookAndFeel().getName())) {
            UIDefaults defaults = new UIDefaults();
            defaults.put("EditorPane[Enabled].backgroundPainter", bg);
            textView.putClientProperty("Nimbus.Overrides", defaults);
            textView.putClientProperty("Nimbus.Overrides.InheritDefaults", true);
            textView.setBackground(bg);
        }
    }

    private final TabAction action(ACTION a) {
        return this.actions.get((Object)a);
    }

    private void installKBActions() {
        for (ACTION a : actionsToInstall) {
            this.installKeyboardAction(this.action(a));
        }
    }

    @Override
    public void setDocument(Document doc) {
        if (Controller.LOG) {
            Controller.log("Set document on " + this + " with " + this.io);
        }
        assert (SwingUtilities.isEventDispatchThread());
        OutputDocument old = this.getDocument();
        this.hasOutputListeners = false;
        super.setDocument(doc);
        if (old != null && old instanceof OutputDocument) {
            old.dispose();
        }
        this.applyOptions();
    }

    public void reset() {
        OutputDocument od;
        if (this.origPane != null) {
            this.setFilter(null, false, false);
        }
        this.outWriter = this.io.out();
        OutputDocument actualDocument = this.getDocument();
        if (actualDocument instanceof OutputDocument && (od = actualDocument).getWriter() == this.outWriter) {
            return;
        }
        this.setDocument(new OutputDocument(this.outWriter));
        this.applyOptions();
    }

    public OutputDocument getDocument() {
        Document d = this.getOutputPane().getDocument();
        if (d instanceof OutputDocument) {
            return (OutputDocument)d;
        }
        return null;
    }

    @Override
    protected AbstractOutputPane createOutputPane() {
        return new OutputPane(this);
    }

    @Override
    public void inputSent(String txt) {
        if (Controller.LOG) {
            Controller.log("Input sent on OutputTab: " + txt);
        }
        this.getOutputPane().lockScroll();
        NbIO.IOReader in = this.io.in();
        if (in != null) {
            if (Controller.LOG) {
                Controller.log("Sending input to " + in);
            }
            in.pushText(txt + "\n");
            this.outWriter.print(txt, null, false, null, null, OutputKind.IN, true);
        }
    }

    @Override
    protected void inputEof() {
        NbIO.IOReader in;
        if (Controller.LOG) {
            Controller.log("Input EOF");
        }
        if ((in = this.io.in()) != null) {
            in.eof();
        }
    }

    @Override
    public void hasSelectionChanged(boolean val) {
        if (this.isShowing() && this.actionsLoaded) {
            this.actions.get((Object)ACTION.COPY).setEnabled(val);
            this.actions.get((Object)ACTION.SELECT_ALL).setEnabled(!this.getOutputPane().isAllSelected());
        }
    }

    public NbIO getIO() {
        return this.io;
    }

    void requestActive() {
        this.io.getIOContainer().requestActive();
    }

    public void lineClicked(int line, int pos) {
        OutWriter out = this.getOut();
        int[] range = new int[2];
        OutputListener l = out.getLines().getListener(pos, range);
        if (l != null) {
            int size = out.getLines().getCharCount();
            assert (range[1] < size);
            Controller.ControllerOutputEvent oe = new Controller.ControllerOutputEvent(this.io, out, line);
            l.outputLineAction((OutputEvent)oe);
            if (this.getOutputPane().getLength() >= range[1]) {
                this.getOutputPane().sendCaretToPos(range[0], range[1], true);
            }
        }
    }

    void enterPressed(int caretMark, int caretDot) {
        int start;
        int end;
        if (caretMark < caretDot) {
            start = caretMark;
            end = caretDot;
        } else {
            start = caretDot;
            end = caretMark;
        }
        if (this.getOut().getLines().isListener(start, end)) {
            int[] range = new int[2];
            OutputListener listener = this.getOut().getLines().nearestListener(start, false, range);
            if (listener != null && range[0] <= start && range[1] >= end) {
                int line = this.getOut().getLines().getLineAt(start);
                Controller.ControllerOutputEvent oe = new Controller.ControllerOutputEvent(this.io, line);
                listener.outputLineAction((OutputEvent)oe);
            }
        }
    }

    @Override
    public String toString() {
        return "OutputTab@" + System.identityHashCode(this) + " for " + this.io;
    }

    public void documentChanged(OutputPane pane) {
        if (this.filtOut != null && pane == this.origPane) {
            this.filtOut.readFrom(this.outWriter);
        }
        boolean hadOutputListeners = this.hasOutputListeners;
        boolean bl = this.hasOutputListeners = this.getOut() != null && (this.getOut().getLines().firstListenerLine() >= 0 || this.getOut().getLines().firstImportantListenerLine() >= 0);
        if (this.hasOutputListeners != hadOutputListeners) {
            this.hasOutputListenersChanged(this.hasOutputListeners);
        }
        IOContainer ioContainer = this.io.getIOContainer();
        if (this.io.isFocusTaken()) {
            ioContainer.open();
            ioContainer.select((JComponent)this);
            ioContainer.requestVisible();
        }
        Controller.getDefault().updateName(this);
        if (this == ioContainer.getSelected() && ioContainer.isActivated()) {
            this.updateActions();
        }
    }

    private void navigateToFirstErrorLine() {
        OutWriter out = this.getOut();
        if (out != null) {
            int line = out.getLines().firstImportantListenerLine();
            if (Controller.LOG) {
                Controller.log("NAV TO FIRST LISTENER LINE: " + line);
            }
            if (line >= 0) {
                this.getOutputPane().sendCaretToLine(line, false);
            }
        }
    }

    void hasOutputListenersChanged(boolean hasOutputListeners) {
        if (hasOutputListeners && this.getOutputPane().isScrollLocked()) {
            this.navigateToFirstErrorLine();
        }
    }

    public void activated() {
        this.updateActions();
    }

    public void closed() {
        this.io.setClosed(true);
        this.io.setStreamClosed(true);
        Controller.getDefault().removeTab(this.io);
        NbWriter w = this.io.writer();
        if (w != null && w.isClosed()) {
            this.setDocument(null);
            this.io.dispose();
        } else if (w != null) {
            if (this.getOut() != null) {
                this.getOut().setDisposeOnClose(true);
            }
            this.getDocument().disposeQuietly();
            NbIOProvider.dispose(this.io);
        }
    }

    public void deactivated() {
    }

    public void selected() {
    }

    public boolean shouldRelock(int dot) {
        OutWriter w = this.getOut();
        if (w != null && !w.isClosed()) {
            int dist = Math.abs(w.getLines().getCharCount() - dot);
            return dist < 100;
        }
        return false;
    }

    void caretPosChanged(int pos) {
        if (!this.ignoreCaretChanges) {
            if (this.lastCaretListenerRange != null && pos >= this.lastCaretListenerRange[0] && pos < this.lastCaretListenerRange[1]) {
                return;
            }
            OutWriter out = this.getOut();
            if (out != null) {
                int[] range = new int[2];
                OutputListener l = out.getLines().getListener(pos, range);
                if (l != null) {
                    Controller.ControllerOutputEvent oe = new Controller.ControllerOutputEvent(this.io, out.getLines().getLineAt(pos));
                    l.outputLineSelected((OutputEvent)oe);
                    this.lastCaretListenerRange = range;
                } else {
                    this.lastCaretListenerRange = null;
                }
            }
        }
    }

    private void sendCaretToError(boolean backward) {
        OutWriter out = this.getOut();
        if (out != null) {
            AbstractOutputPane op = this.getOutputPane();
            int selStart = op.getSelectionStart();
            int selEnd = op.getSelectionEnd();
            int pos = op.getCaretPos();
            if (selStart != selEnd && pos == selStart && out.getLines().isListener(selStart, selEnd)) {
                pos = backward ? selStart - 1 : selEnd + 1;
            }
            int[] lpos = new int[2];
            OutputListener l = out.getLines().nearestListener(pos, backward, lpos);
            if (l != null) {
                op.sendCaretToPos(lpos[0], lpos[1], true);
                if (!this.io.getIOContainer().isActivated()) {
                    Controller.ControllerOutputEvent ce = new Controller.ControllerOutputEvent(this.io, out.getLines().getLineAt(lpos[0]));
                    l.outputLineAction((OutputEvent)ce);
                }
            }
        }
    }

    private boolean find(boolean reversed) {
        OutWriter out = this.getOut();
        if (out != null) {
            String msg;
            int pos;
            String lastPattern = FindDialogPanel.result();
            if (lastPattern == null) {
                return false;
            }
            int n = pos = reversed ? this.getOutputPane().getSelectionStart() : this.getOutputPane().getCaretPos();
            if (pos > this.getOutputPane().getLength() || pos < 0) {
                pos = 0;
            }
            boolean regExp = FindDialogPanel.regExp();
            boolean matchCase = FindDialogPanel.matchCase();
            int[] sel = reversed ? out.getLines().rfind(pos, lastPattern, regExp, matchCase) : out.getLines().find(pos, lastPattern, regExp, matchCase);
            String appendMsg = null;
            if (sel == null) {
                int[] arrn = sel = reversed ? out.getLines().rfind(out.getLines().getCharCount(), lastPattern, regExp, matchCase) : out.getLines().find(0, lastPattern, regExp, matchCase);
                if (sel != null) {
                    appendMsg = NbBundle.getMessage(OutputTab.class, (String)(reversed ? "MSG_SearchFromEnd" : "MSG_SearchFromBeg"));
                }
            }
            if (sel != null) {
                this.getOutputPane().unlockScroll();
                int line = out.getLines().getLineAt(sel[0]);
                this.ensureLineVisible(out, line);
                this.getOutputPane().setSelection(sel[0], sel[1]);
                int col = sel[0] - out.getLines().getLineStart(line);
                msg = NbBundle.getMessage(OutputTab.class, (String)"MSG_Found", (Object)lastPattern, (Object)(line + 1), (Object)(col + 1));
                if (appendMsg != null) {
                    msg = msg + "; " + appendMsg;
                }
            } else {
                msg = NbBundle.getMessage(OutputTab.class, (String)"MSG_NotFound", (Object)lastPattern);
            }
            StatusDisplayer.getDefault().setStatusText(msg);
            return sel != null;
        }
        return false;
    }

    private void ensureLineVisible(OutWriter out, int line) {
        if (!out.getLines().isVisible(line)) {
            out.getLines().showFoldsForLine(line);
            if (out.getLines() instanceof ActionListener) {
                ((ActionListener)((Object)out.getLines())).actionPerformed(null);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void saveAs() {
        OutWriter out = this.getOut();
        if (out == null) {
            return;
        }
        File f = OutputTab.showFileChooser(this);
        if (f != null) {
            try {
                OutWriter outWriter = out;
                synchronized (outWriter) {
                    out.getLines().saveAs(f.getPath());
                }
            }
            catch (IOException ioe) {
                NotifyDescriptor notifyDesc = new NotifyDescriptor((Object)NbBundle.getMessage(OutputTab.class, (String)"MSG_SaveAsFailed", (Object)f.getPath()), NbBundle.getMessage(OutputTab.class, (String)"LBL_SaveAsFailedTitle"), -1, 0, new Object[]{NotifyDescriptor.OK_OPTION}, NotifyDescriptor.OK_OPTION);
                DialogDisplayer.getDefault().notify(notifyDesc);
            }
        }
    }

    private static File showFileChooser(JComponent owner) {
        File f = null;
        String dlgTtl = NbBundle.getMessage(Controller.class, (String)"TITLE_SAVE_DLG");
        boolean isAqua = "Aqua".equals(UIManager.getLookAndFeel().getID());
        if (isAqua) {
            String s;
            Container frameOrDialog = owner.getTopLevelAncestor();
            FileDialog fd = frameOrDialog instanceof Frame ? new FileDialog((Frame)frameOrDialog, dlgTtl, 1) : new FileDialog((Dialog)frameOrDialog, dlgTtl, 1);
            if (lastDir != null && new File(lastDir).exists()) {
                fd.setDirectory(lastDir);
            }
            fd.setModal(true);
            fd.setVisible(true);
            if (fd.getFile() != null && fd.getDirectory() != null && (f = new File(s = fd.getDirectory() + fd.getFile())).exists() && f.isDirectory()) {
                f = null;
            }
        } else {
            File dir;
            JFileChooser jfc = new JFileChooser();
            if (lastDir != null && new File(lastDir).exists() && (dir = new File(lastDir)).exists()) {
                jfc.setCurrentDirectory(dir);
            }
            jfc.setName(dlgTtl);
            jfc.setDialogTitle(dlgTtl);
            if (jfc.showSaveDialog(owner.getTopLevelAncestor()) == 0) {
                f = jfc.getSelectedFile();
            }
        }
        if (f != null && f.exists() && !isAqua) {
            String msg = NbBundle.getMessage(Controller.class, (String)"FMT_FILE_EXISTS", (Object[])new Object[]{f.getName()});
            String title = NbBundle.getMessage(Controller.class, (String)"TITLE_FILE_EXISTS");
            if (JOptionPane.showConfirmDialog(owner.getTopLevelAncestor(), msg, title, 2) != 0) {
                f = null;
            }
        }
        if (f != null) {
            lastDir = f.getParent();
        }
        return f;
    }

    private void openHyperlink() {
        OutWriter out = this.getOut();
        if (out != null) {
            int pos = this.getOutputPane().getCaretPos();
            int[] range = new int[2];
            OutputListener l = out.getLines().getListener(pos, range);
            if (l != null) {
                this.ignoreCaretChanges = true;
                this.getOutputPane().sendCaretToPos(range[0], range[1], true);
                this.ignoreCaretChanges = false;
                Controller.ControllerOutputEvent coe = new Controller.ControllerOutputEvent(this.io, out.getLines().getLineAt(pos));
                l.outputLineAction((OutputEvent)coe);
            }
        }
    }

    void postPopupMenu(Point p, Component src) {
        int i;
        if (!this.actionsLoaded) {
            StatusDisplayer.getDefault().setStatusText(Bundle.STATUS_Initializing());
            return;
        }
        JPopupMenu popup = new JPopupMenu();
        Action[] a = this.getToolbarActions();
        if (a.length > 0) {
            boolean added = false;
            for (i = 0; i < a.length; ++i) {
                if (a[i].getValue("Name") == null) continue;
                popup.add(new ProxyAction(a[i]));
                added = true;
            }
            if (added) {
                popup.addSeparator();
            }
        }
        ArrayList<TabAction> activeActions = new ArrayList<TabAction>(popupItems.length);
        for (i = 0; i < popupItems.length; ++i) {
            AbstractButton item;
            if (popupItems[i] == null) {
                popup.addSeparator();
                continue;
            }
            TabAction ta = this.action(popupItems[i]);
            if (popupItems[i] == ACTION.WRAP) {
                item = new JCheckBoxMenuItem(ta);
                item.setSelected(this.getOutputPane().isWrapped());
                activeActions.add(ta);
                popup.add((JMenuItem)item);
                continue;
            }
            if (popupItems[i] == ACTION.FILTER) {
                item = new JCheckBoxMenuItem(ta);
                item.setSelected(this.origPane != null);
                activeActions.add(ta);
                popup.add((JMenuItem)item);
                continue;
            }
            if (popupItems[i] == ACTION.CLOSE && !this.io.getIOContainer().isCloseable((JComponent)this)) continue;
            item = popup.add(ta);
            activeActions.add(ta);
            if (popupItems[i] != ACTION.FIND) continue;
            item.setMnemonic(70);
        }
        this.addFoldingActionsToPopUpMenu(popup, activeActions);
        KeyStroke esc = KeyStroke.getKeyStroke(27, 0);
        JTextComponent c = this.getOutputPane().getTextView();
        Object escHandle = c.getInputMap().get(esc);
        c.getInputMap().remove(esc);
        this.getInputMap(1).remove(esc);
        popup.addPopupMenuListener(new PMListener(activeActions, escHandle));
        popup.show(src, p.x, p.y);
    }

    private void addFoldingActionsToPopUpMenu(JPopupMenu menu, List<TabAction> activeActions) {
        JMenu submenu = new JMenu(NbBundle.getMessage(OutputTab.class, (String)"LBL_OutputFolds"));
        for (ACTION a : popUpFoldItems) {
            if (a == null) {
                submenu.addSeparator();
                continue;
            }
            TabAction ta = this.action(a);
            activeActions.add(ta);
            submenu.add(new JMenuItem(ta));
        }
        menu.addSeparator();
        menu.add(submenu);
    }

    void updateActions() {
        if (!this.actionsLoaded) {
            return;
        }
        OutputPane pane = (OutputPane)this.getOutputPane();
        int len = pane.getLength();
        boolean enable = len > 0;
        OutWriter out = this.getOut();
        this.action(ACTION.SAVEAS).setEnabled(enable);
        this.action(ACTION.SELECT_ALL).setEnabled(enable);
        this.action(ACTION.COPY).setEnabled(pane.hasSelection());
        boolean hasErrors = out == null ? false : out.getLines().hasListeners();
        this.action(ACTION.NEXT_ERROR).setEnabled(hasErrors);
        this.action(ACTION.PREV_ERROR).setEnabled(hasErrors);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void setFilter(String pattern, boolean regExp, boolean matchCase) {
        if (pattern == null) {
            assert (this.origPane != null);
            this.setOutputPane(this.origPane);
            this.origPane = null;
            this.filtOut.dispose();
            this.filtOut = null;
        } else {
            assert (this.origPane == null);
            this.origPane = this.getOutputPane();
            this.filtOut = new FilteredOutput(pattern, regExp, matchCase);
            this.setOutputPane(this.filtOut.getPane());
            try {
                this.waitCursor(true);
                this.filtOut.readFrom(this.outWriter);
                this.installKBActions();
            }
            finally {
                this.waitCursor(false);
            }
        }
        this.validate();
        this.getOutputPane().repaint();
        this.requestFocus();
    }

    private void waitCursor(boolean enable) {
        RootPaneContainer root = (RootPaneContainer)((Object)this.getTopLevelAncestor());
        Cursor cursor = Cursor.getPredefinedCursor(enable ? 3 : 0);
        root.getGlassPane().setCursor(cursor);
        root.getGlassPane().setVisible(enable);
    }

    OutWriter getOut() {
        return this.origPane != null ? this.filtOut.getWriter() : this.outWriter;
    }

    private void disableHtmlName() {
        String escaped;
        Controller.getDefault().removeFromUpdater(this);
        try {
            escaped = XMLUtil.toAttributeValue((String)(this.io.getName() + " "));
        }
        catch (CharConversionException e) {
            escaped = this.io.getName() + " ";
        }
        this.io.getIOContainer().setTitle((JComponent)this, escaped.replace("&apos;", "'"));
    }

    private void initOptionsListener() {
        this.optionsListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Lines lines = OutputTab.this.getDocumentLines();
                if (lines != null) {
                    String pn = evt.getPropertyName();
                    OutputOptions opts = OutputTab.this.io.getOptions();
                    OutputTab.this.updateOptionsProperty(pn, lines, opts);
                    OutputTab.this.repaint();
                }
            }
        };
        this.io.getOptions().addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.optionsListener, (Object)this.io.getOptions()));
    }

    private void updateOptionsProperty(String pn, Lines lines, OutputOptions opts) {
        if ("color.standard".equals(pn)) {
            lines.setDefColor(IOColors.OutputType.OUTPUT, opts.getColorStandard());
            this.getOutputPane().getFoldingSideBar().setForeground(opts.getColorStandard());
        } else if ("color.error".equals(pn)) {
            lines.setDefColor(IOColors.OutputType.ERROR, opts.getColorError());
        } else if ("color.input".equals(pn)) {
            lines.setDefColor(IOColors.OutputType.INPUT, opts.getColorInput());
        } else if ("color.link".equals(pn)) {
            lines.setDefColor(IOColors.OutputType.HYPERLINK, opts.getColorLink());
        } else if ("color.link.important".equals(pn)) {
            lines.setDefColor(IOColors.OutputType.HYPERLINK_IMPORTANT, opts.getColorLinkImportant());
        } else if ("color.backgorund".equals(pn)) {
            Color bg = opts.getColorBackground();
            this.setTextViewBackground(this.getOutputPane().getTextView(), bg);
        } else if ("font".equals(pn)) {
            if (!this.getOutputPane().isWrapped()) {
                this.getOutputPane().setViewFont(opts.getFont());
            }
        } else if ("font.size.wrap".equals(pn) && this.getOutputPane().isWrapped()) {
            this.getOutputPane().setViewFont(opts.getFontForWrappedMode());
        }
    }

    private void loadAndInitActions() {
        RP.post(new Runnable(){

            @Override
            public void run() {
                OutputTab.this.createActions();
                EventQueue.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        OutputTab.this.installKBActions();
                        OutputTab.this.getActionMap().put("jumpPrev", OutputTab.this.action(ACTION.PREV_ERROR));
                        OutputTab.this.getActionMap().put("jumpNext", OutputTab.this.action(ACTION.NEXT_ERROR));
                        OutputTab.this.getActionMap().put(FindAction.class.getName(), OutputTab.this.action(ACTION.FIND));
                        OutputTab.this.getActionMap().put("copy-to-clipboard", OutputTab.this.action(ACTION.COPY));
                        OutputTab.this.actionsLoaded = true;
                        OutputTab.this.updateActions();
                        OutputTab.this.setInputVisible(OutputTab.this.isInputVisible());
                    }
                });
            }

        });
    }

    private void createActions() {
        KeyStrokeUtils.refreshActionCache();
        for (ACTION a : ACTION.values()) {
            TabAction action;
            switch (a) {
                case COPY: 
                case PASTE: 
                case WRAP: 
                case SAVEAS: 
                case CLOSE: 
                case NEXT_ERROR: 
                case PREV_ERROR: 
                case SELECT_ALL: 
                case FIND: 
                case FIND_NEXT: 
                case FIND_PREVIOUS: 
                case FILTER: 
                case LARGER_FONT: 
                case SMALLER_FONT: 
                case SETTINGS: 
                case CLEAR: 
                case COLLAPSE_FOLD: 
                case EXPAND_FOLD: 
                case COLLAPSE_ALL: 
                case EXPAND_ALL: 
                case COLLAPSE_TREE: 
                case EXPAND_TREE: {
                    action = new TabAction(this, a, "ACTION_" + a.name());
                    break;
                }
                case NAVTOLINE: {
                    action = new TabAction(this, a, "navToLine", KeyStroke.getKeyStroke(10, 0));
                    break;
                }
                case POSTMENU: {
                    action = new TabAction(this, a, "postMenu", KeyStroke.getKeyStroke(121, 64));
                    break;
                }
                case NEXTTAB: {
                    action = new TabAction(this, a, "NextViewAction", null);
                    break;
                }
                case PREVTAB: {
                    action = new TabAction(this, a, "PreviousViewAction", null);
                    break;
                }
                default: {
                    throw new IllegalStateException("Unhandled action " + (Object)((Object)a));
                }
            }
            this.actions.put(a, action);
        }
    }

    @Override
    public void setInputVisible(boolean val) {
        super.setInputVisible(val);
        TabAction pasteAction = this.action(ACTION.PASTE);
        if (pasteAction != null) {
            pasteAction.setEnabled(val);
        }
    }

    private boolean validRegExp(String pattern) {
        try {
            Pattern.compile(pattern);
            return true;
        }
        catch (PatternSyntaxException ex) {
            JOptionPane.showMessageDialog(this.getTopLevelAncestor(), NbBundle.getMessage(OutputTab.class, (String)"FMT_Invalid_RegExp", (Object)pattern), NbBundle.getMessage(OutputTab.class, (String)"LBL_Invalid_RegExp"), 0);
            return false;
        }
    }

    private String getFindDlgResult(String selection, String title, String label, String button) {
        String pattern = FindDialogPanel.getResult(selection, title, label, button);
        while (pattern != null && FindDialogPanel.regExp() && !this.validRegExp(pattern)) {
            pattern = FindDialogPanel.getResult(pattern, title, label, button);
        }
        return pattern;
    }

    private Lines getDocumentLines() {
        OutputDocument doc = this.getDocument();
        return doc == null ? null : doc.getLines();
    }

    private class FilteredOutput {
        String pattern;
        OutWriter out;
        OutputPane pane;
        OutputDocument doc;
        int readCount;
        Pattern compPattern;
        boolean regExp;
        boolean matchCase;

        public FilteredOutput(String pattern, boolean regExp, boolean matchCase) {
            this.pattern = regExp || matchCase ? pattern : pattern.toLowerCase();
            this.regExp = regExp;
            this.matchCase = matchCase;
            this.out = new OutWriter();
            this.pane = new OutputPane(OutputTab.this);
            this.doc = new OutputDocument(this.out);
            this.pane.setDocument(this.doc);
        }

        boolean passFilter(String str) {
            if (this.regExp) {
                if (this.compPattern == null) {
                    this.compPattern = this.matchCase ? Pattern.compile(this.pattern) : Pattern.compile(this.pattern, 2);
                }
                return this.compPattern.matcher(str).find();
            }
            return this.matchCase ? str.contains(this.pattern) : str.toLowerCase().contains(this.pattern);
        }

        OutputPane getPane() {
            return this.pane;
        }

        OutWriter getWriter() {
            return this.out;
        }

        synchronized void readFrom(OutWriter orig) {
            AbstractLines lines = (AbstractLines)orig.getLines();
            int lineCount = lines.getLineCount() - (orig.isClosed() ? 0 : 1);
            while (this.readCount < lineCount) {
                try {
                    int line;
                    ++this.readCount;
                    String str = lines.getLine(line);
                    if (!this.passFilter(str)) continue;
                    LineInfo info = lines.getExistingLineInfo(line);
                    this.out.print(str, info, lines.isImportantLine(line));
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }

        void dispose() {
            this.out.dispose();
        }
    }

    private class PMListener
    implements PopupMenuListener {
        private List<TabAction> popupItems;
        private Object handle;

        PMListener(List<TabAction> popupItems, Object escHandle) {
            this.popupItems = popupItems;
            this.handle = escHandle;
        }

        @Override
        public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
            JPopupMenu popup = (JPopupMenu)e.getSource();
            popup.removeAll();
            popup.setInvoker(null);
            KeyStroke esc = KeyStroke.getKeyStroke(27, 0);
            JTextComponent c = OutputTab.this.getOutputPane().getTextView();
            c.getInputMap().put(esc, this.handle);
            OutputTab.this.getInputMap(1).put(esc, this.handle);
            popup.removePopupMenuListener(this);
            for (TabAction action : this.popupItems) {
                action.clearListeners();
            }
        }

        @Override
        public void popupMenuCanceled(PopupMenuEvent e) {
            this.popupMenuWillBecomeInvisible(e);
        }

        @Override
        public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        }
    }

    private static class ProxyAction
    implements Action {
        private Action orig;

        ProxyAction(Action original) {
            this.orig = original;
        }

        @Override
        public Object getValue(String key) {
            if ("SmallIcon".equals(key)) {
                return null;
            }
            return this.orig.getValue(key);
        }

        @Override
        public void putValue(String key, Object value) {
            this.orig.putValue(key, value);
        }

        @Override
        public void setEnabled(boolean b) {
            this.orig.setEnabled(b);
        }

        @Override
        public boolean isEnabled() {
            return this.orig.isEnabled();
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener listener) {
            this.orig.addPropertyChangeListener(listener);
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener listener) {
            this.orig.removePropertyChangeListener(listener);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.orig.actionPerformed(e);
        }
    }

    class TabAction
    extends AbstractAction {
        private ACTION action;
        final /* synthetic */ OutputTab this$0;

        TabAction(OutputTab outputTab, ACTION action, String bundleKey) {
            this.this$0 = outputTab;
            if (bundleKey != null) {
                String name = NbBundle.getMessage(OutputTab.class, (String)bundleKey);
                List<KeyStroke[]> accels = this.getAcceleratorsFor(action);
                this.action = action;
                this.putValue("Name", name);
                if (accels != null && accels.size() > 0) {
                    ArrayList<KeyStroke> l = new ArrayList<KeyStroke>(accels.size());
                    for (KeyStroke[] ks : accels) {
                        if (ks.length != 1) continue;
                        l.add(ks[0]);
                    }
                    if (l.size() > 0) {
                        this.putValue("ACCELERATORS_KEY", l.toArray(new KeyStroke[l.size()]));
                        this.putValue("AcceleratorKey", l.get(0));
                    }
                }
            }
        }

        TabAction(OutputTab outputTab, ACTION action, String name, KeyStroke stroke) {
            this.this$0 = outputTab;
            this.action = action;
            this.putValue("Name", name);
            this.putValue("AcceleratorKey", stroke);
        }

        void clearListeners() {
            PropertyChangeListener[] l = this.changeSupport.getPropertyChangeListeners();
            for (int i = 0; i < l.length; ++i) {
                this.removePropertyChangeListener(l[i]);
            }
        }

        private List<KeyStroke[]> getAcceleratorsFor(ACTION action) {
            switch (action) {
                case COPY: {
                    return KeyStrokeUtils.getKeyStrokesForAction((String)"copy-to-clipboard", (KeyStroke)null);
                }
                case PASTE: {
                    return KeyStrokeUtils.getKeyStrokesForAction((String)"paste-from-clipboard", (KeyStroke)null);
                }
                case SAVEAS: {
                    return KeyStrokeUtils.getKeyStrokesForAction((String)"output-window-save-as", (KeyStroke)null);
                }
                case CLOSE: {
                    return KeyStrokeUtils.getKeyStrokesForAction((String)"output-window-close", (KeyStroke)null);
                }
                case NEXT_ERROR: {
                    return KeyStrokeUtils.getKeyStrokesForAction((String)"next-error", (KeyStroke)null);
                }
                case PREV_ERROR: {
                    return KeyStrokeUtils.getKeyStrokesForAction((String)"previous-error", (KeyStroke)null);
                }
                case SELECT_ALL: {
                    return KeyStrokeUtils.getKeyStrokesForAction((String)"select-all", (KeyStroke)null);
                }
                case FIND: {
                    return KeyStrokeUtils.getKeyStrokesForAction((String)"org.openide.actions.FindAction", (KeyStroke)null);
                }
                case FIND_NEXT: {
                    return KeyStrokeUtils.getKeyStrokesForAction((String)"find-next", (KeyStroke)null);
                }
                case FIND_PREVIOUS: {
                    return KeyStrokeUtils.getKeyStrokesForAction((String)"find-previous", (KeyStroke)null);
                }
                case FILTER: {
                    return KeyStrokeUtils.getKeyStrokesForAction((String)"output-window-filter", (KeyStroke)null);
                }
                case LARGER_FONT: {
                    return KeyStrokeUtils.getKeyStrokesForAction((String)"output-window-larger-font", (KeyStroke)null);
                }
                case SMALLER_FONT: {
                    return KeyStrokeUtils.getKeyStrokesForAction((String)"output-window-smaller-font", (KeyStroke)null);
                }
                case SETTINGS: {
                    return KeyStrokeUtils.getKeyStrokesForAction((String)"output-window-settings", (KeyStroke)null);
                }
                case CLEAR: {
                    return KeyStrokeUtils.getKeyStrokesForAction((String)"output-window-clear", (KeyStroke)null);
                }
                case WRAP: {
                    return KeyStrokeUtils.getKeyStrokesForAction((String)"output-window-wrap", (KeyStroke)null);
                }
                case COLLAPSE_FOLD: {
                    return KeyStrokeUtils.getKeyStrokesForAction((String)"collapse-fold", (KeyStroke)null);
                }
                case EXPAND_FOLD: {
                    return KeyStrokeUtils.getKeyStrokesForAction((String)"expand-fold", (KeyStroke)null);
                }
                case COLLAPSE_ALL: {
                    return KeyStrokeUtils.getKeyStrokesForAction((String)"collapse-all-folds", (KeyStroke)null);
                }
                case EXPAND_ALL: {
                    return KeyStrokeUtils.getKeyStrokesForAction((String)"expand-all-folds", (KeyStroke)null);
                }
                case COLLAPSE_TREE: {
                    return KeyStrokeUtils.getKeyStrokesForAction((String)"collapse-fold-tree", (KeyStroke)null);
                }
                case EXPAND_TREE: {
                    return KeyStrokeUtils.getKeyStrokesForAction((String)"expand-fold-tree", (KeyStroke)null);
                }
            }
            return null;
        }

        public ACTION getAction() {
            return this.action;
        }

        @Override
        public boolean isEnabled() {
            if (this.this$0.getIO().isClosed()) {
                OutputTab tab;
                TabAction a;
                JComponent selected = this.this$0.getIO().getIOContainer().getSelected();
                if (this.this$0 != selected && selected instanceof OutputTab && (a = (tab = (OutputTab)selected).action(this.action)) != null) {
                    return a.isEnabled();
                }
                return false;
            }
            return super.isEnabled();
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (this.this$0.getIO().isClosed()) {
                TabAction a;
                OutputTab tab;
                JComponent selected = this.this$0.getIO().getIOContainer().getSelected();
                if (this.this$0 != selected && selected instanceof OutputTab && (a = (tab = (OutputTab)selected).action(this.action)) != null) {
                    a.actionPerformed(e);
                }
                return;
            }
            switch (this.getAction()) {
                case COPY: {
                    this.this$0.getOutputPane().copy();
                    break;
                }
                case PASTE: {
                    this.this$0.getOutputPane().paste();
                    break;
                }
                case WRAP: {
                    boolean wrapped = this.this$0.getOutputPane().isWrapped();
                    this.this$0.getOutputPane().setWrapped(!wrapped);
                    break;
                }
                case SAVEAS: {
                    this.this$0.saveAs();
                    break;
                }
                case CLOSE: {
                    this.this$0.io.getIOContainer().remove((JComponent)this.this$0);
                    break;
                }
                case NEXT_ERROR: {
                    this.this$0.sendCaretToError(false);
                    break;
                }
                case PREV_ERROR: {
                    this.this$0.sendCaretToError(true);
                    break;
                }
                case SELECT_ALL: {
                    this.this$0.getOutputPane().selectAll();
                    break;
                }
                case FIND: {
                    String pattern = this.this$0.getFindDlgResult(this.this$0.getOutputPane().getSelectedText(), "LBL_Find_Title", "LBL_Find_What", "BTN_Find");
                    if (pattern == null || !this.this$0.find(false)) break;
                    TabAction findNext = this.this$0.action(ACTION.FIND_NEXT);
                    TabAction findPrev = this.this$0.action(ACTION.FIND_PREVIOUS);
                    if (findNext != null) {
                        findNext.setEnabled(true);
                    }
                    if (findPrev != null) {
                        findPrev.setEnabled(true);
                    }
                    this.this$0.requestFocus();
                    break;
                }
                case FIND_NEXT: {
                    this.this$0.find(false);
                    break;
                }
                case FIND_PREVIOUS: {
                    this.this$0.find(true);
                    break;
                }
                case NAVTOLINE: {
                    this.this$0.openHyperlink();
                    break;
                }
                case POSTMENU: {
                    this.this$0.postPopupMenu(new Point(0, 0), this.this$0);
                    break;
                }
                case CLEAR: {
                    NbWriter writer = this.this$0.io.writer();
                    if (writer == null) break;
                    try {
                        boolean vis = this.this$0.isInputVisible();
                        boolean closed = this.this$0.io.isStreamClosed();
                        writer.reset();
                        this.this$0.setInputVisible(vis);
                        this.this$0.io.setStreamClosed(closed);
                    }
                    catch (IOException ioe) {
                        Exceptions.printStackTrace((Throwable)ioe);
                    }
                    break;
                }
                case SMALLER_FONT: {
                    Controller.getDefault().changeFontSizeBy(-1, this.this$0.getOutputPane().isWrapped());
                    break;
                }
                case LARGER_FONT: {
                    Controller.getDefault().changeFontSizeBy(1, this.this$0.getOutputPane().isWrapped());
                    break;
                }
                case SETTINGS: {
                    OptionsDisplayer.getDefault().open("Advanced/OutputSettings");
                    break;
                }
                case FILTER: {
                    if (this.this$0.origPane != null) {
                        this.this$0.setFilter(null, false, false);
                        break;
                    }
                    String pattern = this.this$0.getFindDlgResult(this.this$0.getOutputPane().getSelectedText(), "LBL_Filter_Title", "LBL_Filter_What", "BTN_Filter");
                    if (pattern == null) break;
                    this.this$0.setFilter(pattern, FindDialogPanel.regExp(), FindDialogPanel.matchCase());
                    break;
                }
                case COLLAPSE_FOLD: {
                    this.this$0.getOutputPane().collapseFold();
                    break;
                }
                case EXPAND_FOLD: {
                    this.this$0.getOutputPane().expandFold();
                    break;
                }
                case COLLAPSE_ALL: {
                    this.this$0.getOutputPane().collapseAllFolds();
                    break;
                }
                case EXPAND_ALL: {
                    this.this$0.getOutputPane().expandAllFolds();
                    break;
                }
                case COLLAPSE_TREE: {
                    this.this$0.getOutputPane().collapseFoldTree();
                    break;
                }
                case EXPAND_TREE: {
                    this.this$0.getOutputPane().expandFoldTree();
                    break;
                }
                default: {
                    assert (false);
                    break;
                }
            }
        }
    }

    static enum ACTION {
        COPY,
        WRAP,
        SAVEAS,
        CLOSE,
        NEXT_ERROR,
        PREV_ERROR,
        SELECT_ALL,
        FIND,
        FIND_NEXT,
        NAVTOLINE,
        POSTMENU,
        FIND_PREVIOUS,
        CLEAR,
        NEXTTAB,
        PREVTAB,
        LARGER_FONT,
        SMALLER_FONT,
        SETTINGS,
        FILTER,
        PASTE,
        COLLAPSE_FOLD,
        EXPAND_FOLD,
        COLLAPSE_ALL,
        EXPAND_ALL,
        COLLAPSE_TREE,
        EXPAND_TREE;
        

        private ACTION() {
        }
    }

}

