/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.core.output2;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Method;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;
import org.netbeans.core.output2.AbstractLines;
import org.netbeans.core.output2.BufferResource;
import org.netbeans.core.output2.Controller;
import org.netbeans.core.output2.OutWriter;
import org.netbeans.core.output2.Storage;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

class FileMapStorage
implements Storage {
    private FileChannel fileChannel;
    private static final int BASE_BUFFER_SIZE = 8196;
    private static final long MAX_MAP_RANGE = 0x100000;
    private static final RequestProcessor RP = new RequestProcessor("FileMapStorage");
    private static final Set<FileMapStorage> undisposed = new HashSet<FileMapStorage>();
    private ByteBuffer master;
    private MappedBufferResource contents;
    private long mappedRange;
    private long mappedStart;
    private ByteBuffer buffer = null;
    protected int bytesWritten = 0;
    private File outfile = null;
    private int outstandingBufferCount = 0;
    private long startOffset = 0;
    private boolean closed;

    FileMapStorage() {
        this.init();
    }

    private void init() {
        this.contents = null;
        this.mappedRange = -1;
        this.mappedStart = 0;
        this.master = ByteBuffer.allocateDirect(8196);
        this.fileChannel = null;
        this.buffer = null;
        this.bytesWritten = 0;
        this.closed = true;
        FileMapStorage.addUndisposed(this);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private void ensureFileExists() throws IOException {
        File dir;
        if (this.outfile != null) return;
        String outdir = System.getProperty("java.io.tmpdir");
        if (!outdir.endsWith(File.separator)) {
            outdir = outdir + File.separator;
        }
        if (!(dir = new File(outdir)).exists()) {
            IllegalStateException ise = new IllegalStateException("Cannot find temp directory " + outdir);
            Exceptions.attachLocalizedMessage((Throwable)ise, (String)NbBundle.getMessage(OutWriter.class, (String)"FMT_CannotWrite", (Object)outdir));
            throw ise;
        }
        Class<FileMapStorage> ise = FileMapStorage.class;
        synchronized (FileMapStorage.class) {
            StringBuilder fname = new StringBuilder(outdir).append("output").append(Long.toString(System.currentTimeMillis()));
            this.outfile = new File(fname.toString());
            while (this.outfile.exists()) {
                fname.append('x');
                this.outfile = new File(fname.toString());
            }
            this.outfile.createNewFile();
            if (!this.outfile.exists() || !this.outfile.canWrite()) {
                IllegalStateException ise2 = new IllegalStateException("Cannot write to " + fname);
                Exceptions.attachLocalizedMessage((Throwable)ise2, (String)NbBundle.getMessage(OutWriter.class, (String)"FMT_CannotWrite", (Object)outdir));
                throw ise2;
            }
            this.outfile.deleteOnExit();
            // ** MonitorExit[ise] (shouldn't be in output)
            return;
        }
    }

    public String toString() {
        return this.outfile == null ? "[unused or disposed FileMapStorage]" : this.outfile.getPath();
    }

    private FileChannel writeChannel() throws IOException {
        FileChannel channel = this.fileChannel();
        this.closed = !channel.isOpen();
        return channel;
    }

    private FileChannel fileChannel() throws IOException {
        if (this.fileChannel == null || !this.fileChannel.isOpen()) {
            this.ensureFileExists();
            RandomAccessFile raf = new RandomAccessFile(this.outfile, "rw");
            this.fileChannel = raf.getChannel();
        }
        return this.fileChannel;
    }

    @Override
    public synchronized ByteBuffer getWriteBuffer(int size) {
        if (this.master.capacity() - this.master.position() < size) {
            int newSize = Math.max(16392, size + 8196);
            this.master = ByteBuffer.allocateDirect(newSize);
        }
        if (this.buffer == null) {
            this.buffer = this.master.slice();
        } else {
            int charsRemaining = AbstractLines.toCharIndex(this.buffer.capacity() - this.buffer.position());
            if (charsRemaining < size) {
                this.buffer.flip();
                this.buffer = this.master.slice();
            }
        }
        ++this.outstandingBufferCount;
        return this.buffer;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int write(ByteBuffer bb) throws IOException {
        FileMapStorage fileMapStorage = this;
        synchronized (fileMapStorage) {
            if (bb == this.buffer) {
                this.buffer = null;
            }
        }
        int position = this.size();
        int byteCount = bb.position();
        bb.flip();
        FileChannel channel = this.writeChannel();
        if (channel.isOpen()) {
            Thread.interrupted();
            channel.write(bb);
            FileMapStorage fileMapStorage2 = this;
            synchronized (fileMapStorage2) {
                this.bytesWritten += byteCount;
                --this.outstandingBufferCount;
            }
        }
        return position;
    }

    @Override
    public synchronized void removeBytesFromEnd(int length) throws IOException {
        if (length == 0) {
            return;
        }
        FileChannel channel = this.writeChannel();
        channel.position(channel.position() - (long)length);
        this.bytesWritten -= length;
    }

    @Override
    public synchronized void dispose() {
        if (Controller.LOG) {
            Controller.log("Disposing file map storage");
            Controller.logStack();
        }
        final FileChannel oldChannel = this.fileChannel;
        final File oldFile = this.outfile;
        final MappedBufferResource oldContents = this.contents;
        this.fileChannel = null;
        this.closed = true;
        this.outfile = null;
        this.buffer = null;
        this.contents = null;
        if (oldChannel != null || oldFile != null) {
            RP.post(new Runnable(){

                @Override
                public void run() {
                    try {
                        if (oldContents != null) {
                            oldContents.releaseBuffer();
                        }
                        if (oldChannel != null && oldChannel.isOpen()) {
                            oldChannel.close();
                        }
                        if (oldFile != null && oldFile.exists()) {
                            oldFile.delete();
                        }
                    }
                    catch (Exception e) {
                        Exceptions.printStackTrace((Throwable)e);
                    }
                }
            });
        }
        FileMapStorage.removeUndisposed(this);
    }

    File getOutputFile() {
        return this.outfile;
    }

    private static void unmap(Object buffer) {
        try {
            Method getCleanerMethod = buffer.getClass().getMethod("cleaner", new Class[0]);
            getCleanerMethod.setAccessible(true);
            Object cleaner = getCleanerMethod.invoke(buffer, new Object[0]);
            if (cleaner != null) {
                cleaner.getClass().getMethod("clean", new Class[0]).invoke(cleaner, new Object[0]);
            }
        }
        catch (Exception e) {
            // empty catch block
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public BufferResource<ByteBuffer> getReadBuffer(int start, int byteCount) throws IOException {
        ByteBuffer cont;
        long fileStart = this.startOffset + (long)start;
        FileMapStorage fileMapStorage = this;
        synchronized (fileMapStorage) {
            ByteBuffer byteBuffer = cont = this.contents == null ? null : this.contents.getBuffer();
            if (cont == null || fileStart + (long)byteCount > this.mappedRange || fileStart < this.mappedStart) {
                FileChannel ch = this.fileChannel();
                this.mappedStart = Math.max(0, fileStart - 524288);
                long prevMappedRange = this.mappedRange;
                long map = (long)byteCount > 524288 ? (long)(byteCount + byteCount / 10) : 524288;
                this.mappedRange = Math.min(ch.size(), fileStart + map);
                try {
                    try {
                        cont = ch.map(FileChannel.MapMode.READ_ONLY, this.mappedStart, this.mappedRange - this.mappedStart);
                        this.updateContents(cont);
                    }
                    catch (IOException ioe) {
                        Logger.getAnonymousLogger().info("Failed to memory map output file for reading. Trying to read it normally.");
                        cont = ByteBuffer.allocate((int)(this.mappedRange - this.mappedStart));
                        ch.read(cont, this.mappedStart);
                        this.updateContents(cont);
                    }
                }
                catch (Exception e) {
                    String msg = MessageFormat.format("Failed to read output file. Start:{0} bytes reqd={1} mapped range={2} previous mapped range={3} channel size: {4}", start, byteCount, this.mappedRange, prevMappedRange, ch.size());
                    throw new IOException(msg, e);
                }
            }
            if (fileStart - this.mappedStart > (long)(cont.limit() - byteCount)) {
                cont.position(Math.max(0, cont.limit() - byteCount));
            } else {
                cont.position((int)(fileStart - this.mappedStart));
            }
        }
        int limit = Math.min(cont.limit(), byteCount);
        try {
            return new ChildBufferResource((ByteBuffer)cont.slice().limit(limit), this.contents);
        }
        catch (Exception e) {
            throw new IllegalStateException("Error setting limit to " + limit + " contents size = " + cont.limit() + " requested: read " + "buffer from " + start + " to be " + byteCount + " bytes");
        }
    }

    private void updateContents(ByteBuffer buffer) {
        if (this.contents != null) {
            this.contents.decRefs();
        }
        this.contents = new MappedBufferResource(buffer);
        this.contents.incRefs();
    }

    @Override
    public synchronized int size() {
        return this.bytesWritten;
    }

    @Override
    public void flush() throws IOException {
        if (this.buffer != null) {
            if (Controller.LOG) {
                Controller.log("FILEMAP STORAGE flush(): " + this.outstandingBufferCount);
            }
            this.write(this.buffer);
            this.fileChannel.force(false);
            this.buffer = null;
        }
    }

    @Override
    public void close() throws IOException {
        if (this.fileChannel != null) {
            this.flush();
        }
        this.closed = true;
    }

    @Override
    public boolean isClosed() {
        return this.fileChannel == null || this.closed;
    }

    private static synchronized void addUndisposed(FileMapStorage fms) {
        undisposed.add(fms);
    }

    private static synchronized void removeUndisposed(FileMapStorage fms) {
        undisposed.remove(fms);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void shiftStart(int byteOffset) {
        FileMapStorage fileMapStorage = this;
        synchronized (fileMapStorage) {
            this.startOffset += (long)byteOffset;
            this.bytesWritten -= byteOffset;
        }
    }

    static {
        Runtime.getRuntime().addShutdownHook(new Thread(){

            @Override
            public void run() {
                for (FileMapStorage fms : undisposed) {
                    if (fms.contents != null) {
                        fms.contents.releaseBuffer();
                    }
                    if (fms.fileChannel != null && fms.fileChannel.isOpen()) {
                        try {
                            fms.fileChannel.close();
                        }
                        catch (IOException ex) {
                            Exceptions.printStackTrace((Throwable)ex);
                        }
                    }
                    if (fms.outfile == null) continue;
                    fms.outfile.delete();
                }
            }
        });
    }

    private class MappedBufferResource
    implements BufferResource<ByteBuffer> {
        private int refs;
        private ByteBuffer buffer;

        public MappedBufferResource(ByteBuffer buffer) {
            this.refs = 0;
            this.buffer = buffer;
        }

        @Override
        public void releaseBuffer() {
            if (this.buffer != null) {
                FileMapStorage.unmap(this.buffer);
                this.buffer = null;
            }
        }

        @Override
        public ByteBuffer getBuffer() {
            return this.buffer;
        }

        synchronized void incRefs() {
            ++this.refs;
        }

        synchronized void decRefs() {
            --this.refs;
            assert (this.refs >= 0);
            if (this.refs == 0) {
                FileMapStorage.unmap(this.buffer);
                this.buffer = null;
            }
        }
    }

    private class ChildBufferResource
    implements BufferResource<ByteBuffer> {
        private ByteBuffer buffer;
        private MappedBufferResource parentResource;

        public ChildBufferResource(ByteBuffer buffer, MappedBufferResource parentResource) {
            this.buffer = buffer;
            this.parentResource = parentResource;
            this.parentResource.incRefs();
        }

        @Override
        public ByteBuffer getBuffer() {
            return this.buffer;
        }

        @Override
        public void releaseBuffer() {
            this.buffer = null;
            this.parentResource.decRefs();
        }
    }

}

