/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.queries.VisibilityQueryImplementation2
 *  org.openide.filesystems.FileObject
 *  org.openide.util.ChangeSupport
 *  org.openide.util.NbPreferences
 */
package org.netbeans.modules.masterfs;

import java.io.File;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.event.ChangeListener;
import org.netbeans.spi.queries.VisibilityQueryImplementation2;
import org.openide.filesystems.FileObject;
import org.openide.util.ChangeSupport;
import org.openide.util.NbPreferences;

public class GlobalVisibilityQueryImpl
implements VisibilityQueryImplementation2 {
    private final ChangeSupport cs;
    private static final String PROP_IGNORED_FILES = "IgnoredFiles";
    private Pattern ignoreFilesPattern;

    public GlobalVisibilityQueryImpl() {
        this.cs = new ChangeSupport((Object)this);
        this.ignoreFilesPattern = null;
    }

    private static Preferences getPreferences() {
        return NbPreferences.root().node("/org/netbeans/core");
    }

    public boolean isVisible(FileObject file) {
        return this.isVisible(file.getNameExt());
    }

    public boolean isVisible(File file) {
        return this.isVisible(file.getName());
    }

    boolean isVisible(String fileName) {
        Pattern pattern = this.getIgnoreFilesPattern();
        return pattern != null ? !pattern.matcher(fileName).find() : true;
    }

    public void addChangeListener(ChangeListener l) {
        this.cs.addChangeListener(l);
    }

    public void removeChangeListener(ChangeListener l) {
        this.cs.removeChangeListener(l);
    }

    private Pattern getIgnoreFilesPattern() {
        if (this.ignoreFilesPattern == null) {
            String ignoredFiles = this.getIgnoredFiles();
            this.ignoreFilesPattern = ignoredFiles != null && ignoredFiles.length() > 0 ? Pattern.compile(ignoredFiles) : null;
        }
        return this.ignoreFilesPattern;
    }

    protected String getIgnoredFiles() {
        String retval = GlobalVisibilityQueryImpl.getPreferences().get("IgnoredFiles", "^(CVS|SCCS|vssver.?\\.scc|#.*#|%.*%|_svn)$|~$|^\\.(?!(htaccess|git.+|hgignore)$).*$");
        GlobalVisibilityQueryImpl.getPreferences().addPreferenceChangeListener(new PreferenceChangeListener(){

            @Override
            public void preferenceChange(PreferenceChangeEvent evt) {
                if ("IgnoredFiles".equals(evt.getKey())) {
                    GlobalVisibilityQueryImpl.this.ignoreFilesPattern = null;
                    GlobalVisibilityQueryImpl.this.cs.fireChange();
                }
            }
        });
        return retval;
    }

}

