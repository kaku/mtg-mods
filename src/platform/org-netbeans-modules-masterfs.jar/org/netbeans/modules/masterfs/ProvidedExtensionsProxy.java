/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 */
package org.netbeans.modules.masterfs;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.masterfs.ProvidedExtensionsAccessor;
import org.netbeans.modules.masterfs.providers.AnnotationProvider;
import org.netbeans.modules.masterfs.providers.InterceptionListener;
import org.netbeans.modules.masterfs.providers.ProvidedExtensions;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;

public class ProvidedExtensionsProxy
extends ProvidedExtensions {
    private Collection<AnnotationProvider> annotationProviders;
    private static ThreadLocal reentrantCheck = new ThreadLocal();

    public ProvidedExtensionsProxy(Collection annotationProviders) {
        this.annotationProviders = annotationProviders;
    }

    @Override
    public ProvidedExtensions.IOHandler getCopyHandler(File from, File to) {
        if (to == null) {
            return null;
        }
        DelegatingIOHandler retValue = null;
        Iterator<AnnotationProvider> it = this.annotationProviders.iterator();
        while (it.hasNext() && retValue == null) {
            AnnotationProvider provider = it.next();
            InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (!(iListener instanceof ProvidedExtensions)) continue;
            ProvidedExtensions.IOHandler delgate = ((ProvidedExtensions)iListener).getCopyHandler(from, to);
            retValue = delgate != null ? new DelegatingIOHandler(delgate) : null;
        }
        return retValue;
    }

    @Override
    public ProvidedExtensions.DeleteHandler getDeleteHandler(File f) {
        DelegatingDeleteHandler retValue = null;
        Iterator<AnnotationProvider> it = this.annotationProviders.iterator();
        while (it.hasNext() && retValue == null) {
            AnnotationProvider provider = it.next();
            InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (!(iListener instanceof ProvidedExtensions)) continue;
            ProvidedExtensions.DeleteHandler delegate = ((ProvidedExtensions)iListener).getDeleteHandler(f);
            retValue = delegate != null ? new DelegatingDeleteHandler(delegate) : null;
        }
        return retValue;
    }

    @Override
    public ProvidedExtensions.IOHandler getRenameHandler(File from, String newName) {
        File to = new File(from.getParentFile(), newName);
        DelegatingIOHandler retValue = null;
        Iterator<AnnotationProvider> it = this.annotationProviders.iterator();
        while (it.hasNext() && retValue == null) {
            AnnotationProvider provider = it.next();
            InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (!(iListener instanceof ProvidedExtensions)) continue;
            ProvidedExtensions.IOHandler delgate = ((ProvidedExtensions)iListener).getRenameHandler(from, newName);
            retValue = delgate != null ? new DelegatingIOHandler(delgate) : null;
        }
        return retValue;
    }

    @Override
    public ProvidedExtensions.IOHandler getMoveHandler(File from, File to) {
        if (to == null) {
            return null;
        }
        DelegatingIOHandler retValue = null;
        Iterator<AnnotationProvider> it = this.annotationProviders.iterator();
        while (it.hasNext() && retValue == null) {
            AnnotationProvider provider = it.next();
            InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (!(iListener instanceof ProvidedExtensions)) continue;
            ProvidedExtensions.IOHandler delgate = ((ProvidedExtensions)iListener).getMoveHandler(from, to);
            retValue = delgate != null ? new DelegatingIOHandler(delgate) : null;
        }
        return retValue;
    }

    @Override
    public void createFailure(final FileObject parent, final String name, final boolean isFolder) {
        for (AnnotationProvider provider : this.annotationProviders) {
            final InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (iListener == null) continue;
            ProvidedExtensionsProxy.runCheckCode(new Runnable(){

                @Override
                public void run() {
                    iListener.createFailure(parent, name, isFolder);
                }
            });
        }
    }

    @Override
    public void beforeCreate(final FileObject parent, final String name, final boolean isFolder) {
        for (AnnotationProvider provider : this.annotationProviders) {
            final InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (iListener == null) continue;
            ProvidedExtensionsProxy.runCheckCode(new Runnable(){

                @Override
                public void run() {
                    iListener.beforeCreate(parent, name, isFolder);
                }
            });
        }
    }

    @Override
    public void deleteSuccess(final FileObject fo) {
        for (AnnotationProvider provider : this.annotationProviders) {
            final InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (iListener == null) continue;
            ProvidedExtensionsProxy.runCheckCode(new Runnable(){

                @Override
                public void run() {
                    iListener.deleteSuccess(fo);
                }
            });
        }
    }

    @Override
    public void deleteFailure(final FileObject fo) {
        for (AnnotationProvider provider : this.annotationProviders) {
            final InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (iListener == null) continue;
            ProvidedExtensionsProxy.runCheckCode(new Runnable(){

                @Override
                public void run() {
                    iListener.deleteFailure(fo);
                }
            });
        }
    }

    @Override
    public void createSuccess(final FileObject fo) {
        for (AnnotationProvider provider : this.annotationProviders) {
            final InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (iListener == null) continue;
            ProvidedExtensionsProxy.runCheckCode(new Runnable(){

                @Override
                public void run() {
                    iListener.createSuccess(fo);
                }
            });
        }
    }

    @Override
    public void beforeDelete(final FileObject fo) {
        for (AnnotationProvider provider : this.annotationProviders) {
            final InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (iListener == null) continue;
            ProvidedExtensionsProxy.runCheckCode(new Runnable(){

                @Override
                public void run() {
                    iListener.beforeDelete(fo);
                }
            });
        }
    }

    @Override
    public boolean canWrite(final File f) {
        final Boolean[] ret = new Boolean[]{null};
        for (AnnotationProvider provider : this.annotationProviders) {
            final InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (!(iListener instanceof ProvidedExtensions)) continue;
            ProvidedExtensionsProxy.runCheckCode(new Runnable(){

                @Override
                public void run() {
                    ProvidedExtensions extension = (ProvidedExtensions)iListener;
                    if (ProvidedExtensionsAccessor.IMPL != null && ProvidedExtensionsAccessor.IMPL.providesCanWrite(extension)) {
                        ret[0] = ((ProvidedExtensions)iListener).canWrite(f);
                    }
                }
            });
            if (ret[0] == null || !ret[0].booleanValue()) continue;
            break;
        }
        return ret[0] != null ? ret[0].booleanValue() : super.canWrite(f);
    }

    @Override
    public void beforeChange(final FileObject f) {
        for (AnnotationProvider provider : this.annotationProviders) {
            final InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (!(iListener instanceof ProvidedExtensions)) continue;
            ProvidedExtensionsProxy.runCheckCode(new Runnable(){

                @Override
                public void run() {
                    ((ProvidedExtensions)iListener).beforeChange(f);
                }
            });
        }
    }

    @Override
    public void fileLocked(final FileObject fo) throws IOException {
        for (AnnotationProvider provider : this.annotationProviders) {
            final InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (!(iListener instanceof ProvidedExtensions)) continue;
            ProvidedExtensionsProxy.runCheckCode(new FileSystem.AtomicAction(){

                public void run() throws IOException {
                    ((ProvidedExtensions)iListener).fileLocked(fo);
                }
            });
        }
    }

    @Override
    public void fileUnlocked(final FileObject fo) {
        for (AnnotationProvider provider : this.annotationProviders) {
            final InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (!(iListener instanceof ProvidedExtensions)) continue;
            ProvidedExtensionsProxy.runCheckCode(new Runnable(){

                @Override
                public void run() {
                    ((ProvidedExtensions)iListener).fileUnlocked(fo);
                }
            });
        }
    }

    @Override
    public Object getAttribute(final File file, final String attrName) {
        final AtomicReference value = new AtomicReference();
        for (AnnotationProvider provider : this.annotationProviders) {
            InterceptionListener iListener;
            InterceptionListener interceptionListener = iListener = provider != null ? provider.getInterceptionListener() : null;
            if (iListener instanceof ProvidedExtensions) {
                ProvidedExtensionsProxy.runCheckCode(new Runnable(){

                    @Override
                    public void run() {
                        value.set(((ProvidedExtensions)iListener).getAttribute(file, attrName));
                    }
                });
            }
            if (value.get() == null) continue;
            return value.get();
        }
        return null;
    }

    @Override
    public long refreshRecursively(File dir, long lastTimeStamp, List<? super File> children) {
        for (AnnotationProvider provider : this.annotationProviders) {
            InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (!(iListener instanceof ProvidedExtensions)) continue;
            ProvidedExtensions pe = (ProvidedExtensions)iListener;
            int prev = children.size();
            long ret = pe.refreshRecursively(dir, lastTimeStamp, children);
            assert (ret != -1 || prev == children.size());
            if (ret == -1) continue;
            return ret;
        }
        File[] arr = dir.listFiles();
        if (arr != null) {
            children.addAll(Arrays.asList(arr));
        }
        return 0;
    }

    @Override
    public void createdExternally(final FileObject fo) {
        for (AnnotationProvider provider : this.annotationProviders) {
            final InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (!(iListener instanceof ProvidedExtensions)) continue;
            ProvidedExtensionsProxy.runCheckCode(new Runnable(){

                @Override
                public void run() {
                    ((ProvidedExtensions)iListener).createdExternally(fo);
                }
            });
        }
    }

    @Override
    public void deletedExternally(final FileObject fo) {
        for (AnnotationProvider provider : this.annotationProviders) {
            final InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (!(iListener instanceof ProvidedExtensions)) continue;
            ProvidedExtensionsProxy.runCheckCode(new Runnable(){

                @Override
                public void run() {
                    ((ProvidedExtensions)iListener).deletedExternally(fo);
                }
            });
        }
    }

    @Override
    public void fileChanged(final FileObject fo) {
        for (AnnotationProvider provider : this.annotationProviders) {
            final InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (!(iListener instanceof ProvidedExtensions)) continue;
            ProvidedExtensionsProxy.runCheckCode(new Runnable(){

                @Override
                public void run() {
                    ((ProvidedExtensions)iListener).fileChanged(fo);
                }
            });
        }
    }

    @Override
    public void beforeMove(final FileObject from, final File to) {
        if (to == null) {
            return;
        }
        for (AnnotationProvider provider : this.annotationProviders) {
            final InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (!(iListener instanceof ProvidedExtensions)) continue;
            ProvidedExtensionsProxy.runCheckCode(new Runnable(){

                @Override
                public void run() {
                    ((ProvidedExtensions)iListener).beforeMove(from, to);
                }
            });
        }
    }

    @Override
    public void moveSuccess(final FileObject from, final File to) {
        if (to == null) {
            return;
        }
        for (AnnotationProvider provider : this.annotationProviders) {
            final InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (!(iListener instanceof ProvidedExtensions)) continue;
            ProvidedExtensionsProxy.runCheckCode(new Runnable(){

                @Override
                public void run() {
                    ((ProvidedExtensions)iListener).moveSuccess(from, to);
                }
            });
        }
    }

    @Override
    public void moveFailure(final FileObject from, final File to) {
        if (to == null) {
            return;
        }
        for (AnnotationProvider provider : this.annotationProviders) {
            final InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (!(iListener instanceof ProvidedExtensions)) continue;
            ProvidedExtensionsProxy.runCheckCode(new Runnable(){

                @Override
                public void run() {
                    ((ProvidedExtensions)iListener).moveFailure(from, to);
                }
            });
        }
    }

    @Override
    public void beforeCopy(final FileObject from, final File to) {
        if (to == null) {
            return;
        }
        for (AnnotationProvider provider : this.annotationProviders) {
            final InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (!(iListener instanceof ProvidedExtensions)) continue;
            ProvidedExtensionsProxy.runCheckCode(new Runnable(){

                @Override
                public void run() {
                    ((ProvidedExtensions)iListener).beforeCopy(from, to);
                }
            });
        }
    }

    @Override
    public void copySuccess(final FileObject from, final File to) {
        if (to == null) {
            return;
        }
        for (AnnotationProvider provider : this.annotationProviders) {
            final InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (!(iListener instanceof ProvidedExtensions)) continue;
            ProvidedExtensionsProxy.runCheckCode(new Runnable(){

                @Override
                public void run() {
                    ((ProvidedExtensions)iListener).copySuccess(from, to);
                }
            });
        }
    }

    @Override
    public void copyFailure(final FileObject from, final File to) {
        if (to == null) {
            return;
        }
        for (AnnotationProvider provider : this.annotationProviders) {
            final InterceptionListener iListener = provider != null ? provider.getInterceptionListener() : null;
            if (!(iListener instanceof ProvidedExtensions)) continue;
            ProvidedExtensionsProxy.runCheckCode(new Runnable(){

                @Override
                public void run() {
                    ((ProvidedExtensions)iListener).copyFailure(from, to);
                }
            });
        }
    }

    public static void checkReentrancy() {
        if (reentrantCheck.get() != null) {
            Logger.getLogger("org.netbeans.modules.masterfs.ProvidedExtensionsProxy").log(Level.INFO, "Unexpected reentrant call", new Throwable());
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void runCheckCode(Runnable code) {
        try {
            reentrantCheck.set(Boolean.TRUE);
            code.run();
        }
        finally {
            reentrantCheck.set(null);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void runCheckCode(FileSystem.AtomicAction code) throws IOException {
        try {
            reentrantCheck.set(Boolean.TRUE);
            code.run();
        }
        finally {
            reentrantCheck.set(null);
        }
    }

    private class DelegatingIOHandler
    implements ProvidedExtensions.IOHandler {
        private ProvidedExtensions.IOHandler delegate;

        private DelegatingIOHandler(ProvidedExtensions.IOHandler delegate) {
            this.delegate = delegate;
        }

        @Override
        public void handle() throws IOException {
            final IOException[] retval = new IOException[1];
            ProvidedExtensionsProxy.runCheckCode(new Runnable(){

                @Override
                public void run() {
                    try {
                        DelegatingIOHandler.this.delegate.handle();
                    }
                    catch (IOException ex) {
                        retval[0] = ex;
                    }
                }
            });
            if (retval[0] != null) {
                throw retval[0];
            }
        }

    }

    private class DelegatingDeleteHandler
    implements ProvidedExtensions.DeleteHandler {
        private ProvidedExtensions.DeleteHandler delegate;

        private DelegatingDeleteHandler(ProvidedExtensions.DeleteHandler delegate) {
            this.delegate = delegate;
        }

        @Override
        public boolean delete(final File file) {
            final boolean[] retval = new boolean[1];
            ProvidedExtensionsProxy.runCheckCode(new Runnable(){

                @Override
                public void run() {
                    retval[0] = DelegatingDeleteHandler.this.delegate.delete(file);
                }
            });
            return retval[0];
        }

    }

}

