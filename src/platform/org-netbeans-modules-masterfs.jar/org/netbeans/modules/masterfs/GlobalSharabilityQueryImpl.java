/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.queries.SharabilityQuery
 *  org.netbeans.api.queries.SharabilityQuery$Sharability
 *  org.netbeans.spi.queries.SharabilityQueryImplementation2
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.masterfs;

import java.net.URI;
import org.netbeans.api.queries.SharabilityQuery;
import org.netbeans.modules.masterfs.GlobalVisibilityQueryImpl;
import org.netbeans.spi.queries.SharabilityQueryImplementation2;
import org.openide.util.Lookup;

public class GlobalSharabilityQueryImpl
implements SharabilityQueryImplementation2 {
    private GlobalVisibilityQueryImpl visibilityQuery;

    public SharabilityQuery.Sharability getSharability(URI uri) {
        if (this.visibilityQuery == null) {
            this.visibilityQuery = (GlobalVisibilityQueryImpl)Lookup.getDefault().lookup(GlobalVisibilityQueryImpl.class);
            assert (this.visibilityQuery != null);
        }
        return this.visibilityQuery.isVisible(uri.toString().replaceFirst(".+/", "")) ? SharabilityQuery.Sharability.UNKNOWN : SharabilityQuery.Sharability.NOT_SHARABLE;
    }
}

