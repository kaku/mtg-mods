/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.AbstractFileSystem
 *  org.openide.filesystems.AbstractFileSystem$Change
 *  org.openide.filesystems.AbstractFileSystem$Info
 *  org.openide.filesystems.AbstractFileSystem$List
 *  org.openide.filesystems.DefaultAttributes
 *  org.openide.filesystems.FileUtil
 *  org.openide.modules.Places
 *  org.openide.util.Exceptions
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.masterfs.providers;

import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import org.netbeans.modules.masterfs.ExLocalFileSystem;
import org.netbeans.modules.masterfs.filebasedfs.utils.FileChangedManager;
import org.openide.filesystems.AbstractFileSystem;
import org.openide.filesystems.DefaultAttributes;
import org.openide.filesystems.FileUtil;
import org.openide.modules.Places;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;

public class Attributes
extends DefaultAttributes {
    public static String ATTRNAME = "attributes.xml";
    private static final String LOCATION = "var";
    private static DefaultAttributes sharedUserAttributes;
    private final String attributePrefix;
    private AbstractFileSystem.List list;
    private static final boolean BACKWARD_COMPATIBILITY = false;
    private static File rootForAttributes;

    public Attributes(File mountPoint, AbstractFileSystem.Info info, AbstractFileSystem.Change change, AbstractFileSystem.List list) {
        super(info, change, list);
        this.list = list;
        this.attributePrefix = this.preparePrefix(mountPoint);
    }

    public Attributes(AbstractFileSystem.Info info, AbstractFileSystem.Change change, AbstractFileSystem.List list) {
        super(info, change, list);
        this.list = list;
        this.attributePrefix = "";
    }

    private String preparePrefix(File fileSystemRoot) {
        fileSystemRoot = FileUtil.normalizeFile((File)fileSystemRoot);
        String rootPath = fileSystemRoot.getAbsolutePath().replace('\\', '/');
        return Utilities.isWindows() || Utilities.getOperatingSystem() == 2048 ? rootPath.toLowerCase() : rootPath;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static File getRootForAttributes() {
        Class<ExLocalFileSystem> class_ = ExLocalFileSystem.class;
        synchronized (ExLocalFileSystem.class) {
            if (rootForAttributes == null) {
                File userDir = Places.getUserDirectory();
                if (userDir != null) {
                    rootForAttributes = new File(userDir, "var");
                } else {
                    rootForAttributes = new File(System.getProperty("java.io.tmpdir"));
                    File tmpAttrs = new File(rootForAttributes, ATTRNAME);
                    if (FileChangedManager.getInstance().exists(tmpAttrs)) {
                        tmpAttrs.delete();
                    }
                    tmpAttrs.deleteOnExit();
                }
                if (!FileChangedManager.getInstance().exists(rootForAttributes)) {
                    rootForAttributes.mkdirs();
                }
            }
            // ** MonitorExit[var0] (shouldn't be in output)
            return rootForAttributes;
        }
    }

    public String[] children(String f) {
        return this.list.children(f);
    }

    public Object readAttribute(String name, String attrName) {
        Object retVal;
        String translatedName = this.translateName(name);
        DefaultAttributes pa = this.getPreferedAttributes();
        Object object = retVal = pa == null ? null : pa.readAttribute(translatedName, attrName);
        if (retVal == null && this.isBackwardCompatible() && (retVal = super.readAttribute(name, attrName)) != null) {
            this.copyAllToUserDir(name, super.attributes(name));
            retVal = this.getPreferedAttributes().readAttribute(translatedName, attrName);
        }
        return retVal;
    }

    public void writeAttribute(String name, String attrName, Object value) throws IOException {
        this.getPreferedAttributes().writeAttribute(this.translateName(name), attrName, value);
    }

    public synchronized Enumeration<String> attributes(String name) {
        Enumeration<String> retVal = this.getPreferedAttributes().attributes(this.translateName(name));
        if ((retVal == null || !retVal.hasMoreElements()) && this.isBackwardCompatible()) {
            retVal = this.copyAllToUserDir(name, super.attributes(name));
        }
        return retVal;
    }

    private Enumeration<String> copyAllToUserDir(String name, Enumeration<String> attributeNames) {
        if (attributeNames != null && attributeNames.hasMoreElements() && this.isBackwardCompatible()) {
            String translatedName = this.translateName(name);
            while (attributeNames.hasMoreElements()) {
                String attrName = attributeNames.nextElement();
                Object value = super.readAttribute(name, attrName);
                try {
                    this.getPreferedAttributes().writeAttribute(translatedName, attrName, value);
                }
                catch (IOException e) {
                    Exceptions.printStackTrace((Throwable)e);
                }
            }
            super.deleteAttributes(name);
            attributeNames = this.getPreferedAttributes().attributes(translatedName);
        }
        return attributeNames;
    }

    public synchronized void renameAttributes(String oldName, String newName) {
        if (this.isBackwardCompatible()) {
            this.copyAllToUserDir(oldName, super.attributes(oldName));
        }
        this.getPreferedAttributes().renameAttributes(this.translateName(oldName), this.translateName(newName));
    }

    public synchronized void deleteAttributes(String name) {
        if (this.isBackwardCompatible()) {
            super.deleteAttributes(name);
        }
        this.getPreferedAttributes().deleteAttributes(this.translateName(name));
    }

    private String translateName(String name) {
        return this.attributePrefix.endsWith("/") ? this.attributePrefix + "/" + name : this.attributePrefix + name;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private DefaultAttributes getPreferedAttributes() {
        Class<Attributes> class_ = Attributes.class;
        synchronized (Attributes.class) {
            if (sharedUserAttributes == null) {
                ExLocalFileSystem exLFs = null;
                try {
                    exLFs = ExLocalFileSystem.getInstance(Attributes.getRootForAttributes());
                }
                catch (PropertyVetoException e) {
                    Exceptions.printStackTrace((Throwable)e);
                }
                catch (IOException e) {
                    Exceptions.printStackTrace((Throwable)e);
                }
                sharedUserAttributes = exLFs.getAttributes();
            }
            // ** MonitorExit[var1_1] (shouldn't be in output)
            assert (sharedUserAttributes != null);
            return sharedUserAttributes != null ? sharedUserAttributes : this;
        }
    }

    private boolean isBackwardCompatible() {
        return false;
    }
}

