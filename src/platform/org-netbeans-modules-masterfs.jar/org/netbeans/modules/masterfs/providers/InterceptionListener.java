/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.masterfs.providers;

import org.openide.filesystems.FileObject;

public interface InterceptionListener {
    public void beforeCreate(FileObject var1, String var2, boolean var3);

    public void createSuccess(FileObject var1);

    public void createFailure(FileObject var1, String var2, boolean var3);

    public void beforeDelete(FileObject var1);

    public void deleteSuccess(FileObject var1);

    public void deleteFailure(FileObject var1);
}

