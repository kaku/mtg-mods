/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.masterfs.providers;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;
import org.netbeans.modules.masterfs.ProvidedExtensionsAccessor;
import org.netbeans.modules.masterfs.filebasedfs.utils.FileChangedManager;
import org.netbeans.modules.masterfs.providers.InterceptionListener;
import org.openide.filesystems.FileObject;

public class ProvidedExtensions
implements InterceptionListener {
    private final boolean providesCanWrite;

    public ProvidedExtensions(boolean providesCanWrite) {
        this.providesCanWrite = providesCanWrite;
        if (ProvidedExtensionsAccessor.IMPL == null) {
            ProvidedExtensionsAccessor.IMPL = new ProvidedExtensionsAccessor(){

                @Override
                public boolean providesCanWrite(ProvidedExtensions pe) {
                    return pe.providesCanWrite;
                }
            };
        }
    }

    public ProvidedExtensions() {
        this(false);
    }

    public IOHandler getCopyHandler(File from, File to) {
        return null;
    }

    public IOHandler getMoveHandler(File from, File to) {
        return null;
    }

    public IOHandler getRenameHandler(File from, String newName) {
        return null;
    }

    public DeleteHandler getDeleteHandler(File f) {
        return null;
    }

    @Override
    public void createSuccess(FileObject fo) {
    }

    @Override
    public void createFailure(FileObject parent, String name, boolean isFolder) {
    }

    @Override
    public void beforeCreate(FileObject parent, String name, boolean isFolder) {
    }

    @Override
    public void deleteSuccess(FileObject fo) {
    }

    @Override
    public void deleteFailure(FileObject fo) {
    }

    @Override
    public void beforeDelete(FileObject fo) {
    }

    public void createdExternally(FileObject fo) {
    }

    public void deletedExternally(FileObject fo) {
    }

    public void fileChanged(FileObject fo) {
    }

    public void beforeCopy(FileObject from, File to) {
    }

    public void copySuccess(FileObject from, File to) {
    }

    public void copyFailure(FileObject from, File to) {
    }

    public void beforeMove(FileObject from, File to) {
    }

    public void moveSuccess(FileObject from, File to) {
    }

    public void moveFailure(FileObject from, File to) {
    }

    public boolean canWrite(File f) {
        return f.canWrite();
    }

    public void beforeChange(FileObject fo) {
    }

    public void fileLocked(FileObject fo) throws IOException {
    }

    public void fileUnlocked(FileObject fo) {
    }

    public Object getAttribute(File file, String attrName) {
        return null;
    }

    public long refreshRecursively(File dir, long lastTimeStamp, List<? super File> children) {
        return -1;
    }

    public static <T> T priorityIO(Callable<T> run) throws Exception {
        return FileChangedManager.priorityIO(run);
    }

    public static interface DeleteHandler {
        public boolean delete(File var1);
    }

    public static interface IOHandler {
        public void handle() throws IOException;
    }

}

