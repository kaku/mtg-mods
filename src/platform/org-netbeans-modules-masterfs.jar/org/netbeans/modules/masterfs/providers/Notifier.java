/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.masterfs.providers;

import java.io.IOException;
import org.netbeans.modules.masterfs.watcher.NotifierAccessor;

public abstract class Notifier<KEY> {
    protected abstract KEY addWatch(String var1) throws IOException;

    protected abstract void removeWatch(KEY var1) throws IOException;

    protected abstract String nextEvent() throws IOException, InterruptedException;

    protected abstract void start() throws IOException;

    protected void stop() throws IOException {
    }

    static {
        NotifierAccessor impl = new NotifierAccessor(){

            @Override
            protected <KEY> KEY addWatch(Notifier<KEY> n, String path) throws IOException {
                return n.addWatch(path);
            }

            @Override
            protected <KEY> void removeWatch(Notifier<KEY> n, KEY key) throws IOException {
                n.removeWatch(key);
            }

            @Override
            protected String nextEvent(Notifier<?> n) throws IOException, InterruptedException {
                return n.nextEvent();
            }

            @Override
            protected void start(Notifier<?> n) throws IOException {
                n.start();
            }

            @Override
            protected void stop(Notifier<?> n) throws IOException {
                n.stop();
            }
        };
    }

}

