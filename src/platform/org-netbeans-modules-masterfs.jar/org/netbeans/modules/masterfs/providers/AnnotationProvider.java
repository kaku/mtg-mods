/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStatusEvent
 *  org.openide.filesystems.FileStatusListener
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.masterfs.providers;

import java.awt.Image;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TooManyListenersException;
import javax.swing.Action;
import org.netbeans.modules.masterfs.providers.InterceptionListener;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStatusEvent;
import org.openide.filesystems.FileStatusListener;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

public abstract class AnnotationProvider {
    private List<FileStatusListener> fsStatusListener = new ArrayList<FileStatusListener>();
    private static final Object LOCK = new Object();

    public abstract String annotateName(String var1, Set<? extends FileObject> var2);

    public abstract Image annotateIcon(Image var1, int var2, Set<? extends FileObject> var3);

    public abstract String annotateNameHtml(String var1, Set<? extends FileObject> var2);

    public Action[] actions(Set<? extends FileObject> files) {
        return this.findExtrasFor(files).lookupAll(Action.class).toArray(new Action[0]);
    }

    public Lookup findExtrasFor(Set<? extends FileObject> files) {
        Action[] arr = this.actions(files);
        return arr == null ? null : Lookups.fixed((Object[])arr);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final void addFileStatusListener(FileStatusListener listener) throws TooManyListenersException {
        Object object = LOCK;
        synchronized (object) {
            this.fsStatusListener.add(listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final void removeFileStatusListener(FileStatusListener listener) {
        Object object = LOCK;
        synchronized (object) {
            this.fsStatusListener.remove((Object)listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected final void fireFileStatusChanged(FileStatusEvent event) {
        ArrayList<FileStatusListener> listeners = new ArrayList<FileStatusListener>();
        Object object = LOCK;
        synchronized (object) {
            listeners.addAll(this.fsStatusListener);
        }
        for (FileStatusListener fileStatusListener : listeners) {
            fileStatusListener.annotationChanged(event);
        }
    }

    public abstract InterceptionListener getInterceptionListener();
}

