/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.URLMapper
 */
package org.netbeans.modules.masterfs;

import java.net.URL;
import org.netbeans.modules.masterfs.filebasedfs.FileBasedURLMapper;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.URLMapper;

public final class MasterURLMapper
extends URLMapper {
    private static final URLMapper delegate = new FileBasedURLMapper();

    public FileObject[] getFileObjects(URL url) {
        return delegate.getFileObjects(url);
    }

    public URL getURL(FileObject fo, int type) {
        return delegate.getURL(fo, type);
    }
}

