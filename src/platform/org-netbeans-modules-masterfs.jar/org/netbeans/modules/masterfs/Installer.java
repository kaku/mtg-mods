/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.masterfs;

import org.netbeans.modules.masterfs.watcher.Watcher;

public final class Installer
implements Runnable {
    @Override
    public void run() {
        Watcher.isEnabled();
    }

    public static final class Down
    implements Runnable {
        @Override
        public void run() {
            Watcher.shutdown();
        }
    }

}

