/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.masterfs;

import org.netbeans.modules.masterfs.providers.ProvidedExtensions;

public abstract class ProvidedExtensionsAccessor {
    public static ProvidedExtensionsAccessor IMPL;

    public abstract boolean providesCanWrite(ProvidedExtensions var1);
}

