/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package org.netbeans.modules.masterfs.filebasedfs.fileobjects;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.concurrent.atomic.AtomicBoolean;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.BaseFileObj;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.FolderObj;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.RootObj;
import org.netbeans.modules.masterfs.filebasedfs.utils.FileChangedManager;
import org.netbeans.modules.masterfs.watcher.Watcher;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

final class RefreshSlow
extends AtomicBoolean
implements Runnable {
    private ActionEvent ref;
    private BaseFileObj preferrable;
    private int size;
    private int index;
    private boolean ignoreRecursiveListener;

    public RefreshSlow() {
        this.set(true);
        this.ignoreRecursiveListener = Watcher.isEnabled();
    }

    @Override
    public void run() {
        RootObj.invokeRefreshFor(this, File.listRoots(), this.ignoreRecursiveListener);
    }

    boolean refreshFileObject(final BaseFileObj fo, final boolean expected, int add) {
        final boolean[] b = new boolean[]{true};
        ActionEvent r = this.ref;
        final Runnable goingIdle = r instanceof Runnable ? (Runnable)((Object)r) : null;
        Runnable refresh = new Runnable(){
            boolean second;

            @Override
            public void run() {
                if (this.second) {
                    RefreshSlow.this.before();
                    fo.refresh(expected);
                    if (!RefreshSlow.this.after()) {
                        b[0] = false;
                        return;
                    }
                } else {
                    this.second = true;
                    FileChangedManager.idleIO(50, this, goingIdle, RefreshSlow.this);
                }
            }
        };
        FileUtil.runAtomicAction((Runnable)refresh);
        if (b[0]) {
            this.progress(add, fo);
        }
        return b[0];
    }

    void progress(int add, FileObject obj) {
        this.index += add;
        if (this.ref != null) {
            Object[] arr = new Object[]{this.index, this.size, obj, this, null};
            if (this.preferrable != null) {
                arr[4] = this.preferrable.getExistingParent();
            }
            this.ref.setSource(arr);
            if (arr[4] instanceof BaseFileObj) {
                this.preferrable = (BaseFileObj)((Object)arr[4]);
            }
        }
    }

    public boolean equals(Object obj) {
        if (obj instanceof ActionEvent) {
            this.ref = (ActionEvent)obj;
        }
        return Object.super.equals(obj);
    }

    public int hashCode() {
        return Object.super.hashCode();
    }

    void before() {
    }

    boolean after() {
        try {
            FileChangedManager.waitIOLoadLowerThan(50);
            return true;
        }
        catch (InterruptedException ex) {
            return false;
        }
    }

    BaseFileObj preferrable() {
        return this.preferrable;
    }

    void estimate(int cnt) {
        this.size = cnt;
    }

}

