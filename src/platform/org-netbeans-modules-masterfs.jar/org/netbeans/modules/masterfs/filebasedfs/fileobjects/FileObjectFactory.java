/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Privileged
 *  org.openide.util.Utilities
 *  org.openide.util.WeakSet
 */
package org.netbeans.modules.masterfs.filebasedfs.fileobjects;

import java.io.File;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.masterfs.filebasedfs.FileBasedFileSystem;
import org.netbeans.modules.masterfs.filebasedfs.Statistics;
import org.netbeans.modules.masterfs.filebasedfs.children.ChildrenCache;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.BaseFileObj;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.FileObj;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.FolderObj;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.RefreshSlow;
import org.netbeans.modules.masterfs.filebasedfs.naming.FileNaming;
import org.netbeans.modules.masterfs.filebasedfs.naming.NamingFactory;
import org.netbeans.modules.masterfs.filebasedfs.utils.FileChangedManager;
import org.netbeans.modules.masterfs.filebasedfs.utils.FileInfo;
import org.netbeans.modules.masterfs.filebasedfs.utils.Utils;
import org.netbeans.modules.masterfs.watcher.Watcher;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Mutex;
import org.openide.util.Utilities;
import org.openide.util.WeakSet;

public final class FileObjectFactory {
    public static final Map<File, FileObjectFactory> AllFactories = new HashMap<File, FileObjectFactory>();
    public static boolean WARNINGS = true;
    final Map<Integer, Object> allIBaseFileObjects = new WeakHashMap<Integer, Object>();
    final ReadWriteLock allIBaseLock = new ReentrantReadWriteLock();
    private BaseFileObj root;
    private static final Logger LOG_REFRESH = Logger.getLogger("org.netbeans.modules.masterfs.REFRESH");

    private FileObjectFactory(File rootFile) {
        this(new FileInfo(rootFile));
    }

    private FileObjectFactory(FileInfo fInfo) {
        this(fInfo, null);
    }

    private FileObjectFactory(FileInfo fInfo, Object msg) {
        BaseFileObj realRoot = this.create(fInfo);
        assert (realRoot != null);
        this.root = realRoot;
    }

    public static FileObjectFactory getInstance(File file) {
        return FileObjectFactory.getInstance(file, true);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static FileObjectFactory getInstance(File file, boolean addMising) {
        FileObjectFactory retVal = null;
        FileInfo rootInfo = new FileInfo(file).getRoot();
        File rootFile = rootInfo.getFile();
        Map<File, FileObjectFactory> map = AllFactories;
        synchronized (map) {
            retVal = AllFactories.get(rootFile);
        }
        if (retVal == null && addMising && rootInfo.isConvertibleToFileObject()) {
            map = AllFactories;
            synchronized (map) {
                retVal = AllFactories.get(rootFile);
                if (retVal == null) {
                    retVal = new FileObjectFactory(new FileInfo(rootFile), file);
                    AllFactories.put(rootFile, retVal);
                }
            }
        }
        return retVal;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static Collection<FileObjectFactory> getInstances() {
        Map<File, FileObjectFactory> map = AllFactories;
        synchronized (map) {
            return new ArrayList<FileObjectFactory>(AllFactories.values());
        }
    }

    public final BaseFileObj getRoot() {
        return this.root;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static int getFactoriesSize() {
        Map<File, FileObjectFactory> map = AllFactories;
        synchronized (map) {
            return AllFactories.size();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private List<FileObject> existingFileObjects() {
        ArrayList<Object> list = new ArrayList<Object>();
        this.allIBaseLock.readLock().lock();
        try {
            list.addAll(this.allIBaseFileObjects.values());
        }
        finally {
            this.allIBaseLock.readLock().unlock();
        }
        ArrayList<FileObject> res = new ArrayList<FileObject>();
        for (Object obj : list) {
            Collection all = obj instanceof Reference ? Collections.singleton(obj) : (List)obj;
            for (Object r : all) {
                Reference ref = (Reference)r;
                Object fo = ref == null ? null : ref.get();
                if (!(fo instanceof FileObject)) continue;
                res.add(fo);
            }
        }
        return res;
    }

    public int getSize() {
        List<FileObject> real = this.existingFileObjects();
        return real.size();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public BaseFileObj getFileObject(FileInfo fInfo, Caller caller) {
        int initTouch;
        File file = fInfo.getFile();
        FolderObj parent = BaseFileObj.getExistingParentFor(file, this);
        FileNaming child = null;
        boolean isInitializedCache = true;
        if (parent != null) {
            ChildrenCache childrenCache = parent.getChildrenCache();
            Mutex.Privileged mutexPrivileged = childrenCache.getMutexPrivileged();
            Runnable[] task = new Runnable[1];
            for (int i = 0; i < 2; ++i) {
                if (i == 1) {
                    if (task[0] == null) break;
                    task[0].run();
                }
                mutexPrivileged.enterReadAccess();
                try {
                    String nameExt = BaseFileObj.getNameExt(file);
                    isInitializedCache = childrenCache.isCacheInitialized();
                    child = childrenCache.getChild(nameExt, false, task);
                }
                finally {
                    mutexPrivileged.exitReadAccess();
                }
            }
        }
        int n = isInitializedCache ? -1 : (initTouch = child != null ? 1 : 0);
        if (initTouch == -1 && FileBasedFileSystem.isModificationInProgress()) {
            initTouch = file.exists() ? 1 : 0;
        }
        return this.issueIfExist(file, caller, parent, child, initTouch, caller.asynchFire());
    }

    private boolean checkCacheState(boolean expectedExists, File file, Caller caller) {
        if (!expectedExists && (caller.equals((Object)Caller.GetParent) || caller.equals((Object)Caller.ToFileObject))) {
            return true;
        }
        if (this.isWarningEnabled() && caller != null && !caller.equals((Object)Caller.GetChildern)) {
            boolean notsame;
            boolean realExists = file.exists();
            boolean bl = notsame = expectedExists != realExists;
            if (notsame) {
                File p;
                File[] children;
                if (!realExists && (p = file.getParentFile()) != null && (children = p.listFiles()) != null && Arrays.asList(children).contains(file)) {
                    return true;
                }
                this.printWarning(file);
            }
        }
        return true;
    }

    private Integer initRealExists(int initTouch) {
        Integer retval = new Integer(initTouch);
        return retval;
    }

    private void printWarning(File file) {
        StringBuilder sb = new StringBuilder("WARNING(please REPORT):  Externally ");
        sb.append(file.exists() ? "created " : "deleted ");
        sb.append(file.isDirectory() ? "folder: " : "file: ");
        sb.append(file.getAbsolutePath());
        sb.append(" (Possibly not refreshed FileObjects when external command finished.");
        sb.append(" For additional information see: http://wiki.netbeans.org/wiki/view/FileSystems)");
        IllegalStateException ise = new IllegalStateException(sb.toString());
        Logger.getLogger(this.getClass().getName()).log(Level.FINE, ise.getMessage(), ise);
    }

    private BaseFileObj issueIfExist(File file, Caller caller, FileObject parent, FileNaming child, int initTouch, boolean asyncFire) {
        boolean exist = false;
        BaseFileObj foForFile = null;
        Integer realExists = this.initRealExists(initTouch);
        FileChangedManager fcb = FileChangedManager.getInstance();
        foForFile = this.getCachedOnly(file);
        if (caller == Caller.Refresh && foForFile != null && !foForFile.isValid()) {
            fcb.impeachExistence(file, true);
            foForFile = null;
        }
        if (parent != null && parent.isValid()) {
            if (child != null) {
                if (foForFile == null) {
                    boolean bl = exist = realExists == -1 ? true : FileObjectFactory.touchExists(file, realExists);
                    if (fcb.impeachExistence(file, exist) && !(exist = FileObjectFactory.touchExists(file, realExists))) {
                        this.refreshFromGetter(parent, asyncFire);
                    }
                    assert (this.checkCacheState(true, file, caller));
                } else if (foForFile.isValid()) {
                    boolean bl = exist = realExists == -1 ? true : FileObjectFactory.touchExists(file, realExists);
                    if (fcb.impeachExistence(file, exist) && !(exist = FileObjectFactory.touchExists(file, realExists))) {
                        this.refreshFromGetter(parent, asyncFire);
                    }
                    assert (this.checkCacheState(exist, file, caller));
                } else {
                    exist = FileObjectFactory.touchExists(file, realExists);
                    if (!exist) {
                        this.refreshFromGetter(parent, asyncFire);
                    }
                }
            } else if (foForFile == null) {
                boolean bl = exist = realExists == -1 ? false : FileObjectFactory.touchExists(file, realExists);
                if (fcb.impeachExistence(file, exist) && exist != FileObjectFactory.touchExists(file, realExists)) {
                    exist = !exist;
                    this.refreshFromGetter(parent, asyncFire);
                }
                assert (this.checkCacheState(exist, file, caller));
            } else if (foForFile.isValid()) {
                exist = FileObjectFactory.touchExists(file, realExists);
                if (!exist) {
                    this.refreshFromGetter(foForFile, asyncFire);
                }
            } else {
                exist = FileObjectFactory.touchExists(file, realExists);
                if (exist) {
                    this.refreshFromGetter(parent, asyncFire);
                }
            }
        } else if (foForFile == null) {
            exist = caller == Caller.GetParent ? true : FileObjectFactory.touchExists(file, realExists);
        } else if (foForFile.isValid()) {
            if (parent == null) {
                boolean bl = exist = realExists == -1 ? true : FileObjectFactory.touchExists(file, realExists);
                if (fcb.impeachExistence(file, exist) && !(exist = FileObjectFactory.touchExists(file, realExists))) {
                    this.refreshFromGetter(foForFile, asyncFire);
                }
                assert (this.checkCacheState(exist, file, caller));
            } else {
                exist = FileObjectFactory.touchExists(file, realExists);
                if (!exist) {
                    this.refreshFromGetter(foForFile, asyncFire);
                }
            }
        } else {
            boolean bl = exist = realExists == -1 ? false : FileObjectFactory.touchExists(file, realExists);
            if (fcb.impeachExistence(file, exist)) {
                exist = FileObjectFactory.touchExists(file, realExists);
            }
            assert (this.checkCacheState(exist, file, caller));
        }
        if (!exist) {
            switch (caller) {
                case GetParent: {
                    BaseFileObj retval = null;
                    retval = foForFile != null && !foForFile.isRoot() ? foForFile : this.getOrCreate(new FileInfo(file, 1));
                    if (retval instanceof BaseFileObj && retval.isValid() && !(exist = FileObjectFactory.touchExists(file, realExists))) {
                        this.invalidateSubtree(retval, false, false);
                    }
                    assert (this.checkCacheState(exist, file, caller));
                    return retval;
                }
                case ToFileObject: {
                    exist = FileObjectFactory.touchExists(file, realExists);
                    if (exist && parent != null && parent.isValid()) {
                        this.refreshFromGetter(parent, asyncFire);
                    }
                    assert (this.checkCacheState(exist, file, caller));
                    break;
                }
            }
        }
        return exist ? this.getOrCreate(new FileInfo(file, 1)) : null;
    }

    private static boolean touchExists(File f, Integer state) {
        if (state == -1) {
            state = FileChangedManager.getInstance().exists(f) ? 1 : 0;
        }
        assert (state != -1);
        return state == 1;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private BaseFileObj getOrCreate(FileInfo fInfo) {
        boolean issue45485;
        BaseFileObj retVal = null;
        File f = fInfo.getFile();
        boolean bl = issue45485 = fInfo.isWindows() && f.getName().endsWith(".") && !f.getName().matches("[.]{1,2}");
        if (issue45485) {
            File f2 = FileUtil.normalizeFile((File)f);
            boolean bl2 = issue45485 = !f2.getName().endsWith(".");
            if (issue45485) {
                return null;
            }
        }
        this.allIBaseLock.writeLock().lock();
        try {
            Object parent;
            retVal = this.getCachedOnly(f);
            if (retVal == null || !retVal.isValid()) {
                parent = f.getParentFile();
                retVal = parent != null ? this.create(fInfo) : this.getRoot();
            }
            parent = retVal;
            return parent;
        }
        finally {
            this.allIBaseLock.writeLock().unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void invalidateSubtree(BaseFileObj root, boolean fire, boolean expected) {
        ArrayList<BaseFileObj> notify;
        notify = fire ? new ArrayList<BaseFileObj>() : Collections.emptyList();
        this.allIBaseLock.writeLock().lock();
        try {
            for (FileNaming fn : NamingFactory.findSubTree(root.getFileName())) {
                BaseFileObj cached = this.getCachedOnly(fn.getFile());
                if (cached == null || !cached.isValid()) continue;
                cached.invalidateFO(false, expected, false);
                if (!fire) continue;
                notify.add(cached);
            }
        }
        finally {
            this.allIBaseLock.writeLock().unlock();
        }
        for (BaseFileObj fo : notify) {
            fo.notifyDeleted(expected);
        }
    }

    public final String dumpObjects() {
        StringBuilder sb = new StringBuilder();
        for (FileObject fileObject : this.existingFileObjects()) {
            sb.append((Object)fileObject).append("\n");
        }
        return sb.toString();
    }

    private BaseFileObj create(FileInfo fInfo) {
        if (!fInfo.isConvertibleToFileObject()) {
            return null;
        }
        File file = fInfo.getFile();
        FileNaming name = fInfo.getFileNaming();
        FileNaming fileNaming = name = name == null ? NamingFactory.fromFile(file) : name;
        if (name == null) {
            return null;
        }
        if (name.isFile() && !name.isDirectory()) {
            FileObj realRoot = new FileObj(file, name);
            return this.putInCache(realRoot, realRoot.getFileName().getId());
        }
        if (!name.isFile() && name.isDirectory()) {
            FolderObj realRoot = new FolderObj(file, name);
            return this.putInCache(realRoot, realRoot.getFileName().getId());
        }
        assert (false);
        return null;
    }

    final void refreshAll(RefreshSlow slow, boolean ignoreRecursiveListeners, boolean expected) {
        Set<BaseFileObj> all2Refresh = this.collectForRefresh(ignoreRecursiveListeners);
        this.refresh(all2Refresh, slow, expected);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Set<BaseFileObj> collectForRefresh(boolean noRecListeners) {
        WeakSet all2Refresh;
        this.allIBaseLock.writeLock().lock();
        try {
            all2Refresh = new WeakSet(this.allIBaseFileObjects.size() * 3 + 11);
            for (Object obj : this.allIBaseFileObjects.values()) {
                if (obj instanceof List) {
                    for (WeakReference ref : (List)obj) {
                        BaseFileObj fo = this.shallBeChecked(ref != null ? (BaseFileObj)((Object)ref.get()) : null, noRecListeners);
                        if (fo == null) continue;
                        all2Refresh.add(fo);
                    }
                    continue;
                }
                WeakReference ref = (WeakReference)obj;
                BaseFileObj fo = this.shallBeChecked(ref != null ? (BaseFileObj)((Object)ref.get()) : null, noRecListeners);
                if (fo == null) continue;
                all2Refresh.add(fo);
            }
        }
        finally {
            this.allIBaseLock.writeLock().unlock();
        }
        all2Refresh.remove((Object)this.root);
        return all2Refresh;
    }

    private BaseFileObj shallBeChecked(BaseFileObj fo, boolean noRecListeners) {
        FolderObj p;
        if (fo != null && noRecListeners && (p = (FolderObj)(fo instanceof FolderObj ? fo : fo.getExistingParent())) != null && Watcher.isWatched(fo)) {
            LOG_REFRESH.log(Level.FINER, "skip: {0}", (Object)fo);
            fo = null;
        }
        return fo;
    }

    private /* varargs */ boolean refresh(Set<BaseFileObj> all2Refresh, RefreshSlow slow, File ... files) {
        return this.refresh(all2Refresh, slow, true, files);
    }

    private static boolean isInFiles(BaseFileObj fo, File[] files) {
        if (fo == null) {
            return false;
        }
        if (files == null) {
            return true;
        }
        for (File file : files) {
            if (!FileObjectFactory.isParentOf(file, fo.getFileName().getFile())) continue;
            return true;
        }
        return false;
    }

    private boolean refresh(Set<BaseFileObj> all2Refresh, RefreshSlow slow, boolean expected) {
        return this.refresh(all2Refresh, slow, expected, null);
    }

    private boolean refresh(Set<BaseFileObj> all2Refresh, RefreshSlow slow, boolean expected, File[] files) {
        int add = 0;
        Iterator<BaseFileObj> it = all2Refresh.iterator();
        while (it.hasNext()) {
            BaseFileObj pref;
            BaseFileObj fo = null;
            if (slow != null && (pref = slow.preferrable()) != null && all2Refresh.remove((Object)pref)) {
                LOG_REFRESH.log(Level.FINER, "Preferring {0}", (Object)pref);
                fo = pref;
                it = all2Refresh.iterator();
            }
            if (fo == null) {
                fo = it.next();
                it.remove();
            }
            ++add;
            if (!FileObjectFactory.isInFiles(fo, files)) continue;
            if (slow != null) {
                if (!slow.refreshFileObject(fo, expected, add)) {
                    return false;
                }
                add = 0;
                continue;
            }
            fo.refresh(expected);
        }
        return true;
    }

    public static boolean isParentOf(File dir, File file) {
        File tempFile;
        for (tempFile = file; tempFile != null && !Utils.equals(tempFile, dir); tempFile = tempFile.getParentFile()) {
        }
        return tempFile != null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final void rename(Set<BaseFileObj> changeId) {
        HashMap<Integer, Object> toRename = new HashMap<Integer, Object>();
        this.allIBaseLock.writeLock().lock();
        try {
            Integer key;
            for (Map.Entry<Integer, Object> entry : this.allIBaseFileObjects.entrySet()) {
                Object obj = entry.getValue();
                key = entry.getKey();
                if (!(obj instanceof List)) {
                    WeakReference ref = (WeakReference)obj;
                    BaseFileObj fo = ref != null ? (BaseFileObj)((Object)ref.get()) : null;
                    if (!changeId.contains((Object)fo)) continue;
                    toRename.put(key, (Object)fo);
                    continue;
                }
                for (WeakReference ref : (List)obj) {
                    BaseFileObj fo = ref != null ? (BaseFileObj)((Object)ref.get()) : null;
                    if (!changeId.contains((Object)fo)) continue;
                    toRename.put(key, ref);
                }
            }
            for (Map.Entry entry2 : toRename.entrySet()) {
                key = (Integer)entry2.getKey();
                Object previous = this.allIBaseFileObjects.remove(key);
                if (previous instanceof List) {
                    List list = (List)previous;
                    list.remove(entry2.getValue());
                    this.allIBaseFileObjects.put(key, previous);
                    continue;
                }
                BaseFileObj bfo = (BaseFileObj)((Object)entry2.getValue());
                this.putInCache(bfo, bfo.getFileName().getId());
            }
        }
        finally {
            this.allIBaseLock.writeLock().unlock();
        }
    }

    public final BaseFileObj getCachedOnly(File file) {
        return this.getCachedOnly(file, true);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final BaseFileObj getCachedOnly(File file, boolean checkExtension) {
        BaseFileObj retval;
        Integer id = NamingFactory.createID(file);
        this.allIBaseLock.readLock().lock();
        try {
            Object value = this.allIBaseFileObjects.get(id);
            retval = value instanceof Reference ? FileObjectFactory.getReference(Collections.nCopies(1, value), file) : FileObjectFactory.getReference((List)value, file);
        }
        finally {
            this.allIBaseLock.readLock().unlock();
        }
        if (retval != null && checkExtension && !file.getName().equals(retval.getNameExt()) && !Utils.equals(file, retval.getFileName().getFile())) {
            retval = null;
        }
        return retval;
    }

    private static BaseFileObj getReference(List<?> list, File file) {
        BaseFileObj retVal = null;
        if (list != null) {
            for (int i = 0; retVal == null && i < list.size(); ++i) {
                BaseFileObj cachedElement;
                Object ce;
                Object item = list.get(i);
                if (!(item instanceof Reference) || !((ce = ((Reference)item).get()) instanceof BaseFileObj) || (cachedElement = (BaseFileObj)((Object)ce)) == null || cachedElement.getFileName().getFile().compareTo(file) != 0) continue;
                retVal = cachedElement;
            }
        }
        return retVal;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private BaseFileObj putInCache(BaseFileObj newValue, Integer id) {
        this.allIBaseLock.writeLock().lock();
        try {
            WeakReference<BaseFileObj> newRef = new WeakReference<BaseFileObj>(newValue);
            WeakReference<BaseFileObj> listOrReference = this.allIBaseFileObjects.put(id, newRef);
            if (listOrReference != null) {
                if (listOrReference instanceof List) {
                    List list = (List)((Object)listOrReference);
                    list.add(newRef);
                    this.allIBaseFileObjects.put(id, listOrReference);
                } else {
                    BaseFileObj oldValue;
                    assert (listOrReference instanceof WeakReference);
                    Reference oldRef = listOrReference;
                    BaseFileObj baseFileObj = oldValue = oldRef != null ? (BaseFileObj)((Object)oldRef.get()) : null;
                    if (oldValue != null && !newValue.getFileName().equals(oldValue.getFileName())) {
                        ArrayList<Reference> l = new ArrayList<Reference>();
                        l.add(oldRef);
                        l.add(newRef);
                        this.allIBaseFileObjects.put(id, l);
                    }
                }
            }
        }
        finally {
            this.allIBaseLock.writeLock().unlock();
        }
        return newValue;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String toString() {
        ArrayList<Object> list = new ArrayList<Object>();
        this.allIBaseLock.readLock().lock();
        try {
            list.addAll(this.allIBaseFileObjects.values());
        }
        finally {
            this.allIBaseLock.readLock().unlock();
        }
        ArrayList<String> l2 = new ArrayList<String>();
        for (Reference ref : list) {
            FileObject fo = ref != null ? (FileObject)ref.get() : null;
            if (fo == null) continue;
            l2.add(fo.getPath());
        }
        return l2.toString();
    }

    public static synchronized Map<File, FileObjectFactory> factories() {
        return new HashMap<File, FileObjectFactory>(AllFactories);
    }

    public boolean isWarningEnabled() {
        return WARNINGS && !Utilities.isMac();
    }

    public static void reinitForTests() {
        AllFactories.clear();
    }

    public final BaseFileObj getValidFileObject(File f, Caller caller) {
        BaseFileObj retVal = this.getFileObject(new FileInfo(f), caller);
        return retVal != null && retVal.isValid() ? retVal : null;
    }

    public void refresh(boolean expected) {
        this.refresh(null, expected);
    }

    void refresh(RefreshSlow slow, boolean expected) {
        this.refresh(slow, false, expected);
    }

    final void refresh(final RefreshSlow slow, final boolean ignoreRecursiveListeners, final boolean expected) {
        Statistics.StopWatch stopWatch = Statistics.getStopWatch(Statistics.REFRESH_FS);
        final Runnable r = new Runnable(){

            @Override
            public void run() {
                FileObjectFactory.this.refreshAll(slow, ignoreRecursiveListeners, expected);
            }
        };
        stopWatch.start();
        try {
            if (slow != null) {
                FileBasedFileSystem.runAsInconsistent(r);
            } else {
                FileBasedFileSystem.getInstance().runAtomicAction(new FileSystem.AtomicAction(){

                    public void run() throws IOException {
                        FileBasedFileSystem.runAsInconsistent(r);
                    }
                });
            }
        }
        catch (IOException iex) {
            // empty catch block
        }
        stopWatch.stop();
        if (LOG_REFRESH.isLoggable(Level.FINE)) {
            LOG_REFRESH.log(Level.FINE, "FS.refresh statistics ({0}FileObjects):\n  {1}\n  {2}\n  {3}\n  {4}\n", new Object[]{Statistics.fileObjects(), Statistics.REFRESH_FS.toString(), Statistics.LISTENERS_CALLS.toString(), Statistics.REFRESH_FOLDER.toString(), Statistics.REFRESH_FILE.toString()});
        }
        Statistics.REFRESH_FS.reset();
        Statistics.LISTENERS_CALLS.reset();
        Statistics.REFRESH_FOLDER.reset();
        Statistics.REFRESH_FILE.reset();
    }

    final /* varargs */ void refreshFor(final RefreshSlow slow, final boolean ignoreRecursiveListeners, final File ... files) {
        Statistics.StopWatch stopWatch = Statistics.getStopWatch(Statistics.REFRESH_FS);
        final Runnable r = new Runnable(){

            @Override
            public void run() {
                Set all2Refresh = FileObjectFactory.this.collectForRefresh(ignoreRecursiveListeners);
                FileObjectFactory.this.refresh(all2Refresh, slow, files);
                if (LOG_REFRESH.isLoggable(Level.FINER)) {
                    LOG_REFRESH.log(Level.FINER, "Refresh for {0} objects", all2Refresh.size());
                    for (BaseFileObj baseFileObj : all2Refresh) {
                        LOG_REFRESH.log(Level.FINER, "  {0}", (Object)baseFileObj);
                    }
                }
            }
        };
        stopWatch.start();
        try {
            if (slow != null) {
                FileBasedFileSystem.runAsInconsistent(r);
            } else {
                FileBasedFileSystem.getInstance().runAtomicAction(new FileSystem.AtomicAction(){

                    public void run() throws IOException {
                        FileBasedFileSystem.runAsInconsistent(r);
                    }
                });
            }
        }
        catch (IOException iex) {
            // empty catch block
        }
        stopWatch.stop();
        if (LOG_REFRESH.isLoggable(Level.FINE)) {
            LOG_REFRESH.log(Level.FINE, "FS.refresh statistics ({0}FileObjects):\n  {1}\n  {2}\n  {3}\n  {4}\n", new Object[]{Statistics.fileObjects(), Statistics.REFRESH_FS.toString(), Statistics.LISTENERS_CALLS.toString(), Statistics.REFRESH_FOLDER.toString(), Statistics.REFRESH_FILE.toString()});
        }
        Statistics.REFRESH_FS.reset();
        Statistics.LISTENERS_CALLS.reset();
        Statistics.REFRESH_FOLDER.reset();
        Statistics.REFRESH_FILE.reset();
    }

    private void refreshFromGetter(FileObject parent, boolean asyncFire) {
        try {
            if (asyncFire) {
                FileUtil.runAtomicAction((FileSystem.AtomicAction)new AsyncRefreshAtomicAction(parent));
            } else {
                parent.refresh();
            }
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    private static class AsyncRefreshAtomicAction
    implements FileSystem.AtomicAction {
        private FileObject fo;

        AsyncRefreshAtomicAction(FileObject fo) {
            this.fo = fo;
        }

        public void run() throws IOException {
            this.fo.refresh();
        }
    }

    public static enum Caller {
        ToFileObject,
        GetFileObject,
        GetChildern,
        GetParent,
        Refresh,
        Others;
        

        private Caller() {
        }

        boolean asynchFire() {
            if (this == Refresh || this == Others) {
                return false;
            }
            return true;
        }
    }

}

