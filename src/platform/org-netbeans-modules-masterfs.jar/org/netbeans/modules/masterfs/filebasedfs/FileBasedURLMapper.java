/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.masterfs.filebasedfs;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.masterfs.filebasedfs.FileBasedFileSystem;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.BaseFileObj;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.FileObjectFactory;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.RootObj;
import org.netbeans.modules.masterfs.filebasedfs.naming.FileNaming;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Utilities;

public final class FileBasedURLMapper
extends URLMapper {
    private static final Logger LOG = Logger.getLogger(FileBasedURLMapper.class.getName());

    public final URL getURL(FileObject fo, int type) {
        if (type == 2) {
            return null;
        }
        URL retVal = null;
        try {
            if (fo instanceof BaseFileObj) {
                BaseFileObj bfo = (BaseFileObj)fo;
                retVal = FileBasedURLMapper.fileToURL(bfo.getFileName().getFile(), fo);
            } else if (fo instanceof RootObj) {
                RootObj rfo = (RootObj)fo;
                return this.getURL((FileObject)rfo.getRealRoot(), type);
            }
        }
        catch (MalformedURLException e) {
            retVal = null;
        }
        return retVal;
    }

    public final FileObject[] getFileObjects(URL url) {
        File file;
        if (!"file".equals(url.getProtocol())) {
            return null;
        }
        if (url.getPath().equals("//") || url.getPath().equals("////")) {
            return null;
        }
        FileObject retVal = null;
        try {
            file = FileUtil.normalizeFile((File)Utilities.toFile((URI)url.toURI()));
        }
        catch (URISyntaxException e) {
            LOG.log(Level.INFO, "URL=" + url, e);
            return null;
        }
        catch (IllegalArgumentException iax) {
            LOG.log(Level.INFO, "URL=" + url, iax);
            return null;
        }
        retVal = FileBasedFileSystem.getFileObject(file, FileObjectFactory.Caller.ToFileObject);
        return new FileObject[]{retVal};
    }

    private static URL fileToURL(File file, FileObject fo) throws MalformedURLException {
        URL retVal = FileBasedURLMapper.toURI(file, fo.isFolder()).toURL();
        if (fo.isFolder()) {
            String urlDef = retVal.toExternalForm();
            String pathSeparator = "/";
            if (!urlDef.endsWith("/")) {
                retVal = new URL(urlDef + "/");
            }
        }
        return retVal;
    }

    private static URI toURI(File file, boolean isDirectory) {
        return FileBasedURLMapper.toURI(file.getAbsolutePath(), isDirectory, File.separatorChar);
    }

    static URI toURI(String path, boolean isDirectory, char separator) {
        try {
            String sp = FileBasedURLMapper.slashify(path, isDirectory, separator);
            return new URI("file", null, sp, null);
        }
        catch (URISyntaxException x) {
            throw new Error(x);
        }
    }

    private static String slashify(String p, boolean isDirectory, char separatorChar) {
        if (separatorChar != '/') {
            p = p.replace(separatorChar, '/');
        }
        if (!p.startsWith("/")) {
            p = "/" + p;
        }
        if (!p.endsWith("/") && isDirectory) {
            p = p + "/";
        }
        return p;
    }
}

