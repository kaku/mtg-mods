/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStatusEvent
 *  org.openide.filesystems.FileStatusListener
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.filesystems.FileSystem$HtmlStatus
 *  org.openide.filesystems.FileSystem$Status
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Utilities
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.modules.masterfs.filebasedfs;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TooManyListenersException;
import java.util.logging.Logger;
import org.netbeans.modules.masterfs.ProvidedExtensionsProxy;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.BaseFileObj;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.FileObjectFactory;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.RootObj;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.RootObjWindows;
import org.netbeans.modules.masterfs.providers.AnnotationProvider;
import org.netbeans.modules.masterfs.providers.ProvidedExtensions;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStatusEvent;
import org.openide.filesystems.FileStatusListener;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Utilities;
import org.openide.util.lookup.ProxyLookup;

public final class FileBasedFileSystem
extends FileSystem {
    private static final Logger LOG = Logger.getLogger(FileBasedFileSystem.class.getName());
    private static FileBasedFileSystem INSTANCE = new FileBasedFileSystem();
    private transient RootObj<? extends FileObject> root;
    private final transient StatusImpl status;
    private static transient int modificationInProgress;

    public FileBasedFileSystem() {
        this.status = new StatusImpl();
        if (Utilities.isWindows()) {
            RootObjWindows realRoot = new RootObjWindows();
            this.root = new RootObj<RootObjWindows>(realRoot);
        } else {
            FileObjectFactory factory = FileObjectFactory.getInstance(new File("/"));
            this.root = new RootObj<BaseFileObj>(factory.getRoot());
        }
    }

    public static synchronized boolean isModificationInProgress() {
        return modificationInProgress != 0;
    }

    private static synchronized void setModificationInProgress(boolean started) {
        modificationInProgress = started ? ++modificationInProgress : --modificationInProgress;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void runAsInconsistent(Runnable r) {
        try {
            FileBasedFileSystem.setModificationInProgress(true);
            r.run();
        }
        finally {
            FileBasedFileSystem.setModificationInProgress(false);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static <Retval> Retval runAsInconsistent(FSCallable<Retval> r) throws IOException {
        Retval retval = null;
        try {
            FileBasedFileSystem.setModificationInProgress(true);
            retval = r.call();
        }
        finally {
            FileBasedFileSystem.setModificationInProgress(false);
        }
        return retval;
    }

    public static Map<File, ? extends FileObjectFactory> factories() {
        return FileObjectFactory.factories();
    }

    public static FileObject getFileObject(File file) {
        return FileBasedFileSystem.getFileObject(file, FileObjectFactory.Caller.GetFileObject);
    }

    public static FileObject getFileObject(File file, FileObjectFactory.Caller caller) {
        FileObjectFactory fs = FileObjectFactory.getInstance(file);
        FileObject retval = null;
        if (fs != null) {
            retval = file.getParentFile() == null && Utilities.isUnix() ? FileBasedFileSystem.getInstance().getRoot() : fs.getValidFileObject(file, caller);
        }
        return retval;
    }

    public static FileBasedFileSystem getInstance() {
        return INSTANCE;
    }

    public void refresh(final boolean expected) {
        final Runnable r = new Runnable(){

            @Override
            public void run() {
                FileBasedFileSystem.this.refreshImpl(expected);
            }
        };
        try {
            FileBasedFileSystem.getInstance().runAtomicAction(new FileSystem.AtomicAction(){

                public void run() throws IOException {
                    FileBasedFileSystem.runAsInconsistent(r);
                }
            });
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    public void refreshImpl(boolean expected) {
        FileObject fo = this.root.getRealRoot();
        if (fo instanceof BaseFileObj) {
            ((BaseFileObj)fo).getFactory().refresh(expected);
        } else if (fo instanceof RootObjWindows) {
            Collection<? extends FileObjectFactory> fcs = FileBasedFileSystem.factories().values();
            for (FileObjectFactory fileObjectFactory : fcs) {
                fileObjectFactory.refresh(expected);
            }
        }
    }

    public String getDisplayName() {
        return this.getClass().getName();
    }

    public boolean isReadOnly() {
        return false;
    }

    public FileObject getRoot() {
        return this.root;
    }

    public FileObject findResource(String name) {
        if (Utilities.isWindows()) {
            if ("".equals(name)) {
                return FileBasedFileSystem.getInstance().getRoot();
            }
        } else {
            name = name.startsWith("/") ? name : "/" + name;
        }
        File f = new File(name);
        if (name.contains("..") || name.contains("./") || name.contains("/.")) {
            f = FileUtil.normalizeFile((File)f);
        }
        return FileBasedFileSystem.getFileObject(f);
    }

    public FileObject getTempFolder() throws IOException {
        FileObject tmpDir = FileUtil.toFileObject((File)new File(System.getProperty("java.io.tmpdir")));
        if (tmpDir != null && tmpDir.isFolder() && tmpDir.isValid()) {
            return tmpDir;
        }
        throw new IOException("Cannot find temporary folder");
    }

    public FileObject createTempFile(FileObject parent, String prefix, String suffix, boolean deleteOnExit) throws IOException {
        if (parent.isFolder() && parent.isValid()) {
            FileObject fo;
            File tmpFile = File.createTempFile(prefix, suffix, FileUtil.toFile((FileObject)parent));
            if (deleteOnExit) {
                tmpFile.deleteOnExit();
            }
            if ((fo = FileUtil.toFileObject((File)tmpFile)) != null && fo.isData() && fo.isValid()) {
                return fo;
            }
            tmpFile.delete();
        }
        throw new IOException("Cannot create temporary file");
    }

    public Lookup findExtrasFor(Set<FileObject> objects) {
        return this.status.findExtrasFor(objects);
    }

    public FileSystem.Status getStatus() {
        return this.status;
    }

    public Object writeReplace() throws ObjectStreamException {
        return new SerReplace();
    }

    public static interface FSCallable<V> {
        public V call() throws IOException;
    }

    private static class SerReplace
    implements Serializable {
        static final long serialVersionUID = -3714631266626840241L;

        private SerReplace() {
        }

        public Object readResolve() throws ObjectStreamException {
            return FileBasedFileSystem.getInstance();
        }
    }

    public final class StatusImpl
    implements FileSystem.HtmlStatus,
    LookupListener,
    FileStatusListener {
        private Lookup.Result<AnnotationProvider> annotationProviders;
        private Collection<? extends AnnotationProvider> previousProviders;

        public StatusImpl() {
            this.annotationProviders = Lookup.getDefault().lookup(new Lookup.Template(AnnotationProvider.class));
            this.annotationProviders.addLookupListener((LookupListener)this);
            this.resultChanged(null);
        }

        public ProvidedExtensions getExtensions() {
            Collection c = this.previousProviders != null ? Collections.unmodifiableCollection(this.previousProviders) : Collections.emptyList();
            return new ProvidedExtensionsProxy(c);
        }

        public void resultChanged(LookupEvent ev) {
            HashSet<? extends AnnotationProvider> add;
            HashSet<? extends AnnotationProvider> now = this.annotationProviders.allInstances();
            if (this.previousProviders != null) {
                add = new HashSet<AnnotationProvider>(now);
                add.removeAll(this.previousProviders);
                HashSet<? extends AnnotationProvider> toRemove = new HashSet<AnnotationProvider>(this.previousProviders);
                toRemove.removeAll(now);
                for (AnnotationProvider ap : toRemove) {
                    ap.removeFileStatusListener(this);
                }
            } else {
                add = now;
            }
            for (AnnotationProvider ap : add) {
                try {
                    ap.addFileStatusListener(this);
                }
                catch (TooManyListenersException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            this.previousProviders = now;
        }

        public Lookup findExtrasFor(Set<FileObject> foSet) {
            ArrayList<Lookup> arr = new ArrayList<Lookup>();
            for (AnnotationProvider ap : this.annotationProviders.allInstances()) {
                Lookup lkp = ap.findExtrasFor(foSet);
                if (lkp == null) continue;
                arr.add(lkp);
            }
            return new ProxyLookup(arr.toArray((T[])new Lookup[arr.size()]));
        }

        public void annotationChanged(FileStatusEvent ev) {
            FileBasedFileSystem.this.fireFileStatusChanged(ev);
        }

        public Image annotateIcon(Image icon, int iconType, Set<? extends FileObject> files) {
            Image retVal = null;
            Iterator it = this.annotationProviders.allInstances().iterator();
            while (retVal == null && it.hasNext()) {
                AnnotationProvider ap = (AnnotationProvider)it.next();
                retVal = ap.annotateIcon(icon, iconType, files);
            }
            if (retVal != null) {
                return retVal;
            }
            return icon;
        }

        public String annotateName(String name, Set<? extends FileObject> files) {
            String retVal = null;
            Iterator it = this.annotationProviders.allInstances().iterator();
            while (retVal == null && it.hasNext()) {
                AnnotationProvider ap = (AnnotationProvider)it.next();
                retVal = ap.annotateName(name, files);
            }
            if (retVal != null) {
                return retVal;
            }
            return name;
        }

        public String annotateNameHtml(String name, Set<? extends FileObject> files) {
            String retVal = null;
            Iterator it = this.annotationProviders.allInstances().iterator();
            while (retVal == null && it.hasNext()) {
                AnnotationProvider ap = (AnnotationProvider)it.next();
                retVal = ap.annotateNameHtml(name, files);
            }
            return retVal;
        }
    }

}

