/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.CharSequences
 */
package org.netbeans.modules.masterfs.filebasedfs.naming;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Comparator;
import org.netbeans.modules.masterfs.filebasedfs.naming.FileNaming;
import org.netbeans.modules.masterfs.filebasedfs.naming.FolderName;
import org.netbeans.modules.masterfs.filebasedfs.naming.NamingFactory;
import org.netbeans.modules.masterfs.filebasedfs.utils.FileChangedManager;
import org.netbeans.modules.masterfs.providers.ProvidedExtensions;
import org.openide.util.CharSequences;

public class FileName
implements FileNaming {
    private final CharSequence name;
    private final FileNaming parent;
    private final Integer id;
    private CharSequence currentName;

    protected FileName(FileNaming parent, File file, Integer theKey) {
        this.parent = parent;
        this.name = CharSequences.create((CharSequence)FileName.parseName(parent, file));
        this.id = theKey == null ? NamingFactory.createID(file) : theKey;
        this.currentName = this.name;
        boolean debug = false;
        if (!$assertionsDisabled) {
            debug = true;
            if (!true) {
                throw new AssertionError();
            }
        }
        if (debug) {
            this.currentName = new Creation(this.name);
        }
    }

    private static String parseName(FileNaming parent, File file) {
        return parent == null ? file.getPath() : file.getName();
    }

    @Override
    public FileNaming rename(String name, ProvidedExtensions.IOHandler handler) throws IOException {
        boolean success = false;
        File f = this.getFile();
        if (FileChangedManager.getInstance().exists(f)) {
            File newFile = new File(f.getParentFile(), name);
            if (handler != null) {
                handler.handle();
                success = true;
            } else {
                success = f.renameTo(newFile);
            }
            if (success) {
                FolderName.freeCaches();
                return NamingFactory.fromFile(this.getParent(), newFile, true);
            }
        }
        return this;
    }

    @Override
    public final boolean isRoot() {
        return this.getParent() == null;
    }

    @Override
    public File getFile() {
        FileNaming myParent = this.getParent();
        return myParent != null ? new File(myParent.getFile(), this.getName()) : new File(this.getName());
    }

    @Override
    public final String getName() {
        return this.currentName.toString();
    }

    @Override
    public FileNaming getParent() {
        return this.parent;
    }

    @Override
    public final Integer getId() {
        return this.id;
    }

    public final boolean equals(Object obj) {
        if (obj instanceof FileName) {
            FileName fn = (FileName)obj;
            if (obj.hashCode() != this.hashCode()) {
                return false;
            }
            if (!this.name.equals(fn.name)) {
                return false;
            }
            if (this.parent == null || fn.parent == null) {
                return this.parent == null && fn.parent == null;
            }
            return this.parent.equals(fn.parent);
        }
        return obj instanceof FileNaming && obj.hashCode() == this.hashCode();
    }

    public final String toString() {
        return this.getFile().getAbsolutePath();
    }

    public final int hashCode() {
        return this.id;
    }

    @Override
    public boolean isFile() {
        return true;
    }

    @Override
    public boolean isDirectory() {
        return !this.isFile();
    }

    void updateCase(String name) {
        assert (String.CASE_INSENSITIVE_ORDER.compare(name, this.name.toString()) == 0);
        CharSequence value = CharSequences.create((CharSequence)name);
        if (this.currentName instanceof Creation) {
            ((Creation)this.currentName).delegate = value;
        } else {
            this.currentName = value;
        }
    }

    public void dumpCreation(StringBuilder sb) {
        if (this.currentName instanceof Creation) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ((Creation)this.currentName).printStackTrace(pw);
            pw.close();
            sb.append(sw.toString());
        }
    }

    final void recordCleanup(String msg) {
        Throwable ex = null;
        if (this.currentName instanceof Creation) {
            ex = (Creation)this.currentName;
        }
        if (ex != null) {
            while (ex.getCause() != null) {
                ex = ex.getCause();
            }
            ex.initCause(new Exception("Reference cleanup: " + msg));
        }
    }

    private static final class Creation
    extends Exception
    implements CharSequence {
        CharSequence delegate;

        private Creation(CharSequence name) {
            this.delegate = name;
        }

        public int hashCode() {
            return this.delegate.hashCode();
        }

        public boolean equals(Object obj) {
            return this.delegate.equals(obj);
        }

        @Override
        public String toString() {
            return this.delegate.toString();
        }

        @Override
        public CharSequence subSequence(int start, int end) {
            return this.delegate.subSequence(start, end);
        }

        @Override
        public int length() {
            return this.delegate.length();
        }

        @Override
        public char charAt(int index) {
            return this.delegate.charAt(index);
        }
    }

}

