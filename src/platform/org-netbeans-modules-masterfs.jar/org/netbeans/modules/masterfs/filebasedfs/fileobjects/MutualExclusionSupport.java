/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.WeakSet
 */
package org.netbeans.modules.masterfs.filebasedfs.fileobjects;

import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.masterfs.filebasedfs.utils.FSException;
import org.openide.util.WeakSet;

public final class MutualExclusionSupport<K> {
    private final Map<K, Set<MutualExclusionSupport<K>>> exclusive = Collections.synchronizedMap(new WeakHashMap());
    private final Map<K, Set<MutualExclusionSupport<K>>> shared = Collections.synchronizedMap(new WeakHashMap());

    public synchronized MutualExclusionSupport<K> addResource(K key, boolean isShared) throws IOException {
        boolean isInUse = true;
        Map<K, Set<MutualExclusionSupport<K>>> unexpected = isShared ? this.exclusive : this.shared;
        Map<K, Set<MutualExclusionSupport<K>>> expected = isShared ? this.shared : this.exclusive;
        Set<MutualExclusionSupport<K>> unexpectedCounter = unexpected.get(key);
        WeakSet expectedCounter = expected.get(key);
        for (int i = 0; i < 10 && isInUse; ++i) {
            boolean bl = isInUse = unexpectedCounter != null && unexpectedCounter.size() > 0;
            if (!isInUse) {
                if (expectedCounter == null) {
                    expectedCounter = new WeakSet();
                    expected.put(key, (Set<MutualExclusionSupport<K>>)expectedCounter);
                }
                boolean bl2 = isInUse = !isShared && expectedCounter.size() > 0;
            }
            if (!isInUse) continue;
            try {
                this.wait(200);
                continue;
            }
            catch (InterruptedException e) {
                break;
            }
        }
        if (isInUse) {
            try {
                FSException.io(isShared ? "EXC_CannotGetSharedAccess" : "EXC_CannotGetExclusiveAccess", key.toString());
            }
            catch (IOException x) {
                assert (this.addStack(x, unexpectedCounter, (Set<MutualExclusionSupport<K>>)expectedCounter));
                throw x;
            }
        }
        Closeable retVal = new Closeable(key, isShared);
        expectedCounter.add((Closeable)retVal);
        return retVal;
    }

    private boolean addStack(IOException x, Set<MutualExclusionSupport<K>> unexpectedCounter, Set<MutualExclusionSupport<K>> expectedCounter) {
        try {
            this.addStack(x, unexpectedCounter);
            this.addStack(x, expectedCounter);
        }
        catch (IllegalArgumentException e) {
            Logger log = Logger.getLogger(MutualExclusionSupport.class.getName());
            log.log(Level.WARNING, "Cannot add stack to exception: unexpectedCounter: {0}, expectedCounter: {1}", new Object[]{Arrays.toString(unexpectedCounter.toArray()), Arrays.toString(expectedCounter.toArray())});
            log.log(Level.INFO, null, e);
            log.log(Level.INFO, "Exception x", x);
        }
        return true;
    }

    private void addStack(IOException x, Set<MutualExclusionSupport<K>> cs) {
        if (cs != null) {
            for (Closeable c : cs) {
                Throwable stack = c.stack;
                if (stack == null) continue;
                Throwable t = x;
                while (t.getCause() != null) {
                    t = t.getCause();
                }
                t.initCause(stack);
            }
        }
    }

    private synchronized void removeResource(K key, MutualExclusionSupport<K> value, boolean isShared) {
        Map<K, Set<MutualExclusionSupport<K>>> expected = isShared ? this.shared : this.exclusive;
        Set<MutualExclusionSupport<K>> expectedCounter = expected.get(key);
        if (expectedCounter != null) {
            expectedCounter.remove(value);
        }
    }

    synchronized boolean isBeingWritten(K file) {
        Set<MutualExclusionSupport<K>> counter = this.exclusive.get(file);
        return counter != null && !counter.isEmpty();
    }

    public final class Closeable {
        private final boolean isShared;
        private final Reference<K> keyRef;
        private boolean isClosed;
        Throwable stack;

        private Closeable(K key, boolean isShared) {
            this.isClosed = false;
            this.isShared = isShared;
            this.keyRef = new WeakReference<K>(key);
            assert (this.populateStack());
        }

        private boolean populateStack() {
            this.stack = new Throwable("opened stream here");
            return true;
        }

        public void close() {
            if (!this.isClosed()) {
                this.isClosed = true;
                K key = this.keyRef.get();
                if (key != null) {
                    MutualExclusionSupport.this.removeResource(key, this, this.isShared);
                }
            }
        }

        public boolean isClosed() {
            return this.isClosed;
        }
    }

}

