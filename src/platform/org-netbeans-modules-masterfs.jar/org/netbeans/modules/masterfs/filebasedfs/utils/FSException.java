/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileSystem
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.masterfs.filebasedfs.utils;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.FileObjectFactory;
import org.openide.filesystems.FileSystem;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public final class FSException
extends IOException {
    private final Object[] args;

    private /* varargs */ FSException(String resource, Object ... args) {
        super(resource);
        this.args = args;
    }

    @Override
    public String getMessage() {
        return " " + this.getLocalizedMessage();
    }

    @Override
    public String getLocalizedMessage() {
        String format;
        block3 : {
            String res = super.getMessage();
            format = null;
            try {
                format = NbBundle.getBundle((String)"org.netbeans.modules.masterfs.filebasedfs.Bundle", (Locale)Locale.getDefault(), (ClassLoader)FileObjectFactory.class.getClassLoader()).getString(res);
            }
            catch (MissingResourceException mex) {
                if (format != null) break block3;
                format = NbBundle.getBundle((String)"org.openide.filesystems.Bundle", (Locale)Locale.getDefault(), (ClassLoader)FileSystem.class.getClassLoader()).getString(res);
            }
        }
        if (this.args != null) {
            return MessageFormat.format(format, this.args);
        }
        return format;
    }

    public static /* varargs */ void io(String resource, Object ... args) throws IOException {
        FSException fsExc = new FSException(resource, args);
        Exceptions.attachLocalizedMessage((Throwable)fsExc, (String)fsExc.getLocalizedMessage());
        throw fsExc;
    }

    public static void annotateException(Throwable t) {
        Exceptions.attachLocalizedMessage((Throwable)t, (String)t.getLocalizedMessage());
    }
}

