/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.masterfs.filebasedfs.utils;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Locale;
import java.util.Stack;
import org.openide.filesystems.FileObject;
import org.openide.util.Utilities;

public class Utils {
    private static final Boolean SENSITIVE = Utils.findCase();

    private static Boolean findCase() {
        String userDef = System.getProperty("org.netbeans.modules.masterfs.case");
        if ("insensitive".equals(userDef)) {
            return false;
        }
        if ("sensitive".equals(userDef)) {
            return true;
        }
        assert (userDef == null);
        if (Utilities.isMac()) {
            return false;
        }
        return null;
    }

    public static boolean equals(File f1, File f2) {
        if (f1 == null) {
            return f2 == null;
        }
        if (f2 == null) {
            return f1 == null;
        }
        if (SENSITIVE == null) {
            return f1.equals(f2);
        }
        if (SENSITIVE.booleanValue()) {
            return f1.getPath().compareTo(f2.getPath()) == 0;
        }
        return f1.getPath().compareToIgnoreCase(f2.getPath()) == 0;
    }

    public static int hashCode(File file) {
        if (SENSITIVE == null) {
            return file.hashCode();
        }
        if (SENSITIVE.booleanValue()) {
            return file.getPath().hashCode() ^ 1234321;
        }
        return file.getPath().toLowerCase(Locale.ENGLISH).hashCode() ^ 1234321;
    }

    public static String getRelativePath(File dir, File file) {
        File tempFile;
        Stack<String> stack = new Stack<String>();
        for (tempFile = file; tempFile != null && !Utils.equals(tempFile, dir); tempFile = tempFile.getParentFile()) {
            stack.push(tempFile.getName());
        }
        if (tempFile == null) {
            return null;
        }
        StringBuilder retval = new StringBuilder();
        while (!stack.isEmpty()) {
            retval.append((String)stack.pop());
            if (stack.isEmpty()) continue;
            retval.append("/");
        }
        return retval.toString();
    }

    public static void reassignLkp(FileObject from, FileObject to) {
        try {
            Class c = Class.forName("org.openide.filesystems.FileObjectLkp");
            Method m = c.getDeclaredMethod("reassign", FileObject.class, FileObject.class);
            m.setAccessible(true);
            m.invoke(null, new Object[]{from, to});
        }
        catch (InvocationTargetException ex) {
            if (ex.getCause() instanceof RuntimeException) {
                throw (RuntimeException)ex.getCause();
            }
            if (ex.getCause() instanceof Error) {
                throw (Error)ex.getCause();
            }
            throw new IllegalStateException(ex);
        }
        catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }
}

