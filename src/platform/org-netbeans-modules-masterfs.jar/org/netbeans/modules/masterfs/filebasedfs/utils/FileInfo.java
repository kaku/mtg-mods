/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.masterfs.filebasedfs.utils;

import java.io.File;
import java.io.IOException;
import javax.swing.filechooser.FileSystemView;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.WriteLockUtils;
import org.netbeans.modules.masterfs.filebasedfs.naming.FileNaming;
import org.netbeans.modules.masterfs.filebasedfs.naming.NamingFactory;
import org.netbeans.modules.masterfs.filebasedfs.utils.FileChangedManager;
import org.openide.filesystems.FileObject;
import org.openide.util.Utilities;

public final class FileInfo {
    private static boolean IS_WINDOWS = Utilities.isWindows();
    private int isFile = -1;
    private int isDirectory = -1;
    private int exists = -1;
    private int isComputeNode = -1;
    private int isUnixSpecialFile = -1;
    private int isUNC = -1;
    private int isConvertibleToFileObject = -1;
    private Integer id = null;
    private FileInfo root = null;
    private final File file;
    private FileInfo parent = null;
    private FileNaming fileNaming = null;
    private FileObject fObject = null;
    private static FileSystemView FILESYSTEMVIEW;

    public FileInfo(File file, int exists) {
        assert (file != null);
        this.file = file;
        this.exists = exists;
    }

    public FileInfo(File file) {
        assert (file != null);
        this.file = file;
    }

    public FileInfo(FileInfo parent, File file) {
        this(file);
        this.parent = parent;
    }

    public boolean isFile() {
        if (this.isFile == -1) {
            this.isFile = this.getFile().isFile() ? 1 : 0;
        }
        return this.isFile != 0;
    }

    public boolean isDirectory() {
        if (this.isDirectory == -1) {
            this.isDirectory = this.getFile().isDirectory() ? 1 : 0;
        }
        return this.isDirectory != 0;
    }

    public boolean isDirectoryComputed() {
        return this.isDirectory != -1;
    }

    public boolean exists() {
        if (this.exists == -1) {
            this.exists = 0;
            if (FileChangedManager.getInstance().exists(this.getFile())) {
                this.exists = 1;
            } else {
                String path = this.getFile().getPath();
                if (path.startsWith("\\\\") && path.indexOf(92, 2) == -1) {
                    this.exists = 1;
                }
            }
        }
        return this.exists != 0;
    }

    private static synchronized FileSystemView fsView() {
        if (FILESYSTEMVIEW == null) {
            try {
                FILESYSTEMVIEW = FileSystemView.getFileSystemView();
            }
            catch (Throwable ex) {
                FILESYSTEMVIEW = new FileSystemView(){

                    @Override
                    public File createNewFolder(File containingDir) throws IOException {
                        throw new IOException();
                    }

                    @Override
                    public boolean isComputerNode(File dir) {
                        return false;
                    }
                };
            }
        }
        return FILESYSTEMVIEW;
    }

    private boolean isComputeNode() {
        if (this.isComputeNode == -1) {
            this.isComputeNode = FileInfo.fsView().isComputerNode(this.getFile()) ? 1 : 0;
        }
        return this.isComputeNode == 1;
    }

    public boolean isUnixSpecialFile() {
        if (this.isUnixSpecialFile == -1) {
            this.isUnixSpecialFile = !IS_WINDOWS && !this.isDirectory() && !this.isFile() && this.exists() ? 1 : 0;
        }
        return this.isUnixSpecialFile == 1;
    }

    public boolean isUNCFolder() {
        if (this.isUNC == -1) {
            this.isUNC = this.isWindows() && !this.isFile() && !this.isDirectory() && !this.exists() && this.isComputeNode() ? 1 : 0;
        }
        return this.isUNC == 1;
    }

    public boolean isWindows() {
        return IS_WINDOWS;
    }

    public boolean isConvertibleToFileObject() {
        if (this.isConvertibleToFileObject == -1) {
            this.isConvertibleToFileObject = this.isSupportedFile() && this.exists() ? 1 : 0;
        }
        return this.isConvertibleToFileObject == 1;
    }

    public boolean isSupportedFile() {
        return !this.getFile().getName().equals(".nbattrs") && !WriteLockUtils.hasActiveLockFileSigns(this.getFile().getName());
    }

    public FileInfo getRoot() {
        if (this.root == null) {
            File tmp;
            File retVal = tmp = this.getFile();
            while (tmp != null) {
                retVal = tmp;
                tmp = tmp.getParentFile();
            }
            if ("\\\\".equals(retVal.getPath())) {
                String filename = this.getFile().getAbsolutePath();
                int firstSlash = filename.indexOf("\\", 2);
                if (firstSlash != -1) {
                    int secondSlash = filename.indexOf("\\", firstSlash + 1);
                    if (secondSlash != -1) {
                        filename = filename.substring(0, secondSlash);
                    }
                    retVal = new File(filename);
                } else {
                    retVal = this.getFile();
                }
            }
            this.root = new FileInfo(retVal);
        }
        return this.root;
    }

    public File getFile() {
        return this.file;
    }

    public Integer getID() {
        if (this.id == null) {
            this.id = NamingFactory.createID(this.getFile());
        }
        return this.id;
    }

    public FileInfo getParent() {
        return this.parent;
    }

    public FileNaming getFileNaming() {
        return this.fileNaming;
    }

    public void setFileNaming(FileNaming fileNaming) {
        this.fileNaming = fileNaming;
    }

    public FileObject getFObject() {
        return this.fObject;
    }

    public void setFObject(FileObject fObject) {
        this.fObject = fObject;
    }

    public String toString() {
        return this.getFile().toString();
    }

    public static final String composeName(String name, String ext) {
        return ext != null && ext.length() > 0 ? name + "." + ext : name;
    }

    public static final String getName(String name) {
        int i = name.lastIndexOf(46);
        return i <= 0 || i == name.length() - 1 ? name : name.substring(0, i);
    }

    public static final String getExt(String name) {
        int i = name.lastIndexOf(46) + 1;
        return i <= 1 || i == name.length() ? "" : name.substring(i);
    }

}

