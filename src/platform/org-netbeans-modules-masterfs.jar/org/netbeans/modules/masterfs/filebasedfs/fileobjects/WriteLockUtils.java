/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.masterfs.filebasedfs.fileobjects;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import org.netbeans.modules.masterfs.filebasedfs.utils.FileChangedManager;
import org.openide.util.Exceptions;

public class WriteLockUtils {
    static final String PREFIX = ".LCK";
    static final String SUFFIX = "~";

    private WriteLockUtils() {
    }

    public static synchronized boolean hasActiveLockFileSigns(String filename) {
        return filename.startsWith(".LCK") && filename.endsWith("~");
    }

    public static synchronized boolean isActiveLockFile(File file) {
        String name = file.getName();
        boolean isActiveLockFile = WriteLockUtils.hasActiveLockFileSigns(name);
        if (isActiveLockFile) {
            String newName = name.substring(".LCK".length(), name.length() - "~".length());
            isActiveLockFile = FileChangedManager.getInstance().exists(new File(file.getParentFile(), newName));
        }
        return isActiveLockFile;
    }

    public static File getAssociatedLockFile(File file) {
        try {
            file = file.getCanonicalFile();
        }
        catch (IOException iex) {
            Exceptions.printStackTrace((Throwable)iex);
        }
        File parentFile = file.getParentFile();
        StringBuilder sb = new StringBuilder();
        sb.append(".LCK");
        sb.append(file.getName());
        sb.append("~");
        String lckName = sb.toString();
        File lck = new File(parentFile, lckName);
        return lck;
    }

    static String getContentOfLckFile(File lckFile, FileChannel channel) throws IOException {
        byte[] readContent = new byte[(int)lckFile.length()];
        channel.read(ByteBuffer.wrap(readContent));
        String retVal = new String(readContent);
        return FileChangedManager.getInstance().exists(new File(retVal)) ? retVal : null;
    }

    static String writeContentOfLckFile(File lck, FileChannel channel) throws IOException {
        String absolutePath = lck.getAbsolutePath();
        ByteBuffer buf = ByteBuffer.wrap(absolutePath.getBytes());
        channel.write(buf);
        return absolutePath;
    }
}

