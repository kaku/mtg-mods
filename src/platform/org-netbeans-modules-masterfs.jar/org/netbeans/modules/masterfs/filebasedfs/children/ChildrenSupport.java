/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Privileged
 */
package org.netbeans.modules.masterfs.filebasedfs.children;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import org.netbeans.modules.masterfs.filebasedfs.children.ChildrenCache;
import org.netbeans.modules.masterfs.filebasedfs.naming.FileName;
import org.netbeans.modules.masterfs.filebasedfs.naming.FileNaming;
import org.netbeans.modules.masterfs.filebasedfs.naming.NamingFactory;
import org.netbeans.modules.masterfs.filebasedfs.utils.FileInfo;
import org.netbeans.modules.masterfs.providers.ProvidedExtensions;
import org.openide.util.Mutex;

public class ChildrenSupport {
    static final int NO_CHILDREN_CACHED = 0;
    static final int SOME_CHILDREN_CACHED = 1;
    static final int ALL_CHILDREN_CACHED = 2;
    private Set<FileNaming> notExistingChildren;
    private Set<FileNaming> existingChildren;
    private int status = 0;
    private static final Mutex.Privileged mutexPrivileged = new Mutex.Privileged();
    private static final Mutex mutex = new Mutex(mutexPrivileged);

    public final Mutex.Privileged getMutexPrivileged() {
        return mutexPrivileged;
    }

    public static boolean isLock() {
        return mutex.isReadAccess() || mutex.isWriteAccess();
    }

    public synchronized Set<FileNaming> getCachedChildren() {
        return new HashSet<FileNaming>(this.getExisting(false));
    }

    public synchronized Set<FileNaming> getChildren(FileNaming folderName, boolean rescan, Runnable[] task) {
        if (rescan || !this.isStatus(2)) {
            if (this.rescanChildren(folderName, false, task) == null) {
                return null;
            }
            this.setStatus(2);
        }
        return this.getExisting(false);
    }

    public boolean isCacheInitialized() {
        return !this.isStatus(0);
    }

    public synchronized FileNaming getChild(String childName, FileNaming folderName, boolean rescan) {
        return this.getChild(childName, folderName, rescan, null);
    }

    public synchronized FileNaming getChild(String childName, FileNaming folderName, boolean rescan, Runnable[] task) {
        FileNaming retval = null;
        if (rescan || this.isStatus(0)) {
            retval = this.rescanChild(folderName, childName, rescan, task);
        } else if (this.isStatus(1)) {
            retval = this.lookupChildInCache(folderName, childName, true);
            if (retval == null && this.lookupChildInCache(folderName, childName, false) == null) {
                retval = this.rescanChild(folderName, childName, rescan, task);
            }
        } else if (this.isStatus(2)) {
            retval = this.lookupChildInCache(folderName, childName, true);
        }
        this.setStatus(1);
        return retval;
    }

    public synchronized void removeChild(FileNaming folderName, FileNaming childName) {
        assert (childName != null);
        this.getExisting().remove(childName);
        if (childName.getParent().equals(folderName)) {
            this.getNotExisting().add(childName);
        }
    }

    private synchronized void addChild(FileNaming folderName, FileNaming childName) {
        assert (childName != null);
        assert (childName.getParent().equals(folderName));
        this.getExisting().add(childName);
        this.getNotExisting().remove(childName);
    }

    public synchronized Map<FileNaming, Integer> refresh(FileNaming folderName, Runnable[] task) {
        Map retVal = new HashMap<FileNaming, Integer>();
        HashSet<FileNaming> e = new HashSet<FileNaming>(this.getExisting(false));
        HashSet<FileNaming> nE = new HashSet<FileNaming>(this.getNotExisting(false));
        if (this.isStatus(1)) {
            HashSet<FileNaming> existingToCheck = new HashSet<FileNaming>(e);
            for (FileNaming fnToCheck : existingToCheck) {
                FileNaming fnRescanned = this.rescanChild(folderName, fnToCheck.getName(), true);
                if (fnRescanned != null) continue;
                retVal.put(fnToCheck, ChildrenCache.REMOVED_CHILD);
            }
            HashSet<FileNaming> notExistingToCheck = new HashSet<FileNaming>(nE);
            for (FileNaming fnToCheck2 : notExistingToCheck) {
                assert (fnToCheck2 != null);
                FileNaming fnRescanned = this.rescanChild(folderName, fnToCheck2.getName(), true);
                if (fnRescanned == null) continue;
                retVal.put(fnToCheck2, ChildrenCache.ADDED_CHILD);
            }
        } else if (this.isStatus(2)) {
            retVal = this.rescanChildren(folderName, true, task);
        }
        return retVal;
    }

    public String toString() {
        return this.getExisting(false).toString();
    }

    boolean isStatus(int status) {
        return this.status == status;
    }

    private void setStatus(int status) {
        if (this.status < status) {
            this.status = status;
        }
    }

    private FileNaming rescanChild(FileNaming folderName, String childName, boolean ignoreCache) {
        return this.rescanChild(folderName, childName, ignoreCache, null);
    }

    private FileNaming rescanChild(FileNaming folderName, String childName, boolean ignoreCache, Runnable[] task) {
        IOJob job;
        class IOJob
        implements Runnable {
            private FileNaming fileNaming;
            final /* synthetic */ FileInfo val$fInfo;
            final /* synthetic */ FileNaming val$folderName;
            final /* synthetic */ File val$child;
            final /* synthetic */ boolean val$ignoreCache;

            IOJob() {
                this.val$fInfo = var2_2;
                this.val$folderName = var3_3;
                this.val$child = var4_4;
                this.val$ignoreCache = n;
                this.fileNaming = null;
            }

            @Override
            public void run() {
                boolean convertibleToFO = this.val$fInfo.isConvertibleToFileObject();
                this.fileNaming = convertibleToFO ? NamingFactory.fromFile(this.val$folderName, this.val$child, this.val$ignoreCache) : null;
            }
        }
        File folder = folderName.getFile();
        File child = new File(folder, childName);
        FileInfo fInfo = new FileInfo(child);
        if (task != null && task[0] == null) {
            task[0] = new IOJob(this, fInfo, folderName, child, ignoreCache);
            return null;
        }
        if (task == null) {
            job = new IOJob(this, fInfo, folderName, child, ignoreCache);
            job.run();
        } else {
            job = (IOJob)task[0];
        }
        if (job.fileNaming != null) {
            this.addChild(folderName, job.fileNaming);
        } else {
            FileName fChild = new FileName(folderName, child, null){

                @Override
                public boolean isDirectory() {
                    return false;
                }

                @Override
                public boolean isFile() {
                    return false;
                }
            };
            this.removeChild(folderName, fChild);
        }
        return job.fileNaming;
    }

    private Map<FileNaming, Integer> rescanChildren(FileNaming folderName, boolean ignoreCache, Runnable[] task) {
        IdentityHashMap<FileNaming, Integer> retval = new IdentityHashMap<FileNaming, Integer>();
        File folder = folderName.getFile();
        assert (folderName.getFile().getAbsolutePath().equals(folderName.toString()));
        if (!(task[0] instanceof IOJob)) {
            class IOJob
            implements Runnable {
                boolean folderExists;
                Set<FileNaming> newChildren;
                final /* synthetic */ File val$folder;
                final /* synthetic */ FileNaming val$folderName;
                final /* synthetic */ boolean val$ignoreCache;

                IOJob() {
                    this.val$folder = var2_2;
                    this.val$folderName = var3_3;
                    this.val$ignoreCache = n;
                }

                @Override
                public void run() {
                    File[] children = this.val$folder.listFiles();
                    if (children != null) {
                        this.newChildren = new LinkedHashSet<FileNaming>();
                        for (int i = 0; i < children.length; ++i) {
                            FileInfo fInfo = new FileInfo(children[i], 1);
                            if (!fInfo.isConvertibleToFileObject()) continue;
                            FileNaming child = NamingFactory.fromFile(this.val$folderName, children[i], this.val$ignoreCache);
                            this.newChildren.add(child);
                        }
                    } else {
                        this.folderExists = this.val$folder.exists();
                    }
                }
            }
            task[0] = new IOJob(this, folder, folderName, ignoreCache);
            return null;
        }
        IOJob job = (IOJob)task[0];
        if (job.newChildren == null) {
            if (job.folderExists) {
                return retval;
            }
            job.newChildren = new LinkedHashSet<FileNaming>();
        }
        Set<FileNaming> deleted = ChildrenSupport.deepMinus(this.getExisting(false), job.newChildren);
        for (FileNaming fnRem : deleted) {
            this.removeChild(folderName, fnRem);
            retval.put(fnRem, ChildrenCache.REMOVED_CHILD);
        }
        Set<FileNaming> added = ChildrenSupport.deepMinus(job.newChildren, this.getExisting(false));
        for (FileNaming fnAdd : added) {
            this.addChild(folderName, fnAdd);
            retval.put(fnAdd, ChildrenCache.ADDED_CHILD);
        }
        return retval;
    }

    private static Set<FileNaming> deepMinus(Set<FileNaming> base, Set<FileNaming> minus) {
        HashMap<FileNaming, FileNaming> detract = new HashMap<FileNaming, FileNaming>(base.size() * 2);
        for (FileNaming fn : base) {
            detract.put(fn, fn);
        }
        assert (minus != null);
        for (FileNaming mm : minus) {
            FileNaming orig = (FileNaming)detract.remove(mm);
            if (orig == null || orig.isFile() == mm.isFile()) continue;
            detract.put(orig, orig);
        }
        return detract.keySet();
    }

    private FileNaming lookupChildInCache(FileNaming folder, String childName, boolean lookupExisting) {
        class FakeNaming
        implements FileNaming {
            public FileNaming lastEqual;
            final /* synthetic */ String val$childName;
            final /* synthetic */ FileNaming val$folder;
            final /* synthetic */ File val$f;
            final /* synthetic */ Integer val$id;

            FakeNaming() {
                this.val$childName = var2_2;
                this.val$folder = var3_3;
                this.val$f = var4_4;
                this.val$id = var5_5;
            }

            @Override
            public String getName() {
                return this.val$childName;
            }

            @Override
            public FileNaming getParent() {
                return this.val$folder;
            }

            @Override
            public boolean isRoot() {
                return false;
            }

            @Override
            public File getFile() {
                return this.val$f;
            }

            @Override
            public Integer getId() {
                return this.val$id;
            }

            @Override
            public FileNaming rename(String name, ProvidedExtensions.IOHandler h) {
                throw new IllegalStateException();
            }

            public boolean equals(Object obj) {
                if (this.hashCode() == obj.hashCode() && this.getName().equals(((FileNaming)obj).getName())) {
                    assert (this.lastEqual == null);
                    if (obj instanceof FileNaming) {
                        this.lastEqual = (FileNaming)obj;
                    }
                    return true;
                }
                return false;
            }

            public int hashCode() {
                return this.val$id;
            }

            @Override
            public boolean isFile() {
                return this.getFile().isFile();
            }

            @Override
            public boolean isDirectory() {
                return !this.isFile();
            }
        }
        Set<FileNaming> cache;
        File f = new File(folder.getFile(), childName);
        Integer id = NamingFactory.createID(f);
        FakeNaming fake = new FakeNaming(this, childName, folder, f, id);
        Set<FileNaming> set = cache = lookupExisting ? this.getExisting(false) : this.getNotExisting(false);
        if (cache.contains(fake)) {
            assert (fake.lastEqual != null);
            assert (fake.lastEqual.getName().equals(childName));
            return fake.lastEqual;
        }
        return null;
    }

    private synchronized Set<FileNaming> getExisting() {
        return this.getExisting(true);
    }

    private synchronized Set<FileNaming> getExisting(boolean init) {
        if (init && this.existingChildren == null) {
            this.existingChildren = new HashSet<FileNaming>();
        }
        return this.existingChildren != null ? this.existingChildren : Collections.emptySet();
    }

    private synchronized Set<FileNaming> getNotExisting() {
        return this.getNotExisting(true);
    }

    private synchronized Set<FileNaming> getNotExisting(boolean init) {
        if (init && this.notExistingChildren == null) {
            this.notExistingChildren = new HashSet<FileNaming>();
        }
        return this.notExistingChildren != null ? this.notExistingChildren : Collections.emptySet();
    }

    protected final synchronized void copyTo(ChildrenSupport newCache, FileNaming name) {
        assert (newCache != this && newCache.existingChildren == null && newCache.notExistingChildren == null);
        for (FileNaming fn2 : this.getExisting(false)) {
            newCache.getChild(fn2.getName(), name, true);
        }
        for (FileNaming fn2 : this.getNotExisting(false)) {
            newCache.getChild(fn2.getName(), name, true);
        }
        newCache.status = this.status;
    }

}

