/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.util.Enumerations
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.masterfs.filebasedfs.fileobjects;

import java.awt.EventQueue;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.masterfs.filebasedfs.FileBasedFileSystem;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.BaseFileObj;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.Bundle;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.FolderObj;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.LockForFile;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.MutualExclusionSupport;
import org.netbeans.modules.masterfs.filebasedfs.naming.FileNaming;
import org.netbeans.modules.masterfs.filebasedfs.utils.FSException;
import org.netbeans.modules.masterfs.filebasedfs.utils.FileChangedManager;
import org.netbeans.modules.masterfs.filebasedfs.utils.FileInfo;
import org.netbeans.modules.masterfs.filebasedfs.utils.Utils;
import org.netbeans.modules.masterfs.providers.ProvidedExtensions;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.util.Enumerations;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class FileObj
extends BaseFileObj {
    static final long serialVersionUID = -1133540210876356809L;
    private static final MutualExclusionSupport<FileObj> MUT_EXCL_SUPPORT = new MutualExclusionSupport();
    private long lastModified = -1;
    private boolean realLastModifiedCached;
    private static final Logger LOGGER = Logger.getLogger(FileObj.class.getName());

    FileObj(File file, FileNaming name) {
        super(file, name);
        this.setLastModified(System.currentTimeMillis(), null, false);
    }

    @Override
    protected boolean noFolderListeners() {
        FolderObj p = this.getExistingParent();
        return p == null ? true : p.noFolderListeners();
    }

    public OutputStream getOutputStream(FileLock lock) throws IOException {
        ProvidedExtensions extensions = this.getProvidedExtensions();
        File file = this.getFileName().getFile();
        if (!Utilities.isWindows() && !file.isFile()) {
            throw new IOException(file.getAbsolutePath());
        }
        return this.getOutputStream(lock, extensions, this);
    }

    public OutputStream getOutputStream(FileLock lock, ProvidedExtensions extensions, FileObject mfo) throws IOException {
        if (LOGGER.isLoggable(Level.FINE) && EventQueue.isDispatchThread()) {
            LOGGER.log(Level.WARNING, "writing " + (Object)((Object)this), new IllegalStateException("getOutputStream invoked in AWT"));
        }
        final File f = this.getFileName().getFile();
        if (!this.isValid()) {
            FileObject recreated = this.getFileSystem().findResource(this.getPath());
            if (recreated instanceof FileObj && recreated != this) {
                return ((FileObj)recreated).getOutputStream(lock, extensions, mfo);
            }
            FileNotFoundException fnf = new FileNotFoundException("FileObject " + (Object)((Object)this) + " is not valid; isFile=" + f.isFile());
            Exceptions.attachLocalizedMessage((Throwable)fnf, (String)Bundle.EXC_INVALID_FILE((Object)this));
            throw fnf;
        }
        if (!Utilities.isWindows() && !f.isFile()) {
            throw new IOException(f.getAbsolutePath());
        }
        final MutualExclusionSupport.Closeable closable = MUT_EXCL_SUPPORT.addResource((Object)this, false);
        if (extensions != null) {
            extensions.beforeChange(mfo);
        }
        OutputStream retVal = null;
        try {
            final OutputStream delegate = Files.newOutputStream(f.toPath(), new OpenOption[0]);
            retVal = new OutputStream(){

                @Override
                public void write(int b) throws IOException {
                    delegate.write(b);
                }

                @Override
                public void close() throws IOException {
                    if (!closable.isClosed()) {
                        delegate.close();
                        LOGGER.log(Level.FINEST, "getOutputStream-close");
                        Long lastModif = BaseFileObj.MOVED_FILE_TIMESTAMP.get();
                        if (lastModif != null) {
                            f.setLastModified(lastModif);
                        }
                        FileObj.this.setLastModified(f.lastModified(), f, false);
                        closable.close();
                        FileObj.this.fireFileChangedEvent(false);
                    }
                }

                @Override
                public void flush() throws IOException {
                    delegate.flush();
                }

                @Override
                public void write(byte[] b, int off, int len) throws IOException {
                    delegate.write(b, off, len);
                }

                @Override
                public void write(byte[] b) throws IOException {
                    delegate.write(b);
                }
            };
        }
        catch (FileNotFoundException e) {
            if (closable != null) {
                closable.close();
            }
            FileNotFoundException fex = e;
            if (!FileChangedManager.getInstance().exists(f)) {
                fex = (FileNotFoundException)new FileNotFoundException(e.getLocalizedMessage()).initCause(e);
            } else if (!f.canWrite()) {
                fex = (FileNotFoundException)new FileNotFoundException(e.getLocalizedMessage()).initCause(e);
            } else if (f.getParentFile() == null) {
                fex = (FileNotFoundException)new FileNotFoundException(e.getLocalizedMessage()).initCause(e);
            } else if (!FileChangedManager.getInstance().exists(f.getParentFile())) {
                fex = (FileNotFoundException)new FileNotFoundException(e.getLocalizedMessage()).initCause(e);
            }
            FSException.annotateException(fex);
            throw fex;
        }
        return retVal;
    }

    public InputStream getInputStream() throws FileNotFoundException {
        FileInputStream inputStream;
        if (LOGGER.isLoggable(Level.FINE) && EventQueue.isDispatchThread()) {
            LOGGER.log(Level.WARNING, "reading " + (Object)((Object)this), new IllegalStateException("getInputStream invoked in AWT"));
        }
        File f = this.getFileName().getFile();
        if (!this.isValid()) {
            FileObject recreated = null;
            try {
                recreated = this.getFileSystem().findResource(this.getPath());
            }
            catch (FileStateInvalidException ex) {
                LOGGER.log(Level.FINE, "Can't get filesystem for " + this.getPath(), (Throwable)ex);
            }
            if (recreated != null && recreated != this) {
                return recreated.getInputStream();
            }
            FileNotFoundException ex = new FileNotFoundException("FileObject " + (Object)((Object)this) + " is not valid.");
            String msg = NbBundle.getMessage(FileBasedFileSystem.class, (String)"EXC_CannotRead", (Object)f.getName(), (Object)f.getParent());
            Exceptions.attachLocalizedMessage((Throwable)ex, (String)msg);
            FileObj.dumpFileInfo(f, ex);
            throw ex;
        }
        LOGGER.log(Level.FINEST, "FileObj.getInputStream_after_is_valid");
        if (!f.exists()) {
            FileNotFoundException ex = new FileNotFoundException("Can't read " + f);
            String msg = NbBundle.getMessage(FileBasedFileSystem.class, (String)"EXC_CannotRead", (Object)f.getName(), (Object)f.getParent());
            Exceptions.attachLocalizedMessage((Throwable)ex, (String)msg);
            FileObj.dumpFileInfo(f, ex);
            throw ex;
        }
        MutualExclusionSupport.Closeable closeableReference = null;
        try {
            MutualExclusionSupport.Closeable closable;
            if (Utilities.isWindows() ? this.getNameExt().toLowerCase().startsWith("ntuser.dat") : !f.isFile()) {
                return new ByteArrayInputStream(new byte[0]);
            }
            closeableReference = closable = MUT_EXCL_SUPPORT.addResource((Object)this, true);
            inputStream = new FileInputStream(f){

                @Override
                public void close() throws IOException {
                    super.close();
                    closable.close();
                }
            };
        }
        catch (IOException e) {
            if (closeableReference != null) {
                closeableReference.close();
            }
            FileNotFoundException fex = null;
            fex = !FileChangedManager.getInstance().exists(f) ? (FileNotFoundException)new FileNotFoundException(e.getLocalizedMessage()).initCause(e) : (!f.canRead() ? (FileNotFoundException)new FileNotFoundException(e.getLocalizedMessage()).initCause(e) : (f.getParentFile() == null ? (FileNotFoundException)new FileNotFoundException(e.getLocalizedMessage()).initCause(e) : (!FileChangedManager.getInstance().exists(f.getParentFile()) ? (FileNotFoundException)new FileNotFoundException(e.getLocalizedMessage()).initCause(e) : (new FileInfo(f).isUnixSpecialFile() ? (FileNotFoundException)new FileNotFoundException(e.toString()).initCause(e) : (FileNotFoundException)new FileNotFoundException(e.toString()).initCause(e)))));
            FSException.annotateException(fex);
            throw fex;
        }
        assert (inputStream != null);
        return inputStream;
    }

    @Override
    public boolean isReadOnly() {
        File f = this.getFileName().getFile();
        boolean res = !Utilities.isWindows() && !f.isFile() ? true : super.isReadOnly();
        this.markReadOnly(res);
        return res;
    }

    @Override
    public boolean canWrite() {
        File f = this.getFileName().getFile();
        if (!Utilities.isWindows() && !f.isFile()) {
            return false;
        }
        return super.canWrite();
    }

    final void setLastModified(long lastModified, File forFile, boolean readOnly) {
        if (this.getLastModified() != 0) {
            if (this.getLastModified() != -1 && !this.realLastModifiedCached) {
                this.realLastModifiedCached = true;
            }
            if (LOGGER.isLoggable(Level.FINER)) {
                Exception trace = LOGGER.isLoggable(Level.FINEST) ? new Exception("StackTrace") : null;
                LOGGER.log(Level.FINER, "setLastModified: " + this.getLastModified() + " -> " + lastModified + " (" + (Object)((Object)this) + ") on " + forFile, trace);
            }
            this.setLastModified(lastModified, readOnly);
        }
    }

    public final FileObject createFolder(String name) throws IOException {
        throw new IOException(this.getPath());
    }

    public final FileObject createData(String name, String ext) throws IOException {
        throw new IOException(this.getPath());
    }

    public final FileObject[] getChildren() {
        return new FileObject[0];
    }

    public final FileObject getFileObject(String name, String ext) {
        return null;
    }

    @Override
    public boolean isValid() {
        boolean retval = this.getLastModified() != 0;
        return retval && super.isValid();
    }

    @Override
    protected void setValid(boolean valid) {
        if (LOGGER.isLoggable(Level.FINEST)) {
            LOGGER.log(Level.FINEST, "setValid: " + valid + " (" + (Object)((Object)this) + ")", new Exception("Stack trace"));
        }
        if (valid) {
            assert (this.isValid());
        } else {
            this.setLastModified(0, true);
        }
    }

    public final boolean isFolder() {
        return false;
    }

    @Override
    public void refreshImpl(boolean expected, boolean fire) {
        File file;
        boolean isModified;
        long oldLastModified = this.getLastModified();
        boolean isReadOnly = this.thinksReadOnly();
        boolean isReal = this.realLastModifiedCached;
        this.setLastModified(file.lastModified(), file, !(file = this.getFileName().getFile()).canWrite());
        boolean bl = isReal ? oldLastModified != this.getLastModified() : (isModified = oldLastModified < this.getLastModified());
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.log(Level.FINER, "refreshImpl for {0} isReal: {1} isModified: {2} oldLastModified: {3} lastModified: {4}", new Object[]{this, isReal, isModified, oldLastModified, this.getLastModified()});
        }
        if (fire && oldLastModified != -1 && this.getLastModified() != -1 && this.getLastModified() != 0 && isModified && !MUT_EXCL_SUPPORT.isBeingWritten(this)) {
            this.fireFileChangedEvent(expected);
        }
        if (fire && isReal && isReadOnly != this.thinksReadOnly() && this.getLastModified() != 0) {
            this.fireFileAttributeChangedEvent("DataEditorSupport.read-only.refresh", null, null);
        }
    }

    public final void refresh(boolean expected) {
        this.refresh(expected, true);
    }

    public final Enumeration<FileObject> getChildren(boolean rec) {
        return Enumerations.empty();
    }

    public final Enumeration<FileObject> getFolders(boolean rec) {
        return Enumerations.empty();
    }

    public final Enumeration<FileObject> getData(boolean rec) {
        return Enumerations.empty();
    }

    public final FileLock lock() throws IOException {
        File me = this.getFileName().getFile();
        if (!this.getProvidedExtensions().canWrite(me)) {
            FSException.io("EXC_CannotLock", me);
        }
        try {
            LockForFile result = LockForFile.tryLock(me);
            try {
                this.getProvidedExtensions().fileLocked(this);
            }
            catch (IOException ex) {
                result.releaseLock(false);
                throw ex;
            }
            return result;
        }
        catch (FileNotFoundException ex) {
            FileNotFoundException fex = ex;
            if (!FileChangedManager.getInstance().exists(me)) {
                fex = (FileNotFoundException)new FileNotFoundException(ex.getLocalizedMessage()).initCause(ex);
            } else if (!me.canRead()) {
                fex = (FileNotFoundException)new FileNotFoundException(ex.getLocalizedMessage()).initCause(ex);
            } else if (!me.canWrite()) {
                fex = (FileNotFoundException)new FileNotFoundException(ex.getLocalizedMessage()).initCause(ex);
            } else if (me.getParentFile() == null) {
                fex = (FileNotFoundException)new FileNotFoundException(ex.getLocalizedMessage()).initCause(ex);
            } else if (!FileChangedManager.getInstance().exists(me.getParentFile())) {
                fex = (FileNotFoundException)new FileNotFoundException(ex.getLocalizedMessage()).initCause(ex);
            }
            FSException.annotateException(fex);
            throw fex;
        }
    }

    public final boolean isLocked() {
        File me = this.getFileName().getFile();
        LockForFile l = LockForFile.findValid(me);
        return l != null && l.isValid();
    }

    @Override
    final boolean checkLock(FileLock lock) throws IOException {
        File f = this.getFileName().getFile();
        return lock instanceof LockForFile && Utils.equals(((LockForFile)lock).getFile(), f);
    }

    @Override
    public void rename(FileLock lock, String name, String ext, ProvidedExtensions.IOHandler handler) throws IOException {
        super.rename(lock, name, ext, handler);
        File rename = this.getFileName().getFile();
        this.setLastModified(rename.lastModified(), rename, !rename.canWrite());
    }

    private long getLastModified() {
        long l = this.lastModified;
        if (l < -10) {
            return - l;
        }
        return l;
    }

    private void setLastModified(long lastModified, boolean readOnly) {
        if (lastModified >= -10 && lastModified < 10) {
            this.lastModified = lastModified;
            return;
        }
        this.lastModified = readOnly ? - lastModified : lastModified;
    }

    private boolean thinksReadOnly() {
        return this.lastModified < -10;
    }

    private void markReadOnly(boolean readOnly) {
        if (this.thinksReadOnly() != readOnly) {
            this.setLastModified(this.getLastModified(), readOnly);
        }
    }

}

