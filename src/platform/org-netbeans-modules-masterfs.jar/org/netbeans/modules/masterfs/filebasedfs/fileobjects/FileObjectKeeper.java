/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.masterfs.filebasedfs.fileobjects;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.InvalidPathException;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.BaseFileObj;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.FileObjectFactory;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.FolderObj;
import org.netbeans.modules.masterfs.filebasedfs.naming.FileNaming;
import org.netbeans.modules.masterfs.providers.ProvidedExtensions;
import org.netbeans.modules.masterfs.watcher.Watcher;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.util.Exceptions;

final class FileObjectKeeper
implements FileChangeListener {
    private static final Logger LOG = Logger.getLogger(FileObjectKeeper.class.getName());
    private static final Object TIME_STAMP_LOCK = new Object();
    private Set<FolderObj> kept;
    private Collection<FileChangeListener> listeners;
    private final FolderObj root;
    private long timeStamp;

    public FileObjectKeeper(FolderObj root) {
        this.root = root;
    }

    public synchronized void addRecursiveListener(FileChangeListener fcl) {
        if (this.listeners == null) {
            this.listeners = new CopyOnWriteArraySet<FileChangeListener>();
        }
        LOG.log(Level.FINEST, "addRecursiveListener for {0} isEmpty: {1}", new Object[]{this.root, this.listeners.isEmpty()});
        if (this.listeners.isEmpty()) {
            Callable stop = null;
            boolean deepClass = fcl.getClass().getName().equals("org.openide.filesystems.DeepListener");
            if (fcl instanceof Callable && deepClass) {
                stop = (Callable)fcl;
            }
            FileFilter filter = null;
            if (fcl instanceof FileFilter && deepClass) {
                filter = (FileFilter)fcl;
            }
            try {
                this.listenToAll(stop, filter);
            }
            catch (Error e) {
                LOG.log(Level.WARNING, null, e);
                throw e;
            }
            catch (RuntimeException e) {
                LOG.log(Level.WARNING, null, e);
                throw e;
            }
        }
        this.listeners.add(fcl);
    }

    public synchronized void removeRecursiveListener(FileChangeListener fcl) {
        if (this.listeners == null) {
            return;
        }
        this.listeners.remove((Object)fcl);
        LOG.log(Level.FINEST, "removeRecursiveListener for {0} isEmpty: {1}", new Object[]{this.root, this.listeners.isEmpty()});
        if (this.listeners.isEmpty()) {
            try {
                this.listenNoMore();
            }
            catch (Error e) {
                LOG.log(Level.WARNING, null, e);
                throw e;
            }
            catch (RuntimeException e) {
                LOG.log(Level.WARNING, null, e);
                throw e;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public List<File> init(long previous, FileObjectFactory factory, boolean expected) {
        boolean recursive;
        Object object = TIME_STAMP_LOCK;
        synchronized (object) {
            boolean bl = recursive = this.timeStamp < -1;
            if (this.timeStamp > 0) {
                this.timeStamp = - this.timeStamp;
            }
            if (this.timeStamp == 0) {
                this.timeStamp = -2;
            }
        }
        File file = Watcher.wrap(this.root.getFileName().getFile(), this.root);
        ArrayList<File> arr = new ArrayList<File>();
        long ts = this.root.getProvidedExtensions().refreshRecursively(file, previous, arr);
        try {
            for (File f : arr) {
                if (f.isDirectory()) continue;
                long lm = f.lastModified();
                LOG.log(Level.FINE, "  check {0} for {1}", new Object[]{lm, f});
                if (lm > ts) {
                    ts = lm;
                }
                if (lm <= previous || factory == null || recursive) continue;
                BaseFileObj prevFO = factory.getCachedOnly(f);
                if (prevFO == null) {
                    BaseFileObj who = factory.getValidFileObject(f, FileObjectFactory.Caller.GetChildern);
                    if (who != null) {
                        LOG.log(Level.FINE, "External change detected {0}", (Object)who);
                        if (who.isData()) {
                            who.fireFileDataCreatedEvent(expected);
                            continue;
                        }
                        who.fireFileFolderCreatedEvent(expected);
                        continue;
                    }
                    LOG.log(Level.FINE, "Cannot get valid FileObject. File probably removed: {0}", f);
                    continue;
                }
                LOG.log(Level.FINE, "Do classical refresh for {0}", (Object)prevFO);
                prevFO.refresh(expected, true);
            }
        }
        catch (StackOverflowError ex2) {
            Exceptions.attachMessage((Throwable)ex2, (String)("FileObjectKeeper.init for " + (Object)((Object)this.root) + " timeStamp: " + this.timeStamp + " recursive: " + recursive));
            throw ex2;
        }
        Object ex2 = TIME_STAMP_LOCK;
        synchronized (ex2) {
            if (!recursive) {
                this.timeStamp = ts;
            }
        }
        LOG.log(Level.FINE, "Testing {0}, time {1}", new Object[]{file, this.timeStamp});
        return arr;
    }

    private void listenTo(FileObject fo, boolean add, Collection<? super File> children) {
        if (add) {
            fo.addFileChangeListener((FileChangeListener)this);
            if (fo instanceof FolderObj) {
                FolderObj folder = (FolderObj)fo;
                folder.getKeeper(children);
                folder.getChildren();
                assert (Thread.holdsLock(this));
                Set<FolderObj> k = this.kept;
                if (k != null) {
                    k.add(folder);
                }
            }
            LOG.log(Level.FINER, "Listening to {0}", (Object)fo);
        } else {
            fo.removeFileChangeListener((FileChangeListener)this);
            LOG.log(Level.FINER, "Ignoring {0}", (Object)fo);
        }
    }

    private void listenToAll(Callable<?> stop, FileFilter filter) {
        assert (Thread.holdsLock(this));
        assert (this.kept == null);
        this.kept = new HashSet<FolderObj>();
        this.listenToAllRecursion(this.root, null, stop, filter);
    }

    private boolean listenToAllRecursion(FolderObj obj, FileObjectFactory knownFactory, Callable<?> stop, FileFilter filter) {
        ArrayList it = new ArrayList();
        this.listenTo(obj, true, it);
        FileObjectFactory factory = knownFactory;
        for (File f : it) {
            LOG.log(Level.FINEST, "listenToAll, processing {0}", f);
            if (f == null || FileObjectKeeper.isCyclicSymlink(f)) {
                return false;
            }
            if (factory == null) {
                factory = FileObjectFactory.getInstance(f);
            }
            BaseFileObj fo = factory.getValidFileObject(f, FileObjectFactory.Caller.Others);
            LOG.log(Level.FINEST, "listenToAll, check {0} for stop {1}", new Object[]{fo, stop});
            if (!(fo instanceof FolderObj)) continue;
            FolderObj child = (FolderObj)fo;
            if (filter != null && !filter.accept(child.getFileName().getFile())) continue;
            Boolean shallStop = null;
            if (stop != null) {
                try {
                    shallStop = (Boolean)stop.call();
                }
                catch (Exception ex) {
                    shallStop = Boolean.TRUE;
                }
            }
            if (Boolean.TRUE.equals(shallStop)) {
                LOG.log(Level.INFO, "addRecursiveListener to {0} interrupted", (Object)child);
                return false;
            }
            if (this.listenToAllRecursion(child, factory, stop, filter)) continue;
            return false;
        }
        return true;
    }

    private void listenNoMore() {
        assert (Thread.holdsLock(this));
        this.listenTo(this.root, false, null);
        Set<FolderObj> k = this.kept;
        if (k != null) {
            for (FolderObj fo : k) {
                this.listenTo(fo, false, null);
            }
            this.kept = null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void fileFolderCreated(FileEvent fe) {
        Object obj;
        Collection<FileChangeListener> arr = this.listeners;
        FileObject folder = fe.getFile();
        if (folder instanceof FolderObj) {
            obj = (FolderObj)folder;
            FileObjectKeeper fileObjectKeeper = this;
            synchronized (fileObjectKeeper) {
                this.fileFolderCreatedRecursion((FolderObj)((Object)obj), null);
            }
        }
        obj = this;
        synchronized (obj) {
            assert (Thread.holdsLock(this));
            if (arr == null || this.kept == null) {
                return;
            }
        }
        for (FileChangeListener l : arr) {
            l.fileFolderCreated(fe);
        }
    }

    private void fileFolderCreatedRecursion(FolderObj obj, FileObjectFactory knownFactory) {
        ArrayList it = new ArrayList();
        this.listenTo(obj, true, it);
        FileObjectFactory factory = knownFactory;
        for (File f : it) {
            BaseFileObj fo;
            if (factory == null) {
                factory = FileObjectFactory.getInstance(f);
            }
            if (!((fo = factory.getValidFileObject(f, FileObjectFactory.Caller.Others)) instanceof FolderObj)) continue;
            this.fileFolderCreatedRecursion((FolderObj)fo, factory);
        }
    }

    public void fileDataCreated(FileEvent fe) {
        Collection<FileChangeListener> arr = this.listeners;
        if (arr == null) {
            return;
        }
        for (FileChangeListener l : arr) {
            l.fileDataCreated(fe);
        }
    }

    public void fileChanged(FileEvent fe) {
        Collection<FileChangeListener> arr = this.listeners;
        if (arr == null) {
            return;
        }
        for (FileChangeListener l : arr) {
            l.fileChanged(fe);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void fileDeleted(FileEvent fe) {
        Collection<FileChangeListener> arr = this.listeners;
        FileObject f = fe.getFile();
        if (f.isFolder() && fe.getSource() == f && f != this.root) {
            return;
        }
        if (f instanceof FolderObj) {
            FolderObj obj = (FolderObj)f;
            FileObjectKeeper fileObjectKeeper = this;
            synchronized (fileObjectKeeper) {
                assert (Thread.holdsLock(this));
                if (this.kept != null) {
                    this.kept.remove((Object)obj);
                }
                this.listenTo(obj, false, null);
            }
        }
        if (arr == null) {
            return;
        }
        for (FileChangeListener l : arr) {
            l.fileDeleted(fe);
        }
    }

    public void fileRenamed(FileRenameEvent fe) {
        Collection<FileChangeListener> arr = this.listeners;
        if (arr == null) {
            return;
        }
        FileObject f = fe.getFile();
        if (f.isFolder() && fe.getSource() == f && f != this.root) {
            return;
        }
        for (FileChangeListener l : arr) {
            l.fileRenamed(fe);
        }
    }

    public void fileAttributeChanged(FileAttributeEvent fe) {
        Collection<FileChangeListener> arr = this.listeners;
        if (arr == null) {
            return;
        }
        for (FileChangeListener l : arr) {
            l.fileAttributeChanged(fe);
        }
    }

    long childrenLastModified() {
        return this.timeStamp == -2 ? 0 : Math.abs(this.timeStamp);
    }

    synchronized boolean isOn() {
        assert (Thread.holdsLock(this));
        if (this.kept != null) {
            return true;
        }
        FolderObj obj = this.root.getExistingParent();
        return obj != null && obj.hasRecursiveListener();
    }

    private static boolean isCyclicSymlink(File f) {
        Path file;
        try {
            file = f.toPath();
        }
        catch (InvalidPathException ex) {
            LOG.log(Level.INFO, null, ex);
            return false;
        }
        Path ancestor = file.getParent();
        Path realFile = null;
        while (ancestor != null && ancestor.getFileName() != null) {
            if (ancestor.getFileName().equals(file.getFileName())) {
                try {
                    if (realFile == null) {
                        realFile = file.toRealPath(new LinkOption[0]);
                    }
                    if (realFile.equals(ancestor.toRealPath(new LinkOption[0]))) {
                        return true;
                    }
                }
                catch (IOException ex) {
                    LOG.log(Level.INFO, "Can't convert to cannonical files {0} and {1}", new Object[]{file, ancestor});
                    LOG.log(Level.FINE, null, ex);
                }
            }
            ancestor = ancestor.getParent();
        }
        return false;
    }
}

