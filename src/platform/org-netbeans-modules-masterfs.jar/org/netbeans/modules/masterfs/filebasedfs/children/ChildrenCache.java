/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Privileged
 */
package org.netbeans.modules.masterfs.filebasedfs.children;

import java.util.Map;
import java.util.Set;
import org.netbeans.modules.masterfs.filebasedfs.naming.FileNaming;
import org.openide.util.Mutex;

public interface ChildrenCache {
    public static final Integer ADDED_CHILD = new Integer(0);
    public static final Integer REMOVED_CHILD = new Integer(1);

    public FileNaming getChild(String var1, boolean var2, Runnable[] var3);

    public FileNaming getChild(String var1, boolean var2);

    public void removeChild(FileNaming var1);

    public Set<FileNaming> getChildren(boolean var1, Runnable[] var2);

    public Set<FileNaming> getCachedChildren();

    public boolean isCacheInitialized();

    public Map<FileNaming, Integer> refresh(Runnable[] var1);

    public Mutex.Privileged getMutexPrivileged();
}

