/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.masterfs.filebasedfs;

import org.netbeans.modules.masterfs.filebasedfs.fileobjects.FileObjectFactory;
import org.netbeans.modules.masterfs.filebasedfs.naming.NamingFactory;

public final class Statistics {
    public static final TimeConsumer REFRESH_FOLDER = new TimeConsumer("Folder refresh");
    public static final TimeConsumer REFRESH_FILE = new TimeConsumer("File refresh");
    public static final TimeConsumer REFRESH_FS = new TimeConsumer("FileSystem refresh");
    public static final TimeConsumer LISTENERS_CALLS = new TimeConsumer("Invocation of FileChangeListeners");

    private Statistics() {
    }

    public static StopWatch getStopWatch(TimeConsumer consumer) {
        return new StopWatch(consumer);
    }

    public static int fileSystems() {
        return FileObjectFactory.getFactoriesSize();
    }

    public static int fileNamings() {
        return NamingFactory.getSize();
    }

    public static int fileObjects() {
        int retVal = 0;
        for (FileObjectFactory fbs : FileObjectFactory.getInstances()) {
            retVal += Statistics.fileObjectsPerFileSystem(fbs);
        }
        return retVal;
    }

    public static int fileObjectsPerFileSystem(FileObjectFactory factory) {
        return factory.getSize();
    }

    public static final class StopWatch {
        private long startTime = 0;
        private final TimeConsumer activity;

        private StopWatch(TimeConsumer activity) {
            this.activity = activity;
        }

        public void start() {
            this.startTime = System.currentTimeMillis();
        }

        public void stop() {
            assert (this.startTime != 0);
            this.activity.elapsedTime = (int)((long)this.activity.elapsedTime + (System.currentTimeMillis() - this.startTime));
            this.activity.incrementNumerOfCalls();
            this.startTime = 0;
        }
    }

    public static final class TimeConsumer {
        private int elapsedTime;
        private int numberOfCalls;
        private final String description;

        private TimeConsumer(String description) {
            this.description = description;
        }

        public int getConsumedTime() {
            return this.elapsedTime;
        }

        public int getNumberOfCalls() {
            return this.numberOfCalls;
        }

        public void reset() {
            this.elapsedTime = 0;
            this.numberOfCalls = 0;
        }

        public String toString() {
            return this.description + ": " + this.numberOfCalls + " calls in " + this.elapsedTime + "ms";
        }

        private void incrementNumerOfCalls() {
            ++this.numberOfCalls;
        }
    }

}

