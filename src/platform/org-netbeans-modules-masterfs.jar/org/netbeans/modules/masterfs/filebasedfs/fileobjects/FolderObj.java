/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Privileged
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.masterfs.filebasedfs.fileobjects;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.SyncFailedException;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.netbeans.modules.masterfs.filebasedfs.FileBasedFileSystem;
import org.netbeans.modules.masterfs.filebasedfs.children.ChildrenCache;
import org.netbeans.modules.masterfs.filebasedfs.children.ChildrenSupport;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.BaseFileObj;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.FileObj;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.FileObjectFactory;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.FileObjectKeeper;
import org.netbeans.modules.masterfs.filebasedfs.naming.FileName;
import org.netbeans.modules.masterfs.filebasedfs.naming.FileNaming;
import org.netbeans.modules.masterfs.filebasedfs.naming.NamingFactory;
import org.netbeans.modules.masterfs.filebasedfs.utils.FSException;
import org.netbeans.modules.masterfs.filebasedfs.utils.FileChangedManager;
import org.netbeans.modules.masterfs.filebasedfs.utils.FileInfo;
import org.netbeans.modules.masterfs.providers.Attributes;
import org.netbeans.modules.masterfs.providers.ProvidedExtensions;
import org.netbeans.modules.masterfs.watcher.Watcher;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;

public final class FolderObj
extends BaseFileObj {
    static final long serialVersionUID = -1022430210876356809L;
    private static final Logger LOG = Logger.getLogger(FolderObj.class.getName());
    private FolderChildrenCache folderChildren;
    volatile boolean valid = true;
    private volatile FileObjectKeeper keeper;

    public FolderObj(File file, FileNaming name) {
        super(file, name);
    }

    public final boolean isFolder() {
        return true;
    }

    public FileObject getFileObject(String relativePath) {
        if (relativePath.equals(".")) {
            return this;
        }
        if (relativePath.indexOf(92) != -1) {
            return null;
        }
        if (relativePath.startsWith("/")) {
            relativePath = relativePath.substring(1);
        }
        File file = new File(this.getFileName().getFile(), relativePath);
        if (relativePath.contains("..") || relativePath.contains("./") || relativePath.contains("/.")) {
            file = FileUtil.normalizeFile((File)file);
        }
        FileObjectFactory factory = this.getFactory();
        assert (factory != null);
        return factory.getValidFileObject(file, FileObjectFactory.Caller.GetFileObject);
    }

    public final FileObject getFileObject(String name, String ext) {
        File file = BaseFileObj.getFile(this.getFileName().getFile(), name, ext);
        FileObjectFactory factory = this.getFactory();
        return name.indexOf("/") == -1 ? factory.getValidFileObject(file, FileObjectFactory.Caller.GetFileObject) : null;
    }

    @Override
    protected boolean noFolderListeners() {
        if (this.noListeners()) {
            for (FileObject f : this.computeChildren(true)) {
                BaseFileObj bfo;
                if (!(f instanceof BaseFileObj) || (bfo = (BaseFileObj)f).noListeners()) continue;
                return false;
            }
            return true;
        }
        return false;
    }

    public final FileObject[] getChildren() {
        return this.computeChildren(false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    private FileObject[] computeChildren(boolean onlyExisting) {
        int counter = 0;
        do {
            LinkedHashMap<FileNaming, BaseFileObj> results = new LinkedHashMap<FileNaming, BaseFileObj>();
            ChildrenCache childrenCache = this.getChildrenCache();
            Mutex.Privileged mutexPrivileged = childrenCache.getMutexPrivileged();
            HashSet<FileNaming> fileNames = null;
            Runnable[] task = new Runnable[1];
            while (fileNames == null) {
                if (task[0] != null) {
                    task[0].run();
                }
                mutexPrivileged.enterWriteAccess();
                try {
                    Set<FileNaming> res = childrenCache.getChildren(counter >= 10, task);
                    if (res == null) continue;
                    fileNames = new HashSet<FileNaming>(res);
                    continue;
                }
                finally {
                    mutexPrivileged.exitWriteAccess();
                    continue;
                }
            }
            LOG.log(Level.FINEST, "computeChildren, filenames: {0}", fileNames);
            FileObjectFactory lfs = this.getFactory();
            Iterator i$ = fileNames.iterator();
            do {
                if (!i$.hasNext()) {
                    return results.values().toArray((T[])new FileObject[0]);
                }
                FileNaming fileName = (FileNaming)i$.next();
                FileInfo fInfo = new FileInfo(fileName.getFile(), 1);
                fInfo.setFileNaming(fileName);
                BaseFileObj fo = onlyExisting ? lfs.getCachedOnly(fileName.getFile()) : lfs.getFileObject(fInfo, FileObjectFactory.Caller.GetChildern);
                if (fo == null) continue;
                FileNaming foFileName = fo.getFileName();
                if (!fo.isValid()) {
                    Level level = counter < 10 ? Level.FINE : Level.WARNING;
                    LOG.log(level, "Invalid fileObject {0}, trying again for {1} with {2}", new Object[]{fo, counter, onlyExisting});
                    if (counter > 5) {
                        onlyExisting = false;
                    }
                    assert (counter < 100);
                    if (counter < 100) break;
                }
                if (fileName != foFileName && counter < 10) break;
                assert (fileName == foFileName);
                results.put(fileName, fo);
            } while (true);
            ++counter;
        } while (true);
    }

    private static String dumpFileNaming(FileNaming fn) {
        if (fn == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("FileName: ").append(fn).append("#").append(Integer.toHexString(fn.hashCode())).append("@").append(Integer.toHexString(System.identityHashCode(fn))).append("\n");
        if (fn instanceof FileName) {
            ((FileName)fn).dumpCreation(sb);
        }
        return sb.toString();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final FileObject createFolderImpl(String name) throws IOException {
        File folder2Create;
        BaseFileObj retVal;
        FileObjectFactory factory;
        if (name.indexOf(92) != -1 || name.indexOf(47) != -1) {
            throw new IllegalArgumentException(name);
        }
        retVal = null;
        ChildrenCache childrenCache = this.getChildrenCache();
        Mutex.Privileged mutexPrivileged = childrenCache.getMutexPrivileged();
        File myFile = this.getFileName().getFile();
        folder2Create = BaseFileObj.getFile(myFile, name, null);
        this.getProvidedExtensions().beforeCreate(this, folder2Create.getName(), true);
        mutexPrivileged.enterWriteAccess();
        try {
            if (!myFile.canWrite()) {
                FSException.io("EXC_CannotCreateFolder", folder2Create.getName(), this.getPath());
            }
            Watcher.lock(this);
            this.createFolder(folder2Create, name);
            FileNaming childName = this.getChildrenCache().getChild(folder2Create.getName(), true);
            if (childName != null && !childName.isDirectory()) {
                childName = NamingFactory.fromFile(this.getFileName(), folder2Create, true);
            }
            if (childName != null) {
                childName = NamingFactory.checkCaseSensitivity(childName, folder2Create);
            }
        }
        finally {
            mutexPrivileged.exitWriteAccess();
        }
        if ((factory = this.getFactory()) != null) {
            BaseFileObj exists = factory.getValidFileObject(folder2Create, FileObjectFactory.Caller.Others);
            if (exists instanceof FolderObj) {
                retVal = (FolderObj)exists;
            } else {
                FSException.io("EXC_CannotCreateFolder", folder2Create.getName(), this.getPath());
            }
        }
        if (retVal != null) {
            retVal.fireFileFolderCreatedEvent(false);
        } else {
            FSException.io("EXC_CannotCreateFolder", folder2Create.getName(), this.getPath());
        }
        this.getProvidedExtensions().createSuccess(retVal);
        return retVal;
    }

    private void createFolder(File folder2Create, String name) throws IOException {
        boolean isSupported = new FileInfo(folder2Create).isSupportedFile();
        ProvidedExtensions extensions = this.getProvidedExtensions();
        if (!isSupported) {
            extensions.createFailure(this, folder2Create.getName(), true);
            FSException.io("EXC_CannotCreateFolder", folder2Create.getName(), this.getPath());
        } else {
            if (FileChangedManager.getInstance().exists(folder2Create)) {
                extensions.createFailure(this, folder2Create.getName(), true);
                SyncFailedException sfe = new SyncFailedException(folder2Create.getAbsolutePath());
                String msg = NbBundle.getMessage(FileBasedFileSystem.class, (String)"EXC_CannotCreateFolder", (Object)folder2Create.getName(), (Object)this.getPath());
                Exceptions.attachLocalizedMessage((Throwable)sfe, (String)msg);
                throw sfe;
            }
            if (!folder2Create.mkdirs()) {
                extensions.createFailure(this, folder2Create.getName(), true);
                FSException.io("EXC_CannotCreateFolder", folder2Create.getName(), this.getPath());
            }
        }
        LogRecord r = new LogRecord(Level.FINEST, "FolderCreated: " + folder2Create.getAbsolutePath());
        r.setParameters(new Object[]{folder2Create});
        Logger.getLogger("org.netbeans.modules.masterfs.filebasedfs.fileobjects.FolderObj").log(r);
    }

    public final FileObject createData(final String name, final String ext) throws IOException {
        FileBasedFileSystem.FSCallable<FileObject> c = new FileBasedFileSystem.FSCallable<FileObject>(){

            @Override
            public FileObject call() throws IOException {
                return FolderObj.this.createDataImpl(name, ext);
            }
        };
        return (FileObject)FileBasedFileSystem.runAsInconsistent(c);
    }

    public final FileObject createFolder(final String name) throws IOException {
        FileBasedFileSystem.FSCallable<FileObject> c = new FileBasedFileSystem.FSCallable<FileObject>(){

            @Override
            public FileObject call() throws IOException {
                return FolderObj.this.createFolderImpl(name);
            }
        };
        return (FileObject)FileBasedFileSystem.runAsInconsistent(c);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final FileObject createDataImpl(String name, String ext) throws IOException {
        File file2Create;
        FileNaming childName;
        if (name.indexOf(92) != -1 || name.indexOf(47) != -1) {
            throw new IOException("Requested name contains invalid characters: " + name);
        }
        ChildrenCache childrenCache = this.getChildrenCache();
        Mutex.Privileged mutexPrivileged = childrenCache.getMutexPrivileged();
        ProvidedExtensions extensions = this.getProvidedExtensions();
        file2Create = BaseFileObj.getFile(this.getFileName().getFile(), name, ext);
        extensions.beforeCreate(this, file2Create.getName(), false);
        mutexPrivileged.enterWriteAccess();
        try {
            Watcher.lock(this);
            this.createData(file2Create);
            childName = this.getChildrenCache().getChild(file2Create.getName(), true);
            if (childName != null && childName.isDirectory()) {
                childName = NamingFactory.fromFile(this.getFileName(), file2Create, true);
            }
            if (childName != null) {
                childName = NamingFactory.checkCaseSensitivity(childName, file2Create);
            }
        }
        finally {
            mutexPrivileged.exitWriteAccess();
        }
        FileObjectFactory factory = this.getFactory();
        BaseFileObj retVal = null;
        if (factory != null) {
            BaseFileObj fo = factory.getValidFileObject(file2Create, FileObjectFactory.Caller.Others);
            try {
                retVal = (FileObj)fo;
            }
            catch (ClassCastException ex) {
                boolean dir = file2Create.isDirectory();
                boolean file = file2Create.isFile();
                Exceptions.attachMessage((Throwable)ex, (String)("isDir: " + dir));
                Exceptions.attachMessage((Throwable)ex, (String)("isFile: " + file));
                Exceptions.attachMessage((Throwable)ex, (String)("file: " + file2Create));
                Exceptions.attachMessage((Throwable)ex, (String)("fo: " + (Object)((Object)fo)));
                Exceptions.attachMessage((Throwable)ex, (String)("fn: " + Integer.toHexString(System.identityHashCode(childName))));
                Exceptions.attachMessage((Throwable)ex, (String)("dump: " + NamingFactory.dumpId(childName.getId())));
                throw ex;
            }
        }
        if (retVal != null) {
            if (retVal instanceof FileObj) {
                retVal.setLastModified(file2Create.lastModified(), file2Create, false);
            }
            retVal.fireFileDataCreatedEvent(false);
        } else {
            FSException.io("EXC_CannotCreateData", file2Create.getName(), this.getPath());
        }
        this.getProvidedExtensions().createSuccess(retVal);
        return retVal;
    }

    private void createData(File file2Create) throws IOException {
        boolean isSupported = new FileInfo(file2Create).isSupportedFile();
        ProvidedExtensions extensions = this.getProvidedExtensions();
        if (!isSupported) {
            extensions.createFailure(this, file2Create.getName(), false);
            FSException.io("EXC_CannotCreateData", file2Create.getName(), this.getPath());
        } else {
            if (FileChangedManager.getInstance().exists(file2Create)) {
                extensions.createFailure(this, file2Create.getName(), false);
                SyncFailedException sfe = new SyncFailedException(file2Create.getAbsolutePath());
                String msg = NbBundle.getMessage(FileBasedFileSystem.class, (String)"EXC_CannotCreateData", (Object)file2Create.getName(), (Object)this.getPath());
                Exceptions.attachLocalizedMessage((Throwable)sfe, (String)msg);
                throw sfe;
            }
            if (!file2Create.createNewFile()) {
                extensions.createFailure(this, file2Create.getName(), false);
                FSException.io("EXC_CannotCreateData", file2Create.getName(), this.getPath());
            }
        }
        LogRecord r = new LogRecord(Level.FINEST, "DataCreated: " + file2Create.getAbsolutePath());
        r.setParameters(new Object[]{file2Create});
        Logger.getLogger("org.netbeans.modules.masterfs.filebasedfs.fileobjects.FolderObj").log(r);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void delete(FileLock lock, ProvidedExtensions.DeleteHandler deleteHandler) throws IOException {
        ArrayDeque<FileObject> all = new ArrayDeque<FileObject>();
        File file = this.getFileName().getFile();
        if (!this.deleteFile(file, all, this.getFactory(), deleteHandler)) {
            FolderObj parent = this.getExistingParent();
            String parentPath = parent != null ? parent.getPath() : file.getParentFile().getAbsolutePath();
            FSException.io("EXC_CannotDelete", file.getName(), parentPath);
        }
        BaseFileObj.attribs.deleteAttributes(file.getAbsolutePath().replace('\\', '/'));
        this.setValid(false);
        for (FileObject fo : all) {
            BaseFileObj toDel;
            block9 : {
                ChildrenCache childrenCache;
                toDel = (BaseFileObj)fo;
                FolderObj existingParent = toDel.getExistingParent();
                ChildrenCache childrenCache2 = childrenCache = existingParent != null ? existingParent.getChildrenCache() : null;
                if (childrenCache != null) {
                    Mutex.Privileged mutexPrivileged;
                    Mutex.Privileged privileged = mutexPrivileged = childrenCache != null ? childrenCache.getMutexPrivileged() : null;
                    if (mutexPrivileged != null) {
                        mutexPrivileged.enterWriteAccess();
                    }
                    try {
                        if (deleteHandler != null) {
                            childrenCache.removeChild(toDel.getFileName());
                            break block9;
                        }
                        childrenCache.getChild(BaseFileObj.getNameExt(file), true);
                    }
                    finally {
                        if (mutexPrivileged != null) {
                            mutexPrivileged.exitWriteAccess();
                        }
                    }
                }
            }
            toDel.setValid(false);
            toDel.fireFileDeletedEvent(false);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void refreshImpl(boolean expected, boolean fire) {
        ChildrenCache cache = this.getChildrenCache();
        Mutex.Privileged mutexPrivileged = cache.getMutexPrivileged();
        long previous = this.keeper == null ? -1 : this.keeper.childrenLastModified();
        Set<FileNaming> oldChildren = null;
        Map<FileNaming, Integer> refreshResult = null;
        Runnable[] task = new Runnable[1];
        while (refreshResult == null) {
            if (task[0] != null) {
                task[0].run();
            }
            mutexPrivileged.enterWriteAccess();
            try {
                oldChildren = cache.getCachedChildren();
                refreshResult = cache.refresh(task);
                continue;
            }
            finally {
                mutexPrivileged.exitWriteAccess();
                continue;
            }
        }
        LOG.log(Level.FINER, "refreshImpl for {0} expected: {1} fire: {2} previous: {3}", new Object[]{this, expected, fire, previous});
        oldChildren.removeAll(refreshResult.keySet());
        for (FileNaming child : oldChildren) {
            BaseFileObj childObj = this.getFactory().getCachedOnly(child.getFile());
            if (childObj == null || !childObj.isData()) continue;
            ((FileObj)childObj).refresh(expected);
        }
        FileObjectFactory factory = this.getFactory();
        for (Map.Entry<FileNaming, Integer> entry : refreshResult.entrySet()) {
            FileNaming child2 = entry.getKey();
            Integer operationId = entry.getValue();
            BaseFileObj newChild = operationId == ChildrenCache.ADDED_CHILD ? factory.getFileObject(new FileInfo(child2.getFile()), FileObjectFactory.Caller.Refresh) : factory.getCachedOnly(child2.getFile());
            BaseFileObj baseFileObj = newChild = newChild != null ? newChild : this.getFileObject(child2.getName());
            if (operationId == ChildrenCache.ADDED_CHILD && newChild != null) {
                if (newChild.isFolder()) {
                    if (!fire) continue;
                    this.getProvidedExtensions().createdExternally(newChild);
                    newChild.fireFileFolderCreatedEvent(expected);
                    continue;
                }
                if (!fire) continue;
                this.getProvidedExtensions().createdExternally(newChild);
                newChild.fireFileDataCreatedEvent(expected);
                continue;
            }
            if (operationId != ChildrenCache.REMOVED_CHILD) continue;
            if (newChild != null) {
                if (!newChild.isValid()) continue;
                if (newChild instanceof FolderObj) {
                    this.getProvidedExtensions().deletedExternally(newChild);
                    ((FolderObj)newChild).refreshImpl(expected, fire);
                    newChild.setValid(false);
                    continue;
                }
                newChild.setValid(false);
                if (!fire) continue;
                this.getProvidedExtensions().deletedExternally(newChild);
                newChild.fireFileDeletedEvent(expected);
                continue;
            }
            File f = child2.getFile();
            if (new FileInfo(f).isConvertibleToFileObject()) continue;
            BaseFileObj fakeInvalid = child2.isFile() ? new FileObj(f, child2) : new FolderObj(f, child2);
            fakeInvalid.setValid(false);
            if (!fire) continue;
            fakeInvalid.fireFileDeletedEvent(expected);
        }
        boolean validityFlag = FileChangedManager.getInstance().exists(this.getFileName().getFile());
        if (!validityFlag) {
            this.getFactory().invalidateSubtree(this, fire, expected);
        }
        if (previous != -1) {
            assert (this.keeper != null);
            this.keeper.init(previous, factory, expected);
        }
    }

    public final void refresh(boolean expected) {
        this.refresh(expected, true);
    }

    private boolean deleteFile(File file, Deque<FileObject> all, FileObjectFactory factory, ProvidedExtensions.DeleteHandler deleteHandler) throws IOException {
        boolean ret;
        boolean retVal;
        File[] arr;
        BaseFileObj aliveFo;
        boolean bl = ret = deleteHandler != null ? deleteHandler.delete(file) : file.delete();
        if (ret) {
            BaseFileObj aliveFo2 = factory.getCachedOnly(file);
            if (aliveFo2 != null) {
                all.addFirst(aliveFo2);
            }
            return true;
        }
        if (!FileChangedManager.getInstance().exists(file)) {
            return false;
        }
        if (file.isDirectory() && (arr = file.listFiles()) != null) {
            for (int i = 0; i < arr.length; ++i) {
                File f2Delete = arr[i];
                if (this.deleteFile(f2Delete, all, factory, deleteHandler)) continue;
                return false;
            }
        }
        boolean bl2 = retVal = deleteHandler != null ? deleteHandler.delete(file) : file.delete();
        if (retVal && (aliveFo = factory.getCachedOnly(file)) != null) {
            all.addFirst(aliveFo);
        }
        return true;
    }

    @Override
    protected void setValid(boolean valid) {
        if (valid) {
            assert (this.isValid());
        } else {
            this.valid = false;
        }
    }

    @Override
    public boolean isValid() {
        return this.valid && super.isValid();
    }

    public final InputStream getInputStream() throws FileNotFoundException {
        throw new FileNotFoundException(this.getPath());
    }

    public final OutputStream getOutputStream(FileLock lock) throws IOException {
        throw new IOException(this.getPath());
    }

    public final FileLock lock() throws IOException {
        return new FileLock();
    }

    @Override
    final boolean checkLock(FileLock lock) throws IOException {
        return true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final ChildrenCache getChildrenCache() {
        Class<FolderChildrenCache> class_ = FolderChildrenCache.class;
        synchronized (FolderChildrenCache.class) {
            if (this.folderChildren == null) {
                this.folderChildren = new FolderChildrenCache();
            }
            // ** MonitorExit[var1_1] (shouldn't be in output)
            return this.folderChildren;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void afterRename() {
        Class<FolderChildrenCache> class_ = FolderChildrenCache.class;
        synchronized (FolderChildrenCache.class) {
            if (this.folderChildren != null) {
                this.folderChildren = this.folderChildren.cloneFor(this.getFileName());
            }
            // ** MonitorExit[var1_1] (shouldn't be in output)
            return;
        }
    }

    public final boolean hasRecursiveListener() {
        FileObjectKeeper k = this.keeper;
        return k != null && k.isOn();
    }

    final synchronized FileObjectKeeper getKeeper(Collection<? super File> arr) {
        if (this.keeper == null) {
            this.keeper = new FileObjectKeeper(this);
            List<File> ch = this.keeper.init(-1, null, false);
            if (arr != null) {
                arr.addAll(ch);
            }
        } else if (arr != null) {
            List<File> ch = this.keeper.init(this.keeper.childrenLastModified(), null, false);
            arr.addAll(ch);
        }
        return this.keeper;
    }

    @Override
    public final void addRecursiveListener(FileChangeListener fcl) {
        this.getKeeper(null).addRecursiveListener(fcl);
    }

    @Override
    public final void removeRecursiveListener(FileChangeListener fcl) {
        this.getKeeper(null).removeRecursiveListener(fcl);
    }

    public final class FolderChildrenCache
    extends ChildrenSupport
    implements ChildrenCache {
        @Override
        public final Set<FileNaming> getChildren(boolean rescan, Runnable[] task) {
            return this.getChildren(FolderObj.this.getFileName(), rescan, task);
        }

        @Override
        public final FileNaming getChild(String childName, boolean rescan) {
            return this.getChild(childName, FolderObj.this.getFileName(), rescan);
        }

        @Override
        public FileNaming getChild(String childName, boolean rescan, Runnable[] task) {
            return this.getChild(childName, FolderObj.this.getFileName(), rescan, task);
        }

        @Override
        public final Map<FileNaming, Integer> refresh(Runnable[] task) {
            return this.refresh(FolderObj.this.getFileName(), task);
        }

        @Override
        public final String toString() {
            return FolderObj.this.getFileName().toString();
        }

        @Override
        public void removeChild(FileNaming childName) {
            this.removeChild(FolderObj.this.getFileName(), childName);
        }

        final FolderChildrenCache cloneFor(FileNaming fileName) {
            FolderChildrenCache newCache = new FolderChildrenCache();
            this.copyTo(newCache, FolderObj.this.getFileName());
            return newCache;
        }
    }

}

