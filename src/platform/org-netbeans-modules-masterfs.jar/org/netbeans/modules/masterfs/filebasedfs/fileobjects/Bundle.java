/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.masterfs.filebasedfs.fileobjects;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String EXC_INVALID_FILE(Object arg0) {
        return NbBundle.getMessage(Bundle.class, (String)"EXC_INVALID_FILE", (Object)arg0);
    }

    private void Bundle() {
    }
}

