/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.masterfs.filebasedfs.naming;

import java.io.File;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import org.netbeans.modules.masterfs.filebasedfs.naming.FileName;
import org.netbeans.modules.masterfs.filebasedfs.naming.FileNaming;
import org.netbeans.modules.masterfs.filebasedfs.naming.FolderName;
import org.netbeans.modules.masterfs.filebasedfs.naming.NameRef;
import org.netbeans.modules.masterfs.filebasedfs.utils.FileInfo;
import org.netbeans.modules.masterfs.filebasedfs.utils.Utils;
import org.netbeans.modules.masterfs.providers.ProvidedExtensions;
import org.openide.util.Utilities;

public final class NamingFactory {
    private static NameRef[] names = new NameRef[2];
    private static int namesCount;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Converted monitor instructions to comments
     * Lifted jumps to return sites
     */
    public static FileNaming fromFile(File file) {
        if (Utilities.isWindows() && file.getPath().length() == 2 && file.getPath().charAt(1) == ':') {
            file = new File(file.getPath() + File.separator);
        }
        ArrayDeque<FileInfo> queue = new ArrayDeque<FileInfo>();
        for (File current = file; current != null; current = current.getParentFile()) {
            queue.addFirst(new FileInfo(current));
        }
        ArrayList checkDirs = new ArrayList();
        FileNaming fileName = null;
        ArrayList list = new ArrayList(queue);
        int i = 0;
        while (i < list.size()) {
            Object fi2;
            FileInfo f = (FileInfo)list.get(i);
            if ("\\\\".equals(f.getFile().getPath())) {
                ++i;
                continue;
            }
            for (Object fi2 : checkDirs) {
                fi2.isDirectory();
            }
            checkDirs.clear();
            FileType type = i == list.size() - 1 ? FileType.unknown : FileType.directory;
            fi2 = NamingFactory.class;
            // MONITORENTER : org.netbeans.modules.masterfs.filebasedfs.naming.NamingFactory.class
            FileNaming fn = NamingFactory.registerInstanceOfFileNaming(fileName, f, type, checkDirs);
            if (fn == null) {
                // MONITOREXIT : fi2
                continue;
            }
            fileName = fn;
            ++i;
            // MONITOREXIT : fi2
        }
        return fileName;
    }

    public static synchronized int getSize() {
        return namesCount;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Converted monitor instructions to comments
     * Lifted jumps to return sites
     */
    public static FileNaming fromFile(FileNaming parentFn, File file, boolean ignoreCache) {
        FileNaming ret;
        FileInfo info = new FileInfo(file);
        ArrayList checkDirs = new ArrayList();
        do {
            Object fileInfo2;
            for (Object fileInfo2 : checkDirs) {
                fileInfo2.isDirectory();
            }
            fileInfo2 = NamingFactory.class;
            // MONITORENTER : org.netbeans.modules.masterfs.filebasedfs.naming.NamingFactory.class
            ret = NamingFactory.registerInstanceOfFileNaming(parentFn, info, null, ignoreCache, FileType.unknown, checkDirs);
            // MONITOREXIT : fileInfo2
        } while (ret == null);
        return ret;
    }

    public static synchronized FileNaming checkCaseSensitivity(FileNaming childName, File f) throws IOException {
        if (!childName.getFile().getName().equals(f.getName())) {
            boolean isCaseSensitive;
            boolean bl = isCaseSensitive = !Utils.equals(new File(f, "a"), new File(f, "A"));
            if (!isCaseSensitive) {
                FileName fn = (FileName)childName;
                fn.updateCase(f.getName());
            }
        }
        return childName;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static FileNaming[] rename(FileNaming fNaming, String newName, ProvidedExtensions.IOHandler handler) throws IOException {
        LinkedHashSet<FileNaming> all = new LinkedHashSet<FileNaming>();
        FileNaming newNaming = fNaming.rename(newName, handler);
        boolean retVal = newNaming != fNaming;
        Class<NamingFactory> class_ = NamingFactory.class;
        synchronized (NamingFactory.class) {
            NamingFactory.collectSubnames(fNaming, all);
            // ** MonitorExit[var6_6] (shouldn't be in output)
            return retVal ? NamingFactory.createArray(newNaming, all) : null;
        }
    }

    private static FileNaming[] createArray(FileNaming first, Collection<FileNaming> rest) {
        FileNaming[] res = new FileNaming[rest.size() + 1];
        res[0] = first;
        int i = 1;
        for (FileNaming fn : rest) {
            res[i++] = fn;
        }
        return res;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static Collection<FileNaming> findSubTree(FileNaming root) {
        LinkedHashSet<FileNaming> all = new LinkedHashSet<FileNaming>();
        Class<NamingFactory> class_ = NamingFactory.class;
        synchronized (NamingFactory.class) {
            NamingFactory.collectSubnames(root, all);
            // ** MonitorExit[var2_2] (shouldn't be in output)
            return all;
        }
    }

    private static void collectSubnames(FileNaming root, Collection<FileNaming> all) {
        assert (Thread.holdsLock(NamingFactory.class));
        HashSet not = new HashSet(names.length);
        for (int i = 0; i < names.length; ++i) {
            block1 : for (NameRef value = NamingFactory.names[i]; value != null; value = value.next()) {
                FileNaming fN = (FileNaming)value.get();
                ArrayDeque<FileNaming> above = new ArrayDeque<FileNaming>();
                FileNaming up = fN;
                do {
                    if (up == null || not.contains(up)) {
                        not.addAll(above);
                        continue block1;
                    }
                    above.addFirst(up);
                    if (root.equals(up) || all.contains(up)) {
                        all.addAll(above);
                        continue block1;
                    }
                    up = up.getParent();
                } while (true);
            }
        }
    }

    public static Integer createID(File file) {
        return Utils.hashCode(file);
    }

    private static FileNaming registerInstanceOfFileNaming(FileNaming parentName, FileInfo file, FileType type, Collection<? super FileInfo> computeDirectoryStatus) {
        return NamingFactory.registerInstanceOfFileNaming(parentName, file, null, false, type, computeDirectoryStatus);
    }

    private static void rehash(int newSize) {
        int i;
        assert (Thread.holdsLock(NamingFactory.class));
        NameRef[] arr = new NameRef[newSize];
        for (i = 0; i < names.length; ++i) {
            NameRef v = names[i];
            if (v == null) continue;
            for (NameRef nr : names[i].disconnectAll()) {
                FileNaming fn = (FileNaming)nr.get();
                if (fn == null) continue;
                Integer id = NamingFactory.createID(fn.getFile());
                int index = Math.abs(id) % arr.length;
                NameRef prev = arr[index];
                arr[index] = nr;
                if (prev == null) {
                    nr.setIndex(index);
                    continue;
                }
                nr.setNext(prev);
            }
        }
        for (i = 0; i < arr.length; ++i) {
            assert (NamingFactory.checkIndex(arr, i));
        }
        names = arr;
    }

    private static FileNaming registerInstanceOfFileNaming(FileNaming parentName, FileInfo file, FileNaming newValue, boolean ignoreCache, FileType type, Collection<? super FileInfo> computeDirectoryStatus) {
        FileNaming retVal;
        assert (Thread.holdsLock(NamingFactory.class));
        NamingFactory.cleanQueue();
        Integer key = NamingFactory.createID(file.getFile());
        int index = Math.abs(key) % names.length;
        NameRef ref = NamingFactory.getReference(names[index], file.getFile());
        FileNaming cachedElement = ref != null ? (FileNaming)ref.get() : null;
        Boolean cachedIsDirectory = null;
        Boolean fileIsDirectory = null;
        if (ignoreCache) {
            if (cachedElement != null) {
                cachedIsDirectory = cachedElement.isDirectory();
                if (!file.isDirectoryComputed()) {
                    computeDirectoryStatus.add(file);
                    return null;
                }
                fileIsDirectory = file.isDirectory();
                if (cachedIsDirectory != fileIsDirectory) {
                    cachedElement = null;
                }
            }
            if (cachedElement != null) {
                try {
                    NamingFactory.checkCaseSensitivity(cachedElement, file.getFile());
                }
                catch (IOException ex) {
                    // empty catch block
                }
            }
        }
        Boolean filesEqual = null;
        if (cachedElement != null && (filesEqual = Boolean.valueOf(Utils.equals(cachedElement.getFile(), file.getFile()))).booleanValue()) {
            retVal = cachedElement;
        } else {
            block23 : {
                if (newValue == null) {
                    if (type == FileType.unknown && !file.isDirectoryComputed()) {
                        computeDirectoryStatus.add(file);
                        return null;
                    }
                    retVal = NamingFactory.createFileNaming(file, key, parentName, type);
                } else {
                    retVal = newValue;
                }
                NameRef refRetVal = new NameRef(retVal);
                NameRef prev = names[index];
                NamingFactory.names[index] = refRetVal;
                if (prev == null) {
                    refRetVal.setIndex(index);
                } else {
                    refRetVal.setNext(prev);
                }
                assert (NamingFactory.checkIndex(names, index));
                if (ref != null) {
                    NameRef nr = refRetVal;
                    do {
                        if (nr.next() == ref) {
                            FileNaming orig = (FileNaming)ref.get();
                            if (orig instanceof FileName) {
                                ((FileName)orig).recordCleanup("cachedElement: " + cachedElement + " ref: " + orig + " file: " + file + " filesEqual: " + filesEqual + " cachedIsDirectory: " + cachedIsDirectory + " fileIsDirectory: " + fileIsDirectory);
                            }
                            ref.clear();
                            nr.skip(ref);
                            break block23;
                        }
                        nr = nr.next();
                    } while (true);
                }
                ++namesCount;
            }
            assert (NamingFactory.checkIndex(names, index));
            if (namesCount * 4 > names.length * 3) {
                NamingFactory.rehash(names.length * 2);
            }
        }
        assert (retVal != null);
        return retVal;
    }

    private static NameRef getReference(NameRef value, File f) {
        while (value != null) {
            FileNaming fn = (FileNaming)value.get();
            if (fn != null && Utils.equals(fn.getFile(), f)) {
                return value;
            }
            value = value.next();
        }
        return null;
    }

    private static FileNaming createFileNaming(FileInfo f, Integer theKey, FileNaming parentName, FileType type) {
        FileName retVal = null;
        if (type.equals((Object)FileType.unknown)) {
            type = f.isDirectory() ? FileType.directory : FileType.file;
        }
        switch (type) {
            case file: {
                retVal = new FileName(parentName, f.getFile(), theKey);
                break;
            }
            case directory: {
                retVal = new FolderName(parentName, f.getFile(), theKey);
            }
        }
        return retVal;
    }

    public static String dumpId(Integer id) {
        return NamingFactory.dump(id, null);
    }

    public static synchronized boolean isValid(FileNaming fn) {
        int index = Math.abs(fn.getId()) % names.length;
        for (NameRef value = NamingFactory.names[index]; value != null; value = value.next()) {
            if (value.get() != fn) continue;
            return true;
        }
        return false;
    }

    static synchronized String dump(Integer id, File file) {
        StringBuilder sb = new StringBuilder();
        String hex = Integer.toHexString(id);
        sb.append("Showing references to ").append(hex).append("\n");
        int cnt = 0;
        int index = Math.abs(id) % names.length;
        for (NameRef value = NamingFactory.names[index]; value != null; value = value.next()) {
            if (file != null && !file.equals(value.getFile())) continue;
            ++cnt;
            NamingFactory.dumpFileNaming(sb, value.get());
        }
        sb.append("References: ").append(cnt);
        return sb.toString();
    }

    private static void dumpFileNaming(StringBuilder sb, Object fn) {
        if (fn == null) {
            sb.append("null");
            return;
        }
        if (fn instanceof FolderName) {
            sb.append("FolderName: ");
        } else {
            sb.append("FileName: ");
        }
        sb.append(fn).append("#").append(Integer.toHexString(fn.hashCode())).append("@").append(Integer.toHexString(System.identityHashCode(fn))).append("\n");
        if (fn instanceof FileName) {
            ((FileName)fn).dumpCreation(sb);
        }
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    private static void cleanQueue() {
        if (!NamingFactory.$assertionsDisabled && !Thread.holdsLock(NamingFactory.class)) {
            throw new AssertionError();
        }
        do lbl-1000: // 3 sources:
        {
            if ((nr = (NameRef)NameRef.QUEUE.poll()) == null) {
                return;
            }
            index = nr.getIndex();
            if (index == -1) ** GOTO lbl-1000
            if (NamingFactory.names[index] == null) continue;
            NamingFactory.names[index] = NamingFactory.names[index].remove(nr);
            --NamingFactory.namesCount;
        } while (NamingFactory.$assertionsDisabled || NamingFactory.checkIndex(NamingFactory.names, index));
        throw new AssertionError();
    }

    private static boolean checkIndex(NameRef[] arr, int index) {
        if (arr[index] == null) {
            return true;
        }
        return index == arr[index].getIndex();
    }

    static enum FileType {
        file,
        directory,
        unknown;
        

        private FileType() {
        }
    }

}

