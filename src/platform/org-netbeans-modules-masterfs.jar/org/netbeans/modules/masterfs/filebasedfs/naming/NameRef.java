/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.masterfs.filebasedfs.naming;

import java.io.File;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import org.netbeans.modules.masterfs.filebasedfs.naming.FileNaming;
import org.netbeans.modules.masterfs.filebasedfs.naming.NamingFactory;

final class NameRef
extends WeakReference<FileNaming> {
    private Object next;
    static final ReferenceQueue<FileNaming> QUEUE = new ReferenceQueue();

    public NameRef(FileNaming referent) {
        super(referent, QUEUE);
    }

    public Integer getIndex() {
        assert (Thread.holdsLock(NamingFactory.class));
        for (NameRef nr = this; nr != null; nr = nr.next()) {
            if (!(nr.next instanceof Integer)) continue;
            return (Integer)nr.next;
        }
        return -1;
    }

    public NameRef next() {
        if (this.next instanceof Integer) {
            return null;
        }
        return (NameRef)this.next;
    }

    public File getFile() {
        FileNaming r = (FileNaming)this.get();
        return r == null ? null : r.getFile();
    }

    public NameRef remove(NameRef what) {
        assert (Thread.holdsLock(NamingFactory.class));
        if (what == this) {
            return this.next();
        }
        NameRef me = this;
        while (me.next != what) {
            if (me.next instanceof Integer) {
                return this;
            }
            me = (NameRef)me.next;
        }
        me.next = me.next().next;
        return this;
    }

    final void setNext(NameRef nr) {
        assert (Thread.holdsLock(NamingFactory.class));
        assert (this.next == null);
        this.next = nr;
    }

    final void setIndex(int index) {
        assert (Thread.holdsLock(NamingFactory.class));
        assert (this.next == null);
        this.next = index;
    }

    final void skip(NameRef ref) {
        assert (Thread.holdsLock(NamingFactory.class));
        assert (this.next == ref);
        assert (ref.get() == null);
        this.next = ref.next;
    }

    final Iterable<NameRef> disconnectAll() {
        assert (Thread.holdsLock(NamingFactory.class));
        ArrayList<NameRef> all = new ArrayList<NameRef>();
        NameRef nr = this;
        while (nr != null) {
            NameRef nn = nr.next();
            nr.next = null;
            if (nr.get() != null) {
                all.add(nr);
            }
            nr = nn;
        }
        return all;
    }
}

