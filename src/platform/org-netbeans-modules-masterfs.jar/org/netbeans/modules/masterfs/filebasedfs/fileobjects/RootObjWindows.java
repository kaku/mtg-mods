/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.util.Enumerations
 */
package org.netbeans.modules.masterfs.filebasedfs.fileobjects;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import org.netbeans.modules.masterfs.filebasedfs.FileBasedFileSystem;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.BaseFileObj;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.FileObjectFactory;
import org.netbeans.modules.masterfs.filebasedfs.utils.FSException;
import org.netbeans.modules.masterfs.filebasedfs.utils.FileInfo;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.util.Enumerations;

public final class RootObjWindows
extends FileObject {
    public final String getName() {
        return "";
    }

    public final String getExt() {
        return "";
    }

    public final FileSystem getFileSystem() throws FileStateInvalidException {
        return FileBasedFileSystem.getInstance();
    }

    public FileObject getFileObject(String relativePath) {
        return super.getFileObject(relativePath);
    }

    public final FileObject getParent() {
        return null;
    }

    public final boolean isFolder() {
        return true;
    }

    public final boolean isData() {
        return !this.isFolder();
    }

    public final Date lastModified() {
        return new Date(0);
    }

    public final boolean isRoot() {
        return true;
    }

    public final boolean isValid() {
        return true;
    }

    public final void rename(FileLock lock, String name, String ext) throws IOException {
        FSException.io("EXC_CannotRenameRoot", this.getFileSystem().getDisplayName());
    }

    public final void delete(FileLock lock) throws IOException {
        FSException.io("EXC_CannotDeleteRoot", this.getFileSystem().getDisplayName());
    }

    public final Object getAttribute(String attrName) {
        return null;
    }

    public final void setAttribute(String attrName, Object value) throws IOException {
        throw new FileStateInvalidException();
    }

    public final Enumeration<String> getAttributes() {
        return Enumerations.empty();
    }

    public final void addFileChangeListener(FileChangeListener fcl) {
    }

    public final void removeFileChangeListener(FileChangeListener fcl) {
    }

    public final long getSize() {
        return 0;
    }

    public final InputStream getInputStream() throws FileNotFoundException {
        throw new FileNotFoundException();
    }

    public final OutputStream getOutputStream(FileLock lock) throws IOException {
        throw new FileNotFoundException();
    }

    public final FileLock lock() throws IOException {
        throw new FileStateInvalidException();
    }

    public final void setImportant(boolean b) {
    }

    public final FileObject[] getChildren() {
        Collection<? extends FileObjectFactory> all = FileBasedFileSystem.factories().values();
        ArrayList<BaseFileObj> rootChildren = new ArrayList<BaseFileObj>();
        for (FileObjectFactory fs : all) {
            rootChildren.add(fs.getRoot());
        }
        return rootChildren.toArray((T[])new FileObject[rootChildren.size()]);
    }

    public final FileObject getFileObject(String name, String ext) {
        FileObject first = this.getFileObjectImpl(name, ext);
        if (first != null) {
            return first;
        }
        if (name.length() >= 2 && name.charAt(1) == ':') {
            File root = new File("" + name.charAt(0) + ":\\");
            FileObjectFactory.getInstance(root, true);
        }
        return this.getFileObjectImpl(name, ext);
    }

    private FileObject getFileObjectImpl(String name, String ext) {
        FileObject[] rootChildren = this.getChildren();
        for (int i = 0; i < rootChildren.length; ++i) {
            String real;
            FileObject fileObject = rootChildren[i];
            if (name.startsWith("//")) {
                name = name.replace('/', '\\');
            }
            if ((real = fileObject.getNameExt()).endsWith("\\")) {
                real = real.substring(0, real.length() - 1);
            }
            if (!FileInfo.composeName(name, ext).equals(real)) continue;
            return fileObject;
        }
        return null;
    }

    public final FileObject createFolder(String name) throws IOException {
        throw new FileStateInvalidException();
    }

    public final FileObject createData(String name, String ext) throws IOException {
        throw new FileStateInvalidException();
    }

    public final boolean isReadOnly() {
        return true;
    }

    public String getPath() {
        return "";
    }

    public String toString() {
        return "";
    }
}

