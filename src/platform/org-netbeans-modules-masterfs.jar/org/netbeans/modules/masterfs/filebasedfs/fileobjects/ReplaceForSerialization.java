/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.masterfs.filebasedfs.fileobjects;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import org.netbeans.modules.masterfs.filebasedfs.FileBasedFileSystem;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.BaseFileObj;
import org.netbeans.modules.masterfs.providers.ProvidedExtensions;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;

public class ReplaceForSerialization
implements Serializable {
    static final long serialVersionUID = -7451332135435542113L;
    private final String absolutePath;

    public ReplaceForSerialization(File file) {
        this.absolutePath = file.getAbsolutePath();
    }

    public final Object readResolve() {
        File file = new File(this.absolutePath);
        FileBasedFileSystem.getInstance();
        FileObject retVal = FileBasedFileSystem.getFileObject(file);
        return retVal != null ? retVal : new Invalid(file);
    }

    private static class Invalid
    extends BaseFileObj {
        protected Invalid(File file) {
            super(file);
        }

        public void delete(FileLock lock, ProvidedExtensions.IOHandler io) throws IOException {
            throw new IOException(this.getPath());
        }

        @Override
        boolean checkLock(FileLock lock) throws IOException {
            return false;
        }

        @Override
        protected void setValid(boolean valid) {
        }

        public boolean isFolder() {
            return false;
        }

        @Override
        public boolean isValid() {
            return false;
        }

        public InputStream getInputStream() throws FileNotFoundException {
            throw new FileNotFoundException(this.getPath());
        }

        public OutputStream getOutputStream(FileLock lock) throws IOException {
            throw new IOException(this.getPath());
        }

        public FileLock lock() throws IOException {
            throw new IOException(this.getPath());
        }

        public FileObject[] getChildren() {
            return new FileObject[0];
        }

        public FileObject getFileObject(String name, String ext) {
            return null;
        }

        public FileObject createFolder(String name) throws IOException {
            throw new IOException(this.getPath());
        }

        public FileObject createData(String name, String ext) throws IOException {
            throw new IOException(this.getPath());
        }

        @Override
        public void refreshImpl(boolean expected, boolean fire) {
        }

        @Override
        protected boolean noFolderListeners() {
            return true;
        }
    }

}

