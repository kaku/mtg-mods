/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileAlreadyLockedException
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.masterfs.filebasedfs.fileobjects;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.BaseFileObj;
import org.netbeans.modules.masterfs.filebasedfs.utils.FileChangedManager;
import org.netbeans.modules.masterfs.filebasedfs.utils.Utils;
import org.netbeans.modules.masterfs.providers.ProvidedExtensions;
import org.openide.filesystems.FileAlreadyLockedException;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;

public class LockForFile
extends FileLock {
    private static final ConcurrentHashMap<String, Namesakes> name2Namesakes = new ConcurrentHashMap();
    private static final String PREFIX = ".LCK";
    private static final String SUFFIX = "~";
    private static final Logger LOGGER = Logger.getLogger(LockForFile.class.getName());
    private File file;
    private File lock;
    private boolean valid = false;

    private LockForFile(File file) {
        this.file = file;
        this.lock = LockForFile.getLockFile(file);
    }

    public static LockForFile findValid(File file) {
        Namesakes namesakes = name2Namesakes.get(file.getName());
        return namesakes != null ? namesakes.getInstance(file) : null;
    }

    public static LockForFile tryLock(File file) throws IOException {
        LockForFile result = new LockForFile(file);
        return LockForFile.registerLock(result);
    }

    private static LockForFile registerLock(LockForFile result) throws IOException, FileAlreadyLockedException {
        File file = result.getFile();
        Namesakes namesakes = new Namesakes();
        Namesakes oldNamesakes = name2Namesakes.putIfAbsent(file.getName(), namesakes);
        if (oldNamesakes != null) {
            namesakes = oldNamesakes;
        }
        if (namesakes.putInstance(file, result) == null) {
            FileAlreadyLockedException alreadyLockedException = new FileAlreadyLockedException(file.getAbsolutePath());
            LockForFile previousLock = namesakes.getInstance(file);
            if (previousLock != null) {
                alreadyLockedException.initCause(previousLock.lockedBy);
            }
            throw alreadyLockedException;
        }
        result.valid = true;
        return result;
    }

    public static void relock(File theOld, File theNew) {
        if (theNew.isDirectory()) {
            Collection<Namesakes> namesakes = name2Namesakes.values();
            for (Namesakes sake : namesakes) {
                Collection all = sake.values();
                for (Reference ref : all) {
                    String relPath;
                    File f;
                    LockForFile lock = (LockForFile)((Object)ref.get());
                    if (lock == null || (relPath = Utils.getRelativePath(theOld, f = lock.getFile())) == null) continue;
                    lock.relock(new File(theNew, relPath));
                }
            }
        } else {
            LockForFile lock = LockForFile.findValid(theOld);
            if (lock != null) {
                lock.relock(theNew);
            }
        }
    }

    private static synchronized void deregisterLock(LockForFile lockForFile) {
        if (lockForFile.isValid()) {
            File file;
            Namesakes namesakes;
            if (lockForFile.isHardLocked()) {
                lockForFile.hardUnlock();
            }
            if ((namesakes = name2Namesakes.get((file = lockForFile.getFile()).getName())) != null) {
                namesakes.remove(file);
                if (namesakes.isEmpty()) {
                    name2Namesakes.remove(file.getName());
                }
            }
        }
    }

    private void relock(File theNew) {
        try {
            LockForFile.deregisterLock(this);
            this.file = theNew;
            this.lock = LockForFile.getLockFile(theNew);
            LockForFile.registerLock(this);
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    boolean hardLock() throws IOException {
        if (this.isHardLocked()) {
            throw new FileAlreadyLockedException(this.file.getAbsolutePath());
        }
        File hardLock = this.getLock();
        hardLock.getParentFile().mkdirs();
        hardLock.createNewFile();
        OutputStream os = Files.newOutputStream(hardLock.toPath(), new OpenOption[0]);
        try {
            os.write(this.getFile().getAbsolutePath().getBytes());
            boolean bl = true;
            return bl;
        }
        finally {
            os.close();
        }
    }

    boolean hardUnlock() {
        return this.getLock().delete();
    }

    private static synchronized boolean hardUnlockAll() {
        boolean result = true;
        Collection<Namesakes> sakes = name2Namesakes.values();
        for (Namesakes namesake : sakes) {
            Collection refs = namesake.values();
            for (Reference reference : refs) {
                LockForFile lockForFile;
                if (reference == null || !(lockForFile = (LockForFile)((Object)reference.get())).isHardLocked() || lockForFile.hardUnlock()) continue;
                result = false;
            }
        }
        return result;
    }

    public File getLock() {
        return this.lock;
    }

    public File getFile() {
        return this.file;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public File getHardLock() {
        if (FileChangedManager.getInstance().exists(this.lock)) {
            FileInputStream is = null;
            try {
                is = new FileInputStream(this.lock);
                byte[] path = new byte[is.available()];
                if (path.length > 0 && is.read(path) == path.length) {
                    File file = new File(new String(path));
                    return file;
                }
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            finally {
                if (is != null) {
                    try {
                        is.close();
                    }
                    catch (IOException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
            }
        }
        return null;
    }

    public boolean isHardLocked() {
        File hLock = this.getHardLock();
        return hLock != null ? LockForFile.findValid(hLock) != null : false;
    }

    public void rename() {
    }

    public static File getLockFile(File file) {
        file = FileUtil.normalizeFile((File)file);
        File parentFile = file.getParentFile();
        StringBuilder sb = new StringBuilder();
        sb.append(".LCK");
        sb.append(file.getName());
        sb.append("~");
        String lckName = sb.toString();
        File lck = new File(parentFile, lckName);
        return lck;
    }

    public boolean isValid() {
        Namesakes namesakes = name2Namesakes.get(this.file.getName());
        Reference ref = namesakes != null ? (Reference)namesakes.get(this.file) : null;
        return ref != null && super.isValid() && this.valid;
    }

    public void releaseLock() {
        this.releaseLock(true);
    }

    final void releaseLock(boolean notify) {
        FileObject fo;
        LockForFile.deregisterLock(this);
        super.releaseLock();
        if (notify && (fo = FileUtil.toFileObject((File)FileUtil.normalizeFile((File)this.file))) instanceof BaseFileObj) {
            ((BaseFileObj)fo).getProvidedExtensions().fileUnlocked(fo);
        }
    }

    static {
        Runtime.getRuntime().addShutdownHook(new Thread(){

            @Override
            public void run() {
                LockForFile.hardUnlockAll();
            }
        });
    }

    private static class Namesakes
    extends ConcurrentHashMap<File, Reference<LockForFile>> {
        private Namesakes() {
        }

        private LockForFile getInstance(File file) {
            Reference ref = (Reference)this.get(file);
            return ref != null ? (LockForFile)((Object)ref.get()) : null;
        }

        private LockForFile putInstance(File file, LockForFile lock) throws IOException {
            Reference old;
            if (!this.isEmpty() && LockForFile.findValid(lock.getFile()) == null) {
                this.hardLock();
                lock.hardLock();
            }
            return (old = (Reference)this.putIfAbsent(file, new WeakReference<LockForFile>(lock))) != null ? null : lock;
        }

        private void hardLock() throws IOException {
            Collection refs = this.values();
            for (Reference reference : refs) {
                LockForFile lockForFile;
                if (reference == null || (lockForFile = (LockForFile)((Object)reference.get())) == null || FileChangedManager.getInstance().exists(lockForFile.getLock())) continue;
                lockForFile.hardLock();
            }
        }
    }

}

