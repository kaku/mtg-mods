/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.masterfs.filebasedfs.naming;

import java.io.File;
import java.io.IOException;
import org.netbeans.modules.masterfs.providers.ProvidedExtensions;

public interface FileNaming {
    public String getName();

    public FileNaming getParent();

    public boolean isRoot();

    public File getFile();

    public boolean isFile();

    public boolean isDirectory();

    public Integer getId();

    public FileNaming rename(String var1, ProvidedExtensions.IOHandler var2) throws IOException;
}

