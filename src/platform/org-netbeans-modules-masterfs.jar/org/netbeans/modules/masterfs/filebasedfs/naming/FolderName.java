/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.masterfs.filebasedfs.naming;

import java.io.File;
import java.util.Map;
import java.util.WeakHashMap;
import org.netbeans.modules.masterfs.filebasedfs.naming.FileName;
import org.netbeans.modules.masterfs.filebasedfs.naming.FileNaming;

public class FolderName
extends FileName {
    private static Map<FolderName, File> fileCache = new WeakHashMap<FolderName, File>();

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    FolderName(FileNaming parent, File file, Integer theKey) {
        super(parent, file, theKey);
        Class<FolderName> class_ = FolderName.class;
        synchronized (FolderName.class) {
            fileCache.put(this, file);
            // ** MonitorExit[var4_4] (shouldn't be in output)
            return;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    void updateCase(String name) {
        super.updateCase(name);
        Class<FolderName> class_ = FolderName.class;
        synchronized (FolderName.class) {
            fileCache.remove(this);
            // ** MonitorExit[var2_2] (shouldn't be in output)
            return;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public File getFile() {
        Class<FolderName> class_ = FolderName.class;
        synchronized (FolderName.class) {
            File retValue = fileCache.get(this);
            if (retValue == null) {
                retValue = super.getFile();
                fileCache.put(this, retValue);
            }
            // ** MonitorExit[var2_1] (shouldn't be in output)
            assert (retValue != null);
            return retValue;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void freeCaches() {
        Class<FolderName> class_ = FolderName.class;
        synchronized (FolderName.class) {
            fileCache = new WeakHashMap<FolderName, File>();
            // ** MonitorExit[var0] (shouldn't be in output)
            return;
        }
    }

    @Override
    public boolean isFile() {
        return false;
    }
}

