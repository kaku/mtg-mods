/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileUtil
 */
package org.netbeans.modules.masterfs.filebasedfs.fileobjects;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.modules.masterfs.filebasedfs.FileBasedFileSystem;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.FileObjectFactory;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.RefreshSlow;
import org.netbeans.modules.masterfs.filebasedfs.utils.FSException;
import org.netbeans.modules.masterfs.filebasedfs.utils.FileInfo;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;

public final class RootObj<T extends FileObject>
extends FileObject {
    private T realRoot = null;

    public RootObj(T realRoot) {
        this.realRoot = realRoot;
    }

    public final String getName() {
        return this.getRealRoot().getName();
    }

    public final String getExt() {
        return this.getRealRoot().getExt();
    }

    public final FileSystem getFileSystem() throws FileStateInvalidException {
        return this.getRealRoot().getFileSystem();
    }

    public final FileObject getParent() {
        return null;
    }

    public final boolean isFolder() {
        return true;
    }

    public final boolean isData() {
        return !this.isFolder();
    }

    public final Date lastModified() {
        return new Date(0);
    }

    public final boolean isRoot() {
        return true;
    }

    public final boolean isValid() {
        return true;
    }

    public final void rename(FileLock lock, String name, String ext) throws IOException {
        FSException.io("EXC_CannotRenameRoot", this.getFileSystem().getDisplayName());
    }

    public final void delete(FileLock lock) throws IOException {
        FSException.io("EXC_CannotDeleteRoot", this.getFileSystem().getDisplayName());
    }

    public final Object getAttribute(String attrName) {
        if (attrName.equals("SupportsRefreshForNoPublicAPI")) {
            return true;
        }
        if (attrName.equals("refreshSlow")) {
            return new RefreshSlow();
        }
        return this.getRealRoot().getAttribute(attrName);
    }

    public final void setAttribute(String attrName, Object value) throws IOException {
        if ("request_for_refreshing_files_be_aware_this_is_not_public_api".equals(attrName) && value instanceof File[]) {
            RootObj.invokeRefreshFor(null, (File[])value);
            return;
        }
        this.getRealRoot().setAttribute(attrName, value);
    }

    static void invokeRefreshFor(RefreshSlow slow, File[] files) {
        RootObj.invokeRefreshFor(slow, files, false);
    }

    static void invokeRefreshFor(RefreshSlow slow, File[] files, boolean ignoreRecursiveListeners) {
        for (int i = 0; i < files.length; ++i) {
            File file = files[i];
            files[i] = FileUtil.normalizeFile((File)file);
        }
        HashMap files2Factory = new HashMap();
        Map<File, ? extends FileObjectFactory> roots2Factory = FileBasedFileSystem.factories();
        Arrays.sort(files);
        for (File file : files) {
            FileObjectFactory factory = roots2Factory.get(file);
            if (factory == null) {
                factory = roots2Factory.get(new FileInfo(file).getRoot().getFile());
            }
            if (factory == null) continue;
            ArrayList<File> lf = (ArrayList<File>)files2Factory.get(factory);
            if (lf == null) {
                lf = new ArrayList<File>();
                files2Factory.put(factory, lf);
            } else {
                File tmp = file;
                while (tmp.getParentFile() != null) {
                    if (lf.contains(tmp)) {
                        tmp = null;
                        break;
                    }
                    tmp = tmp.getParentFile();
                }
                if (tmp == null) continue;
            }
            lf.add(file);
        }
        if (slow != null) {
            int cnt = 0;
            for (Map.Entry entry : files2Factory.entrySet()) {
                FileObjectFactory factory = (FileObjectFactory)entry.getKey();
                cnt += factory.getSize();
            }
            slow.estimate(cnt);
        }
        for (Map.Entry entry : files2Factory.entrySet()) {
            FileObjectFactory factory = (FileObjectFactory)entry.getKey();
            List lf = (List)entry.getValue();
            if (lf.size() == 1) {
                for (File file2 : lf) {
                    if (file2.getParentFile() == null) {
                        factory.refresh(slow, ignoreRecursiveListeners, true);
                        continue;
                    }
                    factory.refreshFor(slow, ignoreRecursiveListeners, file2);
                }
                continue;
            }
            if (lf.size() <= 1) continue;
            Object[] arr = lf.toArray(new File[lf.size()]);
            Arrays.sort(arr);
            factory.refreshFor(slow, ignoreRecursiveListeners, (File[])arr);
        }
    }

    public final Enumeration<String> getAttributes() {
        return this.getRealRoot().getAttributes();
    }

    public final void addFileChangeListener(FileChangeListener fcl) {
        this.getRealRoot().addFileChangeListener(fcl);
    }

    public final void removeFileChangeListener(FileChangeListener fcl) {
        this.getRealRoot().removeFileChangeListener(fcl);
    }

    public final long getSize() {
        return 0;
    }

    public final InputStream getInputStream() throws FileNotFoundException {
        return this.getRealRoot().getInputStream();
    }

    public final OutputStream getOutputStream(FileLock lock) throws IOException {
        return this.getRealRoot().getOutputStream(lock);
    }

    public final FileLock lock() throws IOException {
        return this.getRealRoot().lock();
    }

    @Deprecated
    public final void setImportant(boolean b) {
        this.getRealRoot().setImportant(b);
    }

    public final FileObject[] getChildren() {
        return this.getRealRoot().getChildren();
    }

    public final FileObject getFileObject(String name, String ext) {
        return this.getRealRoot().getFileObject(name, ext);
    }

    public final FileObject getFileObject(String relativePath) {
        return this.getRealRoot().getFileObject(relativePath);
    }

    public final FileObject createFolder(String name) throws IOException {
        return this.getRealRoot().createFolder(name);
    }

    public final FileObject createData(String name, String ext) throws IOException {
        return this.getRealRoot().createData(name, ext);
    }

    @Deprecated
    public final boolean isReadOnly() {
        return this.getRealRoot().isReadOnly();
    }

    public final T getRealRoot() {
        return this.realRoot;
    }

    public String getPath() {
        return "";
    }

    public String toString() {
        return this.getRealRoot().toString();
    }
}

