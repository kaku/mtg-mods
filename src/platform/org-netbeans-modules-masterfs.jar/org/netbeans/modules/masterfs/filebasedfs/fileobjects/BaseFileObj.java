/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.AbstractFileSystem
 *  org.openide.filesystems.AbstractFileSystem$Change
 *  org.openide.filesystems.AbstractFileSystem$Info
 *  org.openide.filesystems.AbstractFileSystem$List
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$Status
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Enumerations
 *  org.openide.util.Exceptions
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Privileged
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.masterfs.filebasedfs.fileobjects;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.EventListener;
import java.util.HashSet;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.EventListenerList;
import org.netbeans.modules.masterfs.filebasedfs.FileBasedFileSystem;
import org.netbeans.modules.masterfs.filebasedfs.Statistics;
import org.netbeans.modules.masterfs.filebasedfs.children.ChildrenCache;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.FileObjectFactory;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.FolderObj;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.LockForFile;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.ReplaceForSerialization;
import org.netbeans.modules.masterfs.filebasedfs.naming.FileNaming;
import org.netbeans.modules.masterfs.filebasedfs.naming.NamingFactory;
import org.netbeans.modules.masterfs.filebasedfs.utils.FSException;
import org.netbeans.modules.masterfs.filebasedfs.utils.FileChangedManager;
import org.netbeans.modules.masterfs.filebasedfs.utils.FileInfo;
import org.netbeans.modules.masterfs.filebasedfs.utils.Utils;
import org.netbeans.modules.masterfs.providers.Attributes;
import org.netbeans.modules.masterfs.providers.ProvidedExtensions;
import org.netbeans.modules.masterfs.watcher.Watcher;
import org.openide.filesystems.AbstractFileSystem;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.util.Enumerations;
import org.openide.util.Exceptions;
import org.openide.util.Mutex;
import org.openide.util.Utilities;

public abstract class BaseFileObj
extends FileObject {
    private static final String PATH_SEPARATOR = File.separator;
    private static final char EXT_SEP = '.';
    static final Logger LOG = Logger.getLogger(BaseFileObj.class.getName());
    static final ThreadLocal<Long> MOVED_FILE_TIMESTAMP = new ThreadLocal<T>();
    static final long serialVersionUID = -1244650210876356809L;
    static final Attributes attribs;
    private static final Object EVENT_SUPPORT_LOCK;
    private EventListenerList eventSupport;
    private FileNaming fileName;

    protected BaseFileObj(File file) {
        this(file, NamingFactory.fromFile(file));
    }

    protected BaseFileObj(File file, FileNaming name) {
        assert (name != null);
        this.fileName = name;
    }

    public final String toString() {
        Object[] arrobject = new Object[4];
        arrobject[0] = this.fileName;
        arrobject[1] = this.fileName;
        arrobject[2] = this;
        arrobject[3] = this.isValid() ? "" : "[invalid]";
        return String.format("%s@%h:%h%s", arrobject);
    }

    public final String getNameExt() {
        String ne = this.getFileName().getName();
        while (ne.endsWith("\\")) {
            ne = ne.substring(0, ne.length() - 1);
        }
        return ne;
    }

    private static boolean isUncRoot(File file) {
        File parent;
        if (file.getPath().startsWith("\\\\") && (parent = file.getParentFile()) != null && (parent = parent.getParentFile()) != null) {
            return parent.getPath().equals("\\\\");
        }
        return false;
    }

    static String getNameExt(File file) {
        String retVal;
        String string = retVal = file.getParent() == null || BaseFileObj.isUncRoot(file) ? file.getAbsolutePath() : file.getName();
        if (retVal.endsWith(PATH_SEPARATOR)) {
            boolean isPermittedToStripSlash;
            boolean bl = isPermittedToStripSlash = file.getParent() != null || !new FileInfo(file).isUNCFolder();
            if (isPermittedToStripSlash) {
                retVal = retVal.substring(0, retVal.length() - 1);
            }
        }
        return retVal;
    }

    public boolean canRead() {
        File file = this.getFileName().getFile();
        return file.canRead();
    }

    public boolean canWrite() {
        File file = this.getFileName().getFile();
        ProvidedExtensions extension = this.getProvidedExtensions();
        return extension.canWrite(file);
    }

    public final boolean isData() {
        return !this.isFolder();
    }

    public final String getName() {
        return FileInfo.getName(this.getNameExt());
    }

    public final String getExt() {
        return FileInfo.getExt(this.getNameExt());
    }

    public final String getPath() {
        ArrayDeque<String> stack = new ArrayDeque<String>();
        for (FileNaming fileNaming = this.getFileName(); fileNaming != null; fileNaming = fileNaming.getParent()) {
            stack.addFirst(fileNaming.getName());
        }
        String rootName = (String)stack.removeFirst();
        if (Utilities.isWindows() && (rootName = rootName.replace(File.separatorChar, '/')).startsWith("//")) {
            rootName = rootName + "/";
        }
        StringBuilder path = new StringBuilder();
        path.append(rootName);
        while (!stack.isEmpty()) {
            path.append((String)stack.removeFirst());
            if (stack.isEmpty()) continue;
            path.append('/');
        }
        return path.toString();
    }

    public final FileSystem getFileSystem() throws FileStateInvalidException {
        return FileBasedFileSystem.getInstance();
    }

    public final boolean isRoot() {
        return false;
    }

    public final Date lastModified() {
        File f = this.getFileName().getFile();
        long lastModified = f.lastModified();
        return new Date(lastModified);
    }

    public FileObject copy(FileObject target, String name, String ext) throws IOException {
        File to;
        ProvidedExtensions extensions;
        FileObject result;
        block13 : {
            if (FileUtil.isParentOf((FileObject)this, (FileObject)target)) {
                FSException.io("EXC_CopyChild", new Object[]{this, target});
            }
            extensions = this.getProvidedExtensions();
            to = this.getToFile(target, name, ext);
            extensions.beforeCopy(target, to);
            result = null;
            Long last = MOVED_FILE_TIMESTAMP.get();
            try {
                ProvidedExtensions.IOHandler copyHandler;
                if (last != null) {
                    MOVED_FILE_TIMESTAMP.set(this.lastModified().getTime());
                }
                if ((copyHandler = extensions.getCopyHandler(this.getFileName().getFile(), to)) != null) {
                    if (target instanceof FolderObj) {
                        result = this.handleMoveCopy((FolderObj)target, name, ext, copyHandler);
                    } else {
                        copyHandler.handle();
                        this.refresh(true);
                        target.refresh(true);
                        result = target.getFileObject(name, ext);
                        assert (result != null);
                    }
                    FileUtil.copyAttributes((FileObject)this, (FileObject)result);
                    break block13;
                }
                if (this.isFolder() && ext != null && !ext.isEmpty()) {
                    name = name + '.' + ext;
                }
                result = super.copy(target, name, ext);
            }
            catch (IOException ioe) {
                extensions.copyFailure(this, to);
                throw ioe;
            }
            finally {
                if (last != null) {
                    MOVED_FILE_TIMESTAMP.set(last);
                }
            }
        }
        extensions.copySuccess(this, to);
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final FileObject move(FileLock lock, FileObject target, String name, String ext) throws IOException {
        if (FileUtil.isParentOf((FileObject)this, (FileObject)target)) {
            FSException.io("EXC_MoveChild", new Object[]{this, target});
        }
        ProvidedExtensions extensions = this.getProvidedExtensions();
        File to = this.getToFile(target, name, ext);
        extensions.beforeMove(this, to);
        BaseFileObj result = null;
        try {
            if (!this.checkLock(lock)) {
                FSException.io("EXC_InvalidLock", new Object[]{lock, this.getPath()});
            }
            Watcher.lock(target);
            Watcher.lock(this.getParent());
            ProvidedExtensions.IOHandler moveHandler = extensions.getMoveHandler(this.getFileName().getFile(), to);
            if (moveHandler != null) {
                if (target instanceof FolderObj) {
                    result = this.move(lock, (FolderObj)target, name, ext, moveHandler);
                } else {
                    moveHandler.handle();
                    this.refresh(true);
                    target.refresh(true);
                    result = target.getFileObject(name, ext);
                    assert (result != null);
                }
            } else {
                MOVED_FILE_TIMESTAMP.set(this.lastModified().getTime());
                try {
                    result = super.move(lock, target, name, ext);
                }
                finally {
                    MOVED_FILE_TIMESTAMP.remove();
                }
            }
            FileUtil.copyAttributes((FileObject)this, (FileObject)result);
            Utils.reassignLkp(this, result);
        }
        catch (IOException ioe) {
            extensions.moveFailure(this, to);
            throw ioe;
        }
        extensions.moveSuccess(this, to);
        return result;
    }

    public BaseFileObj move(FileLock lock, FolderObj target, String name, String ext, ProvidedExtensions.IOHandler moveHandler) throws IOException {
        return this.handleMoveCopy(target, name, ext, moveHandler);
    }

    private File getToFile(FileObject target, String name, String ext) {
        if (target instanceof FolderObj) {
            File tf = ((BaseFileObj)target).getFileName().getFile();
            return new File(tf, FileInfo.composeName(name, ext));
        }
        File tf = FileUtil.toFile((FileObject)target);
        return tf == null ? null : new File(tf, FileInfo.composeName(name, ext));
    }

    static void dumpFileInfo(File f, Throwable ex) {
        for (File p = f.getParentFile(); p != null; p = p.getParentFile()) {
            if (p.exists()) {
                Exceptions.attachMessage((Throwable)ex, (String)("\nParent exists: " + p));
                Exceptions.attachMessage((Throwable)ex, (String)("\nHas children " + Arrays.toString(p.list())));
                break;
            }
            Exceptions.attachMessage((Throwable)ex, (String)("\nParent does not exist " + p));
        }
    }

    private BaseFileObj handleMoveCopy(FolderObj target, String name, String ext, ProvidedExtensions.IOHandler handler) throws IOException {
        handler.handle();
        String nameExt = FileInfo.composeName(name, ext);
        target.getChildrenCache().getChild(nameExt, true);
        BaseFileObj result = null;
        File file = new File(target.getFileName().getFile(), nameExt);
        for (int i = 0; i < 10; ++i) {
            result = (BaseFileObj)FileBasedFileSystem.getFileObject(file);
            if (result != null) {
                if (result.isData()) {
                    result.fireFileDataCreatedEvent(false);
                    break;
                }
                result.fireFileFolderCreatedEvent(false);
                break;
            }
            try {
                Thread.sleep(100);
                continue;
            }
            catch (InterruptedException ex) {
                // empty catch block
            }
        }
        boolean assertsOn = false;
        if (!$assertionsDisabled) {
            assertsOn = true;
            if (!true) {
                throw new AssertionError();
            }
        }
        if (result == null && assertsOn) {
            AssertionError ae = new AssertionError((Object)("FileObject for " + file + " not found."));
            BaseFileObj.dumpFileInfo(file, (Throwable)((Object)ae));
            throw ae;
        }
        FolderObj parent = this.getExistingParent();
        if (parent != null) {
            parent.refresh(true);
        } else {
            this.refresh(true);
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void rename(FileLock lock, String name, String ext, ProvidedExtensions.IOHandler handler) throws IOException {
        File file2Rename;
        File file;
        String originalName;
        String originalExt;
        Object parentPath;
        boolean targetFileExists;
        if (!this.checkLock(lock)) {
            FSException.io("EXC_InvalidLock", new Object[]{lock, this.getPath()});
        }
        file = this.getFileName().getFile();
        File parent = file.getParentFile();
        String newNameExt = FileInfo.composeName(name, ext);
        if (newNameExt.equals(this.getNameExt())) {
            return;
        }
        file2Rename = new File(parent, newNameExt);
        if (parent == null || !FileChangedManager.getInstance().exists(parent) || newNameExt.contains("/") || newNameExt.contains("\\")) {
            FolderObj parentFo = this.getExistingParent();
            String parentPath2 = parentFo != null ? parentFo.getPath() : file.getParentFile().getAbsolutePath();
            FSException.io("EXC_CannotRename", file.getName(), parentPath2, newNameExt);
        }
        originalName = this.getName();
        originalExt = this.getExt();
        if (Utils.equals(file2Rename, file)) {
            boolean success;
            if (handler != null) {
                handler.handle();
                success = true;
            } else {
                success = file.renameTo(file2Rename);
            }
            if (!success) {
                FolderObj parentFo = this.getExistingParent();
                String parentPath3 = parentFo != null ? parentFo.getPath() : file.getParentFile().getAbsolutePath();
                FSException.io("EXC_CannotRename", file.getName(), parentPath3, newNameExt);
            }
            NamingFactory.checkCaseSensitivity(this.fileName, file2Rename);
            this.fireFileRenamedEvent(originalName, originalExt);
            return;
        }
        boolean bl = targetFileExists = FileChangedManager.getInstance().exists(file2Rename) && !Utils.equals(file2Rename, file);
        if (targetFileExists) {
            FolderObj parentFo = this.getExistingParent();
            parentPath = parentFo != null ? parentFo.getPath() : file.getParentFile().getAbsolutePath();
            FSException.io("EXC_CannotRename", file.getName(), parentPath, newNameExt);
        }
        FileObjectFactory fs = this.getFactory();
        fs.allIBaseLock.writeLock().lock();
        try {
            parentPath = FileObjectFactory.AllFactories;
            synchronized (parentPath) {
                FileNaming oldFileName = this.getFileName();
                assert (oldFileName != null);
                FileNaming[] allRenamed = NamingFactory.rename(oldFileName, newNameExt, handler);
                if (allRenamed == null) {
                    FolderObj parentFo = this.getExistingParent();
                    String parentPath4 = parentFo != null ? parentFo.getPath() : file.getParentFile().getAbsolutePath();
                    FSException.io("EXC_CannotRename", file.getName(), parentPath4, newNameExt);
                }
                assert (allRenamed[0] != null);
                this.fileName = allRenamed[0];
                HashSet<BaseFileObj> toRename = new HashSet<BaseFileObj>(allRenamed.length * 2);
                toRename.add(this);
                attribs.renameAttributes(file.getAbsolutePath().replace('\\', '/'), file2Rename.getAbsolutePath().replace('\\', '/'));
                for (int i = 0; i < allRenamed.length; ++i) {
                    Mutex.Privileged mutexPrivileged;
                    BaseFileObj tmpPar;
                    File affected = allRenamed[i].getFile();
                    BaseFileObj obj = fs.getCachedOnly(affected, false);
                    if (obj != null && i >= 1) {
                        obj.updateFileName(allRenamed[i], oldFileName, allRenamed[0]);
                        toRename.add(obj);
                    }
                    BaseFileObj baseFileObj = tmpPar = allRenamed[i].getParent() != null ? fs.getCachedOnly(affected.getParentFile(), false) : null;
                    if (!(tmpPar instanceof FolderObj)) continue;
                    FolderObj par = (FolderObj)tmpPar;
                    ChildrenCache childrenCache = par.getChildrenCache();
                    Mutex.Privileged privileged = mutexPrivileged = childrenCache != null ? childrenCache.getMutexPrivileged() : null;
                    if (mutexPrivileged != null) {
                        mutexPrivileged.enterWriteAccess();
                    }
                    try {
                        if (i >= 1) {
                            childrenCache.removeChild(allRenamed[i]);
                        }
                        childrenCache.getChild(allRenamed[i].getName(), true);
                    }
                    finally {
                        if (mutexPrivileged != null) {
                            mutexPrivileged.exitWriteAccess();
                        }
                    }
                }
                fs.rename(toRename);
            }
        }
        finally {
            fs.allIBaseLock.writeLock().unlock();
        }
        LockForFile.relock(file, file2Rename);
        this.afterRename();
        this.fireFileRenamedEvent(originalName, originalExt);
    }

    protected void afterRename() {
    }

    public final void rename(final FileLock lock, final String name, final String ext) throws IOException {
        FileBasedFileSystem.FSCallable<Boolean> c = new FileBasedFileSystem.FSCallable<Boolean>(){

            @Override
            public Boolean call() throws IOException {
                ProvidedExtensions extensions = BaseFileObj.this.getProvidedExtensions();
                BaseFileObj.this.rename(lock, name, ext, extensions.getRenameHandler(BaseFileObj.this.getFileName().getFile(), FileInfo.composeName(name, ext)));
                return true;
            }
        };
        FileBasedFileSystem.runAsInconsistent(c);
    }

    public Object getAttribute(String attrName) {
        if (attrName.equals("FileSystem.rootPath")) {
            return "";
        }
        if (attrName.equals("java.io.File")) {
            return this.getFileName().getFile();
        }
        if (attrName.equals("ExistsParentNoPublicAPI")) {
            return this.getExistingParent() != null;
        }
        if (attrName.startsWith("ProvidedExtensions")) {
            ProvidedExtensions extension = this.getProvidedExtensions();
            return extension.getAttribute(this.getFileName().getFile(), attrName);
        }
        return attribs.readAttribute(this.getFileName().getFile().getAbsolutePath().replace('\\', '/'), attrName);
    }

    public final void setAttribute(String attrName, Object value) throws IOException {
        Object oldValue = attribs.readAttribute(this.getFileName().getFile().getAbsolutePath().replace('\\', '/'), attrName);
        attribs.writeAttribute(this.getFileName().getFile().getAbsolutePath().replace('\\', '/'), attrName, value);
        this.fireFileAttributeChangedEvent(attrName, oldValue, value);
    }

    public final Enumeration<String> getAttributes() {
        return attribs.attributes(this.getFileName().getFile().getAbsolutePath().replace('\\', '/'));
    }

    public final void addFileChangeListener(FileChangeListener fcl) {
        this.getEventSupport().add(FileChangeListener.class, fcl);
        Watcher.register(this);
    }

    public final void removeFileChangeListener(FileChangeListener fcl) {
        this.getEventSupport().remove(FileChangeListener.class, fcl);
        if (this.noFolderListeners()) {
            Watcher.unregister(this);
        }
    }

    protected abstract boolean noFolderListeners();

    final boolean noListeners() {
        return this.getEventSupport().getListenerCount() == 0;
    }

    public void addRecursiveListener(FileChangeListener fcl) {
        this.addFileChangeListener(fcl);
    }

    public void removeRecursiveListener(FileChangeListener fcl) {
        this.removeFileChangeListener(fcl);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Enumeration<FileChangeListener> getListeners() {
        Object object = EVENT_SUPPORT_LOCK;
        synchronized (object) {
            if (this.eventSupport == null) {
                return Enumerations.empty();
            }
            return Enumerations.array((Object[])this.eventSupport.getListeners(FileChangeListener.class));
        }
    }

    public final long getSize() {
        return this.getFileName().getFile().length();
    }

    public final void setImportant(boolean b) {
    }

    public boolean isReadOnly() {
        File f = this.getFileName().getFile();
        ProvidedExtensions extension = this.getProvidedExtensions();
        return !extension.canWrite(f) && FileChangedManager.getInstance().exists(f);
    }

    public final FileObject getParent() {
        FileObject retVal = null;
        if (!this.isRoot()) {
            FileNaming parent = this.getFileName().getParent();
            if (Utilities.isWindows()) {
                File file;
                FileObjectFactory factory;
                retVal = parent == null ? FileBasedFileSystem.getInstance().getRoot() : ((retVal = (factory = this.getFactory()).getCachedOnly(file = parent.getFile())) == null ? factory.getFileObject(new FileInfo(file), FileObjectFactory.Caller.GetParent) : retVal);
            } else if (parent != null) {
                FileObjectFactory factory = this.getFactory();
                File file = parent.getFile();
                if (file.getParentFile() == null) {
                    retVal = FileBasedFileSystem.getInstance().getRoot();
                } else {
                    retVal = factory.getCachedOnly(file);
                    FileObject fileObject = retVal = retVal == null ? factory.getFileObject(new FileInfo(file), FileObjectFactory.Caller.GetParent) : retVal;
                }
            }
            assert (retVal != null);
        }
        return retVal;
    }

    static File getFile(File f, String name, String ext) {
        StringBuffer sb = new StringBuffer();
        sb.append(name);
        if (ext != null && ext.length() > 0) {
            sb.append('.');
            sb.append(ext);
        }
        File retVal = new File(f, sb.toString());
        return retVal;
    }

    public final FileObjectFactory getFactory() {
        return FileObjectFactory.getInstance(this.getFileName().getFile());
    }

    final void fireFileDataCreatedEvent(boolean expected) {
        Statistics.StopWatch stopWatch = Statistics.getStopWatch(Statistics.LISTENERS_CALLS);
        stopWatch.start();
        FolderObj parent = this.getExistingParent();
        Enumeration<FileChangeListener> pListeners = parent != null ? BaseFileObj.super.getListeners() : null;
        FileEventImpl parentFe = null;
        if (parent != null && pListeners != null) {
            parentFe = new FileEventImpl(parent, this, expected, 0);
        }
        if (parentFe != null) {
            FileEventImpl fe = new FileEventImpl(this, parentFe);
            this.fireFileDataCreatedEvent(this.getListeners(), (FileEvent)fe);
            parent.fireFileDataCreatedEvent(pListeners, (FileEvent)parentFe);
        } else {
            FileEventImpl fe = new FileEventImpl(this, this, expected, 0);
            this.fireFileDataCreatedEvent(this.getListeners(), (FileEvent)fe);
        }
        stopWatch.stop();
        LOG.log(Level.FINER, "fireFileDataCreatedEvent {0}", (Object)((Object)this));
    }

    final void fireFileFolderCreatedEvent(boolean expected) {
        Statistics.StopWatch stopWatch = Statistics.getStopWatch(Statistics.LISTENERS_CALLS);
        stopWatch.start();
        FolderObj parent = this.getExistingParent();
        Enumeration<FileChangeListener> pListeners = parent != null ? BaseFileObj.super.getListeners() : null;
        FileEventImpl parentFe = null;
        if (parent != null && pListeners != null) {
            parentFe = new FileEventImpl(parent, this, expected, 0);
        }
        if (parentFe != null) {
            FileEventImpl fe = new FileEventImpl(this, parentFe);
            this.fireFileFolderCreatedEvent(this.getListeners(), (FileEvent)fe);
            parent.fireFileFolderCreatedEvent(pListeners, (FileEvent)parentFe);
        } else {
            FileEventImpl fe = new FileEventImpl(this, this, expected, 0);
            this.fireFileFolderCreatedEvent(this.getListeners(), (FileEvent)fe);
        }
        stopWatch.stop();
        LOG.log(Level.FINER, "fireFileFolderCreatedEvent {0}", (Object)((Object)this));
    }

    public final void fireFileChangedEvent(boolean expected) {
        this.getProvidedExtensions().fileChanged(this);
        Statistics.StopWatch stopWatch = Statistics.getStopWatch(Statistics.LISTENERS_CALLS);
        stopWatch.start();
        FolderObj p = this.getExistingParent();
        BaseFileObj parent = p instanceof BaseFileObj ? p : null;
        Enumeration<FileChangeListener> pListeners = parent != null ? parent.getListeners() : null;
        FileEventImpl parentFe = null;
        if (parent != null && pListeners != null) {
            parentFe = new FileEventImpl(parent, this, expected, this.lastModified().getTime());
        }
        if (parentFe != null) {
            FileEventImpl fe = new FileEventImpl(this, parentFe);
            this.fireFileChangedEvent(this.getListeners(), (FileEvent)fe);
            parent.fireFileChangedEvent(pListeners, (FileEvent)parentFe);
        } else {
            FileEventImpl fe = new FileEventImpl(this, this, expected, this.lastModified().getTime());
            this.fireFileChangedEvent(this.getListeners(), (FileEvent)fe);
        }
        stopWatch.stop();
        LOG.log(Level.FINER, "fireFileChangedEvent {0}", (Object)((Object)this));
    }

    final void fireFileDeletedEvent(boolean expected) {
        Statistics.StopWatch stopWatch = Statistics.getStopWatch(Statistics.LISTENERS_CALLS);
        stopWatch.start();
        FolderObj p = this.getExistingParent();
        BaseFileObj parent = p instanceof BaseFileObj ? p : null;
        Enumeration<FileChangeListener> pListeners = parent != null ? parent.getListeners() : null;
        FileEventImpl parentFe = null;
        if (parent != null && pListeners != null) {
            parentFe = new FileEventImpl(parent, this, expected, 0);
        }
        if (parentFe != null) {
            FileEventImpl fe = new FileEventImpl(this, parentFe);
            this.fireFileDeletedEvent(this.getListeners(), (FileEvent)fe);
            parent.fireFileDeletedEvent(pListeners, (FileEvent)parentFe);
        } else {
            FileEventImpl fe = new FileEventImpl(this, this, expected, 0);
            this.fireFileDeletedEvent(this.getListeners(), (FileEvent)fe);
        }
        stopWatch.stop();
        LOG.log(Level.FINER, "fireFileDeletedEvent {0}", (Object)((Object)this));
    }

    private void fireFileRenamedEvent(String originalName, String originalExt) {
        Statistics.StopWatch stopWatch = Statistics.getStopWatch(Statistics.LISTENERS_CALLS);
        stopWatch.start();
        FolderObj parent = this.getExistingParent();
        Enumeration<FileChangeListener> pListeners = parent != null ? BaseFileObj.super.getListeners() : null;
        this.fireFileRenamedEvent(this.getListeners(), new FileRenameEvent((FileObject)this, originalName, originalExt));
        if (parent != null && pListeners != null) {
            parent.fireFileRenamedEvent(pListeners, new FileRenameEvent((FileObject)parent, (FileObject)this, originalName, originalExt));
        }
        stopWatch.stop();
        LOG.log(Level.FINER, "fireFileRenamedEvent {0} oldName {1} ext {2}", new Object[]{this, originalName, originalExt});
    }

    final void fireFileAttributeChangedEvent(String attrName, Object oldValue, Object newValue) {
        FolderObj parent = this.getExistingParent();
        Enumeration<FileChangeListener> pListeners = parent != null ? BaseFileObj.super.getListeners() : null;
        this.fireFileAttributeChangedEvent(this.getListeners(), new FileAttributeEvent((FileObject)this, (FileObject)this, attrName, oldValue, newValue));
        if (parent != null && pListeners != null) {
            parent.fireFileAttributeChangedEvent(pListeners, new FileAttributeEvent((FileObject)parent, (FileObject)this, attrName, oldValue, newValue));
        }
        LOG.log(Level.FINER, "fireFileAttributeChangedEvent {0} attribute {1}", new Object[]{this, attrName});
    }

    public final FileNaming getFileName() {
        return this.fileName;
    }

    public final void delete(final FileLock lock) throws IOException {
        FileBasedFileSystem.FSCallable<Boolean> c = new FileBasedFileSystem.FSCallable<Boolean>(){

            @Override
            public Boolean call() throws IOException {
                ProvidedExtensions pe = BaseFileObj.this.getProvidedExtensions();
                pe.beforeDelete(BaseFileObj.this);
                try {
                    BaseFileObj.this.delete(lock, pe.getDeleteHandler(BaseFileObj.this.getFileName().getFile()));
                }
                catch (IOException iex) {
                    BaseFileObj.this.getProvidedExtensions().deleteFailure(BaseFileObj.this);
                    throw iex;
                }
                BaseFileObj.this.getProvidedExtensions().deleteSuccess(BaseFileObj.this);
                return true;
            }
        };
        FileBasedFileSystem.runAsInconsistent(c);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void delete(FileLock lock, ProvidedExtensions.DeleteHandler deleteHandler) throws IOException {
        block8 : {
            Mutex.Privileged mutexPrivileged;
            File f = this.getFileName().getFile();
            FolderObj existingParent = this.getExistingParent();
            ChildrenCache childrenCache = existingParent != null ? existingParent.getChildrenCache() : null;
            Mutex.Privileged privileged = mutexPrivileged = childrenCache != null ? childrenCache.getMutexPrivileged() : null;
            if (mutexPrivileged != null) {
                mutexPrivileged.enterWriteAccess();
            }
            try {
                boolean deleteStatus;
                if (!this.checkLock(lock)) {
                    FSException.io("EXC_InvalidLock", new Object[]{lock, this.getPath()});
                }
                boolean bl = deleteStatus = deleteHandler != null ? deleteHandler.delete(f) : f.delete();
                if (!deleteStatus) {
                    FolderObj parent = this.getExistingParent();
                    String parentPath = parent != null ? parent.getPath() : f.getParentFile().getAbsolutePath();
                    FSException.io("EXC_CannotDelete", f.getName(), parentPath);
                }
                attribs.deleteAttributes(f.getAbsolutePath().replace('\\', '/'));
                if (childrenCache == null) break block8;
                if (deleteHandler != null) {
                    childrenCache.removeChild(this.getFileName());
                    break block8;
                }
                childrenCache.getChild(BaseFileObj.getNameExt(f), true);
            }
            finally {
                if (mutexPrivileged != null) {
                    mutexPrivileged.exitWriteAccess();
                }
            }
        }
        this.setValid(false);
        this.fireFileDeletedEvent(false);
    }

    abstract boolean checkLock(FileLock var1) throws IOException;

    public Object writeReplace() {
        return new ReplaceForSerialization(this.getFileName().getFile());
    }

    protected abstract void setValid(boolean var1);

    abstract void refreshImpl(boolean var1, boolean var2);

    public boolean isValid() {
        return NamingFactory.isValid(this.getFileName());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final void refresh(boolean expected, boolean fire) {
        Statistics.StopWatch stopWatch = Statistics.getStopWatch(Statistics.REFRESH_FILE);
        stopWatch.start();
        try {
            if (this.isValid()) {
                this.refreshImpl(expected, fire);
                if (this.isValid()) {
                    boolean isFile;
                    File file = this.getFileName().getFile();
                    boolean isDir = file.isDirectory();
                    if (isDir == (isFile = file.isFile()) || this.isFolder() != isDir || this.isData() != isFile) {
                        this.invalidateFO(fire, expected, true);
                    }
                } else if (this.isData()) {
                    this.refreshExistingParent(expected, fire);
                }
            }
        }
        finally {
            stopWatch.stop();
        }
    }

    void refreshExistingParent(boolean expected, boolean fire) {
        boolean validityFlag = FileChangedManager.getInstance().exists(this.getFileName().getFile());
        if (!validityFlag) {
            this.invalidateFO(fire, expected, true);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void invalidateFO(boolean fire, boolean expected, boolean createNewFN) {
        FolderObj parent = this.getExistingParent();
        if (parent != null) {
            Mutex.Privileged mutexPrivileged;
            ChildrenCache childrenCache = parent.getChildrenCache();
            Mutex.Privileged privileged = mutexPrivileged = childrenCache != null ? childrenCache.getMutexPrivileged() : null;
            if (mutexPrivileged != null) {
                mutexPrivileged.enterWriteAccess();
            }
            try {
                childrenCache.getChild(this.getFileName().getFile().getName(), true);
            }
            finally {
                if (mutexPrivileged != null) {
                    mutexPrivileged.exitWriteAccess();
                }
            }
        }
        this.setValid(false);
        if (createNewFN) {
            NamingFactory.fromFile(this.getFileName().getParent(), this.getFileName().getFile(), true);
        }
        if (fire) {
            this.notifyDeleted(expected);
        }
    }

    final void notifyDeleted(boolean expected) {
        this.getProvidedExtensions().deletedExternally(this);
        this.fireFileDeletedEvent(expected);
    }

    private void updateFileName(FileNaming oldName, FileNaming oldRoot, FileNaming newRoot) {
        Stack<String> names = new Stack<String>();
        while (oldRoot != oldName && oldName != null) {
            names.add(oldName.getName());
            oldName = oldName.getParent();
        }
        File prev = newRoot.getFile();
        while (!names.isEmpty()) {
            String n = (String)names.pop();
            prev = new File(prev, n);
            newRoot = NamingFactory.fromFile(newRoot, prev, true);
        }
        assert (newRoot != null);
        this.fileName = newRoot;
        this.afterRename();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private EventListenerList getEventSupport() {
        Object object = EVENT_SUPPORT_LOCK;
        synchronized (object) {
            if (this.eventSupport == null) {
                this.eventSupport = new EventListenerList();
            }
            return this.eventSupport;
        }
    }

    final ProvidedExtensions getProvidedExtensions() {
        FileBasedFileSystem.StatusImpl status = (FileBasedFileSystem.StatusImpl)FileBasedFileSystem.getInstance().getStatus();
        ProvidedExtensions extensions = status.getExtensions();
        return extensions;
    }

    public static FolderObj getExistingFor(File f, FileObjectFactory fbs) {
        if (fbs == null) {
            throw new NullPointerException("No factory for " + f);
        }
        BaseFileObj retval = fbs.getCachedOnly(f);
        return (FolderObj)(retval instanceof FolderObj ? retval : null);
    }

    public static FolderObj getExistingParentFor(File f, FileObjectFactory fbs) {
        File parentFile = f.getParentFile();
        return parentFile == null ? null : BaseFileObj.getExistingFor(parentFile, fbs);
    }

    FolderObj getExistingParent() {
        return BaseFileObj.getExistingParentFor(this.getFileName().getFile(), this.getFactory());
    }

    static {
        BridgeForAttributes attrBridge = new BridgeForAttributes();
        attribs = new Attributes(attrBridge, attrBridge, attrBridge);
        EVENT_SUPPORT_LOCK = new Object();
    }

    private static final class Delivered
    implements FileChangeListener {
        private Delivered() {
        }

        private void unlock(FileEvent fe) {
            Watcher.unlock((FileObject)fe.getSource());
        }

        public void fileFolderCreated(FileEvent fe) {
            this.unlock(fe);
        }

        public void fileDataCreated(FileEvent fe) {
            this.unlock(fe);
        }

        public void fileChanged(FileEvent fe) {
            this.unlock(fe);
        }

        public void fileDeleted(FileEvent fe) {
            this.unlock(fe);
        }

        public void fileRenamed(FileRenameEvent fe) {
            this.unlock((FileEvent)fe);
        }

        public void fileAttributeChanged(FileAttributeEvent fe) {
            this.unlock((FileEvent)fe);
        }
    }

    private static class FileEventImpl
    extends FileEvent
    implements Enumeration<FileEvent> {
        private FileEventImpl next;

        @Override
        public boolean hasMoreElements() {
            return this.next != null;
        }

        @Override
        public FileEvent nextElement() {
            if (this.next == null) {
                throw new NoSuchElementException();
            }
            return this.next;
        }

        public FileEventImpl(FileObject src, FileObject file, boolean expected, long time) {
            super(src, file, expected, time);
        }

        public FileEventImpl(FileObject src, FileEventImpl next) {
            super(src, next.getFile(), next.isExpected(), next.getTime());
            this.next = next;
        }

        static {
            FileBasedFileSystem.getInstance().addFileChangeListener((FileChangeListener)new Delivered());
        }
    }

    private static final class BridgeForAttributes
    implements AbstractFileSystem.List,
    AbstractFileSystem.Change,
    AbstractFileSystem.Info {
        private BridgeForAttributes() {
        }

        public final Date lastModified(String name) {
            File file = new File(name);
            return new Date(file.lastModified());
        }

        public final boolean folder(String name) {
            File file = new File(name);
            return file.isDirectory();
        }

        public final boolean readOnly(String name) {
            File file = new File(name);
            return !file.canWrite();
        }

        public final String mimeType(String name) {
            return "content/unknown";
        }

        public final long size(String name) {
            File file = new File(name);
            return file.length();
        }

        public final InputStream inputStream(String name) throws FileNotFoundException {
            File file = new File(name);
            return new FileInputStream(file);
        }

        public final OutputStream outputStream(String name) throws IOException {
            Path path = Paths.get(name, new String[0]);
            return Files.newOutputStream(path, new OpenOption[0]);
        }

        public final void lock(String name) throws IOException {
        }

        public final void unlock(String name) {
        }

        public final void markUnimportant(String name) {
        }

        public final String[] children(String f) {
            File file = new File(f);
            return file.list();
        }

        public final void createFolder(String name) throws IOException {
            File file = new File(name);
            if (!file.mkdirs()) {
                IOException ioException = new IOException(name);
                throw ioException;
            }
        }

        public final void createData(String name) throws IOException {
            File file = new File(name);
            if (!file.createNewFile()) {
                throw new IOException(name);
            }
        }

        public final void rename(String oldName, String newName) throws IOException {
            File file = new File(oldName);
            File dest = new File(newName);
            if (!file.renameTo(dest)) {
                FSException.io("EXC_CannotRename", file.getName(), "", dest.getName());
            }
        }

        public final void delete(String name) throws IOException {
            boolean isDeleted;
            File file = new File(name);
            boolean bl = isDeleted = file.isFile() ? file.delete() : this.deleteFolder(file);
            if (isDeleted) {
                FSException.io("EXC_CannotDelete", file.getName(), "");
            }
        }

        private boolean deleteFolder(File file) throws IOException {
            File[] arr;
            boolean ret = file.delete();
            if (ret) {
                return true;
            }
            if (!FileChangedManager.getInstance().exists(file)) {
                return false;
            }
            if (file.isDirectory() && (arr = file.listFiles()) != null) {
                for (int i = 0; i < arr.length; ++i) {
                    File f2Delete = arr[i];
                    if (this.deleteFolder(f2Delete)) continue;
                    return false;
                }
            }
            return file.delete();
        }
    }

}

