/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.masterfs.filebasedfs.utils;

import java.io.File;
import java.security.Permission;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.masterfs.filebasedfs.children.ChildrenSupport;
import org.netbeans.modules.masterfs.filebasedfs.naming.NamingFactory;
import org.openide.util.Lookup;

public class FileChangedManager
extends SecurityManager {
    private static final Logger LOG = Logger.getLogger(FileChangedManager.class.getName());
    private static final boolean isFine = LOG.isLoggable(Level.FINE);
    private static final boolean isFiner = LOG.isLoggable(Level.FINER);
    private static FileChangedManager INSTANCE;
    private static final int CREATE_HINT = 2;
    private static final int DELETE_HINT = 1;
    private static final int AMBIGOUS_HINT = 3;
    private final ConcurrentHashMap<Integer, Integer> hints = new ConcurrentHashMap();
    private long shrinkTime = System.currentTimeMillis();
    private static volatile long ioTime;
    private static volatile int ioLoad;
    private static final AtomicInteger priorityIO;
    private static final ThreadLocal<Integer> IDLE_IO;
    private static final ThreadLocal<Runnable> IDLE_CALL;
    private static final ThreadLocal<AtomicBoolean> IDLE_ON;

    public FileChangedManager() {
        if (isFine) {
            LOG.fine("Initializing FileChangedManager");
        }
    }

    public static synchronized FileChangedManager getInstance() {
        if (INSTANCE == null && (FileChangedManager.INSTANCE = (FileChangedManager)Lookup.getDefault().lookup(FileChangedManager.class)) == null) {
            INSTANCE = new FileChangedManager();
        }
        return INSTANCE;
    }

    static void assertNoLock() {
        assert (!Thread.holdsLock(IDLE_CALL));
        assert (!Thread.holdsLock(IDLE_IO));
        assert (!Thread.holdsLock(IDLE_ON));
    }

    @Override
    public void checkPermission(Permission perm) {
    }

    @Override
    public void checkDelete(String file) {
        this.put(file, false);
    }

    @Override
    public void checkWrite(String file) {
        this.put(file, true);
    }

    @Override
    public void checkRead(String file) {
        FileChangedManager.pingIO(1);
    }

    @Override
    public void checkRead(String file, Object context) {
        FileChangedManager.pingIO(1);
    }

    public boolean impeachExistence(File f, boolean expectedExixts) {
        boolean retval;
        Integer hint = this.remove(FileChangedManager.getKey(f));
        boolean bl = retval = hint != null;
        if (retval) {
            if (hint == 3) {
                return true;
            }
            retval = expectedExixts != this.toState(hint);
        }
        return retval;
    }

    public boolean exists(File file) {
        long time = 0;
        assert ((time = System.currentTimeMillis()) >= Long.MIN_VALUE);
        boolean retval = file.exists();
        if (time > 0 && (time = System.currentTimeMillis() - time) > 500) {
            Level l;
            String msg;
            if (FileChangedManager.isIdleIO()) {
                l = Level.FINE;
                msg = "{0} new File(\"{1}\").exists() in I/O mode";
            } else {
                l = Level.WARNING;
                msg = "{0} ms in new File(\"{1}\").exists()";
            }
            LOG.log(l, msg, new Object[]{time, file});
        }
        Integer id = FileChangedManager.getKey(file);
        this.remove(id);
        this.put(id, retval);
        return retval;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static <T> T priorityIO(Callable<T> callable) throws Exception {
        try {
            priorityIO.incrementAndGet();
            T t = callable.call();
            return t;
        }
        finally {
            priorityIO.decrementAndGet();
        }
    }

    static boolean isIdleIO() {
        return IDLE_IO.get() != null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void idleIO(int maximumLoad, Runnable r, Runnable goingToSleep, AtomicBoolean goOn) {
        Integer prev = IDLE_IO.get();
        Runnable pGoing = IDLE_CALL.get();
        AtomicBoolean pGoOn = IDLE_ON.get();
        int prevMax = prev == null ? 0 : prev;
        try {
            IDLE_IO.set(Math.max(maximumLoad, prevMax));
            IDLE_CALL.set(goingToSleep);
            IDLE_ON.set(goOn);
            r.run();
        }
        finally {
            IDLE_IO.set(prev);
            IDLE_CALL.set(pGoing);
            IDLE_ON.set(pGoOn);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    public static void waitIOLoadLowerThan(int load) throws InterruptedException {
        do {
            AtomicBoolean goOn;
            if ((goOn = IDLE_ON.get()) != null && !goOn.get()) {
                String msg = "Interrupting manually";
                if (isFine) {
                    LOG.fine("Interrupting manually");
                }
                throw new InterruptedException("Interrupting manually");
            }
            int l = FileChangedManager.pingIO(0);
            if (l < load && priorityIO.get() == 0) {
                return;
            }
            if (ChildrenSupport.isLock() || Thread.holdsLock(NamingFactory.class)) {
                return;
            }
            if (FileChangedManager.isClassLoading()) {
                return;
            }
            Runnable goingToSleep = IDLE_CALL.get();
            if (goingToSleep != null) {
                goingToSleep.run();
            }
            ThreadLocal<Integer> threadLocal = IDLE_IO;
            synchronized (threadLocal) {
                IDLE_IO.wait(100);
            }
        } while (true);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static int pingIO(int inc) {
        long ms = System.currentTimeMillis();
        boolean change = false;
        while (ioTime < ms) {
            ioTime += 100;
            change = true;
            if ((ioLoad /= 2) != 0) continue;
            ioTime = ms + 100;
            break;
        }
        if (change) {
            ThreadLocal<Integer> threadLocal = IDLE_IO;
            synchronized (threadLocal) {
                IDLE_IO.notifyAll();
            }
        }
        if (inc == 0) {
            return ioLoad;
        }
        Integer maxLoad = IDLE_IO.get();
        if (maxLoad != null) {
            try {
                FileChangedManager.waitIOLoadLowerThan(maxLoad);
            }
            catch (InterruptedException ex) {
                if (isFine) {
                    LOG.log(Level.FINE, "Interrupted {0}", ex.getMessage());
                }
            }
        } else {
            ioLoad += inc;
            if (isFiner) {
                LOG.log(Level.FINER, "I/O load: {0} (+{1})", new Object[]{ioLoad, inc});
            }
        }
        return ioLoad;
    }

    private Integer put(int id, boolean state) {
        FileChangedManager.pingIO(2);
        if (this.hints.size() > 150000) {
            this.shrink();
        }
        this.shrinkTime = System.currentTimeMillis();
        int val = this.toValue(state);
        Integer retval = this.hints.putIfAbsent(id, val);
        if (retval != null && retval != 3 && retval != val) {
            this.hints.put(id, 3);
        }
        return retval;
    }

    private int toValue(boolean state) {
        return state ? 2 : 1;
    }

    private boolean toState(int value) {
        switch (value) {
            case 1: {
                return false;
            }
            case 2: {
                return true;
            }
        }
        return false;
    }

    private void shrink() {
        this.hints.keySet().clear();
    }

    private Integer remove(int id) {
        long now = System.currentTimeMillis();
        if (now - this.shrinkTime > 5000) {
            int size = this.hints.size();
            if (size > 1500) {
                this.shrink();
            }
            this.shrinkTime = now;
        }
        return this.hints.remove(id);
    }

    private static int getKey(File f) {
        return NamingFactory.createID(f);
    }

    private static int getKey(String f) {
        return FileChangedManager.getKey(new File(f));
    }

    private Integer put(String f, boolean value) {
        return this.put(FileChangedManager.getKey(f), value);
    }

    private static boolean isClassLoading() {
        StackTraceElement[] arr;
        for (StackTraceElement e : arr = new Throwable().getStackTrace()) {
            if (!e.getClassName().startsWith("org.netbeans.JarClassLoader") && !e.getClassName().startsWith("org.netbeans.ProxyClassLoader") && !e.getClassName().equals("java.lang.ClassLoader")) continue;
            return true;
        }
        return false;
    }

    static {
        ioTime = -1;
        priorityIO = new AtomicInteger();
        IDLE_IO = new ThreadLocal();
        IDLE_CALL = new ThreadLocal();
        IDLE_ON = new ThreadLocal();
    }
}

