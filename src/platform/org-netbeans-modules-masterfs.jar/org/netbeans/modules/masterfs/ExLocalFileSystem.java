/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.AbstractFileSystem
 *  org.openide.filesystems.AbstractFileSystem$Attr
 *  org.openide.filesystems.AbstractFileSystem$Change
 *  org.openide.filesystems.AbstractFileSystem$Info
 *  org.openide.filesystems.AbstractFileSystem$List
 *  org.openide.filesystems.DefaultAttributes
 *  org.openide.filesystems.LocalFileSystem
 */
package org.netbeans.modules.masterfs;

import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import org.netbeans.modules.masterfs.filebasedfs.utils.Utils;
import org.netbeans.modules.masterfs.providers.Attributes;
import org.openide.filesystems.AbstractFileSystem;
import org.openide.filesystems.DefaultAttributes;
import org.openide.filesystems.LocalFileSystem;

public class ExLocalFileSystem
extends LocalFileSystem {
    public static ExLocalFileSystem getInstance(File root) throws PropertyVetoException, IOException {
        ExLocalFileSystem retVal = new ExLocalFileSystem();
        retVal.attr = Utils.equals(root, Attributes.getRootForAttributes()) ? new OneFileAttributeAttachedToRoot(retVal.info, retVal.change, retVal.list) : new DefaultAttributes(root, retVal.info, retVal.change, retVal.list);
        retVal.setRootDirectory(root);
        return retVal;
    }

    public DefaultAttributes getAttributes() {
        return (DefaultAttributes)this.attr;
    }

    private static class OneFileAttributeAttachedToRoot
    extends DefaultAttributes {
        public OneFileAttributeAttachedToRoot(AbstractFileSystem.Info info, AbstractFileSystem.Change change, AbstractFileSystem.List list) {
            super(info, change, list, Attributes.ATTRNAME);
        }

        public String[] children(String f) {
            return super.children(f);
        }

        public Object readAttribute(String name, String attrName) {
            return super.readAttribute(this.transformName(name), attrName);
        }

        public void writeAttribute(String name, String attrName, Object value) throws IOException {
            super.writeAttribute(this.transformName(name), attrName, value);
        }

        public synchronized Enumeration attributes(String name) {
            return super.attributes(this.transformName(name));
        }

        public synchronized void renameAttributes(String oldName, String newName) {
            super.renameAttributes(this.transformName(oldName), this.transformName(newName));
        }

        public synchronized void deleteAttributes(String name) {
            super.deleteAttributes(this.transformName(name));
        }

        private String transformName(String name) {
            char replaceChar = '|';
            if (name.indexOf(replaceChar) != -1) {
                StringBuffer transformed = new StringBuffer(name.length() + 50);
                for (int i = 0; i < name.length(); ++i) {
                    transformed.append(name.charAt(i));
                    if (name.charAt(i) != replaceChar) continue;
                    transformed.append(replaceChar);
                }
                name = transformed.toString();
            }
            return name.replace('/', replaceChar);
        }
    }

}

