/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.masterfs.watcher;

import java.io.IOException;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.masterfs.providers.Notifier;
import org.netbeans.modules.masterfs.watcher.NotifierAccessor;
import org.netbeans.modules.masterfs.watcher.Watcher;
import org.openide.filesystems.FileObject;

class NotifierKeyRef<KEY>
extends WeakReference<FileObject> {
    private final KEY key;
    private final int hash;
    private final Notifier<KEY> outer;

    public NotifierKeyRef(FileObject fo, KEY key, ReferenceQueue<FileObject> queue, Notifier<KEY> outer) {
        super(fo, queue);
        this.outer = outer;
        this.key = key;
        this.hash = fo.hashCode();
        if (key != null) {
            Watcher.LOG.log(Level.FINE, "Adding watch for {0}", key);
        }
    }

    @Override
    public FileObject get() {
        return (FileObject)WeakReference.super.get();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        try {
            NotifierKeyRef kr = (NotifierKeyRef)obj;
            FileObject mine = this.get();
            FileObject theirs = kr.get();
            if (mine == null) {
                return theirs == null;
            }
            return mine.equals((Object)theirs);
        }
        catch (ClassCastException ex) {
            return false;
        }
    }

    final void removeWatch() throws IOException {
        Watcher.LOG.log(Level.FINE, "Removing watch for {0}", this.key);
        NotifierAccessor.getDefault().removeWatch(this.outer, this.key);
    }

    public int hashCode() {
        return this.hash;
    }
}

