/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.masterfs.watcher;

import java.io.IOException;
import org.netbeans.modules.masterfs.providers.Notifier;

public abstract class NotifierAccessor {
    private static NotifierAccessor DEFAULT;

    protected NotifierAccessor() {
        DEFAULT = this;
    }

    public static NotifierAccessor getDefault() {
        assert (DEFAULT != null);
        return DEFAULT;
    }

    protected abstract <KEY> KEY addWatch(Notifier<KEY> var1, String var2) throws IOException;

    protected abstract <KEY> void removeWatch(Notifier<KEY> var1, KEY var2) throws IOException;

    protected abstract String nextEvent(Notifier<?> var1) throws IOException, InterruptedException;

    protected abstract void start(Notifier<?> var1) throws IOException;

    protected abstract void stop(Notifier<?> var1) throws IOException;

    static {
        try {
            Class.forName(Notifier.class.getName(), true, Notifier.class.getClassLoader());
        }
        catch (ClassNotFoundException ex) {
            throw new IllegalStateException(ex);
        }
    }
}

