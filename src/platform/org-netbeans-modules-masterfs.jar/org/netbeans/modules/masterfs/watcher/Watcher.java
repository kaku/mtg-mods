/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Result
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.WeakSet
 */
package org.netbeans.modules.masterfs.watcher;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.BaseFileObj;
import org.netbeans.modules.masterfs.filebasedfs.fileobjects.FileObjectFactory;
import org.netbeans.modules.masterfs.providers.AnnotationProvider;
import org.netbeans.modules.masterfs.providers.InterceptionListener;
import org.netbeans.modules.masterfs.providers.Notifier;
import org.netbeans.modules.masterfs.providers.ProvidedExtensions;
import org.netbeans.modules.masterfs.watcher.FOFile;
import org.netbeans.modules.masterfs.watcher.NotifierAccessor;
import org.netbeans.modules.masterfs.watcher.NotifierKeyRef;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakSet;

public final class Watcher
extends AnnotationProvider {
    static final Logger LOG = Logger.getLogger(Watcher.class.getName());
    private static final Map<FileObject, int[]> MODIFIED = new WeakHashMap<FileObject, int[]>();
    private final Ext<?> ext;
    private final Object lock = "org.netbeans.io.suspend".intern();
    private Set<FileObject> pending;
    private static RequestProcessor RP = new RequestProcessor("Pending refresh", 1);
    private RequestProcessor.Task refreshTask;

    public Watcher() {
        this.refreshTask = RP.create(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                Set toRefresh;
                Object object = Watcher.this.lock;
                synchronized (object) {
                    int cnt;
                    toRefresh = Watcher.this.pending;
                    if (toRefresh == null) {
                        return;
                    }
                    while ((cnt = Integer.getInteger("org.netbeans.io.suspend", 0).intValue()) > 0) {
                        String pndngSize = String.valueOf(toRefresh.size());
                        System.setProperty("org.netbeans.io.pending", pndngSize);
                        Watcher.LOG.log(Level.FINE, "Suspend count {0} pending {1}", new Object[]{cnt, pndngSize});
                        try {
                            Watcher.this.lock.wait(1500);
                        }
                        catch (InterruptedException ex) {
                            Watcher.LOG.log(Level.FINE, null, ex);
                        }
                    }
                    System.getProperties().remove("org.netbeans.io.pending");
                    Watcher.this.pending = null;
                }
                Watcher.LOG.log(Level.FINE, "Refreshing {0} directories", toRefresh.size());
                for (FileObject fileObject : toRefresh) {
                    if (Watcher.isLocked(fileObject)) {
                        Watcher.this.enqueue(fileObject);
                        continue;
                    }
                    Watcher.LOG.log(Level.FINEST, "Refreshing {0}", (Object)fileObject);
                    fileObject.refresh();
                }
                Watcher.LOG.fine("Refresh finished");
            }
        });
        if (Boolean.getBoolean("org.netbeans.modules.masterfs.watcher.disable")) {
            this.ext = null;
            return;
        }
        this.ext = this.make(Watcher.getNotifierForPlatform());
    }

    private static Ext<?> ext() {
        Watcher w = (Watcher)Lookup.getDefault().lookup(Watcher.class);
        return w == null ? null : w.ext;
    }

    public static boolean isEnabled() {
        return Watcher.ext() != null;
    }

    public static boolean isWatched(FileObject fo) {
        Ext ext = Watcher.ext();
        if (ext == null) {
            return false;
        }
        if (fo.isData()) {
            fo = fo.getParent();
        }
        return ext.isWatched(fo);
    }

    public static void register(FileObject fo) {
        Ext ext = Watcher.ext();
        if (ext == null) {
            return;
        }
        if (fo.isData()) {
            fo = fo.getParent();
        }
        ext.register(fo);
    }

    public static void unregister(FileObject fo) {
        Ext ext = Watcher.ext();
        if (ext == null) {
            return;
        }
        if (fo.isData() && !(fo = fo.getParent()).isFolder()) {
            return;
        }
        ext.unregister(fo);
    }

    public static File wrap(File f, FileObject fo) {
        if (f instanceof FOFile) {
            return f;
        }
        return new FOFile(f, fo);
    }

    @Override
    public String annotateName(String name, Set<? extends FileObject> files) {
        return null;
    }

    @Override
    public Image annotateIcon(Image icon, int iconType, Set<? extends FileObject> files) {
        return null;
    }

    @Override
    public String annotateNameHtml(String name, Set<? extends FileObject> files) {
        return null;
    }

    @Override
    public Lookup findExtrasFor(Set<? extends FileObject> files) {
        return null;
    }

    @Override
    public InterceptionListener getInterceptionListener() {
        return this.ext;
    }

    public static void shutdown() {
        if (Watcher.isEnabled()) {
            try {
                Watcher.ext().shutdown();
            }
            catch (IOException ex) {
                LOG.log(Level.INFO, "Error on shutdown", ex);
            }
            catch (InterruptedException ex) {
                LOG.log(Level.INFO, "Error on shutdown", ex);
            }
        }
    }

    private <KEY> Ext<KEY> make(Notifier<KEY> impl) {
        return impl == null ? null : new Ext<KEY>(impl);
    }

    final void clearQueue() throws IOException {
        this.ext.clearQueue();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void enqueue(FileObject fo) {
        assert (fo != null);
        Object object = this.lock;
        synchronized (object) {
            if (this.pending == null) {
                this.refreshTask.schedule(1500);
                this.pending = new WeakSet();
            }
            this.pending.add(fo);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void enqueueAll(Set<FileObject> fos) {
        assert (fos != null);
        assert (!fos.contains(null));
        Object object = this.lock;
        synchronized (object) {
            if (this.pending == null) {
                this.refreshTask.schedule(1500);
                this.pending = new WeakSet();
            }
            this.pending.addAll(fos);
        }
    }

    private static Notifier<?> getNotifierForPlatform() {
        for (Lookup.Item item : Lookup.getDefault().lookupResult(Notifier.class).allItems()) {
            try {
                Notifier notifier = (Notifier)item.getInstance();
                if (notifier == null) continue;
                NotifierAccessor.getDefault().start(notifier);
                return notifier;
            }
            catch (IOException ex) {
                LOG.log(Level.INFO, "Notifier {0} refused to be initialized", item.getType());
                LOG.log(Level.FINE, null, ex);
                continue;
            }
            catch (Exception ex) {
                LOG.log(Level.INFO, "Exception while instantiating " + (Object)item, ex);
                continue;
            }
            catch (LinkageError ex) {
                LOG.log(Level.INFO, "Linkage error for " + (Object)item, ex);
                continue;
            }
        }
        return null;
    }

    public static synchronized void lock(FileObject fileObject) {
        int[] arr;
        if (fileObject.isData()) {
            fileObject = fileObject.getParent();
        }
        if ((arr = MODIFIED.get((Object)fileObject)) == null) {
            arr = new int[]{0};
            MODIFIED.put(fileObject, arr);
        }
        int[] arrn = arr;
        arrn[0] = arrn[0] + 1;
    }

    static synchronized boolean isLocked(FileObject fo) {
        return MODIFIED.get((Object)fo) != null;
    }

    public static synchronized void unlock(FileObject fo) {
        int[] arr;
        if (fo.isData()) {
            fo = fo.getParent();
        }
        if ((arr = MODIFIED.get((Object)fo)) == null) {
            return;
        }
        arr[0] = arr[0] - 1;
        if (arr[0] == 0) {
            MODIFIED.remove((Object)fo);
        }
    }

    private class Ext<KEY>
    extends ProvidedExtensions
    implements Runnable {
        private final ReferenceQueue<FileObject> REF;
        private final Notifier<KEY> impl;
        private final Object LOCK;
        private final Set<NotifierKeyRef> references;
        private final Thread watcher;
        private volatile boolean shutdown;
        private int loggedRegisterExceptions;

        public Ext(Notifier<KEY> impl) {
            this.REF = new ReferenceQueue();
            this.LOCK = new Object();
            this.references = new HashSet<NotifierKeyRef>();
            this.loggedRegisterExceptions = 0;
            this.impl = impl;
            this.watcher = new Thread((Runnable)this, "File Watcher");
            this.watcher.start();
        }

        @Override
        public long refreshRecursively(File dir, long lastTimeStamp, List<? super File> children) {
            assert (dir instanceof FOFile);
            FileObject fo = ((FOFile)dir).fo;
            if (fo == null && !dir.exists()) {
                return -1;
            }
            assert (fo != null);
            this.register(fo);
            return -1;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private boolean isWatched(FileObject fo) {
            if (!(fo.isFolder() || $assertionsDisabled || fo.isValid())) {
                throw new AssertionError((Object)("Should be a folder: " + (Object)fo + " type: " + fo.getClass()));
            }
            try {
                this.clearQueue();
            }
            catch (IOException ex2) {
                Watcher.LOG.log(Level.INFO, "Exception while clearing the queue", ex2);
            }
            Object ex2 = this.LOCK;
            synchronized (ex2) {
                NotifierKeyRef<Object> kr = new NotifierKeyRef<Object>(fo, null, null, this.impl);
                return this.getReferences().contains(kr);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        final void register(FileObject fo) {
            if (fo.isValid() && !fo.isFolder()) {
                Watcher.LOG.log(Level.INFO, "Should be a folder: {0} data: {1} folder: {2} valid: {3}", new Object[]{fo, fo.isData(), fo.isFolder(), fo.isValid()});
            }
            try {
                this.clearQueue();
            }
            catch (IOException ex2) {
                Watcher.LOG.log(Level.INFO, "Exception while clearing the queue", ex2);
            }
            Object ex2 = this.LOCK;
            synchronized (ex2) {
                NotifierKeyRef<Object> kr = new NotifierKeyRef<Object>(fo, null, null, this.impl);
                if (this.getReferences().contains(kr)) {
                    return;
                }
                try {
                    this.getReferences().add(new NotifierKeyRef<KEY>(fo, NotifierAccessor.getDefault().addWatch(this.impl, fo.getPath()), this.REF, this.impl));
                }
                catch (IOException ex) {
                    Level l = this.getLogLevelForRegisterException(fo);
                    Watcher.LOG.log(l, "Cannot add filesystem watch for {0}: {1}", new Object[]{fo.getPath(), ex});
                    Watcher.LOG.log(Level.FINE, null, ex);
                }
            }
        }

        private Level getLogLevelForRegisterException(FileObject fo) {
            if (!fo.isValid()) {
                return Level.FINE;
            }
            ++this.loggedRegisterExceptions;
            if (this.loggedRegisterExceptions < 3) {
                return Level.WARNING;
            }
            if (this.loggedRegisterExceptions < 13) {
                return Level.INFO;
            }
            if (this.loggedRegisterExceptions == 13) {
                Watcher.LOG.info("Following \"Cannot add filesystem watch\" will be logged with log level FINE.");
            }
            return Level.FINE;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        final void unregister(FileObject fo) {
            assert (!fo.isValid() || fo.isFolder());
            Object object = this.LOCK;
            synchronized (object) {
                final NotifierKeyRef[] equalOne = new NotifierKeyRef[1];
                NotifierKeyRef kr = new NotifierKeyRef<KEY>(fo, null, null, this.impl){

                    @Override
                    public boolean equals(Object obj) {
                        if (NotifierKeyRef.super.equals(obj)) {
                            equalOne[0] = (NotifierKeyRef)obj;
                            return true;
                        }
                        return false;
                    }

                    @Override
                    public int hashCode() {
                        return NotifierKeyRef.super.hashCode();
                    }
                };
                if (!this.references.contains(kr)) {
                    return;
                }
                assert (equalOne[0] != null);
                this.getReferences().remove(equalOne[0]);
                try {
                    equalOne[0].removeWatch();
                }
                catch (IOException ex) {
                    Watcher.LOG.log(Level.WARNING, "Cannot remove filesystem watch for {0}", fo.getPath());
                    Watcher.LOG.log(Level.INFO, "Exception", ex);
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        final void clearQueue() throws IOException {
            NotifierKeyRef kr;
            while ((kr = (NotifierKeyRef)this.REF.poll()) != null) {
                Object object = this.LOCK;
                synchronized (object) {
                    this.getReferences().remove(kr);
                    kr.removeWatch();
                    continue;
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            while (!this.shutdown) {
                try {
                    this.clearQueue();
                    String path = NotifierAccessor.getDefault().nextEvent(this.impl);
                    Watcher.LOG.log(Level.FINEST, "nextEvent: {0}", path);
                    if (path == null) {
                        HashSet<FileObject> set = new HashSet<FileObject>();
                        Object object = this.LOCK;
                        synchronized (object) {
                            for (NotifierKeyRef kr2 : this.getReferences()) {
                                FileObject ref = kr2.get();
                                if (ref == null) continue;
                                set.add(ref);
                            }
                        }
                        Watcher.this.enqueueAll(set);
                        continue;
                    }
                    File file = new File(path);
                    FileObjectFactory factory = FileObjectFactory.getInstance(file);
                    BaseFileObj fo = factory.getCachedOnly(file);
                    if (fo == null) {
                        fo = factory.getCachedOnly(file.getParentFile());
                    }
                    if (fo == null) continue;
                    Object kr2 = this.LOCK;
                    synchronized (kr2) {
                        NotifierKeyRef<Object> kr3 = new NotifierKeyRef<Object>(fo, null, null, this.impl);
                        if (this.getReferences().contains(kr3)) {
                            Watcher.this.enqueue(fo);
                        }
                        continue;
                    }
                }
                catch (ThreadDeath td) {
                    throw td;
                }
                catch (InterruptedException ie) {
                    if (this.shutdown) continue;
                    Watcher.LOG.log(Level.INFO, "Interrupted", ie);
                    continue;
                }
                catch (Throwable t) {
                    Watcher.LOG.log(Level.INFO, "Error dispatching FS changes", t);
                    continue;
                }
            }
        }

        final void shutdown() throws IOException, InterruptedException {
            this.shutdown = true;
            this.watcher.interrupt();
            NotifierAccessor.getDefault().stop(this.impl);
            this.watcher.join(1000);
        }

        private Set<NotifierKeyRef> getReferences() {
            assert (Thread.holdsLock(this.LOCK));
            return this.references;
        }

    }

}

