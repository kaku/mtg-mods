/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.masterfs.watcher;

import java.io.File;
import org.openide.filesystems.FileObject;

final class FOFile
extends File {
    final FileObject fo;

    FOFile(File f, FileObject fo) {
        super(f.getPath());
        this.fo = fo;
    }
}

