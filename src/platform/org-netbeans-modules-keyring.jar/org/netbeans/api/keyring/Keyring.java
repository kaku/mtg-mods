/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressRunnable
 *  org.netbeans.api.progress.ProgressUtils
 *  org.openide.util.Cancellable
 *  org.openide.util.Lookup
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.api.keyring;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.keyring.Bundle;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.spi.keyring.KeyringProvider;
import org.openide.util.Cancellable;
import org.openide.util.Lookup;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;

public class Keyring {
    private static final RequestProcessor KEYRING_ACCESS = new RequestProcessor(Keyring.class);
    private static final long SAFE_DELAY = 70;
    private static final Logger LOG = Logger.getLogger("org.netbeans.modules.keyring");
    private static KeyringProvider PROVIDER;

    private Keyring() {
    }

    private static synchronized KeyringProvider provider() {
        if (PROVIDER == null) {
            for (KeyringProvider p : Lookup.getDefault().lookupAll(KeyringProvider.class)) {
                if (!p.enabled()) continue;
                PROVIDER = p;
                break;
            }
            if (PROVIDER == null) {
                PROVIDER = new DummyKeyringProvider();
            }
            LOG.log(Level.FINE, "Using provider: {0}", PROVIDER);
        }
        return PROVIDER;
    }

    private static synchronized char[] readImpl(String key) {
        LOG.log(Level.FINEST, "reading: {0}", key);
        return Keyring.provider().read(key);
    }

    @CheckForNull
    public static char[] read(final @NonNull String key) {
        Parameters.notNull((CharSequence)"key", (Object)key);
        try {
            Future result = KEYRING_ACCESS.submit((Callable)new Callable<char[]>(){

                @Override
                public char[] call() throws Exception {
                    return Keyring.readImpl(key);
                }
            });
            if (SwingUtilities.isEventDispatchThread() && !result.isDone()) {
                try {
                    return (char[])result.get(70, TimeUnit.MILLISECONDS);
                }
                catch (TimeoutException ex) {
                    return (char[])ProgressUtils.showProgressDialogAndRun(new ProgressRunnable(result), (String)Bundle.MSG_KeyringAccess(), (boolean)false);
                }
            }
            return (char[])result.get();
        }
        catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        catch (ExecutionException ex) {
            LOG.log(Level.INFO, null, ex);
        }
        return null;
    }

    private static synchronized void saveImpl(String key, char[] password, String description) {
        LOG.log(Level.FINEST, "saving: {0}", key);
        Keyring.provider().save(key, password, description);
        Arrays.fill(password, '\u0000');
    }

    public static void save(final @NonNull String key, final @NonNull char[] password, final @NullAllowed String description) {
        Parameters.notNull((CharSequence)"key", (Object)key);
        Parameters.notNull((CharSequence)"password", (Object)password);
        KEYRING_ACCESS.post(new Runnable(){

            @Override
            public void run() {
                Keyring.saveImpl(key, password, description);
            }
        });
    }

    private static synchronized void deleteImpl(String key) {
        LOG.log(Level.FINEST, "deleting: {0}", key);
        Keyring.provider().delete(key);
    }

    public static void delete(final @NonNull String key) {
        Parameters.notNull((CharSequence)"key", (Object)key);
        KEYRING_ACCESS.post(new Runnable(){

            @Override
            public void run() {
                Keyring.deleteImpl(key);
            }
        });
    }

    private static byte[] chars2Bytes(char[] chars) {
        byte[] bytes = new byte[chars.length * 2];
        for (int i = 0; i < chars.length; ++i) {
            bytes[i * 2] = (byte)(chars[i] / 256);
            bytes[i * 2 + 1] = (byte)(chars[i] % 256);
        }
        return bytes;
    }

    private static char[] bytes2Chars(byte[] bytes) {
        char[] result = new char[bytes.length / 2];
        for (int i = 0; i < result.length; ++i) {
            result[i] = (char)(bytes[i * 2] * 256 + bytes[i * 2 + 1]);
        }
        return result;
    }

    private static class ProgressRunnable<T>
    implements org.netbeans.api.progress.ProgressRunnable<T>,
    Cancellable {
        private final Future<? extends T> task;

        public ProgressRunnable(Future<? extends T> task) {
            this.task = task;
        }

        public T run(ProgressHandle handle) {
            try {
                return this.task.get();
            }
            catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            catch (ExecutionException ex) {
                LOG.log(Level.INFO, null, ex);
            }
            return null;
        }

        public boolean cancel() {
            return this.task.cancel(true);
        }
    }

    private static class DummyKeyringProvider
    implements KeyringProvider {
        private final Map<String, byte[]> passwords = new HashMap<String, byte[]>();

        private DummyKeyringProvider() {
        }

        @Override
        public boolean enabled() {
            return true;
        }

        @Override
        public char[] read(String key) {
            byte[] pwd = this.passwords.get(key);
            return pwd != null ? Keyring.bytes2Chars(pwd) : null;
        }

        @Override
        public void save(String key, char[] password, String description) {
            this.passwords.put(key, Keyring.chars2Bytes(password));
        }

        @Override
        public void delete(String key) {
            this.passwords.remove(key);
        }
    }

}

