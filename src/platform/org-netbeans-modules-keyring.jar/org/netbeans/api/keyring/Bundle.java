/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.api.keyring;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String MSG_KeyringAccess() {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_KeyringAccess");
    }

    private void Bundle() {
    }
}

