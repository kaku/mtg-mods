/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.keyring;

public interface KeyringProvider {
    public boolean enabled();

    public char[] read(String var1);

    public void save(String var1, char[] var2, String var3);

    public void delete(String var1);
}

