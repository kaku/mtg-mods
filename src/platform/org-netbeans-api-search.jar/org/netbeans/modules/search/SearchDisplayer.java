/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 *  org.openide.nodes.Node
 *  org.openide.util.NbBundle
 *  org.openide.windows.IOProvider
 *  org.openide.windows.InputOutput
 *  org.openide.windows.OutputListener
 *  org.openide.windows.OutputWriter
 */
package org.netbeans.modules.search;

import java.awt.EventQueue;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import org.netbeans.modules.search.ResultView;
import org.openide.ErrorManager;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;
import org.openide.windows.OutputListener;
import org.openide.windows.OutputWriter;

public final class SearchDisplayer {
    public static final String ATTR_OUTPUT_LINE = "output line";
    private OutputWriter ow = null;
    private Reference<OutputWriter> owRef = null;

    SearchDisplayer() {
    }

    void prepareOutput() {
        String tabName = NbBundle.getMessage(ResultView.class, (String)"TITLE_SEARCH_RESULTS");
        InputOutput searchIO = IOProvider.getDefault().getIO(tabName, false);
        this.ow = searchIO.getOut();
        this.owRef = new WeakReference<OutputWriter>(this.ow);
        searchIO.select();
    }

    static void clearOldOutput(Reference<OutputWriter> outputWriterRef) {
        OutputWriter oldWriter;
        if (outputWriterRef != null && (oldWriter = outputWriterRef.get()) != null) {
            try {
                oldWriter.reset();
            }
            catch (IOException ex) {
                ErrorManager.getDefault().notify((Throwable)ex);
            }
        }
    }

    void displayNodes(Node[] nodes) {
        final String[] outputLines = new String[nodes.length];
        final OutputListener[] listeners = new OutputListener[nodes.length];
        for (int i = 0; i < nodes.length; ++i) {
            Node node = nodes[i];
            Object o = node.getValue("output line");
            outputLines[i] = o instanceof String ? (String)o : node.getShortDescription();
            listeners[i] = node instanceof OutputListener ? (OutputListener)node : null;
        }
        try {
            EventQueue.invokeAndWait(new Runnable(){

                @Override
                public void run() {
                    try {
                        for (int i = 0; i < outputLines.length; ++i) {
                            OutputListener listener = listeners[i];
                            if (listener != null) {
                                SearchDisplayer.this.ow.println(outputLines[i], listener);
                                continue;
                            }
                            SearchDisplayer.this.ow.println(outputLines[i]);
                        }
                    }
                    catch (Exception ex) {
                        ErrorManager.getDefault().notify(4096, (Throwable)ex);
                    }
                }
            });
        }
        catch (Exception ex) {
            ErrorManager.getDefault().notify((Throwable)ex);
        }
    }

    void finishDisplaying() {
        this.ow.flush();
        this.ow.close();
        this.ow = null;
    }

    Reference<OutputWriter> getOutputWriterRef() {
        return this.owRef;
    }

}

