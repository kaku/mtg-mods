/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.search.ui;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.text.JTextComponent;

public class TextFieldFocusListener
implements FocusListener {
    @Override
    public void focusGained(FocusEvent e) {
        JTextComponent textComp;
        if (!e.isTemporary() && (textComp = (JTextComponent)e.getSource()).getText().length() != 0) {
            textComp.selectAll();
        }
    }

    @Override
    public void focusLost(FocusEvent e) {
    }
}

