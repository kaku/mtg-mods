/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.search.ui;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class LinkButtonPanel
extends JPanel {
    private JButton button;
    private JLabel leftParenthesis;
    private JLabel rightParenthesis;
    private String enabledText;
    private String disabledText;

    public LinkButtonPanel(JButton button) {
        this.button = button;
        this.initTexts();
        this.init();
    }

    private void init() {
        this.setLayout(new FlowLayout(3, 0, 0));
        this.setLinkLikeButton(this.button);
        this.leftParenthesis = new JLabel("(");
        this.rightParenthesis = new JLabel(")");
        this.add(this.leftParenthesis);
        this.add(this.button);
        this.add(this.rightParenthesis);
        MouseListener ml = this.createLabelMouseListener();
        this.leftParenthesis.addMouseListener(ml);
        this.rightParenthesis.addMouseListener(ml);
        this.button.setEnabled(false);
        this.setMaximumSize(this.getPreferredSize());
    }

    private void initTexts() {
        this.enabledText = this.button.getText();
        this.disabledText = this.enabledText.startsWith("<html><u><a href=\"#\">") && this.enabledText.endsWith("</a></u></html>") ? this.enabledText.substring("<html><u><a href=\"#\">".length(), this.enabledText.length() - "</a></u></html>".length()) : this.enabledText;
    }

    private MouseListener createLabelMouseListener() {
        return new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent e) {
                if (LinkButtonPanel.this.button.isEnabled()) {
                    for (ActionListener al : LinkButtonPanel.this.button.getActionListeners()) {
                        al.actionPerformed(null);
                    }
                }
            }
        };
    }

    private void setLinkLikeButton(JButton button) {
        button.setBorderPainted(false);
        button.setContentAreaFilled(false);
        button.setBorder(new EmptyBorder(0, 0, 0, 0));
        button.setCursor(Cursor.getPredefinedCursor(12));
    }

    public void enableButton() {
        this.button.setText(this.enabledText);
        this.button.setEnabled(true);
        this.leftParenthesis.setCursor(Cursor.getPredefinedCursor(12));
        this.rightParenthesis.setCursor(Cursor.getPredefinedCursor(12));
        this.leftParenthesis.setEnabled(true);
        this.rightParenthesis.setEnabled(true);
        this.setMinimumSize(this.getPreferredSize());
    }

    public void disableButton() {
        this.button.setText(this.disabledText);
        this.button.setEnabled(false);
        this.leftParenthesis.setCursor(Cursor.getDefaultCursor());
        this.rightParenthesis.setCursor(Cursor.getDefaultCursor());
        this.leftParenthesis.setEnabled(false);
        this.rightParenthesis.setEnabled(false);
        this.setMinimumSize(this.getPreferredSize());
    }

    public void setButtonEnabled(boolean enabled) {
        if (enabled) {
            this.enableButton();
        } else {
            this.disableButton();
        }
    }

}

