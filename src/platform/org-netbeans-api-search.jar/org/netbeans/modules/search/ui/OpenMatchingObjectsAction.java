/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.EditCookie
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.NodeAction
 */
package org.netbeans.modules.search.ui;

import java.awt.EventQueue;
import java.util.List;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.TextDetail;
import org.netbeans.modules.search.ui.UiUtils;
import org.openide.cookies.EditCookie;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.NodeAction;

public class OpenMatchingObjectsAction
extends NodeAction {
    public String getName() {
        return UiUtils.getText("LBL_EditAction");
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected void performAction(Node[] activatedNodes) {
        for (Node n : activatedNodes) {
            EditCookie editCookie;
            final MatchingObject mo = (MatchingObject)n.getLookup().lookup(MatchingObject.class);
            if (mo == null) continue;
            if (mo.getTextDetails() != null && !mo.getTextDetails().isEmpty()) {
                EventQueue.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        mo.getTextDetails().get(0).showDetail(2);
                    }
                });
                continue;
            }
            DataObject dob = mo.getDataObject();
            if (dob == null || (editCookie = (EditCookie)dob.getLookup().lookup(EditCookie.class)) == null) continue;
            editCookie.edit();
        }
    }

    protected boolean enable(Node[] activatedNodes) {
        return activatedNodes != null && activatedNodes.length > 0;
    }

}

