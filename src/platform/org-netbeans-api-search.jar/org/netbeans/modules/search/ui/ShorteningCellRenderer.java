/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.search.ui;

import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;

public class ShorteningCellRenderer
extends DefaultListCellRenderer {
    private static final int COMBO_TEXT_LENGHT_LIMIT = 100;
    private static final String THREE_DOTS = "...";

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        String s = value == null ? null : value.toString();
        Component component = super.getListCellRendererComponent(list, s, index, isSelected, cellHasFocus);
        if (s != null && s.length() > 100 && component instanceof JLabel) {
            ((JLabel)component).setText(s.substring(0, 100 - "...".length()) + "...");
        }
        return component;
    }
}

