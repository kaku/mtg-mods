/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.search.ui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import org.netbeans.modules.search.ui.LinkButtonPanel;

public class CheckBoxWithButtonPanel
extends JPanel
implements ItemListener {
    private JCheckBox checkbox;
    private LinkButtonPanel buttonPanel;

    public CheckBoxWithButtonPanel(JCheckBox checkbox, JButton button) {
        this.checkbox = checkbox;
        this.buttonPanel = new LinkButtonPanel(button);
        this.init();
    }

    private void init() {
        this.setLayout(new FlowLayout(3, 0, 0));
        this.add(this.checkbox);
        this.add(this.buttonPanel);
        this.setMaximumSize(this.getMinimumSize());
        this.checkbox.addItemListener(this);
        if (this.checkbox.isSelected()) {
            this.buttonPanel.enableButton();
        } else {
            this.buttonPanel.disableButton();
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if (this.checkbox.isSelected()) {
            this.buttonPanel.enableButton();
        } else {
            this.buttonPanel.disableButton();
        }
        this.setMinimumSize(this.getPreferredSize());
    }
}

