/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.Outline
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.view.OutlineView
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.search.ui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import org.netbeans.modules.search.Constants;
import org.netbeans.modules.search.ui.AbstractSearchResultsPanel;
import org.netbeans.modules.search.ui.UiUtils;
import org.netbeans.spi.search.provider.SearchComposition;
import org.netbeans.spi.search.provider.SearchProvider;
import org.netbeans.spi.search.provider.SearchResultsDisplayer;
import org.netbeans.swing.outline.Outline;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

public class DefaultSearchResultsPanel<T>
extends AbstractSearchResultsPanel {
    private List<T> matchingObjects = new ArrayList<T>();
    private final SearchResultsDisplayer.NodeDisplayer<T> nodeDisplayer;
    private DefaultSearchResultsPanel<T> resultsNode;
    private OutlineView outlineView;

    public DefaultSearchResultsPanel(SearchResultsDisplayer.NodeDisplayer<T> nodeDisplayer, SearchComposition<T> searchComposition, SearchProvider.Presenter searchProviderPresenter) {
        super(searchComposition, searchProviderPresenter);
        this.resultsNode = new ResultsNode();
        this.nodeDisplayer = nodeDisplayer;
        this.resultsNode.update();
        this.outlineView = new OutlineView(UiUtils.getText("BasicSearchResultsPanel.outline.nodes"));
        this.outlineView.getOutline().setRootVisible(false);
        this.initExpandButton();
        this.getContentPanel().add((Component)this.outlineView);
    }

    private void initExpandButton() {
        this.btnExpand.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                DefaultSearchResultsPanel.this.getOutlineView().expandNode((Node)DefaultSearchResultsPanel.this.resultsNode);
                for (Node n : DefaultSearchResultsPanel.this.resultsNode.getChildren().getNodes(true)) {
                    DefaultSearchResultsPanel.this.toggleExpand(n, DefaultSearchResultsPanel.this.btnExpand.isSelected());
                }
            }
        });
        this.btnExpand.setEnabled(true);
    }

    public void addMatchingObject(T object) {
        this.matchingObjects.add(object);
        this.resultsNode.update();
        this.afterMatchingNodeAdded();
    }

    @Override
    public void searchFinished() {
        super.searchFinished();
        this.resultsNode.setDisplayName(NbBundle.getMessage(Constants.class, (String)"TEXT_MSG_FOUND_X_NODES", (Object)this.matchingObjects.size()));
    }

    @Override
    public OutlineView getOutlineView() {
        return this.outlineView;
    }

    public JButton getButtonPrevious() {
        return this.btnPrev;
    }

    public JButton getButtonNext() {
        return this.btnNext;
    }

    public JToggleButton getButtonExpand() {
        return this.btnExpand;
    }

    @Override
    public void addButton(AbstractButton button) {
        super.addButton(button);
    }

    public void setInfoNode(Node infoNode) {
        AbstractSearchResultsPanel.RootNode root = new AbstractSearchResultsPanel.RootNode((Node)this.resultsNode, infoNode);
        this.getExplorerManager().setRootContext((Node)root);
        this.getOutlineView().expandNode(this.resultsNode);
    }

    @Override
    protected boolean isDetailNode(Node n) {
        return true;
    }

    private class ResultsNodeChildren
    extends Children.Keys<T> {
        private ResultsNodeChildren() {
        }

        protected Node[] createNodes(T key) {
            return new Node[]{DefaultSearchResultsPanel.this.nodeDisplayer.matchToNode(key)};
        }

        void update() {
            this.setKeys((Collection)DefaultSearchResultsPanel.this.matchingObjects);
        }
    }

    private class ResultsNode
    extends AbstractNode {
        private DefaultSearchResultsPanel<T> children;

        public ResultsNode() {
            this(defaultSearchResultsPanel, defaultSearchResultsPanel.new ResultsNodeChildren());
        }

        public ResultsNode() {
            super(children);
            this.children = children;
        }

        void update() {
            this.setDisplayName(NbBundle.getMessage(Constants.class, (String)"TXT_RootSearchedNodes", (Object)this$0.matchingObjects.size()));
            this.children.update();
        }
    }

}

