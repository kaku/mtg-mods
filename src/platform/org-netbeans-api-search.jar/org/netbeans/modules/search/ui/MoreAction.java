/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 *  org.openide.util.actions.ActionPresenterProvider
 *  org.openide.util.actions.NodeAction
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Popup
 */
package org.netbeans.modules.search.ui;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.ui.Bundle;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.ContextAwareAction;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.openide.util.actions.ActionPresenterProvider;
import org.openide.util.actions.NodeAction;
import org.openide.util.actions.Presenter;

public class MoreAction
extends NodeAction
implements Presenter.Popup {
    private static final KeyStroke DELETE_KS = KeyStroke.getKeyStroke(127, 0);

    protected void performAction(Node[] activatedNodes) {
    }

    protected boolean enable(Node[] activatedNodes) {
        return true;
    }

    public String getName() {
        return Bundle.MoreAction_name();
    }

    public HelpCtx getHelpCtx() {
        return null;
    }

    public JMenuItem getPopupPresenter() {
        Lookup l = Utilities.actionsGlobalContext();
        Collection matchingObjects = l.lookupAll(MatchingObject.class);
        LinkedHashSet<Action> commonActions = new LinkedHashSet<Action>();
        boolean first = true;
        for (MatchingObject mo : matchingObjects) {
            DataObject dob = mo.getDataObject();
            if (dob == null) continue;
            Node nodeDelegate = dob.getNodeDelegate();
            List<Action> dobActions = Arrays.asList(nodeDelegate.getActions(false));
            if (first) {
                commonActions.addAll(dobActions);
                first = false;
                continue;
            }
            commonActions.retainAll(dobActions);
        }
        return this.actionsToMenu(commonActions, l);
    }

    private JMenuItem actionsToMenu(Set<Action> actions, Lookup lookup) {
        HashSet<Action> counted = new HashSet<Action>();
        ArrayList<Component> components = new ArrayList<Component>();
        Iterator<Action> i$ = actions.iterator();
        while (i$.hasNext()) {
            JMenuItem item;
            Action action = i$.next();
            if (action == null || !counted.add(action)) continue;
            if (action instanceof ContextAwareAction) {
                Action contextAwareAction = ((ContextAwareAction)action).createContextAwareInstance(lookup);
                if (contextAwareAction == null) {
                    Logger.getLogger(Utilities.class.getName()).log(Level.WARNING, "ContextAwareAction.createContextAwareInstance(lookup) returns null. That is illegal! action={0}, lookup={1}", new Object[]{action, lookup});
                } else {
                    action = contextAwareAction;
                }
            }
            if (action instanceof Presenter.Popup) {
                item = ((Presenter.Popup)action).getPopupPresenter();
                if (item == null) {
                    Logger.getLogger(Utilities.class.getName()).log(Level.WARNING, "findContextMenuImpl, getPopupPresenter returning null for {0}", action);
                    continue;
                }
            } else {
                item = ActionPresenterProvider.getDefault().createPopupPresenter(action);
            }
            if (!action.isEnabled()) continue;
            for (Component c : ActionPresenterProvider.getDefault().convertComponents((Component)item)) {
                if (c instanceof JSeparator) continue;
                components.add(c);
            }
        }
        JMenu menu = new JMenu((Action)((Object)this));
        boolean nonempty = false;
        boolean pendingSep = false;
        for (Component c : components) {
            try {
                if (c == null) {
                    pendingSep = nonempty;
                    continue;
                }
                this.removeDeleteAccelerator(c);
                nonempty = true;
                if (pendingSep) {
                    pendingSep = false;
                    menu.addSeparator();
                }
                menu.add(c);
            }
            catch (RuntimeException ex) {
                Exceptions.attachMessage((Throwable)ex, (String)("Current component: " + c));
                Exceptions.attachMessage((Throwable)ex, (String)("List of components: " + components));
                Exceptions.attachMessage((Throwable)ex, (String)("List of actions: " + actions));
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return menu;
    }

    private void removeDeleteAccelerator(Component item) {
        JMenuItem mItem;
        if (item instanceof JMenuItem && DELETE_KS.equals((mItem = (JMenuItem)item).getAccelerator())) {
            mItem.setAccelerator(null);
        }
    }
}

