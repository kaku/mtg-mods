/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.search.ui;

import java.awt.event.ActionEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.modules.search.BasicComposition;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.matcher.AbstractMatcher;
import org.netbeans.modules.search.ui.BasicReplaceResultsPanel;
import org.netbeans.modules.search.ui.Bundle;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.util.RequestProcessor;

public class RefreshAction
extends AbstractAction {
    private final MatchingObject mo;

    public RefreshAction(MatchingObject mo) {
        super(Bundle.RefreshAction_name());
        this.mo = mo;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.mo != null) {
            RequestProcessor.getDefault().post(new Runnable(){

                @Override
                public void run() {
                    MatchingObject.Def def;
                    FileObject fo = RefreshAction.this.mo.getFileObject();
                    if (!fo.isValid()) {
                        try {
                            fo = fo.getFileSystem().findResource(fo.getPath());
                        }
                        catch (FileStateInvalidException ex) {
                            Logger.getLogger(BasicReplaceResultsPanel.class.getName()).log(Level.FINE, null, (Throwable)ex);
                        }
                    }
                    if (fo != null && fo.isValid() && (def = RefreshAction.this.mo.getBasicComposition().getMatcher().check(fo, new SearchListener(){})) != null) {
                        RefreshAction.this.mo.refresh(def);
                    }
                }

            });
        }
    }

}

