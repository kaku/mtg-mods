/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.Outline
 *  org.openide.awt.Mnemonics
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.view.OutlineView
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.search.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JToggleButton;
import org.netbeans.api.search.provider.SearchInfo;
import org.netbeans.modules.search.BasicComposition;
import org.netbeans.modules.search.BasicSearchCriteria;
import org.netbeans.modules.search.ContextView;
import org.netbeans.modules.search.FindDialogMemory;
import org.netbeans.modules.search.Manager;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.ReplaceTask;
import org.netbeans.modules.search.ResultModel;
import org.netbeans.modules.search.ResultView;
import org.netbeans.modules.search.matcher.AbstractMatcher;
import org.netbeans.modules.search.ui.BasicAbstractResultsPanel;
import org.netbeans.modules.search.ui.IssuesPanel;
import org.netbeans.modules.search.ui.ResultsOutlineSupport;
import org.netbeans.spi.search.provider.SearchComposition;
import org.netbeans.swing.outline.Outline;
import org.openide.awt.Mnemonics;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

public class BasicReplaceResultsPanel
extends BasicAbstractResultsPanel {
    private static final RequestProcessor RP = new RequestProcessor(BasicReplaceResultsPanel.class.getName());
    private final RequestProcessor.Task SAVE_TASK;
    private JButton replaceButton;
    private JSplitPane splitPane;

    public BasicReplaceResultsPanel(ResultModel resultModel, BasicComposition composition, Node infoNode) {
        super(resultModel, composition, true, new ResultsOutlineSupport(true, true, resultModel, composition, infoNode));
        this.SAVE_TASK = RP.create((Runnable)new SaveTask());
        this.init();
    }

    private void init() {
        JPanel leftPanel = new JPanel();
        this.replaceButton = new JButton();
        this.replaceButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                BasicReplaceResultsPanel.this.replace();
            }
        });
        this.updateReplaceButton();
        leftPanel.setLayout(new BoxLayout(leftPanel, 3));
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout(3, 2, 1));
        buttonPanel.add(this.replaceButton);
        this.replaceButton.setMaximumSize(this.replaceButton.getPreferredSize());
        buttonPanel.setMaximumSize(new Dimension((int)buttonPanel.getMaximumSize().getWidth(), (int)buttonPanel.getPreferredSize().getHeight()));
        leftPanel.add((Component)this.resultsOutlineSupport.getOutlineView());
        leftPanel.add(buttonPanel);
        this.splitPane = new JSplitPane();
        this.splitPane.setLeftComponent(leftPanel);
        this.splitPane.setRightComponent(new ContextView(this.resultModel, this.getExplorerManager()));
        this.initSplitDividerLocationHandling();
        this.getContentPanel().add(this.splitPane);
        this.initResultModelListener();
        this.replaceButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ResultView.class, (String)"ACS_TEXT_BUTTON_REPLACE"));
    }

    private void replace() {
        ReplaceTask taskReplace = new ReplaceTask(this.resultModel.getMatchingObjects(), this);
        this.resultsOutlineSupport.clean();
        this.replaceButton.setEnabled(false);
        Manager.getInstance().scheduleReplaceTask(taskReplace);
    }

    private void initResultModelListener() {
        this.resultModel.addPropertyChangeListener(new ModelListener());
    }

    private void initSplitDividerLocationHandling() {
        int location = FindDialogMemory.getDefault().getReplaceResultsDivider();
        if (location > 0) {
            this.splitPane.setDividerLocation(location);
        }
        this.splitPane.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                String pn = evt.getPropertyName();
                if (pn.equals("dividerLocation")) {
                    BasicReplaceResultsPanel.this.SAVE_TASK.schedule(1000);
                }
            }
        });
    }

    @Override
    public void searchFinished() {
        super.searchFinished();
        if (this.resultModel.isValid()) {
            this.updateReplaceButton();
        }
        if (this.replaceButton.isVisible() && this.replaceButton.isEnabled()) {
            this.replaceButton.requestFocusInWindow();
        }
    }

    private void updateReplaceButton() {
        int matches = this.resultModel.getSelectedMatchesCount();
        Mnemonics.setLocalizedText((AbstractButton)this.replaceButton, (String)NbBundle.getMessage(ResultView.class, (String)"TEXT_BUTTON_REPLACE", (Object)matches));
        this.replaceButton.setEnabled(matches > 0 && this.isFinished());
    }

    public void displayIssuesToUser(ReplaceTask task, final String title, final String[] problems, final boolean reqAtt) {
        Mutex.EVENT.writeAccess(new Runnable(){

            @Override
            public void run() {
                IssuesPanel issuesPanel = new IssuesPanel(title, problems);
                if (BasicAbstractResultsPanel.isMacLaf) {
                    issuesPanel.setBackground(BasicAbstractResultsPanel.macBackground);
                }
                BasicReplaceResultsPanel.this.displayIssues(issuesPanel);
                if (!ResultView.getInstance().isOpened()) {
                    ResultView.getInstance().open();
                }
                if (reqAtt) {
                    ResultView.getInstance().requestAttention(true);
                }
            }
        });
    }

    void displayIssues(IssuesPanel issuesPanel) {
        if (issuesPanel != null) {
            this.showRefreshButton();
            this.removeButtons(this.btnNext, this.btnPrev, this.btnFlatView, this.btnTreeView, this.btnExpand, this.showDetailsButton);
            JPanel p = this.getContentPanel();
            p.removeAll();
            p.add(issuesPanel);
            this.validate();
            this.repaint();
        }
    }

    private /* varargs */ void removeButtons(AbstractButton ... abstractButtons) {
        for (AbstractButton ab : abstractButtons) {
            if (ab == null) continue;
            Container c = ab.getParent();
            c.remove(ab);
        }
    }

    public void rescan() {
        BasicComposition bc = new BasicComposition(this.composition.getSearchInfo(), this.composition.getMatcher(), this.composition.getBasicSearchCriteria(), this.composition.getScopeDisplayName());
        Manager.getInstance().scheduleSearchTask(bc, true);
    }

    public void showFinishedInfo() {
        final AbstractNode an = new AbstractNode(Children.LEAF);
        an.setIconBaseWithExtension("org/netbeans/modules/search/res/info.png");
        an.setDisplayName(NbBundle.getMessage(ResultView.class, (String)"TEXT_INFO_REPLACE_FINISHED", (Object)this.resultModel.getSelectedMatchesCount()));
        Mutex.EVENT.writeAccess(new Runnable(){

            @Override
            public void run() {
                BasicReplaceResultsPanel.this.getOutlineView().getOutline().setRootVisible(true);
                BasicReplaceResultsPanel.this.getExplorerManager().setRootContext((Node)an);
                BasicReplaceResultsPanel.this.getOutlineView().validate();
                BasicReplaceResultsPanel.this.getOutlineView().repaint();
                BasicReplaceResultsPanel.this.btnNext.setEnabled(false);
                BasicReplaceResultsPanel.this.btnPrev.setEnabled(false);
                BasicReplaceResultsPanel.this.btnTreeView.setEnabled(false);
                BasicReplaceResultsPanel.this.btnFlatView.setEnabled(false);
                BasicReplaceResultsPanel.this.btnExpand.setEnabled(false);
            }
        });
    }

    private class SaveTask
    implements Runnable {
        private SaveTask() {
        }

        @Override
        public void run() {
            if (BasicReplaceResultsPanel.this.splitPane != null) {
                FindDialogMemory.getDefault().setReplaceResultsDivider(BasicReplaceResultsPanel.this.splitPane.getDividerLocation());
            }
        }
    }

    private class ModelListener
    implements PropertyChangeListener {
        private ModelListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            String pn = evt.getPropertyName();
            if ("valid".equals(pn) && Boolean.FALSE.equals(evt.getNewValue())) {
                BasicReplaceResultsPanel.this.replaceButton.setText(NbBundle.getMessage(ResultView.class, (String)"TEXT_BUTTON_REPLACE_INVALID"));
                BasicReplaceResultsPanel.this.replaceButton.setEnabled(false);
            } else if (BasicReplaceResultsPanel.this.resultModel.isValid()) {
                if ("valid".equals(pn) && Boolean.TRUE.equals(evt.getNewValue())) {
                    BasicReplaceResultsPanel.this.setFinalRootNodeText();
                }
                BasicReplaceResultsPanel.this.updateReplaceButton();
            }
        }
    }

}

