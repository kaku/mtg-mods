/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.etable.ETableColumnModel
 *  org.netbeans.swing.outline.Outline
 *  org.openide.explorer.view.OutlineView
 *  org.openide.explorer.view.Visualizer
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.FilterNode
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.datatransfer.PasteType
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.search.ui;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Image;
import java.awt.datatransfer.Transferable;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.KeyStroke;
import javax.swing.ToolTipManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.tree.TreePath;
import org.netbeans.modules.search.BasicComposition;
import org.netbeans.modules.search.FindDialogMemory;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.Removable;
import org.netbeans.modules.search.ResultModel;
import org.netbeans.modules.search.Selectable;
import org.netbeans.modules.search.TextDetail;
import org.netbeans.modules.search.ui.AbstractSearchResultsPanel;
import org.netbeans.modules.search.ui.HideResultAction;
import org.netbeans.modules.search.ui.MatchingObjectNode;
import org.netbeans.modules.search.ui.ReplaceCheckableNode;
import org.netbeans.modules.search.ui.ResultsOutlineCellRenderer;
import org.netbeans.modules.search.ui.UiUtils;
import org.netbeans.swing.etable.ETableColumnModel;
import org.netbeans.swing.outline.Outline;
import org.openide.explorer.view.OutlineView;
import org.openide.explorer.view.Visualizer;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.Lookups;

public class ResultsOutlineSupport {
    private static final String ROOT_NODE_ICON = "org/netbeans/modules/search/res/context.gif";
    private static final int VERTICAL_ROW_SPACE = 2;
    OutlineView outlineView;
    private boolean replacing;
    private boolean details;
    private ResultsNode resultsNode;
    private ResultModel resultModel;
    private FolderTreeItem rootPathItem = new FolderTreeItem();
    private BasicComposition basicComposition;
    private List<FileObject> rootFiles = null;
    private Node infoNode;
    private Node invisibleRoot;
    private List<TableColumn> allColumns = new ArrayList<TableColumn>(5);
    private ETableColumnModel columnModel;
    private List<MatchingObjectNode> matchingObjectNodes;
    private boolean closed = false;

    public ResultsOutlineSupport(boolean replacing, boolean details, ResultModel resultModel, BasicComposition basicComposition, Node infoNode) {
        this.replacing = replacing;
        this.details = details;
        this.resultModel = resultModel;
        this.basicComposition = basicComposition;
        this.resultsNode = new ResultsNode(resultModel);
        this.infoNode = infoNode;
        this.invisibleRoot = new AbstractSearchResultsPanel.RootNode((Node)this.resultsNode, infoNode);
        this.matchingObjectNodes = new LinkedList<MatchingObjectNode>();
        this.createOutlineView();
    }

    private void createOutlineView() {
        this.outlineView = new OutlineView(UiUtils.getText("BasicSearchResultsPanel.outline.nodes"));
        this.outlineView.getOutline().setDefaultRenderer(Node.Property.class, (TableCellRenderer)((Object)new ResultsOutlineCellRenderer()));
        this.setOutlineColumns();
        this.outlineView.getOutline().setAutoCreateColumnsFromModel(false);
        this.outlineView.addTreeExpansionListener((TreeExpansionListener)new ExpandingTreeExpansionListener());
        this.outlineView.getOutline().setRootVisible(false);
        this.outlineView.addHierarchyListener(new HierarchyListener(){

            @Override
            public void hierarchyChanged(HierarchyEvent e) {
                if ((e.getChangeFlags() & 2) != 0 && ResultsOutlineSupport.this.outlineView.isDisplayable()) {
                    ResultsOutlineSupport.this.outlineView.expandNode((Node)ResultsOutlineSupport.this.resultsNode);
                }
            }
        });
        this.outlineView.getOutline().getColumnModel().addColumnModelListener(new ColumnsListener());
        this.outlineView.getOutline().getInputMap().remove(KeyStroke.getKeyStroke(27, 0));
        this.outlineView.getOutline().getInputMap().put(KeyStroke.getKeyStroke(127, 0), "hide");
        this.outlineView.getOutline().getActionMap().put("hide", (Action)SystemAction.get(HideResultAction.class));
        this.outlineView.getOutline().setShowGrid(false);
        Font font = this.outlineView.getOutline().getFont();
        FontMetrics fm = this.outlineView.getOutline().getFontMetrics(font);
        this.outlineView.getOutline().setRowHeight(Math.max(16, fm.getHeight()) + 2);
        this.outlineView.setTreeHorizontalScrollBarPolicy(30);
        this.setTooltipHidingBehavior();
    }

    private void setTooltipHidingBehavior() {
        MouseAdapter ma = new MouseAdapter(){
            private long lastMoveTime;

            @Override
            public void mouseMoved(MouseEvent e) {
                long time = System.currentTimeMillis();
                if (time - this.lastMoveTime >= (long)ToolTipManager.sharedInstance().getInitialDelay()) {
                    ToolTipManager.sharedInstance().mousePressed(e);
                }
                this.lastMoveTime = time;
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                ToolTipManager.sharedInstance().mousePressed(e);
                this.lastMoveTime = System.currentTimeMillis();
            }

            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                ToolTipManager.sharedInstance().mouseMoved(e);
                this.lastMoveTime = System.currentTimeMillis();
            }
        };
        this.outlineView.getOutline().addMouseListener((MouseListener)ma);
        this.outlineView.getOutline().addMouseMotionListener((MouseMotionListener)ma);
        this.outlineView.addMouseWheelListener((MouseWheelListener)ma);
    }

    public synchronized void closed() {
        this.clean();
        this.saveColumnState();
    }

    public synchronized void clean() {
        this.resultModel.close();
        for (MatchingObjectNode mo : this.matchingObjectNodes) {
            mo.clean();
        }
        this.closed = true;
    }

    private void loadColumnState() {
        FindDialogMemory memory = FindDialogMemory.getDefault();
        String state = this.replacing ? memory.getResultsColumnWidthsReplacing() : (this.details ? memory.getResultsColumnWidthsDetails() : memory.getResultsColumnWidths());
        String[] parts = state.split("\\|");
        if (parts == null || parts.length != 2) {
            return;
        }
        String[] order = parts[1].split(":");
        for (int i = 0; i < order.length; ++i) {
            try {
                int modelIndex = Integer.parseInt(order[i]);
                int oldIndex = this.columnModel.getColumnIndex(this.allColumns.get(modelIndex).getIdentifier());
                this.columnModel.moveColumn(oldIndex, i);
                continue;
            }
            catch (NumberFormatException e) {
                // empty catch block
            }
        }
        String[] widths = parts[0].split(":");
        for (int i2 = 0; i2 < widths.length && i2 < this.allColumns.size(); ++i2) {
            String widthStr = widths[i2];
            if (widthStr == null || widthStr.isEmpty()) continue;
            try {
                int width = Integer.parseInt(widthStr);
                if (width == -1) {
                    this.columnModel.setColumnHidden(this.allColumns.get(i2), true);
                    continue;
                }
                this.allColumns.get(i2).setPreferredWidth(width);
                continue;
            }
            catch (NumberFormatException e) {
                // empty catch block
            }
        }
    }

    private void saveColumnState() {
        StringBuilder sb = new StringBuilder();
        for (TableColumn tc2 : this.allColumns) {
            if (this.columnModel.isColumnHidden(tc2)) {
                sb.append(-1);
            } else {
                sb.append(tc2.getWidth());
            }
            sb.append(":");
        }
        sb.append("|");
        Enumeration columns = this.columnModel.getColumns();
        while (columns.hasMoreElements()) {
            TableColumn tc2;
            tc2 = (TableColumn)columns.nextElement();
            int index = this.allColumns.indexOf(tc2);
            if (index < 0) continue;
            sb.append(index);
            sb.append(":");
        }
        String str = sb.toString();
        if (this.replacing) {
            FindDialogMemory.getDefault().setResultsColumnWidthsReplacing(str);
        } else if (this.details) {
            FindDialogMemory.getDefault().setResultsColumnWidthsDetails(str);
        } else {
            FindDialogMemory.getDefault().setResultsColumnWidths(str);
        }
    }

    private void setOutlineColumns() {
        if (this.details) {
            this.outlineView.addPropertyColumn("detailsCount", UiUtils.getText("BasicSearchResultsPanel.outline.detailsCount"), UiUtils.getText("BasicSearchResultsPanel.outline.detailsCount.desc"));
        }
        this.outlineView.addPropertyColumn("path", UiUtils.getText("BasicSearchResultsPanel.outline.path"), UiUtils.getText("BasicSearchResultsPanel.outline.path.desc"));
        this.outlineView.addPropertyColumn("size", UiUtils.getText("BasicSearchResultsPanel.outline.size"), UiUtils.getText("BasicSearchResultsPanel.outline.size.desc"));
        this.outlineView.addPropertyColumn("lastModified", UiUtils.getText("BasicSearchResultsPanel.outline.lastModified"), UiUtils.getText("BasicSearchResultsPanel.outline.lastModified.desc"));
        this.outlineView.getOutline().setAutoResizeMode(2);
        this.columnModel = (ETableColumnModel)this.outlineView.getOutline().getColumnModel();
        Enumeration cols = this.columnModel.getColumns();
        while (cols.hasMoreElements()) {
            this.allColumns.add((TableColumn)cols.nextElement());
        }
        this.loadColumnState();
        this.outlineView.setVerticalScrollBarPolicy(22);
    }

    public synchronized void update() {
        this.resultsNode.update();
    }

    private void expandOnlyChilds(Node parent) {
        if (parent.getChildren().getNodesCount(true) == 1) {
            Node onlyChild = parent.getChildren().getNodeAt(0);
            this.outlineView.expandNode(onlyChild);
            this.expandOnlyChilds(onlyChild);
        }
    }

    private Node createNodeForMatchingObject(MatchingObject key) {
        if (key.getDataObject() == null) {
            AbstractNode n = new AbstractNode(Children.LEAF);
            n.setDisplayName("Error");
            return n;
        }
        Node delegate = key.getDataObject().getNodeDelegate();
        Children children = key.getTextDetails() == null || key.getTextDetails().isEmpty() ? Children.LEAF : key.getDetailsChildren(this.replacing);
        MatchingObjectNode mon = new MatchingObjectNode(delegate, children, key, this.replacing);
        assert (this.warnIfNotSynchronized());
        if (!this.closed) {
            this.matchingObjectNodes.add(mon);
        }
        return mon;
    }

    private boolean warnIfNotSynchronized() {
        if (!Thread.holdsLock(this)) {
            Logger.getLogger(ResultsOutlineSupport.class.getName()).log(Level.WARNING, "Thread does not hold lock ResultsOutlineSupport", new Exception());
        }
        return true;
    }

    public synchronized void addMatchingObject(MatchingObject mo) {
        if (this.closed) {
            return;
        }
        for (FileObject fo : this.getRootFiles()) {
            if (fo != mo.getFileObject() && !FileUtil.isParentOf((FileObject)fo, (FileObject)mo.getFileObject())) continue;
            this.addToTreeView(this.rootPathItem, this.getRelativePath(fo, mo.getFileObject()), mo);
            return;
        }
        this.addToTreeView(this.rootPathItem, Collections.singletonList(mo.getFileObject()), mo);
    }

    private synchronized List<FileObject> getRootFiles() {
        if (this.rootFiles == null) {
            this.rootFiles = this.basicComposition.getRootFiles();
        }
        return this.rootFiles;
    }

    private List<FileObject> getRelativePath(FileObject parent, FileObject fo) {
        LinkedList<FileObject> l = new LinkedList<FileObject>();
        for (FileObject part = fo; part != null; part = part.getParent()) {
            l.add(0, part);
            if (part == parent) break;
        }
        return l;
    }

    private void addToTreeView(FolderTreeItem parentItem, List<FileObject> path, MatchingObject matchingObject) {
        for (FolderTreeItem pi : parentItem.getChildren()) {
            if (pi.isPathLeaf() || !pi.getFolder().getPrimaryFile().equals((Object)path.get(0))) continue;
            this.addToTreeView(pi, path.subList(1, path.size()), matchingObject);
            return;
        }
        this.createInTreeView(parentItem, path, matchingObject);
    }

    private void createInTreeView(FolderTreeItem parentItem, List<FileObject> path, MatchingObject matchingObject) {
        if (path.size() == 1) {
            for (FolderTreeItem pi : parentItem.getChildren()) {
                if (!pi.isPathLeaf() || !pi.getMatchingObject().equals(matchingObject)) continue;
                return;
            }
            parentItem.addChild(new FolderTreeItem(matchingObject, parentItem));
        } else {
            try {
                FolderTreeItem newChild = new FolderTreeItem(DataObject.find((FileObject)path.get(0)), parentItem);
                parentItem.addChild(newChild);
                this.createInTreeView(newChild, path.subList(1, path.size()), matchingObject);
            }
            catch (DataObjectNotFoundException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
    }

    public static void toggleParentSelected(Node parent) {
        if (parent == null) {
            return;
        }
        Selectable parentSelectable = (Selectable)parent.getLookup().lookup(Selectable.class);
        if (parentSelectable != null) {
            Node[] children = parent.getChildren().getNodes(true);
            boolean selectedChildFound = false;
            for (Node child : children) {
                Selectable childSelectable = (Selectable)child.getLookup().lookup(Selectable.class);
                if (childSelectable == null || !childSelectable.isSelected()) continue;
                selectedChildFound = true;
                break;
            }
            if (parentSelectable.isSelected() != selectedChildFound) {
                parentSelectable.setSelected(selectedChildFound);
            }
        }
    }

    public synchronized void setFolderTreeMode() {
        this.resultsNode.setFolderTreeMode();
    }

    public synchronized void setFlatMode() {
        this.resultsNode.setFlatMode();
    }

    public OutlineView getOutlineView() {
        return this.outlineView;
    }

    public Node getRootNode() {
        return this.invisibleRoot;
    }

    public Node getResultsNode() {
        return this.resultsNode;
    }

    public void setResultsNodeText(String text) {
        this.resultsNode.setHtmlAndRawDisplayName(text);
    }

    private class ColumnsListener
    implements TableColumnModelListener {
        private ColumnsListener() {
        }

        @Override
        public void columnAdded(TableColumnModelEvent e) {
            ResultsOutlineSupport.this.saveColumnState();
        }

        @Override
        public void columnRemoved(TableColumnModelEvent e) {
            ResultsOutlineSupport.this.saveColumnState();
        }

        @Override
        public void columnMoved(TableColumnModelEvent e) {
            ResultsOutlineSupport.this.saveColumnState();
        }

        @Override
        public void columnMarginChanged(ChangeEvent e) {
            ResultsOutlineSupport.this.saveColumnState();
        }

        @Override
        public void columnSelectionChanged(ListSelectionEvent e) {
        }
    }

    private class FolderTreeChildren
    extends Children.Keys<FolderTreeItem> {
        private FolderTreeItem item;

        public FolderTreeChildren(FolderTreeItem pathItem) {
            this.item = null;
            this.item = pathItem;
            pathItem.addPropertyChangeListener(new PropertyChangeListener(ResultsOutlineSupport.this){
                final /* synthetic */ ResultsOutlineSupport val$this$0;

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if (evt.getPropertyName().equals("children")) {
                        FolderTreeChildren.this.update();
                    }
                }
            });
        }

        protected void addNotify() {
            this.update();
        }

        void update() {
            this.setKeys(this.item.getChildren());
        }

        protected Node[] createNodes(FolderTreeItem key) {
            Node n = key.isPathLeaf() ? ResultsOutlineSupport.this.createNodeForMatchingObject(key.getMatchingObject()) : new FolderTreeNode(key);
            return new Node[]{n};
        }

    }

    private class FolderTreeNode
    extends FilterNode
    implements Removable {
        public FolderTreeNode(FolderTreeItem pathItem) {
            super(pathItem.getFolder().getNodeDelegate(), (Children)new FolderTreeChildren(pathItem), Lookups.fixed((Object[])new Object[]{pathItem, new ReplaceCheckableNode(pathItem, ResultsOutlineSupport.this.replacing), pathItem.getFolder().getPrimaryFile()}));
            pathItem.addPropertyChangeListener(new PropertyChangeListener(ResultsOutlineSupport.this){
                final /* synthetic */ ResultsOutlineSupport val$this$0;

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    FolderTreeNode.this.fireIconChange();
                    String prop = evt.getPropertyName();
                    if (prop.equals("selected")) {
                        ResultsOutlineSupport.toggleParentSelected(FolderTreeNode.this.getParentNode());
                    } else if (prop.equals("children")) {
                        ResultsOutlineSupport.toggleParentSelected((Node)FolderTreeNode.this);
                    }
                }
            });
            if (!pathItem.isPathLeaf()) {
                this.setShortDescription(pathItem.getFolder().getPrimaryFile().getPath());
            }
        }

        public PasteType[] getPasteTypes(Transferable t) {
            return new PasteType[0];
        }

        public PasteType getDropType(Transferable t, int action, int index) {
            return null;
        }

        public Transferable clipboardCopy() throws IOException {
            return UiUtils.DISABLE_TRANSFER;
        }

        public Transferable clipboardCut() throws IOException {
            return UiUtils.DISABLE_TRANSFER;
        }

        @Override
        public void remove() {
            FolderTreeItem folder = (FolderTreeItem)this.getLookup().lookup(FolderTreeItem.class);
            folder.remove();
        }

        public boolean canDestroy() {
            return true;
        }

        public void destroy() throws IOException {
            this.remove();
        }

        public Transferable drag() throws IOException {
            return UiUtils.DISABLE_TRANSFER;
        }

        public Action[] getActions(boolean context) {
            return new Action[]{SystemAction.get(HideResultAction.class)};
        }

    }

    static class FolderTreeItem
    implements Selectable {
        static final String PROP_SELECTED = "selected";
        static final String PROP_CHILDREN = "children";
        private FolderTreeItem parent;
        private DataObject folder = null;
        private MatchingObject matchingObject = null;
        private List<FolderTreeItem> children = new LinkedList<FolderTreeItem>();
        private boolean selected = true;
        PropertyChangeSupport changeSupport;

        public FolderTreeItem() {
            this.changeSupport = new PropertyChangeSupport(this);
            this.parent = null;
        }

        public FolderTreeItem(MatchingObject matchingObject, FolderTreeItem parent) {
            this.changeSupport = new PropertyChangeSupport(this);
            this.parent = parent;
            this.matchingObject = matchingObject;
            matchingObject.addPropertyChangeListener(new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    String pn = evt.getPropertyName();
                    if (pn.equals("selected")) {
                        FolderTreeItem.this.setSelected(FolderTreeItem.this.matchingObject.isSelected());
                    } else if (pn.equals("removed")) {
                        FolderTreeItem.this.remove();
                    }
                }
            });
        }

        public FolderTreeItem(DataObject file, FolderTreeItem parent) {
            this.changeSupport = new PropertyChangeSupport(this);
            this.parent = parent;
            this.folder = file;
        }

        synchronized void addChild(FolderTreeItem pathItem) {
            this.children.add(pathItem);
            this.firePropertyChange("children", null, null);
        }

        public DataObject getFolder() {
            return this.folder;
        }

        public synchronized List<FolderTreeItem> getChildren() {
            return new ArrayList<FolderTreeItem>(this.children);
        }

        public synchronized void remove() {
            for (FolderTreeItem fti : new ArrayList<FolderTreeItem>(this.children)) {
                if (fti.isPathLeaf()) {
                    fti.getMatchingObject().remove();
                    continue;
                }
                fti.remove();
            }
            if (this.parent != null) {
                this.parent.removeChild(this);
            }
        }

        private synchronized void removeChild(FolderTreeItem child) {
            boolean result = this.children.remove(child);
            if (result) {
                child.parent = null;
            }
            if (this.children.isEmpty() && this.parent != null) {
                this.remove();
            } else {
                this.firePropertyChange("children", null, null);
            }
        }

        public MatchingObject getMatchingObject() {
            return this.matchingObject;
        }

        public boolean isPathLeaf() {
            return this.matchingObject != null;
        }

        @Override
        public boolean isSelected() {
            return this.selected;
        }

        @Override
        public void setSelected(boolean selected) {
            if (selected == this.selected) {
                return;
            }
            this.selected = selected;
            this.firePropertyChange("selected", null, null);
        }

        @Override
        public void setSelectedRecursively(boolean selected) {
            if (this.selected == selected) {
                return;
            }
            if (this.isPathLeaf()) {
                this.getMatchingObject().setSelectedRecursively(selected);
            } else {
                for (FolderTreeItem child : this.children) {
                    child.setSelectedRecursively(selected);
                }
            }
            this.setSelected(selected);
        }

        public synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
            this.changeSupport.addPropertyChangeListener(listener);
        }

        public synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
            this.changeSupport.removePropertyChangeListener(listener);
        }

        public void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
            this.changeSupport.firePropertyChange(propertyName, oldValue, newValue);
        }

    }

    private class FlatChildren
    extends Children.Keys<MatchingObject> {
        public FlatChildren() {
            ResultsOutlineSupport.this.resultModel.addPropertyChangeListener("resultsEdit", new PropertyChangeListener(ResultsOutlineSupport.this){
                final /* synthetic */ ResultsOutlineSupport val$this$0;

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    FlatChildren.this.update();
                }
            });
        }

        protected Node[] createNodes(MatchingObject key) {
            return new Node[]{ResultsOutlineSupport.this.createNodeForMatchingObject(key)};
        }

        private synchronized void update() {
            this.setKeys(ResultsOutlineSupport.this.resultModel.getMatchingObjects());
        }

    }

    private class ResultsNode
    extends AbstractNode {
        private FlatChildren flatChildren;
        private FolderTreeChildren folderTreeChildren;
        private String htmlDisplayName;

        public ResultsNode(ResultModel model) {
            super((Children)new FlatChildren());
            this.htmlDisplayName = null;
            this.flatChildren = (FlatChildren)this.getChildren();
            this.folderTreeChildren = new FolderTreeChildren(ResultsOutlineSupport.this.rootPathItem);
        }

        void update() {
            this.flatChildren.update();
        }

        void setFlatMode() {
            this.setChildren((Children)this.flatChildren);
            this.expand();
        }

        void setFolderTreeMode() {
            this.setChildren((Children)this.folderTreeChildren);
            this.expand();
        }

        private void expand() {
            ResultsOutlineSupport.this.outlineView.expandNode((Node)ResultsOutlineSupport.this.resultsNode);
        }

        public Image getIcon(int type) {
            return ImageUtilities.loadImage((String)"org/netbeans/modules/search/res/context.gif");
        }

        public Image getOpenedIcon(int type) {
            return this.getIcon(type);
        }

        public Action[] getActions(boolean context) {
            return new Action[0];
        }

        protected void createPasteTypes(Transferable t, List<PasteType> s) {
        }

        public void setHtmlAndRawDisplayName(String htmlName) {
            this.htmlDisplayName = htmlName == null ? null : "<html>" + htmlName + "</html>";
            String stripped = htmlName == null ? null : htmlName.replaceAll("<b>", "").replaceAll("</b>", "");
            this.setDisplayName(stripped);
        }

        public String getHtmlDisplayName() {
            return this.htmlDisplayName;
        }
    }

    private class ExpandingTreeExpansionListener
    implements TreeExpansionListener {
        private ExpandingTreeExpansionListener() {
        }

        @Override
        public void treeExpanded(TreeExpansionEvent event) {
            Object lpc = event.getPath().getLastPathComponent();
            Node node = Visualizer.findNode((Object)lpc);
            if (node != null) {
                ResultsOutlineSupport.this.expandOnlyChilds(node);
            }
        }

        @Override
        public void treeCollapsed(TreeExpansionEvent event) {
        }
    }

}

