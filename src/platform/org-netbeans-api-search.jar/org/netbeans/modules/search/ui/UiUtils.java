/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.modules.search.ui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.CharConversionException;
import java.io.File;
import java.io.IOException;
import javax.swing.AbstractButton;
import javax.swing.JLabel;
import javax.swing.UIManager;
import org.netbeans.modules.search.BasicSearchProvider;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;
import org.openide.xml.XMLUtil;

public class UiUtils {
    private static Color ERROR_COLOR = null;
    public static final String HTML_LINK_PREFIX = "<html><u><a href=\"#\">";
    public static final String HTML_LINK_SUFFIX = "</a></u></html>";
    public static final Transferable DISABLE_TRANSFER = new Transferable(){
        private final DataFlavor[] NO_FLAVOR = new DataFlavor[0];

        @Override
        public DataFlavor[] getTransferDataFlavors() {
            return this.NO_FLAVOR;
        }

        @Override
        public boolean isDataFlavorSupported(DataFlavor flavor) {
            return false;
        }

        @Override
        public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
            return null;
        }
    };

    public static Color getErrorTextColor() {
        assert (EventQueue.isDispatchThread());
        if (ERROR_COLOR == null && (UiUtils.ERROR_COLOR = UIManager.getDefaults().getColor("TextField.errorForeground")) == null) {
            ERROR_COLOR = Color.RED;
        }
        return ERROR_COLOR;
    }

    public static String getText(String bundleKey) {
        return NbBundle.getMessage(BasicSearchProvider.class, (String)bundleKey);
    }

    public static String getHtmlLink(String key) {
        return "<html><u><a href=\"#\">" + UiUtils.getText(key) + "</a></u></html>";
    }

    public static void lclz(AbstractButton obj, String key) {
        Mnemonics.setLocalizedText((AbstractButton)obj, (String)UiUtils.getText(key));
    }

    public static void lclz(JLabel obj, String key) {
        Mnemonics.setLocalizedText((JLabel)obj, (String)UiUtils.getText(key));
    }

    public static String escapeHtml(String s) {
        if (s == null) {
            return null;
        }
        try {
            return XMLUtil.toElementContent((String)s);
        }
        catch (CharConversionException cce) {
            return s;
        }
    }

    public static String getFileNamePatternsExample(boolean regexp) {
        if (regexp) {
            String separator = "\\".equals(File.separator) ? "\\\\" : File.separator;
            return NbBundle.getMessage(BasicSearchProvider.class, (String)"BasicSearchForm.cboxFileNamePattern.example.re", (Object)separator);
        }
        return NbBundle.getMessage(BasicSearchProvider.class, (String)"BasicSearchForm.cboxFileNamePattern.example");
    }

}

