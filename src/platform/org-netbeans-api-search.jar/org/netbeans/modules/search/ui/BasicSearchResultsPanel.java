/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.view.OutlineView
 *  org.openide.nodes.Node
 */
package org.netbeans.modules.search.ui;

import java.awt.Component;
import javax.swing.JPanel;
import org.netbeans.modules.search.BasicComposition;
import org.netbeans.modules.search.ResultModel;
import org.netbeans.modules.search.ui.BasicAbstractResultsPanel;
import org.netbeans.modules.search.ui.ResultsOutlineSupport;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;

public class BasicSearchResultsPanel
extends BasicAbstractResultsPanel {
    public BasicSearchResultsPanel(ResultModel resultModel, BasicComposition composition, boolean details, Node infoNode) {
        super(resultModel, composition, details, new ResultsOutlineSupport(false, details, resultModel, composition, infoNode));
        this.init();
    }

    private void init() {
        this.getContentPanel().add((Component)this.resultsOutlineSupport.getOutlineView());
    }
}

