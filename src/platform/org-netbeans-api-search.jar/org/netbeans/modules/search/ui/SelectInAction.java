/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Actions
 *  org.openide.nodes.Node
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.NodeAction
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Popup
 */
package org.netbeans.modules.search.ui;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import org.netbeans.modules.search.ui.Bundle;
import org.openide.awt.Actions;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.actions.NodeAction;
import org.openide.util.actions.Presenter;

public class SelectInAction
extends NodeAction
implements Presenter.Popup {
    private static final String SEL_IN_PROJECTS = "org.netbeans.modules.project.ui.SelectInProjects";
    private static final String SEL_IN_FILES = "org.netbeans.modules.project.ui.SelectInFiles";
    private static final String SEL_IN_FAVS = "org.netbeans.modules.favorites.Select";

    protected void performAction(Node[] activatedNodes) {
    }

    protected boolean enable(Node[] activatedNodes) {
        return activatedNodes.length == 1 && (this.getAction("org.netbeans.modules.project.ui.SelectInFiles") != null || this.getAction("org.netbeans.modules.project.ui.SelectInProjects") != null || this.getAction("org.netbeans.modules.favorites.Select") != null);
    }

    public String getName() {
        return Bundle.SelectInAction_name();
    }

    public HelpCtx getHelpCtx() {
        return null;
    }

    public JMenuItem getPopupPresenter() {
        JMenu submenu = new JMenu((Action)((Object)this));
        this.addActionItem(submenu, "org.netbeans.modules.project.ui.SelectInProjects", Bundle.SelectIn_Projects());
        this.addActionItem(submenu, "org.netbeans.modules.project.ui.SelectInFiles", Bundle.SelectIn_Files());
        this.addActionItem(submenu, "org.netbeans.modules.favorites.Select", Bundle.SelectIn_Favorites());
        return submenu;
    }

    private void addActionItem(JMenu parent, String action, String displayName) {
        Action a = this.getAction(action);
        if (a != null) {
            JMenuItem item = new JMenuItem(a);
            item.setText(displayName);
            item.setIcon(null);
            parent.add(item);
        }
    }

    private Action getAction(String name) {
        return Actions.forID((String)"Window/SelectDocumentNode", (String)name);
    }
}

