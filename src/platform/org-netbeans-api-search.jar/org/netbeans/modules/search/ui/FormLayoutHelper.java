/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.search.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;

public class FormLayoutHelper {
    public static final Column DEFAULT_COLUMN = new DefaultColumn();
    public static final Column EAGER_COLUMN = new EagerColumn();
    public static final Column EXPANDING_COLUMN = new ExpandingColumn();
    JPanel panel;
    GroupLayout layout;
    Column[] columns;
    GroupLayout.Group[] columnGroups;
    private GroupLayout.Group horizontalGroup;
    private GroupLayout.Group verticalGroup;

    public /* varargs */ FormLayoutHelper(JPanel panel, Column ... columns) {
        this.panel = panel;
        this.layout = new GroupLayout(panel);
        panel.setLayout(this.layout);
        this.horizontalGroup = this.layout.createSequentialGroup();
        this.verticalGroup = this.layout.createSequentialGroup();
        this.columns = columns;
        this.columnGroups = new GroupLayout.Group[columns.length];
        for (int i = 0; i < columns.length; ++i) {
            GroupLayout.Group columnGroup;
            this.columnGroups[i] = columnGroup = columns[i].createParallelGroup(this.layout);
            this.horizontalGroup.addGroup(columnGroup);
        }
        this.layout.setHorizontalGroup(this.horizontalGroup);
        this.layout.setVerticalGroup(this.verticalGroup);
    }

    public void setAllGaps(boolean gaps) {
        this.setInlineGaps(gaps);
        this.setContainerGaps(gaps);
    }

    public void setInlineGaps(boolean gaps) {
        this.layout.setAutoCreateGaps(gaps);
    }

    public void setContainerGaps(boolean gaps) {
        this.layout.setAutoCreateContainerGaps(gaps);
    }

    public GroupLayout getLayout() {
        return this.layout;
    }

    public /* varargs */ void addRow(int min, int pref, int max, JComponent ... component) {
        GroupLayout.ParallelGroup newRowGroup = this.layout.createParallelGroup(GroupLayout.Alignment.BASELINE);
        for (int i = 0; i < component.length && i < this.columns.length; ++i) {
            JComponent cmp = component[i];
            newRowGroup.addComponent(cmp, min, pref, max);
            this.columns[i].addComponent(cmp, this.columnGroups[i]);
        }
        this.verticalGroup.addGroup(newRowGroup);
    }

    public /* varargs */ void addRow(JComponent ... component) {
        this.addRow(-1, -1, -1, component);
    }

    private static class EagerColumn
    extends DefaultColumn {
        private EagerColumn() {
            super();
        }

        @Override
        protected void addComponent(JComponent component, GroupLayout.Group parallelColumnGroup) {
            parallelColumnGroup.addComponent(component, -1, -1, 32767);
        }
    }

    private static class ExpandingColumn
    extends Column {
        private ExpandingColumn() {
        }

        @Override
        protected void addComponent(JComponent component, GroupLayout.Group parallelColumnGroup) {
            parallelColumnGroup.addComponent(component, -1, -1, 32767);
        }

        @Override
        protected GroupLayout.Group createParallelGroup(GroupLayout layout) {
            return layout.createParallelGroup(GroupLayout.Alignment.LEADING, false);
        }
    }

    private static class DefaultColumn
    extends Column {
        private DefaultColumn() {
        }

        @Override
        protected void addComponent(JComponent component, GroupLayout.Group parallelColumnGroup) {
            parallelColumnGroup.addComponent(component);
        }

        @Override
        protected GroupLayout.Group createParallelGroup(GroupLayout layout) {
            return layout.createParallelGroup();
        }
    }

    public static abstract class Column {
        protected abstract void addComponent(JComponent var1, GroupLayout.Group var2);

        protected abstract GroupLayout.Group createParallelGroup(GroupLayout var1);
    }

}

