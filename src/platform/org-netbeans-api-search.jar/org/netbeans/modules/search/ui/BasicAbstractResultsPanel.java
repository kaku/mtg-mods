/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.view.OutlineView
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeAdapter
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeMemberEvent
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.search.ui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.util.List;
import java.util.ResourceBundle;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JScrollBar;
import javax.swing.JToggleButton;
import javax.swing.UIManager;
import org.netbeans.modules.search.BasicComposition;
import org.netbeans.modules.search.BasicSearchCriteria;
import org.netbeans.modules.search.FindDialogMemory;
import org.netbeans.modules.search.Manager;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.PrintDetailsTask;
import org.netbeans.modules.search.ResultModel;
import org.netbeans.modules.search.ResultView;
import org.netbeans.modules.search.TextDetail;
import org.netbeans.modules.search.ui.AbstractSearchResultsPanel;
import org.netbeans.modules.search.ui.ResultsOutlineSupport;
import org.netbeans.modules.search.ui.UiUtils;
import org.netbeans.spi.search.provider.SearchComposition;
import org.netbeans.spi.search.provider.SearchProvider;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeAdapter;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public abstract class BasicAbstractResultsPanel
extends AbstractSearchResultsPanel
implements PropertyChangeListener {
    private static final String SHOW_DETAILS_ICON = "org/netbeans/modules/search/res/search.gif";
    private static final String FOLDER_VIEW_ICON = "org/netbeans/modules/search/res/logical_view.png";
    private static final String FLAT_VIEW_ICON = "org/netbeans/modules/search/res/file_view.png";
    private static final String MODE_FLAT = "flat";
    private static final String MODE_TREE = "tree";
    protected ResultModel resultModel;
    protected JToggleButton btnTreeView;
    protected JToggleButton btnFlatView;
    protected JButton showDetailsButton;
    protected boolean details;
    protected BasicComposition composition;
    protected final ResultsOutlineSupport resultsOutlineSupport;
    private NodeListener resultsNodeAdditionListener;
    private volatile boolean finished = false;
    protected static final boolean isMacLaf = "Aqua".equals(UIManager.getLookAndFeel().getID());
    protected static final Color macBackground = UIManager.getColor("NbExplorerView.background");

    public BasicAbstractResultsPanel(ResultModel resultModel, BasicComposition composition, boolean details, ResultsOutlineSupport resultsOutlineSupport) {
        super(composition, composition.getSearchProviderPresenter());
        this.composition = composition;
        this.details = details;
        this.resultModel = resultModel;
        this.resultsOutlineSupport = resultsOutlineSupport;
        this.getExplorerManager().setRootContext(resultsOutlineSupport.getRootNode());
        this.initButtons();
        this.initResultNodeAdditionListener();
        if ("tree".equals(FindDialogMemory.getDefault().getResultsViewMode())) {
            resultsOutlineSupport.setFolderTreeMode();
        }
        this.setRootDisplayName(NbBundle.getMessage(ResultView.class, (String)"TEXT_SEARCHING___"));
        this.initAccessibility();
        this.resultModel.addPropertyChangeListener("resultsEdit", this);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (this.finished) {
            this.setFinalRootNodeText();
        } else {
            this.updateRootNodeText();
        }
    }

    public void update() {
        if (this.details && this.btnExpand.isVisible() && !this.btnExpand.isEnabled()) {
            this.btnExpand.setEnabled(this.resultModel.size() > 0);
        }
        EventQueue.invokeLater(new Runnable(){

            @Override
            public void run() {
                BasicAbstractResultsPanel.this.updateShiftButtons();
            }
        });
        this.resultsOutlineSupport.update();
    }

    protected void initButtons() {
        FindDialogMemory memory = FindDialogMemory.getDefault();
        this.btnTreeView = new JToggleButton();
        this.btnTreeView.setEnabled(true);
        this.btnTreeView.setIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/search/res/logical_view.png", (boolean)true));
        this.btnTreeView.setToolTipText(UiUtils.getText("TEXT_BUTTON_TREE_VIEW"));
        this.btnTreeView.setSelected("tree".equals(memory.getResultsViewMode()));
        this.btnTreeView.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                BasicAbstractResultsPanel.this.toggleView(!BasicAbstractResultsPanel.this.btnTreeView.isSelected());
            }
        });
        this.btnFlatView = new JToggleButton();
        this.btnFlatView.setEnabled(true);
        this.btnFlatView.setIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/search/res/file_view.png", (boolean)true));
        this.btnFlatView.setToolTipText(UiUtils.getText("TEXT_BUTTON_FLAT_VIEW"));
        this.btnFlatView.setSelected(!this.btnTreeView.isSelected());
        this.btnFlatView.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                BasicAbstractResultsPanel.this.toggleView(BasicAbstractResultsPanel.this.btnFlatView.isSelected());
            }
        });
        this.addButton(this.btnTreeView);
        this.addButton(this.btnFlatView);
        if (!this.details) {
            this.btnPrev.setVisible(false);
            this.btnNext.setVisible(false);
            this.btnExpand.setVisible(false);
            return;
        }
        this.btnExpand.setSelected(true);
        this.btnExpand.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                BasicAbstractResultsPanel.this.toggleExpandNodeChildren(BasicAbstractResultsPanel.this.btnExpand.isSelected());
            }
        });
        this.showDetailsButton = new JButton();
        this.showDetailsButton.setEnabled(false);
        this.showDetailsButton.setIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/search/res/search.gif", (boolean)true));
        this.showDetailsButton.setToolTipText(UiUtils.getText("TEXT_BUTTON_FILL"));
        this.showDetailsButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                BasicAbstractResultsPanel.this.fillOutput();
            }
        });
        this.showDetailsButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ResultView.class, (String)"ACS_TEXT_BUTTON_FILL"));
        this.addButton(this.showDetailsButton);
    }

    private void toggleView(boolean flat) {
        FindDialogMemory memory = FindDialogMemory.getDefault();
        if (flat) {
            this.resultsOutlineSupport.setFlatMode();
            memory.setResultsViewMode("flat");
        } else {
            this.resultsOutlineSupport.setFolderTreeMode();
            memory.setResultsViewMode("tree");
        }
        this.btnTreeView.setSelected(!flat);
        this.btnFlatView.setSelected(flat);
        try {
            this.getExplorerManager().setSelectedNodes(new Node[]{this.resultsOutlineSupport.getResultsNode()});
        }
        catch (PropertyVetoException ex) {
            // empty catch block
        }
    }

    private void initAccessibility() {
        ResourceBundle bundle = NbBundle.getBundle(ResultView.class);
        OutlineView outlineView = this.resultsOutlineSupport.getOutlineView();
        AccessibleContext accessCtx = outlineView.getHorizontalScrollBar().getAccessibleContext();
        accessCtx.setAccessibleName(bundle.getString("ACSN_HorizontalScrollbar"));
        accessCtx = outlineView.getVerticalScrollBar().getAccessibleContext();
        accessCtx.setAccessibleName(bundle.getString("ACSN_VerticalScrollbar"));
        accessCtx = outlineView.getAccessibleContext();
        accessCtx.setAccessibleName(bundle.getString("ACSN_ResultTree"));
        accessCtx.setAccessibleDescription(bundle.getString("ACSD_ResultTree"));
    }

    private void toggleExpandNodeChildren(boolean expand) {
        Node resultsNode = this.resultsOutlineSupport.getResultsNode();
        for (Node n : resultsNode.getChildren().getNodes()) {
            this.toggleExpand(n, expand);
        }
    }

    @Override
    public void searchFinished() {
        super.searchFinished();
        this.finished = true;
        if (this.details && this.resultModel.size() > 0 && this.showDetailsButton != null) {
            this.showDetailsButton.setEnabled(true);
        }
        this.setFinalRootNodeText();
    }

    public void fillOutput() {
        Manager.getInstance().schedulePrintTask(new PrintDetailsTask(this.resultModel.getMatchingObjects(), this.composition.getBasicSearchCriteria()));
    }

    public void addMatchingObject(MatchingObject mo) {
        this.resultsOutlineSupport.addMatchingObject(mo);
        this.updateRootNodeText();
        this.afterMatchingNodeAdded();
    }

    @Override
    public final OutlineView getOutlineView() {
        return this.resultsOutlineSupport.getOutlineView();
    }

    protected final void setFinalRootNodeText() {
        String baseMsg;
        int resultSize = this.resultModel.size();
        if (this.resultModel.wasLimitReached()) {
            this.setRootDisplayName(NbBundle.getMessage(ResultView.class, (String)"TEXT_MSG_FOUND_X_NODES_LIMIT", (Object)resultSize, (Object)this.resultModel.getTotalDetailsCount()) + ' ' + this.resultModel.getLimitDisplayName());
            return;
        }
        if (resultSize == 0) {
            baseMsg = NbBundle.getMessage(ResultView.class, (String)"TEXT_MSG_NO_NODE_FOUND");
        } else {
            String bundleKey;
            Object[] args;
            if (this.resultModel.isSearchAndReplace()) {
                bundleKey = "TEXT_MSG_FOUND_X_NODES_REPLACE";
                args = new Object[4];
            } else if (this.resultModel.canHaveDetails()) {
                bundleKey = "TEXT_MSG_FOUND_X_NODES_FULLTEXT";
                args = new Object[3];
            } else {
                bundleKey = "TEXT_MSG_FOUND_X_NODES";
                args = new Object[]{new Integer(this.resultModel.size())};
            }
            if (args.length > 1) {
                args[1] = new Integer(this.resultModel.getTotalDetailsCount());
            }
            if (args.length > 2) {
                BasicSearchCriteria bsc = this.composition.getBasicSearchCriteria();
                args[2] = UiUtils.escapeHtml(bsc.getTextPatternExpr());
                if (args.length > 3) {
                    args[3] = UiUtils.escapeHtml(bsc.getReplaceExpr());
                }
            }
            baseMsg = NbBundle.getMessage(ResultView.class, (String)bundleKey, (Object[])args);
        }
        String exMsg = this.resultModel.getExceptionMsg();
        String msg = exMsg == null ? baseMsg : baseMsg + " (" + exMsg + ")";
        this.setRootDisplayName(msg);
    }

    private void setRootDisplayName(String displayName) {
        this.resultsOutlineSupport.setResultsNodeText(displayName);
    }

    protected void updateRootNodeText() {
        Integer objectsCount = this.resultModel.size();
        if (this.details) {
            Integer detailsCount = this.resultModel.getTotalDetailsCount();
            this.setRootDisplayName(NbBundle.getMessage(ResultView.class, (String)"TXT_RootSearchedNodesFulltext", (Object)objectsCount, (Object)detailsCount));
        } else {
            this.setRootDisplayName(NbBundle.getMessage(ResultView.class, (String)"TXT_RootSearchedNodes", (Object)objectsCount));
        }
    }

    private void initResultNodeAdditionListener() {
        this.resultsNodeAdditionListener = new NodeAdapter(){

            public void childrenAdded(NodeMemberEvent ev) {
                if (BasicAbstractResultsPanel.this.btnExpand != null) {
                    for (final Node n : ev.getDelta()) {
                        if (BasicAbstractResultsPanel.this.btnExpand.isSelected()) {
                            EventQueue.invokeLater(new Runnable(){

                                @Override
                                public void run() {
                                    BasicAbstractResultsPanel.this.toggleExpand(n, true);
                                }
                            });
                        }
                        BasicAbstractResultsPanel.this.addChildAdditionListener(n);
                    }
                }
            }

            public void childrenRemoved(NodeMemberEvent ev) {
                if (BasicAbstractResultsPanel.this.btnExpand != null) {
                    for (Node removedChild : ev.getDelta()) {
                        BasicAbstractResultsPanel.this.removeChildAdditionListener(removedChild);
                    }
                }
            }

        };
        this.resultsOutlineSupport.getResultsNode().getChildren().getNodes(true);
        this.resultsOutlineSupport.getResultsNode().addNodeListener(this.resultsNodeAdditionListener);
    }

    private void addChildAdditionListener(Node addedNode) {
        for (Node n : addedNode.getChildren().getNodes(true)) {
            this.addChildAdditionListener(n);
        }
        addedNode.addNodeListener(this.resultsNodeAdditionListener);
    }

    private void removeChildAdditionListener(Node removedNode) {
        for (Node n : removedNode.getChildren().getNodes(true)) {
            this.removeChildAdditionListener(n);
        }
        removedNode.removeNodeListener(this.resultsNodeAdditionListener);
    }

    @Override
    public boolean requestFocusInWindow() {
        return this.getOutlineView().requestFocusInWindow();
    }

    @Override
    protected boolean isDetailNode(Node n) {
        return n.getLookup().lookup(TextDetail.class) != null;
    }

    @Override
    protected void onDetailShift(Node next) {
        TextDetail textDetail = (TextDetail)next.getLookup().lookup(TextDetail.class);
        if (textDetail != null) {
            textDetail.showDetail(2);
        }
    }

    public void closed() {
        this.resultsOutlineSupport.closed();
    }

    protected boolean isFinished() {
        return this.finished;
    }

}

