/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 */
package org.netbeans.modules.search.ui;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import org.openide.filesystems.FileObject;
import org.openide.nodes.Node;

public class FileObjectPropertySet
extends Node.PropertySet {
    private FileObject fileObject;
    Node.Property[] properties;

    public FileObjectPropertySet(FileObject fileObject) {
        this.fileObject = fileObject;
        this.properties = new Node.Property[]{new PathProperty(), new SizeProperty(), new LastModifiedProperty()};
    }

    public Node.Property<?>[] getProperties() {
        return this.properties;
    }

    private class PathProperty
    extends Node.Property<String> {
        public PathProperty() {
            super(String.class);
        }

        public boolean canRead() {
            return true;
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            return FileObjectPropertySet.this.fileObject.getPath();
        }

        public boolean canWrite() {
            return false;
        }

        public void setValue(String val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            throw new UnsupportedOperationException();
        }

        public String getName() {
            return "path";
        }
    }

    private class LastModifiedProperty
    extends Node.Property<Date> {
        public LastModifiedProperty() {
            super(Date.class);
        }

        public boolean canRead() {
            return true;
        }

        public Date getValue() throws IllegalAccessException, InvocationTargetException {
            return FileObjectPropertySet.this.fileObject.lastModified();
        }

        public boolean canWrite() {
            return false;
        }

        public void setValue(Date val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            throw new UnsupportedOperationException();
        }

        public String getName() {
            return "lastModified";
        }
    }

    private class SizeProperty
    extends Node.Property<Long> {
        public SizeProperty() {
            super(Long.class);
        }

        public boolean canRead() {
            return true;
        }

        public Long getValue() throws IllegalAccessException, InvocationTargetException {
            return FileObjectPropertySet.this.fileObject.getSize();
        }

        public boolean canWrite() {
            return false;
        }

        public void setValue(Long val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            throw new UnsupportedOperationException();
        }

        public String getName() {
            return "size";
        }
    }

}

