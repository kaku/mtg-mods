/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.view.CheckableNode
 */
package org.netbeans.modules.search.ui;

import org.netbeans.modules.search.Selectable;
import org.openide.explorer.view.CheckableNode;

public class ReplaceCheckableNode
implements CheckableNode {
    private final boolean replacing;
    private Selectable model;

    public ReplaceCheckableNode(Selectable model, boolean replacing) {
        this.replacing = replacing;
        this.model = model;
    }

    public boolean isCheckable() {
        return this.replacing;
    }

    public boolean isCheckEnabled() {
        return true;
    }

    public Boolean isSelected() {
        return this.model.isSelected();
    }

    public void setSelected(Boolean selected) {
        this.model.setSelectedRecursively(selected);
    }
}

