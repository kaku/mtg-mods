/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Actions
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeMemberEvent
 *  org.openide.nodes.NodeReorderEvent
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.datatransfer.ExClipboard
 *  org.openide.util.datatransfer.PasteType
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.search.ui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.UIManager;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.Removable;
import org.netbeans.modules.search.Selectable;
import org.netbeans.modules.search.ui.FileObjectPropertySet;
import org.netbeans.modules.search.ui.HideResultAction;
import org.netbeans.modules.search.ui.MoreAction;
import org.netbeans.modules.search.ui.OpenMatchingObjectsAction;
import org.netbeans.modules.search.ui.RefreshAction;
import org.netbeans.modules.search.ui.ReplaceCheckableNode;
import org.netbeans.modules.search.ui.ResultsOutlineSupport;
import org.netbeans.modules.search.ui.SelectInAction;
import org.netbeans.modules.search.ui.UiUtils;
import org.openide.awt.Actions;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeReorderEvent;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.ExClipboard;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.Lookups;

public class MatchingObjectNode
extends AbstractNode
implements Removable {
    private static final String INVALID_ICON = "org/netbeans/modules/search/res/invalid.png";
    private static final String WARNING_ICON = "org/netbeans/modules/search/res/warning.gif";
    private static RequestProcessor RP = new RequestProcessor("MatchingObjectNode");
    private MatchingObject matchingObject;
    private Node original;
    private OrigNodeListener origNodeListener;
    private boolean valid = true;
    private PropertyChangeListener validityListener;
    private PropertyChangeListener selectionListener;
    private final boolean replacing;
    Node.PropertySet[] propertySets;

    public MatchingObjectNode(Node original, Children children, MatchingObject matchingObject, boolean replacing) {
        this(original, children, matchingObject, new ReplaceCheckableNode(matchingObject, replacing));
    }

    private MatchingObjectNode(Node original, Children children, MatchingObject matchingObject, ReplaceCheckableNode checkableNode) {
        super(children, Lookups.fixed((Object[])new Object[]{matchingObject, checkableNode, matchingObject.getFileObject(), matchingObject.getDataObject()}));
        this.replacing = checkableNode.isCheckable();
        Parameters.notNull((CharSequence)"original", (Object)original);
        this.matchingObject = matchingObject;
        if (matchingObject.isObjectValid()) {
            this.original = original;
            this.setValidOriginal();
            this.origNodeListener = new OrigNodeListener();
            original.addNodeListener((NodeListener)this.origNodeListener);
        } else {
            this.setInvalidOriginal();
        }
        this.validityListener = new ValidityListener(matchingObject);
        matchingObject.addPropertyChangeListener("invalidityStatus", this.validityListener);
        this.selectionListener = new SelectionListener();
        matchingObject.addPropertyChangeListener(this.selectionListener);
    }

    public PasteType getDropType(Transferable t, int action, int index) {
        return null;
    }

    protected void createPasteTypes(Transferable t, List<PasteType> s) {
    }

    public Transferable drag() throws IOException {
        return UiUtils.DISABLE_TRANSFER;
    }

    public Image getIcon(int type) {
        if (this.valid) {
            return this.original.getIcon(type);
        }
        MatchingObject.InvalidityStatus is = this.matchingObject.getInvalidityStatus();
        String icon = is == null || is == MatchingObject.InvalidityStatus.DELETED ? "org/netbeans/modules/search/res/invalid.png" : "org/netbeans/modules/search/res/warning.gif";
        return ImageUtilities.loadImage((String)icon);
    }

    public Action[] getActions(boolean context) {
        if (!context) {
            Action copyPath = Actions.forID((String)"Edit", (String)"org.netbeans.modules.utilities.CopyPathToClipboard");
            Action[] arraction = new Action[8];
            arraction[0] = SystemAction.get(OpenMatchingObjectsAction.class);
            arraction[1] = this.replacing && !this.matchingObject.isObjectValid() ? new RefreshAction(this.matchingObject) : null;
            arraction[2] = null;
            arraction[3] = copyPath == null ? new CopyPathAction() : copyPath;
            arraction[4] = SystemAction.get(HideResultAction.class);
            arraction[5] = null;
            arraction[6] = SystemAction.get(SelectInAction.class);
            arraction[7] = SystemAction.get(MoreAction.class);
            return arraction;
        }
        return new Action[0];
    }

    public Image getOpenedIcon(int type) {
        return this.getIcon(type);
    }

    public String getHtmlDisplayName() {
        if (this.valid) {
            return this.original.getHtmlDisplayName();
        }
        return this.getInvalidHtmlDisplayName();
    }

    public String getDisplayName() {
        return this.original.getDisplayName();
    }

    public String getShortDescription() {
        return this.matchingObject.getFileObject().getPath();
    }

    public Action getPreferredAction() {
        return SystemAction.get(OpenMatchingObjectsAction.class);
    }

    private void setValidOriginal() {
        this.fireIconChange();
        this.fireDisplayNameChange(null, null);
    }

    private void resetValidOriginal() {
        try {
            this.valid = true;
            this.original = this.matchingObject.getDataObject().getNodeDelegate();
            this.setValidOriginal();
        }
        catch (NullPointerException npe) {
            this.setInvalidOriginal();
        }
    }

    private void setInvalidOriginal() {
        if (!this.valid) {
            this.fireIconChange();
            return;
        }
        this.valid = false;
        if (this.origNodeListener != null && this.original != null) {
            this.original.removeNodeListener((NodeListener)this.origNodeListener);
            this.origNodeListener = null;
        }
        String oldDisplayName = this.original == null ? null : this.original.getDisplayName();
        this.original = new AbstractNode(Children.LEAF){

            public String getHtmlDisplayName() {
                return MatchingObjectNode.this.getInvalidHtmlDisplayName();
            }
        };
        this.original.setDisplayName(this.matchingObject.getFileObject().getNameExt());
        this.fireIconChange();
        this.fireDisplayNameChange(oldDisplayName, this.matchingObject.getFileObject().getNameExt());
    }

    public void clean() {
        if (this.original != null && this.origNodeListener != null && this.valid) {
            this.original.removeNodeListener((NodeListener)this.origNodeListener);
        }
        if (this.validityListener != null) {
            this.matchingObject.removePropertyChangeListener("invalidityStatus", this.validityListener);
            this.validityListener = null;
        }
        if (this.selectionListener != null) {
            this.matchingObject.removePropertyChangeListener("selected", this.selectionListener);
            this.selectionListener = null;
        }
    }

    private String getInvalidHtmlDisplayName() {
        Color colorMngr = UIManager.getColor("nb.search.sandbox.regexp.wrong");
        Color color = colorMngr == null ? Color.RED : colorMngr;
        String stringHex = Integer.toHexString(color.getRGB());
        String stringClr = stringHex.substring(2, 8);
        return "<html><font color='#" + stringClr + "'>" + this.getDisplayName() + "</font></html>";
    }

    public synchronized Node.PropertySet[] getPropertySets() {
        if (this.propertySets == null) {
            Node.PropertySet set;
            this.propertySets = new Node.PropertySet[2];
            this.propertySets[0] = set = new Node.PropertySet(){

                public Node.Property<?>[] getProperties() {
                    Node.Property[] properties = new Node.Property[]{new DetailsCountProperty()};
                    return properties;
                }
            };
            this.propertySets[1] = new FileObjectPropertySet(this.matchingObject.getFileObject());
        }
        return this.propertySets;
    }

    @Override
    public void remove() {
        this.matchingObject.remove();
    }

    public boolean canDestroy() {
        return true;
    }

    public void destroy() throws IOException {
        this.remove();
    }

    private void checkFileObjectValid() {
        FileObject fo = this.matchingObject.getFileObject();
        if (fo != null && fo.isValid()) {
            try {
                DataObject reloaded = DataObject.find((FileObject)fo);
                this.matchingObject.updateDataObject(reloaded);
                this.valid = reloaded.isValid();
                if (this.valid) {
                    EventQueue.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            MatchingObjectNode.this.resetValidOriginal();
                        }
                    });
                }
            }
            catch (DataObjectNotFoundException ex) {
                // empty catch block
            }
        }
    }

    private class SelectionListener
    implements PropertyChangeListener {
        @Override
        public void propertyChange(PropertyChangeEvent e) {
            MatchingObjectNode.this.fireIconChange();
            ResultsOutlineSupport.toggleParentSelected(MatchingObjectNode.this.getParentNode());
        }
    }

    private class ValidityListener
    implements PropertyChangeListener {
        private final MatchingObject matchingObject;

        public ValidityListener(MatchingObject matchingObject) {
            this.matchingObject = matchingObject;
        }

        @Override
        public void propertyChange(PropertyChangeEvent e) {
            EventQueue.invokeLater(new Runnable(){

                @Override
                public void run() {
                    if (ValidityListener.this.matchingObject.getInvalidityStatus() == null) {
                        MatchingObjectNode.this.resetValidOriginal();
                        MatchingObjectNode.this.setChildren(ValidityListener.this.matchingObject.getDetailsChildren(true));
                    } else {
                        MatchingObjectNode.this.setInvalidOriginal();
                    }
                }
            });
        }

    }

    private class CopyPathAction
    extends AbstractAction {
        public CopyPathAction() {
            super(UiUtils.getText("LBL_CopyFilePathAction"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            File f = FileUtil.toFile((FileObject)MatchingObjectNode.this.matchingObject.getFileObject());
            if (f != null) {
                Toolkit toolkit;
                String path = f.getPath();
                Clipboard clipboard = (Clipboard)Lookup.getDefault().lookup(ExClipboard.class);
                if (clipboard == null && (toolkit = Toolkit.getDefaultToolkit()) != null) {
                    clipboard = toolkit.getSystemClipboard();
                }
                if (clipboard != null) {
                    StringSelection strSel = new StringSelection(path);
                    clipboard.setContents(strSel, null);
                }
            }
        }
    }

    private class OrigNodeListener
    implements NodeListener {
        public void childrenAdded(NodeMemberEvent ev) {
        }

        public void childrenRemoved(NodeMemberEvent ev) {
        }

        public void childrenReordered(NodeReorderEvent ev) {
        }

        public void nodeDestroyed(NodeEvent ev) {
            EventQueue.invokeLater(new Runnable(){

                @Override
                public void run() {
                    MatchingObjectNode.this.setInvalidOriginal();
                    RP.post(new Runnable(){

                        @Override
                        public void run() {
                            MatchingObjectNode.this.checkFileObjectValid();
                        }
                    }, 2500);
                }

            });
        }

        public void propertyChange(PropertyChangeEvent evt) {
            MatchingObjectNode.this.setValidOriginal();
        }

    }

    private class DetailsCountProperty
    extends Node.Property<Integer> {
        public DetailsCountProperty() {
            super(Integer.class);
        }

        public boolean canRead() {
            return true;
        }

        public Integer getValue() throws IllegalAccessException, InvocationTargetException {
            return MatchingObjectNode.this.matchingObject.getDetailsCount();
        }

        public boolean canWrite() {
            return false;
        }

        public void setValue(Integer val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            throw new UnsupportedOperationException();
        }

        public String getName() {
            return "detailsCount";
        }
    }

}

