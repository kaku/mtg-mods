/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.Outline
 *  org.openide.awt.ToolbarWithOverflow
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.ExplorerUtils
 *  org.openide.explorer.view.OutlineView
 *  org.openide.explorer.view.Visualizer
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeAdapter
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeMemberEvent
 *  org.openide.nodes.NodeReorderEvent
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.search.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.util.LinkedList;
import java.util.MissingResourceException;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.table.TableModel;
import org.netbeans.api.search.SearchControl;
import org.netbeans.modules.search.ResultView;
import org.netbeans.modules.search.ui.UiUtils;
import org.netbeans.spi.search.provider.SearchComposition;
import org.netbeans.spi.search.provider.SearchProvider;
import org.netbeans.swing.outline.Outline;
import org.openide.awt.ToolbarWithOverflow;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.OutlineView;
import org.openide.explorer.view.Visualizer;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeAdapter;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeReorderEvent;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;

public abstract class AbstractSearchResultsPanel
extends JPanel
implements ExplorerManager.Provider,
Lookup.Provider {
    private static final String REFRESH_ICON = "org/netbeans/modules/search/res/refresh.png";
    private static final String STOP_ICON = "org/netbeans/modules/search/res/stop.png";
    private static final String NEXT_ICON = "org/netbeans/modules/search/res/next.png";
    private static final String PREV_ICON = "org/netbeans/modules/search/res/prev.png";
    private static final String EXPAND_ICON = "org/netbeans/modules/search/res/expandTree.png";
    private static final String COLLAPSE_ICON = "org/netbeans/modules/search/res/collapseTree.png";
    private ExplorerManager explorerManager;
    private SearchComposition<?> searchComposition;
    protected JButton btnStopRefresh = new JButton();
    protected JButton btnPrev = new JButton();
    protected JButton btnNext = new JButton();
    protected JToggleButton btnExpand = new JToggleButton();
    private final SearchProvider.Presenter searchProviderPresenter;
    private Lookup lookup;
    private volatile boolean btnStopRefreshInRefreshMode = false;
    private JPanel contentPanel;
    private JPanel jPanel1;
    private JToolBar toolBar;

    public AbstractSearchResultsPanel(SearchComposition<?> searchComposition, SearchProvider.Presenter searchProviderPresenter) {
        this.searchComposition = searchComposition;
        this.searchProviderPresenter = searchProviderPresenter;
        this.initComponents();
        this.explorerManager = new ExplorerManager();
        ActionMap map = this.getActionMap();
        map.put("delete", ExplorerUtils.actionDelete((ExplorerManager)this.explorerManager, (boolean)false));
        this.lookup = ExplorerUtils.createLookup((ExplorerManager)this.explorerManager, (ActionMap)ResultView.getInstance().getActionMap());
        this.initActions();
        this.initToolbar();
        this.initSelectionListeners();
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.toolBar = new ToolbarWithOverflow();
        this.contentPanel = new JPanel();
        GroupLayout jPanel1Layout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 100, 32767));
        jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 100, 32767));
        this.setLayout(new BorderLayout());
        this.toolBar.setFloatable(false);
        this.toolBar.setOrientation(1);
        this.toolBar.setRollover(true);
        this.toolBar.setPreferredSize(null);
        this.toolBar.setRequestFocusEnabled(false);
        this.add((Component)this.toolBar, "West");
        this.contentPanel.setBorder(BorderFactory.createLineBorder(new Color(153, 153, 153)));
        this.contentPanel.setLayout(new BoxLayout(this.contentPanel, 2));
        this.add((Component)this.contentPanel, "Center");
    }

    public final ExplorerManager getExplorerManager() {
        return this.explorerManager;
    }

    private void initToolbar() {
        this.toolBar.setRollover(true);
        this.initStopRefreshButton();
        this.toolBar.add(this.btnStopRefresh);
        this.initPrevButton();
        this.toolBar.add(this.btnPrev);
        this.initNextButton();
        this.toolBar.add(this.btnNext);
        this.initExpandButton();
        this.toolBar.add(this.btnExpand);
        this.toolBar.setMinimumSize(new Dimension((int)this.toolBar.getMinimumSize().getWidth(), (int)this.btnStopRefresh.getMinimumSize().getHeight()));
    }

    private void initStopRefreshButton() throws MissingResourceException {
        this.sizeButton(this.btnStopRefresh);
        this.btnStopRefresh.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                if (AbstractSearchResultsPanel.this.btnStopRefreshInRefreshMode) {
                    AbstractSearchResultsPanel.this.modifyCriteria();
                } else {
                    AbstractSearchResultsPanel.this.getSearchComposition().terminate();
                }
            }
        });
        this.btnStopRefresh.setToolTipText(UiUtils.getText("TEXT_BUTTON_STOP"));
        this.btnStopRefresh.setIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/search/res/stop.png", (boolean)true));
        this.btnStopRefresh.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ResultView.class, (String)"ACS_TEXT_BUTTON_STOP"));
    }

    private void initExpandButton() {
        this.sizeButton(this.btnExpand);
        this.btnExpand.setIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/search/res/expandTree.png", (boolean)true));
        this.btnExpand.setSelectedIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/search/res/collapseTree.png", (boolean)true));
        this.btnExpand.setToolTipText(UiUtils.getText("TEXT_BUTTON_EXPAND"));
        this.btnExpand.setEnabled(false);
        this.btnExpand.setSelected(false);
    }

    private void initNextButton() {
        this.sizeButton(this.btnNext);
        this.btnNext.setIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/search/res/next.png", (boolean)true));
        this.btnNext.setToolTipText(UiUtils.getText("TEXT_BUTTON_NEXT_MATCH"));
        this.btnNext.setEnabled(false);
        this.btnNext.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                AbstractSearchResultsPanel.this.shift(1);
            }
        });
    }

    private void initPrevButton() {
        this.sizeButton(this.btnPrev);
        this.btnPrev.setIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/search/res/prev.png", (boolean)true));
        this.btnPrev.setToolTipText(UiUtils.getText("TEXT_BUTTON_PREV_MATCH"));
        this.btnPrev.setEnabled(false);
        this.btnPrev.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                AbstractSearchResultsPanel.this.shift(-1);
            }
        });
    }

    protected void sizeButton(AbstractButton button) {
        Dimension dim = new Dimension(24, 24);
        button.setMinimumSize(dim);
        button.setMaximumSize(dim);
        button.setPreferredSize(dim);
    }

    protected JPanel getContentPanel() {
        return this.contentPanel;
    }

    protected SearchComposition<?> getSearchComposition() {
        return this.searchComposition;
    }

    public void searchStarted() {
    }

    public void searchFinished() {
        Mutex.EVENT.writeAccess(new Runnable(){

            @Override
            public void run() {
                AbstractSearchResultsPanel.this.showRefreshButton();
            }
        });
    }

    protected void showRefreshButton() {
        this.btnStopRefresh.setToolTipText(UiUtils.getText("TEXT_BUTTON_CUSTOMIZE"));
        this.btnStopRefresh.setIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/search/res/refresh.png", (boolean)true));
        this.btnStopRefresh.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ResultView.class, (String)"ACS_TEXT_BUTTON_CUSTOMIZE"));
        this.btnStopRefreshInRefreshMode = true;
    }

    protected void modifyCriteria() {
        if (this.searchProviderPresenter != null) {
            if (this.searchProviderPresenter.isReplacing()) {
                SearchControl.openReplaceDialog(this.searchProviderPresenter);
            } else {
                SearchControl.openFindDialog(this.searchProviderPresenter);
            }
        }
    }

    protected void addButton(AbstractButton button) {
        this.toolBar.add(button);
    }

    protected void toggleExpand(Node root, boolean expand) {
        if (expand) {
            this.getOutlineView().expandNode(root);
        }
        for (Node n : root.getChildren().getNodes()) {
            this.toggleExpand(n, expand);
        }
        if (!expand) {
            this.getOutlineView().collapseNode(root);
        }
    }

    protected abstract OutlineView getOutlineView();

    private void initActions() {
        ActionMap map = this.getActionMap();
        map.put("jumpNext", new PrevNextAction(1));
        map.put("jumpPrev", new PrevNextAction(-1));
    }

    private void initSelectionListeners() {
        this.getExplorerManager().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().equals("selectedNodes")) {
                    EventQueue.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            AbstractSearchResultsPanel.this.updateShiftButtons();
                        }
                    });
                }
            }

        });
    }

    protected void updateShiftButtons() {
        if (this.btnPrev.isVisible() && this.btnNext.isVisible()) {
            this.btnPrev.setEnabled(this.findShiftNode(-1, this.getOutlineView(), false) != null);
            this.btnNext.setEnabled(this.findShiftNode(1, this.getOutlineView(), false) != null);
        }
    }

    private void shift(int direction) {
        Node next = this.findShiftNode(direction, this.getOutlineView(), true);
        if (next != null) {
            try {
                this.getExplorerManager().setSelectedNodes(new Node[]{next});
                this.onDetailShift(next);
            }
            catch (PropertyVetoException pve) {
                Exceptions.printStackTrace((Throwable)pve);
            }
        }
    }

    protected void afterMatchingNodeAdded() {
        Mutex.EVENT.writeAccess(new Runnable(){

            @Override
            public void run() {
                if (AbstractSearchResultsPanel.this.btnNext.isVisible() && !AbstractSearchResultsPanel.this.btnNext.isEnabled()) {
                    AbstractSearchResultsPanel.this.updateShiftButtons();
                }
            }
        });
    }

    protected void onDetailShift(Node n) {
    }

    private Node findShiftNode(int direction, OutlineView outlineView, boolean canExpand) {
        Node[] selected = this.getExplorerManager().getSelectedNodes();
        Node n = null;
        if (selected == null || selected.length == 0) {
            n = this.getExplorerManager().getRootContext();
        } else if (selected.length == 1) {
            n = selected[0];
        }
        return n == null ? null : this.findDetailNode(n, direction, outlineView, canExpand);
    }

    Node findDetailNode(Node fromNode, int direction, OutlineView outlineView, boolean canExpand) {
        return this.findUp(fromNode, direction, this.isDetailNode(fromNode) || direction < 0 ? direction : 0, outlineView, canExpand);
    }

    Node findUp(Node node, int dir, int offset, OutlineView outlineView, boolean canExpand) {
        if (node == null) {
            return null;
        }
        Node parent = node.getParentNode();
        Node[] siblings = parent == null ? new Node[]{node} : AbstractSearchResultsPanel.getChildren(parent, outlineView, canExpand);
        int nodeIndex = AbstractSearchResultsPanel.findChildIndex(node, siblings);
        if (nodeIndex + offset < 0 || nodeIndex + offset >= siblings.length) {
            return this.findUp(parent, dir, dir, outlineView, canExpand);
        }
        int i = nodeIndex + offset;
        if (i >= 0 && i < siblings.length) {
            Node found = this.findDown(siblings[i], siblings, i, dir, outlineView, canExpand);
            return found;
        }
        return this.findUp(parent, dir, offset, outlineView, canExpand);
    }

    private Node findDown(Node node, Node[] siblings, int nodeIndex, int dir, OutlineView outlineView, boolean canExpand) {
        int i;
        Node[] children = AbstractSearchResultsPanel.getChildren(node, outlineView, canExpand);
        int n = i = dir > 0 ? 0 : children.length - 1;
        while (i >= 0 && i < children.length) {
            Node found = this.findDown(children[i], children, i, dir, outlineView, canExpand);
            if (found != null) {
                return found;
            }
            i += dir;
        }
        for (i = nodeIndex; i >= 0 && i < siblings.length; i += dir) {
            if (!this.isDetailNode(siblings[i])) continue;
            return siblings[i];
        }
        return null;
    }

    protected abstract boolean isDetailNode(Node var1);

    private static int findChildIndex(Node selectedNode, Node[] siblings) {
        int pos = -1;
        for (int i = 0; i < siblings.length; ++i) {
            if (siblings[i] != selectedNode) continue;
            pos = i;
            break;
        }
        return pos;
    }

    private static Node[] getChildren(Node n, OutlineView outlineView, boolean canExpand) {
        if (outlineView != null) {
            if (!outlineView.isExpanded(n)) {
                if (canExpand) {
                    outlineView.expandNode(n);
                } else {
                    return n.getChildren().getNodes(true);
                }
            }
            return AbstractSearchResultsPanel.getChildrenInDisplayedOrder(n, outlineView);
        }
        return n.getChildren().getNodes(true);
    }

    private static Node[] getChildrenInDisplayedOrder(Node parent, OutlineView outlineView) {
        Outline outline = outlineView.getOutline();
        Node[] unsortedChildren = parent.getChildren().getNodes(true);
        int rows = outlineView.getOutline().getRowCount();
        int start = AbstractSearchResultsPanel.findRowIndexInOutline(parent, outline, rows);
        if (start == -1) {
            return unsortedChildren;
        }
        LinkedList<Node> children = new LinkedList<Node>();
        for (int j = start + 1; j < rows; ++j) {
            int childModelIndex = outline.convertRowIndexToModel(j);
            if (childModelIndex == -1) continue;
            Object childObject = outline.getModel().getValueAt(childModelIndex, 0);
            Node childNode = Visualizer.findNode((Object)childObject);
            if (childNode.getParentNode() == parent) {
                children.add(childNode);
                continue;
            }
            if (children.size() == unsortedChildren.length) break;
        }
        return children.toArray((T[])new Node[children.size()]);
    }

    private static int findRowIndexInOutline(Node node, Outline outline, int rows) {
        int startRow = Math.max(outline.getSelectedRow(), 0);
        int offset = 0;
        while (startRow + offset < rows || startRow - offset >= 0) {
            int up = startRow + offset + 1;
            int down = startRow - offset;
            if (up < rows && AbstractSearchResultsPanel.testNodeInRow(outline, node, up)) {
                return up;
            }
            if (down >= 0 && AbstractSearchResultsPanel.testNodeInRow(outline, node, down)) {
                return down;
            }
            ++offset;
        }
        return -1;
    }

    private static boolean testNodeInRow(Outline outline, Node node, int i) {
        Node n;
        Object o;
        int modelIndex = outline.convertRowIndexToModel(i);
        if (modelIndex != -1 && (n = Visualizer.findNode((Object)(o = outline.getModel().getValueAt(modelIndex, 0)))) == node) {
            return true;
        }
        return false;
    }

    public Lookup getLookup() {
        return this.lookup;
    }

    private final class PrevNextAction
    extends AbstractAction {
        private int direction;

        public PrevNextAction(int direction) {
            this.direction = direction;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            AbstractSearchResultsPanel.this.shift(this.direction);
        }
    }

    private static class RootNodeChildren
    extends Children.Keys<Node> {
        private Node[] standard;
        private Node[] withInfo;
        private boolean infoNodeShown = false;

        public RootNodeChildren(Node resultsNode, Node infoNode) {
            this.standard = new Node[]{resultsNode};
            this.withInfo = new Node[]{resultsNode, infoNode};
            this.setKeys(this.standard);
        }

        private synchronized void showInfoNode() {
            if (!this.infoNodeShown) {
                this.setKeys(this.withInfo);
                this.infoNodeShown = true;
            }
        }

        protected Node[] createNodes(Node key) {
            return new Node[]{key};
        }
    }

    static class RootNode
    extends AbstractNode {
        Node resultsNode;
        Node infoNode;

        public RootNode(Node resultsNode, Node infoNode) {
            this(resultsNode, infoNode, new RootNodeChildren(resultsNode, infoNode));
        }

        private RootNode(Node resultsNode, Node infoNode, RootNodeChildren rootNodeChildren) {
            super((Children)rootNodeChildren);
            this.infoNode = infoNode;
            this.resultsNode = resultsNode;
            if (infoNode != null) {
                this.setInfoNodeListener(rootNodeChildren);
            }
        }

        private void setInfoNodeListener(final RootNodeChildren rootNodeChildren) {
            assert (this.infoNode != null);
            this.infoNode.getChildren().getNodes(true);
            this.infoNode.addNodeListener((NodeListener)new NodeAdapter(){
                private boolean added;

                public synchronized void childrenAdded(NodeMemberEvent ev) {
                    if (!this.added) {
                        rootNodeChildren.showInfoNode();
                        RootNode.this.infoNode.removeNodeListener((NodeListener)this);
                        this.added = true;
                    }
                }

                public void propertyChange(PropertyChangeEvent ev) {
                    super.propertyChange(ev);
                }

                public void childrenReordered(NodeReorderEvent ev) {
                    super.childrenReordered(ev);
                }
            });
        }

    }

}

