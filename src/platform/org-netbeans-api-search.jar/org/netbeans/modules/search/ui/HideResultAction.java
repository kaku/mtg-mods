/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.NodeAction
 */
package org.netbeans.modules.search.ui;

import javax.swing.KeyStroke;
import org.netbeans.modules.search.Removable;
import org.netbeans.modules.search.ui.Bundle;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.actions.NodeAction;

public class HideResultAction
extends NodeAction {
    public HideResultAction() {
        this.putValue("AcceleratorKey", (Object)KeyStroke.getKeyStroke(127, 0));
    }

    public String getName() {
        return Bundle.HideResultAction_displayName();
    }

    protected void performAction(Node[] activatedNodes) {
        for (Node n : activatedNodes) {
            if (!(n instanceof Removable)) continue;
            ((Removable)n).remove();
        }
    }

    protected boolean enable(Node[] activatedNodes) {
        return true;
    }

    public HelpCtx getHelpCtx() {
        return null;
    }
}

