/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 */
package org.netbeans.modules.search.ui;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.openide.ErrorManager;

public abstract class PatternChangeListener
implements DocumentListener {
    @Override
    public void insertUpdate(DocumentEvent e) {
        this.update(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        this.update(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        this.update(e);
    }

    private void update(DocumentEvent e) {
        String text;
        Document doc = e.getDocument();
        try {
            text = doc.getText(0, doc.getLength());
        }
        catch (BadLocationException ex) {
            assert (false);
            ErrorManager.getDefault().notify(65536, (Throwable)ex);
            text = "";
        }
        this.handleComboBoxChange(text);
    }

    public abstract void handleComboBoxChange(String var1);
}

