/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.DefaultOutlineCellRenderer
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.search.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Rectangle;
import java.lang.reflect.InvocationTargetException;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JTable;
import org.netbeans.modules.search.ResultView;
import org.netbeans.swing.outline.DefaultOutlineCellRenderer;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

public class ResultsOutlineCellRenderer
extends DefaultOutlineCellRenderer {
    private static final Logger LOG = Logger.getLogger(ResultsOutlineCellRenderer.class.getName());
    private static final long MINUTE = 60000;
    private static final long HOUR = 3600000;
    private long todayStart;

    public ResultsOutlineCellRenderer() {
        this.todayStart = this.getMidnightTime();
        this.setHorizontalAlignment(4);
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component renderer = null;
        if (value instanceof Node.Property) {
            Node.Property property = (Node.Property)value;
            try {
                String valueString = this.getDisplayValue(property);
                if (property.getName().equals("path")) {
                    renderer = super.getTableCellRendererComponent(table, (Object)this.computeFitText(table, row, column, valueString), isSelected, hasFocus, row, column);
                    this.setToolTip(renderer, property);
                } else if (property.getName().equals("size")) {
                    renderer = super.getTableCellRendererComponent(table, (Object)this.formatFileSize((Long)property.getValue()), isSelected, hasFocus, row, column);
                    this.setToolTip(renderer, property);
                } else if (property.getName().equals("lastModified")) {
                    renderer = super.getTableCellRendererComponent(table, (Object)this.formatDate((Date)property.getValue()), isSelected, hasFocus, row, column);
                    this.setToolTip(renderer, property);
                } else {
                    renderer = super.getTableCellRendererComponent(table, (Object)valueString, isSelected, hasFocus, row, column);
                }
            }
            catch (Exception e) {
                LOG.log(Level.WARNING, null, e);
            }
        }
        if (renderer == null) {
            renderer = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
        if (renderer instanceof JLabel) {
            ((JLabel)renderer).setHorizontalAlignment(4);
            ((JLabel)renderer).setHorizontalTextPosition(4);
        }
        renderer.setForeground(Color.BLACK);
        return renderer;
    }

    String getDisplayValue(Node.Property<?> p) throws IllegalAccessException, InvocationTargetException {
        Object value = p.getValue();
        return value != null ? value.toString() : "";
    }

    private String computeFitText(JTable table, int rowIdx, int columnIdx, String text) {
        String prefix;
        if (text == null) {
            text = "";
        }
        if (text.length() <= 3) {
            return text;
        }
        int width = table.getCellRect((int)rowIdx, (int)columnIdx, (boolean)false).width;
        FontMetrics fm = table.getFontMetrics(table.getFont());
        int sufixLength = fm.stringWidth((prefix = "...") + "  ");
        int desired = width - sufixLength - 15;
        if (desired <= 0) {
            return text;
        }
        for (int i = 1; i <= text.length() - 1; ++i) {
            String part = text.substring(text.length() - i, text.length());
            int swidth = fm.stringWidth(part);
            if (swidth < desired) continue;
            return part.length() > 0 ? prefix + part + " " : text;
        }
        return text;
    }

    private String formatFileSize(Long value) {
        if (value < 1024) {
            return NbBundle.getMessage(ResultView.class, (String)"TXT_FILE_SIZE_B", (Object)value);
        }
        if (value < 0x100000) {
            return NbBundle.getMessage(ResultView.class, (String)"TXT_FILE_SIZE_KB", (Object)(value >> 10));
        }
        if (value < 0x40000000) {
            return NbBundle.getMessage(ResultView.class, (String)"TXT_FILE_SIZE_MB", (Object)(value >> 20));
        }
        return NbBundle.getMessage(ResultView.class, (String)"TXT_FILE_SIZE_GB", (Object)(value >> 30));
    }

    private String formatDate(Date date) {
        long time = date.getTime();
        long now = System.currentTimeMillis();
        if (now - time < 3600000) {
            return NbBundle.getMessage(ResultView.class, (String)"TXT_LAST_MODIFIED_RECENT", (Object)((now - time) / 60000));
        }
        if (time > this.todayStart) {
            return NbBundle.getMessage(ResultView.class, (String)"TXT_LAST_MODIFIED_TODAY", (Object)date);
        }
        return NbBundle.getMessage(ResultView.class, (String)"TXT_LAST_MODIFIED_OLD", (Object)date);
    }

    private long getMidnightTime() {
        Calendar c = Calendar.getInstance();
        c.set(14, 0);
        c.set(13, 0);
        c.set(12, 0);
        c.set(10, 0);
        return c.getTimeInMillis();
    }

    private void setToolTip(Component renderer, Node.Property<?> property) throws IllegalAccessException, InvocationTargetException {
        Object val;
        if (renderer instanceof JLabel && (val = property.getValue()) != null) {
            ((JLabel)renderer).setToolTipText(val.toString());
        }
    }
}

