/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.search.ui;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String HideResultAction_displayName() {
        return NbBundle.getMessage(Bundle.class, (String)"HideResultAction.displayName");
    }

    static String MoreAction_name() {
        return NbBundle.getMessage(Bundle.class, (String)"MoreAction.name");
    }

    static String RefreshAction_name() {
        return NbBundle.getMessage(Bundle.class, (String)"RefreshAction.name");
    }

    static String SelectInAction_name() {
        return NbBundle.getMessage(Bundle.class, (String)"SelectInAction.name");
    }

    static String SelectIn_Favorites() {
        return NbBundle.getMessage(Bundle.class, (String)"SelectIn.Favorites");
    }

    static String SelectIn_Files() {
        return NbBundle.getMessage(Bundle.class, (String)"SelectIn.Files");
    }

    static String SelectIn_Projects() {
        return NbBundle.getMessage(Bundle.class, (String)"SelectIn.Projects");
    }

    private void Bundle() {
    }
}

