/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.search.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

final class IssuesPanel
extends JPanel {
    IssuesPanel(String title, String[] issues) {
        super(new BorderLayout());
        this.add((Component)new JLabel(title), "North");
        this.add((Component)new JScrollPane(issues != null ? new JList<String>(issues) : new JList()), "Center");
    }
}

