/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.LifecycleManager
 *  org.openide.util.Cancellable
 *  org.openide.util.Mutex
 */
package org.netbeans.modules.search;

import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.modules.search.GraphicalSearchListener;
import org.netbeans.modules.search.ResultView;
import org.netbeans.modules.search.ResultViewPanel;
import org.netbeans.spi.search.provider.SearchComposition;
import org.netbeans.spi.search.provider.SearchResultsDisplayer;
import org.openide.LifecycleManager;
import org.openide.util.Cancellable;
import org.openide.util.Mutex;

final class SearchTask
implements Runnable,
Cancellable {
    private boolean notifyWhenFinished = true;
    private volatile boolean interrupted = false;
    private volatile boolean finished = false;
    private final SearchComposition<?> searchComposition;
    private final boolean replacing;
    private ResultViewPanel resultViewPanel = null;

    public SearchTask(SearchComposition<?> searchComposition, boolean replacing) {
        this.searchComposition = searchComposition;
        this.replacing = replacing;
    }

    private boolean isSearchAndReplace() {
        return this.replacing;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void run() {
        if (this.interrupted) {
            return;
        }
        if (this.isSearchAndReplace()) {
            LifecycleManager.getDefault().saveAll();
        }
        if (this.resultViewPanel == null) {
            this.resultViewPanel = ResultView.getInstance().addTab(this);
        }
        GraphicalSearchListener searchListener = this.resultViewPanel.createListener();
        try {
            this.makeResultViewBusy(true);
            searchListener.searchStarted();
            Mutex.EVENT.writeAccess(new Runnable(){

                @Override
                public void run() {
                    SearchTask.this.resultViewPanel.requestFocusInWindow();
                }
            });
            this.searchComposition.start(searchListener);
        }
        catch (RuntimeException e) {
            searchListener.generalError(e);
        }
        finally {
            this.finished = true;
            searchListener.searchFinished();
            this.makeResultViewBusy(false);
        }
    }

    void stop(boolean notifyWhenFinished) {
        if (!notifyWhenFinished) {
            this.notifyWhenFinished = notifyWhenFinished;
        }
        this.stop();
    }

    void stop() {
        if (!this.finished) {
            this.interrupted = true;
        }
        if (this.searchComposition != null) {
            this.searchComposition.terminate();
        }
    }

    public boolean cancel() {
        this.stop();
        return true;
    }

    boolean notifyWhenFinished() {
        return this.notifyWhenFinished;
    }

    boolean wasInterrupted() {
        return this.interrupted;
    }

    SearchResultsDisplayer<?> getDisplayer() {
        return this.searchComposition.getSearchResultsDisplayer();
    }

    SearchComposition<?> getComposition() {
        return this.searchComposition;
    }

    void setResultViewPanel(ResultViewPanel resultViewPanel) {
        this.resultViewPanel = resultViewPanel;
    }

    private void makeResultViewBusy(final boolean busy) {
        Mutex.EVENT.writeAccess(new Runnable(){

            @Override
            public void run() {
                ResultView.getInstance().makeBusy(busy);
            }
        });
    }

}

