/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 */
package org.netbeans.modules.search.matcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.spi.AbstractInterruptibleChannel;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.api.search.SearchPattern;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.TextDetail;
import org.netbeans.modules.search.TextRegexpUtil;
import org.netbeans.modules.search.matcher.AbstractMatcher;
import org.netbeans.modules.search.matcher.MatcherUtils;
import org.netbeans.modules.search.matcher.MultiLineMappedMatcherSmall;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;

public class AsciiMultiLineMappedMatcher
extends AbstractMatcher {
    private SearchPattern searchPattern;
    private Pattern pattern;
    private int fileMatches = 0;
    private int itemMatches = 0;

    public AsciiMultiLineMappedMatcher(SearchPattern searchPattern) {
        this.searchPattern = searchPattern;
        this.pattern = TextRegexpUtil.makeTextPattern(searchPattern);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected MatchingObject.Def checkMeasuredInternal(FileObject fo, SearchListener listener) {
        FileInputStream fis;
        MappedByteBuffer bb = null;
        AbstractInterruptibleChannel fc = null;
        try {
            listener.fileContentMatchingStarted(fo.getPath());
            File file = FileUtil.toFile((FileObject)fo);
            fis = new FileInputStream(file);
            fc = fis.getChannel();
            int sz = (int)fc.size();
            bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, sz);
            List<TextDetail> list = this.matchWholeFile(new FastCharSequence(bb, 0), fo);
            if (list != null && !list.isEmpty()) {
                MatchingObject.Def def = new MatchingObject.Def(fo, Charset.forName("ASCII"), list);
                return def;
            }
            MatchingObject.Def def = null;
            return def;
        }
        catch (Exception e) {
            listener.generalError(e);
            fis = null;
            return fis;
        }
        finally {
            if (fc != null) {
                try {
                    fc.close();
                }
                catch (IOException ex) {
                    listener.generalError(ex);
                }
            }
            MatcherUtils.unmap(bb);
        }
    }

    private List<TextDetail> matchWholeFile(CharSequence cb, FileObject fo) throws DataObjectNotFoundException {
        Matcher textMatcher = this.pattern.matcher(cb);
        DataObject dataObject = null;
        MultiLineMappedMatcherSmall.LineInfoHelper lineInfoHelper = new MultiLineMappedMatcherSmall.LineInfoHelper(cb);
        LinkedList<TextDetail> textDetails = null;
        while (textMatcher.find()) {
            if (textDetails == null) {
                textDetails = new LinkedList<TextDetail>();
                dataObject = DataObject.find((FileObject)fo);
                ++this.fileMatches;
            }
            ++this.itemMatches;
            TextDetail ntd = new TextDetail(dataObject, this.searchPattern);
            lineInfoHelper.findAndSetPositionInfo(ntd, textMatcher.start(), textMatcher.end(), textMatcher.group());
            textDetails.add(ntd);
            if (this.fileMatches < 500 && this.itemMatches < 5000) continue;
            break;
        }
        return textDetails;
    }

    @Override
    public void terminate() {
    }

    private class FastCharSequence
    implements CharSequence {
        private ByteBuffer bb;
        private int start;

        public FastCharSequence(ByteBuffer bb, int start) {
            this.bb = bb;
            this.start = start;
        }

        @Override
        public int length() {
            return this.bb.limit();
        }

        @Override
        public char charAt(int index) {
            return (char)this.bb.get(this.start + index);
        }

        @Override
        public CharSequence subSequence(int start, int end) {
            StringBuilder sb = new StringBuilder();
            for (int i = start; i < end; ++i) {
                sb.append(this.charAt(i));
            }
            return sb.toString();
        }
    }

}

