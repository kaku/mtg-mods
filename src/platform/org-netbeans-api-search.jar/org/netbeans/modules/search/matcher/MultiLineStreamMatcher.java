/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.queries.FileEncodingQuery
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 */
package org.netbeans.modules.search.matcher;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.api.queries.FileEncodingQuery;
import org.netbeans.api.search.SearchPattern;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.TextDetail;
import org.netbeans.modules.search.TextRegexpUtil;
import org.netbeans.modules.search.matcher.AbstractMatcher;
import org.netbeans.modules.search.matcher.BufferedCharSequence;
import org.netbeans.modules.search.matcher.DefaultMatcher;
import org.netbeans.modules.search.matcher.MatcherUtils;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;

public class MultiLineStreamMatcher
extends AbstractMatcher {
    private static final Logger LOG = Logger.getLogger(MultiLineStreamMatcher.class.getName());
    private volatile boolean terminated = false;
    private List<BufferedCharSequence> currentlyProcessedSequences = new ArrayList<BufferedCharSequence>(1);
    private Pattern pattern;
    private SearchPattern searchPattern;

    public MultiLineStreamMatcher(SearchPattern searchPattern) {
        this.searchPattern = searchPattern;
        this.pattern = TextRegexpUtil.makeTextPattern(searchPattern);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public MatchingObject.Def checkMeasuredInternal(FileObject fo, SearchListener listener) {
        block32 : {
            listener.fileContentMatchingStarted(fo.getPath());
            Charset lastCharset = FileEncodingQuery.getEncoding((FileObject)fo);
            BufferedCharSequence bcs = null;
            CharsetDecoder decoder = this.prepareDecoder(lastCharset);
            try {
                bcs = new BufferedCharSequence(fo, decoder, fo.getSize());
                bcs.setSearchListener(listener);
                this.registerProcessedSequence(bcs);
                ArrayList<TextDetail> txtDetails = this.getTextDetailsML(bcs, fo, this.searchPattern);
                this.unregisterProcessedSequence(bcs);
                if (txtDetails != null && !txtDetails.isEmpty()) {
                    MatchingObject.Def def = new MatchingObject.Def(fo, lastCharset, txtDetails);
                    return def;
                }
            }
            catch (BufferedCharSequence.TerminatedException e) {
                LOG.log(Level.INFO, "Search in {0} was terminated.", (Object)fo);
            }
            catch (DataObjectNotFoundException e) {
                LOG.log(Level.SEVERE, "Unable to get data object for the {0}", (Object)fo);
                LOG.throwing(DefaultMatcher.class.getName(), "checkFileContent", (Throwable)e);
                listener.generalError((Throwable)e);
            }
            catch (FileNotFoundException e) {
                LOG.log(Level.SEVERE, "Unable to get input stream for the {0}", (Object)fo);
                LOG.throwing(DefaultMatcher.class.getName(), "checkFileContent", e);
                listener.generalError(e);
            }
            catch (BufferedCharSequence.SourceIOException e) {
                if (e.getCause() instanceof CharacterCodingException) {
                    this.handleDecodingError(listener, fo, decoder, (CharacterCodingException)e.getCause());
                    break block32;
                }
                LOG.log(Level.SEVERE, "IOException during process for the {0}", (Object)fo);
                LOG.log(Level.INFO, "checkFileContent", e);
                listener.generalError(e);
            }
            catch (Exception e) {
                LOG.log(Level.SEVERE, "Unexpected Exception during process for the {0}", (Object)fo);
                LOG.log(Level.INFO, "checkFileContent", e);
                listener.generalError(e);
            }
            finally {
                if (bcs != null) {
                    try {
                        bcs.close();
                    }
                    catch (IOException ex) {}
                }
            }
        }
        return null;
    }

    private ArrayList<TextDetail> getTextDetailsML(BufferedCharSequence bcs, FileObject fo, SearchPattern sp) throws BufferedCharSequence.SourceIOException, DataObjectNotFoundException {
        ArrayList<TextDetail> txtDetails = null;
        DataObject dataObject = null;
        FindState fs = new FindState(bcs);
        int limit = 5000;
        Matcher matcher = this.pattern.matcher(bcs);
        while (matcher.find() && (txtDetails == null || txtDetails.size() < 5000)) {
            if (txtDetails == null) {
                txtDetails = new ArrayList<TextDetail>();
                dataObject = DataObject.find((FileObject)fo);
            }
            int matcherStart = matcher.start();
            int column = fs.calcColumn(matcherStart);
            int lineNumber = fs.getLineNumber();
            String lineText = fs.getLineText();
            TextDetail det = MatcherUtils.createTextDetail(matcher, dataObject, lineNumber, lineText, 0, this.searchPattern);
            det.associate(lineNumber, column, lineText);
            txtDetails.add(det);
        }
        return txtDetails;
    }

    @Override
    public void terminate() {
        this.terminated = true;
        try {
            this.terminateCurrentSearches();
        }
        catch (IOException ex) {
            LOG.log(Level.INFO, ex.getMessage(), ex);
        }
    }

    private synchronized void registerProcessedSequence(BufferedCharSequence bcs) throws IOException {
        if (this.terminated) {
            bcs.close();
        } else {
            this.currentlyProcessedSequences.add(bcs);
        }
    }

    private synchronized void unregisterProcessedSequence(BufferedCharSequence bcc) {
        this.currentlyProcessedSequences.remove(bcc);
    }

    private synchronized void terminateCurrentSearches() throws IOException {
        for (BufferedCharSequence bcs : this.currentlyProcessedSequences) {
            bcs.terminate();
        }
        this.currentlyProcessedSequences.clear();
    }

    private class FindState {
        int lineNumber;
        int lineStartOffset;
        int prevCR;
        BufferedCharSequence bcs;

        FindState(BufferedCharSequence bcs) {
            this.lineNumber = 1;
            this.lineStartOffset = 0;
            this.prevCR = 0;
            this.bcs = bcs;
        }

        int getLineNumber() {
            return this.lineNumber;
        }

        String getLineText() {
            return this.bcs.getLineText(this.lineStartOffset);
        }

        /*
         * Enabled aggressive block sorting
         * Enabled unnecessary exception pruning
         * Enabled aggressive exception aggregation
         */
        int calcColumn(int matcherStart) {
            try {
                block6 : while (this.bcs.position() < matcherStart) {
                    char curChar = this.bcs.nextChar();
                    switch (curChar) {
                        case '\n': 
                        case '\u0085': 
                        case '\u2028': 
                        case '\u2029': {
                            ++this.lineNumber;
                            this.lineStartOffset = this.bcs.position();
                            this.prevCR = 0;
                            continue block6;
                        }
                        case '\r': {
                            ++this.prevCR;
                            char nextChar = this.bcs.charAt(this.bcs.position());
                            if (nextChar == '\n') continue block6;
                            ++this.lineNumber;
                            this.lineStartOffset = this.bcs.position();
                            this.prevCR = 0;
                            continue block6;
                        }
                    }
                    this.prevCR = 0;
                }
                return matcherStart - this.lineStartOffset + 1 - this.prevCR;
            }
            catch (IndexOutOfBoundsException ioobe) {
                // empty catch block
            }
            return matcherStart - this.lineStartOffset + 1 - this.prevCR;
        }
    }

}

