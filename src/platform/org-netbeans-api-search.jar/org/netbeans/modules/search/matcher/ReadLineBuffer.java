/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.search.matcher;

import java.util.ArrayList;
import java.util.List;

class ReadLineBuffer {
    private int currentIndex = 0;
    private int size;
    private final int capacity;
    private final Line[] lines;

    ReadLineBuffer(int capacity) {
        this.capacity = capacity;
        this.lines = new Line[capacity];
    }

    void addLine(int number, String text) {
        this.lines[this.currentIndex] = new Line(number, text);
        this.currentIndex = (this.currentIndex + 1) % this.capacity;
        if (this.size < this.capacity) {
            ++this.size;
        }
    }

    List<Line> getLines() {
        ArrayList<Line> l = new ArrayList<Line>(this.size);
        for (int i = 0; i < this.size; ++i) {
            l.add(this.lines[(this.currentIndex - this.size + i + this.capacity) % this.capacity]);
        }
        return l;
    }

    static class Line {
        private final int number;
        private final String text;

        Line(int number, String text) {
            this.number = number;
            this.text = text;
        }

        int getNumber() {
            return this.number;
        }

        String getText() {
            return this.text;
        }
    }

}

