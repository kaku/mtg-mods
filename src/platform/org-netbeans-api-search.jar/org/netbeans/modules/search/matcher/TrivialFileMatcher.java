/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.search.matcher;

import java.nio.charset.Charset;
import java.util.List;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.TextDetail;
import org.netbeans.modules.search.matcher.AbstractMatcher;
import org.openide.filesystems.FileObject;

public class TrivialFileMatcher
extends AbstractMatcher {
    @Override
    public MatchingObject.Def checkMeasuredInternal(FileObject file, SearchListener listener) {
        return new MatchingObject.Def(file, null, null);
    }

    @Override
    public void terminate() {
    }
}

