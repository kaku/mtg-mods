/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.queries.FileEncodingQuery
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.search.matcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.api.queries.FileEncodingQuery;
import org.netbeans.api.search.SearchPattern;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.TextDetail;
import org.netbeans.modules.search.TextRegexpUtil;
import org.netbeans.modules.search.matcher.AbstractMatcher;
import org.netbeans.modules.search.matcher.MatcherUtils;
import org.netbeans.modules.search.matcher.MultiLineMappedMatcherSmall;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Exceptions;

public class MultiLineMappedMatcherBig
extends AbstractMatcher {
    private static int SIZE_LIMIT = 10485760;
    private static int LINE_LIMIT = 4096;
    private SearchPattern searchPattern;
    private Pattern pattern;
    private int fileMatches = 0;
    private int itemMatches = 0;
    private volatile boolean terminated = false;

    public MultiLineMappedMatcherBig(SearchPattern searchPattern) {
        this.searchPattern = searchPattern;
        this.pattern = TextRegexpUtil.makeTextPattern(searchPattern);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected MatchingObject.Def checkMeasuredInternal(FileObject file, SearchListener listener) {
        List<TextDetail> textDetails;
        Charset charset = FileEncodingQuery.getEncoding((FileObject)file);
        LongCharSequence longSequence = null;
        try {
            MatchingObject.Def def;
            File f = FileUtil.toFile((FileObject)file);
            longSequence = new LongCharSequence(f, charset);
            textDetails = this.matchWholeFile(longSequence, file);
            if (textDetails == null) {
                MatchingObject.Def def2 = null;
                return def2;
            }
            MatchingObject.Def def3 = def = new MatchingObject.Def(file, charset, textDetails);
            return def3;
        }
        catch (Exception ex) {
            listener.generalError(ex);
            textDetails = null;
            return textDetails;
        }
        finally {
            if (longSequence != null) {
                longSequence.close();
            }
        }
    }

    private List<TextDetail> matchWholeFile(CharSequence cb, FileObject fo) throws DataObjectNotFoundException {
        Matcher textMatcher = this.pattern.matcher(cb);
        DataObject dataObject = null;
        MultiLineMappedMatcherSmall.LineInfoHelper lineInfoHelper = new MultiLineMappedMatcherSmall.LineInfoHelper(cb);
        LinkedList<TextDetail> textDetails = null;
        while (textMatcher.find()) {
            if (textDetails == null) {
                textDetails = new LinkedList<TextDetail>();
                dataObject = DataObject.find((FileObject)fo);
                ++this.fileMatches;
            }
            ++this.itemMatches;
            TextDetail ntd = new TextDetail(dataObject, this.searchPattern);
            lineInfoHelper.findAndSetPositionInfo(ntd, textMatcher.start(), textMatcher.end(), textMatcher.group());
            textDetails.add(ntd);
            if (this.fileMatches < 500 && this.itemMatches < 5000) continue;
            break;
        }
        return textDetails;
    }

    @Override
    public void terminate() {
        this.terminated = true;
    }

    class TerminatedException
    extends RuntimeException {
        TerminatedException() {
        }
    }

    private static enum State {
        STANDARD,
        ENDING,
        FLUSHING,
        FLUSHED;
        

        private State() {
        }
    }

    private class LongCharSequence
    implements CharSequence {
        private long fileSize;
        private FileInputStream fileInputStream;
        private FileChannel fileChannel;
        private int charBufferStartsAt;
        private int charBufferEndsAt;
        private CharBuffer charBuffer;
        private MappedByteBuffer byteBuffer;
        private CharsetDecoder decoder;
        private int length;
        private long decodedBytes;
        private boolean overflow;
        private int shifts;
        private int maps;
        private State state;

        public LongCharSequence(File file, Charset charset) throws FileNotFoundException {
            this.charBufferStartsAt = -1;
            this.charBufferEndsAt = -1;
            this.charBuffer = null;
            this.decoder = null;
            this.length = -1;
            this.decodedBytes = 0;
            this.overflow = false;
            this.shifts = 0;
            this.maps = 0;
            this.state = State.STANDARD;
            this.decoder = MultiLineMappedMatcherBig.this.prepareDecoder(charset);
            this.fileInputStream = new FileInputStream(file);
            this.fileChannel = this.fileInputStream.getChannel();
            this.fileSize = file.length();
            this.charBuffer = CharBuffer.allocate((int)Math.min(this.fileSize, (long)SIZE_LIMIT));
        }

        public void reset() {
            this.decoder.reset();
            this.decoder.onUnmappableCharacter(CodingErrorAction.REPLACE);
            this.decodedBytes = 0;
            this.charBuffer.clear();
            this.charBufferStartsAt = -1;
            this.charBufferEndsAt = -1;
            this.overflow = false;
            this.state = State.STANDARD;
            if (this.byteBuffer != null) {
                MatcherUtils.unmap(this.byteBuffer);
                this.byteBuffer = null;
            }
        }

        @Override
        public synchronized int length() {
            if (this.length == -1) {
                try {
                    if (this.charBufferStartsAt == -1) {
                        this.reset();
                    }
                    while (this.shiftBuffer()) {
                        if (!MultiLineMappedMatcherBig.this.terminated) continue;
                        throw new TerminatedException();
                    }
                    this.length = this.charBufferStartsAt + this.charBuffer.limit();
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            return this.length;
        }

        @Override
        public synchronized char charAt(int index) {
            if (MultiLineMappedMatcherBig.this.terminated) {
                throw new TerminatedException();
            }
            if (this.isInBuffer(index)) {
                return this.getFromBuffer(index);
            }
            if (index > this.length()) {
                throw new IndexOutOfBoundsException();
            }
            if (index < this.charBufferStartsAt || this.charBufferStartsAt == -1) {
                this.reset();
            }
            try {
                while (this.shiftBuffer()) {
                    if (!this.isInBuffer(index)) continue;
                    return this.getFromBuffer(index);
                }
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            throw new IllegalStateException("Cannot get character.");
        }

        private boolean shiftBuffer() throws IOException {
            boolean res;
            ++this.shifts;
            if (this.state == State.FLUSHED) {
                assert (!this.overflow);
                res = false;
            } else if (this.state == State.STANDARD) {
                assert (!this.overflow);
                res = this.shiftBufferStandard();
            } else if (this.state == State.ENDING) {
                assert (this.overflow);
                res = this.shiftBufferEnding();
            } else if (this.state == State.FLUSHING) {
                assert (this.overflow);
                res = this.shiftBufferFlushing();
            } else {
                throw new IllegalStateException();
            }
            this.updateBufferBounds();
            return res;
        }

        private boolean shiftBufferStandard() throws IllegalStateException, IOException {
            if (this.byteBuffer == null || this.byteBuffer.remaining() == 0) {
                if (this.byteBuffer != null) {
                    MatcherUtils.unmap(this.byteBuffer);
                }
                long size = Math.min((long)SIZE_LIMIT, this.fileSize - this.decodedBytes);
                this.byteBuffer = this.fileChannel.map(FileChannel.MapMode.READ_ONLY, this.decodedBytes, size);
                ++this.maps;
            }
            long origByteBufPosition = this.byteBuffer.position();
            int origCharBufLimit = this.charBufferStartsAt == -1 ? 0 : this.charBuffer.limit();
            this.charBuffer.clear();
            CoderResult res = this.decoder.decode(this.byteBuffer, this.charBuffer, false);
            this.charBufferStartsAt = this.charBufferStartsAt == -1 ? 0 : this.charBufferStartsAt + origCharBufLimit;
            this.decodedBytes += (long)this.byteBuffer.position() - origByteBufPosition;
            if (res.isOverflow()) {
                if (origByteBufPosition == (long)this.byteBuffer.position()) {
                    throw new IllegalStateException("Neverending loop?");
                }
                this.charBuffer.flip();
                return true;
            }
            if (this.decodedBytes < this.fileSize) {
                this.charBuffer.flip();
                MatcherUtils.unmap(this.byteBuffer);
                this.byteBuffer = null;
                return true;
            }
            this.state = State.ENDING;
            return this.shiftBufferEnding();
        }

        private boolean shiftBufferEnding() {
            CoderResult res;
            assert (this.state == State.ENDING);
            if (this.overflow) {
                this.charBufferStartsAt += this.charBuffer.limit();
                this.charBuffer.clear();
            }
            if ((res = this.decoder.decode(this.byteBuffer, this.charBuffer, true)).isOverflow()) {
                this.charBuffer.flip();
                this.overflow = true;
                return true;
            }
            this.overflow = false;
            this.state = State.FLUSHING;
            return this.shiftBufferFlushing();
        }

        private boolean shiftBufferFlushing() {
            assert (this.state == State.FLUSHING);
            if (this.overflow) {
                this.charBufferStartsAt += this.charBuffer.limit();
                this.charBuffer.clear();
            }
            CoderResult res = this.decoder.flush(this.charBuffer);
            this.charBuffer.flip();
            if (res.isOverflow()) {
                this.overflow = true;
            } else {
                this.overflow = false;
                this.state = State.FLUSHED;
            }
            return true;
        }

        @Override
        public synchronized CharSequence subSequence(int start, int end) {
            if (end - start < LINE_LIMIT) {
                StringBuilder sb = new StringBuilder();
                for (int i = start; i < end; ++i) {
                    sb.append(this.charAt(i));
                }
                return sb.toString();
            }
            throw new IllegalArgumentException("Long subSequences are not supported.");
        }

        @Override
        public synchronized String toString() {
            return this.subSequence(0, this.length()).toString();
        }

        public void close() {
            if (this.fileChannel != null) {
                try {
                    this.fileChannel.close();
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            if (this.fileInputStream != null) {
                try {
                    this.fileInputStream.close();
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            if (this.byteBuffer != null) {
                MatcherUtils.unmap(this.byteBuffer);
            }
        }

        private boolean isInBuffer(int index) {
            return index >= this.charBufferStartsAt && index < this.charBufferEndsAt;
        }

        private char getFromBuffer(int index) {
            return this.charBuffer.charAt(index - this.charBufferStartsAt);
        }

        private void updateBufferBounds() {
            this.charBufferEndsAt = this.charBufferStartsAt == -1 ? -1 : this.charBufferStartsAt + this.charBuffer.limit();
        }
    }

}

