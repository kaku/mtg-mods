/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.search.matcher;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.netbeans.modules.search.TextDetail;

class FinishingTextDetailList {
    private final int linesAfterMatch;
    List<TextDetail> waitList = new LinkedList<TextDetail>();
    Map<Integer, List<TextDetail>> detailMap = new HashMap<Integer, List<TextDetail>>();

    FinishingTextDetailList(int linesAfterMatch) {
        this.linesAfterMatch = linesAfterMatch;
    }

    void addTextDetail(TextDetail textDetail) {
        this.waitList.add(textDetail);
    }

    void nextLineRead(int number, String line) {
        Iterator<TextDetail> iterator = this.waitList.iterator();
        while (iterator.hasNext()) {
            TextDetail next = iterator.next();
            if (number > next.getLine()) {
                next.addSurroundingLine(number, line);
            }
            if (next.getLine() > number - this.linesAfterMatch) continue;
            iterator.remove();
        }
    }
}

