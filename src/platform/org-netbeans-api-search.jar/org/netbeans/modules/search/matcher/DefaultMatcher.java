/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.search.matcher;

import java.io.InputStream;
import java.util.Collection;
import java.util.HashSet;
import org.netbeans.api.search.SearchPattern;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.TextRegexpUtil;
import org.netbeans.modules.search.matcher.AbstractMatcher;
import org.netbeans.modules.search.matcher.MatcherUtils;
import org.netbeans.modules.search.matcher.MultiLineStreamMatcher;
import org.netbeans.modules.search.matcher.SingleLineStreamMatcher;
import org.netbeans.modules.search.matcher.TrivialFileMatcher;
import org.netbeans.spi.search.SearchFilterDefinition;
import org.openide.filesystems.FileObject;

public class DefaultMatcher
extends AbstractMatcher {
    private static final int MAX_UNRECOGNIZED_FILE_SIZE = 5242880;
    private static final Collection<String> searchableXMimeTypes = new HashSet<String>(17);
    private static final Collection<String> searchableExtensions;
    private AbstractMatcher realMatcher;
    private boolean trivial;

    public DefaultMatcher(SearchPattern searchPattern) {
        boolean multiline;
        this.trivial = MatcherUtils.isTrivialPattern(searchPattern);
        this.realMatcher = this.trivial ? new TrivialFileMatcher() : ((multiline = TextRegexpUtil.canBeMultilinePattern(searchPattern.getSearchExpression())) ? new MultiLineStreamMatcher(searchPattern) : new SingleLineStreamMatcher(searchPattern));
    }

    @Override
    public MatchingObject.Def checkMeasuredInternal(FileObject file, SearchListener listener) {
        if (this.trivial) {
            return this.realMatcher.check(file, listener);
        }
        if (DefaultMatcher.isTextFile(file)) {
            try {
                return this.realMatcher.check(file, listener);
            }
            catch (Exception e) {
                listener.fileContentMatchingError(file.getPath(), e);
                return null;
            }
        }
        listener.fileSkipped(file, null, "Not a text file");
        return null;
    }

    @Override
    public synchronized void terminate() {
        this.realMatcher.terminate();
    }

    private static boolean isTextFile(FileObject fileObj) {
        String mimeType = fileObj.getMIMEType();
        if (mimeType.equals("content/unknown")) {
            if (searchableExtensions.contains(fileObj.getExt().toLowerCase())) {
                return true;
            }
            return fileObj.getSize() <= 0x500000 || DefaultMatcher.hasTextContent(fileObj);
        }
        if (mimeType.startsWith("text/")) {
            return true;
        }
        if (mimeType.startsWith("application/")) {
            String subtype = mimeType.substring(12);
            return subtype.equals("rtf") || subtype.equals("sgml") || subtype.startsWith("xml-") || subtype.endsWith("+xml") || DefaultMatcher.isApplicationXSource(subtype, fileObj);
        }
        return mimeType.endsWith("+xml");
    }

    private static boolean isApplicationXSource(String subtype, FileObject fo) {
        return subtype.startsWith("x-") && (searchableXMimeTypes.contains(subtype.substring(2)) || DefaultMatcher.hasTextContent(fo));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    static boolean hasTextContent(FileObject fo) {
        byte[] bytes = new byte[1024];
        byte fe = -2;
        byte ff = -1;
        byte oo = 0;
        byte ef = -17;
        byte bf = -65;
        byte bb = -69;
        try {
            InputStream is = fo.getInputStream();
            try {
                int i;
                int read = is.read(bytes);
                if (read > 2 && bytes[0] == ef && bytes[1] == bb && bytes[2] == bf) {
                    boolean bl = true;
                    return bl;
                }
                if (read > 1 && (bytes[0] == ff && bytes[1] == fe || bytes[0] == fe && bytes[1] == ff)) {
                    boolean bl = true;
                    return bl;
                }
                if (read > 3 && (bytes[0] == oo && bytes[1] == oo && bytes[2] == fe && bytes[3] == ff || bytes[0] == ff && bytes[1] == fe && bytes[2] == oo && bytes[3] == oo)) {
                    boolean bl = true;
                    return bl;
                }
                for (i = 0; i < read; ++i) {
                    if (bytes[i] != oo) continue;
                    boolean bl = false;
                    return bl;
                }
                i = 1;
                return (boolean)i;
            }
            finally {
                is.close();
            }
        }
        catch (Exception e) {
            return false;
        }
    }

    @Override
    public void setStrict(boolean strict) {
        super.setStrict(strict);
        this.realMatcher.setStrict(strict);
    }

    static {
        searchableXMimeTypes.add("csh");
        searchableXMimeTypes.add("httpd-eruby");
        searchableXMimeTypes.add("httpd-php");
        searchableXMimeTypes.add("httpd-php-source");
        searchableXMimeTypes.add("javascript");
        searchableXMimeTypes.add("latex");
        searchableXMimeTypes.add("php");
        searchableXMimeTypes.add("sh");
        searchableXMimeTypes.add("tcl");
        searchableXMimeTypes.add("tex");
        searchableXMimeTypes.add("texinfo");
        searchableXMimeTypes.add("troff");
        searchableExtensions = new HashSet<String>();
        searchableExtensions.add("txt");
        searchableExtensions.add("log");
    }
}

