/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.search.matcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.CharsetDecoder;
import org.openide.util.Exceptions;

class LineReader {
    private static final int LINE_LENGHT_LIMIT = 5242880;
    int lastChar = 0;
    int pos = 0;
    int line = 1;
    InputStreamReader isr;
    BufferedReader br;

    LineReader(CharsetDecoder decoder, InputStream stream) throws IOException {
        this.isr = new InputStreamReader(stream, decoder);
        try {
            this.br = new BufferedReader(this.isr);
        }
        catch (Throwable t) {
            if (this.isr != null) {
                try {
                    this.isr.close();
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            throw new IOException(t);
        }
    }

    LineInfo readNext() throws IOException {
        int ch;
        LineInfo nel = new LineInfo(this.pos, this.line);
        while ((ch = this.br.read()) != -1) {
            ++this.pos;
            if (ch == 10 && this.lastChar == 13) {
                nel = new LineInfo(this.pos, this.line);
            } else {
                if (this.isLineTerminator(ch)) {
                    ++this.line;
                    this.lastChar = ch;
                    nel.close();
                    return nel;
                }
                nel.appendCharacter(ch);
            }
            this.lastChar = ch;
        }
        if (nel.isNotEmpty()) {
            nel.close();
            return nel;
        }
        return null;
    }

    private boolean isLineTerminator(int ch) {
        return ch == 10 || ch == 13 || ch == 8232 || ch == 133 || ch == 8233;
    }

    void close() {
        if (this.br != null) {
            try {
                this.br.close();
            }
            catch (Throwable t) {
                Exceptions.printStackTrace((Throwable)t);
            }
        }
        if (this.isr != null) {
            try {
                this.isr.close();
            }
            catch (Throwable t) {
                Exceptions.printStackTrace((Throwable)t);
            }
        }
    }

    static class LineInfo {
        private int start;
        private int length = 0;
        private int number;
        private StringBuilder sb = new StringBuilder();
        private String string = null;

        private LineInfo(int start, int number) {
            this.start = start;
            this.number = number;
        }

        private void appendCharacter(int c) throws IOException {
            this.sb.append((char)c);
            ++this.length;
            if (this.length > 5242880) {
                throw new IOException("Line is too long: " + this.number);
            }
        }

        String getString() {
            return this.string;
        }

        int getNumber() {
            return this.number;
        }

        int getFileStart() {
            return this.start;
        }

        int getFileEnd() {
            return this.start + this.length;
        }

        private boolean isNotEmpty() {
            return this.length > 0;
        }

        int getLength() {
            return this.length;
        }

        void close() {
            this.string = this.sb.toString();
            this.sb = null;
        }
    }

}

