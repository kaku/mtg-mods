/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.search.matcher;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.search.provider.SearchListener;
import org.openide.filesystems.FileObject;

public class BufferedCharSequence
implements CharSequence {
    private static final Logger LOG = Logger.getLogger("org.netbeans.modules.search.BufferedCharSequence");
    public static final int K = 1024;
    public static final int MAX_FILE_SIZE = Integer.MAX_VALUE;
    private static final int MAX_SOURCE_BUFFER_SIZE = 16384;
    public static final int MAX_SUBSEQUENCE_LENGTH = 4096;
    private static final int MIN_SINK_BUFFER_SIZE = 16;
    private Source source;
    private Sink sink;
    private final CharsetDecoder decoder;
    private SearchListener listener;
    private long maxReadOffset = 0;
    private CoderResult coderResult;
    private boolean isClosed = false;
    private volatile boolean isTerminated = false;
    private int position = 0;

    @Deprecated
    public BufferedCharSequence(InputStream stream, CharsetDecoder decoder, long size) {
        this.source = new Source(this, stream, size);
        this.decoder = decoder;
        this.sink = new Sink(this.source);
    }

    public BufferedCharSequence(FileObject fo, CharsetDecoder decoder, long size) {
        this.source = new Source(this, fo, size);
        this.decoder = decoder;
        this.sink = new Sink(this.source);
        LOG.log(Level.FINER, "<init> {0}; decoder = {1}; {2}", new Object[]{this.source, this.decoder, this.sink});
    }

    private BufferedCharSequence(BufferedCharSequence bufferedCharSequence) {
        this.source = bufferedCharSequence.source;
        this.sink = bufferedCharSequence.sink;
        this.decoder = bufferedCharSequence.decoder;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void finalize() throws Throwable {
        try {
            this.close();
        }
        finally {
            super.finalize();
        }
    }

    public int getMaxBufferSize() {
        return this.source.maxBufferSize;
    }

    public void setMaxBufferSize(int maxBufferSize) {
        if (maxBufferSize < 1) {
            throw new IllegalArgumentException();
        }
        this.source.maxBufferSize = maxBufferSize;
    }

    public void setSearchListener(SearchListener listener) {
        this.listener = listener;
    }

    public BufferedCharSequence reset() throws SourceIOException {
        this.source.reset();
        this.sink.reset();
        this.decoder.reset();
        this.coderResult = CoderResult.UNDERFLOW;
        return this;
    }

    public synchronized BufferedCharSequence close() throws IOException {
        if (!this.isClosed) {
            this.reset();
            this.source.close();
            this.source = null;
            this.sink = null;
            this.coderResult = null;
            this.listener = null;
            this.isClosed = true;
        }
        return this;
    }

    void terminate() {
        this.isTerminated = true;
    }

    public BufferedCharSequence duplicate() {
        this.checkState();
        return new BufferedCharSequence(this);
    }

    @Override
    public int length() {
        this.checkState();
        this.reset();
        int length = 0;
        try {
            while (this.sink.next()) {
            }
        }
        catch (CharacterCodingException ex) {
            throw new SourceIOException(ex);
        }
        length = this.sink.buffer.scope.end;
        return length;
    }

    @Override
    public char charAt(int index) throws IndexOutOfBoundsException {
        if (this.isTerminated) {
            throw new TerminatedException();
        }
        this.checkState();
        String errMsg = this.check(index);
        if (errMsg != null) {
            throw new IndexOutOfBoundsException(errMsg);
        }
        return this.getCharAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end) throws IndexOutOfBoundsException {
        this.checkState();
        String errMsg = this.check(start, end);
        if (errMsg != null) {
            throw new IndexOutOfBoundsException(errMsg);
        }
        int subSequenceLength = this.getLength(start, end);
        if (subSequenceLength > 4096) {
            throw new IndexOutOfBoundsException("requested subSequence has a big length (" + subSequenceLength + ") > " + 4096);
        }
        CharSequence subSequence = this.getSubSequence(start, end);
        return subSequence;
    }

    @Override
    public String toString() {
        this.checkState();
        return this.subSequence(0, this.length()).toString();
    }

    public final int position() {
        this.checkState();
        return this.position;
    }

    public final int changePosition(int newPosition) {
        int oldPosition = this.position;
        this.position = newPosition;
        return oldPosition;
    }

    public char nextChar() throws IndexOutOfBoundsException {
        this.checkState();
        return this.charAt(this.position++);
    }

    public final BufferedCharSequence rewind() {
        this.checkState();
        this.position = 0;
        return this;
    }

    public String nextLineText() {
        StringBuilder sb = new StringBuilder();
        try {
            do {
                char c = this.nextChar();
                switch (c) {
                    case '\n': 
                    case '\u0085': 
                    case '\u2028': 
                    case '\u2029': {
                        return sb.toString();
                    }
                    case '\r': {
                        if (this.charAt(this.position) == '\n') {
                            this.nextChar();
                        }
                        return sb.toString();
                    }
                }
                sb.append(c);
            } while (true);
        }
        catch (IndexOutOfBoundsException ioobe) {
            return sb.toString();
        }
    }

    public String getLineText(int start) {
        int oldPosition = this.changePosition(start);
        String text = this.nextLineText();
        this.changePosition(oldPosition);
        return text;
    }

    private char getCharAt(int index) throws IndexOutOfBoundsException {
        if (this.sink.buffer.scope.isBefore(index)) {
            this.reset();
        }
        while (!this.sink.buffer.scope.isInside(index)) {
            boolean hasNext;
            try {
                hasNext = this.sink.next();
            }
            catch (CharacterCodingException e) {
                throw new SourceIOException(e);
            }
            if (this.listener != null && (long)this.sink.buffer.scope.start > this.maxReadOffset) {
                this.maxReadOffset = this.sink.buffer.scope.start;
                this.listener.fileContentMatchingProgress(this.source.fo.getPath(), this.maxReadOffset);
            }
            if (hasNext) continue;
            throw new IndexOutOfBoundsException("index is " + index + " > lenght");
        }
        return this.sink.charAt(index);
    }

    private CharSequence getSubSequence(int start, int end) throws IndexOutOfBoundsException {
        StringBuilder sb = new StringBuilder(this.getLength(start, end));
        for (int i = start; i < end; ++i) {
            sb.append(this.charAt(i));
        }
        return sb.toString();
    }

    private int getLength(int start, int end) {
        return end - start;
    }

    private void checkState() {
        if (this.isClosed) {
            String msg = "BufferedCharSequence is closed";
            throw new IllegalStateException(msg);
        }
    }

    private String check(int index) {
        if (index < 0) {
            return "index = " + index;
        }
        return null;
    }

    private String check(int start, int end) {
        if (start < 0 || end < 0 || start > end) {
            return "start = " + start + ", end = " + end;
        }
        return null;
    }

    static class TerminatedException
    extends RuntimeException {
    }

    public static class SourceIOException
    extends RuntimeException {
        public SourceIOException(IOException cause) {
            super(cause);
        }
    }

    private class Sink {
        private Buffer buffer;
        private boolean wasEndOfInput;

        public Sink(Source source) {
            int sourceCapacity = source.getCapacity();
            this.buffer = this.newBuffer(sourceCapacity);
        }

        public String toString() {
            return "sink = [" + this.buffer + "]";
        }

        public void reset() {
            this.wasEndOfInput = false;
            this.buffer.reset();
        }

        public char charAt(int index) throws IndexOutOfBoundsException {
            assert (index >= 0);
            return this.buffer.getCharAt(index);
        }

        private boolean next() throws CharacterCodingException {
            if (this.wasEndOfInput) {
                return false;
            }
            CharBuffer out = this.buffer.clear();
            boolean endOfInput = this.wasEndOfInput;
            do {
                if (BufferedCharSequence.this.coderResult == CoderResult.UNDERFLOW) {
                    endOfInput = BufferedCharSequence.this.source.readNext();
                }
                BufferedCharSequence.this.coderResult = BufferedCharSequence.this.decoder.decode(BufferedCharSequence.this.source.buffer, out, endOfInput);
                if (BufferedCharSequence.this.coderResult.isOverflow()) {
                    out = this.buffer.growBuffer();
                }
                this.checkError(BufferedCharSequence.this.coderResult);
            } while (out.position() == 0 && BufferedCharSequence.this.coderResult.isUnderflow() && !endOfInput);
            if (endOfInput) {
                while ((BufferedCharSequence.this.coderResult = BufferedCharSequence.this.decoder.flush(out)) == CoderResult.OVERFLOW) {
                    out = this.buffer.growBuffer();
                }
                this.checkError(BufferedCharSequence.this.coderResult);
            }
            this.buffer.adjustScope();
            this.wasEndOfInput = endOfInput;
            return !this.buffer.scope.isEmpty();
        }

        private void checkError(CoderResult result) throws CharacterCodingException {
            if (result.isError()) {
                result.throwException();
            }
        }

        private Buffer newBuffer(int sourceCapacity) {
            return new Buffer(sourceCapacity);
        }

        private class Buffer {
            private CharBuffer charBuffer;
            private Scope scope;

            public Buffer(int sourceCapacity) {
                this.scope = new Scope();
                this.allocate(sourceCapacity);
                this.reset();
            }

            public String toString() {
                return "buffer[capacity = " + this.charBuffer.capacity() + "]";
            }

            public CharBuffer growBuffer() {
                int capacity = this.charBuffer.capacity();
                CharBuffer o = CharBuffer.allocate(capacity << 1);
                this.charBuffer.flip();
                o.put(this.charBuffer);
                this.charBuffer = o;
                LOG.finer("The sink char buffer capacity has been grown: " + capacity + " -> " + this.charBuffer.capacity());
                return this.charBuffer;
            }

            private void allocate(int sourceCapacity) {
                if (sourceCapacity == 0) {
                    this.charBuffer = CharBuffer.allocate(0);
                    return;
                }
                int bufferSize = this.bufferSize(sourceCapacity);
                this.charBuffer = CharBuffer.allocate(bufferSize);
            }

            private int bufferSize(int sourceCapacity) {
                int n = (int)((float)sourceCapacity * BufferedCharSequence.this.decoder.averageCharsPerByte());
                return n < 16 ? 16 : n;
            }

            private char getCharAt(int index) {
                int position = this.charBuffer.position();
                this.charBuffer.position(0);
                char c = this.charBuffer.charAt(index - this.scope.start);
                this.charBuffer.position(position);
                return c;
            }

            private void reset() {
                this.scope.reset();
                this.charBuffer.clear();
            }

            private void flip() {
                this.charBuffer.flip();
            }

            private CharBuffer clear() {
                this.charBuffer.clear();
                return this.charBuffer;
            }

            private void adjustScope() {
                this.scope.start = this.scope.end == -1 ? 0 : this.scope.end;
                this.flip();
                this.scope.end = this.scope.start + this.charBuffer.limit();
            }

            private class Scope {
                public static final int EOF = -1;
                private int start;
                private int end;

                private Scope() {
                }

                public boolean isInside(int index) {
                    return index >= this.start && index < this.end;
                }

                public boolean isBefore(int index) {
                    return this.start == -1 || index < this.start;
                }

                public void reset() {
                    this.end = -1;
                    this.start = -1;
                }

                private boolean isEmpty() {
                    return this.start == this.end;
                }
            }

        }

    }

    private class Source {
        private int maxBufferSize;
        private FileObject fo;
        private ByteBuffer buffer;
        private InputStream istream;
        private BufferedInputStream bstream;
        private int bufferSize;
        final /* synthetic */ BufferedCharSequence this$0;

        @Deprecated
        public Source(BufferedCharSequence bufferedCharSequence, InputStream inputStream, long bufferSize) {
            this.this$0 = bufferedCharSequence;
            this.maxBufferSize = 16384;
            this.fo = null;
            this.bstream = new BufferedInputStream(inputStream);
            this.bstream.mark(Integer.MAX_VALUE);
            this.bufferSize = this.getBufferSize(bufferSize);
            this.buffer = this.newBuffer();
            this.buffer.position(this.buffer.limit());
        }

        public Source(BufferedCharSequence bufferedCharSequence, FileObject fo, long bufferSize) {
            this.this$0 = bufferedCharSequence;
            this.maxBufferSize = 16384;
            this.fo = null;
            this.fo = fo;
            this.bufferSize = this.getBufferSize(bufferSize);
            this.initStreams();
            this.buffer = this.newBuffer();
            this.buffer.position(this.buffer.limit());
        }

        public String toString() {
            return "source=[stream = " + this.bstream.toString() + ", buffer = " + this.buffer + "]";
        }

        private ByteBuffer newBuffer() {
            return ByteBuffer.allocate(this.bufferSize);
        }

        public void reset() {
            try {
                if (this.fo == null) {
                    this.bstream.reset();
                } else {
                    this.closeStreams();
                    this.initStreams();
                }
            }
            catch (IOException ex) {
                throw new SourceIOException(ex);
            }
            this.buffer.clear();
            this.buffer.position(this.buffer.limit());
        }

        private int read() {
            try {
                if (this.buffer.hasArray()) {
                    if (this.buffer.capacity() == 0) {
                        return -1;
                    }
                    int res = this.bstream.read(this.buffer.array(), this.buffer.position(), this.buffer.remaining());
                    if (res > 0) {
                        this.buffer.position(res + this.buffer.position());
                    }
                    return res;
                }
                throw new IOException("No byte array");
            }
            catch (IOException ex) {
                throw new SourceIOException(ex);
            }
        }

        public void close() throws IOException {
            this.closeStreams();
        }

        private void initStreams() {
            try {
                this.istream = this.fo.getInputStream();
                try {
                    this.bstream = new BufferedInputStream(this.istream, this.bufferSize);
                }
                catch (Throwable t) {
                    if (this.istream != null) {
                        this.istream.close();
                    }
                    throw t;
                }
            }
            catch (Throwable t) {
                throw new SourceIOException(new IOException(t));
            }
        }

        private void closeStreams() throws IOException {
            IOException t = null;
            try {
                if (this.bstream != null) {
                    this.bstream.close();
                    this.bstream = null;
                }
            }
            catch (IOException e) {
                t = e;
            }
            if (this.istream != null) {
                this.istream.close();
                this.istream = null;
            }
            if (t != null) {
                throw t;
            }
        }

        public int getSize(long size) {
            if (size > Integer.MAX_VALUE) {
                LOG.warning("File size is " + size + "bytes. " + "Only first " + Integer.MAX_VALUE + " bytes will be processed.");
                return Integer.MAX_VALUE;
            }
            return (int)size;
        }

        private int getBufferSize(long bufferSize) {
            int size = Math.min(this.getSize(bufferSize), this.maxBufferSize);
            return size;
        }

        public boolean readNext() {
            this.buffer.compact();
            int status = this.read();
            this.buffer.flip();
            return status == -1;
        }

        private int getCapacity() {
            return this.buffer.capacity();
        }
    }

    static interface UnicodeLineTerminator {
        public static final char LF = '\n';
        public static final char CR = '\r';
        public static final char LS = '\u2028';
        public static final char PS = '\u2029';
        public static final char NEL = '\u0085';
    }

}

