/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.queries.FileEncodingQuery
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.search.matcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.spi.AbstractInterruptibleChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.api.queries.FileEncodingQuery;
import org.netbeans.api.search.SearchPattern;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.TextDetail;
import org.netbeans.modules.search.TextRegexpUtil;
import org.netbeans.modules.search.matcher.AbstractMatcher;
import org.netbeans.modules.search.matcher.DefaultMatcher;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Exceptions;

public class FastMatcher
extends AbstractMatcher {
    private static final int SIZE_LIMIT = 1048576;
    private static final int LINE_LIMIT = 4096;
    private static final Logger LOG = Logger.getLogger(FastMatcher.class.getName());
    private SearchPattern searchPattern;
    private Pattern pattern;
    private DefaultMatcher defaultMatcher = null;
    private static final Pattern linePattern = Pattern.compile("(.*)(\\r\\n|\\n|\\r)");
    private boolean trivial;
    private boolean asciiPattern;
    private long totalTime;
    private int fileMatches = 0;
    private int itemMatches = 0;
    private boolean multiline = false;

    public FastMatcher(SearchPattern searchPattern) {
        boolean bl = this.trivial = searchPattern.getSearchExpression() == null || searchPattern.getSearchExpression().isEmpty();
        if (!this.trivial) {
            this.searchPattern = searchPattern;
            String expr = searchPattern.getSearchExpression();
            this.pattern = TextRegexpUtil.makeTextPattern(searchPattern);
            this.multiline = TextRegexpUtil.canBeMultilinePattern(expr);
            boolean bl2 = this.asciiPattern = expr.matches("\\p{ASCII}+") && !expr.contains(".") && !expr.matches(".*\\\\[0xXuU].*");
            if (this.asciiPattern) {
                LOG.info("Using ASCII pattern");
            }
        }
    }

    @Override
    public MatchingObject.Def checkMeasuredInternal(FileObject file, SearchListener listener) {
        if (this.trivial) {
            return new MatchingObject.Def(file, null, null);
        }
        listener.fileContentMatchingStarted(file.getPath());
        long start = System.currentTimeMillis();
        File f = FileUtil.toFile((FileObject)file);
        MatchingObject.Def def = file.getSize() > 0x100000 || f == null ? this.checkBig(file, f, listener) : this.checkSmall(file, f, listener);
        this.totalTime += System.currentTimeMillis() - start;
        return def;
    }

    @Override
    public void terminate() {
        if (this.defaultMatcher != null) {
            this.defaultMatcher.terminate();
        }
    }

    private DefaultMatcher getDefaultMatcher() {
        if (this.defaultMatcher == null) {
            this.defaultMatcher = new DefaultMatcher(this.searchPattern);
        }
        return this.defaultMatcher;
    }

    private List<TextDetail> matchWholeFile(CharSequence cb, FileObject fo) throws DataObjectNotFoundException {
        Matcher textMatcher = this.pattern.matcher(cb);
        DataObject dataObject = null;
        LineInfoHelper lineInfoHelper = new LineInfoHelper(cb);
        LinkedList<TextDetail> textDetails = null;
        while (textMatcher.find()) {
            if (textDetails == null) {
                textDetails = new LinkedList<TextDetail>();
                dataObject = DataObject.find((FileObject)fo);
                ++this.fileMatches;
            }
            ++this.itemMatches;
            TextDetail ntd = new TextDetail(dataObject, this.searchPattern);
            lineInfoHelper.findAndSetPositionInfo(ntd, textMatcher.start(), textMatcher.end(), textMatcher.group());
            textDetails.add(ntd);
            if (this.fileMatches < 500 && this.itemMatches < 5000) continue;
            break;
        }
        return textDetails;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private List<TextDetail> matchLines(CharSequence cs, FileObject fo) throws DataObjectNotFoundException, IOException {
        LineInfo line;
        ArrayList<Object> dets = null;
        DataObject dataObject = null;
        int count = 0;
        int limit = 5000;
        boolean canRun = true;
        LineReader nelr = new LineReader(cs);
        while ((line = nelr.readNext()) != null && canRun && count < limit) {
            Object det;
            Matcher m = this.pattern.matcher(line.getString());
            while (m.find() && canRun) {
                if (dets == null) {
                    dets = new ArrayList<Object>();
                    dataObject = DataObject.find((FileObject)fo);
                }
                det = this.createLineMatchTextDetail(dataObject, line.getNumber(), m, line.getString(), line.start);
                dets.add(det);
                ++count;
            }
            if (line.getNumber() % 50 != 0) continue;
            det = this;
            synchronized (det) {
                canRun = true;
                continue;
            }
        }
        return dets;
    }

    private TextDetail createLineMatchTextDetail(DataObject dataObject, int lineNumber, Matcher textMatcher, String text, int lineStart) {
        TextDetail textDetail = new TextDetail(dataObject, this.searchPattern);
        textDetail.associate(lineNumber, textMatcher.start() + 1, text);
        textDetail.setMatchedText(textMatcher.group());
        textDetail.setStartOffset(lineStart + textMatcher.start());
        textDetail.setEndOffset(lineStart + textMatcher.end());
        textDetail.setMarkLength(textMatcher.end() - textMatcher.start());
        return textDetail;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private MatchingObject.Def checkSmall(FileObject fo, File file, SearchListener listener) {
        MappedByteBuffer bb = null;
        AbstractInterruptibleChannel fc = null;
        try {
            List<TextDetail> textDetails;
            MatchingObject.Def def;
            FileInputStream fis = new FileInputStream(file);
            fc = fis.getChannel();
            int sz = (int)fc.size();
            bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, sz);
            if (this.asciiPattern && !this.matchesIgnoringEncoding(bb)) {
                MatchingObject.Def def2 = null;
                return def2;
            }
            Charset charset = FileEncodingQuery.getEncoding((FileObject)fo);
            CharsetDecoder decoder = this.prepareDecoder(charset);
            decoder.onUnmappableCharacter(CodingErrorAction.IGNORE);
            CharBuffer cb = decoder.decode(bb);
            List<TextDetail> list = textDetails = this.multiline ? this.matchWholeFile(cb, fo) : this.matchLines(cb, fo);
            if (textDetails == null) {
                MatchingObject.Def def3 = null;
                return def3;
            }
            MatchingObject.Def ex = def = new MatchingObject.Def(fo, decoder.charset(), textDetails);
            return ex;
        }
        catch (Exception e) {
            listener.generalError(e);
            MatchingObject.Def sz = null;
            return sz;
        }
        finally {
            if (fc != null) {
                try {
                    fc.close();
                }
                catch (IOException ex) {
                    listener.generalError(ex);
                }
            }
            this.unmap(bb);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private MatchingObject.Def checkBig(FileObject fileObject, File file, SearchListener listener) {
        Charset charset = FileEncodingQuery.getEncoding((FileObject)fileObject);
        CharsetDecoder decoder = this.prepareDecoder(charset);
        LongCharSequence longSequence = null;
        try {
            MatchingObject.Def def;
            List<TextDetail> textDetails;
            longSequence = new LongCharSequence(file, charset);
            List<TextDetail> list = textDetails = this.multiline ? this.matchWholeFile(longSequence, fileObject) : this.matchLines(longSequence, fileObject);
            if (textDetails == null) {
                MatchingObject.Def def2 = null;
                return def2;
            }
            MatchingObject.Def def3 = def = new MatchingObject.Def(fileObject, decoder.charset(), textDetails);
            return def3;
        }
        catch (Exception ex) {
            listener.generalError(ex);
            MatchingObject.Def def = null;
            return def;
        }
        finally {
            if (longSequence != null) {
                longSequence.close();
            }
        }
    }

    private boolean matchesIgnoringEncoding(ByteBuffer byteBuffer) {
        Matcher m = this.pattern.matcher(new FastCharSequence(byteBuffer, 0));
        boolean found = m.find();
        return found;
    }

    private void unmap(MappedByteBuffer buffer) {
        try {
            Method getCleanerMethod = buffer.getClass().getMethod("cleaner", new Class[0]);
            getCleanerMethod.setAccessible(true);
            Object cleaner = getCleanerMethod.invoke(buffer, new Object[0]);
            cleaner.getClass().getMethod("clean", new Class[0]).invoke(cleaner, new Object[0]);
        }
        catch (Exception e) {
            // empty catch block
        }
    }

    class LineInfo {
        private int start;
        private int length;
        private int number;
        private StringBuilder sb;
        private String string;

        LineInfo(int start, int number) {
            this.length = 0;
            this.sb = new StringBuilder();
            this.string = null;
            this.start = start;
            this.number = number;
        }

        private void appendCharacter(int c) throws IOException {
            this.sb.append((char)c);
            ++this.length;
            if (this.length > 4096) {
                throw new IOException("Line is too long: " + this.number);
            }
        }

        String getString() {
            return this.string;
        }

        int getNumber() {
            return this.number;
        }

        int getFileStart() {
            return this.start;
        }

        int getFileEnd() {
            return this.start + this.length;
        }

        private boolean isNotEmpty() {
            return this.length > 0;
        }

        int getLength() {
            return this.length;
        }

        void close() {
            this.string = this.sb.toString();
            this.sb = null;
        }
    }

    class LineReader {
        int lastChar;
        int pos;
        int line;
        int length;
        CharSequence charSequence;

        LineReader(CharSequence charSequence) throws IOException {
            this.lastChar = 0;
            this.pos = 0;
            this.line = 1;
            this.length = 0;
            this.charSequence = charSequence;
            this.length = charSequence.length();
        }

        LineInfo readNext() throws IOException {
            LineInfo li = new LineInfo(this.pos, this.line);
            if (this.pos >= this.length) {
                return null;
            }
            while (this.pos < this.length) {
                char ch = this.charSequence.charAt(this.pos);
                ++this.pos;
                if (ch == '\n' && this.lastChar == 13) {
                    li = new LineInfo(this.pos, this.line);
                } else {
                    if (this.isLineTerminator(ch)) {
                        ++this.line;
                        this.lastChar = ch;
                        li.close();
                        return li;
                    }
                    li.appendCharacter(ch);
                }
                this.lastChar = ch;
            }
            li.close();
            return li;
        }

        private boolean isLineTerminator(int ch) {
            return ch == 10 || ch == 13 || ch == 8232 || ch == 133 || ch == 8233;
        }
    }

    private class LineInfoHelper {
        private CharSequence charSequence;
        private Matcher lineMatcher;
        private int lastStartPos;
        private int currentLineNumber;
        private int currentLineStart;
        private int currentLineEnd;
        private String lastLine;

        public LineInfoHelper(CharSequence charSequence) {
            this.lastStartPos = 0;
            this.currentLineNumber = 0;
            this.currentLineStart = -1;
            this.currentLineEnd = -1;
            this.lastLine = null;
            this.charSequence = charSequence;
            this.lineMatcher = linePattern.matcher(charSequence);
        }

        public void findAndSetPositionInfo(TextDetail textDetail, int startPos, int endPos, String text) {
            if (startPos < this.lastStartPos) {
                throw new IllegalStateException("Start offset lower than the previous one.");
            }
            this.updateStateForPosition(startPos);
            this.setTextDetailInfo(textDetail, startPos, endPos, text);
        }

        private void updateStateForPosition(int pos) {
            if (pos > this.currentLineEnd) {
                boolean found = false;
                while (this.lineMatcher.find()) {
                    ++this.currentLineNumber;
                    this.currentLineEnd = this.lineMatcher.end() - 1;
                    if (this.lineMatcher.end() <= pos) continue;
                    this.currentLineStart = this.lineMatcher.start();
                    this.lastLine = this.lineMatcher.group().trim();
                    found = true;
                    break;
                }
                if (!found) {
                    if (this.currentLineNumber == 0) {
                        this.setupOnlyLine();
                    } else {
                        this.setupLastLine();
                    }
                }
            }
        }

        private void setTextDetailInfo(TextDetail textDetail, int startPos, int endPos, String text) {
            textDetail.associate(this.currentLineNumber, startPos - this.currentLineStart + 1, this.lastLine);
            textDetail.setStartOffset(startPos);
            textDetail.setEndOffset(endPos);
            textDetail.setMarkLength(endPos - startPos);
            textDetail.setMatchedText(text);
        }

        private void setupLastLine() {
            ++this.currentLineNumber;
            this.currentLineStart = this.currentLineEnd + 1;
            this.currentLineEnd = this.charSequence.length();
            this.lastLine = this.charSequence.subSequence(this.currentLineStart, this.currentLineEnd).toString().trim();
        }

        private void setupOnlyLine() {
            this.currentLineNumber = 1;
            String s = this.charSequence.toString();
            this.currentLineStart = 0;
            this.currentLineEnd = s.length();
            this.lastLine = s.trim();
        }
    }

    private class LongCharSequence
    implements CharSequence {
        private long fileSize;
        private FileInputStream fileInputStream;
        private FileChannel fileChannel;
        private Charset charset;
        private int currentStart;
        private CharBuffer currentBuffer;
        private CharsetDecoder currentDecoder;
        private int length;
        private long readBytes;
        private int lastIndex;
        private int returns;
        private int retrieves;
        private int maps;

        public LongCharSequence(File file, Charset charset) throws FileNotFoundException {
            this.currentStart = -1;
            this.currentBuffer = CharBuffer.allocate(1048576);
            this.currentDecoder = null;
            this.length = -1;
            this.readBytes = 0;
            this.lastIndex = 0;
            this.returns = 0;
            this.retrieves = 0;
            this.maps = 0;
            this.charset = charset;
            this.fileInputStream = new FileInputStream(file);
            this.fileChannel = this.fileInputStream.getChannel();
            this.fileSize = file.length();
        }

        public void reset() {
            this.currentDecoder = FastMatcher.this.prepareDecoder(this.charset);
            this.currentDecoder.onUnmappableCharacter(CodingErrorAction.REPLACE);
            this.readBytes = 0;
            this.currentBuffer.clear();
            this.currentStart = -1;
        }

        @Override
        public int length() {
            if (this.length != -1) {
                return this.length;
            }
            long start = System.currentTimeMillis();
            int charactersRead = 0;
            MappedByteBuffer mappedByteBuffer = null;
            CharBuffer charBuffer = CharBuffer.allocate(1048576);
            CharsetDecoder decoder = FastMatcher.this.prepareDecoder(this.charset);
            decoder.onUnmappableCharacter(CodingErrorAction.IGNORE);
            try {
                boolean repeat;
                int readNow;
                for (long bytesRead = 0; bytesRead < this.fileSize; bytesRead += (long)readNow) {
                    CoderResult result;
                    mappedByteBuffer = this.fileChannel.map(FileChannel.MapMode.READ_ONLY, bytesRead, Math.min(0x100000, this.fileSize - bytesRead));
                    do {
                        charBuffer.clear();
                        result = decoder.decode(mappedByteBuffer, charBuffer, bytesRead + 0x100000 >= this.fileSize);
                        if (result.isUnmappable() || result.isMalformed() || result.isError()) {
                            throw new IOException("Error decoding file: " + result.toString() + " ");
                        }
                        if (bytesRead + 0x100000 >= this.fileSize) {
                            LOG.info("Coding end");
                        }
                        charactersRead += charBuffer.position();
                    } while (result.isOverflow());
                    readNow = mappedByteBuffer.position();
                    FastMatcher.this.unmap(mappedByteBuffer);
                }
                charBuffer.clear();
                do {
                    repeat = decoder.flush(charBuffer).isOverflow();
                    charactersRead += charBuffer.position();
                    charBuffer.clear();
                } while (repeat);
            }
            catch (IOException ex) {
                if (mappedByteBuffer != null) {
                    FastMatcher.this.unmap(mappedByteBuffer);
                }
                Exceptions.printStackTrace((Throwable)ex);
            }
            this.length = charactersRead;
            LOG.log(Level.INFO, "Length computed in {0} ms.", System.currentTimeMillis() - start);
            return this.length;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         * Enabled aggressive block sorting
         * Enabled unnecessary exception pruning
         * Enabled aggressive exception aggregation
         */
        @Override
        public char charAt(int index) {
            boolean repeat;
            block16 : {
                if (index < this.lastIndex) {
                    ++this.returns;
                }
                this.lastIndex = index;
                if (index > this.length()) {
                    throw new IndexOutOfBoundsException();
                }
                if (this.isInBuffer(index)) {
                    return this.getFromBuffer(index);
                }
                if (index < this.currentStart || this.currentStart == -1) {
                    this.reset();
                }
                ++this.retrieves;
                Buffer mappedByteBuffer = null;
                try {
                    while (this.readBytes < this.fileSize) {
                        try {
                            CoderResult result;
                            mappedByteBuffer = this.fileChannel.map(FileChannel.MapMode.READ_ONLY, this.readBytes, Math.min(0x100000, this.fileSize - this.readBytes));
                            ++this.maps;
                            do {
                                this.currentStart = this.currentStart == -1 ? 0 : this.currentStart + this.currentBuffer.limit();
                                this.currentBuffer.clear();
                                result = this.currentDecoder.decode((ByteBuffer)mappedByteBuffer, this.currentBuffer, this.readBytes + 0x100000 >= this.fileSize);
                                this.currentBuffer.flip();
                                int readChars = this.currentBuffer.limit();
                                if (this.currentStart + readChars > index) {
                                    char c = this.getFromBuffer(index);
                                    return c;
                                }
                                if (result.isUnmappable()) throw new IOException("Error decoding file: " + result.toString() + " ");
                                if (result.isMalformed()) throw new IOException("Error decoding file: " + result.toString() + " ");
                                if (!result.isError()) continue;
                                throw new IOException("Error decoding file: " + result.toString() + " ");
                            } while (result.isOverflow());
                            continue;
                        }
                        finally {
                            if (mappedByteBuffer == null) continue;
                            int readNow = mappedByteBuffer.position();
                            this.readBytes += (long)readNow;
                            FastMatcher.this.unmap((MappedByteBuffer)mappedByteBuffer);
                            continue;
                        }
                    }
                    break block16;
                }
                catch (IOException ex) {
                    if (mappedByteBuffer != null) {
                        FastMatcher.this.unmap((MappedByteBuffer)mappedByteBuffer);
                    }
                    Exceptions.printStackTrace((Throwable)ex);
                }
                throw new IllegalStateException("Cannot get character.");
            }
            do {
                repeat = this.currentDecoder.flush(this.currentBuffer).isOverflow();
                int size = this.currentBuffer.position();
                if (size + this.currentStart > index) {
                    this.currentBuffer.flip();
                    return this.currentBuffer.get(index - this.currentStart);
                }
                this.currentBuffer.clear();
                this.currentStart += size;
            } while (repeat);
            throw new IllegalStateException("Cannot get character.");
        }

        @Override
        public CharSequence subSequence(int start, int end) {
            if (end - start < 4096) {
                StringBuilder sb = new StringBuilder();
                for (int i = start; i < end; ++i) {
                    sb.append(this.charAt(i));
                }
                return sb.toString();
            }
            throw new IllegalArgumentException("Long subSequences are not supported.");
        }

        @Override
        public String toString() {
            return this.subSequence(0, this.length()).toString();
        }

        public void close() {
            if (this.fileChannel != null) {
                try {
                    this.fileChannel.close();
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            if (this.fileInputStream != null) {
                try {
                    this.fileInputStream.close();
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }

        public void terminate() {
        }

        private boolean isInBuffer(int index) {
            return this.currentStart != -1 && index >= this.currentStart && index < this.currentStart + this.currentBuffer.limit();
        }

        private char getFromBuffer(int index) {
            char c = this.currentBuffer.charAt(index - this.currentStart);
            return c;
        }
    }

    private class FastCharSequence
    implements CharSequence {
        private ByteBuffer bb;
        private int start;

        public FastCharSequence(ByteBuffer bb, int start) {
            this.bb = bb;
            this.start = start;
        }

        @Override
        public int length() {
            return this.bb.limit();
        }

        @Override
        public char charAt(int index) {
            return (char)this.bb.get(this.start + index);
        }

        @Override
        public CharSequence subSequence(int start, int end) {
            return new FastCharSequence(this.bb, start);
        }
    }

}

