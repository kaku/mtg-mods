/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.loaders.DataObject
 */
package org.netbeans.modules.search.matcher;

import java.lang.reflect.Method;
import java.nio.MappedByteBuffer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.api.search.SearchPattern;
import org.netbeans.modules.search.TextDetail;
import org.openide.loaders.DataObject;

final class MatcherUtils {
    private static final Pattern patternCR = Pattern.compile("\r");

    MatcherUtils() {
    }

    static boolean isTrivialPattern(SearchPattern sp) {
        return sp == null || sp.getSearchExpression() == null || sp.getSearchExpression().isEmpty();
    }

    static TextDetail createTextDetail(Matcher matcher, DataObject dataObject, int lineNumber, String lineText, int matcherOffset, SearchPattern searchPattern) {
        String group = matcher.group();
        int start = matcher.start();
        int end = matcher.end();
        int countCR = MatcherUtils.countCR(group);
        int markLength = end - start - countCR;
        assert (dataObject != null);
        TextDetail det = new TextDetail(dataObject, searchPattern);
        det.setMatchedText(group);
        det.setStartOffset(start + matcherOffset);
        det.setEndOffset(end + matcherOffset);
        det.setMarkLength(markLength);
        det.setLineText(lineText);
        det.setLine(lineNumber);
        det.setColumn(start + 1);
        return det;
    }

    private static int countCR(String s) {
        Matcher matcherCR = patternCR.matcher(s);
        int countCR = 0;
        while (matcherCR.find()) {
            ++countCR;
        }
        return countCR;
    }

    public static void unmap(MappedByteBuffer buffer) {
        try {
            Method getCleanerMethod = buffer.getClass().getMethod("cleaner", new Class[0]);
            getCleanerMethod.setAccessible(true);
            Object cleaner = getCleanerMethod.invoke(buffer, new Object[0]);
            cleaner.getClass().getMethod("clean", new Class[0]).invoke(cleaner, new Object[0]);
        }
        catch (Exception e) {
            // empty catch block
        }
    }
}

