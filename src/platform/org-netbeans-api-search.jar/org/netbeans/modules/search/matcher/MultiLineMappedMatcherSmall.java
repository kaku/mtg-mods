/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.queries.FileEncodingQuery
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 */
package org.netbeans.modules.search.matcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.spi.AbstractInterruptibleChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.api.queries.FileEncodingQuery;
import org.netbeans.api.search.SearchPattern;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.TextDetail;
import org.netbeans.modules.search.TextRegexpUtil;
import org.netbeans.modules.search.matcher.AbstractMatcher;
import org.netbeans.modules.search.matcher.MatcherUtils;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;

public class MultiLineMappedMatcherSmall
extends AbstractMatcher {
    private SearchPattern searchPattern;
    private Pattern pattern;
    private int fileMatches = 0;
    private int itemMatches = 0;

    public MultiLineMappedMatcherSmall(SearchPattern searchPattern) {
        this.searchPattern = searchPattern;
        this.pattern = TextRegexpUtil.makeTextPattern(searchPattern);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected MatchingObject.Def checkMeasuredInternal(FileObject fo, SearchListener listener) {
        FileInputStream fis;
        MappedByteBuffer bb = null;
        AbstractInterruptibleChannel fc = null;
        try {
            MatchingObject.Def def;
            listener.fileContentMatchingStarted(fo.getPath());
            File file = FileUtil.toFile((FileObject)fo);
            fis = new FileInputStream(file);
            fc = fis.getChannel();
            int sz = (int)fc.size();
            bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, sz);
            Charset charset = FileEncodingQuery.getEncoding((FileObject)fo);
            CharsetDecoder decoder = this.prepareDecoder(charset);
            decoder.onUnmappableCharacter(CodingErrorAction.IGNORE);
            CharBuffer cb = decoder.decode(bb);
            List<TextDetail> textDetails = this.matchWholeFile(cb, fo);
            if (textDetails == null) {
                MatchingObject.Def def2 = null;
                return def2;
            }
            MatchingObject.Def ex = def = new MatchingObject.Def(fo, decoder.charset(), textDetails);
            return ex;
        }
        catch (Exception e) {
            listener.generalError(e);
            fis = null;
            return fis;
        }
        finally {
            if (fc != null) {
                try {
                    fc.close();
                }
                catch (IOException ex) {
                    listener.generalError(ex);
                }
            }
            MatcherUtils.unmap(bb);
        }
    }

    private List<TextDetail> matchWholeFile(CharSequence cb, FileObject fo) throws DataObjectNotFoundException {
        Matcher textMatcher = this.pattern.matcher(cb);
        DataObject dataObject = null;
        LineInfoHelper lineInfoHelper = new LineInfoHelper(cb);
        LinkedList<TextDetail> textDetails = null;
        while (textMatcher.find()) {
            if (textDetails == null) {
                textDetails = new LinkedList<TextDetail>();
                dataObject = DataObject.find((FileObject)fo);
                ++this.fileMatches;
            }
            ++this.itemMatches;
            TextDetail ntd = new TextDetail(dataObject, this.searchPattern);
            lineInfoHelper.findAndSetPositionInfo(ntd, textMatcher.start(), textMatcher.end(), textMatcher.group());
            textDetails.add(ntd);
            if (this.fileMatches < 500 && this.itemMatches < 5000) continue;
            break;
        }
        return textDetails;
    }

    @Override
    public void terminate() {
    }

    static class LineInfoHelper {
        private static final Pattern linePattern = Pattern.compile("(.*)(\\r\\n|\\n|\\r)");
        private CharSequence charSequence;
        private Matcher lineMatcher;
        private int lastStartPos = 0;
        private int currentLineNumber = 0;
        private int currentLineStart = -1;
        private int currentLineEnd = -1;
        private String lastLine = null;

        public LineInfoHelper(CharSequence charSequence) {
            this.charSequence = charSequence;
            this.lineMatcher = linePattern.matcher(charSequence);
        }

        public void findAndSetPositionInfo(TextDetail textDetail, int startPos, int endPos, String text) {
            if (startPos < this.lastStartPos) {
                throw new IllegalStateException("Start offset lower than the previous one.");
            }
            this.updateStateForPosition(startPos);
            this.setTextDetailInfo(textDetail, startPos, endPos, text);
        }

        private void updateStateForPosition(int pos) {
            if (pos > this.currentLineEnd) {
                boolean found = false;
                while (this.lineMatcher.find()) {
                    ++this.currentLineNumber;
                    this.currentLineEnd = this.lineMatcher.end() - 1;
                    if (this.lineMatcher.end() <= pos) continue;
                    this.currentLineStart = this.lineMatcher.start();
                    this.lastLine = this.lineMatcher.group().trim();
                    found = true;
                    break;
                }
                if (!found) {
                    if (this.currentLineNumber == 0) {
                        this.setupOnlyLine();
                    } else {
                        this.setupLastLine();
                    }
                }
            }
        }

        private void setTextDetailInfo(TextDetail textDetail, int startPos, int endPos, String text) {
            textDetail.associate(this.currentLineNumber, startPos - this.currentLineStart + 1, this.lastLine);
            textDetail.setStartOffset(startPos);
            textDetail.setEndOffset(endPos);
            textDetail.setMarkLength(endPos - startPos);
            textDetail.setMatchedText(text);
        }

        private void setupLastLine() {
            ++this.currentLineNumber;
            this.currentLineStart = this.currentLineEnd + 1;
            this.currentLineEnd = this.charSequence.length();
            this.lastLine = this.charSequence.subSequence(this.currentLineStart, this.currentLineEnd).toString().trim();
        }

        private void setupOnlyLine() {
            this.currentLineNumber = 1;
            String s = this.charSequence.toString();
            this.currentLineStart = 0;
            this.currentLineEnd = s.length();
            this.lastLine = s.trim();
        }
    }

}

