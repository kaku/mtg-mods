/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.queries.FileEncodingQuery
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 */
package org.netbeans.modules.search.matcher;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.api.queries.FileEncodingQuery;
import org.netbeans.api.search.SearchPattern;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.TextDetail;
import org.netbeans.modules.search.TextRegexpUtil;
import org.netbeans.modules.search.matcher.AbstractMatcher;
import org.netbeans.modules.search.matcher.FinishingTextDetailList;
import org.netbeans.modules.search.matcher.LineReader;
import org.netbeans.modules.search.matcher.MatcherUtils;
import org.netbeans.modules.search.matcher.ReadLineBuffer;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;

public class SingleLineStreamMatcher
extends AbstractMatcher {
    private static final int limit = 5000;
    private volatile boolean terminated = false;
    private SearchPattern searchPattern;
    private Pattern pattern;
    private int count = 0;

    public SingleLineStreamMatcher(SearchPattern searchPattern) {
        this.searchPattern = searchPattern;
        this.pattern = TextRegexpUtil.makeTextPattern(searchPattern);
    }

    @Override
    public MatchingObject.Def checkMeasuredInternal(FileObject file, SearchListener listener) {
        Charset charset = FileEncodingQuery.getEncoding((FileObject)file);
        CharsetDecoder decoder = this.prepareDecoder(charset);
        try {
            listener.fileContentMatchingStarted(file.getPath());
            List<TextDetail> textDetails = this.getTextDetailsSL(file, decoder, listener);
            if (textDetails == null) {
                return null;
            }
            return new MatchingObject.Def(file, charset, textDetails);
        }
        catch (CharacterCodingException e) {
            this.handleDecodingError(listener, file, decoder, e);
            return null;
        }
        catch (Exception e) {
            listener.fileContentMatchingError(file.getPath(), e);
            return null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private List<TextDetail> getTextDetailsSL(FileObject fo, CharsetDecoder decoder, SearchListener listener) throws FileNotFoundException, DataObjectNotFoundException, IOException {
        LinkedList<Object> dets;
        dets = null;
        DataObject dataObject = null;
        ReadLineBuffer lineBuffer = new ReadLineBuffer(3);
        FinishingTextDetailList finishList = new FinishingTextDetailList(3);
        boolean canRun = true;
        InputStream stream = fo.getInputStream();
        try {
            LineReader nelr = new LineReader(decoder, stream);
            try {
                LineReader.LineInfo line;
                while ((line = nelr.readNext()) != null && canRun && this.count < 5000) {
                    Object det;
                    Matcher m = this.pattern.matcher(line.getString());
                    while (m.find() && canRun) {
                        if (dets == null) {
                            dets = new LinkedList<Object>();
                            dataObject = DataObject.find((FileObject)fo);
                        }
                        det = MatcherUtils.createTextDetail(m, dataObject, line.getNumber(), line.getString(), line.getFileStart(), this.searchPattern);
                        dets.add(det);
                        for (ReadLineBuffer.Line bl : lineBuffer.getLines()) {
                            det.addSurroundingLine(bl.getNumber(), bl.getText());
                        }
                        finishList.addTextDetail((TextDetail)det);
                        ++this.count;
                    }
                    if (line.getNumber() % 50 == 0) {
                        det = this;
                        synchronized (det) {
                            canRun = !this.terminated;
                        }
                        listener.fileContentMatchingProgress(fo.getPath(), line.getFileEnd());
                    }
                    lineBuffer.addLine(line.getNumber(), line.getString());
                    finishList.nextLineRead(line.getNumber(), line.getString());
                }
            }
            finally {
                nelr.close();
            }
        }
        finally {
            stream.close();
        }
        return dets;
    }

    @Override
    public void terminate() {
        this.terminated = true;
    }
}

