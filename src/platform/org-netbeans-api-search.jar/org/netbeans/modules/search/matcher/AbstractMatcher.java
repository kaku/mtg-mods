/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.search.matcher;

import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.ResultView;
import org.netbeans.modules.search.TextDetail;
import org.openide.filesystems.FileObject;
import org.openide.util.NbBundle;

public abstract class AbstractMatcher {
    private static final Logger LOG = Logger.getLogger(AbstractMatcher.class.getName());
    private long totalTime = 0;
    private int matchingFiles = 0;
    private int matchingItems = 0;
    private boolean strict = true;

    public final MatchingObject.Def check(FileObject file, SearchListener listener) {
        long start = System.currentTimeMillis();
        MatchingObject.Def def = this.checkMeasuredInternal(file, listener);
        long end = System.currentTimeMillis();
        if (def != null) {
            ++this.matchingFiles;
            if (def.getTextDetails() != null && !def.getTextDetails().isEmpty()) {
                ++this.matchingItems;
            }
        }
        this.totalTime += end - start;
        return def;
    }

    protected abstract MatchingObject.Def checkMeasuredInternal(FileObject var1, SearchListener var2);

    public long getTotalTime() {
        return this.totalTime;
    }

    public int getMatchingFiles() {
        return this.matchingFiles;
    }

    public int getMatchingItems() {
        return this.matchingItems;
    }

    public abstract void terminate();

    public boolean isStrict() {
        return this.strict;
    }

    public void setStrict(boolean strict) {
        this.strict = strict;
    }

    public CharsetDecoder prepareDecoder(Charset charset) {
        CharsetDecoder decoder = charset.newDecoder();
        if (this.strict) {
            decoder.onMalformedInput(CodingErrorAction.REPORT);
            decoder.onUnmappableCharacter(CodingErrorAction.REPORT);
        } else {
            decoder.onMalformedInput(CodingErrorAction.IGNORE);
            decoder.onUnmappableCharacter(CodingErrorAction.REPLACE);
        }
        return decoder;
    }

    protected final void handleDecodingError(SearchListener listener, FileObject file, CharsetDecoder decoder, CharacterCodingException e) {
        String charsetName;
        try {
            Charset c;
            charsetName = decoder.isAutoDetecting() && decoder.isCharsetDetected() ? ((c = decoder.detectedCharset()) != null ? c.displayName() : decoder.charset().displayName()) : decoder.charset().displayName();
        }
        catch (Exception ex) {
            LOG.log(Level.INFO, "Failed to obtain actual charset", ex);
            charsetName = decoder == null ? "null" : decoder.toString();
        }
        String msg = NbBundle.getMessage(ResultView.class, (String)"TEXT_INFO_ERROR_ENCODING", (Object)charsetName);
        listener.fileContentMatchingError(file.getPath(), new Exception(msg, e));
        LOG.log(Level.INFO, "{0}; UnmappableCharacterException: {1}", new Object[]{file.getPath(), e.getMessage()});
    }
}

