/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 *  org.openide.nodes.Node
 *  org.openide.windows.OutputWriter
 */
package org.netbeans.modules.search;

import java.awt.EventQueue;
import java.lang.ref.Reference;
import java.lang.reflect.Method;
import java.util.List;
import org.netbeans.modules.search.BasicSearchCriteria;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.SearchDisplayer;
import org.openide.ErrorManager;
import org.openide.nodes.Node;
import org.openide.windows.OutputWriter;

public final class PrintDetailsTask
implements Runnable {
    private static final int BUFFER_SIZE = 8;
    private final List<MatchingObject> objects;
    private final BasicSearchCriteria basicSearchCriteria;
    private final Node[] buffer = new Node[8];
    private int bufPos = 0;
    private SearchDisplayer displayer;
    private volatile boolean interrupted = false;

    public PrintDetailsTask(List<MatchingObject> matchingObjects, BasicSearchCriteria basicCriteria) {
        this.objects = matchingObjects;
        this.basicSearchCriteria = basicCriteria;
    }

    @Override
    public void run() {
        this.displayer = new SearchDisplayer();
        this.callDisplayerFromAWT("prepareOutput");
        int freeBufSpace = 0;
        for (MatchingObject obj : this.objects) {
            Node[] details;
            Node[] allDetails = null;
            if (this.basicSearchCriteria != null && (details = obj.getDetails()) != null && details.length != 0) {
                allDetails = details;
            }
            if (allDetails == null) continue;
            freeBufSpace = this.addToBuffer(allDetails, 0);
            while (freeBufSpace < 0) {
                this.printBuffer();
                int remainderIndex = allDetails.length + freeBufSpace;
                freeBufSpace = this.addToBuffer(allDetails, remainderIndex);
            }
            if (freeBufSpace == 0) {
                this.printBuffer();
            }
            if (!this.interrupted) continue;
            break;
        }
        if (freeBufSpace != 0 && !this.interrupted) {
            int smallBufSize = 8 - freeBufSpace;
            Node[] smallBuffer = new Node[smallBufSize];
            System.arraycopy(this.buffer, 0, smallBuffer, 0, smallBufSize);
            this.displayer.displayNodes(smallBuffer);
        }
        this.callDisplayerFromAWT("finishDisplaying");
    }

    void stop() {
        this.interrupted = true;
    }

    Reference<OutputWriter> getOutputWriterRef() {
        return this.displayer.getOutputWriterRef();
    }

    private int addToBuffer(Node[] detailNodes, int firstIndex) {
        assert (firstIndex >= 0 && firstIndex <= detailNodes.length);
        int nodesToAddCount = detailNodes.length - firstIndex;
        int newBufPos = this.bufPos + nodesToAddCount;
        int remainingSpace = 8 - newBufPos;
        if (remainingSpace <= 0) {
            nodesToAddCount += remainingSpace;
            newBufPos = 0;
        }
        System.arraycopy(detailNodes, firstIndex, this.buffer, this.bufPos, nodesToAddCount);
        this.bufPos = newBufPos;
        return remainingSpace;
    }

    private void printBuffer() {
        this.displayer.displayNodes(this.buffer);
    }

    private Node[] concatNodeArrays(Node[] arrA, Node[] arrB) {
        Node[] result = new Node[arrA.length + arrB.length];
        System.arraycopy(arrA, 0, result, 0, arrA.length);
        System.arraycopy(arrB, 0, result, arrA.length, arrB.length);
        return result;
    }

    private void callDisplayerFromAWT(String methodName) {
        try {
            final Method method = SearchDisplayer.class.getDeclaredMethod(methodName, new Class[0]);
            Runnable runnable = new Runnable(){

                @Override
                public void run() {
                    try {
                        method.invoke(PrintDetailsTask.this.displayer, null);
                    }
                    catch (Exception ex) {
                        ErrorManager.getDefault().notify((Throwable)ex);
                    }
                }
            };
            if (EventQueue.isDispatchThread()) {
                runnable.run();
            } else {
                EventQueue.invokeAndWait(runnable);
            }
        }
        catch (Exception ex) {
            ErrorManager.getDefault().notify((Throwable)ex);
        }
    }

}

