/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.actions.Savable
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 *  org.openide.util.Cancellable
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.search;

import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.api.actions.Savable;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.modules.search.ResultView;
import org.netbeans.modules.search.ResultViewPanel;
import org.netbeans.modules.search.SearchTask;
import org.netbeans.modules.search.ui.FileObjectPropertySet;
import org.netbeans.modules.search.ui.UiUtils;
import org.netbeans.spi.search.SearchFilterDefinition;
import org.netbeans.spi.search.provider.SearchComposition;
import org.netbeans.spi.search.provider.SearchResultsDisplayer;
import org.openide.filesystems.FileObject;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Cancellable;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

class GraphicalSearchListener
extends SearchListener {
    private static final int INFO_EVENT_LIMIT = 100;
    private static final Logger LOG = Logger.getLogger(GraphicalSearchListener.class.getName());
    private static final int PATH_LENGTH_LIMIT = 153;
    private SearchComposition<?> searchComposition;
    private ProgressHandle progressHandle;
    private String longTextMiddle = null;
    private ResultViewPanel resultViewPanel;
    private RootInfoNode rootInfoNode;
    private EventChildren eventChildren;

    public GraphicalSearchListener(SearchComposition<?> searchComposition, ResultViewPanel resultViewPanel) {
        this.searchComposition = searchComposition;
        this.resultViewPanel = resultViewPanel;
        this.rootInfoNode = new RootInfoNode();
    }

    public void searchStarted() {
        this.progressHandle = ProgressHandleFactory.createHandle((String)NbBundle.getMessage(ResultView.class, (String)"TEXT_SEARCHING___"), (Cancellable)new Cancellable(){

            public boolean cancel() {
                GraphicalSearchListener.this.searchComposition.terminate();
                return true;
            }
        });
        this.progressHandle.start();
        this.resultViewPanel.searchStarted();
        this.searchComposition.getSearchResultsDisplayer().searchStarted();
        Collection unsaved = Savable.REGISTRY.lookupAll(Savable.class);
        if (unsaved.size() > 0) {
            String msg = NbBundle.getMessage(ResultView.class, (String)"TEXT_INFO_WARNING_UNSAVED");
            this.eventChildren.addEvent(new EventNode(EventType.WARNING, msg));
        }
    }

    public void searchFinished() {
        if (this.progressHandle != null) {
            this.progressHandle.finish();
            this.progressHandle = null;
        }
        this.resultViewPanel.searchFinished();
        this.searchComposition.getSearchResultsDisplayer().searchFinished();
    }

    @Override
    public void directoryEntered(String path) {
        if (this.progressHandle != null) {
            this.progressHandle.progress(this.shortenPath(path));
        }
    }

    @Override
    public void fileContentMatchingStarted(String fileName) {
        if (this.progressHandle != null) {
            this.progressHandle.progress(this.shortenPath(fileName));
        }
    }

    private String shortenPath(String p) {
        if (p.length() <= 153) {
            return p;
        }
        String mid = this.getLongTextMiddle();
        int halfLength = (153 - mid.length()) / 2;
        return p.substring(0, halfLength) + mid + p.substring(p.length() - halfLength);
    }

    private String getLongTextMiddle() {
        if (this.longTextMiddle == null) {
            this.longTextMiddle = NbBundle.getMessage(SearchTask.class, (String)"TEXT_SEARCH_LONG_STRING_MIDDLE");
        }
        return this.longTextMiddle;
    }

    @Override
    public void generalError(Throwable t) {
        String msg = NbBundle.getMessage(ResultView.class, (String)"TEXT_INFO_ERROR", (Object)t.getMessage());
        this.eventChildren.addEvent(new EventNode(EventType.ERROR, msg));
        LOG.log(Level.INFO, t.getMessage(), t);
    }

    @Override
    public void fileContentMatchingError(String path, Throwable t) {
        String msg = NbBundle.getMessage(ResultView.class, (String)"TEXT_INFO_ERROR_MATCHING", (Object)this.fileName(path), (Object)t.getMessage());
        String tooltip = NbBundle.getMessage(ResultView.class, (String)"TEXT_INFO_ERROR_MATCHING", (Object)path, (Object)t.getMessage());
        this.eventChildren.addEvent(new PathEventNode(EventType.ERROR, msg, path, tooltip));
        String logMsg = path + ": " + t.getMessage();
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, logMsg, t);
        } else {
            LOG.log(Level.INFO, logMsg);
        }
    }

    private String fileName(String filePath) {
        Pattern p = Pattern.compile("(/|\\\\)([^/\\\\]+)(/|\\\\)?$");
        Matcher m = p.matcher(filePath);
        if (m.find()) {
            return m.group(2);
        }
        return filePath;
    }

    @Override
    public void fileSkipped(FileObject fileObject, SearchFilterDefinition filter, String message) {
        this.fileSkipped(fileObject.toURI(), filter, message);
    }

    @Override
    public void fileSkipped(URI uri, SearchFilterDefinition filter, String message) {
        Object[] arrobject = new Object[3];
        arrobject[0] = uri.toString();
        arrobject[1] = filter != null ? filter.getClass().getName() : "";
        arrobject[2] = message != null ? message : "";
        LOG.log(Level.FINE, "{0} skipped {1} {2}", arrobject);
    }

    public Node getInfoNode() {
        return this.rootInfoNode;
    }

    private static String getIconForEventType(EventType eventType) {
        String icon;
        String iconBase = "org/netbeans/modules/search/res/";
        switch (eventType) {
            case INFO: {
                icon = "info.png";
                break;
            }
            case WARNING: {
                icon = "warning.gif";
                break;
            }
            case ERROR: {
                icon = "error.gif";
                break;
            }
            default: {
                icon = "info.png";
            }
        }
        return iconBase + icon;
    }

    private class EventNode
    extends AbstractNode {
        private Node.PropertySet[] propertySets;
        private EventType type;

        public EventNode(EventType type, String message) {
            super(Children.LEAF);
            this.type = type;
            this.setDisplayName(message);
            this.setIconBaseWithExtension(GraphicalSearchListener.getIconForEventType(type));
        }

        public Node.PropertySet[] getPropertySets() {
            if (this.propertySets == null) {
                this.propertySets = this.createPropertySets();
            }
            return this.propertySets;
        }

        protected Node.PropertySet[] createPropertySets() {
            return new Node.PropertySet[0];
        }

        public EventType getType() {
            return this.type;
        }
    }

    private class PathEventNode
    extends EventNode {
        private String path;
        private String tooltip;

        public PathEventNode(EventType type, String message, String path, String tooltip) {
            super(type, message);
            this.path = path;
            this.tooltip = tooltip;
        }

        public String getShortDescription() {
            return this.tooltip;
        }

        @Override
        public Node.PropertySet[] createPropertySets() {
            Node.Property<String> pathProperty = new Node.Property<String>(String.class){

                public boolean canRead() {
                    return true;
                }

                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return PathEventNode.this.path;
                }

                public boolean canWrite() {
                    return false;
                }

                public void setValue(String val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
                    throw new UnsupportedOperationException();
                }

                public String getName() {
                    return "path";
                }
            };
            final Node.Property[] properties = new Node.Property[]{pathProperty};
            Node.PropertySet[] sets = new Node.PropertySet[]{new Node.PropertySet(){

                public Node.Property<?>[] getProperties() {
                    return properties;
                }
            }};
            return sets;
        }

    }

    private class FileObjectEventNode
    extends EventNode {
        private FileObject fileObject;

        public FileObjectEventNode(EventType type, String message, FileObject fileObject) {
            super(type, message);
            this.fileObject = fileObject;
        }

        @Override
        public Node.PropertySet[] createPropertySets() {
            Node.PropertySet[] propertySets = new Node.PropertySet[]{new FileObjectPropertySet(this.fileObject)};
            return propertySets;
        }
    }

    private class EventChildren
    extends Children.Keys<EventNode> {
        private List<EventNode> events;
        EventType worstType;

        private EventChildren() {
            this.events = new ArrayList<EventNode>();
            this.worstType = EventType.INFO;
        }

        public synchronized void addEvent(EventNode event) {
            if (this.events.size() < 100) {
                this.events.add(event);
                this.setKeys(this.events);
                if (event.getType().worseThan(this.worstType)) {
                    this.worstType = event.getType();
                }
                GraphicalSearchListener.this.rootInfoNode.setIconBaseWithExtension(GraphicalSearchListener.getIconForEventType(this.worstType));
            } else if (this.events.size() == 100) {
                this.events.add(new EventNode(EventType.INFO, UiUtils.getText("TEXT_INFO_LIMIT_REACHED")));
                this.setKeys(this.events);
            }
        }

        protected Node[] createNodes(EventNode key) {
            return new Node[]{key};
        }
    }

    private static enum EventType {
        INFO(1),
        WARNING(2),
        ERROR(3);
        
        private int badness;

        private EventType(int badness) {
            this.badness = badness;
        }

        public boolean worseThan(EventType eventType) {
            return this.badness > eventType.badness;
        }
    }

    private class RootInfoNode
    extends AbstractNode {
        public RootInfoNode() {
            this(graphicalSearchListener.new EventChildren());
        }

        private RootInfoNode(EventChildren eventChildren) {
            super((Children)eventChildren);
            GraphicalSearchListener.this.eventChildren = eventChildren;
            this.setDisplayName(UiUtils.getText("TEXT_INFO_TITLE"));
            this.setIcon(EventType.INFO);
        }

        public final void setIcon(EventType mostSeriousEventType) {
            this.setIconBaseWithExtension(GraphicalSearchListener.getIconForEventType(mostSeriousEventType));
        }

        public void fireUpdate() {
        }
    }

}

