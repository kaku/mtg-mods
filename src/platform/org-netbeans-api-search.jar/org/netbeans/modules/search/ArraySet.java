/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

public class ArraySet<E extends Comparable<E>>
extends ArrayList<E>
implements Set<E> {
    private int limit;
    private boolean isSorted;
    private boolean isNullAllowed;

    public ArraySet(int limit) {
        this.limit = limit;
    }

    @Override
    public boolean add(E e) throws IllegalStateException, IllegalArgumentException {
        if (!this.isNullAllowed && e == null) {
            throw new IllegalArgumentException();
        }
        if (this.size() >= this.limit) {
            throw new IllegalStateException();
        }
        if (this.contains(e)) {
            throw new IllegalArgumentException();
        }
        if (ArrayList.super.add(e)) {
            if (this.isSorted) {
                Collections.sort(this);
            }
            return true;
        }
        return false;
    }

    public ArraySet<E> ordering(boolean doOrdering) {
        this.isSorted = doOrdering;
        return this;
    }

    public ArraySet<E> nullIsAllowed(boolean isNullAllowed) {
        this.isNullAllowed = isNullAllowed;
        return this;
    }
}

