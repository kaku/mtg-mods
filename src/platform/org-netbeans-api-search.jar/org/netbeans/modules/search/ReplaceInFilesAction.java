/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 */
package org.netbeans.modules.search;

import org.netbeans.modules.search.FindInFilesAction;
import org.openide.util.HelpCtx;

public class ReplaceInFilesAction
extends FindInFilesAction {
    static final long serialVersionUID = 4554342565076372612L;

    public ReplaceInFilesAction() {
        this(false);
    }

    protected ReplaceInFilesAction(boolean preferScopeSelection) {
        super("LBL_Action_ReplaceInProjects", preferScopeSelection);
    }

    @Override
    protected void initialize() {
        super.initialize();
        this.putProperty("replacing", (Object)Boolean.TRUE, false);
    }

    @Override
    protected String iconResource() {
        return "org/openide/resources/actions/find.gif";
    }

    @Override
    public HelpCtx getHelpCtx() {
        return new HelpCtx("org.netbeans.modules.search.ReplaceInFilesAction");
    }

    public static class Selection
    extends ReplaceInFilesAction {
        public Selection() {
            super(true);
        }
    }

}

