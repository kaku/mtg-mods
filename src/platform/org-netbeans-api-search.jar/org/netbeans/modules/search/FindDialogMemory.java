/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 */
package org.netbeans.modules.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.prefs.Preferences;
import org.netbeans.api.search.SearchPattern;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

public final class FindDialogMemory {
    private static final int maxFileNamePatternCount = 10;
    private static FindDialogMemory singleton;
    private List<String> fileNamePatterns;
    private boolean wholeWords;
    private boolean caseSensitive;
    private boolean preserveCase;
    private SearchPattern.MatchType matchType;
    private boolean textPatternSpecified;
    private boolean replacePatternSpecified;
    private String scopeTypeId;
    private boolean fileNamePatternSpecified;
    private boolean searchInArchives;
    private boolean searchInGenerated;
    private boolean filePathRegex;
    private boolean useIgnoreList;
    private List<String> ignoreList;
    private String textSandboxContent;
    private String pathSandboxContent;
    private String resultsColumnWidths;
    private String resultsColumnWidthsDetails;
    private String resultsColumnWidthsReplacing;
    private int replaceResultsDivider;
    private String resultsViewMode;
    private String provider;
    private boolean openInNewTab;
    private static Preferences prefs;
    private static final String PREFS_NODE = "FindDialogMemory";
    private static final String PROP_WHOLE_WORDS = "whole_words";
    private static final String PROP_CASE_SENSITIVE = "case_sensitive";
    private static final String PROP_PRESERVE_CASE = "preserve_case";
    private static final String PROP_MATCH_TYPE = "match_type";
    private static final String PROP_SCOPE_TYPE_ID = "scope_type_id";
    private static final String PROP_FILENAME_PATTERN_SPECIFIED = "filename_specified";
    private static final String PROP_FILENAME_PATTERN_PREFIX = "filename_pattern_";
    private static final String PROP_REPLACE_PATTERN_PREFIX = "replace_pattern_";
    private static final String PROP_SEARCH_IN_ARCHIVES = "search_in_archives";
    private static final String PROP_SEARCH_IN_GENERATED = "search_in_generated";
    private static final String PROP_FILE_PATH_REGEX = "file_path_regex";
    private static final String PROP_USE_IGNORE_LIST = "use_ignore_list";
    private static final String PROP_IGNORE_LIST_PREFIX = "ignore_list_";
    private static final String PROP_TEXT_SANDBOX_CONTENT = "text_sandbox_content";
    private static final String PROP_PATH_SANDBOX_CONTENT = "path_sandbox_content";
    private static final String PROP_RESULTS_COLUMN_WIDTHS = "results_column_widths";
    private static final String PROP_RESULTS_COLUMN_WIDTHS_DETAILS = "results_column_widths_details";
    private static final String PROP_RESULTS_COLUMN_WIDTHS_REPLACING = "results_column_widths_replacing";
    private static final String PROP_REPLACE_RESULTS_DIVIDER = "replace_results_divider";
    private static final String PROP_RESULTS_VIEW_MODE = "results_view_mode";
    private static final String PROP_PROVIDER = "provider";
    private static final String PROP_OPEN_IN_NEW_TAB = "open_in_new_tab";

    private FindDialogMemory() {
        prefs = NbPreferences.forModule(FindDialogMemory.class).node("FindDialogMemory");
        this.load();
    }

    public static FindDialogMemory getDefault() {
        if (singleton == null) {
            singleton = new FindDialogMemory();
        }
        return singleton;
    }

    private void load() {
        int i;
        String item;
        this.wholeWords = prefs.getBoolean("whole_words", false);
        this.caseSensitive = prefs.getBoolean("case_sensitive", false);
        try {
            String name = prefs.get("match_type", SearchPattern.MatchType.LITERAL.name());
            this.matchType = SearchPattern.MatchType.valueOf(name);
        }
        catch (Exception e) {
            this.matchType = SearchPattern.MatchType.LITERAL;
        }
        this.preserveCase = prefs.getBoolean("preserve_case", false);
        this.scopeTypeId = prefs.get("scope_type_id", "open projects");
        this.fileNamePatternSpecified = prefs.getBoolean("filename_specified", false);
        this.searchInArchives = prefs.getBoolean("search_in_archives", false);
        this.searchInGenerated = prefs.getBoolean("search_in_generated", false);
        this.filePathRegex = prefs.getBoolean("file_path_regex", false);
        this.useIgnoreList = prefs.getBoolean("use_ignore_list", false);
        this.textSandboxContent = prefs.get("text_sandbox_content", this.getText("TextPatternSandbox.textPane.text.default"));
        this.pathSandboxContent = prefs.get("path_sandbox_content", this.getText("PathPatternSandbox.textPane.text.default"));
        this.resultsColumnWidths = prefs.get("results_column_widths", "100:-1:-1:-1:|0:");
        this.resultsColumnWidthsDetails = prefs.get("results_column_widths_details", "100:-1:-1:-1:-1:|0:");
        this.resultsColumnWidthsReplacing = prefs.get("results_column_widths_replacing", "100:-1:-1:-1:-1:|0:");
        this.replaceResultsDivider = prefs.getInt("replace_results_divider", -1);
        this.resultsViewMode = prefs.get("results_view_mode", null);
        this.provider = prefs.get("provider", null);
        this.openInNewTab = prefs.getBoolean("open_in_new_tab", true);
        this.fileNamePatterns = new ArrayList<String>(10);
        this.ignoreList = new ArrayList<String>();
        for (i = 0; i < 10; ++i) {
            String fileNamePattern = prefs.get("filename_pattern_" + i, null);
            if (fileNamePattern == null) continue;
            this.fileNamePatterns.add(fileNamePattern);
        }
        if (this.fileNamePatterns.isEmpty()) {
            FindDialogMemory.addDefaultFileNamePatterns(this.fileNamePatterns);
        }
        i = 0;
        while ((item = prefs.get("ignore_list_" + i, null)) != null) {
            this.ignoreList.add(item);
            ++i;
        }
    }

    public void storeFileNamePattern(String pattern) {
        int index = this.fileNamePatterns.indexOf(pattern);
        if (index != -1) {
            if (index == this.fileNamePatterns.size() - 1) {
                return;
            }
            this.fileNamePatterns.remove(index);
        } else if (this.fileNamePatterns.size() == 10) {
            this.fileNamePatterns.remove(0);
        }
        this.fileNamePatterns.add(pattern);
        for (int i = 0; i < this.fileNamePatterns.size(); ++i) {
            prefs.put("filename_pattern_" + i, this.fileNamePatterns.get(i));
        }
    }

    public List<String> getFileNamePatterns() {
        return this.fileNamePatterns != null ? this.fileNamePatterns : Collections.emptyList();
    }

    private static void addDefaultFileNamePatterns(List<String> l) {
        String[] patterns = new String[]{"*.properties", "*.txt", "*.php", "*.xml", "*.java"};
        int free = 10 - l.size();
        for (int i = 0; i < free && i < patterns.length; ++i) {
            l.add(patterns[i]);
        }
    }

    public boolean isWholeWords() {
        return this.wholeWords;
    }

    public void setWholeWords(boolean wholeWords) {
        this.wholeWords = wholeWords;
        prefs.putBoolean("whole_words", wholeWords);
    }

    public boolean isCaseSensitive() {
        return this.caseSensitive;
    }

    public boolean isPreserveCase() {
        return this.preserveCase;
    }

    public void setCaseSensitive(boolean caseSensitive) {
        this.caseSensitive = caseSensitive;
        prefs.putBoolean("case_sensitive", caseSensitive);
    }

    public void setPreserveCase(boolean preserveCase) {
        this.preserveCase = preserveCase;
        prefs.putBoolean("preserve_case", preserveCase);
    }

    public SearchPattern.MatchType getMatchType() {
        return this.matchType;
    }

    public void setMatchType(SearchPattern.MatchType matchType) {
        this.matchType = matchType;
        prefs.put("match_type", matchType.name());
    }

    public String getScopeTypeId() {
        return this.scopeTypeId;
    }

    public void setScopeTypeId(String scopeTypeId) {
        this.scopeTypeId = scopeTypeId;
        prefs.put("scope_type_id", scopeTypeId);
    }

    public boolean isTextPatternSpecified() {
        return this.textPatternSpecified;
    }

    void setTextPatternSpecified(boolean specified) {
        this.textPatternSpecified = specified;
    }

    public boolean isReplacePatternSpecified() {
        return this.replacePatternSpecified;
    }

    public void setReplacePatternSpecified(boolean replacePatternSpecified) {
        this.replacePatternSpecified = replacePatternSpecified;
    }

    boolean isFileNamePatternSpecified() {
        return this.fileNamePatternSpecified;
    }

    public void setFileNamePatternSpecified(boolean specified) {
        this.fileNamePatternSpecified = specified;
        prefs.putBoolean("filename_specified", specified);
    }

    boolean isSearchInArchives() {
        return this.searchInArchives;
    }

    void setSearchInArchives(boolean searchInArchives) {
        this.searchInArchives = searchInArchives;
        prefs.putBoolean("search_in_generated", searchInArchives);
    }

    boolean isSearchInGenerated() {
        return this.searchInGenerated;
    }

    void setSearchInGenerated(boolean searchInGenerated) {
        this.searchInGenerated = searchInGenerated;
        prefs.putBoolean("search_in_generated", searchInGenerated);
    }

    boolean isFilePathRegex() {
        return this.filePathRegex;
    }

    void setFilePathRegex(boolean filePathRegex) {
        this.filePathRegex = filePathRegex;
        prefs.putBoolean("file_path_regex", filePathRegex);
    }

    boolean IsUseIgnoreList() {
        return this.useIgnoreList;
    }

    void setUseIgnoreList(boolean useIgnoreList) {
        this.useIgnoreList = useIgnoreList;
        prefs.putBoolean("use_ignore_list", useIgnoreList);
    }

    String getTextSandboxContent() {
        return this.textSandboxContent;
    }

    void setTextSandboxContent(String textSandboxContent) {
        this.textSandboxContent = textSandboxContent;
        prefs.put("text_sandbox_content", textSandboxContent);
    }

    String getPathSandboxContent() {
        return this.pathSandboxContent;
    }

    void setPathSandboxContent(String pathSandboxContent) {
        this.pathSandboxContent = pathSandboxContent;
        prefs.put("path_sandbox_content", pathSandboxContent);
    }

    List<String> getIgnoreList() {
        if (this.ignoreList == null) {
            return Collections.emptyList();
        }
        return this.ignoreList;
    }

    void setIgnoreList(List<String> ignoreList) {
        this.ignoreList = ignoreList;
        int i = 0;
        while (prefs.get("ignore_list_" + i, null) != null) {
            prefs.remove("ignore_list_" + i);
            ++i;
        }
        for (int j = 0; j < ignoreList.size(); ++j) {
            prefs.put("ignore_list_" + j, ignoreList.get(j));
        }
    }

    private String getText(String key) {
        return NbBundle.getMessage(FindDialogMemory.class, (String)key);
    }

    public String getResultsColumnWidths() {
        return this.resultsColumnWidths;
    }

    public void setResultsColumnWidths(String resultsColumnWidths) {
        this.resultsColumnWidths = resultsColumnWidths;
        prefs.put("results_column_widths", resultsColumnWidths);
    }

    public String getResultsColumnWidthsDetails() {
        return this.resultsColumnWidthsDetails;
    }

    public void setResultsColumnWidthsDetails(String resultsColumnWidthsDetails) {
        this.resultsColumnWidthsDetails = resultsColumnWidthsDetails;
        prefs.put("results_column_widths_details", resultsColumnWidthsDetails);
    }

    public String getResultsColumnWidthsReplacing() {
        return this.resultsColumnWidthsReplacing;
    }

    public void setResultsColumnWidthsReplacing(String resultsColumnWidthsReplacing) {
        this.resultsColumnWidthsReplacing = resultsColumnWidthsReplacing;
        prefs.put("results_column_widths_replacing", resultsColumnWidthsReplacing);
    }

    public int getReplaceResultsDivider() {
        return this.replaceResultsDivider;
    }

    public void setReplaceResultsDivider(int splitDividerLocation) {
        this.replaceResultsDivider = splitDividerLocation;
        prefs.putInt("replace_results_divider", splitDividerLocation);
    }

    public String getResultsViewMode() {
        return this.resultsViewMode;
    }

    public void setResultsViewMode(String resultsViewMode) {
        this.resultsViewMode = resultsViewMode;
        prefs.put("results_view_mode", resultsViewMode);
    }

    public String getProvider() {
        return this.provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
        prefs.put("provider", provider);
    }

    public boolean isOpenInNewTab() {
        return this.openInNewTab;
    }

    public void setOpenInNewTab(boolean openInNewTab) {
        this.openInNewTab = openInNewTab;
        prefs.putBoolean("open_in_new_tab", openInNewTab);
    }
}

