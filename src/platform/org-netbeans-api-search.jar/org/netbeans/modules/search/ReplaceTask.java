/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.openide.filesystems.FileAlreadyLockedException
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.NbBundle
 *  org.openide.util.UserQuestionException
 */
package org.netbeans.modules.search;

import java.awt.EventQueue;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.ui.BasicReplaceResultsPanel;
import org.openide.filesystems.FileAlreadyLockedException;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.NbBundle;
import org.openide.util.UserQuestionException;

public final class ReplaceTask
implements Runnable {
    private static final int MAX_ERRORS_CHECKED = 20;
    private final List<MatchingObject> matchingObjects;
    private final ProgressHandle progressHandle;
    private final List<String> problems;
    private final BasicReplaceResultsPanel panel;
    private ResultStatus resultStatus = null;

    public ReplaceTask(List<MatchingObject> matchingObjects, BasicReplaceResultsPanel panel) {
        this.matchingObjects = matchingObjects;
        this.panel = panel;
        this.problems = new ArrayList<String>(4);
        this.progressHandle = ProgressHandleFactory.createHandle((String)NbBundle.getMessage(this.getClass(), (String)"LBL_Replacing"));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void run() {
        assert (!EventQueue.isDispatchThread());
        this.progressHandle.start(this.matchingObjects.size() * 2);
        try {
            this.replace();
            assert (this.resultStatus != null);
        }
        finally {
            this.progressHandle.finish();
        }
    }

    private void replace() {
        assert (!EventQueue.isDispatchThread());
        this.checkForErrors();
        if (this.resultStatus == null) {
            FileUtil.runAtomicAction((Runnable)new Runnable(){

                @Override
                public void run() {
                    ReplaceTask.this.doReplace();
                }
            });
        }
    }

    private void checkForErrors() {
        assert (!EventQueue.isDispatchThread());
        int errorsCount = 0;
        for (MatchingObject mo : this.matchingObjects) {
            MatchingObject.InvalidityStatus status = mo.checkValidity();
            if (status == null) continue;
            this.problems.add(status.getDescription(mo.getFileObject().getPath()));
            if (++errorsCount <= 20) continue;
            break;
        }
        if (!this.problems.isEmpty()) {
            this.resultStatus = ResultStatus.PRE_CHECK_FAILED;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void doReplace() {
        assert (!EventQueue.isDispatchThread());
        int i = 0;
        int moSize = this.matchingObjects.size();
        for (MatchingObject obj : this.matchingObjects) {
            String errMessage;
            block15 : {
                int workunit = moSize + i++;
                this.progressHandle.progress(obj.getName(), workunit);
                if (!obj.isSelected() || !obj.isValid()) continue;
                String invDescription = obj.getInvalidityDescription();
                if (invDescription != null) {
                    this.problems.add(invDescription);
                    continue;
                }
                errMessage = null;
                FileLock fileLock = null;
                try {
                    fileLock = obj.lock();
                    MatchingObject.InvalidityStatus status = obj.replace();
                    if (status == null) {
                        obj.write(fileLock);
                        break block15;
                    }
                    errMessage = status.getDescription(obj.getFileObject().getPath());
                }
                catch (FileAlreadyLockedException ex) {
                    errMessage = ReplaceTask.createMsgFileLocked(obj);
                }
                catch (UserQuestionException ex) {
                    errMessage = ReplaceTask.createMsgFileLocked(obj);
                }
                catch (IOException ex) {
                    ex.printStackTrace();
                    errMessage = ex.getLocalizedMessage();
                    if (errMessage == null) {
                        errMessage = ex.getMessage();
                    }
                }
                finally {
                    if (fileLock != null) {
                        fileLock.releaseLock();
                    }
                }
            }
            if (errMessage == null) continue;
            this.problems.add(errMessage);
        }
        this.resultStatus = this.problems.isEmpty() ? ResultStatus.SUCCESS : ResultStatus.PROBLEMS_ENCOUNTERED;
    }

    private static String createMsgFileLocked(MatchingObject matchingObj) {
        return NbBundle.getMessage(ReplaceTask.class, (String)"MSG_cannot_access_file_already_locked", (Object)matchingObj.getName());
    }

    ResultStatus getResultStatus() {
        return this.resultStatus;
    }

    String[] getProblems() {
        return this.problems.isEmpty() ? null : this.problems.toArray(new String[this.problems.size()]);
    }

    BasicReplaceResultsPanel getPanel() {
        return this.panel;
    }

    static enum ResultStatus {
        SUCCESS,
        PRE_CHECK_FAILED,
        PROBLEMS_ENCOUNTERED;
        

        private ResultStatus() {
        }
    }

}

