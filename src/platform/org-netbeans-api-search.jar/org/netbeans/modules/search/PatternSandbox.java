/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.Mnemonics
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.search;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.ItemSelectable;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.SystemColor;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BoxLayout;
import javax.swing.ComboBoxEditor;
import javax.swing.DefaultListCellRenderer;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Document;
import javax.swing.text.Highlighter;
import org.netbeans.api.search.SearchHistory;
import org.netbeans.api.search.SearchPattern;
import org.netbeans.modules.search.BasicSearchCriteria;
import org.netbeans.modules.search.Bundle;
import org.netbeans.modules.search.FindDialogMemory;
import org.netbeans.modules.search.ui.FormLayoutHelper;
import org.netbeans.modules.search.ui.UiUtils;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;

public abstract class PatternSandbox
extends JPanel
implements HierarchyListener {
    private static final Logger LOG = Logger.getLogger(PatternSandbox.class.getName());
    private static final RequestProcessor RP = new RequestProcessor(PatternSandbox.class);
    protected JComboBox<String> cboxPattern;
    private JLabel lblPattern;
    protected JLabel lblHint;
    private JLabel lblOptions;
    private JPanel pnlOptions;
    protected JTextPane textPane;
    private JButton btnApply;
    private JButton btnCancel;
    private JScrollPane textScrollPane;
    protected Highlighter highlighter;
    protected Highlighter.HighlightPainter painter;
    protected BasicSearchCriteria searchCriteria;
    private Color cboxPatternForegroundStd = null;
    private static final Color errorColor = PatternSandbox.chooseErrorColor();

    protected void initComponents() {
        this.cboxPattern = new JComboBox();
        this.cboxPattern.setEditable(true);
        this.cboxPattern.setRenderer(new ShorteningCellRenderer());
        this.lblPattern = new JLabel();
        this.lblPattern.setLabelFor(this.cboxPattern);
        this.lblHint = new JLabel();
        this.lblHint.setForeground(SystemColor.controlDkShadow);
        this.lblOptions = new JLabel();
        this.textPane = new JTextPane();
        this.textScrollPane = new JScrollPane();
        this.textScrollPane.setViewportView(this.textPane);
        this.textScrollPane.setPreferredSize(new Dimension(350, 100));
        this.textScrollPane.setBorder(new BevelBorder(1));
        this.searchCriteria = new BasicSearchCriteria();
        this.initSpecificComponents();
        this.pnlOptions = this.createOptionsPanel();
        this.btnApply = new JButton();
        this.btnCancel = new JButton();
        this.cboxPatternForegroundStd = this.cboxPattern.getEditor().getEditorComponent().getForeground();
        this.initTextPaneContent();
        this.initHighlighter();
        this.setMnemonics();
        this.layoutComponents();
        this.initInteraction();
        this.addHierarchyListener(this);
        this.highlightMatchesLater();
    }

    private void initButtonsInteraction() {
        this.btnCancel.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                PatternSandbox.this.closeDialog();
            }
        });
        this.btnApply.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                PatternSandbox.this.apply();
                PatternSandbox.this.closeDialog();
            }
        });
    }

    private void initTextInputInteraction() {
        this.cboxPattern.getEditor().getEditorComponent().addKeyListener(new KeyAdapter(){

            @Override
            public void keyReleased(KeyEvent e) {
                if (!e.isActionKey()) {
                    PatternSandbox.this.highlightMatchesLater();
                }
            }
        });
        this.cboxPattern.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                PatternSandbox.this.highlightMatchesLater();
            }
        });
        this.textPane.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent e) {
                PatternSandbox.this.highlightMatchesLater();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                PatternSandbox.this.highlightMatchesLater();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                PatternSandbox.this.highlightMatchesLater();
            }
        });
    }

    private void layoutComponents() {
        FormLayoutHelper mainHelper = new FormLayoutHelper(this, FormLayoutHelper.EAGER_COLUMN);
        mainHelper.setAllGaps(true);
        JPanel form = this.createFormPanel();
        JPanel buttonsPanel = this.createButtonsPanel();
        mainHelper.addRow(-1, form.getPreferredSize().height, form.getPreferredSize().height, form);
        mainHelper.addRow(-1, 200, 32767, this.textScrollPane);
        mainHelper.addRow(-1, buttonsPanel.getPreferredSize().height, buttonsPanel.getPreferredSize().height, buttonsPanel);
    }

    private JPanel createButtonsPanel() {
        JPanel buttonsPanel = new JPanel();
        FormLayoutHelper buttonsHelper = new FormLayoutHelper(buttonsPanel, FormLayoutHelper.EAGER_COLUMN, FormLayoutHelper.DEFAULT_COLUMN, FormLayoutHelper.DEFAULT_COLUMN);
        buttonsHelper.setInlineGaps(true);
        buttonsHelper.addRow(this.getExtraButton(), this.btnApply, this.btnCancel);
        return buttonsPanel;
    }

    private JPanel createFormPanel() {
        JPanel form = new JPanel();
        FormLayoutHelper formHelper = new FormLayoutHelper(form, FormLayoutHelper.DEFAULT_COLUMN, FormLayoutHelper.EAGER_COLUMN);
        formHelper.setInlineGaps(true);
        formHelper.addRow(this.lblPattern, this.cboxPattern);
        if (this.lblHint.getText() != null && !"".equals(this.lblHint.getText())) {
            formHelper.addRow(new JLabel(), this.lblHint);
        }
        formHelper.addRow(this.lblOptions, this.pnlOptions);
        return form;
    }

    private void setMnemonics() {
        Mnemonics.setLocalizedText((JLabel)this.lblPattern, (String)this.getPatternLabelText());
        Mnemonics.setLocalizedText((JLabel)this.lblHint, (String)this.getHintLabelText());
        Mnemonics.setLocalizedText((AbstractButton)this.btnCancel, (String)PatternSandbox.getText("PatternSandbox.btnCancel.text"));
        Mnemonics.setLocalizedText((AbstractButton)this.btnApply, (String)PatternSandbox.getText("PatternSandbox.btnApply.text"));
    }

    private void initHighlighter() {
        this.highlighter = new DefaultHighlighter();
        this.painter = new DefaultHighlighter.DefaultHighlightPainter(PatternSandbox.chooseHighlightColor());
        this.textPane.setHighlighter(this.highlighter);
    }

    private void initInteraction() {
        this.initTextInputInteraction();
        this.initButtonsInteraction();
    }

    private void setKeys() {
        KeyStroke k = KeyStroke.getKeyStroke(27, 0);
        String actionKey = "cancel";
        this.getRootPane().getInputMap(1).put(k, actionKey);
        AbstractAction cancelAction = new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent ev) {
                PatternSandbox.this.closeDialog();
            }
        };
        this.getRootPane().getActionMap().put(actionKey, cancelAction);
        this.getRootPane().setDefaultButton(this.btnApply);
    }

    protected void highlightMatchesLater() {
        EventQueue.invokeLater(new Runnable(){

            @Override
            public void run() {
                PatternSandbox.this.highlightMatches();
            }
        });
    }

    protected void highlightMatches() {
        Pattern p;
        this.highlighter.removeAllHighlights();
        Object value = this.cboxPattern.getEditor().getItem();
        if (value == null || value.toString().isEmpty()) {
            return;
        }
        String regex = value.toString();
        try {
            p = this.getPatternForHighlighting(regex);
            if (p == null) {
                throw new NullPointerException();
            }
            this.cboxPattern.getEditor().getEditorComponent().setForeground(this.cboxPatternForegroundStd);
        }
        catch (Throwable e) {
            this.cboxPattern.getEditor().getEditorComponent().setForeground(errorColor);
            return;
        }
        try {
            this.highlightIndividualMatches(p);
        }
        catch (TimeoutExeption e) {
            DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)new NotifyDescriptor.Message((Object)Bundle.MSG_PatternSansboxTimout(), 0));
            LOG.log(Level.INFO, e.getMessage(), e);
        }
    }

    private static String getText(String key) {
        return NbBundle.getMessage(PatternSandbox.class, (String)key);
    }

    private static <T> List<T> reverse(List<T> list) {
        LinkedList<T> ll = new LinkedList<T>();
        for (T t : list) {
            ll.add(0, t);
        }
        return ll;
    }

    private void closeDialog() {
        this.saveTextPaneContent();
        Window w = (Window)SwingUtilities.getAncestorOfClass(Window.class, this);
        if (w != null) {
            w.dispose();
        }
    }

    private static String getSelectedItemAsString(JComboBox<String> cbox) {
        if (cbox.getSelectedItem() != null) {
            return cbox.getSelectedItem().toString();
        }
        return "";
    }

    @Override
    public void hierarchyChanged(HierarchyEvent e) {
        if (e.getID() == 1400) {
            this.setKeys();
        }
    }

    protected JComponent getExtraButton() {
        return new JLabel();
    }

    protected abstract String getPatternLabelText();

    protected abstract String getHintLabelText();

    protected abstract JPanel createOptionsPanel();

    protected abstract void initSpecificComponents();

    protected abstract void apply();

    protected abstract Pattern getPatternForHighlighting(String var1);

    protected abstract void highlightIndividualMatches(Pattern var1);

    protected abstract void initTextPaneContent();

    protected abstract void saveTextPaneContent();

    protected abstract String getTitle();

    public static void openDialog(PatternSandbox sandbox, JComponent baseComponent) {
        JDialog jd = new JDialog((JDialog)SwingUtilities.getAncestorOfClass(JDialog.class, baseComponent));
        jd.add(sandbox);
        jd.setTitle(sandbox.getTitle());
        jd.setModal(true);
        jd.setLocation(baseComponent.getLocationOnScreen());
        jd.pack();
        sandbox.cboxPattern.requestFocusInWindow();
        jd.setVisible(true);
    }

    private static Color chooseErrorColor() {
        return PatternSandbox.chooseColor("nb.search.sandbox.regexp.wrong", Color.RED);
    }

    private static Color chooseHighlightColor() {
        return PatternSandbox.chooseColor("nb.search.sandbox.highlight", Color.ORANGE);
    }

    private static Color chooseColor(String uiManagerKey, Color defaultColor) {
        Color colorFromManager = UIManager.getColor(uiManagerKey);
        return colorFromManager == null ? defaultColor : colorFromManager;
    }

    private class ShorteningCellRenderer
    extends DefaultListCellRenderer {
        private static final int MAX_LENGTH = 50;
        private static final String THREE_DOTS = "...";

        private ShorteningCellRenderer() {
        }

        @Override
        public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            Component component = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            if (value instanceof String && component instanceof JLabel && value.toString().length() > 50) {
                ((JLabel)component).setText(value.toString().substring(0, 50 - "...".length()) + "...");
            }
            return component;
        }
    }

    private static class TimeoutExeption
    extends RuntimeException {
        private TimeoutExeption() {
        }
    }

    private static class TimeLimitedCharSequence
    implements CharSequence {
        private final CharSequence content;
        private final long dateCreated;
        int counter = 0;

        public TimeLimitedCharSequence(CharSequence content) {
            this(content, System.currentTimeMillis());
        }

        public TimeLimitedCharSequence(CharSequence content, long dateCreated) {
            this.content = content == null ? "" : content;
            this.dateCreated = dateCreated;
        }

        @Override
        public int length() {
            return this.content.length();
        }

        @Override
        public char charAt(int index) {
            if (this.counter++ % 1024 == 0 && System.currentTimeMillis() - this.dateCreated > 1000) {
                throw new TimeoutExeption();
            }
            return this.content.charAt(index);
        }

        @Override
        public CharSequence subSequence(int start, int end) {
            return new TimeLimitedCharSequence(this.content.subSequence(start, end), this.dateCreated);
        }
    }

    protected class RegexpModeListener
    implements ItemListener {
        protected RegexpModeListener() {
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            PatternSandbox.this.lblHint.setText(UiUtils.getFileNamePatternsExample(e.getStateChange() == 1));
        }
    }

    static class PathPatternComposer
    extends PathPatternSandbox
    implements ItemListener {
        private JCheckBox chkFileRegexp;

        public PathPatternComposer(String value, boolean pathRegexp) {
            super(value);
            this.pathRegexp = pathRegexp;
            this.initComponents();
            if (pathRegexp) {
                this.searchCriteria.setFileNameRegexp(true);
            }
        }

        @Override
        protected void initSpecificComponents() {
            super.initSpecificComponents();
            this.chkFileRegexp = new JCheckBox();
            this.chkFileRegexp.addItemListener(this);
            this.chkFileRegexp.addItemListener(new RegexpModeListener());
            this.chkFileRegexp.setSelected(this.pathRegexp);
            Mnemonics.setLocalizedText((AbstractButton)this.chkFileRegexp, (String)PatternSandbox.getText("BasicSearchForm.chkFileNameRegex.text"));
        }

        @Override
        protected JPanel createOptionsPanel() {
            JPanel jp = new JPanel();
            jp.setLayout(new FlowLayout(3, 0, 0));
            jp.setLayout(new BoxLayout(jp, 2));
            jp.add(this.chkFileRegexp);
            return jp;
        }

        @Override
        protected final void apply() {
            this.onApply(PatternSandbox.getSelectedItemAsString(this.cboxPattern), this.chkFileRegexp.isSelected());
        }

        protected void onApply(String pattern, boolean regexp) {
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            ItemSelectable is = e.getItemSelectable();
            if (is == this.chkFileRegexp) {
                this.searchCriteria.setFileNameRegexp(this.chkFileRegexp.isSelected());
                this.highlightMatchesLater();
            }
        }

        @Override
        protected String getHintLabelText() {
            return UiUtils.getFileNamePatternsExample(this.pathRegexp);
        }
    }

    public static class PathPatternSandbox
    extends PatternSandbox {
        protected boolean pathRegexp;
        protected String value;

        public PathPatternSandbox(String value) {
            this.value = value;
            this.initComponents();
            this.searchCriteria.setFileNameRegexp(true);
        }

        @Override
        protected void initSpecificComponents() {
            this.cboxPattern.setSelectedItem(this.value);
            FindDialogMemory memory = FindDialogMemory.getDefault();
            for (String s : PatternSandbox.reverse(memory.getFileNamePatterns())) {
                this.cboxPattern.addItem(s);
            }
        }

        @Override
        protected String getPatternLabelText() {
            return PatternSandbox.getText("BasicSearchForm.lblFileNamePattern.text");
        }

        @Override
        protected String getHintLabelText() {
            return "";
        }

        @Override
        protected JPanel createOptionsPanel() {
            return new JPanel();
        }

        @Override
        protected void apply() {
            this.onApply(PatternSandbox.getSelectedItemAsString(this.cboxPattern));
        }

        protected void onApply(String regexp) {
        }

        @Override
        protected void initTextPaneContent() {
            String c = FindDialogMemory.getDefault().getPathSandboxContent();
            this.textPane.setText(c);
        }

        @Override
        protected void saveTextPaneContent() {
            String c = this.textPane.getText();
            FindDialogMemory.getDefault().setPathSandboxContent(c);
        }

        @Override
        protected Pattern getPatternForHighlighting(String patternExpr) {
            this.searchCriteria.setFileNamePattern(patternExpr);
            this.searchCriteria.onOk();
            return this.searchCriteria.getFileNamePattern();
        }

        @Override
        protected void highlightIndividualMatches(Pattern p) {
            String text = this.textPane.getText().replaceAll("\r\n", "\n");
            Pattern sep = Pattern.compile("\n");
            Matcher m = sep.matcher(new TimeLimitedCharSequence(text));
            int lastStart = 0;
            while (m.find()) {
                this.matchLine(text, p, lastStart, m.start());
                lastStart = m.end();
            }
            this.matchLine(text, p, lastStart, text.length());
            this.textPane.repaint();
        }

        private void matchLine(String text, Pattern p, int start, int end) {
            boolean matches;
            if (this.searchCriteria.isFileNameRegexp()) {
                Matcher m = p.matcher(text.substring(start, end));
                matches = m.find();
            } else {
                int lastSlash = text.lastIndexOf("/", end);
                if (lastSlash == -1 || lastSlash < start) {
                    lastSlash = text.lastIndexOf("\\", end);
                }
                int fileNameStart = lastSlash == -1 || lastSlash < start ? start : lastSlash + 1;
                Matcher m = p.matcher(text.substring(fileNameStart, end));
                matches = m.matches();
            }
            if (matches) {
                try {
                    this.highlighter.addHighlight(start, end, this.painter);
                }
                catch (BadLocationException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }

        @Override
        protected JComponent getExtraButton() {
            JPanel jp = new JPanel();
            jp.setLayout(new FlowLayout(3, 0, 0));
            final JButton b = new JButton();
            jp.add(b);
            Mnemonics.setLocalizedText((AbstractButton)b, (String)PatternSandbox.getText("PathPatternSandbox.browseButton.text"));
            b.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    JFileChooser jFileChooser = new JFileChooser();
                    jFileChooser.setFileSelectionMode(0);
                    jFileChooser.setMultiSelectionEnabled(true);
                    jFileChooser.showOpenDialog(b);
                    if (jFileChooser.getSelectedFiles() == null) {
                        return;
                    }
                    for (File f : jFileChooser.getSelectedFiles()) {
                        PathPatternSandbox.this.textPane.setText(PathPatternSandbox.this.textPane.getText() + "\n" + f.getAbsolutePath());
                    }
                }
            });
            return jp;
        }

        @Override
        protected String getTitle() {
            return PatternSandbox.getText("PathPatternSandbox.title");
        }

    }

    static class TextPatternSandbox
    extends PatternSandbox
    implements ItemListener {
        private static final String LINE_SEP = "pattern.sandbox.line.separator";
        private JCheckBox chkMatchCase;
        private String regexp;
        private boolean matchCase;
        private LineEnding lineEnding = null;

        public TextPatternSandbox(String regexp, boolean matchCase) {
            this.regexp = regexp;
            this.matchCase = matchCase;
            this.initComponents();
            this.searchCriteria.setMatchType(SearchPattern.MatchType.REGEXP);
        }

        @Override
        protected void initSpecificComponents() {
            this.chkMatchCase = new JCheckBox();
            this.chkMatchCase.addItemListener(this);
            this.setSpecificMnemonics();
            this.chkMatchCase.setSelected(this.matchCase);
            this.cboxPattern.setSelectedItem(this.regexp);
            SearchHistory history = SearchHistory.getDefault();
            for (SearchPattern sp : history.getSearchPatterns()) {
                this.cboxPattern.addItem(sp.getSearchExpression());
            }
        }

        private void setSpecificMnemonics() {
            Mnemonics.setLocalizedText((AbstractButton)this.chkMatchCase, (String)PatternSandbox.getText("BasicSearchForm.chkCaseSensitive.text"));
        }

        @Override
        protected String getPatternLabelText() {
            return PatternSandbox.getText("BasicSearchForm.lblTextToFind.text");
        }

        @Override
        protected JPanel createOptionsPanel() {
            JPanel p = new JPanel();
            p.setLayout(new FlowLayout(3, 0, 0));
            p.add(this.chkMatchCase);
            return p;
        }

        @Override
        protected final void apply() {
            this.onApply(PatternSandbox.getSelectedItemAsString(this.cboxPattern), this.chkMatchCase.isSelected());
        }

        protected void onApply(String regexpExpr, boolean matchCase) {
        }

        @Override
        protected void initTextPaneContent() {
            String c = FindDialogMemory.getDefault().getTextSandboxContent();
            this.textPane.setText(c);
        }

        @Override
        protected void saveTextPaneContent() {
            String c = this.textPane.getText();
            FindDialogMemory.getDefault().setTextSandboxContent(c);
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            ItemSelectable is = e.getItemSelectable();
            if (is == this.chkMatchCase) {
                this.searchCriteria.setCaseSensitive(this.chkMatchCase.isSelected());
            }
            this.highlightMatchesLater();
        }

        @Override
        protected Pattern getPatternForHighlighting(String patternExpr) {
            this.searchCriteria.onOk();
            this.searchCriteria.setTextPattern(patternExpr);
            return this.searchCriteria.getTextPattern();
        }

        @Override
        protected void highlightIndividualMatches(Pattern p) {
            String text = this.textPane.getText();
            Matcher m = p.matcher(new TimeLimitedCharSequence(text));
            int correction = 0;
            int lastCorrected = 0;
            while (m.find()) {
                try {
                    int start = m.start() - (correction += this.countCRs(text, lastCorrected, m.start()));
                    int end = m.end() - (correction += this.countCRs(text, m.start(), m.end()));
                    lastCorrected = m.end();
                    this.highlighter.addHighlight(start, end, this.painter);
                }
                catch (BadLocationException ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }
            this.textPane.repaint();
        }

        private int countCRs(String text, int from, int to) {
            if (!LineEnding.CRLF.equals((Object)this.lineEnding)) {
                return 0;
            }
            int count = 0;
            for (int i = from; i < to; ++i) {
                if (text.charAt(i) != '\r') continue;
                ++count;
            }
            return count;
        }

        @Override
        protected String getTitle() {
            return PatternSandbox.getText("TextPatternSandbox.title");
        }

        @Override
        protected String getHintLabelText() {
            return "";
        }

        @Override
        protected JComponent getExtraButton() {
            JPanel panel = new JPanel();
            JLabel label = new JLabel();
            Mnemonics.setLocalizedText((JLabel)label, (String)Bundle.LBL_LineEnding());
            JComboBox<LineEnding> cbox = new JComboBox<LineEnding>((E[])new LineEnding[0]);
            cbox.getAccessibleContext().setAccessibleName(Bundle.LBL_LineEnding_accName());
            cbox.setToolTipText(Bundle.LBL_LineEnding_tooltip());
            label.setLabelFor(cbox);
            panel.setLayout(new FlowLayout(3, 0, 0));
            panel.add(label);
            panel.add(cbox);
            this.loadLineEnding(cbox);
            return panel;
        }

        private void updateLineEnding() {
            if (this.lineEnding != null) {
                this.textPane.getDocument().putProperty("__EndOfLine__", this.lineEnding.getSequence());
                this.highlightMatchesLater();
            }
        }

        private void loadLineEnding(final JComboBox<LineEnding> comboBox) {
            RP.post(new Runnable(){

                @Override
                public void run() {
                    String typeStr = NbPreferences.forModule(PatternSandbox.class).get("pattern.sandbox.line.separator", null);
                    if (typeStr != null) {
                        try {
                            TextPatternSandbox.this.lineEnding = LineEnding.valueOf(typeStr);
                        }
                        catch (IllegalArgumentException e) {
                            LOG.log(Level.FINE, "Unknown LEType {0}", typeStr);
                        }
                    }
                    if (TextPatternSandbox.this.lineEnding == null) {
                        TextPatternSandbox.this.lineEnding = Utilities.isWindows() ? LineEnding.CRLF : LineEnding.LF;
                    }
                    EventQueue.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            TextPatternSandbox.this.fillLineEndingComboBox(comboBox);
                        }
                    });
                }

            });
        }

        private void fillLineEndingComboBox(final JComboBox<LineEnding> comboBox) {
            comboBox.addItem(LineEnding.CRLF);
            comboBox.addItem(LineEnding.LF);
            comboBox.addItem(LineEnding.CR);
            comboBox.setSelectedItem((Object)this.lineEnding);
            comboBox.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    TextPatternSandbox.this.lineEnding = (LineEnding)((Object)comboBox.getSelectedItem());
                    TextPatternSandbox.this.updateLineEnding();
                    TextPatternSandbox.this.saveLineEnding();
                }
            });
            this.updateLineEnding();
        }

        private void saveLineEnding() {
            RP.post(new Runnable(){

                @Override
                public void run() {
                    if (TextPatternSandbox.this.lineEnding != null) {
                        NbPreferences.forModule(PatternSandbox.class).put("pattern.sandbox.line.separator", TextPatternSandbox.this.lineEnding.name());
                    }
                }
            });
        }

        private static enum LineEnding {
            CRLF("\r\n", Bundle.LBL_Windows()),
            LF("\n", Bundle.LBL_Unix()),
            CR("\r", Bundle.LBL_MacOld());
            
            private final String sequence;
            private final String name;

            private LineEnding(String sequence, String name) {
                this.sequence = sequence;
                this.name = name;
            }

            public String toString() {
                return this.name;
            }

            public String getSequence() {
                return this.sequence;
            }
        }

    }

}

