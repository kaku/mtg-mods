/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.search;

public interface Selectable {
    public boolean isSelected();

    public void setSelected(boolean var1);

    public void setSelectedRecursively(boolean var1);
}

