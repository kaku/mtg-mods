/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.search;

import org.netbeans.modules.search.ResultModel;

class CleanTask
implements Runnable {
    ResultModel resultModel;

    CleanTask(ResultModel resultModel) {
        this.resultModel = resultModel;
    }

    @Override
    public void run() {
        this.resultModel.close();
    }
}

