/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.search;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.netbeans.api.search.SearchPattern;
import org.netbeans.api.search.SearchRoot;
import org.netbeans.api.search.SearchScopeOptions;
import org.netbeans.api.search.provider.SearchInfo;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.modules.search.BasicSearchCriteria;
import org.netbeans.modules.search.BasicSearchProvider;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.ResultDisplayer;
import org.netbeans.modules.search.matcher.AbstractMatcher;
import org.netbeans.modules.search.ui.UiUtils;
import org.netbeans.spi.search.SearchScopeDefinition;
import org.netbeans.spi.search.provider.SearchComposition;
import org.netbeans.spi.search.provider.SearchProvider;
import org.netbeans.spi.search.provider.SearchResultsDisplayer;
import org.openide.filesystems.FileObject;

public class BasicComposition
extends SearchComposition<MatchingObject.Def> {
    private SearchInfo searchInfo;
    private AbstractMatcher matcher;
    private SearchResultsDisplayer<MatchingObject.Def> displayer = null;
    private BasicSearchCriteria basicSearchCriteria;
    private SearchProvider.Presenter presenter;
    private String scopeDisplayName;
    AtomicBoolean terminated = new AtomicBoolean(false);

    public BasicComposition(SearchInfo searchInfo, AbstractMatcher matcher, BasicSearchCriteria basicSearchCriteria, String scopeDisplayName) {
        this.searchInfo = searchInfo;
        this.matcher = matcher;
        this.basicSearchCriteria = basicSearchCriteria;
        this.scopeDisplayName = scopeDisplayName;
        this.presenter = BasicSearchProvider.createBasicPresenter(basicSearchCriteria.isSearchAndReplace(), basicSearchCriteria.getSearchPattern(), basicSearchCriteria.getReplaceExpr(), basicSearchCriteria.isPreserveCase(), basicSearchCriteria.getSearcherOptions(), basicSearchCriteria.isUseIgnoreList(), "last", new LastScopeDefinition(searchInfo, scopeDisplayName));
    }

    @Override
    public void start(SearchListener listener) {
        Iterable<FileObject> iterable = this.searchInfo.getFilesToSearch(this.basicSearchCriteria.getSearcherOptions(), listener, this.terminated);
        for (FileObject fo : iterable) {
            MatchingObject.Def result = this.matcher.check(fo, listener);
            if (result != null) {
                this.getSearchResultsDisplayer().addMatchingObject(result);
            }
            if (!this.terminated.get()) continue;
            break;
        }
    }

    @Override
    public void terminate() {
        this.terminated.set(true);
        this.matcher.terminate();
    }

    @Override
    public boolean isTerminated() {
        return this.terminated.get();
    }

    @Override
    public synchronized SearchResultsDisplayer<MatchingObject.Def> getSearchResultsDisplayer() {
        if (this.displayer == null) {
            this.displayer = new ResultDisplayer(this.basicSearchCriteria, this);
        }
        return this.displayer;
    }

    public SearchResultsDisplayer<MatchingObject.Def> getDisplayer() {
        return this.displayer;
    }

    public List<FileObject> getRootFiles() {
        LinkedList<FileObject> list = new LinkedList<FileObject>();
        List<SearchRoot> searchRoots = this.searchInfo.getSearchRoots();
        if (searchRoots == null) {
            return Collections.emptyList();
        }
        for (SearchRoot sr : searchRoots) {
            list.add(sr.getFileObject());
        }
        return list;
    }

    public SearchProvider.Presenter getSearchProviderPresenter() {
        return this.presenter;
    }

    public BasicSearchCriteria getBasicSearchCriteria() {
        return this.basicSearchCriteria;
    }

    public SearchInfo getSearchInfo() {
        return this.searchInfo;
    }

    public AbstractMatcher getMatcher() {
        return this.matcher;
    }

    public String getScopeDisplayName() {
        return this.scopeDisplayName;
    }

    private static class LastScopeDefinition
    extends SearchScopeDefinition {
        private static final String PREFIX = UiUtils.getText("LBL_ScopeLastName");
        private SearchInfo searchInfo;
        private String lastScopeDisplayName;

        public LastScopeDefinition(SearchInfo lastSearchInfo, String lastScopeDisplayName) {
            this.searchInfo = lastSearchInfo;
            this.lastScopeDisplayName = this.normalizeTitle(lastScopeDisplayName);
        }

        @Override
        public String getTypeId() {
            return "last";
        }

        @Override
        public String getDisplayName() {
            return PREFIX + (this.lastScopeDisplayName == null ? "" : new StringBuilder().append(": ").append(this.lastScopeDisplayName).toString());
        }

        @Override
        public boolean isApplicable() {
            return true;
        }

        @Override
        public SearchInfo getSearchInfo() {
            return this.searchInfo;
        }

        @Override
        public int getPriority() {
            return 0;
        }

        @Override
        public void clean() {
        }

        private String normalizeTitle(String title) {
            if (title == null || title.equals(PREFIX)) {
                return null;
            }
            if (title.startsWith(PREFIX + ": ")) {
                return title.substring(PREFIX.length() + 2);
            }
            return title;
        }
    }

}

