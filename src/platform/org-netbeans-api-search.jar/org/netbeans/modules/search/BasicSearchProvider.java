/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.DialogDisplayer
 *  org.openide.NotificationLineSupport
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.filesystems.FileObject
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.search;

import java.util.List;
import javax.swing.JComponent;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.search.SearchPattern;
import org.netbeans.api.search.SearchScopeOptions;
import org.netbeans.api.search.provider.SearchInfo;
import org.netbeans.modules.search.BasicComposition;
import org.netbeans.modules.search.BasicSearchCriteria;
import org.netbeans.modules.search.BasicSearchForm;
import org.netbeans.modules.search.FindDialogMemory;
import org.netbeans.modules.search.IgnoreListPanel;
import org.netbeans.modules.search.Manager;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.SearchPanel;
import org.netbeans.modules.search.SearchScopeList;
import org.netbeans.modules.search.SearchTask;
import org.netbeans.modules.search.matcher.AbstractMatcher;
import org.netbeans.modules.search.matcher.DefaultMatcher;
import org.netbeans.modules.search.ui.UiUtils;
import org.netbeans.spi.search.SearchFilterDefinition;
import org.netbeans.spi.search.SearchScopeDefinition;
import org.netbeans.spi.search.provider.SearchComposition;
import org.netbeans.spi.search.provider.SearchProvider;
import org.openide.DialogDisplayer;
import org.openide.NotificationLineSupport;
import org.openide.NotifyDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public class BasicSearchProvider
extends SearchProvider {
    @Override
    public SearchProvider.Presenter createPresenter(boolean replaceMode) {
        return new BasicSearchPresenter(replaceMode, null, null, this, new SearchScopeDefinition[0]);
    }

    @Override
    public boolean isReplaceSupported() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public static SearchProvider.Presenter createBasicPresenter(boolean replacing) {
        return new BasicSearchPresenter(replacing, null, null, new SearchScopeDefinition[0]);
    }

    public static /* varargs */ SearchProvider.Presenter createBasicPresenter(boolean replacing, @NullAllowed SearchPattern searchPattern, @NullAllowed String replaceString, @NullAllowed Boolean preserveCase, @NullAllowed SearchScopeOptions searchScopeOptions, @NullAllowed Boolean useIgnoreList, @NullAllowed String scopeId, @NonNull SearchScopeDefinition ... extraSearchScopes) {
        BasicSearchCriteria bsc = BasicSearchProvider.createCriteria(searchScopeOptions, useIgnoreList, searchPattern, preserveCase, replacing, replaceString);
        return new BasicSearchPresenter(replacing, scopeId, bsc, extraSearchScopes);
    }

    @Override
    public String getTitle() {
        return UiUtils.getText("BasicSearchForm.tabText");
    }

    public static void startSearch(@NonNull SearchPattern searchPattern, @NonNull SearchScopeOptions searchScopeOptions, @NullAllowed String scopeId) throws IllegalArgumentException {
        BasicSearchCriteria criteria = BasicSearchProvider.createCriteria(searchScopeOptions, Boolean.FALSE, searchPattern, null, false, null);
        if (!criteria.isUsable()) {
            throw new IllegalArgumentException("Search cannot be started - No restrictions set.");
        }
        SearchScopeDefinition bestScope = BasicSearchProvider.findBestSearchScope(scopeId);
        BasicComposition composition = new BasicComposition(bestScope.getSearchInfo(), new DefaultMatcher(searchPattern), criteria, null);
        Manager.getInstance().scheduleSearchTask(new SearchTask(composition, false));
    }

    private static BasicSearchCriteria createCriteria(SearchScopeOptions searchScopeOptions, Boolean useIgnoreList, SearchPattern searchPattern, Boolean preserveCase, boolean replacing, String replaceString) {
        BasicSearchCriteria bsc = new BasicSearchCriteria();
        bsc.setFileNamePattern(searchScopeOptions.getPattern());
        bsc.setFileNameRegexp(searchScopeOptions.isRegexp());
        bsc.setTextPattern(searchPattern.getSearchExpression());
        bsc.setCaseSensitive(searchPattern.isMatchCase());
        bsc.setWholeWords(searchPattern.isWholeWords());
        bsc.setMatchType(searchPattern.getMatchType());
        if (preserveCase != null) {
            bsc.setPreserveCase(preserveCase);
        }
        if (useIgnoreList != null) {
            bsc.setUseIgnoreList(useIgnoreList);
        }
        if (replacing) {
            bsc.setReplaceExpr(replaceString);
        } else {
            bsc.setSearchInArchives(searchScopeOptions.isSearchInArchives());
            bsc.setSearchInGenerated(searchScopeOptions.isSearchInGenerated());
        }
        return bsc;
    }

    private static SearchScopeDefinition findBestSearchScope(String preferredscopeId) throws IllegalStateException {
        SearchScopeList ssl = new SearchScopeList(new SearchScopeDefinition[0]);
        SearchScopeDefinition bestScope = null;
        for (SearchScopeDefinition ssd : ssl.getSeachScopeDefinitions()) {
            if (!ssd.isApplicable()) continue;
            if (preferredscopeId != null && ssd.getTypeId().equals(preferredscopeId)) {
                bestScope = ssd;
                break;
            }
            if (bestScope != null) continue;
            bestScope = ssd;
        }
        if (bestScope == null) {
            throw new IllegalStateException("No default search scope");
        }
        return bestScope;
    }

    public static SearchFilterDefinition getIgnoreListFilter() {
        return new IgnoreListFilter();
    }

    private static class IgnoreListFilter
    extends SearchFilterDefinition {
        private IgnoreListPanel.IgnoreListManager ignoreListManager;

        private IgnoreListFilter() {
        }

        @Override
        public boolean searchFile(FileObject file) throws IllegalArgumentException {
            if (file.isFolder()) {
                throw new IllegalArgumentException((Object)file + " is folder, but should be regular file.");
            }
            return !this.isIgnored(file);
        }

        @Override
        public SearchFilterDefinition.FolderResult traverseFolder(FileObject folder) throws IllegalArgumentException {
            if (!folder.isFolder()) {
                throw new IllegalArgumentException((Object)folder + " is file, but should be folder.");
            }
            if (this.isIgnored(folder)) {
                return SearchFilterDefinition.FolderResult.DO_NOT_TRAVERSE;
            }
            return SearchFilterDefinition.FolderResult.TRAVERSE;
        }

        private boolean isIgnored(FileObject fileObj) {
            return this.getIgnoreListManager().isIgnored(fileObj);
        }

        IgnoreListPanel.IgnoreListManager getIgnoreListManager() {
            if (this.ignoreListManager == null) {
                List<String> il = FindDialogMemory.getDefault().getIgnoreList();
                this.ignoreListManager = new IgnoreListPanel.IgnoreListManager(il);
            }
            return this.ignoreListManager;
        }
    }

    private static class BasicSearchPresenter
    extends SearchProvider.Presenter {
        BasicSearchForm form = null;
        private String scopeId;
        private BasicSearchCriteria explicitCriteria;
        private SearchScopeDefinition[] extraSearchScopes;
        private boolean wasUsableAlready = false;

        public /* varargs */ BasicSearchPresenter(boolean replacing, String scopeId, BasicSearchCriteria explicitCriteria, SearchScopeDefinition ... extraSearchScopes) {
            this(replacing, scopeId, explicitCriteria, (BasicSearchProvider)Lookup.getDefault().lookup(BasicSearchProvider.class), extraSearchScopes);
        }

        public /* varargs */ BasicSearchPresenter(boolean replacing, String scopeId, BasicSearchCriteria explicitCriteria, BasicSearchProvider provider, SearchScopeDefinition ... extraSearchScopes) {
            super(provider, replacing);
            this.scopeId = scopeId;
            this.explicitCriteria = explicitCriteria;
            this.extraSearchScopes = extraSearchScopes;
        }

        @Override
        public JComponent getForm() {
            if (this.form == null) {
                String scopeToUse = this.chooseSearchScope(this.scopeId);
                this.form = new BasicSearchForm(scopeToUse, this.isReplacing(), this.explicitCriteria, this.extraSearchScopes);
                this.form.setUsabilityChangeListener(new ChangeListener(){

                    @Override
                    public void stateChanged(ChangeEvent e) {
                        BasicSearchPresenter.this.fireChange();
                    }
                });
            }
            return this.form;
        }

        private String chooseSearchScope(String preferredscopeId) {
            return preferredscopeId == null ? FindDialogMemory.getDefault().getScopeTypeId() : preferredscopeId;
        }

        @Override
        public HelpCtx getHelpCtx() {
            return new HelpCtx(SearchPanel.class.getCanonicalName() + "." + this.isReplacing());
        }

        public SearchComposition<MatchingObject.Def> composeSearch() {
            String msg = Manager.getInstance().mayStartSearching();
            if (msg != null) {
                DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)msg, 1));
                return null;
            }
            this.form.onOk();
            BasicSearchCriteria basicSearchCriteria = this.form.getBasicSearchCriteria();
            SearchScopeOptions so = basicSearchCriteria.getSearcherOptions();
            if (basicSearchCriteria.isUseIgnoreList()) {
                so.addFilter(new IgnoreListFilter());
            }
            SearchInfo ssi = this.form.getSearchInfo();
            DefaultMatcher am = new DefaultMatcher(basicSearchCriteria.getSearchPattern());
            am.setStrict(this.isReplacing());
            return new BasicComposition(ssi, am, basicSearchCriteria, this.form.getSelectedScopeName());
        }

        @Override
        public boolean isUsable(NotificationLineSupport notifySupport) {
            boolean usable = this.form.isUsable();
            BasicSearchCriteria bsc = this.form.getBasicSearchCriteria();
            if (!usable) {
                String msg;
                if (bsc.isTextPatternInvalid()) {
                    msg = "BasicSearchForm.txtErrorTextPattern";
                } else if (bsc.isSearchAndReplace() && bsc.isReplacePatternInvalid()) {
                    msg = "BasicSearchForm.txtErrorReplacePattern";
                } else if (bsc.isFileNamePatternInvalid()) {
                    msg = "BasicSearchForm.txtErrorFileName";
                } else {
                    msg = "BasicSearchForm.txtErrorMissingCriteria";
                    if (!this.wasUsableAlready) {
                        notifySupport.setInformationMessage(UiUtils.getText(msg));
                        return false;
                    }
                }
                notifySupport.setErrorMessage(UiUtils.getText(msg));
            } else {
                this.wasUsableAlready = true;
                if (!bsc.isFileNameRegexp() && !bsc.getFileNamePatternExpr().isEmpty() && bsc.getFileNamePatternExpr().matches("^[\\w-]*$")) {
                    notifySupport.setInformationMessage(UiUtils.getText("BasicSearchForm.txtInfoNoWildcards"));
                } else {
                    notifySupport.clearMessages();
                }
            }
            return usable;
        }

        @Override
        public void clean() {
            super.clean();
        }

    }

}

