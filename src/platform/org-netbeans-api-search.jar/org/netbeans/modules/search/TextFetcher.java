/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.search;

import java.awt.EventQueue;
import java.io.IOException;
import java.nio.channels.ClosedByInterruptException;
import org.netbeans.modules.search.Item;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.TextDetail;
import org.netbeans.modules.search.TextDisplayer;
import org.openide.filesystems.FileObject;
import org.openide.util.RequestProcessor;

final class TextFetcher
implements Runnable {
    private final Item source;
    private final TextDisplayer textDisplayer;
    private final RequestProcessor.Task task;
    private TextDetail location;
    private boolean done = false;
    private boolean cancelled = false;
    private volatile String text = null;

    TextFetcher(Item source, TextDisplayer receiver, RequestProcessor rp) {
        assert (EventQueue.isDispatchThread());
        this.source = source;
        this.textDisplayer = receiver;
        this.location = source.getLocation();
        this.task = rp.post((Runnable)this, 50);
    }

    void cancel() {
        assert (EventQueue.isDispatchThread());
        this.cancelled = true;
        this.task.cancel();
    }

    @Override
    public void run() {
        if (EventQueue.isDispatchThread()) {
            if (this.cancelled) {
                return;
            }
            FileObject fob = this.source.matchingObj.getFileObject();
            String mimeType = fob.getMIMEType();
            if ("text/html".equals(mimeType)) {
                mimeType = "text/plain";
            }
            this.textDisplayer.setText(this.text, mimeType, this.getLocation());
            this.done = true;
        } else {
            if (Thread.interrupted()) {
                return;
            }
            String invalidityDescription = this.source.matchingObj.getInvalidityDescription();
            if (invalidityDescription != null) {
                this.text = invalidityDescription;
            } else {
                try {
                    this.text = this.source.matchingObj.getText();
                }
                catch (ClosedByInterruptException cbie) {
                    this.cancelled = true;
                    return;
                }
                catch (IOException ioe) {
                    this.text = ioe.getLocalizedMessage();
                }
            }
            if (Thread.interrupted()) {
                return;
            }
            EventQueue.invokeLater(this);
        }
    }

    boolean replaceLocation(Item item, TextDisplayer textDisplayer) {
        assert (EventQueue.isDispatchThread());
        if (this.done || textDisplayer != this.textDisplayer) {
            return false;
        }
        boolean result = this.source.matchingObj.getFileObject().equals((Object)item.matchingObj.getFileObject());
        if (result) {
            this.setLocation(item.getLocation());
            this.task.schedule(50);
        }
        return result;
    }

    private synchronized void setLocation(TextDetail location) {
        assert (EventQueue.isDispatchThread());
        this.location = location;
    }

    private synchronized TextDetail getLocation() {
        assert (EventQueue.isDispatchThread());
        return this.location;
    }
}

