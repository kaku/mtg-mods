/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.EditorCookie
 *  org.openide.nodes.Node
 *  org.openide.text.NbDocument
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.WeakListeners
 *  org.openide.windows.TopComponent
 */
package org.netbeans.modules.search;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.ItemSelectable;
import java.awt.LayoutManager;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.ComboBoxEditor;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.search.ReplacePattern;
import org.netbeans.api.search.SearchHistory;
import org.netbeans.api.search.SearchPattern;
import org.netbeans.api.search.provider.SearchInfo;
import org.netbeans.api.search.ui.ComponentUtils;
import org.netbeans.api.search.ui.FileNameController;
import org.netbeans.api.search.ui.ScopeController;
import org.netbeans.api.search.ui.ScopeOptionsController;
import org.netbeans.api.search.ui.SearchPatternController;
import org.netbeans.modules.search.BasicSearchCriteria;
import org.netbeans.modules.search.FindDialogMemory;
import org.netbeans.modules.search.ListComboBoxModel;
import org.netbeans.modules.search.PatternSandbox;
import org.netbeans.modules.search.SearchPanel;
import org.netbeans.modules.search.ui.FormLayoutHelper;
import org.netbeans.modules.search.ui.LinkButtonPanel;
import org.netbeans.modules.search.ui.PatternChangeListener;
import org.netbeans.modules.search.ui.ShorteningCellRenderer;
import org.netbeans.modules.search.ui.TextFieldFocusListener;
import org.netbeans.modules.search.ui.UiUtils;
import org.netbeans.spi.search.SearchScopeDefinition;
import org.openide.cookies.EditorCookie;
import org.openide.nodes.Node;
import org.openide.text.NbDocument;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.WeakListeners;
import org.openide.windows.TopComponent;

final class BasicSearchForm
extends JPanel
implements ChangeListener,
ItemListener {
    private final String preferredSearchScopeType;
    private ChangeListener usabilityChangeListener;
    private BasicSearchCriteria searchCriteria = new BasicSearchCriteria();
    private SearchScopeDefinition[] extraSearchScopes;
    private boolean searchInGeneratedSetAutomatically = false;
    private PropertyChangeListener topComponentRegistryListener;
    private static final Logger watcherLogger = Logger.getLogger("org.netbeans.modules.search.BasicSearchForm.FileNamePatternWatcher");
    private SearchPatternController cboxTextToFind;
    private JComboBox<ReplaceModelItem> cboxReplacement;
    private FileNameController cboxFileNamePattern;
    private JCheckBox chkWholeWords;
    private JCheckBox chkCaseSensitive;
    private TextToFindTypeComboBox textToFindType;
    private JCheckBox chkPreserveCase;
    private JTextComponent replacementPatternEditor;
    protected SearchFormPanel formPanel;
    private JButton btnTestTextToFind;
    private LinkButtonPanel btnTestTextToFindPanel;
    private JLabel lblTextToFind;
    private JLabel lblTextToFindHint;
    private ScopeController cboxScope;
    private JLabel lblFileNamePattern;
    private JLabel lblFileNameHint;
    private JLabel lblScope;
    private JLabel lblReplacement;
    private Color errorTextColor;
    private Color defaultTextColor;
    private boolean invalidTextPattern = false;
    private boolean invalidReplacePattern = false;
    private ScopeOptionsController scopeSettingsPanel;

    /* varargs */ BasicSearchForm(String preferredSearchScopeType, boolean searchAndReplace, BasicSearchCriteria initialCriteria, SearchScopeDefinition ... extraSearchScopes) {
        this.preferredSearchScopeType = preferredSearchScopeType;
        this.extraSearchScopes = extraSearchScopes;
        this.initComponents(searchAndReplace);
        this.initAccessibility(searchAndReplace);
        this.initHistory();
        if (searchAndReplace && this.searchCriteria.getReplaceExpr() == null) {
            this.searchCriteria.setReplaceExpr("");
        }
        this.initInteraction(searchAndReplace);
        this.setValuesOfComponents(initialCriteria, searchAndReplace);
        this.setContextAwareOptions(searchAndReplace);
    }

    private void setValuesOfComponents(BasicSearchCriteria initialCriteria, boolean searchAndReplace) {
        if (initialCriteria != null) {
            this.initValuesFromCriteria(initialCriteria, searchAndReplace);
        } else {
            this.initValuesFromHistory(searchAndReplace);
        }
        if (searchAndReplace) {
            this.updateReplacePatternColor();
        }
        this.useCurrentlySelectedText();
        this.setSearchCriteriaValues();
        this.updateTextToFindInfo();
        this.updateFileNamePatternInfo();
    }

    public void useCurrentlySelectedText() {
        EditorCookie ec;
        String initSearchText;
        JEditorPane recentPane;
        Node[] arr = TopComponent.getRegistry().getActivatedNodes();
        if (arr.length > 0 && (ec = (EditorCookie)arr[0].getLookup().lookup(EditorCookie.class)) != null && (recentPane = NbDocument.findRecentEditorPane((EditorCookie)ec)) != null && (initSearchText = recentPane.getSelectedText()) != null) {
            this.cboxTextToFind.setSearchPattern(SearchPattern.create(initSearchText, false, false, false));
            this.searchCriteria.setTextPattern(initSearchText);
            return;
        }
        this.searchCriteria.setTextPattern(this.cboxTextToFind.getSearchPattern().getSearchExpression());
    }

    private void setContextAwareOptions(boolean searchAndReplace) {
        if (!searchAndReplace) {
            this.updateSearchInGeneratedForActiveTopComponent();
            this.topComponentRegistryListener = new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if (evt.getPropertyName().equals("activated")) {
                        BasicSearchForm.this.updateSearchInGeneratedForActiveTopComponent();
                    }
                }
            };
            TopComponent.getRegistry().addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.topComponentRegistryListener, (Object)TopComponent.getRegistry()));
        }
    }

    private void updateSearchInGeneratedForActiveTopComponent() {
        assert (this.searchCriteria != null && !this.searchCriteria.isSearchAndReplace());
        TopComponent tc = TopComponent.getRegistry().getActivated();
        if (tc != null && tc.getHelpCtx() != null && "ProjectTab_Files".equals(tc.getHelpCtx().getHelpID())) {
            if (!this.scopeSettingsPanel.isSearchInGenerated()) {
                this.scopeSettingsPanel.setSearchInGenerated(true);
                this.searchInGeneratedSetAutomatically = true;
            }
        } else if (this.searchInGeneratedSetAutomatically) {
            this.scopeSettingsPanel.setSearchInGenerated(false);
            this.searchInGeneratedSetAutomatically = false;
        }
    }

    private void initComponents(boolean searchAndReplace) {
        this.lblTextToFind = new JLabel();
        this.cboxTextToFind = ComponentUtils.adjustComboForSearchPattern(new JComboBox());
        this.lblTextToFind.setLabelFor((Component)this.cboxTextToFind.getComponent());
        this.btnTestTextToFind = new JButton();
        this.lblTextToFindHint = new JLabel();
        this.lblTextToFindHint.setForeground(SystemColor.controlDkShadow);
        if (searchAndReplace) {
            this.lblReplacement = new JLabel();
            this.cboxReplacement = new JComboBox();
            this.cboxReplacement.setEditable(true);
            this.cboxReplacement.setRenderer(new ShorteningCellRenderer());
            this.lblReplacement.setLabelFor(this.cboxReplacement);
            this.chkPreserveCase = new JCheckBox();
        }
        this.lblScope = new JLabel();
        this.cboxScope = ComponentUtils.adjustComboForScope(new JComboBox(), this.preferredSearchScopeType, this.extraSearchScopes);
        this.lblScope.setLabelFor((Component)this.cboxScope.getComponent());
        this.lblFileNamePattern = new JLabel();
        this.lblFileNameHint = new JLabel();
        this.lblFileNameHint.setForeground(SystemColor.controlDkShadow);
        this.cboxFileNamePattern = ComponentUtils.adjustComboForFileName(new JComboBox());
        this.lblFileNamePattern.setLabelFor((Component)this.cboxFileNamePattern.getComponent());
        this.chkWholeWords = new JCheckBox();
        this.chkCaseSensitive = new JCheckBox();
        this.textToFindType = new TextToFindTypeComboBox();
        TextPatternCheckBoxGroup.bind(this.chkCaseSensitive, this.chkWholeWords, this.textToFindType, this.chkPreserveCase);
        this.setMnemonics(searchAndReplace);
        this.initFormPanel(searchAndReplace);
        this.updateTextToFindInfo();
        this.add(this.formPanel);
        if (this.cboxReplacement != null) {
            Component cboxEditorComp = this.cboxReplacement.getEditor().getEditorComponent();
            this.replacementPatternEditor = (JTextComponent)cboxEditorComp;
        }
        this.setLayout(new BoxLayout(this, 3));
    }

    protected void initFormPanel(boolean searchAndReplace) {
        this.formPanel = new SearchFormPanel();
        this.formPanel.addRow(this.lblTextToFind, (JComponent)this.cboxTextToFind.getComponent());
        JPanel hintAndButtonPanel = this.initHintAndButtonPanel();
        this.formPanel.addRow(new JLabel(), hintAndButtonPanel);
        this.initContainingTextOptionsRow(searchAndReplace);
        if (searchAndReplace) {
            this.formPanel.addRow(this.lblReplacement, this.cboxReplacement);
        }
        this.formPanel.addSeparator();
        this.formPanel.addRow(this.lblScope, (JComponent)this.cboxScope.getComponent());
        this.initScopeOptionsRow(searchAndReplace);
        this.formPanel.addSeparator();
        this.formPanel.addRow(this.lblFileNamePattern, (JComponent)this.cboxFileNamePattern.getComponent());
        this.formPanel.addRow(new JLabel(), this.lblFileNameHint);
        this.formPanel.addRow(new JLabel(), this.scopeSettingsPanel.getFileNameComponent());
        this.formPanel.addEmptyLine();
    }

    private void initContainingTextOptionsRow(boolean searchAndReplace) {
        JPanel jp = new JPanel();
        JPanel typeWithLabelPanel = new JPanel();
        typeWithLabelPanel.setLayout(new BoxLayout(typeWithLabelPanel, 2));
        JLabel typeLabel = new JLabel();
        typeLabel.setLabelFor(this.textToFindType);
        this.lclz(typeLabel, "BasicSearchForm.textToFindType.label.text");
        typeLabel.setBorder(new EmptyBorder(0, 10, 0, 5));
        typeWithLabelPanel.add(typeLabel);
        typeWithLabelPanel.add(this.textToFindType);
        if (searchAndReplace) {
            FormLayoutHelper flh = new FormLayoutHelper(jp, FormLayoutHelper.DEFAULT_COLUMN, FormLayoutHelper.DEFAULT_COLUMN);
            flh.addRow(this.chkCaseSensitive, this.chkPreserveCase);
            flh.addRow(this.chkWholeWords, typeWithLabelPanel);
            jp.setMaximumSize(jp.getMinimumSize());
            this.formPanel.addRow(new JLabel(), jp);
        } else {
            jp.setLayout(new BoxLayout(jp, 2));
            jp.add(this.chkCaseSensitive);
            jp.add(this.chkWholeWords);
            jp.add(typeWithLabelPanel);
            this.formPanel.addRow(new JLabel(), jp);
        }
    }

    private void initScopeOptionsRow(boolean searchAndReplace) {
        this.scopeSettingsPanel = ComponentUtils.adjustPanelsForOptions(new JPanel(), new JPanel(), searchAndReplace, this.cboxFileNamePattern);
        this.formPanel.addRow(new JLabel(), (JComponent)this.scopeSettingsPanel.getComponent());
    }

    private void initAccessibility(boolean searchAndReplace) {
        this.chkCaseSensitive.getAccessibleContext().setAccessibleDescription(UiUtils.getText("BasicSearchForm.chkCaseSensitive.AccessibleDescription"));
        this.textToFindType.getAccessibleContext().setAccessibleDescription(UiUtils.getText("BasicSearchForm.textToFindType.AccessibleDescription"));
        this.chkWholeWords.getAccessibleContext().setAccessibleDescription(UiUtils.getText("BasicSearchForm.chkWholeWords.AccessibleDescription"));
        if (searchAndReplace) {
            this.cboxReplacement.getAccessibleContext().setAccessibleDescription(UiUtils.getText("BasicSearchForm.cbox.Replacement.AccessibleDescription"));
            this.chkPreserveCase.getAccessibleContext().setAccessibleDescription(UiUtils.getText("BasicSearchForm.chkPreserveCase.AccessibleDescription"));
        }
    }

    private void initValuesFromCriteria(BasicSearchCriteria initialCriteria, boolean searchAndReplace) {
        this.cboxTextToFind.setSearchPattern(initialCriteria.getSearchPattern());
        if (this.cboxReplacement != null) {
            this.cboxReplacement.setSelectedItem(new ReplaceModelItem(ReplacePattern.create(initialCriteria.getReplaceExpr(), initialCriteria.isPreserveCase())));
        }
        BasicSearchForm.selectChk(this.chkPreserveCase, initialCriteria.isPreserveCase());
        this.scopeSettingsPanel.setFileNameRegexp(initialCriteria.isFileNameRegexp());
        this.scopeSettingsPanel.setUseIgnoreList(initialCriteria.isUseIgnoreList());
        this.cboxFileNamePattern.setRegularExpression(initialCriteria.isFileNameRegexp());
        this.cboxFileNamePattern.setFileNamePattern(initialCriteria.getFileNamePatternExpr());
        if (!searchAndReplace) {
            this.scopeSettingsPanel.setSearchInArchives(initialCriteria.isSearchInArchives());
            this.scopeSettingsPanel.setSearchInGenerated(initialCriteria.isSearchInGenerated());
        }
    }

    private static void selectChk(JCheckBox checkbox, boolean value) {
        if (checkbox != null) {
            checkbox.setSelected(value);
        }
    }

    private void initInteraction(boolean searchAndReplace) {
        TextFieldFocusListener focusListener = new TextFieldFocusListener();
        if (this.replacementPatternEditor != null) {
            this.replacementPatternEditor.addFocusListener(focusListener);
        }
        if (this.replacementPatternEditor != null) {
            this.replacementPatternEditor.getDocument().addDocumentListener(new ReplacementPatternListener());
        }
        this.textToFindType.addItemListener(this);
        this.cboxTextToFind.bindMatchTypeComboBox(this.textToFindType);
        this.cboxTextToFind.bind(SearchPatternController.Option.MATCH_CASE, this.chkCaseSensitive);
        this.cboxTextToFind.bind(SearchPatternController.Option.WHOLE_WORDS, this.chkWholeWords);
        this.textToFindType.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
            }
        });
        boolean regexp = this.textToFindType.isRegexp();
        boolean caseSensitive = this.chkCaseSensitive.isSelected();
        this.chkWholeWords.setEnabled(!regexp);
        if (searchAndReplace) {
            this.chkPreserveCase.addItemListener(this);
            this.chkPreserveCase.setEnabled(!regexp && !caseSensitive);
        }
        this.searchCriteria.setUsabilityChangeListener(this);
        this.scopeSettingsPanel.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                BasicSearchForm.this.searchCriteria.setSearchInArchives(BasicSearchForm.this.scopeSettingsPanel.isSearchInArchives());
                BasicSearchForm.this.searchCriteria.setSearchInGenerated(BasicSearchForm.this.scopeSettingsPanel.isSearchInGenerated());
                BasicSearchForm.this.searchCriteria.setUseIgnoreList(BasicSearchForm.this.scopeSettingsPanel.isUseIgnoreList());
            }
        });
        this.cboxFileNamePattern.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                BasicSearchForm.this.searchCriteria.setFileNamePattern(BasicSearchForm.this.cboxFileNamePattern.getFileNamePattern());
                BasicSearchForm.this.searchCriteria.setFileNameRegexp(BasicSearchForm.this.cboxFileNamePattern.isRegularExpression());
                BasicSearchForm.this.updateFileNamePatternInfo();
            }
        });
        this.cboxTextToFind.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                SearchPattern sp = BasicSearchForm.this.cboxTextToFind.getSearchPattern();
                BasicSearchForm.this.searchCriteria.setTextPattern(sp.getSearchExpression());
                BasicSearchForm.this.searchCriteria.setMatchType(sp.getMatchType());
                BasicSearchForm.this.searchCriteria.setWholeWords(sp.isWholeWords());
                BasicSearchForm.this.searchCriteria.setCaseSensitive(sp.isMatchCase());
            }
        });
        this.initButtonInteraction();
    }

    private void initButtonInteraction() {
        this.btnTestTextToFind.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                BasicSearchForm.this.openTextPatternSandbox();
            }
        });
    }

    private void openTextPatternSandbox() {
        SearchPattern sp = this.cboxTextToFind.getSearchPattern();
        String expr = sp.getSearchExpression() == null ? "" : sp.getSearchExpression();
        boolean matchCase = this.chkCaseSensitive.isSelected();
        PatternSandbox.openDialog(new PatternSandbox.TextPatternSandbox(expr, matchCase){

            @Override
            protected void onApply(String newExpr, boolean newMatchCase) {
                BasicSearchForm.this.cboxTextToFind.setSearchPattern(SearchPattern.create(newExpr, false, newMatchCase, true));
            }
        }, this.btnTestTextToFind);
    }

    private void initHistory() {
        ArrayList<ReplaceModelItem> entries = new ArrayList<ReplaceModelItem>(10);
        if (this.cboxReplacement != null) {
            for (ReplacePattern replacePattern : SearchHistory.getDefault().getReplacePatterns()) {
                entries.add(0, new ReplaceModelItem(replacePattern));
            }
            if (!entries.isEmpty()) {
                this.cboxReplacement.setModel(new ListComboBoxModel(entries, true));
            }
        }
    }

    private void initValuesFromHistory(boolean searchAndReplace) {
        FindDialogMemory memory = FindDialogMemory.getDefault();
        if (memory.isFileNamePatternSpecified() && ((JComboBox)this.cboxFileNamePattern.getComponent()).getItemCount() != 0) {
            ((JComboBox)this.cboxFileNamePattern.getComponent()).setSelectedIndex(0);
        }
        this.cboxFileNamePattern.setRegularExpression(memory.isFilePathRegex());
        if (this.cboxReplacement != null && this.cboxReplacement.getItemCount() != 0 && FindDialogMemory.getDefault().isReplacePatternSpecified()) {
            this.cboxReplacement.setSelectedIndex(0);
        }
        this.chkWholeWords.setSelected(memory.isWholeWords());
        this.chkCaseSensitive.setSelected(memory.isCaseSensitive());
        this.textToFindType.setSelectedItem((Object)memory.getMatchType());
        this.scopeSettingsPanel.setFileNameRegexp(memory.isFilePathRegex());
        this.scopeSettingsPanel.setUseIgnoreList(memory.IsUseIgnoreList());
        if (searchAndReplace) {
            this.chkPreserveCase.setSelected(memory.isPreserveCase());
        } else {
            this.scopeSettingsPanel.setSearchInArchives(memory.isSearchInArchives());
            this.scopeSettingsPanel.setSearchInGenerated(memory.isSearchInGenerated());
        }
    }

    private void setSearchCriteriaValues() {
        this.searchCriteria.setWholeWords(this.chkWholeWords.isSelected());
        this.searchCriteria.setCaseSensitive(this.chkCaseSensitive.isSelected());
        this.searchCriteria.setMatchType(this.textToFindType.getSelectedMatchType());
        this.searchCriteria.setFileNameRegexp(this.scopeSettingsPanel.isFileNameRegExp());
        this.searchCriteria.setUseIgnoreList(this.scopeSettingsPanel.isUseIgnoreList());
        this.searchCriteria.setSearchInArchives(this.scopeSettingsPanel.isSearchInArchives());
        this.searchCriteria.setSearchInGenerated(this.scopeSettingsPanel.isSearchInGenerated());
        if (this.chkPreserveCase != null) {
            this.searchCriteria.setPreserveCase(this.chkPreserveCase.isSelected());
        }
    }

    @Override
    public boolean requestFocusInWindow() {
        return ((JComboBox)this.cboxTextToFind.getComponent()).requestFocusInWindow();
    }

    private void updateReplacePatternColor() {
        boolean wasInvalid = this.invalidReplacePattern;
        this.invalidReplacePattern = this.searchCriteria.isReplacePatternInvalid();
        if (this.invalidReplacePattern != wasInvalid) {
            if (this.defaultTextColor == null) {
                assert (!wasInvalid);
                this.defaultTextColor = this.cboxReplacement.getForeground();
            }
            this.replacementPatternEditor.setForeground(this.invalidReplacePattern ? this.getErrorTextColor() : this.defaultTextColor);
        }
    }

    private static boolean isBackrefSyntaxUsed(String text) {
        int index;
        int len = text.length();
        if (len < 2) {
            return false;
        }
        String textToSearch = text.substring(0, len - 1);
        int startIndex = 0;
        while ((index = textToSearch.indexOf(92, startIndex)) != -1) {
            char c = text.charAt(index + 1);
            if (c == '\\') {
                startIndex = index + 1;
                continue;
            }
            if (c >= '0' && c <= '9') {
                return true;
            }
            startIndex = index + 2;
        }
        return false;
    }

    private Color getErrorTextColor() {
        if (this.errorTextColor == null) {
            this.errorTextColor = UIManager.getDefaults().getColor("TextField.errorForeground");
            if (this.errorTextColor == null) {
                this.errorTextColor = Color.RED;
            }
        }
        return this.errorTextColor;
    }

    void setUsabilityChangeListener(ChangeListener l) {
        this.usabilityChangeListener = l;
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if (this.usabilityChangeListener != null) {
            this.usabilityChangeListener.stateChanged(new ChangeEvent(this));
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        boolean selected;
        ItemSelectable toggle = e.getItemSelectable();
        boolean bl = selected = e.getStateChange() == 1;
        if (toggle == this.textToFindType) {
            if (this.cboxReplacement != null) {
                this.updateReplacePatternColor();
            }
            this.updateTextToFindInfo();
        } else if (toggle == this.chkPreserveCase) {
            this.searchCriteria.setPreserveCase(selected);
        } else assert (false);
    }

    private void updateTextToFindInfo() {
        String key;
        switch (this.cboxTextToFind.getSearchPattern().getMatchType()) {
            case LITERAL: {
                key = "BasicSearchForm.cboxTextToFind.info.literal";
                break;
            }
            case BASIC: {
                key = "BasicSearchForm.cboxTextToFind.info";
                break;
            }
            case REGEXP: {
                key = "BasicSearchForm.cboxTextToFind.info.re";
                break;
            }
            default: {
                key = "BasicSearchForm.cboxTextToFind.info";
            }
        }
        String text = UiUtils.getText(key);
        ((JComboBox)this.cboxTextToFind.getComponent()).setToolTipText(text);
        this.lblTextToFindHint.setText(text);
        this.btnTestTextToFindPanel.setButtonEnabled(this.searchCriteria.getSearchPattern().isRegExp());
    }

    private void updateFileNamePatternInfo() {
        this.lblFileNameHint.setText(UiUtils.getFileNamePatternsExample(this.cboxFileNamePattern.isRegularExpression()));
    }

    void onOk() {
        this.searchCriteria.onOk();
        FindDialogMemory memory = FindDialogMemory.getDefault();
        if (this.searchCriteria.isTextPatternUsable()) {
            SearchHistory.getDefault().add(this.getCurrentSearchPattern());
            memory.setTextPatternSpecified(true);
        } else {
            memory.setTextPatternSpecified(false);
        }
        if (this.searchCriteria.isFileNamePatternUsable()) {
            memory.storeFileNamePattern(this.searchCriteria.getFileNamePatternExpr());
            memory.setFileNamePatternSpecified(true);
        } else {
            memory.setFileNamePatternSpecified(false);
        }
        if (this.replacementPatternEditor != null) {
            String replaceText = this.replacementPatternEditor.getText();
            SearchHistory.getDefault().addReplace(ReplacePattern.create(replaceText, this.chkPreserveCase.isSelected()));
            FindDialogMemory.getDefault().setReplacePatternSpecified(replaceText != null && !replaceText.isEmpty());
        }
        memory.setWholeWords(this.chkWholeWords.isSelected());
        memory.setCaseSensitive(this.chkCaseSensitive.isSelected());
        memory.setMatchType(this.textToFindType.getSelectedMatchType());
        if (this.searchCriteria.isSearchAndReplace()) {
            memory.setPreserveCase(this.chkPreserveCase.isSelected());
        } else {
            memory.setSearchInArchives(this.scopeSettingsPanel.isSearchInArchives());
            if (!this.searchInGeneratedSetAutomatically) {
                memory.setSearchInGenerated(this.scopeSettingsPanel.isSearchInGenerated());
            }
        }
        memory.setFilePathRegex(this.scopeSettingsPanel.isFileNameRegExp());
        memory.setUseIgnoreList(this.scopeSettingsPanel.isUseIgnoreList());
        if (this.cboxScope.getSelectedScopeId() != null && !SearchPanel.isOpenedForSelection()) {
            memory.setScopeTypeId(this.cboxScope.getSelectedScopeId());
        }
    }

    private SearchPattern getCurrentSearchPattern() {
        return this.cboxTextToFind.getSearchPattern();
    }

    public SearchInfo getSearchInfo() {
        return this.cboxScope.getSearchInfo();
    }

    public String getSelectedScopeName() {
        return this.cboxScope.getSelectedScopeTitle();
    }

    BasicSearchCriteria getBasicSearchCriteria() {
        return this.searchCriteria;
    }

    boolean isUsable() {
        return this.cboxScope.getSearchInfo() != null && this.searchCriteria.isUsable();
    }

    private void setMnemonics(boolean searchAndReplace) {
        this.lclz(this.lblTextToFind, "BasicSearchForm.lblTextToFind.text");
        this.lclz(this.lblScope, "BasicSearchForm.lblScope.text");
        this.lclz(this.lblFileNamePattern, "BasicSearchForm.lblFileNamePattern.text");
        this.lclz(this.chkWholeWords, "BasicSearchForm.chkWholeWords.text");
        this.lclz(this.chkCaseSensitive, "BasicSearchForm.chkCaseSensitive.text");
        this.btnTestTextToFind.setText(UiUtils.getHtmlLink("BasicSearchForm.btnTestTextToFind.text"));
        this.btnTestTextToFind.setToolTipText(UiUtils.getText("BasicSearchForm.btnTestTextToFind.tooltip"));
        if (searchAndReplace) {
            this.lclz(this.lblReplacement, "BasicSearchForm.lblReplacement.text");
            this.lclz(this.chkPreserveCase, "BasicSearchForm.chkPreserveCase.text");
        }
    }

    private void lclz(AbstractButton ab, String msg) {
        UiUtils.lclz(ab, msg);
    }

    private void lclz(JLabel l, String msg) {
        UiUtils.lclz(l, msg);
    }

    private JPanel initHintAndButtonPanel() {
        this.btnTestTextToFindPanel = new LinkButtonPanel(this.btnTestTextToFind);
        this.btnTestTextToFindPanel.setButtonEnabled(this.searchCriteria.getSearchPattern().isRegExp());
        this.lblTextToFindHint.setMaximumSize(new Dimension(Integer.MAX_VALUE, (int)this.btnTestTextToFindPanel.getPreferredSize().getHeight()));
        JPanel hintAndButtonPanel = new JPanel();
        hintAndButtonPanel.setLayout(new BoxLayout(hintAndButtonPanel, 2));
        hintAndButtonPanel.add(this.lblTextToFindHint);
        hintAndButtonPanel.add(this.btnTestTextToFindPanel);
        return hintAndButtonPanel;
    }

    private static class TextToFindTypeComboBox
    extends JComboBox<SearchPattern.MatchType> {
        public TextToFindTypeComboBox() {
            super((E[])new SearchPattern.MatchType[]{SearchPattern.MatchType.LITERAL, SearchPattern.MatchType.BASIC, SearchPattern.MatchType.REGEXP});
        }

        public boolean isRegexp() {
            return this.isSelected(SearchPattern.MatchType.REGEXP);
        }

        public boolean isBasic() {
            return this.isSelected(SearchPattern.MatchType.BASIC);
        }

        public boolean isLiteral() {
            return this.isSelected(SearchPattern.MatchType.LITERAL);
        }

        public boolean isSelected(SearchPattern.MatchType type) {
            return this.getSelectedItem() == type;
        }

        private SearchPattern.MatchType getSelectedMatchType() {
            Object selected = this.getSelectedItem();
            if (selected instanceof SearchPattern.MatchType) {
                return (SearchPattern.MatchType)((Object)selected);
            }
            throw new IllegalStateException("MatchType expected");
        }
    }

    private static class ReplaceModelItem {
        private ReplacePattern replacePattern;

        public ReplaceModelItem(ReplacePattern replacePattern) {
            this.replacePattern = replacePattern;
        }

        public ReplacePattern getReplacePattern() {
            return this.replacePattern;
        }

        public String toString() {
            return this.replacePattern.getReplaceExpression();
        }
    }

    private class ReplacementPatternListener
    extends PatternChangeListener {
        @Override
        public void handleComboBoxChange(String text) {
            BasicSearchForm.this.searchCriteria.setReplaceExpr(text);
            if (BasicSearchForm.this.cboxReplacement != null) {
                BasicSearchForm.this.updateReplacePatternColor();
            }
        }
    }

    static class TextPatternCheckBoxGroup
    implements ItemListener {
        private JCheckBox matchCase;
        private JCheckBox wholeWords;
        private TextToFindTypeComboBox textToFindType;
        private JCheckBox preserveCase;
        private boolean lastPreserveCaseValue;
        private boolean lastWholeWordsValue;

        private TextPatternCheckBoxGroup(JCheckBox matchCase, JCheckBox wholeWords, TextToFindTypeComboBox textToFindType, JCheckBox preserveCase) {
            this.matchCase = matchCase;
            this.wholeWords = wholeWords;
            this.textToFindType = textToFindType;
            this.preserveCase = preserveCase;
        }

        private void initListeners() {
            this.matchCase.addItemListener(this);
            this.wholeWords.addItemListener(this);
            this.textToFindType.addItemListener(this);
            if (this.preserveCase != null) {
                this.preserveCase.addItemListener(this);
            }
        }

        private void matchCaseChanged() {
            this.updatePreserveCaseAllowed();
        }

        private void regexpChanged() {
            this.updateWholeWordsAllowed();
            this.updatePreserveCaseAllowed();
        }

        private void updateWholeWordsAllowed() {
            if (this.textToFindType.isRegexp() == this.wholeWords.isEnabled()) {
                if (this.textToFindType.isRegexp()) {
                    this.lastWholeWordsValue = this.wholeWords.isSelected();
                    this.wholeWords.setSelected(false);
                    this.wholeWords.setEnabled(false);
                } else {
                    this.wholeWords.setEnabled(true);
                    this.wholeWords.setSelected(this.lastWholeWordsValue);
                }
            }
        }

        private void updatePreserveCaseAllowed() {
            if (this.preserveCase == null) {
                return;
            }
            if (this.preserveCase.isEnabled() == (this.textToFindType.isRegexp() || this.matchCase.isSelected())) {
                if (this.preserveCase.isEnabled()) {
                    this.lastPreserveCaseValue = this.preserveCase.isSelected();
                    this.preserveCase.setSelected(false);
                    this.preserveCase.setEnabled(false);
                } else {
                    this.preserveCase.setEnabled(true);
                    this.preserveCase.setSelected(this.lastPreserveCaseValue);
                }
            }
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            ItemSelectable is = e.getItemSelectable();
            if (is == this.matchCase) {
                this.matchCaseChanged();
            } else if (is == this.textToFindType) {
                this.regexpChanged();
            }
        }

        static void bind(JCheckBox matchCase, JCheckBox wholeWords, TextToFindTypeComboBox regexp, JCheckBox preserveCase) {
            TextPatternCheckBoxGroup tpcbg = new TextPatternCheckBoxGroup(matchCase, wholeWords, regexp, preserveCase);
            tpcbg.initListeners();
        }
    }

    private final class SearchFormPanel
    extends JPanel {
        private FormLayoutHelper flh;
        private int row;

        public SearchFormPanel() {
            this.row = 0;
            this.setLayout(new GridBagLayout());
            this.setBorder(new EmptyBorder(5, 5, 5, 5));
        }

        public void addRow(JComponent label, JComponent component) {
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 0;
            c.anchor = 18;
            c.gridy = this.row++;
            c.weightx = 0.0;
            c.weighty = 0.0;
            c.insets = new Insets(5, 5, 5, 5);
            this.add((Component)label, c);
            c.gridx = 1;
            c.weightx = 1.0;
            c.fill = 2;
            this.add((Component)component, c);
        }

        public void addSeparator() {
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 0;
            c.gridy = this.row++;
            c.gridwidth = 2;
            c.weightx = 1.0;
            c.insets = new Insets(5, 5, 5, 5);
            c.fill = 2;
            JSeparator separator = new JSeparator(0);
            separator.setForeground(SystemColor.controlShadow);
            this.add((Component)separator, c);
        }

        public void addEmptyLine() {
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 0;
            c.gridy = this.row++;
            c.gridwidth = 2;
            c.fill = 2;
            c.weighty = 1.0;
            c.weightx = 0.1;
            JLabel emptyLabel = new JLabel();
            emptyLabel.setPreferredSize(new Dimension(0, 0));
            emptyLabel.setMinimumSize(new Dimension(0, 0));
            this.add((Component)emptyLabel, c);
        }
    }

}

