/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotificationLineSupport
 *  org.openide.awt.Mnemonics
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.search;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.MissingResourceException;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.search.FindDialogMemory;
import org.netbeans.modules.search.Manager;
import org.netbeans.modules.search.ResultView;
import org.netbeans.modules.search.SearchTask;
import org.netbeans.spi.search.provider.SearchComposition;
import org.netbeans.spi.search.provider.SearchProvider;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotificationLineSupport;
import org.openide.awt.Mnemonics;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class SearchPanel
extends JPanel
implements FocusListener,
ActionListener {
    private static SearchPanel currentlyShown = null;
    private boolean replacing;
    private List<PresenterProxy> presenters;
    private DialogDescriptor dialogDescr;
    private JButton okButton;
    private JButton cancelButton;
    private JCheckBox newTabCheckBox;
    JTabbedPane tabbedPane = null;
    private Dialog dialog;
    private SearchProvider.Presenter selectedPresenter = null;
    private boolean preferScopeSelection = false;

    public SearchPanel(boolean replacing) {
        this(replacing, null);
    }

    public SearchPanel(boolean replacing, SearchProvider.Presenter presenter) {
        this.replacing = replacing;
        this.init(presenter);
    }

    private void init(SearchProvider.Presenter explicitPresenter) {
        this.presenters = this.makePresenters(explicitPresenter);
        this.setLayout(new GridLayout(1, 1));
        if (this.presenters.isEmpty()) {
            throw new IllegalStateException("No presenter found");
        }
        if (this.presenters.size() == 1) {
            this.selectedPresenter = this.presenters.get(0).getPresenter();
            this.add(this.selectedPresenter.getForm());
        } else {
            this.tabbedPane = new JTabbedPane();
            for (PresenterProxy pp : this.presenters) {
                Component tab = this.tabbedPane.add(pp.getForm());
                if (!pp.isInitialized()) continue;
                this.tabbedPane.setSelectedComponent(tab);
                this.selectedPresenter = pp.getPresenter();
            }
            this.tabbedPane.addChangeListener(new ChangeListener(){

                @Override
                public void stateChanged(ChangeEvent e) {
                    SearchPanel.this.tabChanged();
                }
            });
            this.add(this.tabbedPane);
        }
        if (this.selectedPresenter == null) {
            this.chooseLastUsedPresenter();
        }
        this.newTabCheckBox = new JCheckBox(NbBundle.getMessage(SearchPanel.class, (String)"TEXT_BUTTON_NEW_TAB"));
        this.newTabCheckBox.setMaximumSize(new Dimension(1000, 200));
        this.newTabCheckBox.setSelected(FindDialogMemory.getDefault().isOpenInNewTab());
        this.initLocalStrings();
        this.initAccessibility();
    }

    private void chooseLastUsedPresenter() {
        FindDialogMemory memory = FindDialogMemory.getDefault();
        String lastProv = memory.getProvider();
        if (lastProv != null) {
            for (PresenterProxy pp : this.presenters) {
                if (!lastProv.equals(pp.getTitle())) continue;
                this.selectedPresenter = pp.getPresenter();
                this.tabbedPane.setSelectedComponent(pp.getForm());
                return;
            }
        }
        this.selectedPresenter = this.presenters.get(0).getPresenter();
    }

    private void initLocalStrings() throws MissingResourceException {
        this.setName(NbBundle.getMessage(SearchPanel.class, (String)"TEXT_TITLE_CUSTOMIZE"));
        String bundleKey = this.isSearchAndReplace() ? "TEXT_BUTTON_SEARCH_CONTINUE" : "TEXT_BUTTON_SEARCH";
        this.okButton = new JButton();
        Mnemonics.setLocalizedText((AbstractButton)this.okButton, (String)NbBundle.getMessage(SearchPanel.class, (String)bundleKey));
        this.cancelButton = new JButton();
        Mnemonics.setLocalizedText((AbstractButton)this.cancelButton, (String)NbBundle.getMessage(SearchPanel.class, (String)"TEXT_BUTTON_CANCEL"));
    }

    private void setDialogDescriptor(DialogDescriptor dialogDescriptor) {
        this.dialogDescr = dialogDescriptor;
    }

    private void initAccessibility() {
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(SearchPanel.class, (String)"ACS_SearchPanel"));
        if (this.tabbedPane != null) {
            this.tabbedPane.getAccessibleContext().setAccessibleName(NbBundle.getMessage(SearchPanel.class, (String)"ACSN_Tabs"));
            this.tabbedPane.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(SearchPanel.class, (String)"ACSD_Tabs"));
        }
        String descSearchContinue = NbBundle.getMessage(SearchPanel.class, (String)(this.isSearchAndReplace() ? "ACS_TEXT_BUTTON_SEARCH_CONTINUE" : "ACS_TEXT_BUTTON_SEARCH"));
        this.okButton.getAccessibleContext().setAccessibleDescription(descSearchContinue);
        this.okButton.setToolTipText(descSearchContinue);
        this.cancelButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(SearchPanel.class, (String)"ACS_TEXT_BUTTON_CANCEL"));
        this.newTabCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(SearchPanel.class, (String)"ACS_TEXT_BUTTON_NEW_TAB"));
    }

    private List<PresenterProxy> makePresenters(SearchProvider.Presenter explicitPresenter) {
        LinkedList<PresenterProxy> presenterList = new LinkedList<PresenterProxy>();
        SearchProvider explicitProvider = explicitPresenter == null ? null : explicitPresenter.getSearchProvider();
        for (SearchProvider p : Lookup.getDefault().lookupAll(SearchProvider.class)) {
            if (this.replacing && !p.isReplaceSupported() || !p.isEnabled()) continue;
            if (explicitProvider == p) {
                presenterList.add(new PresenterProxy(explicitProvider, explicitPresenter));
                continue;
            }
            presenterList.add(new PresenterProxy(p));
        }
        return presenterList;
    }

    public void showDialog() {
        String titleMsgKey = this.replacing ? "LBL_ReplaceInProjects" : "LBL_FindInProjects";
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)this, NbBundle.getMessage(this.getClass(), (String)titleMsgKey), false, new Object[]{this.okButton, this.cancelButton}, (Object)this.okButton, 0, new HelpCtx(this.getClass().getCanonicalName() + "." + this.replacing), (ActionListener)this);
        dialogDescriptor.setTitle(NbBundle.getMessage(this.getClass(), (String)titleMsgKey));
        dialogDescriptor.createNotificationLineSupport();
        dialogDescriptor.setAdditionalOptions(new Object[]{this.newTabCheckBox});
        this.dialog = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
        this.dialog.addWindowListener(new DialogCloseListener());
        this.setDialogDescriptor(dialogDescriptor);
        this.dialog.pack();
        SearchPanel.setCurrentlyShown(this);
        this.dialog.setVisible(true);
        this.dialog.requestFocus();
        this.requestFocusInWindow();
        this.updateHelp();
        this.updateUsability();
        if (this.selectedPresenter == null) {
            this.chooseLastUsedPresenter();
        }
    }

    @Override
    public void focusGained(FocusEvent e) {
        this.tabChanged();
    }

    @Override
    public void focusLost(FocusEvent e) {
        this.tabChanged();
    }

    @Override
    public boolean requestFocusInWindow() {
        return this.selectedPresenter.getForm().requestFocusInWindow();
    }

    private void tabChanged() {
        if (this.tabbedPane != null) {
            int i = this.tabbedPane.getSelectedIndex();
            PresenterProxy pp = this.presenters.get(i);
            this.selectedPresenter = pp.getPresenter();
            if (this.dialogDescr != null) {
                this.dialogDescr.getNotificationLineSupport().clearMessages();
                this.updateUsability();
                this.dialog.pack();
            }
            this.updateHelp();
            FindDialogMemory.getDefault().setProvider(this.selectedPresenter.getSearchProvider().getTitle());
        }
    }

    private void updateHelp() {
        HelpCtx ctx = this.selectedPresenter.getHelpCtx();
        if (this.dialogDescr != null) {
            this.dialogDescr.setHelpCtx(ctx);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.okButton) {
            this.search();
        } else if (e.getSource() == this.cancelButton) {
            this.cancel();
        }
    }

    private void search() {
        SearchComposition sc;
        if (this.selectedPresenter != null && (sc = this.selectedPresenter.composeSearch()) != null) {
            SearchTask st = new SearchTask(sc, this.replacing);
            boolean openInNewTab = this.newTabCheckBox.isSelected();
            if (!openInNewTab) {
                ResultView.getInstance().markCurrentTabAsReusable();
            }
            FindDialogMemory.getDefault().setOpenInNewTab(openInNewTab);
            Manager.getInstance().scheduleSearchTask(st);
            this.close();
        }
    }

    private void cancel() {
        this.close();
        ResultView.getInstance().clearReusableTab();
    }

    boolean isSearchAndReplace() {
        return this.replacing;
    }

    public void close() {
        if (this.dialog != null) {
            this.dialog.dispose();
            this.dialog = null;
        }
    }

    void focusDialog() {
        if (this.dialog != null) {
            this.dialog.requestFocus();
        }
        this.requestFocusInWindow();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static SearchPanel getCurrentlyShown() {
        Class<SearchPanel> class_ = SearchPanel.class;
        synchronized (SearchPanel.class) {
            // ** MonitorExit[var0] (shouldn't be in output)
            return currentlyShown;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void setCurrentlyShown(SearchPanel searchPanel) {
        Class<SearchPanel> class_ = SearchPanel.class;
        synchronized (SearchPanel.class) {
            currentlyShown = searchPanel;
            // ** MonitorExit[var1_1] (shouldn't be in output)
            return;
        }
    }

    private void initChangeListener(final SearchProvider.Presenter p) {
        p.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                SearchPanel.this.okButton.setEnabled(p.isUsable(SearchPanel.this.dialogDescr.getNotificationLineSupport()));
            }
        });
    }

    private void updateUsability() {
        this.okButton.setEnabled(this.selectedPresenter.isUsable(this.dialogDescr.getNotificationLineSupport()));
    }

    public boolean isPreferScopeSelection() {
        return this.preferScopeSelection;
    }

    public void setPreferScopeSelection(boolean preferScopeSelection) {
        this.preferScopeSelection = preferScopeSelection;
    }

    public static boolean isOpenedForSelection() {
        SearchPanel sp = SearchPanel.getCurrentlyShown();
        if (sp == null) {
            return false;
        }
        return sp.isPreferScopeSelection();
    }

    private class PresenterProxy {
        private SearchProvider searchProvider;
        private SearchProvider.Presenter presenter;
        private JPanel panel;

        PresenterProxy(SearchProvider searchProvider) {
            this(searchProvider, null);
        }

        PresenterProxy(SearchProvider searchProvider, SearchProvider.Presenter presenter) {
            this.searchProvider = searchProvider;
            this.presenter = presenter;
            this.panel = new JPanel();
            this.panel.setLayout(new BoxLayout(this.panel, 3));
            this.panel.setName(this.getTitle());
            if (presenter != null) {
                this.initUI();
            }
        }

        final String getTitle() {
            return this.searchProvider.getTitle();
        }

        synchronized SearchProvider.Presenter getPresenter() {
            if (this.presenter == null) {
                this.presenter = this.searchProvider.createPresenter(SearchPanel.this.replacing);
                this.initUI();
            }
            return this.presenter;
        }

        synchronized boolean isInitialized() {
            return this.presenter != null;
        }

        synchronized JComponent getForm() {
            return this.panel;
        }

        private void initUI() {
            this.panel.add(this.presenter.getForm());
            SearchPanel.this.initChangeListener(this.presenter);
            this.panel.validate();
        }
    }

    private class DialogCloseListener
    extends WindowAdapter {
        private DialogCloseListener() {
        }

        @Override
        public void windowClosed(WindowEvent e) {
            for (PresenterProxy presenter : SearchPanel.this.presenters) {
                if (!presenter.isInitialized()) continue;
                presenter.getPresenter().clean();
            }
            if (SearchPanel.getCurrentlyShown() == SearchPanel.this) {
                SearchPanel.setCurrentlyShown(null);
            }
        }
    }

}

