/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

public final class ListComboBoxModel<E>
implements ComboBoxModel<E> {
    private final List<? extends E> elements;
    private final int maxIndex;
    private final boolean reverseOrder;
    private Object selectedItem;
    private Collection<ListDataListener> listeners;
    private ListDataEvent event;

    public ListComboBoxModel(List<E> elements) {
        this(elements, false);
    }

    public ListComboBoxModel(List<? extends E> elements, boolean reverseOrder) {
        this.event = new ListDataEvent(this, 0, -1, -1);
        if (elements == null) {
            throw new IllegalArgumentException("the list of elements must not be null");
        }
        if (elements.isEmpty()) {
            throw new IllegalArgumentException("empty list of elements is not allowed");
        }
        this.elements = elements;
        this.maxIndex = elements.size() - 1;
        this.reverseOrder = reverseOrder;
    }

    @Override
    public void setSelectedItem(Object item) {
        if (this.selectedItem != null && !this.selectedItem.equals(item) || this.selectedItem == null && item != null) {
            this.selectedItem = item;
            this.fireSelectionChange();
        }
    }

    @Override
    public Object getSelectedItem() {
        return this.selectedItem;
    }

    @Override
    public int getSize() {
        return this.maxIndex + 1;
    }

    @Override
    public E getElementAt(int index) {
        return this.elements.get(this.reverseOrder ? this.maxIndex - index : index);
    }

    @Override
    public void addListDataListener(ListDataListener l) {
        if (this.listeners == null) {
            this.listeners = new ArrayList<ListDataListener>(3);
            this.event = new ListDataEvent(this, 0, -1, -1);
        }
        this.listeners.add(l);
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
        if (this.listeners != null && this.listeners.remove(l) && this.listeners.isEmpty()) {
            this.listeners = null;
            this.event = null;
        }
    }

    private void fireSelectionChange() {
        ListDataListener[] arrayListeners;
        if (this.listeners == null) {
            return;
        }
        for (ListDataListener l : arrayListeners = this.listeners.toArray(new ListDataListener[0])) {
            l.contentsChanged(this.event);
        }
    }
}

