/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.ErrorManager
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 *  org.openide.util.TaskListener
 *  org.openide.windows.OutputWriter
 */
package org.netbeans.modules.search;

import java.awt.EventQueue;
import java.lang.ref.Reference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.netbeans.modules.search.CleanTask;
import org.netbeans.modules.search.PrintDetailsTask;
import org.netbeans.modules.search.ReplaceTask;
import org.netbeans.modules.search.ResultDisplayer;
import org.netbeans.modules.search.ResultModel;
import org.netbeans.modules.search.ResultView;
import org.netbeans.modules.search.ResultViewPanel;
import org.netbeans.modules.search.SearchDisplayer;
import org.netbeans.modules.search.SearchTask;
import org.netbeans.modules.search.ui.BasicReplaceResultsPanel;
import org.netbeans.spi.search.provider.SearchComposition;
import org.netbeans.spi.search.provider.SearchResultsDisplayer;
import org.openide.DialogDisplayer;
import org.openide.ErrorManager;
import org.openide.NotifyDescriptor;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.TaskListener;
import org.openide.windows.OutputWriter;

public final class Manager {
    static final int SEARCHING = 1;
    static final int REPLACING = 8;
    static final int EVENT_SEARCH_STARTED = 1;
    static final int EVENT_SEARCH_FINISHED = 2;
    static final int EVENT_SEARCH_INTERRUPTED = 3;
    static final int EVENT_SEARCH_CANCELLED = 4;
    private static final Manager instance = new Manager();
    private final List<Runnable> pendingTasks = new LinkedList<Runnable>();
    private TaskListener taskListener;
    private final List<Runnable> currentTasks = new LinkedList<Runnable>();
    private final List<Runnable> stoppingTasks = new LinkedList<Runnable>();
    private boolean searchWindowOpen = false;
    private Reference<OutputWriter> outputWriterRef;
    private Map<Task, Runnable> tasksMap = new HashMap<Task, Runnable>();
    private static final RequestProcessor RP = new RequestProcessor(Manager.class.getName());

    public static Manager getInstance() {
        return instance;
    }

    private Manager() {
    }

    synchronized void scheduleSearchTask(SearchTask task) {
        assert (EventQueue.isDispatchThread());
        ResultView resultView = ResultView.getInstance();
        resultView.open();
        resultView.requestActive();
        task.setResultViewPanel(resultView.addTab(task));
        this.pendingTasks.add(task);
        this.processNextPendingTask();
    }

    public <R> void scheduleSearchTask(SearchComposition<R> searchComposition, boolean replacing) {
        this.scheduleSearchTask(new SearchTask(searchComposition, replacing));
    }

    public synchronized void scheduleReplaceTask(ReplaceTask task) {
        assert (EventQueue.isDispatchThread());
        this.pendingTasks.add(task);
        this.processNextPendingTask();
    }

    public synchronized void schedulePrintTask(PrintDetailsTask task) {
        assert (EventQueue.isDispatchThread());
        this.pendingTasks.add(task);
        this.processNextPendingTask();
    }

    synchronized void scheduleCleanTask(CleanTask task) {
        assert (EventQueue.isDispatchThread());
        this.pendingTasks.add(task);
        this.processNextPendingTask();
    }

    synchronized String mayStartSearching() {
        String msgKey = this.haveRunningReplaceTask() ? "MSG_Cannot_start_search__replacing" : null;
        return msgKey != null ? NbBundle.getMessage(this.getClass(), (String)msgKey) : null;
    }

    private void notifySearchStarted(SearchTask task) {
        this.notifySearchTaskStateChange(task, 1);
    }

    private void notifySearchFinished(SearchTask task) {
        this.notifySearchTaskStateChange(task, 2);
    }

    private void notifySearchInterrupted(SearchTask task) {
        this.notifySearchTaskStateChange(task, 3);
    }

    private void notifySearchCancelled(SearchTask task) {
        this.notifySearchTaskStateChange(task, 4);
    }

    private void notifySearchTaskStateChange(SearchTask task, int changeType) {
        Method theMethod;
        try {
            theMethod = ResultView.class.getDeclaredMethod("searchTaskStateChanged", SearchTask.class, Integer.TYPE);
        }
        catch (NoSuchMethodException ex) {
            throw new IllegalStateException(ex);
        }
        this.callOnWindowFromAWT(theMethod, new Object[]{task, new Integer(changeType)});
    }

    private void notifySearchPending(SearchTask sTask, int blockingTask) {
        Method theMethod;
        if (!this.searchWindowOpen) {
            return;
        }
        try {
            theMethod = ResultView.class.getDeclaredMethod("notifySearchPending", SearchTask.class, Integer.TYPE);
        }
        catch (NoSuchMethodException ex) {
            throw new IllegalStateException(ex);
        }
        this.callOnWindowFromAWT(theMethod, new Object[]{sTask, new Integer(blockingTask)});
    }

    private void notifyReplaceFinished(ReplaceTask task) {
        ReplaceTask.ResultStatus resultStatus = task.getResultStatus();
        if (resultStatus == ReplaceTask.ResultStatus.SUCCESS) {
            StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(this.getClass(), (String)"MSG_Success"));
            if (this.searchWindowOpen) {
                task.getPanel().showFinishedInfo();
            }
        } else {
            String msgKey = resultStatus == ReplaceTask.ResultStatus.PRE_CHECK_FAILED ? "MSG_Issues_found_during_precheck" : "MSG_Issues_found_during_replace";
            String title = NbBundle.getMessage(this.getClass(), (String)msgKey);
            task.getPanel().displayIssuesToUser(task, title, task.getProblems(), resultStatus != ReplaceTask.ResultStatus.PRE_CHECK_FAILED);
            if (resultStatus == ReplaceTask.ResultStatus.PRE_CHECK_FAILED) {
                this.offerRescanAfterIssuesFound(task);
            }
        }
    }

    private void offerRescanAfterIssuesFound(final ReplaceTask task) {
        String msg = NbBundle.getMessage(this.getClass(), (String)"MSG_IssuesFound_Rescan_");
        NotifyDescriptor.Message nd = new NotifyDescriptor.Message((Object)msg, 3);
        String rerunOption = NbBundle.getMessage(this.getClass(), (String)"LBL_Rerun");
        nd.setOptions(new Object[]{rerunOption, NotifyDescriptor.CANCEL_OPTION});
        Object dlgResult = DialogDisplayer.getDefault().notify((NotifyDescriptor)nd);
        if (rerunOption.equals(dlgResult)) {
            Mutex.EVENT.writeAccess(new Runnable(){

                @Override
                public void run() {
                    task.getPanel().rescan();
                }
            });
        }
    }

    private void notifyPrintingDetailsFinished() {
        if (!this.searchWindowOpen) {
            return;
        }
        this.callOnWindowFromAWT("showAllDetailsFinished");
    }

    private void activateResultWindow() {
        Method theMethod;
        try {
            theMethod = ResultView.class.getMethod("requestActive", new Class[0]);
        }
        catch (NoSuchMethodException ex) {
            throw new IllegalArgumentException();
        }
        this.callOnWindowFromAWT(theMethod, null);
    }

    private void displayIssuesFromAWT(ReplaceTask task, String title, boolean att) {
        Method theMethod;
        try {
            theMethod = ResultView.class.getDeclaredMethod("displayIssuesToUser", ReplaceTask.class, String.class, String[].class, Boolean.TYPE);
        }
        catch (NoSuchMethodException ex) {
            throw new IllegalStateException(ex);
        }
        this.callOnWindowFromAWT(theMethod, new Object[]{task, title, task.getProblems(), att}, false);
    }

    private void callOnWindowFromAWT(String methodName) {
        this.callOnWindowFromAWT(methodName, true);
    }

    private void callOnWindowFromAWT(String methodName, boolean wait) {
        Method theMethod;
        try {
            theMethod = ResultView.class.getDeclaredMethod(methodName, new Class[0]);
        }
        catch (NoSuchMethodException ex) {
            throw new IllegalArgumentException();
        }
        this.callOnWindowFromAWT(theMethod, null, wait);
    }

    private void callOnWindowFromAWT(Method method, Object[] params) {
        this.callOnWindowFromAWT(method, params, true);
    }

    private void callOnWindowFromAWT(final Method method, final Object[] params, boolean wait) {
        Runnable runnable = new Runnable(){

            @Override
            public void run() {
                ResultView resultViewInstance = ResultView.getInstance();
                try {
                    method.invoke((Object)resultViewInstance, params);
                }
                catch (Exception ex) {
                    ErrorManager.getDefault().notify((Throwable)ex);
                }
            }
        };
        if (EventQueue.isDispatchThread()) {
            runnable.run();
        } else if (wait) {
            try {
                EventQueue.invokeAndWait(runnable);
            }
            catch (InvocationTargetException ex1) {
                ErrorManager.getDefault().notify((Throwable)ex1);
            }
            catch (Exception ex2) {
                ErrorManager.getDefault().notify(65536, (Throwable)ex2);
            }
        } else {
            EventQueue.invokeLater(runnable);
        }
    }

    synchronized void searchWindowOpened() {
        this.searchWindowOpen = true;
    }

    synchronized void searchWindowClosed() {
        assert (EventQueue.isDispatchThread());
        this.searchWindowOpen = false;
        Runnable[] tasks = this.currentTasks.toArray(new Runnable[this.currentTasks.size()]);
        for (int i = 0; i < tasks.length; ++i) {
            if (!(tasks[i] instanceof SearchTask)) continue;
            SearchTask sTask = (SearchTask)tasks[i];
            sTask.stop(true);
            if (!(sTask.getDisplayer() instanceof ResultDisplayer)) continue;
            ResultDisplayer disp = (ResultDisplayer)sTask.getDisplayer();
            this.scheduleCleanTask(new CleanTask(disp.getResultModel()));
        }
    }

    private synchronized void processNextPendingTask() {
        Runnable[] pTasks = this.pendingTasks.toArray(new Runnable[this.pendingTasks.size()]);
        for (int i = 0; i < pTasks.length; ++i) {
            boolean haveReplaceRunning = this.haveRunningReplaceTask();
            if (pTasks[i] instanceof SearchTask) {
                if (!this.stoppingTasks.isEmpty()) {
                    this.notifySearchPending((SearchTask)pTasks[i], 1);
                    continue;
                }
                if (haveReplaceRunning) {
                    this.notifySearchPending((SearchTask)pTasks[i], 8);
                    continue;
                }
                if (!this.pendingTasks.remove(pTasks[i])) continue;
                this.startSearching((SearchTask)pTasks[i]);
                continue;
            }
            if (pTasks[i] instanceof ReplaceTask) {
                if (haveReplaceRunning || this.haveRunningSearchTask() || !this.pendingTasks.remove(pTasks[i])) continue;
                this.startReplacing((ReplaceTask)pTasks[i]);
                continue;
            }
            if (pTasks[i] instanceof PrintDetailsTask) {
                if (!this.pendingTasks.remove(pTasks[i])) continue;
                this.startPrintingDetails((PrintDetailsTask)pTasks[i]);
                continue;
            }
            if (pTasks[i] instanceof CleanTask) {
                if (!this.pendingTasks.remove(pTasks[i])) continue;
                this.startCleaning((CleanTask)pTasks[i]);
                continue;
            }
            assert (false);
        }
    }

    private boolean haveRunningReplaceTask() {
        for (Runnable r : this.currentTasks) {
            if (!(r instanceof ReplaceTask)) continue;
            return true;
        }
        return false;
    }

    private boolean haveRunningSearchTask() {
        for (Runnable r : this.currentTasks) {
            if (!(r instanceof SearchTask)) continue;
            return true;
        }
        return false;
    }

    private void startSearching(SearchTask sTask) {
        this.notifySearchStarted(sTask);
        if (this.outputWriterRef != null) {
            SearchDisplayer.clearOldOutput(this.outputWriterRef);
            this.outputWriterRef = null;
            this.activateResultWindow();
        }
        this.runTask(sTask);
    }

    private void startReplacing(ReplaceTask rTask) {
        this.runTask(rTask);
    }

    private void startPrintingDetails(PrintDetailsTask pTask) {
        if (this.outputWriterRef != null) {
            SearchDisplayer.clearOldOutput(this.outputWriterRef);
            this.outputWriterRef = null;
        }
        this.runTask(pTask);
    }

    private void startCleaning(CleanTask cTask) {
        this.runTask(cTask);
    }

    private void runTask(Runnable task) {
        assert (task != null);
        this.currentTasks.add(task);
        RequestProcessor.Task pTask = RP.create(task);
        this.tasksMap.put((Task)pTask, task);
        pTask.addTaskListener(this.getTaskListener());
        pTask.schedule(0);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void stopSearching(SearchTask sTask) {
        boolean stopTask = false;
        Manager manager = this;
        synchronized (manager) {
            if (this.pendingTasks.remove(sTask)) {
                this.notifySearchCancelled(sTask);
            } else if (this.currentTasks.contains(sTask) && !this.stoppingTasks.contains(sTask)) {
                this.stoppingTasks.add(sTask);
                stopTask = true;
            }
        }
        if (stopTask) {
            sTask.stop();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void taskFinished(Runnable task) {
        this.notifyTaskFinished(task);
        Manager manager = this;
        synchronized (manager) {
            if (task instanceof SearchTask) {
                this.stoppingTasks.remove(task);
            } else if (task instanceof PrintDetailsTask) {
                PrintDetailsTask pTask = (PrintDetailsTask)task;
                this.outputWriterRef = pTask.getOutputWriterRef();
            }
            this.currentTasks.remove(task);
        }
        this.processNextPendingTask();
    }

    private void notifyTaskFinished(Runnable task) {
        if (task instanceof SearchTask) {
            SearchTask sTask = (SearchTask)task;
            if (sTask.notifyWhenFinished()) {
                if (sTask.wasInterrupted()) {
                    this.notifySearchInterrupted(sTask);
                } else {
                    this.notifySearchFinished(sTask);
                }
            }
        } else if (task instanceof ReplaceTask) {
            ReplaceTask rTask = (ReplaceTask)task;
            this.notifyReplaceFinished(rTask);
        } else if (task instanceof PrintDetailsTask) {
            PrintDetailsTask pTask = (PrintDetailsTask)task;
            this.notifyPrintingDetailsFinished();
        }
    }

    private TaskListener getTaskListener() {
        if (this.taskListener == null) {
            this.taskListener = new MyTaskListener();
        }
        return this.taskListener;
    }

    private class MyTaskListener
    implements TaskListener {
        MyTaskListener() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void taskFinished(Task task) {
            Runnable rTask;
            Manager manager = Manager.this;
            synchronized (manager) {
                rTask = (Runnable)Manager.this.tasksMap.remove((Object)task);
            }
            if (rTask != null) {
                Manager.this.taskFinished(rTask);
            }
        }
    }

}

