/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.search;

import java.util.List;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.ResultModel;
import org.netbeans.modules.search.TextDetail;
import org.openide.filesystems.FileObject;

final class Item {
    private final ResultModel resultModel;
    final MatchingObject matchingObj;
    final int detailIndex;

    Item(ResultModel resultModel, MatchingObject matchingObj, int detailIndex) {
        this.resultModel = resultModel;
        this.matchingObj = matchingObj;
        this.detailIndex = detailIndex;
    }

    TextDetail getLocation() {
        if (this.detailIndex == -1) {
            return null;
        }
        FileObject fo = this.matchingObj.getFileObject();
        List<TextDetail> textDetails = this.matchingObj.getTextDetails();
        return this.detailIndex < textDetails.size() ? textDetails.get(this.detailIndex) : null;
    }
}

