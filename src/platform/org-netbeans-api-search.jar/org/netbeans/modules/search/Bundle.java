/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.search;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String LBL_ContextView_ApplyAll() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_ContextView_ApplyAll");
    }

    static String LBL_ContextView_Show() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_ContextView_Show");
    }

    static String LBL_ContextView_Skip() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_ContextView_Skip");
    }

    static String LBL_LineEnding() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_LineEnding");
    }

    static String LBL_LineEnding_accName() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_LineEnding.accName");
    }

    static String LBL_LineEnding_tooltip() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_LineEnding.tooltip");
    }

    static String LBL_MacOld() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_MacOld");
    }

    static String LBL_OpenFileScope(Object number_of_files_Please_do_not_translate_choice_number_and_integer_) {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_OpenFileScope", (Object)number_of_files_Please_do_not_translate_choice_number_and_integer_);
    }

    static String LBL_Unix() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_Unix");
    }

    static String LBL_Windows() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_Windows");
    }

    static String MSG_CannotShowTextDetai() {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_CannotShowTextDetai");
    }

    static String MSG_ContextView_fileTooBig() {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_ContextView_fileTooBig");
    }

    static String MSG_ContextView_showBigFile(Object file_name, Object file_size_in_kilobytes) {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_ContextView_showBigFile", (Object)file_name, (Object)file_size_in_kilobytes);
    }

    static String MSG_FileDoesNotExist(Object file_path) {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_FileDoesNotExist", (Object)file_path);
    }

    static String MSG_PatternSansboxTimout() {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_PatternSansboxTimout");
    }

    static String TTL_ContextView_showBigFile() {
        return NbBundle.getMessage(Bundle.class, (String)"TTL_ContextView_showBigFile");
    }

    static String TTL_FileDoesNotExist() {
        return NbBundle.getMessage(Bundle.class, (String)"TTL_FileDoesNotExist");
    }

    private void Bundle() {
    }
}

