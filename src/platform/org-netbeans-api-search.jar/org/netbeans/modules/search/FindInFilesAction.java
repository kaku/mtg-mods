/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallableSystemAction
 */
package org.netbeans.modules.search;

import java.awt.Component;
import java.awt.EventQueue;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.search.SearchPanel;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallableSystemAction;

public class FindInFilesAction
extends CallableSystemAction {
    static final long serialVersionUID = 4554342565076372611L;
    private static final Logger LOG = Logger.getLogger("org.netbeans.modules.search.FindAction_state");
    private static final String VAR_TOOLBAR_COMP_REF = "toolbar presenter ref";
    protected static final String REPLACING = "replacing";
    private static final String VAR_LAST_SEARCH_SCOPE_TYPE = "lastScopeType";
    private final String name;
    protected final boolean preferScopeSelection;
    private final String shortClassName;

    public FindInFilesAction() {
        this(false);
    }

    private FindInFilesAction(boolean preferScopeSelection) {
        this("LBL_Action_FindInProjects", preferScopeSelection);
    }

    protected FindInFilesAction(String nameKey, boolean preferScopeSelection) {
        String clsName = this.getClass().getName();
        int lastDot = clsName.lastIndexOf(46);
        this.shortClassName = lastDot != -1 ? clsName.substring(lastDot + 1) : clsName;
        this.name = NbBundle.getMessage(this.getClass(), (String)nameKey);
        this.preferScopeSelection = preferScopeSelection;
    }

    protected void initialize() {
        super.initialize();
        this.putValue("noIconInMenu", (Object)Boolean.TRUE);
        this.putProperty("replacing", (Object)Boolean.FALSE, false);
    }

    public Component getToolbarPresenter() {
        assert (EventQueue.isDispatchThread());
        if (this.shouldLog(LOG)) {
            this.log("getMenuPresenter()");
        }
        Component presenter = this.getStoredToolbarPresenter();
        return presenter;
    }

    private Component getStoredToolbarPresenter() {
        Object refObj;
        Reference ref;
        Object presenterObj;
        assert (EventQueue.isDispatchThread());
        if (this.shouldLog(LOG)) {
            this.log("getStoredToolbarPresenter()");
        }
        if ((refObj = this.getProperty((Object)"toolbar presenter ref")) instanceof Reference && (presenterObj = (ref = (Reference)refObj).get()) instanceof Component) {
            return (Component)presenterObj;
        }
        Component presenter = super.getToolbarPresenter();
        this.putProperty((Object)"toolbar presenter ref", new WeakReference<Component>(presenter));
        return presenter;
    }

    private boolean checkToolbarPresenterExists() {
        Object refObj;
        assert (EventQueue.isDispatchThread());
        if (this.shouldLog(LOG)) {
            this.log("checkToolbarPresenterExists()");
        }
        if ((refObj = this.getProperty((Object)"toolbar presenter ref")) == null) {
            return false;
        }
        return ((Reference)refObj).get() != null;
    }

    protected String iconResource() {
        return "org/openide/resources/actions/find.gif";
    }

    public String getName() {
        return this.name;
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx("org.netbeans.modules.search.FindInFilesAction");
    }

    public void performAction() {
        assert (EventQueue.isDispatchThread());
        boolean replacing = (Boolean)this.getProperty((Object)"replacing");
        SearchPanel current = SearchPanel.getCurrentlyShown();
        if (current != null) {
            if (current.isSearchAndReplace() == replacing) {
                current.focusDialog();
            } else {
                current.close();
                this.showSearchDialog(replacing);
            }
        } else {
            this.showSearchDialog(replacing);
        }
    }

    private void showSearchDialog(boolean replacing) {
        SearchPanel sp = new SearchPanel(replacing);
        sp.setPreferScopeSelection(this.preferScopeSelection);
        sp.showDialog();
    }

    protected boolean asynchronous() {
        return false;
    }

    private boolean shouldLog(Logger logger) {
        return logger.isLoggable(Level.FINER) && this.shortClassName.equals("FindInFilesAction");
    }

    private void log(String msg) {
        LOG.log(Level.FINER, "{0}: {1}", new Object[]{this.shortClassName, msg});
    }

    public static class Selection
    extends FindInFilesAction {
        public Selection() {
            super(true);
        }
    }

}

