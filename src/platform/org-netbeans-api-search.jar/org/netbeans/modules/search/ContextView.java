/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.ErrorManager
 *  org.openide.NotifyDescriptor
 *  org.openide.explorer.ExplorerManager
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.Node
 *  org.openide.text.NbDocument
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.search;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.StyledDocument;
import javax.swing.tree.TreePath;
import org.netbeans.modules.search.Bundle;
import org.netbeans.modules.search.Item;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.ResultModel;
import org.netbeans.modules.search.TextDetail;
import org.netbeans.modules.search.TextDisplayer;
import org.netbeans.modules.search.TextFetcher;
import org.openide.DialogDisplayer;
import org.openide.ErrorManager;
import org.openide.NotifyDescriptor;
import org.openide.explorer.ExplorerManager;
import org.openide.filesystems.FileObject;
import org.openide.nodes.Node;
import org.openide.text.NbDocument;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

public final class ContextView
extends JPanel {
    private static final String FILE_VIEW = "file view";
    private static final String MESSAGE_VIEW = "message view";
    private static final int FILE_SIZE_LIMIT = 8388608;
    private final CardLayout cardLayout;
    private final JEditorPane editorPane = new JEditorPane();
    private final JScrollPane editorScroll;
    private final JLabel lblPath = new JLabel();
    private final JLabel lblMessage = new JLabel();
    private final Displayer displayer;
    private final RequestProcessor requestProcessor;
    private ResultModel resultModel;
    private RequestProcessor.Task task;
    private TextFetcher textFetcher;
    private String displayedCard;
    private String msgNoFileSelected;
    private String msgMultipleFilesSelected;
    private String editorMimeType;
    ExplorerManager explorerManager;
    private Boolean allApproved;
    private static boolean approveApplyToAllSelected = false;
    private static boolean lastApproveOption = false;
    private final Map<FileObject, Boolean> APPROVED_FILES;

    public ContextView(ResultModel resultModel, ExplorerManager explorerManager) {
        this.displayer = new Displayer();
        this.requestProcessor = new RequestProcessor("TextView", 5, true);
        this.task = null;
        this.textFetcher = null;
        this.displayedCard = null;
        this.msgNoFileSelected = null;
        this.msgMultipleFilesSelected = null;
        this.editorMimeType = null;
        this.allApproved = null;
        this.APPROVED_FILES = new WeakHashMap<FileObject, Boolean>();
        CompoundBorder b = BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, UIManager.getColor("controlShadow")), BorderFactory.createEmptyBorder(5, 5, 1, 5));
        this.lblPath.setBorder(b);
        this.editorPane.setEditable(false);
        this.editorPane.getCaret().setBlinkRate(0);
        this.editorScroll = new JScrollPane(this.editorPane);
        this.editorScroll.setViewportBorder(BorderFactory.createEmptyBorder());
        this.editorScroll.setBorder(BorderFactory.createEmptyBorder());
        JPanel fileViewPanel = new JPanel();
        fileViewPanel.setLayout(new BorderLayout());
        fileViewPanel.add((Component)this.lblPath, "North");
        fileViewPanel.add((Component)this.editorScroll, "Center");
        Box messagePanel = Box.createVerticalBox();
        messagePanel.add(Box.createVerticalGlue());
        messagePanel.add(this.lblMessage);
        messagePanel.add(Box.createVerticalGlue());
        this.lblMessage.setAlignmentX(0.5f);
        this.lblMessage.setHorizontalAlignment(0);
        this.lblMessage.setEnabled(false);
        this.cardLayout = new CardLayout();
        this.setLayout(this.cardLayout);
        this.add((Component)fileViewPanel, "file view");
        this.add((Component)messagePanel, "message view");
        this.setResultModel(resultModel);
        this.explorerManager = explorerManager;
        explorerManager.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().equals("selectedNodes")) {
                    ContextView.this.updateForSelection();
                }
            }
        });
    }

    @Override
    public Dimension getMinimumSize() {
        Dimension minSize = super.getMinimumSize();
        minSize.width = 0;
        return minSize;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void setResultModel(ResultModel resultModel) {
        if (resultModel == this.resultModel) {
            return;
        }
        ContextView contextView = this;
        synchronized (contextView) {
            if (this.textFetcher != null) {
                this.textFetcher.cancel();
                this.textFetcher = null;
            }
        }
        this.resultModel = resultModel;
    }

    private void displaySelectedFiles(JTree tree) {
        TreePath[] selectedPaths = tree.getSelectionPaths();
        if (selectedPaths == null || selectedPaths.length == 0) {
            this.displayNoFileSelected();
        } else if (selectedPaths.length > 1) {
            this.displayMultipleItemsSelected();
        } else {
            assert (selectedPaths.length == 1);
            TreePath path = selectedPaths[0];
            int pathCount = path.getPathCount();
            if (pathCount == 1) {
                this.displayNoFileSelected();
            } else {
                int matchIndex;
                MatchingObject matchingObj;
                assert (pathCount == 2 || pathCount == 3);
                if (pathCount == 2) {
                    matchingObj = (MatchingObject)path.getLastPathComponent();
                    matchIndex = -1;
                } else {
                    TreePath matchingObjPath = path.getParentPath();
                    matchingObj = (MatchingObject)matchingObjPath.getLastPathComponent();
                    int matchingObjRow = tree.getRowForPath(matchingObjPath);
                    int matchRow = tree.getRowForPath(path);
                    matchIndex = matchRow - matchingObjRow - 1;
                }
                this.displayFile(matchingObj, matchIndex);
            }
        }
    }

    private void displayNoFileSelected() {
        if (this.msgNoFileSelected == null) {
            this.msgNoFileSelected = NbBundle.getMessage(this.getClass(), (String)"MsgNoFileSelected");
        }
        this.displayMessage(this.msgNoFileSelected);
    }

    private void displayMultipleItemsSelected() {
        if (this.msgMultipleFilesSelected == null) {
            this.msgMultipleFilesSelected = NbBundle.getMessage(this.getClass(), (String)"MsgMultipleFilesSelected");
        }
        this.displayMessage(this.msgMultipleFilesSelected);
    }

    private void displayMessage(String message) {
        this.lblMessage.setText(message);
        if (this.displayedCard != "message view") {
            this.displayedCard = "message view";
            this.cardLayout.show(this, "message view");
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void displayFile(MatchingObject matchingObj, int partIndex) {
        assert (EventQueue.isDispatchThread());
        Displayer displayer = this.displayer;
        synchronized (displayer) {
            FileObject fo;
            if (this.task != null) {
                this.task.cancel();
                this.task = null;
            }
            if ((fo = matchingObj.getFileObject()).getSize() > 0x800000) {
                Boolean fileApproved = this.APPROVED_FILES.get((Object)fo);
                if (this.allApproved == null && fileApproved == null) {
                    this.approveFetchingOfBigFile(matchingObj, partIndex);
                    return;
                }
                if (Boolean.FALSE.equals(fileApproved) || Boolean.FALSE.equals(this.allApproved)) {
                    this.displayMessage(Bundle.MSG_ContextView_fileTooBig());
                    return;
                }
            }
            Item item = new Item(this.resultModel, matchingObj, partIndex);
            MatchingObject.InvalidityStatus invalidityStatus = matchingObj.checkValidity();
            if (invalidityStatus != null) {
                this.displayMessage(invalidityStatus.getDescription(matchingObj.getFileObject().getPath()));
                return;
            }
            this.requestText(item, this.displayer);
            String description = matchingObj.getDescription();
            this.lblPath.setText(description);
            this.lblPath.setToolTipText(description);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void requestText(Item item, TextDisplayer textDisplayer) {
        assert (EventQueue.isDispatchThread());
        ContextView contextView = this;
        synchronized (contextView) {
            if (this.textFetcher != null) {
                if (this.textFetcher.replaceLocation(item, textDisplayer)) {
                    return;
                }
                this.textFetcher.cancel();
                this.textFetcher = null;
            }
            if (this.textFetcher == null) {
                this.textFetcher = new TextFetcher(item, textDisplayer, this.requestProcessor);
            }
        }
    }

    private void updateForSelection() {
        Node[] nodes = this.explorerManager.getSelectedNodes();
        if (nodes.length == 0) {
            this.displayNoFileSelected();
        } else if (nodes.length == 1) {
            Node n = nodes[0];
            MatchingObject mo = (MatchingObject)n.getLookup().lookup(MatchingObject.class);
            if (mo != null) {
                this.displayFile(mo, -1);
            } else {
                Node parent = n.getParentNode();
                TextDetail td = (TextDetail)n.getLookup().lookup(TextDetail.class);
                if (td != null && parent != null) {
                    mo = (MatchingObject)parent.getLookup().lookup(MatchingObject.class);
                    if (mo != null) {
                        int index = -1;
                        for (int i = 0; i < mo.getTextDetails().size(); ++i) {
                            if (mo.getTextDetails().get(i) != td) continue;
                            index = i;
                            break;
                        }
                        this.displayFile(mo, index);
                    }
                } else {
                    this.displayNoFileSelected();
                }
            }
        } else {
            this.displayMultipleItemsSelected();
        }
    }

    private void approveFetchingOfBigFile(MatchingObject mo, int partIndex) {
        FileObject fo = mo.getFileObject();
        long fileSize = fo.getSize() / 1024;
        JButton showButton = new JButton(Bundle.LBL_ContextView_Show());
        JButton skipButton = new JButton(Bundle.LBL_ContextView_Skip());
        JCheckBox all = new JCheckBox(Bundle.LBL_ContextView_ApplyAll());
        all.setSelected(approveApplyToAllSelected);
        JPanel allPanel = new JPanel();
        allPanel.add(all);
        NotifyDescriptor nd = new NotifyDescriptor((Object)Bundle.MSG_ContextView_showBigFile(fo.getNameExt(), fileSize), Bundle.TTL_ContextView_showBigFile(), 0, 2, new Object[]{skipButton, showButton}, (Object)(lastApproveOption ? showButton : skipButton));
        nd.setAdditionalOptions(new Object[]{allPanel});
        DialogDisplayer.getDefault().notify(nd);
        boolean app = nd.getValue() == showButton;
        this.APPROVED_FILES.put(fo, app);
        if (all.isSelected()) {
            this.allApproved = app;
        }
        approveApplyToAllSelected = all.isSelected();
        lastApproveOption = app;
        this.displayFile(mo, partIndex);
    }

    private class Displayer
    implements TextDisplayer,
    Runnable {
        private TextDetail location;

        private Displayer() {
        }

        @Override
        public void setText(String text, String mimeType, TextDetail location) {
            assert (EventQueue.isDispatchThread());
            if ("content/unknown".equals(mimeType)) {
                mimeType = "text/plain";
            }
            if (ContextView.this.editorMimeType == null || !ContextView.this.editorMimeType.equals(mimeType)) {
                ContextView.this.editorPane.setContentType(mimeType);
                ContextView.this.editorMimeType = mimeType;
            }
            ContextView.this.editorPane.setText(text);
            if (ContextView.this.displayedCard != "file view") {
                ContextView.this.cardLayout.show(ContextView.this, ContextView.this.displayedCard = "file view");
            }
            if (location != null) {
                this.location = location;
                EventQueue.invokeLater(this);
            } else {
                this.scrollToTop();
            }
        }

        @Override
        public void run() {
            assert (EventQueue.isDispatchThread());
            boolean scrolled = false;
            try {
                if (!ContextView.this.editorPane.isShowing()) {
                    return;
                }
                if (this.location != null) {
                    Document document = ContextView.this.editorPane.getDocument();
                    if (document instanceof StyledDocument) {
                        StyledDocument styledDocument = (StyledDocument)document;
                        int cursorOffset = this.getCursorOffset((StyledDocument)document, this.location.getLine() - 1);
                        int startOff = cursorOffset + this.location.getColumn() - 1;
                        int endOff = startOff + this.location.getMarkLength();
                        ContextView.this.editorPane.setSelectionStart(startOff);
                        ContextView.this.editorPane.setSelectionEnd(endOff);
                        Rectangle r = ContextView.this.editorPane.modelToView(startOff);
                        if (r != null) {
                            ContextView.this.editorPane.scrollRectToVisible(r);
                            scrolled = true;
                        }
                    }
                    ContextView.this.editorPane.getCaret().setBlinkRate(0);
                    ContextView.this.editorPane.repaint();
                }
            }
            catch (BadLocationException e) {
                ErrorManager.getDefault().notify(1, (Throwable)e);
            }
            if (!scrolled) {
                this.scrollToTop();
            }
        }

        private int getCursorOffset(StyledDocument doc, int line) {
            assert (EventQueue.isDispatchThread());
            assert (line >= 0);
            try {
                return NbDocument.findLineOffset((StyledDocument)doc, (int)line);
            }
            catch (IndexOutOfBoundsException ex) {
                Element lineRootElement = NbDocument.findLineRootElement((StyledDocument)doc);
                int lineCount = lineRootElement.getElementCount();
                if (line >= lineCount) {
                    return NbDocument.findLineOffset((StyledDocument)doc, (int)(lineCount - 1));
                }
                throw ex;
            }
        }

        private void scrollToTop() {
            JScrollBar scrollBar = ContextView.this.editorScroll.getHorizontalScrollBar();
            scrollBar.setValue(scrollBar.getMinimum());
            scrollBar = ContextView.this.editorScroll.getVerticalScrollBar();
            scrollBar.setValue(scrollBar.getMinimum());
        }
    }

}

