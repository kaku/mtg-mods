/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.search;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.GroupLayout;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import org.netbeans.api.search.RegexpUtil;
import org.netbeans.api.search.SearchScopeOptions;
import org.netbeans.modules.search.FindDialogMemory;
import org.netbeans.modules.search.PatternSandbox;
import org.netbeans.modules.search.ui.UiUtils;
import org.openide.awt.Mnemonics;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.NbBundle;

public final class IgnoreListPanel
extends JPanel {
    private IgnoredListModel ignoreListModel;
    private JFileChooser jFileChooser;
    private JButton btnBrowse;
    private JButton btnClose;
    private JButton btnDelete;
    private JButton btnEdit;
    private JButton btnPattern;
    private JScrollPane jScrollPane1;
    private JTable table;

    public IgnoreListPanel() {
        this.ignoreListModel = new IgnoredListModel();
        this.initComponents();
        this.setMnemonics();
        this.updateEnabledButtons();
        this.table.getSelectionModel().addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent e) {
                IgnoreListPanel.this.updateEnabledButtons();
            }
        });
        this.table.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    IgnoreListPanel.this.btnEdit.doClick();
                }
            }
        });
        this.selectAndScrollToRow(this.table, 0);
    }

    private void selectAndScrollToRow(JTable aTable, int row) {
        boolean isValidRow;
        int rowcount = aTable.getModel().getRowCount();
        boolean rowsAvailable = rowcount > 0;
        boolean bl = isValidRow = row < rowcount;
        if (rowsAvailable && isValidRow) {
            aTable.getSelectionModel().setSelectionInterval(row, row);
            aTable.scrollRectToVisible(aTable.getCellRect(row, 0, false));
        }
    }

    private void updateEnabledButtons() {
        int cnt = this.table.getSelectedRows().length;
        this.btnDelete.setEnabled(cnt > 0);
        boolean editable = false;
        int index = this.table.getSelectedRow();
        if (cnt == 1 && index >= 0) {
            IgnoreListItem ili = this.ignoreListModel.list.get(index);
            editable = ili.type == ItemType.PATTERN || ili.type == ItemType.REGEXP;
        }
        this.btnEdit.setEnabled(editable);
    }

    private void initComponents() {
        this.btnBrowse = new JButton();
        this.btnPattern = new JButton();
        this.btnDelete = new JButton();
        this.btnClose = new JButton();
        this.btnEdit = new JButton();
        this.jScrollPane1 = new JScrollPane();
        this.table = new JTable();
        this.btnBrowse.setText("Add Folder...");
        this.btnBrowse.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                IgnoreListPanel.this.btnBrowseActionPerformed(evt);
            }
        });
        this.btnPattern.setText("Add Path Pattern....");
        this.btnPattern.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                IgnoreListPanel.this.btnPatternActionPerformed(evt);
            }
        });
        this.btnDelete.setText("Delete");
        this.btnDelete.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                IgnoreListPanel.this.btnDeleteActionPerformed(evt);
            }
        });
        this.btnClose.setText("Close");
        this.btnClose.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                IgnoreListPanel.this.btnCloseActionPerformed(evt);
            }
        });
        this.btnEdit.setText("Edit");
        this.btnEdit.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                IgnoreListPanel.this.btnEditActionPerformed(evt);
            }
        });
        this.table.setModel(this.ignoreListModel);
        this.jScrollPane1.setViewportView(this.table);
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jScrollPane1, -1, 331, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.btnDelete, GroupLayout.Alignment.TRAILING, -1, -1, 32767).addComponent(this.btnEdit, GroupLayout.Alignment.TRAILING, -1, -1, 32767).addComponent(this.btnPattern, GroupLayout.Alignment.TRAILING, -1, -1, 32767).addComponent(this.btnBrowse, GroupLayout.Alignment.TRAILING, -1, -1, 32767).addComponent(this.btnClose, -1, -1, 32767)).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.btnBrowse).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnPattern).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnEdit).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnDelete).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 53, 32767).addComponent(this.btnClose)).addComponent(this.jScrollPane1, -2, 0, 32767)).addContainerGap()));
    }

    private void btnDeleteActionPerformed(ActionEvent evt) {
        int firstSelectedRow = this.table.getSelectedRow();
        ArrayList<IgnoreListItem> itemsToDelete = new ArrayList<IgnoreListItem>();
        for (int i : this.table.getSelectedRows()) {
            itemsToDelete.add(this.ignoreListModel.list.get(i));
        }
        for (IgnoreListItem item : itemsToDelete) {
            this.ignoreListModel.remove(item);
        }
        int row = Math.min(Math.max(0, firstSelectedRow), this.table.getModel().getRowCount() - 1);
        this.selectAndScrollToRow(this.table, row);
    }

    private void btnCloseActionPerformed(ActionEvent evt) {
        Window w = (Window)SwingUtilities.getAncestorOfClass(Window.class, this);
        if (w != null) {
            w.dispose();
        }
    }

    private void btnBrowseActionPerformed(ActionEvent evt) {
        int showOpenDialog;
        File[] selected;
        if (this.jFileChooser == null) {
            this.jFileChooser = new JFileChooser();
            this.jFileChooser.setFileSelectionMode(2);
            this.jFileChooser.setMultiSelectionEnabled(true);
        }
        if ((showOpenDialog = this.jFileChooser.showOpenDialog(this.table)) == 0 && (selected = this.jFileChooser.getSelectedFiles()) != null) {
            for (File f : selected) {
                this.ignoreListModel.addFile(f);
            }
        }
    }

    private void btnPatternActionPerformed(ActionEvent evt) {
        PatternSandbox.openDialog(new PatternSandbox.PathPatternComposer("", false){

            @Override
            protected void onApply(String pattern, boolean regexp) {
                IgnoreListPanel.this.ignoreListModel.addPattern(pattern, regexp);
            }
        }, this);
    }

    private void btnEditActionPerformed(ActionEvent evt) {
        final IgnoreListItem ili = this.ignoreListModel.list.get(this.table.getSelectedRow());
        boolean regex = ili.type == ItemType.REGEXP;
        PatternSandbox.openDialog(new PatternSandbox.PathPatternComposer(ili.value, regex){

            @Override
            protected void onApply(String pattern, boolean regexp) {
                IgnoreListPanel.this.ignoreListModel.remove(ili);
                IgnoreListPanel.this.ignoreListModel.addPattern(pattern, regexp);
            }
        }, this.btnEdit);
    }

    public static void main(String[] args) {
        JFrame jf = new JFrame();
        IgnoreListPanel ilp = new IgnoreListPanel();
        jf.add(ilp);
        jf.setDefaultCloseOperation(2);
        jf.pack();
        jf.setLocationRelativeTo(null);
        jf.setVisible(true);
    }

    private void setMnemonics() {
        IgnoreListPanel.setMnem(this.btnBrowse, "IgnoreListPanel.btnBrowse.text");
        IgnoreListPanel.setMnem(this.btnDelete, "IgnoreListPanel.btnDelete.text");
        IgnoreListPanel.setMnem(this.btnEdit, "IgnoreListPanel.btnEdit.text");
        IgnoreListPanel.setMnem(this.btnClose, "IgnoreListPanel.btnClose.text");
        IgnoreListPanel.setMnem(this.btnPattern, "IgnoreListPanel.btnPattern.text");
    }

    private static void setMnem(AbstractButton button, String key) {
        Mnemonics.setLocalizedText((AbstractButton)button, (String)IgnoreListPanel.getText(key));
    }

    private static String getText(String key) {
        return NbBundle.getMessage(IgnoreListPanel.class, (String)key);
    }

    public static void openDialog(JComponent baseComponent) {
        JDialog jd = new JDialog((JDialog)SwingUtilities.getAncestorOfClass(JDialog.class, baseComponent));
        IgnoreListPanel ilp = new IgnoreListPanel();
        jd.add(ilp);
        jd.setModal(true);
        jd.setLocationRelativeTo(baseComponent);
        jd.getRootPane().setDefaultButton(ilp.btnClose);
        IgnoreListPanel.registerCloseKey(jd, ilp);
        IgnoreListPanel.registerDeleteKey(jd, ilp);
        jd.pack();
        jd.setTitle(IgnoreListPanel.getText("IgnoreListPanel.title"));
        jd.setVisible(true);
    }

    private static void registerCloseKey(JDialog jd, final IgnoreListPanel ilp) {
        String actionKey = "cancel";
        jd.getRootPane().getInputMap(1).put(KeyStroke.getKeyStroke(27, 0), actionKey);
        AbstractAction cancelAction = new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent ev) {
                ilp.btnClose.doClick();
            }
        };
        jd.getRootPane().getActionMap().put(actionKey, cancelAction);
    }

    private static void registerDeleteKey(JDialog jd, final IgnoreListPanel ilp) {
        String actionKey = "delete";
        jd.getRootPane().getInputMap(1).put(KeyStroke.getKeyStroke(127, 0), actionKey);
        AbstractAction deleteAction = new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent ev) {
                ilp.btnDelete.doClick();
            }
        };
        jd.getRootPane().getActionMap().put(actionKey, deleteAction);
    }

    static class IgnoreListManager {
        List<IgnoredItemDefinition> items = new LinkedList<IgnoredItemDefinition>();

        public IgnoreListManager(List<String> ignoreList) {
            for (String s : ignoreList) {
                IgnoreListItem ili = IgnoreListItem.fromString(s);
                switch (ili.type) {
                    case PATTERN: {
                        this.items.add(new IgnoredPatternDefinition(ili.value));
                        break;
                    }
                    case REGEXP: {
                        this.items.add(new IgnoredRegexpDefinition(ili.value));
                        break;
                    }
                    case FILE: 
                    case FOLDER: {
                        this.items.add(new IgnoredDirDefinition(ili.value));
                    }
                }
            }
        }

        boolean isIgnored(FileObject fo) {
            for (IgnoredItemDefinition iid : this.items) {
                if (!iid.isIgnored(fo)) continue;
                return true;
            }
            return false;
        }

        private class IgnoredDirDefinition
        extends IgnoredItemDefinition {
            FileObject dir;

            public IgnoredDirDefinition(String path) {
                super();
                this.dir = FileUtil.toFileObject((File)new File(path));
            }

            @Override
            boolean isIgnored(FileObject obj) {
                return FileUtil.isParentOf((FileObject)this.dir, (FileObject)obj) || obj.equals((Object)this.dir);
            }
        }

        private class IgnoredRegexpDefinition
        extends IgnoredItemDefinition {
            private Pattern p;

            public IgnoredRegexpDefinition(String pattern) {
                super();
                this.p = Pattern.compile(pattern);
            }

            @Override
            boolean isIgnored(FileObject obj) {
                return this.p.matcher(obj.getPath()).find();
            }
        }

        private class IgnoredPatternDefinition
        extends IgnoredItemDefinition {
            private Pattern p;

            public IgnoredPatternDefinition(String pattern) {
                super();
                this.p = RegexpUtil.makeFileNamePattern(SearchScopeOptions.create(pattern, false));
            }

            @Override
            boolean isIgnored(FileObject obj) {
                return this.p.matcher(obj.getNameExt()).matches();
            }
        }

        private abstract class IgnoredItemDefinition {
            private IgnoredItemDefinition() {
            }

            abstract boolean isIgnored(FileObject var1);
        }

    }

    class IgnoredListModel
    extends AbstractTableModel {
        List<IgnoreListItem> list;

        public void remove(Object o) {
            int index = this.list.indexOf(o);
            this.list.remove(index);
            this.fireTableRowsDeleted(index, index);
            this.persist();
        }

        public void addFile(File f) {
            this.list.add(IgnoreListItem.forFile(f));
            this.fireTableRowsInserted(this.list.size() - 1, this.list.size());
            this.persist();
        }

        public void addPattern(String p, boolean regexp) {
            if (regexp) {
                this.addRegularExpression(p);
            } else {
                this.addSimplePatter(p);
            }
        }

        public void addSimplePatter(String p) {
            this.list.add(IgnoreListItem.forPattern(p));
            this.fireTableRowsInserted(this.list.size() - 1, this.list.size());
            this.persist();
        }

        public void addRegularExpression(String x) {
            this.list.add(IgnoreListItem.forRegexp(x));
            this.fireTableRowsInserted(this.list.size() - 1, this.list.size());
            this.persist();
        }

        public IgnoredListModel() {
            List<String> orig = FindDialogMemory.getDefault().getIgnoreList();
            this.list = new ArrayList<IgnoreListItem>(orig.size());
            for (String s : orig) {
                IgnoreListItem ili = IgnoreListItem.fromString(s);
                if (ili.type == ItemType.INVALID) continue;
                this.list.add(ili);
            }
        }

        public void persist() {
            ArrayList<String> copy = new ArrayList<String>(this.list.size());
            for (IgnoreListItem ili : this.list) {
                copy.add(ili.toString());
            }
            FindDialogMemory.getDefault().setIgnoreList(copy);
        }

        @Override
        public int getRowCount() {
            return this.list.size();
        }

        @Override
        public int getColumnCount() {
            return 2;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            IgnoreListItem ili = this.list.get(rowIndex);
            if (columnIndex == 0) {
                String msg;
                switch (ili.type) {
                    case FILE: {
                        msg = "IgnoreListPanel.type.file";
                        break;
                    }
                    case FOLDER: {
                        msg = "IgnoreListPanel.type.folder";
                        break;
                    }
                    case PATTERN: {
                        msg = "IgnoreListPanel.type.pattern";
                        break;
                    }
                    case REGEXP: {
                        msg = "IgnoreListPanel.type.regexp";
                        break;
                    }
                    default: {
                        msg = "IgnoreListPanel.type.invalid";
                    }
                }
                return UiUtils.getText(msg);
            }
            return ili.value;
        }

        @Override
        public String getColumnName(int column) {
            if (column == 0) {
                return UiUtils.getText("IgnoreListPanel.item.type");
            }
            return UiUtils.getText("IgnoreListPanel.item.value");
        }
    }

    static class IgnoreListItem {
        private ItemType type;
        private String value;

        private IgnoreListItem(String string) {
            if (ItemType.PATTERN.isTypeOf(string)) {
                this.type = ItemType.PATTERN;
                this.value = string.substring(3);
            } else if (ItemType.REGEXP.isTypeOf(string)) {
                this.type = ItemType.REGEXP;
                this.value = string.substring(3);
            } else if (ItemType.FILE.isTypeOf(string)) {
                String path = string.substring(3);
                File f = new File(path);
                if (!f.exists()) {
                    this.type = ItemType.INVALID;
                    return;
                }
                this.type = f.isDirectory() ? ItemType.FOLDER : ItemType.FILE;
                this.value = path;
            } else {
                this.type = ItemType.INVALID;
                return;
            }
        }

        private IgnoreListItem(ItemType type, String value) {
            this.type = type;
            this.value = value;
        }

        public String toString() {
            return this.type.getTypePrefix() + this.value;
        }

        static IgnoreListItem fromString(String string) {
            return new IgnoreListItem(string);
        }

        static IgnoreListItem forFile(File f) {
            ItemType type = !f.exists() ? ItemType.INVALID : (f.isDirectory() ? ItemType.FOLDER : ItemType.FILE);
            return new IgnoreListItem(type, f.getAbsolutePath());
        }

        static IgnoreListItem forRegexp(String regexp) {
            return new IgnoreListItem(ItemType.REGEXP, regexp);
        }

        static IgnoreListItem forPattern(String pattern) {
            return new IgnoreListItem(ItemType.PATTERN, pattern);
        }
    }

    static enum ItemType {
        FILE("f: "),
        FOLDER("f: "),
        REGEXP("x: "),
        PATTERN("s: "),
        INVALID("i: ");
        
        private String typePrefix;

        private ItemType(String typePrefix) {
            this.typePrefix = typePrefix;
        }

        public String getTypePrefix() {
            return this.typePrefix;
        }

        public boolean isTypeOf(String s) {
            return s.startsWith(this.typePrefix);
        }
    }

}

