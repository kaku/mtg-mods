/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.search;

import org.openide.util.NbBundle;

public final class Constants {
    public static final int COUNT_LIMIT = 500;
    public static final int DETAILS_COUNT_LIMIT = 5000;

    public static enum Limit {
        FILES_COUNT_LIMIT("TEXT_MSG_LIMIT_REACHED_FILES_COUNT", 500),
        MATCHES_COUNT_LIMIT("TEXT_MSG_LIMIT_REACHED_MATCHES_COUNT", 5000);
        
        private final String bundleKey;
        private final Integer value;

        private Limit(String bundleKey, Integer limit) {
            this.bundleKey = bundleKey;
            this.value = limit;
        }

        String getDisplayName() {
            return NbBundle.getMessage(Limit.class, (String)this.bundleKey, (Object)this.value);
        }

        public Integer getValue() {
            return this.value;
        }
    }

}

