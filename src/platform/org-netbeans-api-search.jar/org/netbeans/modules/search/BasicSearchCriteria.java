/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.loaders.DataObject
 */
package org.netbeans.modules.search;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.search.RegexpUtil;
import org.netbeans.api.search.SearchPattern;
import org.netbeans.api.search.SearchScopeOptions;
import org.netbeans.modules.search.TextRegexpUtil;
import org.openide.loaders.DataObject;

public final class BasicSearchCriteria {
    private static int instanceCounter;
    private final int instanceId = instanceCounter++;
    private static final Logger LOG;
    private SearchPattern searchPattern = SearchPattern.create(null, false, false, false);
    private SearchScopeOptions searcherOptions = SearchScopeOptions.create();
    private String replaceExpr;
    private String replaceString;
    private boolean preserveCase;
    private boolean textPatternSpecified = false;
    private boolean fileNamePatternSpecified = false;
    private boolean textPatternValid = false;
    private boolean replacePatternValid = false;
    private boolean fileNamePatternValid = false;
    private Pattern textPattern;
    private Pattern fileNamePattern;
    private boolean useIgnoreList = false;
    private boolean criteriaUsable = false;
    private ChangeListener usabilityChangeListener;
    private DataObject dataObject;

    BasicSearchCriteria() {
        if (LOG.isLoggable(Level.FINER)) {
            LOG.log(Level.FINER, "#{0}: <init>()", this.instanceId);
        }
    }

    BasicSearchCriteria(BasicSearchCriteria template) {
        if (LOG.isLoggable(Level.FINER)) {
            LOG.log(Level.FINER, "#{0}: <init>(template)", this.instanceId);
        }
        this.setCaseSensitive(template.searchPattern.isMatchCase());
        this.setWholeWords(template.searchPattern.isWholeWords());
        this.setMatchType(template.searchPattern.getMatchType());
        this.setPreserveCase(template.preserveCase);
        this.setSearchInArchives(template.searcherOptions.isSearchInArchives());
        this.setSearchInGenerated(template.searcherOptions.isSearchInGenerated());
        this.setFileNameRegexp(template.searcherOptions.isRegexp());
        this.setUseIgnoreList(template.useIgnoreList);
        this.setTextPattern(template.searchPattern.getSearchExpression());
        this.setFileNamePattern(template.searcherOptions.getPattern());
        this.setReplaceExpr(template.replaceExpr);
    }

    Pattern getTextPattern() {
        if (!this.textPatternValid || !this.textPatternSpecified) {
            return null;
        }
        if (this.textPattern != null) {
            return this.textPattern;
        }
        try {
            return TextRegexpUtil.makeTextPattern(this.searchPattern);
        }
        catch (PatternSyntaxException e) {
            this.textPatternValid = false;
            return null;
        }
    }

    public String getTextPatternExpr() {
        return this.searchPattern.getSearchExpression() != null ? this.searchPattern.getSearchExpression() : "";
    }

    void setTextPattern(String pattern) {
        this.searchPattern = this.searchPattern.changeSearchExpression(pattern);
        boolean wasValid = this.textPatternValid;
        if (pattern == null || pattern.equals("")) {
            this.textPattern = null;
            this.textPatternSpecified = false;
            this.textPatternValid = false;
        } else {
            this.textPatternSpecified = true;
            this.updateTextPattern();
        }
        this.replacePatternValid = this.validateReplacePattern();
        this.updateUsability(this.textPatternValid != wasValid);
    }

    private void updateFileNamePattern() {
        try {
            if (this.fileNamePatternSpecified) {
                this.fileNamePattern = RegexpUtil.makeFileNamePattern(this.searcherOptions);
                this.fileNamePatternValid = true;
            }
        }
        catch (PatternSyntaxException e) {
            this.fileNamePattern = null;
            this.fileNamePatternValid = false;
        }
    }

    private void updateTextPattern() throws NullPointerException {
        try {
            if (this.textPatternSpecified) {
                this.textPattern = TextRegexpUtil.makeTextPattern(this.searchPattern);
                this.textPatternValid = true;
            }
        }
        catch (PatternSyntaxException e) {
            this.textPatternValid = false;
        }
    }

    private boolean validateReplacePattern() {
        if (this.searchPattern.isRegExp() && this.textPatternValid && this.textPatternSpecified && this.replaceExpr != null && !this.replaceExpr.isEmpty()) {
            int groups = this.getTextPattern().matcher("").groupCount();
            String tmpSearch = "";
            for (int i = 1; i <= groups; ++i) {
                tmpSearch = tmpSearch + "(" + i + ")";
            }
            try {
                Pattern.compile(tmpSearch).matcher("123456789").replaceFirst(this.replaceExpr);
            }
            catch (Exception e) {
                return false;
            }
        }
        return true;
    }

    SearchPattern.MatchType getMatchType() {
        return this.searchPattern.getMatchType();
    }

    boolean isPreserveCase() {
        return this.preserveCase;
    }

    void setPreserveCase(boolean preserveCase) {
        if (LOG.isLoggable(Level.FINER)) {
            LOG.log(Level.FINER, "setPreservecase({0}{1}", new Object[]{preserveCase, Character.valueOf(')')});
        }
        if (preserveCase == this.preserveCase) {
            LOG.finest(" - no change");
            return;
        }
        this.preserveCase = preserveCase;
        if (!this.searchPattern.isRegExp()) {
            this.textPattern = null;
        }
    }

    public boolean isFileNameRegexp() {
        return this.searcherOptions.isRegexp();
    }

    public void setFileNameRegexp(boolean fileNameRegexp) {
        if (this.searcherOptions.isRegexp() != fileNameRegexp) {
            this.searcherOptions.setRegexp(fileNameRegexp);
            this.updateFileNamePattern();
            this.updateUsability(true);
        }
    }

    public boolean isSearchInArchives() {
        return this.searcherOptions.isSearchInArchives();
    }

    public void setSearchInArchives(boolean searchInArchives) {
        this.searcherOptions.setSearchInArchives(searchInArchives);
    }

    public boolean isSearchInGenerated() {
        return this.searcherOptions.isSearchInGenerated();
    }

    public void setSearchInGenerated(boolean searchInGenerated) {
        this.searcherOptions.setSearchInGenerated(searchInGenerated);
    }

    public boolean isUseIgnoreList() {
        return this.useIgnoreList;
    }

    public void setUseIgnoreList(boolean useIgnoreList) {
        this.useIgnoreList = useIgnoreList;
    }

    void setMatchType(SearchPattern.MatchType matchType) {
        this.searchPattern = this.searchPattern.changeMatchType(matchType);
        this.updateTextPattern();
        this.replacePatternValid = this.validateReplacePattern();
        this.updateUsability(true);
    }

    boolean isWholeWords() {
        return this.searchPattern.isWholeWords();
    }

    void setWholeWords(boolean wholeWords) {
        this.searchPattern = this.searchPattern.changeWholeWords(wholeWords);
        this.updateTextPattern();
    }

    boolean isCaseSensitive() {
        return this.searchPattern.isMatchCase();
    }

    void setCaseSensitive(boolean caseSensitive) {
        this.searchPattern = this.searchPattern.changeMatchCase(caseSensitive);
        this.updateTextPattern();
    }

    boolean isFullText() {
        return this.textPatternValid;
    }

    Pattern getFileNamePattern() {
        if (!this.fileNamePatternValid || !this.fileNamePatternSpecified) {
            return null;
        }
        if (this.fileNamePattern == null) {
            this.updateFileNamePattern();
            return this.fileNamePattern;
        }
        return this.fileNamePattern;
    }

    String getFileNamePatternExpr() {
        return this.searcherOptions.getPattern();
    }

    void setFileNamePattern(String pattern) {
        this.searcherOptions.setPattern(pattern);
        if (this.searcherOptions.getPattern().isEmpty()) {
            this.fileNamePatternSpecified = false;
        } else {
            this.fileNamePatternSpecified = true;
            this.updateFileNamePattern();
        }
        boolean force = !this.isFileNameRegexp();
        this.updateUsability(force);
    }

    boolean isSearchAndReplace() {
        return this.replaceExpr != null;
    }

    public String getReplaceExpr() {
        return this.replaceExpr;
    }

    String getReplaceString() {
        if (this.replaceString == null && this.replaceExpr != null) {
            String[] sGroups = this.replaceExpr.split("\\\\\\\\", this.replaceExpr.length());
            String res = "";
            for (int i = 0; i < sGroups.length; ++i) {
                String tmp = sGroups[i];
                tmp = tmp.replace("\\r", "\r");
                tmp = tmp.replace("\\n", "\n");
                tmp = tmp.replace("\\t", "\t");
                res = res + tmp;
                if (i == sGroups.length - 1) continue;
                res = res + "\\\\";
            }
            this.replaceString = res;
        }
        return this.replaceString;
    }

    void setReplaceExpr(String replaceExpr) {
        this.replaceExpr = replaceExpr;
        this.replaceString = null;
        this.replacePatternValid = this.validateReplacePattern();
        this.updateUsability(false);
    }

    private void updateUsability(boolean force) {
        boolean wasUsable = this.criteriaUsable;
        this.criteriaUsable = this.isUsable();
        if (this.criteriaUsable != wasUsable || force) {
            this.fireUsabilityChanged();
        }
    }

    boolean isUsable() {
        return (this.textPatternSpecified || !this.isSearchAndReplace() && this.fileNamePatternSpecified) && !this.isInvalid();
    }

    private boolean isInvalid() {
        return this.isTextPatternInvalid() || this.isFileNamePatternInvalid() || this.isReplacePatternInvalid();
    }

    void setUsabilityChangeListener(ChangeListener l) {
        this.usabilityChangeListener = l;
    }

    private void fireUsabilityChanged() {
        if (this.usabilityChangeListener != null) {
            this.usabilityChangeListener.stateChanged(new ChangeEvent(this));
        }
    }

    boolean isTextPatternUsable() {
        return this.textPatternSpecified && this.textPatternValid;
    }

    boolean isTextPatternInvalid() {
        return this.textPatternSpecified && !this.textPatternValid;
    }

    boolean isReplacePatternInvalid() {
        return !this.replacePatternValid;
    }

    boolean isFileNamePatternUsable() {
        return this.fileNamePatternSpecified && this.fileNamePatternValid;
    }

    boolean isFileNamePatternInvalid() {
        return this.fileNamePatternSpecified && !this.fileNamePatternValid;
    }

    void onOk() {
        LOG.finer("onOk()");
        if (this.textPatternValid && this.textPattern == null) {
            this.textPattern = TextRegexpUtil.makeTextPattern(this.searchPattern);
        }
        if (this.fileNamePatternValid && this.fileNamePattern == null) {
            this.fileNamePattern = RegexpUtil.makeFileNamePattern(this.searcherOptions);
        }
        assert (!this.textPatternValid || this.textPattern != null);
        assert (!this.fileNamePatternValid || this.fileNamePattern != null);
    }

    boolean isTextPatternValidAndSpecified() {
        return this.textPatternValid && this.textPatternSpecified;
    }

    SearchPattern getSearchPattern() {
        return this.searchPattern;
    }

    SearchScopeOptions getSearcherOptions() {
        return this.searcherOptions;
    }

    static {
        LOG = Logger.getLogger("org.netbeans.modules.search.BasicSearchCriteria");
    }
}

