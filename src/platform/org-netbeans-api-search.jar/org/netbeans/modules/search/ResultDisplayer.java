/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.Node
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.search;

import java.nio.charset.Charset;
import java.util.List;
import java.util.regex.Pattern;
import javax.swing.JComponent;
import org.netbeans.modules.search.BasicComposition;
import org.netbeans.modules.search.BasicSearchCriteria;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.ResultModel;
import org.netbeans.modules.search.ResultView;
import org.netbeans.modules.search.TextDetail;
import org.netbeans.modules.search.ui.BasicAbstractResultsPanel;
import org.netbeans.modules.search.ui.BasicReplaceResultsPanel;
import org.netbeans.modules.search.ui.BasicSearchResultsPanel;
import org.netbeans.spi.search.provider.SearchResultsDisplayer;
import org.openide.filesystems.FileObject;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

class ResultDisplayer
extends SearchResultsDisplayer<MatchingObject.Def> {
    private final ResultModel resultModel;
    private final BasicSearchCriteria criteria;
    private final BasicComposition composition;
    private BasicAbstractResultsPanel resultPanel;
    private Node infoNode;

    public ResultDisplayer(BasicSearchCriteria criteria, BasicComposition composition) {
        this.criteria = criteria;
        this.resultModel = new ResultModel(criteria, criteria.getReplaceString(), composition);
        this.composition = composition;
    }

    @Override
    public synchronized JComponent getVisualComponent() {
        if (this.resultPanel != null) {
            return this.resultPanel;
        }
        this.resultPanel = this.criteria.isSearchAndReplace() ? new BasicReplaceResultsPanel(this.resultModel, this.composition, this.infoNode) : new BasicSearchResultsPanel(this.resultModel, this.composition, this.criteria.isFullText(), this.infoNode);
        this.resultPanel.setToolTipText(this.composition.getScopeDisplayName() + ": " + this.getTitle());
        return this.resultPanel;
    }

    @Override
    public void addMatchingObject(MatchingObject.Def object) {
        if (this.resultModel.objectFound(object.getFileObject(), object.getCharset(), object.getTextDetails())) {
            this.resultPanel.update();
            this.resultPanel.addMatchingObject(this.resultModel.getMatchingObjects().get(this.resultModel.size() - 1));
        }
        if (this.resultModel.wasLimitReached()) {
            this.composition.terminate();
        }
    }

    ResultModel getResultModel() {
        return this.resultModel;
    }

    @Override
    public String getTitle() {
        if (this.criteria.getTextPattern() == null) {
            if (this.criteria.getFileNamePattern() == null) {
                return NbBundle.getMessage(ResultView.class, (String)"TEXT_MSG_RESULTS_FOR_FILE_PATTERN");
            }
            return this.criteria.getFileNamePatternExpr();
        }
        return this.criteria.getTextPatternExpr();
    }

    @Override
    public void searchStarted() {
        this.resultModel.setStartTime();
        this.resultPanel.searchStarted();
    }

    @Override
    public void searchFinished() {
        this.resultPanel.searchFinished();
    }

    @Override
    public void setInfoNode(Node infoNode) {
        this.infoNode = infoNode;
    }

    @Override
    public void closed() {
        this.resultPanel.closed();
    }
}

