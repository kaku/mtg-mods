/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.text.NbDocument
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 *  org.openide.windows.TopComponent
 */
package org.netbeans.modules.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.swing.Icon;
import org.netbeans.api.search.provider.SearchInfo;
import org.netbeans.api.search.provider.SearchInfoUtils;
import org.netbeans.modules.search.Bundle;
import org.netbeans.spi.search.SearchScopeDefinition;
import org.netbeans.spi.search.SearchScopeDefinitionProvider;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.text.NbDocument;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.windows.TopComponent;

public class OpenFilesSearchScopeProvider
extends SearchScopeDefinitionProvider {
    @Override
    public List<SearchScopeDefinition> createSearchScopeDefinitions() {
        ArrayList<SearchScopeDefinition> list = new ArrayList<SearchScopeDefinition>(1);
        list.add(new OpenFilesScope());
        return list;
    }

    private static class OpenFilesScope
    extends SearchScopeDefinition {
        private static final String ICON_PATH = "org/netbeans/modules/search/res/multi_selection.png";
        private static final Icon ICON = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/search/res/multi_selection.png", (boolean)false);

        private OpenFilesScope() {
        }

        @Override
        public String getTypeId() {
            return "openFiles";
        }

        @Override
        public String getDisplayName() {
            return Bundle.LBL_OpenFileScope(this.getCurrentlyOpenedFiles().size());
        }

        @Override
        public boolean isApplicable() {
            Collection<FileObject> files = this.getCurrentlyOpenedFiles();
            return files != null && !files.isEmpty();
        }

        @Override
        public SearchInfo getSearchInfo() {
            Collection<FileObject> files = this.getCurrentlyOpenedFiles();
            return SearchInfoUtils.createSearchInfoForRoots(files.toArray((T[])new FileObject[files.size()]));
        }

        @Override
        public int getPriority() {
            return 450;
        }

        @Override
        public void clean() {
        }

        @Override
        public Icon getIcon() {
            return ICON;
        }

        private Collection<FileObject> getCurrentlyOpenedFiles() {
            LinkedHashSet<FileObject> result = new LinkedHashSet<FileObject>();
            for (TopComponent tc : TopComponent.getRegistry().getOpened()) {
                FileObject primaryFile;
                DataObject dob = (DataObject)tc.getLookup().lookup(DataObject.class);
                if (dob == null || !this.isFromEditorWindow(dob) || (primaryFile = dob.getPrimaryFile()) == null) continue;
                result.add(primaryFile);
            }
            return result;
        }

        protected boolean isFromEditorWindow(DataObject dobj) {
            final EditorCookie editor = (EditorCookie)dobj.getLookup().lookup(EditorCookie.class);
            if (editor != null) {
                return (Boolean)Mutex.EVENT.readAccess((Mutex.Action)new Mutex.Action<Boolean>(){

                    public Boolean run() {
                        return NbDocument.findRecentEditorPane((EditorCookie)editor) != null;
                    }
                });
            }
            return false;
        }

    }

}

