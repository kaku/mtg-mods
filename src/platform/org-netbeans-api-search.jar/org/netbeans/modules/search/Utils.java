/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.search;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import javax.swing.BorderFactory;
import javax.swing.UIManager;
import javax.swing.border.Border;

final class Utils {
    private Utils() {
    }

    static Border getExplorerViewBorder() {
        Border border = (Border)UIManager.get("Nb.ScrollPane.border");
        if (border == null) {
            border = BorderFactory.createEtchedBorder();
        }
        return border;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static CharBuffer getCharSequence(FileInputStream stream, Charset encoding) throws IOException {
        FileChannel channel = stream.getChannel();
        ByteBuffer bbuf = ByteBuffer.allocate((int)channel.size());
        try {
            channel.read(bbuf, 0);
        }
        catch (ClosedByInterruptException cbie) {
            CharBuffer charBuffer = null;
            return charBuffer;
        }
        finally {
            channel.close();
        }
        bbuf.rewind();
        CharBuffer cbuf = encoding.decode(bbuf);
        return cbuf;
    }
}

