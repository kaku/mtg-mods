/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 */
package org.netbeans.modules.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.spi.search.SearchScopeDefinition;
import org.netbeans.spi.search.SearchScopeDefinitionProvider;
import org.openide.util.Lookup;
import org.openide.util.Mutex;

public class SearchScopeList {
    private final List<SearchScopeDefinition> scopes;
    private final List<ChangeListener> changeListeners = new ArrayList<ChangeListener>(1);
    private ProxyChangeListener proxyChangeListener;

    public /* varargs */ SearchScopeList(SearchScopeDefinition ... extraSearchScopes) {
        this.proxyChangeListener = new ProxyChangeListener();
        this.scopes = this.createScopeList(extraSearchScopes);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void clean() {
        List list = this.scopes;
        synchronized (list) {
            for (SearchScopeDefinition ssd : this.scopes) {
                ssd.removeChangeListener(this.proxyChangeListener);
                ssd.clean();
            }
            this.scopes.clear();
        }
        list = this.changeListeners;
        synchronized (list) {
            this.changeListeners.clear();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addChangeListener(ChangeListener changeListener) {
        List<ChangeListener> list = this.changeListeners;
        synchronized (list) {
            this.changeListeners.add(changeListener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeChangeListener(ChangeListener changeListener) {
        List<ChangeListener> list = this.changeListeners;
        synchronized (list) {
            this.changeListeners.remove(changeListener);
        }
    }

    private /* varargs */ List<SearchScopeDefinition> createScopeList(SearchScopeDefinition ... extraSearchScopes) {
        ArrayList<SearchScopeDefinition> scopeList = new ArrayList<SearchScopeDefinition>(6);
        Collection providers = Lookup.getDefault().lookupAll(SearchScopeDefinitionProvider.class);
        for (SearchScopeDefinitionProvider provider : providers) {
            scopeList.addAll(provider.createSearchScopeDefinitions());
        }
        scopeList.addAll(Arrays.asList(extraSearchScopes));
        Collections.sort(scopeList, new ScopePriorityComparator());
        for (SearchScopeDefinition scope : scopeList) {
            scope.addChangeListener(this.proxyChangeListener);
        }
        return scopeList;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public List<SearchScopeDefinition> getSeachScopeDefinitions() {
        List<SearchScopeDefinition> list = this.scopes;
        synchronized (list) {
            return new ArrayList<SearchScopeDefinition>(this.scopes);
        }
    }

    private class ProxyChangeListener
    implements ChangeListener {
        private ProxyChangeListener() {
        }

        @Override
        public void stateChanged(final ChangeEvent e) {
            Mutex.EVENT.writeAccess(new Runnable(){

                @Override
                public void run() {
                    ProxyChangeListener.this.notifyDelegates(e);
                }
            });
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void notifyDelegates(ChangeEvent e) {
            List list = SearchScopeList.this.changeListeners;
            synchronized (list) {
                for (ChangeListener changeListener : SearchScopeList.this.changeListeners) {
                    changeListener.stateChanged(e);
                }
            }
        }

    }

    private class ScopePriorityComparator
    implements Comparator<SearchScopeDefinition> {
        private ScopePriorityComparator() {
        }

        @Override
        public int compare(SearchScopeDefinition o1, SearchScopeDefinition o2) {
            return o1.getPriority() - o2.getPriority();
        }
    }

}

