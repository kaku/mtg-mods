/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.actions.FindAction
 *  org.openide.actions.ReplaceAction
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.CloneableEditorSupport$Pane
 *  org.openide.util.Mutex
 *  org.openide.util.SharedClassObject
 *  org.openide.util.WeakSet
 *  org.openide.util.actions.CallbackSystemAction
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 */
package org.netbeans.modules.search;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.WeakReference;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.ActionMap;
import org.netbeans.modules.search.FindInFilesAction;
import org.netbeans.modules.search.ReplaceInFilesAction;
import org.openide.actions.FindAction;
import org.openide.actions.ReplaceAction;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.Mutex;
import org.openide.util.SharedClassObject;
import org.openide.util.WeakSet;
import org.openide.util.actions.CallbackSystemAction;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;

public abstract class ActionManager<A extends SystemAction, S extends CallbackSystemAction>
implements PropertyChangeListener,
Runnable {
    protected static final Logger LOG = Logger.getLogger(ActionManager.class.getName());
    protected final A action;
    private final Set<TopComponent> activatedOnWindows = new WeakSet(8);
    private Object actionMapKey;
    private Class<S> origSysActionCls;

    protected ActionManager(Class<A> actionCls, Class<S> origSysActionCls) {
        this.origSysActionCls = origSysActionCls;
        this.action = (SystemAction)SharedClassObject.findObject(actionCls, (boolean)true);
    }

    void init() {
        TopComponent.getRegistry().addPropertyChangeListener((PropertyChangeListener)this);
        Mutex.EVENT.writeAccess((Runnable)this);
    }

    @Override
    public void run() {
        this.someoneActivated();
    }

    private void someoneActivated() {
        TopComponent win = TopComponent.getRegistry().getActivated();
        if (LOG.isLoggable(Level.FINER)) {
            String winId;
            if (win == null) {
                winId = "<null>";
            } else {
                String winName = win.getDisplayName();
                if (winName == null) {
                    winName = win.getHtmlDisplayName();
                }
                if (winName == null) {
                    winName = win.getName();
                }
                winName = winName != null ? "" + '\"' + winName + '\"' : "<noname>";
                winId = winName + '(' + win.getClass().getName() + ')';
            }
            LOG.log(Level.FINER, "someoneActivated ({0})", winId);
        }
        if (win == null || win instanceof CloneableEditorSupport.Pane) {
            return;
        }
        Object key = this.getActionMapKey();
        ActionMap actionMap = win.getActionMap();
        if (actionMap.get(key) == null && this.activatedOnWindows.add(win)) {
            Action ls = this.getAction();
            actionMap.put(key, ls);
            win.putClientProperty((Object)this.getMappedActionKey(), new WeakReference<Action>(ls));
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String propName = evt.getPropertyName();
        if ("activated".equals(propName)) {
            this.someoneActivated();
        }
    }

    private Object getActionMapKey() {
        if (this.actionMapKey == null) {
            CallbackSystemAction systemAction = (CallbackSystemAction)SharedClassObject.findObject(this.origSysActionCls, (boolean)true);
            assert (systemAction != null);
            this.actionMapKey = systemAction.getActionMapKey();
        }
        return this.actionMapKey;
    }

    public abstract String getMappedActionKey();

    protected abstract Action getAction();

    static final class ReplaceActionManager
    extends ActionManager<ReplaceInFilesAction.Selection, ReplaceAction> {
        private static final String MAPPED_FIND_ACTION = ReplaceActionManager.class.getName() + " - ReplActionImpl";
        private static ReplaceActionManager instance = null;

        private ReplaceActionManager() {
            super(ReplaceInFilesAction.Selection.class, ReplaceAction.class);
        }

        @Override
        public String getMappedActionKey() {
            return MAPPED_FIND_ACTION;
        }

        @Override
        protected Action getAction() {
            return this.action;
        }

        static ReplaceActionManager getInstance() {
            LOG.finer("getInstance()");
            if (instance == null) {
                instance = new ReplaceActionManager();
            }
            return instance;
        }
    }

    static final class FindActionManager
    extends ActionManager<FindInFilesAction.Selection, FindAction> {
        private static final String MAPPED_FIND_ACTION = FindActionManager.class.getName() + " - FindActionImpl";
        private static FindActionManager instance = null;

        private FindActionManager() {
            super(FindInFilesAction.Selection.class, FindAction.class);
        }

        @Override
        public String getMappedActionKey() {
            return MAPPED_FIND_ACTION;
        }

        @Override
        protected Action getAction() {
            return this.action;
        }

        static FindActionManager getInstance() {
            LOG.finer("getInstance()");
            if (instance == null) {
                instance = new FindActionManager();
            }
            return instance;
        }
    }

}

