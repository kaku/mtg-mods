/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.search;

import org.netbeans.modules.search.TextDetail;

interface TextDisplayer {
    public void setText(String var1, String var2, TextDetail var3);
}

