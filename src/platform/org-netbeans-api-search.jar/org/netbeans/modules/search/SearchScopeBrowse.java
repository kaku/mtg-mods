/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.filesystems.FileChooserBuilder
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.ImageUtilities
 */
package org.netbeans.modules.search;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.Icon;
import javax.swing.UIManager;
import org.netbeans.api.search.SearchRoot;
import org.netbeans.api.search.SearchScopeOptions;
import org.netbeans.api.search.provider.SearchInfo;
import org.netbeans.api.search.provider.SearchInfoUtils;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.modules.search.Bundle;
import org.netbeans.modules.search.ui.UiUtils;
import org.netbeans.spi.search.SearchInfoDefinition;
import org.netbeans.spi.search.SearchScopeDefinition;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.ImageUtilities;

public class SearchScopeBrowse {
    private static final String ICON_KEY_UIMANAGER_NB = "Nb.Explorer.Folder.openedIcon";
    private static final Icon ICON;
    private static FileObject[] roots;
    private SearchScopeDefinition browseScope;
    private SearchScopeDefinition getLastScope;

    public SearchScopeBrowse() {
        this.browseScope = new BrowseScope();
        this.getLastScope = new GetLastScope();
    }

    private FileObject[] chooseRoots() {
        FileChooserBuilder chooserBuilder = new FileChooserBuilder(SearchScopeBrowse.class);
        chooserBuilder.setTitle(UiUtils.getText("LBL_ScopeBrowseFileChooserTitle"));
        chooserBuilder.setApproveText(UiUtils.getText("LBL_ScopeBrowseFileChooserApprove"));
        File[] files = chooserBuilder.showMultiOpenDialog();
        if (files == null) {
            files = new File[]{};
        }
        List<File> existingFiles = this.selectExistingFiles(files);
        FileObject[] fileObjects = new FileObject[existingFiles.size()];
        for (int i = 0; i < existingFiles.size(); ++i) {
            fileObjects[i] = FileUtil.toFileObject((File)existingFiles.get(i));
        }
        if (fileObjects.length > 0) {
            roots = fileObjects;
        }
        return fileObjects;
    }

    private List<File> selectExistingFiles(File[] files) {
        ArrayList<File> existingFiles = new ArrayList<File>(files.length);
        boolean errorShown = false;
        for (int i = 0; i < files.length; ++i) {
            if (files[i].exists()) {
                existingFiles.add(files[i]);
                continue;
            }
            if (errorShown) continue;
            DialogDisplayer.getDefault().notifyLater(new NotifyDescriptor((Object)Bundle.MSG_FileDoesNotExist(files[i]), Bundle.TTL_FileDoesNotExist(), -1, 0, null, NotifyDescriptor.OK_OPTION));
            errorShown = true;
        }
        return existingFiles;
    }

    public SearchScopeDefinition getBrowseScope() {
        return this.browseScope;
    }

    public SearchScopeDefinition getGetLastScope() {
        return this.getLastScope;
    }

    static {
        roots = null;
        Icon icon = UIManager.getIcon("Nb.Explorer.Folder.openedIcon");
        if (icon == null) {
            icon = ImageUtilities.loadImageIcon((String)"org/openide/loaders/defaultFolder.gif", (boolean)false);
        }
        ICON = icon;
    }

    private class BrowseSearchInfo
    extends SearchInfoDefinition {
        private SearchInfo delegate;

        private BrowseSearchInfo() {
        }

        @Override
        public boolean canSearch() {
            return true;
        }

        @Override
        public Iterator<FileObject> filesToSearch(SearchScopeOptions options, SearchListener listener, AtomicBoolean terminated) {
            return this.getDelegate().getFilesToSearch(options, listener, terminated).iterator();
        }

        @Override
        public List<SearchRoot> getSearchRoots() {
            return this.getDelegate().getSearchRoots();
        }

        private synchronized SearchInfo getDelegate() {
            if (this.delegate == null) {
                this.delegate = this.createDelegate();
            }
            return this.delegate;
        }

        private SearchInfo createDelegate() {
            FileObject[] fileObjects = SearchScopeBrowse.this.chooseRoots();
            return SearchInfoUtils.createSearchInfoForRoots(fileObjects);
        }
    }

    private class GetLastScope
    extends SearchScopeDefinition {
        private GetLastScope() {
        }

        @Override
        public String getTypeId() {
            return "browse";
        }

        @Override
        public String getDisplayName() {
            if (roots != null && roots.length > 0) {
                return UiUtils.getText("LBL_ScopeBrowseName") + " [" + roots[0].getName() + (roots.length > 1 ? "..." : "") + "]";
            }
            return "no files selected";
        }

        @Override
        public boolean isApplicable() {
            return roots != null && roots.length > 0;
        }

        @Override
        public SearchInfo getSearchInfo() {
            return SearchInfoUtils.createSearchInfoForRoots(roots);
        }

        @Override
        public int getPriority() {
            return 500;
        }

        @Override
        public void clean() {
        }

        @Override
        public Icon getIcon() {
            return ICON;
        }
    }

    private class BrowseScope
    extends SearchScopeDefinition {
        private SearchInfo searchInfo;

        private BrowseScope() {
            this.searchInfo = SearchInfoUtils.createForDefinition(new BrowseSearchInfo());
        }

        @Override
        public String getTypeId() {
            return "browse";
        }

        @Override
        public String getDisplayName() {
            return UiUtils.getText("LBL_ScopeBrowseName");
        }

        @Override
        public boolean isApplicable() {
            return true;
        }

        @Override
        public SearchInfo getSearchInfo() {
            return this.searchInfo;
        }

        @Override
        public int getPriority() {
            return 501;
        }

        @Override
        public void clean() {
        }

        @Override
        public void selected() {
            SearchScopeBrowse.this.chooseRoots();
            this.notifyListeners();
        }

        @Override
        public Icon getIcon() {
            return ICON;
        }
    }

}

