/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.search;

import java.awt.EventQueue;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.search.SearchPattern;
import org.netbeans.modules.search.BasicComposition;
import org.netbeans.modules.search.BasicSearchCriteria;
import org.netbeans.modules.search.ResultModel;
import org.netbeans.modules.search.Selectable;
import org.netbeans.modules.search.TextDetail;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;

public final class MatchingObject
implements Comparable<MatchingObject>,
Selectable {
    public static final String PROP_INVALIDITY_STATUS = "invalidityStatus";
    public static final String PROP_MATCHES_SELECTED = "matchesSelected";
    public static final String PROP_SELECTED = "selected";
    public static final String PROP_REMOVED = "removed";
    public static final String PROP_CHILD_REMOVED = "child_removed";
    private static final Logger LOG = Logger.getLogger(MatchingObject.class.getName());
    private static final int FILE_READ_BUFFER_SIZE = 4096;
    private final ResultModel resultModel;
    private FileObject fileObject;
    private DataObject dataObject;
    private long timestamp;
    private int matchesCount = 0;
    private Node nodeDelegate = null;
    List<TextDetail> textDetails;
    private Charset charset;
    private boolean selected = true;
    private boolean expanded = false;
    private int selectedMatchesCount = 0;
    private boolean valid = true;
    private boolean refreshed = false;
    private InvalidityStatus invalidityStatus = null;
    private StringBuilder text;
    private final PropertyChangeSupport changeSupport;
    private FileListener fileListener;
    private final MatchSelectionListener matchSelectionListener;
    private static final boolean REALLY_WRITE = true;

    MatchingObject(ResultModel resultModel, FileObject fileObject, Charset charset, List<TextDetail> textDetails) {
        this.changeSupport = new PropertyChangeSupport(this);
        this.matchSelectionListener = new MatchSelectionListener();
        if (resultModel == null) {
            throw new IllegalArgumentException("resultModel = null");
        }
        if (fileObject == null) {
            throw new IllegalArgumentException("object = null");
        }
        this.textDetails = textDetails;
        this.resultModel = resultModel;
        this.charset = charset;
        this.fileObject = fileObject;
        this.dataObject = this.dataObject();
        this.timestamp = fileObject.lastModified().getTime();
        boolean bl = this.valid = this.timestamp != 0;
        if (this.dataObject != null) {
            this.matchesCount = this.computeMatchesCount();
            this.nodeDelegate = this.dataObject.getNodeDelegate();
        }
        this.setUpDataObjValidityChecking();
        if (textDetails != null && !textDetails.isEmpty()) {
            this.adjustTextDetails();
        }
    }

    private void adjustTextDetails() {
        TextDetail lastDetail = this.textDetails.get(this.textDetails.size() - 1);
        int maxLine = lastDetail.getLine();
        int maxDigits = this.countDigits(maxLine);
        for (TextDetail td : this.textDetails) {
            ++this.selectedMatchesCount;
            int digits = this.countDigits(td.getLine());
            if (digits < maxDigits) {
                td.setLineNumberIndent(this.indent(maxDigits - digits));
            }
            td.addChangeListener(this.matchSelectionListener);
        }
    }

    private int countDigits(int number) {
        int digits = 0;
        while (number > 0) {
            number /= 10;
            ++digits;
        }
        return digits;
    }

    private String indent(int chars) {
        switch (chars) {
            case 1: {
                return "&nbsp;&nbsp;";
            }
            case 2: {
                return "&nbsp;&nbsp;&nbsp;&nbsp;";
            }
            case 3: {
                return "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            }
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < chars; ++i) {
            sb.append("&nbsp;&nbsp;");
        }
        return sb.toString();
    }

    private void setUpDataObjValidityChecking() {
        if (this.fileObject != null && this.fileObject.isValid()) {
            this.fileListener = new FileListener();
            this.fileObject.addFileChangeListener((FileChangeListener)this.fileListener);
        }
    }

    void cleanup() {
        if (this.fileObject != null && this.fileListener != null) {
            this.fileObject.removeFileChangeListener((FileChangeListener)this.fileListener);
            this.fileListener = null;
        }
        this.dataObject = null;
        this.nodeDelegate = null;
        this.changeSupport.firePropertyChange("removed", null, null);
    }

    private void setInvalid(InvalidityStatus invalidityStatus) {
        if (this.invalidityStatus == invalidityStatus) {
            return;
        }
        InvalidityStatus oldStatus = this.invalidityStatus;
        this.valid = false;
        this.invalidityStatus = invalidityStatus;
        if (this.fileObject != null && this.fileListener != null && invalidityStatus == InvalidityStatus.DELETED) {
            this.fileObject.removeFileChangeListener((FileChangeListener)this.fileListener);
        }
        this.changeSupport.firePropertyChange("invalidityStatus", (Object)oldStatus, (Object)invalidityStatus);
    }

    public boolean isObjectValid() {
        return this.valid && this.dataObject != null ? this.dataObject.isValid() : false;
    }

    public FileObject getFileObject() {
        return this.fileObject;
    }

    @Override
    public void setSelected(boolean selected) {
        if (selected == this.selected) {
            return;
        }
        this.selected = selected;
        this.changeSupport.firePropertyChange("selected", !selected, selected);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setSelectedRecursively(boolean selected) {
        if (this.selected == selected) {
            return;
        }
        this.matchSelectionListener.setEnabled(false);
        int origMatchesSelected = this.selectedMatchesCount;
        try {
            if (this.textDetails != null) {
                for (TextDetail td : this.getTextDetails()) {
                    td.setSelectedRecursively(selected);
                }
            }
            this.setSelected(selected);
            this.selectedMatchesCount = selected ? this.getTextDetails().size() : 0;
            this.changeSupport.firePropertyChange("matchesSelected", origMatchesSelected, this.selectedMatchesCount);
        }
        finally {
            this.matchSelectionListener.setEnabled(true);
        }
    }

    @Override
    public boolean isSelected() {
        return this.selected;
    }

    void markExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    boolean isExpanded() {
        return this.expanded;
    }

    String getName() {
        return this.getFileObject().getNameExt();
    }

    String getHtmlDisplayName() {
        return this.getFileObject().getNameExt();
    }

    long getTimestamp() {
        return this.timestamp;
    }

    String getDescription() {
        return this.getFileObject().getParent().getPath();
    }

    String getText() throws IOException {
        StringBuilder txt = this.text(false);
        return txt != null ? txt.toString() : null;
    }

    public List<TextDetail> getTextDetails() {
        return this.textDetails;
    }

    public int getDetailsCount() {
        if (this.textDetails == null) {
            return 0;
        }
        return this.textDetails.size();
    }

    public Node[] getDetails() {
        if (this.textDetails == null) {
            return null;
        }
        ArrayList<TextDetail.DetailNode> detailNodes = new ArrayList<TextDetail.DetailNode>(this.textDetails.size());
        for (TextDetail txtDetail : this.textDetails) {
            detailNodes.add(new TextDetail.DetailNode(txtDetail, false, this));
        }
        return detailNodes.toArray((T[])new Node[detailNodes.size()]);
    }

    public Children getDetailsChildren(boolean replacing) {
        return new DetailsChildren(replacing, this.resultModel);
    }

    FileLock lock() throws IOException {
        return this.getFileObject().lock();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    StringBuilder text(boolean refreshCache) throws IOException {
        assert (!EventQueue.isDispatchThread());
        if (refreshCache || this.text == null) {
            if (this.charset == null) {
                this.text = new StringBuilder(this.getFileObject().asText());
            } else {
                this.text = new StringBuilder();
                InputStream istm = this.getFileObject().getInputStream();
                try {
                    CharsetDecoder decoder = this.charset.newDecoder();
                    InputStreamReader isr = new InputStreamReader(istm, decoder);
                    try {
                        BufferedReader br = new BufferedReader(isr, 4096);
                        try {
                            int read;
                            char[] chars = new char[4096];
                            while ((read = br.read(chars)) != -1) {
                                this.text.append(chars, 0, read);
                            }
                        }
                        finally {
                            br.close();
                        }
                    }
                    finally {
                        isr.close();
                    }
                }
                finally {
                    istm.close();
                }
            }
        }
        return this.text;
    }

    @Override
    public int compareTo(MatchingObject o) {
        if (o == null) {
            return Integer.MAX_VALUE;
        }
        return this.getName().compareToIgnoreCase(o.getName());
    }

    private DataObject dataObject() {
        try {
            return DataObject.find((FileObject)this.fileObject);
        }
        catch (DataObjectNotFoundException ex) {
            this.valid = false;
            return null;
        }
    }

    public DataObject getDataObject() {
        return this.dataObject;
    }

    public synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
        this.changeSupport.addPropertyChangeListener(listener);
    }

    public synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
        this.changeSupport.removePropertyChangeListener(listener);
    }

    public synchronized void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        this.changeSupport.addPropertyChangeListener(propertyName, listener);
    }

    public synchronized void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        this.changeSupport.removePropertyChangeListener(propertyName, listener);
    }

    InvalidityStatus checkValidity() {
        InvalidityStatus oldStatus = this.invalidityStatus;
        InvalidityStatus status = this.getFreshInvalidityStatus();
        if (status != null) {
            this.valid = false;
            this.invalidityStatus = status;
        }
        if (oldStatus != this.invalidityStatus) {
            this.changeSupport.firePropertyChange("invalidityStatus", (Object)oldStatus, (Object)this.invalidityStatus);
        }
        return status;
    }

    public InvalidityStatus getInvalidityStatus() {
        return this.invalidityStatus;
    }

    String getInvalidityDescription() {
        InvalidityStatus status = this.getFreshInvalidityStatus();
        String descr = status != null ? status.getDescription(this.getFileObject().getPath()) : null;
        return descr;
    }

    private InvalidityStatus getFreshInvalidityStatus() {
        this.log(Level.FINER, "getInvalidityStatus()");
        FileObject f = this.getFileObject();
        if (!f.isValid()) {
            this.log(Level.FINEST, " - DELETED");
            return InvalidityStatus.DELETED;
        }
        if (f.isFolder()) {
            this.log(Level.FINEST, " - BECAME_DIR");
            return InvalidityStatus.BECAME_DIR;
        }
        long stamp = f.lastModified().getTime();
        if (!this.refreshed && stamp > this.resultModel.getStartTime() || this.refreshed && stamp > this.timestamp) {
            this.log(Level.SEVERE, "file's timestamp changed since start of the search");
            if (LOG.isLoggable(Level.FINEST)) {
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(stamp);
                this.log(Level.FINEST, " - file stamp:           " + stamp + " (" + cal.getTime() + ')');
                cal.setTimeInMillis(this.resultModel.getStartTime());
                this.log(Level.FINEST, " - result model created: " + this.resultModel.getStartTime() + " (" + cal.getTime() + ')');
            }
            return InvalidityStatus.CHANGED;
        }
        if (f.getSize() > Integer.MAX_VALUE) {
            return InvalidityStatus.TOO_BIG;
        }
        if (!f.canRead()) {
            return InvalidityStatus.CANT_READ;
        }
        return null;
    }

    boolean isValid() {
        return this.valid;
    }

    public void updateDataObject(DataObject updatedDataObject) {
        FileObject updatedPF = updatedDataObject.getPrimaryFile();
        if (this.dataObject == null || this.dataObject.getPrimaryFile().equals((Object)updatedPF)) {
            if (updatedPF.isValid()) {
                this.invalidityStatus = null;
                if (this.fileListener == null) {
                    this.fileListener = new FileListener();
                    updatedPF.addFileChangeListener((FileChangeListener)this.fileListener);
                } else if (updatedPF != this.dataObject.getPrimaryFile()) {
                    this.dataObject.getPrimaryFile().removeFileChangeListener((FileChangeListener)this.fileListener);
                    updatedPF.addFileChangeListener((FileChangeListener)this.fileListener);
                }
                this.dataObject = updatedDataObject;
                this.nodeDelegate = updatedDataObject.getNodeDelegate();
                this.valid = true;
                for (TextDetail td : this.textDetails) {
                    td.updateDataObject(updatedDataObject);
                }
            }
        } else {
            throw new IllegalArgumentException("Expected data object for the same file");
        }
    }

    public InvalidityStatus replace() throws IOException {
        assert (!EventQueue.isDispatchThread());
        assert (this.isSelected());
        StringBuilder content = this.text(true);
        List<TextDetail> textMatches = this.getTextDetails();
        int toReplace = 0;
        for (TextDetail td : textMatches) {
            toReplace += td.isSelected() ? 1 : 0;
        }
        if (toReplace == 0) {
            return null;
        }
        int offsetShift = 0;
        for (int i = 0; i < textMatches.size(); ++i) {
            TextDetail textDetail = textMatches.get(i);
            if (!textDetail.isSelected()) continue;
            String matchedSubstring = content.substring(textDetail.getStartOffset() + offsetShift, textDetail.getEndOffset() + offsetShift);
            if (!matchedSubstring.equals(textDetail.getMatchedText())) {
                this.log(Level.SEVERE, "file match part differs from the expected match");
                if (LOG.isLoggable(Level.FINEST)) {
                    this.log(Level.SEVERE, " - expected line: \"" + textDetail.getMatchedText() + '\"');
                    this.log(Level.SEVERE, " - file line:     \"" + matchedSubstring + '\"');
                }
                return InvalidityStatus.CHANGED;
            }
            String replacedString = this.resultModel.basicCriteria.getReplaceExpr();
            if (this.resultModel.basicCriteria.getSearchPattern().isRegExp()) {
                Matcher m = this.resultModel.basicCriteria.getTextPattern().matcher(matchedSubstring);
                replacedString = m.replaceFirst(this.resultModel.basicCriteria.getReplaceString());
            } else if (this.resultModel.basicCriteria.isPreserveCase()) {
                replacedString = MatchingObject.adaptCase(replacedString, matchedSubstring);
            }
            content.replace(textDetail.getStartOffset() + offsetShift, textDetail.getEndOffset() + offsetShift, replacedString);
            offsetShift += replacedString.length() - matchedSubstring.length();
        }
        return null;
    }

    public static String adaptCase(String value, String casePattern) {
        if (casePattern.equals(casePattern.toUpperCase())) {
            return value.toUpperCase();
        }
        if (casePattern.equals(casePattern.toLowerCase())) {
            return value.toLowerCase();
        }
        if (Character.isUpperCase(casePattern.charAt(0))) {
            return "" + Character.toUpperCase(value.charAt(0)) + value.substring(1);
        }
        if (Character.isLowerCase(casePattern.charAt(0))) {
            if (casePattern.substring(1).equals(casePattern.substring(1).toUpperCase())) {
                return "" + Character.toLowerCase(value.charAt(0)) + value.substring(1).toUpperCase();
            }
            return "" + Character.toLowerCase(value.charAt(0)) + value.substring(1);
        }
        return value;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void write(FileLock fileLock) throws IOException {
        if (this.text == null) {
            throw new IllegalStateException("Buffer is gone");
        }
        Writer writer = null;
        try {
            writer = new OutputStreamWriter(this.fileObject.getOutputStream(fileLock), this.charset);
            writer.write(this.makeStringToWrite());
        }
        finally {
            if (writer != null) {
                writer.close();
            }
        }
    }

    private String makeStringToWrite() {
        return MatchingObject.makeStringToWrite(this.text);
    }

    static String makeStringToWrite(StringBuilder text) {
        return text.toString();
    }

    private void log(Level logLevel, String msg) {
        String id;
        String string = id = this.dataObject != null ? this.dataObject.getName() : this.fileObject.toString();
        if (LOG.isLoggable(logLevel)) {
            LOG.log(logLevel, "{0}: {1}", new Object[]{id, msg});
        }
    }

    public String toString() {
        return super.toString() + "[" + this.getName() + "]";
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        MatchingObject other = (MatchingObject)obj;
        if (this.resultModel == other.resultModel || this.resultModel != null && this.resultModel.equals(other.resultModel)) {
            return this.fileObject == other.fileObject || this.fileObject != null && this.fileObject.equals((Object)other.fileObject);
        }
        return false;
    }

    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + (this.fileObject != null ? this.fileObject.hashCode() : 0);
        hash = 73 * hash + (this.resultModel != null ? this.resultModel.hashCode() : 0);
        return hash;
    }

    private int computeMatchesCount() {
        return this.resultModel.getDetailsCount(this);
    }

    String getFileDisplayName() {
        return FileUtil.getFileDisplayName((FileObject)this.fileObject);
    }

    int getMatchesCount() {
        return this.matchesCount;
    }

    Node getNodeDelegate() {
        return this.nodeDelegate;
    }

    public void removeDetail(TextDetail textDetail) {
        boolean removed = this.textDetails.remove(textDetail);
        if (removed) {
            this.matchesCount = this.getDetailsCount();
            this.resultModel.removeDetailMatch(this, textDetail);
            this.changeSupport.firePropertyChange("child_removed", null, null);
        }
    }

    public void remove() {
        this.resultModel.remove(this);
    }

    public void refresh(Def def) {
        this.refreshed = true;
        this.charset = def.getCharset();
        FileObject origFileObject = this.fileObject;
        this.fileObject = def.getFileObject();
        this.textDetails = def.getTextDetails();
        this.dataObject = this.dataObject();
        this.timestamp = this.fileObject.lastModified().getTime();
        boolean bl = this.valid = this.timestamp != 0;
        if (this.dataObject == null) {
            return;
        }
        if (this.fileObject != origFileObject) {
            if (this.fileListener != null) {
                origFileObject.removeFileChangeListener((FileChangeListener)this.fileListener);
            }
            this.setUpDataObjValidityChecking();
        }
        this.nodeDelegate = this.dataObject.getNodeDelegate();
        Mutex.EVENT.writeAccess(new Runnable(){

            @Override
            public void run() {
                int origSelectedMatches = MatchingObject.this.selectedMatchesCount;
                MatchingObject.this.selectedMatchesCount = 0;
                if (MatchingObject.this.textDetails != null && !MatchingObject.this.textDetails.isEmpty()) {
                    MatchingObject.this.adjustTextDetails();
                }
                MatchingObject.this.changeSupport.firePropertyChange("matchesSelected", origSelectedMatches, MatchingObject.this.selectedMatchesCount);
                if (MatchingObject.this.matchesCount > 0) {
                    MatchingObject.this.setSelected(true);
                }
                InvalidityStatus origInvStat = MatchingObject.this.invalidityStatus;
                MatchingObject.this.invalidityStatus = null;
                MatchingObject.this.changeSupport.firePropertyChange("invalidityStatus", (Object)origInvStat, (Object)MatchingObject.this.invalidityStatus);
            }
        });
    }

    public BasicComposition getBasicComposition() {
        return this.resultModel.basicComposition;
    }

    static /* synthetic */ ResultModel access$800(MatchingObject x0) {
        return x0.resultModel;
    }

    static /* synthetic */ int access$212(MatchingObject x0, int x1) {
        return x0.selectedMatchesCount += x1;
    }

    private class MatchSelectionListener
    implements ChangeListener {
        private boolean enabled;

        private MatchSelectionListener() {
            this.enabled = true;
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            if (this.enabled) {
                TextDetail td = (TextDetail)e.getSource();
                int origMatchesSelected = MatchingObject.this.selectedMatchesCount;
                MatchingObject.access$212(MatchingObject.this, td.isSelected() ? 1 : -1);
                MatchingObject.this.changeSupport.firePropertyChange("matchesSelected", origMatchesSelected, MatchingObject.this.selectedMatchesCount);
                if (MatchingObject.this.selected && MatchingObject.this.selectedMatchesCount == 0) {
                    MatchingObject.this.setSelected(false);
                } else if (!MatchingObject.this.selected && MatchingObject.this.selectedMatchesCount > 0) {
                    MatchingObject.this.setSelected(true);
                }
            }
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }
    }

    private class FileListener
    extends FileChangeAdapter {
        private FileListener() {
        }

        public void fileDeleted(FileEvent fe) {
            MatchingObject.this.setInvalid(InvalidityStatus.DELETED);
        }

        public void fileChanged(FileEvent fe) {
            if (MatchingObject.access$800((MatchingObject)MatchingObject.this).basicCriteria.isSearchAndReplace()) {
                MatchingObject.this.setInvalid(InvalidityStatus.CHANGED);
            }
        }
    }

    private class DetailsChildren
    extends Children.Keys<TextDetail> {
        private final boolean replacing;

        public DetailsChildren(boolean replacing, ResultModel model) {
            this.replacing = replacing;
            this.setKeys(MatchingObject.this.getTextDetails());
            MatchingObject.this.addPropertyChangeListener("child_removed", new PropertyChangeListener(MatchingObject.this){
                final /* synthetic */ MatchingObject val$this$0;

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    DetailsChildren.this.update();
                }
            });
        }

        protected Node[] createNodes(TextDetail key) {
            return new Node[]{new TextDetail.DetailNode(key, this.replacing, MatchingObject.this)};
        }

        public void update() {
            this.setKeys(MatchingObject.this.getTextDetails());
        }

    }

    public static class Def {
        private FileObject fileObject;
        private Charset charset;
        private List<TextDetail> textDetails;

        public Def(FileObject fileObject, Charset charset, List<TextDetail> textDetails) {
            this.fileObject = fileObject;
            this.charset = charset;
            this.textDetails = textDetails;
        }

        public Charset getCharset() {
            return this.charset;
        }

        public void setCharset(Charset charset) {
            this.charset = charset;
        }

        public FileObject getFileObject() {
            return this.fileObject;
        }

        public void setFileObject(FileObject fileObject) {
            this.fileObject = fileObject;
        }

        public List<TextDetail> getTextDetails() {
            return this.textDetails;
        }

        public void setTextDetails(List<TextDetail> textDetails) {
            this.textDetails = textDetails;
        }
    }

    public static enum InvalidityStatus {
        DELETED(true, "Inv_status_Err_deleted"),
        BECAME_DIR(true, "Inv_status_Err_became_dir"),
        CHANGED(false, "Inv_status_Err_changed"),
        TOO_BIG(false, "Inv_status_Err_too_big"),
        CANT_READ(false, "Inv_status_Err_cannot_read");
        
        private final boolean fatal;
        private final String descrBundleKey;

        private InvalidityStatus(boolean fatal, String descrBundleKey) {
            this.fatal = fatal;
            this.descrBundleKey = descrBundleKey;
        }

        boolean isFatal() {
            return this.fatal;
        }

        String getDescription(String path) {
            return NbBundle.getMessage(this.getClass(), (String)this.descrBundleKey, (Object)path);
        }
    }

}

