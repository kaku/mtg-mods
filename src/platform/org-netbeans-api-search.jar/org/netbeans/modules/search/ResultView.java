/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.MouseUtils
 *  org.openide.awt.MouseUtils$PopupMouseAdapter
 *  org.openide.awt.TabbedPaneFactory
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.NbBundle
 *  org.openide.util.lookup.Lookups
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Description
 *  org.openide.windows.WindowManager
 */
package org.netbeans.modules.search;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.FocusManager;
import javax.swing.Icon;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.search.Manager;
import org.netbeans.modules.search.ReplaceTask;
import org.netbeans.modules.search.ResultViewPanel;
import org.netbeans.modules.search.SearchTask;
import org.netbeans.modules.search.ui.UiUtils;
import org.netbeans.spi.search.provider.SearchComposition;
import org.netbeans.spi.search.provider.SearchResultsDisplayer;
import org.openide.awt.MouseUtils;
import org.openide.awt.TabbedPaneFactory;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

@TopComponent.Description(preferredID="search-results", persistenceType=0, iconBase="org/netbeans/modules/search/res/find.gif")
public final class ResultView
extends TopComponent {
    private static final boolean isMacLaf = "Aqua".equals(UIManager.getLookAndFeel().getID());
    private static final Color macBackground = UIManager.getColor("NbExplorerView.background");
    private static final String CARD_NAME_EMPTY = "empty";
    private static final String CARD_NAME_TABS = "tabs";
    private static final String CARD_NAME_SINGLE = "single";
    static final String ID = "search-results";
    private JPopupMenu pop;
    private PopupListener popL;
    private CloseListener closeL;
    private JPanel emptyPanel;
    private JPanel singlePanel;
    private JTabbedPane tabs;
    private WeakReference<ResultViewPanel> tabToReuse;
    private CurrentLookupProvider lookupProvider = new CurrentLookupProvider();
    private final CardLayout contentCards = new CardLayout();
    private Map<SearchTask, ResultViewPanel> searchToViewMap = new HashMap<SearchTask, ResultViewPanel>();
    private Map<ResultViewPanel, SearchTask> viewToSearchMap = new HashMap<ResultViewPanel, SearchTask>();
    private Map<ReplaceTask, SearchTask> replaceToSearchMap = new HashMap<ReplaceTask, SearchTask>();
    private Map<SearchTask, ReplaceTask> searchToReplaceMap = new HashMap<SearchTask, ReplaceTask>();

    public static synchronized ResultView getInstance() {
        ResultView view = (ResultView)WindowManager.getDefault().findTopComponent("search-results");
        if (view == null) {
            view = new ResultView();
        }
        return view;
    }

    public ResultView() {
        this.setLayout((LayoutManager)this.contentCards);
        this.setName("Search Results");
        this.setDisplayName(NbBundle.getMessage(ResultView.class, (String)"TITLE_SEARCH_RESULTS"));
        this.initAccessibility();
        this.pop = new JPopupMenu();
        this.pop.add(new Close());
        this.pop.add(new CloseAll());
        this.pop.add(new CloseAllButCurrent());
        this.popL = new PopupListener();
        this.closeL = new CloseListener();
        this.emptyPanel = new JPanel();
        this.singlePanel = new JPanel();
        this.singlePanel.setLayout(new BoxLayout(this.singlePanel, 3));
        this.emptyPanel.setOpaque(true);
        this.tabs = TabbedPaneFactory.createCloseButtonTabbedPane();
        this.tabs.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                ResultView.this.updateLookup();
            }
        });
        this.tabs.setMinimumSize(new Dimension(0, 0));
        this.tabs.addMouseListener((MouseListener)((Object)this.popL));
        this.tabs.addPropertyChangeListener(this.closeL);
        this.add((Component)this.emptyPanel, (Object)"empty");
        this.add((Component)this.tabs, (Object)"tabs");
        this.add((Component)this.singlePanel, (Object)"single");
        if (isMacLaf) {
            this.emptyPanel.setBackground(macBackground);
            this.tabs.setBackground(macBackground);
            this.tabs.setOpaque(true);
            this.setBackground(macBackground);
            this.setOpaque(true);
        } else {
            this.emptyPanel.setBackground(UIManager.getColor("Tree.background"));
        }
        this.contentCards.show((Container)((Object)this), "empty");
        this.associateLookup(Lookups.proxy((Lookup.Provider)this.lookupProvider));
    }

    private void initAccessibility() {
        ResourceBundle bundle = NbBundle.getBundle(ResultView.class);
        this.getAccessibleContext().setAccessibleName(bundle.getString("ACSN_ResultViewTopComponent"));
        this.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_ResultViewTopComponent"));
    }

    void closeResults() {
        this.close();
    }

    protected void componentOpened() {
        assert (EventQueue.isDispatchThread());
        Manager.getInstance().searchWindowOpened();
        ResultViewPanel panel = this.getCurrentResultViewPanel();
        if (panel != null) {
            panel.componentOpened();
        }
        this.setToolTipText(NbBundle.getMessage(ResultView.class, (String)"TOOLTIP_SEARCH_RESULTS"));
    }

    public void requestFocus() {
        ResultViewPanel panel = this.getCurrentResultViewPanel();
        if (panel != null) {
            panel.requestFocus();
        }
    }

    public boolean requestFocusInWindow() {
        ResultViewPanel panel = this.getCurrentResultViewPanel();
        if (panel != null) {
            return panel.requestFocusInWindow();
        }
        return false;
    }

    private ResultViewPanel getCurrentResultViewPanel() {
        if (this.singlePanel.getComponents().length == 1) {
            Component comp = this.singlePanel.getComponents()[0];
            if (comp instanceof ResultViewPanel) {
                return (ResultViewPanel)comp;
            }
            return null;
        }
        if (this.tabs.getTabCount() > 0) {
            Component comp = this.tabs.getSelectedComponent();
            if (comp instanceof ResultViewPanel) {
                return (ResultViewPanel)comp;
            }
            return null;
        }
        return null;
    }

    private String getTabTitle(Component panel) {
        return NbBundle.getMessage(ResultView.class, (String)"TEXT_MSG_RESULTS_FOR_X", (Object)String.valueOf(panel.getName()));
    }

    private void updateTabTitle(JPanel panel) {
        if (this.getComponentCount() != 0 && this.tabs.getTabCount() > 0) {
            int index = this.tabs.indexOfComponent(panel);
            this.tabs.setTitleAt(index, this.getTabTitle(panel));
            this.tabs.setToolTipTextAt(index, panel.getToolTipText());
        }
    }

    private void removePanel(ResultViewPanel panel) {
        if (this.tabs.getTabCount() > 0) {
            if (panel == null) {
                panel = (ResultViewPanel)this.tabs.getSelectedComponent();
            }
            if (panel.isSearchInProgress()) {
                panel.getSearchComposition().terminate();
            }
            this.tabs.remove(panel);
            panel.getSearchComposition().getSearchResultsDisplayer().closed();
            if (this.tabs.getTabCount() == 0) {
                this.contentCards.show((Container)((Object)this), "empty");
                this.updateLookup();
            } else if (this.tabs.getTabCount() == 1) {
                Component c = this.tabs.getComponentAt(0);
                this.singlePanel.add(c);
                this.contentCards.show((Container)((Object)this), "single");
                this.updateLookup();
            }
            this.repaint();
        } else if (this.singlePanel.getComponents().length == 1) {
            Component comp = this.singlePanel.getComponents()[0];
            ResultViewPanel rvp = (ResultViewPanel)comp;
            if (rvp.isSearchInProgress()) {
                Manager.getInstance().stopSearching(this.viewToSearchMap.get(rvp));
            }
            this.singlePanel.remove(comp);
            this.contentCards.show((Container)((Object)this), "empty");
            rvp.getSearchComposition().getSearchResultsDisplayer().closed();
            this.repaint();
        } else {
            this.close();
        }
        SearchTask sTask = this.viewToSearchMap.remove(panel);
        this.searchToViewMap.remove(sTask);
        ReplaceTask rTask = this.searchToReplaceMap.remove(sTask);
        this.replaceToSearchMap.remove(rTask);
        this.validate();
        this.updateTooltip();
    }

    protected void componentClosed() {
        assert (EventQueue.isDispatchThread());
        Manager.getInstance().searchWindowClosed();
        this.closeAll(false);
    }

    void notifySearchPending(SearchTask task, int blockingTask) {
        assert (EventQueue.isDispatchThread());
        ResultViewPanel panel = this.searchToViewMap.get(task);
        if (panel != null) {
            String msgKey = null;
            switch (blockingTask) {
                case 8: {
                    msgKey = "TEXT_FINISHING_REPLACE";
                    break;
                }
                case 1: {
                    msgKey = "TEXT_FINISHING_PREV_SEARCH";
                    break;
                }
                default: {
                    assert (false);
                    break;
                }
            }
            panel.showInfo(NbBundle.getMessage(ResultView.class, (String)msgKey));
            panel.setBtnStopEnabled(true);
        }
    }

    void searchTaskStateChanged(SearchTask task, int changeType) {
        assert (EventQueue.isDispatchThread());
        ResultViewPanel panel = this.searchToViewMap.get(task);
        if (panel == null) {
            return;
        }
        switch (changeType) {
            case 1: {
                this.updateTabTitle(panel);
                panel.searchStarted();
                break;
            }
            case 2: {
                panel.searchFinished();
                break;
            }
            case 3: {
                panel.searchInterrupted();
                break;
            }
            case 4: {
                panel.searchCancelled();
                break;
            }
            default: {
                assert (false);
                break;
            }
        }
    }

    void showAllDetailsFinished() {
        assert (EventQueue.isDispatchThread());
    }

    void addSearchPair(ResultViewPanel panel, SearchTask task) {
        if (task != null && panel != null) {
            SearchTask oldTask = this.viewToSearchMap.get(panel);
            if (oldTask != null) {
                this.searchToViewMap.remove(oldTask);
            }
            this.searchToViewMap.put(task, panel);
            this.viewToSearchMap.put(panel, task);
        }
    }

    private void closeAll(boolean butCurrent) {
        Component comp;
        if (this.tabs.getTabCount() > 0) {
            Component current = this.tabs.getSelectedComponent();
            Component[] c = this.tabs.getComponents();
            for (int i = 0; i < c.length; ++i) {
                if (butCurrent && c[i] == current || !(c[i] instanceof ResultViewPanel)) continue;
                this.removePanel((ResultViewPanel)c[i]);
            }
        } else if (this.singlePanel.getComponents().length > 0 && (comp = this.singlePanel.getComponents()[0]) instanceof ResultViewPanel) {
            this.removePanel((ResultViewPanel)comp);
        }
    }

    ResultViewPanel addTab(SearchTask searchTask) {
        int tabIndex = this.tryReuse();
        ResultViewPanel panel = new ResultViewPanel(searchTask);
        SearchComposition composition = searchTask.getComposition();
        String title = composition.getSearchResultsDisplayer().getTitle();
        if (this.singlePanel.getComponents().length == 0 && this.tabs.getTabCount() == 0) {
            this.singlePanel.add(panel);
            this.contentCards.show((Container)((Object)this), "single");
            this.updateLookup();
        } else if (this.singlePanel.getComponents().length == 1) {
            ResultViewPanel comp = (ResultViewPanel)this.singlePanel.getComponents()[0];
            this.tabs.insertTab(comp.getName(), null, comp, comp.getToolTipText(), 0);
            this.tabs.setToolTipTextAt(0, comp.getToolTipText());
            int tabToInsert = tabIndex > -1 ? tabIndex : 1;
            this.tabs.insertTab(title, null, panel, panel.getToolTipText(), tabToInsert);
            this.tabs.setToolTipTextAt(tabToInsert, panel.getToolTipText());
            this.tabs.setSelectedIndex(tabIndex > -1 ? tabIndex : 1);
            this.contentCards.show((Container)((Object)this), "tabs");
        } else {
            this.tabs.insertTab(title, null, panel, panel.getToolTipText(), tabIndex > -1 ? tabIndex : this.tabs.getTabCount());
            this.tabs.setToolTipTextAt(tabIndex > -1 ? tabIndex : this.tabs.getTabCount() - 1, panel.getToolTipText());
            this.tabs.setSelectedComponent(panel);
            this.tabs.validate();
        }
        this.validate();
        this.requestActive();
        this.updateTooltip();
        return panel;
    }

    private int tryReuse() {
        int index;
        ResultViewPanel toReuse = this.getTabToReuse();
        if (toReuse == null) {
            return -1;
        }
        if (this.singlePanel.getComponents().length == 1 && this.singlePanel.getComponent(0) == toReuse) {
            this.removePanel(toReuse);
            this.clearReusableTab();
            return 0;
        }
        if (this.tabs.getTabCount() > 0 && (index = this.tabs.indexOfComponent(toReuse)) >= 0) {
            this.removePanel(toReuse);
            this.clearReusableTab();
            return index;
        }
        return this.tabs.getTabCount();
    }

    public boolean isFocused() {
        ResultViewPanel rvp = this.getCurrentResultViewPanel();
        if (rvp != null) {
            Component owner = FocusManager.getCurrentManager().getFocusOwner();
            return owner != null && SwingUtilities.isDescendingFrom(owner, rvp);
        }
        return false;
    }

    private synchronized void setTabToReuse(ResultViewPanel resultViewPanel) {
        this.tabToReuse = resultViewPanel == null ? null : new WeakReference<ResultViewPanel>(resultViewPanel);
    }

    private synchronized ResultViewPanel getTabToReuse() {
        return this.tabToReuse == null || this.tabToReuse.get() == null ? null : this.tabToReuse.get();
    }

    public synchronized void markCurrentTabAsReusable() {
        this.setTabToReuse(this.getCurrentResultViewPanel());
    }

    public synchronized void clearReusableTab() {
        this.setTabToReuse(null);
    }

    private void updateLookup() {
        ResultViewPanel rvp = this.getCurrentResultViewPanel();
        this.lookupProvider.setLookup(rvp == null ? Lookup.EMPTY : rvp.getLookup());
        this.getLookup().lookup(Object.class);
    }

    private void updateTooltip() {
        StringBuilder sb = new StringBuilder();
        sb.append("<html><b>");
        sb.append(NbBundle.getMessage(ResultView.class, (String)"TOOLTIP_SEARCH_RESULTS"));
        sb.append("</b>");
        if (this.singlePanel.getComponents().length == 1) {
            this.appendTabToToolTip(this.singlePanel.getComponent(0), sb);
        } else if (this.tabs.getComponents().length > 0) {
            Component[] comps = this.tabs.getComponents();
            for (int i = 0; i < comps.length; ++i) {
                this.appendTabToToolTip(comps[i], sb);
            }
        }
        sb.append("</html>");
        this.setToolTipText(sb.toString());
    }

    private void appendTabToToolTip(Component c, StringBuilder sb) {
        ResultViewPanel rvp;
        if (c instanceof ResultViewPanel && (rvp = (ResultViewPanel)c).getToolTipText() != null) {
            sb.append("<br>&nbsp;&nbsp;");
            sb.append(UiUtils.escapeHtml(rvp.getToolTipText()));
            sb.append("&nbsp;");
        }
    }

    private static class CurrentLookupProvider
    implements Lookup.Provider {
        private Lookup currentLookup = Lookup.EMPTY;

        private CurrentLookupProvider() {
        }

        public void setLookup(Lookup lookup) {
            this.currentLookup = lookup;
        }

        public Lookup getLookup() {
            return this.currentLookup;
        }
    }

    private class CloseAllButCurrent
    extends AbstractAction {
        public CloseAllButCurrent() {
            super(NbBundle.getMessage(ResultView.class, (String)"LBL_CloseAllButCurrent"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            ResultView.this.closeAll(true);
        }
    }

    private final class CloseAll
    extends AbstractAction {
        public CloseAll() {
            super(NbBundle.getMessage(ResultView.class, (String)"LBL_CloseAll"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            ResultView.this.closeAll(false);
        }
    }

    private class Close
    extends AbstractAction {
        public Close() {
            super(NbBundle.getMessage(ResultView.class, (String)"LBL_CloseWindow"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            ResultView.this.removePanel(null);
        }
    }

    private class PopupListener
    extends MouseUtils.PopupMouseAdapter {
        private PopupListener() {
        }

        protected void showPopup(MouseEvent e) {
            ResultView.this.pop.show((Component)((Object)ResultView.this), e.getX(), e.getY());
        }
    }

    private class CloseListener
    implements PropertyChangeListener {
        private CloseListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("close".equals(evt.getPropertyName())) {
                ResultView.this.removePanel((ResultViewPanel)evt.getNewValue());
            }
        }
    }

    @Deprecated
    public static final class ResolvableHelper
    implements Serializable {
        static final long serialVersionUID = 7398708142639457544L;

        public Object readResolve() {
            return null;
        }
    }

}

