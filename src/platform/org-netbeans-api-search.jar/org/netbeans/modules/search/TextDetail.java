/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.StatusDisplayer
 *  org.openide.cookies.EditCookie
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.LineCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.text.Line
 *  org.openide.text.Line$Set
 *  org.openide.text.Line$ShowOpenType
 *  org.openide.text.Line$ShowVisibilityType
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 *  org.openide.util.TaskListener
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.datatransfer.PasteType
 *  org.openide.util.lookup.Lookups
 *  org.openide.windows.OutputEvent
 *  org.openide.windows.OutputListener
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.modules.search;

import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.io.CharConversionException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.Caret;
import org.netbeans.api.search.SearchHistory;
import org.netbeans.api.search.SearchPattern;
import org.netbeans.modules.search.Bundle;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.Removable;
import org.netbeans.modules.search.Selectable;
import org.netbeans.modules.search.ui.HideResultAction;
import org.netbeans.modules.search.ui.ReplaceCheckableNode;
import org.netbeans.modules.search.ui.ResultsOutlineSupport;
import org.netbeans.modules.search.ui.UiUtils;
import org.openide.awt.StatusDisplayer;
import org.openide.cookies.EditCookie;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.LineCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.text.Line;
import org.openide.util.ChangeSupport;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.TaskListener;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.Lookups;
import org.openide.windows.OutputEvent;
import org.openide.windows.OutputListener;
import org.openide.xml.XMLUtil;

public final class TextDetail
implements Selectable {
    private static final Logger LOG = Logger.getLogger(TextDetail.class.getName());
    private static final RequestProcessor RP = new RequestProcessor(TextDetail.class);
    public static final int DH_SHOW = 1;
    public static final int DH_GOTO = 2;
    public static final int DH_HIDE = 3;
    private DataObject dobj;
    private int line;
    private String lineText;
    private int column;
    private int markLength;
    private Line lineObj;
    private List<SurroundingLine> surroundingLines = null;
    private SearchPattern searchPattern;
    private int startOffset;
    private int endOffset;
    private String matchedText;
    private boolean selected = true;
    private String lineNumberIndent = "";
    private boolean showAfterDataObjectUpdated = false;
    private ChangeSupport changeSupport;

    public TextDetail(DataObject dobj, SearchPattern pattern) {
        this.changeSupport = new ChangeSupport((Object)this);
        this.dobj = dobj;
        this.searchPattern = pattern;
    }

    public void showDetail(final int how) {
        this.prepareLine();
        if (this.lineObj == null) {
            Toolkit.getDefaultToolkit().beep();
            EditCookie ed = (EditCookie)this.dobj.getLookup().lookup(EditCookie.class);
            if (ed != null) {
                ed.edit();
                this.showAfterDataObjectUpdated = true;
            }
            return;
        }
        if (how == 3) {
            return;
        }
        final EditorCookie edCookie = (EditorCookie)this.dobj.getLookup().lookup(EditorCookie.class);
        if (edCookie != null) {
            Task prepareTask = edCookie.prepareDocument();
            prepareTask.addTaskListener(new TaskListener(){

                public void taskFinished(Task task) {
                    EventQueue.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            edCookie.open();
                            TextDetail.this.showLine(how);
                            TextDetail.this.highlightDetail(edCookie);
                        }
                    });
                }

            });
        } else {
            this.showLine(how);
        }
        SearchHistory.getDefault().add(SearchPattern.create(this.searchPattern.getSearchExpression(), this.searchPattern.isWholeWords(), this.searchPattern.isMatchCase(), this.searchPattern.isRegExp()));
    }

    private void showLine(int how) {
        if (this.lineObj == null) {
            return;
        }
        if (how == 1) {
            this.lineObj.show(Line.ShowOpenType.NONE, Line.ShowVisibilityType.NONE, this.column - 1);
        } else if (how == 2) {
            this.lineObj.show(Line.ShowOpenType.OPEN, Line.ShowVisibilityType.FOCUS, this.column - 1);
        }
    }

    private void highlightDetail(EditorCookie edCookie) {
        JEditorPane[] panes;
        if (this.markLength > 0 && (panes = edCookie.getOpenedPanes()) != null && panes.length > 0) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    try {
                        Caret caret = panes[0].getCaret();
                        caret.moveDot(caret.getDot() + TextDetail.this.markLength);
                    }
                    catch (Exception e) {
                        StatusDisplayer.getDefault().setStatusText(Bundle.MSG_CannotShowTextDetai());
                        LOG.log(Level.FINE, Bundle.MSG_CannotShowTextDetai(), e);
                    }
                }
            });
        }
    }

    public String getLineText() {
        return this.lineText;
    }

    public void setLineText(String text) {
        this.lineText = text;
    }

    String getLineTextPart(int beginIndex, int endIndex) {
        if (beginIndex >= 0 && beginIndex <= endIndex) {
            return this.lineText.substring(beginIndex, endIndex);
        }
        LOG.log(Level.WARNING, "Invalid range: beginIndex = {0}, endIndex = {1}.", new Object[]{beginIndex, endIndex});
        return this.lineText.substring(Math.max(0, beginIndex), Math.max(0, Math.max(beginIndex, endIndex)));
    }

    String getLineTextPart(int beginIndex) {
        return this.lineText.substring(beginIndex);
    }

    public int getLineTextLength() {
        return this.lineText == null ? 0 : this.lineText.length();
    }

    public DataObject getDataObject() {
        return this.dobj;
    }

    public int getLine() {
        return this.line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public int getColumn() {
        return this.column;
    }

    public void setColumn(int col) {
        this.column = col;
    }

    int getColumn0() {
        return this.column - 1;
    }

    public void setMarkLength(int len) {
        this.markLength = len;
    }

    public int getMarkLength() {
        return this.markLength;
    }

    public int getEndOffset() {
        return this.endOffset;
    }

    public void setEndOffset(int endOffset) {
        this.endOffset = endOffset;
    }

    public int getStartOffset() {
        return this.startOffset;
    }

    public void setStartOffset(int startOffset) {
        this.startOffset = startOffset;
    }

    public String getMatchedText() {
        return this.matchedText;
    }

    public void setMatchedText(String matchedText) {
        this.matchedText = matchedText;
    }

    public void associate(int lineNumber, int column, String lineText) {
        this.setLine(lineNumber);
        this.setColumn(column);
        this.setLineText(lineText);
    }

    private void prepareLine() {
        LineCookie lineCookie;
        if (this.dobj == null || !this.dobj.isValid()) {
            this.lineObj = null;
        } else if (this.lineObj == null && (lineCookie = (LineCookie)this.dobj.getLookup().lookup(LineCookie.class)) != null) {
            Line.Set lineSet = lineCookie.getLineSet();
            try {
                this.lineObj = lineSet.getOriginal(this.line - 1);
            }
            catch (IndexOutOfBoundsException ioobex) {
                this.lineObj = lineSet.getOriginal(TextDetail.findMaxLine(lineSet));
                this.markLength = 0;
                this.column = 0;
            }
        }
    }

    public void addSurroundingLine(int number, String text) {
        if (this.surroundingLines == null) {
            this.surroundingLines = new ArrayList<SurroundingLine>(5);
        }
        this.surroundingLines.add(new SurroundingLine(number, text));
    }

    private static int findMaxLine(Line.Set set) {
        int from = 0;
        int to = 32000;
        try {
            do {
                set.getOriginal(to);
                from = to;
                to *= 2;
            } while (true);
        }
        catch (IndexOutOfBoundsException ex) {
            while (from < to) {
                int middle = (from + to + 1) / 2;
                try {
                    set.getOriginal(middle);
                    from = middle;
                }
                catch (IndexOutOfBoundsException ex) {
                    to = middle - 1;
                }
            }
            return from;
        }
    }

    @Override
    public boolean isSelected() {
        return this.selected;
    }

    @Override
    public void setSelected(boolean selected) {
        if (this.selected != selected) {
            this.selected = selected;
            this.fireChange();
        }
    }

    @Override
    public void setSelectedRecursively(boolean selected) {
        this.setSelected(selected);
    }

    public void addChangeListener(ChangeListener listener) {
        this.changeSupport.addChangeListener(listener);
    }

    public void removeChangeListener(ChangeListener listener) {
        this.changeSupport.removeChangeListener(listener);
    }

    public void fireChange() {
        this.changeSupport.fireChange();
    }

    public void updateDataObject(DataObject dataObject) {
        if (this.dobj.getPrimaryFile().equals((Object)dataObject.getPrimaryFile())) {
            this.dobj = dataObject;
            this.lineObj = null;
            if (this.showAfterDataObjectUpdated) {
                EventQueue.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        TextDetail.this.showDetail(2);
                    }
                });
                this.showAfterDataObjectUpdated = false;
            }
        } else {
            throw new IllegalArgumentException("Expected data object for the same file");
        }
    }

    void setLineNumberIndent(String lineNumberIndent) {
        this.lineNumberIndent = lineNumberIndent;
    }

    private class SurroundingLine {
        private final int number;
        private final String text;

        public SurroundingLine(int number, String text) {
            this.number = number;
            this.text = DetailNode.cutLongLine(text);
        }

        public int getNumber() {
            return this.number;
        }

        public String getText() {
            return this.text;
        }
    }

    private static class GotoDetailAction
    extends AbstractAction {
        private DetailNode detailNode;

        public GotoDetailAction(DetailNode detailNode) {
            super(UiUtils.getText("LBL_GotoDetailAction"));
            this.detailNode = detailNode;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.detailNode.gotoDetail();
        }
    }

    static final class DetailNode
    extends AbstractNode
    implements OutputListener,
    Removable {
        private static final String ICON = "org/netbeans/modules/search/res/textDetail.png";
        static final int DETAIL_DISPLAY_LENGTH = 240;
        private static final String ELLIPSIS = "...";
        private TextDetail txtDetail;
        private String name;
        private String htmlDisplayName;
        private final MatchingObject mo;

        public DetailNode(TextDetail txtDetail, boolean replacing, MatchingObject mo) {
            super(Children.LEAF, Lookups.fixed((Object[])new Object[]{txtDetail, new ReplaceCheckableNode(txtDetail, replacing)}));
            this.txtDetail = txtDetail;
            this.mo = mo;
            RP.post(new Runnable(){

                @Override
                public void run() {
                    DetailNode.this.txtDetail.prepareLine();
                }
            });
            txtDetail.addChangeListener(new ChangeListener(){

                @Override
                public void stateChanged(ChangeEvent e) {
                    DetailNode.this.fireIconChange();
                    ResultsOutlineSupport.toggleParentSelected(DetailNode.this.getParentNode());
                }
            });
            this.setIconBaseWithExtension("org/netbeans/modules/search/res/textDetail.png");
        }

        public Action[] getActions(boolean context) {
            if (!context) {
                return new Action[]{this.getPreferredAction(), SystemAction.get(HideResultAction.class)};
            }
            return new Action[0];
        }

        public String getShortDescription() {
            if (this.txtDetail.surroundingLines == null || this.txtDetail.surroundingLines.isEmpty()) {
                return super.getShortDescription();
            }
            StringBuilder sb = new StringBuilder("<html>");
            try {
                boolean used = false;
                for (SurroundingLine l : this.txtDetail.surroundingLines) {
                    if (this.txtDetail.getLine() == l.getNumber() - 1) {
                        this.appendMarkedText(sb, false);
                        sb.append("<br/>");
                        used = true;
                    }
                    sb.append(DetailNode.escape(l.getText()));
                    sb.append("<br/>");
                }
                if (!used) {
                    this.appendMarkedText(sb, false);
                }
            }
            catch (CharConversionException e) {
                return null;
            }
            sb.append("</html>");
            return sb.toString();
        }

        public Action getPreferredAction() {
            return new GotoDetailAction(this);
        }

        public boolean equals(Object anotherObj) {
            return anotherObj != null && anotherObj.getClass() == DetailNode.class && ((DetailNode)anotherObj).txtDetail.equals(this.txtDetail);
        }

        public int hashCode() {
            return this.txtDetail.hashCode() + 1;
        }

        public String getName() {
            if (this.name == null) {
                this.name = DetailNode.cutLongLine(this.txtDetail.getLineText()) + "      [" + DetailNode.getName(this.txtDetail) + "]";
            }
            return this.name;
        }

        public String getHtmlDisplayName() {
            if (this.htmlDisplayName != null) {
                return this.htmlDisplayName;
            }
            try {
                StringBuilder text = new StringBuilder();
                text.append("<html><font color='!controlShadow'>");
                text.append(this.txtDetail.lineNumberIndent);
                text.append(this.txtDetail.getLine());
                text.append(": ");
                text.append("</font>");
                if (this.canBeMarked()) {
                    this.appendMarkedText(text, true);
                } else {
                    text.append(DetailNode.escape(DetailNode.cutLongLine(this.txtDetail.getLineText())));
                }
                text.append("      ");
                text.append("<font color='!controlShadow'>[");
                text.append(DetailNode.escape(DetailNode.getLinePos(this.txtDetail)));
                text.append("]</font></html>");
                this.htmlDisplayName = text.toString();
                return this.htmlDisplayName;
            }
            catch (CharConversionException e) {
                return null;
            }
        }

        private static String cutLongLine(String s) {
            if (s == null) {
                return "";
            }
            if (s.length() < 240) {
                return s;
            }
            return s.substring(0, 240 - "...".length()) + "...";
        }

        private boolean canBeMarked() {
            int col0 = this.txtDetail.getColumn0();
            return this.txtDetail.getMarkLength() > 0 && col0 > -1 && col0 < this.txtDetail.getLineTextLength();
        }

        private void appendMarkedText(StringBuilder sb, boolean trim) throws CharConversionException {
            int suffixEnd;
            int prefixStart;
            int lineLen = this.txtDetail.getLineTextLength();
            int matchStart = this.txtDetail.getColumn0();
            int matchEnd = matchStart + Math.min(this.txtDetail.getMarkLength(), lineLen - matchStart);
            int detailLen = matchEnd - matchStart;
            if (detailLen > 240) {
                prefixStart = matchStart;
                suffixEnd = matchEnd;
            } else if (lineLen > 240) {
                int remaining = 240 - detailLen;
                int quarter = remaining / 4;
                int shownPrefix = Math.min(quarter, matchStart);
                int shownSuffix = Math.min(3 * quarter, lineLen - matchEnd);
                int extraForSuffix = quarter - shownPrefix;
                int extraForPrefix = 3 * quarter - shownSuffix;
                prefixStart = Math.max(0, matchStart - shownPrefix - extraForPrefix);
                suffixEnd = Math.min(lineLen, matchEnd + shownSuffix + extraForSuffix);
            } else {
                prefixStart = 0;
                suffixEnd = lineLen;
            }
            this.appendMarkedTextPrefix(sb, prefixStart, matchStart, trim);
            this.appendMarkedTextMatch(sb, matchStart, matchEnd, lineLen, detailLen);
            this.appendMarkedTextSuffix(sb, matchEnd, suffixEnd, lineLen);
        }

        private void appendMarkedTextPrefix(StringBuilder text, int prefixStart, int matchStart, boolean trim) throws CharConversionException {
            int first;
            if (trim) {
                String lineText = this.txtDetail.getLineText();
                for (first = 0; first < matchStart && lineText.charAt(first) <= ' '; ++first) {
                }
            }
            if (prefixStart > 0 && first < prefixStart) {
                text.append("...");
            }
            text.append(DetailNode.escape(this.txtDetail.getLineTextPart(Math.max(prefixStart, first), matchStart)));
        }

        private void appendMarkedTextMatch(StringBuilder text, int matchStart, int matchEnd, int lineLength, int matchedLength) throws CharConversionException {
            text.append("<b>");
            if (matchedLength > 240) {
                int off = (240 - "...".length()) / 2;
                text.append(DetailNode.escape(this.txtDetail.getLineTextPart(matchStart, matchStart + off)));
                text.append("</b>");
                text.append("...");
                text.append("<b>");
                text.append(DetailNode.escape(this.txtDetail.getLineTextPart(matchEnd - off, matchEnd)));
            } else {
                text.append(DetailNode.escape(this.txtDetail.getLineTextPart(matchStart, matchEnd)));
            }
            int markEnd = matchStart + this.txtDetail.getMarkLength();
            text.append("</b>");
            if (markEnd > lineLength) {
                text.append("...");
            }
        }

        private void appendMarkedTextSuffix(StringBuilder text, int matchEnd, int suffixEnd, int lineLength) throws CharConversionException {
            if (lineLength > matchEnd) {
                text.append(DetailNode.escape(this.txtDetail.getLineTextPart(matchEnd, suffixEnd)));
                if (suffixEnd < lineLength) {
                    text.append("...");
                }
            }
        }

        private static String escape(String s) throws CharConversionException {
            return XMLUtil.toElementContent((String)s).replace(" ", "&nbsp;");
        }

        void gotoDetail() {
            this.txtDetail.showDetail(2);
        }

        private void showDetail() {
            this.txtDetail.showDetail(1);
        }

        public void outputLineSelected(OutputEvent evt) {
            this.txtDetail.showDetail(1);
        }

        public void outputLineAction(OutputEvent evt) {
            this.txtDetail.showDetail(2);
        }

        public void outputLineCleared(OutputEvent evt) {
            this.txtDetail.showDetail(3);
        }

        private static String getName(TextDetail det) {
            int line = det.getLine();
            int col = det.getColumn();
            if (col > 0) {
                return NbBundle.getMessage(DetailNode.class, (String)"TEXT_DETAIL_FMT_NAME1", (Object)Integer.toString(line), (Object)Integer.toString(col));
            }
            return NbBundle.getMessage(DetailNode.class, (String)"TEXT_DETAIL_FMT_NAME2", (Object)Integer.toString(line));
        }

        private static String getLinePos(TextDetail det) {
            int col = det.getColumn();
            if (col > 0) {
                return NbBundle.getMessage(DetailNode.class, (String)"TEXT_DETAIL_FMT_NAME3", (Object)col);
            }
            return "";
        }

        private static String getShortDesc(TextDetail det) {
            int line = det.getLine();
            int col = det.getColumn();
            if (col > 0) {
                return NbBundle.getMessage(DetailNode.class, (String)"TEXT_DETAIL_FMT_SHORT1", (Object[])new Object[]{Integer.toString(line), Integer.toString(col)});
            }
            return NbBundle.getMessage(DetailNode.class, (String)"TEXT_DETAIL_FMT_SHORT2", (Object)Integer.toString(line));
        }

        private static String getFullDesc(TextDetail det) {
            String filename = det.getDataObject().getPrimaryFile().getNameExt();
            String lineText = det.getLineText();
            if (lineText != null && lineText.length() > 16384) {
                lineText = lineText.substring(0, 16384) + "...";
            }
            int line = det.getLine();
            int col = det.getColumn();
            if (col > 0) {
                return NbBundle.getMessage(DetailNode.class, (String)"TEXT_DETAIL_FMT_FULL1", (Object[])new Object[]{lineText, filename, Integer.toString(line), Integer.toString(col)});
            }
            return NbBundle.getMessage(DetailNode.class, (String)"TEXT_DETAIL_FMT_FULL2", (Object[])new Object[]{lineText, filename, Integer.toString(line)});
        }

        protected void createPasteTypes(Transferable t, List<PasteType> s) {
        }

        @Override
        public void remove() {
            this.mo.removeDetail(this.txtDetail);
        }

        public boolean canDestroy() {
            return true;
        }

        public void destroy() throws IOException {
            this.remove();
        }

        public Object getValue(String attributeName) {
            if ("output line".equals(attributeName)) {
                return DetailNode.getFullDesc(this.txtDetail);
            }
            return super.getValue(attributeName);
        }

    }

}

