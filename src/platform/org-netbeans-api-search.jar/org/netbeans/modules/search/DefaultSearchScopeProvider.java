/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.search;

import java.util.ArrayList;
import java.util.List;
import org.netbeans.modules.search.SearchScopeBrowse;
import org.netbeans.spi.search.SearchScopeDefinition;
import org.netbeans.spi.search.SearchScopeDefinitionProvider;

public class DefaultSearchScopeProvider
extends SearchScopeDefinitionProvider {
    @Override
    public List<SearchScopeDefinition> createSearchScopeDefinitions() {
        SearchScopeBrowse ssb = new SearchScopeBrowse();
        ArrayList<SearchScopeDefinition> list = new ArrayList<SearchScopeDefinition>(2);
        list.add(ssb.getBrowseScope());
        list.add(ssb.getGetLastScope());
        return list;
    }
}

