/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.search;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.netbeans.api.search.SearchPattern;

public final class TextRegexpUtil {
    private static final String wordCharsExpr = "[\\p{javaLetterOrDigit}_]";
    private static final String checkNotAfterWordChar = "(?<![\\p{javaLetterOrDigit}_])";
    private static final String checkNotBeforeWordChar = "(?![\\p{javaLetterOrDigit}_])";
    private static String MULTILINE_REGEXP_PATTERN = ".*(\\\\n|\\\\r|\\\\f|\\\\u|\\\\0|\\\\x|\\(\\?[idmux]*s).*";
    private static final Logger LOG = Logger.getLogger(TextRegexpUtil.class.getName());

    private TextRegexpUtil() {
    }

    private static String makeLiteralRegexp(String literalPattern, boolean wholeWords) {
        StringBuilder sb = new StringBuilder();
        if (wholeWords) {
            sb.append("(?<![\\p{javaLetterOrDigit}_])");
        }
        sb.append(Pattern.quote(literalPattern));
        if (wholeWords) {
            sb.append("(?![\\p{javaLetterOrDigit}_])");
        }
        return sb.toString();
    }

    private static String makeRegexp(String simplePattern, boolean wholeWords) {
        if (simplePattern.length() == 0) {
            return simplePattern;
        }
        if (!wholeWords && Pattern.matches("[a-zA-Z0-9 ]*", simplePattern)) {
            return simplePattern;
        }
        StringBuilder buf = new StringBuilder(simplePattern.length() + 16);
        boolean quoted = false;
        boolean starPresent = false;
        int minCount = 0;
        boolean bufIsEmpty = true;
        char lastInputChar = '*';
        for (char c : simplePattern.toCharArray()) {
            if (quoted && (c == '?' || c == '*')) {
                assert (!starPresent && minCount == 0);
                if (wholeWords && bufIsEmpty) {
                    buf.append("(?<![\\p{javaLetterOrDigit}_])");
                }
                buf.append('\\');
                buf.append(c);
                lastInputChar = c;
                bufIsEmpty = false;
                quoted = false;
                continue;
            }
            if (c == '?') {
                assert (!quoted);
                ++minCount;
                continue;
            }
            if (c == '*') {
                assert (!quoted);
                starPresent = true;
                continue;
            }
            if (starPresent || minCount != 0) {
                if (wholeWords && bufIsEmpty && !starPresent) {
                    buf.append("(?<![\\p{javaLetterOrDigit}_])");
                }
                bufIsEmpty &= !TextRegexpUtil.addMetachars(buf, starPresent, minCount, wholeWords, !bufIsEmpty);
                starPresent = false;
                minCount = 0;
            }
            if (quoted) {
                buf.append("\\\\");
                quoted = false;
            }
            if (c == '\\') {
                quoted = true;
                continue;
            }
            if (wholeWords && bufIsEmpty && TextRegexpUtil.isWordChar(c)) {
                buf.append("(?<![\\p{javaLetterOrDigit}_])");
            }
            if (TextRegexpUtil.isSpecialCharacter(c)) {
                buf.append('\\');
            }
            buf.append(c);
            lastInputChar = c;
            bufIsEmpty = false;
        }
        if (quoted) {
            assert (!starPresent && minCount == 0);
            buf.append('\\').append('\\');
            lastInputChar = '\\';
        } else if (starPresent || minCount != 0) {
            if (wholeWords && !starPresent && bufIsEmpty) {
                buf.append("(?<![\\p{javaLetterOrDigit}_])");
            }
            bufIsEmpty &= !TextRegexpUtil.addMetachars(buf, starPresent, minCount, wholeWords, false);
            if (wholeWords && !starPresent) {
                buf.append("(?![\\p{javaLetterOrDigit}_])");
            }
            lastInputChar = '*';
        }
        if (wholeWords && TextRegexpUtil.isWordChar(lastInputChar)) {
            buf.append("(?![\\p{javaLetterOrDigit}_])");
        }
        return buf.toString();
    }

    private static Pattern compileSimpleTextPattern(SearchPattern sp) throws PatternSyntaxException {
        String searchRegexp;
        assert (sp != null);
        assert (sp.getSearchExpression() != null);
        assert (!sp.isRegExp());
        int flags = 0;
        if (!sp.isMatchCase()) {
            flags |= 2;
            flags |= 64;
        }
        if (LOG.isLoggable(Level.FINEST)) {
            LOG.log(Level.FINEST, " - textPatternExpr = \"{0}{1}", new Object[]{sp.getSearchExpression(), Character.valueOf('\"')});
        }
        switch (sp.getMatchType()) {
            case BASIC: {
                searchRegexp = TextRegexpUtil.makeRegexp(sp.getSearchExpression(), sp.isWholeWords());
                break;
            }
            case LITERAL: {
                searchRegexp = TextRegexpUtil.makeLiteralRegexp(sp.getSearchExpression(), sp.isWholeWords());
                break;
            }
            default: {
                throw new IllegalStateException();
            }
        }
        if (LOG.isLoggable(Level.FINEST)) {
            LOG.log(Level.FINEST, " - regexp = \"{0}{1}", new Object[]{searchRegexp, Character.valueOf('\"')});
        }
        return Pattern.compile(searchRegexp, flags);
    }

    private static Pattern compileRegexpPattern(SearchPattern sp) throws PatternSyntaxException {
        assert (sp != null);
        assert (sp.getSearchExpression() != null);
        assert (sp.isRegExp());
        boolean multiline = TextRegexpUtil.canBeMultilinePattern(sp.getSearchExpression());
        if (LOG.isLoggable(Level.FINEST)) {
            LOG.log(Level.FINEST, " - textPatternExpr = \"{0}{1}", new Object[]{sp.getSearchExpression(), Character.valueOf('\"')});
        }
        int flags = 0;
        if (!sp.isMatchCase()) {
            flags |= 2;
            flags |= 64;
        }
        return Pattern.compile(sp.getSearchExpression(), flags |= 8);
    }

    public static Pattern makeTextPattern(SearchPattern pattern) throws PatternSyntaxException, NullPointerException {
        if (pattern == null) {
            throw new NullPointerException("search pattern is null");
        }
        if (pattern.getSearchExpression() == null) {
            throw new NullPointerException("expression is null");
        }
        if (pattern.isRegExp()) {
            return TextRegexpUtil.compileRegexpPattern(pattern);
        }
        return TextRegexpUtil.compileSimpleTextPattern(pattern);
    }

    private static boolean isWordChar(char c) {
        if (c == '*' || c == '\\') {
            return false;
        }
        assert ("[\\p{javaLetterOrDigit}_]".equals("[\\p{javaLetterOrDigit}_]"));
        return c == '_' || Character.isLetterOrDigit(c);
    }

    private static boolean addMetachars(StringBuilder buf, boolean starPresent, int minCount, boolean wholeWords, boolean middle) {
        assert (starPresent || minCount != 0);
        if (starPresent && !wholeWords && !middle) {
            starPresent = false;
        }
        if (minCount == 0 && !starPresent) {
            return false;
        }
        if (wholeWords) {
            buf.append("[\\p{javaLetterOrDigit}_]");
        } else {
            buf.append('.');
        }
        switch (minCount) {
            case 0: {
                assert (starPresent);
                buf.append('*');
                break;
            }
            case 1: {
                if (!starPresent) break;
                buf.append('+');
                break;
            }
            default: {
                if (wholeWords) {
                    buf.append('{').append(minCount);
                    if (starPresent) {
                        buf.append(',');
                    }
                    buf.append('}');
                    break;
                }
                for (int i = 1; i < minCount; ++i) {
                    buf.append('.');
                }
                if (!starPresent) break;
                buf.append('+');
            }
        }
        if (starPresent && middle) {
            buf.append('?');
        }
        return true;
    }

    private static boolean isSpecialCharacter(char c) {
        return c > ' ' && c < '' && !TextRegexpUtil.isAlnum(c);
    }

    private static boolean isAlnum(char c) {
        return TextRegexpUtil.isAlpha(c) || TextRegexpUtil.isDigit(c);
    }

    private static boolean isAlpha(char c) {
        return (c = (char)(c | 32)) >= 'a' && c <= 'z';
    }

    private static boolean isDigit(char c) {
        return c >= '0' && c <= '9';
    }

    public static boolean canBeMultilinePattern(String expr) {
        if (expr == null) {
            return false;
        }
        return expr.matches(MULTILINE_REGEXP_PATTERN);
    }

}

