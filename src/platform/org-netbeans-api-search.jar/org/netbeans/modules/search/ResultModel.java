/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 *  org.openide.ErrorManager$Annotation
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.Node
 */
package org.netbeans.modules.search;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.nio.charset.Charset;
import java.util.List;
import org.netbeans.modules.search.ArraySet;
import org.netbeans.modules.search.BasicComposition;
import org.netbeans.modules.search.BasicSearchCriteria;
import org.netbeans.modules.search.Constants;
import org.netbeans.modules.search.MatchingObject;
import org.netbeans.modules.search.TextDetail;
import org.openide.ErrorManager;
import org.openide.filesystems.FileObject;
import org.openide.nodes.Node;

public final class ResultModel {
    public static final String PROP_SELECTION = "selection";
    public static final String PROP_VALID = "valid";
    public static final String PROP_MATCHING_OBJECTS = "matchingObjects";
    public static final String PROP_RESULTS_EDIT = "resultsEdit";
    private long startTime;
    private int totalDetailsCount = 0;
    private Constants.Limit limitReached = null;
    final BasicSearchCriteria basicCriteria;
    final BasicComposition basicComposition;
    private final boolean isFullText;
    final String replaceString;
    final boolean searchAndReplace;
    private final List<MatchingObject> matchingObjects = new ArraySet(500).ordering(false).nullIsAllowed(false);
    private String finishMessage;
    private volatile boolean valid = true;
    private final PropertyChangeSupport propertyChangeSupport;
    private int selectedMatches;
    private final MatchingObjectListener matchingObjectListener;
    private MatchingObject infoCacheMatchingObject;
    private Boolean infoCacheHasDetails;
    private int infoCacheDetailsCount;
    private Node[] infoCacheDetailNodes;
    private final Node[] EMPTY_NODES_ARRAY;

    ResultModel(BasicSearchCriteria basicSearchCriteria, String replaceString, BasicComposition basicComposition) {
        this.propertyChangeSupport = new PropertyChangeSupport(this);
        this.selectedMatches = 0;
        this.matchingObjectListener = new MatchingObjectListener();
        this.EMPTY_NODES_ARRAY = new Node[0];
        this.replaceString = replaceString;
        this.searchAndReplace = replaceString != null;
        this.basicComposition = basicComposition;
        this.basicCriteria = basicSearchCriteria;
        this.isFullText = this.basicCriteria != null && this.basicCriteria.isFullText();
        this.startTime = -1;
    }

    public synchronized boolean remove(MatchingObject mo) {
        if (this.matchingObjects.remove(mo)) {
            this.totalDetailsCount -= mo.getMatchesCount();
            int deselected = 0;
            if (mo.getTextDetails() != null) {
                for (TextDetail td : mo.getTextDetails()) {
                    deselected += td.isSelected() ? -1 : 0;
                }
            }
            mo.cleanup();
            this.propertyChangeSupport.firePropertyChange("resultsEdit", null, null);
            if (deselected < 0) {
                this.updateSelected(deselected);
            }
            if (mo.isSelected() && !mo.isValid()) {
                this.checkValid();
            }
            return true;
        }
        return false;
    }

    public synchronized void removeDetailMatch(MatchingObject mo, TextDetail txtDetail) {
        if (txtDetail.isSelected()) {
            this.updateSelected(-1);
        }
        --this.totalDetailsCount;
        this.propertyChangeSupport.firePropertyChange("resultsEdit", null, null);
        if (mo.textDetails.isEmpty()) {
            this.remove(mo);
        }
    }

    synchronized long getStartTime() {
        if (this.startTime == -1) {
            throw new IllegalStateException("Search has not started yet");
        }
        return this.startTime;
    }

    public synchronized void close() {
        if (this.matchingObjects != null && !this.matchingObjects.isEmpty()) {
            for (MatchingObject matchingObj : this.matchingObjects) {
                matchingObj.cleanup();
            }
        }
    }

    public synchronized boolean objectFound(FileObject object, Charset charset, List<TextDetail> textDetails) {
        assert (this.limitReached == null);
        MatchingObject mo = new MatchingObject(this, object, charset, textDetails);
        boolean added = this.add(mo);
        if (added) {
            this.totalDetailsCount += this.getDetailsCount(mo);
            int newSelectedMatches = 0;
            if (mo.getTextDetails() != null) {
                mo.addPropertyChangeListener(this.matchingObjectListener);
                for (TextDetail td : mo.getTextDetails()) {
                    if (!td.isSelected()) continue;
                    ++newSelectedMatches;
                }
                this.updateSelected(newSelectedMatches);
            }
            this.propertyChangeSupport.firePropertyChange("matchingObjects", null, null);
        } else {
            mo.cleanup();
        }
        this.checkLimits();
        return added;
    }

    private boolean add(MatchingObject matchingObject) {
        try {
            return this.matchingObjects.add(matchingObject);
        }
        catch (IllegalStateException ise) {
            this.limitReached = Constants.Limit.FILES_COUNT_LIMIT;
            return false;
        }
        catch (IllegalArgumentException iae) {
            return false;
        }
    }

    private boolean checkLimits() {
        if (this.totalDetailsCount >= 5000) {
            this.limitReached = Constants.Limit.MATCHES_COUNT_LIMIT;
            return true;
        }
        return false;
    }

    private void setInvalid() {
        if (this.valid) {
            this.valid = false;
            this.propertyChangeSupport.firePropertyChange("valid", true, false);
        }
    }

    private void checkValid() {
        boolean allValid = true;
        for (MatchingObject mo : this.getMatchingObjects()) {
            if (!mo.isSelected() || mo.isValid()) continue;
            allValid = false;
        }
        if (this.valid != allValid) {
            this.valid = allValid;
            this.propertyChangeSupport.firePropertyChange("valid", !allValid, allValid);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void objectValidityChanged(MatchingObject mo) {
        if (mo.isSelected()) {
            if (mo.isValid()) {
                ResultModel resultModel = this;
                synchronized (resultModel) {
                    this.totalDetailsCount = 0;
                    for (MatchingObject item : this.matchingObjects) {
                        this.totalDetailsCount += item.getDetailsCount();
                    }
                }
                this.checkValid();
            } else {
                this.setInvalid();
            }
        }
    }

    private void objectSelectionChanged(MatchingObject mo) {
        if (!mo.isValid()) {
            if (mo.isSelected()) {
                this.setInvalid();
            } else {
                this.checkValid();
            }
        }
    }

    public synchronized int getTotalDetailsCount() {
        return this.totalDetailsCount;
    }

    public synchronized List<MatchingObject> getMatchingObjects() {
        return this.matchingObjects;
    }

    public boolean hasDetails() {
        return this.totalDetailsCount != 0;
    }

    public boolean canHaveDetails() {
        return this.isFullText;
    }

    private void prepareCacheFor(MatchingObject matchingObject) {
        if (matchingObject != this.infoCacheMatchingObject) {
            this.infoCacheHasDetails = null;
            this.infoCacheDetailsCount = -1;
            this.infoCacheDetailNodes = null;
            this.infoCacheMatchingObject = matchingObject;
        }
    }

    synchronized int getDetailsCount(MatchingObject matchingObject) {
        this.prepareCacheFor(matchingObject);
        if (this.infoCacheDetailsCount == -1) {
            this.infoCacheDetailsCount = this.getDetailsCountReal(matchingObject);
            if (this.infoCacheDetailsCount == 0) {
                this.infoCacheDetailNodes = this.EMPTY_NODES_ARRAY;
            }
        }
        assert (this.infoCacheDetailsCount >= 0);
        return this.infoCacheDetailsCount;
    }

    private int getDetailsCountReal(MatchingObject matchingObject) {
        int count = this.isFullText ? matchingObject.getDetailsCount() : 0;
        return count;
    }

    synchronized Node[] getDetails(MatchingObject matchingObject) {
        Node[] detailNodes;
        if (matchingObject == null) {
            return null;
        }
        this.prepareCacheFor(matchingObject);
        if (this.infoCacheDetailNodes == null) {
            detailNodes = this.getDetailsReal(matchingObject);
            this.infoCacheDetailNodes = detailNodes != null ? detailNodes : this.EMPTY_NODES_ARRAY;
            this.infoCacheDetailsCount = this.infoCacheDetailNodes.length;
        } else {
            Node[] arrnode = detailNodes = this.infoCacheDetailNodes != this.EMPTY_NODES_ARRAY ? this.infoCacheDetailNodes : null;
        }
        assert (this.infoCacheDetailNodes != null && (this.infoCacheDetailNodes == this.EMPTY_NODES_ARRAY || this.infoCacheDetailNodes.length > 0));
        assert (detailNodes == null || detailNodes.length > 0);
        return detailNodes;
    }

    private Node[] getDetailsReal(MatchingObject matchingObject) {
        Node[] nodesTotal = null;
        if (this.basicCriteria != null) {
            nodesTotal = this.basicCriteria.isFullText() ? matchingObject.getDetails() : null;
        }
        return nodesTotal;
    }

    public synchronized int size() {
        return this.matchingObjects.size();
    }

    public synchronized boolean wasLimitReached() {
        return this.limitReached != null;
    }

    public String getLimitDisplayName() {
        return this.limitReached != null ? this.limitReached.getDisplayName() : null;
    }

    synchronized void searchException(RuntimeException ex) {
        ErrorManager.Annotation[] annotations;
        for (ErrorManager.Annotation annotation : annotations = ErrorManager.getDefault().findAnnotations((Throwable)ex)) {
            if (annotation.getSeverity() != 256) continue;
            this.finishMessage = annotation.getLocalizedMessage();
            if (this.finishMessage == null) continue;
            return;
        }
        this.finishMessage = ex.getLocalizedMessage();
    }

    public synchronized String getExceptionMsg() {
        return this.finishMessage;
    }

    public boolean isSearchAndReplace() {
        return this.searchAndReplace;
    }

    public synchronized int getSelectedMatchesCount() {
        return this.selectedMatches;
    }

    public boolean isValid() {
        return this.valid;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void updateSelected(int inc) {
        int newSelected;
        int origSelected;
        ResultModel resultModel = this;
        synchronized (resultModel) {
            origSelected = this.selectedMatches;
            this.selectedMatches = newSelected = origSelected + inc;
        }
        this.propertyChangeSupport.firePropertyChange("selection", origSelected, newSelected);
    }

    public synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
        this.propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
        this.propertyChangeSupport.removePropertyChangeListener(listener);
    }

    public synchronized void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        this.propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
    }

    public synchronized void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        this.propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
    }

    synchronized void setStartTime() {
        if (this.startTime != -1) {
            throw new IllegalStateException();
        }
        this.startTime = System.currentTimeMillis();
    }

    private class MatchingObjectListener
    implements PropertyChangeListener {
        private MatchingObjectListener() {
        }

        /*
         * Enabled force condition propagation
         * Lifted jumps to return sites
         */
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            Object source = evt.getSource();
            if (!(source instanceof MatchingObject)) {
                throw new IllegalArgumentException();
            }
            MatchingObject mo = (MatchingObject)source;
            String pn = evt.getPropertyName();
            if ("matchesSelected".equals(pn)) {
                Object newVal = evt.getNewValue();
                Object oldVal = evt.getOldValue();
                if (!(newVal instanceof Integer) || !(oldVal instanceof Integer)) throw new IllegalArgumentException();
                ResultModel.this.updateSelected((Integer)newVal - (Integer)oldVal);
                return;
            } else if ("invalidityStatus".equals(pn)) {
                ResultModel.this.objectValidityChanged(mo);
                return;
            } else {
                if (!"selected".equals(pn)) return;
                ResultModel.this.objectSelectionChanged(mo);
            }
        }
    }

}

