/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.search;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import org.netbeans.modules.search.GraphicalSearchListener;
import org.netbeans.modules.search.Manager;
import org.netbeans.modules.search.ResultView;
import org.netbeans.modules.search.SearchTask;
import org.netbeans.modules.search.ui.UiUtils;
import org.netbeans.spi.search.provider.SearchComposition;
import org.netbeans.spi.search.provider.SearchResultsDisplayer;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

class ResultViewPanel
extends JPanel
implements Lookup.Provider {
    private static final String STOP_ICON = "org/netbeans/modules/search/res/stop.png";
    private static final String INFO_ICON = "org/netbeans/modules/search/res/info.png";
    private static final String CARD_NAME_RESULTS = "results";
    private static final String CARD_NAME_INFO = "info";
    private final CardLayout resultViewCards = new CardLayout();
    private JPanel resultsPanel;
    private JPanel infoPanel;
    private JPanel infoPanelContent;
    private JButton btnStop = new JButton();
    private SearchTask searchTask;
    private GraphicalSearchListener searchListener = null;
    private final JComponent visualComponent;
    private final Lookup lookup;
    private volatile boolean searchInProgress = false;
    private SearchComposition<?> searchComposition;

    public ResultViewPanel(SearchTask searchTask) {
        this.setLayout(this.resultViewCards);
        this.searchComposition = searchTask.getComposition();
        this.searchTask = searchTask;
        SearchResultsDisplayer displayer = this.searchComposition.getSearchResultsDisplayer();
        this.setName(displayer.getTitle());
        displayer.setInfoNode(this.createListener().getInfoNode());
        this.resultsPanel = new JPanel();
        this.resultsPanel.setLayout(new BoxLayout(this.resultsPanel, 3));
        SearchResultsDisplayer disp = this.searchComposition.getSearchResultsDisplayer();
        this.visualComponent = disp.getVisualComponent();
        this.lookup = this.visualComponent instanceof Lookup.Provider ? ((Lookup.Provider)this.visualComponent).getLookup() : Lookup.EMPTY;
        this.resultsPanel.add(this.visualComponent);
        this.add((Component)this.resultsPanel, "results");
        this.showInfo(UiUtils.getText("TEXT_WAITING_FOR_PREVIOUS"));
    }

    void componentOpened() {
    }

    final synchronized void showInfo(String title) {
        if (this.infoPanel == null) {
            this.infoPanel = new JPanel();
            this.infoPanel.setLayout(new BorderLayout());
            JScrollPane scrollPane = new JScrollPane();
            this.infoPanelContent = new JPanel(new FlowLayout(3));
            this.infoPanelContent.setBackground(UIManager.getColor("TextField.background"));
            scrollPane.setViewportView(this.infoPanelContent);
            this.infoPanel.add((Component)scrollPane, "Center");
            this.add((Component)this.infoPanel, "info");
            JToolBar toolBar = new JToolBar();
            toolBar.setFloatable(false);
            toolBar.setOrientation(1);
            this.btnStop.setIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/search/res/stop.png", (boolean)false));
            this.btnStop.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    ResultViewPanel.this.searchCancelled();
                }
            });
            this.btnStop.setToolTipText(UiUtils.getText("TEXT_BUTTON_STOP"));
            toolBar.add(this.btnStop);
            this.infoPanel.add((Component)toolBar, "West");
            this.revalidate();
        }
        this.infoPanelContent.removeAll();
        this.infoPanelContent.add(new JLabel(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/search/res/info.png", (boolean)false)));
        this.infoPanelContent.add(new JLabel(title));
        this.infoPanel.validate();
        this.infoPanel.repaint();
        this.resultViewCards.show(this, "info");
    }

    synchronized void showResults() {
        this.resultViewCards.show(this, "results");
    }

    public final synchronized GraphicalSearchListener createListener() {
        if (this.searchListener == null) {
            this.searchListener = new GraphicalSearchListener(this.searchComposition, this);
        }
        return this.searchListener;
    }

    void searchStarted() {
        this.searchInProgress = true;
        this.resultViewCards.show(this, "results");
    }

    void searchFinished() {
        this.searchInProgress = false;
    }

    void searchInterrupted() {
        this.searchFinished();
    }

    void searchCancelled() {
        Manager.getInstance().stopSearching(this.searchTask);
        this.searchTask.cancel();
        this.showInfo(NbBundle.getMessage(ResultView.class, (String)"TEXT_TASK_CANCELLED"));
        this.setBtnStopEnabled(false);
        this.searchInProgress = false;
        this.searchComposition.getSearchResultsDisplayer().searchFinished();
    }

    @Override
    public boolean requestFocusInWindow() {
        JComponent comp;
        if (this.resultsPanel != null && this.resultsPanel.getComponentCount() > 0 && (comp = (JComponent)this.resultsPanel.getComponent(0)) != null) {
            return comp.requestFocusInWindow();
        }
        return super.requestFocusInWindow();
    }

    void setBtnStopEnabled(boolean enabled) {
        this.btnStop.setEnabled(enabled);
    }

    boolean isSearchInProgress() {
        return this.searchInProgress;
    }

    SearchComposition<?> getSearchComposition() {
        return this.searchComposition;
    }

    public Lookup getLookup() {
        return this.lookup;
    }

    @Override
    public String getToolTipText() {
        return this.visualComponent.getToolTipText();
    }

}

