/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.spi.search;

import java.util.List;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.spi.search.SearchScopeDefinition;

public abstract class SearchScopeDefinitionProvider {
    @NonNull
    public abstract List<SearchScopeDefinition> createSearchScopeDefinitions();
}

