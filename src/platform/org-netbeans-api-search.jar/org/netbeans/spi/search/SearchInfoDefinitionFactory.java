/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.Children
 *  org.openide.util.Parameters
 */
package org.netbeans.spi.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.spi.search.SearchFilterDefinition;
import org.netbeans.spi.search.SearchInfoDefinition;
import org.netbeans.spi.search.impl.CompoundSearchInfoDefinition;
import org.netbeans.spi.search.impl.FlatSearchInfoDefinition;
import org.netbeans.spi.search.impl.SharabilityFilter;
import org.netbeans.spi.search.impl.SimpleSearchInfoDefinition;
import org.netbeans.spi.search.impl.SubnodesSearchInfoDefinition;
import org.netbeans.spi.search.impl.VisibilityFilter;
import org.openide.filesystems.FileObject;
import org.openide.nodes.Children;
import org.openide.util.Parameters;

public final class SearchInfoDefinitionFactory {
    public static final SearchFilterDefinition SHARABILITY_FILTER = SharabilityFilter.getInstance();
    public static final SearchFilterDefinition VISIBILITY_FILTER = VisibilityFilter.getInstance();
    private static final SearchFilterDefinition[] DEFAULT_FILTERS = new SearchFilterDefinition[]{SHARABILITY_FILTER, VISIBILITY_FILTER};
    public static final List<SearchFilterDefinition> DEFAULT_FILTER_DEFS = SearchInfoDefinitionFactory.createDefaultFilterDefList();

    private SearchInfoDefinitionFactory() {
    }

    @NonNull
    public static SearchInfoDefinition createSearchInfo(@NonNull FileObject root, @NonNull SearchFilterDefinition[] filters) {
        Parameters.notNull((CharSequence)"root", (Object)root);
        return new SimpleSearchInfoDefinition(root, filters);
    }

    @NonNull
    public static SearchInfoDefinition createFlatSearchInfo(@NonNull FileObject root, @NonNull SearchFilterDefinition[] filters) {
        Parameters.notNull((CharSequence)"root", (Object)root);
        return new FlatSearchInfoDefinition(root, filters);
    }

    @NonNull
    public static SearchInfoDefinition createFlatSearchInfo(@NonNull FileObject root) {
        return SearchInfoDefinitionFactory.createFlatSearchInfo(root, DEFAULT_FILTERS);
    }

    @NonNull
    public static SearchInfoDefinition createSearchInfo(@NonNull FileObject root) {
        return SearchInfoDefinitionFactory.createSearchInfo(root, DEFAULT_FILTERS);
    }

    @NonNull
    public static SearchInfoDefinition createSearchInfo(@NonNull FileObject[] roots, @NonNull SearchFilterDefinition[] filters) {
        Parameters.notNull((CharSequence)"roots", (Object)roots);
        if (roots.length == 0) {
            return SimpleSearchInfoDefinition.EMPTY_SEARCH_INFO;
        }
        if (roots.length == 1) {
            return SearchInfoDefinitionFactory.createSearchInfo(roots[0], filters);
        }
        SearchInfoDefinition[] nested = new SearchInfoDefinition[roots.length];
        for (int i = 0; i < roots.length; ++i) {
            nested[i] = SearchInfoDefinitionFactory.createSearchInfo(roots[i], filters);
        }
        return new CompoundSearchInfoDefinition(nested);
    }

    @NonNull
    public static SearchInfoDefinition createSearchInfo(@NonNull FileObject[] roots) {
        return SearchInfoDefinitionFactory.createSearchInfo(roots, DEFAULT_FILTERS);
    }

    @NonNull
    public static SearchInfoDefinition createSearchInfoBySubnodes(@NonNull Children children) {
        Parameters.notNull((CharSequence)"children", (Object)children);
        return new SubnodesSearchInfoDefinition(children);
    }

    private static List<SearchFilterDefinition> createDefaultFilterDefList() {
        ArrayList<SearchFilterDefinition> list = new ArrayList<SearchFilterDefinition>(2);
        list.add(SHARABILITY_FILTER);
        list.add(VISIBILITY_FILTER);
        return Collections.unmodifiableList(list);
    }
}

