/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.queries.SharabilityQuery
 *  org.netbeans.api.queries.SharabilityQuery$Sharability
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package org.netbeans.spi.search.impl;

import java.io.File;
import java.net.URI;
import org.netbeans.api.queries.SharabilityQuery;
import org.netbeans.spi.search.SearchFilterDefinition;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public final class SharabilityFilter
extends SearchFilterDefinition {
    private static final SharabilityFilter INSTANCE = new SharabilityFilter();

    private SharabilityFilter() {
    }

    @Override
    public boolean searchFile(FileObject file) throws IllegalArgumentException {
        if (file.isFolder()) {
            throw new IllegalArgumentException("file (not folder) expected");
        }
        File f = FileUtil.toFile((FileObject)file);
        if (f == null && !file.canWrite()) {
            return true;
        }
        return SharabilityQuery.getSharability((FileObject)file) != SharabilityQuery.Sharability.NOT_SHARABLE;
    }

    @Override
    public boolean searchFile(URI uri) {
        return SharabilityQuery.getSharability((URI)uri) != SharabilityQuery.Sharability.NOT_SHARABLE;
    }

    @Override
    public SearchFilterDefinition.FolderResult traverseFolder(URI uri) throws IllegalArgumentException {
        switch (SharabilityQuery.getSharability((URI)uri)) {
            case SHARABLE: {
                return SearchFilterDefinition.FolderResult.TRAVERSE_ALL_SUBFOLDERS;
            }
            case MIXED: {
                return SearchFilterDefinition.FolderResult.TRAVERSE;
            }
            case UNKNOWN: {
                return SearchFilterDefinition.FolderResult.TRAVERSE;
            }
            case NOT_SHARABLE: {
                return SearchFilterDefinition.FolderResult.DO_NOT_TRAVERSE;
            }
        }
        return SearchFilterDefinition.FolderResult.TRAVERSE;
    }

    @Override
    public SearchFilterDefinition.FolderResult traverseFolder(FileObject folder) throws IllegalArgumentException {
        if (!folder.isFolder()) {
            throw new IllegalArgumentException("folder expected");
        }
        File f = FileUtil.toFile((FileObject)folder);
        if (f == null && !folder.canWrite()) {
            return SearchFilterDefinition.FolderResult.TRAVERSE;
        }
        SharabilityQuery.Sharability sharability = SharabilityQuery.getSharability((FileObject)folder);
        switch (sharability) {
            case NOT_SHARABLE: {
                return SearchFilterDefinition.FolderResult.DO_NOT_TRAVERSE;
            }
            case SHARABLE: {
                return SearchFilterDefinition.FolderResult.TRAVERSE_ALL_SUBFOLDERS;
            }
        }
        return SearchFilterDefinition.FolderResult.TRAVERSE;
    }

    public static SearchFilterDefinition getInstance() {
        return INSTANCE;
    }

}

