/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.search.impl;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.netbeans.api.search.SearchRoot;
import org.netbeans.api.search.SearchScopeOptions;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.api.search.provider.impl.AbstractCompoundIterator;
import org.netbeans.spi.search.SearchInfoDefinition;
import org.openide.filesystems.FileObject;

public class CompoundSearchInfoDefinition
extends SearchInfoDefinition {
    private final SearchInfoDefinition[] elements;

    public /* varargs */ CompoundSearchInfoDefinition(SearchInfoDefinition ... elements) {
        if (elements == null) {
            throw new IllegalArgumentException();
        }
        this.elements = elements.length != 0 ? elements : null;
    }

    @Override
    public boolean canSearch() {
        if (this.elements != null) {
            for (SearchInfoDefinition element : this.elements) {
                if (!element.canSearch()) continue;
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<FileObject> filesToSearch(SearchScopeOptions options, SearchListener listener, AtomicBoolean terminated) {
        if (this.elements == null) {
            return Collections.emptyList().iterator();
        }
        ArrayList<SearchInfoDefinition> searchableElements = new ArrayList<SearchInfoDefinition>(this.elements.length);
        for (SearchInfoDefinition element : this.elements) {
            if (!element.canSearch()) continue;
            searchableElements.add(element);
        }
        return new AbstractCompoundIterator<SearchInfoDefinition, FileObject>(searchableElements.toArray(new SearchInfoDefinition[searchableElements.size()]), options, listener, terminated){

            @Override
            protected Iterator<FileObject> getIteratorFor(SearchInfoDefinition element, SearchScopeOptions options, SearchListener listener, AtomicBoolean terminated) {
                return element.filesToSearch(options, listener, terminated);
            }
        };
    }

    @Override
    public Iterator<URI> urisToSearch(SearchScopeOptions options, SearchListener listener, AtomicBoolean terminated) {
        if (this.elements == null) {
            return Collections.emptyList().iterator();
        }
        ArrayList<SearchInfoDefinition> searchableElements = new ArrayList<SearchInfoDefinition>(this.elements.length);
        for (SearchInfoDefinition element : this.elements) {
            if (!element.canSearch()) continue;
            searchableElements.add(element);
        }
        return new AbstractCompoundIterator<SearchInfoDefinition, URI>(searchableElements.toArray(new SearchInfoDefinition[searchableElements.size()]), options, listener, terminated){

            @Override
            protected Iterator<URI> getIteratorFor(SearchInfoDefinition element, SearchScopeOptions options, SearchListener listener, AtomicBoolean terminated) {
                return element.urisToSearch(options, listener, terminated);
            }
        };
    }

    @Override
    public List<SearchRoot> getSearchRoots() {
        LinkedList<SearchRoot> allRoots = new LinkedList<SearchRoot>();
        for (SearchInfoDefinition si : this.elements) {
            allRoots.addAll(si.getSearchRoots());
        }
        return allRoots;
    }

}

