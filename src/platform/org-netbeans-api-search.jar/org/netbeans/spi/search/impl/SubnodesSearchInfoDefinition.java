/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 */
package org.netbeans.spi.search.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.netbeans.api.search.SearchRoot;
import org.netbeans.api.search.SearchScopeOptions;
import org.netbeans.api.search.provider.SearchInfo;
import org.netbeans.api.search.provider.SearchInfoUtils;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.api.search.provider.impl.AbstractCompoundIterator;
import org.netbeans.spi.search.SearchInfoDefinition;
import org.netbeans.spi.search.impl.SearchInfoDefinitionUtils;
import org.netbeans.spi.search.impl.SimpleSearchInfoDefinition;
import org.openide.filesystems.FileObject;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

public final class SubnodesSearchInfoDefinition
extends SearchInfoDefinition {
    private final Children children;

    public SubnodesSearchInfoDefinition(Children children) {
        this.children = children;
    }

    @Override
    public boolean canSearch() {
        return true;
    }

    @Override
    public Iterator<FileObject> filesToSearch(SearchScopeOptions options, SearchListener listener, AtomicBoolean terminated) {
        Node[] nodes = this.children.getNodes(true);
        if (nodes.length == 0) {
            return SimpleSearchInfoDefinition.EMPTY_SEARCH_INFO.filesToSearch(options, listener, terminated);
        }
        ArrayList<SearchInfo> searchInfoElements = new ArrayList<SearchInfo>(nodes.length);
        for (int i = 0; i < nodes.length; ++i) {
            Node child = nodes[i];
            SearchInfo subInfo = SearchInfoUtils.getSearchInfoForNode(child);
            if (subInfo == null || !subInfo.canSearch()) continue;
            searchInfoElements.add(subInfo);
        }
        int size = searchInfoElements.size();
        switch (size) {
            case 0: {
                return Collections.emptyList().iterator();
            }
            case 1: {
                return ((SearchInfo)searchInfoElements.get(0)).getFilesToSearch(options, listener, terminated).iterator();
            }
        }
        return new AbstractCompoundIterator<SearchInfo, FileObject>(searchInfoElements.toArray(new SearchInfo[size]), options, listener, terminated){

            @Override
            protected Iterator<FileObject> getIteratorFor(SearchInfo element, SearchScopeOptions options, SearchListener listener, AtomicBoolean terminated) {
                return element.getFilesToSearch(options, listener, terminated).iterator();
            }
        };
    }

    @Override
    public List<SearchRoot> getSearchRoots() {
        Node[] nodes = this.children.getNodes(true);
        if (nodes.length == 0) {
            return Collections.emptyList();
        }
        LinkedList<SearchRoot> allRoots = new LinkedList<SearchRoot>();
        for (Node subNode : nodes) {
            SearchInfoDefinition subInfo = SearchInfoDefinitionUtils.getSearchInfoDefinition(subNode);
            if (subInfo == null || !subInfo.canSearch()) continue;
            allRoots.addAll(subInfo.getSearchRoots());
        }
        return allRoots;
    }

}

