/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 */
package org.netbeans.spi.search.impl;

import java.util.List;
import org.netbeans.spi.search.SearchFilterDefinition;
import org.netbeans.spi.search.SearchInfoDefinition;
import org.netbeans.spi.search.SearchInfoDefinitionFactory;
import org.netbeans.spi.search.SubTreeSearchOptions;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

public final class SearchInfoDefinitionUtils {
    private SearchInfoDefinitionUtils() {
    }

    public static SearchInfoDefinition findSearchInfoDefinition(Node node) {
        return SearchInfoDefinitionUtils.getSearchInfoDefinition(node, false);
    }

    public static SearchInfoDefinition getSearchInfoDefinition(Node node) {
        return SearchInfoDefinitionUtils.getSearchInfoDefinition(node, true);
    }

    private static SearchInfoDefinition getSearchInfoDefinition(Node node, boolean createDefault) {
        DataObject dataObject;
        SearchInfoDefinition info = (SearchInfoDefinition)node.getLookup().lookup(SearchInfoDefinition.class);
        if (info != null) {
            return info;
        }
        FileObject fileObject = (FileObject)node.getLookup().lookup(FileObject.class);
        if (fileObject == null && (dataObject = (DataObject)node.getLookup().lookup(DataObject.class)) != null) {
            fileObject = dataObject.getPrimaryFile();
        }
        if (fileObject == null) {
            return null;
        }
        SubTreeSearchOptions subTreeSearchOptions = SearchInfoDefinitionUtils.findSubTreeSearchOptions(node);
        if (subTreeSearchOptions == null && !createDefault) {
            return null;
        }
        return SearchInfoDefinitionFactory.createSearchInfo(fileObject, SearchInfoDefinitionUtils.getFiltersForNode(subTreeSearchOptions));
    }

    private static SearchFilterDefinition[] getFiltersForNode(SubTreeSearchOptions subTreeSearchOptions) {
        if (subTreeSearchOptions != null) {
            List<SearchFilterDefinition> filterList = subTreeSearchOptions.getFilters();
            SearchFilterDefinition[] filterArray = new SearchFilterDefinition[filterList.size()];
            return filterList.toArray(filterArray);
        }
        List<SearchFilterDefinition> defaults = SearchInfoDefinitionFactory.DEFAULT_FILTER_DEFS;
        return defaults.toArray(new SearchFilterDefinition[defaults.size()]);
    }

    private static SubTreeSearchOptions findSubTreeSearchOptions(Node node) {
        for (Node n = node; n != null; n = n.getParentNode()) {
            SubTreeSearchOptions subTreeSearchOptions = (SubTreeSearchOptions)n.getLookup().lookup(SubTreeSearchOptions.class);
            if (subTreeSearchOptions == null) continue;
            return subTreeSearchOptions;
        }
        return null;
    }
}

