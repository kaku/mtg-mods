/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.queries.VisibilityQuery
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 */
package org.netbeans.spi.search.impl;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.queries.VisibilityQuery;
import org.netbeans.spi.search.SearchFilterDefinition;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;

public final class VisibilityFilter
extends SearchFilterDefinition {
    private static final VisibilityFilter INSTANCE = new VisibilityFilter();

    private VisibilityFilter() {
    }

    @Override
    public boolean searchFile(FileObject file) throws IllegalArgumentException {
        if (file.isFolder()) {
            throw new IllegalArgumentException("file (not folder) expected");
        }
        return this.isPermittedByQuery(file) && this.isPrimaryFile(file);
    }

    private boolean isPermittedByQuery(FileObject file) {
        return VisibilityQuery.getDefault().isVisible(file);
    }

    private boolean isPrimaryFile(FileObject file) {
        try {
            DataObject dob = DataObject.find((FileObject)file);
            if (dob.getPrimaryFile().equals((Object)file)) {
                return true;
            }
            return false;
        }
        catch (DataObjectNotFoundException ex) {
            String msg = "DataObject not found for file:" + (Object)file;
            Logger logger = Logger.getLogger(VisibilityFilter.class.getName());
            logger.log(Level.INFO, msg, (Throwable)ex);
            return true;
        }
    }

    @Override
    public SearchFilterDefinition.FolderResult traverseFolder(FileObject folder) throws IllegalArgumentException {
        if (!folder.isFolder()) {
            throw new IllegalArgumentException("folder expected");
        }
        return VisibilityQuery.getDefault().isVisible(folder) ? SearchFilterDefinition.FolderResult.TRAVERSE : SearchFilterDefinition.FolderResult.DO_NOT_TRAVERSE;
    }

    public static SearchFilterDefinition getInstance() {
        return INSTANCE;
    }
}

