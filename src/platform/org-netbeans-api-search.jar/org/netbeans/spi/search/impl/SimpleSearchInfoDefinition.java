/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.search.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.netbeans.api.search.SearchRoot;
import org.netbeans.api.search.SearchScopeOptions;
import org.netbeans.api.search.provider.SearchFilter;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.api.search.provider.impl.DefinitionUtils;
import org.netbeans.api.search.provider.impl.SimpleSearchIterator;
import org.netbeans.spi.search.SearchFilterDefinition;
import org.netbeans.spi.search.SearchInfoDefinition;
import org.netbeans.spi.search.SearchInfoDefinitionFactory;
import org.openide.filesystems.FileObject;

public final class SimpleSearchInfoDefinition
extends SearchInfoDefinition {
    public static final SearchInfoDefinition EMPTY_SEARCH_INFO = new SearchInfoDefinition(){

        @Override
        public boolean canSearch() {
            return true;
        }

        @Override
        public Iterator<FileObject> filesToSearch(SearchScopeOptions options, SearchListener listener, AtomicBoolean terminated) {
            return Collections.emptyList().iterator();
        }

        @Override
        public List<SearchRoot> getSearchRoots() {
            return Collections.emptyList();
        }
    };
    private final FileObject rootFile;
    private final SearchFilterDefinition[] filters;

    public SimpleSearchInfoDefinition(FileObject rootFile, SearchFilterDefinition[] filters) {
        if (rootFile == null) {
            throw new IllegalArgumentException();
        }
        if (filters != null && filters.length == 0) {
            filters = null;
        }
        this.rootFile = rootFile;
        this.filters = filters != null ? SimpleSearchInfoDefinition.niceFilters(rootFile, filters) : null;
    }

    private static SearchFilterDefinition[] niceFilters(FileObject fo, SearchFilterDefinition[] allFilters) {
        int i;
        boolean[] mask = new boolean[allFilters.length];
        if (fo.isFolder()) {
            for (i = 0; i < allFilters.length; ++i) {
                SearchFilterDefinition.FolderResult result = allFilters[i].traverseFolder(fo);
                mask[i] = result != SearchFilterDefinition.FolderResult.DO_NOT_TRAVERSE;
            }
        } else {
            assert (fo.isData());
            for (i = 0; i < allFilters.length; ++i) {
                mask[i] = allFilters[i].searchFile(fo);
            }
        }
        SearchFilterDefinition[] nice = new SearchFilterDefinition[SimpleSearchInfoDefinition.countTrues(mask)];
        int niceIndex = 0;
        for (int i2 = 0; i2 < allFilters.length; ++i2) {
            if (!mask[i2]) continue;
            nice[niceIndex++] = allFilters[i2];
        }
        return nice;
    }

    private static int countTrues(boolean[] booleans) {
        int trues = 0;
        for (boolean b : booleans) {
            if (!b) continue;
            ++trues;
        }
        return trues;
    }

    @Override
    public boolean canSearch() {
        return this.filters != null ? this.checkFolderAgainstFilters(this.rootFile) : true;
    }

    @Override
    public Iterator<FileObject> filesToSearch(SearchScopeOptions options, SearchListener listener, AtomicBoolean terminated) {
        return new SimpleSearchIterator(this.rootFile, options, this.filters != null ? Arrays.asList(this.filters) : null, listener, terminated);
    }

    private boolean checkFolderAgainstFilters(FileObject folder) {
        if (folder.isFolder()) {
            for (int i = 0; i < this.filters.length; ++i) {
                if (this.isSuppressableFilter(this.filters[i]) || this.filters[i].traverseFolder(folder) != SearchFilterDefinition.FolderResult.DO_NOT_TRAVERSE) continue;
                return false;
            }
        } else {
            for (int i = 0; i < this.filters.length; ++i) {
                if (this.isSuppressableFilter(this.filters[i]) || this.filters[i].searchFile(folder)) continue;
                return false;
            }
        }
        return true;
    }

    private boolean isSuppressableFilter(SearchFilterDefinition filter) {
        return filter == SearchInfoDefinitionFactory.SHARABILITY_FILTER;
    }

    @Override
    public List<SearchRoot> getSearchRoots() {
        SearchRoot searchRoot = new SearchRoot(this.rootFile, DefinitionUtils.createSearchFilterList(this.filters));
        return Collections.singletonList(searchRoot);
    }

}

