/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.search.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.netbeans.api.search.SearchRoot;
import org.netbeans.api.search.SearchScopeOptions;
import org.netbeans.api.search.provider.SearchFilter;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.api.search.provider.impl.DefinitionUtils;
import org.netbeans.api.search.provider.impl.FlatSearchIterator;
import org.netbeans.spi.search.SearchFilterDefinition;
import org.netbeans.spi.search.SearchInfoDefinition;
import org.openide.filesystems.FileObject;

public class FlatSearchInfoDefinition
extends SearchInfoDefinition {
    private FileObject rootFile;
    private SearchFilterDefinition[] filters;

    public FlatSearchInfoDefinition(FileObject rootFile, SearchFilterDefinition[] filters) {
        this.rootFile = rootFile;
        this.filters = filters;
    }

    @Override
    public boolean canSearch() {
        return true;
    }

    @Override
    public Iterator<FileObject> filesToSearch(SearchScopeOptions options, SearchListener listener, AtomicBoolean terminated) {
        return new FlatSearchIterator(this.rootFile, options, this.filters != null ? Arrays.asList(this.filters) : null, listener, terminated);
    }

    @Override
    public List<SearchRoot> getSearchRoots() {
        SearchRoot searchRoot = new SearchRoot(this.rootFile, DefinitionUtils.createSearchFilterList(this.filters));
        return Collections.singletonList(searchRoot);
    }
}

