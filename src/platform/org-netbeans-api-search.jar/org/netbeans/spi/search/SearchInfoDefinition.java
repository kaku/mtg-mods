/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.search;

import java.net.URI;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.search.SearchRoot;
import org.netbeans.api.search.SearchScopeOptions;
import org.netbeans.api.search.provider.SearchListener;
import org.openide.filesystems.FileObject;

public abstract class SearchInfoDefinition {
    public abstract boolean canSearch();

    @NonNull
    public abstract Iterator<FileObject> filesToSearch(@NonNull SearchScopeOptions var1, @NonNull SearchListener var2, @NonNull AtomicBoolean var3);

    @NonNull
    public abstract List<SearchRoot> getSearchRoots();

    @NonNull
    public Iterator<URI> urisToSearch(@NonNull SearchScopeOptions options, @NonNull SearchListener listener, @NonNull AtomicBoolean terminated) {
        final Iterator<FileObject> inner = this.filesToSearch(options, listener, terminated);
        return new Iterator<URI>(){

            @Override
            public boolean hasNext() {
                return inner.hasNext();
            }

            @Override
            public URI next() {
                FileObject next = (FileObject)inner.next();
                return next == null ? null : next.toURI();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

}

