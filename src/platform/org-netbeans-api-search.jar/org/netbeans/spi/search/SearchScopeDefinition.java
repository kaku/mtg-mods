/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.spi.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.Icon;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.search.provider.SearchInfo;

public abstract class SearchScopeDefinition {
    private List<ChangeListener> changeListeners = new ArrayList<ChangeListener>(1);

    @NonNull
    public abstract String getTypeId();

    @NonNull
    public abstract String getDisplayName();

    @CheckForNull
    public String getAdditionalInfo() {
        return null;
    }

    public abstract boolean isApplicable();

    public final synchronized void addChangeListener(@NonNull ChangeListener l) {
        if (!this.changeListeners.contains(l)) {
            this.changeListeners.add(l);
        }
    }

    public final synchronized void removeChangeListener(@NonNull ChangeListener l) {
        this.changeListeners.remove(l);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected final void notifyListeners() {
        ArrayList<ChangeListener> listenersCopy;
        SearchScopeDefinition searchScopeDefinition = this;
        synchronized (searchScopeDefinition) {
            listenersCopy = new ArrayList<ChangeListener>(this.changeListeners);
        }
        ChangeEvent ev = new ChangeEvent(this);
        for (ChangeListener cl : listenersCopy) {
            cl.stateChanged(ev);
        }
    }

    @NonNull
    public abstract SearchInfo getSearchInfo();

    public String toString() {
        return this.getDisplayName();
    }

    public abstract int getPriority();

    public abstract void clean();

    public void selected() {
    }

    @CheckForNull
    public Icon getIcon() {
        return null;
    }
}

