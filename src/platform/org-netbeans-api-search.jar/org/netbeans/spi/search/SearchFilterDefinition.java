/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package org.netbeans.spi.search;

import java.io.File;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public abstract class SearchFilterDefinition {
    private static final Logger LOG = Logger.getLogger(SearchFilterDefinition.class.getName());

    public abstract boolean searchFile(@NonNull FileObject var1) throws IllegalArgumentException;

    @NonNull
    public abstract FolderResult traverseFolder(@NonNull FileObject var1) throws IllegalArgumentException;

    public boolean searchFile(@NonNull URI uri) {
        File f = null;
        try {
            f = new File(uri);
        }
        catch (IllegalArgumentException iae) {
            LOG.log(Level.INFO, null, iae);
            return false;
        }
        FileObject fo = FileUtil.toFileObject((File)f);
        if (fo == null) {
            return false;
        }
        return this.searchFile(fo);
    }

    @NonNull
    public FolderResult traverseFolder(@NonNull URI uri) throws IllegalArgumentException {
        File f = null;
        try {
            f = new File(uri);
        }
        catch (IllegalArgumentException iae) {
            LOG.log(Level.INFO, null, iae);
            return FolderResult.DO_NOT_TRAVERSE;
        }
        FileObject fo = FileUtil.toFileObject((File)f);
        if (fo == null) {
            return FolderResult.DO_NOT_TRAVERSE;
        }
        return this.traverseFolder(fo);
    }

    public static enum FolderResult {
        DO_NOT_TRAVERSE,
        TRAVERSE,
        TRAVERSE_ALL_SUBFOLDERS;
        

        private FolderResult() {
        }
    }

}

