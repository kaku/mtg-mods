/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.NotificationLineSupport
 *  org.openide.util.ChangeSupport
 *  org.openide.util.HelpCtx
 */
package org.netbeans.spi.search.provider;

import javax.swing.JComponent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.spi.search.provider.SearchComposition;
import org.openide.NotificationLineSupport;
import org.openide.util.ChangeSupport;
import org.openide.util.HelpCtx;

public abstract class SearchProvider {
    protected SearchProvider() {
    }

    @NonNull
    public abstract Presenter createPresenter(boolean var1);

    @NonNull
    public abstract String getTitle();

    public abstract boolean isReplaceSupported();

    public abstract boolean isEnabled();

    public static abstract class Presenter {
        private SearchProvider searchProvider;
        private boolean replacing;
        private final ChangeSupport changeSupport;

        protected Presenter(SearchProvider searchProvider, boolean replacing) {
            this.changeSupport = new ChangeSupport((Object)this);
            this.searchProvider = searchProvider;
            this.replacing = replacing;
        }

        @NonNull
        public abstract JComponent getForm();

        @NonNull
        public abstract SearchComposition<?> composeSearch();

        public abstract boolean isUsable(@NonNull NotificationLineSupport var1);

        public final void addChangeListener(@NullAllowed ChangeListener changeListener) {
            this.changeSupport.addChangeListener(changeListener);
        }

        public final void removeChangeListener(@NullAllowed ChangeListener listener) {
            this.changeSupport.removeChangeListener(listener);
        }

        protected final void fireChange() {
            this.changeSupport.fireChange();
        }

        public final boolean hasListeners() {
            return this.changeSupport.hasListeners();
        }

        public void clean() {
        }

        public final SearchProvider getSearchProvider() {
            return this.searchProvider;
        }

        public final boolean isReplacing() {
            return this.replacing;
        }

        @CheckForNull
        public HelpCtx getHelpCtx() {
            return null;
        }
    }

}

