/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.explorer.view.OutlineView
 *  org.openide.nodes.Node
 *  org.openide.util.Parameters
 */
package org.netbeans.spi.search.provider;

import javax.swing.AbstractButton;
import javax.swing.JComponent;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.search.ui.DefaultSearchResultsPanel;
import org.netbeans.spi.search.provider.SearchComposition;
import org.netbeans.spi.search.provider.SearchProvider;
import org.netbeans.spi.search.provider.SearchResultsDisplayer;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;
import org.openide.util.Parameters;

public final class DefaultSearchResultsDisplayer<U>
extends SearchResultsDisplayer<U> {
    private static final ResultNodeShiftSupport DEFAULT_NODE_SHIFT_SUPPORT = new TrivialResultNodeShiftSupport();
    private final SearchResultsDisplayer.NodeDisplayer<U> helper;
    private final SearchComposition<U> searchComposition;
    private final SearchProvider.Presenter presenter;
    private final String title;
    private ResultNodeShiftSupport shiftSupport = DEFAULT_NODE_SHIFT_SUPPORT;
    private DefaultSearchResultsPanel<U> panel = null;

    DefaultSearchResultsDisplayer(SearchResultsDisplayer.NodeDisplayer<U> helper, SearchComposition<U> searchComposition, SearchProvider.Presenter presenter, String title) {
        this.helper = helper;
        this.searchComposition = searchComposition;
        this.presenter = presenter;
        this.title = title;
    }

    @Override
    public synchronized JComponent getVisualComponent() {
        if (this.panel == null) {
            this.panel = new DefaultSearchResultsPanel<U>(this.helper, this.searchComposition, this.presenter){

                @Override
                protected void onDetailShift(Node n) {
                    DefaultSearchResultsDisplayer.this.shiftSupport.relevantNodeSelected(n);
                }

                @Override
                protected boolean isDetailNode(Node n) {
                    return DefaultSearchResultsDisplayer.this.shiftSupport.isRelevantNode(n);
                }
            };
        }
        return this.panel;
    }

    private DefaultSearchResultsPanel<U> getPanel() {
        if (this.panel == null) {
            this.getVisualComponent();
        }
        return this.panel;
    }

    @Override
    public void addMatchingObject(U object) {
        Parameters.notNull((CharSequence)"object", object);
        this.panel.addMatchingObject(object);
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public void searchStarted() {
        SearchResultsDisplayer.super.searchStarted();
        this.panel.searchStarted();
    }

    @Override
    public void searchFinished() {
        SearchResultsDisplayer.super.searchFinished();
        this.panel.searchFinished();
    }

    @NonNull
    public OutlineView getOutlineView() {
        return this.panel.getOutlineView();
    }

    @Override
    public void setInfoNode(Node infoNode) {
        this.getPanel().setInfoNode(infoNode);
    }

    public void setResultNodeShiftSupport(ResultNodeShiftSupport resultNodeShiftSupport) {
        this.shiftSupport = resultNodeShiftSupport;
    }

    public void addButton(@NonNull AbstractButton button) {
        Parameters.notNull((CharSequence)"button", (Object)button);
        this.getPanel().addButton(button);
    }

    private static class TrivialResultNodeShiftSupport
    extends ResultNodeShiftSupport {
        private TrivialResultNodeShiftSupport() {
        }

        @Override
        public boolean isRelevantNode(Node node) {
            if (node == null) {
                return false;
            }
            Node parent = node.getParentNode();
            return node.isLeaf() && parent != null && parent.getParentNode() != null;
        }

        @Override
        public void relevantNodeSelected(Node node) {
        }
    }

    public static abstract class ResultNodeShiftSupport {
        public abstract boolean isRelevantNode(Node var1);

        public abstract void relevantNodeSelected(Node var1);
    }

}

