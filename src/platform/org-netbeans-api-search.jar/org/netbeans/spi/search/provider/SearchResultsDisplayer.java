/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.nodes.Node
 */
package org.netbeans.spi.search.provider;

import javax.swing.JComponent;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.spi.search.provider.DefaultSearchResultsDisplayer;
import org.netbeans.spi.search.provider.SearchComposition;
import org.netbeans.spi.search.provider.SearchProvider;
import org.openide.nodes.Node;

public abstract class SearchResultsDisplayer<T> {
    protected SearchResultsDisplayer() {
    }

    @NonNull
    public abstract JComponent getVisualComponent();

    public abstract void addMatchingObject(@NonNull T var1);

    public void searchStarted() {
    }

    public void searchFinished() {
    }

    public static <U> DefaultSearchResultsDisplayer<U> createDefault(@NonNull NodeDisplayer<U> helper, @NonNull SearchComposition<U> searchComposition, @NullAllowed SearchProvider.Presenter presenter, @NonNull String title) {
        return new DefaultSearchResultsDisplayer<U>(helper, searchComposition, presenter, title);
    }

    @NonNull
    public abstract String getTitle();

    public void setInfoNode(Node infoNode) {
    }

    public void closed() {
    }

    public static abstract class NodeDisplayer<T> {
        protected NodeDisplayer() {
        }

        public abstract Node matchToNode(T var1);
    }

}

