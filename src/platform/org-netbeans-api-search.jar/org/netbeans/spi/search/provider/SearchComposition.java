/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.spi.search.provider;

import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.spi.search.provider.SearchResultsDisplayer;

public abstract class SearchComposition<R> {
    protected SearchComposition() {
    }

    public abstract void start(@NonNull SearchListener var1);

    public abstract void terminate();

    public abstract boolean isTerminated();

    @NonNull
    public abstract SearchResultsDisplayer<R> getSearchResultsDisplayer();
}

