/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package org.netbeans.api.search;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.prefs.Preferences;
import org.netbeans.api.search.ReplacePattern;
import org.netbeans.api.search.SearchPattern;
import org.netbeans.modules.search.FindDialogMemory;
import org.openide.util.NbPreferences;

public final class SearchHistory {
    private PropertyChangeSupport pcs;
    private static final int MAX_SEARCH_PATTERNS_ITEMS = 10;
    private static final int MAX_PATTERN_LENGTH = 16384;
    private List<SearchPattern> searchPatternsList = new ArrayList<SearchPattern>(10);
    private List<ReplacePattern> replacePatternsList = new ArrayList<ReplacePattern>(10);
    private static SearchHistory INSTANCE = null;
    @Deprecated
    public static final String LAST_SELECTED = "last-selected";
    public static final String ADD_TO_HISTORY = "add-to-history";
    public static final String ADD_TO_REPLACE = "add-to-replace";
    private static Preferences prefs;
    private static final String PREFS_NODE = "SearchHistory";
    private static final String PROP_SEARCH_PATTERN_PREFIX = "search_";
    private static final String PROP_REPLACE_PATTERN_PREFIX = "replace_";

    private SearchHistory() {
        prefs = NbPreferences.forModule(SearchHistory.class).node("SearchHistory");
        this.load();
    }

    public static synchronized SearchHistory getDefault() {
        if (INSTANCE == null) {
            INSTANCE = new SearchHistory();
        }
        return INSTANCE;
    }

    private void load() {
        Object pattern;
        int i;
        for (i = 0; i < 10; ++i) {
            pattern = SearchPattern.parsePattern(prefs.get("search_" + i, null));
            if (pattern == null) continue;
            this.searchPatternsList.add((SearchPattern)pattern);
        }
        for (i = 0; i < 10; ++i) {
            pattern = ReplacePattern.parsePattern(prefs.get("replace_" + i, null));
            if (pattern == null) continue;
            this.replacePatternsList.add((ReplacePattern)pattern);
        }
    }

    @Deprecated
    public SearchPattern getLastSelected() {
        return this.searchPatternsList.get(0);
    }

    @Deprecated
    public void setLastSelected(SearchPattern pattern) {
        SearchPattern oldPattern = this.searchPatternsList.get(0);
        this.add(pattern);
        if (this.pcs != null) {
            this.pcs.firePropertyChange("last-selected", oldPattern, pattern);
        }
    }

    private synchronized PropertyChangeSupport getPropertyChangeSupport() {
        if (this.pcs == null) {
            this.pcs = new PropertyChangeSupport(this);
        }
        return this.pcs;
    }

    public void addPropertyChangeListener(PropertyChangeListener pcl) {
        this.getPropertyChangeSupport().addPropertyChangeListener(pcl);
    }

    public void removePropertyChangeListener(PropertyChangeListener pcl) {
        if (this.pcs != null) {
            this.pcs.removePropertyChangeListener(pcl);
        }
    }

    public List<SearchPattern> getSearchPatterns() {
        return Collections.unmodifiableList(this.searchPatternsList);
    }

    public List<ReplacePattern> getReplacePatterns() {
        return Collections.unmodifiableList(this.replacePatternsList);
    }

    public synchronized void add(SearchPattern pattern) {
        int i;
        if (pattern == null || pattern.getSearchExpression() == null || pattern.getSearchExpression().length() == 0 || this.searchPatternsList.size() > 0 && pattern.equals(this.searchPatternsList.get(0)) || pattern.getSearchExpression().length() > 16384) {
            return;
        }
        for (i = 0; i < this.searchPatternsList.size(); ++i) {
            if (!pattern.getSearchExpression().equals(this.searchPatternsList.get(i).getSearchExpression())) continue;
            this.searchPatternsList.remove(i);
            break;
        }
        if (this.searchPatternsList.size() == 10) {
            this.searchPatternsList.remove(9);
        }
        this.searchPatternsList.add(0, pattern);
        for (i = 0; i < this.searchPatternsList.size(); ++i) {
            prefs.put("search_" + i, this.searchPatternsList.get(i).toCanonicalString());
        }
        if (this.pcs != null) {
            this.pcs.firePropertyChange("add-to-history", null, pattern);
        }
    }

    public synchronized void addReplace(ReplacePattern pattern) {
        int i;
        if (pattern == null || pattern.getReplaceExpression() == null || pattern.getReplaceExpression().length() == 0 || this.replacePatternsList.size() > 0 && pattern.equals(this.replacePatternsList.get(0)) || pattern.getReplaceExpression().length() > 16384) {
            return;
        }
        for (i = 0; i < this.replacePatternsList.size(); ++i) {
            if (!pattern.getReplaceExpression().equals(this.replacePatternsList.get(i).getReplaceExpression())) continue;
            this.replacePatternsList.remove(i);
            break;
        }
        if (this.replacePatternsList.size() == 10) {
            this.replacePatternsList.remove(9);
        }
        this.replacePatternsList.add(0, pattern);
        for (i = 0; i < this.replacePatternsList.size(); ++i) {
            prefs.put("replace_" + i, this.replacePatternsList.get(i).toCanonicalString());
        }
        if (this.pcs != null) {
            this.pcs.firePropertyChange("add-to-replace", null, pattern);
        }
    }

    public void storeFileNamePattern(String pattern) {
        FindDialogMemory mem = FindDialogMemory.getDefault();
        if (pattern == null) {
            mem.setFileNamePatternSpecified(false);
        } else {
            mem.setFileNamePatternSpecified(true);
            mem.storeFileNamePattern(pattern);
        }
    }
}

