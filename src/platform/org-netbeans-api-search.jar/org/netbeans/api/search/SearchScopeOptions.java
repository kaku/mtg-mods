/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.util.Parameters
 */
package org.netbeans.api.search;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.spi.search.SearchFilterDefinition;
import org.openide.util.Parameters;

public class SearchScopeOptions {
    static final SearchScopeOptions DEFAULT = new DefaultSearchOptions();
    private String pattern = "";
    private boolean regexp = false;
    private boolean searchInArchives = false;
    private boolean searchInGenerated = false;
    private List<SearchFilterDefinition> filters = new LinkedList<SearchFilterDefinition>();

    private SearchScopeOptions() {
    }

    public void addFilter(@NonNull SearchFilterDefinition filter) {
        Parameters.notNull((CharSequence)"filter", (Object)filter);
        if (!this.filters.contains(filter)) {
            this.filters.add(filter);
        }
    }

    @NonNull
    public List<SearchFilterDefinition> getFilters() {
        return this.filters;
    }

    public boolean isSearchInArchives() {
        return this.searchInArchives;
    }

    public void setSearchInArchives(boolean searchInArchives) {
        this.searchInArchives = searchInArchives;
    }

    public boolean isSearchInGenerated() {
        return this.searchInGenerated;
    }

    public void setSearchInGenerated(boolean searchInGenerated) {
        this.searchInGenerated = searchInGenerated;
    }

    public String getPattern() {
        return this.pattern;
    }

    public void setPattern(@NullAllowed String pattern) {
        this.pattern = pattern == null ? "" : pattern;
    }

    public boolean isRegexp() {
        return this.regexp;
    }

    public void setRegexp(boolean regexp) {
        this.regexp = regexp;
    }

    @NonNull
    public static SearchScopeOptions create() {
        return new SearchScopeOptions();
    }

    @NonNull
    public static SearchScopeOptions create(@NullAllowed String pattern, boolean regexp) {
        SearchScopeOptions so = SearchScopeOptions.create();
        so.setPattern(pattern);
        so.setRegexp(regexp);
        return so;
    }

    @NonNull
    public static SearchScopeOptions create(@NullAllowed String pattern, boolean regexp, boolean searchInArchives, boolean searchInGenerated, @NullAllowed List<SearchFilterDefinition> filters) {
        SearchScopeOptions so = SearchScopeOptions.create(pattern, regexp);
        so.setSearchInArchives(searchInArchives);
        so.setSearchInGenerated(searchInGenerated);
        if (filters != null) {
            for (SearchFilterDefinition fof : filters) {
                so.addFilter(fof);
            }
        }
        return so;
    }

    static class DefaultSearchOptions
    extends SearchScopeOptions {
        private static final List<SearchFilterDefinition> LIST = Collections.emptyList();

        private DefaultSearchOptions() {
            super();
        }

        @Override
        public void addFilter(SearchFilterDefinition filter) {
            throw new UnsupportedOperationException();
        }

        @Override
        public List<SearchFilterDefinition> getFilters() {
            return LIST;
        }
    }

}

