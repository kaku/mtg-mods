/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.api.search.provider.impl;

import java.net.URI;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.netbeans.api.search.SearchRoot;
import org.netbeans.api.search.SearchScopeOptions;
import org.netbeans.api.search.provider.SearchInfo;
import org.netbeans.api.search.provider.SearchListener;
import org.openide.filesystems.FileObject;

public class EmptySearchInfo
extends SearchInfo {
    @Override
    public boolean canSearch() {
        return false;
    }

    @Override
    public List<SearchRoot> getSearchRoots() {
        return Collections.emptyList();
    }

    @Override
    public Iterator<FileObject> createFilesToSearchIterator(SearchScopeOptions options, SearchListener listener, AtomicBoolean terminated) {
        return Collections.emptyList().iterator();
    }

    @Override
    public Iterator<URI> createUrisToSearchIterator(SearchScopeOptions options, SearchListener listener, AtomicBoolean terminated) {
        return Collections.emptyList().iterator();
    }
}

