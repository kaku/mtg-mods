/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.Node
 *  org.openide.util.Parameters
 */
package org.netbeans.api.search.provider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.search.provider.SearchFilter;
import org.netbeans.api.search.provider.SearchInfo;
import org.netbeans.api.search.provider.impl.CompoundSearchInfo;
import org.netbeans.api.search.provider.impl.DelegatingSearchFilter;
import org.netbeans.api.search.provider.impl.DelegatingSearchInfo;
import org.netbeans.api.search.provider.impl.EmptySearchInfo;
import org.netbeans.spi.search.SearchFilterDefinition;
import org.netbeans.spi.search.SearchInfoDefinition;
import org.netbeans.spi.search.SearchInfoDefinitionFactory;
import org.netbeans.spi.search.impl.SearchInfoDefinitionUtils;
import org.openide.filesystems.FileObject;
import org.openide.nodes.Node;
import org.openide.util.Parameters;

public final class SearchInfoUtils {
    public static final SearchFilter VISIBILITY_FILTER = SearchInfoUtils.createVisibilityFilter();
    public static final SearchFilter SHARABILITY_FILTER = SearchInfoUtils.createSharabilityFilter();
    public static final List<SearchFilter> DEFAULT_FILTERS = SearchInfoUtils.createDefaultFilterList();

    @CheckForNull
    public static SearchInfo getSearchInfoForNode(@NonNull Node node) {
        Parameters.notNull((CharSequence)"node", (Object)node);
        SearchInfoDefinition sid = SearchInfoDefinitionUtils.getSearchInfoDefinition(node);
        if (sid == null) {
            return null;
        }
        return new DelegatingSearchInfo(sid);
    }

    @CheckForNull
    public static SearchInfo findDefinedSearchInfo(@NonNull Node node) {
        Parameters.notNull((CharSequence)"node", (Object)node);
        SearchInfoDefinition sid = SearchInfoDefinitionUtils.findSearchInfoDefinition(node);
        if (sid != null) {
            return new DelegatingSearchInfo(sid);
        }
        return null;
    }

    @NonNull
    public static SearchInfo createForDefinition(@NonNull SearchInfoDefinition definition) {
        Parameters.notNull((CharSequence)"definition", (Object)definition);
        return new DelegatingSearchInfo(definition);
    }

    @NonNull
    public static /* varargs */ SearchInfo createCompoundSearchInfo(@NonNull SearchInfo ... delegates) {
        Parameters.notNull((CharSequence)"delegates", (Object)delegates);
        return new CompoundSearchInfo(delegates);
    }

    @NonNull
    public static SearchInfo createEmptySearchInfo() {
        return new EmptySearchInfo();
    }

    @NonNull
    public static SearchInfo createSearchInfoForRoot(@NonNull FileObject root) {
        Parameters.notNull((CharSequence)"root", (Object)root);
        return new DelegatingSearchInfo(SearchInfoDefinitionFactory.createSearchInfo(root));
    }

    @NonNull
    public static SearchInfo createSearchInfoForRoots(@NonNull FileObject[] roots) {
        Parameters.notNull((CharSequence)"roots", (Object)roots);
        return new DelegatingSearchInfo(SearchInfoDefinitionFactory.createSearchInfo(roots));
    }

    @NonNull
    public static /* varargs */ SearchInfo createSearchInfoForRoots(@NonNull FileObject[] roots, boolean useDefaultFilters, @NonNull SearchFilterDefinition ... extraFilters) {
        Parameters.notNull((CharSequence)"roots", (Object)roots);
        int defFiltersCount = useDefaultFilters ? SearchInfoDefinitionFactory.DEFAULT_FILTER_DEFS.size() : 0;
        int extFiltersCount = extraFilters.length;
        SearchFilterDefinition[] filters = new SearchFilterDefinition[defFiltersCount + extFiltersCount];
        for (int i = 0; i < defFiltersCount; ++i) {
            filters[i] = SearchInfoDefinitionFactory.DEFAULT_FILTER_DEFS.get(i);
        }
        System.arraycopy(extraFilters, 0, filters, defFiltersCount, extFiltersCount);
        return new DelegatingSearchInfo(SearchInfoDefinitionFactory.createSearchInfo(roots, filters));
    }

    private static SearchFilter createVisibilityFilter() {
        return new DelegatingSearchFilter(SearchInfoDefinitionFactory.VISIBILITY_FILTER);
    }

    private static SearchFilter createSharabilityFilter() {
        return new DelegatingSearchFilter(SearchInfoDefinitionFactory.SHARABILITY_FILTER);
    }

    private static List<SearchFilter> createDefaultFilterList() {
        ArrayList<SearchFilter> l = new ArrayList<SearchFilter>(2);
        l.add(VISIBILITY_FILTER);
        l.add(SHARABILITY_FILTER);
        return Collections.unmodifiableList(l);
    }
}

