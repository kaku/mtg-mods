/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.api.search.provider.impl;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.netbeans.api.search.SearchRoot;
import org.netbeans.api.search.SearchScopeOptions;
import org.netbeans.api.search.provider.SearchInfo;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.api.search.provider.impl.AbstractCompoundIterator;
import org.openide.filesystems.FileObject;

public class CompoundSearchInfo
extends SearchInfo {
    private final SearchInfo[] elements;

    public /* varargs */ CompoundSearchInfo(SearchInfo ... elements) {
        if (elements == null) {
            throw new IllegalArgumentException();
        }
        this.elements = elements.length != 0 ? elements : null;
    }

    @Override
    public boolean canSearch() {
        if (this.elements != null) {
            for (SearchInfo element : this.elements) {
                if (!element.canSearch()) continue;
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<FileObject> createFilesToSearchIterator(SearchScopeOptions options, SearchListener listener, AtomicBoolean terminated) {
        if (this.elements == null) {
            return Collections.emptyList().iterator();
        }
        ArrayList<SearchInfo> searchableElements = new ArrayList<SearchInfo>(this.elements.length);
        for (SearchInfo element : this.elements) {
            if (!element.canSearch()) continue;
            searchableElements.add(element);
        }
        return new AbstractCompoundIterator<SearchInfo, FileObject>(searchableElements.toArray(new SearchInfo[searchableElements.size()]), options, listener, terminated){

            @Override
            protected Iterator<FileObject> getIteratorFor(SearchInfo element, SearchScopeOptions options, SearchListener listener, AtomicBoolean terminated) {
                return element.getFilesToSearch(options, listener, terminated).iterator();
            }
        };
    }

    @Override
    protected Iterator<URI> createUrisToSearchIterator(SearchScopeOptions options, SearchListener listener, AtomicBoolean terminated) {
        if (this.elements == null) {
            return Collections.emptyList().iterator();
        }
        ArrayList<SearchInfo> searchableElements = new ArrayList<SearchInfo>(this.elements.length);
        for (SearchInfo element : this.elements) {
            if (!element.canSearch()) continue;
            searchableElements.add(element);
        }
        return new AbstractCompoundIterator<SearchInfo, URI>(searchableElements.toArray(new SearchInfo[searchableElements.size()]), options, listener, terminated){

            @Override
            protected Iterator<URI> getIteratorFor(SearchInfo element, SearchScopeOptions options, SearchListener listener, AtomicBoolean terminated) {
                return element.getUrisToSearch(options, listener, terminated).iterator();
            }
        };
    }

    @Override
    public List<SearchRoot> getSearchRoots() {
        LinkedList<SearchRoot> allRoots = new LinkedList<SearchRoot>();
        for (SearchInfo si : this.elements) {
            allRoots.addAll(si.getSearchRoots());
        }
        return allRoots;
    }

}

