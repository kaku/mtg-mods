/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.search.provider.impl;

import java.util.ArrayList;
import java.util.List;
import org.netbeans.api.search.provider.SearchFilter;
import org.netbeans.api.search.provider.impl.DelegatingSearchFilter;
import org.netbeans.spi.search.SearchFilterDefinition;

public final class DefinitionUtils {
    private DefinitionUtils() {
    }

    public static List<SearchFilter> createSearchFilterList(SearchFilterDefinition[] definitions) {
        ArrayList<SearchFilter> list = new ArrayList<SearchFilter>(definitions.length);
        for (SearchFilterDefinition def : definitions) {
            list.add(new DelegatingSearchFilter(def));
        }
        return list;
    }
}

