/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.search.provider.impl;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicBoolean;
import org.netbeans.api.search.SearchScopeOptions;
import org.netbeans.api.search.provider.SearchListener;

public abstract class AbstractCompoundIterator<E, T>
implements Iterator<T> {
    private final E[] elements;
    private int elementIndex;
    private Iterator<T> elementIterator = null;
    private T nextObject;
    private boolean upToDate;
    private SearchScopeOptions options;
    private SearchListener listener;
    private AtomicBoolean terminated;

    public AbstractCompoundIterator(E[] elements, SearchScopeOptions options, SearchListener listener, AtomicBoolean terminated) {
        if (elements == null) {
            throw new IllegalArgumentException("Elements are null");
        }
        if (options == null) {
            throw new IllegalArgumentException("Options are null");
        }
        this.options = options;
        if (elements.length == 0) {
            this.elements = null;
            this.elementIndex = 0;
            this.upToDate = true;
        } else {
            this.elements = elements;
            this.upToDate = false;
        }
        this.listener = listener;
        this.terminated = terminated;
    }

    @Override
    public boolean hasNext() {
        if (!this.upToDate) {
            this.update();
        }
        return this.elements != null && this.elementIndex < this.elements.length;
    }

    @Override
    public T next() {
        if (!this.hasNext()) {
            throw new NoSuchElementException();
        }
        this.upToDate = false;
        return this.nextObject;
    }

    private void update() {
        assert (!this.upToDate);
        if (this.elementIterator == null) {
            this.elementIndex = 0;
            this.elementIterator = this.getIteratorFor(this.elements[0], this.options, this.listener, this.terminated);
        }
        while (!this.elementIterator.hasNext()) {
            this.elements[this.elementIndex] = null;
            if (++this.elementIndex == this.elements.length) break;
            this.elementIterator = this.getIteratorFor(this.elements[this.elementIndex], this.options, this.listener, this.terminated);
        }
        if (this.elementIndex < this.elements.length) {
            this.nextObject = this.elementIterator.next();
        } else {
            this.elementIterator = null;
            this.nextObject = null;
        }
        this.upToDate = true;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    protected abstract Iterator<T> getIteratorFor(E var1, SearchScopeOptions var2, SearchListener var3, AtomicBoolean var4);
}

