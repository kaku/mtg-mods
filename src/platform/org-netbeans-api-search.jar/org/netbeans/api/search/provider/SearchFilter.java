/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.api.search.provider;

import java.net.URI;
import org.netbeans.api.annotations.common.NonNull;
import org.openide.filesystems.FileObject;

public abstract class SearchFilter {
    public abstract boolean searchFile(@NonNull FileObject var1) throws IllegalArgumentException;

    public abstract boolean searchFile(@NonNull URI var1);

    @NonNull
    public abstract FolderResult traverseFolder(@NonNull FileObject var1) throws IllegalArgumentException;

    @NonNull
    public abstract FolderResult traverseFolder(@NonNull URI var1) throws IllegalArgumentException;

    public static enum FolderResult {
        DO_NOT_TRAVERSE,
        TRAVERSE,
        TRAVERSE_ALL_SUBFOLDERS;
        

        private FolderResult() {
        }
    }

}

