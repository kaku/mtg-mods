/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package org.netbeans.api.search.provider.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import org.netbeans.api.search.SearchScopeOptions;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.spi.search.SearchFilterDefinition;
import org.netbeans.spi.search.SearchInfoDefinitionFactory;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

class FilterHelper {
    private List<SearchFilterDefinition> filters = new ArrayList<SearchFilterDefinition>();
    private boolean active;
    private Stack<List<SearchFilterDefinition>> stack;

    public FilterHelper(List<SearchFilterDefinition> defaultFilters, SearchScopeOptions options) {
        if (options == null) {
            throw new NullPointerException("Options cannot be null.");
        }
        if (defaultFilters != null) {
            for (SearchFilterDefinition fof : defaultFilters) {
                if (options.isSearchInGenerated()) {
                    if (fof == SearchInfoDefinitionFactory.SHARABILITY_FILTER) continue;
                    this.filters.add(fof);
                    continue;
                }
                this.filters.add(fof);
            }
        }
        for (SearchFilterDefinition fof : options.getFilters()) {
            this.filters.add(fof);
        }
        this.stack = new Stack();
        this.stack.push(this.filters);
        this.active = !this.filters.isEmpty();
    }

    public boolean fileAllowed(File f, SearchListener listener) {
        if (this.active) {
            FileObject fo = FileUtil.toFileObject((File)f);
            if (fo != null) {
                return this.fileAllowed(fo, listener);
            }
            listener.generalError(new RuntimeException("No FileObject for file " + f.getPath()));
            return false;
        }
        return true;
    }

    public boolean fileAllowed(FileObject fo, SearchListener listener) {
        if (this.active) {
            for (SearchFilterDefinition fl : this.stack.peek()) {
                if (fl.searchFile(fo)) continue;
                listener.fileSkipped(fo, fl, null);
                return false;
            }
            return true;
        }
        return true;
    }

    public boolean directoryAllowed(File d, SearchListener listener) {
        if (this.active) {
            FileObject fo = FileUtil.toFileObject((File)d);
            if (fo != null) {
                return this.directoryAllowed(fo, listener);
            }
            listener.generalError(new RuntimeException("No FileObject for directory " + d.getPath()));
            return false;
        }
        return true;
    }

    public boolean directoryAllowed(FileObject d, SearchListener listener) {
        if (this.active) {
            LinkedList remainingFilters = null;
            boolean result = true;
            block5 : for (SearchFilterDefinition filter : this.stack.peek()) {
                SearchFilterDefinition.FolderResult traverseCommand = filter.traverseFolder(d);
                switch (traverseCommand) {
                    case TRAVERSE: {
                        continue block5;
                    }
                    case DO_NOT_TRAVERSE: {
                        result = false;
                        listener.fileSkipped(d, filter, null);
                        break block5;
                    }
                    case TRAVERSE_ALL_SUBFOLDERS: {
                        if (remainingFilters == null) {
                            remainingFilters = new LinkedList(this.stack.peek());
                        }
                        remainingFilters.remove(filter);
                        continue block5;
                    }
                    default: {
                        assert (false);
                        continue block5;
                    }
                }
            }
            if (result) {
                if (remainingFilters != null) {
                    this.stack.push(remainingFilters);
                } else {
                    this.stack.push(this.stack.peek());
                }
            }
            return result;
        }
        return true;
    }

    public void popStack() {
        if (this.active) {
            this.stack.pop();
        }
    }

}

