/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.api.search.provider;

import java.io.File;
import java.net.URI;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.api.search.RegexpUtil;
import org.netbeans.api.search.SearchScopeOptions;
import org.openide.filesystems.FileObject;

public abstract class FileNameMatcher {
    private static final FileNameMatcher TAKE_ALL_INSTANCE = new TakeAllMatcher();

    private FileNameMatcher() {
    }

    public abstract boolean pathMatches(File var1);

    public abstract boolean pathMatches(FileObject var1);

    public abstract boolean pathMatches(URI var1);

    public static FileNameMatcher create(SearchScopeOptions options) {
        if (options.getPattern().isEmpty()) {
            return TAKE_ALL_INSTANCE;
        }
        if (!options.isRegexp() && options.getPattern().matches("\\*\\.\\w+")) {
            return new ExtensionMatcher(options.getPattern().substring(2));
        }
        if (!options.isRegexp()) {
            return new SimplePatternMatcher(options);
        }
        return new RegexpPatternMatcher(options);
    }

    private static class SimplePatternMatcher
    extends FileNameMatcher {
        private Pattern pattern = null;

        protected SimplePatternMatcher(SearchScopeOptions options) {
            super();
            if (options != null) {
                this.pattern = RegexpUtil.makeFileNamePattern(options);
            }
        }

        @Override
        public boolean pathMatches(File file) {
            return this.pattern.matcher(file.getName()).matches();
        }

        @Override
        public boolean pathMatches(FileObject fileObject) {
            return this.pattern.matcher(fileObject.getNameExt()).matches();
        }

        @Override
        public boolean pathMatches(URI uri) {
            String path = uri.getPath();
            int lastSeparator = path.lastIndexOf("\\");
            if (lastSeparator == -1) {
                lastSeparator = path.lastIndexOf("/");
            }
            if (lastSeparator == -1) {
                return false;
            }
            String name = path.substring(lastSeparator + 1, path.length());
            return this.pattern.matcher(name).matches();
        }
    }

    private static class RegexpPatternMatcher
    extends FileNameMatcher {
        private Pattern pattern = null;

        protected RegexpPatternMatcher(SearchScopeOptions options) {
            super();
            if (options != null) {
                this.pattern = RegexpUtil.makeFileNamePattern(options);
            }
        }

        @Override
        public boolean pathMatches(File file) {
            return this.pattern.matcher(file.getPath()).find();
        }

        @Override
        public boolean pathMatches(FileObject fileObject) {
            return this.pattern.matcher(fileObject.getPath()).find();
        }

        @Override
        public boolean pathMatches(URI uri) {
            return this.pattern.matcher(uri.getPath()).find();
        }
    }

    private static class ExtensionMatcher
    extends FileNameMatcher {
        private String ext;
        private String extWithDot;
        private int extWithDotLen;

        public ExtensionMatcher(String ext) {
            super();
            this.ext = ext;
            this.extWithDot = "." + ext;
            this.extWithDotLen = ext.length() + 1;
        }

        private boolean pathMatches(String fileName) {
            if (fileName == null || fileName.length() <= this.extWithDotLen) {
                return false;
            }
            return fileName.substring(fileName.length() - this.extWithDotLen, fileName.length()).equalsIgnoreCase(this.extWithDot);
        }

        @Override
        public boolean pathMatches(File file) {
            String fileName = file.getName();
            return this.pathMatches(fileName);
        }

        @Override
        public boolean pathMatches(FileObject fileObject) {
            String fileExt = fileObject.getExt();
            return fileExt != null && fileExt.equalsIgnoreCase(this.ext);
        }

        @Override
        public boolean pathMatches(URI uri) {
            String fileName = uri.getPath();
            return this.pathMatches(fileName);
        }
    }

    private static class TakeAllMatcher
    extends FileNameMatcher {
        private TakeAllMatcher() {
            super();
        }

        @Override
        public boolean pathMatches(File file) {
            return true;
        }

        @Override
        public boolean pathMatches(FileObject fileObject) {
            return true;
        }

        @Override
        public boolean pathMatches(URI uri) {
            return true;
        }
    }

}

