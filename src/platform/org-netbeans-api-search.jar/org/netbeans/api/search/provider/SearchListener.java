/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.api.search.provider;

import java.net.URI;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.spi.search.SearchFilterDefinition;
import org.openide.filesystems.FileObject;

public abstract class SearchListener {
    protected SearchListener() {
    }

    public void fileSkipped(@NonNull FileObject fileObject, @NullAllowed SearchFilterDefinition filter, @NullAllowed String message) {
    }

    public void fileSkipped(@NonNull URI uri, @NullAllowed SearchFilterDefinition filter, @NullAllowed String message) {
    }

    public void directoryEntered(@NonNull String path) {
    }

    public void fileContentMatchingStarted(@NonNull String fileName) {
    }

    public void fileContentMatchingProgress(@NonNull String fileName, long fileOffset) {
    }

    public void fileContentMatchingError(@NonNull String fileName, @NonNull Throwable throwable) {
    }

    public void generalError(@NonNull Throwable t) {
    }
}

