/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.api.search.provider.impl;

import java.net.URI;
import org.netbeans.api.search.provider.SearchFilter;
import org.netbeans.spi.search.SearchFilterDefinition;
import org.openide.filesystems.FileObject;

public class DelegatingSearchFilter
extends SearchFilter {
    private SearchFilterDefinition definition;

    public DelegatingSearchFilter(SearchFilterDefinition definition) {
        this.definition = definition;
    }

    @Override
    public boolean searchFile(FileObject file) throws IllegalArgumentException {
        return this.definition.searchFile(file);
    }

    @Override
    public boolean searchFile(URI fileUri) {
        return this.definition.searchFile(fileUri);
    }

    @Override
    public SearchFilter.FolderResult traverseFolder(URI folderUri) throws IllegalArgumentException {
        return this.definitionToClientFolderResult(this.definition.traverseFolder(folderUri));
    }

    @Override
    public SearchFilter.FolderResult traverseFolder(FileObject folder) throws IllegalArgumentException {
        return this.definitionToClientFolderResult(this.definition.traverseFolder(folder));
    }

    private SearchFilter.FolderResult definitionToClientFolderResult(SearchFilterDefinition.FolderResult result) {
        switch (result) {
            case DO_NOT_TRAVERSE: {
                return SearchFilter.FolderResult.DO_NOT_TRAVERSE;
            }
            case TRAVERSE: {
                return SearchFilter.FolderResult.TRAVERSE;
            }
            case TRAVERSE_ALL_SUBFOLDERS: {
                return SearchFilter.FolderResult.TRAVERSE_ALL_SUBFOLDERS;
            }
        }
        assert (false);
        return SearchFilter.FolderResult.TRAVERSE;
    }

}

