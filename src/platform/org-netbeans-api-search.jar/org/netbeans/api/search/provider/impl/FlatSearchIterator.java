/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Enumerations
 */
package org.netbeans.api.search.provider.impl;

import java.awt.EventQueue;
import java.util.Enumeration;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicBoolean;
import org.netbeans.api.search.SearchScopeOptions;
import org.netbeans.api.search.provider.FileNameMatcher;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.api.search.provider.impl.AbstractFileObjectIterator;
import org.netbeans.api.search.provider.impl.FilterHelper;
import org.netbeans.api.search.provider.impl.SimpleSearchIterator;
import org.netbeans.spi.search.SearchFilterDefinition;
import org.openide.filesystems.FileObject;
import org.openide.util.Enumerations;

public class FlatSearchIterator
extends AbstractFileObjectIterator {
    private Enumeration<? extends FileObject> childrenEnum;
    private boolean upToDate = false;
    private FileObject nextObject;
    private FileObject rootFile;
    private SearchListener listener;
    private FileNameMatcher fileNameMatcher;
    private FilterHelper filterHelper;
    private AtomicBoolean terminated;

    public FlatSearchIterator(FileObject root, SearchScopeOptions options, List<SearchFilterDefinition> filters, SearchListener listener, AtomicBoolean terminated) {
        this.rootFile = root;
        this.childrenEnum = this.rootFile.isFolder() ? SimpleSearchIterator.sortEnum(this.rootFile.getChildren(false)) : Enumerations.singleton((Object)this.rootFile);
        this.listener = listener;
        this.fileNameMatcher = FileNameMatcher.create(options);
        this.filterHelper = new FilterHelper(filters, options);
        this.terminated = terminated;
    }

    @Override
    public boolean hasNext() {
        assert (!EventQueue.isDispatchThread());
        if (this.terminated.get()) {
            return false;
        }
        if (!this.upToDate) {
            this.update();
        }
        return this.nextObject != null;
    }

    @Override
    public FileObject next() {
        if (!this.hasNext()) {
            throw new NoSuchElementException();
        }
        this.upToDate = false;
        return this.nextObject;
    }

    private void update() {
        assert (!this.upToDate);
        while (this.childrenEnum.hasMoreElements()) {
            FileObject fo = this.childrenEnum.nextElement();
            if (!fo.isData() || !this.fileNameMatcher.pathMatches(fo) || !this.filterHelper.fileAllowed(fo, this.listener)) continue;
            this.nextObject = fo;
            this.upToDate = true;
            return;
        }
        this.nextObject = null;
        this.upToDate = true;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}

