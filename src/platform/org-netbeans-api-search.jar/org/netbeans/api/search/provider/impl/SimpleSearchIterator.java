/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Enumerations
 */
package org.netbeans.api.search.provider.impl;

import java.awt.EventQueue;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Stack;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicBoolean;
import org.netbeans.api.search.SearchScopeOptions;
import org.netbeans.api.search.provider.FileNameMatcher;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.api.search.provider.impl.AbstractFileObjectIterator;
import org.netbeans.api.search.provider.impl.FilterHelper;
import org.netbeans.spi.search.SearchFilterDefinition;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Enumerations;

public class SimpleSearchIterator
extends AbstractFileObjectIterator {
    private Enumeration<? extends FileObject> childrenEnum;
    private final Stack<Enumeration<? extends FileObject>> enums = new Stack();
    private boolean upToDate = false;
    private FileObject nextObject;
    private FileObject rootFile;
    private SearchListener listener;
    private FileNameMatcher fileNameMatcher;
    private FilterHelper filterHelper;
    private boolean searchInArchives;
    private AtomicBoolean terminated;

    public SimpleSearchIterator(FileObject root, SearchScopeOptions options, List<SearchFilterDefinition> filters, SearchListener listener, AtomicBoolean terminated) {
        this.rootFile = root;
        this.childrenEnum = this.rootFile.isFolder() ? SimpleSearchIterator.sortEnum(this.rootFile.getChildren(false)) : Enumerations.singleton((Object)this.rootFile);
        this.listener = listener;
        this.fileNameMatcher = FileNameMatcher.create(options);
        this.searchInArchives = options.isSearchInArchives();
        this.filterHelper = new FilterHelper(filters, options);
        this.terminated = terminated;
    }

    @Override
    public boolean hasNext() {
        assert (!EventQueue.isDispatchThread());
        if (!this.upToDate) {
            this.update();
        }
        return this.nextObject != null;
    }

    @Override
    public FileObject next() {
        if (!this.hasNext()) {
            throw new NoSuchElementException();
        }
        this.upToDate = false;
        return this.nextObject;
    }

    private void update() {
        assert (!this.upToDate);
        assert (this.childrenEnum != null);
        do {
            if (this.childrenEnum.hasMoreElements()) {
                FileObject file = this.childrenEnum.nextElement();
                if (file.isFolder()) {
                    this.processFolder(file);
                    continue;
                }
                if (this.searchInArchives && this.isArchive(file)) {
                    this.listener.directoryEntered(file.getPath());
                    FileObject archRoot = FileUtil.getArchiveRoot((FileObject)file);
                    this.processFolder(archRoot);
                    continue;
                }
                if (!this.fileNameMatcher.pathMatches(file) || !this.checkFileFilters(file)) continue;
                this.nextObject = file;
                break;
            }
            this.nextObject = null;
            if (this.enums.isEmpty()) {
                this.childrenEnum = null;
                continue;
            }
            this.childrenEnum = this.enums.pop();
            this.filterHelper.popStack();
        } while (this.childrenEnum != null);
        this.upToDate = true;
    }

    private void processFolder(FileObject folder) {
        if (!this.terminated.get() && this.checkFolderFilters(folder)) {
            this.listener.directoryEntered(folder.getPath());
            this.enums.push(this.childrenEnum);
            this.childrenEnum = SimpleSearchIterator.sortEnum(folder.getChildren(false));
        }
    }

    private boolean checkFolderFilters(FileObject folder) {
        assert (folder.isFolder());
        return this.filterHelper.directoryAllowed(folder, this.listener);
    }

    private boolean checkFileFilters(FileObject file) {
        assert (!file.isFolder());
        return this.filterHelper.fileAllowed(file, this.listener);
    }

    private boolean isArchive(FileObject fo) {
        return this.isArchiveExtension(fo.getExt()) && FileUtil.isArchiveFile((FileObject)fo);
    }

    private boolean isArchiveExtension(String ext) {
        return "zip".equalsIgnoreCase(ext) || "jar".equalsIgnoreCase(ext);
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    static <T extends FileObject> Enumeration<T> sortEnum(Enumeration<T> enm) {
        TreeMap<String, FileObject> map = new TreeMap<String, FileObject>();
        while (enm.hasMoreElements()) {
            FileObject o = (FileObject)enm.nextElement();
            map.put(o.getNameExt(), o);
        }
        final Iterator iterator = map.values().iterator();
        return new Enumeration<T>(){

            @Override
            public boolean hasMoreElements() {
                return iterator.hasNext();
            }

            @Override
            public T nextElement() {
                return (T)((FileObject)iterator.next());
            }
        };
    }

}

