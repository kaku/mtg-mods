/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.api.search.provider.impl;

import java.net.URI;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.netbeans.api.search.SearchRoot;
import org.netbeans.api.search.SearchScopeOptions;
import org.netbeans.api.search.provider.SearchInfo;
import org.netbeans.api.search.provider.SearchListener;
import org.netbeans.spi.search.SearchInfoDefinition;
import org.openide.filesystems.FileObject;

public class DelegatingSearchInfo
extends SearchInfo {
    private SearchInfoDefinition delegate;

    public DelegatingSearchInfo(SearchInfoDefinition delegate) {
        this.delegate = delegate;
    }

    @Override
    public List<SearchRoot> getSearchRoots() {
        return this.delegate.getSearchRoots();
    }

    @Override
    public Iterator<FileObject> createFilesToSearchIterator(SearchScopeOptions options, SearchListener listener, AtomicBoolean terminated) {
        return this.delegate.filesToSearch(options, listener, terminated);
    }

    @Override
    protected Iterator<URI> createUrisToSearchIterator(SearchScopeOptions options, SearchListener listener, AtomicBoolean terminated) {
        return this.delegate.urisToSearch(options, listener, terminated);
    }

    @Override
    public boolean canSearch() {
        return this.delegate.canSearch();
    }
}

