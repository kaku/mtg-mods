/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.api.search.provider.impl;

import java.util.Iterator;
import org.openide.filesystems.FileObject;

public abstract class AbstractFileObjectIterator
implements Iterator<FileObject> {
    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}

