/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.api.search.provider;

import java.net.URI;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.search.SearchRoot;
import org.netbeans.api.search.SearchScopeOptions;
import org.netbeans.api.search.provider.SearchListener;
import org.openide.filesystems.FileObject;

public abstract class SearchInfo {
    public abstract boolean canSearch();

    @NonNull
    public abstract List<SearchRoot> getSearchRoots();

    @NonNull
    protected abstract Iterator<FileObject> createFilesToSearchIterator(@NonNull SearchScopeOptions var1, @NonNull SearchListener var2, @NonNull AtomicBoolean var3);

    @NonNull
    protected abstract Iterator<URI> createUrisToSearchIterator(@NonNull SearchScopeOptions var1, @NonNull SearchListener var2, @NonNull AtomicBoolean var3);

    @NonNull
    public final Iterable<FileObject> getFilesToSearch(final @NonNull SearchScopeOptions options, final @NonNull SearchListener listener, final @NonNull AtomicBoolean terminated) {
        return new Iterable<FileObject>(){

            @Override
            public Iterator<FileObject> iterator() {
                return SearchInfo.this.createFilesToSearchIterator(options, listener, terminated);
            }
        };
    }

    @NonNull
    public final Iterable<URI> getUrisToSearch(final @NonNull SearchScopeOptions options, final @NonNull SearchListener listener, final @NonNull AtomicBoolean terminated) {
        return new Iterable<URI>(){

            @Override
            public Iterator<URI> iterator() {
                return SearchInfo.this.createUrisToSearchIterator(options, listener, terminated);
            }
        };
    }

}

