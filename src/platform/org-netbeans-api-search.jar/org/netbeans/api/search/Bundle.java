/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.api.search;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String LBL_MatchType_Basic_Wildcards() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_MatchType_Basic_Wildcards");
    }

    static String LBL_MatchType_Literal() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_MatchType_Literal");
    }

    static String LBL_MatchType_Regular_Expression() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_MatchType_Regular_Expression");
    }

    private void Bundle() {
    }
}

