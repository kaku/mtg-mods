/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.search;

public final class ReplacePattern {
    private String replaceExpression;
    private boolean preserveCase;

    private ReplacePattern(String replaceExpression, boolean preserveCase) {
        this.replaceExpression = replaceExpression;
        this.preserveCase = preserveCase;
    }

    public static ReplacePattern create(String replaceExpression, boolean preserveCase) {
        return new ReplacePattern(replaceExpression, preserveCase);
    }

    public String getReplaceExpression() {
        return this.replaceExpression;
    }

    public boolean isPreserveCase() {
        return this.preserveCase;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ReplacePattern)) {
            return false;
        }
        ReplacePattern sp = (ReplacePattern)obj;
        return this.replaceExpression.equals(sp.getReplaceExpression()) && this.preserveCase == sp.isPreserveCase();
    }

    public int hashCode() {
        int result = 17;
        result = 37 * result + (this.preserveCase ? 1 : 0);
        result = 37 * result + this.replaceExpression.hashCode();
        return result;
    }

    public ReplacePattern changeReplaceExpression(String expression) {
        if (expression == null && this.replaceExpression == null || expression != null && expression.equals(this.replaceExpression)) {
            return this;
        }
        return ReplacePattern.create(expression, this.preserveCase);
    }

    public ReplacePattern changePreserveCase(boolean preserveCase) {
        if (this.preserveCase == preserveCase) {
            return this;
        }
        return ReplacePattern.create(this.replaceExpression, preserveCase);
    }

    String toCanonicalString() {
        char p = this.isPreserveCase() ? 'P' : 'p';
        return "" + p + "-" + this.getReplaceExpression();
    }

    static ReplacePattern parsePattern(String canonicalString) {
        if (canonicalString == null || Character.toUpperCase(canonicalString.charAt(0)) != 'P' || canonicalString.charAt(1) != '-') {
            return null;
        }
        boolean preserveCase = Character.isUpperCase(canonicalString.charAt(0));
        String replaceWith = canonicalString.substring(2);
        return new ReplacePattern(replaceWith, preserveCase);
    }
}

