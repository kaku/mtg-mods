/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.search;

import org.netbeans.api.search.Bundle;

public final class SearchPattern {
    private String searchExpression;
    private boolean wholeWords;
    private boolean matchCase;
    private MatchType matchType;

    private SearchPattern(String searchExpression, boolean wholeWords, boolean matchCase, MatchType matchType) {
        this.searchExpression = searchExpression;
        this.wholeWords = wholeWords;
        this.matchCase = matchCase;
        this.matchType = matchType;
    }

    public static SearchPattern create(String searchExpression, boolean wholeWords, boolean matchCase, boolean regExp) {
        return new SearchPattern(searchExpression, wholeWords, matchCase, regExp ? MatchType.REGEXP : MatchType.LITERAL);
    }

    public static SearchPattern create(String searchExpression, boolean wholeWords, boolean matchCase, MatchType matchType) {
        return new SearchPattern(searchExpression, wholeWords, matchCase, matchType);
    }

    public String getSearchExpression() {
        return this.searchExpression;
    }

    public boolean isWholeWords() {
        return this.wholeWords;
    }

    public boolean isMatchCase() {
        return this.matchCase;
    }

    public boolean isRegExp() {
        return this.matchType == MatchType.REGEXP;
    }

    public MatchType getMatchType() {
        return this.matchType;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof SearchPattern)) {
            return false;
        }
        SearchPattern sp = (SearchPattern)obj;
        return this.searchExpression.equals(sp.getSearchExpression()) && this.wholeWords == sp.isWholeWords() && this.matchCase == sp.isMatchCase() && this.matchType == sp.matchType;
    }

    public int hashCode() {
        int result = 17;
        result = 37 * result + (this.wholeWords ? 1 : 0);
        result = 37 * result + (this.matchCase ? 1 : 0);
        result = 37 * result + this.matchType.hashCode();
        result = 37 * result + this.searchExpression.hashCode();
        return result;
    }

    public SearchPattern changeSearchExpression(String expression) {
        if (expression == null && this.searchExpression == null || expression != null && expression.equals(this.searchExpression)) {
            return this;
        }
        return SearchPattern.create(expression, this.wholeWords, this.matchCase, this.matchType);
    }

    public SearchPattern changeWholeWords(boolean wholeWords) {
        if (this.wholeWords == wholeWords) {
            return this;
        }
        return SearchPattern.create(this.searchExpression, wholeWords, this.matchCase, this.matchType);
    }

    public SearchPattern changeMatchCase(boolean matchCase) {
        if (this.matchCase == matchCase) {
            return this;
        }
        return SearchPattern.create(this.searchExpression, this.wholeWords, matchCase, this.matchType);
    }

    public SearchPattern changeRegExp(boolean regExp) {
        if (this.isRegExp() == regExp) {
            return this;
        }
        return SearchPattern.create(this.searchExpression, this.wholeWords, this.matchCase, regExp);
    }

    public SearchPattern changeMatchType(MatchType matchType) {
        if (this.matchType == matchType) {
            return this;
        }
        return SearchPattern.create(this.searchExpression, this.wholeWords, this.matchCase, matchType);
    }

    String toCanonicalString() {
        char m = this.isMatchCase() ? 'M' : 'm';
        char r = this.matchType.getCanonicalPatternFlag();
        char w = this.isWholeWords() ? 'W' : 'w';
        return "" + m + r + w + "-" + this.getSearchExpression();
    }

    static SearchPattern parsePattern(String canonicalString) {
        if (canonicalString == null || Character.toUpperCase(canonicalString.charAt(0)) != 'M' || !MatchType.isCanonicalPatternFlag(canonicalString.charAt(1)) || Character.toUpperCase(canonicalString.charAt(2)) != 'W' || canonicalString.charAt(3) != '-') {
            return null;
        }
        boolean matchCase = Character.isUpperCase(canonicalString.charAt(0));
        MatchType matchType = MatchType.fromCanonicalPatternFlag(canonicalString.charAt(1));
        boolean wholeWords = Character.isUpperCase(canonicalString.charAt(2));
        String findWhat = canonicalString.substring(4);
        return new SearchPattern(findWhat, wholeWords, matchCase, matchType);
    }

    public static enum MatchType {
        LITERAL(Bundle.LBL_MatchType_Literal(), 'L'),
        BASIC(Bundle.LBL_MatchType_Basic_Wildcards(), 'r'),
        REGEXP(Bundle.LBL_MatchType_Regular_Expression(), 'R');
        
        private final String displayName;
        private final char canonicalPatternFlag;

        private MatchType(String displayName, char canonicalPatternFlag) {
            this.displayName = displayName;
            this.canonicalPatternFlag = canonicalPatternFlag;
        }

        public String toString() {
            return this.displayName;
        }

        private char getCanonicalPatternFlag() {
            return this.canonicalPatternFlag;
        }

        private static MatchType fromCanonicalPatternFlag(char ch) {
            switch (ch) {
                case 'R': {
                    return REGEXP;
                }
                case 'r': {
                    return BASIC;
                }
                case 'L': {
                    return LITERAL;
                }
            }
            return BASIC;
        }

        private static boolean isCanonicalPatternFlag(char ch) {
            for (MatchType mt : MatchType.values()) {
                if (mt.getCanonicalPatternFlag() != ch) continue;
                return true;
            }
            return false;
        }
    }

}

