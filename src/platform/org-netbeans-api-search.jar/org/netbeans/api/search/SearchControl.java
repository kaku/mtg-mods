/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 */
package org.netbeans.api.search;

import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.search.SearchPattern;
import org.netbeans.api.search.SearchScopeOptions;
import org.netbeans.modules.search.BasicSearchProvider;
import org.netbeans.modules.search.ResultView;
import org.netbeans.modules.search.SearchPanel;
import org.netbeans.spi.search.SearchScopeDefinition;
import org.netbeans.spi.search.provider.SearchProvider;

public final class SearchControl {
    private SearchControl() {
    }

    public static void openFindDialog(@NullAllowed SearchPattern searchPattern, @NullAllowed SearchScopeOptions searchScopeOptions, @NullAllowed Boolean useIgnoreList, @NullAllowed String scopeId) {
        SearchControl.openFindDialog(BasicSearchProvider.createBasicPresenter(false, searchPattern, null, false, searchScopeOptions, useIgnoreList, scopeId, new SearchScopeDefinition[0]));
    }

    public static void openReplaceDialog(@NullAllowed SearchPattern searchPattern, @NullAllowed String replaceString, @NullAllowed Boolean preserveCase, @NullAllowed SearchScopeOptions searchScopeOptions, @NullAllowed Boolean useIgnoreList, @NullAllowed String scopeId) {
        SearchControl.openReplaceDialog(BasicSearchProvider.createBasicPresenter(true, searchPattern, replaceString, preserveCase, searchScopeOptions, useIgnoreList, scopeId, new SearchScopeDefinition[0]));
    }

    public static void openFindDialog(SearchProvider.Presenter presenter) {
        SearchControl.openDialog(false, presenter);
    }

    public static void openReplaceDialog(SearchProvider.Presenter presenter) {
        SearchControl.openDialog(true, presenter);
    }

    private static void openDialog(boolean replaceMode, SearchProvider.Presenter presenter) {
        SearchPanel current = SearchPanel.getCurrentlyShown();
        if (current != null) {
            current.close();
        }
        if (ResultView.getInstance().isFocused()) {
            ResultView.getInstance().markCurrentTabAsReusable();
        }
        new SearchPanel(replaceMode, presenter).showDialog();
    }

    public static void startBasicSearch(@NonNull SearchPattern searchPattern, @NonNull SearchScopeOptions searchScopeOptions, @NullAllowed String scopeId) throws IllegalArgumentException {
        BasicSearchProvider.startSearch(searchPattern, searchScopeOptions, scopeId);
    }
}

