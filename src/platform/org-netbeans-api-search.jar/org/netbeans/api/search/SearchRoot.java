/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Parameters
 */
package org.netbeans.api.search;

import java.io.File;
import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.search.provider.SearchFilter;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Parameters;

public final class SearchRoot {
    private List<SearchFilter> filters;
    private FileObject rootFile;
    private URI rootUri;
    private static final List<SearchFilter> EMPTY_FILTER_LIST = Collections.emptyList();
    private static final Logger LOG = Logger.getLogger(SearchRoot.class.getName());

    public SearchRoot(@NonNull FileObject rootFile, @NullAllowed List<SearchFilter> filters) {
        Parameters.notNull((CharSequence)"rootFile", (Object)rootFile);
        this.rootFile = rootFile;
        this.filters = filters == null ? EMPTY_FILTER_LIST : filters;
    }

    public SearchRoot(@NonNull URI rootUri, @NullAllowed List<SearchFilter> filters) {
        Parameters.notNull((CharSequence)"rootFile", (Object)this.rootFile);
        this.rootUri = rootUri;
        this.filters = filters == null ? EMPTY_FILTER_LIST : filters;
    }

    @NonNull
    public List<SearchFilter> getFilters() {
        return this.filters;
    }

    @NonNull
    public FileObject getFileObject() {
        if (this.rootFile == null) {
            try {
                FileObject fo = FileUtil.toFileObject((File)new File(this.rootUri));
                this.rootFile = fo == null ? this.createFakeFile(this.rootUri, null) : fo;
            }
            catch (Exception e) {
                this.rootFile = this.createFakeFile(this.rootUri, e);
            }
        }
        return this.rootFile;
    }

    @NonNull
    public URI getUri() {
        if (this.rootUri == null) {
            this.rootUri = this.rootFile.toURI();
        }
        return this.rootUri;
    }

    private FileObject createFakeFile(URI uri, Throwable t) {
        LOG.log(Level.INFO, "Invalid URI: " + uri, t);
        return FileUtil.createMemoryFileSystem().getRoot();
    }
}

