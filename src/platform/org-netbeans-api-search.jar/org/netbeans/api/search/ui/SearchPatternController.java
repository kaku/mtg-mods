/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.util.Parameters
 */
package org.netbeans.api.search.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import javax.swing.AbstractButton;
import javax.swing.ComboBoxEditor;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.ListCellRenderer;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.search.SearchHistory;
import org.netbeans.api.search.SearchPattern;
import org.netbeans.api.search.ui.ComponentController;
import org.netbeans.modules.search.FindDialogMemory;
import org.netbeans.modules.search.ui.PatternChangeListener;
import org.netbeans.modules.search.ui.ShorteningCellRenderer;
import org.netbeans.modules.search.ui.TextFieldFocusListener;
import org.netbeans.modules.search.ui.UiUtils;
import org.openide.util.Parameters;

public final class SearchPatternController
extends ComponentController<JComboBox> {
    private JTextComponent textToFindEditor;
    private final Map<Option, AbstractButton> bindings = new EnumMap<Option, AbstractButton>(Option.class);
    private final Map<Option, Boolean> options = new EnumMap<Option, Boolean>(Option.class);
    private JComboBox matchTypeComboBox = null;
    private SearchPattern.MatchType matchType = SearchPattern.MatchType.LITERAL;
    private final ItemListener listener;
    private boolean valid;
    private Color defaultTextColor = null;

    SearchPatternController(final JComboBox component) {
        super(component);
        component.setEditable(true);
        Component cboxEditorComp = component.getEditor().getEditorComponent();
        component.setRenderer(new ShorteningCellRenderer());
        this.textToFindEditor = (JTextComponent)cboxEditorComp;
        this.textToFindEditor.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent e) {
                SearchPatternController.this.patternChanged();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                SearchPatternController.this.patternChanged();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                SearchPatternController.this.patternChanged();
            }
        });
        this.initHistory();
        this.valid = this.checkValid();
        this.updateTextPatternColor();
        this.textToFindEditor.addFocusListener(new TextFieldFocusListener());
        this.textToFindEditor.getDocument().addDocumentListener(new TextToFindChangeListener());
        component.addItemListener(new ItemListener(){

            @Override
            public void itemStateChanged(ItemEvent e) {
                Object si = component.getSelectedItem();
                if (si instanceof ModelItem) {
                    SearchPattern sp = ((ModelItem)si).sp;
                    for (Map.Entry be : SearchPatternController.this.bindings.entrySet()) {
                        switch ((Option)((Object)be.getKey())) {
                            case MATCH_CASE: {
                                ((AbstractButton)be.getValue()).setSelected(sp.isMatchCase());
                                break;
                            }
                            case WHOLE_WORDS: {
                                ((AbstractButton)be.getValue()).setSelected(sp.isWholeWords());
                                break;
                            }
                            case REGULAR_EXPRESSION: {
                                ((AbstractButton)be.getValue()).setSelected(sp.isRegExp());
                            }
                        }
                    }
                    if (SearchPatternController.this.matchTypeComboBox != null) {
                        SearchPatternController.this.matchTypeComboBox.setSelectedItem((Object)sp.getMatchType());
                        SearchPatternController.this.matchType = (SearchPattern.MatchType)((Object)SearchPatternController.this.matchTypeComboBox.getSelectedItem());
                    } else {
                        SearchPatternController.this.matchType = sp.getMatchType();
                    }
                    SearchPatternController.this.options.put(Option.MATCH_CASE, sp.isMatchCase());
                    SearchPatternController.this.options.put(Option.WHOLE_WORDS, sp.isWholeWords());
                    SearchPatternController.this.options.put(Option.REGULAR_EXPRESSION, sp.isRegExp());
                }
            }
        });
        this.listener = new ItemListener(){

            @Override
            public void itemStateChanged(ItemEvent e) {
                for (Map.Entry entry : SearchPatternController.this.bindings.entrySet()) {
                    if (entry.getValue() != e.getSource()) continue;
                    SearchPatternController.this.setOption((Option)((Object)entry.getKey()), e.getStateChange() == 1);
                    break;
                }
            }
        };
    }

    private void initHistory() {
        DefaultComboBoxModel<ModelItem> model = new DefaultComboBoxModel<ModelItem>();
        List<SearchPattern> data = SearchHistory.getDefault().getSearchPatterns();
        for (SearchPattern sp : data) {
            model.addElement(new ModelItem(sp));
        }
        ((JComboBox)this.component).setModel(model);
        if (data.size() > 0) {
            this.setSearchPattern(data.get(0));
        }
        if (!FindDialogMemory.getDefault().isTextPatternSpecified()) {
            ((JComboBox)this.component).setSelectedItem("");
        }
    }

    @NonNull
    private String getText() {
        String s = this.textToFindEditor.getText();
        return s == null ? "" : s;
    }

    private void setText(@NullAllowed String text) {
        ((JComboBox)this.component).setSelectedItem(text == null ? "" : text);
    }

    private boolean getOption(@NonNull Option option) {
        Parameters.notNull((CharSequence)"option", (Object)((Object)option));
        Boolean b = this.options.get((Object)option);
        if (b == null) {
            return false;
        }
        return b;
    }

    private void setOption(@NonNull Option option, boolean value) {
        Parameters.notNull((CharSequence)"option", (Object)((Object)option));
        this.options.put(option, value);
        AbstractButton button = this.bindings.get((Object)option);
        if (button != null) {
            button.setSelected(value);
        }
        if (option == Option.REGULAR_EXPRESSION) {
            if (this.matchType == SearchPattern.MatchType.REGEXP != value) {
                this.setMatchType(value ? SearchPattern.MatchType.REGEXP : SearchPattern.MatchType.LITERAL);
            }
            this.updateValidity();
        }
        this.fireChange();
    }

    private void setMatchType(SearchPattern.MatchType newMatchType) {
        Parameters.notNull((CharSequence)"matchType", (Object)((Object)this.matchType));
        if (this.matchTypeComboBox != null) {
            if (this.matchTypeComboBox.getSelectedItem() != newMatchType) {
                this.matchTypeComboBox.setSelectedItem((Object)newMatchType);
            }
            this.matchType = (SearchPattern.MatchType)((Object)this.matchTypeComboBox.getSelectedItem());
        } else {
            this.matchType = newMatchType;
        }
        if (this.matchTypeComboBox != null && this.matchTypeComboBox.getSelectedItem() != this.matchType) {
            this.matchTypeComboBox.setSelectedItem((Object)this.matchType);
        }
        if (this.getOption(Option.REGULAR_EXPRESSION) != (SearchPattern.MatchType.REGEXP == this.matchType)) {
            this.setOption(Option.REGULAR_EXPRESSION, this.matchType == SearchPattern.MatchType.REGEXP);
        }
        this.updateValidity();
        this.fireChange();
    }

    @NonNull
    public SearchPattern getSearchPattern() {
        return SearchPattern.create(this.getText(), this.getOption(Option.WHOLE_WORDS), this.getOption(Option.MATCH_CASE), this.matchType);
    }

    public void setSearchPattern(@NonNull SearchPattern searchPattern) {
        Parameters.notNull((CharSequence)"searchPattern", (Object)searchPattern);
        this.setText(searchPattern.getSearchExpression());
        this.setOption(Option.WHOLE_WORDS, searchPattern.isWholeWords());
        this.setOption(Option.MATCH_CASE, searchPattern.isMatchCase());
        this.setMatchType(searchPattern.getMatchType());
    }

    public void bind(final @NonNull Option option, final @NonNull AbstractButton button) {
        Parameters.notNull((CharSequence)"option", (Object)((Object)option));
        Parameters.notNull((CharSequence)"button", (Object)button);
        if (this.bindings.containsKey((Object)option)) {
            throw new IllegalStateException("Already binded with option " + (Object)((Object)option));
        }
        this.bindings.put(option, button);
        button.setSelected(this.getOption(option));
        button.addItemListener(new ItemListener(){

            @Override
            public void itemStateChanged(ItemEvent e) {
                SearchPatternController.this.setOption(option, button.isSelected());
            }
        });
    }

    public void bindMatchTypeComboBox(final @NonNull JComboBox comboBox) {
        Parameters.notNull((CharSequence)"comboBox", (Object)comboBox);
        boolean regexpFound = false;
        boolean literalFound = false;
        for (int i = 0; i < comboBox.getItemCount(); ++i) {
            if (comboBox.getItemAt(i) == SearchPattern.MatchType.LITERAL) {
                literalFound = true;
                continue;
            }
            if (comboBox.getItemAt(i) == SearchPattern.MatchType.REGEXP) {
                regexpFound = true;
                continue;
            }
            if (comboBox.getItemAt(i) instanceof SearchPattern.MatchType) continue;
            throw new IllegalArgumentException("Model of the combo box can contain only MatchType items");
        }
        if (!regexpFound || !literalFound) {
            throw new IllegalArgumentException("At least MatchType.LITERAL and MatchType.REGEXP must be contained in the combo box model.");
        }
        if (this.matchTypeComboBox != null) {
            throw new IllegalStateException("Already bound with option MATCH_TYPE");
        }
        this.matchTypeComboBox = comboBox;
        comboBox.setEditable(false);
        this.setMatchType(this.matchType);
        comboBox.setSelectedItem((Object)this.matchType);
        comboBox.addItemListener(new ItemListener(){

            @Override
            public void itemStateChanged(ItemEvent e) {
                SearchPatternController.this.setMatchType((SearchPattern.MatchType)((Object)comboBox.getSelectedItem()));
            }
        });
    }

    public void unbind(@NonNull Option option, @NonNull AbstractButton button) {
        Parameters.notNull((CharSequence)"option", (Object)((Object)option));
        Parameters.notNull((CharSequence)"button", (Object)button);
        this.bindings.remove((Object)option);
        button.removeItemListener(this.listener);
    }

    private void patternChanged() {
        this.updateValidity();
        this.fireChange();
    }

    private void updateValidity() {
        boolean wasValid = this.valid;
        this.valid = this.checkValid();
        if (this.valid != wasValid) {
            this.updateTextPatternColor();
        }
    }

    private boolean checkValid() {
        String expr = this.getText();
        if (!this.getOption(Option.REGULAR_EXPRESSION) || expr == null) {
            return true;
        }
        try {
            Pattern p = Pattern.compile(this.getText());
            return true;
        }
        catch (PatternSyntaxException p) {
            return false;
        }
    }

    private void updateTextPatternColor() {
        Color dfltColor = this.getDefaultTextColor();
        this.textToFindEditor.setForeground(this.valid ? dfltColor : UiUtils.getErrorTextColor());
    }

    private Color getDefaultTextColor() {
        if (this.defaultTextColor == null) {
            this.defaultTextColor = ((JComboBox)this.component).getForeground();
        }
        return this.defaultTextColor;
    }

    private static class ModelItem {
        final SearchPattern sp;

        public ModelItem(SearchPattern sp) {
            this.sp = sp;
        }

        public String toString() {
            return this.sp.getSearchExpression();
        }
    }

    private class TextToFindChangeListener
    extends PatternChangeListener {
        private TextToFindChangeListener() {
        }

        @Override
        public void handleComboBoxChange(String text) {
            SearchPatternController.this.patternChanged();
        }
    }

    public static enum Option {
        MATCH_CASE,
        WHOLE_WORDS,
        REGULAR_EXPRESSION;
        

        private Option() {
        }
    }

}

