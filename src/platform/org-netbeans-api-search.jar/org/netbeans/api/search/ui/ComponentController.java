/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.ChangeSupport
 */
package org.netbeans.api.search.ui;

import javax.swing.JComponent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.NonNull;
import org.openide.util.ChangeSupport;

abstract class ComponentController<T extends JComponent> {
    private final ChangeSupport changeSupport;
    protected T component;

    protected ComponentController(T component) {
        this.changeSupport = new ChangeSupport((Object)this);
        this.component = component;
    }

    public final T getComponent() {
        return this.component;
    }

    public final void addChangeListener(@NonNull ChangeListener listener) {
        this.changeSupport.addChangeListener(listener);
    }

    public final void removeChangeListener(@NonNull ChangeListener listener) {
        this.changeSupport.removeChangeListener(listener);
    }

    protected final void fireChange() {
        this.changeSupport.fireChange();
    }

    public final boolean hasListeners() {
        return this.changeSupport.hasListeners();
    }
}

