/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 */
package org.netbeans.api.search.ui;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.search.ui.FileNameController;
import org.netbeans.api.search.ui.ScopeController;
import org.netbeans.api.search.ui.ScopeOptionsController;
import org.netbeans.api.search.ui.SearchPatternController;
import org.netbeans.modules.search.FindDialogMemory;
import org.netbeans.spi.search.SearchScopeDefinition;

public class ComponentUtils {
    private ComponentUtils() {
    }

    @NonNull
    public static FileNameController adjustComboForFileName(@NonNull JComboBox jComboBox) {
        return new FileNameController(jComboBox);
    }

    @NonNull
    public static /* varargs */ ScopeController adjustComboForScope(@NonNull JComboBox jComboBox, @NullAllowed String preferredScopeId, @NonNull SearchScopeDefinition ... extraSearchScopes) {
        return new ScopeController(jComboBox, preferredScopeId == null ? FindDialogMemory.getDefault().getScopeTypeId() : preferredScopeId, extraSearchScopes);
    }

    @NonNull
    public static ScopeOptionsController adjustPanelForOptions(@NonNull JPanel jPanel, boolean searchAndReplace, @NonNull FileNameController fileNameController) {
        return new ScopeOptionsController(jPanel, fileNameController, searchAndReplace);
    }

    @NonNull
    public static ScopeOptionsController adjustPanelsForOptions(@NonNull JPanel scopePanel, @NonNull JPanel fileNamePanel, boolean searchAndReplace, @NonNull FileNameController fileNameController) {
        return new ScopeOptionsController(scopePanel, fileNamePanel, fileNameController, searchAndReplace);
    }

    @NonNull
    public static SearchPatternController adjustComboForSearchPattern(@NonNull JComboBox jComboBox) {
        return new SearchPatternController(jComboBox);
    }
}

