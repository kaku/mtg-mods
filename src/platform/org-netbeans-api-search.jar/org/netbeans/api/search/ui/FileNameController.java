/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package org.netbeans.api.search.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import javax.swing.ComboBoxEditor;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.search.RegexpUtil;
import org.netbeans.api.search.SearchScopeOptions;
import org.netbeans.api.search.ui.ComponentController;
import org.netbeans.modules.search.FindDialogMemory;
import org.netbeans.modules.search.ListComboBoxModel;
import org.netbeans.modules.search.ui.PatternChangeListener;
import org.netbeans.modules.search.ui.TextFieldFocusListener;
import org.netbeans.modules.search.ui.UiUtils;
import org.openide.util.Exceptions;

public final class FileNameController
extends ComponentController<JComboBox> {
    private FileNamePatternWatcher fileNamePatternWatcher;
    private JTextComponent fileNamePatternEditor;
    private boolean regexp = false;
    private boolean ignoreFileNamePatternChanges;
    private boolean patternValid;
    private Color defaultColor;

    FileNameController(JComboBox jComboBox) {
        super(jComboBox);
        this.init();
    }

    private void init() {
        Component cboxEditorComp = ((JComboBox)this.component).getEditor().getEditorComponent();
        this.fileNamePatternEditor = (JTextComponent)cboxEditorComp;
        this.fileNamePatternWatcher = new FileNamePatternWatcher(this.fileNamePatternEditor);
        this.fileNamePatternEditor.addFocusListener(this.fileNamePatternWatcher);
        this.fileNamePatternEditor.addHierarchyListener(this.fileNamePatternWatcher);
        this.fileNamePatternEditor.getDocument().addDocumentListener(new FileNameChangeListener());
        this.defaultColor = ((JComboBox)this.component).getForeground();
        ((JComboBox)this.component).setEditable(true);
        List<String> entries = FindDialogMemory.getDefault().getFileNamePatterns();
        if (!entries.isEmpty()) {
            ((JComboBox)this.component).setModel(new ListComboBoxModel<String>(entries, true));
        }
    }

    public String getFileNamePattern() {
        if (this.isAllFilesInfoDisplayed()) {
            return "";
        }
        return this.fileNamePatternEditor.getText();
    }

    public void setFileNamePattern(String pattern) {
        ((JComboBox)this.component).setSelectedItem(pattern);
    }

    public boolean isRegularExpression() {
        return this.regexp;
    }

    public boolean isAllFilesInfoDisplayed() {
        return this.fileNamePatternWatcher.infoDisplayed;
    }

    public void setRegularExpression(boolean regularExpression) {
        this.regexp = regularExpression;
        this.setFileNamePatternToolTip();
        this.patternChanged();
    }

    public void displayAllFilesInfo() {
        this.fileNamePatternWatcher.displayInfo();
    }

    public void hideAllFilesInfo() {
        this.fileNamePatternWatcher.hideInfo();
    }

    private void setFileNamePatternToolTip() {
        ((JComboBox)this.component).setToolTipText(UiUtils.getFileNamePatternsExample(this.regexp));
    }

    private void patternChanged() {
        if (!this.ignoreFileNamePatternChanges) {
            this.updateFileNamePatternColor();
            this.fireChange();
        }
    }

    private void updateFileNamePatternColor() {
        boolean wasInvalid = this.patternValid;
        String pattern = this.getFileNamePattern();
        if (pattern == null || pattern.isEmpty()) {
            this.patternValid = true;
        } else {
            try {
                Pattern p = RegexpUtil.makeFileNamePattern(SearchScopeOptions.create(this.getFileNamePattern(), this.regexp));
                this.patternValid = p != null;
            }
            catch (PatternSyntaxException e) {
                this.patternValid = false;
            }
        }
        if (this.patternValid != wasInvalid && !this.isAllFilesInfoDisplayed()) {
            this.fileNamePatternEditor.setForeground(this.patternValid ? this.defaultColor : UiUtils.getErrorTextColor());
        }
    }

    private final class FileNameChangeListener
    extends PatternChangeListener {
        private FileNameChangeListener() {
        }

        @Override
        public void handleComboBoxChange(String text) {
            FileNameController.this.patternChanged();
        }
    }

    private final class FileNamePatternWatcher
    extends TextFieldFocusListener
    implements HierarchyListener {
        private final JTextComponent txtComp;
        private final Document doc;
        private Color foregroundColor;
        private String infoText;
        private boolean infoDisplayed;
        private final Logger watcherLogger;

        private FileNamePatternWatcher(JTextComponent txtComp) {
            this.watcherLogger = Logger.getLogger(this.getClass().getName());
            this.txtComp = txtComp;
            this.doc = txtComp.getDocument();
        }

        @Override
        public void hierarchyChanged(HierarchyEvent e) {
            if (e.getComponent() != this.txtComp || (e.getChangeFlags() & 2) == 0 || !this.txtComp.isDisplayable()) {
                return;
            }
            this.watcherLogger.finer("componentShown()");
            if (this.foregroundColor == null) {
                this.foregroundColor = this.txtComp.getForeground();
            }
            if (this.doc.getLength() == 0 && !this.txtComp.isFocusOwner()) {
                this.displayInfo();
            }
        }

        @Override
        public void focusGained(FocusEvent e) {
            if (this.infoDisplayed) {
                this.hideInfo();
            }
            super.focusGained(e);
        }

        @Override
        public void focusLost(FocusEvent e) {
            super.focusLost(e);
            if (this.isEmptyText()) {
                this.displayInfo();
            }
        }

        private boolean isEmptyText() {
            String text;
            int length = this.doc.getLength();
            if (length == 0) {
                return true;
            }
            try {
                text = this.doc.getText(0, length);
            }
            catch (Exception ex) {
                Exceptions.printStackTrace((Throwable)ex);
                text = null;
            }
            return text != null && text.trim().length() == 0;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void displayInfo() {
            assert (this.isEmptyText() && !this.txtComp.isFocusOwner());
            this.watcherLogger.finer("displayInfo()");
            try {
                this.txtComp.setForeground(this.txtComp.getDisabledTextColor());
                FileNameController.this.ignoreFileNamePatternChanges = true;
                this.doc.remove(0, this.doc.getLength());
                this.doc.insertString(0, this.getInfoText(), null);
            }
            catch (BadLocationException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            finally {
                FileNameController.this.ignoreFileNamePatternChanges = false;
                this.infoDisplayed = true;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void hideInfo() {
            this.watcherLogger.finer("hideInfo()");
            this.txtComp.setEnabled(true);
            try {
                FileNameController.this.ignoreFileNamePatternChanges = true;
                if (this.doc.getText(0, this.doc.getLength()).equals(this.getInfoText())) {
                    this.doc.remove(0, this.doc.getLength());
                }
            }
            catch (BadLocationException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            finally {
                FileNameController.this.ignoreFileNamePatternChanges = false;
                this.txtComp.setForeground(this.foregroundColor);
                this.infoDisplayed = false;
            }
        }

        private String getInfoText() {
            if (this.infoText == null) {
                this.infoText = UiUtils.getText("BasicSearchForm.cboxFileNamePattern.allFiles");
            }
            return this.infoText;
        }
    }

}

