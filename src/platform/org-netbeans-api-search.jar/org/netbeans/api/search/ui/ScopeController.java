/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.openide.util.WeakListeners
 */
package org.netbeans.api.search.ui;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.util.List;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.search.provider.SearchInfo;
import org.netbeans.api.search.ui.ComponentController;
import org.netbeans.modules.search.SearchPanel;
import org.netbeans.modules.search.SearchScopeList;
import org.netbeans.spi.search.SearchScopeDefinition;
import org.openide.util.WeakListeners;

public final class ScopeController
extends ComponentController<JComboBox> {
    SearchScopeChangeListener searchScopeChangeListener;
    ChangeListener searchScopeChangeListenerWeak;
    private SearchScopeDefinition selectedSearchScope;
    private ManualSelectionListener manualSelectionListener;
    private String preferredId = null;
    private String manuallySelectedId = null;
    private boolean active = false;
    SearchScopeList scopeList;
    SearchScopeDefinition[] extraSearchScopes;

    /* varargs */ ScopeController(JComboBox jComboBox, String preferredId, SearchScopeDefinition ... extraSearchScopes) {
        super(jComboBox);
        this.preferredId = preferredId;
        this.manuallySelectedId = null;
        this.extraSearchScopes = extraSearchScopes;
        ((JComboBox)this.component).addHierarchyListener(new ScopeComboBoxHierarchyListener());
        ((JComboBox)this.component).setEditable(false);
        ((JComboBox)this.component).setRenderer(new ScopeCellRenderer());
    }

    private String chooseId() {
        if (SearchPanel.isOpenedForSelection()) {
            return "node selection";
        }
        return this.preferredId;
    }

    private void updateScopeItems(String preferredId) {
        ((JComboBox)this.component).removeAllItems();
        this.selectedSearchScope = null;
        for (SearchScopeDefinition ss : this.scopeList.getSeachScopeDefinitions()) {
            if (!ss.isApplicable()) continue;
            ScopeItem si = new ScopeItem(ss);
            ((JComboBox)this.component).addItem(si);
            if (this.selectedSearchScope != null || !ss.getTypeId().equals(preferredId)) continue;
            this.selectedSearchScope = ss;
            ((JComboBox)this.component).setSelectedItem(si);
        }
        if (this.selectedSearchScope == null) {
            ScopeItem si = (ScopeItem)((JComboBox)this.component).getItemAt(0);
            this.selectedSearchScope = si.getSearchScope();
            ((JComboBox)this.component).setSelectedIndex(0);
        }
    }

    private SearchScopeDefinition getSelectedSearchScope() {
        return this.selectedSearchScope;
    }

    @CheckForNull
    public String getSelectedScopeId() {
        SearchScopeDefinition ss = this.getSelectedSearchScope();
        return ss == null ? null : ss.getTypeId();
    }

    @CheckForNull
    public String getSelectedScopeTitle() {
        ScopeItem si = (ScopeItem)((JComboBox)this.component).getSelectedItem();
        return si == null ? null : si.toString();
    }

    @CheckForNull
    public SearchInfo getSearchInfo() {
        SearchScopeDefinition ss = this.getSelectedSearchScope();
        if (ss == null) {
            return null;
        }
        SearchInfo ssi = ss.getSearchInfo();
        return ssi;
    }

    private static class ScopeCellRenderer
    extends DefaultListCellRenderer {
        private ScopeCellRenderer() {
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            Component component = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            if (component instanceof JLabel) {
                JLabel label = (JLabel)component;
                if (value instanceof ScopeItem) {
                    ScopeItem item = (ScopeItem)value;
                    label.setIcon(item.getSearchScope().getIcon());
                }
            }
            return component;
        }
    }

    private class ScopeComboBoxHierarchyListener
    implements HierarchyListener {
        private ScopeComboBoxHierarchyListener() {
        }

        @Override
        public void hierarchyChanged(HierarchyEvent e) {
            if ((e.getChangeFlags() & 4) != 0) {
                this.toggleListeners();
            }
        }

        private synchronized void toggleListeners() {
            if (((JComboBox)ScopeController.this.component).isShowing() && !ScopeController.this.active) {
                this.initListeners();
            } else if (!((JComboBox)ScopeController.this.component).isShowing() && ScopeController.this.active) {
                this.cleanListeners();
            }
        }

        private void initListeners() {
            if (ScopeController.this.manuallySelectedId == null) {
                ScopeController.this.manuallySelectedId = ScopeController.this.chooseId();
            }
            ScopeController.this.scopeList = new SearchScopeList(ScopeController.this.extraSearchScopes);
            ScopeController.this.manualSelectionListener = new ManualSelectionListener();
            ScopeController.this.searchScopeChangeListener = new SearchScopeChangeListener();
            ScopeController.this.searchScopeChangeListenerWeak = WeakListeners.change((ChangeListener)ScopeController.this.searchScopeChangeListener, (Object)ScopeController.this.scopeList);
            ScopeController.this.scopeList.addChangeListener(ScopeController.this.searchScopeChangeListenerWeak);
            ScopeController.this.updateScopeItems(ScopeController.this.manuallySelectedId);
            ((JComboBox)ScopeController.this.component).addActionListener(ScopeController.this.manualSelectionListener);
            ScopeController.this.active = true;
        }

        private void cleanListeners() {
            ScopeController.this.scopeList.removeChangeListener(ScopeController.this.searchScopeChangeListenerWeak);
            ScopeController.this.searchScopeChangeListenerWeak = null;
            ScopeController.this.searchScopeChangeListener = null;
            ScopeController.this.scopeList.clean();
            ScopeController.this.scopeList = null;
            ((JComboBox)ScopeController.this.component).removeActionListener(ScopeController.this.manualSelectionListener);
            ScopeController.this.manualSelectionListener = null;
            ScopeController.this.active = false;
        }
    }

    private class ManualSelectionListener
    implements ActionListener {
        private ManualSelectionListener() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            ScopeItem item = (ScopeItem)((JComboBox)ScopeController.this.component).getSelectedItem();
            if (item != null) {
                ScopeController.this.selectedSearchScope = item.getSearchScope();
                ScopeController.this.manuallySelectedId = ScopeController.this.selectedSearchScope.getTypeId();
                ScopeController.this.selectedSearchScope.selected();
            } else {
                ScopeController.this.selectedSearchScope = null;
            }
        }
    }

    private class SearchScopeChangeListener
    implements ChangeListener {
        private SearchScopeChangeListener() {
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            if (ScopeController.this.manuallySelectedId == null && ScopeController.this.selectedSearchScope != null) {
                ScopeController.this.manuallySelectedId = ScopeController.this.selectedSearchScope.getTypeId();
            }
            ((JComboBox)ScopeController.this.component).removeActionListener(ScopeController.this.manualSelectionListener);
            ScopeController.this.updateScopeItems(ScopeController.this.manuallySelectedId);
            ((JComboBox)ScopeController.this.component).addActionListener(ScopeController.this.manualSelectionListener);
            Dialog d = (Dialog)SwingUtilities.getAncestorOfClass(Dialog.class, ScopeController.this.component);
            if (d != null) {
                d.repaint();
            }
        }
    }

    private final class ScopeItem {
        private static final String START = "(";
        private static final String END = ")";
        private static final String SP = " ";
        private static final String ELLIPSIS = "...";
        private static final int MAX_EXTRA_INFO_LEN = 20;
        private SearchScopeDefinition searchScope;

        public ScopeItem(SearchScopeDefinition searchScope) {
            this.searchScope = searchScope;
        }

        public SearchScopeDefinition getSearchScope() {
            return this.searchScope;
        }

        private boolean isAdditionaInfoAvailable() {
            return this.searchScope.getAdditionalInfo() != null && this.searchScope.getAdditionalInfo().length() > 0;
        }

        private String getTextForLabel(String text) {
            String extraInfo;
            String extraText = extraInfo = this.searchScope.getAdditionalInfo();
            if (extraInfo.length() > 20 && (extraText = extraInfo.substring(0, 20) + "...").length() >= extraInfo.length()) {
                extraText = extraInfo;
            }
            return this.getFullText(text, extraText);
        }

        private String getFullText(String text, String extraText) {
            return text + " " + "(" + extraText + ")";
        }

        public String toString() {
            if (this.isAdditionaInfoAvailable()) {
                return this.getTextForLabel(this.clr(this.searchScope.getDisplayName()));
            }
            return this.clr(this.searchScope.getDisplayName());
        }

        private String clr(String s) {
            return s == null ? "" : s.replaceAll("\\&", "");
        }
    }

}

