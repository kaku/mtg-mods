/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NullUnknown
 */
package org.netbeans.api.search.ui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import org.netbeans.api.annotations.common.NullUnknown;
import org.netbeans.api.search.SearchScopeOptions;
import org.netbeans.api.search.ui.ComponentController;
import org.netbeans.api.search.ui.FileNameController;
import org.netbeans.modules.search.BasicSearchProvider;
import org.netbeans.modules.search.IgnoreListPanel;
import org.netbeans.modules.search.PatternSandbox;
import org.netbeans.modules.search.ui.CheckBoxWithButtonPanel;
import org.netbeans.modules.search.ui.FormLayoutHelper;
import org.netbeans.modules.search.ui.UiUtils;
import org.netbeans.spi.search.SearchFilterDefinition;

public final class ScopeOptionsController
extends ComponentController<JPanel> {
    private FileNameController fileNameComboBox;
    private boolean replacing;
    protected JPanel ignoreListOptionPanel;
    private JButton btnEditIgnoreList;
    protected JCheckBox chkUseIgnoreList;
    private JCheckBox chkFileNameRegex;
    private JButton btnTestFileNamePattern;
    private JCheckBox chkArchives;
    private JCheckBox chkGenerated;
    private ItemListener checkBoxListener;
    private JPanel fileNameComponent;

    ScopeOptionsController(JPanel component, FileNameController fileNameComboBox, boolean replacing) {
        this(component, null, fileNameComboBox, replacing);
    }

    ScopeOptionsController(JPanel scopeComponent, JPanel fileNameComponent, FileNameController fileNameController, boolean replacing) {
        super(scopeComponent);
        this.fileNameComponent = fileNameComponent;
        this.fileNameComboBox = fileNameController;
        this.replacing = replacing;
        this.init();
    }

    public SearchScopeOptions getSearchScopeOptions() {
        SearchScopeOptions sso = SearchScopeOptions.create();
        if (this.fileNameComboBox != null) {
            sso.setPattern(this.fileNameComboBox.getFileNamePattern());
        }
        sso.setRegexp(this.isFileNameRegExp());
        sso.setSearchInArchives(this.isSearchInArchives());
        sso.setSearchInGenerated(this.isSearchInGenerated());
        if (this.isUseIgnoreList()) {
            sso.addFilter(BasicSearchProvider.getIgnoreListFilter());
        }
        return sso;
    }

    private void init() {
        this.btnTestFileNamePattern = new JButton();
        this.chkFileNameRegex = new JCheckBox();
        this.chkFileNameRegex.setToolTipText(UiUtils.getText("BasicSearchForm.chkFileNameRegex.tooltip"));
        if (!this.replacing) {
            this.chkArchives = new JCheckBox();
            this.chkGenerated = new JCheckBox();
        }
        this.chkUseIgnoreList = new JCheckBox();
        this.btnEditIgnoreList = new JButton();
        this.checkBoxListener = new CheckBoxListener();
        ((JPanel)this.component).setLayout(new FlowLayout(3, 0, 0));
        this.setMnemonics();
        this.initIgnoreListControlComponents();
        this.initScopeOptionsRow(this.replacing);
        this.initInteraction();
    }

    private void initIgnoreListControlComponents() {
        this.ignoreListOptionPanel = new CheckBoxWithButtonPanel(this.chkUseIgnoreList, this.btnEditIgnoreList);
    }

    private void initScopeOptionsRow(boolean searchAndReplace) {
        CheckBoxWithButtonPanel regexpPanel = new CheckBoxWithButtonPanel(this.chkFileNameRegex, this.btnTestFileNamePattern);
        if (this.fileNameComponent != null) {
            this.fileNameComponent.setLayout(new FlowLayout(3, 0, 0));
            this.fileNameComponent.add(this.ignoreListOptionPanel);
            this.fileNameComponent.add(regexpPanel);
            if (!searchAndReplace) {
                ((JPanel)this.component).add(this.chkArchives);
                ((JPanel)this.component).add(this.chkGenerated);
            }
        } else {
            JPanel jp = new JPanel();
            if (searchAndReplace) {
                jp.setLayout(new FlowLayout(3, 0, 0));
                jp.add(this.ignoreListOptionPanel);
                jp.add(regexpPanel);
                jp.setMaximumSize(jp.getMinimumSize());
            } else {
                FormLayoutHelper flh = new FormLayoutHelper(jp, FormLayoutHelper.DEFAULT_COLUMN, FormLayoutHelper.DEFAULT_COLUMN);
                flh.addRow(this.chkArchives, this.chkGenerated);
                flh.addRow(this.ignoreListOptionPanel, new CheckBoxWithButtonPanel(this.chkFileNameRegex, this.btnTestFileNamePattern));
                jp.setMaximumSize(jp.getMinimumSize());
            }
            ((JPanel)this.component).add(jp);
        }
    }

    private void initInteraction() {
        this.btnTestFileNamePattern.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                ScopeOptionsController.this.openPathPatternSandbox();
            }
        });
        this.btnEditIgnoreList.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                IgnoreListPanel.openDialog(ScopeOptionsController.this.btnEditIgnoreList);
            }
        });
        if (!this.replacing) {
            this.chkArchives.addItemListener(this.checkBoxListener);
            this.chkGenerated.addItemListener(this.checkBoxListener);
        }
        this.chkUseIgnoreList.addItemListener(this.checkBoxListener);
        if (this.fileNameComboBox != null) {
            this.chkFileNameRegex.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    ScopeOptionsController.this.fileNameComboBox.setRegularExpression(ScopeOptionsController.this.chkFileNameRegex.isSelected());
                }
            });
        } else {
            this.chkFileNameRegex.addItemListener(this.checkBoxListener);
        }
    }

    private void openPathPatternSandbox() {
        PatternSandbox.openDialog(new PatternSandbox.PathPatternSandbox(((JComboBox)this.fileNameComboBox.getComponent()).getSelectedItem() == null ? "" : this.fileNameComboBox.getFileNamePattern()){

            @Override
            protected void onApply(String pattern) {
                if (pattern.isEmpty()) {
                    if (!ScopeOptionsController.this.fileNameComboBox.isAllFilesInfoDisplayed()) {
                        ((JComboBox)ScopeOptionsController.this.fileNameComboBox.getComponent()).setSelectedItem(pattern);
                        ScopeOptionsController.this.fileNameComboBox.displayAllFilesInfo();
                    }
                } else {
                    if (ScopeOptionsController.this.fileNameComboBox.isAllFilesInfoDisplayed()) {
                        ScopeOptionsController.this.fileNameComboBox.hideAllFilesInfo();
                    }
                    ((JComboBox)ScopeOptionsController.this.fileNameComboBox.getComponent()).setSelectedItem(pattern);
                }
            }
        }, this.btnTestFileNamePattern);
    }

    private void setMnemonics() {
        UiUtils.lclz(this.chkFileNameRegex, "BasicSearchForm.chkFileNameRegex.text");
        this.btnTestFileNamePattern.setText(UiUtils.getHtmlLink("BasicSearchForm.btnTestFileNamePattern.text"));
        this.btnEditIgnoreList.setText(UiUtils.getHtmlLink("BasicSearchForm.btnEditIgnoreList.text"));
        UiUtils.lclz(this.chkUseIgnoreList, "BasicSearchForm.chkUseIgnoreList.text");
        if (!this.replacing) {
            UiUtils.lclz(this.chkArchives, "BasicSearchForm.chkArchives.text");
            UiUtils.lclz(this.chkGenerated, "BasicSearchForm.chkGenerated.text");
        }
    }

    public boolean isSearchInArchives() {
        return this.isOn(this.chkArchives);
    }

    public boolean isSearchInGenerated() {
        return this.isOn(this.chkGenerated);
    }

    public boolean isUseIgnoreList() {
        return this.isOn(this.chkUseIgnoreList);
    }

    public boolean isFileNameRegExp() {
        return this.isOn(this.chkFileNameRegex);
    }

    private boolean isOn(JCheckBox chbox) {
        return chbox != null && chbox.isEnabled() && chbox.isSelected();
    }

    public void setSearchInArchives(boolean searchInArchives) {
        if (this.chkArchives == null) {
            if (searchInArchives) {
                throw new IllegalArgumentException("Searching in archives not allowed when replacing");
            }
        } else {
            this.chkArchives.setSelected(searchInArchives);
        }
    }

    public void setSearchInGenerated(boolean searchInGenerated) {
        if (this.chkGenerated == null) {
            if (searchInGenerated) {
                throw new IllegalArgumentException("Searching in generated sources not allowed when replacing");
            }
        } else {
            this.chkGenerated.setSelected(searchInGenerated);
        }
    }

    public void setUseIgnoreList(boolean useIgnoreList) {
        this.chkUseIgnoreList.setSelected(useIgnoreList);
    }

    public void setFileNameRegexp(boolean fileNameRegexp) {
        this.chkFileNameRegex.setSelected(fileNameRegexp);
    }

    @NullUnknown
    public JPanel getFileNameComponent() {
        return this.fileNameComponent;
    }

    private final class CheckBoxListener
    implements ItemListener {
        private CheckBoxListener() {
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            ScopeOptionsController.this.fireChange();
        }
    }

}

