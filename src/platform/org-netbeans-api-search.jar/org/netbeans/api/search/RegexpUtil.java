/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 */
package org.netbeans.api.search;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.search.SearchScopeOptions;
import org.openide.util.Parameters;

public final class RegexpUtil {
    private RegexpUtil() {
    }

    private static String makeMultiRegexp(String simplePatternList) {
        if (simplePatternList.length() == 0) {
            return simplePatternList;
        }
        if (Pattern.matches("[a-zA-Z0-9]*", simplePatternList)) {
            return simplePatternList;
        }
        StringBuilder buf = new StringBuilder(simplePatternList.length() + 16);
        boolean lastWasSeparator = false;
        boolean quoted = false;
        boolean starPresent = false;
        for (char c : simplePatternList.toCharArray()) {
            if (quoted) {
                if (c == 'n' || RegexpUtil.isSpecialCharacter(c)) {
                    buf.append('\\');
                }
                buf.append(c);
                quoted = false;
                continue;
            }
            if (c == ',' || c == ' ') {
                if (starPresent) {
                    buf.append('.').append('*');
                    starPresent = false;
                }
                lastWasSeparator = true;
                continue;
            }
            if (lastWasSeparator && buf.length() != 0) {
                buf.append('|');
            }
            if (c == '?') {
                buf.append('.');
            } else if (c == '*') {
                starPresent = true;
            } else {
                if (starPresent) {
                    buf.append('.').append('*');
                    starPresent = false;
                }
                if (c == '\\') {
                    quoted = true;
                } else {
                    if (RegexpUtil.isSpecialCharacter(c)) {
                        buf.append('\\');
                    }
                    buf.append(c);
                }
            }
            lastWasSeparator = false;
        }
        if (quoted) {
            buf.append('\\').append('\\');
        } else if (starPresent) {
            buf.append('.').append('*');
        }
        return buf.toString();
    }

    private static Pattern compileSimpleFileNamePattern(String expr) throws PatternSyntaxException {
        assert (expr != null);
        return Pattern.compile(RegexpUtil.makeMultiRegexp(expr), 2);
    }

    private static Pattern compileRegexpFileNamePattern(String expr) throws PatternSyntaxException {
        assert (expr != null);
        return Pattern.compile(expr, 2);
    }

    private static boolean isSpecialCharacter(char c) {
        return c > ' ' && c < '' && !RegexpUtil.isAlnum(c);
    }

    private static boolean isAlnum(char c) {
        return RegexpUtil.isAlpha(c) || RegexpUtil.isDigit(c);
    }

    private static boolean isAlpha(char c) {
        return (c = (char)(c | 32)) >= 'a' && c <= 'z';
    }

    private static boolean isDigit(char c) {
        return c >= '0' && c <= '9';
    }

    public static Pattern makeFileNamePattern(@NonNull SearchScopeOptions searchScopeOptions) throws PatternSyntaxException {
        Parameters.notNull((CharSequence)"searchScopeOptions", (Object)searchScopeOptions);
        if (searchScopeOptions.isRegexp()) {
            return RegexpUtil.compileRegexpFileNamePattern(searchScopeOptions.getPattern());
        }
        return RegexpUtil.compileSimpleFileNamePattern(searchScopeOptions.getPattern());
    }
}

