/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.annotations.common.proc;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.Set;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.tools.Diagnostic;
import javax.tools.FileObject;
import javax.tools.JavaFileManager;
import javax.tools.StandardLocation;
import org.netbeans.api.annotations.common.StaticResource;

@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
public class StaticResourceProcessor
extends AbstractProcessor {
    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return Collections.singleton(StaticResource.class.getCanonicalName());
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if (!roundEnv.processingOver()) {
            for (Element e : roundEnv.getElementsAnnotatedWith(StaticResource.class)) {
                StaticResource sr = e.getAnnotation(StaticResource.class);
                if (sr == null) continue;
                Object v = ((VariableElement)e).getConstantValue();
                if (!(v instanceof String)) {
                    this.processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "@StaticResource may only be used on a String constant", e);
                    continue;
                }
                String resource = (String)v;
                if (sr.relative()) {
                    try {
                        resource = new URI(null, StaticResourceProcessor.findPackage(e).replace('.', '/') + "/", null).resolve(new URI(null, resource, null)).getPath();
                    }
                    catch (URISyntaxException x) {
                        this.processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, x.getMessage(), e);
                        continue;
                    }
                }
                if (resource.startsWith("/")) {
                    this.processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "do not use leading slashes on resource paths", e);
                    continue;
                }
                if (sr.searchClasspath()) {
                    boolean ok = false;
                    for (JavaFileManager.Location loc : new JavaFileManager.Location[]{StandardLocation.SOURCE_PATH, StandardLocation.CLASS_OUTPUT, StandardLocation.CLASS_PATH, StandardLocation.PLATFORM_CLASS_PATH}) {
                        try {
                            this.processingEnv.getFiler().getResource(loc, "", resource);
                            ok = true;
                            continue;
                        }
                        catch (IOException ex) {
                            // empty catch block
                        }
                    }
                    if (ok) continue;
                    this.processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "cannot find resource " + resource, e);
                    continue;
                }
                try {
                    try {
                        this.processingEnv.getFiler().getResource(StandardLocation.SOURCE_PATH, "", resource).openInputStream().close();
                        continue;
                    }
                    catch (FileNotFoundException x) {
                        this.processingEnv.getFiler().getResource(StandardLocation.CLASS_OUTPUT, "", resource).openInputStream().close();
                    }
                }
                catch (IOException x) {
                    this.processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "cannot find resource " + resource, e);
                }
            }
        }
        return true;
    }

    private static String findPackage(Element e) {
        switch (e.getKind()) {
            case PACKAGE: {
                return ((PackageElement)e).getQualifiedName().toString();
            }
        }
        return StaticResourceProcessor.findPackage(e.getEnclosingElement());
    }

}

