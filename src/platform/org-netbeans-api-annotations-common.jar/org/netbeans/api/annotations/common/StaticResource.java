/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.annotations.common;

import java.lang.annotation.Annotation;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value={ElementType.FIELD})
@Retention(value=RetentionPolicy.SOURCE)
@Documented
public @interface StaticResource {
    public boolean searchClasspath() default 0;

    public boolean relative() default 0;
}

