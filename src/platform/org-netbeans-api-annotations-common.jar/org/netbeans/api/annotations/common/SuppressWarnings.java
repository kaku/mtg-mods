/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.annotations.common;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value={ElementType.CONSTRUCTOR, ElementType.FIELD, ElementType.LOCAL_VARIABLE, ElementType.METHOD, ElementType.PACKAGE, ElementType.PARAMETER, ElementType.TYPE})
@Retention(value=RetentionPolicy.CLASS)
public @interface SuppressWarnings {
    public String[] value() default {};

    public String justification() default "";
}

