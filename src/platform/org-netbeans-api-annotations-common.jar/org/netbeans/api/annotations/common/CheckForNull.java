/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.annotations.common;

import java.lang.annotation.Annotation;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.annotation.Nonnull;
import javax.annotation.meta.TypeQualifierNickname;
import javax.annotation.meta.When;

@Documented
@Target(value={ElementType.METHOD})
@Retention(value=RetentionPolicy.CLASS)
@Nonnull(when=When.MAYBE)
@TypeQualifierNickname
public @interface CheckForNull {
}

