/*
 * Decompiled with CFR 0_118.
 */
package javax.annotation.meta;

public enum When {
    ALWAYS,
    UNKNOWN,
    MAYBE,
    NEVER;
    

    private When() {
    }
}

