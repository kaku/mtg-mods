/*
 * Decompiled with CFR 0_118.
 */
package javax.annotation;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import javax.annotation.meta.TypeQualifier;
import javax.annotation.meta.When;

@Retention(value=RetentionPolicy.CLASS)
@TypeQualifier
public @interface Nonnull {
    public When when() default When.ALWAYS;
}

