/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.AssignmentTree
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreePathScanner
 *  com.sun.source.util.Trees
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.java.source.CodeStyle
 *  org.netbeans.api.java.source.CodeStyle$InsertionPoint
 *  org.netbeans.api.java.source.CodeStyleUtils
 *  org.netbeans.api.java.source.Comment
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementUtilities
 *  org.netbeans.api.java.source.GeneratorUtilities
 *  org.netbeans.api.java.source.ModificationResult
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.api.java.source.TreeMaker
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.WorkingCopy
 *  org.netbeans.editor.GuardedDocument
 *  org.netbeans.editor.GuardedException
 *  org.netbeans.editor.MarkBlockChain
 *  org.netbeans.editor.Utilities
 *  org.openide.DialogDescriptor
 *  org.openide.ErrorManager
 *  org.openide.filesystems.FileObject
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.codegen;

import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreePathScanner;
import com.sun.source.util.Trees;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.CodeStyle;
import org.netbeans.api.java.source.CodeStyleUtils;
import org.netbeans.api.java.source.Comment;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.java.source.GeneratorUtilities;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.TreeMaker;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.editor.GuardedDocument;
import org.netbeans.editor.GuardedException;
import org.netbeans.editor.MarkBlockChain;
import org.netbeans.editor.Utilities;
import org.openide.DialogDescriptor;
import org.openide.ErrorManager;
import org.openide.filesystems.FileObject;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

public class GeneratorUtils {
    private static final ErrorManager ERR = ErrorManager.getDefault().getInstance(GeneratorUtils.class.getName());
    private static final String ERROR = "<error>";
    public static final int GETTERS_ONLY = 1;
    public static final int SETTERS_ONLY = 2;
    private static final Set<Modifier> NOT_OVERRIDABLE = EnumSet.of(Modifier.STATIC, Modifier.FINAL, Modifier.PRIVATE);

    private GeneratorUtils() {
    }

    public static List<? extends ExecutableElement> findUndefs(CompilationInfo info, TypeElement impl) {
        if (ERR.isLoggable(1)) {
            ERR.log(1, "findUndefs(" + (Object)info + ", " + impl + ")");
        }
        List undef = info.getElementUtilities().findUnimplementedMethods(impl);
        if (ERR.isLoggable(1)) {
            ERR.log(1, "undef=" + undef);
        }
        return undef;
    }

    public static List<? extends ExecutableElement> findOverridable(CompilationInfo info, TypeElement impl) {
        if (ERR.isLoggable(1)) {
            ERR.log(1, "findOverridable(" + (Object)info + ", " + impl + ")");
        }
        ArrayList<ExecutableElement> overridable = new ArrayList<ExecutableElement>();
        EnumSet<Modifier> notOverridable = EnumSet.copyOf(NOT_OVERRIDABLE);
        if (!impl.getModifiers().contains((Object)Modifier.ABSTRACT)) {
            notOverridable.add(Modifier.ABSTRACT);
        }
        for (ExecutableElement ee : ElementFilter.methodsIn(info.getElements().getAllMembers(impl))) {
            EnumSet<Modifier> set = EnumSet.copyOf(notOverridable);
            set.removeAll(ee.getModifiers());
            if (set.size() != notOverridable.size() || GeneratorUtils.overridesPackagePrivateOutsidePackage(ee, impl) || GeneratorUtils.isOverridden(info, ee, impl)) continue;
            overridable.add(ee);
        }
        Collections.reverse(overridable);
        if (ERR.isLoggable(1)) {
            ERR.log(1, "overridable=" + overridable);
        }
        return overridable;
    }

    public static Map<? extends TypeElement, ? extends List<? extends VariableElement>> findAllAccessibleFields(CompilationInfo info, TypeElement clazz) {
        HashMap<TypeElement, List<? extends VariableElement>> result = new HashMap<TypeElement, List<? extends VariableElement>>();
        result.put(clazz, GeneratorUtils.findAllAccessibleFields(info, clazz, clazz));
        for (TypeElement te : GeneratorUtils.getAllParents(clazz)) {
            result.put(te, GeneratorUtils.findAllAccessibleFields(info, clazz, te));
        }
        return result;
    }

    public static void scanForFieldsAndConstructors(CompilationInfo info, TreePath clsPath, final Set<VariableElement> initializedFields, final Set<VariableElement> uninitializedFields, final List<ExecutableElement> constructors) {
        final Trees trees = info.getTrees();
        new TreePathScanner<Void, Boolean>(){

            public Void visitVariable(VariableTree node, Boolean p) {
                if ("<error>".contentEquals(node.getName())) {
                    return null;
                }
                Element el = trees.getElement(this.getCurrentPath());
                if (el != null && el.getKind() == ElementKind.FIELD && !el.getModifiers().contains((Object)Modifier.STATIC) && node.getInitializer() == null && !initializedFields.remove(el)) {
                    uninitializedFields.add((VariableElement)el);
                }
                return null;
            }

            public Void visitAssignment(AssignmentTree node, Boolean p) {
                Element el = trees.getElement(new TreePath(this.getCurrentPath(), (Tree)node.getVariable()));
                if (el != null && el.getKind() == ElementKind.FIELD && !uninitializedFields.remove(el)) {
                    initializedFields.add((VariableElement)el);
                }
                return null;
            }

            public Void visitClass(ClassTree node, Boolean p) {
                return p != false ? (Void)TreePathScanner.super.visitClass(node, (Object)false) : null;
            }

            public Void visitMethod(MethodTree node, Boolean p) {
                Element el = trees.getElement(this.getCurrentPath());
                if (el != null && el.getKind() == ElementKind.CONSTRUCTOR) {
                    constructors.add((ExecutableElement)el);
                }
                return null;
            }
        }.scan(clsPath, (Object)Boolean.TRUE);
    }

    public static void generateAllAbstractMethodImplementations(WorkingCopy wc, TreePath path) {
        assert (TreeUtilities.CLASS_TREE_KINDS.contains((Object)path.getLeaf().getKind()));
        TypeElement te = (TypeElement)wc.getTrees().getElement(path);
        if (te != null) {
            ClassTree clazz = (ClassTree)path.getLeaf();
            GeneratorUtilities gu = GeneratorUtilities.get((WorkingCopy)wc);
            ElementUtilities elemUtils = wc.getElementUtilities();
            clazz = gu.insertClassMembers(clazz, (Iterable)gu.createAbstractMethodImplementations(te, (Iterable)elemUtils.findUnimplementedMethods(te)));
            wc.rewrite(path.getLeaf(), (Tree)clazz);
        }
    }

    public static void generateAbstractMethodImplementations(WorkingCopy wc, TreePath path, List<? extends ExecutableElement> elements, int offset) {
        assert (TreeUtilities.CLASS_TREE_KINDS.contains((Object)path.getLeaf().getKind()));
        TypeElement te = (TypeElement)wc.getTrees().getElement(path);
        if (te != null) {
            ClassTree clazz = (ClassTree)path.getLeaf();
            wc.rewrite((Tree)clazz, (Tree)GeneratorUtils.insertClassMembers(wc, clazz, GeneratorUtilities.get((WorkingCopy)wc).createAbstractMethodImplementations(te, elements), offset));
        }
    }

    public static void generateAbstractMethodImplementation(WorkingCopy wc, TreePath path, ExecutableElement element, int offset) {
        assert (TreeUtilities.CLASS_TREE_KINDS.contains((Object)path.getLeaf().getKind()));
        TypeElement te = (TypeElement)wc.getTrees().getElement(path);
        if (te != null) {
            ClassTree clazz = (ClassTree)path.getLeaf();
            wc.rewrite((Tree)clazz, (Tree)GeneratorUtils.insertClassMember(wc, clazz, (Tree)GeneratorUtilities.get((WorkingCopy)wc).createAbstractMethodImplementation(te, element), offset));
        }
    }

    public static void generateMethodOverrides(WorkingCopy wc, TreePath path, List<? extends ExecutableElement> elements, int offset) {
        assert (TreeUtilities.CLASS_TREE_KINDS.contains((Object)path.getLeaf().getKind()));
        TypeElement te = (TypeElement)wc.getTrees().getElement(path);
        if (te != null) {
            ClassTree clazz = (ClassTree)path.getLeaf();
            wc.rewrite((Tree)clazz, (Tree)GeneratorUtils.insertClassMembers(wc, clazz, GeneratorUtilities.get((WorkingCopy)wc).createOverridingMethods(te, elements), offset));
        }
    }

    public static void generateMethodOverride(WorkingCopy wc, TreePath path, ExecutableElement element, int offset) {
        assert (TreeUtilities.CLASS_TREE_KINDS.contains((Object)path.getLeaf().getKind()));
        TypeElement te = (TypeElement)wc.getTrees().getElement(path);
        if (te != null) {
            ClassTree clazz = (ClassTree)path.getLeaf();
            wc.rewrite((Tree)clazz, (Tree)GeneratorUtils.insertClassMember(wc, clazz, (Tree)GeneratorUtilities.get((WorkingCopy)wc).createOverridingMethod(te, element), offset));
        }
    }

    public static void generateConstructor(WorkingCopy wc, TreePath path, Iterable<? extends VariableElement> initFields, ExecutableElement inheritedConstructor, int offset) {
        ClassTree clazz = (ClassTree)path.getLeaf();
        TypeElement te = (TypeElement)wc.getTrees().getElement(path);
        Tree c2 = wc.resolveRewriteTarget((Tree)clazz);
        if (c2 instanceof ClassTree && clazz != c2) {
            clazz = (ClassTree)c2;
        }
        wc.rewrite((Tree)clazz, (Tree)GeneratorUtils.insertClassMembers(wc, clazz, Collections.singletonList(GeneratorUtilities.get((WorkingCopy)wc).createConstructor(te, initFields, inheritedConstructor)), offset));
    }

    public static void generateConstructors(WorkingCopy wc, TreePath path, Iterable<? extends VariableElement> initFields, List<? extends ExecutableElement> inheritedConstructors, int offset) {
        ClassTree clazz = (ClassTree)path.getLeaf();
        TypeElement te = (TypeElement)wc.getTrees().getElement(path);
        GeneratorUtilities gu = GeneratorUtilities.get((WorkingCopy)wc);
        ArrayList<MethodTree> members = new ArrayList<MethodTree>();
        for (ExecutableElement inheritedConstructor : inheritedConstructors) {
            members.add(gu.createConstructor(te, initFields, inheritedConstructor));
        }
        wc.rewrite((Tree)clazz, (Tree)GeneratorUtils.insertClassMembers(wc, clazz, members, offset));
    }

    public static void generateGettersAndSetters(WorkingCopy wc, TreePath path, Iterable<? extends VariableElement> fields, int type, int offset) {
        assert (TreeUtilities.CLASS_TREE_KINDS.contains((Object)path.getLeaf().getKind()));
        TypeElement te = (TypeElement)wc.getTrees().getElement(path);
        if (te != null) {
            GeneratorUtilities gu = GeneratorUtilities.get((WorkingCopy)wc);
            ClassTree clazz = (ClassTree)path.getLeaf();
            ArrayList<MethodTree> members = new ArrayList<MethodTree>();
            for (VariableElement element : fields) {
                if (type != 2) {
                    members.add(gu.createGetter(te, element));
                }
                if (type == 1) continue;
                members.add(gu.createSetter(te, element));
            }
            wc.rewrite((Tree)clazz, (Tree)GeneratorUtils.insertClassMembers(wc, clazz, members, offset));
        }
    }

    public static boolean hasGetter(CompilationInfo info, TypeElement typeElement, VariableElement field, Map<String, List<ExecutableElement>> methods, CodeStyle cs) {
        Name name = field.getSimpleName();
        assert (name.length() > 0);
        TypeMirror type = field.asType();
        boolean isStatic = field.getModifiers().contains((Object)Modifier.STATIC);
        String getterName = CodeStyleUtils.computeGetterName((CharSequence)name, (boolean)org.netbeans.modules.editor.java.Utilities.isBoolean(type), (boolean)isStatic, (CodeStyle)cs);
        Types types = info.getTypes();
        List<ExecutableElement> candidates = methods.get(getterName);
        if (candidates != null) {
            for (ExecutableElement candidate : candidates) {
                if (candidate.getModifiers().contains((Object)Modifier.ABSTRACT) && candidate.getEnclosingElement() != typeElement || !candidate.getParameters().isEmpty() || !types.isSameType(candidate.getReturnType(), type)) continue;
                return true;
            }
        }
        return false;
    }

    public static boolean hasSetter(CompilationInfo info, TypeElement typeElement, VariableElement field, Map<String, List<ExecutableElement>> methods, CodeStyle cs) {
        Name name = field.getSimpleName();
        assert (name.length() > 0);
        TypeMirror type = field.asType();
        boolean isStatic = field.getModifiers().contains((Object)Modifier.STATIC);
        String setterName = CodeStyleUtils.computeSetterName((CharSequence)name, (boolean)isStatic, (CodeStyle)cs);
        Types types = info.getTypes();
        List<ExecutableElement> candidates = methods.get(setterName);
        if (candidates != null) {
            for (ExecutableElement candidate : candidates) {
                if (candidate.getModifiers().contains((Object)Modifier.ABSTRACT) && candidate.getEnclosingElement() != typeElement || candidate.getReturnType().getKind() != TypeKind.VOID || candidate.getParameters().size() != 1 || !types.isSameType(candidate.getParameters().get(0).asType(), type)) continue;
                return true;
            }
        }
        return false;
    }

    public static ClassTree insertClassMembers(WorkingCopy wc, ClassTree clazz, List<? extends Tree> members, int offset) throws IllegalStateException {
        if (members.isEmpty()) {
            return clazz;
        }
        if (offset < 0 || GeneratorUtils.getCodeStyle((CompilationInfo)wc).getClassMemberInsertionPoint() != CodeStyle.InsertionPoint.CARET_LOCATION) {
            return GeneratorUtilities.get((WorkingCopy)wc).insertClassMembers(clazz, members);
        }
        int index = 0;
        SourcePositions sp = wc.getTrees().getSourcePositions();
        GuardedDocument gdoc = null;
        try {
            Document doc = wc.getDocument();
            if (doc != null && doc instanceof GuardedDocument) {
                gdoc = (GuardedDocument)doc;
            }
        }
        catch (IOException ioe) {
            // empty catch block
        }
        Tree lastMember = null;
        Tree nextMember = null;
        for (Tree tree : clazz.getMembers()) {
            if ((long)offset <= sp.getStartPosition(wc.getCompilationUnit(), tree)) {
                if (gdoc == null) {
                    nextMember = tree;
                    break;
                }
                int pos = (int)(lastMember != null ? sp.getEndPosition(wc.getCompilationUnit(), lastMember) : sp.getStartPosition(wc.getCompilationUnit(), (Tree)clazz));
                pos = gdoc.getGuardedBlockChain().adjustToBlockEnd(pos);
                if ((long)pos <= sp.getStartPosition(wc.getCompilationUnit(), tree)) {
                    nextMember = tree;
                    break;
                }
            }
            ++index;
            lastMember = tree;
        }
        if (lastMember != null) {
            GeneratorUtils.moveCommentsAfterOffset(wc, lastMember, members.get(0), offset, gdoc);
        }
        if (nextMember != null) {
            GeneratorUtils.moveCommentsBeforeOffset(wc, nextMember, members.get(members.size() - 1), offset, gdoc);
        }
        TreeMaker tm = wc.getTreeMaker();
        for (int i = members.size() - 1; i >= 0; --i) {
            clazz = tm.insertClassMember(clazz, index, members.get(i));
        }
        return clazz;
    }

    public static ClassTree insertClassMember(WorkingCopy wc, ClassTree clazz, Tree member, int offset) throws IllegalStateException {
        int idx = 0;
        Tree prev = null;
        Tree next = null;
        GuardedDocument gdoc = null;
        try {
            Document doc = wc.getDocument();
            if (doc != null && doc instanceof GuardedDocument) {
                gdoc = (GuardedDocument)doc;
            }
        }
        catch (IOException ioe) {
            // empty catch block
        }
        for (Tree tree : clazz.getMembers()) {
            if (wc.getTrees().getSourcePositions().getStartPosition(wc.getCompilationUnit(), tree) < (long)offset) {
                prev = tree;
                ++idx;
                continue;
            }
            next = tree;
            break;
        }
        if (prev != null) {
            GeneratorUtils.moveCommentsAfterOffset(wc, prev, member, offset, gdoc);
        }
        if (next != null) {
            GeneratorUtils.moveCommentsBeforeOffset(wc, next, member, offset, gdoc);
        }
        return wc.getTreeMaker().insertClassMember(clazz, idx, member);
    }

    private static void moveCommentsAfterOffset(WorkingCopy wc, Tree from, Tree to, int offset, GuardedDocument gdoc) {
        LinkedList<Comment> toMove = new LinkedList<Comment>();
        int idx = 0;
        int firstToRemove = -1;
        for (Comment comment : wc.getTreeUtilities().getComments(from, false)) {
            int epAfterBlock;
            if (comment.endPos() <= offset) {
                ++idx;
                continue;
            }
            if (gdoc != null && (epAfterBlock = gdoc.getGuardedBlockChain().adjustToBlockEnd(comment.endPos())) >= comment.endPos()) {
                ++idx;
                continue;
            }
            toMove.add(comment);
            if (firstToRemove == -1) {
                firstToRemove = idx;
            }
            ++idx;
        }
        if (toMove.isEmpty()) {
            return;
        }
        GeneratorUtils.doMoveComments(wc, from, to, offset, toMove, firstToRemove, idx);
    }

    private static void doMoveComments(WorkingCopy wc, Tree from, Tree to, int offset, List<Comment> comments, int fromIdx, int toIdx) {
        boolean before;
        if (comments.isEmpty()) {
            return;
        }
        TreeMaker tm = wc.getTreeMaker();
        Tree tree = from;
        switch (from.getKind()) {
            case METHOD: {
                tree = tm.setLabel(from, (CharSequence)((MethodTree)from).getName());
                break;
            }
            case VARIABLE: {
                tree = tm.setLabel(from, (CharSequence)((VariableTree)from).getName());
                break;
            }
            case BLOCK: {
                tree = tm.Block(((BlockTree)from).getStatements(), ((BlockTree)from).isStatic());
                GeneratorUtilities gu = GeneratorUtilities.get((WorkingCopy)wc);
                gu.copyComments(from, tree, true);
                gu.copyComments(from, tree, false);
            }
        }
        boolean bl = before = (int)wc.getTrees().getSourcePositions().getStartPosition(wc.getCompilationUnit(), from) >= offset;
        if (fromIdx >= 0 && toIdx >= 0 && toIdx - fromIdx > 0) {
            for (int i = toIdx - 1; i >= fromIdx; --i) {
                tm.removeComment(tree, i, before);
            }
        }
        wc.rewrite(from, tree);
        for (Comment comment : comments) {
            tm.addComment(to, comment, comment.pos() <= offset);
        }
    }

    private static void moveCommentsBeforeOffset(WorkingCopy wc, Tree from, Tree to, int offset, GuardedDocument gdoc) {
        Comment comment;
        int epAfterBlock;
        LinkedList<Comment> toMove = new LinkedList<Comment>();
        int idx = 0;
        Iterator i$ = wc.getTreeUtilities().getComments(from, true).iterator();
        while (i$.hasNext() && (comment = (Comment)i$.next()).pos() < offset && comment.endPos() <= offset && (gdoc == null || (epAfterBlock = gdoc.getGuardedBlockChain().adjustToBlockEnd(comment.pos())) < comment.endPos())) {
            toMove.add(comment);
            ++idx;
        }
        if (toMove.size() > 0) {
            GeneratorUtils.doMoveComments(wc, from, to, offset, toMove, 0, idx);
        }
    }

    private static CodeStyle getCodeStyle(CompilationInfo info) {
        if (info != null) {
            FileObject file;
            try {
                Document doc = info.getDocument();
                if (doc != null) {
                    CodeStyle cs = (CodeStyle)doc.getProperty(CodeStyle.class);
                    return cs != null ? cs : CodeStyle.getDefault((Document)doc);
                }
            }
            catch (IOException ioe) {
                // empty catch block
            }
            if ((file = info.getFileObject()) != null) {
                return CodeStyle.getDefault((FileObject)file);
            }
        }
        return CodeStyle.getDefault((Document)null);
    }

    private static List<? extends VariableElement> findAllAccessibleFields(CompilationInfo info, TypeElement accessibleFrom, TypeElement toScan) {
        ArrayList<VariableElement> result = new ArrayList<VariableElement>();
        for (VariableElement ve : ElementFilter.fieldsIn(toScan.getEnclosedElements())) {
            if (ve.getModifiers().contains((Object)Modifier.PUBLIC)) {
                result.add(ve);
                continue;
            }
            if (ve.getModifiers().contains((Object)Modifier.PRIVATE)) {
                if (accessibleFrom != toScan) continue;
                result.add(ve);
                continue;
            }
            if (!ve.getModifiers().contains((Object)Modifier.PROTECTED) || !GeneratorUtils.getAllParents(accessibleFrom).contains(toScan)) continue;
            result.add(ve);
        }
        return result;
    }

    public static Collection<TypeElement> getAllParents(TypeElement of) {
        TypeElement te;
        HashSet<TypeElement> result = new HashSet<TypeElement>();
        for (TypeMirror t : of.getInterfaces()) {
            TypeElement te2 = (TypeElement)((DeclaredType)t).asElement();
            if (te2 != null) {
                result.add(te2);
                result.addAll(GeneratorUtils.getAllParents(te2));
                continue;
            }
            if (!ERR.isLoggable(1)) continue;
            ERR.log(1, "te=null, t=" + t);
        }
        TypeMirror sup = of.getSuperclass();
        TypeElement typeElement = te = sup.getKind() == TypeKind.DECLARED ? (TypeElement)((DeclaredType)sup).asElement() : null;
        if (te != null) {
            result.add(te);
            result.addAll(GeneratorUtils.getAllParents(te));
        } else if (ERR.isLoggable(1)) {
            ERR.log(1, "te=null, t=" + of);
        }
        return result;
    }

    public static boolean supportsOverride(@NonNull CompilationInfo info) {
        return SourceVersion.RELEASE_5.compareTo(info.getSourceVersion()) <= 0 && info.getElements().getTypeElement("java.lang.Override") != null;
    }

    private static List<TypeElement> getAllClasses(TypeElement of) {
        ArrayList<TypeElement> result = new ArrayList<TypeElement>();
        TypeMirror sup = of.getSuperclass();
        TypeElement te = sup.getKind() == TypeKind.DECLARED ? (TypeElement)((DeclaredType)sup).asElement() : null;
        result.add(of);
        if (te != null) {
            result.addAll(GeneratorUtils.getAllClasses(te));
        } else if (ERR.isLoggable(1)) {
            ERR.log(1, "te=null, t=" + of);
        }
        return result;
    }

    private static boolean isOverridden(CompilationInfo info, ExecutableElement methodBase, TypeElement origin) {
        Element impl;
        if (ERR.isLoggable(1)) {
            ERR.log(1, "isOverridden(" + (Object)info + ", " + methodBase + ", " + origin + ")");
        }
        if ((impl = info.getElementUtilities().getImplementationOf(methodBase, origin)) == null || impl == methodBase && origin != methodBase.getEnclosingElement()) {
            if (ERR.isLoggable(1)) {
                ERR.log(1, "no overriding methods");
            }
            return false;
        }
        if (ERR.isLoggable(1)) {
            ERR.log(1, "overrides:");
            ERR.log(1, "impl=" + impl.getEnclosingElement());
            ERR.log(1, "methodImpl=" + impl);
        }
        return true;
    }

    public static boolean isAccessible(TypeElement from, Element what) {
        TypeElement whatTopLevel;
        if (what.getModifiers().contains((Object)Modifier.PUBLIC)) {
            return true;
        }
        TypeElement fromTopLevel = SourceUtils.getOutermostEnclosingTypeElement((Element)from);
        if (fromTopLevel.equals(whatTopLevel = SourceUtils.getOutermostEnclosingTypeElement((Element)what))) {
            return true;
        }
        if (what.getModifiers().contains((Object)Modifier.PRIVATE)) {
            return false;
        }
        if (what.getModifiers().contains((Object)Modifier.PROTECTED) && GeneratorUtils.getAllClasses(fromTopLevel).contains(SourceUtils.getEnclosingTypeElement((Element)what))) {
            return true;
        }
        return ((PackageElement)fromTopLevel.getEnclosingElement()).getQualifiedName().toString().contentEquals(((PackageElement)whatTopLevel.getEnclosingElement()).getQualifiedName());
    }

    static DialogDescriptor createDialogDescriptor(JComponent content, String label) {
        Object[] buttons = new JButton[2];
        buttons[0] = new JButton(NbBundle.getMessage(GeneratorUtils.class, (String)"LBL_generate_button"));
        buttons[0].getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(GeneratorUtils.class, (String)"A11Y_Generate"));
        buttons[1] = new JButton(NbBundle.getMessage(GeneratorUtils.class, (String)"LBL_cancel_button"));
        return new DialogDescriptor((Object)content, label, true, buttons, (Object)buttons[0], 0, null, null);
    }

    private static boolean overridesPackagePrivateOutsidePackage(ExecutableElement ee, TypeElement impl) {
        String elemPackageName = GeneratorUtils.getPackageName(ee);
        String currentPackageName = GeneratorUtils.getPackageName(impl);
        if (!(ee.getModifiers().contains((Object)Modifier.PRIVATE) || ee.getModifiers().contains((Object)Modifier.PUBLIC) || ee.getModifiers().contains((Object)Modifier.PROTECTED) || currentPackageName.equals(elemPackageName))) {
            return true;
        }
        return false;
    }

    private static String getPackageName(Element e) {
        while (e.getEnclosingElement().getKind() != ElementKind.PACKAGE) {
            e = e.getEnclosingElement();
        }
        return ((PackageElement)e.getEnclosingElement()).getQualifiedName().toString();
    }

    public static void guardedCommit(JTextComponent component, ModificationResult mr) throws IOException {
        block2 : {
            try {
                mr.commit();
            }
            catch (IOException e) {
                if (!(e.getCause() instanceof GuardedException)) break block2;
                String message = NbBundle.getMessage(GeneratorUtils.class, (String)"ERR_CannotApplyGuarded");
                Utilities.setStatusBoldText((JTextComponent)component, (String)message);
                Logger.getLogger(GeneratorUtils.class.getName()).log(Level.FINE, null, e);
            }
        }
    }

}

