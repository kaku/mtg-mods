/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.completion.Completion
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.support.ReferencesCount
 *  org.netbeans.spi.editor.completion.CompletionItem
 *  org.netbeans.spi.editor.completion.CompletionTask
 *  org.netbeans.spi.editor.completion.support.CompletionUtilities
 *  org.openide.text.NbDocument
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 */
package org.netbeans.modules.java.editor.javadoc;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ExecutableType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.swing.ImageIcon;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyledDocument;
import org.netbeans.api.editor.completion.Completion;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.support.ReferencesCount;
import org.netbeans.modules.editor.java.JavaCompletionItem;
import org.netbeans.modules.editor.java.Utilities;
import org.netbeans.modules.java.editor.javadoc.TagRegistery;
import org.netbeans.spi.editor.completion.CompletionItem;
import org.netbeans.spi.editor.completion.CompletionTask;
import org.netbeans.spi.editor.completion.support.CompletionUtilities;
import org.openide.text.NbDocument;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;

final class JavadocCompletionItem
implements CompletionItem {
    private static final String JAVADOC_TAG_ICON = "org/netbeans/modules/java/editor/resources/javadoctag.gif";
    private static final String JAVADOC_PARAM_ICON = "org/netbeans/modules/editor/resources/completion/localVariable.gif";
    private static final String PARAMETER_COLOR = "<font color=#00007c>";
    private static final String COLOR_END = "</font>";
    private static final String BOLD = "<b>";
    private static final String BOLD_END = "</b>";
    private int substitutionOffset;
    private String txt;
    private String leftHtmlText;
    private String rightHtmlText;
    private final String iconPath;
    private final int sortPriority;
    private ImageIcon icon = null;

    public JavadocCompletionItem(String txt, int substitutionOffset, String leftHtmlText, String rightHtmlText, String iconPath, int sortPriority) {
        this.substitutionOffset = substitutionOffset;
        this.txt = txt;
        this.leftHtmlText = leftHtmlText;
        this.rightHtmlText = rightHtmlText;
        this.iconPath = iconPath;
        this.sortPriority = sortPriority;
    }

    public void defaultAction(JTextComponent component) {
        Completion.get().hideAll();
        JavadocCompletionItem.complete(component, this.txt + ' ', this.substitutionOffset);
    }

    public void processKeyEvent(KeyEvent evt) {
    }

    public final int getPreferredWidth(Graphics g, Font defaultFont) {
        return CompletionUtilities.getPreferredWidth((String)this.getLeftHtmlText(), (String)this.getRightHtmlText(), (Graphics)g, (Font)defaultFont);
    }

    public final void render(Graphics g, Font defaultFont, Color defaultColor, Color backgroundColor, int width, int height, boolean selected) {
        if (this.icon == null) {
            this.icon = this.createIcon();
        }
        CompletionUtilities.renderHtml((ImageIcon)this.icon, (String)this.getLeftHtmlText(), (String)this.getRightHtmlText(), (Graphics)g, (Font)defaultFont, (Color)defaultColor, (int)width, (int)height, (boolean)selected);
    }

    protected ImageIcon createIcon() {
        return ImageUtilities.loadImageIcon((String)this.iconPath, (boolean)false);
    }

    protected String getLeftHtmlText() {
        if (this.leftHtmlText == null) {
            this.leftHtmlText = this.txt;
        }
        return this.leftHtmlText;
    }

    protected String getRightHtmlText() {
        return this.rightHtmlText;
    }

    public CompletionTask createDocumentationTask() {
        return null;
    }

    public CompletionTask createToolTipTask() {
        return null;
    }

    public boolean instantSubstitution(JTextComponent component) {
        return false;
    }

    public int getSortPriority() {
        return this.sortPriority;
    }

    public CharSequence getSortText() {
        return this.txt;
    }

    public CharSequence getInsertPrefix() {
        return this.txt;
    }

    public String toString() {
        return super.toString() + String.format("[txt:%1$s, substitutionOffset:%2$d]", this.txt, this.substitutionOffset);
    }

    public static List<CompletionItem> addBlockTagItems(ElementKind kind, String prefix, int startOffset) {
        List<TagRegistery.TagEntry> tags = TagRegistery.getDefault().getTags(kind, false);
        ArrayList<CompletionItem> items = new ArrayList<CompletionItem>(tags.size());
        for (TagRegistery.TagEntry tagEntry : tags) {
            if (!tagEntry.name.startsWith(prefix)) continue;
            items.add(new JavadocCompletionItem(tagEntry.name, startOffset, null, null, "org/netbeans/modules/java/editor/resources/javadoctag.gif", 500));
        }
        return items;
    }

    public static List<CompletionItem> addInlineTagItems(ElementKind kind, String prefix, int startOffset) {
        List<TagRegistery.TagEntry> tags = TagRegistery.getDefault().getTags(kind, true);
        ArrayList<CompletionItem> items = new ArrayList<CompletionItem>(tags.size());
        for (TagRegistery.TagEntry tagEntry : tags) {
            if (!tagEntry.name.startsWith(prefix)) continue;
            items.add(new JavadocCompletionItem(tagEntry.name, startOffset, null, null, "org/netbeans/modules/java/editor/resources/javadoctag.gif", 500));
        }
        return items;
    }

    public static CompletionItem createNameItem(String name, int startOffset) {
        String html = name.charAt(0) == '<' ? "&lt;" + name.substring(1, name.length() - 1) + "&gt;" : name;
        return new JavadocCompletionItem(name, startOffset, "<font color=#00007c><b>" + html + "</b>" + "</font>", null, "org/netbeans/modules/editor/resources/completion/localVariable.gif", 100);
    }

    public static CompletionItem createExecutableItem(CompilationInfo info, ExecutableElement e, ExecutableType et, int startOffset, boolean isInherited, boolean isDeprecated) {
        JavaCompletionItem delegate = JavaCompletionItem.createExecutableItem(info, e, et, startOffset, null, isInherited, isDeprecated, false, false, false, -1, false, null);
        return new JavadocExecutableItem(delegate, e, startOffset);
    }

    public static CompletionItem createTypeItem(CompilationInfo info, TypeElement elem, int startOffset, ReferencesCount referencesCount, boolean isDeprecated) {
        JavaCompletionItem delegate = JavaCompletionItem.createTypeItem(info, elem, (DeclaredType)elem.asType(), startOffset, referencesCount, isDeprecated, false, false, false, false, false, null);
        return new JavadocTypeItem(delegate, startOffset);
    }

    private static void complete(final JTextComponent comp, final String what, final int where) {
        try {
            Document doc = comp.getDocument();
            NbDocument.runAtomicAsUser((StyledDocument)((StyledDocument)doc), (Runnable)new Runnable(){

                @Override
                public void run() {
                    JavadocCompletionItem.completeAsUser(comp, what, where);
                }
            });
        }
        catch (BadLocationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    private static void completeAsUser(JTextComponent comp, String what, int where) {
        Document doc = comp.getDocument();
        try {
            int end = comp.getSelectionEnd();
            int len = end - where;
            if (len > 0) {
                doc.remove(where, len);
            }
            doc.insertString(where, what, null);
            comp.setCaretPosition(where + what.length());
        }
        catch (BadLocationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    private static class JavadocTypeItem
    implements CompletionItem {
        private final CompletionItem delegate;

        public JavadocTypeItem(CompletionItem item, int substitutionOffset) {
            this.delegate = item;
        }

        public void defaultAction(JTextComponent component) {
            this.delegate.defaultAction(component);
        }

        public void processKeyEvent(KeyEvent evt) {
            if (evt.getID() == 400 && Utilities.getJavadocCompletionSelectors().indexOf(evt.getKeyChar()) >= 0) {
                JTextComponent comp = (JTextComponent)evt.getSource();
                this.delegate.defaultAction(comp);
                if (Utilities.getJavadocCompletionAutoPopupTriggers().indexOf(evt.getKeyChar()) >= 0) {
                    Completion.get().showCompletion();
                }
                evt.consume();
            }
        }

        public int getPreferredWidth(Graphics g, Font defaultFont) {
            return this.delegate.getPreferredWidth(g, defaultFont);
        }

        public void render(Graphics g, Font defaultFont, Color defaultColor, Color backgroundColor, int width, int height, boolean selected) {
            this.delegate.render(g, defaultFont, defaultColor, backgroundColor, width, height, selected);
        }

        public CompletionTask createDocumentationTask() {
            return this.delegate.createDocumentationTask();
        }

        public CompletionTask createToolTipTask() {
            return this.delegate.createToolTipTask();
        }

        public boolean instantSubstitution(JTextComponent component) {
            return this.delegate.instantSubstitution(component);
        }

        public int getSortPriority() {
            return this.delegate.getSortPriority();
        }

        public CharSequence getSortText() {
            return this.delegate.getSortText();
        }

        public CharSequence getInsertPrefix() {
            return this.delegate.getInsertPrefix();
        }

        public String toString() {
            return this.delegate.toString();
        }
    }

    private static class JavadocExecutableItem
    implements CompletionItem {
        private final CompletionItem delegate;
        private final String[] paramTypes;
        private final CharSequence name;
        private final int substitutionOffset;

        public JavadocExecutableItem(CompletionItem jmethod, ExecutableElement ee, int substitutionOffset) {
            this.delegate = jmethod;
            this.substitutionOffset = substitutionOffset;
            this.name = ee.getKind() == ElementKind.METHOD ? ee.getSimpleName() : ee.getEnclosingElement().getSimpleName();
            List<? extends VariableElement> params = ee.getParameters();
            this.paramTypes = new String[params.size()];
            int i = 0;
            for (VariableElement p : params) {
                TypeMirror asType = p.asType();
                this.paramTypes[i++] = this.resolveTypeName(asType, ee.isVarArgs()).toString();
            }
        }

        private CharSequence resolveTypeName(TypeMirror asType, boolean isVarArgs) {
            CharSequence ptype2;
            CharSequence ptype2;
            if (asType.getKind() == TypeKind.DECLARED) {
                Element e = ((DeclaredType)asType).asElement();
                ptype2 = e.getKind().isClass() || e.getKind().isInterface() ? ((TypeElement)e).getQualifiedName() : e.getSimpleName();
            } else if (asType.getKind() == TypeKind.TYPEVAR) {
                while ((asType = ((TypeVariable)asType).getUpperBound()).getKind() == TypeKind.TYPEVAR) {
                }
                ptype2 = this.resolveTypeName(asType, isVarArgs);
            } else {
                ptype2 = isVarArgs && asType.getKind() == TypeKind.ARRAY ? this.resolveTypeName(((ArrayType)asType).getComponentType(), false) + "..." : asType.toString();
            }
            return ptype2;
        }

        public void defaultAction(JTextComponent component) {
            if (component != null) {
                Completion.get().hideDocumentation();
                Completion.get().hideCompletion();
                StringBuilder sb = new StringBuilder();
                sb.append(this.name);
                if (this.paramTypes.length == 0) {
                    sb.append("() ");
                } else {
                    sb.append('(');
                    for (String pt : this.paramTypes) {
                        sb.append(pt).append(", ");
                    }
                    sb.setCharAt(sb.length() - 2, ')');
                }
                JavadocCompletionItem.complete(component, sb.toString(), this.substitutionOffset);
            }
        }

        public void processKeyEvent(KeyEvent evt) {
        }

        public int getPreferredWidth(Graphics g, Font defaultFont) {
            return this.delegate.getPreferredWidth(g, defaultFont);
        }

        public void render(Graphics g, Font defaultFont, Color defaultColor, Color backgroundColor, int width, int height, boolean selected) {
            this.delegate.render(g, defaultFont, defaultColor, backgroundColor, width, height, selected);
        }

        public CompletionTask createDocumentationTask() {
            return this.delegate.createDocumentationTask();
        }

        public CompletionTask createToolTipTask() {
            return this.delegate.createToolTipTask();
        }

        public boolean instantSubstitution(JTextComponent component) {
            if (component != null) {
                try {
                    int caretOffset = component.getSelectionEnd();
                    if (caretOffset > this.substitutionOffset) {
                        String text = component.getDocument().getText(this.substitutionOffset, caretOffset - this.substitutionOffset);
                        if (!this.getInsertPrefix().toString().startsWith(text)) {
                            return false;
                        }
                    }
                }
                catch (BadLocationException ble) {
                    // empty catch block
                }
            }
            this.defaultAction(component);
            return true;
        }

        public int getSortPriority() {
            return this.delegate.getSortPriority();
        }

        public CharSequence getSortText() {
            return this.delegate.getSortText();
        }

        public CharSequence getInsertPrefix() {
            return this.delegate.getInsertPrefix();
        }

        public String toString() {
            return this.delegate.toString();
        }
    }

}

