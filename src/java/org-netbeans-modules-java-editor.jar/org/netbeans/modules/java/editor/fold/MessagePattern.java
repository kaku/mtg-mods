/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.editor.fold;

import java.util.regex.Pattern;

public final class MessagePattern {
    public static final int BUNDLE_FROM_INSTANCE = -2;
    public static final int BUNDLE_FROM_CLASS = -4;
    public static final int GET_BUNDLE_CALL = -3;
    public static final int KEY_FROM_METHODNAME = -5;
    private Pattern ownerTypePattern;
    private Pattern methodNamePattern;
    private int bundleParam;
    private int keyParam;
    private String bundleFile = "Bundle";

    public MessagePattern(Pattern ownerTypePattern, Pattern methodNamePattern, int bundleParam, int keyParam) {
        assert (bundleParam >= 0 || bundleParam == -4 || bundleParam == -2);
        assert (keyParam >= 0 || keyParam == -5 || keyParam == -3);
        this.ownerTypePattern = ownerTypePattern;
        this.methodNamePattern = methodNamePattern;
        this.bundleParam = bundleParam;
        this.keyParam = keyParam;
    }

    public Pattern getOwnerTypePattern() {
        return this.ownerTypePattern;
    }

    public Pattern getMethodNamePattern() {
        return this.methodNamePattern;
    }

    public int getBundleParam() {
        return this.bundleParam;
    }

    public int getKeyParam() {
        return this.keyParam;
    }

    public String getBundleFile() {
        return this.bundleFile;
    }

    public void setBundleFile(String bundleFile) {
        this.bundleFile = bundleFile;
    }
}

