/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.progress.ProgressUtils
 *  org.netbeans.editor.BaseAction
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.imports;

import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.ElementFilter;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.editor.BaseAction;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.java.editor.imports.ComputeImports;
import org.netbeans.modules.java.editor.imports.ImportClassPanel;
import org.netbeans.modules.java.editor.overridden.PopupUtil;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class FastImportAction
extends BaseAction {
    public static final String NAME = "fast-import";

    public FastImportAction() {
        super("fast-import");
    }

    public void actionPerformed(ActionEvent evt, final JTextComponent target) {
        try {
            final Rectangle carretRectangle = target.modelToView(target.getCaretPosition());
            final Font font = target.getFont();
            final Point where = new Point(carretRectangle.x, carretRectangle.y + carretRectangle.height);
            SwingUtilities.convertPointToScreen(where, target);
            final int position = target.getCaretPosition();
            final String ident = Utilities.getIdentifier((BaseDocument)Utilities.getDocument((JTextComponent)target), (int)position);
            FileObject file = this.getFile(target.getDocument());
            if (ident == null || file == null) {
                Toolkit.getDefaultToolkit().beep();
                return;
            }
            final JavaSource js = JavaSource.forFileObject((FileObject)file);
            if (js == null) {
                Toolkit.getDefaultToolkit().beep();
                return;
            }
            final AtomicBoolean cancel = new AtomicBoolean();
            Task<CompilationController> task = new Task<CompilationController>(){

                public void run(CompilationController parameter) throws IOException {
                    parameter.toPhase(JavaSource.Phase.RESOLVED);
                    if (cancel.get()) {
                        return;
                    }
                    final JavaSource javaSource = parameter.getJavaSource();
                    ComputeImports.Pair<Map<String, List<Element>>, Map<String, List<Element>>> result = new ComputeImports().computeCandidates((CompilationInfo)parameter, Collections.singleton(ident));
                    final List<TypeElement> priviledged = ElementFilter.typesIn((Iterable)((Map)result.a).get(ident));
                    if (priviledged == null) {
                        Toolkit.getDefaultToolkit().beep();
                        return;
                    }
                    final ArrayList<TypeElement> denied = new ArrayList<TypeElement>(ElementFilter.typesIn((Iterable)((Map)result.b).get(ident)));
                    denied.removeAll(priviledged);
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            ImportClassPanel panel = new ImportClassPanel(priviledged, denied, font, javaSource, position, target);
                            PopupUtil.showPopup(panel, "", where.x, where.y, true, carretRectangle.height);
                        }
                    });
                }

            };
            ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable((Task)task){
                final /* synthetic */ Task val$task;

                @Override
                public void run() {
                    try {
                        js.runUserActionTask(this.val$task, true);
                    }
                    catch (IOException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
            }, (String)NbBundle.getMessage(FastImportAction.class, (String)"LBL_Fast_Import"), (AtomicBoolean)cancel, (boolean)false);
        }
        catch (BadLocationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    private FileObject getFile(Document doc) {
        DataObject od = (DataObject)doc.getProperty("stream");
        if (od == null) {
            return null;
        }
        return od.getPrimaryFile();
    }

}

