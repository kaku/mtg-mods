/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.DoWhileLoopTree
 *  com.sun.source.tree.EnhancedForLoopTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.ForLoopTree
 *  com.sun.source.tree.IfTree
 *  com.sun.source.tree.LiteralTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.ParenthesizedTree
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.SynchronizedTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.TryTree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.tree.WhileLoopTree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.source.Comment
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.ModificationResult
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.java.source.TreeMaker
 *  org.netbeans.api.java.source.TreePathHandle
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.WorkingCopy
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.api.progress.ProgressUtils
 *  org.netbeans.editor.BaseAction
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.spi.editor.highlighting.support.OffsetsBag
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.codegen;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.DoWhileLoopTree;
import com.sun.source.tree.EnhancedForLoopTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.ForLoopTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.ParenthesizedTree;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.SynchronizedTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TryTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.tree.WhileLoopTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.MissingResourceException;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.lang.model.element.Name;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyleConstants;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.source.Comment;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.TreeMaker;
import org.netbeans.api.java.source.TreePathHandle;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.editor.BaseAction;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.editor.java.Utilities;
import org.netbeans.modules.java.editor.codegen.CodeDeleter;
import org.netbeans.modules.java.editor.codegen.GeneratorUtils;
import org.netbeans.modules.java.editor.codegen.RemoveSurroundingCodePanel;
import org.netbeans.modules.java.editor.overridden.PopupUtil;
import org.netbeans.spi.editor.highlighting.support.OffsetsBag;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class RemoveSurroundingCodeAction
extends BaseAction {
    private static final AttributeSet DELETE_HIGHLIGHT = AttributesUtilities.createImmutable((Object[])new Object[]{StyleConstants.Background, new Color(245, 245, 245), StyleConstants.Foreground, new Color(180, 180, 180)});
    private static final AttributeSet REMAIN_HIGHLIGHT = AttributesUtilities.createImmutable((Object[])new Object[]{StyleConstants.Background, new Color(210, 240, 210)});

    public void actionPerformed(ActionEvent evt, final JTextComponent component) {
        if (component == null || !component.isEditable() || !component.isEnabled()) {
            Toolkit.getDefaultToolkit().beep();
            return;
        }
        BaseDocument doc = (BaseDocument)component.getDocument();
        final JavaSource js = JavaSource.forDocument((Document)doc);
        if (js != null) {
            final AtomicBoolean cancel = new AtomicBoolean();
            ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

                @Override
                public void run() {
                    try {
                        js.runUserActionTask((Task)new Task<CompilationController>(){

                            public void run(CompilationController controller) throws Exception {
                                try {
                                    if (cancel.get()) {
                                        return;
                                    }
                                    controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                                    if (cancel.get()) {
                                        return;
                                    }
                                    TreeUtilities tu = controller.getTreeUtilities();
                                    final ArrayList<CodeDeleter> codeDeleters = new ArrayList<CodeDeleter>();
                                    final int caretOffset = component.getCaretPosition();
                                    TokenSequence ts = controller.getTokenHierarchy().tokenSequence(JavaTokenId.language());
                                    ts.move(caretOffset);
                                    if (ts.moveNext() && (ts.token().id() == JavaTokenId.BLOCK_COMMENT || ts.token().id() == JavaTokenId.LINE_COMMENT)) {
                                        codeDeleters.add(new CommentDeleter(component, ts));
                                    }
                                    block8 : for (TreePath tp = tu.pathFor((int)caretOffset); tp != null; tp = tp.getParentPath()) {
                                        Tree leaf = tp.getLeaf();
                                        switch (leaf.getKind()) {
                                            case IF: {
                                                if (RemoveSurroundingCodeAction.this.insideElse(controller, (IfTree)leaf, component.getCaretPosition())) {
                                                    codeDeleters.add(new TreeDeleter((CompilationInfo)controller, component, tp, false));
                                                }
                                            }
                                            case FOR_LOOP: 
                                            case ENHANCED_FOR_LOOP: 
                                            case WHILE_LOOP: 
                                            case DO_WHILE_LOOP: 
                                            case SYNCHRONIZED: 
                                            case TRY: {
                                                codeDeleters.add(new TreeDeleter((CompilationInfo)controller, component, tp));
                                                continue block8;
                                            }
                                            case BLOCK: {
                                                if (tp.getParentPath().getLeaf().getKind() != Tree.Kind.BLOCK) continue block8;
                                                codeDeleters.add(new TreeDeleter((CompilationInfo)controller, component, tp));
                                                continue block8;
                                            }
                                            case PARENTHESIZED: {
                                                if (tp.getParentPath().getLeaf().getKind() == Tree.Kind.IF || Utilities.containErrors(tp.getParentPath().getLeaf())) continue block8;
                                                codeDeleters.add(new TreeDeleter((CompilationInfo)controller, component, tp));
                                            }
                                        }
                                    }
                                    if (codeDeleters.size() > 0) {
                                        SwingUtilities.invokeLater(new Runnable(){

                                            @Override
                                            public void run() {
                                                int altHeight = -1;
                                                Point where = null;
                                                try {
                                                    Rectangle carretRectangle = component.modelToView(caretOffset);
                                                    altHeight = carretRectangle.height;
                                                    where = new Point(carretRectangle.x, carretRectangle.y + carretRectangle.height);
                                                    SwingUtilities.convertPointToScreen(where, component);
                                                }
                                                catch (BadLocationException ble) {
                                                    // empty catch block
                                                }
                                                if (where == null) {
                                                    where = new Point(-1, -1);
                                                }
                                                PopupUtil.showPopup(new RemoveSurroundingCodePanel(component, codeDeleters), null, where.x, where.y, true, altHeight);
                                            }
                                        });
                                    } else {
                                        component.getToolkit().beep();
                                    }
                                }
                                catch (IOException ioe) {
                                    component.getToolkit().beep();
                                }
                            }

                        }, true);
                    }
                    catch (IOException ioe) {
                        component.getToolkit().beep();
                    }
                }

            }, (String)this.getShortDescription(), (AtomicBoolean)cancel, (boolean)false);
        }
    }

    private String getShortDescription() {
        String name = (String)this.getValue("Name");
        if (name != null) {
            try {
                return NbBundle.getMessage(RemoveSurroundingCodeAction.class, (String)name);
            }
            catch (MissingResourceException mre) {
                // empty catch block
            }
        }
        return name;
    }

    private boolean insideElse(CompilationController controller, IfTree ifTree, int caretPosition) {
        if (ifTree.getElseStatement() == null) {
            return false;
        }
        SourcePositions sp = controller.getTrees().getSourcePositions();
        int end = (int)sp.getEndPosition(controller.getCompilationUnit(), (Tree)ifTree.getThenStatement());
        return end > 0 && caretPosition > end;
    }

    private static class CommentDeleter
    implements CodeDeleter {
        private final boolean lineComment;
        private final JTextComponent component;
        private final int offset;
        private final int length;
        private final OffsetsBag bag;

        public CommentDeleter(JTextComponent component, TokenSequence<JavaTokenId> ts) {
            this.lineComment = ts.token().id() == JavaTokenId.LINE_COMMENT;
            this.component = component;
            this.offset = ts.offset();
            this.length = ts.token().length();
            this.bag = new OffsetsBag(component.getDocument(), true);
            if (this.lineComment) {
                this.bag.addHighlight(this.offset, this.offset + 2, DELETE_HIGHLIGHT);
                this.bag.addHighlight(this.offset + 2, this.offset + this.length, REMAIN_HIGHLIGHT);
            } else {
                this.bag.addHighlight(this.offset, this.offset + 2, DELETE_HIGHLIGHT);
                this.bag.addHighlight(this.offset + 2, this.offset + this.length - 2, REMAIN_HIGHLIGHT);
                this.bag.addHighlight(this.offset + this.length - 2, this.offset + this.length, DELETE_HIGHLIGHT);
            }
        }

        @Override
        public String getDisplayName() {
            return this.lineComment ? "// ..." : "/* ... */";
        }

        @Override
        public void invoke() {
            Document doc = this.component.getDocument();
            TokenSequence ts = TokenHierarchy.get((Document)doc).tokenSequence(JavaTokenId.language());
            ts.move(this.component.getCaretPosition());
            if (ts.moveNext() && (ts.token().id() == JavaTokenId.BLOCK_COMMENT || ts.token().id() == JavaTokenId.LINE_COMMENT) && ts.offset() == this.offset && ts.token().length() == this.length) {
                try {
                    if (!this.lineComment) {
                        doc.remove(this.offset + this.length - 2, 2);
                    }
                    doc.remove(this.offset, 2);
                }
                catch (BadLocationException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }

        @Override
        public OffsetsBag getHighlight() {
            return this.bag;
        }
    }

    private static class TreeDeleter
    implements CodeDeleter {
        private JTextComponent component;
        private TreePathHandle tpHandle;
        private boolean unwrap;
        private OffsetsBag bag;

        private TreeDeleter(CompilationInfo cInfo, JTextComponent component, TreePath path) throws BadLocationException {
            this(cInfo, component, path, true);
        }

        private TreeDeleter(CompilationInfo cInfo, JTextComponent component, TreePath path, boolean unwrap) throws BadLocationException {
            this.component = component;
            this.tpHandle = TreePathHandle.create((TreePath)path, (CompilationInfo)cInfo);
            this.unwrap = unwrap;
            this.bag = this.createOffsetsBag(component, cInfo.getTreeUtilities(), cInfo.getTrees().getSourcePositions(), path);
        }

        @Override
        public String getDisplayName() {
            switch (this.tpHandle.getKind()) {
                case IF: {
                    return this.unwrap ? "if (...) ..." : "else ...";
                }
                case FOR_LOOP: 
                case ENHANCED_FOR_LOOP: {
                    return "for (...) ...";
                }
                case WHILE_LOOP: {
                    return "while (...) ...";
                }
                case DO_WHILE_LOOP: {
                    return "do ... while(...)";
                }
                case SYNCHRONIZED: {
                    return "synchronized (...) ...";
                }
                case TRY: {
                    return "try ...";
                }
                case BLOCK: {
                    return "{...}";
                }
                case PARENTHESIZED: {
                    return "(...)";
                }
            }
            throw new IllegalStateException("Unsupported kind: " + (Object)this.tpHandle.getKind());
        }

        @Override
        public void invoke() {
            JavaSource js = JavaSource.forDocument((Document)this.component.getDocument());
            if (js != null) {
                try {
                    ModificationResult mr = js.runModificationTask((Task)new Task<WorkingCopy>(){

                        public void run(WorkingCopy copy) throws IOException {
                            copy.toPhase(JavaSource.Phase.PARSED);
                            TreePath tp = TreeDeleter.this.tpHandle.resolve((CompilationInfo)copy);
                            if (tp != null) {
                                TreeMaker tm = copy.getTreeMaker();
                                TreeUtilities tu = copy.getTreeUtilities();
                                Tree tree = tp.getLeaf();
                                Tree parent = tp.getParentPath().getLeaf();
                                ArrayList<VariableTree> stats = new ArrayList<VariableTree>();
                                List trailingComments = null;
                                switch (tree.getKind()) {
                                    case IF: {
                                        IfTree it = (IfTree)tree;
                                        if (TreeDeleter.this.unwrap) {
                                            TreeDeleter.this.addStat(it.getThenStatement(), stats);
                                        } else {
                                            TreeDeleter.this.addStat((StatementTree)tm.If(it.getCondition(), it.getThenStatement(), null), stats);
                                        }
                                        TreeDeleter.this.addStat(it.getElseStatement(), stats);
                                        trailingComments = TreeDeleter.this.getTrailingComments(tu, (Tree)(it.getElseStatement() != null ? it.getElseStatement() : it.getThenStatement()));
                                        break;
                                    }
                                    case FOR_LOOP: {
                                        ForLoopTree flt = (ForLoopTree)tree;
                                        stats.addAll(flt.getInitializer());
                                        TreeDeleter.this.addStat(flt.getStatement(), stats);
                                        trailingComments = TreeDeleter.this.getTrailingComments(tu, (Tree)flt.getStatement());
                                        break;
                                    }
                                    case ENHANCED_FOR_LOOP: {
                                        EnhancedForLoopTree eflt = (EnhancedForLoopTree)tree;
                                        VariableTree var = eflt.getVariable();
                                        stats.add(tm.Variable(var.getModifiers(), (CharSequence)var.getName(), var.getType(), (ExpressionTree)tm.Literal((Object)null)));
                                        TreeDeleter.this.addStat(eflt.getStatement(), stats);
                                        trailingComments = TreeDeleter.this.getTrailingComments(tu, (Tree)eflt.getStatement());
                                        break;
                                    }
                                    case WHILE_LOOP: {
                                        WhileLoopTree wlt = (WhileLoopTree)tree;
                                        TreeDeleter.this.addStat(wlt.getStatement(), stats);
                                        trailingComments = TreeDeleter.this.getTrailingComments(tu, (Tree)wlt.getStatement());
                                        break;
                                    }
                                    case DO_WHILE_LOOP: {
                                        DoWhileLoopTree dwlt = (DoWhileLoopTree)tree;
                                        TreeDeleter.this.addStat(dwlt.getStatement(), stats);
                                        break;
                                    }
                                    case SYNCHRONIZED: {
                                        SynchronizedTree st = (SynchronizedTree)tree;
                                        TreeDeleter.this.addStat((StatementTree)st.getBlock(), stats);
                                        trailingComments = TreeDeleter.this.getTrailingComments(tu, (Tree)st.getBlock());
                                        break;
                                    }
                                    case TRY: {
                                        TryTree tt = (TryTree)tree;
                                        for (Tree t : tt.getResources()) {
                                            TreeDeleter.this.addStat((StatementTree)t, stats);
                                        }
                                        TreeDeleter.this.addStat((StatementTree)tt.getBlock(), stats);
                                        TreeDeleter.this.addStat((StatementTree)tt.getFinallyBlock(), stats);
                                        trailingComments = TreeDeleter.this.getTrailingComments(tu, (Tree)(tt.getFinallyBlock() != null ? tt.getFinallyBlock() : (tt.getCatches().isEmpty() ? tt.getBlock() : (Tree)tt.getCatches().get(tt.getCatches().size() - 1))));
                                        break;
                                    }
                                    case BLOCK: {
                                        BlockTree bt = (BlockTree)tree;
                                        TreeDeleter.this.addStat((StatementTree)bt, stats);
                                        break;
                                    }
                                    case PARENTHESIZED: {
                                        ParenthesizedTree pt = (ParenthesizedTree)tree;
                                        copy.rewrite(tree, (Tree)pt.getExpression());
                                        return;
                                    }
                                }
                                if (!stats.isEmpty()) {
                                    int i = 0;
                                    for (Comment comment2 : tu.getComments(tree, true)) {
                                        tm.insertComment((Tree)stats.get(0), comment2, i++, true);
                                    }
                                    if (trailingComments == null) {
                                        trailingComments = tu.getComments(tree, false);
                                    }
                                    for (Comment comment2 : trailingComments) {
                                        tm.addComment((Tree)stats.get(stats.size() - 1), comment2, false);
                                    }
                                }
                                if (parent.getKind() == Tree.Kind.BLOCK) {
                                    int i;
                                    BlockTree block = (BlockTree)parent;
                                    int idx = -1;
                                    List blockStats = block.getStatements();
                                    for (i = 0; i < blockStats.size(); ++i) {
                                        if (tree != blockStats.get(i)) continue;
                                        idx = i;
                                        break;
                                    }
                                    if (idx >= 0) {
                                        block = tm.removeBlockStatement(block, idx);
                                        for (i = stats.size() - 1; i >= 0; --i) {
                                            block = tm.insertBlockStatement(block, idx, (StatementTree)stats.get(i));
                                        }
                                    }
                                    copy.rewrite(parent, (Tree)block);
                                } else {
                                    BlockTree newTree = stats.size() > 1 ? tm.Block(stats, false) : (stats.size() == 1 ? (StatementTree)stats.get(0) : null);
                                    copy.rewrite(tree, (Tree)newTree);
                                }
                            }
                        }
                    });
                    GeneratorUtils.guardedCommit(this.component, mr);
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }

        @Override
        public OffsetsBag getHighlight() {
            return this.bag;
        }

        private List<Comment> getTrailingComments(TreeUtilities tu, Tree tree) {
            return tree.getKind() == Tree.Kind.BLOCK ? tu.getComments(tree, false) : null;
        }

        private OffsetsBag createOffsetsBag(JTextComponent component, TreeUtilities tu, SourcePositions sp, TreePath path) throws BadLocationException {
            Document doc = component.getDocument();
            OffsetsBag offsetsBag = new OffsetsBag(doc, true);
            int start = (int)sp.getStartPosition(path.getCompilationUnit(), path.getLeaf());
            if (start >= 0) {
                ArrayList<int[]> positions = new ArrayList<int[]>();
                Tree tree = path.getLeaf();
                switch (tree.getKind()) {
                    case IF: {
                        IfTree it = (IfTree)tree;
                        if (this.unwrap) {
                            positions.add(this.getBounds(tu, sp, path.getCompilationUnit(), (Tree)it.getThenStatement()));
                        } else {
                            int end;
                            start = (int)sp.getEndPosition(path.getCompilationUnit(), (Tree)it.getThenStatement());
                            int off = doc.getText(start, (end = (int)sp.getStartPosition(path.getCompilationUnit(), (Tree)it.getElseStatement())) - start).indexOf("else");
                            if (off > 0) {
                                start += off;
                            }
                        }
                        positions.add(this.getBounds(tu, sp, path.getCompilationUnit(), (Tree)it.getElseStatement()));
                        break;
                    }
                    case FOR_LOOP: {
                        ForLoopTree flt = (ForLoopTree)tree;
                        List inits = flt.getInitializer();
                        if (inits != null && !inits.isEmpty()) {
                            int[] bounds = new int[]{-1, -1};
                            bounds[0] = this.getStart(tu, sp, path.getCompilationUnit(), (Tree)inits.get(0));
                            bounds[1] = this.getEnd(tu, sp, path.getCompilationUnit(), (Tree)inits.get(inits.size() - 1));
                            positions.add(bounds);
                        }
                        positions.add(this.getBounds(tu, sp, path.getCompilationUnit(), (Tree)flt.getStatement()));
                        break;
                    }
                    case ENHANCED_FOR_LOOP: {
                        EnhancedForLoopTree eflt = (EnhancedForLoopTree)tree;
                        positions.add(this.getBounds(tu, sp, path.getCompilationUnit(), (Tree)eflt.getVariable()));
                        positions.add(this.getBounds(tu, sp, path.getCompilationUnit(), (Tree)eflt.getStatement()));
                        break;
                    }
                    case WHILE_LOOP: {
                        WhileLoopTree wlt = (WhileLoopTree)tree;
                        positions.add(this.getBounds(tu, sp, path.getCompilationUnit(), (Tree)wlt.getStatement()));
                        break;
                    }
                    case DO_WHILE_LOOP: {
                        DoWhileLoopTree dwlt = (DoWhileLoopTree)tree;
                        positions.add(this.getBounds(tu, sp, path.getCompilationUnit(), (Tree)dwlt.getStatement()));
                        break;
                    }
                    case SYNCHRONIZED: {
                        SynchronizedTree st = (SynchronizedTree)tree;
                        positions.add(this.getBounds(tu, sp, path.getCompilationUnit(), (Tree)st.getBlock()));
                        break;
                    }
                    case TRY: {
                        TryTree tt = (TryTree)tree;
                        for (Tree t : tt.getResources()) {
                            positions.add(this.getBounds(tu, sp, path.getCompilationUnit(), (Tree)((StatementTree)t)));
                        }
                        positions.add(this.getBounds(tu, sp, path.getCompilationUnit(), (Tree)tt.getBlock()));
                        positions.add(this.getBounds(tu, sp, path.getCompilationUnit(), (Tree)tt.getFinallyBlock()));
                        break;
                    }
                    case BLOCK: {
                        BlockTree bt = (BlockTree)tree;
                        positions.add(this.getBounds(tu, sp, path.getCompilationUnit(), (Tree)bt));
                        break;
                    }
                    case PARENTHESIZED: {
                        ParenthesizedTree pt = (ParenthesizedTree)tree;
                        positions.add(this.getBounds(tu, sp, path.getCompilationUnit(), (Tree)pt.getExpression()));
                    }
                }
                for (int[] bounds : positions) {
                    if (bounds[0] < 0 || bounds[1] <= bounds[0]) continue;
                    offsetsBag.addHighlight(start, bounds[0], DELETE_HIGHLIGHT);
                    offsetsBag.addHighlight(bounds[0], bounds[1], REMAIN_HIGHLIGHT);
                    start = bounds[1];
                }
                int end = (int)sp.getEndPosition(path.getCompilationUnit(), path.getLeaf());
                if (end > start) {
                    offsetsBag.addHighlight(start, end, DELETE_HIGHLIGHT);
                }
            }
            return offsetsBag;
        }

        private void addStat(StatementTree stat, List<StatementTree> to) {
            if (stat != null) {
                if (stat.getKind() == Tree.Kind.BLOCK) {
                    to.addAll(((BlockTree)stat).getStatements());
                } else {
                    to.add(stat);
                }
            }
        }

        private int[] getBounds(TreeUtilities tu, SourcePositions sp, CompilationUnitTree cut, Tree tree) {
            int[] bounds = new int[]{-1, -1};
            if (tree != null) {
                if (tree.getKind() == Tree.Kind.BLOCK) {
                    List stats = ((BlockTree)tree).getStatements();
                    if (stats != null && !stats.isEmpty()) {
                        bounds[0] = this.getStart(tu, sp, cut, (Tree)stats.get(0));
                        bounds[1] = this.getEnd(tu, sp, cut, (Tree)stats.get(stats.size() - 1));
                    }
                } else {
                    bounds[0] = this.getStart(tu, sp, cut, tree);
                    bounds[1] = this.getEnd(tu, sp, cut, tree);
                }
            }
            return bounds;
        }

        private int getStart(TreeUtilities tu, SourcePositions sp, CompilationUnitTree cut, Tree tree) {
            List comments = tu.getComments(tree, true);
            return comments.isEmpty() ? (int)sp.getStartPosition(cut, tree) : ((Comment)comments.get(0)).pos();
        }

        private int getEnd(TreeUtilities tu, SourcePositions sp, CompilationUnitTree cut, Tree tree) {
            List comments = tu.getComments(tree, false);
            return comments.isEmpty() ? (int)sp.getEndPosition(cut, tree) : ((Comment)comments.get(comments.size() - 1)).endPos();
        }

    }

}

