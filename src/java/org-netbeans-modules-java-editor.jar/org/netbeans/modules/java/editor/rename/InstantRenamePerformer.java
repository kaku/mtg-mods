/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.BreakTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ContinueTree
 *  com.sun.source.tree.LabeledStatementTree
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreePathScanner
 *  com.sun.source.util.Trees
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.lexer.JavadocTokenId
 *  org.netbeans.api.java.source.CodeStyle
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementUtilities
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.GuardedDocument
 *  org.netbeans.editor.MarkBlock
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.swing.MutablePositionRegion
 *  org.netbeans.modules.refactoring.api.AbstractRefactoring
 *  org.netbeans.modules.refactoring.api.Problem
 *  org.netbeans.modules.refactoring.api.ProgressEvent
 *  org.netbeans.modules.refactoring.api.ProgressListener
 *  org.netbeans.modules.refactoring.api.RefactoringSession
 *  org.netbeans.modules.refactoring.api.ui.RefactoringActionsFactory
 *  org.netbeans.modules.refactoring.spi.RefactoringElementsBag
 *  org.netbeans.modules.refactoring.spi.RefactoringPlugin
 *  org.netbeans.modules.refactoring.spi.RefactoringPluginFactory
 *  org.netbeans.spi.editor.highlighting.support.OffsetsBag
 *  org.netbeans.spi.editor.typinghooks.DeletedTextInterceptor
 *  org.netbeans.spi.editor.typinghooks.DeletedTextInterceptor$Context
 *  org.netbeans.spi.editor.typinghooks.DeletedTextInterceptor$Factory
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.NbDocument
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakSet
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package org.netbeans.modules.java.editor.rename;

import com.sun.source.tree.BreakTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ContinueTree;
import com.sun.source.tree.LabeledStatementTree;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreePathScanner;
import com.sun.source.util.Trees;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.ElementScanner6;
import javax.lang.model.util.Elements;
import javax.swing.Action;
import javax.swing.SwingUtilities;
import javax.swing.event.CaretEvent;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.StyleConstants;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.lexer.JavadocTokenId;
import org.netbeans.api.java.source.CodeStyle;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.GuardedDocument;
import org.netbeans.editor.MarkBlock;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.swing.MutablePositionRegion;
import org.netbeans.modules.editor.java.ComputeOffAWT;
import org.netbeans.modules.java.editor.codegen.GeneratorUtils;
import org.netbeans.modules.java.editor.javadoc.JavadocImports;
import org.netbeans.modules.java.editor.rename.SyncDocumentRegion;
import org.netbeans.modules.java.editor.semantic.FindLocalUsagesQuery;
import org.netbeans.modules.refactoring.api.AbstractRefactoring;
import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.api.ProgressEvent;
import org.netbeans.modules.refactoring.api.ProgressListener;
import org.netbeans.modules.refactoring.api.RefactoringSession;
import org.netbeans.modules.refactoring.api.ui.RefactoringActionsFactory;
import org.netbeans.modules.refactoring.spi.RefactoringElementsBag;
import org.netbeans.modules.refactoring.spi.RefactoringPlugin;
import org.netbeans.modules.refactoring.spi.RefactoringPluginFactory;
import org.netbeans.spi.editor.highlighting.support.OffsetsBag;
import org.netbeans.spi.editor.typinghooks.DeletedTextInterceptor;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.NbDocument;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.WeakSet;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class InstantRenamePerformer
implements DocumentListener,
KeyListener {
    private static final Logger LOG = Logger.getLogger(InstantRenamePerformer.class.getName());
    private static final Set<InstantRenamePerformer> registry = Collections.synchronizedSet(new WeakSet());
    private SyncDocumentRegion region;
    private int span;
    private Document doc;
    private JTextComponent target;
    private AttributeSet attribs = null;
    private AttributeSet attribsLeft = null;
    private AttributeSet attribsRight = null;
    private AttributeSet attribsMiddle = null;
    private AttributeSet attribsAll = null;
    private AttributeSet attribsSlave = null;
    private AttributeSet attribsSlaveLeft = null;
    private AttributeSet attribsSlaveRight = null;
    private AttributeSet attribsSlaveMiddle = null;
    private AttributeSet attribsSlaveAll = null;
    private static final Set<ElementKind> LOCAL_CLASS_PARENTS = EnumSet.of(ElementKind.CONSTRUCTOR, ElementKind.INSTANCE_INIT, ElementKind.METHOD, ElementKind.STATIC_INIT);
    private volatile boolean inSync;
    private static final AttributeSet defaultSyncedTextBlocksHighlight = AttributesUtilities.createImmutable((Object[])new Object[]{StyleConstants.Foreground, Color.red});

    private InstantRenamePerformer(JTextComponent target, Set<Token> highlights, int caretOffset) throws BadLocationException {
        this.target = target;
        this.doc = target.getDocument();
        MutablePositionRegion mainRegion = null;
        ArrayList<MutablePositionRegion> regions = new ArrayList<MutablePositionRegion>();
        for (Token h : highlights) {
            Position end;
            int delta = h.id() == JavadocTokenId.IDENT && h.text().charAt(0) == '<' && h.text().charAt(h.length() - 1) == '>' ? 1 : 0;
            Position start = NbDocument.createPosition((Document)this.doc, (int)(h.offset(null) + delta), (Position.Bias)Position.Bias.Backward);
            MutablePositionRegion current = new MutablePositionRegion(start, end = NbDocument.createPosition((Document)this.doc, (int)(h.offset(null) + h.length() - delta), (Position.Bias)Position.Bias.Forward));
            if (InstantRenamePerformer.isIn(current, caretOffset)) {
                mainRegion = current;
                continue;
            }
            regions.add(current);
        }
        if (mainRegion == null) {
            throw new IllegalArgumentException("No highlight contains the caret.");
        }
        regions.add(0, mainRegion);
        this.region = new SyncDocumentRegion(this.doc, regions);
        if (this.doc instanceof BaseDocument) {
            BaseDocument bdoc = (BaseDocument)this.doc;
            bdoc.setPostModificationDocumentListener((DocumentListener)this);
            CancelInstantRenameUndoableEdit undo = new CancelInstantRenameUndoableEdit(this);
            for (UndoableEditListener l : bdoc.getUndoableEditListeners()) {
                l.undoableEditHappened(new UndoableEditEvent(this.doc, undo));
            }
        }
        target.addKeyListener(this);
        target.putClientProperty(InstantRenamePerformer.class, this);
        target.putClientProperty("NetBeansEditor.navigateBoundaries", (Object)mainRegion);
        this.requestRepaint();
        target.select(mainRegion.getStartOffset(), mainRegion.getEndOffset());
        this.span = this.region.getFirstRegionLength();
        registry.add(this);
        InstantRenamePerformer.sendUndoableEdit(this.doc, CloneableEditorSupport.BEGIN_COMMIT_GROUP);
    }

    public static void invokeInstantRename(JTextComponent target) {
        try {
            JavaSource js;
            final int caret = target.getCaretPosition();
            String ident = Utilities.getIdentifier((BaseDocument)Utilities.getDocument((JTextComponent)target), (int)caret);
            if (ident == null) {
                Utilities.setStatusBoldText((JTextComponent)target, (String)NbBundle.getMessage(InstantRenamePerformer.class, (String)"WARN_CannotPerformHere"));
                return;
            }
            DataObject od = (DataObject)target.getDocument().getProperty("stream");
            JavaSource javaSource = js = od != null ? JavaSource.forFileObject((FileObject)od.getPrimaryFile()) : null;
            if (js == null) {
                Utilities.setStatusBoldText((JTextComponent)target, (String)NbBundle.getMessage(InstantRenamePerformer.class, (String)"WARN_CannotPerformHere"));
                return;
            }
            final boolean[] wasResolved = new boolean[1];
            Set changePoints = (Set)ComputeOffAWT.computeOffAWT(new ComputeOffAWT.Worker<Set<Token>>(){

                @Override
                public Set<Token> process(CompilationInfo info) {
                    try {
                        return InstantRenamePerformer.computeChangePoints(info, caret, wasResolved);
                    }
                    catch (IOException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                        return null;
                    }
                }
            }, "Instant Rename", js, JavaSource.Phase.RESOLVED);
            if (wasResolved[0]) {
                if (changePoints != null) {
                    InstantRenamePerformer.doInstantRename(changePoints, target, caret, ident);
                } else {
                    InstantRenamePerformer.doFullRename((EditorCookie)od.getCookie(EditorCookie.class), od.getNodeDelegate());
                }
            } else {
                Utilities.setStatusBoldText((JTextComponent)target, (String)NbBundle.getMessage(InstantRenamePerformer.class, (String)"WARN_CannotPerformHere"));
            }
        }
        catch (BadLocationException e) {
            Exceptions.printStackTrace((Throwable)e);
        }
    }

    private static void doFullRename(EditorCookie ec, Node n) {
        InstanceContent ic = new InstanceContent();
        ic.add((Object)ec);
        ic.add((Object)n);
        AbstractLookup actionContext = new AbstractLookup((AbstractLookup.Content)ic);
        Action a = RefactoringActionsFactory.renameAction().createContextAwareInstance((Lookup)actionContext);
        a.actionPerformed(RefactoringActionsFactory.DEFAULT_EVENT);
    }

    private static void doInstantRename(Set<Token> changePoints, JTextComponent target, int caret, String ident) throws BadLocationException {
        InstantRenamePerformer.performInstantRename(target, changePoints, caret);
    }

    static Set<Token> computeChangePoints(final CompilationInfo info, final int caret, final boolean[] wasResolved) throws IOException {
        Token name;
        TreePath path;
        Element el;
        final Document doc = info.getDocument();
        if (doc == null) {
            return null;
        }
        final int[] adjustedCaret = new int[]{caret};
        final boolean[] insideJavadoc = new boolean[]{false};
        doc.render(new Runnable(){

            @Override
            public void run() {
                TokenSequence ts = SourceUtils.getJavaTokenSequence((TokenHierarchy)info.getTokenHierarchy(), (int)caret);
                ts.move(caret);
                if (ts.moveNext() && ts.token() != null) {
                    if (ts.token().id() == JavaTokenId.IDENTIFIER) {
                        adjustedCaret[0] = ts.offset() + ts.token().length() / 2 + 1;
                    } else if (ts.token().id() == JavaTokenId.JAVADOC_COMMENT) {
                        TokenSequence jdts = ts.embedded(JavadocTokenId.language());
                        if (jdts != null && JavadocImports.isInsideReference(jdts, caret)) {
                            jdts.move(caret);
                            if (jdts.moveNext() && jdts.token().id() == JavadocTokenId.IDENT) {
                                adjustedCaret[0] = jdts.offset();
                                insideJavadoc[0] = true;
                            }
                        } else if (jdts != null && JavadocImports.isInsideParamName(jdts, caret)) {
                            jdts.move(caret);
                            if (jdts.moveNext()) {
                                adjustedCaret[0] = jdts.offset();
                                insideJavadoc[0] = true;
                            }
                        }
                    }
                }
            }
        });
        TreePath treePath = path = insideJavadoc[0] ? null : info.getTreeUtilities().pathFor(adjustedCaret[0]);
        if (path != null && path.getParentPath() != null) {
            long typeEnd;
            long variableEnd;
            Tree.Kind leafKind = path.getLeaf().getKind();
            Tree.Kind parentKind = path.getParentPath().getLeaf().getKind();
            if (leafKind == Tree.Kind.ARRAY_TYPE && parentKind == Tree.Kind.VARIABLE && (typeEnd = info.getTrees().getSourcePositions().getEndPosition(info.getCompilationUnit(), path.getLeaf())) == (variableEnd = info.getTrees().getSourcePositions().getEndPosition(info.getCompilationUnit(), path.getLeaf()))) {
                path = path.getParentPath();
            }
        }
        Element element = el = insideJavadoc[0] ? JavadocImports.findReferencedElement(info, adjustedCaret[0]) : info.getTrees().getElement(path);
        if (el == null && path != null) {
            Token<JavaTokenId> span;
            if (path != null && EnumSet.of(Tree.Kind.LABELED_STATEMENT, Tree.Kind.BREAK, Tree.Kind.CONTINUE).contains((Object)path.getLeaf().getKind()) && (span = org.netbeans.modules.java.editor.semantic.Utilities.findIdentifierSpan(info, doc, path)) != null && span.offset(null) <= adjustedCaret[0] && adjustedCaret[0] <= span.offset(null) + span.length()) {
                if (path.getLeaf().getKind() != Tree.Kind.LABELED_STATEMENT) {
                    StatementTree tgt = info.getTreeUtilities().getBreakContinueTarget(path);
                    TreePath treePath2 = path = tgt != null ? info.getTrees().getPath(info.getCompilationUnit(), (Tree)tgt) : null;
                }
                if (path != null) {
                    wasResolved[0] = true;
                    return InstantRenamePerformer.collectLabels(info, doc, path);
                }
            }
            wasResolved[0] = false;
            return null;
        }
        Token token = name = insideJavadoc[0] ? JavadocImports.findNameTokenOfReferencedElement(info, adjustedCaret[0]) : org.netbeans.modules.java.editor.semantic.Utilities.getToken(info, doc, path);
        if (name == null) {
            return null;
        }
        doc.render(new Runnable(){

            @Override
            public void run() {
                wasResolved[0] = name.offset(null) <= caret && caret <= name.offset(null) + name.length();
            }
        });
        if (!wasResolved[0]) {
            return null;
        }
        if (insideJavadoc[0] && el == null) {
            return Collections.singleton(name);
        }
        if (el.getKind() == ElementKind.CONSTRUCTOR) {
            el = el.getEnclosingElement();
        }
        if (InstantRenamePerformer.allowInstantRename(info, el, info.getElementUtilities())) {
            final HashSet<Token> points = new HashSet<Token>(new FindLocalUsagesQuery(true).findUsages(el, info, doc));
            if (el.getKind().isClass()) {
                for (ExecutableElement c : ElementFilter.constructorsIn(el.getEnclosedElements())) {
                    Token<JavaTokenId> token2;
                    TreePath t = info.getTrees().getPath((Element)c);
                    if (t == null || (token2 = org.netbeans.modules.java.editor.semantic.Utilities.getToken(info, doc, t)) == null) continue;
                    points.add(token2);
                }
            }
            final boolean[] overlapsWithGuardedBlocks = new boolean[1];
            doc.render(new Runnable(){

                @Override
                public void run() {
                    overlapsWithGuardedBlocks[0] = InstantRenamePerformer.overlapsWithGuardedBlocks(doc, points);
                }
            });
            if (overlapsWithGuardedBlocks[0]) {
                return null;
            }
            return points;
        }
        if (insideJavadoc[0]) {
            wasResolved[0] = false;
        }
        return null;
    }

    private static Set<Token> collectLabels(final CompilationInfo info, final Document document, TreePath labeledStatement) {
        final LinkedHashSet<Token> result = new LinkedHashSet<Token>();
        if (labeledStatement.getLeaf().getKind() == Tree.Kind.LABELED_STATEMENT) {
            result.add(org.netbeans.modules.java.editor.semantic.Utilities.findIdentifierSpan(info, document, labeledStatement));
            final Name label = ((LabeledStatementTree)labeledStatement.getLeaf()).getLabel();
            new TreePathScanner<Void, Void>(){

                public Void visitBreak(BreakTree node, Void p) {
                    if (node.getLabel() != null && label.contentEquals(node.getLabel())) {
                        result.add(org.netbeans.modules.java.editor.semantic.Utilities.findIdentifierSpan(info, document, this.getCurrentPath()));
                    }
                    return (Void)TreePathScanner.super.visitBreak(node, (Object)p);
                }

                public Void visitContinue(ContinueTree node, Void p) {
                    if (node.getLabel() != null && label.contentEquals(node.getLabel())) {
                        result.add(org.netbeans.modules.java.editor.semantic.Utilities.findIdentifierSpan(info, document, this.getCurrentPath()));
                    }
                    return (Void)TreePathScanner.super.visitContinue(node, (Object)p);
                }
            }.scan(labeledStatement, (Object)null);
        }
        return result;
    }

    private static boolean allowInstantRename(CompilationInfo info, Element e, ElementUtilities eu) {
        if (e.getKind() == ElementKind.FIELD) {
            VariableElement variableElement = (VariableElement)e;
            TypeElement typeElement = eu.enclosingTypeElement(e);
            HashMap<String, List<ExecutableElement>> methods = new HashMap<String, List<ExecutableElement>>();
            for (ExecutableElement method : ElementFilter.methodsIn(info.getElements().getAllMembers(typeElement))) {
                ArrayList<ExecutableElement> l = (ArrayList<ExecutableElement>)methods.get(method.getSimpleName().toString());
                if (l == null) {
                    l = new ArrayList<ExecutableElement>();
                    methods.put(method.getSimpleName().toString(), l);
                }
                l.add(method);
            }
            boolean isProperty = false;
            try {
                CodeStyle codeStyle = CodeStyle.getDefault((Document)info.getDocument());
                isProperty = GeneratorUtils.hasGetter(info, typeElement, variableElement, methods, codeStyle);
                isProperty = isProperty || !variableElement.getModifiers().contains((Object)Modifier.FINAL) && GeneratorUtils.hasSetter(info, typeElement, variableElement, methods, codeStyle);
            }
            catch (IOException ex) {
                // empty catch block
            }
            if (isProperty) {
                return false;
            }
        }
        if (org.netbeans.modules.java.editor.semantic.Utilities.isPrivateElement(e)) {
            return true;
        }
        if (InstantRenamePerformer.isInaccessibleOutsideOuterClass(e, eu)) {
            return true;
        }
        if (e.getKind() == ElementKind.CLASS) {
            Element enclosing = e.getEnclosingElement();
            ElementKind enclosingKind = enclosing.getKind();
            if (enclosingKind == ElementKind.CLASS) {
                EnumSet<ElementKind> fm = EnumSet.of(ElementKind.METHOD, ElementKind.FIELD);
                if (enclosing.getSimpleName().length() == 0 || fm.contains((Object)enclosing.getEnclosingElement().getKind())) {
                    return true;
                }
            }
            return LOCAL_CLASS_PARENTS.contains((Object)enclosingKind);
        }
        if (e.getKind() == ElementKind.TYPE_PARAMETER) {
            return true;
        }
        return false;
    }

    private static boolean isInaccessibleOutsideOuterClass(Element e, ElementUtilities eu) {
        Element enclosing = e.getEnclosingElement();
        boolean isStatic = e.getModifiers().contains((Object)Modifier.STATIC);
        ElementKind kind = e.getKind();
        if (isStatic || kind.isClass() || kind.isInterface() || kind.isField()) {
            return InstantRenamePerformer.isAnyEncloserPrivate(e);
        }
        if (enclosing != null && kind == ElementKind.METHOD) {
            ElementKind enclosingKind = enclosing.getKind();
            boolean isEnclosingFinal = enclosing.getModifiers().contains((Object)Modifier.FINAL) || enclosingKind == ElementKind.ANNOTATION_TYPE;
            return InstantRenamePerformer.isAnyEncloserPrivate(e) && !eu.overridesMethod((ExecutableElement)e) && !eu.implementsMethod((ExecutableElement)e) && (isEnclosingFinal || !InstantRenamePerformer.isOverriddenInsideOutermostEnclosingClass((ExecutableElement)e, eu));
        }
        return false;
    }

    private static boolean isAnyEncloserPrivate(Element e) {
        for (Element enclosing = e.getEnclosingElement(); enclosing != null && (enclosing.getKind().isClass() || enclosing.getKind().isInterface()); enclosing = enclosing.getEnclosingElement()) {
            boolean isPrivateClass = enclosing.getModifiers().contains((Object)Modifier.PRIVATE);
            if (!isPrivateClass) continue;
            return true;
        }
        return false;
    }

    private static boolean isOverriddenInsideOutermostEnclosingClass(final ExecutableElement ee, final ElementUtilities eu) {
        final boolean[] ret = new boolean[]{false};
        new ElementScanner6<Void, Void>(){

            @Override
            public Void visitType(TypeElement te, Void p) {
                if (ret[0]) {
                    return null;
                }
                if (te != ee.getEnclosingElement() && eu.getImplementationOf(ee, te) != null && !InstantRenamePerformer.isAnyEncloserPrivate(te)) {
                    ret[0] = true;
                }
                return (Void)ElementScanner6.super.visitType(te, p);
            }
        }.scan(eu.outermostTypeElement((Element)ee));
        return ret[0];
    }

    private static boolean overlapsWithGuardedBlocks(Document doc, Set<Token> highlights) {
        if (!(doc instanceof GuardedDocument)) {
            return false;
        }
        GuardedDocument gd = (GuardedDocument)doc;
        for (MarkBlock current = gd.getGuardedBlockChain().getChain(); current != null; current = current.getNext()) {
            for (Token h : highlights) {
                if ((current.compare(h.offset(null), h.offset(null) + h.length()) & 1) == 0) continue;
                return true;
            }
        }
        return false;
    }

    public static void performInstantRename(JTextComponent target, Set<Token> highlights, int caretOffset) throws BadLocationException {
        new InstantRenamePerformer(target, highlights, caretOffset);
    }

    private static boolean isIn(MutablePositionRegion region, int caretOffset) {
        return region.getStartOffset() <= caretOffset && caretOffset <= region.getEndOffset();
    }

    @Override
    public synchronized void insertUpdate(DocumentEvent e) {
        if (this.inSync) {
            return;
        }
        if (e.getOffset() < this.region.getFirstRegionStartOffset() || e.getOffset() + e.getLength() > this.region.getFirstRegionEndOffset()) {
            this.release();
            return;
        }
        this.inSync = true;
        this.region.sync(0);
        this.span = this.region.getFirstRegionLength();
        this.inSync = false;
        this.requestRepaint();
    }

    @Override
    public synchronized void removeUpdate(DocumentEvent e) {
        if (this.inSync) {
            return;
        }
        if (e.getLength() == 1) {
            if (e.getOffset() < this.region.getFirstRegionStartOffset() || e.getOffset() > this.region.getFirstRegionEndOffset()) {
                this.release();
                return;
            }
            if (e.getOffset() == this.region.getFirstRegionStartOffset() && this.region.getFirstRegionLength() > 0 && this.region.getFirstRegionLength() == this.span) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "e.getOffset()={0}", e.getOffset());
                    LOG.log(Level.FINE, "region.getFirstRegionStartOffset()={0}", this.region.getFirstRegionStartOffset());
                    LOG.log(Level.FINE, "region.getFirstRegionEndOffset()={0}", this.region.getFirstRegionEndOffset());
                    LOG.log(Level.FINE, "span= {0}", this.span);
                }
                this.release();
                return;
            }
            if (e.getOffset() == this.region.getFirstRegionEndOffset() && this.region.getFirstRegionLength() > 0 && this.region.getFirstRegionLength() == this.span) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "e.getOffset()={0}", e.getOffset());
                    LOG.log(Level.FINE, "region.getFirstRegionStartOffset()={0}", this.region.getFirstRegionStartOffset());
                    LOG.log(Level.FINE, "region.getFirstRegionEndOffset()={0}", this.region.getFirstRegionEndOffset());
                    LOG.log(Level.FINE, "span= {0}", this.span);
                }
                this.release();
                return;
            }
            if (e.getOffset() == this.region.getFirstRegionEndOffset() && e.getOffset() == this.region.getFirstRegionStartOffset() && this.region.getFirstRegionLength() == 0 && this.region.getFirstRegionLength() == this.span) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "e.getOffset()={0}", e.getOffset());
                    LOG.log(Level.FINE, "region.getFirstRegionStartOffset()={0}", this.region.getFirstRegionStartOffset());
                    LOG.log(Level.FINE, "region.getFirstRegionEndOffset()={0}", this.region.getFirstRegionEndOffset());
                    LOG.log(Level.FINE, "span= {0}", this.span);
                }
                this.release();
                return;
            }
        } else {
            int removeSpan = e.getLength() + this.region.getFirstRegionLength();
            if (this.span < removeSpan) {
                this.release();
                return;
            }
        }
        if (this.doc.getProperty("doc-replace-selection-property") != null) {
            return;
        }
        this.inSync = true;
        this.region.sync(0);
        this.span = this.region.getFirstRegionLength();
        this.inSync = false;
        this.requestRepaint();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    public void caretUpdate(CaretEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public synchronized void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == 27 && e.getModifiers() == 0 || e.getKeyCode() == 10 && e.getModifiers() == 0) {
            this.release();
            e.consume();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    private synchronized void release() {
        if (this.target == null) {
            return;
        }
        InstantRenamePerformer.sendUndoableEdit(this.doc, CloneableEditorSupport.END_COMMIT_GROUP);
        this.target.putClientProperty("NetBeansEditor.navigateBoundaries", null);
        this.target.putClientProperty(InstantRenamePerformer.class, null);
        if (this.doc instanceof BaseDocument) {
            ((BaseDocument)this.doc).setPostModificationDocumentListener(null);
        }
        this.target.removeKeyListener(this);
        this.target = null;
        this.region = null;
        this.attribs = null;
        this.requestRepaint();
        this.doc = null;
    }

    private void requestRepaint() {
        if (this.region == null) {
            OffsetsBag bag = InstantRenamePerformer.getHighlightsBag(this.doc);
            bag.clear();
        } else {
            if (this.attribs == null) {
                this.attribs = InstantRenamePerformer.getSyncedTextBlocksHighlight("synchronized-text-blocks-ext");
                Color foreground = (Color)this.attribs.getAttribute(StyleConstants.Foreground);
                Color background = (Color)this.attribs.getAttribute(StyleConstants.Background);
                this.attribsLeft = InstantRenamePerformer.createAttribs(StyleConstants.Background, background, EditorStyleConstants.LeftBorderLineColor, foreground, EditorStyleConstants.TopBorderLineColor, foreground, EditorStyleConstants.BottomBorderLineColor, foreground);
                this.attribsRight = InstantRenamePerformer.createAttribs(StyleConstants.Background, background, EditorStyleConstants.RightBorderLineColor, foreground, EditorStyleConstants.TopBorderLineColor, foreground, EditorStyleConstants.BottomBorderLineColor, foreground);
                this.attribsMiddle = InstantRenamePerformer.createAttribs(StyleConstants.Background, background, EditorStyleConstants.TopBorderLineColor, foreground, EditorStyleConstants.BottomBorderLineColor, foreground);
                this.attribsAll = InstantRenamePerformer.createAttribs(StyleConstants.Background, background, EditorStyleConstants.LeftBorderLineColor, foreground, EditorStyleConstants.RightBorderLineColor, foreground, EditorStyleConstants.TopBorderLineColor, foreground, EditorStyleConstants.BottomBorderLineColor, foreground);
                this.attribsSlave = InstantRenamePerformer.getSyncedTextBlocksHighlight("synchronized-text-blocks-ext-slave");
                Color slaveForeground = (Color)this.attribsSlave.getAttribute(StyleConstants.Foreground);
                Color slaveBackground = (Color)this.attribsSlave.getAttribute(StyleConstants.Background);
                this.attribsSlaveLeft = InstantRenamePerformer.createAttribs(StyleConstants.Background, slaveBackground, EditorStyleConstants.LeftBorderLineColor, slaveForeground, EditorStyleConstants.TopBorderLineColor, slaveForeground, EditorStyleConstants.BottomBorderLineColor, slaveForeground);
                this.attribsSlaveRight = InstantRenamePerformer.createAttribs(StyleConstants.Background, slaveBackground, EditorStyleConstants.RightBorderLineColor, slaveForeground, EditorStyleConstants.TopBorderLineColor, slaveForeground, EditorStyleConstants.BottomBorderLineColor, slaveForeground);
                this.attribsSlaveMiddle = InstantRenamePerformer.createAttribs(StyleConstants.Background, slaveBackground, EditorStyleConstants.TopBorderLineColor, slaveForeground, EditorStyleConstants.BottomBorderLineColor, slaveForeground);
                this.attribsSlaveAll = InstantRenamePerformer.createAttribs(StyleConstants.Background, slaveBackground, EditorStyleConstants.LeftBorderLineColor, slaveForeground, EditorStyleConstants.RightBorderLineColor, slaveForeground, EditorStyleConstants.TopBorderLineColor, slaveForeground, EditorStyleConstants.BottomBorderLineColor, slaveForeground);
            }
            OffsetsBag nue = new OffsetsBag(this.doc);
            for (int i = 0; i < this.region.getRegionCount(); ++i) {
                int startOffset = this.region.getRegion(i).getStartOffset();
                int endOffset = this.region.getRegion(i).getEndOffset();
                int size = this.region.getRegion(i).getLength();
                if (size == 1) {
                    nue.addHighlight(startOffset, endOffset, i == 0 ? this.attribsAll : this.attribsSlaveAll);
                    continue;
                }
                if (size <= 1) continue;
                nue.addHighlight(startOffset, startOffset + 1, i == 0 ? this.attribsLeft : this.attribsSlaveLeft);
                nue.addHighlight(endOffset - 1, endOffset, i == 0 ? this.attribsRight : this.attribsSlaveRight);
                if (size <= 2) continue;
                nue.addHighlight(startOffset + 1, endOffset - 1, i == 0 ? this.attribsMiddle : this.attribsSlaveMiddle);
            }
            OffsetsBag bag = InstantRenamePerformer.getHighlightsBag(this.doc);
            bag.setHighlights(nue);
        }
    }

    private static AttributeSet getSyncedTextBlocksHighlight(String name) {
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(FontColorSettings.class);
        AttributeSet as = fcs != null ? fcs.getFontColors(name) : null;
        return as == null ? defaultSyncedTextBlocksHighlight : as;
    }

    private static /* varargs */ AttributeSet createAttribs(Object ... keyValuePairs) {
        assert (keyValuePairs.length % 2 == 0);
        ArrayList<Object> list = new ArrayList<Object>();
        for (int i = keyValuePairs.length / 2 - 1; i >= 0; --i) {
            Object attrKey = keyValuePairs[2 * i];
            Object attrValue = keyValuePairs[2 * i + 1];
            if (attrKey == null || attrValue == null) continue;
            list.add(attrKey);
            list.add(attrValue);
        }
        return AttributesUtilities.createImmutable((Object[])list.toArray());
    }

    public static OffsetsBag getHighlightsBag(Document doc) {
        OffsetsBag bag = (OffsetsBag)doc.getProperty(InstantRenamePerformer.class);
        if (bag == null) {
            bag = new OffsetsBag(doc);
            doc.putProperty(InstantRenamePerformer.class, (Object)bag);
            Object stream = doc.getProperty("stream");
            if (stream instanceof DataObject) {
                Logger.getLogger("TIMER").log(Level.FINE, "Instant Rename Highlights Bag", new Object[]{((DataObject)stream).getPrimaryFile(), bag});
            }
        }
        return bag;
    }

    private static void sendUndoableEdit(Document d, UndoableEdit ue) {
        if (d instanceof AbstractDocument) {
            UndoableEditListener[] uels = ((AbstractDocument)d).getUndoableEditListeners();
            UndoableEditEvent ev = new UndoableEditEvent(d, ue);
            for (UndoableEditListener uel : uels) {
                uel.undoableEditHappened(ev);
            }
        }
    }

    public static class RenameDeletedTextInterceptor
    implements DeletedTextInterceptor {
        public boolean beforeRemove(DeletedTextInterceptor.Context context) throws BadLocationException {
            Object getObject = context.getComponent().getClientProperty(InstantRenamePerformer.class);
            if (getObject instanceof InstantRenamePerformer) {
                InstantRenamePerformer instantRenamePerformer = (InstantRenamePerformer)getObject;
                MutablePositionRegion region = instantRenamePerformer.region.getRegion(0);
                return context.isBackwardDelete() && region.getStartOffset() == context.getOffset() || !context.isBackwardDelete() && region.getEndOffset() == context.getOffset();
            }
            return false;
        }

        public void remove(DeletedTextInterceptor.Context context) throws BadLocationException {
        }

        public void afterRemove(DeletedTextInterceptor.Context context) throws BadLocationException {
        }

        public void cancelled(DeletedTextInterceptor.Context context) {
        }

        public static class Factory
        implements DeletedTextInterceptor.Factory {
            public DeletedTextInterceptor createDeletedTextInterceptor(MimePath mimePath) {
                return new RenameDeletedTextInterceptor();
            }
        }

    }

    public static class AllRefactoringsPluginFactory
    implements RefactoringPluginFactory {
        public RefactoringPlugin createInstance(AbstractRefactoring refactoring) {
            return new RefactoringPluginImpl();
        }

        private static final class RefactoringPluginImpl
        implements RefactoringPlugin {
            private RefactoringPluginImpl() {
            }

            public Problem preCheck() {
                return null;
            }

            public Problem checkParameters() {
                return null;
            }

            public Problem fastCheckParameters() {
                return null;
            }

            public void cancelRequest() {
            }

            public Problem prepare(RefactoringElementsBag refactoringElements) {
                refactoringElements.getSession().addProgressListener(new ProgressListener(){

                    public void start(ProgressEvent event) {
                        InstantRenamePerformer[] performers;
                        for (InstantRenamePerformer p : performers = registry.toArray(new InstantRenamePerformer[0])) {
                            p.inSync = true;
                        }
                        SwingUtilities.invokeLater(new Runnable(){

                            @Override
                            public void run() {
                                for (InstantRenamePerformer p : performers) {
                                    p.release();
                                }
                            }
                        });
                    }

                    public void step(ProgressEvent event) {
                    }

                    public void stop(ProgressEvent event) {
                    }

                });
                return null;
            }

        }

    }

    private static class CancelInstantRenameUndoableEdit
    extends AbstractUndoableEdit {
        private final Reference<InstantRenamePerformer> performer;

        public CancelInstantRenameUndoableEdit(InstantRenamePerformer performer) {
            this.performer = new WeakReference<InstantRenamePerformer>(performer);
        }

        @Override
        public boolean isSignificant() {
            return false;
        }

        @Override
        public void undo() throws CannotUndoException {
            InstantRenamePerformer perf = this.performer.get();
            if (perf != null) {
                perf.release();
            }
        }
    }

}

