/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.Fold
 *  org.netbeans.api.editor.fold.FoldTemplate
 *  org.netbeans.api.editor.fold.FoldType
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.spi.editor.fold.ContentReader
 *  org.netbeans.spi.editor.fold.ContentReader$Factory
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.java.editor.fold;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldTemplate;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.modules.java.editor.fold.JavaFoldTypeProvider;
import org.netbeans.modules.java.editor.fold.ResourceStringFoldInfo;
import org.netbeans.modules.java.editor.fold.ResourceStringLoader;
import org.netbeans.spi.editor.fold.ContentReader;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;

public class ResourceContentReader
implements ContentReader,
ChangeListener {
    private volatile Method accessor;
    private volatile boolean reported;
    private ResourceStringLoader loader;

    public ResourceContentReader() {
        this.loader = new ResourceStringLoader(this);
    }

    @Override
    public void stateChanged(ChangeEvent e) {
    }

    ResourceStringFoldInfo access(Fold f) {
        if (this.accessor == null && !this.reported) {
            try {
                Method m = f.getClass().getDeclaredMethod("getExtraInfo", new Class[0]);
                m.setAccessible(true);
                this.accessor = m;
            }
            catch (NoSuchMethodException | SecurityException ex) {
                Exceptions.printStackTrace((Throwable)ex);
                this.reported = true;
            }
        }
        if (this.accessor != null) {
            try {
                Object o = this.accessor.invoke((Object)f, new Object[0]);
                if (o instanceof ResourceStringFoldInfo) {
                    return (ResourceStringFoldInfo)o;
                }
            }
            catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                Exceptions.printStackTrace((Throwable)ex);
                this.reported = true;
            }
        }
        return null;
    }

    public CharSequence read(Document d, Fold f, FoldTemplate ft) throws BadLocationException {
        Properties props;
        FileObject anchor;
        ResourceStringFoldInfo info = this.access(f);
        if (info == null) {
            return null;
        }
        String resName = info.getResourceName();
        Object stream = d.getProperty("stream");
        if (stream instanceof DataObject) {
            anchor = ((DataObject)stream).getPrimaryFile();
        } else if (stream instanceof FileObject) {
            anchor = (FileObject)stream;
        } else {
            return null;
        }
        ClassPath cp = ClassPath.getClassPath((FileObject)anchor, (String)"classpath/source");
        FileObject root = cp.findOwnerRoot(anchor);
        if (root == null) {
            return null;
        }
        FileObject bundleFile = cp.findResource(resName);
        try {
            InputStream i = bundleFile.getInputStream();
            Throwable throwable = null;
            try {
                props = new Properties();
                props.load(i);
            }
            catch (Throwable x2) {
                throwable = x2;
                throw x2;
            }
            finally {
                if (i != null) {
                    if (throwable != null) {
                        try {
                            i.close();
                        }
                        catch (Throwable x2) {
                            throwable.addSuppressed(x2);
                        }
                    } else {
                        i.close();
                    }
                }
            }
        }
        catch (IOException ex) {
            return null;
        }
        String content = props.getProperty(info.getKey());
        if (content == null) {
            return null;
        }
        content = this.loader.getMessage(bundleFile, info.getKey());
        return "\"" + content + "\"";
    }

    public static class F
    implements ContentReader.Factory {
        public ContentReader createReader(FoldType ft) {
            if (ft == JavaFoldTypeProvider.BUNDLE_STRING) {
                return new ResourceContentReader();
            }
            return null;
        }
    }

}

