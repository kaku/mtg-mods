/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package org.netbeans.modules.java.editor.options;

import java.util.prefs.Preferences;
import org.netbeans.modules.java.editor.options.MarkOccurencesOptionsPanelController;
import org.openide.util.NbPreferences;

public class MarkOccurencesSettings {
    private static final String MARK_OCCURENCES = "MarkOccurences";
    public static String ON_OFF = "OnOff";
    public static String TYPES = "Types";
    public static String METHODS = "Methods";
    public static String CONSTANTS = "Constants";
    public static String FIELDS = "Fields";
    public static String LOCAL_VARIABLES = "LocalVariables";
    public static String EXCEPTIONS = "Exceptions";
    public static String EXIT = "Exit";
    public static String IMPLEMENTS = "Implements";
    public static String OVERRIDES = "Overrides";
    public static String BREAK_CONTINUE = "BreakContinue";
    public static String KEEP_MARKS = "KeepMarks";

    private MarkOccurencesSettings() {
    }

    public static Preferences getCurrentNode() {
        Preferences preferences = NbPreferences.forModule(MarkOccurencesOptionsPanelController.class);
        return preferences.node("MarkOccurences").node(MarkOccurencesSettings.getCurrentProfileId());
    }

    private static String getCurrentProfileId() {
        return "default";
    }
}

