/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.AnnotatedTypeTree
 *  com.sun.source.tree.ArrayTypeTree
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.BreakTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ContinueTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.LabeledStatementTree
 *  com.sun.source.tree.MemberReferenceTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.ParameterizedTypeTree
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 */
package org.netbeans.modules.java.editor.semantic;

import com.sun.source.tree.AnnotatedTypeTree;
import com.sun.source.tree.ArrayTypeTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.BreakTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ContinueTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.LabeledStatementTree;
import com.sun.source.tree.MemberReferenceTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.ParameterizedTypeTree;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;

public class Utilities {
    private static final Logger LOG = Logger.getLogger(Utilities.class.getName());
    @Deprecated
    private static final boolean DEBUG = false;
    private static final Map<Class, List<Tree.Kind>> class2Kind = new HashMap<Class, List<Tree.Kind>>();
    private static final Set<String> keywords;
    private static final Set<String> nonCtorKeywords;
    private static final Set<ElementKind> LOCAL_ELEMENT_KINDS;

    private static Token<JavaTokenId> findTokenWithText(CompilationInfo info, String text, int start, int end) {
        TokenHierarchy th = info.getTokenHierarchy();
        TokenSequence ts = th.tokenSequence(JavaTokenId.language()).subSequence(start, end);
        while (ts.moveNext()) {
            Token t = ts.token();
            if (t.id() != JavaTokenId.IDENTIFIER) continue;
            boolean nameMatches = text.equals(info.getTreeUtilities().decodeIdentifier(t.text()).toString());
            if (!nameMatches) {
                ExpressionTree expr = info.getTreeUtilities().parseExpression(t.text().toString(), new SourcePositions[1]);
                boolean bl = nameMatches = expr.getKind() == Tree.Kind.IDENTIFIER && text.contentEquals(((IdentifierTree)expr).getName());
            }
            if (!nameMatches) continue;
            return t;
        }
        return null;
    }

    private static Tree normalizeLastLeftTree(Tree lastLeft) {
        while (lastLeft != null && lastLeft.getKind() == Tree.Kind.ARRAY_TYPE) {
            lastLeft = ((ArrayTypeTree)lastLeft).getType();
        }
        return lastLeft;
    }

    private static Token<JavaTokenId> findIdentifierSpanImpl(CompilationInfo info, Tree decl, Tree lastLeft, List<? extends Tree> firstRight, String name, CompilationUnitTree cu, SourcePositions positions) {
        int start;
        int declStart = (int)positions.getStartPosition(cu, decl);
        int n = start = (lastLeft = Utilities.normalizeLastLeftTree(lastLeft)) != null ? (int)positions.getEndPosition(cu, lastLeft) : declStart;
        if (start == -1 && (start = declStart) == -1) {
            return null;
        }
        int end = (int)positions.getEndPosition(cu, decl);
        for (Tree t : firstRight) {
            int proposedEnd;
            if (t == null || (proposedEnd = (int)positions.getStartPosition(cu, t)) == -1 || proposedEnd >= end) continue;
            end = proposedEnd;
        }
        if (end == -1) {
            return null;
        }
        if (start > end) {
            start = (int)positions.getStartPosition(cu, decl);
        }
        return Utilities.findTokenWithText(info, name, start, end);
    }

    private static Token<JavaTokenId> findIdentifierSpanImpl(CompilationInfo info, MemberSelectTree tree, CompilationUnitTree cu, SourcePositions positions) {
        int start = (int)positions.getStartPosition(cu, (Tree)tree);
        int endPosition = (int)positions.getEndPosition(cu, (Tree)tree);
        if (start == -1 || endPosition == -1) {
            return null;
        }
        String member = tree.getIdentifier().toString();
        TokenHierarchy th = info.getTokenHierarchy();
        TokenSequence ts = th.tokenSequence(JavaTokenId.language());
        if (ts.move(endPosition) == Integer.MAX_VALUE) {
            return null;
        }
        if (ts.moveNext()) {
            while (ts.offset() >= start) {
                Token t = ts.token();
                if (t.id() == JavaTokenId.IDENTIFIER && member.equals(info.getTreeUtilities().decodeIdentifier(t.text()).toString())) {
                    return t;
                }
                if (ts.movePrevious()) continue;
                break;
            }
        }
        return null;
    }

    private static Token<JavaTokenId> findIdentifierSpanImpl(CompilationInfo info, MemberReferenceTree tree, CompilationUnitTree cu, SourcePositions positions) {
        int start = (int)positions.getStartPosition(cu, (Tree)tree);
        int endPosition = (int)positions.getEndPosition(cu, (Tree)tree);
        if (start == -1 || endPosition == -1) {
            return null;
        }
        String member = tree.getName().toString();
        TokenHierarchy th = info.getTokenHierarchy();
        TokenSequence ts = th.tokenSequence(JavaTokenId.language());
        if (ts.move(endPosition) == Integer.MAX_VALUE) {
            return null;
        }
        if (ts.moveNext()) {
            while (ts.offset() >= start) {
                Token t = ts.token();
                if (t.id() == JavaTokenId.IDENTIFIER && member.equals(info.getTreeUtilities().decodeIdentifier(t.text()).toString())) {
                    return t;
                }
                if (ts.movePrevious()) continue;
                break;
            }
        }
        return null;
    }

    private static Token<JavaTokenId> findIdentifierSpanImpl(CompilationInfo info, IdentifierTree tree, CompilationUnitTree cu, SourcePositions positions) {
        int start = (int)positions.getStartPosition(cu, (Tree)tree);
        int endPosition = (int)positions.getEndPosition(cu, (Tree)tree);
        if (start == -1 || endPosition == -1) {
            return null;
        }
        TokenHierarchy th = info.getTokenHierarchy();
        TokenSequence ts = th.tokenSequence(JavaTokenId.language());
        if (ts.move(start) == Integer.MAX_VALUE) {
            return null;
        }
        if (ts.moveNext() && ts.offset() >= start) {
            Token t = ts.token();
            return t;
        }
        return null;
    }

    private static Token<JavaTokenId> findIdentifierSpanImpl(CompilationInfo info, TreePath decl) {
        if (info.getTreeUtilities().isSynthetic(decl)) {
            return null;
        }
        Tree leaf = decl.getLeaf();
        if (class2Kind.get(MethodTree.class).contains((Object)leaf.getKind())) {
            MethodTree method = (MethodTree)leaf;
            ArrayList<BlockTree> rightTrees = new ArrayList<BlockTree>();
            rightTrees.addAll(method.getParameters());
            rightTrees.addAll(method.getThrows());
            rightTrees.add(method.getBody());
            Name name = method.getName();
            if (method.getReturnType() == null) {
                name = ((ClassTree)decl.getParentPath().getLeaf()).getSimpleName();
            }
            return Utilities.findIdentifierSpanImpl(info, leaf, method.getReturnType(), rightTrees, name.toString(), info.getCompilationUnit(), info.getTrees().getSourcePositions());
        }
        if (class2Kind.get(VariableTree.class).contains((Object)leaf.getKind())) {
            VariableTree var = (VariableTree)leaf;
            boolean typeSynthetic = var.getType() == null || info.getTreeUtilities().isSynthetic(new TreePath(decl, var.getType()));
            return Utilities.findIdentifierSpanImpl(info, leaf, typeSynthetic ? null : var.getType(), Collections.singletonList(var.getInitializer()), var.getName().toString(), info.getCompilationUnit(), info.getTrees().getSourcePositions());
        }
        if (class2Kind.get(MemberSelectTree.class).contains((Object)leaf.getKind())) {
            return Utilities.findIdentifierSpanImpl(info, (MemberSelectTree)leaf, info.getCompilationUnit(), info.getTrees().getSourcePositions());
        }
        if (class2Kind.get(MemberReferenceTree.class).contains((Object)leaf.getKind())) {
            return Utilities.findIdentifierSpanImpl(info, (MemberReferenceTree)leaf, info.getCompilationUnit(), info.getTrees().getSourcePositions());
        }
        if (class2Kind.get(ClassTree.class).contains((Object)leaf.getKind())) {
            int start;
            String name = ((ClassTree)leaf).getSimpleName().toString();
            if (name.length() == 0) {
                return null;
            }
            SourcePositions positions = info.getTrees().getSourcePositions();
            CompilationUnitTree cu = info.getCompilationUnit();
            ModifiersTree mods = ((ClassTree)leaf).getModifiers();
            int n = start = mods != null ? (int)positions.getEndPosition(cu, (Tree)mods) : -1;
            if (start == -1) {
                start = (int)positions.getStartPosition(cu, leaf);
            }
            int end = (int)positions.getEndPosition(cu, leaf);
            if (start == -1 || end == -1) {
                return null;
            }
            return Utilities.findTokenWithText(info, name, start, end);
        }
        if (class2Kind.get(IdentifierTree.class).contains((Object)leaf.getKind())) {
            return Utilities.findIdentifierSpanImpl(info, (IdentifierTree)leaf, info.getCompilationUnit(), info.getTrees().getSourcePositions());
        }
        if (class2Kind.get(ParameterizedTypeTree.class).contains((Object)leaf.getKind())) {
            return Utilities.findIdentifierSpanImpl(info, new TreePath(decl, ((ParameterizedTypeTree)leaf).getType()));
        }
        if (class2Kind.get(AnnotatedTypeTree.class).contains((Object)leaf.getKind())) {
            return Utilities.findIdentifierSpanImpl(info, new TreePath(decl, (Tree)((AnnotatedTypeTree)leaf).getUnderlyingType()));
        }
        if (class2Kind.get(BreakTree.class).contains((Object)leaf.getKind())) {
            Name name = ((BreakTree)leaf).getLabel();
            if (name == null || name.length() == 0) {
                return null;
            }
            SourcePositions positions = info.getTrees().getSourcePositions();
            CompilationUnitTree cu = info.getCompilationUnit();
            int start = (int)positions.getStartPosition(cu, leaf);
            int end = (int)positions.getEndPosition(cu, leaf);
            if (start == -1 || end == -1) {
                return null;
            }
            return Utilities.findTokenWithText(info, name.toString(), start, end);
        }
        if (class2Kind.get(ContinueTree.class).contains((Object)leaf.getKind())) {
            Name name = ((ContinueTree)leaf).getLabel();
            if (name == null || name.length() == 0) {
                return null;
            }
            SourcePositions positions = info.getTrees().getSourcePositions();
            CompilationUnitTree cu = info.getCompilationUnit();
            int start = (int)positions.getStartPosition(cu, leaf);
            int end = (int)positions.getEndPosition(cu, leaf);
            if (start == -1 || end == -1) {
                return null;
            }
            return Utilities.findTokenWithText(info, name.toString(), start, end);
        }
        if (class2Kind.get(LabeledStatementTree.class).contains((Object)leaf.getKind())) {
            Name name = ((LabeledStatementTree)leaf).getLabel();
            if (name == null || name.length() == 0) {
                return null;
            }
            SourcePositions positions = info.getTrees().getSourcePositions();
            CompilationUnitTree cu = info.getCompilationUnit();
            int start = (int)positions.getStartPosition(cu, leaf);
            int end = (int)positions.getStartPosition(cu, (Tree)((LabeledStatementTree)leaf).getStatement());
            if (start == -1 || end == -1) {
                return null;
            }
            return Utilities.findTokenWithText(info, name.toString(), start, end);
        }
        throw new IllegalArgumentException("Only MethodDecl, VariableDecl, MemberSelectTree, IdentifierTree, ParameterizedTypeTree, AnnotatedTypeTree, ClassDecl, BreakTree, ContinueTree, and LabeledStatementTree are accepted by this method. Got: " + (Object)leaf.getKind());
    }

    public static int[] findIdentifierSpan(final TreePath decl, final CompilationInfo info, final Document doc) {
        final int[] result = new int[]{-1, -1};
        Runnable r = new Runnable(){

            @Override
            public void run() {
                Token<JavaTokenId> t = Utilities.findIdentifierSpan(info, doc, decl);
                if (t != null) {
                    result[0] = t.offset(null);
                    result[1] = t.offset(null) + t.length();
                }
            }
        };
        if (doc != null) {
            doc.render(r);
        } else {
            r.run();
        }
        return result;
    }

    public static Token<JavaTokenId> findIdentifierSpan(final CompilationInfo info, Document doc, final TreePath decl) {
        final Token[] result = new Token[1];
        Runnable r = new Runnable(){

            @Override
            public void run() {
                result[0] = Utilities.findIdentifierSpanImpl(info, decl);
            }
        };
        if (doc != null) {
            doc.render(r);
        } else {
            r.run();
        }
        return result[0];
    }

    private static int findBodyStartImpl(Tree cltree, CompilationUnitTree cu, SourcePositions positions, Document doc) {
        int start = (int)positions.getStartPosition(cu, cltree);
        int end = (int)positions.getEndPosition(cu, cltree);
        if (start == -1 || end == -1) {
            return -1;
        }
        if (start > doc.getLength() || end > doc.getLength()) {
            return -1;
        }
        try {
            String text = doc.getText(start, end - start);
            int index = text.indexOf(123);
            if (index == -1) {
                return -1;
            }
            return start + index;
        }
        catch (BadLocationException e) {
            LOG.log(Level.INFO, null, e);
            return -1;
        }
    }

    public static int findBodyStart(final Tree cltree, final CompilationUnitTree cu, final SourcePositions positions, final Document doc) {
        Tree.Kind kind = cltree.getKind();
        if (!TreeUtilities.CLASS_TREE_KINDS.contains((Object)kind) && kind != Tree.Kind.METHOD) {
            throw new IllegalArgumentException("Unsupported kind: " + (Object)kind);
        }
        final int[] result = new int[1];
        doc.render(new Runnable(){

            @Override
            public void run() {
                result[0] = Utilities.findBodyStartImpl(cltree, cu, positions, doc);
            }
        });
        return result[0];
    }

    private static int findLastBracketImpl(Tree tree, CompilationUnitTree cu, SourcePositions positions, Document doc) {
        int start = (int)positions.getStartPosition(cu, tree);
        int end = (int)positions.getEndPosition(cu, tree);
        if (start == -1 || end == -1) {
            return -1;
        }
        if (start > doc.getLength() || end > doc.getLength()) {
            return -1;
        }
        try {
            String text = doc.getText(end - 1, 1);
            if (text.charAt(0) == '}') {
                return end - 1;
            }
        }
        catch (BadLocationException e) {
            LOG.log(Level.INFO, null, e);
        }
        return -1;
    }

    public static int findLastBracket(final Tree tree, final CompilationUnitTree cu, final SourcePositions positions, final Document doc) {
        final int[] result = new int[1];
        doc.render(new Runnable(){

            @Override
            public void run() {
                result[0] = Utilities.findLastBracketImpl(tree, cu, positions, doc);
            }
        });
        return result[0];
    }

    private static Token<JavaTokenId> createHighlightImpl(CompilationInfo info, Document doc, TreePath tree) {
        Tree leaf = tree.getLeaf();
        SourcePositions positions = info.getTrees().getSourcePositions();
        CompilationUnitTree cu = info.getCompilationUnit();
        if (leaf instanceof MethodTree || leaf instanceof VariableTree || leaf instanceof ClassTree || leaf instanceof MemberSelectTree || leaf instanceof AnnotatedTypeTree || leaf instanceof MemberReferenceTree) {
            return Utilities.findIdentifierSpan(info, doc, tree);
        }
        int start = (int)positions.getStartPosition(cu, leaf);
        int end = (int)positions.getEndPosition(cu, leaf);
        if ((long)start == -1 || (long)end == -1) {
            return null;
        }
        TokenHierarchy th = info.getTokenHierarchy();
        TokenSequence ts = th.tokenSequence(JavaTokenId.language());
        if (ts.move(start) == Integer.MAX_VALUE) {
            return null;
        }
        if (ts.moveNext()) {
            Token token = ts.token();
            if (ts.offset() == start && token != null) {
                JavaTokenId id = (JavaTokenId)token.id();
                if (id == JavaTokenId.IDENTIFIER) {
                    return token;
                }
                if (id == JavaTokenId.THIS || id == JavaTokenId.SUPER) {
                    return ts.offsetToken();
                }
            }
        }
        return null;
    }

    public static Token<JavaTokenId> getToken(final CompilationInfo info, final Document doc, final TreePath tree) {
        final Token[] result = new Token[1];
        doc.render(new Runnable(){

            @Override
            public void run() {
                result[0] = Utilities.createHighlightImpl(info, doc, tree);
            }
        });
        return result[0];
    }

    public static boolean isKeyword(Tree tree) {
        if (tree.getKind() == Tree.Kind.IDENTIFIER) {
            return keywords.contains(((IdentifierTree)tree).getName().toString());
        }
        if (tree.getKind() == Tree.Kind.MEMBER_SELECT) {
            return keywords.contains(((MemberSelectTree)tree).getIdentifier().toString());
        }
        return false;
    }

    public static boolean isNonCtorKeyword(Tree tree) {
        if (tree.getKind() == Tree.Kind.IDENTIFIER) {
            return nonCtorKeywords.contains(((IdentifierTree)tree).getName().toString());
        }
        if (tree.getKind() == Tree.Kind.MEMBER_SELECT) {
            return nonCtorKeywords.contains(((MemberSelectTree)tree).getIdentifier().toString());
        }
        return false;
    }

    public static boolean isPrivateElement(Element el) {
        return LOCAL_ELEMENT_KINDS.contains((Object)el.getKind()) || el.getModifiers().contains((Object)Modifier.PRIVATE);
    }

    static {
        for (Tree.Kind k : Tree.Kind.values()) {
            Class c = k.asInterface();
            List<Tree.Kind> kinds = class2Kind.get(c);
            if (kinds == null) {
                kinds = new ArrayList<Tree.Kind>();
                class2Kind.put(c, kinds);
            }
            kinds.add(k);
        }
        keywords = new HashSet<String>();
        keywords.add("true");
        keywords.add("false");
        keywords.add("null");
        keywords.add("this");
        keywords.add("super");
        keywords.add("class");
        nonCtorKeywords = new HashSet<String>(keywords);
        nonCtorKeywords.remove("this");
        nonCtorKeywords.remove("super");
        LOCAL_ELEMENT_KINDS = EnumSet.of(ElementKind.PARAMETER, ElementKind.LOCAL_VARIABLE, ElementKind.EXCEPTION_PARAMETER, ElementKind.RESOURCE_VARIABLE);
    }

}

