/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.AnnotationTree
 *  com.sun.source.tree.BinaryTree
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.LiteralTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.ReturnTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.ModificationResult
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.java.source.TreeMaker
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.WorkingCopy
 *  org.netbeans.editor.Utilities
 *  org.netbeans.spi.editor.codegen.CodeGenerator
 *  org.netbeans.spi.editor.codegen.CodeGenerator$Factory
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.codegen;

import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.BinaryTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.ReturnTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.awt.Dialog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.Elements;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.TreeMaker;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.java.editor.codegen.GeneratorUtils;
import org.netbeans.modules.java.editor.codegen.ui.ElementNode;
import org.netbeans.modules.java.editor.codegen.ui.ToStringPanel;
import org.netbeans.spi.editor.codegen.CodeGenerator;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class ToStringGenerator
implements CodeGenerator {
    private JTextComponent component;
    private ElementNode.Description description;

    private ToStringGenerator(JTextComponent component, ElementNode.Description description) {
        this.component = component;
        this.description = description;
    }

    public String getDisplayName() {
        return NbBundle.getMessage(ToStringGenerator.class, (String)"LBL_tostring");
    }

    public void invoke() {
        final int caretOffset = this.component.getCaretPosition();
        final ToStringPanel panel = new ToStringPanel(this.description);
        DialogDescriptor dialogDescriptor = GeneratorUtils.createDialogDescriptor(panel, NbBundle.getMessage(ToStringGenerator.class, (String)"LBL_generate_tostring"));
        Dialog dialog = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
        dialog.setVisible(true);
        if (dialogDescriptor.getValue() != dialogDescriptor.getDefaultValue()) {
            return;
        }
        JavaSource js = JavaSource.forDocument((Document)this.component.getDocument());
        if (js != null) {
            try {
                ModificationResult mr = js.runModificationTask((Task)new Task<WorkingCopy>(){

                    public void run(WorkingCopy copy) throws IOException {
                        copy.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                        Element e = ToStringGenerator.this.description.getElementHandle().resolve((CompilationInfo)copy);
                        TreePath path = e != null ? copy.getTrees().getPath(e) : copy.getTreeUtilities().pathFor(caretOffset);
                        path = org.netbeans.modules.editor.java.Utilities.getPathElementOfKind(TreeUtilities.CLASS_TREE_KINDS, path);
                        if (path == null) {
                            String message = NbBundle.getMessage(ToStringGenerator.class, (String)"ERR_CannotFindOriginalClass");
                            Utilities.setStatusBoldText((JTextComponent)ToStringGenerator.this.component, (String)message);
                        } else {
                            ClassTree cls = (ClassTree)path.getLeaf();
                            ArrayList<VariableElement> fields = new ArrayList<VariableElement>();
                            for (ElementHandle<? extends Element> elementHandle : panel.getVariables()) {
                                VariableElement field = (VariableElement)elementHandle.resolve((CompilationInfo)copy);
                                if (field == null) {
                                    return;
                                }
                                fields.add(field);
                            }
                            MethodTree mth = ToStringGenerator.createToStringMethod(copy, fields, cls.getSimpleName().toString());
                            copy.rewrite((Tree)cls, (Tree)GeneratorUtils.insertClassMembers(copy, cls, Collections.singletonList(mth), caretOffset));
                        }
                    }
                });
                GeneratorUtils.guardedCommit(this.component, mr);
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
    }

    private static MethodTree createToStringMethod(WorkingCopy wc, Iterable<? extends VariableElement> fields, String typeName) {
        TypeElement override;
        TreeMaker make = wc.getTreeMaker();
        EnumSet<Modifier> mods = EnumSet.of(Modifier.PUBLIC);
        LinkedList<AnnotationTree> annotations = new LinkedList<AnnotationTree>();
        if (GeneratorUtils.supportsOverride((CompilationInfo)wc) && (override = wc.getElements().getTypeElement("java.lang.Override")) != null) {
            annotations.add(wc.getTreeMaker().Annotation((Tree)wc.getTreeMaker().QualIdent((Element)override), Collections.emptyList()));
        }
        ModifiersTree modifiers = make.Modifiers(mods, annotations);
        LiteralTree exp = make.Literal((Object)(typeName + '{'));
        boolean first = true;
        for (VariableElement variableElement : fields) {
            StringBuilder sb = new StringBuilder();
            if (!first) {
                sb.append(", ");
            }
            sb.append(variableElement.getSimpleName().toString()).append('=');
            exp = make.Binary(Tree.Kind.PLUS, (ExpressionTree)exp, (ExpressionTree)make.Literal((Object)sb.toString()));
            exp = make.Binary(Tree.Kind.PLUS, (ExpressionTree)exp, (ExpressionTree)make.Identifier((CharSequence)variableElement.getSimpleName()));
            first = false;
        }
        ReturnTree stat = make.Return((ExpressionTree)make.Binary(Tree.Kind.PLUS, (ExpressionTree)exp, (ExpressionTree)make.Literal((Object)Character.valueOf('}'))));
        BlockTree body = make.Block(Collections.singletonList(stat), false);
        return make.Method(modifiers, (CharSequence)"toString", (Tree)make.Identifier((CharSequence)"String"), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), body, null);
    }

    public static class Factory
    implements CodeGenerator.Factory {
        private static final String ERROR = "<error>";

        public List<? extends CodeGenerator> create(Lookup context) {
            ArrayList<ToStringGenerator> ret = new ArrayList<ToStringGenerator>();
            JTextComponent component = (JTextComponent)context.lookup(JTextComponent.class);
            CompilationController controller = (CompilationController)context.lookup(CompilationController.class);
            TreePath path = (TreePath)context.lookup(TreePath.class);
            TreePath treePath = path = path != null ? org.netbeans.modules.editor.java.Utilities.getPathElementOfKind(TreeUtilities.CLASS_TREE_KINDS, path) : null;
            if (component == null || controller == null || path == null) {
                return ret;
            }
            try {
                controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
            }
            catch (IOException ioe) {
                return ret;
            }
            TypeElement typeElement = (TypeElement)controller.getTrees().getElement(path);
            if (typeElement == null || !typeElement.getKind().isClass()) {
                return ret;
            }
            ArrayList<ElementNode.Description> descriptions = new ArrayList<ElementNode.Description>();
            for (Element element : typeElement.getEnclosedElements()) {
                switch (element.getKind()) {
                    case METHOD: {
                        if (!element.getSimpleName().contentEquals("toString") || !((ExecutableElement)element).getParameters().isEmpty()) break;
                        return ret;
                    }
                    case FIELD: {
                        if ("<error>".contentEquals(element.getSimpleName()) || element.getModifiers().contains((Object)Modifier.STATIC)) break;
                        descriptions.add(ElementNode.Description.create((CompilationInfo)controller, element, null, true, true));
                    }
                }
            }
            ret.add(new ToStringGenerator(component, ElementNode.Description.create((CompilationInfo)controller, typeElement, descriptions, false, false)));
            return ret;
        }
    }

}

