/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.AnnotationTree
 *  com.sun.source.tree.ArrayAccessTree
 *  com.sun.source.tree.ArrayTypeTree
 *  com.sun.source.tree.AssertTree
 *  com.sun.source.tree.AssignmentTree
 *  com.sun.source.tree.BinaryTree
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.CaseTree
 *  com.sun.source.tree.CatchTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.CompoundAssignmentTree
 *  com.sun.source.tree.ConditionalExpressionTree
 *  com.sun.source.tree.EnhancedForLoopTree
 *  com.sun.source.tree.ExpressionStatementTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.ForLoopTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.InstanceOfTree
 *  com.sun.source.tree.LambdaExpressionTree
 *  com.sun.source.tree.MemberReferenceTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodInvocationTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.NewArrayTree
 *  com.sun.source.tree.NewClassTree
 *  com.sun.source.tree.ParameterizedTypeTree
 *  com.sun.source.tree.ParenthesizedTree
 *  com.sun.source.tree.ReturnTree
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.ThrowTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.TypeCastTree
 *  com.sun.source.tree.TypeParameterTree
 *  com.sun.source.tree.UnaryTree
 *  com.sun.source.tree.UnionTypeTree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.tree.WildcardTree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.JavaParserResultTask
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.TreePathHandle
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.support.CancellableTreePathScanner
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.modules.editor.NbEditorUtilities
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.netbeans.modules.parsing.spi.TaskIndexingMode
 *  org.netbeans.spi.editor.highlighting.support.OffsetsBag
 *  org.netbeans.spi.editor.hints.ErrorDescription
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 */
package org.netbeans.modules.java.editor.semantic;

import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.ArrayAccessTree;
import com.sun.source.tree.ArrayTypeTree;
import com.sun.source.tree.AssertTree;
import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BinaryTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.CaseTree;
import com.sun.source.tree.CatchTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.CompoundAssignmentTree;
import com.sun.source.tree.ConditionalExpressionTree;
import com.sun.source.tree.EnhancedForLoopTree;
import com.sun.source.tree.ExpressionStatementTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.ForLoopTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.InstanceOfTree;
import com.sun.source.tree.LambdaExpressionTree;
import com.sun.source.tree.MemberReferenceTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.NewArrayTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.ParameterizedTypeTree;
import com.sun.source.tree.ParenthesizedTree;
import com.sun.source.tree.ReturnTree;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.ThrowTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TypeCastTree;
import com.sun.source.tree.TypeParameterTree;
import com.sun.source.tree.UnaryTree;
import com.sun.source.tree.UnionTypeTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.tree.WildcardTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.JavaParserResultTask;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.TreePathHandle;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.support.CancellableTreePathScanner;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.modules.java.editor.imports.UnusedImports;
import org.netbeans.modules.java.editor.semantic.ColoringAttributes;
import org.netbeans.modules.java.editor.semantic.ColoringManager;
import org.netbeans.modules.java.editor.semantic.LexerBasedHighlightLayer;
import org.netbeans.modules.java.editor.semantic.TokenList;
import org.netbeans.modules.java.editor.semantic.Utilities;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.TaskIndexingMode;
import org.netbeans.spi.editor.highlighting.support.OffsetsBag;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;

public class SemanticHighlighter
extends JavaParserResultTask {
    private FileObject file;
    private AtomicBoolean cancel = new AtomicBoolean();
    static ErrorDescriptionSetter ERROR_DESCRIPTION_SETTER = new ErrorDescriptionSetter(){

        @Override
        public void setErrors(Document doc, List<ErrorDescription> errors, List<TreePathHandle> allUnusedImports) {
        }

        @Override
        public void setHighlights(final Document doc, final OffsetsBag highlights) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    SemanticHighlighter.getImportHighlightsBag(doc).setHighlights(highlights);
                }
            });
        }

        @Override
        public void setColorings(final Document doc, final Map<Token, ColoringAttributes.Coloring> colorings, final Set<Token> addedTokens, final Set<Token> removedTokens) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    LexerBasedHighlightLayer.getLayer(SemanticHighlighter.class, doc).setColorings(colorings, addedTokens, removedTokens);
                }
            });
        }

    };
    private static final Object KEY_UNUSED_IMPORTS = new Object();

    public static List<TreePathHandle> computeUnusedImports(CompilationInfo info) throws IOException {
        ArrayList<TreePathHandle> result = new ArrayList<TreePathHandle>();
        for (TreePath unused : UnusedImports.process(info, new AtomicBoolean())) {
            result.add(TreePathHandle.create((TreePath)unused, (CompilationInfo)info));
        }
        return result;
    }

    SemanticHighlighter(FileObject file) {
        super(JavaSource.Phase.RESOLVED, TaskIndexingMode.ALLOWED_DURING_SCAN);
        this.file = file;
    }

    public void run(Parser.Result result, SchedulerEvent event) {
        CompilationInfo info = CompilationInfo.get((Parser.Result)result);
        if (info == null) {
            return;
        }
        this.cancel.set(false);
        Document doc = result.getSnapshot().getSource().getDocument(false);
        if (!SemanticHighlighter.verifyDocument(doc)) {
            return;
        }
        if (this.process(info, doc)) {
            // empty if block
        }
    }

    private static boolean verifyDocument(final Document doc) {
        if (doc == null) {
            Logger.getLogger(SemanticHighlighter.class.getName()).log(Level.FINE, "SemanticHighlighter: Cannot get document!");
            return false;
        }
        final boolean[] tokenSequenceNull = new boolean[1];
        doc.render(new Runnable(){

            @Override
            public void run() {
                tokenSequenceNull[0] = TokenHierarchy.get((Document)doc).tokenSequence() == null;
            }
        });
        if (tokenSequenceNull[0]) {
            return false;
        }
        return true;
    }

    public void cancel() {
        this.cancel.set(true);
    }

    public int getPriority() {
        return 100;
    }

    public Class<? extends Scheduler> getSchedulerClass() {
        return Scheduler.EDITOR_SENSITIVE_TASK_SCHEDULER;
    }

    boolean process(CompilationInfo info, Document doc) {
        return this.process(info, doc, ERROR_DESCRIPTION_SETTER);
    }

    static ColoringAttributes.Coloring collection2Coloring(Collection<ColoringAttributes> attr) {
        ColoringAttributes.Coloring c = ColoringAttributes.empty();
        for (ColoringAttributes a : attr) {
            c = ColoringAttributes.add(c, a);
        }
        return c;
    }

    boolean process(CompilationInfo info, Document doc, ErrorDescriptionSetter setter) {
        OffsetsBag imports;
        DetectorVisitor v = new DetectorVisitor(info, doc, this.cancel);
        long start = System.currentTimeMillis();
        IdentityHashMap<Token, ColoringAttributes.Coloring> newColoring = new IdentityHashMap<Token, ColoringAttributes.Coloring>();
        ArrayList<ErrorDescription> errors = new ArrayList<ErrorDescription>();
        CompilationUnitTree cu = info.getCompilationUnit();
        v.scan((Tree)cu, (Object)null);
        if (this.cancel.get()) {
            return true;
        }
        boolean computeUnusedImports = "text/x-java".equals(FileUtil.getMIMEType((FileObject)info.getFileObject()));
        ArrayList<TreePathHandle> allUnusedImports = computeUnusedImports ? new ArrayList<TreePathHandle>() : null;
        OffsetsBag offsetsBag = imports = computeUnusedImports ? new OffsetsBag(doc) : null;
        if (computeUnusedImports) {
            ColoringAttributes.Coloring unused = ColoringAttributes.add(ColoringAttributes.empty(), ColoringAttributes.UNUSED);
            Collection<TreePath> unusedImports = UnusedImports.process(info, this.cancel);
            if (unusedImports == null) {
                return true;
            }
            for (TreePath tree : unusedImports) {
                if (this.cancel.get()) {
                    return true;
                }
                int startPos = (int)info.getTrees().getSourcePositions().getStartPosition(cu, tree.getLeaf());
                int endPos = (int)info.getTrees().getSourcePositions().getEndPosition(cu, tree.getLeaf());
                imports.addHighlight(startPos, endPos, ColoringManager.getColoringImpl(unused));
                TreePathHandle handle = TreePathHandle.create((TreePath)tree, (CompilationInfo)info);
                allUnusedImports.add(handle);
            }
        }
        Map<Token, ColoringAttributes.Coloring> oldColors = LexerBasedHighlightLayer.getLayer(SemanticHighlighter.class, doc).getColorings();
        IdentityHashMap<Token, ColoringAttributes.Coloring> removedTokens = new IdentityHashMap<Token, ColoringAttributes.Coloring>(oldColors);
        HashSet<Token> addedTokens = new HashSet<Token>();
        for (Element decl : v.type2Uses.keySet()) {
            if (this.cancel.get()) {
                return true;
            }
            List uses = (List)v.type2Uses.get(decl);
            for (Use u : uses) {
                if (u.spec == null) continue;
                if (u.type.contains((Object)UseTypes.DECLARATION) && Utilities.isPrivateElement(decl)) {
                    if ((decl.getKind().isField() && !SemanticHighlighter.isSerialVersionUID(info, decl) || SemanticHighlighter.isLocalVariableClosure(decl)) && !this.hasAllTypes(uses, EnumSet.of(UseTypes.READ, UseTypes.WRITE))) {
                        u.spec.add(ColoringAttributes.UNUSED);
                    }
                    if ((decl.getKind() == ElementKind.CONSTRUCTOR && !decl.getModifiers().contains((Object)Modifier.PRIVATE) || decl.getKind() == ElementKind.METHOD) && !this.hasAllTypes(uses, EnumSet.of(UseTypes.EXECUTE))) {
                        u.spec.add(ColoringAttributes.UNUSED);
                    }
                    if ((decl.getKind().isClass() || decl.getKind().isInterface()) && !this.hasAllTypes(uses, EnumSet.of(UseTypes.CLASS_USE))) {
                        u.spec.add(ColoringAttributes.UNUSED);
                    }
                }
                ColoringAttributes.Coloring c = SemanticHighlighter.collection2Coloring(u.spec);
                Token t = (Token)v.tree2Token.get((Object)u.tree.getLeaf());
                if (t == null) continue;
                newColoring.put(t, c);
                ColoringAttributes.Coloring oldColoring = removedTokens.remove((Object)t);
                if (oldColoring != null && oldColoring.equals(c)) continue;
                addedTokens.add(t);
            }
        }
        if (this.cancel.get()) {
            return true;
        }
        if (computeUnusedImports) {
            setter.setErrors(doc, errors, allUnusedImports);
            setter.setHighlights(doc, imports);
        }
        setter.setColorings(doc, newColoring, addedTokens, removedTokens.keySet());
        Logger.getLogger("TIMER").log(Level.FINE, "Semantic", new Object[]{NbEditorUtilities.getFileObject((Document)doc), System.currentTimeMillis() - start});
        return false;
    }

    private boolean hasAllTypes(List<Use> uses, Collection<UseTypes> types) {
        EnumSet<UseTypes> e = EnumSet.copyOf(types);
        for (Use u : uses) {
            if (types.isEmpty()) {
                return true;
            }
            types.removeAll(u.type);
        }
        return types.isEmpty();
    }

    private static boolean isLocalVariableClosure(Element el) {
        return el.getKind() == ElementKind.PARAMETER || el.getKind() == ElementKind.LOCAL_VARIABLE || el.getKind() == ElementKind.RESOURCE_VARIABLE || el.getKind() == ElementKind.EXCEPTION_PARAMETER;
    }

    private static boolean isSerialVersionUID(CompilationInfo info, Element el) {
        if (el.getKind().isField() && el.getModifiers().contains((Object)Modifier.FINAL) && el.getModifiers().contains((Object)Modifier.STATIC) && info.getTypes().getPrimitiveType(TypeKind.LONG).equals(el.asType()) && el.getSimpleName().toString().equals("serialVersionUID")) {
            return true;
        }
        return false;
    }

    static OffsetsBag getImportHighlightsBag(Document doc) {
        OffsetsBag bag = (OffsetsBag)doc.getProperty(KEY_UNUSED_IMPORTS);
        if (bag == null) {
            bag = new OffsetsBag(doc);
            doc.putProperty(KEY_UNUSED_IMPORTS, (Object)bag);
            Object stream = doc.getProperty("stream");
            if (stream instanceof DataObject) {
                // empty if block
            }
        }
        return bag;
    }

    public static interface ErrorDescriptionSetter {
        public void setErrors(Document var1, List<ErrorDescription> var2, List<TreePathHandle> var3);

        public void setHighlights(Document var1, OffsetsBag var2);

        public void setColorings(Document var1, Map<Token, ColoringAttributes.Coloring> var2, Set<Token> var3, Set<Token> var4);
    }

    private static class DetectorVisitor
    extends CancellableTreePathScanner<Void, EnumSet<UseTypes>> {
        private CompilationInfo info;
        private Document doc;
        private Map<Element, List<Use>> type2Uses;
        private Map<Tree, Token> tree2Token;
        private TokenList tl;
        private long memberSelectBypass = -1;
        private SourcePositions sourcePositions;
        private ExecutableElement recursionDetector;
        private static final Set<Tree.Kind> LITERALS = EnumSet.of(Tree.Kind.BOOLEAN_LITERAL, new Tree.Kind[]{Tree.Kind.CHAR_LITERAL, Tree.Kind.DOUBLE_LITERAL, Tree.Kind.FLOAT_LITERAL, Tree.Kind.INT_LITERAL, Tree.Kind.LONG_LITERAL, Tree.Kind.STRING_LITERAL});

        private DetectorVisitor(CompilationInfo info, Document doc, AtomicBoolean cancel) {
            super(cancel);
            this.info = info;
            this.doc = doc;
            this.type2Uses = new HashMap<Element, List<Use>>();
            this.tree2Token = new IdentityHashMap<Tree, Token>();
            this.tl = new TokenList(info, doc, cancel);
            this.sourcePositions = info.getTrees().getSourcePositions();
        }

        private void firstIdentifier(String name) {
            this.tl.firstIdentifier(this.getCurrentPath(), name, this.tree2Token);
        }

        public Void visitAssignment(AssignmentTree tree, EnumSet<UseTypes> d) {
            this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), (Tree)tree.getVariable()), EnumSet.of(UseTypes.WRITE));
            ExpressionTree expr = tree.getExpression();
            if (expr instanceof IdentifierTree) {
                TreePath tp = new TreePath(this.getCurrentPath(), (Tree)expr);
                this.handlePossibleIdentifier(tp, EnumSet.of(UseTypes.READ));
            }
            this.scan((Tree)tree.getVariable(), EnumSet.of(UseTypes.WRITE));
            this.scan((Tree)tree.getExpression(), EnumSet.of(UseTypes.READ));
            return null;
        }

        public Void visitCompoundAssignment(CompoundAssignmentTree tree, EnumSet<UseTypes> d) {
            EnumSet<UseTypes> useTypes = EnumSet.of(UseTypes.WRITE);
            if (d != null) {
                useTypes.addAll(d);
            }
            this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), (Tree)tree.getVariable()), useTypes);
            ExpressionTree expr = tree.getExpression();
            if (expr instanceof IdentifierTree) {
                TreePath tp = new TreePath(this.getCurrentPath(), (Tree)expr);
                this.handlePossibleIdentifier(tp, EnumSet.of(UseTypes.READ));
            }
            this.scan((Tree)tree.getVariable(), EnumSet.of(UseTypes.WRITE));
            this.scan((Tree)tree.getExpression(), EnumSet.of(UseTypes.READ));
            return null;
        }

        public Void visitReturn(ReturnTree tree, EnumSet<UseTypes> d) {
            if (tree.getExpression() instanceof IdentifierTree) {
                this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), (Tree)tree.getExpression()), EnumSet.of(UseTypes.READ));
            }
            CancellableTreePathScanner.super.visitReturn(tree, EnumSet.of(UseTypes.READ));
            return null;
        }

        public Void visitMemberSelect(MemberSelectTree tree, EnumSet<UseTypes> d) {
            Element el;
            long memberSelectBypassLoc = this.memberSelectBypass;
            this.memberSelectBypass = -1;
            ExpressionTree expr = tree.getExpression();
            if (expr instanceof IdentifierTree) {
                TreePath tp = new TreePath(this.getCurrentPath(), (Tree)expr);
                this.handlePossibleIdentifier(tp, EnumSet.of(UseTypes.READ));
            }
            if ((el = this.info.getTrees().getElement(this.getCurrentPath())) != null && el.getKind().isField()) {
                this.handlePossibleIdentifier(this.getCurrentPath(), d == null ? EnumSet.of(UseTypes.READ) : d);
            }
            if (el != null && (el.getKind().isClass() || el.getKind().isInterface()) && this.getCurrentPath().getParentPath().getLeaf().getKind() != Tree.Kind.NEW_CLASS) {
                this.handlePossibleIdentifier(this.getCurrentPath(), EnumSet.of(UseTypes.CLASS_USE));
            }
            CancellableTreePathScanner.super.visitMemberSelect(tree, d);
            this.tl.moveToEnd((Tree)tree.getExpression());
            if (memberSelectBypassLoc != -1) {
                this.tl.moveToOffset(memberSelectBypassLoc);
            }
            this.firstIdentifier(tree.getIdentifier().toString());
            return null;
        }

        private void addModifiers(Element decl, Collection<ColoringAttributes> c) {
            if (decl.getModifiers().contains((Object)Modifier.STATIC)) {
                c.add(ColoringAttributes.STATIC);
            }
            if (decl.getModifiers().contains((Object)Modifier.ABSTRACT) && !decl.getKind().isInterface()) {
                c.add(ColoringAttributes.ABSTRACT);
            }
            boolean accessModifier = false;
            if (decl.getModifiers().contains((Object)Modifier.PUBLIC)) {
                c.add(ColoringAttributes.PUBLIC);
                accessModifier = true;
            }
            if (decl.getModifiers().contains((Object)Modifier.PROTECTED)) {
                c.add(ColoringAttributes.PROTECTED);
                accessModifier = true;
            }
            if (decl.getModifiers().contains((Object)Modifier.PRIVATE)) {
                c.add(ColoringAttributes.PRIVATE);
                accessModifier = true;
            }
            if (!accessModifier && !SemanticHighlighter.isLocalVariableClosure(decl)) {
                c.add(ColoringAttributes.PACKAGE_PRIVATE);
            }
            if (this.info.getElements().isDeprecated(decl)) {
                c.add(ColoringAttributes.DEPRECATED);
            }
        }

        private Collection<ColoringAttributes> getMethodColoring(ExecutableElement mdecl, boolean nct) {
            ArrayList<ColoringAttributes> c = new ArrayList<ColoringAttributes>();
            this.addModifiers(mdecl, c);
            if (mdecl.getKind() == ElementKind.CONSTRUCTOR) {
                c.add(ColoringAttributes.CONSTRUCTOR);
                if (nct && mdecl.getEnclosingElement() != null && this.info.getElements().isDeprecated(mdecl.getEnclosingElement())) {
                    c.add(ColoringAttributes.DEPRECATED);
                }
            } else {
                c.add(ColoringAttributes.METHOD);
            }
            return c;
        }

        private Collection<ColoringAttributes> getVariableColoring(Element decl) {
            ArrayList<ColoringAttributes> c = new ArrayList<ColoringAttributes>();
            this.addModifiers(decl, c);
            if (decl.getKind().isField()) {
                c.add(ColoringAttributes.FIELD);
                return c;
            }
            if (decl.getKind() == ElementKind.LOCAL_VARIABLE || decl.getKind() == ElementKind.RESOURCE_VARIABLE || decl.getKind() == ElementKind.EXCEPTION_PARAMETER) {
                c.add(ColoringAttributes.LOCAL_VARIABLE);
                return c;
            }
            if (decl.getKind() == ElementKind.PARAMETER) {
                c.add(ColoringAttributes.PARAMETER);
                return c;
            }
            assert (false);
            return null;
        }

        private void handlePossibleIdentifier(TreePath expr, Collection<UseTypes> type) {
            this.handlePossibleIdentifier(expr, type, null, false, false);
        }

        private void handlePossibleIdentifier(TreePath expr, Collection<UseTypes> type, Element decl, boolean providesDecl, boolean nct) {
            if (Utilities.isKeyword(expr.getLeaf())) {
                return;
            }
            if (expr.getLeaf().getKind() == Tree.Kind.PRIMITIVE_TYPE) {
                return;
            }
            if (LITERALS.contains((Object)expr.getLeaf().getKind())) {
                return;
            }
            decl = !providesDecl ? this.info.getTrees().getElement(expr) : decl;
            Collection<ColoringAttributes> c = null;
            if (decl != null && (decl.getKind().isField() || SemanticHighlighter.isLocalVariableClosure(decl))) {
                c = this.getVariableColoring(decl);
            }
            if (decl != null && decl instanceof ExecutableElement) {
                c = this.getMethodColoring((ExecutableElement)decl, nct);
            }
            if (decl != null && (decl.getKind().isClass() || decl.getKind().isInterface())) {
                if (type.contains((Object)UseTypes.READ)) {
                    type = EnumSet.copyOf(type);
                    type.remove((Object)UseTypes.READ);
                    type.add(UseTypes.CLASS_USE);
                }
                c = new ArrayList<ColoringAttributes>();
                this.addModifiers(decl, c);
                switch (decl.getKind()) {
                    case CLASS: {
                        c.add(ColoringAttributes.CLASS);
                        break;
                    }
                    case INTERFACE: {
                        c.add(ColoringAttributes.INTERFACE);
                        break;
                    }
                    case ANNOTATION_TYPE: {
                        c.add(ColoringAttributes.ANNOTATION_TYPE);
                        break;
                    }
                    case ENUM: {
                        c.add(ColoringAttributes.ENUM);
                    }
                }
            }
            if (decl != null && type.contains((Object)UseTypes.DECLARATION)) {
                if (c == null) {
                    c = new ArrayList<ColoringAttributes>();
                }
                c.add(ColoringAttributes.DECLARATION);
            }
            if (c != null) {
                this.addUse(decl, type, expr, c);
            }
        }

        private void addUse(Element decl, Collection<UseTypes> useTypes, TreePath t, Collection<ColoringAttributes> c) {
            List<Use> uses;
            if (decl == this.recursionDetector) {
                useTypes.remove((Object)UseTypes.EXECUTE);
            }
            if ((uses = this.type2Uses.get(decl)) == null) {
                uses = new ArrayList<Use>();
                this.type2Uses.put(decl, uses);
            }
            Use u = new Use(useTypes, t, c);
            uses.add(u);
        }

        public Void visitTypeCast(TypeCastTree tree, EnumSet<UseTypes> d) {
            Tree cast;
            ExpressionTree expr = tree.getExpression();
            if (expr.getKind() == Tree.Kind.IDENTIFIER) {
                this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), (Tree)expr), EnumSet.of(UseTypes.READ));
            }
            if ((cast = tree.getType()).getKind() == Tree.Kind.IDENTIFIER) {
                this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), cast), EnumSet.of(UseTypes.READ));
            }
            CancellableTreePathScanner.super.visitTypeCast(tree, d);
            return null;
        }

        public Void visitInstanceOf(InstanceOfTree tree, EnumSet<UseTypes> d) {
            ExpressionTree expr = tree.getExpression();
            if (expr instanceof IdentifierTree) {
                this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), (Tree)expr), EnumSet.of(UseTypes.READ));
            }
            TreePath tp = new TreePath(this.getCurrentPath(), tree.getType());
            this.handlePossibleIdentifier(tp, EnumSet.of(UseTypes.CLASS_USE));
            CancellableTreePathScanner.super.visitInstanceOf(tree, (Object)null);
            return null;
        }

        public Void visitCompilationUnit(CompilationUnitTree tree, EnumSet<UseTypes> d) {
            this.tl.moveBefore(tree.getImports());
            this.scan((Iterable)tree.getImports(), d);
            this.tl.moveBefore(tree.getPackageAnnotations());
            this.scan((Iterable)tree.getPackageAnnotations(), d);
            this.tl.moveToEnd(tree.getImports());
            this.scan((Iterable)tree.getTypeDecls(), d);
            return null;
        }

        private long startOf(List<? extends Tree> trees) {
            if (trees.isEmpty()) {
                return -1;
            }
            return this.sourcePositions.getStartPosition(this.info.getCompilationUnit(), trees.get(0));
        }

        private void handleMethodTypeArguments(TreePath method, List<? extends Tree> tArgs) {
            this.tl.moveBefore(tArgs);
            for (Tree expr : tArgs) {
                if (!(expr instanceof IdentifierTree)) continue;
                this.handlePossibleIdentifier(new TreePath(method, expr), EnumSet.of(UseTypes.CLASS_USE));
            }
        }

        public Void visitMethodInvocation(MethodInvocationTree tree, EnumSet<UseTypes> d) {
            String ident;
            List ta;
            ExpressionTree possibleIdent = tree.getMethodSelect();
            boolean handled = false;
            if (possibleIdent.getKind() == Tree.Kind.IDENTIFIER && ("super".equals(ident = ((IdentifierTree)possibleIdent).getName().toString()) || "this".equals(ident))) {
                Element resolved = this.info.getTrees().getElement(this.getCurrentPath());
                this.addUse(resolved, EnumSet.of(UseTypes.EXECUTE), null, null);
                handled = true;
            }
            if (!handled) {
                this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), (Tree)possibleIdent), EnumSet.of(UseTypes.EXECUTE));
            }
            long afterTypeArguments = (ta = tree.getTypeArguments()).isEmpty() ? -1 : this.info.getTrees().getSourcePositions().getEndPosition(this.info.getCompilationUnit(), (Tree)ta.get(ta.size() - 1));
            switch (tree.getMethodSelect().getKind()) {
                case IDENTIFIER: 
                case MEMBER_SELECT: {
                    this.memberSelectBypass = afterTypeArguments;
                    this.scan((Tree)tree.getMethodSelect(), EnumSet.of(UseTypes.READ));
                    this.memberSelectBypass = -1;
                    break;
                }
                default: {
                    this.scan((Tree)tree.getMethodSelect(), EnumSet.of(UseTypes.READ));
                }
            }
            this.handleMethodTypeArguments(this.getCurrentPath(), ta);
            this.scan((Iterable)tree.getTypeArguments(), (Object)null);
            if (tree.getArguments() != null) {
                for (Tree expr : tree.getArguments()) {
                    if (!(expr instanceof IdentifierTree)) continue;
                    this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), expr), EnumSet.of(UseTypes.READ));
                }
            }
            this.scan((Iterable)tree.getArguments(), EnumSet.of(UseTypes.READ));
            return null;
        }

        public Void visitIdentifier(IdentifierTree tree, EnumSet<UseTypes> d) {
            if (this.info.getTreeUtilities().isSynthetic(this.getCurrentPath())) {
                return null;
            }
            this.tl.moveToOffset(this.sourcePositions.getStartPosition(this.info.getCompilationUnit(), (Tree)tree));
            if (this.memberSelectBypass != -1) {
                this.tl.moveToOffset(this.memberSelectBypass);
                this.memberSelectBypass = -1;
            }
            this.tl.identifierHere(tree, this.tree2Token);
            if (d != null) {
                this.handlePossibleIdentifier(this.getCurrentPath(), d);
            }
            CancellableTreePathScanner.super.visitIdentifier(tree, (Object)null);
            return null;
        }

        public Void visitMethod(MethodTree tree, EnumSet<UseTypes> d) {
            String name;
            if (this.info.getTreeUtilities().isSynthetic(this.getCurrentPath())) {
                return (Void)CancellableTreePathScanner.super.visitMethod(tree, d);
            }
            this.tl.moveToOffset(this.sourcePositions.getStartPosition(this.info.getCompilationUnit(), (Tree)tree));
            this.handlePossibleIdentifier(this.getCurrentPath(), EnumSet.of(UseTypes.DECLARATION));
            for (Tree t : tree.getThrows()) {
                TreePath tp = new TreePath(this.getCurrentPath(), t);
                this.handlePossibleIdentifier(tp, EnumSet.of(UseTypes.CLASS_USE));
            }
            Element el = this.info.getTrees().getElement(this.getCurrentPath());
            EnumSet<UseTypes> paramsUseTypes = el != null && (el.getModifiers().contains((Object)Modifier.ABSTRACT) || el.getModifiers().contains((Object)Modifier.NATIVE) || !el.getModifiers().contains((Object)Modifier.PRIVATE)) ? EnumSet.of(UseTypes.WRITE, UseTypes.READ) : EnumSet.of(UseTypes.WRITE);
            this.scan((Tree)tree.getModifiers(), (Object)null);
            this.tl.moveToEnd((Tree)tree.getModifiers());
            this.scan((Iterable)tree.getTypeParameters(), (Object)null);
            this.tl.moveToEnd(tree.getTypeParameters());
            this.scan(tree.getReturnType(), EnumSet.of(UseTypes.CLASS_USE));
            this.tl.moveToEnd(tree.getReturnType());
            if (tree.getReturnType() != null) {
                name = tree.getName().toString();
            } else {
                TreePath tp;
                for (tp = this.getCurrentPath(); tp != null && !TreeUtilities.CLASS_TREE_KINDS.contains((Object)tp.getLeaf().getKind()); tp = tp.getParentPath()) {
                }
                name = tp != null && TreeUtilities.CLASS_TREE_KINDS.contains((Object)tp.getLeaf().getKind()) ? ((ClassTree)tp.getLeaf()).getSimpleName().toString() : null;
            }
            if (name != null) {
                this.firstIdentifier(name);
            }
            this.scan((Iterable)tree.getParameters(), paramsUseTypes);
            this.scan((Iterable)tree.getThrows(), (Object)null);
            this.scan(tree.getDefaultValue(), (Object)null);
            this.recursionDetector = el != null && el.getKind() == ElementKind.METHOD ? (ExecutableElement)el : null;
            this.scan((Tree)tree.getBody(), (Object)null);
            this.recursionDetector = null;
            return null;
        }

        public Void visitExpressionStatement(ExpressionStatementTree tree, EnumSet<UseTypes> d) {
            CancellableTreePathScanner.super.visitExpressionStatement(tree, (Object)null);
            return null;
        }

        public Void visitParenthesized(ParenthesizedTree tree, EnumSet<UseTypes> d) {
            ExpressionTree expr = tree.getExpression();
            if (expr instanceof IdentifierTree) {
                this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), (Tree)expr), EnumSet.of(UseTypes.READ));
            }
            CancellableTreePathScanner.super.visitParenthesized(tree, d);
            return null;
        }

        public Void visitEnhancedForLoop(EnhancedForLoopTree tree, EnumSet<UseTypes> d) {
            this.scan((Tree)tree.getVariable(), EnumSet.of(UseTypes.WRITE));
            if (tree.getExpression().getKind() == Tree.Kind.IDENTIFIER) {
                this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), (Tree)tree.getExpression()), EnumSet.of(UseTypes.READ));
            }
            this.scan((Tree)tree.getExpression(), (Object)null);
            this.scan((Tree)tree.getStatement(), (Object)null);
            return null;
        }

        private boolean isStar(ImportTree tree) {
            Tree qualIdent = tree.getQualifiedIdentifier();
            if (qualIdent == null || qualIdent.getKind() == Tree.Kind.IDENTIFIER) {
                return false;
            }
            return ((MemberSelectTree)qualIdent).getIdentifier().contentEquals("*");
        }

        public Void visitVariable(VariableTree tree, EnumSet<UseTypes> d) {
            this.tl.moveToOffset(this.sourcePositions.getStartPosition(this.info.getCompilationUnit(), (Tree)tree));
            if (tree.getType() != null) {
                TreePath type = new TreePath(this.getCurrentPath(), tree.getType());
                if (type.getLeaf() instanceof ArrayTypeTree) {
                    type = new TreePath(type, ((ArrayTypeTree)type.getLeaf()).getType());
                }
                if (type.getLeaf().getKind() == Tree.Kind.IDENTIFIER) {
                    this.handlePossibleIdentifier(type, EnumSet.of(UseTypes.CLASS_USE));
                }
            }
            EnumSet uses = null;
            Element e = this.info.getTrees().getElement(this.getCurrentPath());
            if (tree.getInitializer() != null) {
                uses = EnumSet.of(UseTypes.DECLARATION, UseTypes.WRITE);
                if (tree.getInitializer().getKind() == Tree.Kind.IDENTIFIER) {
                    this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), (Tree)tree.getInitializer()), EnumSet.of(UseTypes.READ));
                }
            } else {
                uses = e != null && e.getKind() == ElementKind.FIELD ? EnumSet.of(UseTypes.DECLARATION, UseTypes.WRITE) : EnumSet.of(UseTypes.DECLARATION);
            }
            if (d != null) {
                HashSet<UseTypes> ut = new HashSet<UseTypes>();
                ut.addAll(uses);
                ut.addAll(d);
                uses = EnumSet.copyOf(ut);
            }
            this.handlePossibleIdentifier(this.getCurrentPath(), uses);
            this.scan((Tree)tree.getModifiers(), (Object)null);
            this.tl.moveToEnd((Tree)tree.getModifiers());
            this.scan(tree.getType(), (Object)null);
            int[] span = this.info.getTreeUtilities().findNameSpan(tree);
            if (span != null) {
                this.tl.moveToOffset(span[0]);
            } else {
                this.tl.moveToEnd(tree.getType());
            }
            this.firstIdentifier(tree.getName().toString());
            this.tl.moveNext();
            this.scan((Tree)tree.getInitializer(), EnumSet.of(UseTypes.READ));
            return null;
        }

        public Void visitAnnotation(AnnotationTree tree, EnumSet<UseTypes> d) {
            TreePath tp = new TreePath(this.getCurrentPath(), tree.getAnnotationType());
            this.handlePossibleIdentifier(tp, EnumSet.of(UseTypes.CLASS_USE));
            CancellableTreePathScanner.super.visitAnnotation(tree, EnumSet.noneOf(UseTypes.class));
            return null;
        }

        public Void visitNewClass(NewClassTree tree, EnumSet<UseTypes> d) {
            ExpressionTree ident;
            ExpressionTree exp = tree.getEnclosingExpression();
            if (exp instanceof IdentifierTree) {
                this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), (Tree)exp), EnumSet.of(UseTypes.READ));
            }
            TreePath tp = (ident = tree.getIdentifier()).getKind() == Tree.Kind.PARAMETERIZED_TYPE ? new TreePath(new TreePath(this.getCurrentPath(), (Tree)ident), ((ParameterizedTypeTree)ident).getType()) : new TreePath(this.getCurrentPath(), (Tree)ident);
            this.handlePossibleIdentifier(tp, EnumSet.of(UseTypes.EXECUTE), this.info.getTrees().getElement(this.getCurrentPath()), true, true);
            Element clazz = this.info.getTrees().getElement(tp);
            if (clazz != null) {
                this.addUse(clazz, EnumSet.of(UseTypes.CLASS_USE), null, null);
            }
            for (Tree expr : tree.getArguments()) {
                if (!(expr instanceof IdentifierTree)) continue;
                this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), expr), EnumSet.of(UseTypes.READ));
            }
            this.scan((Tree)tree.getEnclosingExpression(), (Object)null);
            this.scan((Tree)tree.getIdentifier(), (Object)null);
            this.scan((Iterable)tree.getTypeArguments(), (Object)null);
            this.scan((Iterable)tree.getArguments(), EnumSet.of(UseTypes.READ));
            this.scan((Tree)tree.getClassBody(), (Object)null);
            return null;
        }

        public Void visitParameterizedType(ParameterizedTypeTree tree, EnumSet<UseTypes> d) {
            NewClassTree nct;
            boolean alreadyHandled = false;
            if (this.getCurrentPath().getParentPath().getLeaf().getKind() == Tree.Kind.NEW_CLASS) {
                nct = (NewClassTree)this.getCurrentPath().getParentPath().getLeaf();
                boolean bl = alreadyHandled = nct.getTypeArguments().contains((Object)tree) || nct.getIdentifier() == tree;
            }
            if (this.getCurrentPath().getParentPath().getParentPath().getLeaf().getKind() == Tree.Kind.NEW_CLASS) {
                nct = (NewClassTree)this.getCurrentPath().getParentPath().getParentPath().getLeaf();
                Tree leafToTest = this.getCurrentPath().getParentPath().getLeaf();
                boolean bl = alreadyHandled = nct.getTypeArguments().contains((Object)leafToTest) || nct.getIdentifier() == leafToTest;
            }
            if (!alreadyHandled) {
                TreePath tp = new TreePath(this.getCurrentPath(), tree.getType());
                this.handlePossibleIdentifier(tp, EnumSet.of(UseTypes.CLASS_USE));
            }
            for (Tree t : tree.getTypeArguments()) {
                TreePath tp = new TreePath(this.getCurrentPath(), t);
                this.handlePossibleIdentifier(tp, EnumSet.of(UseTypes.CLASS_USE));
            }
            CancellableTreePathScanner.super.visitParameterizedType(tree, (Object)null);
            return null;
        }

        public Void visitBinary(BinaryTree tree, EnumSet<UseTypes> d) {
            TreePath tp;
            ExpressionTree left = tree.getLeftOperand();
            ExpressionTree right = tree.getRightOperand();
            if (left instanceof IdentifierTree) {
                tp = new TreePath(this.getCurrentPath(), (Tree)left);
                this.handlePossibleIdentifier(tp, EnumSet.of(UseTypes.READ));
            }
            if (right instanceof IdentifierTree) {
                tp = new TreePath(this.getCurrentPath(), (Tree)right);
                this.handlePossibleIdentifier(tp, EnumSet.of(UseTypes.READ));
            }
            CancellableTreePathScanner.super.visitBinary(tree, EnumSet.of(UseTypes.READ));
            return null;
        }

        public Void visitClass(ClassTree tree, EnumSet<UseTypes> d) {
            this.tl.moveToOffset(this.sourcePositions.getStartPosition(this.info.getCompilationUnit(), (Tree)tree));
            for (TypeParameterTree t : tree.getTypeParameters()) {
                for (Tree bound : t.getBounds()) {
                    TreePath tp = new TreePath(new TreePath(this.getCurrentPath(), (Tree)t), bound);
                    this.handlePossibleIdentifier(tp, EnumSet.of(UseTypes.CLASS_USE));
                }
            }
            if (this.getCurrentPath().getParentPath().getLeaf().getKind() != Tree.Kind.NEW_CLASS) {
                Tree extnds = tree.getExtendsClause();
                if (extnds != null) {
                    TreePath tp = new TreePath(this.getCurrentPath(), extnds);
                    this.handlePossibleIdentifier(tp, EnumSet.of(UseTypes.CLASS_USE));
                }
                for (Tree t2 : tree.getImplementsClause()) {
                    TreePath tp = new TreePath(this.getCurrentPath(), t2);
                    this.handlePossibleIdentifier(tp, EnumSet.of(UseTypes.CLASS_USE));
                }
            }
            this.handlePossibleIdentifier(this.getCurrentPath(), EnumSet.of(UseTypes.DECLARATION));
            this.scan((Tree)tree.getModifiers(), (Object)null);
            this.tl.moveToEnd((Tree)tree.getModifiers());
            this.firstIdentifier(tree.getSimpleName().toString());
            this.scan((Iterable)tree.getTypeParameters(), (Object)null);
            this.scan(tree.getExtendsClause(), (Object)null);
            this.scan((Iterable)tree.getImplementsClause(), (Object)null);
            ExecutableElement prevRecursionDetector = this.recursionDetector;
            this.recursionDetector = null;
            this.scan((Iterable)tree.getMembers(), (Object)null);
            this.recursionDetector = prevRecursionDetector;
            return null;
        }

        public Void visitUnary(UnaryTree tree, EnumSet<UseTypes> d) {
            if (tree.getExpression() instanceof IdentifierTree) {
                switch (tree.getKind()) {
                    case PREFIX_INCREMENT: 
                    case PREFIX_DECREMENT: 
                    case POSTFIX_INCREMENT: 
                    case POSTFIX_DECREMENT: {
                        EnumSet<UseTypes> useTypes = EnumSet.of(UseTypes.WRITE);
                        if (d != null) {
                            useTypes.addAll(d);
                        }
                        this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), (Tree)tree.getExpression()), useTypes);
                        break;
                    }
                    default: {
                        this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), (Tree)tree.getExpression()), EnumSet.of(UseTypes.READ));
                    }
                }
            }
            CancellableTreePathScanner.super.visitUnary(tree, d);
            return null;
        }

        public Void visitArrayAccess(ArrayAccessTree tree, EnumSet<UseTypes> d) {
            this.scan((Tree)tree.getExpression(), EnumSet.of(UseTypes.READ));
            this.scan((Tree)tree.getIndex(), EnumSet.of(UseTypes.READ));
            return null;
        }

        public Void visitArrayType(ArrayTypeTree node, EnumSet<UseTypes> p) {
            if (node.getType() != null) {
                this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), node.getType()), EnumSet.of(UseTypes.CLASS_USE));
            }
            return (Void)CancellableTreePathScanner.super.visitArrayType(node, p);
        }

        public Void visitUnionType(UnionTypeTree node, EnumSet<UseTypes> p) {
            for (Tree tree : node.getTypeAlternatives()) {
                this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), tree), EnumSet.of(UseTypes.CLASS_USE));
            }
            return (Void)CancellableTreePathScanner.super.visitUnionType(node, p);
        }

        public Void visitNewArray(NewArrayTree tree, EnumSet<UseTypes> d) {
            if (tree.getType() != null) {
                this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), tree.getType()), EnumSet.of(UseTypes.CLASS_USE));
            }
            this.scan(tree.getType(), (Object)null);
            this.scan((Iterable)tree.getDimensions(), EnumSet.of(UseTypes.READ));
            this.scan((Iterable)tree.getInitializers(), EnumSet.of(UseTypes.READ));
            return null;
        }

        public Void visitCatch(CatchTree tree, EnumSet<UseTypes> d) {
            this.scan((Tree)tree.getParameter(), EnumSet.of(UseTypes.WRITE));
            this.scan((Tree)tree.getBlock(), (Object)null);
            return null;
        }

        public Void visitConditionalExpression(ConditionalExpressionTree node, EnumSet<UseTypes> p) {
            return (Void)CancellableTreePathScanner.super.visitConditionalExpression(node, EnumSet.of(UseTypes.READ));
        }

        public Void visitAssert(AssertTree tree, EnumSet<UseTypes> p) {
            if (tree.getCondition().getKind() == Tree.Kind.IDENTIFIER) {
                this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), (Tree)tree.getCondition()), EnumSet.of(UseTypes.READ));
            }
            if (tree.getDetail() != null && tree.getDetail().getKind() == Tree.Kind.IDENTIFIER) {
                this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), (Tree)tree.getDetail()), EnumSet.of(UseTypes.READ));
            }
            return (Void)CancellableTreePathScanner.super.visitAssert(tree, EnumSet.of(UseTypes.READ));
        }

        public Void visitCase(CaseTree tree, EnumSet<UseTypes> p) {
            if (tree.getExpression() != null && tree.getExpression().getKind() == Tree.Kind.IDENTIFIER) {
                this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), (Tree)tree.getExpression()), EnumSet.of(UseTypes.READ));
            }
            return (Void)CancellableTreePathScanner.super.visitCase(tree, (Object)null);
        }

        public Void visitThrow(ThrowTree tree, EnumSet<UseTypes> p) {
            if (tree.getExpression() != null && tree.getExpression().getKind() == Tree.Kind.IDENTIFIER) {
                this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), (Tree)tree.getExpression()), EnumSet.of(UseTypes.READ));
            }
            return (Void)CancellableTreePathScanner.super.visitThrow(tree, p);
        }

        public Void visitTypeParameter(TypeParameterTree tree, EnumSet<UseTypes> p) {
            for (Tree bound : tree.getBounds()) {
                if (bound.getKind() != Tree.Kind.IDENTIFIER) continue;
                TreePath tp = new TreePath(this.getCurrentPath(), bound);
                this.handlePossibleIdentifier(tp, EnumSet.of(UseTypes.CLASS_USE));
            }
            return (Void)CancellableTreePathScanner.super.visitTypeParameter(tree, p);
        }

        public Void visitForLoop(ForLoopTree node, EnumSet<UseTypes> p) {
            if (node.getCondition() != null && node.getCondition().getKind() == Tree.Kind.IDENTIFIER) {
                this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), (Tree)node.getCondition()), EnumSet.of(UseTypes.READ));
            }
            return (Void)CancellableTreePathScanner.super.visitForLoop(node, p);
        }

        public Void visitWildcard(WildcardTree node, EnumSet<UseTypes> p) {
            if (node.getBound() != null && node.getBound().getKind() == Tree.Kind.IDENTIFIER) {
                this.handlePossibleIdentifier(new TreePath(this.getCurrentPath(), node.getBound()), EnumSet.of(UseTypes.CLASS_USE));
            }
            return (Void)CancellableTreePathScanner.super.visitWildcard(node, p);
        }

        public Void visitLambdaExpression(LambdaExpressionTree node, EnumSet<UseTypes> p) {
            this.scan((Iterable)node.getParameters(), EnumSet.of(UseTypes.WRITE));
            this.scan(node.getBody(), EnumSet.noneOf(UseTypes.class));
            return null;
        }

        public Void visitMemberReference(MemberReferenceTree node, EnumSet<UseTypes> p) {
            this.scan((Tree)node.getQualifierExpression(), EnumSet.of(UseTypes.READ));
            this.tl.moveToEnd((Tree)node.getQualifierExpression());
            this.scan((Iterable)node.getTypeArguments(), (Object)null);
            this.tl.moveToEnd(node.getTypeArguments());
            this.handlePossibleIdentifier(this.getCurrentPath(), EnumSet.of(UseTypes.EXECUTE));
            this.firstIdentifier(node.getName().toString());
            return null;
        }
    }

    private static class Use {
        private Collection<UseTypes> type;
        private TreePath tree;
        private Collection<ColoringAttributes> spec;

        public Use(Collection<UseTypes> type, TreePath tree, Collection<ColoringAttributes> spec) {
            this.type = type;
            this.tree = tree;
            this.spec = spec;
        }

        public String toString() {
            return "Use: " + this.type;
        }
    }

    private static enum UseTypes {
        READ,
        WRITE,
        EXECUTE,
        DECLARATION,
        CLASS_USE;
        

        private UseTypes() {
        }
    }

}

