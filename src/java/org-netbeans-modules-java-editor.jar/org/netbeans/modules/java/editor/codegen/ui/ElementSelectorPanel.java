/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.source.ElementHandle
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.java.editor.codegen.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.List;
import javax.lang.model.element.Element;
import javax.swing.JPanel;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.modules.java.editor.codegen.ui.CheckTreeView;
import org.netbeans.modules.java.editor.codegen.ui.ElementNode;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

public class ElementSelectorPanel
extends JPanel
implements ExplorerManager.Provider {
    private ExplorerManager manager = new ExplorerManager();
    private CheckTreeView elementView;

    public ElementSelectorPanel(ElementNode.Description elementDescription, boolean singleSelection) {
        this.setLayout(new BorderLayout());
        this.elementView = new CheckTreeView();
        this.elementView.setRootVisible(false);
        this.add((Component)((Object)this.elementView), "Center");
        this.setRootElement(elementDescription, singleSelection);
        Node root = this.manager.getRootContext();
        Node[] children = root.getChildren().getNodes();
        if (null != children && children.length > 0) {
            try {
                this.manager.setSelectedNodes(new Node[]{children[0]});
            }
            catch (PropertyVetoException ex) {
                // empty catch block
            }
        }
    }

    public List<ElementHandle<? extends Element>> getTreeSelectedElements() {
        ArrayList<ElementHandle<? extends Element>> handles = new ArrayList<ElementHandle<? extends Element>>();
        for (Node node : this.manager.getSelectedNodes()) {
            if (!(node instanceof ElementNode)) continue;
            ElementNode.Description description = (ElementNode.Description)node.getLookup().lookup(ElementNode.Description.class);
            handles.add(description.getElementHandle());
        }
        return handles;
    }

    public List<ElementHandle<? extends Element>> getSelectedElements() {
        ArrayList<ElementHandle<? extends Element>> handles = new ArrayList<ElementHandle<? extends Element>>();
        Node n = this.manager.getRootContext();
        ElementNode.Description description = (ElementNode.Description)n.getLookup().lookup(ElementNode.Description.class);
        this.getSelectedHandles(description, handles);
        return handles;
    }

    public void setRootElement(ElementNode.Description elementDescription, boolean singleSelection) {
        Object n;
        if (elementDescription != null) {
            ElementNode en = new ElementNode(elementDescription);
            en.setSingleSelection(singleSelection);
            n = en;
        } else {
            n = Node.EMPTY;
        }
        this.manager.setRootContext((Node)n);
    }

    public void doInitialExpansion(int howMuch) {
        Node root = this.getExplorerManager().getRootContext();
        Node[] subNodes = root.getChildren().getNodes(true);
        if (subNodes == null) {
            return;
        }
        Node toSelect = null;
        int row = 0;
        boolean oldScroll = this.elementView.getScrollsOnExpand();
        this.elementView.setScrollsOnExpand(false);
        for (int i = 0; subNodes != null && i < (howMuch == -1 || howMuch > subNodes.length ? subNodes.length : howMuch); ++i) {
            this.elementView.expandRow(++row);
            Node[] ssn = subNodes[i].getChildren().getNodes(true);
            row += ssn.length;
            if (toSelect != null || ssn.length <= 0) continue;
            toSelect = ssn[0];
        }
        this.elementView.setScrollsOnExpand(oldScroll);
        try {
            if (toSelect != null) {
                this.getExplorerManager().setSelectedNodes(new Node[]{toSelect});
            }
        }
        catch (PropertyVetoException ex) {
            // empty catch block
        }
    }

    public ExplorerManager getExplorerManager() {
        return this.manager;
    }

    private void getSelectedHandles(ElementNode.Description description, ArrayList<ElementHandle<? extends Element>> target) {
        if (description == null) {
            return;
        }
        List<ElementNode.Description> subs = description.getSubs();
        if (subs == null) {
            return;
        }
        for (ElementNode.Description d : subs) {
            if (d.isSelectable() && d.isSelected()) {
                target.add(d.getElementHandle());
                continue;
            }
            this.getSelectedHandles(d, target);
        }
    }
}

