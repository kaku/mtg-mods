/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.lib.editor.hyperlink.spi.HyperlinkProviderExt
 *  org.netbeans.lib.editor.hyperlink.spi.HyperlinkType
 */
package org.netbeans.modules.java.editor.hyperlink;

import java.util.EnumSet;
import java.util.Set;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.lib.editor.hyperlink.spi.HyperlinkProviderExt;
import org.netbeans.lib.editor.hyperlink.spi.HyperlinkType;
import org.netbeans.modules.editor.java.GoToSupport;
import org.netbeans.modules.java.editor.overridden.GoToImplementation;

public final class JavaHyperlinkProvider
implements HyperlinkProviderExt {
    public Set<HyperlinkType> getSupportedHyperlinkTypes() {
        return EnumSet.of(HyperlinkType.GO_TO_DECLARATION, HyperlinkType.ALT_HYPERLINK);
    }

    public boolean isHyperlinkPoint(Document doc, int offset, HyperlinkType type) {
        return this.getHyperlinkSpan(doc, offset, type) != null;
    }

    public int[] getHyperlinkSpan(Document doc, int offset, HyperlinkType type) {
        return GoToSupport.getIdentifierSpan(doc, offset, null);
    }

    public void performClickAction(Document doc, int offset, HyperlinkType type) {
        switch (type) {
            case GO_TO_DECLARATION: {
                GoToSupport.goTo(doc, offset, false);
                break;
            }
            case ALT_HYPERLINK: {
                JTextComponent focused = EditorRegistry.focusedComponent();
                if (focused == null || focused.getDocument() != doc) break;
                focused.setCaretPosition(offset);
                GoToImplementation.goToImplementation(focused);
            }
        }
    }

    public String getTooltipText(Document doc, int offset, HyperlinkType type) {
        return GoToSupport.getGoToElementTooltip(doc, offset, false, type);
    }

}

