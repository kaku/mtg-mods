/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.javadoc.Doc
 *  com.sun.source.doctree.DocTree
 *  com.sun.source.doctree.DocTree$Kind
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.util.DocTreePath
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.lexer.JavadocTokenId
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementUtilities
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.PartType
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 */
package org.netbeans.modules.java.editor.javadoc;

import com.sun.javadoc.Doc;
import com.sun.source.doctree.DocTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.DocTreePath;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.EnumSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.util.Elements;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.lexer.JavadocTokenId;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.PartType;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;

final class JavadocCompletionUtils {
    static final Pattern JAVADOC_LINE_BREAK = Pattern.compile("\\n[ \\t]*\\**[ \\t]*\\z");
    static final Pattern JAVADOC_WHITE_SPACE = Pattern.compile("[^ \\t]");
    static final Pattern JAVADOC_EMPTY = Pattern.compile("(\\s*\\**\\s*\n)*\\s*\\**\\s*\\**");
    static final Pattern JAVADOC_FIRST_WHITE_SPACE = Pattern.compile("[ \\t]*\\**[ \\t]*");
    private static Set<JavaTokenId> IGNORE_TOKES = EnumSet.of(JavaTokenId.WHITESPACE, JavaTokenId.BLOCK_COMMENT, JavaTokenId.LINE_COMMENT);
    private static final Logger LOGGER = Logger.getLogger(JavadocCompletionUtils.class.getName());
    private static final Set<DocTree.Kind> BLOCK_TAGS = EnumSet.of(DocTree.Kind.AUTHOR, new DocTree.Kind[]{DocTree.Kind.DEPRECATED, DocTree.Kind.PARAM, DocTree.Kind.RETURN, DocTree.Kind.SEE, DocTree.Kind.SERIAL, DocTree.Kind.SERIAL_DATA, DocTree.Kind.SERIAL_FIELD, DocTree.Kind.SINCE, DocTree.Kind.THROWS, DocTree.Kind.UNKNOWN_BLOCK_TAG, DocTree.Kind.VERSION});
    private static final int MAX_DUMPS = 255;

    JavadocCompletionUtils() {
    }

    public static boolean isJavadocContext(final Document doc, final int offset) {
        final boolean[] result = new boolean[]{false};
        doc.render(new Runnable(){

            @Override
            public void run() {
                result[0] = JavadocCompletionUtils.isJavadocContext(TokenHierarchy.get((Document)doc), offset);
            }
        });
        return result[0];
    }

    static boolean isJavadocContext(TokenHierarchy hierarchy, int offset) {
        TokenSequence ts = SourceUtils.getJavaTokenSequence((TokenHierarchy)hierarchy, (int)offset);
        if (!JavadocCompletionUtils.movedToJavadocToken(ts, offset)) {
            return false;
        }
        TokenSequence jdts = ts.embedded(JavadocTokenId.language());
        if (jdts == null) {
            return false;
        }
        if (jdts.isEmpty()) {
            return JavadocCompletionUtils.isEmptyJavadoc(ts.token(), offset - ts.offset());
        }
        jdts.move(offset);
        if (!jdts.moveNext() && !jdts.movePrevious()) {
            return false;
        }
        return JavadocCompletionUtils.isInsideToken(jdts, offset) && !JavadocCompletionUtils.isInsideIndent(jdts.token(), offset - jdts.offset());
    }

    public static TreePath findJavadoc(CompilationInfo javac, int offset) {
        TokenSequence ts = SourceUtils.getJavaTokenSequence((TokenHierarchy)javac.getTokenHierarchy(), (int)offset);
        if (ts == null || !JavadocCompletionUtils.movedToJavadocToken(ts, offset)) {
            return null;
        }
        int offsetBehindJavadoc = ts.offset() + ts.token().length();
        while (ts.moveNext()) {
            TokenId tid = ts.token().id();
            if (tid == JavaTokenId.BLOCK_COMMENT) {
                if (!"/**/".contentEquals(ts.token().text())) continue;
                return null;
            }
            if (tid == JavaTokenId.JAVADOC_COMMENT) {
                if (ts.token().partType() != PartType.COMPLETE) continue;
                return null;
            }
            if (IGNORE_TOKES.contains((Object)tid)) continue;
            offsetBehindJavadoc = ts.offset();
            ++offsetBehindJavadoc;
            break;
        }
        TreePath tp = javac.getTreeUtilities().pathFor(offsetBehindJavadoc);
        while (!TreeUtilities.CLASS_TREE_KINDS.contains((Object)tp.getLeaf().getKind()) && tp.getLeaf().getKind() != Tree.Kind.METHOD && tp.getLeaf().getKind() != Tree.Kind.VARIABLE && tp.getLeaf().getKind() != Tree.Kind.COMPILATION_UNIT && (tp = tp.getParentPath()) != null) {
        }
        return tp;
    }

    public static Doc findJavadoc(CompilationInfo javac, Document doc, int offset) {
        Doc jdoc;
        TokenSequence ts = SourceUtils.getJavaTokenSequence((TokenHierarchy)javac.getTokenHierarchy(), (int)offset);
        if (ts == null || !JavadocCompletionUtils.movedToJavadocToken(ts, offset)) {
            return null;
        }
        TokenSequence jdts = ts.embedded(JavadocTokenId.language());
        int offsetBehindJavadoc = ts.offset() + ts.token().length();
        while (ts.moveNext()) {
            TokenId tid = ts.token().id();
            if (tid == JavaTokenId.BLOCK_COMMENT) {
                if (!"/**/".contentEquals(ts.token().text())) continue;
                return null;
            }
            if (tid == JavaTokenId.JAVADOC_COMMENT) {
                if (ts.token().partType() != PartType.COMPLETE) continue;
                return null;
            }
            if (IGNORE_TOKES.contains((Object)tid)) continue;
            offsetBehindJavadoc = ts.offset();
            ++offsetBehindJavadoc;
            break;
        }
        TreePath tp = javac.getTreeUtilities().pathFor(offsetBehindJavadoc);
        Tree leaf = tp.getLeaf();
        Tree.Kind kind = leaf.getKind();
        SourcePositions positions = javac.getTrees().getSourcePositions();
        while (!TreeUtilities.CLASS_TREE_KINDS.contains((Object)kind) && kind != Tree.Kind.METHOD && kind != Tree.Kind.VARIABLE && kind != Tree.Kind.COMPILATION_UNIT) {
            if ((tp = tp.getParentPath()) == null) {
                leaf = null;
                kind = null;
                break;
            }
            leaf = tp.getLeaf();
            kind = leaf.getKind();
        }
        if (leaf == null || kind == Tree.Kind.COMPILATION_UNIT || positions.getStartPosition(javac.getCompilationUnit(), leaf) < (long)offset) {
            return null;
        }
        Element el = javac.getTrees().getElement(tp);
        Doc doc2 = jdoc = el != null ? javac.getElementUtilities().javaDocFor(el) : null;
        if (JavadocCompletionUtils.isInvalidDocInstance(jdoc, jdts)) {
            JavadocCompletionUtils.dumpOutOfSyncError(javac.getSnapshot(), offset, leaf, el, jdts, jdoc, false);
            jdoc = null;
        }
        return jdoc;
    }

    static TokenSequence<JavadocTokenId> findJavadocTokenSequence(CompilationInfo javac, int offset) {
        TokenSequence ts = SourceUtils.getJavaTokenSequence((TokenHierarchy)javac.getTokenHierarchy(), (int)offset);
        if (ts == null || !JavadocCompletionUtils.movedToJavadocToken(ts, offset)) {
            return null;
        }
        TokenSequence jdts = ts.embedded(JavadocTokenId.language());
        if (jdts == null) {
            return null;
        }
        jdts.move(offset);
        return jdts;
    }

    static TokenSequence<JavadocTokenId> findJavadocTokenSequence(CompilationInfo javac, Tree tree, Element e) {
        if (e == null || javac.getElementUtilities().isSynthetic(e)) {
            return null;
        }
        if (tree == null) {
            tree = javac.getTrees().getTree(e);
        }
        if (tree == null) {
            return null;
        }
        int elementStartOffset = (int)javac.getTrees().getSourcePositions().getStartPosition(javac.getCompilationUnit(), tree);
        TokenSequence s = SourceUtils.getJavaTokenSequence((TokenHierarchy)javac.getTokenHierarchy(), (int)elementStartOffset);
        if (s == null) {
            return null;
        }
        s.move(elementStartOffset);
        Token token = null;
        block5 : while (s.movePrevious()) {
            token = s.token();
            switch ((JavaTokenId)token.id()) {
                case BLOCK_COMMENT: {
                    if (!"/**/".contentEquals(token.text())) continue block5;
                }
                case JAVADOC_COMMENT: {
                    if (token.partType() != PartType.COMPLETE) continue block5;
                    return javac.getElements().getDocComment(e) == null ? null : s.embedded(JavadocTokenId.language());
                }
                case WHITESPACE: 
                case LINE_COMMENT: {
                    continue block5;
                }
            }
            return null;
        }
        return null;
    }

    static boolean isInsideIndent(Token<JavadocTokenId> token, int offset) {
        int indent = -1;
        if (token.id() == JavadocTokenId.OTHER_TEXT) {
            CharSequence text = token.text();
            for (int i = 0; i < text.length(); ++i) {
                char c = text.charAt(i);
                if (c == '\n') {
                    if (i > offset) break;
                    indent = -1;
                    if (i >= offset) break;
                    continue;
                }
                if (i == 0) break;
                if (c != '*' || indent >= 0) continue;
                indent = i;
                if (offset <= i) break;
            }
        }
        return indent >= offset;
    }

    public static boolean isLineBreak(Token<JavadocTokenId> token) {
        return JavadocCompletionUtils.isLineBreak(token, token.length());
    }

    public static boolean isLineBreak(Token<JavadocTokenId> token, int pos) {
        if (token == null || token.id() != JavadocTokenId.OTHER_TEXT) {
            return false;
        }
        try {
            CharSequence text = token.text();
            if (pos < token.length()) {
                text = text.subSequence(0, pos);
            }
            boolean result = pos > 0 && JAVADOC_LINE_BREAK.matcher(text).find() && (pos == token.length() || !JavadocCompletionUtils.isInsideIndent(token, pos));
            return result;
        }
        catch (IndexOutOfBoundsException e) {
            throw (IndexOutOfBoundsException)new IndexOutOfBoundsException("pos: " + pos + ", token.length: " + token.length() + ", token text: " + token.text()).initCause(e);
        }
    }

    public static boolean isWhiteSpace(CharSequence text) {
        return text != null && text.length() > 0 && !JAVADOC_WHITE_SPACE.matcher(text).find();
    }

    public static boolean isWhiteSpace(Token<JavadocTokenId> token) {
        if (token == null || token.id() != JavadocTokenId.OTHER_TEXT) {
            return false;
        }
        CharSequence text = token.text();
        boolean result = !JAVADOC_WHITE_SPACE.matcher(text).find();
        return result;
    }

    public static boolean isFirstWhiteSpaceAtFirstLine(Token<JavadocTokenId> token) {
        if (token == null || token.id() != JavadocTokenId.OTHER_TEXT) {
            return false;
        }
        CharSequence text = token.text();
        boolean result = JAVADOC_FIRST_WHITE_SPACE.matcher(text).matches();
        return result;
    }

    public static boolean isWhiteSpaceFirst(Token<JavadocTokenId> token) {
        if (token == null || token.id() != JavadocTokenId.OTHER_TEXT || token.length() < 1) {
            return false;
        }
        CharSequence text = token.text();
        char c = text.charAt(0);
        return c == ' ' || c == '\t';
    }

    public static boolean isWhiteSpaceLast(Token<JavadocTokenId> token) {
        if (token == null || token.id() != JavadocTokenId.OTHER_TEXT || token.length() < 1) {
            return false;
        }
        CharSequence text = token.text();
        char c = text.charAt(text.length() - 1);
        return c == ' ' || c == '\t';
    }

    public static boolean isInlineTagStart(Token<JavadocTokenId> token) {
        if (token == null || token.id() != JavadocTokenId.OTHER_TEXT) {
            return false;
        }
        CharSequence text = token.text();
        boolean result = text.charAt(text.length() - 1) == '{';
        return result;
    }

    public static boolean isBlockTag(DocTreePath tag) {
        return BLOCK_TAGS.contains((Object)tag.getLeaf().getKind());
    }

    static CharSequence getCharSequence(Document doc) {
        CharSequence cs = (CharSequence)doc.getProperty(CharSequence.class);
        if (cs == null) {
            try {
                cs = doc.getText(0, doc.getLength());
            }
            catch (BadLocationException ex) {
                throw (IndexOutOfBoundsException)new IndexOutOfBoundsException().initCause(ex);
            }
        }
        return cs;
    }

    static CharSequence getCharSequence(Document doc, int begin, int end) {
        CharSequence cs = (CharSequence)doc.getProperty(CharSequence.class);
        if (cs != null) {
            cs = cs.subSequence(begin, end);
        } else {
            try {
                cs = doc.getText(begin, end - begin);
            }
            catch (BadLocationException ex) {
                throw (IndexOutOfBoundsException)new IndexOutOfBoundsException().initCause(ex);
            }
        }
        return cs;
    }

    private static boolean isInsideToken(TokenSequence<?> ts, int offset) {
        return offset >= ts.offset() && offset <= ts.offset() + ts.token().length();
    }

    private static boolean movedToJavadocToken(TokenSequence<JavaTokenId> ts, int offset) {
        if (ts == null || !ts.moveNext() && !ts.movePrevious()) {
            return false;
        }
        if (ts.token().id() != JavaTokenId.JAVADOC_COMMENT) {
            return false;
        }
        return JavadocCompletionUtils.isInsideToken(ts, offset);
    }

    private static boolean isEmptyJavadoc(Token<JavaTokenId> token, int offset) {
        if (token != null && token.id() == JavaTokenId.JAVADOC_COMMENT) {
            CharSequence text = token.text();
            return offset == 3 && "/***/".contentEquals(text);
        }
        return false;
    }

    static boolean isInvalidDocInstance(Doc javadoc, TokenSequence<JavadocTokenId> ts) {
        if (javadoc != null && javadoc.getRawCommentText().trim().length() == 0 && !ts.isEmpty()) {
            ts.moveStart();
            return !ts.moveNext() || !JavadocCompletionUtils.isTokenOfEmptyJavadoc(ts.token()) || ts.moveNext();
        }
        return false;
    }

    static boolean isTokenOfEmptyJavadoc(Token<JavadocTokenId> token) {
        if (token == null || token.id() != JavadocTokenId.OTHER_TEXT) {
            return false;
        }
        return JAVADOC_EMPTY.matcher(token.text()).matches();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void dumpOutOfSyncError(Snapshot snapshot, int offset, Tree tree, Element elm, TokenSequence<JavadocTokenId> ts, Doc jdoc, boolean throwNotLogException) {
        String dumpDir;
        IllegalStateException throwable;
        boolean dumpSucceeded;
        String dumpExt;
        dumpDir = System.getProperty("netbeans.user") + "/var/log/";
        dumpExt = ".jddump";
        FileObject source = snapshot.getSource().getFileObject();
        Document doc = snapshot.getSource().getDocument(false);
        String uri = "<unknown>";
        try {
            uri = source.getURL().toURI().toASCIIString();
        }
        catch (URISyntaxException ex) {
            LOGGER.log(Level.INFO, null, ex);
        }
        catch (FileStateInvalidException ex) {
            LOGGER.log(Level.INFO, null, (Throwable)ex);
        }
        String origName = source.getName();
        File f = new File(dumpDir + origName + dumpExt);
        dumpSucceeded = false;
        for (int i = 1; i < 255 && f.exists(); ++i) {
            f = new File(dumpDir + origName + '_' + i + dumpExt);
        }
        throwable = new IllegalStateException("Please attach dump file " + f.toURI().toASCIIString() + " to bug.");
        if (!f.exists()) {
            try {
                FileOutputStream os = new FileOutputStream(f);
                PrintWriter writer = new PrintWriter(new OutputStreamWriter((OutputStream)os, "UTF-8"));
                try {
                    writer.printf("Out of sync error: source file %s\n", uri);
                    writer.println("----- Element: -------------------------------------------------------");
                    if (elm != null) {
                        writer.printf("kind: %s, %s\n", new Object[]{elm.getKind(), elm});
                    } else {
                        writer.println("null");
                    }
                    writer.println("----- Tree: -------------------------------------------------------");
                    if (tree != null) {
                        writer.printf("kind: %s\n %s\n", new Object[]{tree.getKind(), tree});
                    } else {
                        writer.println("null");
                    }
                    writer.println("----- Offset: -------------------------------------------------------");
                    writer.printf("offset: %s\n", offset);
                    writer.println("----- Token sequence: ----------------------------------------");
                    writer.println(ts);
                    writer.println("----- Doc instance: -------------------------------------------------------");
                    writer.printf("class: %s, toString: %s,\nraw text:'%s'\n", new Object[]{jdoc.getClass(), jdoc, jdoc.getRawCommentText()});
                    writer.println("----- Stack trace: ---------------------------------------------");
                    throwable.printStackTrace(writer);
                    writer.println("----- Source file content/snapshot: ----------------------------------------");
                    writer.println(snapshot.getText());
                    writer.println("----- Document content: ----------------------------------------");
                    try {
                        if (doc != null) {
                            writer.println(doc.getText(0, doc.getLength()));
                        } else {
                            writer.println("doc: null");
                        }
                    }
                    catch (BadLocationException ex) {
                        LOGGER.log(Level.INFO, null, ex);
                    }
                }
                finally {
                    writer.close();
                    dumpSucceeded = true;
                }
            }
            catch (IOException ioe) {
                LOGGER.log(Level.INFO, "Error when writing javadoc dump file!", ioe);
            }
        }
        if (dumpSucceeded) {
            if (throwNotLogException) {
                throw throwable;
            }
            LOGGER.log(Level.SEVERE, null, throwable);
        } else {
            LOGGER.log(Level.WARNING, "Dump could not be written. Either dump file could not be created or all dump files were already used. Please check that you have write permission to ''{0}'' and clean all *{1} files in that directory.", new Object[]{dumpDir, dumpExt});
            if (throwNotLogException) {
                throw throwable;
            }
        }
    }

    static void dumpOutOfSyncError(Snapshot snapshot, TokenSequence<JavadocTokenId> ts, Doc jdoc, boolean throwNotLogException) {
        JavadocCompletionUtils.dumpOutOfSyncError(snapshot, JavadocCompletionUtils.dumpOffset(ts), null, null, ts, jdoc, throwNotLogException);
    }

    static void dumpOutOfSyncError(CompilationInfo javac, TokenSequence<JavadocTokenId> ts, Doc jdoc, boolean throwNotLogException) {
        Element elm = javac.getElementUtilities().elementFor(jdoc);
        Tree tree = elm != null ? javac.getTrees().getTree(elm) : null;
        JavadocCompletionUtils.dumpOutOfSyncError(javac.getSnapshot(), JavadocCompletionUtils.dumpOffset(ts), tree, elm, ts, jdoc, throwNotLogException);
    }

    private static int dumpOffset(TokenSequence<JavadocTokenId> ts) {
        int offset = -1;
        if (!ts.isEmpty()) {
            ts.moveStart();
            ts.moveNext();
            offset = ts.offset();
        }
        return offset;
    }

}

