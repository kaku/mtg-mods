/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.Tree
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.SourceUtils
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.editor.overridden;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.Tree;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.modules.java.editor.overridden.AnnotationType;
import org.netbeans.modules.java.editor.overridden.ElementDescription;
import org.netbeans.modules.java.editor.overridden.IsOverriddenVisitor;
import org.openide.filesystems.FileObject;
import org.openide.util.Utilities;

public class ComputeOverriding {
    static final Logger LOG = Logger.getLogger(ComputeOverriding.class.getName());
    private final AtomicBoolean cancel;
    private static final Map<Reference<Elements>, DataHolder> CACHE = new HashMap<Reference<Elements>, DataHolder>();

    public ComputeOverriding(AtomicBoolean cancel) {
        this.cancel = cancel;
    }

    public static AnnotationType detectOverrides(CompilationInfo info, TypeElement type, ExecutableElement ee, List<ElementDescription> result) {
        Map<ElementHandle<ExecutableElement>, List<ElementDescription>> method2Overriding = ComputeOverriding.compute(info, ElementHandle.create((Element)type), new AtomicBoolean());
        List<ElementDescription> res = method2Overriding.get((Object)ElementHandle.create((Element)ee));
        if (res != null) {
            result.addAll(res);
        }
        if (!result.isEmpty()) {
            for (ElementDescription ed : result) {
                if (ed.getModifiers().contains((Object)Modifier.ABSTRACT)) continue;
                return AnnotationType.OVERRIDES;
            }
            return AnnotationType.IMPLEMENTS;
        }
        return null;
    }

    public Map<ElementHandle<? extends Element>, List<ElementDescription>> process(CompilationInfo info) {
        IsOverriddenVisitor v = new IsOverriddenVisitor(info, this.cancel);
        CompilationUnitTree unit = info.getCompilationUnit();
        long startTime1 = System.currentTimeMillis();
        v.scan((Tree)unit, (Object)null);
        long endTime1 = System.currentTimeMillis();
        Logger.getLogger("TIMER").log(Level.FINE, "Overridden Scanner", new Object[]{info.getFileObject(), endTime1 - startTime1});
        HashMap<ElementHandle<? extends Element>, List<ElementDescription>> result = new HashMap<ElementHandle<? extends Element>, List<ElementDescription>>();
        for (ElementHandle<TypeElement> td : v.type2Declaration.keySet()) {
            if (this.isCanceled()) {
                return null;
            }
            Map<ElementHandle<ExecutableElement>, List<ElementDescription>> overrides = ComputeOverriding.compute(info, td, this.cancel);
            if (overrides == null) continue;
            result.putAll(overrides);
        }
        if (this.isCanceled()) {
            return null;
        }
        return result;
    }

    private synchronized boolean isCanceled() {
        return this.cancel.get();
    }

    private static void sortOutMethods(CompilationInfo info, Map<Name, List<ExecutableElement>> where, Element td, boolean current) {
        if (current) {
            HashMap<Name, ArrayList<ExecutableElement>> newlyAdded = new HashMap<Name, ArrayList<ExecutableElement>>();
            block0 : for (ExecutableElement ee : ElementFilter.methodsIn(td.getEnclosedElements())) {
                ArrayList<ExecutableElement> lee;
                Name name = ee.getSimpleName();
                List<ExecutableElement> alreadySeen = where.get(name);
                if (alreadySeen != null) {
                    for (ExecutableElement seen : alreadySeen) {
                        if (!info.getElements().overrides(seen, ee, (TypeElement)seen.getEnclosingElement())) continue;
                        continue block0;
                    }
                }
                if ((lee = (ArrayList<ExecutableElement>)newlyAdded.get(name)) == null) {
                    lee = new ArrayList<ExecutableElement>();
                    newlyAdded.put(name, lee);
                }
                lee.add(ee);
            }
            for (Map.Entry e : newlyAdded.entrySet()) {
                List<ExecutableElement> lee = where.get(e.getKey());
                if (lee == null) {
                    where.put((Name)e.getKey(), (List<ExecutableElement>)e.getValue());
                    continue;
                }
                lee.addAll((Collection)e.getValue());
            }
        }
        for (TypeMirror superType : info.getTypes().directSupertypes(td.asType())) {
            if (superType.getKind() != TypeKind.DECLARED) continue;
            ComputeOverriding.sortOutMethods(info, where, ((DeclaredType)superType).asElement(), true);
        }
    }

    private static Map<ElementHandle<ExecutableElement>, List<ElementDescription>> compute(CompilationInfo info, ElementHandle<TypeElement> forType, AtomicBoolean cancel) {
        DataHolder data = ComputeOverriding.getDataFromCache(info);
        HashMap<ElementHandle<ExecutableElement>, List<ElementDescription>> result = (HashMap<ElementHandle<ExecutableElement>, List<ElementDescription>>)data.data.get(forType);
        if (result == null) {
            result = new HashMap<ElementHandle<ExecutableElement>, List<ElementDescription>>();
            data.data.put(forType, result);
            if (cancel.get()) {
                return null;
            }
            LOG.log(Level.FINE, "type: {0}", forType.getQualifiedName());
            HashMap<Name, List<ExecutableElement>> name2Method = new HashMap<Name, List<ExecutableElement>>();
            TypeElement resolvedType = (TypeElement)forType.resolve(info);
            if (resolvedType == null) {
                return result;
            }
            ComputeOverriding.sortOutMethods(info, name2Method, resolvedType, false);
            for (ExecutableElement ee : ElementFilter.methodsIn(resolvedType.getEnclosedElements())) {
                List<ExecutableElement> lee;
                if (cancel.get()) {
                    return null;
                }
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "method: {0}", ee.toString());
                }
                if ((lee = name2Method.get(ee.getSimpleName())) == null || lee.isEmpty()) continue;
                HashSet<ExecutableElement> seenMethods = new HashSet<ExecutableElement>();
                LinkedList<ElementDescription> descriptions = new LinkedList<ElementDescription>();
                for (ExecutableElement overridee : lee) {
                    if (!info.getElements().overrides(ee, overridee, SourceUtils.getEnclosingTypeElement((Element)ee)) || !seenMethods.add(overridee)) continue;
                    descriptions.add(new ElementDescription(info, overridee, false));
                }
                if (descriptions.isEmpty()) continue;
                result.put((ElementHandle)ElementHandle.create((Element)ee), descriptions);
            }
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static DataHolder getDataFromCache(CompilationInfo info) {
        Elements elements = info.getElements();
        Map<Reference<Elements>, DataHolder> map = CACHE;
        synchronized (map) {
            Iterator<Map.Entry<Reference<Elements>, DataHolder>> it = CACHE.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<Reference<Elements>, DataHolder> e = it.next();
                if (e.getKey().get() == elements) {
                    return e.getValue();
                }
                it.remove();
            }
            DataHolder holder = new DataHolder();
            CACHE.put(new CleaningWR(info.getElements()), new DataHolder());
            return holder;
        }
    }

    private static final class CleaningWR
    extends WeakReference<Elements>
    implements Runnable {
        public CleaningWR(Elements el) {
            super(el, Utilities.activeReferenceQueue());
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            Map map = CACHE;
            synchronized (map) {
                CACHE.remove(this);
            }
        }
    }

    private static final class DataHolder {
        private final Map<ElementHandle<TypeElement>, Map<ElementHandle<ExecutableElement>, List<ElementDescription>>> data = new HashMap<ElementHandle<TypeElement>, Map<ElementHandle<ExecutableElement>, List<ElementDescription>>>();
    }

}

