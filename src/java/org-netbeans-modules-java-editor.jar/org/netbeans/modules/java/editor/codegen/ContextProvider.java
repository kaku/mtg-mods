/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.util.TreePath
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.spi.editor.codegen.CodeGeneratorContextProvider
 *  org.netbeans.spi.editor.codegen.CodeGeneratorContextProvider$Task
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.modules.java.editor.codegen;

import com.sun.source.util.TreePath;
import java.io.IOException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.spi.editor.codegen.CodeGeneratorContextProvider;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

public class ContextProvider
implements CodeGeneratorContextProvider {
    public void runTaskWithinContext(final Lookup context, final CodeGeneratorContextProvider.Task task) {
        JTextComponent component = (JTextComponent)context.lookup(JTextComponent.class);
        if (component != null) {
            try {
                JavaSource js = JavaSource.forDocument((Document)component.getDocument());
                if (js != null) {
                    final int caretOffset = component.getCaretPosition();
                    js.runUserActionTask((Task)new Task<CompilationController>(){

                        public void run(CompilationController controller) throws Exception {
                            controller.toPhase(JavaSource.Phase.PARSED);
                            TreePath path = controller.getTreeUtilities().pathFor(caretOffset);
                            ProxyLookup newContext = new ProxyLookup(new Lookup[]{context, Lookups.fixed((Object[])new Object[]{controller, path})});
                            task.run((Lookup)newContext);
                        }
                    }, true);
                    return;
                }
            }
            catch (IOException ioe) {
                // empty catch block
            }
        }
        task.run(context);
    }

}

