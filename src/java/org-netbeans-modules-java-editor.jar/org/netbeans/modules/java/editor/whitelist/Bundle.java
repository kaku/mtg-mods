/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.whitelist;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String LBL_ProviderDescription() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_ProviderDescription");
    }

    static String LBL_ProviderName() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_ProviderName");
    }

    static String MSG_Violations() {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_Violations");
    }

    private void Bundle() {
    }
}

