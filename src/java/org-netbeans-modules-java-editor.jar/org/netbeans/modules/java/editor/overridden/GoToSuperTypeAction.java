/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.progress.ProgressUtils
 *  org.netbeans.editor.BaseAction
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.overridden;

import com.sun.source.tree.Tree;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.editor.BaseAction;
import org.netbeans.modules.editor.java.GoToSupport;
import org.netbeans.modules.editor.java.JavaKit;
import org.netbeans.modules.java.editor.overridden.AnnotationType;
import org.netbeans.modules.java.editor.overridden.ComputeOverriding;
import org.netbeans.modules.java.editor.overridden.ElementDescription;
import org.netbeans.modules.java.editor.overridden.IsOverriddenAnnotationAction;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class GoToSuperTypeAction
extends BaseAction {
    public GoToSuperTypeAction() {
        super("goto-super-implementation", 132);
        this.putValue("ShortDescription", (Object)NbBundle.getBundle(JavaKit.class).getString("goto-super-implementation"));
        String name = NbBundle.getBundle(JavaKit.class).getString("goto-super-implementation-trimmed");
        this.putValue("trimmed-text", (Object)name);
        this.putValue("PopupMenuText", (Object)name);
    }

    public void actionPerformed(ActionEvent evt, final JTextComponent target) {
        final JavaSource js = JavaSource.forDocument((Document)target.getDocument());
        if (js == null) {
            StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(GoToSupport.class, (String)"WARN_CannotGoToGeneric", (Object)1));
            return;
        }
        final int caretPos = target.getCaretPosition();
        final AtomicBoolean cancel = new AtomicBoolean();
        ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

            @Override
            public void run() {
                GoToSuperTypeAction.goToImpl(target, js, caretPos, cancel);
            }
        }, (String)NbBundle.getMessage(JavaKit.class, (String)"goto-super-implementation"), (AtomicBoolean)cancel, (boolean)false);
    }

    private static void goToImpl(final JTextComponent c, JavaSource js, final int caretPos, final AtomicBoolean cancel) {
        try {
            js.runUserActionTask((Task)new Task<CompilationController>(){

                public void run(CompilationController parameter) throws Exception {
                    if (cancel != null && cancel.get()) {
                        return;
                    }
                    parameter.toPhase(JavaSource.Phase.RESOLVED);
                    ExecutableElement ee = GoToSuperTypeAction.resolveMethodElement((CompilationInfo)parameter, caretPos);
                    if (ee == null) {
                        ee = GoToSuperTypeAction.resolveMethodElement((CompilationInfo)parameter, caretPos + 1);
                    }
                    if (ee == null) {
                        Toolkit.getDefaultToolkit().beep();
                        return;
                    }
                    final ArrayList<ElementDescription> result = new ArrayList<ElementDescription>();
                    final AnnotationType type = ComputeOverriding.detectOverrides((CompilationInfo)parameter, (TypeElement)ee.getEnclosingElement(), ee, result);
                    if (type == null) {
                        Toolkit.getDefaultToolkit().beep();
                        return;
                    }
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            try {
                                Point p = c.modelToView(c.getCaretPosition()).getLocation();
                                IsOverriddenAnnotationAction.mouseClicked(Collections.singletonMap(IsOverriddenAnnotationAction.computeCaption(type, ""), result), c, p);
                            }
                            catch (BadLocationException e) {
                                Exceptions.printStackTrace((Throwable)e);
                            }
                        }
                    });
                }

            }, true);
        }
        catch (IOException e) {
            Exceptions.printStackTrace((Throwable)e);
        }
    }

    private static ExecutableElement resolveMethodElement(CompilationInfo info, int caret) {
        TreePath path;
        for (path = info.getTreeUtilities().pathFor((int)caret); path != null && path.getLeaf().getKind() != Tree.Kind.METHOD; path = path.getParentPath()) {
        }
        if (path == null) {
            return null;
        }
        Element resolved = info.getTrees().getElement(path);
        if (resolved == null || resolved.getKind() != ElementKind.METHOD) {
            return null;
        }
        return (ExecutableElement)resolved;
    }

}

