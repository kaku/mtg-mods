/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.ModificationResult
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.java.source.TreePathHandle
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.WorkingCopy
 *  org.netbeans.editor.Utilities
 *  org.netbeans.spi.editor.codegen.CodeGenerator
 *  org.netbeans.spi.editor.codegen.CodeGenerator$Factory
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.codegen;

import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.awt.Dialog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.NestingKind;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.TreePathHandle;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.java.editor.codegen.GeneratorUtils;
import org.netbeans.modules.java.editor.codegen.ui.ConstructorPanel;
import org.netbeans.modules.java.editor.codegen.ui.ElementNode;
import org.netbeans.spi.editor.codegen.CodeGenerator;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class ConstructorGenerator
implements CodeGenerator {
    private JTextComponent component;
    private TreePathHandle typeHandle;
    private ElementHandle<? extends Element> constructorHandle;
    private ElementNode.Description constructorDescription;
    private ElementNode.Description fieldsDescription;
    private final WorkingCopy existingWorkingCopy;

    private ConstructorGenerator(JTextComponent component, TreePathHandle typeHandle, ElementHandle<? extends Element> constructorHandle, ElementNode.Description constructorDescription, ElementNode.Description fieldsDescription, WorkingCopy wc) {
        this.component = component;
        this.typeHandle = typeHandle;
        this.constructorHandle = constructorHandle;
        this.constructorDescription = constructorDescription;
        this.fieldsDescription = fieldsDescription;
        this.existingWorkingCopy = wc;
    }

    public String getDisplayName() {
        return NbBundle.getMessage(ConstructorGenerator.class, (String)"LBL_constructor");
    }

    public void invoke() {
        List<ElementHandle<? extends Element>> constrHandles;
        List<ElementHandle<? extends Element>> fieldHandles;
        final int caretOffset = this.component.getCaretPosition();
        if (this.constructorDescription != null || this.fieldsDescription != null) {
            ConstructorPanel panel = new ConstructorPanel(this.constructorDescription, this.fieldsDescription);
            DialogDescriptor dialogDescriptor = GeneratorUtils.createDialogDescriptor(panel, NbBundle.getMessage(ConstructorGenerator.class, (String)"LBL_generate_constructor"));
            Dialog dialog = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
            dialog.setVisible(true);
            if (dialogDescriptor.getValue() != dialogDescriptor.getDefaultValue()) {
                return;
            }
            constrHandles = this.constructorHandle == null ? panel.getInheritedConstructors() : null;
            fieldHandles = panel.getVariablesToInitialize();
        } else {
            fieldHandles = null;
            constrHandles = null;
        }
        try {
            if (this.existingWorkingCopy == null) {
                JavaSource js = JavaSource.forDocument((Document)this.component.getDocument());
                if (js != null) {
                    ModificationResult mr = js.runModificationTask((Task)new Task<WorkingCopy>(){

                        public void run(WorkingCopy copy) throws IOException {
                            ConstructorGenerator.this.doGenerateConstructor(copy, fieldHandles, constrHandles, caretOffset);
                        }
                    });
                    GeneratorUtils.guardedCommit(this.component, mr);
                }
            } else {
                this.doGenerateConstructor(this.existingWorkingCopy, fieldHandles, constrHandles, caretOffset);
            }
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    private void doGenerateConstructor(WorkingCopy copy, List<ElementHandle<? extends Element>> fieldHandles, List<ElementHandle<? extends Element>> constrHandles, int caretOffset) throws IOException {
        copy.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
        TreePath path = this.typeHandle.resolve((CompilationInfo)copy);
        if (path == null) {
            path = copy.getTreeUtilities().pathFor(caretOffset);
        }
        if ((path = org.netbeans.modules.editor.java.Utilities.getPathElementOfKind(TreeUtilities.CLASS_TREE_KINDS, path)) == null) {
            String message = NbBundle.getMessage(ConstructorGenerator.class, (String)"ERR_CannotFindOriginalClass");
            Utilities.setStatusBoldText((JTextComponent)this.component, (String)message);
        } else {
            ArrayList<VariableElement> variableElements = new ArrayList<VariableElement>();
            if (fieldHandles != null) {
                for (ElementHandle<? extends Element> elementHandle : fieldHandles) {
                    VariableElement field = (VariableElement)elementHandle.resolve((CompilationInfo)copy);
                    if (field == null) {
                        return;
                    }
                    variableElements.add(field);
                }
            }
            if (constrHandles != null && !constrHandles.isEmpty()) {
                ArrayList<ExecutableElement> constrElements = new ArrayList<ExecutableElement>();
                for (ElementHandle<? extends Element> elementHandle : constrHandles) {
                    ExecutableElement constr = (ExecutableElement)elementHandle.resolve((CompilationInfo)copy);
                    if (constr == null) {
                        return;
                    }
                    constrElements.add(constr);
                }
                GeneratorUtils.generateConstructors(copy, path, variableElements, constrElements, caretOffset);
            } else {
                GeneratorUtils.generateConstructor(copy, path, variableElements, this.constructorHandle != null ? (ExecutableElement)this.constructorHandle.resolve((CompilationInfo)copy) : null, caretOffset);
            }
        }
    }

    public static class Factory
    implements CodeGenerator.Factory {
        public List<? extends CodeGenerator> create(Lookup context) {
            ArrayList<ConstructorGenerator> ret = new ArrayList<ConstructorGenerator>();
            JTextComponent component = (JTextComponent)context.lookup(JTextComponent.class);
            CompilationController controller = (CompilationController)context.lookup(CompilationController.class);
            TreePath path = (TreePath)context.lookup(TreePath.class);
            TreePath treePath = path = path != null ? org.netbeans.modules.editor.java.Utilities.getPathElementOfKind(TreeUtilities.CLASS_TREE_KINDS, path) : null;
            if (component == null || controller == null || path == null) {
                return ret;
            }
            try {
                controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
            }
            catch (IOException ioe) {
                return ret;
            }
            TypeElement typeElement = (TypeElement)controller.getTrees().getElement(path);
            if (typeElement == null || !typeElement.getKind().isClass() || NestingKind.ANONYMOUS.equals((Object)typeElement.getNestingKind())) {
                return ret;
            }
            WorkingCopy wc = (WorkingCopy)context.lookup(WorkingCopy.class);
            LinkedHashSet<VariableElement> initializedFields = new LinkedHashSet<VariableElement>();
            LinkedHashSet<VariableElement> uninitializedFields = new LinkedHashSet<VariableElement>();
            ArrayList<ExecutableElement> constructors = new ArrayList<ExecutableElement>();
            ArrayList<ExecutableElement> inheritedConstructors = new ArrayList<ExecutableElement>();
            TypeMirror superClassType = typeElement.getSuperclass();
            TypeElement superClass = null;
            if (superClassType.getKind() == TypeKind.DECLARED) {
                superClass = (TypeElement)((DeclaredType)superClassType).asElement();
                for (ExecutableElement executableElement : ElementFilter.constructorsIn(superClass.getEnclosedElements())) {
                    PackageElement currentPackage = controller.getElements().getPackageOf(typeElement);
                    PackageElement ctorPackage = controller.getElements().getPackageOf(executableElement);
                    Set<Modifier> ctorMods = executableElement.getModifiers();
                    if (currentPackage != ctorPackage && !ctorMods.contains((Object)Modifier.PUBLIC) && !ctorMods.contains((Object)Modifier.PROTECTED) || ctorMods.contains((Object)Modifier.PRIVATE)) continue;
                    inheritedConstructors.add(executableElement);
                }
            }
            GeneratorUtils.scanForFieldsAndConstructors((CompilationInfo)controller, path, initializedFields, uninitializedFields, constructors);
            ElementHandle constructorHandle = null;
            ElementNode.Description constructorDescription = null;
            if (typeElement.getKind() != ElementKind.ENUM && inheritedConstructors.size() == 1) {
                constructorHandle = ElementHandle.create((Element)((Element)inheritedConstructors.get(0)));
            } else if (inheritedConstructors.size() > 1) {
                ArrayList<ElementNode.Description> constructorDescriptions = new ArrayList<ElementNode.Description>();
                for (ExecutableElement constructorElement : inheritedConstructors) {
                    constructorDescriptions.add(ElementNode.Description.create((CompilationInfo)controller, constructorElement, null, true, false));
                }
                constructorDescription = ElementNode.Description.create((CompilationInfo)controller, superClass, constructorDescriptions, false, false);
            }
            ElementNode.Description fieldsDescription = null;
            if (!uninitializedFields.isEmpty()) {
                ArrayList<ElementNode.Description> fieldDescriptions = new ArrayList<ElementNode.Description>();
                for (VariableElement variableElement : uninitializedFields) {
                    fieldDescriptions.add(ElementNode.Description.create((CompilationInfo)controller, variableElement, null, true, variableElement.getModifiers().contains((Object)Modifier.FINAL)));
                }
                fieldsDescription = ElementNode.Description.create((CompilationInfo)controller, typeElement, fieldDescriptions, false, false);
            }
            if (constructorHandle != null || constructorDescription != null || fieldsDescription != null) {
                ret.add(new ConstructorGenerator(component, TreePathHandle.create((TreePath)path, (CompilationInfo)controller), constructorHandle, constructorDescription, fieldsDescription, wc));
            }
            return ret;
        }
    }

}

