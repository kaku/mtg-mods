/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.completion.support.CompletionUtilities
 *  org.openide.util.ImageUtilities
 */
package org.netbeans.modules.java.editor.overridden;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import org.netbeans.modules.java.editor.overridden.ElementDescription;
import org.netbeans.modules.java.editor.overridden.PopupUtil;
import org.netbeans.spi.editor.completion.support.CompletionUtilities;
import org.openide.util.ImageUtilities;

public class IsOverriddenPopup
extends JPanel
implements FocusListener {
    private String caption;
    private List<ElementDescription> declarations;
    private JLabel jLabel1;
    private JList jList1;
    private JScrollPane jScrollPane1;

    public IsOverriddenPopup(String caption, List<ElementDescription> declarations) {
        this.caption = caption;
        this.declarations = declarations;
        Collections.sort(declarations, new Comparator<ElementDescription>(){

            @Override
            public int compare(ElementDescription o1, ElementDescription o2) {
                if (o1.isOverridden() == o2.isOverridden()) {
                    return o1.getDisplayName().compareTo(o2.getDisplayName());
                }
                if (o1.isOverridden()) {
                    return 1;
                }
                return -1;
            }
        });
        this.initComponents();
        this.jList1.setCursor(Cursor.getPredefinedCursor(12));
        this.addFocusListener(this);
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this.jList1 = new JList();
        this.setFocusCycleRoot(true);
        this.setLayout(new GridBagLayout());
        this.jLabel1.setHorizontalAlignment(0);
        this.jLabel1.setText(this.caption);
        this.jLabel1.setFocusable(false);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 2;
        this.add((Component)this.jLabel1, gridBagConstraints);
        this.jList1.setModel(this.createListModel());
        this.jList1.setCellRenderer(new RendererImpl());
        this.jList1.setSelectedIndex(0);
        this.jList1.setVisibleRowCount(this.declarations.size());
        this.jList1.addKeyListener(new KeyAdapter(){

            @Override
            public void keyPressed(KeyEvent evt) {
                IsOverriddenPopup.this.jList1KeyPressed(evt);
            }
        });
        this.jList1.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent evt) {
                IsOverriddenPopup.this.jList1MouseClicked(evt);
            }
        });
        this.jScrollPane1.setViewportView(this.jList1);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.jScrollPane1, gridBagConstraints);
    }

    private void jList1MouseClicked(MouseEvent evt) {
        if (evt.getButton() == 1 && evt.getClickCount() == 1) {
            this.openSelected();
        }
    }

    private void jList1KeyPressed(KeyEvent evt) {
        if (evt.getKeyCode() == 10 && evt.getModifiers() == 0) {
            this.openSelected();
        }
    }

    private void openSelected() {
        ElementDescription desc = (ElementDescription)this.jList1.getSelectedValue();
        if (desc != null) {
            desc.open();
        }
        PopupUtil.hidePopup();
    }

    private ListModel createListModel() {
        DefaultListModel<ElementDescription> dlm = new DefaultListModel<ElementDescription>();
        for (ElementDescription el : this.declarations) {
            dlm.addElement(el);
        }
        return dlm;
    }

    @Override
    public void focusGained(FocusEvent arg0) {
        this.jList1.requestFocus();
        this.jList1.requestFocusInWindow();
    }

    @Override
    public void focusLost(FocusEvent arg0) {
    }

    private static class RendererImpl
    extends DefaultListCellRenderer {
        private static final int DARKER_COLOR_COMPONENT = 5;
        private boolean selected;

        private RendererImpl() {
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            if (value instanceof ElementDescription) {
                ElementDescription desc = (ElementDescription)value;
                this.setIcon(desc.getIcon());
                this.setText(desc.getDisplayName());
            }
            this.selected = isSelected;
            if (!isSelected) {
                Color bgColor = list.getBackground();
                if (index % 2 == 0) {
                    bgColor = new Color(Math.abs(bgColor.getRed() - 5), Math.abs(bgColor.getGreen() - 5), Math.abs(bgColor.getBlue() - 5));
                }
                if (this.getBackground() != bgColor) {
                    this.setBackground(bgColor);
                }
            }
            return this;
        }

        @Override
        public void paintComponent(Graphics g) {
            Color bgColor = this.getBackground();
            Color fgColor = this.getForeground();
            g.setColor(bgColor);
            g.fillRect(0, 0, this.getWidth(), this.getHeight());
            g.setColor(fgColor);
            CompletionUtilities.renderHtml((ImageIcon)new ImageIcon(ImageUtilities.icon2Image((Icon)this.getIcon())), (String)this.getText(), (String)null, (Graphics)g, (Font)this.getFont(), (Color)fgColor, (int)this.getWidth(), (int)this.getHeight(), (boolean)this.selected);
        }
    }

}

