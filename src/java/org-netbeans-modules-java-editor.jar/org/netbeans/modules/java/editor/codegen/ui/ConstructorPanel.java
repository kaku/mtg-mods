/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.source.ElementHandle
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.codegen.ui;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.util.Collections;
import java.util.List;
import javax.accessibility.AccessibleContext;
import javax.lang.model.element.Element;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.modules.java.editor.codegen.ConstructorGenerator;
import org.netbeans.modules.java.editor.codegen.ui.ElementNode;
import org.netbeans.modules.java.editor.codegen.ui.ElementSelectorPanel;
import org.openide.util.NbBundle;

public class ConstructorPanel
extends JPanel {
    private JLabel constructorSelectorLabel;
    private ElementSelectorPanel constructorSelector;
    private JLabel fieldSelectorLabel;
    private ElementSelectorPanel fieldSelector;

    public ConstructorPanel(ElementNode.Description constructorDescription, ElementNode.Description fieldsDescription) {
        GridBagConstraints gridBagConstraints;
        this.initComponents();
        if (constructorDescription != null) {
            this.constructorSelectorLabel = new JLabel();
            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 0;
            gridBagConstraints.gridy = 0;
            gridBagConstraints.fill = 1;
            gridBagConstraints.anchor = 17;
            gridBagConstraints.insets = new Insets(12, 12, 6, 12);
            this.add((Component)this.constructorSelectorLabel, gridBagConstraints);
            this.constructorSelector = new ElementSelectorPanel(constructorDescription, false);
            gridBagConstraints.gridy = 1;
            gridBagConstraints.weightx = 0.5;
            gridBagConstraints.weighty = 1.0;
            gridBagConstraints.insets = new Insets(0, 12, 0, 12);
            this.add((Component)this.constructorSelector, gridBagConstraints);
            this.constructorSelectorLabel.setText(NbBundle.getMessage(ConstructorGenerator.class, (String)"LBL_super_constructor_select"));
            this.constructorSelectorLabel.setLabelFor(this.constructorSelector);
        }
        if (fieldsDescription != null) {
            this.fieldSelectorLabel = new JLabel();
            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 1;
            gridBagConstraints.gridy = 0;
            gridBagConstraints.fill = 1;
            gridBagConstraints.anchor = 17;
            gridBagConstraints.insets = new Insets(12, constructorDescription != null ? 0 : 12, 6, 12);
            this.add((Component)this.fieldSelectorLabel, gridBagConstraints);
            this.fieldSelector = new ElementSelectorPanel(fieldsDescription, false);
            gridBagConstraints.gridy = 1;
            gridBagConstraints.weightx = 0.5;
            gridBagConstraints.weighty = 1.0;
            gridBagConstraints.insets = new Insets(0, constructorDescription != null ? 0 : 12, 0, 12);
            this.add((Component)this.fieldSelector, gridBagConstraints);
            this.fieldSelectorLabel.setText(NbBundle.getMessage(ConstructorGenerator.class, (String)"LBL_constructor_select"));
            this.fieldSelectorLabel.setLabelFor(this.fieldSelector);
        }
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ConstructorGenerator.class, (String)"A11Y_Generate_Constructor"));
    }

    public ElementHandle<? extends Element> getInheritedConstructor() {
        if (this.constructorSelector == null) {
            return null;
        }
        List<ElementHandle<? extends Element>> handles = this.constructorSelector.getSelectedElements();
        return handles.size() == 1 ? handles.get(0) : null;
    }

    public List<ElementHandle<? extends Element>> getInheritedConstructors() {
        if (this.constructorSelector == null) {
            return Collections.EMPTY_LIST;
        }
        return this.constructorSelector.getSelectedElements();
    }

    public List<ElementHandle<? extends Element>> getVariablesToInitialize() {
        if (this.fieldSelector == null) {
            return null;
        }
        return this.fieldSelector.getSelectedElements();
    }

    private void initComponents() {
        this.setLayout(new GridBagLayout());
    }
}

