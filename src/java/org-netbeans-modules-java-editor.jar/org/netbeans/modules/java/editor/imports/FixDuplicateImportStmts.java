/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.imports;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.netbeans.modules.java.editor.imports.JavaFixAllImports;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class FixDuplicateImportStmts
extends JPanel {
    private JComboBox[] combos;
    private JCheckBox checkUnusedImports;
    private JPanel bottomPanel;
    private JPanel contentPanel;
    private JScrollPane jScrollPane1;
    private JLabel lblHeader;
    private JLabel lblTitle;

    public FixDuplicateImportStmts() {
        this.initComponents();
    }

    public void initPanel(JavaFixAllImports.ImportData data, boolean removeUnusedImports) {
        this.initComponentsMore(data, removeUnusedImports);
        this.setAccessible();
    }

    private void initComponentsMore(JavaFixAllImports.ImportData data, boolean removeUnusedImports) {
        this.contentPanel.setLayout(new GridBagLayout());
        this.contentPanel.setBackground(UIManager.getColor("Table.background"));
        this.jScrollPane1.setBorder(UIManager.getBorder("ScrollPane.border"));
        this.jScrollPane1.getVerticalScrollBar().setUnitIncrement(new JLabel((String)"X").getPreferredSize().height);
        this.jScrollPane1.getVerticalScrollBar().setBlockIncrement(new JLabel((String)"X").getPreferredSize().height * 10);
        if (data.variants.length > 0) {
            int row = 0;
            this.combos = new JComboBox[data.variants.length];
            Font monoSpaced = new Font("Monospaced", 0, new JLabel().getFont().getSize());
            FocusListener focusListener = new FocusListener(){

                @Override
                public void focusGained(FocusEvent e) {
                    Component c = e.getComponent();
                    Rectangle r = c.getBounds();
                    FixDuplicateImportStmts.this.contentPanel.scrollRectToVisible(r);
                }

                @Override
                public void focusLost(FocusEvent arg0) {
                }
            };
            for (int i = 0; i < data.variants.length; ++i) {
                this.combos[i] = this.createComboBox(data.variants[i], data.defaults[i], monoSpaced, focusListener);
                JLabel lblSimpleName = new JLabel(data.simpleNames[i]);
                lblSimpleName.setOpaque(false);
                lblSimpleName.setFont(monoSpaced);
                lblSimpleName.setLabelFor(this.combos[i]);
                this.contentPanel.add((Component)lblSimpleName, new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, 17, 0, new Insets(3, 5, 2, 5), 0, 0));
                this.contentPanel.add((Component)this.combos[i], new GridBagConstraints(1, row++, 1, 1, 1.0, 0.0, 17, 2, new Insets(3, 5, 2, 5), 0, 0));
            }
            this.contentPanel.add((Component)new JLabel(), new GridBagConstraints(2, row, 2, 1, 0.0, 1.0, 10, 0, new Insets(0, 0, 0, 0), 0, 0));
            Dimension d = this.contentPanel.getPreferredSize();
            d.height = this.getRowHeight() * Math.min(this.combos.length, 6);
            this.jScrollPane1.getViewport().setPreferredSize(d);
        } else {
            this.contentPanel.add((Component)new JLabel(FixDuplicateImportStmts.getBundleString("FixDupImportStmts_NothingToFix")), new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, 10, 0, new Insets(20, 20, 20, 20), 0, 0));
        }
        this.lblTitle.setText(FixDuplicateImportStmts.getBundleString("FixDupImportStmts_IntroLbl"));
        this.lblHeader.setText(FixDuplicateImportStmts.getBundleString("FixDupImportStmts_Header"));
        this.checkUnusedImports = new JCheckBox();
        Mnemonics.setLocalizedText((AbstractButton)this.checkUnusedImports, (String)FixDuplicateImportStmts.getBundleString("FixDupImportStmts_UnusedImports"));
        this.bottomPanel.add((Component)this.checkUnusedImports, "West");
        this.checkUnusedImports.setEnabled(true);
        this.checkUnusedImports.setSelected(removeUnusedImports);
    }

    private JComboBox createComboBox(JavaFixAllImports.CandidateDescription[] choices, JavaFixAllImports.CandidateDescription defaultValue, Font font, FocusListener listener) {
        JComboBox<JavaFixAllImports.CandidateDescription> combo = new JComboBox<JavaFixAllImports.CandidateDescription>(choices);
        combo.setSelectedItem(defaultValue);
        combo.getAccessibleContext().setAccessibleDescription(FixDuplicateImportStmts.getBundleString("FixDupImportStmts_Combo_ACSD"));
        combo.getAccessibleContext().setAccessibleName(FixDuplicateImportStmts.getBundleString("FixDupImportStmts_Combo_Name_ACSD"));
        combo.setOpaque(false);
        combo.setFont(font);
        combo.addFocusListener(listener);
        combo.setEnabled(choices.length > 1);
        combo.setRenderer(new DelegatingRenderer(combo.getRenderer()));
        InputMap inputMap = combo.getInputMap(0);
        inputMap.put(KeyStroke.getKeyStroke(32, 0), "showPopup");
        combo.getActionMap().put("showPopup", new TogglePopupAction());
        return combo;
    }

    private int getRowHeight() {
        return this.combos.length == 0 ? 0 : this.combos[0].getPreferredSize().height + 6;
    }

    private static String getBundleString(String s) {
        return NbBundle.getMessage(FixDuplicateImportStmts.class, (String)s);
    }

    private void setAccessible() {
        this.getAccessibleContext().setAccessibleDescription(FixDuplicateImportStmts.getBundleString("FixDupImportStmts_IntroLbl"));
        this.checkUnusedImports.getAccessibleContext().setAccessibleDescription(FixDuplicateImportStmts.getBundleString("FixDupImportStmts_checkUnusedImports_a11y"));
    }

    public JavaFixAllImports.CandidateDescription[] getSelections() {
        JavaFixAllImports.CandidateDescription[] res = new JavaFixAllImports.CandidateDescription[null == this.combos ? 0 : this.combos.length];
        for (int i = 0; i < res.length; ++i) {
            res[i] = (JavaFixAllImports.CandidateDescription)this.combos[i].getSelectedItem();
        }
        return res;
    }

    public boolean getRemoveUnusedImports() {
        return this.checkUnusedImports.isSelected();
    }

    private void initComponents() {
        this.lblTitle = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this.contentPanel = new JPanel();
        this.bottomPanel = new JPanel();
        this.lblHeader = new JLabel();
        this.setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));
        this.setPreferredSize(null);
        this.setLayout(new GridBagLayout());
        this.lblTitle.setText("~Select the fully qualified name to use in the import statement.");
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new Insets(0, 0, 6, 0);
        this.add((Component)this.lblTitle, gridBagConstraints);
        this.jScrollPane1.setBorder(null);
        this.contentPanel.setLayout(new GridBagLayout());
        this.jScrollPane1.setViewportView(this.contentPanel);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.jScrollPane1, gridBagConstraints);
        this.bottomPanel.setLayout(new BorderLayout());
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        this.add((Component)this.bottomPanel, gridBagConstraints);
        this.lblHeader.setText("~Import Statements:");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 0, 3, 0);
        this.add((Component)this.lblHeader, gridBagConstraints);
    }

    private static class TogglePopupAction
    extends AbstractAction {
        private TogglePopupAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() instanceof JComboBox) {
                JComboBox combo;
                combo.setPopupVisible(!(combo = (JComboBox)e.getSource()).isPopupVisible());
            }
        }
    }

    private static class DelegatingRenderer
    implements ListCellRenderer {
        private ListCellRenderer orig;

        public DelegatingRenderer(ListCellRenderer orig) {
            this.orig = orig;
        }

        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            if (value instanceof JavaFixAllImports.CandidateDescription) {
                JavaFixAllImports.CandidateDescription cd = (JavaFixAllImports.CandidateDescription)value;
                Component res = this.orig.getListCellRendererComponent(list, cd.displayName, index, isSelected, cellHasFocus);
                if (res instanceof JLabel) {
                    ((JLabel)res).setIcon(cd.icon);
                }
                return res;
            }
            return this.orig.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        }
    }

}

