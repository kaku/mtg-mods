/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.editor.fold.Fold
 *  org.netbeans.api.editor.fold.FoldHierarchy
 *  org.netbeans.api.editor.fold.FoldTemplate
 *  org.netbeans.api.editor.fold.FoldType
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.support.CancellableTreePathScanner
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.ext.java.JavaFoldManager
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.spi.editor.fold.FoldHierarchyTransaction
 *  org.netbeans.spi.editor.fold.FoldInfo
 *  org.netbeans.spi.editor.fold.FoldOperation
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.java.editor.fold;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldTemplate;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.support.CancellableTreePathScanner;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.ext.java.JavaFoldManager;
import org.netbeans.modules.java.editor.fold.JavaElementFoldManagerTaskFactory;
import org.netbeans.modules.java.editor.semantic.ScanningCancellableTask;
import org.netbeans.modules.java.editor.semantic.Utilities;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.spi.editor.fold.FoldHierarchyTransaction;
import org.netbeans.spi.editor.fold.FoldInfo;
import org.netbeans.spi.editor.fold.FoldOperation;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

public class JavaElementFoldManager
extends JavaFoldManager {
    private FoldOperation operation;
    private FileObject file;
    private JavaElementFoldTask task;
    private boolean first = true;

    public void init(FoldOperation operation) {
        this.operation = operation;
    }

    public synchronized void initFolds(FoldHierarchyTransaction transaction) {
        Document doc = this.operation.getHierarchy().getComponent().getDocument();
        Object od = doc.getProperty("stream");
        if (od instanceof DataObject) {
            FileObject file = ((DataObject)od).getPrimaryFile();
            this.task = JavaElementFoldTask.getTask(file);
            this.task.setJavaElementFoldManager(this, file);
        }
    }

    public void insertUpdate(DocumentEvent evt, FoldHierarchyTransaction transaction) {
        this.invalidate();
    }

    public void removeUpdate(DocumentEvent evt, FoldHierarchyTransaction transaction) {
        this.invalidate();
    }

    public void changedUpdate(DocumentEvent evt, FoldHierarchyTransaction transaction) {
    }

    public void removeEmptyNotify(Fold emptyFold) {
        this.removeDamagedNotify(emptyFold);
    }

    public void removeDamagedNotify(Fold damagedFold) {
    }

    public void expandNotify(Fold expandedFold) {
    }

    public synchronized void release() {
        if (this.task != null) {
            this.task.setJavaElementFoldManager(this, null);
        }
        this.task = null;
        this.file = null;
    }

    private synchronized void invalidate() {
        if (this.task != null) {
            this.task.invalidate();
        }
    }

    private static final class JavaElementFoldVisitor
    extends CancellableTreePathScanner<Object, Object> {
        private List<Integer> anchors = new ArrayList<Integer>();
        private List<FoldInfo> folds = new ArrayList<FoldInfo>();
        private CompilationInfo info;
        private CompilationUnitTree cu;
        private SourcePositions sp;
        private boolean stopped;
        private int initialCommentStopPos = Integer.MAX_VALUE;
        private Document doc;
        int expandMembersAtCaret = -1;

        public JavaElementFoldVisitor(CompilationInfo info, CompilationUnitTree cu, SourcePositions sp, Document doc) {
            this.info = info;
            this.cu = cu;
            this.sp = sp;
            this.doc = doc;
        }

        private void addFold(FoldInfo f, int anchor) {
            this.folds.add(f);
            this.anchors.add(anchor);
        }

        public void checkInitialFold() {
            try {
                TokenHierarchy th = this.info.getTokenHierarchy();
                TokenSequence ts = th.tokenSequence(JavaTokenId.language());
                while (ts.moveNext() && ts.offset() < this.initialCommentStopPos) {
                    Token token = ts.token();
                    if (token.id() != JavaTokenId.BLOCK_COMMENT && token.id() != JavaTokenId.JAVADOC_COMMENT) continue;
                    int startOffset = ts.offset();
                    this.addFold(FoldInfo.range((int)startOffset, (int)(startOffset + token.length()), (FoldType)JavaFoldManager.INITIAL_COMMENT_FOLD_TYPE), startOffset);
                    break;
                }
            }
            catch (ConcurrentModificationException e) {
                this.stopped = true;
            }
        }

        private void handleJavadoc(Tree t) throws BadLocationException, ConcurrentModificationException {
            TokenSequence ts;
            TokenHierarchy th;
            int start = (int)this.sp.getStartPosition(this.cu, t);
            if (start == -1) {
                return;
            }
            if (start < this.initialCommentStopPos) {
                this.initialCommentStopPos = start;
            }
            if ((ts = (th = this.info.getTokenHierarchy()).tokenSequence(JavaTokenId.language())).move(start) == Integer.MAX_VALUE) {
                return;
            }
            while (ts.movePrevious()) {
                Token token = ts.token();
                if (token.id() == JavaTokenId.JAVADOC_COMMENT) {
                    int startOffset = ts.offset();
                    this.addFold(FoldInfo.range((int)startOffset, (int)(startOffset + token.length()), (FoldType)JavaFoldManager.JAVADOC_FOLD_TYPE), startOffset);
                    if (startOffset < this.initialCommentStopPos) {
                        this.initialCommentStopPos = startOffset;
                    }
                }
                if (token.id() == JavaTokenId.WHITESPACE || token.id() == JavaTokenId.BLOCK_COMMENT || token.id() == JavaTokenId.LINE_COMMENT) continue;
                break;
            }
        }

        private void handleTree(Tree node, Tree javadocTree, boolean handleOnlyJavadoc) {
            this.handleTree((int)this.sp.getStartPosition(this.cu, node), node, javadocTree, handleOnlyJavadoc);
        }

        private void handleTree(int symStart, Tree node, Tree javadocTree, boolean handleOnlyJavadoc) {
            try {
                if (!handleOnlyJavadoc) {
                    int start = (int)this.sp.getStartPosition(this.cu, node);
                    int end = (int)this.sp.getEndPosition(this.cu, node);
                    if (start != -1 && end != -1) {
                        FoldInfo fi = FoldInfo.range((int)start, (int)end, (FoldType)JavaFoldManager.CODE_BLOCK_FOLD_TYPE);
                        this.addFold(fi, symStart);
                    }
                }
                this.handleJavadoc(javadocTree != null ? javadocTree : node);
            }
            catch (BadLocationException e) {
                this.stopped = true;
            }
            catch (ConcurrentModificationException e) {
                this.stopped = true;
            }
        }

        public Object visitMethod(MethodTree node, Object p) {
            CancellableTreePathScanner.super.visitMethod(node, p);
            this.handleTree((int)this.sp.getStartPosition(this.cu, (Tree)node), (Tree)node.getBody(), (Tree)node, false);
            return null;
        }

        public Object visitClass(ClassTree node, Object p) {
            CancellableTreePathScanner.super.visitClass(node, (Object)Boolean.TRUE);
            try {
                if (p == Boolean.TRUE) {
                    int start = Utilities.findBodyStart((Tree)node, this.cu, this.sp, this.doc);
                    int end = (int)this.sp.getEndPosition(this.cu, (Tree)node);
                    if (start != -1 && end != -1) {
                        this.addFold(FoldInfo.range((int)start, (int)end, (FoldType)JavaFoldManager.INNERCLASS_TYPE), (int)this.sp.getStartPosition(this.cu, (Tree)node));
                    }
                }
                this.handleJavadoc((Tree)node);
            }
            catch (BadLocationException e) {
                this.stopped = true;
            }
            catch (ConcurrentModificationException e) {
                this.stopped = true;
            }
            return null;
        }

        public Object visitVariable(VariableTree node, Object p) {
            CancellableTreePathScanner.super.visitVariable(node, p);
            if (TreeUtilities.CLASS_TREE_KINDS.contains((Object)this.getCurrentPath().getParentPath().getLeaf().getKind())) {
                this.handleTree((Tree)node, null, true);
            }
            return null;
        }

        public Object visitBlock(BlockTree node, Object p) {
            CancellableTreePathScanner.super.visitBlock(node, p);
            TreePath path = this.getCurrentPath();
            if (TreeUtilities.CLASS_TREE_KINDS.contains((Object)path.getParentPath().getLeaf().getKind())) {
                this.handleTree((Tree)node, null, false);
            }
            return null;
        }

        public Object visitCompilationUnit(CompilationUnitTree node, Object p) {
            int importsStart = Integer.MAX_VALUE;
            int importsEnd = -1;
            for (ImportTree imp : node.getImports()) {
                int start = (int)this.sp.getStartPosition(this.cu, (Tree)imp);
                int end = (int)this.sp.getEndPosition(this.cu, (Tree)imp);
                if (importsStart > start) {
                    importsStart = start;
                }
                if (end <= importsEnd) continue;
                importsEnd = end;
            }
            if (importsEnd != -1 && importsStart != -1) {
                if (importsStart < this.initialCommentStopPos) {
                    this.initialCommentStopPos = importsStart;
                }
                if ((importsStart += 7) < importsEnd) {
                    this.addFold(FoldInfo.range((int)importsStart, (int)importsEnd, (FoldType)JavaFoldManager.IMPORTS_FOLD_TYPE), importsStart);
                }
            }
            return CancellableTreePathScanner.super.visitCompilationUnit(node, p);
        }
    }

    private class CommitFolds
    implements Runnable {
        private boolean insideRender;
        private Document doc;
        private List<FoldInfo> infos;
        private List<Integer> anchors;
        private long startTime;
        private AtomicLong version;
        private long stamp;

        public CommitFolds(Document doc, List<FoldInfo> infos, List<Integer> anchors, AtomicLong version, long stamp) {
            this.doc = doc;
            this.infos = infos;
            this.version = version;
            this.stamp = stamp;
            this.anchors = anchors;
        }

        private FoldInfo expanded(FoldInfo info) {
            FoldInfo ex = FoldInfo.range((int)info.getStart(), (int)info.getEnd(), (FoldType)info.getType());
            if (info.getTemplate() != info.getType().getTemplate()) {
                ex = ex.withTemplate(info.getTemplate());
            }
            if (info.getDescriptionOverride() != null) {
                ex = ex.withDescription(info.getDescriptionOverride());
            }
            ex.attach(info.getExtraInfo());
            return ex.collapsed(false);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            int caretPos = -1;
            if (!this.insideRender) {
                this.startTime = System.currentTimeMillis();
                this.insideRender = true;
                JavaElementFoldManager.this.operation.getHierarchy().getComponent().getDocument().render(this);
                return;
            }
            if (JavaElementFoldManager.this.first) {
                JTextComponent c = JavaElementFoldManager.this.operation.getHierarchy().getComponent();
                Object od = this.doc.getProperty("stream");
                if (od instanceof DataObject) {
                    int idx;
                    DataObject d = (DataObject)od;
                    EditorCookie cake = (EditorCookie)d.getCookie(EditorCookie.class);
                    JEditorPane[] panes = cake.getOpenedPanes();
                    int n = idx = panes == null ? -1 : Arrays.asList(panes).indexOf(c);
                    if (idx != -1) {
                        caretPos = c.getCaret().getDot();
                    }
                }
            }
            JavaElementFoldManager.this.operation.getHierarchy().lock();
            try {
                Map folds;
                if (this.version.get() != this.stamp || JavaElementFoldManager.this.operation.getHierarchy().getComponent().getDocument() != this.doc) {
                    return;
                }
                int expandIndex = -1;
                if (caretPos >= 0) {
                    for (int i = 0; i < this.anchors.size(); ++i) {
                        FoldType ft;
                        int a = this.anchors.get(i);
                        if (a > caretPos) continue;
                        FoldInfo fi = this.infos.get(i);
                        if (a == caretPos && ((ft = fi.getType()).isKindOf(FoldType.INITIAL_COMMENT) || ft.isKindOf(FoldType.COMMENT) || ft.isKindOf(FoldType.DOCUMENTATION)) || fi.getEnd() <= caretPos) continue;
                        expandIndex = i;
                        break;
                    }
                }
                if (expandIndex != -1) {
                    this.infos = new ArrayList<FoldInfo>(this.infos);
                    this.infos.set(expandIndex, this.expanded(this.infos.get(expandIndex)));
                }
                if ((folds = JavaElementFoldManager.this.operation.update(this.infos, null, null)) == null) {
                    return;
                }
                JavaElementFoldManager.this.first = false;
            }
            catch (BadLocationException e) {
                Exceptions.printStackTrace((Throwable)e);
            }
            finally {
                JavaElementFoldManager.this.operation.getHierarchy().unlock();
            }
            long endTime = System.currentTimeMillis();
            Logger.getLogger("TIMER").log(Level.FINE, "Folds - 2", new Object[]{JavaElementFoldManager.this.file, endTime - this.startTime});
        }
    }

    static final class JavaElementFoldTask
    extends ScanningCancellableTask<CompilationInfo> {
        private static final Map<DataObject, JavaElementFoldTask> file2Task = new WeakHashMap<DataObject, JavaElementFoldTask>();
        private AtomicLong version = new AtomicLong(0);
        private Collection<Reference<JavaElementFoldManager>> managers = new ArrayList<Reference<JavaElementFoldManager>>(2);

        JavaElementFoldTask() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        static JavaElementFoldTask getTask(FileObject file) {
            try {
                DataObject od = DataObject.find((FileObject)file);
                Map<DataObject, JavaElementFoldTask> map = file2Task;
                synchronized (map) {
                    JavaElementFoldTask task = file2Task.get((Object)od);
                    if (task == null) {
                        task = new JavaElementFoldTask();
                        file2Task.put(od, task);
                    }
                    return task;
                }
            }
            catch (DataObjectNotFoundException ex) {
                Logger.getLogger(JavaElementFoldManager.class.getName()).log(Level.FINE, null, (Throwable)ex);
                return new JavaElementFoldTask();
            }
        }

        void invalidate() {
            this.version.incrementAndGet();
        }

        synchronized void setJavaElementFoldManager(JavaElementFoldManager manager, FileObject file) {
            if (file == null) {
                Iterator<Reference<JavaElementFoldManager>> it = this.managers.iterator();
                while (it.hasNext()) {
                    Reference<JavaElementFoldManager> ref = it.next();
                    JavaElementFoldManager fm = ref.get();
                    if (fm != null && fm != manager) continue;
                    it.remove();
                    break;
                }
            } else {
                this.managers.add(new WeakReference<JavaElementFoldManager>(manager));
                JavaElementFoldManagerTaskFactory.doRefresh(file);
            }
        }

        private synchronized Object findLiveManagers() {
            ArrayList<JavaElementFoldManager> oneMgr = null;
            ArrayList<JavaElementFoldManager> result = null;
            Iterator<Reference<JavaElementFoldManager>> it = this.managers.iterator();
            while (it.hasNext()) {
                Reference<JavaElementFoldManager> ref = it.next();
                JavaElementFoldManager fm = ref.get();
                if (fm == null) {
                    it.remove();
                    continue;
                }
                if (result != null) {
                    result.add(fm);
                    continue;
                }
                if (oneMgr != null) {
                    result = new ArrayList<JavaElementFoldManager>(2);
                    result.add((JavaElementFoldManager)((Object)oneMgr));
                    result.add(fm);
                    continue;
                }
                oneMgr = fm;
            }
            return result != null ? result : oneMgr;
        }

        @Override
        public void run(CompilationInfo info) {
            this.resume();
            final Object mgrs = this.findLiveManagers();
            if (mgrs == null) {
                return;
            }
            long startTime = System.currentTimeMillis();
            CompilationUnitTree cu = info.getCompilationUnit();
            final Document doc = info.getSnapshot().getSource().getDocument(false);
            if (doc == null) {
                return;
            }
            final JavaElementFoldVisitor v = new JavaElementFoldVisitor(info, cu, info.getTrees().getSourcePositions(), doc);
            this.scan(v, (Tree)cu, null);
            final long stamp = this.version.get();
            if (v.stopped || this.isCancelled()) {
                return;
            }
            v.checkInitialFold();
            if (v.stopped || this.isCancelled()) {
                return;
            }
            if (mgrs instanceof JavaElementFoldManager) {
                JavaElementFoldManager javaElementFoldManager = (JavaElementFoldManager)((Object)mgrs);
                javaElementFoldManager.getClass();
                SwingUtilities.invokeLater(javaElementFoldManager.new CommitFolds(doc, v.folds, v.anchors, this.version, stamp));
            } else {
                SwingUtilities.invokeLater(new Runnable(){
                    Collection<JavaElementFoldManager> jefms;

                    @Override
                    public void run() {
                        Iterator<JavaElementFoldManager> i$ = this.jefms.iterator();
                        while (i$.hasNext()) {
                            JavaElementFoldManager jefm;
                            JavaElementFoldManager javaElementFoldManager = jefm = i$.next();
                            javaElementFoldManager.getClass();
                            javaElementFoldManager.new CommitFolds(doc, v.folds, v.anchors, JavaElementFoldTask.this.version, stamp).run();
                        }
                    }
                });
            }
            long endTime = System.currentTimeMillis();
            Logger.getLogger("TIMER").log(Level.FINE, "Folds - 1", new Object[]{info.getFileObject(), endTime - startTime});
        }

    }

}

