/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.java.editor.options;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JComponent;
import org.netbeans.modules.java.editor.options.MarkOccurencesPanel;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public final class MarkOccurencesOptionsPanelController
extends OptionsPanelController {
    private MarkOccurencesPanel panel;
    private final PropertyChangeSupport pcs;

    public MarkOccurencesOptionsPanelController() {
        this.pcs = new PropertyChangeSupport((Object)this);
    }

    public void update() {
        this.panel.load(this);
    }

    public void applyChanges() {
        this.panel.store();
    }

    public void cancel() {
    }

    public boolean isValid() {
        return true;
    }

    public boolean isChanged() {
        return this.panel.changed();
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx("netbeans.optionsDialog.java.markoccurrences");
    }

    public synchronized JComponent getComponent(Lookup masterLookup) {
        if (this.panel == null) {
            this.panel = new MarkOccurencesPanel(this);
        }
        return this.panel;
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }
}

