/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.progress.ProgressUtils
 *  org.netbeans.editor.BaseAction
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.overridden;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.editor.BaseAction;
import org.netbeans.modules.editor.java.GoToSupport;
import org.netbeans.modules.java.editor.overridden.ComputeOverriders;
import org.netbeans.modules.java.editor.overridden.ElementDescription;
import org.netbeans.modules.java.editor.overridden.IsOverriddenAnnotationAction;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public final class GoToImplementation
extends BaseAction {
    private static Set<ElementKind> SUPPORTED_ELEMENTS = EnumSet.of(ElementKind.METHOD, ElementKind.ANNOTATION_TYPE, ElementKind.CLASS, ElementKind.ENUM, ElementKind.INTERFACE);

    public GoToImplementation() {
        super(132);
    }

    public void actionPerformed(ActionEvent e, JTextComponent c) {
        GoToImplementation.goToImplementation(c);
    }

    public static void goToImplementation(final JTextComponent c) {
        final Document doc = c.getDocument();
        final int caretPos = c.getCaretPosition();
        final AtomicBoolean cancel = new AtomicBoolean();
        ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

            @Override
            public void run() {
                GoToImplementation.goToImpl(c, doc, caretPos, cancel);
            }
        }, (String)NbBundle.getMessage(GoToImplementation.class, (String)"CTL_GoToImplementation"), (AtomicBoolean)cancel, (boolean)false);
    }

    public static void goToImpl(final JTextComponent c, final Document doc, final int caretPos, final AtomicBoolean cancel) {
        try {
            JavaSource js = JavaSource.forDocument((Document)doc);
            if (js != null) {
                js.runUserActionTask((Task)new Task<CompilationController>(){

                    public void run(CompilationController parameter) throws Exception {
                        List<ElementDescription> overridingMethods;
                        if (cancel != null && cancel.get()) {
                            return;
                        }
                        parameter.toPhase(JavaSource.Phase.RESOLVED);
                        AtomicBoolean onDeclaration = new AtomicBoolean();
                        Element el = GoToImplementation.resolveTarget((CompilationInfo)parameter, doc, caretPos, onDeclaration);
                        if (el == null) {
                            StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(GoToImplementation.class, (String)"LBL_NoMethod"));
                            return;
                        }
                        TypeElement type = el.getKind() == ElementKind.METHOD ? (TypeElement)el.getEnclosingElement() : (TypeElement)el;
                        ExecutableElement method = el.getKind() == ElementKind.METHOD ? (ExecutableElement)el : null;
                        Map<ElementHandle<? extends Element>, List<ElementDescription>> overriding = new ComputeOverriders(new AtomicBoolean()).process((CompilationInfo)parameter, type, method, true);
                        List<ElementDescription> list = overridingMethods = overriding != null ? overriding.get((Object)ElementHandle.create((Element)el)) : null;
                        if (!onDeclaration.get()) {
                            ElementDescription ed = new ElementDescription((CompilationInfo)parameter, el, true);
                            if (overridingMethods == null) {
                                overridingMethods = Collections.singletonList(ed);
                            } else {
                                overridingMethods.add(ed);
                            }
                        }
                        if (overridingMethods == null || overridingMethods.isEmpty()) {
                            String key = el.getKind() == ElementKind.METHOD ? "LBL_NoOverridingMethod" : "LBL_NoOverridingType";
                            StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(GoToImplementation.class, (String)key));
                            return;
                        }
                        final List<ElementDescription> finalOverridingMethods = overridingMethods;
                        final String caption = NbBundle.getMessage(GoToImplementation.class, (String)(method != null ? "LBL_ImplementorsOverridersMethod" : "LBL_ImplementorsOverridersClass"));
                        SwingUtilities.invokeLater(new Runnable(){

                            @Override
                            public void run() {
                                try {
                                    Point p = new Point(c.modelToView(caretPos).getLocation());
                                    IsOverriddenAnnotationAction.mouseClicked(Collections.singletonMap(caption, finalOverridingMethods), c, p);
                                }
                                catch (BadLocationException ex) {
                                    Exceptions.printStackTrace((Throwable)ex);
                                }
                            }
                        });
                    }

                }, true);
            }
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    static Element resolveTarget(CompilationInfo info, Document doc, int caretPos, AtomicBoolean onDeclaration) {
        GoToSupport.Context context = GoToSupport.resolveContext(info, doc, caretPos, false);
        onDeclaration.set(context == null);
        if (context == null) {
            TreePath tp = info.getTreeUtilities().pathFor(caretPos);
            if (tp.getLeaf().getKind() == Tree.Kind.MODIFIERS) {
                tp = tp.getParentPath();
            }
            int[] elementNameSpan = null;
            switch (tp.getLeaf().getKind()) {
                case ANNOTATION_TYPE: 
                case CLASS: 
                case ENUM: 
                case INTERFACE: {
                    elementNameSpan = info.getTreeUtilities().findNameSpan((ClassTree)tp.getLeaf());
                    break;
                }
                case METHOD: {
                    elementNameSpan = info.getTreeUtilities().findNameSpan((MethodTree)tp.getLeaf());
                }
            }
            if (elementNameSpan != null && caretPos <= elementNameSpan[1]) {
                return info.getTrees().getElement(tp);
            }
            return null;
        }
        if (!SUPPORTED_ELEMENTS.contains((Object)context.resolved.getKind())) {
            return null;
        }
        return context.resolved;
    }

}

