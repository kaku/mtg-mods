/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.FoldTemplate
 *  org.netbeans.api.editor.fold.FoldType
 *  org.netbeans.editor.ext.java.JavaFoldManager
 *  org.netbeans.spi.editor.fold.FoldTypeProvider
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.fold;

import java.util.ArrayList;
import java.util.Collection;
import org.netbeans.api.editor.fold.FoldTemplate;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.editor.ext.java.JavaFoldManager;
import org.netbeans.spi.editor.fold.FoldTypeProvider;
import org.openide.util.NbBundle;

public class JavaFoldTypeProvider
implements FoldTypeProvider {
    private Collection<FoldType> types = new ArrayList<FoldType>(5);
    public static final FoldType BUNDLE_STRING = FoldType.create((String)"bundle-string", (String)NbBundle.getMessage(JavaFoldTypeProvider.class, (String)"Fold_BundleString"), (FoldTemplate)FoldTemplate.DEFAULT_BLOCK);

    public JavaFoldTypeProvider() {
        this.types.add(JavaFoldManager.CODE_BLOCK_FOLD_TYPE);
        this.types.add(JavaFoldManager.INNERCLASS_TYPE);
        this.types.add(JavaFoldManager.IMPORTS_FOLD_TYPE);
        this.types.add(JavaFoldManager.JAVADOC_FOLD_TYPE);
        this.types.add(JavaFoldManager.INITIAL_COMMENT_FOLD_TYPE);
        this.types.add(BUNDLE_STRING);
    }

    public Collection getValues(Class type) {
        return this.types;
    }

    public boolean inheritable() {
        return false;
    }
}

