/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.doctree.DocCommentTree
 *  com.sun.source.doctree.DocTree
 *  com.sun.source.doctree.DocTree$Kind
 *  com.sun.source.doctree.ReferenceTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.Scope
 *  com.sun.source.tree.Tree
 *  com.sun.source.util.DocSourcePositions
 *  com.sun.source.util.DocTreePath
 *  com.sun.source.util.DocTreePathScanner
 *  com.sun.source.util.DocTrees
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.editor.completion.Completion
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.lexer.JavadocTokenId
 *  org.netbeans.api.java.source.ClassIndex
 *  org.netbeans.api.java.source.ClassIndex$NameKind
 *  org.netbeans.api.java.source.ClassIndex$SearchKind
 *  org.netbeans.api.java.source.ClassIndex$SearchScope
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.ClasspathInfo$PathKind
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.ElementUtilities
 *  org.netbeans.api.java.source.ElementUtilities$ElementAcceptor
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.support.ReferencesCount
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.spi.editor.completion.CompletionItem
 *  org.netbeans.spi.editor.completion.CompletionResultSet
 *  org.netbeans.spi.editor.completion.support.AsyncCompletionQuery
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.java.editor.javadoc;

import com.sun.source.doctree.DocCommentTree;
import com.sun.source.doctree.DocTree;
import com.sun.source.doctree.ReferenceTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.Scope;
import com.sun.source.tree.Tree;
import com.sun.source.util.DocSourcePositions;
import com.sun.source.util.DocTreePath;
import com.sun.source.util.DocTreePathScanner;
import com.sun.source.util.DocTrees;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ExecutableType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.completion.Completion;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.lexer.JavadocTokenId;
import org.netbeans.api.java.source.ClassIndex;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.support.ReferencesCount;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.editor.java.JavaCompletionItem;
import org.netbeans.modules.editor.java.LazyJavaCompletionItem;
import org.netbeans.modules.editor.java.Utilities;
import org.netbeans.modules.java.editor.javadoc.JavadocCompletionItem;
import org.netbeans.modules.java.editor.javadoc.JavadocCompletionUtils;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.spi.editor.completion.CompletionItem;
import org.netbeans.spi.editor.completion.CompletionResultSet;
import org.netbeans.spi.editor.completion.support.AsyncCompletionQuery;
import org.openide.util.Exceptions;

final class JavadocCompletionQuery
extends AsyncCompletionQuery {
    private static final String CLASS_KEYWORD = "class";
    private final int queryType;
    private int caretOffset;
    private List<CompletionItem> items;
    private boolean hasAdditionalItems;
    private JTextComponent component;
    private boolean ignoreCancel;
    private static final Set<ElementKind> EXECUTABLE = EnumSet.of(ElementKind.METHOD, ElementKind.CONSTRUCTOR);

    public JavadocCompletionQuery(int queryType) {
        this.queryType = queryType;
    }

    protected void prepareQuery(JTextComponent component) {
        this.component = component;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void query(CompletionResultSet resultSet, Document doc, int caretOffset) {
        try {
            this.queryImpl(resultSet, doc, caretOffset);
        }
        catch (InterruptedException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (ExecutionException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        finally {
            resultSet.finish();
        }
    }

    protected boolean canFilter(JTextComponent component) {
        int newOffset = component.getSelectionStart();
        Document doc = component.getDocument();
        if (newOffset > this.caretOffset && this.items != null && !this.items.isEmpty()) {
            try {
                String prefix = doc.getText(this.caretOffset, newOffset - this.caretOffset);
                if (!this.isJavaIdentifierPart(prefix)) {
                    Completion.get().hideDocumentation();
                    Completion.get().hideCompletion();
                }
            }
            catch (BadLocationException ble) {
                // empty catch block
            }
        }
        return false;
    }

    private boolean isJavaIdentifierPart(String text) {
        for (int i = 0; i < text.length(); ++i) {
            if (Character.isJavaIdentifierPart(text.charAt(i))) continue;
            return false;
        }
        return true;
    }

    static List<CompletionItem> runCompletionQuery(int queryType, Document doc, int caret) {
        JavadocCompletionQuery q = new JavadocCompletionQuery(queryType);
        JavadocContext jdctx = new JavadocContext();
        q.items = new ArrayList<CompletionItem>();
        q.caretOffset = caret;
        q.ignoreCancel = true;
        q.runInJavac(JavaSource.forDocument((Document)doc), jdctx);
        return q.items;
    }

    private void queryImpl(CompletionResultSet resultSet, Document doc, int caretOffset) throws InterruptedException, ExecutionException {
        JavadocContext jdctx = new JavadocContext();
        this.items = new ArrayList<CompletionItem>();
        this.caretOffset = caretOffset;
        this.runInJavac(JavaSource.forDocument((Document)doc), jdctx);
        if (!this.ignoreCancel && this.isTaskCancelled()) {
            return;
        }
        if ((this.queryType & 1) != 0) {
            if (!this.items.isEmpty()) {
                resultSet.addAllItems(this.items);
            }
            resultSet.setHasAdditionalItems(this.hasAdditionalItems);
        }
        if (jdctx.anchorOffset >= 0) {
            resultSet.setAnchorOffset(jdctx.anchorOffset);
        }
    }

    private void setCompletionHack(boolean flag) {
        if (this.component != null) {
            this.component.putClientProperty("completion-active", flag);
        }
    }

    private void runInJavac(JavaSource js, final JavadocContext jdctx) {
        try {
            if (js == null) {
                return;
            }
            js.runUserActionTask((Task)new Task<CompilationController>(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                public void run(CompilationController javac) throws Exception {
                    if (!JavadocCompletionQuery.this.ignoreCancel && JavadocCompletionQuery.this.isTaskCancelled()) {
                        return;
                    }
                    if (!JavadocCompletionUtils.isJavadocContext(javac.getTokenHierarchy(), JavadocCompletionQuery.this.caretOffset)) {
                        return;
                    }
                    JavadocCompletionQuery.this.setCompletionHack(true);
                    JavaSource.Phase toPhase = javac.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                    if (toPhase != JavaSource.Phase.ELEMENTS_RESOLVED) {
                        return;
                    }
                    try {
                        jdctx.javac = javac;
                        if (JavadocCompletionQuery.this.resolveContext((CompilationInfo)javac, jdctx)) {
                            JavadocCompletionQuery.this.analyzeContext(jdctx);
                        }
                    }
                    finally {
                        jdctx.javac = null;
                        if (JavadocCompletionQuery.this.component != null) {
                            JavadocCompletionQuery.this.setCompletionHack(false);
                        }
                    }
                }
            }, true);
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    private boolean resolveContext(CompilationInfo javac, JavadocContext jdctx) throws IOException {
        jdctx.doc = javac.getDocument();
        DocTrees trees = javac.getDocTrees();
        TreePath javadocFor = JavadocCompletionUtils.findJavadoc(javac, this.caretOffset);
        if (javadocFor == null) {
            return false;
        }
        jdctx.javadocFor = javadocFor;
        DocCommentTree docCommentTree = trees.getDocCommentTree(javadocFor);
        if (docCommentTree == null) {
            return false;
        }
        jdctx.comment = docCommentTree;
        Element elm = trees.getElement(javadocFor);
        if (elm == null) {
            return false;
        }
        jdctx.handle = ElementHandle.create((Element)elm);
        jdctx.commentFor = elm;
        jdctx.jdts = JavadocCompletionUtils.findJavadocTokenSequence(javac, this.caretOffset);
        if (jdctx.jdts == null) {
            return false;
        }
        jdctx.positions = trees.getSourcePositions();
        return jdctx.positions != null;
    }

    private void analyzeContext(JavadocContext jdctx) {
        TokenSequence<JavadocTokenId> jdts = jdctx.jdts;
        if (jdts == null) {
            return;
        }
        jdts.move(this.caretOffset);
        if (!jdts.moveNext() && !jdts.movePrevious()) {
            return;
        }
        if (this.caretOffset - jdts.offset() == 0) {
            jdts.movePrevious();
        }
        switch ((JavadocTokenId)jdts.token().id()) {
            case TAG: {
                this.resolveTagToken(jdctx);
                break;
            }
            case IDENT: {
                this.resolveIdent(jdctx);
                break;
            }
            case DOT: {
                this.resolveDotToken(jdctx);
                break;
            }
            case HASH: {
                this.resolveHashToken(jdctx);
                break;
            }
            case OTHER_TEXT: {
                this.resolveOtherText(jdctx, jdts);
                break;
            }
            case HTML_TAG: {
                this.resolveHTMLToken(jdctx);
            }
        }
    }

    void resolveTagToken(JavadocContext jdctx) {
        assert (jdctx.jdts.token() != null);
        assert (jdctx.jdts.token().id() == JavadocTokenId.TAG);
        DocTreePath tag = this.getTag(jdctx, this.caretOffset);
        if (tag == null) {
            return;
        }
        if (JavadocCompletionUtils.isBlockTag(tag)) {
            this.resolveBlockTag(tag, jdctx);
        } else {
            this.resolveInlineTag(tag, jdctx);
        }
    }

    void resolveBlockTag(DocTreePath tag, JavadocContext jdctx) {
        String prefix;
        int pos;
        if (tag != null) {
            pos = (int)jdctx.positions.getStartPosition(jdctx.javac.getCompilationUnit(), jdctx.comment, tag.getLeaf());
            prefix = JavadocCompletionUtils.getCharSequence(jdctx.doc, pos, this.caretOffset).toString();
        } else {
            prefix = "";
            pos = this.caretOffset;
        }
        this.items.addAll(JavadocCompletionItem.addBlockTagItems(jdctx.handle.getKind(), prefix, pos));
        jdctx.anchorOffset = pos;
    }

    void resolveInlineTag(DocTreePath tag, JavadocContext jdctx) {
        String prefix;
        int pos;
        if (tag != null) {
            pos = (int)jdctx.positions.getStartPosition(jdctx.javac.getCompilationUnit(), jdctx.comment, tag.getLeaf()) + 1;
            prefix = JavadocCompletionUtils.getCharSequence(jdctx.doc, pos, this.caretOffset).toString();
            jdctx.anchorOffset = pos;
        } else {
            pos = this.caretOffset;
            prefix = "";
            jdctx.anchorOffset = pos;
        }
        this.items.addAll(JavadocCompletionItem.addInlineTagItems(jdctx.handle.getKind(), prefix, pos));
    }

    private int skipWhitespacesBackwards(JavadocContext jdctx, int offset) {
        jdctx.jdts.move(offset);
        while (jdctx.jdts.movePrevious()) {
            Token t = jdctx.jdts.token();
            if (t.id() != JavadocTokenId.OTHER_TEXT) {
                return jdctx.jdts.offset();
            }
            CharSequence text = t.text();
            for (int i = 0; i < text.length(); ++i) {
                if (Character.isWhitespace(text.charAt(i))) continue;
                return jdctx.jdts.offset();
            }
        }
        return jdctx.jdts.moveNext() ? jdctx.jdts.offset() : offset;
    }

    private DocTreePath getTag(final JavadocContext jdctx, int offset) {
        final DocTreePath[] result = new DocTreePath[1];
        final int normalizedOffset = this.skipWhitespacesBackwards(jdctx, offset);
        new DocTreePathScanner<Void, Void>(){

            public Void scan(DocTree node, Void p) {
                if (node != null && jdctx.positions.getStartPosition(jdctx.javac.getCompilationUnit(), jdctx.comment, node) <= (long)normalizedOffset && jdctx.positions.getEndPosition(jdctx.javac.getCompilationUnit(), jdctx.comment, node) >= (long)normalizedOffset) {
                    result[0] = new DocTreePath(this.getCurrentPath(), node);
                    return (Void)DocTreePathScanner.super.scan(node, (Object)p);
                }
                return null;
            }
        }.scan(new DocTreePath(jdctx.javadocFor, jdctx.comment), (Object)null);
        return result[0];
    }

    void resolveIdent(JavadocContext jdctx) {
        TokenSequence<JavadocTokenId> jdts = jdctx.jdts;
        assert (jdts.token() != null);
        assert (jdts.token().id() == JavadocTokenId.IDENT);
        DocTreePath tag = this.getTag(jdctx, this.caretOffset);
        if (tag != null) {
            this.insideTag(tag, jdctx);
        }
    }

    void resolveDotToken(JavadocContext jdctx) {
        assert (jdctx.jdts.token() != null);
        assert (jdctx.jdts.token().id() == JavadocTokenId.DOT);
        DocTreePath tag = this.getTag(jdctx, this.caretOffset);
        if (tag != null) {
            this.insideTag(tag, jdctx);
        }
    }

    void resolveHashToken(JavadocContext jdctx) {
        assert (jdctx.jdts.token() != null);
        assert (jdctx.jdts.token().id() == JavadocTokenId.HASH);
        DocTreePath tag = this.getTag(jdctx, this.caretOffset);
        if (tag != null) {
            this.insideTag(tag, jdctx);
        }
    }

    void resolveHTMLToken(JavadocContext jdctx) {
        assert (jdctx.jdts.token() != null);
        assert (jdctx.jdts.token().id() == JavadocTokenId.HTML_TAG);
        DocTreePath tag = this.getTag(jdctx, this.caretOffset);
        if (tag != null && tag.getLeaf().getKind() == DocTree.Kind.PARAM) {
            this.insideParamTag(tag, jdctx);
        }
    }

    private void insideTag(DocTreePath tag, JavadocContext jdctx) {
        switch (tag.getLeaf().getKind()) {
            case IDENTIFIER: {
                if (tag.getParentPath() == null || tag.getParentPath().getLeaf().getKind() != DocTree.Kind.PARAM) break;
                tag = tag.getParentPath();
            }
            case PARAM: {
                this.insideParamTag(tag, jdctx);
                break;
            }
            case SEE: 
            case THROWS: 
            case VALUE: 
            case LINK: 
            case LINK_PLAIN: {
                this.insideSeeTag(tag, jdctx);
                break;
            }
            case REFERENCE: {
                this.insideReference(tag, jdctx);
            }
        }
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private void insideSeeTag(DocTreePath tag, JavadocContext jdctx) {
        boolean isThrowsKind;
        TokenSequence<JavadocTokenId> jdts = jdctx.jdts;
        assert (jdts.token() != null);
        int start = (int)jdctx.positions.getStartPosition(jdctx.javac.getCompilationUnit(), jdctx.comment, tag.getLeaf());
        boolean bl = isThrowsKind = tag.getLeaf().getKind() == DocTree.Kind.THROWS;
        if (isThrowsKind && !EXECUTABLE.contains((Object)jdctx.commentFor.getKind())) {
            return;
        }
        jdts.move(start + (JavadocCompletionUtils.isBlockTag(tag) ? 0 : 1));
        if (!jdts.moveNext() || this.caretOffset <= jdts.offset() + jdts.token().length()) {
            return;
        }
        if (!jdts.moveNext() || this.caretOffset <= jdts.offset()) {
            return;
        }
        boolean noPrefix = false;
        if (this.caretOffset <= jdts.offset() + jdts.token().length()) {
            CharSequence cs;
            int pos = this.caretOffset - jdts.offset();
            CharSequence charSequence = cs = pos < (cs = jdts.token().text()).length() ? cs.subSequence(0, pos) : cs;
            if (!JavadocCompletionUtils.isWhiteSpace(cs) && !JavadocCompletionUtils.isLineBreak(jdts.token(), pos)) return;
            noPrefix = true;
        } else if (!JavadocCompletionUtils.isWhiteSpace(jdts.token()) && !JavadocCompletionUtils.isLineBreak(jdts.token())) {
            return;
        }
        if (!noPrefix) return;
        if (isThrowsKind) {
            this.completeThrowsOrPkg(null, "", this.caretOffset, jdctx);
        } else {
            this.completeClassOrPkg(null, "", this.caretOffset, jdctx);
        }
        jdctx.anchorOffset = this.caretOffset;
    }

    private void insideReference(DocTreePath tag, JavadocContext jdctx) {
        ReferenceTree ref = (ReferenceTree)tag.getLeaf();
        DocTree.Kind kind = tag.getParentPath().getLeaf().getKind();
        int start = (int)jdctx.positions.getStartPosition(jdctx.javac.getCompilationUnit(), jdctx.comment, (DocTree)ref);
        int end = (int)jdctx.positions.getEndPosition(jdctx.javac.getCompilationUnit(), jdctx.comment, (DocTree)ref);
        CharSequence cs = JavadocCompletionUtils.getCharSequence(jdctx.doc, start, end);
        StringBuilder sb = new StringBuilder();
        jdctx.anchorOffset = start;
        for (int i = this.caretOffset - start - 1; i >= 0; --i) {
            char c = cs.charAt(i);
            if (c == '#') {
                String prefix = sb.toString();
                int substitutionOffset = this.caretOffset - sb.length();
                ExpressionTree classReference = jdctx.javac.getTreeUtilities().getReferenceClass(tag);
                if (classReference == null) {
                    if (kind == DocTree.Kind.VALUE) {
                        this.addLocalConstants(jdctx, prefix, substitutionOffset);
                    } else {
                        this.addLocalMembersAndVars(jdctx, prefix, substitutionOffset);
                    }
                } else {
                    Element elm = jdctx.javac.getDocTrees().getElement(new TreePath(jdctx.javadocFor, (Tree)classReference));
                    if (elm != null) {
                        if (kind == DocTree.Kind.VALUE) {
                            this.addMemberConstants(jdctx, prefix, substitutionOffset, elm.asType());
                        } else {
                            this.addMembers(jdctx, prefix, substitutionOffset, elm.asType(), elm, EnumSet.of(ElementKind.ENUM_CONSTANT, ElementKind.FIELD, ElementKind.METHOD, ElementKind.CONSTRUCTOR), null);
                        }
                    }
                }
                return;
            }
            if (c == '.') {
                String prefix = sb.toString();
                String fqn = cs.subSequence(0, i).toString();
                int substitutionOffset = this.caretOffset - sb.length();
                if (kind == DocTree.Kind.THROWS) {
                    this.completeThrowsOrPkg(fqn, prefix, substitutionOffset, jdctx);
                } else {
                    this.completeClassOrPkg(fqn, prefix, substitutionOffset, jdctx);
                }
                return;
            }
            sb.insert(0, c);
        }
        String prefix = sb.toString();
        String fqn = null;
        int substitutionOffset = this.caretOffset - sb.length();
        if (kind == DocTree.Kind.THROWS) {
            this.completeThrowsOrPkg(fqn, prefix, substitutionOffset, jdctx);
        } else {
            this.completeClassOrPkg(fqn, prefix, substitutionOffset, jdctx);
        }
    }

    private void insideParamTag(DocTreePath tag, JavadocContext jdctx) {
        TokenSequence<JavadocTokenId> jdts = jdctx.jdts;
        assert (jdts.token() != null);
        int start = (int)jdctx.positions.getStartPosition(jdctx.javac.getCompilationUnit(), jdctx.comment, tag.getLeaf());
        jdts.move(start);
        if (!jdts.moveNext() || this.caretOffset <= jdts.offset() + jdts.token().length()) {
            return;
        }
        if (!jdts.moveNext() || this.caretOffset <= jdts.offset()) {
            return;
        }
        if (this.caretOffset <= jdts.offset() + jdts.token().length()) {
            CharSequence cs;
            int pos = this.caretOffset - jdts.offset();
            CharSequence charSequence = cs = pos < (cs = jdts.token().text()).length() ? cs.subSequence(0, pos) : cs;
            if (JavadocCompletionUtils.isWhiteSpace(cs) || JavadocCompletionUtils.isLineBreak(jdts.token(), pos)) {
                jdctx.anchorOffset = this.caretOffset;
                this.completeParamName(tag, "", this.caretOffset, jdctx);
                return;
            }
            return;
        }
        jdts.moveNext();
        if (jdts.token().id() != JavadocTokenId.IDENT && jdts.token().id() != JavadocTokenId.HTML_TAG) {
            return;
        }
        if (this.caretOffset <= jdts.offset() + jdts.token().length()) {
            CharSequence prefix = jdts.token().text().subSequence(0, this.caretOffset - jdts.offset());
            jdctx.anchorOffset = jdts.offset();
            this.completeParamName(tag, prefix.toString(), jdts.offset(), jdctx);
            return;
        }
    }

    private void completeParamName(DocTreePath tag, String prefix, int substitutionOffset, JavadocContext jdctx) {
        if (EXECUTABLE.contains((Object)jdctx.commentFor.getKind())) {
            ExecutableElement method = (ExecutableElement)jdctx.commentFor;
            for (VariableElement param : method.getParameters()) {
                String name = param.getSimpleName().toString();
                if (!name.startsWith(prefix)) continue;
                this.items.add(JavadocCompletionItem.createNameItem(name, substitutionOffset));
            }
            this.completeTypeVarName(jdctx.commentFor, prefix, substitutionOffset);
        } else if (jdctx.commentFor.getKind().isClass() || jdctx.commentFor.getKind().isInterface()) {
            this.completeTypeVarName(jdctx.commentFor, prefix, substitutionOffset);
        }
    }

    private void completeTypeVarName(Element forElement, String prefix, int substitutionOffset) {
        if (prefix.length() > 0) {
            if (prefix.charAt(0) == '<') {
                prefix = prefix.substring(1, prefix.length());
            } else {
                return;
            }
        }
        List<? extends TypeParameterElement> tparams = forElement.getKind().isClass() || forElement.getKind().isInterface() ? ((TypeElement)forElement).getTypeParameters() : ((ExecutableElement)forElement).getTypeParameters();
        for (TypeParameterElement typeVariable : tparams) {
            String name = typeVariable.getSimpleName().toString();
            if (!name.startsWith(prefix)) continue;
            this.items.add(JavadocCompletionItem.createNameItem("" + '<' + name + '>', substitutionOffset));
        }
    }

    private void completeClassOrPkg(String fqn, String prefix, int substitutionOffset, JavadocContext jdctx) {
        String pkgPrefix;
        if (fqn == null) {
            pkgPrefix = prefix;
            this.addTypes(EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE), null, null, prefix, substitutionOffset, jdctx);
        } else {
            TypeElement typeElm;
            pkgPrefix = fqn + '.' + prefix;
            PackageElement pkgElm = jdctx.javac.getElements().getPackageElement(fqn);
            if (pkgElm != null) {
                this.addPackageContent(pkgElm, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE), null, null, prefix, substitutionOffset, jdctx);
            }
            if ((typeElm = jdctx.javac.getElements().getTypeElement(fqn)) != null) {
                this.addInnerClasses(typeElm, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE), null, null, prefix, substitutionOffset, jdctx);
            }
        }
        for (String pkgName : jdctx.javac.getClasspathInfo().getClassIndex().getPackageNames(pkgPrefix, true, EnumSet.allOf(ClassIndex.SearchScope.class))) {
            if (pkgName.length() <= 0 || Utilities.isExcluded(pkgName + ".")) continue;
            this.items.add(JavaCompletionItem.createPackageItem(pkgName, substitutionOffset, false));
        }
    }

    private void completeThrowsOrPkg(String fqn, String prefix, int substitutionOffset, JavadocContext jdctx) {
        String pkgPrefix;
        Elements elements = jdctx.javac.getElements();
        HashSet<TypeElement> excludes = new HashSet<TypeElement>();
        ExecutableElement method = (ExecutableElement)jdctx.commentFor;
        for (TypeMirror type : method.getThrownTypes()) {
            TypeElement typeElement;
            String qualTypeName;
            TypeElement clazz;
            String typeName;
            if (type.getKind() != TypeKind.DECLARED || !this.startsWith(typeName = (clazz = (TypeElement)((DeclaredType)type).asElement()).getSimpleName().toString(), prefix) || (typeElement = elements.getTypeElement(qualTypeName = clazz.getQualifiedName().toString())) == null) continue;
            this.items.add(JavaCompletionItem.createTypeItem(jdctx.javac, typeElement, (DeclaredType)typeElement.asType(), substitutionOffset, typeName != qualTypeName ? jdctx.getReferencesCount() : null, elements.isDeprecated(typeElement), false, false, false, true, false, null));
            excludes.add(typeElement);
        }
        if (fqn == null) {
            pkgPrefix = prefix;
            this.addTypes(EnumSet.of(ElementKind.CLASS), JavadocCompletionQuery.findDeclaredType("java.lang.Throwable", elements), excludes, prefix, substitutionOffset, jdctx);
        } else {
            TypeElement typeElm;
            pkgPrefix = fqn + '.' + prefix;
            PackageElement pkgElm = elements.getPackageElement(fqn);
            if (pkgElm != null) {
                this.addPackageContent(pkgElm, EnumSet.of(ElementKind.CLASS), JavadocCompletionQuery.findDeclaredType("java.lang.Throwable", elements), excludes, prefix, substitutionOffset, jdctx);
            }
            if ((typeElm = elements.getTypeElement(fqn)) != null) {
                this.addInnerClasses(typeElm, EnumSet.of(ElementKind.CLASS), JavadocCompletionQuery.findDeclaredType("java.lang.Throwable", elements), excludes, prefix, substitutionOffset, jdctx);
            }
        }
        for (String pkgName : jdctx.javac.getClasspathInfo().getClassIndex().getPackageNames(pkgPrefix, true, EnumSet.allOf(ClassIndex.SearchScope.class))) {
            if (pkgName.length() <= 0) continue;
            this.items.add(JavaCompletionItem.createPackageItem(pkgName, substitutionOffset, false));
        }
    }

    private static DeclaredType findDeclaredType(CharSequence fqn, Elements elements) {
        TypeMirror asType;
        TypeElement re = elements.getTypeElement(fqn);
        if (re != null && (asType = re.asType()).getKind() == TypeKind.DECLARED) {
            return (DeclaredType)asType;
        }
        return null;
    }

    private void addMembers(JavadocContext env, final String prefix, int substitutionOffset, TypeMirror type, final Element elem, EnumSet<ElementKind> kinds, DeclaredType baseType) {
        Object smartTypes = null;
        CompilationInfo controller = env.javac;
        final Trees trees = controller.getTrees();
        final Elements elements = controller.getElements();
        Types types = controller.getTypes();
        TreeUtilities tu = controller.getTreeUtilities();
        TypeElement typeElem = type.getKind() == TypeKind.DECLARED ? (TypeElement)((DeclaredType)type).asElement() : null;
        Element docelm = env.handle.resolve(controller);
        TreePath docpath = docelm != null ? trees.getPath(docelm) : null;
        final Scope scope = docpath != null ? trees.getScope(docpath) : tu.scopeFor(this.caretOffset);
        TypeElement enclClass = scope.getEnclosingClass();
        TypeMirror enclType = enclClass != null ? enclClass.asType() : null;
        ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

            public boolean accept(Element e, TypeMirror t) {
                switch (e.getKind()) {
                    case FIELD: {
                        String name = e.getSimpleName().toString();
                        return Utilities.startsWith(name, prefix) && !"class".equals(name) && (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(e));
                    }
                    case ENUM_CONSTANT: {
                        return Utilities.startsWith(e.getSimpleName().toString(), prefix) && (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(e)) && trees.isAccessible(scope, e, (DeclaredType)t);
                    }
                    case METHOD: {
                        String sn = e.getSimpleName().toString();
                        return !(!Utilities.startsWith(sn, prefix) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(e) || Utilities.isExcludeMethods() && Utilities.isExcluded(Utilities.getElementName(e.getEnclosingElement(), true) + "." + sn));
                    }
                    case CONSTRUCTOR: {
                        return (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(e)) && (trees.isAccessible(scope, e, (DeclaredType)t) || elem.getModifiers().contains((Object)Modifier.ABSTRACT) && !e.getModifiers().contains((Object)Modifier.PRIVATE));
                    }
                }
                return false;
            }
        };
        for (Element e : controller.getElementUtilities().getMembers(type, acceptor)) {
            switch (e.getKind()) {
                case FIELD: 
                case ENUM_CONSTANT: {
                    TypeMirror tm = type.getKind() == TypeKind.DECLARED ? types.asMemberOf((DeclaredType)type, e) : e.asType();
                    this.items.add(JavaCompletionItem.createVariableItem(controller, (VariableElement)e, tm, substitutionOffset, null, typeElem != e.getEnclosingElement(), elements.isDeprecated(e), false, -1, null));
                    break;
                }
                case METHOD: 
                case CONSTRUCTOR: {
                    ExecutableType et = (ExecutableType)(type.getKind() == TypeKind.DECLARED ? types.asMemberOf((DeclaredType)type, e) : e.asType());
                    this.items.add(JavadocCompletionItem.createExecutableItem(controller, (ExecutableElement)e, et, substitutionOffset, typeElem != e.getEnclosingElement(), elements.isDeprecated(e)));
                }
            }
        }
    }

    private void addMemberConstants(JavadocContext env, final String prefix, int substitutionOffset, TypeMirror type) {
        CompilationInfo controller = env.javac;
        final Trees trees = controller.getTrees();
        final Elements elements = controller.getElements();
        Types types = controller.getTypes();
        TreeUtilities tu = controller.getTreeUtilities();
        TypeElement typeElem = type.getKind() == TypeKind.DECLARED ? (TypeElement)((DeclaredType)type).asElement() : null;
        Element docelm = env.handle.resolve(controller);
        TreePath docpath = docelm != null ? trees.getPath(docelm) : null;
        final Scope scope = docpath != null ? trees.getScope(docpath) : tu.scopeFor(this.caretOffset);
        ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

            public boolean accept(Element e, TypeMirror t) {
                switch (e.getKind()) {
                    case FIELD: {
                        String name = e.getSimpleName().toString();
                        return ((VariableElement)e).getConstantValue() != null && Utilities.startsWith(name, prefix) && !"class".equals(name) && (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(e));
                    }
                    case ENUM_CONSTANT: {
                        return ((VariableElement)e).getConstantValue() != null && Utilities.startsWith(e.getSimpleName().toString(), prefix) && (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(e)) && trees.isAccessible(scope, e, (DeclaredType)t);
                    }
                }
                return false;
            }
        };
        for (Element e : controller.getElementUtilities().getMembers(type, acceptor)) {
            switch (e.getKind()) {
                case FIELD: 
                case ENUM_CONSTANT: {
                    TypeMirror tm = type.getKind() == TypeKind.DECLARED ? types.asMemberOf((DeclaredType)type, e) : e.asType();
                    this.items.add(JavaCompletionItem.createVariableItem(controller, (VariableElement)e, tm, substitutionOffset, null, typeElem != e.getEnclosingElement(), elements.isDeprecated(e), false, -1, null));
                }
            }
        }
    }

    private void addLocalConstants(JavadocContext env, final String prefix, int substitutionOffset) {
        CompilationInfo controller = env.javac;
        final Elements elements = controller.getElements();
        Types types = controller.getTypes();
        TreeUtilities tu = controller.getTreeUtilities();
        final Trees trees = controller.getTrees();
        Element docelm = env.handle.resolve(controller);
        TreePath docpath = docelm != null ? trees.getPath(docelm) : null;
        final Scope scope = docpath != null ? trees.getScope(docpath) : tu.scopeFor(this.caretOffset);
        TypeElement enclClass = scope.getEnclosingClass();
        ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

            public boolean accept(Element e, TypeMirror t) {
                switch (e.getKind()) {
                    case FIELD: {
                        String name = e.getSimpleName().toString();
                        return ((VariableElement)e).getConstantValue() != null && Utilities.startsWith(name, prefix) && !"class".equals(name) && (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(e));
                    }
                    case ENUM_CONSTANT: {
                        return ((VariableElement)e).getConstantValue() != null && Utilities.startsWith(e.getSimpleName().toString(), prefix) && (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(e)) && trees.isAccessible(scope, e, (DeclaredType)t);
                    }
                }
                return false;
            }
        };
        for (Element e : controller.getElementUtilities().getLocalMembersAndVars(scope, acceptor)) {
            switch (e.getKind()) {
                case ENUM_CONSTANT: {
                    this.items.add(JavaCompletionItem.createVariableItem(controller, (VariableElement)e, e.asType(), substitutionOffset, null, scope.getEnclosingClass() != e.getEnclosingElement(), elements.isDeprecated(e), false, -1, null));
                    break;
                }
                case FIELD: {
                    TypeMirror tm = JavadocCompletionQuery.asMemberOf(e, enclClass != null ? enclClass.asType() : null, types);
                    this.items.add(JavaCompletionItem.createVariableItem(controller, (VariableElement)e, tm, substitutionOffset, null, scope.getEnclosingClass() != e.getEnclosingElement(), elements.isDeprecated(e), false, -1, null));
                }
            }
        }
    }

    private void addLocalMembersAndVars(JavadocContext env, final String prefix, int substitutionOffset) {
        CompilationInfo controller = env.javac;
        final Elements elements = controller.getElements();
        Types types = controller.getTypes();
        TreeUtilities tu = controller.getTreeUtilities();
        final Trees trees = controller.getTrees();
        Element docelm = env.handle.resolve(controller);
        TreePath docpath = docelm != null ? trees.getPath(docelm) : null;
        final Scope scope = docpath != null ? trees.getScope(docpath) : tu.scopeFor(this.caretOffset);
        Object smartTypes = null;
        TypeElement enclClass = scope.getEnclosingClass();
        ExecutableElement method = scope.getEnclosingMethod();
        ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

            public boolean accept(Element e, TypeMirror t) {
                switch (e.getKind()) {
                    case CONSTRUCTOR: {
                        return Utilities.startsWith(e.getEnclosingElement().getSimpleName().toString(), prefix) && (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(e)) && trees.isAccessible(scope, e, (DeclaredType)t);
                    }
                    case FIELD: {
                        String name = e.getSimpleName().toString();
                        return Utilities.startsWith(name, prefix) && !"class".equals(name) && (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(e));
                    }
                    case ENUM_CONSTANT: {
                        return Utilities.startsWith(e.getSimpleName().toString(), prefix) && (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(e)) && trees.isAccessible(scope, e, (DeclaredType)t);
                    }
                    case METHOD: {
                        return Utilities.startsWith(e.getSimpleName().toString(), prefix) && (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(e)) && trees.isAccessible(scope, e, (DeclaredType)t);
                    }
                }
                return false;
            }
        };
        for (Element e : controller.getElementUtilities().getLocalMembersAndVars(scope, acceptor)) {
            switch (e.getKind()) {
                case ENUM_CONSTANT: {
                    this.items.add(JavaCompletionItem.createVariableItem(controller, (VariableElement)e, e.asType(), substitutionOffset, null, scope.getEnclosingClass() != e.getEnclosingElement(), elements.isDeprecated(e), false, -1, null));
                    break;
                }
                case FIELD: {
                    String name = e.getSimpleName().toString();
                    TypeMirror tm = JavadocCompletionQuery.asMemberOf(e, enclClass != null ? enclClass.asType() : null, types);
                    this.items.add(JavaCompletionItem.createVariableItem(controller, (VariableElement)e, tm, substitutionOffset, null, scope.getEnclosingClass() != e.getEnclosingElement(), elements.isDeprecated(e), false, -1, null));
                    break;
                }
                case METHOD: 
                case CONSTRUCTOR: {
                    ExecutableType et = (ExecutableType)JavadocCompletionQuery.asMemberOf(e, enclClass != null ? enclClass.asType() : null, types);
                    this.items.add(JavadocCompletionItem.createExecutableItem(controller, (ExecutableElement)e, et, substitutionOffset, scope.getEnclosingClass() != e.getEnclosingElement(), elements.isDeprecated(e)));
                }
            }
        }
    }

    private static TypeMirror asMemberOf(Element element, TypeMirror type, Types types) {
        TypeMirror ret = element.asType();
        TypeMirror enclType = element.getEnclosingElement().asType();
        if (enclType.getKind() == TypeKind.DECLARED) {
            enclType = types.erasure(enclType);
        }
        while (type != null && type.getKind() == TypeKind.DECLARED) {
            if (types.isSubtype(type, enclType)) {
                ret = types.asMemberOf((DeclaredType)type, element);
                break;
            }
            type = ((DeclaredType)type).getEnclosingType();
        }
        return ret;
    }

    private void addTypes(EnumSet<ElementKind> kinds, DeclaredType baseType, Set<? extends Element> toExclude, String prefix, int substitutionOffset, JavadocContext jdctx) {
        if (this.queryType == 9) {
            if (baseType == null) {
                this.addAllTypes(jdctx, kinds, toExclude, prefix, substitutionOffset);
            } else {
                Elements elements = jdctx.javac.getElements();
                for (DeclaredType subtype : this.getSubtypesOf(baseType, prefix, jdctx)) {
                    TypeElement elem = (TypeElement)subtype.asElement();
                    if (toExclude != null && toExclude.contains(elem) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(elem)) continue;
                    this.items.add(JavaCompletionItem.createTypeItem(jdctx.javac, elem, subtype, substitutionOffset, jdctx.getReferencesCount(), elements.isDeprecated(elem), false, false, false, false, false, null));
                }
            }
        } else {
            this.addLocalAndImportedTypes(jdctx, kinds, baseType, toExclude, prefix, substitutionOffset);
            this.hasAdditionalItems = true;
        }
    }

    private void addLocalAndImportedTypes(JavadocContext env, final EnumSet<ElementKind> kinds, final DeclaredType baseType, final Set<? extends Element> toExclude, final String prefix, int substitutionOffset) {
        CompilationInfo controller = env.javac;
        final Trees trees = controller.getTrees();
        final Elements elements = controller.getElements();
        final Types types = controller.getTypes();
        TreeUtilities tu = controller.getTreeUtilities();
        Element docelm = env.handle.resolve(controller);
        TreePath docpath = docelm != null ? trees.getPath(docelm) : null;
        final Scope scope = docpath != null ? trees.getScope(docpath) : tu.scopeFor(this.caretOffset);
        TypeElement enclClass = scope.getEnclosingClass();
        final boolean isStatic = enclClass == null ? false : tu.isStaticContext(scope);
        Object acceptor = new ElementUtilities.ElementAcceptor(){

            public boolean accept(Element e, TypeMirror t) {
                if (!(toExclude != null && toExclude.contains(e) || !e.getKind().isClass() && !e.getKind().isInterface() && e.getKind() != ElementKind.TYPE_PARAMETER)) {
                    String name = e.getSimpleName().toString();
                    return !(name.length() <= 0 || Character.isDigit(name.charAt(0)) || !JavadocCompletionQuery.this.startsWith(name, prefix) || isStatic && !e.getModifiers().contains((Object)Modifier.STATIC) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(e) || !JavadocCompletionQuery.this.isOfKindAndType(e.asType(), e, kinds, baseType, scope, trees, types));
                }
                return false;
            }
        };
        for (Element e2 : controller.getElementUtilities().getLocalMembersAndVars(scope, acceptor)) {
            switch (e2.getKind()) {
                case CLASS: 
                case ENUM: 
                case INTERFACE: 
                case ANNOTATION_TYPE: {
                    this.items.add(JavadocCompletionItem.createTypeItem(env.javac, (TypeElement)e2, substitutionOffset, null, elements.isDeprecated(e2)));
                }
            }
        }
        acceptor = new ElementUtilities.ElementAcceptor(){

            public boolean accept(Element e, TypeMirror t) {
                if (e.getKind().isClass() || e.getKind().isInterface()) {
                    return !(toExclude != null && toExclude.contains(e) || !JavadocCompletionQuery.this.startsWith(e.getSimpleName().toString(), prefix) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(e) || !trees.isAccessible(scope, (TypeElement)e) || !JavadocCompletionQuery.this.isOfKindAndType(e.asType(), e, kinds, baseType, scope, trees, types));
                }
                return false;
            }
        };
        for (Element e2 : controller.getElementUtilities().getGlobalTypes(acceptor)) {
            this.items.add(JavadocCompletionItem.createTypeItem(env.javac, (TypeElement)e2, substitutionOffset, null, elements.isDeprecated(e2)));
        }
    }

    private void addAllTypes(JavadocContext env, EnumSet<ElementKind> kinds, Set<? extends Element> toExclude, String prefix, int substitutionOffset) {
        CompilationInfo controller = env.javac;
        boolean isCaseSensitive = false;
        ClassIndex.NameKind kind = isCaseSensitive ? ClassIndex.NameKind.PREFIX : ClassIndex.NameKind.CASE_INSENSITIVE_PREFIX;
        HashSet<ElementHandle> excludeHandles = null;
        if (toExclude != null) {
            excludeHandles = new HashSet<ElementHandle>(toExclude.size());
            for (Element el : toExclude) {
                excludeHandles.add(ElementHandle.create((Element)el));
            }
        }
        for (ElementHandle name : controller.getClasspathInfo().getClassIndex().getDeclaredTypes(prefix, kind, EnumSet.allOf(ClassIndex.SearchScope.class))) {
            if (excludeHandles != null && excludeHandles.contains((Object)name) || JavadocCompletionQuery.isAnnonInner(name)) continue;
            this.items.add(LazyJavaCompletionItem.createTypeItem(name, kinds, substitutionOffset, env.getReferencesCount(), controller.getSnapshot().getSource(), false, false, false, null));
        }
    }

    private void addInnerClasses(TypeElement te, EnumSet<ElementKind> kinds, DeclaredType baseType, Set<? extends Element> toExclude, String prefix, int substitutionOffset, JavadocContext jdctx) {
        CompilationInfo controller = jdctx.javac;
        Element srcEl = jdctx.handle.resolve(controller);
        Elements elements = controller.getElements();
        Types types = controller.getTypes();
        Trees trees = controller.getTrees();
        TreeUtilities tu = controller.getTreeUtilities();
        TreePath docpath = srcEl != null ? trees.getPath(srcEl) : null;
        Scope scope = docpath != null ? trees.getScope(docpath) : tu.scopeFor(this.caretOffset);
        for (Element e : controller.getElementUtilities().getMembers(te.asType(), null)) {
            String name;
            if (!e.getKind().isClass() && !e.getKind().isInterface() || toExclude != null && toExclude.contains(e) || !Utilities.startsWith(name = e.getSimpleName().toString(), prefix) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(e) || !trees.isAccessible(scope, (TypeElement)e) || !this.isOfKindAndType(e.asType(), e, kinds, baseType, scope, trees, types)) continue;
            this.items.add(JavadocCompletionItem.createTypeItem(jdctx.javac, (TypeElement)e, substitutionOffset, null, elements.isDeprecated(e)));
        }
    }

    private void addPackageContent(PackageElement pe, EnumSet<ElementKind> kinds, DeclaredType baseType, Set<? extends Element> toExclude, String prefix, int substitutionOffset, JavadocContext jdctx) {
        CompilationInfo controller = jdctx.javac;
        Element srcEl = jdctx.handle.resolve(controller);
        Elements elements = controller.getElements();
        Types types = controller.getTypes();
        Trees trees = controller.getTrees();
        TreeUtilities tu = controller.getTreeUtilities();
        TreePath docpath = srcEl != null ? trees.getPath(srcEl) : null;
        Scope scope = docpath != null ? trees.getScope(docpath) : tu.scopeFor(this.caretOffset);
        for (Element e : pe.getEnclosedElements()) {
            String name;
            if (!e.getKind().isClass() && !e.getKind().isInterface() || toExclude != null && toExclude.contains(e) || !Utilities.startsWith(name = e.getSimpleName().toString(), prefix) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(e) || !trees.isAccessible(scope, (TypeElement)e) || !this.isOfKindAndType(e.asType(), e, kinds, baseType, scope, trees, types) || Utilities.isExcluded(Utilities.getElementName(e, true))) continue;
            this.items.add(JavadocCompletionItem.createTypeItem(jdctx.javac, (TypeElement)e, substitutionOffset, null, elements.isDeprecated(e)));
        }
    }

    private boolean isOfKindAndType(TypeMirror type, Element e, EnumSet<ElementKind> kinds, TypeMirror base, Scope scope, Trees trees, Types types) {
        if (kinds.contains((Object)e.getKind())) {
            if (base == null) {
                return true;
            }
            if (types.isSubtype(type, base)) {
                return true;
            }
        }
        if ((e.getKind().isClass() || e.getKind().isInterface()) && (kinds.contains((Object)ElementKind.ANNOTATION_TYPE) || kinds.contains((Object)ElementKind.CLASS) || kinds.contains((Object)ElementKind.ENUM) || kinds.contains((Object)ElementKind.INTERFACE))) {
            DeclaredType dt = (DeclaredType)e.asType();
            for (Element ee : e.getEnclosedElements()) {
                if (!trees.isAccessible(scope, ee, dt) || !this.isOfKindAndType(ee.asType(), ee, kinds, base, scope, trees, types)) continue;
                return true;
            }
        }
        return false;
    }

    private static boolean isAnnonInner(ElementHandle<TypeElement> elem) {
        String name = elem.getQualifiedName();
        int idx = name.lastIndexOf(46);
        String simpleName = idx > -1 ? name.substring(idx + 1) : name;
        return simpleName.length() == 0 || Character.isDigit(simpleName.charAt(0));
    }

    private List<DeclaredType> getSubtypesOf(DeclaredType baseType, String prefix, JavadocContext jdctx) {
        Scope scope;
        if (((TypeElement)baseType.asElement()).getQualifiedName().contentEquals("java.lang.Object")) {
            return Collections.emptyList();
        }
        LinkedList<DeclaredType> subtypes = new LinkedList<DeclaredType>();
        CompilationInfo controller = jdctx.javac;
        Types types = controller.getTypes();
        Trees trees = controller.getTrees();
        TreeUtilities tu = controller.getTreeUtilities();
        Element resolvedElm = jdctx.handle.resolve(controller);
        TreePath docpath = resolvedElm != null ? trees.getPath(resolvedElm) : null;
        Scope scope2 = scope = docpath != null ? trees.getScope(docpath) : tu.scopeFor(this.caretOffset);
        if (prefix != null && prefix.length() > 2 && baseType.getTypeArguments().isEmpty()) {
            ClassIndex.NameKind kind = Utilities.isCaseSensitive() ? ClassIndex.NameKind.PREFIX : ClassIndex.NameKind.CASE_INSENSITIVE_PREFIX;
            for (ElementHandle handle : controller.getClasspathInfo().getClassIndex().getDeclaredTypes(prefix, kind, EnumSet.allOf(ClassIndex.SearchScope.class))) {
                TypeElement te = (TypeElement)handle.resolve(controller);
                if (te == null || !trees.isAccessible(scope, te) || !types.isSubtype(types.getDeclaredType(te, new TypeMirror[0]), baseType)) continue;
                subtypes.add(types.getDeclaredType(te, new TypeMirror[0]));
            }
        } else {
            HashSet<TypeElement> elems = new HashSet<TypeElement>();
            LinkedList<DeclaredType> bases = new LinkedList<DeclaredType>();
            bases.add(baseType);
            ClassIndex index = controller.getClasspathInfo().getClassIndex();
            while (!bases.isEmpty()) {
                List<? extends TypeMirror> tas;
                DeclaredType head = (DeclaredType)bases.remove();
                TypeElement elem = (TypeElement)head.asElement();
                if (!elems.add(elem)) continue;
                if (this.startsWith(elem.getSimpleName().toString(), prefix) && trees.isAccessible(scope, elem)) {
                    subtypes.add(head);
                }
                boolean isRaw = !(tas = head.getTypeArguments()).iterator().hasNext();
                block2 : for (ElementHandle eh : index.getElements(ElementHandle.create((Element)elem), EnumSet.of(ClassIndex.SearchKind.IMPLEMENTORS), EnumSet.allOf(ClassIndex.SearchScope.class))) {
                    TypeElement e = (TypeElement)eh.resolve(controller);
                    if (e != null) {
                        if (!trees.isAccessible(scope, e)) continue;
                        if (isRaw) {
                            DeclaredType dt = types.getDeclaredType(e, new TypeMirror[0]);
                            bases.add(dt);
                            continue;
                        }
                        HashMap<Element, TypeMirror> map = new HashMap<Element, TypeMirror>();
                        TypeMirror sup = e.getSuperclass();
                        if (sup.getKind() == TypeKind.DECLARED && ((DeclaredType)sup).asElement() == elem) {
                            DeclaredType dt = (DeclaredType)sup;
                            Iterator<? extends TypeMirror> ittas = tas.iterator();
                            Iterator<? extends TypeMirror> it = dt.getTypeArguments().iterator();
                            while (it.hasNext() && ittas.hasNext()) {
                                TypeMirror stm;
                                TypeMirror basetm = ittas.next();
                                if (basetm == (stm = it.next())) continue;
                                if (stm.getKind() != TypeKind.TYPEVAR) continue block2;
                                map.put(((TypeVariable)stm).asElement(), basetm);
                            }
                            if (it.hasNext() != ittas.hasNext()) {
                                continue;
                            }
                        } else {
                            for (TypeMirror tm : e.getInterfaces()) {
                                if (((DeclaredType)tm).asElement() != elem) continue;
                                DeclaredType dt = (DeclaredType)tm;
                                Iterator<? extends TypeMirror> ittas = tas.iterator();
                                Iterator<? extends TypeMirror> it = dt.getTypeArguments().iterator();
                                while (it.hasNext() && ittas.hasNext()) {
                                    TypeMirror stm;
                                    TypeMirror basetm = ittas.next();
                                    if (basetm == (stm = it.next())) continue;
                                    if (stm.getKind() != TypeKind.TYPEVAR) continue block2;
                                    map.put(((TypeVariable)stm).asElement(), basetm);
                                }
                                if (it.hasNext() != ittas.hasNext()) continue block2;
                            }
                        }
                        bases.add(this.getDeclaredType(e, map, types));
                        continue;
                    }
                    Logger.getLogger("global").log(Level.FINE, String.format("Cannot resolve: %s on bootpath: %s classpath: %s sourcepath: %s\n", new Object[]{eh.toString(), controller.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.BOOT), controller.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.COMPILE), controller.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.SOURCE)}));
                }
            }
        }
        return subtypes;
    }

    private DeclaredType getDeclaredType(TypeElement e, HashMap<? extends Element, ? extends TypeMirror> map, Types types) {
        List<? extends TypeParameterElement> tpes = e.getTypeParameters();
        TypeMirror[] targs = new TypeMirror[tpes.size()];
        int i = 0;
        for (TypeParameterElement tpe : tpes) {
            TypeMirror t = map.get(tpe);
            targs[i++] = t != null ? t : tpe.asType();
        }
        Element encl = e.getEnclosingElement();
        if ((encl.getKind().isClass() || encl.getKind().isInterface()) && !((TypeElement)encl).getTypeParameters().isEmpty()) {
            return types.getDeclaredType(this.getDeclaredType((TypeElement)encl, map, types), e, targs);
        }
        return types.getDeclaredType(e, targs);
    }

    private boolean startsWith(String theString, String prefix) {
        return Utilities.startsWith(theString, prefix);
    }

    void resolveOtherText(JavadocContext jdctx, TokenSequence<JavadocTokenId> jdts) {
        Token token = jdts.token();
        assert (token != null);
        assert (token.id() == JavadocTokenId.OTHER_TEXT);
        CharSequence text = token.text();
        int pos = this.caretOffset - jdts.offset();
        DocTreePath tag = this.getTag(jdctx, this.caretOffset);
        if (pos > 0 && pos <= text.length() && text.charAt(pos - 1) == '{') {
            int start;
            if (tag != null && !JavadocCompletionUtils.isBlockTag(tag) && (start = (int)jdctx.positions.getStartPosition(jdctx.javac.getCompilationUnit(), jdctx.comment, tag.getLeaf())) + 1 != this.caretOffset) {
                return;
            }
            this.resolveInlineTag(null, jdctx);
            return;
        }
        if (tag != null) {
            this.insideTag(tag, jdctx);
            if (JavadocCompletionUtils.isBlockTag(tag) && JavadocCompletionUtils.isLineBreak(token, pos)) {
                this.resolveBlockTag(null, jdctx);
            }
        } else if (JavadocCompletionUtils.isLineBreak(token, pos)) {
            this.resolveBlockTag(null, jdctx);
        }
    }

    static final class JavadocContext {
        int anchorOffset = -1;
        ElementHandle<Element> handle;
        Element commentFor;
        DocCommentTree comment;
        DocSourcePositions positions;
        TokenSequence<JavadocTokenId> jdts;
        Document doc;
        CompilationInfo javac;
        private ReferencesCount count;
        private TreePath javadocFor;

        JavadocContext() {
        }

        ReferencesCount getReferencesCount() {
            if (this.count == null) {
                this.count = ReferencesCount.get((ClasspathInfo)this.javac.getClasspathInfo());
            }
            return this.count;
        }
    }

}

