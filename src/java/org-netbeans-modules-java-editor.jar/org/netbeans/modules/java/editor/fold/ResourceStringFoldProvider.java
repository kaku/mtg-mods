/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.AssignmentTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.LiteralTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodInvocationTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreePathScanner
 *  com.sun.source.util.Trees
 *  org.netbeans.api.editor.fold.FoldType
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.JavaParserResultTask
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.ParserResultTask
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.netbeans.modules.parsing.spi.TaskIndexingMode
 *  org.netbeans.spi.editor.fold.FoldInfo
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.java.editor.fold;

import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreePathScanner;
import com.sun.source.util.Trees;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.JavaParserResultTask;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.modules.java.editor.fold.JavaFoldTypeProvider;
import org.netbeans.modules.java.editor.fold.MessagePattern;
import org.netbeans.modules.java.editor.fold.ParsingFoldSupport;
import org.netbeans.modules.java.editor.fold.ResourceStringFoldInfo;
import org.netbeans.modules.java.editor.fold.ResourceStringLoader;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserResultTask;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.TaskIndexingMode;
import org.netbeans.spi.editor.fold.FoldInfo;
import org.openide.filesystems.FileObject;

public class ResourceStringFoldProvider
extends ParsingFoldSupport {
    private List<MessagePattern> messages = new ArrayList<MessagePattern>();

    public ResourceStringFoldProvider() {
        this.messages.add(new MessagePattern(Pattern.compile("^" + Pattern.quote("java.util.ResourceBundle") + "$"), Pattern.compile("^getString$"), -2, 0));
        this.messages.add(new MessagePattern(Pattern.compile("^" + Pattern.quote("org.openide.util.NbBundle") + "$"), Pattern.compile("^getMessage$"), 0, 1));
        this.messages.add(new MessagePattern(Pattern.compile("^" + Pattern.quote("java.util.ResourceBundle") + "$"), Pattern.compile("^getBundle$"), 0, -3));
        this.messages.add(new MessagePattern(Pattern.compile("^" + Pattern.quote("org.openide.util.NbBundle") + "$"), Pattern.compile("^getBundle$"), 0, -3));
        this.messages.add(new MessagePattern(Pattern.compile("\\.[^.]+(Bundle|Messages)$"), Pattern.compile("^(getMessage|getString)"), -4, 0));
        this.messages.add(new MessagePattern(Pattern.compile("\\.Bundle$"), Pattern.compile(".*"), -4, -5));
    }

    @Override
    protected ParsingFoldSupport.FoldProcessor createTask(FileObject f) {
        return new Proc(f);
    }

    @Override
    protected ParserResultTask createParserTask(FileObject file, ParsingFoldSupport.FoldProcessor processor) {
        final ParserResultTask wrapped = super.createParserTask(file, processor);
        return new JavaParserResultTask(JavaSource.Phase.RESOLVED, TaskIndexingMode.ALLOWED_DURING_SCAN){

            public void run(Parser.Result result, SchedulerEvent event) {
                wrapped.run(result, event);
            }

            public int getPriority() {
                return wrapped.getPriority();
            }

            public Class<? extends Scheduler> getSchedulerClass() {
                return wrapped.getSchedulerClass();
            }

            public void cancel() {
                wrapped.cancel();
            }
        };
    }

    private static class V
    extends TreePathScanner<Void, Void> {
        private final FileObject anchor;
        private final Proc proc;
        private final CompilationInfo info;
        private MessagePattern messageMethod;
        private Map<Element, String> variableBundles = new HashMap<Element, String>();
        private TypeMirror resourceBundleType;
        private String exprBundleName;
        private TreePath methodOwnerPath;
        private String methodName;
        private List<MessagePattern> descriptions = new ArrayList<MessagePattern>();

        public V(CompilationInfo info, FileObject anchor, Proc proc) {
            this.proc = proc;
            this.info = info;
            this.anchor = anchor;
            TypeElement el = info.getElements().getTypeElement("java.util.ResourceBundle");
            if (el != null) {
                this.resourceBundleType = el.asType();
            }
        }

        public void setDescriptions(List<MessagePattern> descs) {
            this.descriptions = descs;
        }

        private boolean isCancelled() {
            return this.proc.isCancelled();
        }

        private void defineFold(String bundle, String key, Tree expr) {
            ClassPath cp = ClassPath.getClassPath((FileObject)this.anchor, (String)"classpath/source");
            FileObject bundleFile = cp != null ? cp.findResource(bundle + ".properties") : null;
            SourcePositions spos = this.info.getTrees().getSourcePositions();
            int start = (int)spos.getStartPosition(this.info.getCompilationUnit(), expr);
            int end = (int)spos.getEndPosition(this.info.getCompilationUnit(), expr);
            if (start == -1 || end == -1) {
                return;
            }
            if (bundleFile == null) {
                return;
            }
            String message = this.proc.loader.getMessage(bundleFile, key);
            if (message == null) {
                return;
            }
            int newline = message.indexOf(10);
            if (newline >= 0) {
                message = message.substring(0, newline);
            }
            message = message.replaceAll("\\.\\.\\.", "\u2026");
            FoldInfo info = FoldInfo.range((int)start, (int)end, (FoldType)JavaFoldTypeProvider.BUNDLE_STRING).withDescription(message).attach((Object)new ResourceStringFoldInfo(bundle, key));
            this.proc.addFold(info, -1);
        }

        public Void scan(Tree tree, Void p) {
            if (this.isCancelled()) {
                return null;
            }
            TreePathScanner.super.scan(tree, (Object)p);
            return p;
        }

        public Void visitMemberSelect(MemberSelectTree node, Void p) {
            Void d = (Void)TreePathScanner.super.visitMemberSelect(node, (Object)p);
            Element el = this.info.getTrees().getElement(this.getCurrentPath());
            this.messageMethod = null;
            if (el == null || el.getKind() != ElementKind.METHOD) {
                return d;
            }
            ExecutableElement ee = (ExecutableElement)el;
            String sn = ee.getSimpleName().toString();
            for (MessagePattern desc : this.descriptions) {
                if (!desc.getMethodNamePattern().matcher(sn).matches() || (el = ee.getEnclosingElement()) == null || !el.getKind().isClass() && !el.getKind().isInterface()) continue;
                TypeElement tel = (TypeElement)el;
                if (!desc.getOwnerTypePattern().matcher(tel.getQualifiedName().toString()).matches()) continue;
                this.messageMethod = desc;
                this.methodName = sn;
                this.methodOwnerPath = new TreePath(this.getCurrentPath(), (Tree)node.getExpression());
                break;
            }
            return d;
        }

        public Void visitVariable(VariableTree node, Void p) {
            this.scan((Tree)node.getInitializer(), null);
            if (this.exprBundleName == null) {
                return null;
            }
            TypeMirror tm = this.info.getTrees().getTypeMirror(this.getCurrentPath());
            if (this.resourceBundleType == null || !this.info.getTypes().isAssignable(tm, this.resourceBundleType)) {
                return null;
            }
            Element dest = this.info.getTrees().getElement(this.getCurrentPath());
            if (dest != null && (dest.getKind() == ElementKind.LOCAL_VARIABLE || dest.getKind() == ElementKind.FIELD)) {
                this.variableBundles.put(dest, this.exprBundleName);
            }
            return null;
        }

        public Void visitAssignment(AssignmentTree node, Void p) {
            Element dest;
            this.exprBundleName = null;
            Void d = (Void)TreePathScanner.super.visitAssignment(node, (Object)p);
            if (this.exprBundleName != null && (dest = this.info.getTrees().getElement(this.getCurrentPath())) != null && (dest.getKind() == ElementKind.LOCAL_VARIABLE || dest.getKind() == ElementKind.FIELD)) {
                this.variableBundles.put(dest, this.exprBundleName);
            }
            return d;
        }

        private void processGetBundleCall(MethodInvocationTree node) {
            TypeMirror tm = this.info.getTrees().getTypeMirror(this.getCurrentPath());
            if (this.resourceBundleType == null || tm == null || tm.getKind() != TypeKind.DECLARED) {
                return;
            }
            if (!this.info.getTypes().isAssignable(tm, this.resourceBundleType)) {
                return;
            }
            this.exprBundleName = this.getBundleName(node, this.messageMethod.getBundleParam(), this.messageMethod.getBundleFile());
        }

        private String getBundleName(MethodInvocationTree n, int index, String bfn) {
            if (n.getArguments().size() <= index) {
                return null;
            }
            ExpressionTree t = (ExpressionTree)n.getArguments().get(index);
            if (t.getKind() == Tree.Kind.STRING_LITERAL) {
                Object o = ((LiteralTree)t).getValue();
                return o == null ? null : o.toString();
            }
            if (t.getKind() == Tree.Kind.MEMBER_SELECT) {
                MemberSelectTree mst = (MemberSelectTree)t;
                if (!mst.getIdentifier().contentEquals("class")) {
                    return null;
                }
                return this.bundleFileFromClass(new TreePath(this.getCurrentPath(), (Tree)mst.getExpression()), bfn);
            }
            return null;
        }

        private String bundleFileFromClass(TreePath classTreePath, String bfn) {
            TypeMirror tm = this.info.getTrees().getTypeMirror(classTreePath);
            if (tm.getKind() != TypeKind.DECLARED) {
                return null;
            }
            Element clazz = ((DeclaredType)tm).asElement();
            while (clazz instanceof TypeElement) {
                clazz = ((TypeElement)clazz).getEnclosingElement();
            }
            if (clazz.getKind() == ElementKind.PACKAGE) {
                PackageElement pack = (PackageElement)clazz;
                if (pack.isUnnamed()) {
                    return null;
                }
                return pack.getQualifiedName().toString().replaceAll("\\.", "/") + "/" + bfn;
            }
            return null;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Void visitMethodInvocation(MethodInvocationTree node, Void p) {
            this.messageMethod = null;
            this.exprBundleName = null;
            Void d = this.scan((Tree)node.getMethodSelect(), p);
            try {
                String keyVal;
                if (this.messageMethod == null) {
                    Void void_ = d;
                    return void_;
                }
                String bundleFile = null;
                if (this.messageMethod.getKeyParam() == -3) {
                    this.processGetBundleCall(node);
                } else {
                    int bp = this.messageMethod.getBundleParam();
                    if (bp == -4) {
                        TypeMirror tm = this.info.getTrees().getTypeMirror(this.methodOwnerPath);
                        if (tm != null && tm.getKind() == TypeKind.DECLARED) {
                            bundleFile = this.bundleFileFromClass(this.methodOwnerPath, this.messageMethod.getBundleFile());
                        }
                    } else if (bp == -2) {
                        Element el = this.info.getTrees().getElement(this.methodOwnerPath);
                        bundleFile = el != null && (el.getKind() == ElementKind.LOCAL_VARIABLE || el.getKind() == ElementKind.FIELD) ? this.variableBundles.get(el) : this.exprBundleName;
                    } else if (bp >= 0 && bp < node.getArguments().size()) {
                        bundleFile = this.getBundleName(node, bp, this.messageMethod.getBundleFile());
                    }
                }
                if (bundleFile == null) {
                    Void bp = d;
                    return bp;
                }
                int keyIndex = this.messageMethod.getKeyParam();
                if (node.getArguments().size() <= keyIndex) {
                    Void expr = d;
                    return expr;
                }
                if (keyIndex == -5) {
                    keyVal = this.methodName;
                } else {
                    ExpressionTree keyArg = (ExpressionTree)node.getArguments().get(keyIndex);
                    if (keyArg.getKind() != Tree.Kind.STRING_LITERAL) {
                        Void void_ = d;
                        return void_;
                    }
                    Object o = ((LiteralTree)keyArg).getValue();
                    if (o == null) {
                        Void expr = d;
                        return expr;
                    }
                    keyVal = o.toString();
                }
                this.defineFold(bundleFile, keyVal, (Tree)node);
            }
            finally {
                String expr = this.exprBundleName;
                this.scan((Iterable)node.getArguments(), (Object)p);
                this.exprBundleName = expr;
            }
            return d;
        }
    }

    private final class Proc
    extends ParsingFoldSupport.FoldProcessor
    implements ChangeListener {
        ResourceStringLoader loader;

        public Proc(FileObject f) {
            super(f, "text/x-java");
        }

        @Override
        protected boolean processResult(Parser.Result result) {
            CompilationInfo info = CompilationInfo.get((Parser.Result)result);
            if (info == null) {
                return false;
            }
            CompilationController ctrl = CompilationController.get((Parser.Result)result);
            if (ctrl != null) {
                try {
                    ctrl.toPhase(JavaSource.Phase.RESOLVED);
                }
                catch (IOException ex) {
                    return false;
                }
            }
            if (this.loader == null) {
                this.loader = new ResourceStringLoader(this);
            }
            V v = new V(info, this.getFile(), this);
            v.setDescriptions(ResourceStringFoldProvider.this.messages);
            v.scan((Tree)info.getCompilationUnit(), null);
            return true;
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            this.performRefresh();
        }
    }

}

