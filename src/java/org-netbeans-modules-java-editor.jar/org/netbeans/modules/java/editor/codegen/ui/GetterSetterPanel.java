/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.source.ElementHandle
 *  org.openide.awt.Mnemonics
 *  org.openide.explorer.ExplorerManager
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.java.editor.codegen.ui;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import javax.accessibility.AccessibleContext;
import javax.lang.model.element.Element;
import javax.swing.AbstractButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.modules.java.editor.codegen.GetterSetterGenerator;
import org.netbeans.modules.java.editor.codegen.ui.ElementNode;
import org.netbeans.modules.java.editor.codegen.ui.ElementSelectorPanel;
import org.openide.awt.Mnemonics;
import org.openide.explorer.ExplorerManager;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

public class GetterSetterPanel
extends JPanel
implements PropertyChangeListener {
    private ElementSelectorPanel elementSelector;
    private HashSet<ElementHandle<? extends Element>> thisFields;
    private RequestProcessor.Task currentTask;
    private final RequestProcessor RP = new RequestProcessor(GetterSetterPanel.class);
    private JCheckBox performEncapsulate;
    private JLabel selectorLabel;

    public GetterSetterPanel(ElementNode.Description description, int type) {
        this.initComponents();
        this.elementSelector = new ElementSelectorPanel(description, false);
        this.elementSelector.getExplorerManager().addPropertyChangeListener((PropertyChangeListener)this);
        this.thisFields = new HashSet();
        for (ElementNode.Description desc : description.getSubs().get(0).getSubs()) {
            this.thisFields.add(desc.getElementHandle());
        }
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 12, 0, 12);
        this.add((Component)this.elementSelector, gridBagConstraints);
        if (type == 1) {
            this.selectorLabel.setText(NbBundle.getMessage(GetterSetterGenerator.class, (String)"LBL_getter_field_select"));
        } else if (type == 2) {
            this.selectorLabel.setText(NbBundle.getMessage(GetterSetterGenerator.class, (String)"LBL_setter_field_select"));
        } else {
            this.selectorLabel.setText(NbBundle.getMessage(GetterSetterGenerator.class, (String)"LBL_getter_and_setter_field_select"));
        }
        this.selectorLabel.setLabelFor(this.elementSelector);
        this.elementSelector.doInitialExpansion(1);
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(GetterSetterGenerator.class, (String)"A11Y_Generate_GetterSetter"));
    }

    public List<ElementHandle<? extends Element>> getVariables() {
        return this.elementSelector.getSelectedElements();
    }

    private void initComponents() {
        this.selectorLabel = new JLabel();
        this.performEncapsulate = new JCheckBox();
        this.setLayout(new GridBagLayout());
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(12, 12, 6, 12);
        this.add((Component)this.selectorLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.performEncapsulate, (String)NbBundle.getMessage(GetterSetterGenerator.class, (String)"GetterSetterPanel.performEncapsulate.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(0, 0, 0, 12);
        this.add((Component)this.performEncapsulate, gridBagConstraints);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("nodeChange".equals(evt.getPropertyName())) {
            this.updateEncapsulateCheckBox();
        }
    }

    private void updateEncapsulateCheckBox() {
        if (this.currentTask != null) {
            this.currentTask.cancel();
        }
        this.currentTask = this.RP.post(new Runnable(){

            @Override
            public void run() {
                final List<ElementHandle<? extends Element>> selected = GetterSetterPanel.this.elementSelector.getSelectedElements();
                selected.removeAll(GetterSetterPanel.this.thisFields);
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        GetterSetterPanel.this.performEncapsulate.setEnabled(selected.isEmpty());
                    }
                });
            }

        });
    }

    public boolean isPerformEnsapsulate() {
        return this.performEncapsulate.isEnabled() && this.performEncapsulate.isSelected();
    }

}

