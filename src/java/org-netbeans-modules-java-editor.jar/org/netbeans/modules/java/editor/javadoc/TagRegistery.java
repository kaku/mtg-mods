/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.editor.javadoc;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import javax.lang.model.element.ElementKind;

final class TagRegistery {
    private static final Set<ElementKind> ALL_KINDS = EnumSet.of(ElementKind.ANNOTATION_TYPE, new ElementKind[]{ElementKind.CLASS, ElementKind.CONSTRUCTOR, ElementKind.ENUM, ElementKind.ENUM_CONSTANT, ElementKind.FIELD, ElementKind.INTERFACE, ElementKind.METHOD, ElementKind.OTHER, ElementKind.PACKAGE});
    private static final TagRegistery DEFAULT = new TagRegistery();
    private final List<TagEntry> tags = new ArrayList<TagEntry>(20);

    public static TagRegistery getDefault() {
        return DEFAULT;
    }

    public List<TagEntry> getTags(ElementKind kind, boolean inline) {
        ArrayList<TagEntry> selection = new ArrayList<TagEntry>();
        for (TagEntry te : this.tags) {
            if (te.isInline != inline || !te.whereUsed.contains((Object)kind)) continue;
            selection.add(te);
        }
        return selection;
    }

    private TagRegistery() {
        this.addTag("@author", false, EnumSet.of(ElementKind.ANNOTATION_TYPE, new ElementKind[]{ElementKind.CLASS, ElementKind.ENUM, ElementKind.INTERFACE, ElementKind.OTHER, ElementKind.PACKAGE}));
        this.addTag("@exception", false, EnumSet.of(ElementKind.METHOD, ElementKind.CONSTRUCTOR));
        this.addTag("@deprecated", false, EnumSet.of(ElementKind.ANNOTATION_TYPE, new ElementKind[]{ElementKind.CLASS, ElementKind.CONSTRUCTOR, ElementKind.ENUM, ElementKind.ENUM_CONSTANT, ElementKind.FIELD, ElementKind.INTERFACE, ElementKind.METHOD}));
        this.addTag("@param", false, EnumSet.of(ElementKind.METHOD, ElementKind.CONSTRUCTOR, ElementKind.CLASS, ElementKind.INTERFACE));
        this.addTag("@return", false, EnumSet.of(ElementKind.METHOD));
        this.addTag("@see", false, ALL_KINDS);
        this.addTag("@serial", false, EnumSet.of(ElementKind.ANNOTATION_TYPE, new ElementKind[]{ElementKind.CLASS, ElementKind.ENUM, ElementKind.ENUM_CONSTANT, ElementKind.FIELD, ElementKind.INTERFACE, ElementKind.PACKAGE}));
        this.addTag("@serialData", false, EnumSet.of(ElementKind.METHOD));
        this.addTag("@serialField", false, EnumSet.of(ElementKind.FIELD));
        this.addTag("@since", false, ALL_KINDS);
        this.addTag("@throws", false, EnumSet.of(ElementKind.METHOD, ElementKind.CONSTRUCTOR));
        this.addTag("@version", false, EnumSet.of(ElementKind.ANNOTATION_TYPE, new ElementKind[]{ElementKind.CLASS, ElementKind.ENUM, ElementKind.INTERFACE, ElementKind.OTHER, ElementKind.PACKAGE}));
        this.addTag("@code", true, ALL_KINDS);
        this.addTag("@docRoot", true, ALL_KINDS);
        this.addTag("@inheritDoc", true, EnumSet.of(ElementKind.METHOD));
        this.addTag("@link", true, ALL_KINDS);
        this.addTag("@linkplain", true, ALL_KINDS);
        this.addTag("@literal", true, ALL_KINDS);
        this.addTag("@value", true, EnumSet.of(ElementKind.FIELD));
    }

    void addTag(String name, boolean isInline, Set<ElementKind> where) {
        if (name == null || where == null) {
            throw new NullPointerException();
        }
        TagEntry te = new TagEntry();
        te.name = name;
        te.isInline = isInline;
        te.whereUsed = where;
        this.tags.add(te);
    }

    static final class TagEntry {
        String name;
        boolean isInline;
        Set<ElementKind> whereUsed;
        String format;

        TagEntry() {
        }
    }

}

