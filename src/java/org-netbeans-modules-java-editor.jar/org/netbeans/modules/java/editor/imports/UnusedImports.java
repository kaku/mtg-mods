/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreeScanner
 *  com.sun.source.util.Trees
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.CompilationInfo$CacheClearPolicy
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.support.CancellableTreePathScanner
 */
package org.netbeans.modules.java.editor.imports;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreeScanner;
import com.sun.source.util.Trees;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.support.CancellableTreePathScanner;
import org.netbeans.modules.java.editor.javadoc.JavadocImports;

public class UnusedImports {
    private static final Object KEY_CACHE = new Object();

    public static Collection<TreePath> process(CompilationInfo info, AtomicBoolean cancel) {
        Collection result = (Collection)info.getCachedValue(KEY_CACHE);
        if (result != null) {
            return result;
        }
        DetectorVisitor v = new DetectorVisitor(info, cancel);
        CompilationUnitTree cu = info.getCompilationUnit();
        v.scan((Tree)cu, (Object)null);
        if (cancel.get()) {
            return null;
        }
        List allUnusedImports = new ArrayList();
        for (TreePath tree : v.import2Highlight.values()) {
            if (cancel.get()) {
                return null;
            }
            allUnusedImports.add(tree);
        }
        allUnusedImports = Collections.unmodifiableList(allUnusedImports);
        info.putCachedValue(KEY_CACHE, allUnusedImports, CompilationInfo.CacheClearPolicy.ON_CHANGE);
        return allUnusedImports;
    }

    private static class DetectorVisitor
    extends CancellableTreePathScanner<Void, Void> {
        private final CompilationInfo info;
        private final Map<Element, ImportTree> element2Import = new HashMap<Element, ImportTree>();
        private final Set<Element> importedBySingleImport = new HashSet<Element>();
        private final Map<String, Collection<ImportTree>> simpleName2UnresolvableImports = new HashMap<String, Collection<ImportTree>>();
        private final Set<ImportTree> unresolvablePackageImports = new HashSet<ImportTree>();
        private final Map<ImportTree, TreePath> import2Highlight = new HashMap<ImportTree, TreePath>();

        private DetectorVisitor(CompilationInfo info, AtomicBoolean cancel) {
            super(cancel);
            this.info = info;
        }

        private void handleJavadoc(TreePath classMember) {
            if (classMember == null) {
                return;
            }
            for (Element el : JavadocImports.computeReferencedElements(this.info, classMember)) {
                this.typeUsed(el, null, false);
            }
        }

        public Void visitClass(ClassTree node, Void p) {
            this.handleJavadoc(this.getCurrentPath());
            return (Void)CancellableTreePathScanner.super.visitClass(node, (Object)p);
        }

        public Void visitMethod(MethodTree node, Void p) {
            this.handleJavadoc(this.getCurrentPath());
            return (Void)CancellableTreePathScanner.super.visitMethod(node, (Object)p);
        }

        public Void visitVariable(VariableTree node, Void p) {
            Element e = this.info.getTrees().getElement(this.getCurrentPath());
            if (e != null && e.getKind().isField()) {
                this.handleJavadoc(this.getCurrentPath());
            }
            return (Void)CancellableTreePathScanner.super.visitVariable(node, (Object)p);
        }

        public Void visitCompilationUnit(CompilationUnitTree tree, Void d) {
            this.scan((Iterable)tree.getImports(), (Object)d);
            this.scan((Iterable)tree.getPackageAnnotations(), (Object)d);
            this.scan((Iterable)tree.getTypeDecls(), (Object)d);
            return null;
        }

        public Void visitIdentifier(IdentifierTree tree, Void d) {
            if (this.info.getTreeUtilities().isSynthetic(this.getCurrentPath())) {
                return null;
            }
            this.typeUsed(this.info.getTrees().getElement(this.getCurrentPath()), this.getCurrentPath(), this.getCurrentPath().getParentPath().getLeaf().getKind() == Tree.Kind.METHOD_INVOCATION);
            return (Void)CancellableTreePathScanner.super.visitIdentifier(tree, (Object)null);
        }

        private boolean isStar(ImportTree tree) {
            Tree qualIdent = tree.getQualifiedIdentifier();
            if (qualIdent == null || qualIdent.getKind() == Tree.Kind.IDENTIFIER) {
                return false;
            }
            return ((MemberSelectTree)qualIdent).getIdentifier().contentEquals("*");
        }

        private boolean parseErrorInImport(ImportTree imp) {
            if (this.isStar(imp)) {
                return false;
            }
            final StringBuilder fqn = new StringBuilder();
            new TreeScanner<Void, Void>(){

                public Void visitMemberSelect(MemberSelectTree node, Void p) {
                    TreeScanner.super.visitMemberSelect(node, (Object)p);
                    fqn.append('.');
                    fqn.append(node.getIdentifier());
                    return null;
                }

                public Void visitIdentifier(IdentifierTree node, Void p) {
                    fqn.append(node.getName());
                    return null;
                }
            }.scan(imp.getQualifiedIdentifier(), (Object)null);
            return !SourceVersion.isName(fqn);
        }

        public boolean isErroneous(@NullAllowed Element e) {
            if (e == null) {
                return true;
            }
            TypeMirror type = e.asType();
            if (type == null) {
                return false;
            }
            return type.getKind() == TypeKind.ERROR || type.getKind() == TypeKind.OTHER;
        }

        public Void visitImport(ImportTree tree, Void d) {
            if (this.parseErrorInImport(tree)) {
                return (Void)CancellableTreePathScanner.super.visitImport(tree, (Object)null);
            }
            if (tree.getQualifiedIdentifier() == null || tree.getQualifiedIdentifier().getKind() != Tree.Kind.MEMBER_SELECT) {
                return (Void)CancellableTreePathScanner.super.visitImport(tree, (Object)null);
            }
            MemberSelectTree qualIdent = (MemberSelectTree)tree.getQualifiedIdentifier();
            boolean assign = false;
            boolean star = this.isStar(tree);
            TreePath tp = tree.isStatic() || star ? new TreePath(new TreePath(this.getCurrentPath(), (Tree)qualIdent), (Tree)qualIdent.getExpression()) : new TreePath(this.getCurrentPath(), tree.getQualifiedIdentifier());
            Element decl = this.info.getTrees().getElement(tp);
            this.import2Highlight.put(tree, this.getCurrentPath());
            if (decl != null && !this.isErroneous(decl)) {
                if (!tree.isStatic()) {
                    if (star) {
                        List<TypeElement> types = ElementFilter.typesIn(decl.getEnclosedElements());
                        for (TypeElement te : types) {
                            assign = true;
                            if (this.element2Import.containsKey(te)) continue;
                            this.element2Import.put(te, tree);
                        }
                    } else {
                        this.element2Import.put(decl, tree);
                        this.importedBySingleImport.add(decl);
                    }
                } else if (decl.getKind().isClass() || decl.getKind().isInterface()) {
                    Name simpleName = this.isStar(tree) ? null : qualIdent.getIdentifier();
                    for (Element e : this.info.getElements().getAllMembers((TypeElement)decl)) {
                        if (!e.getModifiers().contains((Object)Modifier.STATIC) || simpleName != null && !e.getSimpleName().equals(simpleName)) continue;
                        this.element2Import.put(e, tree);
                        assign = true;
                    }
                }
            }
            if (!assign) {
                if (!tree.isStatic() && star) {
                    this.unresolvablePackageImports.add(tree);
                } else {
                    this.addUnresolvableImport(qualIdent.getIdentifier(), tree);
                }
            }
            CancellableTreePathScanner.super.visitImport(tree, (Object)null);
            return null;
        }

        private void addUnresolvableImport(Name name, ImportTree imp) {
            String key = name.toString();
            Collection<ImportTree> l = this.simpleName2UnresolvableImports.get(key);
            if (l == null) {
                l = new LinkedList<ImportTree>();
                this.simpleName2UnresolvableImports.put(key, l);
            }
            l.add(imp);
        }

        /*
         * Enabled force condition propagation
         * Lifted jumps to return sites
         */
        private void typeUsed(Element decl, TreePath expr, boolean methodInvocation) {
            if (decl == null || expr != null && expr.getLeaf().getKind() != Tree.Kind.IDENTIFIER && expr.getLeaf().getKind() != Tree.Kind.PARAMETERIZED_TYPE) return;
            if (!this.isErroneous(decl)) {
                ImportTree imp = this.element2Import.get(decl);
                if (imp == null) return;
                this.import2Highlight.remove((Object)imp);
                if (!this.isStar(imp)) return;
                this.handleUnresolvableImports(decl, methodInvocation, false);
                return;
            } else {
                this.handleUnresolvableImports(decl, methodInvocation, true);
                for (Map.Entry<Element, ImportTree> e : this.element2Import.entrySet()) {
                    if (this.importedBySingleImport.contains(e.getKey()) || !e.getKey().getSimpleName().equals(decl.getSimpleName())) continue;
                    this.import2Highlight.remove((Object)e.getValue());
                }
            }
        }

        /*
         * Enabled force condition propagation
         * Lifted jumps to return sites
         */
        private void handleUnresolvableImports(Element decl, boolean methodInvocation, boolean removeStarImports) {
            Name simpleName = decl.getSimpleName();
            if (simpleName == null) return;
            Collection<ImportTree> imps = this.simpleName2UnresolvableImports.get(simpleName.toString());
            if (imps != null) {
                for (ImportTree imp : imps) {
                    if (methodInvocation && !imp.isStatic()) continue;
                    this.import2Highlight.remove((Object)imp);
                }
                return;
            } else {
                if (!removeStarImports) return;
                for (ImportTree unresolvable : this.unresolvablePackageImports) {
                    if (methodInvocation && !unresolvable.isStatic()) continue;
                    this.import2Highlight.remove((Object)unresolvable);
                }
            }
        }

    }

}

