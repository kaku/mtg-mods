/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseAction
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.spi.editor.highlighting.HighlightsSequence
 *  org.netbeans.spi.editor.highlighting.support.OffsetsBag
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.semantic;

import java.awt.event.ActionEvent;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.BaseAction;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.java.editor.semantic.MarkOccurrencesHighlighter;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.netbeans.spi.editor.highlighting.support.OffsetsBag;
import org.openide.awt.StatusDisplayer;
import org.openide.util.NbBundle;

public class GoToMarkOccurrencesAction
extends BaseAction {
    private static final String prevActionName = "java-prev-marked-occurrence";
    private static final String nextActionName = "java-next-marked-occurrence";
    static final String markedOccurence = "marked-occurrence";
    private final boolean next;

    public GoToMarkOccurrencesAction(boolean nextOccurrence) {
        super(GoToMarkOccurrencesAction.getNameString(nextOccurrence));
        this.next = nextOccurrence;
        this.putValue("ShortDescription", this.getDefaultShortDescription());
    }

    public void actionPerformed(ActionEvent evt, JTextComponent txt) {
        GoToMarkOccurrencesAction.navigateToOccurence(this.next, txt);
    }

    protected Object getDefaultShortDescription() {
        return NbBundle.getMessage(GoToMarkOccurrencesAction.class, (String)GoToMarkOccurrencesAction.getNameString(this.next));
    }

    private static String getNameString(boolean nextOccurrence) {
        return nextOccurrence ? "java-next-marked-occurrence" : "java-prev-marked-occurrence";
    }

    private static int findOccurrencePosition(boolean directionForward, Document doc, int curPos) {
        OffsetsBag bag = MarkOccurrencesHighlighter.getHighlightsBag(doc);
        HighlightsSequence hs = bag.getHighlights(0, doc.getLength());
        if (hs.moveNext()) {
            if (directionForward) {
                int firstStart = hs.getStartOffset();
                int firstEnd = hs.getEndOffset();
                while (hs.getStartOffset() <= curPos && hs.moveNext()) {
                }
                if (hs.getStartOffset() > curPos) {
                    return hs.getStartOffset();
                }
                if (firstEnd < curPos || firstStart > curPos) {
                    return firstStart;
                }
            } else {
                int last;
                int current = hs.getStartOffset();
                boolean stuck = false;
                do {
                    last = current;
                    current = hs.getStartOffset();
                } while (hs.getEndOffset() < curPos && (stuck = hs.moveNext()));
                if (last == current) {
                    while (hs.moveNext()) {
                    }
                    if (hs.getEndOffset() < curPos || hs.getStartOffset() > curPos) {
                        return hs.getStartOffset();
                    }
                } else {
                    if (stuck) {
                        return last;
                    }
                    return current;
                }
            }
        }
        return -1;
    }

    private static void navigateToOccurence(boolean next, JTextComponent txt) {
        if (txt != null && txt.getDocument() != null) {
            int position;
            Document doc = txt.getDocument();
            int goTo = GoToMarkOccurrencesAction.findOccurrencePosition(next, doc, position = txt.getCaretPosition());
            if (goTo > 0) {
                txt.setCaretPosition(goTo);
                doc.putProperty("marked-occurrence", new long[]{DocumentUtilities.getDocumentVersion((Document)doc), goTo});
            } else {
                StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(GoToMarkOccurrencesAction.class, (String)"java-no-marked-occurrence"));
                doc.putProperty("marked-occurrence", null);
            }
        }
    }
}

