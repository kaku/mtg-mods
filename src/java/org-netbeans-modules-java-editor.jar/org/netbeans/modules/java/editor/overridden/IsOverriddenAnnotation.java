/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.text.Annotation
 *  org.openide.text.NbDocument
 */
package org.netbeans.modules.java.editor.overridden;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.netbeans.modules.java.editor.overridden.AnnotationType;
import org.netbeans.modules.java.editor.overridden.ElementDescription;
import org.openide.text.Annotation;
import org.openide.text.NbDocument;

public class IsOverriddenAnnotation
extends Annotation {
    private final StyledDocument document;
    private final Position pos;
    private final String shortDescription;
    private final AnnotationType type;
    private final List<ElementDescription> declarations;

    public IsOverriddenAnnotation(StyledDocument document, Position pos, AnnotationType type, String shortDescription, List<ElementDescription> declarations) {
        assert (pos != null);
        this.document = document;
        this.pos = pos;
        this.type = type;
        this.shortDescription = shortDescription;
        this.declarations = declarations;
    }

    public String getShortDescription() {
        return this.shortDescription;
    }

    public String getAnnotationType() {
        switch (this.type) {
            case IS_OVERRIDDEN: {
                return "org-netbeans-modules-editor-annotations-is_overridden";
            }
            case HAS_IMPLEMENTATION: {
                return "org-netbeans-modules-editor-annotations-has_implementations";
            }
            case IMPLEMENTS: {
                return "org-netbeans-modules-editor-annotations-implements";
            }
            case OVERRIDES: {
                return "org-netbeans-modules-editor-annotations-overrides";
            }
        }
        throw new IllegalStateException("Currently not implemented: " + (Object)((Object)this.type));
    }

    public void attach() {
        NbDocument.addAnnotation((StyledDocument)this.document, (Position)this.pos, (int)-1, (Annotation)this);
    }

    public void detachImpl() {
        NbDocument.removeAnnotation((StyledDocument)this.document, (Annotation)this);
    }

    public String toString() {
        return "[IsOverriddenAnnotation: " + this.shortDescription + "]";
    }

    public Position getPosition() {
        return this.pos;
    }

    public String debugDump(boolean includePosition) {
        ArrayList<String> elementNames = new ArrayList<String>();
        for (ElementDescription desc : this.declarations) {
            elementNames.add(desc.getDisplayName());
        }
        Collections.sort(elementNames);
        return "IsOverriddenAnnotation: type=" + this.type.name() + ", elements:" + elementNames.toString() + (includePosition ? new StringBuilder().append(":").append(this.pos.getOffset()).toString() : "");
    }

    public AnnotationType getType() {
        return this.type;
    }

    public List<ElementDescription> getDeclarations() {
        return this.declarations;
    }

}

