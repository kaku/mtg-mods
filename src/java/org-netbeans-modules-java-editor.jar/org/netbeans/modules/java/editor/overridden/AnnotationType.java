/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.editor.overridden;

public enum AnnotationType {
    HAS_IMPLEMENTATION,
    IS_OVERRIDDEN,
    IMPLEMENTS,
    OVERRIDES;
    

    private AnnotationType() {
    }
}

