/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.support.CancellableTreePathScanner
 */
package org.netbeans.modules.java.editor.overridden;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.support.CancellableTreePathScanner;

class IsOverriddenVisitor
extends CancellableTreePathScanner<Void, Tree> {
    private CompilationInfo info;
    Map<ElementHandle<TypeElement>, List<ElementHandle<ExecutableElement>>> type2Declaration;
    Map<ElementHandle<ExecutableElement>, MethodTree> declaration2Tree;
    Map<ElementHandle<TypeElement>, ClassTree> declaration2Class;
    private Map<TypeElement, ElementHandle<TypeElement>> type2Handle;
    private ElementHandle<TypeElement> currentClass;

    IsOverriddenVisitor(CompilationInfo info, AtomicBoolean cancel) {
        super(cancel);
        this.info = info;
        this.type2Declaration = new HashMap<ElementHandle<TypeElement>, List<ElementHandle<ExecutableElement>>>();
        this.declaration2Tree = new HashMap<ElementHandle<ExecutableElement>, MethodTree>();
        this.declaration2Class = new HashMap<ElementHandle<TypeElement>, ClassTree>();
        this.type2Handle = new HashMap<TypeElement, ElementHandle<TypeElement>>();
    }

    private ElementHandle<TypeElement> getHandle(TypeElement type) {
        ElementHandle result = this.type2Handle.get(type);
        if (result == null) {
            result = ElementHandle.create((Element)type);
            this.type2Handle.put(type, (ElementHandle)result);
        }
        return result;
    }

    public Void visitMethod(MethodTree tree, Tree d) {
        Element el;
        if (this.currentClass != null && (el = this.info.getTrees().getElement(this.getCurrentPath())) != null && el.getKind() == ElementKind.METHOD && !el.getModifiers().contains((Object)Modifier.PRIVATE) && !el.getModifiers().contains((Object)Modifier.STATIC)) {
            ExecutableElement overridee = (ExecutableElement)el;
            List<ElementHandle<ExecutableElement>> methods = this.type2Declaration.get(this.currentClass);
            if (methods == null) {
                methods = new ArrayList<ElementHandle<ExecutableElement>>();
                this.type2Declaration.put(this.currentClass, methods);
            }
            ElementHandle methodHandle = ElementHandle.create((Element)overridee);
            methods.add((ElementHandle)methodHandle);
            this.declaration2Tree.put((ElementHandle)methodHandle, tree);
        }
        CancellableTreePathScanner.super.visitMethod(tree, (Object)tree);
        return null;
    }

    public Void visitClass(ClassTree tree, Tree d) {
        Element decl = this.info.getTrees().getElement(this.getCurrentPath());
        if (decl != null && (decl.getKind().isClass() || decl.getKind().isInterface())) {
            ElementHandle<TypeElement> oldCurrentClass = this.currentClass;
            this.currentClass = this.getHandle((TypeElement)decl);
            this.declaration2Class.put(this.currentClass, tree);
            CancellableTreePathScanner.super.visitClass(tree, (Object)d);
            this.currentClass = oldCurrentClass;
        } else {
            CancellableTreePathScanner.super.visitClass(tree, (Object)d);
        }
        return null;
    }
}

