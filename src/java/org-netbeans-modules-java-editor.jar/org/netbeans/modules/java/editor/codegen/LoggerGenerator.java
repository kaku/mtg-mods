/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodInvocationTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.source.CodeStyle
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.ModificationResult
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.java.source.TreeMaker
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.WorkingCopy
 *  org.netbeans.editor.Utilities
 *  org.netbeans.spi.editor.codegen.CodeGenerator
 *  org.netbeans.spi.editor.codegen.CodeGenerator$Factory
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.codegen;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.java.source.CodeStyle;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.TreeMaker;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.java.editor.codegen.GeneratorUtils;
import org.netbeans.modules.java.editor.codegen.ui.ElementNode;
import org.netbeans.spi.editor.codegen.CodeGenerator;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class LoggerGenerator
implements CodeGenerator {
    private JTextComponent component;
    private ElementNode.Description description;

    private LoggerGenerator(JTextComponent component, ElementNode.Description description) {
        this.component = component;
        this.description = description;
    }

    public String getDisplayName() {
        return NbBundle.getMessage(LoggerGenerator.class, (String)"LBL_logger");
    }

    public void invoke() {
        final int caretOffset = this.component.getCaretPosition();
        JavaSource js = JavaSource.forDocument((Document)this.component.getDocument());
        if (js != null) {
            try {
                ModificationResult mr = js.runModificationTask((Task)new Task<WorkingCopy>(){

                    public void run(WorkingCopy copy) throws IOException {
                        copy.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                        Element e = LoggerGenerator.this.description.getElementHandle().resolve((CompilationInfo)copy);
                        TreePath path = e != null ? copy.getTrees().getPath(e) : copy.getTreeUtilities().pathFor(caretOffset);
                        path = org.netbeans.modules.editor.java.Utilities.getPathElementOfKind(TreeUtilities.CLASS_TREE_KINDS, path);
                        if (path == null) {
                            String message = NbBundle.getMessage(LoggerGenerator.class, (String)"ERR_CannotFindOriginalClass");
                            Utilities.setStatusBoldText((JTextComponent)LoggerGenerator.this.component, (String)message);
                        } else {
                            List<String> names;
                            EnumSet<Modifier> mods;
                            ClassTree cls = (ClassTree)path.getLeaf();
                            CodeStyle cs = CodeStyle.getDefault((Document)LoggerGenerator.this.component.getDocument());
                            VariableTree var = LoggerGenerator.createLoggerField(copy.getTreeMaker(), cls, (names = org.netbeans.modules.editor.java.Utilities.varNamesSuggestions(null, ElementKind.FIELD, mods = EnumSet.of(Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL), "LOG", null, copy.getTypes(), copy.getElements(), e.getEnclosedElements(), cs)).size() > 0 ? names.get(0) : "LOG", mods);
                            copy.rewrite((Tree)cls, (Tree)GeneratorUtils.insertClassMembers(copy, cls, Collections.singletonList(var), caretOffset));
                        }
                    }
                });
                GeneratorUtils.guardedCommit(this.component, mr);
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
    }

    private static VariableTree createLoggerField(TreeMaker make, ClassTree cls, CharSequence name, Set<Modifier> mods) {
        ModifiersTree modifiers = make.Modifiers(mods, Collections.emptyList());
        List none = Collections.emptyList();
        IdentifierTree className = make.Identifier((CharSequence)cls.getSimpleName());
        MemberSelectTree classType = make.MemberSelect((ExpressionTree)className, (CharSequence)"class");
        MemberSelectTree getName = make.MemberSelect((ExpressionTree)classType, (CharSequence)"getName");
        MethodInvocationTree initClass = make.MethodInvocation(none, (ExpressionTree)getName, none);
        ExpressionTree logger = make.QualIdent(Logger.class.getName());
        MemberSelectTree getLogger = make.MemberSelect(logger, (CharSequence)"getLogger");
        MethodInvocationTree initField = make.MethodInvocation(none, (ExpressionTree)getLogger, Collections.nCopies(1, initClass));
        return make.Variable(modifiers, name, (Tree)logger, (ExpressionTree)initField);
    }

    public static class Factory
    implements CodeGenerator.Factory {
        private static final String ERROR = "<error>";

        public List<? extends CodeGenerator> create(Lookup context) {
            ArrayList<LoggerGenerator> ret = new ArrayList<LoggerGenerator>();
            JTextComponent component = (JTextComponent)context.lookup(JTextComponent.class);
            CompilationController controller = (CompilationController)context.lookup(CompilationController.class);
            TreePath path = (TreePath)context.lookup(TreePath.class);
            TreePath treePath = path = path != null ? org.netbeans.modules.editor.java.Utilities.getPathElementOfKind(TreeUtilities.CLASS_TREE_KINDS, path) : null;
            if (component == null || controller == null || path == null) {
                return ret;
            }
            try {
                controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
            }
            catch (IOException ioe) {
                return ret;
            }
            TypeElement typeElement = (TypeElement)controller.getTrees().getElement(path);
            if (typeElement == null || !typeElement.getKind().isClass()) {
                return ret;
            }
            for (VariableElement ve : ElementFilter.fieldsIn(typeElement.getEnclosedElements())) {
                TypeMirror type = ve.asType();
                if (type.getKind() != TypeKind.DECLARED || !((TypeElement)((DeclaredType)type).asElement()).getQualifiedName().contentEquals(Logger.class.getName())) continue;
                return ret;
            }
            ArrayList<ElementNode.Description> descriptions = new ArrayList<ElementNode.Description>();
            ret.add(new LoggerGenerator(component, ElementNode.Description.create((CompilationInfo)controller, typeElement, descriptions, false, false)));
            return ret;
        }
    }

}

