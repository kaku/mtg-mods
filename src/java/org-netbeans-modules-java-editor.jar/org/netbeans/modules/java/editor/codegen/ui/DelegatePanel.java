/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.source.ElementHandle
 *  org.openide.explorer.ExplorerManager
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.codegen.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import javax.accessibility.AccessibleContext;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.text.JTextComponent;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.modules.java.editor.codegen.DelegateMethodGenerator;
import org.netbeans.modules.java.editor.codegen.ui.ElementNode;
import org.netbeans.modules.java.editor.codegen.ui.ElementSelectorPanel;
import org.openide.explorer.ExplorerManager;
import org.openide.util.NbBundle;

public class DelegatePanel
extends JPanel
implements PropertyChangeListener {
    private JTextComponent component;
    private ElementHandle<TypeElement> handle;
    private ElementSelectorPanel delegateSelector;
    private ElementSelectorPanel methodSelector;
    public JLabel delegateLabel;
    public JPanel delegatePanel;
    public JSplitPane jSplitPane1;
    public JLabel methodLabel;
    public JPanel methodPanel;

    public DelegatePanel(JTextComponent component, ElementHandle<TypeElement> handle, ElementNode.Description description) {
        this.component = component;
        this.handle = handle;
        this.initComponents();
        this.delegateSelector = new ElementSelectorPanel(description, false);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 0, 0, 12);
        this.delegatePanel.add((Component)this.delegateSelector, gridBagConstraints);
        this.delegateSelector.getExplorerManager().addPropertyChangeListener((PropertyChangeListener)this);
        this.methodSelector = new ElementSelectorPanel(null, false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 12, 0, 0);
        this.methodPanel.add((Component)this.methodSelector, gridBagConstraints);
        this.delegateLabel.setText(NbBundle.getMessage(DelegateMethodGenerator.class, (String)"LBL_delegate_field_select"));
        this.delegateLabel.setLabelFor(this.delegateSelector);
        this.methodLabel.setText(NbBundle.getMessage(DelegateMethodGenerator.class, (String)"LBL_delegate_method_select"));
        this.methodLabel.setLabelFor(this.methodSelector);
        this.delegateSelector.doInitialExpansion(1);
        this.jSplitPane1.setDividerLocation(0.5);
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DelegateMethodGenerator.class, (String)"A11Y_Generate_Delegate"));
    }

    public ElementHandle<? extends Element> getDelegateField() {
        List<ElementHandle<? extends Element>> handles = this.delegateSelector.getTreeSelectedElements();
        return handles.size() == 1 ? handles.get(0) : null;
    }

    public List<ElementHandle<? extends Element>> getDelegateMethods() {
        return this.methodSelector.getSelectedElements();
    }

    private void initComponents() {
        this.jSplitPane1 = new JSplitPane();
        this.methodPanel = new JPanel();
        this.methodLabel = new JLabel();
        this.delegatePanel = new JPanel();
        this.delegateLabel = new JLabel();
        this.setFocusable(false);
        this.jSplitPane1.setBorder(null);
        this.jSplitPane1.setDividerSize(5);
        this.jSplitPane1.setResizeWeight(0.5);
        this.methodPanel.setLayout(new GridBagLayout());
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 12, 0, 0);
        this.methodPanel.add((Component)this.methodLabel, gridBagConstraints);
        this.jSplitPane1.setRightComponent(this.methodPanel);
        this.delegatePanel.setLayout(new GridBagLayout());
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 0, 0, 12);
        this.delegatePanel.add((Component)this.delegateLabel, gridBagConstraints);
        this.jSplitPane1.setLeftComponent(this.delegatePanel);
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jSplitPane1, -1, 652, 32767).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jSplitPane1, -1, 410, 32767)));
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("selectedNodes".equals(evt.getPropertyName())) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    ElementHandle<? extends Element> fieldHandle = DelegatePanel.this.getDelegateField();
                    DelegatePanel.this.methodSelector.setRootElement(DelegatePanel.this.handle == null || fieldHandle == null ? null : DelegateMethodGenerator.getAvailableMethods(DelegatePanel.this.component, DelegatePanel.this.handle, fieldHandle), false);
                    DelegatePanel.this.methodSelector.doInitialExpansion(-1);
                }
            });
        }
    }

}

