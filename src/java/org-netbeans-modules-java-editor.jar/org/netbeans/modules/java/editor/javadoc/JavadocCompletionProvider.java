/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.completion.CompletionProvider
 *  org.netbeans.spi.editor.completion.CompletionTask
 *  org.netbeans.spi.editor.completion.support.AsyncCompletionQuery
 *  org.netbeans.spi.editor.completion.support.AsyncCompletionTask
 */
package org.netbeans.modules.java.editor.javadoc;

import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.editor.java.Utilities;
import org.netbeans.modules.java.editor.javadoc.JavadocCompletionQuery;
import org.netbeans.modules.java.editor.javadoc.JavadocCompletionUtils;
import org.netbeans.spi.editor.completion.CompletionProvider;
import org.netbeans.spi.editor.completion.CompletionTask;
import org.netbeans.spi.editor.completion.support.AsyncCompletionQuery;
import org.netbeans.spi.editor.completion.support.AsyncCompletionTask;

public final class JavadocCompletionProvider
implements CompletionProvider {
    public CompletionTask createTask(int queryType, JTextComponent component) {
        AsyncCompletionTask task = null;
        if (queryType == 1 || queryType == 9) {
            task = new AsyncCompletionTask((AsyncCompletionQuery)new JavadocCompletionQuery(queryType), component);
        }
        return task;
    }

    public int getAutoQueryTypes(JTextComponent component, String typedText) {
        if (typedText != null && typedText.length() == 1 && Utilities.getJavadocCompletionAutoPopupTriggers().indexOf(typedText.charAt(0)) >= 0 && JavadocCompletionUtils.isJavadocContext(component.getDocument(), component.getCaretPosition())) {
            return 1;
        }
        return 0;
    }
}

