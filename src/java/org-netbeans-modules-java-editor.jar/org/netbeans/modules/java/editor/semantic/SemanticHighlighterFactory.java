/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.SchedulerTask
 *  org.netbeans.modules.parsing.spi.TaskFactory
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.java.editor.semantic;

import java.util.Collection;
import java.util.Collections;
import org.netbeans.modules.java.editor.semantic.SemanticHighlighter;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.SchedulerTask;
import org.netbeans.modules.parsing.spi.TaskFactory;
import org.openide.filesystems.FileObject;

public class SemanticHighlighterFactory
extends TaskFactory {
    public Collection<? extends SchedulerTask> create(Snapshot snapshot) {
        return Collections.singleton(new SemanticHighlighter(snapshot.getSource().getFileObject()));
    }
}

