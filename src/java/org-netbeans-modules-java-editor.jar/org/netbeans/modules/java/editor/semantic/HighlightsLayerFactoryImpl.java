/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.highlighting.HighlightsContainer
 *  org.netbeans.spi.editor.highlighting.HighlightsLayer
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory$Context
 *  org.netbeans.spi.editor.highlighting.ZOrder
 */
package org.netbeans.modules.java.editor.semantic;

import javax.swing.text.Document;
import org.netbeans.modules.java.editor.rename.InstantRenamePerformer;
import org.netbeans.modules.java.editor.semantic.LexerBasedHighlightLayer;
import org.netbeans.modules.java.editor.semantic.MarkOccurrencesHighlighter;
import org.netbeans.modules.java.editor.semantic.SemanticHighlighter;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsLayer;
import org.netbeans.spi.editor.highlighting.HighlightsLayerFactory;
import org.netbeans.spi.editor.highlighting.ZOrder;

public class HighlightsLayerFactoryImpl
implements HighlightsLayerFactory {
    public HighlightsLayer[] createLayers(HighlightsLayerFactory.Context context) {
        LexerBasedHighlightLayer semantic = LexerBasedHighlightLayer.getLayer(SemanticHighlighter.class, context.getDocument());
        semantic.clearColoringCache();
        return new HighlightsLayer[]{HighlightsLayer.create((String)(SemanticHighlighter.class.getName() + "-1"), (ZOrder)ZOrder.SYNTAX_RACK.forPosition(1000), (boolean)false, (HighlightsContainer)semantic), HighlightsLayer.create((String)(SemanticHighlighter.class.getName() + "-2"), (ZOrder)ZOrder.SYNTAX_RACK.forPosition(1500), (boolean)false, (HighlightsContainer)SemanticHighlighter.getImportHighlightsBag(context.getDocument())), HighlightsLayer.create((String)MarkOccurrencesHighlighter.class.getName(), (ZOrder)ZOrder.SHOW_OFF_RACK.forPosition(20), (boolean)true, (HighlightsContainer)MarkOccurrencesHighlighter.getHighlightsBag(context.getDocument())), HighlightsLayer.create((String)InstantRenamePerformer.class.getName(), (ZOrder)ZOrder.SHOW_OFF_RACK.forPosition(25), (boolean)true, (HighlightsContainer)InstantRenamePerformer.getHighlightsBag(context.getDocument()))};
    }
}

