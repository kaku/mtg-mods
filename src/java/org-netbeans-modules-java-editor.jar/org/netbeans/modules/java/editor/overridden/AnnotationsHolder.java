/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.EditorCookie$Observable
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 */
package org.netbeans.modules.java.editor.overridden;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import org.netbeans.modules.java.editor.overridden.IsOverriddenAnnotation;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;

public class AnnotationsHolder
implements PropertyChangeListener {
    private static final Logger LOGGER = Logger.getLogger(AnnotationsHolder.class.getName());
    private static final RequestProcessor WORKER = new RequestProcessor(AnnotationsHolder.class.getName(), 1, false, false);
    private static final Map<DataObject, AnnotationsHolder> file2Annotations = new HashMap<DataObject, AnnotationsHolder>();
    private final DataObject file;
    private final EditorCookie.Observable ec;
    private final List<IsOverriddenAnnotation> annotations;

    public static synchronized AnnotationsHolder get(FileObject file) {
        try {
            DataObject od = DataObject.find((FileObject)file);
            AnnotationsHolder a = file2Annotations.get((Object)od);
            if (a != null) {
                return a;
            }
            EditorCookie.Observable ec = (EditorCookie.Observable)od.getLookup().lookup(EditorCookie.Observable.class);
            if (ec == null) {
                return null;
            }
            a = new AnnotationsHolder(od, ec);
            file2Annotations.put(od, a);
            return a;
        }
        catch (IOException ex) {
            LOGGER.log(Level.INFO, null, ex);
            return null;
        }
    }

    private AnnotationsHolder(DataObject file, EditorCookie.Observable ec) {
        this.file = file;
        this.ec = ec;
        this.annotations = new ArrayList<IsOverriddenAnnotation>();
        ec.addPropertyChangeListener((PropertyChangeListener)this);
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                AnnotationsHolder.this.checkForReset();
            }
        });
        Logger.getLogger("TIMER").log(Level.FINE, "Overridden AnnotationsHolder", new Object[]{file.getPrimaryFile(), this});
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("openedPanes".endsWith(evt.getPropertyName()) || evt.getPropertyName() == null) {
            this.checkForReset();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private void checkForReset() {
        assert (SwingUtilities.isEventDispatchThread());
        if (this.ec.getOpenedPanes() != null) return;
        Class<AnnotationsHolder> class_ = AnnotationsHolder.class;
        synchronized (AnnotationsHolder.class) {
            file2Annotations.remove((Object)this.file);
            // ** MonitorExit[var1_1] (shouldn't be in output)
            this.setNewAnnotations(Collections.emptyList());
            this.ec.removePropertyChangeListener((PropertyChangeListener)this);
            return;
        }
    }

    public void setNewAnnotations(final List<IsOverriddenAnnotation> as) {
        Runnable doAttachDetach = new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                ArrayList toAdd;
                ArrayList toRemove;
                AnnotationsHolder annotationsHolder = AnnotationsHolder.this;
                synchronized (annotationsHolder) {
                    toRemove = new ArrayList(AnnotationsHolder.this.annotations);
                    toAdd = new ArrayList(as);
                    AnnotationsHolder.this.annotations.clear();
                    AnnotationsHolder.this.annotations.addAll(as);
                }
                for (IsOverriddenAnnotation a2 : toRemove) {
                    a2.detachImpl();
                }
                for (IsOverriddenAnnotation a2 : toAdd) {
                    a2.attach();
                }
            }
        };
        WORKER.submit(doAttachDetach);
    }

    public synchronized List<IsOverriddenAnnotation> getAnnotations() {
        return new ArrayList<IsOverriddenAnnotation>(this.annotations);
    }

}

