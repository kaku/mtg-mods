/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseAction
 */
package org.netbeans.modules.java.editor.rename;

import java.awt.event.ActionEvent;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.BaseAction;
import org.netbeans.modules.java.editor.rename.InstantRenamePerformer;

public class InstantRenameAction
extends BaseAction {
    public InstantRenameAction() {
        super("in-place-refactoring", 14);
    }

    public void actionPerformed(ActionEvent evt, JTextComponent target) {
        InstantRenamePerformer.invokeInstantRename(target);
    }

    protected Class getShortDescriptionBundleClass() {
        return InstantRenameAction.class;
    }
}

