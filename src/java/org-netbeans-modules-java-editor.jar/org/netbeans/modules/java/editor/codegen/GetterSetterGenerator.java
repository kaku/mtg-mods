/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.CodeStyle
 *  org.netbeans.api.java.source.CodeStyleUtils
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.ModificationResult
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.java.source.TreePathHandle
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.WorkingCopy
 *  org.netbeans.api.progress.ProgressUtils
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.refactoring.api.Problem
 *  org.netbeans.modules.refactoring.api.RefactoringSession
 *  org.netbeans.spi.editor.codegen.CodeGenerator
 *  org.netbeans.spi.editor.codegen.CodeGenerator$Factory
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.codegen;

import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.awt.Dialog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.swing.SwingUtilities;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CodeStyle;
import org.netbeans.api.java.source.CodeStyleUtils;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.TreePathHandle;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.java.editor.codegen.ConstructorGenerator;
import org.netbeans.modules.java.editor.codegen.GeneratorUtils;
import org.netbeans.modules.java.editor.codegen.ui.ElementNode;
import org.netbeans.modules.java.editor.codegen.ui.GetterSetterPanel;
import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.api.RefactoringSession;
import org.netbeans.spi.editor.codegen.CodeGenerator;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class GetterSetterGenerator
implements CodeGenerator {
    private JTextComponent component;
    private ElementNode.Description description;
    private int type;
    private CodeStyle codestyle;

    private GetterSetterGenerator(JTextComponent component, ElementNode.Description description, int type, CodeStyle codeStyle) {
        this.component = component;
        this.description = description;
        this.type = type;
        this.codestyle = codeStyle;
    }

    public String getDisplayName() {
        if (this.type == 1) {
            return NbBundle.getMessage(GetterSetterGenerator.class, (String)"LBL_getter");
        }
        if (this.type == 2) {
            return NbBundle.getMessage(GetterSetterGenerator.class, (String)"LBL_setter");
        }
        return NbBundle.getMessage(GetterSetterGenerator.class, (String)"LBL_getter_and_setter");
    }

    public void invoke() {
        final int caretOffset = this.component.getCaretPosition();
        final GetterSetterPanel panel = new GetterSetterPanel(this.description, this.type);
        String title = this.type == 1 ? NbBundle.getMessage(ConstructorGenerator.class, (String)"LBL_generate_getter") : (this.type == 2 ? NbBundle.getMessage(ConstructorGenerator.class, (String)"LBL_generate_setter") : NbBundle.getMessage(ConstructorGenerator.class, (String)"LBL_generate_getter_and_setter"));
        DialogDescriptor dialogDescriptor = GeneratorUtils.createDialogDescriptor(panel, title);
        Dialog dialog = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
        dialog.setVisible(true);
        if (dialogDescriptor.getValue() == dialogDescriptor.getDefaultValue()) {
            if (panel.isPerformEnsapsulate()) {
                this.performEncapsulate(panel.getVariables());
            } else {
                JavaSource js = JavaSource.forDocument((Document)this.component.getDocument());
                if (js != null) {
                    try {
                        ModificationResult mr = js.runModificationTask((Task)new Task<WorkingCopy>(){

                            public void run(WorkingCopy copy) throws IOException {
                                copy.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                                Element e = GetterSetterGenerator.this.description.getElementHandle().resolve((CompilationInfo)copy);
                                TreePath path = e != null ? copy.getTrees().getPath(e) : copy.getTreeUtilities().pathFor(caretOffset);
                                path = org.netbeans.modules.editor.java.Utilities.getPathElementOfKind(TreeUtilities.CLASS_TREE_KINDS, path);
                                if (path == null) {
                                    String message = NbBundle.getMessage(GetterSetterGenerator.class, (String)"ERR_CannotFindOriginalClass");
                                    Utilities.setStatusBoldText((JTextComponent)GetterSetterGenerator.this.component, (String)message);
                                } else {
                                    ArrayList<VariableElement> variableElements = new ArrayList<VariableElement>();
                                    for (ElementHandle<? extends Element> elementHandle : panel.getVariables()) {
                                        VariableElement elem = (VariableElement)elementHandle.resolve((CompilationInfo)copy);
                                        if (elem == null) {
                                            String message = NbBundle.getMessage(GetterSetterGenerator.class, (String)"ERR_CannotFindOriginalMember");
                                            Utilities.setStatusBoldText((JTextComponent)GetterSetterGenerator.this.component, (String)message);
                                            return;
                                        }
                                        variableElements.add(elem);
                                    }
                                    GeneratorUtils.generateGettersAndSetters(copy, path, variableElements, GetterSetterGenerator.this.type, caretOffset);
                                }
                            }
                        });
                        GeneratorUtils.guardedCommit(this.component, mr);
                    }
                    catch (IOException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
            }
        }
    }

    private void performEncapsulate(final List<ElementHandle<? extends Element>> variables) {
        try {
            JavaSource js = JavaSource.forDocument((Document)this.component.getDocument());
            final ArrayList getters = new ArrayList();
            final ArrayList setters = new ArrayList();
            final ArrayList handles = new ArrayList(variables.size());
            js.runUserActionTask((Task)new Task<CompilationController>(){

                public void run(CompilationController parameter) throws Exception {
                    GetterSetterGenerator.this.createGetterSetterLists(parameter, variables, handles, getters, setters, GetterSetterGenerator.this.codestyle);
                }
            }, true);
            ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

                @Override
                public void run() {
                    GetterSetterGenerator.this.doDefaultEncapsulate(handles, getters, setters);
                }
            }, (String)NbBundle.getMessage(GetterSetterGenerator.class, (String)"LBL_EncapsulateFields"), (AtomicBoolean)new AtomicBoolean(), (boolean)false);
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    private void createGetterSetterLists(CompilationController cc, List<ElementHandle<? extends Element>> variables, List<? super TreePathHandle> handles, List<String> getters, List<String> setters, CodeStyle codestyle) {
        for (ElementHandle<? extends Element> handle : variables) {
            Element el = handle.resolve((CompilationInfo)cc);
            handles.add((TreePathHandle)TreePathHandle.create((Element)el, (CompilationInfo)cc));
            boolean isStatic = el.getModifiers().contains((Object)Modifier.STATIC);
            if (this.type != 1) {
                setters.add(CodeStyleUtils.computeSetterName((CharSequence)el.getSimpleName(), (boolean)isStatic, (CodeStyle)codestyle));
            } else {
                setters.add(null);
            }
            if (this.type != 2) {
                getters.add(CodeStyleUtils.computeGetterName((CharSequence)el.getSimpleName(), (boolean)org.netbeans.modules.editor.java.Utilities.isBoolean(el.asType()), (boolean)isStatic, (CodeStyle)codestyle));
                continue;
            }
            getters.add(null);
        }
    }

    private void doDefaultEncapsulate(List<TreePathHandle> variables, List<String> getters, List<String> setters) {
        RefactoringSession encapsulate = RefactoringSession.create((String)NbBundle.getMessage(GetterSetterGenerator.class, (String)"LBL_EncapsulateFields"));
        Iterator<String> setIterator = setters.iterator();
        Iterator<String> getIterator = getters.iterator();
        ClasspathInfo cpinfo = ClasspathInfo.create((Document)this.component.getDocument());
        Iterator<TreePathHandle> it = variables.iterator();
        while (it.hasNext()) {
        }
        encapsulate.doRefactoring(true);
    }

    private void doFullEncapsulate() {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
            }
        });
    }

    public static class Factory
    implements CodeGenerator.Factory {
        private static final String ERROR = "<error>";

        public List<? extends CodeGenerator> create(Lookup context) {
            ArrayList<ElementNode.Description> descriptions;
            ArrayList<GetterSetterGenerator> ret = new ArrayList<GetterSetterGenerator>();
            JTextComponent component = (JTextComponent)context.lookup(JTextComponent.class);
            CodeStyle codeStyle = CodeStyle.getDefault((Document)component.getDocument());
            CompilationController controller = (CompilationController)context.lookup(CompilationController.class);
            TreePath path = (TreePath)context.lookup(TreePath.class);
            TreePath treePath = path = path != null ? org.netbeans.modules.editor.java.Utilities.getPathElementOfKind(TreeUtilities.CLASS_TREE_KINDS, path) : null;
            if (component == null || controller == null || path == null) {
                return ret;
            }
            try {
                controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
            }
            catch (IOException ioe) {
                return ret;
            }
            Elements elements = controller.getElements();
            TypeElement typeElement = (TypeElement)controller.getTrees().getElement(path);
            if (typeElement == null || !typeElement.getKind().isClass()) {
                return ret;
            }
            HashMap<String, List<ExecutableElement>> methods = new HashMap<String, List<ExecutableElement>>();
            for (ExecutableElement method : ElementFilter.methodsIn(elements.getAllMembers(typeElement))) {
                ArrayList<ExecutableElement> l = (ArrayList<ExecutableElement>)methods.get(method.getSimpleName().toString());
                if (l == null) {
                    l = new ArrayList<ExecutableElement>();
                    methods.put(method.getSimpleName().toString(), l);
                }
                l.add(method);
            }
            LinkedHashMap<Element, List> gDescriptions = new LinkedHashMap<Element, List>();
            LinkedHashMap<Element, List> sDescriptions = new LinkedHashMap<Element, List>();
            LinkedHashMap<Element, List> gsDescriptions = new LinkedHashMap<Element, List>();
            for (VariableElement variableElement : ElementFilter.fieldsIn(elements.getAllMembers(typeElement))) {
                List descriptions2;
                boolean hasSetter;
                if ("<error>".contentEquals(variableElement.getSimpleName())) continue;
                ElementNode.Description description = ElementNode.Description.create((CompilationInfo)controller, variableElement, null, true, false);
                boolean hasGetter = GeneratorUtils.hasGetter((CompilationInfo)controller, typeElement, variableElement, methods, codeStyle);
                boolean bl = hasSetter = variableElement.getModifiers().contains((Object)Modifier.FINAL) || GeneratorUtils.hasSetter((CompilationInfo)controller, typeElement, variableElement, methods, codeStyle);
                if (!hasGetter) {
                    descriptions2 = (ArrayList<ElementNode.Description>)gDescriptions.get(variableElement.getEnclosingElement());
                    if (descriptions2 == null) {
                        descriptions2 = new ArrayList<ElementNode.Description>();
                        gDescriptions.put(variableElement.getEnclosingElement(), descriptions2);
                    }
                    descriptions2.add(description);
                }
                if (!hasSetter) {
                    descriptions2 = (List)sDescriptions.get(variableElement.getEnclosingElement());
                    if (descriptions2 == null) {
                        descriptions2 = new ArrayList();
                        sDescriptions.put(variableElement.getEnclosingElement(), descriptions2);
                    }
                    descriptions2.add((ElementNode.Description)description);
                }
                if (hasGetter || hasSetter) continue;
                descriptions2 = (List)gsDescriptions.get(variableElement.getEnclosingElement());
                if (descriptions2 == null) {
                    descriptions2 = new ArrayList();
                    gsDescriptions.put(variableElement.getEnclosingElement(), descriptions2);
                }
                descriptions2.add(description);
            }
            if (!gDescriptions.isEmpty()) {
                descriptions = new ArrayList<ElementNode.Description>();
                for (Map.Entry entry : gDescriptions.entrySet()) {
                    descriptions.add(ElementNode.Description.create((CompilationInfo)controller, (Element)entry.getKey(), (List)entry.getValue(), false, false));
                }
                Collections.reverse(descriptions);
                ret.add(new GetterSetterGenerator(component, ElementNode.Description.create((CompilationInfo)controller, typeElement, descriptions, false, false), 1, codeStyle));
            }
            if (!sDescriptions.isEmpty()) {
                descriptions = new ArrayList();
                for (Map.Entry entry : sDescriptions.entrySet()) {
                    descriptions.add(ElementNode.Description.create((CompilationInfo)controller, (Element)entry.getKey(), (List)entry.getValue(), false, false));
                }
                Collections.reverse(descriptions);
                ret.add(new GetterSetterGenerator(component, ElementNode.Description.create((CompilationInfo)controller, typeElement, descriptions, false, false), 2, codeStyle));
            }
            if (!gsDescriptions.isEmpty()) {
                descriptions = new ArrayList();
                for (Map.Entry entry : gsDescriptions.entrySet()) {
                    descriptions.add(ElementNode.Description.create((CompilationInfo)controller, (Element)entry.getKey(), (List)entry.getValue(), false, false));
                }
                Collections.reverse(descriptions);
                ret.add(new GetterSetterGenerator(component, ElementNode.Description.create((CompilationInfo)controller, typeElement, descriptions, false, false), 0, codeStyle));
            }
            return ret;
        }
    }

}

