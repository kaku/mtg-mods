/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.api.java.source.ui.ElementIcons
 *  org.netbeans.api.java.source.ui.ElementOpen
 *  org.openide.filesystems.FileObject
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.overridden;

import java.awt.Image;
import java.awt.Toolkit;
import java.util.Collection;
import java.util.Set;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.swing.Icon;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.ui.ElementIcons;
import org.netbeans.api.java.source.ui.ElementOpen;
import org.netbeans.modules.editor.java.Utilities;
import org.openide.filesystems.FileObject;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class ElementDescription {
    private static final String PKG_COLOR = Utilities.getHTMLColor(192, 192, 192);
    private ClasspathInfo originalCPInfo;
    private ElementHandle<Element> handle;
    private ElementHandle<TypeElement> outtermostElement;
    private Collection<Modifier> modifiers;
    private String displayName;
    private final boolean overriddenFlag;

    public ElementDescription(CompilationInfo info, Element element, boolean overriddenFlag) {
        this.originalCPInfo = info.getClasspathInfo();
        this.handle = ElementHandle.create((Element)element);
        this.outtermostElement = ElementHandle.create((Element)SourceUtils.getOutermostEnclosingTypeElement((Element)element));
        this.modifiers = element.getModifiers();
        this.displayName = overriddenFlag ? ElementDescription.computeDisplayNameIsOverridden(element) : ElementDescription.computeDisplayNameOverrides(element);
        this.overriddenFlag = overriddenFlag;
    }

    private static String computeDisplayNameIsOverridden(Element element) throws IllegalStateException {
        TypeElement clazz;
        if (element.getKind().isClass() || element.getKind().isInterface()) {
            clazz = (TypeElement)element;
        } else {
            assert (element.getKind() == ElementKind.METHOD);
            clazz = (TypeElement)element.getEnclosingElement();
        }
        StringBuilder displayName = new StringBuilder();
        Element parent = clazz.getEnclosingElement();
        displayName.append("<html>");
        displayName.append(ElementDescription.computeSimpleName(clazz));
        while (ElementDescription.isAnonymous(parent) || parent.getKind() == ElementKind.CONSTRUCTOR || parent.getKind() == ElementKind.INSTANCE_INIT || parent.getKind() == ElementKind.METHOD || parent.getKind() == ElementKind.STATIC_INIT) {
            displayName.append(' ');
            displayName.append(NbBundle.getMessage(ElementDescription.class, (String)"NAME_In"));
            displayName.append(' ');
            displayName.append(ElementDescription.computeSimpleName(parent));
            parent = parent.getEnclosingElement();
        }
        displayName.append(' ');
        displayName.append(PKG_COLOR);
        displayName.append('(');
        displayName.append(ElementDescription.getQualifiedName(parent));
        displayName.append(')');
        return displayName.toString();
    }

    private static boolean isAnonymous(Element el) {
        if (!el.getKind().isClass()) {
            return false;
        }
        TypeElement clazz = (TypeElement)el;
        return clazz.getQualifiedName() == null || clazz.getQualifiedName().length() == 0 || clazz.getSimpleName() == null || clazz.getSimpleName().length() == 0;
    }

    private static String computeSimpleName(Element clazz) {
        if (ElementDescription.isAnonymous(clazz)) {
            return NbBundle.getMessage(ElementDescription.class, (String)"NAME_AnonynmousInner");
        }
        String simpleName = clazz.getSimpleName().toString();
        return simpleName;
    }

    private static CharSequence getQualifiedName(Element el) {
        if (el.getKind() == ElementKind.PACKAGE) {
            return ((PackageElement)el).getQualifiedName();
        }
        if (el.getKind().isClass() || el.getKind().isInterface()) {
            return ((TypeElement)el).getQualifiedName();
        }
        throw new IllegalStateException();
    }

    private static String computeDisplayNameOverrides(Element element) {
        return ((TypeElement)element.getEnclosingElement()).getQualifiedName().toString();
    }

    public FileObject getSourceFile() {
        FileObject file = SourceUtils.getFile(this.outtermostElement, (ClasspathInfo)this.originalCPInfo);
        if (file != null) {
            return SourceUtils.getFile(this.outtermostElement, (ClasspathInfo)ClasspathInfo.create((FileObject)file));
        }
        return null;
    }

    public void open() {
        if (!ElementOpen.open((ClasspathInfo)this.originalCPInfo, this.getHandle())) {
            Toolkit.getDefaultToolkit().beep();
        }
    }

    public ElementHandle<Element> getHandle() {
        return this.handle;
    }

    public Icon getIcon() {
        Image badge = this.overriddenFlag ? ImageUtilities.loadImage((String)"org/netbeans/modules/java/editor/resources/is-overridden-badge.png") : ImageUtilities.loadImage((String)"org/netbeans/modules/java/editor/resources/overrides-badge.png");
        Image icon = ImageUtilities.icon2Image((Icon)ElementIcons.getElementIcon((ElementKind)this.handle.getKind(), this.modifiers));
        return ImageUtilities.image2Icon((Image)ImageUtilities.mergeImages((Image)icon, (Image)badge, (int)16, (int)0));
    }

    public boolean isOverridden() {
        return this.overriddenFlag;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public Collection<Modifier> getModifiers() {
        return this.modifiers;
    }
}

