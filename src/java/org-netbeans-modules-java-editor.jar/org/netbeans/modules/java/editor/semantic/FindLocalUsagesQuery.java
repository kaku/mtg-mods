/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.MemberReferenceTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.NewClassTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.TypeParameterTree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.support.CancellableTreePathScanner
 *  org.netbeans.api.lexer.Token
 */
package org.netbeans.modules.java.editor.semantic;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.MemberReferenceTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TypeParameterTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.swing.text.Document;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.support.CancellableTreePathScanner;
import org.netbeans.api.lexer.Token;
import org.netbeans.modules.java.editor.javadoc.JavadocImports;
import org.netbeans.modules.java.editor.semantic.Utilities;

public class FindLocalUsagesQuery
extends CancellableTreePathScanner<Void, Stack<Tree>> {
    private CompilationInfo info;
    private Set<Token> usages;
    private Element toFind;
    private Document doc;
    private boolean instantRename;

    public FindLocalUsagesQuery() {
        this(false);
    }

    public FindLocalUsagesQuery(boolean instantRename) {
        this.instantRename = instantRename;
    }

    public Set<Token> findUsages(Element element, CompilationInfo info, Document doc) {
        this.info = info;
        this.usages = new HashSet<Token>();
        this.toFind = element;
        this.doc = doc;
        this.scan((Tree)info.getCompilationUnit(), (Object)null);
        return this.usages;
    }

    private void handlePotentialVariable(TreePath tree) {
        Token<JavaTokenId> t;
        Element el = this.info.getTrees().getElement(tree);
        if (this.toFind.equals(el) && (t = Utilities.getToken(this.info, this.doc, tree)) != null) {
            this.usages.add(t);
        }
    }

    private void handleJavadoc(TreePath el) {
        List<Token> tokens = JavadocImports.computeTokensOfReferencedElements(this.info, el, this.toFind);
        this.usages.addAll(tokens);
    }

    public Void visitIdentifier(IdentifierTree tree, Stack<Tree> d) {
        this.handlePotentialVariable(this.getCurrentPath());
        CancellableTreePathScanner.super.visitIdentifier(tree, d);
        return null;
    }

    public Void visitMemberReference(MemberReferenceTree node, Stack<Tree> p) {
        this.handlePotentialVariable(this.getCurrentPath());
        CancellableTreePathScanner.super.visitMemberReference(node, p);
        return null;
    }

    public Void visitMethod(MethodTree tree, Stack<Tree> d) {
        this.handlePotentialVariable(this.getCurrentPath());
        this.handleJavadoc(this.getCurrentPath());
        CancellableTreePathScanner.super.visitMethod(tree, d);
        return null;
    }

    public Void visitMemberSelect(MemberSelectTree node, Stack<Tree> p) {
        this.handlePotentialVariable(this.getCurrentPath());
        CancellableTreePathScanner.super.visitMemberSelect(node, p);
        return null;
    }

    public Void visitVariable(VariableTree tree, Stack<Tree> d) {
        this.handlePotentialVariable(this.getCurrentPath());
        Element el = this.info.getTrees().getElement(this.getCurrentPath());
        if (el != null && el.getKind().isField()) {
            this.handleJavadoc(this.getCurrentPath());
        }
        CancellableTreePathScanner.super.visitVariable(tree, d);
        return null;
    }

    public Void visitClass(ClassTree tree, Stack<Tree> d) {
        this.handlePotentialVariable(this.getCurrentPath());
        this.handleJavadoc(this.getCurrentPath());
        CancellableTreePathScanner.super.visitClass(tree, d);
        return null;
    }

    public Void visitTypeParameter(TypeParameterTree node, Stack<Tree> p) {
        this.handlePotentialVariable(this.getCurrentPath());
        CancellableTreePathScanner.super.visitTypeParameter(node, p);
        return null;
    }

    public Void visitNewClass(NewClassTree node, Stack<Tree> p) {
        if (this.instantRename) {
            return (Void)CancellableTreePathScanner.super.visitNewClass(node, p);
        }
        Element el = this.info.getTrees().getElement(this.getCurrentPath());
        if (this.toFind.equals(el) && node.getIdentifier() != null) {
            Token<JavaTokenId> t = Utilities.getToken(this.info, this.doc, new TreePath(this.getCurrentPath(), (Tree)node.getIdentifier()));
            if (t != null) {
                this.usages.add(t);
            }
            return null;
        }
        if (el != null && this.toFind.equals(el.getEnclosingElement())) {
            return null;
        }
        return (Void)CancellableTreePathScanner.super.visitNewClass(node, p);
    }

    public Void visitImport(ImportTree node, Stack<Tree> p) {
        Tree qualIdent;
        if (node.isStatic() && this.toFind.getModifiers().contains((Object)Modifier.STATIC) && (qualIdent = node.getQualifiedIdentifier()).getKind() == Tree.Kind.MEMBER_SELECT) {
            Token<JavaTokenId> t;
            Element el;
            MemberSelectTree mst = (MemberSelectTree)qualIdent;
            if (this.toFind.getSimpleName().contentEquals(mst.getIdentifier()) && (el = this.info.getTrees().getElement(new TreePath(this.getCurrentPath(), (Tree)mst.getExpression()))) != null && el.equals(this.toFind.getEnclosingElement()) && (t = Utilities.getToken(this.info, this.doc, new TreePath(this.getCurrentPath(), (Tree)mst))) != null) {
                this.usages.add(t);
            }
        }
        return (Void)CancellableTreePathScanner.super.visitImport(node, p);
    }
}

