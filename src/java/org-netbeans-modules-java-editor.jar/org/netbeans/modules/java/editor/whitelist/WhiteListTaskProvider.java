/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$PathConversionMode
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ProjectUtils
 *  org.netbeans.api.project.SourceGroup
 *  org.netbeans.api.whitelist.WhiteListQuery
 *  org.netbeans.api.whitelist.WhiteListQuery$Result
 *  org.netbeans.api.whitelist.WhiteListQuery$RuleDescription
 *  org.netbeans.api.whitelist.index.WhiteListIndex
 *  org.netbeans.api.whitelist.index.WhiteListIndex$Problem
 *  org.netbeans.api.whitelist.index.WhiteListIndexEvent
 *  org.netbeans.api.whitelist.index.WhiteListIndexListener
 *  org.netbeans.spi.tasklist.PushTaskScanner
 *  org.netbeans.spi.tasklist.PushTaskScanner$Callback
 *  org.netbeans.spi.tasklist.Task
 *  org.netbeans.spi.tasklist.TaskScanningScope
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 *  org.openide.util.TaskListener
 *  org.openide.util.WeakSet
 */
package org.netbeans.modules.java.editor.whitelist;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.SourceGroup;
import org.netbeans.api.whitelist.WhiteListQuery;
import org.netbeans.api.whitelist.index.WhiteListIndex;
import org.netbeans.api.whitelist.index.WhiteListIndexEvent;
import org.netbeans.api.whitelist.index.WhiteListIndexListener;
import org.netbeans.modules.java.editor.whitelist.Bundle;
import org.netbeans.spi.tasklist.PushTaskScanner;
import org.netbeans.spi.tasklist.Task;
import org.netbeans.spi.tasklist.TaskScanningScope;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;
import org.openide.util.TaskListener;
import org.openide.util.WeakSet;

public class WhiteListTaskProvider
extends PushTaskScanner {
    private static final RequestProcessor WORKER = new RequestProcessor(WhiteListTaskProvider.class);
    private static final Logger LOG = Logger.getLogger(WhiteListTaskProvider.class.getName());
    private static final Map<RequestProcessor.Task, Work> TASKS = new HashMap<RequestProcessor.Task, Work>();
    private static final Map<FileObject, Set<FileObject>> root2FilesWithAttachedErrors = new WeakHashMap<FileObject, Set<FileObject>>();
    private static boolean clearing;
    private PushTaskScanner.Callback currentCallback;
    private Set<FileObject> currentFiles;

    public WhiteListTaskProvider() {
        super(Bundle.LBL_ProviderName(), Bundle.LBL_ProviderDescription(), null);
        WhiteListIndex.getDefault().addWhiteListIndexListener(new WhiteListIndexListener(){

            public void indexChanged(WhiteListIndexEvent event) {
                WhiteListTaskProvider.this.refresh(event.getRoot());
            }
        });
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setScope(TaskScanningScope scope, PushTaskScanner.Callback callback) {
        WhiteListTaskProvider.cancelAllCurrent();
        if (scope == null || callback == null) {
            return;
        }
        WeakSet files = new WeakSet();
        for (FileObject file : scope.getLookup().lookupAll(FileObject.class)) {
            files.add(file);
        }
        for (Project p : scope.getLookup().lookupAll(Project.class)) {
            for (SourceGroup javaSG : ProjectUtils.getSources((Project)p).getSourceGroups("java")) {
                files.add(javaSG.getRootFolder());
            }
        }
        for (FileObject fo : files) {
            WhiteListTaskProvider.enqueue(new Work(fo, callback));
        }
        WhiteListTaskProvider i$ = this;
        synchronized (i$) {
            this.currentFiles = files;
            this.currentCallback = callback;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void refresh(@NonNull URL root) {
        PushTaskScanner.Callback callback;
        Set<FileObject> files;
        assert (root != null);
        FileObject rootFo = URLMapper.findFileObject((URL)root);
        WhiteListTaskProvider whiteListTaskProvider = this;
        synchronized (whiteListTaskProvider) {
            files = this.currentFiles;
            callback = this.currentCallback;
        }
        if (rootFo != null && files != null && files.contains((Object)rootFo)) {
            assert (callback != null);
            WhiteListTaskProvider.enqueue(new Work(rootFo, this.currentCallback));
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void enqueue(Work w) {
        Map<RequestProcessor.Task, Work> map = TASKS;
        synchronized (map) {
            RequestProcessor.Task task = WORKER.post((Runnable)w);
            TASKS.put(task, w);
            task.addTaskListener(new TaskListener(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                public void taskFinished(org.openide.util.Task task) {
                    Map map = TASKS;
                    synchronized (map) {
                        if (!clearing) {
                            TASKS.remove((Object)task);
                        }
                    }
                }
            });
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void cancelAllCurrent() {
        Map map = TASKS;
        synchronized (map) {
            clearing = true;
            try {
                Iterator<Map.Entry<RequestProcessor.Task, Work>> it = TASKS.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry<RequestProcessor.Task, Work> t = it.next();
                    t.getKey().cancel();
                    t.getValue().cancel();
                    it.remove();
                }
            }
            finally {
                clearing = false;
            }
        }
        map = root2FilesWithAttachedErrors;
        synchronized (map) {
            root2FilesWithAttachedErrors.clear();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static Set<FileObject> getFilesWithAttachedErrors(FileObject root) {
        Map<FileObject, Set<FileObject>> map = root2FilesWithAttachedErrors;
        synchronized (map) {
            WeakSet result = root2FilesWithAttachedErrors.get((Object)root);
            if (result == null) {
                result = new WeakSet();
                root2FilesWithAttachedErrors.put(root, (Set<FileObject>)result);
            }
            return result;
        }
    }

    @CheckForNull
    private static Map.Entry<FileObject, List<? extends Task>> createTask(@NonNull WhiteListIndex.Problem problem) {
        final FileObject file = problem.getFile();
        if (file == null) {
            return null;
        }
        WhiteListQuery.Result result = problem.getResult();
        assert (result != null);
        assert (!result.isAllowed());
        final ArrayList<Task> tasks = new ArrayList<Task>(result.getViolatedRules().size());
        for (WhiteListQuery.RuleDescription ruleDesc : result.getViolatedRules()) {
            tasks.add(Task.create((FileObject)file, (String)"nb-whitelist-warning", (String)ruleDesc.getRuleDescription(), (int)problem.getLine()));
        }
        return new Map.Entry<FileObject, List<? extends Task>>(){

            @Override
            public FileObject getKey() {
                return file;
            }

            @Override
            public List<? extends Task> getValue() {
                return tasks;
            }

            @Override
            public List<? extends Task> setValue(List<? extends Task> value) {
                throw new UnsupportedOperationException();
            }
        };
    }

    private static void updateErrorsInRoot(@NonNull PushTaskScanner.Callback callback, @NonNull FileObject root, @NonNull AtomicBoolean canceled) {
        Set<FileObject> filesWithErrors = WhiteListTaskProvider.getFilesWithAttachedErrors(root);
        HashSet<FileObject> fixedFiles = new HashSet<FileObject>(filesWithErrors);
        filesWithErrors.clear();
        HashSet nueFilesWithErrors = new HashSet();
        HashMap filesToTasks = new HashMap();
        for (WhiteListIndex.Problem problem : WhiteListIndex.getDefault().getWhiteListViolations(root, null, new String[0])) {
            if (canceled.get()) {
                return;
            }
            Map.Entry<FileObject, List<? extends Task>> task = WhiteListTaskProvider.createTask(problem);
            if (task == null) continue;
            ArrayList tasks = (ArrayList)filesToTasks.get((Object)task.getKey());
            if (tasks == null) {
                tasks = new ArrayList();
                filesToTasks.put(task.getKey(), tasks);
            }
            tasks.addAll(task.getValue());
        }
        for (Map.Entry e : filesToTasks.entrySet()) {
            LOG.log(Level.FINE, "Setting {1} for {0}\n", new Object[]{e.getKey(), e.getValue()});
            callback.setTasks((FileObject)e.getKey(), (List)e.getValue());
            if (fixedFiles.remove(e.getKey())) continue;
            nueFilesWithErrors.add(e.getKey());
        }
        for (FileObject f : fixedFiles) {
            LOG.log(Level.FINE, "Clearing errors for {0}", (Object)f);
            callback.setTasks(f, Collections.emptyList());
        }
        filesWithErrors.addAll(nueFilesWithErrors);
    }

    private static void updateErrorsInFile(@NonNull PushTaskScanner.Callback callback, @NonNull FileObject root, @NonNull FileObject file) {
        ArrayList tasks = new ArrayList();
        for (WhiteListIndex.Problem problem : WhiteListIndex.getDefault().getWhiteListViolations(root, file, new String[0])) {
            Map.Entry<FileObject, List<? extends Task>> task = WhiteListTaskProvider.createTask(problem);
            if (task == null) continue;
            tasks.addAll(task.getValue());
        }
        Set<FileObject> filesWithErrors = WhiteListTaskProvider.getFilesWithAttachedErrors(root);
        if (tasks.isEmpty()) {
            filesWithErrors.remove((Object)file);
        } else {
            filesWithErrors.add(file);
        }
        LOG.log(Level.FINE, "setting {1} for {0}", new Object[]{file, tasks});
        callback.setTasks(file, tasks);
    }

    private static final class Work
    implements Runnable {
        private final FileObject fileOrRoot;
        private final PushTaskScanner.Callback callback;
        private final AtomicBoolean canceled;

        public Work(FileObject fileOrRoot, PushTaskScanner.Callback callback) {
            this.fileOrRoot = fileOrRoot;
            this.callback = callback;
            this.canceled = new AtomicBoolean();
        }

        @Override
        public void run() {
            LOG.log(Level.FINE, "dequeued work for: {0}", (Object)this.fileOrRoot);
            ClassPath cp = ClassPath.getClassPath((FileObject)this.fileOrRoot, (String)"classpath/source");
            if (cp == null) {
                LOG.log(Level.FINE, "cp == null");
                return;
            }
            FileObject root = cp.findOwnerRoot(this.fileOrRoot);
            if (root == null) {
                Project p = FileOwnerQuery.getOwner((FileObject)this.fileOrRoot);
                Object[] arrobject = new Object[3];
                arrobject[0] = FileUtil.getFileDisplayName((FileObject)this.fileOrRoot);
                arrobject[1] = cp.toString(ClassPath.PathConversionMode.PRINT);
                arrobject[2] = p != null ? p.getClass() : "null";
                LOG.log(Level.WARNING, "file: {0} is not on its own source classpath: {1}, project: {2}", arrobject);
                return;
            }
            if (this.fileOrRoot.isData()) {
                WhiteListTaskProvider.updateErrorsInFile(this.callback, root, this.fileOrRoot);
            } else {
                WhiteListTaskProvider.updateErrorsInRoot(this.callback, root, this.canceled);
            }
        }

        private void cancel() {
            this.canceled.set(true);
        }
    }

}

