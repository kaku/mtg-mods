/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.editor.semantic;

import java.util.Collection;
import java.util.EnumSet;
import java.util.Iterator;

public enum ColoringAttributes {
    UNUSED,
    ABSTRACT,
    FIELD,
    LOCAL_VARIABLE,
    PARAMETER,
    METHOD,
    CONSTRUCTOR,
    CLASS,
    INTERFACE,
    ANNOTATION_TYPE,
    ENUM,
    DEPRECATED,
    STATIC,
    DECLARATION,
    PRIVATE,
    PACKAGE_PRIVATE,
    PROTECTED,
    PUBLIC,
    TYPE_PARAMETER_DECLARATION,
    TYPE_PARAMETER_USE,
    UNDEFINED,
    MARK_OCCURRENCES,
    JAVADOC_IDENTIFIER;
    

    private ColoringAttributes() {
    }

    public static Coloring empty() {
        return new Coloring();
    }

    public static Coloring add(Coloring c, ColoringAttributes a) {
        Coloring ci = new Coloring();
        ci.value = c.value | 1 << a.ordinal();
        return ci;
    }

    public static final class Coloring
    implements Collection<ColoringAttributes> {
        private int value;

        private Coloring() {
        }

        @Override
        public int size() {
            return Integer.bitCount(this.value);
        }

        @Override
        public boolean isEmpty() {
            return this.value == 0;
        }

        @Override
        public boolean contains(Object o) {
            if (o instanceof ColoringAttributes) {
                return (this.value & 1 << ((ColoringAttributes)((Object)o)).ordinal()) != 0;
            }
            return false;
        }

        @Override
        public Iterator<ColoringAttributes> iterator() {
            EnumSet<ColoringAttributes> s = EnumSet.noneOf(ColoringAttributes.class);
            for (ColoringAttributes c : ColoringAttributes.values()) {
                if (!this.contains((Object)c)) continue;
                s.add(c);
            }
            return s.iterator();
        }

        @Override
        public Object[] toArray() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public <T> T[] toArray(T[] a) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean add(ColoringAttributes o) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean remove(Object o) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            for (Object o : c) {
                if (this.contains(o)) continue;
                return false;
            }
            return true;
        }

        @Override
        public boolean addAll(Collection<? extends ColoringAttributes> c) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void clear() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public int hashCode() {
            return this.value;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Coloring) {
                return ((Coloring)obj).value == this.value;
            }
            return false;
        }
    }

}

