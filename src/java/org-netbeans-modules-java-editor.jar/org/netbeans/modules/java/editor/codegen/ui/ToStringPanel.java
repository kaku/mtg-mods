/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.source.ElementHandle
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.codegen.ui;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.util.List;
import javax.accessibility.AccessibleContext;
import javax.lang.model.element.Element;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.modules.java.editor.codegen.ToStringGenerator;
import org.netbeans.modules.java.editor.codegen.ui.ElementNode;
import org.netbeans.modules.java.editor.codegen.ui.ElementSelectorPanel;
import org.openide.util.NbBundle;

public class ToStringPanel
extends JPanel {
    private ElementSelectorPanel elementSelector;
    private JLabel selectorLabel;

    public ToStringPanel(ElementNode.Description description) {
        this.initComponents();
        this.elementSelector = new ElementSelectorPanel(description, false);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 12, 0, 12);
        this.add((Component)this.elementSelector, gridBagConstraints);
        this.selectorLabel.setText(NbBundle.getMessage(ToStringGenerator.class, (String)"LBL_tostring_select"));
        this.selectorLabel.setLabelFor(this.elementSelector);
        this.elementSelector.doInitialExpansion(1);
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ToStringGenerator.class, (String)"A11Y_Generate_ToString"));
    }

    public List<ElementHandle<? extends Element>> getVariables() {
        return this.elementSelector.getSelectedElements();
    }

    private void initComponents() {
        this.selectorLabel = new JLabel();
        this.setLayout(new GridBagLayout());
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(12, 12, 6, 12);
        this.add((Component)this.selectorLabel, gridBagConstraints);
    }
}

