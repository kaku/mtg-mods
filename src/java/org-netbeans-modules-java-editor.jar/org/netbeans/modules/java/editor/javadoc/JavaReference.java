/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.javadoc.ClassDoc
 *  com.sun.javadoc.ConstructorDoc
 *  com.sun.javadoc.Doc
 *  com.sun.javadoc.FieldDoc
 *  com.sun.javadoc.MemberDoc
 *  com.sun.javadoc.MethodDoc
 *  org.netbeans.api.java.lexer.JavadocTokenId
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementUtilities
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.java.editor.javadoc;

import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.ConstructorDoc;
import com.sun.javadoc.Doc;
import com.sun.javadoc.FieldDoc;
import com.sun.javadoc.MemberDoc;
import com.sun.javadoc.MethodDoc;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import org.netbeans.api.java.lexer.JavadocTokenId;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.openide.util.Exceptions;

public final class JavaReference {
    CharSequence fqn;
    CharSequence member;
    CharSequence tag;
    List<JavaReference> parameters;
    int begin = -1;
    int end = -1;
    private int tagEndPosition;
    private String paramsText;

    private JavaReference() {
    }

    public String toString() {
        return String.format("fqn: %1$s, member: %2$s, [%3$d, %4$d]", this.fqn, this.member, this.begin, this.end);
    }

    public static JavaReference resolve(TokenSequence<JavadocTokenId> jdts, int offset, int tagEndPosition) {
        JavaReference ref = new JavaReference();
        ref.tagEndPosition = tagEndPosition;
        jdts.move(offset);
        ref.insideFQN(jdts);
        return ref;
    }

    public List<JavaReference> getAllReferences() {
        if (this.parameters == null) {
            return Collections.singletonList(this);
        }
        ArrayList<JavaReference> references = new ArrayList<JavaReference>();
        references.add(this);
        references.addAll(this.parameters);
        return references;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    public Element getReferencedElement(CompilationInfo javac, TypeElement scope) {
        if (!this.isReference()) {
            return null;
        }
        result = null;
        declaredElement = null;
        if (this.fqn == null || this.fqn.length() <= 0) ** GOTO lbl18
        type = javac.getTreeUtilities().parseType(this.fqn.toString(), scope);
        if (type == null) return null;
        switch (1.$SwitchMap$javax$lang$model$type$TypeKind[type.getKind().ordinal()]) {
            case 1: 
            case 2: {
                declaredElement = (TypeElement)((DeclaredType)type).asElement();
                result = declaredElement;
                ** GOTO lbl19
            }
            case 3: {
                result = ((TypeVariable)type).asElement();
                ** GOTO lbl19
            }
            default: {
                return null;
            }
        }
lbl18: // 1 sources:
        declaredElement = scope;
lbl19: // 3 sources:
        if (declaredElement == null) return result;
        if (this.member == null) return result;
        if (this.member.length() <= 0) return result;
        memName = this.member.toString();
        referencedClass = (ClassDoc)javac.getElementUtilities().javaDocFor((Element)declaredElement);
        if (this.paramsText != null) {
            paramarr = new ParameterParseMachine(this.paramsText).parseParameters();
            if (paramarr == null) return result;
            referencedMember = this.findExecutableMember(memName, paramarr, referencedClass);
        } else {
            referencedMember = this.findExecutableMember(memName, null, referencedClass);
            fd = this.findField(referencedClass, memName);
            if (referencedMember == null || fd != null && fd.containingClass().subclassOf(referencedMember.containingClass())) {
                referencedMember = fd;
            }
        }
        if (referencedMember == null) return result;
        return javac.getElementUtilities().elementFor((Doc)referencedMember);
    }

    boolean isReference() {
        return this.begin > 0;
    }

    private void insideMember(TokenSequence<JavadocTokenId> jdts) {
        Token token;
        if (!jdts.moveNext() || JavadocTokenId.IDENT != (token = jdts.token()).id()) {
            return;
        }
        this.member = token.text();
        this.end = jdts.offset() + token.length();
        if (!jdts.moveNext()) {
            return;
        }
        token = jdts.token();
        if (JavadocTokenId.OTHER_TEXT != token.id()) {
            return;
        }
        CharSequence cs = token.text();
        if (cs.length() == 0 || cs.charAt(0) != '(') {
            return;
        }
        StringBuilder params = new StringBuilder();
        while (jdts.offset() < this.tagEndPosition) {
            int len = this.tagEndPosition - jdts.offset();
            CharSequence charSequence = cs = len > 0 ? token.text() : token.text().subSequence(0, len);
            if (token.id() == JavadocTokenId.IDENT) {
                JavaReference parameter = JavaReference.resolve(jdts, jdts.offset(), jdts.offset() + len);
                if (this.parameters == null) {
                    this.parameters = new ArrayList<JavaReference>();
                }
                this.parameters.add(parameter);
                if (parameter.fqn != null) {
                    params.append(parameter.fqn);
                } else {
                    params.append(cs);
                }
            } else {
                params.append(cs);
            }
            if (params.indexOf(")") > 0 || !jdts.moveNext()) break;
            token = jdts.token();
        }
        this.paramsText = this.parseParamString(params);
    }

    private void insideFQN(TokenSequence<JavadocTokenId> tokenSequence) {
        StringBuilder sb = new StringBuilder();
        block5 : while (tokenSequence.moveNext()) {
            Token token = tokenSequence.token();
            switch ((JavadocTokenId)token.id()) {
                case IDENT: {
                    sb.append(token.text());
                    if (this.begin < 0) {
                        this.begin = tokenSequence.offset();
                    }
                    this.end = tokenSequence.offset() + token.length();
                    continue block5;
                }
                case HASH: {
                    if (this.begin < 0) {
                        this.begin = tokenSequence.offset();
                    }
                    this.end = tokenSequence.offset() + token.length();
                    this.insideMember(tokenSequence);
                    break;
                }
                case DOT: {
                    if (sb.length() == 0) break block5;
                    if ('.' == sb.charAt(sb.length() - 1)) break;
                    sb.append('.');
                    this.end = tokenSequence.offset() + token.length();
                    continue block5;
                }
                default: {
                    tokenSequence.movePrevious();
                    break;
                }
            }
            break;
        }
        if (sb.length() > 0) {
            this.fqn = sb;
        }
    }

    private String parseParamString(CharSequence text) {
        int cp;
        int start;
        int len = text.length();
        if (len == 0 || text.charAt(0) != '(') {
            return null;
        }
        int parens = 0;
        int commentstart = 0;
        block7 : for (int i = start = 0; i < len; i += Character.charCount((int)cp)) {
            cp = Character.codePointAt(text, i);
            switch (cp) {
                case 40: {
                    ++parens;
                    continue block7;
                }
                case 41: {
                    --parens;
                    continue block7;
                }
                case 35: 
                case 46: 
                case 91: 
                case 93: {
                    continue block7;
                }
                case 44: {
                    if (parens > 0) continue block7;
                    return null;
                }
                case 9: 
                case 10: 
                case 32: {
                    if (parens != 0) continue block7;
                    commentstart = i;
                    i = len;
                    break;
                }
            }
        }
        if (parens != 0) {
            return null;
        }
        String params = commentstart > 0 ? text.subSequence(start, commentstart).toString() : text.toString();
        return params;
    }

    private MemberDoc findReferencedMethod(String memName, String[] paramarr, ClassDoc referencedClass) {
        MemberDoc meth = this.findExecutableMember(memName, paramarr, referencedClass);
        ClassDoc[] nestedclasses = referencedClass.innerClasses();
        if (meth == null) {
            for (int i = 0; i < nestedclasses.length; ++i) {
                meth = this.findReferencedMethod(memName, paramarr, nestedclasses[i]);
                if (meth == null) continue;
                return meth;
            }
        }
        return null;
    }

    private MemberDoc findExecutableMember(String memName, String[] paramarr, ClassDoc referencedClass) {
        if (memName.equals(referencedClass.name())) {
            return JavaReference.findConstructor(referencedClass, memName, paramarr);
        }
        return JavaReference.findMethod(referencedClass, memName, paramarr);
    }

    private static MethodDoc findMethod(ClassDoc clazz, String methodName, String[] paramTypes) {
        try {
            Method findMethod = clazz.getClass().getMethod("findMethod", String.class, String[].class);
            Object result = findMethod.invoke((Object)clazz, methodName, paramTypes);
            return result instanceof MethodDoc ? (MethodDoc)result : null;
        }
        catch (IllegalAccessException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (IllegalArgumentException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (InvocationTargetException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (NoSuchMethodException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (SecurityException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        return null;
    }

    private static ConstructorDoc findConstructor(ClassDoc clazz, String methodName, String[] paramTypes) {
        try {
            Method findConstructor = clazz.getClass().getMethod("findConstructor", String.class, String[].class);
            Object result = findConstructor.invoke((Object)clazz, methodName, paramTypes);
            return result instanceof ConstructorDoc ? (ConstructorDoc)result : null;
        }
        catch (IllegalAccessException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (IllegalArgumentException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (InvocationTargetException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (NoSuchMethodException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (SecurityException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        return null;
    }

    private FieldDoc findField(ClassDoc clazz, String fieldName) {
        try {
            Method findField = clazz.getClass().getMethod("findField", String.class);
            Object result = findField.invoke((Object)clazz, fieldName);
            return result instanceof FieldDoc ? (FieldDoc)result : null;
        }
        catch (IllegalAccessException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (IllegalArgumentException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (InvocationTargetException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (NoSuchMethodException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (SecurityException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        return null;
    }

    private static final class ParameterParseMachine {
        final int START = 0;
        final int TYPE = 1;
        final int NAME = 2;
        final int TNSPACE = 3;
        final int ARRAYDECORATION = 4;
        final int ARRAYSPACE = 5;
        String parameters;
        StringBuilder typeId;
        List<String> paramList;

        ParameterParseMachine(String parameters) {
            this.parameters = parameters;
            this.paramList = new ArrayList<String>();
            this.typeId = new StringBuilder();
        }

        public String[] parseParameters() {
            int cp;
            if (this.parameters.equals("()")) {
                return new String[0];
            }
            int state = 0;
            int prevstate = 0;
            this.parameters = this.parameters.substring(1, this.parameters.length() - 1);
            block7 : for (int index = 0; index < this.parameters.length(); index += Character.charCount((int)cp)) {
                cp = this.parameters.codePointAt(index);
                switch (state) {
                    case 0: {
                        if (Character.isJavaIdentifierStart(cp)) {
                            this.typeId.append(Character.toChars(cp));
                            state = 1;
                        }
                        prevstate = 0;
                        continue block7;
                    }
                    case 1: {
                        if (Character.isJavaIdentifierPart(cp) || cp == 46) {
                            this.typeId.append(Character.toChars(cp));
                        } else if (cp == 91) {
                            this.typeId.append('[');
                            state = 4;
                        } else if (Character.isWhitespace(cp)) {
                            state = 3;
                        } else if (cp == 44) {
                            this.addTypeToParamList();
                            state = 0;
                        }
                        prevstate = 1;
                        continue block7;
                    }
                    case 3: {
                        if (Character.isJavaIdentifierStart(cp)) {
                            if (prevstate == 4) {
                                return null;
                            }
                            this.addTypeToParamList();
                            state = 2;
                        } else if (cp == 91) {
                            this.typeId.append('[');
                            state = 4;
                        } else if (cp == 44) {
                            this.addTypeToParamList();
                            state = 0;
                        }
                        prevstate = 3;
                        continue block7;
                    }
                    case 4: {
                        if (cp == 93) {
                            this.typeId.append(']');
                            state = 3;
                        } else if (!Character.isWhitespace(cp)) {
                            return null;
                        }
                        prevstate = 4;
                        continue block7;
                    }
                    case 2: {
                        if (cp == 44) {
                            state = 0;
                        }
                        prevstate = 2;
                    }
                }
            }
            if (state == 4 || state != 0 || prevstate == 3) {
                // empty if block
            }
            if (this.typeId.length() > 0) {
                this.paramList.add(this.typeId.toString());
            }
            return this.paramList.toArray(new String[this.paramList.size()]);
        }

        void addTypeToParamList() {
            if (this.typeId.length() > 0) {
                this.paramList.add(this.typeId.toString());
                this.typeId.setLength(0);
            }
        }
    }

}

