/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.util.TreePath
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.api.java.source.GeneratorUtilities
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.ModificationResult
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.java.source.TreeMaker
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.WorkingCopy
 *  org.netbeans.api.java.source.ui.ElementIcons
 *  org.netbeans.api.progress.ProgressUtils
 *  org.netbeans.editor.Utilities
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.editor.imports;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.TreePath;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.accessibility.AccessibleContext;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.QualifiedNameable;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.border.Border;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.java.source.GeneratorUtilities;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.TreeMaker;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.api.java.source.ui.ElementIcons;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.java.editor.codegen.GeneratorUtils;
import org.netbeans.modules.java.editor.imports.JavaFixAllImports;
import org.netbeans.modules.java.editor.overridden.PopupUtil;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class ImportClassPanel
extends JPanel {
    private JavaSource javaSource;
    private DefaultListModel model;
    private final int position;
    private final JTextComponent target;
    private final String altKey = KeyEvent.getKeyText(org.openide.util.Utilities.isMac() ? 157 : 18);
    public JLabel ctrlLabel;
    public JLabel jLabel1;
    public JList jList1;
    public JPanel jPanel1;
    public JScrollPane jScrollPane1;

    public ImportClassPanel(List<TypeElement> priviledged, List<TypeElement> denied, Font font, JavaSource javaSource, int position, JTextComponent target) {
        this.javaSource = javaSource;
        this.position = position;
        this.target = target;
        this.createModel(priviledged, denied);
        this.initComponents();
        this.setBackground(this.jList1.getBackground());
        if (this.model.size() > 0) {
            int modelSize;
            this.jList1.setModel(this.model);
            this.setFocusable(false);
            this.setNextFocusableComponent(this.jList1);
            this.jScrollPane1.setBackground(this.jList1.getBackground());
            this.setBackground(this.jList1.getBackground());
            if (font != null) {
                this.jList1.setFont(font);
            }
            if ((modelSize = this.jList1.getModel().getSize()) > 0) {
                this.jList1.setSelectedIndex(0);
            }
            this.jList1.setVisibleRowCount(modelSize > 8 ? 8 : modelSize);
            this.jList1.setCellRenderer(new Renderer(this.jList1));
            this.jList1.grabFocus();
        } else {
            this.remove(this.jScrollPane1);
            JLabel nothingFoundJL = new JLabel("<No Classes Found>");
            if (font != null) {
                nothingFoundJL.setFont(font);
            }
            nothingFoundJL.setBorder(BorderFactory.createEmptyBorder(2, 4, 4, 4));
            nothingFoundJL.setEnabled(false);
            nothingFoundJL.setBackground(this.jList1.getBackground());
            this.add(nothingFoundJL);
        }
        this.setA11Y();
    }

    private void setA11Y() {
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ImportClassPanel.class, (String)"ImportClassPanel_ACN"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ImportClassPanel.class, (String)"ImportClassPanel_ACSD"));
        this.jList1.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ImportClassPanel.class, (String)"ImportClassPanel_JList1_ACN"));
        this.jList1.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ImportClassPanel.class, (String)"ImportClassPanel_JList1_ACSD"));
    }

    private void initComponents() {
        this.jScrollPane1 = new JScrollPane();
        this.jList1 = new JList();
        this.jLabel1 = new JLabel();
        this.jPanel1 = new JPanel();
        this.ctrlLabel = new JLabel();
        this.setBorder(BorderFactory.createLineBorder(new Color(64, 64, 64)));
        this.setLayout(new BorderLayout());
        this.jScrollPane1.setBorder(BorderFactory.createEmptyBorder(2, 4, 4, 4));
        this.jList1.setSelectionMode(0);
        this.jList1.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseReleased(MouseEvent evt) {
                ImportClassPanel.this.listMouseReleased(evt);
            }
        });
        this.jList1.addKeyListener(new KeyAdapter(){

            @Override
            public void keyReleased(KeyEvent evt) {
                ImportClassPanel.this.listKeyReleased(evt);
            }
        });
        this.jScrollPane1.setViewportView(this.jList1);
        this.add((Component)this.jScrollPane1, "Center");
        this.jLabel1.setLabelFor(this.jList1);
        this.jLabel1.setText("Type to import:");
        this.jLabel1.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        this.jLabel1.setOpaque(true);
        this.add((Component)this.jLabel1, "First");
        this.jPanel1.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        this.jPanel1.setLayout(new BorderLayout());
        this.ctrlLabel.setText(NbBundle.getMessage(ImportClassPanel.class, (String)"LBL_PackageImport", (Object)this.altKey));
        this.jPanel1.add((Component)this.ctrlLabel, "Center");
        this.add((Component)this.jPanel1, "Last");
    }

    private void listMouseReleased(MouseEvent evt) {
        this.importClass(this.getSelected(), (evt.getModifiers() & 8) > 0, (evt.getModifiers() & 1) > 0);
    }

    private void listKeyReleased(KeyEvent evt) {
        KeyStroke ks = KeyStroke.getKeyStrokeForEvent(evt);
        if (ks.getKeyCode() == 10 || ks.getKeyCode() == 32) {
            this.importClass(this.getSelected(), (evt.getModifiers() & 8) > 0, (evt.getModifiers() & 1) > 0);
        }
    }

    public String getSelected() {
        TypeDescription typeDescription = (TypeDescription)this.jList1.getSelectedValue();
        return typeDescription == null ? null : typeDescription.qualifiedName;
    }

    private void createModel(List<TypeElement> priviledged, List<TypeElement> denied) {
        ArrayList<TypeDescription> l = new ArrayList<TypeDescription>(priviledged.size());
        for (TypeElement typeElement : priviledged) {
            l.add(new TypeDescription(typeElement, false));
        }
        ArrayList ld = new ArrayList(priviledged.size());
        for (TypeElement typeElement2 : denied) {
            l.add(new TypeDescription(typeElement2, true));
        }
        Collections.sort(l);
        this.model = new DefaultListModel();
        for (TypeDescription td : l) {
            this.model.addElement(td);
        }
    }

    private void importClass(String name, final boolean packageImport, final boolean useFqn) {
        String fqn;
        int index;
        PopupUtil.hidePopup();
        if (packageImport && !useFqn && (index = name.lastIndexOf(46)) != -1) {
            name = name.substring(0, index);
        }
        if ((fqn = name) != null) {
            final AtomicBoolean cancel = new AtomicBoolean();
            Task<WorkingCopy> task = new Task<WorkingCopy>(){

                public void run(WorkingCopy wc) throws IOException {
                    QualifiedNameable e;
                    if (cancel != null && cancel.get()) {
                        return;
                    }
                    wc.toPhase(JavaSource.Phase.RESOLVED);
                    if (cancel != null && cancel.get()) {
                        return;
                    }
                    CompilationUnitTree cut = wc.getCompilationUnit();
                    if (useFqn && this.replaceSimpleName(fqn, wc)) {
                        return;
                    }
                    if (this.isImported(fqn, cut.getImports())) {
                        Utilities.setStatusText((JTextComponent)EditorRegistry.lastFocusedComponent(), (String)NbBundle.getMessage(ImportClassPanel.class, (String)(packageImport ? "MSG_PackageAlreadyImported" : "MSG_ClassAlreadyImported"), (Object)fqn), (int)700);
                        return;
                    }
                    QualifiedNameable qualifiedNameable = e = packageImport ? wc.getElements().getPackageElement(fqn) : wc.getElements().getTypeElement(fqn);
                    if (e == null) {
                        Utilities.setStatusText((JTextComponent)EditorRegistry.lastFocusedComponent(), (String)NbBundle.getMessage(ImportClassPanel.class, (String)(packageImport ? "MSG_CannotResolvePackage" : "MSG_CannotResolveClass"), (Object)fqn), (int)700);
                        return;
                    }
                    CompilationUnitTree cutCopy = GeneratorUtilities.get((WorkingCopy)wc).addImports(cut, Collections.singleton(e));
                    wc.rewrite((Tree)cut, (Tree)cutCopy);
                }

                private boolean replaceSimpleName(String fqn2, WorkingCopy wc) {
                    TreeUtilities tu = wc.getTreeUtilities();
                    TreePath tp = tu.pathFor(ImportClassPanel.this.position);
                    TreePath tpPlusOne = tu.pathFor(ImportClassPanel.this.position + 1);
                    TreeMaker tm = wc.getTreeMaker();
                    if (tp.getLeaf().getKind() == Tree.Kind.IDENTIFIER) {
                        wc.rewrite(tp.getLeaf(), (Tree)tm.Identifier((CharSequence)fqn2));
                        return true;
                    }
                    if (tp.getLeaf().getKind() != Tree.Kind.IDENTIFIER && tpPlusOne.getLeaf().getKind() == Tree.Kind.IDENTIFIER) {
                        wc.rewrite(tpPlusOne.getLeaf(), (Tree)tm.Identifier((CharSequence)fqn2));
                        return true;
                    }
                    return false;
                }

                private boolean isImported(String fqn2, List<? extends ImportTree> imports) {
                    for (ImportTree i : imports) {
                        if (!fqn2.equals(i.getQualifiedIdentifier().toString())) continue;
                        return true;
                    }
                    return false;
                }
            };
            ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable((Task)task){
                final /* synthetic */ Task val$task;

                @Override
                public void run() {
                    try {
                        ModificationResult mr = ImportClassPanel.this.javaSource.runModificationTask(this.val$task);
                        GeneratorUtils.guardedCommit(ImportClassPanel.this.target, mr);
                    }
                    catch (IOException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
            }, (String)NbBundle.getMessage(ImportClassPanel.class, (String)"LBL_Fast_Import"), (AtomicBoolean)cancel, (boolean)false);
        }
    }

    private static class TypeDescription
    implements Comparable<TypeDescription> {
        private boolean isDenied;
        private final ElementKind kind;
        private final String qualifiedName;

        public TypeDescription(TypeElement typeElement, boolean isDenied) {
            this.isDenied = isDenied;
            this.kind = typeElement.getKind();
            this.qualifiedName = typeElement.getQualifiedName().toString();
        }

        @Override
        public int compareTo(TypeDescription o) {
            if (this.isDenied && !o.isDenied) {
                return 1;
            }
            if (!this.isDenied && o.isDenied) {
                return -1;
            }
            return this.qualifiedName.compareTo(o.qualifiedName);
        }
    }

    private static class Renderer
    extends DefaultListCellRenderer {
        private static int DARKER_COLOR_COMPONENT;
        private static int LIGHTER_COLOR_COMPONENT;
        private Color fgColor;
        private Color bgColor;
        private Color bgColorDarker;
        private Color bgSelectionColor;
        private Color fgSelectionColor;

        public Renderer(JList list) {
            this.setFont(list.getFont());
            this.fgColor = list.getForeground();
            this.bgColor = list.getBackground();
            this.bgColorDarker = new Color(Math.abs(this.bgColor.getRed() - DARKER_COLOR_COMPONENT), Math.abs(this.bgColor.getGreen() - DARKER_COLOR_COMPONENT), Math.abs(this.bgColor.getBlue() - DARKER_COLOR_COMPONENT));
            this.bgSelectionColor = list.getSelectionBackground();
            this.fgSelectionColor = list.getSelectionForeground();
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean hasFocus) {
            if (isSelected) {
                this.setForeground(this.fgSelectionColor);
                this.setBackground(this.bgSelectionColor);
            } else {
                this.setForeground(this.fgColor);
                this.setBackground(index % 2 == 0 ? this.bgColor : this.bgColorDarker);
            }
            if (value instanceof TypeDescription) {
                TypeDescription td = (TypeDescription)value;
                if (td.isDenied) {
                    this.setText(JavaFixAllImports.NOT_VALID_IMPORT_HTML + td.qualifiedName);
                } else {
                    this.setText(td.qualifiedName);
                }
                this.setIcon(ElementIcons.getElementIcon((ElementKind)td.kind, (Collection)null));
            } else {
                this.setText(value.toString());
            }
            return this;
        }

        static {
            LIGHTER_COLOR_COMPONENT = Renderer.DARKER_COLOR_COMPONENT = 5;
        }
    }

}

