/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.KeyBindingSettings
 *  org.netbeans.api.editor.settings.MultiKeyBinding
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.JavaParserResultTask
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.netbeans.modules.parsing.spi.SchedulerTask
 *  org.netbeans.modules.parsing.spi.TaskFactory
 *  org.netbeans.modules.parsing.spi.TaskIndexingMode
 *  org.openide.filesystems.FileObject
 *  org.openide.text.NbDocument
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.overridden;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.awt.event.KeyEvent;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.swing.KeyStroke;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.KeyBindingSettings;
import org.netbeans.api.editor.settings.MultiKeyBinding;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.JavaParserResultTask;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.modules.java.editor.overridden.AnnotationType;
import org.netbeans.modules.java.editor.overridden.AnnotationsHolder;
import org.netbeans.modules.java.editor.overridden.ComputeOverriders;
import org.netbeans.modules.java.editor.overridden.ComputeOverriding;
import org.netbeans.modules.java.editor.overridden.ElementDescription;
import org.netbeans.modules.java.editor.overridden.IsOverriddenAnnotation;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.SchedulerTask;
import org.netbeans.modules.parsing.spi.TaskFactory;
import org.netbeans.modules.parsing.spi.TaskIndexingMode;
import org.openide.filesystems.FileObject;
import org.openide.text.NbDocument;
import org.openide.util.NbBundle;

public class ComputeAnnotations
extends JavaParserResultTask<Parser.Result> {
    private final AtomicBoolean cancel = new AtomicBoolean();

    public ComputeAnnotations() {
        super(JavaSource.Phase.RESOLVED, TaskIndexingMode.ALLOWED_DURING_SCAN);
    }

    public void run(Parser.Result result, SchedulerEvent event) {
        this.cancel.set(false);
        CompilationInfo info = CompilationInfo.get((Parser.Result)result);
        if (info.getChangedTree() != null) {
            return;
        }
        long start = System.currentTimeMillis();
        StyledDocument doc = (StyledDocument)result.getSnapshot().getSource().getDocument(false);
        if (doc == null) {
            return;
        }
        List<IsOverriddenAnnotation> annotations = this.computeAnnotations(info, doc);
        if (this.cancel.get()) {
            return;
        }
        AnnotationsHolder holder = AnnotationsHolder.get(info.getFileObject());
        if (holder != null) {
            holder.setNewAnnotations(annotations);
        }
        long end = System.currentTimeMillis();
        Logger.getLogger("TIMER").log(Level.FINE, "Is Overridden Annotations", new Object[]{info.getFileObject(), end - start});
    }

    List<IsOverriddenAnnotation> computeAnnotations(CompilationInfo info, StyledDocument doc) {
        LinkedList<IsOverriddenAnnotation> annotations = new LinkedList<IsOverriddenAnnotation>();
        this.createAnnotations(info, doc, new ComputeOverriding(this.cancel).process(info), false, annotations);
        this.createAnnotations(info, doc, new ComputeOverriders(this.cancel).process(info, null, null, false), true, annotations);
        return annotations;
    }

    private void createAnnotations(CompilationInfo info, StyledDocument doc, Map<ElementHandle<? extends Element>, List<ElementDescription>> descriptions, boolean overridden, List<IsOverriddenAnnotation> annotations) {
        String kb = ComputeAnnotations.findKeyBinding(overridden ? "goto-implementation" : "goto-super-implementation");
        if (descriptions != null) {
            for (Map.Entry<ElementHandle<? extends Element>, List<ElementDescription>> e : descriptions.entrySet()) {
                String dn;
                Position pos;
                AnnotationType type;
                int[] elementNameSpan;
                Element ee = e.getKey().resolve(info);
                Tree t = info.getTrees().getTree(ee);
                if (t == null) continue;
                if (overridden) {
                    int choice;
                    if (ee.getModifiers().contains((Object)Modifier.ABSTRACT)) {
                        type = AnnotationType.HAS_IMPLEMENTATION;
                        dn = NbBundle.getMessage(ComputeAnnotations.class, (String)"TP_HasImplementations");
                        choice = 0;
                    } else {
                        type = AnnotationType.IS_OVERRIDDEN;
                        if (ee.getKind().isClass()) {
                            dn = NbBundle.getMessage(ComputeAnnotations.class, (String)"TP_HasSubclasses");
                            choice = 1;
                        } else {
                            dn = NbBundle.getMessage(ComputeAnnotations.class, (String)"TP_IsOverridden");
                            choice = 2;
                        }
                    }
                    if (kb != null) {
                        dn = dn + NbBundle.getMessage(ComputeAnnotations.class, (String)"LBL_shortcut_promotion", (Object)kb, (Object)choice);
                    }
                } else {
                    StringBuffer tooltip = new StringBuffer();
                    boolean wasOverrides = false;
                    boolean newline = false;
                    for (ElementDescription ed : e.getValue()) {
                        if (newline) {
                            tooltip.append("\n");
                        }
                        newline = true;
                        if (ed.getModifiers().contains((Object)Modifier.ABSTRACT)) {
                            tooltip.append(NbBundle.getMessage(ComputeAnnotations.class, (String)"TP_Implements", (Object)ed.getDisplayName()));
                            continue;
                        }
                        tooltip.append(NbBundle.getMessage(ComputeAnnotations.class, (String)"TP_Overrides", (Object)ed.getDisplayName()));
                        wasOverrides = true;
                    }
                    type = wasOverrides ? AnnotationType.OVERRIDES : AnnotationType.IMPLEMENTS;
                    dn = tooltip.toString();
                    if (kb != null) {
                        dn = dn + NbBundle.getMessage(ComputeAnnotations.class, (String)"LBL_shortcut_promotion", (Object)kb, (Object)3);
                    }
                }
                switch (t.getKind()) {
                    case ANNOTATION_TYPE: 
                    case CLASS: 
                    case ENUM: 
                    case INTERFACE: {
                        elementNameSpan = info.getTreeUtilities().findNameSpan((ClassTree)t);
                        break;
                    }
                    case METHOD: {
                        elementNameSpan = info.getTreeUtilities().findNameSpan((MethodTree)t);
                        break;
                    }
                    default: {
                        elementNameSpan = new int[]{(int)info.getTrees().getSourcePositions().getStartPosition(info.getCompilationUnit(), t), -1};
                    }
                }
                if (elementNameSpan == null || (pos = ComputeAnnotations.getPosition(doc, elementNameSpan[0])) == null) continue;
                annotations.add(new IsOverriddenAnnotation(doc, pos, type, dn, e.getValue()));
            }
        }
    }

    public int getPriority() {
        return Integer.MAX_VALUE;
    }

    public Class<? extends Scheduler> getSchedulerClass() {
        return Scheduler.EDITOR_SENSITIVE_TASK_SCHEDULER;
    }

    public void cancel() {
        this.cancel.set(true);
    }

    private static Position getPosition(StyledDocument doc, int offset) {
        class Impl
        implements Runnable {
            private Position pos;
            final /* synthetic */ StyledDocument val$doc;

            Impl() {
                this.val$doc = var2_2;
            }

            @Override
            public void run() {
                if (val$offset < 0 || val$offset >= this.val$doc.getLength()) {
                    return;
                }
                try {
                    this.pos = this.val$doc.createPosition(val$offset - NbDocument.findLineColumn((StyledDocument)this.val$doc, (int)val$offset));
                }
                catch (BadLocationException ex) {
                    Logger.getLogger(ComputeAnnotations.class.getName()).log(Level.FINE, null, ex);
                }
            }
        }
        Impl i = new Impl(offset, doc);
        doc.render(i);
        return i.pos;
    }

    private static String findKeyBinding(String actionName) {
        KeyBindingSettings kbs = (KeyBindingSettings)MimeLookup.getLookup((MimePath)MimePath.get((String)"text/x-java")).lookup(KeyBindingSettings.class);
        for (MultiKeyBinding mkb : kbs.getKeyBindings()) {
            if (!actionName.equals(mkb.getActionName())) continue;
            KeyStroke ks = mkb.getKeyStrokeCount() > 0 ? mkb.getKeyStroke(0) : null;
            return ks != null ? KeyEvent.getKeyModifiersText(ks.getModifiers()) + '+' + KeyEvent.getKeyText(ks.getKeyCode()) : null;
        }
        return null;
    }

    public static final class FactoryImpl
    extends TaskFactory {
        public Collection<? extends SchedulerTask> create(Snapshot snapshot) {
            return Collections.singleton(new ComputeAnnotations());
        }
    }

}

