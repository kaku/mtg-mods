/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.spi.editor.highlighting.HighlightsSequence
 */
package org.netbeans.modules.java.editor.semantic;

import java.util.Map;
import java.util.Stack;
import javax.swing.text.AttributeSet;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.java.editor.semantic.ColoringAttributes;
import org.netbeans.modules.java.editor.semantic.LexerBasedHighlightLayer;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;

public class EmbeddedLexerBasedHighlightSequence
implements HighlightsSequence {
    private LexerBasedHighlightLayer layer;
    private Map<Token, ColoringAttributes.Coloring> colorings;
    private Stack<TokenSequence> stack;
    private TokenSequence ts;
    private boolean started;

    public EmbeddedLexerBasedHighlightSequence(LexerBasedHighlightLayer layer, TokenSequence ts, Map<Token, ColoringAttributes.Coloring> colorings) {
        this.layer = layer;
        this.ts = ts;
        this.colorings = colorings;
        this.stack = new Stack();
    }

    private boolean moveNextImpl2() {
        if (this.started) {
            return this.ts.moveNext();
        }
        this.started = true;
        return this.ts.moveNext();
    }

    private boolean moveNextImpl() {
        if (this.moveNextImpl2()) {
            TokenSequence tseq = this.ts.embedded();
            if (tseq != null) {
                this.stack.push(this.ts);
                this.ts = tseq;
            }
            return true;
        }
        if (this.stack.isEmpty()) {
            return false;
        }
        this.ts = this.stack.pop();
        return this.moveNextImpl();
    }

    public boolean moveNext() {
        while (this.moveNextImpl()) {
            Token t = this.ts.token();
            if (t == null || t.id() != JavaTokenId.IDENTIFIER || !this.colorings.containsKey((Object)this.ts.token())) continue;
            return true;
        }
        return false;
    }

    public int getStartOffset() {
        return this.ts.offset();
    }

    public int getEndOffset() {
        return this.ts.offset() + this.ts.token().length();
    }

    public AttributeSet getAttributes() {
        return this.layer.getColoring(this.colorings.get((Object)this.ts.token()));
    }
}

