/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer$CustomCustomizer
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer$Factory
 *  org.openide.awt.Mnemonics
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.options;

import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;
import java.util.Map;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.ListModel;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.openide.awt.Mnemonics;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

public class CodeCompletionPanel
extends JPanel
implements DocumentListener {
    public static final String JAVA_AUTO_POPUP_ON_IDENTIFIER_PART = "javaAutoPopupOnIdentifierPart";
    public static final boolean JAVA_AUTO_POPUP_ON_IDENTIFIER_PART_DEFAULT = false;
    public static final String JAVA_AUTO_COMPLETION_TRIGGERS = "javaAutoCompletionTriggers";
    public static final String JAVA_AUTO_COMPLETION_TRIGGERS_DEFAULT = ".";
    public static final String JAVA_COMPLETION_SELECTORS = "javaCompletionSelectors";
    public static final String JAVA_COMPLETION_SELECTORS_DEFAULT = ".,;:([+-=";
    public static final String JAVADOC_AUTO_COMPLETION_TRIGGERS = "javadocAutoCompletionTriggers";
    public static final String JAVADOC_AUTO_COMPLETION_TRIGGERS_DEFAULT = ".#@";
    public static final String JAVADOC_COMPLETION_SELECTORS = "javadocCompletionSelectors";
    public static final String JAVADOC_COMPLETION_SELECTORS_DEFAULT = ".#";
    public static final String GUESS_METHOD_ARGUMENTS = "guessMethodArguments";
    public static final boolean GUESS_METHOD_ARGUMENTS_DEFAULT = true;
    public static final String JAVA_COMPLETION_WHITELIST = "javaCompletionWhitelist";
    public static final String JAVA_COMPLETION_WHITELIST_DEFAULT = "";
    public static final String JAVA_COMPLETION_BLACKLIST = "javaCompletionBlacklist";
    public static final String JAVA_COMPLETION_BLACKLIST_DEFAULT = "";
    public static final String JAVA_COMPLETION_EXCLUDER_METHODS = "javaCompletionExcluderMethods";
    public static final boolean JAVA_COMPLETION_EXCLUDER_METHODS_DEFAULT = false;
    public static final String JAVA_AUTO_COMPLETION_SUBWORDS = "javaCompletionSubwords";
    public static final boolean JAVA_AUTO_COMPLETION_SUBWORDS_DEFAULT = false;
    private static final String JAVA_FQN_REGEX = "[$\\p{L}\\p{Digit}._]*";
    private final Preferences preferences;
    private volatile String javaExcluderEditing;
    private final Map<String, Object> id2Saved = new HashMap<String, Object>();
    private JCheckBox guessMethodArguments;
    private JSeparator jSeparator1;
    private JSeparator jSeparator2;
    private JCheckBox javaAutoCompletionSubwords;
    private JTextField javaAutoCompletionTriggersField;
    private JLabel javaAutoCompletionTriggersLabel;
    private JCheckBox javaAutoPopupOnIdentifierPart;
    private JList javaCompletionExcludeJlist;
    private JScrollPane javaCompletionExcludeScrollPane;
    private JButton javaCompletionExcluderAddButton;
    private JDialog javaCompletionExcluderDialog2;
    private JButton javaCompletionExcluderDialogCancelButton;
    private JLabel javaCompletionExcluderDialogLabel;
    private JButton javaCompletionExcluderDialogOkButton;
    private JTextField javaCompletionExcluderDialogTextField;
    private JButton javaCompletionExcluderEditButton;
    private JLabel javaCompletionExcluderLabel;
    private JCheckBox javaCompletionExcluderMethodsCheckBox;
    private JButton javaCompletionExcluderRemoveButton;
    private JTabbedPane javaCompletionExcluderTab;
    private JList javaCompletionIncludeJlist;
    private JScrollPane javaCompletionIncludeScrollPane;
    private JTextField javaCompletionSelectorsField;
    private JLabel javaCompletionSelectorsLabel;
    private JTextField javadocAutoCompletionTriggersField;
    private JLabel javadocAutoCompletionTriggersLabel;
    private JTextField javadocCompletionSelectorsField;
    private JLabel javadocCompletionSelectorsLabel;

    public CodeCompletionPanel(Preferences p) {
        this.initComponents();
        this.preferences = p;
        this.guessMethodArguments.setSelected(this.preferences.getBoolean("guessMethodArguments", true));
        this.javaAutoPopupOnIdentifierPart.setSelected(this.preferences.getBoolean("javaAutoPopupOnIdentifierPart", false));
        this.javaAutoCompletionTriggersField.setText(this.preferences.get("javaAutoCompletionTriggers", "."));
        this.javaCompletionSelectorsField.setText(this.preferences.get("javaCompletionSelectors", ".,;:([+-="));
        this.javaAutoCompletionSubwords.setSelected(this.preferences.getBoolean("javaCompletionSubwords", false));
        this.javadocAutoCompletionTriggersField.setText(this.preferences.get("javadocAutoCompletionTriggers", ".#@"));
        this.javadocCompletionSelectorsField.setText(this.preferences.get("javadocCompletionSelectors", ".#"));
        String blacklist = this.preferences.get("javaCompletionBlacklist", "");
        this.initExcluderList(this.javaCompletionExcludeJlist, blacklist);
        String whitelist = this.preferences.get("javaCompletionWhitelist", "");
        this.initExcluderList(this.javaCompletionIncludeJlist, whitelist);
        this.javaCompletionExcluderMethodsCheckBox.setSelected(this.preferences.getBoolean("javaCompletionExcluderMethods", false));
        this.javaCompletionExcluderDialog2.getRootPane().setDefaultButton(this.javaCompletionExcluderDialogOkButton);
        this.id2Saved.put("guessMethodArguments", this.guessMethodArguments.isSelected());
        this.id2Saved.put("javaAutoPopupOnIdentifierPart", this.javaAutoPopupOnIdentifierPart.isSelected());
        this.id2Saved.put("javaCompletionSubwords", this.javaAutoCompletionSubwords.isSelected());
        this.id2Saved.put("javaCompletionExcluderMethods", this.javaCompletionExcluderMethodsCheckBox.isSelected());
        this.id2Saved.put("javaAutoCompletionTriggers", this.javaAutoCompletionTriggersField.getText());
        this.id2Saved.put("javaCompletionSelectors", this.javaCompletionSelectorsField.getText());
        this.id2Saved.put("javadocAutoCompletionTriggers", this.javadocAutoCompletionTriggersField.getText());
        this.id2Saved.put("javadocCompletionSelectors", this.javadocCompletionSelectorsField.getText());
        this.id2Saved.put("javaCompletionBlacklist", blacklist);
        this.id2Saved.put("javaCompletionWhitelist", whitelist);
        this.javaCompletionExcluderDialog2.pack();
        this.javaCompletionExcluderDialog2.setLocationRelativeTo(this);
        this.javaAutoCompletionTriggersField.getDocument().addDocumentListener(this);
        this.javaCompletionSelectorsField.getDocument().addDocumentListener(this);
        this.javadocAutoCompletionTriggersField.getDocument().addDocumentListener(this);
        this.javadocCompletionSelectorsField.getDocument().addDocumentListener(this);
    }

    private void initExcluderList(JList jList, String list) {
        String[] entries;
        DefaultListModel<String> model = new DefaultListModel<String>();
        for (String entry : entries = list.split(",")) {
            if (entry.length() == 0) continue;
            model.addElement(entry);
        }
        jList.setModel(model);
    }

    private void openExcluderEditor() {
        assert (!this.javaCompletionExcluderDialog2.isVisible());
        this.javaCompletionExcluderDialogTextField.setText(this.javaExcluderEditing);
        this.javaCompletionExcluderDialog2.setVisible(true);
        this.javaCompletionExcluderDialogTextField.requestFocus();
    }

    public static PreferencesCustomizer.Factory getCustomizerFactory() {
        return new PreferencesCustomizer.Factory(){

            public PreferencesCustomizer create(Preferences preferences) {
                return new CodeCompletionPreferencesCustomizer(preferences);
            }
        };
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        this.update(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        this.update(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        this.update(e);
    }

    private void initComponents() {
        this.javaCompletionExcluderDialog2 = new JDialog();
        this.javaCompletionExcluderDialogTextField = new JTextField();
        this.javaCompletionExcluderDialogOkButton = new JButton();
        this.javaCompletionExcluderDialogLabel = new JLabel();
        this.javaCompletionExcluderDialogCancelButton = new JButton();
        this.javaCompletionExcluderEditButton = new JButton();
        this.jSeparator2 = new JSeparator();
        this.javaCompletionExcluderAddButton = new JButton();
        this.javaCompletionExcluderRemoveButton = new JButton();
        this.javaAutoCompletionSubwords = new JCheckBox();
        this.javaCompletionExcluderMethodsCheckBox = new JCheckBox();
        this.javaCompletionExcluderTab = new JTabbedPane();
        this.javaCompletionExcludeScrollPane = new JScrollPane();
        this.javaCompletionExcludeJlist = new JList();
        this.javaCompletionIncludeScrollPane = new JScrollPane();
        this.javaCompletionIncludeJlist = new JList();
        this.javaCompletionExcluderLabel = new JLabel();
        this.guessMethodArguments = new JCheckBox();
        this.jSeparator1 = new JSeparator();
        this.javadocAutoCompletionTriggersField = new JTextField();
        this.javaCompletionSelectorsLabel = new JLabel();
        this.javaAutoCompletionTriggersField = new JTextField();
        this.javaAutoCompletionTriggersLabel = new JLabel();
        this.javaAutoPopupOnIdentifierPart = new JCheckBox();
        this.javadocAutoCompletionTriggersLabel = new JLabel();
        this.javaCompletionSelectorsField = new JTextField();
        this.javadocCompletionSelectorsLabel = new JLabel();
        this.javadocCompletionSelectorsField = new JTextField();
        this.javaCompletionExcluderDialog2.setTitle(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ExcluderDialogTitle"));
        this.javaCompletionExcluderDialog2.setModal(true);
        this.javaCompletionExcluderDialogTextField.setText(NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.javaCompletionExcluderDialogTextField.text"));
        this.javaCompletionExcluderDialogTextField.addKeyListener(new KeyAdapter(){

            @Override
            public void keyTyped(KeyEvent evt) {
                CodeCompletionPanel.this.javaCompletionExcluderDialogTextFieldKeyTyped(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.javaCompletionExcluderDialogOkButton, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.javaCompletionExcluderDialogOkButton.text"));
        this.javaCompletionExcluderDialogOkButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                CodeCompletionPanel.this.javaCompletionExcluderDialogOkButtonActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((JLabel)this.javaCompletionExcluderDialogLabel, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.javaCompletionExcluderDialogLabel.text"));
        Mnemonics.setLocalizedText((AbstractButton)this.javaCompletionExcluderDialogCancelButton, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.javaCompletionExcluderDialogCancelButton.text"));
        this.javaCompletionExcluderDialogCancelButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                CodeCompletionPanel.this.javaCompletionExcluderDialogCancelButtonActionPerformed(evt);
            }
        });
        GroupLayout javaCompletionExcluderDialog2Layout = new GroupLayout(this.javaCompletionExcluderDialog2.getContentPane());
        this.javaCompletionExcluderDialog2.getContentPane().setLayout(javaCompletionExcluderDialog2Layout);
        javaCompletionExcluderDialog2Layout.setHorizontalGroup(javaCompletionExcluderDialog2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(javaCompletionExcluderDialog2Layout.createSequentialGroup().addContainerGap().addGroup(javaCompletionExcluderDialog2Layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false).addComponent(this.javaCompletionExcluderDialogLabel, GroupLayout.Alignment.LEADING, 0, 0, 32767).addComponent(this.javaCompletionExcluderDialogTextField, GroupLayout.Alignment.LEADING, -1, 307, 32767)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(javaCompletionExcluderDialog2Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.javaCompletionExcluderDialogOkButton, -1, -1, 32767).addComponent(this.javaCompletionExcluderDialogCancelButton, -1, -1, 32767)).addContainerGap(-1, 32767)));
        javaCompletionExcluderDialog2Layout.setVerticalGroup(javaCompletionExcluderDialog2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, javaCompletionExcluderDialog2Layout.createSequentialGroup().addGroup(javaCompletionExcluderDialog2Layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addGroup(javaCompletionExcluderDialog2Layout.createSequentialGroup().addContainerGap().addComponent(this.javaCompletionExcluderDialogLabel, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767)).addGroup(javaCompletionExcluderDialog2Layout.createSequentialGroup().addContainerGap().addComponent(this.javaCompletionExcluderDialogCancelButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED))).addGroup(javaCompletionExcluderDialog2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.javaCompletionExcluderDialogTextField, -2, -1, -2).addComponent(this.javaCompletionExcluderDialogOkButton)).addGap(61, 61, 61)));
        this.javaCompletionExcluderDialogTextField.getAccessibleContext().setAccessibleName(NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.javaCompletionExcluderDialogTextField.AccessibleContext.accessibleName"));
        this.javaCompletionExcluderDialogTextField.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.javaCompletionExcluderDialogTextField.AccessibleContext.accessibleDescription"));
        this.javaCompletionExcluderDialogOkButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSD_OKButton"));
        this.javaCompletionExcluderDialogLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.javaCompletionExcluderDialogLabel.AccessibleContext.accessibleName"));
        this.javaCompletionExcluderDialogCancelButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSD_CancelButton"));
        this.javaCompletionExcluderDialog2.getAccessibleContext().setAccessibleName(NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.javaCompletionExcluderDialog2.AccessibleContext.accessibleName"));
        this.javaCompletionExcluderDialog2.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSD_PopupDialog"));
        this.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.javaCompletionExcluderEditButton.setMnemonic('E');
        Mnemonics.setLocalizedText((AbstractButton)this.javaCompletionExcluderEditButton, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.javaCompletionExcluderEditButton.text"));
        this.javaCompletionExcluderEditButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                CodeCompletionPanel.this.javaCompletionExcluderEditButtonActionPerformed(evt);
            }
        });
        this.javaCompletionExcluderAddButton.setMnemonic('A');
        Mnemonics.setLocalizedText((AbstractButton)this.javaCompletionExcluderAddButton, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.javaCompletionExcluderAddButton.text"));
        this.javaCompletionExcluderAddButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                CodeCompletionPanel.this.javaCompletionExcluderAddButtonActionPerformed(evt);
            }
        });
        this.javaCompletionExcluderRemoveButton.setMnemonic('R');
        Mnemonics.setLocalizedText((AbstractButton)this.javaCompletionExcluderRemoveButton, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.javaCompletionExcluderRemoveButton.text"));
        this.javaCompletionExcluderRemoveButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                CodeCompletionPanel.this.javaCompletionExcluderRemoveButtonActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.javaAutoCompletionSubwords, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.javaAutoCompletionSubwords.text"));
        this.javaAutoCompletionSubwords.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                CodeCompletionPanel.this.javaAutoCompletionSubwordsActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.javaCompletionExcluderMethodsCheckBox, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.javaCompletionExcluderMethodsCheckBox.text"));
        this.javaCompletionExcluderMethodsCheckBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                CodeCompletionPanel.this.javaCompletionExcluderMethodsCheckBoxActionPerformed(evt);
            }
        });
        this.javaCompletionExcludeScrollPane.setViewportView(this.javaCompletionExcludeJlist);
        this.javaCompletionExcludeJlist.getAccessibleContext().setAccessibleName(NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.javaCompletionExcludeJlist.AccessibleContext.accessibleName"));
        this.javaCompletionExcludeJlist.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSD_ExcludeList"));
        this.javaCompletionExcluderTab.addTab(NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.javaCompletionExcludeScrollPane.TabConstraints.tabTitle"), this.javaCompletionExcludeScrollPane);
        this.javaCompletionExcludeScrollPane.getAccessibleContext().setAccessibleName(NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.javaCompletionExcludeScrollPane.AccessibleContext.accessibleName"));
        this.javaCompletionIncludeScrollPane.setViewportView(this.javaCompletionIncludeJlist);
        this.javaCompletionIncludeJlist.getAccessibleContext().setAccessibleName(NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.javaCompletionIncludeJlist.AccessibleContext.accessibleName"));
        this.javaCompletionIncludeJlist.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSD_LT_Include"));
        this.javaCompletionExcluderTab.addTab(NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.javaCompletionIncludeScrollPane.TabConstraints.tabTitle"), this.javaCompletionIncludeScrollPane);
        this.javaCompletionIncludeScrollPane.getAccessibleContext().setAccessibleName(NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.javaCompletionIncludeScrollPane.AccessibleContext.accessibleName"));
        this.javaCompletionExcluderLabel.setLabelFor(this.javaCompletionExcluderTab);
        Mnemonics.setLocalizedText((JLabel)this.javaCompletionExcluderLabel, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.javaCompletionExcluderLabel.text"));
        Mnemonics.setLocalizedText((AbstractButton)this.guessMethodArguments, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"LBL_GuessMethodArgs"));
        this.guessMethodArguments.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                CodeCompletionPanel.this.guessMethodArgumentsActionPerformed(evt);
            }
        });
        this.javadocAutoCompletionTriggersField.setText(NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.javadocAutoCompletionTriggersField.text"));
        this.javaCompletionSelectorsLabel.setLabelFor(this.javaCompletionSelectorsField);
        Mnemonics.setLocalizedText((JLabel)this.javaCompletionSelectorsLabel, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"LBL_JavaCompletionSelectors"));
        this.javaAutoCompletionTriggersField.setAlignmentX(1.0f);
        this.javaAutoCompletionTriggersLabel.setLabelFor(this.javaAutoCompletionTriggersField);
        Mnemonics.setLocalizedText((JLabel)this.javaAutoCompletionTriggersLabel, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"LBL_JavaAutoCompletionTriggers"));
        Mnemonics.setLocalizedText((AbstractButton)this.javaAutoPopupOnIdentifierPart, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"LBL_AutoPopupOnIdentifierPartBox"));
        this.javaAutoPopupOnIdentifierPart.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                CodeCompletionPanel.this.javaAutoPopupOnIdentifierPartActionPerformed(evt);
            }
        });
        this.javadocAutoCompletionTriggersLabel.setLabelFor(this.javadocAutoCompletionTriggersField);
        Mnemonics.setLocalizedText((JLabel)this.javadocAutoCompletionTriggersLabel, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"LBL_JavadocAutoCompletionTriggers"));
        this.javadocCompletionSelectorsLabel.setLabelFor(this.javadocCompletionSelectorsField);
        Mnemonics.setLocalizedText((JLabel)this.javadocCompletionSelectorsLabel, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"LBL_JavadocCompletionSelectors"));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jSeparator1).addComponent(this.jSeparator2).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.guessMethodArguments).addComponent(this.javaAutoPopupOnIdentifierPart).addComponent(this.javaCompletionExcluderMethodsCheckBox).addComponent(this.javaCompletionExcluderLabel).addGroup(layout.createSequentialGroup().addComponent(this.javaCompletionSelectorsLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.javaCompletionSelectorsField, -2, 86, -2)).addGroup(layout.createSequentialGroup().addComponent(this.javaAutoCompletionTriggersLabel).addGap(46, 46, 46).addComponent(this.javaAutoCompletionTriggersField, -2, 86, -2)).addComponent(this.javaAutoCompletionSubwords).addComponent(this.javaCompletionExcluderTab).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.javadocAutoCompletionTriggersLabel).addComponent(this.javadocCompletionSelectorsLabel)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.javadocAutoCompletionTriggersField, -1, 86, 32767).addComponent(this.javadocCompletionSelectorsField)))).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.javaCompletionExcluderRemoveButton).addComponent(this.javaCompletionExcluderEditButton).addComponent(this.javaCompletionExcluderAddButton)))).addContainerGap()));
        layout.linkSize(0, this.javaCompletionExcluderAddButton, this.javaCompletionExcluderEditButton, this.javaCompletionExcluderRemoveButton);
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.guessMethodArguments, -2, 23, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.javaAutoCompletionTriggersLabel).addComponent(this.javaAutoCompletionTriggersField, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.javaAutoPopupOnIdentifierPart).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.javaCompletionSelectorsLabel).addComponent(this.javaCompletionSelectorsField, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.javaAutoCompletionSubwords).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.jSeparator2, -2, 4, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.javaCompletionExcluderLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.javaCompletionExcluderTab, -2, 124, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addComponent(this.javaCompletionExcluderAddButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.javaCompletionExcluderRemoveButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.javaCompletionExcluderEditButton).addGap(19, 19, 19))).addComponent(this.javaCompletionExcluderMethodsCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.jSeparator1, -2, 4, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.javadocAutoCompletionTriggersLabel).addComponent(this.javadocAutoCompletionTriggersField, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.javadocCompletionSelectorsLabel).addComponent(this.javadocCompletionSelectorsField, -2, -1, -2)).addContainerGap(-1, 32767)));
        this.javaCompletionExcluderEditButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSD_Edit"));
        this.javaCompletionExcluderAddButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSD_Add"));
        this.javaCompletionExcluderRemoveButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSD_Remove"));
        this.javaCompletionExcluderMethodsCheckBox.getAccessibleContext().setAccessibleName(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSN_CB_ApplyRulesToMethods"));
        this.javaCompletionExcluderMethodsCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSD_MethodsCB"));
        this.javaCompletionExcluderTab.getAccessibleContext().setAccessibleName(NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.javaCompletionExcluderTab.AccessibleContext.accessibleName"));
        this.javaCompletionExcluderTab.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSD_Table"));
        this.guessMethodArguments.getAccessibleContext().setAccessibleName(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSN_CB_GuessMethodArgs"));
        this.guessMethodArguments.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSD_CB_GuessMethodArgs"));
        this.javadocAutoCompletionTriggersField.getAccessibleContext().setAccessibleName(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSN_JavadocTriggers"));
        this.javadocAutoCompletionTriggersField.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSD_JavadocTrigger"));
        this.javaCompletionSelectorsLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSN_JavaCompletionSelectors"));
        this.javaCompletionSelectorsLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSD_JavaCompletionSelectors"));
        this.javaAutoCompletionTriggersLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSN_JavaTriggers"));
        this.javaAutoCompletionTriggersLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSD_JavaTrigger"));
        this.javaAutoPopupOnIdentifierPart.getAccessibleContext().setAccessibleName(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSN_CB_AutoPopupOnIdentifierPartBox"));
        this.javaAutoPopupOnIdentifierPart.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSD_CB_AutoPopupOnIdentifierPartBox"));
        this.javadocAutoCompletionTriggersLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSN_JavadocTriggers"));
        this.javadocAutoCompletionTriggersLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSD_JavadocTrigger"));
        this.javaCompletionSelectorsField.getAccessibleContext().setAccessibleName(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSN_JavaCompletionSelectors"));
        this.javaCompletionSelectorsField.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CodeCompletionPanel.class, (String)"ACSD_JavaCompletionSelectors"));
    }

    private void javaCompletionExcluderDialogOkButtonActionPerformed(ActionEvent evt) {
        String[] entries;
        JList list = this.getSelectedExcluderList();
        String text = this.javaCompletionExcluderDialogTextField.getText();
        DefaultListModel model = (DefaultListModel)list.getModel();
        int index = model.size();
        if (this.javaExcluderEditing != null) {
            index = model.indexOf(this.javaExcluderEditing);
            model.remove(index);
            this.javaExcluderEditing = null;
        }
        for (String entry : entries = text.split(",")) {
            if ((entry = entry.replaceAll("\u200b", "")).contains("*")) {
                entry = entry.replaceAll("\\*", "");
            }
            if ((entry = entry.trim()).length() == 0 || !entry.matches("[$\\p{L}\\p{Digit}._]*")) continue;
            model.insertElementAt(entry, index);
            ++index;
        }
        this.updateExcluder(list);
        this.javaCompletionExcluderDialog2.setVisible(false);
        this.javaCompletionExcluderDialogTextField.setText(null);
    }

    private void javaCompletionExcluderDialogCancelButtonActionPerformed(ActionEvent evt) {
        this.javaCompletionExcluderDialog2.setVisible(false);
        this.javaCompletionExcluderDialogTextField.setText(null);
        this.javaExcluderEditing = null;
    }

    private void javaCompletionExcluderDialogTextFieldKeyTyped(KeyEvent evt) {
        char c = evt.getKeyChar();
        if (c != ' ' && c != ',' && c != '*' && !String.valueOf(c).matches("[$\\p{L}\\p{Digit}._]*") && c != '\b') {
            this.getToolkit().beep();
            evt.consume();
        }
    }

    private void javaAutoCompletionSubwordsActionPerformed(ActionEvent evt) {
        this.preferences.putBoolean("javaCompletionSubwords", this.javaAutoCompletionSubwords.isSelected());
    }

    private void javaCompletionExcluderEditButtonActionPerformed(ActionEvent evt) {
        JList list = this.getSelectedExcluderList();
        int index = list.getSelectedIndex();
        if (index == -1) {
            return;
        }
        DefaultListModel model = (DefaultListModel)list.getModel();
        this.javaExcluderEditing = (String)model.getElementAt(index);
        this.openExcluderEditor();
    }

    private void javaCompletionExcluderRemoveButtonActionPerformed(ActionEvent evt) {
        JList list = this.getSelectedExcluderList();
        int[] rows = list.getSelectedIndices();
        DefaultListModel model = (DefaultListModel)list.getModel();
        for (int row = rows.length - 1; row >= 0; --row) {
            model.remove(rows[row]);
        }
        this.updateExcluder(list);
    }

    private void javaCompletionExcluderAddButtonActionPerformed(ActionEvent evt) {
        this.openExcluderEditor();
    }

    private void javaCompletionExcluderMethodsCheckBoxActionPerformed(ActionEvent evt) {
        this.preferences.putBoolean("javaCompletionExcluderMethods", this.javaCompletionExcluderMethodsCheckBox.isSelected());
    }

    private void javaAutoPopupOnIdentifierPartActionPerformed(ActionEvent evt) {
        this.preferences.putBoolean("javaAutoPopupOnIdentifierPart", this.javaAutoPopupOnIdentifierPart.isSelected());
    }

    private void guessMethodArgumentsActionPerformed(ActionEvent evt) {
        this.preferences.putBoolean("guessMethodArguments", this.guessMethodArguments.isSelected());
    }

    private void update(DocumentEvent e) {
        if (e.getDocument() == this.javaAutoCompletionTriggersField.getDocument()) {
            this.preferences.put("javaAutoCompletionTriggers", this.javaAutoCompletionTriggersField.getText());
        } else if (e.getDocument() == this.javaCompletionSelectorsField.getDocument()) {
            this.preferences.put("javaCompletionSelectors", this.javaCompletionSelectorsField.getText());
        } else if (e.getDocument() == this.javadocAutoCompletionTriggersField.getDocument()) {
            this.preferences.put("javadocAutoCompletionTriggers", this.javadocAutoCompletionTriggersField.getText());
        } else if (e.getDocument() == this.javadocCompletionSelectorsField.getDocument()) {
            this.preferences.put("javadocCompletionSelectors", this.javadocCompletionSelectorsField.getText());
        }
    }

    private void updateExcluder(JList list) {
        String pref;
        DefaultListModel model = (DefaultListModel)list.getModel();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < model.size(); ++i) {
            String entry = (String)model.getElementAt(i);
            if (builder.length() > 0) {
                builder.append(",");
            }
            builder.append(entry);
        }
        if (list == this.javaCompletionExcludeJlist) {
            pref = "javaCompletionBlacklist";
        } else if (list == this.javaCompletionIncludeJlist) {
            pref = "javaCompletionWhitelist";
        } else {
            throw new RuntimeException(list.getName());
        }
        this.preferences.put(pref, builder.toString());
    }

    private JList getSelectedExcluderList() {
        Component selected = this.javaCompletionExcluderTab.getSelectedComponent();
        if (selected == this.javaCompletionExcludeScrollPane) {
            return this.javaCompletionExcludeJlist;
        }
        if (selected == this.javaCompletionIncludeScrollPane) {
            return this.javaCompletionIncludeJlist;
        }
        throw new RuntimeException(selected.getName());
    }

    String getSavedValue(String key) {
        return this.id2Saved.get(key).toString();
    }

    public static final class CustomCustomizerImpl
    extends PreferencesCustomizer.CustomCustomizer {
        public String getSavedValue(PreferencesCustomizer customCustomizer, String key) {
            if (customCustomizer instanceof CodeCompletionPreferencesCustomizer) {
                return ((CodeCompletionPanel)customCustomizer.getComponent()).getSavedValue(key);
            }
            return null;
        }
    }

    private static class CodeCompletionPreferencesCustomizer
    implements PreferencesCustomizer {
        private final Preferences preferences;
        private CodeCompletionPanel component;

        private CodeCompletionPreferencesCustomizer(Preferences p) {
            this.preferences = p;
        }

        public String getId() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getDisplayName() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public HelpCtx getHelpCtx() {
            return new HelpCtx("netbeans.optionsDialog.editor.codeCompletion.java");
        }

        public JComponent getComponent() {
            if (this.component == null) {
                this.component = new CodeCompletionPanel(this.preferences);
            }
            return this.component;
        }
    }

}

