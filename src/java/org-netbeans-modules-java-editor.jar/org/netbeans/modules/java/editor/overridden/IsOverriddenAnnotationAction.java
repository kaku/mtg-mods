/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.AnnotationDesc
 *  org.netbeans.editor.Annotations
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.ImplementationProvider
 *  org.netbeans.editor.JumpList
 *  org.netbeans.editor.Utilities
 *  org.openide.ErrorManager
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.overridden;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import org.netbeans.editor.AnnotationDesc;
import org.netbeans.editor.Annotations;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.ImplementationProvider;
import org.netbeans.editor.JumpList;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.java.editor.overridden.AnnotationType;
import org.netbeans.modules.java.editor.overridden.AnnotationsHolder;
import org.netbeans.modules.java.editor.overridden.ElementDescription;
import org.netbeans.modules.java.editor.overridden.IsOverriddenAnnotation;
import org.netbeans.modules.java.editor.overridden.IsOverriddenPopup;
import org.netbeans.modules.java.editor.overridden.PopupUtil;
import org.openide.ErrorManager;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.util.NbBundle;

public final class IsOverriddenAnnotationAction
extends AbstractAction {
    private static final Set<String> COMBINED_TYPES = new HashSet<String>(Arrays.asList("org-netbeans-modules-editor-annotations-implements-has-implementations-combined", "org-netbeans-modules-editor-annotations-implements-is-overridden-combined", "org-netbeans-modules-editor-annotations-override-is-overridden-combined"));

    public IsOverriddenAnnotationAction() {
        this.putValue("Name", NbBundle.getMessage(IsOverriddenAnnotationAction.class, (String)"CTL_IsOverriddenAnnotationAction"));
        this.putValue("supported-annotation-types", new String[]{"org-netbeans-modules-editor-annotations-is_overridden", "org-netbeans-modules-editor-annotations-has_implementations", "org-netbeans-modules-editor-annotations-implements", "org-netbeans-modules-editor-annotations-overrides"});
        this.setEnabled(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!this.invokeDefaultAction((JTextComponent)e.getSource())) {
            int nextAction;
            Action a;
            Action[] actions = ImplementationProvider.getDefault().getGlyphGutterActions((JTextComponent)e.getSource());
            if (actions == null) {
                return;
            }
            for (nextAction = 0; nextAction < actions.length && actions[nextAction] != this; ++nextAction) {
            }
            if (actions.length > ++nextAction && (a = actions[nextAction]) != null && a.isEnabled()) {
                a.actionPerformed(e);
            }
        }
    }

    private FileObject getFile(JTextComponent component) {
        Document doc = component.getDocument();
        DataObject od = (DataObject)doc.getProperty("stream");
        if (od == null) {
            return null;
        }
        return od.getPrimaryFile();
    }

    private IsOverriddenAnnotation findAnnotation(JTextComponent component, AnnotationDesc desc, int offset) {
        FileObject file = this.getFile(component);
        if (file == null) {
            if (ErrorManager.getDefault().isLoggable(16)) {
                ErrorManager.getDefault().log(16, "component=" + component + " does not have a file specified in the document.");
            }
            return null;
        }
        AnnotationsHolder ah = AnnotationsHolder.get(file);
        if (ah == null) {
            Logger.getLogger(IsOverriddenAnnotationAction.class.getName()).log(Level.INFO, "component=" + component + " does not have attached a IsOverriddenAnnotationHandler");
            return null;
        }
        for (IsOverriddenAnnotation a : ah.getAnnotations()) {
            if (a.getPosition().getOffset() != offset || !desc.getShortDescription().equals(a.getShortDescription())) continue;
            return a;
        }
        return null;
    }

    private List<IsOverriddenAnnotation> findAnnotations(JTextComponent component, int offset) {
        FileObject file = this.getFile(component);
        if (file == null) {
            if (ErrorManager.getDefault().isLoggable(16)) {
                ErrorManager.getDefault().log(16, "component=" + component + " does not have a file specified in the document.");
            }
            return null;
        }
        AnnotationsHolder ah = AnnotationsHolder.get(file);
        if (ah == null) {
            Logger.getLogger(IsOverriddenAnnotationAction.class.getName()).log(Level.INFO, "component=" + component + " does not have attached a IsOverriddenAnnotationHandler");
            return null;
        }
        LinkedList<IsOverriddenAnnotation> annotations = new LinkedList<IsOverriddenAnnotation>();
        for (IsOverriddenAnnotation a : ah.getAnnotations()) {
            if (a.getPosition().getOffset() != offset) continue;
            annotations.add(a);
        }
        return annotations;
    }

    boolean invokeDefaultAction(final JTextComponent comp) {
        final Document doc = comp.getDocument();
        if (doc instanceof BaseDocument) {
            final int currentPosition = comp.getCaretPosition();
            final Annotations annotations = ((BaseDocument)doc).getAnnotations();
            final LinkedHashMap<String, List<ElementDescription>> caption2Descriptions = new LinkedHashMap<String, List<ElementDescription>>();
            final Point[] p = new Point[1];
            doc.render(new Runnable(){

                @Override
                public void run() {
                    try {
                        int line = Utilities.getLineOffset((BaseDocument)((BaseDocument)doc), (int)currentPosition);
                        int startOffset = Utilities.getRowStartFromLineOffset((BaseDocument)((BaseDocument)doc), (int)line);
                        p[0] = comp.modelToView(startOffset).getLocation();
                        AnnotationDesc desc = annotations.getActiveAnnotation(line);
                        if (desc == null) {
                            return;
                        }
                        List<IsOverriddenAnnotation> annots = COMBINED_TYPES.contains(desc.getAnnotationType()) ? IsOverriddenAnnotationAction.this.findAnnotations(comp, startOffset) : Collections.singletonList(IsOverriddenAnnotationAction.this.findAnnotation(comp, desc, startOffset));
                        for (IsOverriddenAnnotation a : annots) {
                            if (a == null) continue;
                            caption2Descriptions.put(IsOverriddenAnnotationAction.computeCaption(a.getType(), a.getShortDescription()), a.getDeclarations());
                        }
                    }
                    catch (BadLocationException ex) {
                        ErrorManager.getDefault().notify((Throwable)ex);
                    }
                }
            });
            if (caption2Descriptions.isEmpty()) {
                return false;
            }
            JumpList.checkAddEntry((JTextComponent)comp, (int)currentPosition);
            IsOverriddenAnnotationAction.mouseClicked(caption2Descriptions, comp, p[0]);
            return true;
        }
        return false;
    }

    static void mouseClicked(Map<String, List<ElementDescription>> caption2Descriptions, JTextComponent c, Point p) {
        if (caption2Descriptions.size() == 1 && caption2Descriptions.values().iterator().next().size() == 1) {
            ElementDescription desc = caption2Descriptions.values().iterator().next().get(0);
            desc.open();
            return;
        }
        Point position = new Point(p);
        SwingUtilities.convertPointToScreen(position, c);
        StringBuilder caption = new StringBuilder();
        LinkedList<ElementDescription> descriptions = new LinkedList<ElementDescription>();
        boolean first = true;
        for (Map.Entry<String, List<ElementDescription>> e : caption2Descriptions.entrySet()) {
            if (!first) {
                caption.append("/");
            }
            first = false;
            caption.append(e.getKey());
            descriptions.addAll((Collection)e.getValue());
        }
        PopupUtil.showPopup(new IsOverriddenPopup(caption.toString(), descriptions), caption.toString(), position.x, position.y, true, 0);
    }

    static String computeCaption(AnnotationType type, String shortDescription) throws MissingResourceException, IllegalStateException {
        String caption;
        switch (type) {
            case IMPLEMENTS: {
                caption = NbBundle.getMessage(IsOverriddenAnnotation.class, (String)"CAP_Implements");
                break;
            }
            case OVERRIDES: {
                caption = NbBundle.getMessage(IsOverriddenAnnotation.class, (String)"CAP_Overrides");
                break;
            }
            case HAS_IMPLEMENTATION: 
            case IS_OVERRIDDEN: {
                caption = shortDescription;
                break;
            }
            default: {
                throw new IllegalStateException("Currently not implemented: " + (Object)((Object)type));
            }
        }
        return caption;
    }

}

