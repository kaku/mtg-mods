/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.Tree
 *  org.netbeans.api.java.source.CancellableTask
 *  org.netbeans.api.java.source.support.CancellableTreePathScanner
 *  org.netbeans.api.java.source.support.CancellableTreeScanner
 */
package org.netbeans.modules.java.editor.semantic;

import com.sun.source.tree.Tree;
import java.util.concurrent.atomic.AtomicBoolean;
import org.netbeans.api.java.source.CancellableTask;
import org.netbeans.api.java.source.support.CancellableTreePathScanner;
import org.netbeans.api.java.source.support.CancellableTreeScanner;

public abstract class ScanningCancellableTask<T>
implements CancellableTask<T> {
    protected AtomicBoolean canceled = new AtomicBoolean();
    private CancellableTreePathScanner pathScanner;
    private CancellableTreeScanner scanner;

    protected ScanningCancellableTask() {
    }

    public final synchronized void cancel() {
        this.canceled.set(true);
        if (this.pathScanner != null) {
            this.pathScanner.cancel();
        }
        if (this.scanner != null) {
            this.scanner.cancel();
        }
    }

    public abstract void run(T var1) throws Exception;

    protected final synchronized boolean isCancelled() {
        return this.canceled.get();
    }

    protected final synchronized void resume() {
        this.canceled.set(false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected <R, P> R scan(CancellableTreePathScanner<R, P> scanner, Tree toScan, P p) {
        if (this.isCancelled()) {
            return null;
        }
        try {
            Object object = this;
            synchronized (object) {
                this.pathScanner = scanner;
            }
            if (this.isCancelled()) {
                object = null;
                return (R)object;
            }
            object = scanner.scan(toScan, p);
            return (R)object;
        }
        finally {
            ScanningCancellableTask scanningCancellableTask = this;
            synchronized (scanningCancellableTask) {
                this.pathScanner = null;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected <R, P> R scan(CancellableTreeScanner<R, P> scanner, Tree toScan, P p) {
        if (this.isCancelled()) {
            return null;
        }
        try {
            Object object = this;
            synchronized (object) {
                this.scanner = scanner;
            }
            if (this.isCancelled()) {
                object = null;
                return (R)object;
            }
            object = scanner.scan(toScan, p);
            return (R)object;
        }
        finally {
            ScanningCancellableTask scanningCancellableTask = this;
            synchronized (scanningCancellableTask) {
                this.scanner = null;
            }
        }
    }
}

