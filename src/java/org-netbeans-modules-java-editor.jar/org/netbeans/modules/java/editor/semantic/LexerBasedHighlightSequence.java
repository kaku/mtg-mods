/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.lexer.JavadocTokenId
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.spi.editor.highlighting.HighlightsSequence
 */
package org.netbeans.modules.java.editor.semantic;

import java.util.Map;
import javax.swing.text.AttributeSet;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.lexer.JavadocTokenId;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.java.editor.semantic.ColoringAttributes;
import org.netbeans.modules.java.editor.semantic.LexerBasedHighlightLayer;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;

public class LexerBasedHighlightSequence
implements HighlightsSequence {
    private LexerBasedHighlightLayer layer;
    private Map<Token, ColoringAttributes.Coloring> colorings;
    private TokenSequence ts;
    private TokenSequence java;

    public LexerBasedHighlightSequence(LexerBasedHighlightLayer layer, TokenSequence ts, Map<Token, ColoringAttributes.Coloring> colorings) {
        this.layer = layer;
        this.ts = ts;
        this.java = ts;
        this.colorings = colorings;
    }

    public boolean moveNext() {
        Token t;
        if (this.ts != this.java) {
            while (this.ts.moveNext()) {
                t = this.ts.token();
                if (t.id() != JavadocTokenId.IDENT || t.getProperty((Object)"javadoc-identifier") == null) continue;
                return true;
            }
            this.ts = this.java;
        }
        while (this.ts.moveNext()) {
            t = this.ts.token();
            if (t.id() == JavaTokenId.JAVADOC_COMMENT) {
                this.ts = this.ts.embedded();
                return this.moveNext();
            }
            if (t.id() != JavaTokenId.IDENTIFIER || !this.colorings.containsKey((Object)this.ts.token())) continue;
            return true;
        }
        return false;
    }

    public int getStartOffset() {
        return this.ts.offset();
    }

    public int getEndOffset() {
        return this.ts.offset() + this.ts.token().length();
    }

    public AttributeSet getAttributes() {
        if (this.ts.token().id() == JavadocTokenId.IDENT) {
            return this.layer.getColoring(ColoringAttributes.add(ColoringAttributes.empty(), ColoringAttributes.JAVADOC_IDENTIFIER));
        }
        return this.layer.getColoring(this.colorings.get((Object)this.ts.token()));
    }
}

