/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.ui.ElementHeaders
 *  org.netbeans.spi.editor.highlighting.HighlightAttributeValue
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.semantic;

import com.sun.source.tree.Tree;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.io.IOException;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Name;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.ui.ElementHeaders;
import org.netbeans.modules.java.editor.semantic.SemanticHighlighter;
import org.netbeans.spi.editor.highlighting.HighlightAttributeValue;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

final class UnusedTooltipResolver
implements HighlightAttributeValue<String> {
    UnusedTooltipResolver() {
    }

    public String getValue(JTextComponent component, Document document, Object attributeKey, int startOffset, final int endOffset) {
        try {
            JavaSource js = JavaSource.forDocument((Document)document);
            if (js == null) {
                return null;
            }
            final String[] result = new String[1];
            js.runUserActionTask((Task)new Task<CompilationController>(){

                public void run(CompilationController cont) throws Exception {
                    cont.toPhase(JavaSource.Phase.RESOLVED);
                    TreePath tp = cont.getTreeUtilities().pathFor(endOffset);
                    if (tp == null) {
                        return;
                    }
                    boolean isInImport = false;
                    block15 : for (TreePath lookingFor = tp; lookingFor != null; lookingFor = lookingFor.getParentPath()) {
                        Tree t = lookingFor.getLeaf();
                        switch (t.getKind()) {
                            case IMPORT: {
                                isInImport = true;
                                break block15;
                            }
                            case ANNOTATION_TYPE: 
                            case CLASS: 
                            case ENUM: 
                            case INTERFACE: 
                            case METHOD: 
                            case BLOCK: {
                                isInImport = false;
                                break block15;
                            }
                            default: {
                                continue block15;
                            }
                        }
                    }
                    if (isInImport) {
                        result[0] = NbBundle.getMessage(SemanticHighlighter.class, (String)"LBL_UnusedImport");
                        return;
                    }
                    Element e = cont.getTrees().getElement(tp);
                    if (e == null) {
                        return;
                    }
                    String elementDisplayName = null;
                    String key = null;
                    switch (e.getKind()) {
                        case LOCAL_VARIABLE: 
                        case RESOURCE_VARIABLE: 
                        case EXCEPTION_PARAMETER: {
                            key = "LBL_UnusedVariable";
                            elementDisplayName = e.getSimpleName().toString();
                            break;
                        }
                        case PARAMETER: {
                            key = "LBL_UnusedParameter";
                            elementDisplayName = e.getSimpleName().toString();
                            break;
                        }
                        case FIELD: {
                            key = "LBL_UnusedField";
                            elementDisplayName = e.getSimpleName().toString();
                            break;
                        }
                        case METHOD: {
                            key = "LBL_UnusedMethod";
                            elementDisplayName = ElementHeaders.getHeader((Element)e, (CompilationInfo)cont, (String)"%name%%parameters%");
                            break;
                        }
                        case CONSTRUCTOR: {
                            key = "LBL_UnusedConstructor";
                            elementDisplayName = e.getEnclosingElement().getSimpleName().toString() + ElementHeaders.getHeader((Element)e, (CompilationInfo)cont, (String)"%parameters%");
                            break;
                        }
                        case CLASS: {
                            key = "LBL_UnusedClass";
                            elementDisplayName = e.getSimpleName().toString();
                            break;
                        }
                        case INTERFACE: {
                            key = "LBL_UnusedInterface";
                            elementDisplayName = e.getSimpleName().toString();
                            break;
                        }
                        case ANNOTATION_TYPE: {
                            key = "LBL_UnusedAnnotationType";
                            elementDisplayName = e.getSimpleName().toString();
                            break;
                        }
                        case ENUM: {
                            key = "LBL_UnusedEnum";
                            elementDisplayName = e.getSimpleName().toString();
                        }
                    }
                    if (elementDisplayName != null) {
                        result[0] = NbBundle.getMessage(UnusedTooltipResolver.class, (String)key, (Object)elementDisplayName);
                    }
                }
            }, true);
            return result[0];
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
            return null;
        }
    }

}

