/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.ModificationResult
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.WorkingCopy
 *  org.netbeans.editor.Utilities
 *  org.netbeans.spi.editor.codegen.CodeGenerator
 *  org.netbeans.spi.editor.codegen.CodeGenerator$Factory
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.codegen;

import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.awt.Dialog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.modules.editor.java.Utilities;
import org.netbeans.modules.java.editor.codegen.ConstructorGenerator;
import org.netbeans.modules.java.editor.codegen.GeneratorUtils;
import org.netbeans.modules.java.editor.codegen.ui.ElementNode;
import org.netbeans.modules.java.editor.codegen.ui.ImplementOverridePanel;
import org.netbeans.spi.editor.codegen.CodeGenerator;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class ImplementOverrideMethodGenerator
implements CodeGenerator {
    private final JTextComponent component;
    private final ElementHandle<TypeElement> handle;
    private final ElementNode.Description description;
    private final boolean isImplement;

    private ImplementOverrideMethodGenerator(JTextComponent component, ElementHandle<TypeElement> handle, ElementNode.Description description, boolean isImplement) {
        this.component = component;
        this.handle = handle;
        this.description = description;
        this.isImplement = isImplement;
    }

    public String getDisplayName() {
        return NbBundle.getMessage(ImplementOverrideMethodGenerator.class, (String)(this.isImplement ? "LBL_implement_method" : "LBL_override_method"));
    }

    public void invoke() {
        JavaSource js;
        final ImplementOverridePanel panel = new ImplementOverridePanel(this.description, this.isImplement);
        final int caretOffset = this.component.getCaretPosition();
        DialogDescriptor dialogDescriptor = GeneratorUtils.createDialogDescriptor(panel, NbBundle.getMessage(ConstructorGenerator.class, (String)(this.isImplement ? "LBL_generate_implement" : "LBL_generate_override")));
        Dialog dialog = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
        dialog.setVisible(true);
        if (dialogDescriptor.getValue() == dialogDescriptor.getDefaultValue() && (js = JavaSource.forDocument((Document)this.component.getDocument())) != null) {
            try {
                ModificationResult mr = js.runModificationTask((Task)new Task<WorkingCopy>(){

                    public void run(WorkingCopy copy) throws IOException {
                        copy.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                        Element e = ImplementOverrideMethodGenerator.this.handle.resolve((CompilationInfo)copy);
                        TreePath path = e != null ? copy.getTrees().getPath(e) : copy.getTreeUtilities().pathFor(caretOffset);
                        path = Utilities.getPathElementOfKind(TreeUtilities.CLASS_TREE_KINDS, path);
                        if (path == null) {
                            String message = NbBundle.getMessage(ImplementOverrideMethodGenerator.class, (String)"ERR_CannotFindOriginalClass");
                            org.netbeans.editor.Utilities.setStatusBoldText((JTextComponent)ImplementOverrideMethodGenerator.this.component, (String)message);
                        } else {
                            ArrayList<ExecutableElement> methodElements = new ArrayList<ExecutableElement>();
                            for (ElementHandle<? extends Element> elementHandle : panel.getSelectedMethods()) {
                                ExecutableElement methodElement = (ExecutableElement)elementHandle.resolve((CompilationInfo)copy);
                                if (methodElement == null) continue;
                                methodElements.add(methodElement);
                            }
                            if (!methodElements.isEmpty()) {
                                if (ImplementOverrideMethodGenerator.this.isImplement) {
                                    GeneratorUtils.generateAbstractMethodImplementations(copy, path, methodElements, caretOffset);
                                } else {
                                    GeneratorUtils.generateMethodOverrides(copy, path, methodElements, caretOffset);
                                }
                            }
                        }
                    }
                });
                GeneratorUtils.guardedCommit(this.component, mr);
                int[] span = mr.getSpan((Object)"methodBodyTag");
                if (span != null) {
                    this.component.setSelectionStart(span[0]);
                    this.component.setSelectionEnd(span[1]);
                }
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
    }

    public static class Factory
    implements CodeGenerator.Factory {
        public List<? extends CodeGenerator> create(Lookup context) {
            LinkedHashMap map;
            ArrayList<ImplementOverrideMethodGenerator> ret = new ArrayList<ImplementOverrideMethodGenerator>();
            JTextComponent component = (JTextComponent)context.lookup(JTextComponent.class);
            CompilationController controller = (CompilationController)context.lookup(CompilationController.class);
            TreePath path = (TreePath)context.lookup(TreePath.class);
            TreePath treePath = path = path != null ? Utilities.getPathElementOfKind(TreeUtilities.CLASS_TREE_KINDS, path) : null;
            if (component == null || controller == null || path == null) {
                return ret;
            }
            try {
                controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
            }
            catch (IOException ioe) {
                return ret;
            }
            TypeElement typeElement = (TypeElement)controller.getTrees().getElement(path);
            if (typeElement == null) {
                return ret;
            }
            if (typeElement.getKind().isClass() || typeElement.getKind().isInterface() && SourceVersion.RELEASE_8.compareTo(controller.getSourceVersion()) <= 0) {
                map = new LinkedHashMap();
                for (ExecutableElement method : GeneratorUtils.findUndefs((CompilationInfo)controller, typeElement)) {
                    void descriptions22;
                    List descriptions22 = (List)map.get(method.getEnclosingElement());
                    if (descriptions22 == null) {
                        ArrayList descriptions22 = new ArrayList();
                        map.put(method.getEnclosingElement(), descriptions22);
                    }
                    descriptions22.add(ElementNode.Description.create((CompilationInfo)controller, method, null, true, false));
                }
                ArrayList<ElementNode.Description> implementDescriptions = new ArrayList<ElementNode.Description>();
                for (Map.Entry entry : map.entrySet()) {
                    implementDescriptions.add(ElementNode.Description.create((CompilationInfo)controller, (Element)entry.getKey(), (List)entry.getValue(), false, false));
                }
                if (!implementDescriptions.isEmpty()) {
                    ret.add(new ImplementOverrideMethodGenerator(component, ElementHandle.create((Element)typeElement), ElementNode.Description.create(implementDescriptions), true));
                }
            }
            if (typeElement.getKind().isClass() || typeElement.getKind().isInterface()) {
                map = new LinkedHashMap();
                ArrayList<Element> orderedElements = new ArrayList<Element>();
                for (ExecutableElement method : GeneratorUtils.findOverridable((CompilationInfo)controller, typeElement)) {
                    ArrayList<ElementNode.Description> descriptions = (ArrayList<ElementNode.Description>)map.get(method.getEnclosingElement());
                    if (descriptions == null) {
                        descriptions = new ArrayList<ElementNode.Description>();
                        Element e = method.getEnclosingElement();
                        map.put(e, descriptions);
                        if (!orderedElements.contains(e)) {
                            orderedElements.add(e);
                        }
                    }
                    descriptions.add(ElementNode.Description.create((CompilationInfo)controller, method, null, true, false));
                }
                ArrayList<ElementNode.Description> overrideDescriptions = new ArrayList<ElementNode.Description>();
                for (Element e : orderedElements) {
                    overrideDescriptions.add(ElementNode.Description.create((CompilationInfo)controller, e, (List)map.get(e), false, false));
                }
                if (!overrideDescriptions.isEmpty()) {
                    ret.add(new ImplementOverrideMethodGenerator(component, ElementHandle.create((Element)typeElement), ElementNode.Description.create(overrideDescriptions), false));
                }
            }
            return ret;
        }
    }

}

