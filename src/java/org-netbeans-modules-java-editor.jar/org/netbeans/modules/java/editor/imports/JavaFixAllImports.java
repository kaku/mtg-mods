/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.AssignmentTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.Scope
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreePathScanner
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.CodeStyle
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.ElementUtilities
 *  org.netbeans.api.java.source.ElementUtilities$ElementAcceptor
 *  org.netbeans.api.java.source.GeneratorUtilities
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.ModificationResult
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.java.source.TreeMaker
 *  org.netbeans.api.java.source.TreePathHandle
 *  org.netbeans.api.java.source.WorkingCopy
 *  org.netbeans.api.java.source.support.CancellableTreePathScanner
 *  org.netbeans.api.java.source.support.ReferencesCount
 *  org.netbeans.api.java.source.ui.ElementIcons
 *  org.netbeans.api.progress.ProgressUtils
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.awt.StatusDisplayer
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.java.editor.imports;

import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.Scope;
import com.sun.source.tree.Tree;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreePathScanner;
import com.sun.source.util.Trees;
import java.awt.Dialog;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.prefs.Preferences;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.SwingUtilities;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CodeStyle;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.java.source.GeneratorUtilities;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.TreeMaker;
import org.netbeans.api.java.source.TreePathHandle;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.api.java.source.support.CancellableTreePathScanner;
import org.netbeans.api.java.source.support.ReferencesCount;
import org.netbeans.api.java.source.ui.ElementIcons;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.modules.editor.java.Utilities;
import org.netbeans.modules.java.editor.codegen.GeneratorUtils;
import org.netbeans.modules.java.editor.imports.ComputeImports;
import org.netbeans.modules.java.editor.imports.FixDuplicateImportStmts;
import org.netbeans.modules.java.editor.semantic.SemanticHighlighter;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.awt.StatusDisplayer;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;

public class JavaFixAllImports {
    public static final String NOT_VALID_IMPORT_HTML = System.getProperty(JavaFixAllImports.class.getName() + ".invalid_import_html", "");
    private static final String PREFS_KEY = JavaFixAllImports.class.getName();
    private static final String KEY_REMOVE_UNUSED_IMPORTS = "removeUnusedImports";
    private static final JavaFixAllImports INSTANCE = new JavaFixAllImports();
    private static final RequestProcessor WORKER = new RequestProcessor(JavaFixAllImports.class.getName(), 1);

    public static JavaFixAllImports getDefault() {
        return INSTANCE;
    }

    private JavaFixAllImports() {
    }

    public void fixAllImports(FileObject fo, final JTextComponent target) {
        final AtomicBoolean cancel = new AtomicBoolean();
        final JavaSource javaSource = JavaSource.forFileObject((FileObject)fo);
        final AtomicReference id = new AtomicReference();
        Task<WorkingCopy> task = new Task<WorkingCopy>(){

            public void run(WorkingCopy wc) {
                try {
                    wc.toPhase(JavaSource.Phase.RESOLVED);
                    if (cancel.get()) {
                        return;
                    }
                    ImportData data = JavaFixAllImports.computeImports((CompilationInfo)wc);
                    if (cancel.get()) {
                        return;
                    }
                    if (data.shouldShowImportsPanel) {
                        if (!cancel.get()) {
                            id.set(data);
                        }
                    } else {
                        Preferences prefs = NbPreferences.forModule(JavaFixAllImports.class).node(PREFS_KEY);
                        boolean removeUnusedImports = prefs.getBoolean("removeUnusedImports", true);
                        JavaFixAllImports.performFixImports(wc, data, data.defaults, removeUnusedImports);
                    }
                }
                catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };
        if (javaSource == null) {
            StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(JavaFixAllImports.class, (String)"MSG_CannotFixImports"));
        } else {
            ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable((Task)task, target){
                final /* synthetic */ Task val$task;
                final /* synthetic */ JTextComponent val$target;

                @Override
                public void run() {
                    try {
                        ModificationResult mr = javaSource.runModificationTask(this.val$task);
                        GeneratorUtils.guardedCommit(this.val$target, mr);
                    }
                    catch (IOException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
            }, (String)"Fix All Imports", (AtomicBoolean)cancel, (boolean)false);
            if (id.get() != null && !cancel.get()) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        JavaFixAllImports.showFixImportsDialog(javaSource, target, (ImportData)id.get());
                    }
                });
            }
        }
    }

    private static List<TreePathHandle> getImportsFromSamePackage(WorkingCopy wc) {
        ImportVisitor v = new ImportVisitor((CompilationInfo)wc);
        v.scan((Tree)wc.getCompilationUnit(), (Object)null);
        return v.getImports();
    }

    private static void performFixImports(WorkingCopy wc, ImportData data, CandidateDescription[] selections, boolean removeUnusedImports) throws IOException {
        HashSet<Element> toImport = new HashSet<Element>();
        HashMap<Name, Element> useFQNsFor = new HashMap<Name, Element>();
        CodeStyle cs = CodeStyle.getDefault((Document)wc.getDocument());
        for (CandidateDescription cd : selections) {
            Element el;
            Element element = el = cd.toImport != null ? cd.toImport.resolve((CompilationInfo)wc) : null;
            if (el == null) continue;
            if (cs.useFQNs()) {
                useFQNsFor.put(el.getSimpleName(), el);
                continue;
            }
            toImport.add(el);
        }
        CompilationUnitTree cut = wc.getCompilationUnit();
        if (!toImport.isEmpty()) {
            cut = GeneratorUtilities.get((WorkingCopy)wc).addImports(cut, toImport);
        }
        if (!useFQNsFor.isEmpty()) {
            new TreeVisitorImpl(wc, useFQNsFor).scan((Tree)cut, (Object)null);
        }
        boolean someImportsWereRemoved = false;
        if (removeUnusedImports) {
            List<TreePathHandle> unusedImports = SemanticHighlighter.computeUnusedImports((CompilationInfo)wc);
            unusedImports.addAll(JavaFixAllImports.getImportsFromSamePackage(wc));
            someImportsWereRemoved = !unusedImports.isEmpty();
            for (TreePathHandle handle : unusedImports) {
                TreePath path = handle.resolve((CompilationInfo)wc);
                assert (path != null);
                cut = wc.getTreeMaker().removeCompUnitImport(cut, (ImportTree)path.getLeaf());
            }
        }
        wc.rewrite((Tree)wc.getCompilationUnit(), (Tree)cut);
        if (!data.shouldShowImportsPanel) {
            String statusText;
            if (toImport.isEmpty() && useFQNsFor.isEmpty() && !someImportsWereRemoved) {
                Toolkit.getDefaultToolkit().beep();
                statusText = NbBundle.getMessage(JavaFixAllImports.class, (String)"MSG_NothingToFix");
            } else {
                statusText = toImport.isEmpty() && someImportsWereRemoved ? NbBundle.getMessage(JavaFixAllImports.class, (String)"MSG_UnusedImportsRemoved") : NbBundle.getMessage(JavaFixAllImports.class, (String)"MSG_ImportsFixed");
            }
            StatusDisplayer.getDefault().setStatusText(statusText);
        }
    }

    private static ImportData computeImports(CompilationInfo info) {
        ComputeImports.Pair<Map<String, List<Element>>, Map<String, List<Element>>> candidates = new ComputeImports().computeCandidates(info);
        Map filteredCandidates = (Map)candidates.a;
        Map notFilteredCandidates = (Map)candidates.b;
        int size = notFilteredCandidates.size();
        ImportData data = new ImportData(size);
        ReferencesCount referencesCount = ReferencesCount.get((ClasspathInfo)info.getClasspathInfo());
        int index = 0;
        boolean shouldShowImportsPanel = false;
        Iterator i$ = notFilteredCandidates.keySet().iterator();
        while (i$.hasNext()) {
            String key;
            data.simpleNames[index] = key = (String)i$.next();
            List unfilteredVars = (List)notFilteredCandidates.get(key);
            List filteredVars = (List)filteredCandidates.get(key);
            shouldShowImportsPanel |= unfilteredVars.size() > 1;
            if (!unfilteredVars.isEmpty()) {
                String displayName;
                int level;
                Icon icon;
                boolean staticImports = true;
                for (Element e : unfilteredVars) {
                    if (!e.getKind().isClass() && !e.getKind().isInterface()) continue;
                    staticImports = false;
                }
                shouldShowImportsPanel |= staticImports;
                data.variants[index] = new CandidateDescription[staticImports ? unfilteredVars.size() + 1 : unfilteredVars.size()];
                int i = -1;
                int minImportanceLevel = Integer.MAX_VALUE;
                for (Element e22 : filteredVars) {
                    displayName = ComputeImports.displayNameForImport(info, e22);
                    icon = ElementIcons.getElementIcon((ElementKind)e22.getKind(), e22.getModifiers());
                    data.variants[index][++i] = new CandidateDescription(displayName, icon, ElementHandle.create((Element)e22));
                    level = Utilities.getImportanceLevel(info, referencesCount, e22);
                    if (level >= minImportanceLevel) continue;
                    data.defaults[index] = data.variants[index][i];
                    minImportanceLevel = level;
                }
                if (data.defaults[index] != null) {
                    minImportanceLevel = Integer.MIN_VALUE;
                }
                for (Element e22 : unfilteredVars) {
                    if (filteredVars.contains(e22)) continue;
                    displayName = NOT_VALID_IMPORT_HTML + ComputeImports.displayNameForImport(info, e22);
                    icon = ElementIcons.getElementIcon((ElementKind)e22.getKind(), e22.getModifiers());
                    data.variants[index][++i] = new CandidateDescription(displayName, icon, ElementHandle.create((Element)e22));
                    level = Utilities.getImportanceLevel(info, referencesCount, e22);
                    if (level >= minImportanceLevel) continue;
                    data.defaults[index] = data.variants[index][i];
                    minImportanceLevel = level;
                }
                if (staticImports) {
                    data.variants[index][++i] = new CandidateDescription(NbBundle.getMessage(JavaFixAllImports.class, (String)"FixDupImportStmts_DoNotImport"), ImageUtilities.loadImageIcon((String)"org/netbeans/modules/java/editor/resources/error-glyph.gif", (boolean)false), null);
                }
            } else {
                data.variants[index] = new CandidateDescription[1];
                data.variants[index][0] = new CandidateDescription(NbBundle.getMessage(JavaFixAllImports.class, (String)"FixDupImportStmts_CannotResolve"), ImageUtilities.loadImageIcon((String)"org/netbeans/modules/java/editor/resources/error-glyph.gif", (boolean)false), null);
                data.defaults[index] = data.variants[index][0];
            }
            ++index;
        }
        data.shouldShowImportsPanel = shouldShowImportsPanel;
        return data;
    }

    private static void showFixImportsDialog(final JavaSource js, final JTextComponent target, final ImportData data) {
        final Preferences prefs = NbPreferences.forModule(JavaFixAllImports.class).node(PREFS_KEY);
        final FixDuplicateImportStmts panel = new FixDuplicateImportStmts();
        panel.initPanel(data, prefs.getBoolean("removeUnusedImports", true));
        final JButton ok = new JButton("OK");
        final JButton cancel = new JButton("Cancel");
        final AtomicBoolean stop = new AtomicBoolean();
        DialogDescriptor dd = new DialogDescriptor((Object)panel, NbBundle.getMessage(JavaFixAllImports.class, (String)"FixDupImportStmts_Title"), true, new Object[]{ok, cancel}, (Object)ok, 0, HelpCtx.DEFAULT_HELP, new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
            }
        }, true);
        final Dialog d = DialogDisplayer.getDefault().createDialog(dd);
        ok.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                ok.setEnabled(false);
                final CandidateDescription[] selections = panel.getSelections();
                final boolean removeUnusedImports = panel.getRemoveUnusedImports();
                WORKER.post(new Runnable(){

                    @Override
                    public void run() {
                        try {
                            ModificationResult mr = js.runModificationTask((Task)new Task<WorkingCopy>(){

                                public void run(WorkingCopy wc) throws Exception {
                                    SwingUtilities.invokeLater(new Runnable(){

                                        @Override
                                        public void run() {
                                            cancel.setEnabled(false);
                                            ((JDialog)d).setDefaultCloseOperation(0);
                                        }
                                    });
                                    wc.toPhase(JavaSource.Phase.RESOLVED);
                                    if (stop.get()) {
                                        return;
                                    }
                                    JavaFixAllImports.performFixImports(wc, data, selections, removeUnusedImports);
                                }

                            });
                            GeneratorUtils.guardedCommit(target, mr);
                        }
                        catch (IOException ex) {
                            Exceptions.printStackTrace((Throwable)ex);
                        }
                        prefs.putBoolean("removeUnusedImports", removeUnusedImports);
                        SwingUtilities.invokeLater(new Runnable(){

                            @Override
                            public void run() {
                                d.setVisible(false);
                            }
                        });
                    }

                });
            }

        });
        cancel.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                stop.set(true);
                d.setVisible(false);
            }
        });
        d.setVisible(true);
        d.dispose();
    }

    private static class TreeVisitorImpl
    extends CancellableTreePathScanner<Void, Void> {
        private WorkingCopy wc;
        private Map<Name, Element> name2Element;

        public TreeVisitorImpl(WorkingCopy wc, Map<Name, Element> name2Element) {
            this.wc = wc;
            this.name2Element = name2Element;
        }

        public Void visitIdentifier(IdentifierTree node, Void p) {
            TypeMirror type;
            Void ret = (Void)CancellableTreePathScanner.super.visitIdentifier(node, (Object)p);
            final Element el = this.wc.getTrees().getElement(this.getCurrentPath());
            if (el != null && (el.getKind().isClass() || el.getKind().isInterface() || el.getKind() == ElementKind.PACKAGE) && (type = el.asType()) != null) {
                if (type.getKind() == TypeKind.ERROR) {
                    Element e;
                    boolean allowImport = true;
                    if (this.getCurrentPath().getParentPath() != null) {
                        if (this.getCurrentPath().getParentPath().getLeaf().getKind() == Tree.Kind.ASSIGNMENT) {
                            AssignmentTree at = (AssignmentTree)this.getCurrentPath().getParentPath().getLeaf();
                            allowImport = at.getVariable() != node;
                        } else if (this.getCurrentPath().getParentPath().getLeaf().getKind() == Tree.Kind.METHOD_INVOCATION) {
                            for (Scope s = this.wc.getTrees().getScope((TreePath)this.getCurrentPath()); s != null; s = s.getEnclosingScope()) {
                                allowImport &= !this.wc.getElementUtilities().getLocalMembersAndVars(s, new ElementUtilities.ElementAcceptor(){

                                    public boolean accept(Element e, TypeMirror type) {
                                        return e.getSimpleName().contentEquals(el.getSimpleName());
                                    }
                                }).iterator().hasNext();
                            }
                        }
                    }
                    if (allowImport && (e = this.name2Element.get(node.getName())) != null) {
                        this.wc.rewrite((Tree)node, (Tree)this.wc.getTreeMaker().QualIdent(e));
                    }
                } else if (type.getKind() == TypeKind.PACKAGE) {
                    Element e;
                    String s = ((PackageElement)el).getQualifiedName().toString();
                    if (this.wc.getElements().getPackageElement(s) == null && (e = this.name2Element.get(node.getName())) != null) {
                        this.wc.rewrite((Tree)node, (Tree)this.wc.getTreeMaker().QualIdent(e));
                    }
                }
            }
            return ret;
        }

    }

    static final class CandidateDescription {
        public final String displayName;
        public final Icon icon;
        public final ElementHandle<Element> toImport;

        public CandidateDescription(String displayName, Icon icon, ElementHandle<Element> toImport) {
            this.displayName = displayName;
            this.icon = icon;
            this.toImport = toImport;
        }
    }

    static final class ImportData {
        public final String[] simpleNames;
        public final CandidateDescription[][] variants;
        public final CandidateDescription[] defaults;
        public boolean shouldShowImportsPanel;

        public ImportData(int size) {
            this.simpleNames = new String[size];
            this.variants = new CandidateDescription[size][];
            this.defaults = new CandidateDescription[size];
        }
    }

    private static class ImportVisitor
    extends TreePathScanner {
        private CompilationInfo info;
        private String currentPackage;
        private List<TreePathHandle> imports;

        private ImportVisitor(CompilationInfo info) {
            this.info = info;
            ExpressionTree pkg = info.getCompilationUnit().getPackageName();
            this.currentPackage = pkg != null ? pkg.toString() : "";
            this.imports = new ArrayList<TreePathHandle>();
        }

        public Object visitImport(ImportTree node, Object d) {
            ExpressionTree exp;
            if (node.getQualifiedIdentifier().getKind() == Tree.Kind.MEMBER_SELECT && (exp = ((MemberSelectTree)node.getQualifiedIdentifier()).getExpression()).toString().equals(this.currentPackage)) {
                this.imports.add(TreePathHandle.create((TreePath)this.getCurrentPath(), (CompilationInfo)this.info));
            }
            super.visitImport(node, (Object)null);
            return null;
        }

        List<TreePathHandle> getImports() {
            return this.imports;
        }
    }

}

