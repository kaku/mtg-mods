/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.util.TreePath
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.ModificationResult
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.java.source.TreeMaker
 *  org.netbeans.api.java.source.TreePathHandle
 *  org.netbeans.api.java.source.WorkingCopy
 *  org.netbeans.spi.editor.hints.ChangeInfo
 *  org.netbeans.spi.editor.hints.Fix
 *  org.netbeans.spi.editor.hints.Severity
 *  org.openide.awt.StatusDisplayer
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 */
package org.netbeans.modules.java.editor.semantic;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.TreePath;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.TreeMaker;
import org.netbeans.api.java.source.TreePathHandle;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.modules.java.editor.imports.JavaFixAllImports;
import org.netbeans.spi.editor.hints.ChangeInfo;
import org.netbeans.spi.editor.hints.Fix;
import org.netbeans.spi.editor.hints.Severity;
import org.openide.awt.StatusDisplayer;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

public class RemoveUnusedImportFix
implements Fix {
    public static final String IS_ENABLED_KEY = "Enabled";
    private static final String SEVERITY_KEY = "Severity";
    private static Preferences preferences;
    private FileObject file;
    private List<TreePathHandle> importsToRemove;
    private String bundleKey;
    private static Map<Severity, Integer> severity2index;

    public static RemoveUnusedImportFix create(FileObject file, TreePathHandle importToRemove) {
        return new RemoveUnusedImportFix(file, Collections.singletonList(importToRemove), "FIX_Remove_Unused_Import");
    }

    public static RemoveUnusedImportFix create(FileObject file, List<TreePathHandle> importsToRemove) {
        return new RemoveUnusedImportFix(file, importsToRemove, "FIX_All_Remove_Unused_Import");
    }

    private RemoveUnusedImportFix(FileObject file, List<TreePathHandle> importsToRemove, String bundleKey) {
        this.file = file;
        this.importsToRemove = importsToRemove;
        this.bundleKey = bundleKey;
    }

    public String getText() {
        return NbBundle.getMessage(RemoveUnusedImportFix.class, (String)this.bundleKey);
    }

    public ChangeInfo implement() {
        JavaSource js = JavaSource.forFileObject((FileObject)this.file);
        if (js == null) {
            StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(JavaFixAllImports.class, (String)"MSG_CannotFixImports"));
        } else {
            try {
                js.runModificationTask((Task)new Task<WorkingCopy>(){

                    public void run(WorkingCopy copy) throws Exception {
                        copy.toPhase(JavaSource.Phase.PARSED);
                        CompilationUnitTree nueCUT = copy.getCompilationUnit();
                        for (TreePathHandle handle : RemoveUnusedImportFix.this.importsToRemove) {
                            TreePath tp = handle.resolve((CompilationInfo)copy);
                            if (tp == null) {
                                Logger.getLogger(RemoveUnusedImportFix.class.getName()).info("Cannot resolve import to remove.");
                                return;
                            }
                            nueCUT = copy.getTreeMaker().removeCompUnitImport(nueCUT, (ImportTree)tp.getLeaf());
                        }
                        copy.rewrite((Tree)copy.getCompilationUnit(), (Tree)nueCUT);
                    }
                }).commit();
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return null;
    }

    private static synchronized Preferences getPreferences() {
        if (preferences == null) {
            preferences = NbPreferences.forModule(RemoveUnusedImportFix.class);
        }
        return preferences;
    }

    public static synchronized boolean isEnabled() {
        return RemoveUnusedImportFix.getPreferences().getBoolean("Enabled", true);
    }

    public static void setEnabled(boolean enabled) {
        RemoveUnusedImportFix.getPreferences().putBoolean("Enabled", enabled);
    }

    public static Severity getSeverity() {
        int severity = RemoveUnusedImportFix.getPreferences().getInt("Severity", 1);
        for (Map.Entry<Severity, Integer> e : severity2index.entrySet()) {
            if (e.getValue() != severity) continue;
            return e.getKey();
        }
        return Severity.VERIFIER;
    }

    public static void setSeverity(Severity severity) {
        RemoveUnusedImportFix.getPreferences().putInt("Severity", severity2index.get((Object)severity));
    }

    static {
        severity2index = new HashMap<Severity, Integer>();
        severity2index.put(Severity.ERROR, 0);
        severity2index.put(Severity.VERIFIER, 1);
        severity2index.put(Severity.HINT, 2);
    }

}

