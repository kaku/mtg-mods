/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CaseTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.NewClassTree
 *  com.sun.source.tree.Scope
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreePathScanner
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementUtilities
 *  org.netbeans.api.java.source.GeneratorUtilities
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.ModificationResult
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.WorkingCopy
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.ActionFactory
 *  org.netbeans.editor.ActionFactory$CutToLineBeginOrEndAction
 *  org.netbeans.editor.BaseKit
 *  org.netbeans.editor.BaseKit$CutAction
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.NbEditorUtilities
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.filesystems.FileObject
 *  org.openide.text.NbDocument
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.java.editor.imports;

import com.sun.source.tree.CaseTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.Scope;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreePathScanner;
import com.sun.source.util.Trees;
import java.awt.Dialog;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.java.source.GeneratorUtilities;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.ActionFactory;
import org.netbeans.editor.BaseKit;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.modules.java.editor.imports.ClipboardImportPanel;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.filesystems.FileObject;
import org.openide.text.NbDocument;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

public class ClipboardHandler {
    private static final Logger LOG = Logger.getLogger(ClipboardHandler.class.getName());
    private static final RequestProcessor WORKER = new RequestProcessor(ClipboardHandler.class.getName(), 3, false, false);
    static boolean autoImport = false;
    private static final Object NO_IMPORTS = new Object();
    private static final Object RUN_SYNCHRONOUSLY = new Object();
    private static final DataFlavor IMPORT_FLAVOR = new DataFlavor(ImportsWrapper.class, NbBundle.getMessage(ClipboardHandler.class, (String)"MSG_ClipboardImportFlavor"));
    private static final DataFlavor COPY_FROM_STRING_FLAVOR = new DataFlavor(Boolean.class, NbBundle.getMessage(ClipboardHandler.class, (String)"MSG_ClipboardCopyFromStringFlavor"));

    public static void install(JTextComponent c) {
        c.setTransferHandler(new ImportingTransferHandler(c.getTransferHandler()));
    }

    private static void doImport(JavaSource js, final Document doc, final int caret, final Map<String, String> simple2ImportFQN, final List<Position[]> inSpans, AtomicBoolean cancel) {
        final HashMap putFQNs = new HashMap();
        try {
            final ModificationResult mr = js.runModificationTask((Task)new Task<WorkingCopy>(){

                public void run(WorkingCopy copy) throws Exception {
                    copy.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                    TreePath context = copy.getTreeUtilities().pathFor(caret);
                    Scope scope = copy.getTrees().getScope(context);
                    Scope cutScope = copy.getTrees().getScope(new TreePath(context.getCompilationUnit()));
                    ArrayList spans = new ArrayList(inSpans);
                    Collections.sort(spans, new Comparator<Position[]>(){

                        @Override
                        public int compare(Position[] o1, Position[] o2) {
                            return o1[0].getOffset() - o2[0].getOffset();
                        }
                    });
                    HashMap<String, String> imported = new HashMap<String, String>();
                    for (Position[] span : spans) {
                        String currentSimpleName = copy.getText().substring(span[0].getOffset(), span[1].getOffset());
                        String handled = (String)imported.get(currentSimpleName);
                        if (handled == null) {
                            String fqn = (String)simple2ImportFQN.get(currentSimpleName);
                            Element e = ClipboardHandler.fqn2element(copy.getElements(), fqn);
                            if (e == null) continue;
                            if (e.getKind().isClass() || e.getKind().isInterface()) {
                                handled = SourceUtils.resolveImport((CompilationInfo)copy, (TreePath)context, (String)fqn);
                            } else {
                                CompilationUnitTree cut = (CompilationUnitTree)copy.resolveRewriteTarget((Tree)copy.getCompilationUnit());
                                if (e.getModifiers().contains((Object)Modifier.STATIC) && copy.getTrees().isAccessible(cutScope, e, (DeclaredType)e.getEnclosingElement().asType()) && (scope.getEnclosingClass() == null || copy.getElementUtilities().outermostTypeElement(e) != copy.getElementUtilities().outermostTypeElement((Element)scope.getEnclosingClass()))) {
                                    copy.rewrite((Tree)copy.getCompilationUnit(), (Tree)GeneratorUtilities.get((WorkingCopy)copy).addImports(cut, Collections.singleton(e)));
                                }
                                handled = e.getSimpleName().toString();
                            }
                            imported.put(currentSimpleName, handled);
                        }
                        putFQNs.put(span, handled);
                    }
                }

            });
            if (cancel.get()) {
                return;
            }
            NbDocument.runAtomicAsUser((StyledDocument)((StyledDocument)doc), (Runnable)new Runnable(){

                @Override
                public void run() {
                    try {
                        mr.commit();
                        for (Map.Entry e : putFQNs.entrySet()) {
                            doc.remove(((Position[])e.getKey())[0].getOffset(), ((Position[])e.getKey())[1].getOffset() - ((Position[])e.getKey())[0].getOffset());
                            doc.insertString(((Position[])e.getKey())[0].getOffset(), (String)e.getValue(), null);
                        }
                    }
                    catch (BadLocationException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                    catch (IOException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
            });
        }
        catch (BadLocationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    private static void showImportDialog(final JavaSource js, final Document doc, final int caret, final Map<String, String> simple2ImportFQN, Collection<String> toShow, final List<Position[]> inSpans) {
        if (autoImport) {
            ClipboardHandler.doImport(js, doc, caret, simple2ImportFQN, inSpans, new AtomicBoolean());
            return;
        }
        ClipboardImportPanel panel = new ClipboardImportPanel(toShow);
        final AtomicBoolean cancel = new AtomicBoolean();
        final JButton okButton = new JButton(NbBundle.getMessage(ClipboardHandler.class, (String)"BTN_ClipboardImportOK"));
        JButton cancelButton = new JButton(NbBundle.getMessage(ClipboardHandler.class, (String)"BTN_ClipboardImportCancel"));
        DialogDescriptor dd = new DialogDescriptor((Object)panel, NbBundle.getMessage(ClipboardHandler.class, (String)"MSG_ClipboardImportImportClasses"), true, new Object[]{okButton, cancelButton}, (Object)okButton, 0, null, new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
            }
        });
        final Dialog[] d = new Dialog[1];
        okButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                okButton.setEnabled(false);
                WORKER.post(new Runnable(){

                    @Override
                    public void run() {
                        ClipboardHandler.doImport(js, doc, caret, simple2ImportFQN, inSpans, cancel);
                        SwingUtilities.invokeLater(new Runnable(){

                            @Override
                            public void run() {
                                d[0].setVisible(false);
                                d[0].dispose();
                            }
                        });
                    }

                });
            }

        });
        cancelButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                cancel.set(true);
                d[0].setVisible(false);
                d[0].dispose();
            }
        });
        d[0] = DialogDisplayer.getDefault().createDialog(dd);
        d[0].setVisible(true);
    }

    private static Collection<? extends String> needsImports(JavaSource js, final int caret, final Map<String, String> simple2FQNs) {
        final ArrayList unavailable = new ArrayList();
        boolean finished = ClipboardHandler.runQuickly(js, new Task<CompilationController>(){

            public void run(CompilationController cc) throws Exception {
                cc.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                TreePath tp = cc.getTreeUtilities().pathFor(caret);
                Scope context = cc.getTrees().getScope(tp);
                SourcePositions[] sps = new SourcePositions[1];
                block0 : for (Map.Entry e : simple2FQNs.entrySet()) {
                    Element el = ClipboardHandler.fqn2element(cc.getElements(), (String)e.getValue());
                    if (el == null) continue;
                    if (el.getKind().isClass() || el.getKind().isInterface()) {
                        ExpressionTree simpleName = cc.getTreeUtilities().parseExpression((String)e.getKey() + ".class", sps);
                        cc.getTreeUtilities().attributeTree((Tree)simpleName, context);
                        Element elm = cc.getTrees().getElement(new TreePath(tp, (Tree)((MemberSelectTree)simpleName).getExpression()));
                        if (el.equals(elm)) {
                            continue;
                        }
                    } else {
                        if (!cc.getTrees().isAccessible(context, el, (DeclaredType)el.getEnclosingElement().asType()) || context.getEnclosingClass() != null && (cc.getElementUtilities().outermostTypeElement(el) == cc.getElementUtilities().outermostTypeElement((Element)context.getEnclosingClass()) || cc.getElements().getAllMembers(context.getEnclosingClass()).contains(el))) continue;
                        for (ImportTree importTree : cc.getCompilationUnit().getImports()) {
                            if (!importTree.isStatic() || importTree.getQualifiedIdentifier().getKind() != Tree.Kind.MEMBER_SELECT) continue;
                            MemberSelectTree mst = (MemberSelectTree)importTree.getQualifiedIdentifier();
                            Element elm = cc.getTrees().getElement(TreePath.getPath((CompilationUnitTree)cc.getCompilationUnit(), (Tree)mst.getExpression()));
                            if (!el.getEnclosingElement().equals(elm) || !"*".contentEquals(mst.getIdentifier()) && !el.getSimpleName().contentEquals(mst.getIdentifier())) continue;
                            continue block0;
                        }
                    }
                    unavailable.add(e.getValue());
                }
            }
        });
        if (finished) {
            return unavailable;
        }
        return null;
    }

    private static Element fqn2element(Elements elements, String fqn) {
        if (fqn == null) {
            return null;
        }
        TypeElement type = elements.getTypeElement(fqn);
        if (type != null) {
            return type;
        }
        int idx = fqn.lastIndexOf(46);
        if (idx > 0) {
            type = elements.getTypeElement(fqn.substring(0, idx));
            String name = fqn.substring(idx + 1);
            if (type != null && name.length() > 0) {
                for (Element el : type.getEnclosedElements()) {
                    if (!el.getModifiers().contains((Object)Modifier.STATIC) || !name.contentEquals(el.getSimpleName())) continue;
                    return el;
                }
            }
        }
        return null;
    }

    private static boolean runQuickly(final JavaSource js, final Task<CompilationController> task) {
        boolean finished;
        final CountDownLatch started = new CountDownLatch(1);
        final AtomicBoolean cancel = new AtomicBoolean();
        RequestProcessor.Task t = WORKER.post(new Runnable(){

            @Override
            public void run() {
                try {
                    js.runUserActionTask((Task)new Task<CompilationController>(){

                        public void run(CompilationController parameter) throws Exception {
                            started.countDown();
                            if (cancel.get()) {
                                return;
                            }
                            task.run((Object)parameter);
                        }
                    }, true);
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }

        });
        try {
            finished = started.await(100, TimeUnit.MILLISECONDS);
        }
        catch (InterruptedException ex) {
            Exceptions.printStackTrace((Throwable)ex);
            finished = false;
        }
        if (finished) {
            try {
                finished = t.waitFinished(1000);
            }
            catch (InterruptedException ex) {
                Exceptions.printStackTrace((Throwable)ex);
                finished = false;
            }
        } else {
            cancel.set(true);
        }
        return finished;
    }

    public static class JavaCutToLineBeginOrEndAction
    extends ActionFactory.CutToLineBeginOrEndAction {
        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void actionPerformed(final ActionEvent evt, final JTextComponent target) {
            boolean finished22;
            Document doc = target.getDocument();
            JavaSource js = JavaSource.forDocument((Document)doc);
            final Object lock = new Object();
            final AtomicBoolean cancel = new AtomicBoolean();
            final AtomicBoolean alreadyRunning = new AtomicBoolean();
            if (js != null && !DocumentUtilities.isWriteLocked((Document)doc) && (finished22 = ClipboardHandler.runQuickly(js, (Task)new Task<CompilationController>(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                public void run(CompilationController parameter) throws Exception {
                    Object object = lock;
                    synchronized (object) {
                        if (cancel.get()) {
                            return;
                        }
                        alreadyRunning.set(true);
                    }
                    try {
                        target.putClientProperty(RUN_SYNCHRONOUSLY, true);
                        JavaCutToLineBeginOrEndAction.this.actionPerformed(evt, target);
                    }
                    finally {
                        target.putClientProperty(RUN_SYNCHRONOUSLY, null);
                    }
                }
            }))) {
                return;
            }
            Object finished22 = lock;
            synchronized (finished22) {
                if (alreadyRunning.get()) {
                    return;
                }
                cancel.set(true);
            }
            try {
                target.putClientProperty(NO_IMPORTS, true);
                super.actionPerformed(evt, target);
            }
            finally {
                target.putClientProperty(NO_IMPORTS, null);
            }
        }

    }

    public static final class JavaCutAction
    extends BaseKit.CutAction {
        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void actionPerformed(final ActionEvent evt, final JTextComponent target) {
            Object work;
            Document doc = target.getDocument();
            JavaSource js = JavaSource.forDocument((Document)doc);
            final Object lock = new Object();
            final AtomicBoolean cancel = new AtomicBoolean();
            final AtomicBoolean alreadyRunning = new AtomicBoolean();
            if (js != null) {
                work = new Task<CompilationController>(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    public void run(CompilationController parameter) throws Exception {
                        Object object = lock;
                        synchronized (object) {
                            if (cancel.get()) {
                                return;
                            }
                            alreadyRunning.set(true);
                        }
                        try {
                            target.putClientProperty(RUN_SYNCHRONOUSLY, true);
                            JavaCutAction.this.actionPerformed(evt, target);
                        }
                        finally {
                            target.putClientProperty(RUN_SYNCHRONOUSLY, null);
                        }
                    }
                };
                if (target.getClientProperty(RUN_SYNCHRONOUSLY) == null) {
                    boolean finished;
                    if (!DocumentUtilities.isWriteLocked((Document)doc) && (finished = ClipboardHandler.runQuickly(js, (Task)work))) {
                        return;
                    }
                } else {
                    try {
                        js.runUserActionTask((Task)work, true);
                    }
                    catch (IOException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                    return;
                }
            }
            work = lock;
            synchronized (work) {
                if (alreadyRunning.get()) {
                    return;
                }
                cancel.set(true);
            }
            try {
                target.putClientProperty(NO_IMPORTS, true);
                super.actionPerformed(evt, target);
            }
            finally {
                target.putClientProperty(NO_IMPORTS, null);
            }
        }

    }

    public static final class ImportsWrapper {
        private final FileObject sourceFO;
        private final Map<String, String> simple2ImportFQN;
        private final List<int[]> identifiers;

        public ImportsWrapper(FileObject sourceFO, Map<String, String> simple2ImportFQN, List<int[]> identifiers) {
            this.sourceFO = sourceFO;
            this.simple2ImportFQN = simple2ImportFQN;
            this.identifiers = identifiers;
        }
    }

    private static final class WrappedTransferable
    implements Transferable {
        private final Transferable delegate;
        private final ImportsWrapper importsData;
        private final boolean copiedFromString;
        private DataFlavor[] transferDataFlavorsCache;

        public WrappedTransferable(Transferable delegate, ImportsWrapper importsData, boolean copiedFromString) {
            this.delegate = delegate;
            this.importsData = importsData;
            this.copiedFromString = copiedFromString;
        }

        @Override
        public synchronized DataFlavor[] getTransferDataFlavors() {
            if (this.transferDataFlavorsCache != null) {
                return this.transferDataFlavorsCache;
            }
            DataFlavor[] f = this.delegate.getTransferDataFlavors();
            DataFlavor[] result = Arrays.copyOf(f, f.length + (this.importsData != null ? 1 : 0) + (this.copiedFromString ? 1 : 0));
            if (this.importsData != null) {
                result[f.length] = IMPORT_FLAVOR;
            }
            if (this.copiedFromString) {
                result[result.length - 1] = COPY_FROM_STRING_FLAVOR;
            }
            this.transferDataFlavorsCache = result;
            return this.transferDataFlavorsCache;
        }

        @Override
        public boolean isDataFlavorSupported(DataFlavor flavor) {
            return IMPORT_FLAVOR.equals(flavor) && this.importsData != null || COPY_FROM_STRING_FLAVOR.equals(flavor) && this.copiedFromString || this.delegate.isDataFlavorSupported(flavor);
        }

        @Override
        public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
            if (IMPORT_FLAVOR.equals(flavor)) {
                return this.importsData;
            }
            if (COPY_FROM_STRING_FLAVOR.equals(flavor)) {
                return this.copiedFromString;
            }
            return this.delegate.getTransferData(flavor);
        }
    }

    private static final class ImportingTransferHandler
    extends TransferHandler {
        private final TransferHandler delegate;

        public ImportingTransferHandler(TransferHandler delegate) {
            this.delegate = delegate;
        }

        @Override
        public boolean canImport(TransferHandler.TransferSupport support) {
            return this.delegate.canImport(support);
        }

        @Override
        public boolean canImport(JComponent comp, DataFlavor[] transferFlavors) {
            return this.delegate.canImport(comp, transferFlavors);
        }

        @Override
        protected Transferable createTransferable(JComponent c) {
            try {
                Method method = this.delegate.getClass().getDeclaredMethod("createTransferable", JComponent.class);
                method.setAccessible(true);
                return (Transferable)method.invoke(this.delegate, c);
            }
            catch (NoSuchMethodException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            catch (IllegalAccessException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            catch (InvocationTargetException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            return null;
        }

        @Override
        public void exportAsDrag(JComponent comp, InputEvent e, int action) {
            this.delegate.exportAsDrag(comp, e, action);
        }

        @Override
        protected void exportDone(JComponent source, Transferable data, int action) {
            try {
                Method method = this.delegate.getClass().getDeclaredMethod("exportDone", JComponent.class, Transferable.class, Integer.TYPE);
                method.setAccessible(true);
                method.invoke(this.delegate, source, data, new Integer(action));
            }
            catch (NoSuchMethodException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            catch (IllegalAccessException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            catch (InvocationTargetException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }

        @Override
        public void exportToClipboard(JComponent comp, Clipboard clip, int action) throws IllegalStateException {
            ImportsWrapper iw = null;
            boolean copiedFromString = false;
            if (comp instanceof JTextComponent) {
                JavaSource js;
                copiedFromString = this.insideToken((JTextComponent)comp, JavaTokenId.STRING_LITERAL, JavaTokenId.CHAR_LITERAL);
                if (comp.getClientProperty(NO_IMPORTS) == null && (js = JavaSource.forDocument((Document)((JTextComponent)comp).getDocument())) != null) {
                    JTextComponent tc = (JTextComponent)comp;
                    final int start = tc.getSelectionStart();
                    final int end = tc.getSelectionEnd();
                    final HashMap<String, String> simple2ImportFQN = new HashMap<String, String>();
                    final ArrayList<int[]> spans = new ArrayList<int[]>();
                    Task<CompilationController> w = new Task<CompilationController>(){

                        public void run(final CompilationController parameter) throws Exception {
                            parameter.toPhase(JavaSource.Phase.RESOLVED);
                            new TreePathScanner<Void, Void>(){
                                private final Set<Element> declaredInCopiedText;
                                private Tree lastType;

                                public Void visitIdentifier(IdentifierTree node, Void p) {
                                    int s = (int)parameter.getTrees().getSourcePositions().getStartPosition(parameter.getCompilationUnit(), (Tree)node);
                                    int e = (int)parameter.getTrees().getSourcePositions().getEndPosition(parameter.getCompilationUnit(), (Tree)node);
                                    Element el = parameter.getTrees().getElement(this.getCurrentPath());
                                    if (s >= start && e >= start && e <= end && el != null && !this.declaredInCopiedText.contains(el)) {
                                        if (el.getKind().isClass() || el.getKind().isInterface()) {
                                            TreePath parentPath = this.getCurrentPath().getParentPath();
                                            if (parentPath == null || parentPath.getLeaf().getKind() != Tree.Kind.NEW_CLASS || ((NewClassTree)parentPath.getLeaf()).getEnclosingExpression() == null || ((NewClassTree)parentPath.getLeaf()).getIdentifier() != node) {
                                                simple2ImportFQN.put(el.getSimpleName().toString(), ((TypeElement)el).getQualifiedName().toString());
                                                spans.add(new int[]{s - start, e - start});
                                            }
                                        } else if (el.getKind() == ElementKind.ENUM_CONSTANT) {
                                            TreePath parentPath = this.getCurrentPath().getParentPath();
                                            if (parentPath.getLeaf().getKind() != Tree.Kind.CASE || ((CaseTree)parentPath.getLeaf()).getExpression() != node) {
                                                simple2ImportFQN.put(el.getSimpleName().toString(), ((TypeElement)el.getEnclosingElement()).getQualifiedName().toString() + '.' + el.getSimpleName().toString());
                                                spans.add(new int[]{s - start, e - start});
                                            }
                                        } else if ((el.getKind() == ElementKind.FIELD || el.getKind() == ElementKind.METHOD) && el.getModifiers().contains((Object)Modifier.STATIC) && !el.getModifiers().contains((Object)Modifier.PRIVATE)) {
                                            simple2ImportFQN.put(el.getSimpleName().toString(), ((TypeElement)el.getEnclosingElement()).getQualifiedName().toString() + '.' + el.getSimpleName().toString());
                                            spans.add(new int[]{s - start, e - start});
                                        }
                                    }
                                    return (Void)TreePathScanner.super.visitIdentifier(node, (Object)p);
                                }

                                public Void visitClass(ClassTree node, Void p) {
                                    this.handleDeclaration();
                                    return (Void)TreePathScanner.super.visitClass(node, (Object)p);
                                }

                                public Void visitMethod(MethodTree node, Void p) {
                                    this.handleDeclaration();
                                    return (Void)TreePathScanner.super.visitMethod(node, (Object)p);
                                }

                                private void handleDeclaration() {
                                    int s = (int)parameter.getTrees().getSourcePositions().getStartPosition(parameter.getCompilationUnit(), this.getCurrentPath().getLeaf());
                                    int e = (int)parameter.getTrees().getSourcePositions().getEndPosition(parameter.getCompilationUnit(), this.getCurrentPath().getLeaf());
                                    Element el = parameter.getTrees().getElement(this.getCurrentPath());
                                    if (el != null && (start <= s && s <= end || start <= e && e <= end)) {
                                        simple2ImportFQN.remove(el.getSimpleName().toString());
                                        this.declaredInCopiedText.add(el);
                                    }
                                }

                                public Void visitVariable(VariableTree node, Void p) {
                                    this.handleDeclaration();
                                    if (this.lastType == node.getType()) {
                                        this.scan((Tree)node.getInitializer(), null);
                                        return null;
                                    }
                                    this.lastType = node.getType();
                                    return (Void)TreePathScanner.super.visitVariable(node, (Object)p);
                                }

                                public Void scan(Tree tree, Void p) {
                                    if (tree == null || parameter.getTreeUtilities().isSynthetic(new TreePath(this.getCurrentPath(), tree))) {
                                        return null;
                                    }
                                    return (Void)TreePathScanner.super.scan(tree, (Object)p);
                                }
                            }.scan((Tree)parameter.getCompilationUnit(), null);
                        }

                    };
                    boolean finished = false;
                    if (comp.getClientProperty(RUN_SYNCHRONOUSLY) != null || ClipboardHandler.autoImport) {
                        try {
                            js.runUserActionTask((Task)w, true);
                            finished = true;
                        }
                        catch (IOException ex) {
                            Exceptions.printStackTrace((Throwable)ex);
                        }
                    } else {
                        finished = ClipboardHandler.runQuickly(js, (Task)w);
                    }
                    if (finished) {
                        iw = new ImportsWrapper(NbEditorUtilities.getFileObject((Document)tc.getDocument()), simple2ImportFQN, spans);
                    }
                }
            }
            this.delegate.exportToClipboard(comp, clip, action);
            if (iw != null || copiedFromString) {
                clip.setContents(new WrappedTransferable(clip.getContents(null), iw, copiedFromString), null);
            }
        }

        @Override
        public int getSourceActions(JComponent c) {
            return this.delegate.getSourceActions(c);
        }

        @Override
        public Icon getVisualRepresentation(Transferable t) {
            return this.delegate.getVisualRepresentation(t);
        }

        @Override
        public boolean importData(TransferHandler.TransferSupport support) {
            return this.delegate.importData(support);
        }

        @Override
        public boolean importData(JComponent comp, Transferable t) {
            if (t.isDataFlavorSupported(IMPORT_FLAVOR) && comp instanceof JTextComponent && !this.insideToken((JTextComponent)comp, JavaTokenId.STRING_LITERAL, JavaTokenId.BLOCK_COMMENT, JavaTokenId.JAVADOC_COMMENT, JavaTokenId.LINE_COMMENT)) {
                boolean result = false;
                try {
                    final JTextComponent tc = (JTextComponent)comp;
                    final int caret = tc.getSelectionStart();
                    result = this.delegatedImportData(comp, t);
                    if (result) {
                        final ImportsWrapper imports = (ImportsWrapper)t.getTransferData(IMPORT_FLAVOR);
                        final FileObject file = NbEditorUtilities.getFileObject((Document)tc.getDocument());
                        final Document doc = tc.getDocument();
                        int len = doc.getLength();
                        final ArrayList<Position[]> inSpans = new ArrayList<Position[]>();
                        for (int[] span : imports.identifiers) {
                            int start = caret + span[0];
                            int end = caret + span[1];
                            if (0 > start || start > end || end > len) continue;
                            inSpans.add(new Position[]{doc.createPosition(start), doc.createPosition(end)});
                        }
                        SwingUtilities.invokeLater(new Runnable(){

                            @Override
                            public void run() {
                                JavaSource js = JavaSource.forDocument((Document)tc.getDocument());
                                if (js == null) {
                                    return;
                                }
                                Collection unavailable = ClipboardHandler.needsImports(js, caret, imports.simple2ImportFQN);
                                if (unavailable == null) {
                                    unavailable = file == null || !file.equals((Object)imports.sourceFO) ? imports.simple2ImportFQN.values() : Collections.emptyList();
                                }
                                HashSet toShow = new HashSet(imports.simple2ImportFQN.values());
                                toShow.retainAll(unavailable);
                                if (!unavailable.isEmpty()) {
                                    ClipboardHandler.showImportDialog(js, doc, caret, imports.simple2ImportFQN, toShow, inSpans);
                                }
                            }
                        });
                    }
                }
                catch (BadLocationException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
                catch (UnsupportedFlavorException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
                return result;
            }
            return this.delegatedImportData(comp, t);
        }

        private boolean delegatedImportData(JComponent comp, final Transferable t) {
            if (comp instanceof JTextComponent && !t.isDataFlavorSupported(COPY_FROM_STRING_FLAVOR) && this.insideToken((JTextComponent)comp, JavaTokenId.STRING_LITERAL, new JavaTokenId[0])) {
                return this.delegate.importData(comp, new Transferable(){

                    @Override
                    public DataFlavor[] getTransferDataFlavors() {
                        return t.getTransferDataFlavors();
                    }

                    @Override
                    public boolean isDataFlavorSupported(DataFlavor flavor) {
                        return t.isDataFlavorSupported(flavor);
                    }

                    @Override
                    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
                        Object data = t.getTransferData(flavor);
                        if (data instanceof String) {
                            String s = (String)data;
                            s = s.replace("\\", "\\\\");
                            s = s.replace("\"", "\\\"");
                            s = s.replace("\r\n", "\n");
                            s = s.replace("\n", "\\n\" +\n\"");
                            data = s;
                        } else if (data instanceof Reader) {
                            String line;
                            BufferedReader br = new BufferedReader((Reader)data);
                            StringBuilder sb = new StringBuilder();
                            while ((line = br.readLine()) != null) {
                                line = line.replace("\\", "\\\\");
                                line = line.replace("\"", "\\\"");
                                if (sb.length() > 0) {
                                    sb.append("\\n\" +\n\"");
                                }
                                sb.append(line);
                            }
                            data = new StringReader(sb.toString());
                        }
                        return data;
                    }
                });
            }
            return this.delegate.importData(comp, t);
        }

        private /* varargs */ boolean insideToken(final JTextComponent jtc, final JavaTokenId first, final JavaTokenId ... rest) {
            final Document doc = jtc.getDocument();
            final boolean[] result = new boolean[1];
            doc.render(new Runnable(){

                @Override
                public void run() {
                    int offset = jtc.getSelectionStart();
                    TokenSequence ts = SourceUtils.getJavaTokenSequence((TokenHierarchy)TokenHierarchy.get((Document)doc), (int)offset);
                    if (ts == null || !ts.moveNext() && !ts.movePrevious() || offset == ts.offset()) {
                        result[0] = false;
                    } else {
                        EnumSet<JavaTokenId[]> tokenIds = EnumSet.of(first, rest);
                        result[0] = tokenIds.contains((Object)ts.token().id());
                    }
                }
            });
            return result[0];
        }

    }

}

