/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.CatchTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IfTree
 *  com.sun.source.tree.LambdaExpressionTree
 *  com.sun.source.tree.MethodInvocationTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.NewClassTree
 *  com.sun.source.tree.ReturnTree
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.ThrowTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.TryTree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.support.CancellableTreePathScanner
 */
package org.netbeans.modules.java.editor.semantic;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.CatchTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.LambdaExpressionTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.ReturnTree;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.ThrowTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TryTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;
import javax.swing.text.Document;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.support.CancellableTreePathScanner;
import org.netbeans.modules.java.editor.semantic.Utilities;

public class MethodExitDetector
extends CancellableTreePathScanner<Boolean, Stack<Tree>> {
    private CompilationInfo info;
    private Document doc;
    private List<int[]> highlights;
    private boolean doExitPoints;
    private Collection<TypeMirror> exceptions;
    private Stack<Map<TypeMirror, List<Tree>>> exceptions2HighlightsStack;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public List<int[]> process(CompilationInfo info, Document document, Tree methoddeclorblock, Collection<Tree> excs) {
        this.info = info;
        this.doc = document;
        this.highlights = new ArrayList<int[]>();
        this.exceptions2HighlightsStack = new Stack();
        this.exceptions2HighlightsStack.push(null);
        try {
            int lastBracket;
            CompilationUnitTree cu = info.getCompilationUnit();
            this.doExitPoints = excs == null;
            Boolean wasReturn = (Boolean)this.scan(TreePath.getPath((CompilationUnitTree)cu, (Tree)methoddeclorblock), (Object)null);
            if (this.isCanceled()) {
                List<int[]> list = null;
                return list;
            }
            if (this.doExitPoints && wasReturn != Boolean.TRUE && (lastBracket = Utilities.findLastBracket(methoddeclorblock, cu, info.getTrees().getSourcePositions(), document)) != -1) {
                this.highlights.add(new int[]{lastBracket, lastBracket + 1});
            }
            ArrayList<TypeMirror> exceptions = null;
            if (excs != null) {
                exceptions = new ArrayList<TypeMirror>();
                for (Tree t : excs) {
                    if (this.isCanceled()) {
                        List<int[]> list = null;
                        return list;
                    }
                    TypeMirror m = info.getTrees().getTypeMirror(TreePath.getPath((CompilationUnitTree)cu, (Tree)t));
                    if (m == null) continue;
                    exceptions.add(m);
                }
            }
            Types t = info.getTypes();
            assert (this.exceptions2HighlightsStack.size() == 1);
            Map<TypeMirror, List<Tree>> exceptions2Highlights = this.exceptions2HighlightsStack.peek();
            if (exceptions2Highlights != null) {
                for (TypeMirror type1 : exceptions2Highlights.keySet()) {
                    if (this.isCanceled()) {
                        List<int[]> list = null;
                        return list;
                    }
                    boolean add = true;
                    if (exceptions != null) {
                        add = false;
                        for (TypeMirror type2 : exceptions) {
                            add |= t.isAssignable(type1, type2);
                        }
                    }
                    if (!add) continue;
                    for (Tree tree : exceptions2Highlights.get(type1)) {
                        this.addHighlightFor(tree);
                    }
                }
            }
            List<int[]> i$ = this.highlights;
            return i$;
        }
        finally {
            this.info = null;
            this.doc = null;
            this.highlights = null;
            this.exceptions2HighlightsStack = null;
        }
    }

    private void addHighlightFor(Tree t) {
        int start = (int)this.info.getTrees().getSourcePositions().getStartPosition(this.info.getCompilationUnit(), t);
        int end = (int)this.info.getTrees().getSourcePositions().getEndPosition(this.info.getCompilationUnit(), t);
        this.highlights.add(new int[]{start, end});
    }

    private void addToExceptionsMap(TypeMirror key, Tree value) {
        List<Tree> l;
        if (key == null || value == null) {
            return;
        }
        Map<TypeMirror, List<Tree>> map = this.exceptions2HighlightsStack.peek();
        if (map == null) {
            map = new HashMap<TypeMirror, List<Tree>>();
            this.exceptions2HighlightsStack.pop();
            this.exceptions2HighlightsStack.push(map);
        }
        if ((l = map.get(key)) == null) {
            l = new ArrayList<Tree>();
            map.put(key, l);
        }
        l.add(value);
    }

    private void doPopup() {
        Map<TypeMirror, List<Tree>> top = this.exceptions2HighlightsStack.pop();
        if (top == null) {
            return;
        }
        Map<TypeMirror, List<Tree>> result = this.exceptions2HighlightsStack.pop();
        if (result == null) {
            this.exceptions2HighlightsStack.push(top);
            return;
        }
        for (TypeMirror key : top.keySet()) {
            List<Tree> topKey = top.get(key);
            List<Tree> resultKey = result.get(key);
            if (topKey == null) continue;
            if (resultKey == null) {
                result.put(key, topKey);
                continue;
            }
            resultKey.addAll(topKey);
        }
        this.exceptions2HighlightsStack.push(result);
    }

    public Boolean visitTry(TryTree tree, Stack<Tree> d) {
        this.exceptions2HighlightsStack.push(null);
        Boolean returnInTryBlock = (Boolean)this.scan((Tree)tree.getBlock(), d);
        boolean returnInCatchBlock = true;
        for (Tree t : tree.getCatches()) {
            Boolean b = (Boolean)this.scan(t, d);
            returnInCatchBlock &= b == Boolean.TRUE;
        }
        Boolean returnInFinallyBlock = (Boolean)this.scan((Tree)tree.getFinallyBlock(), d);
        this.doPopup();
        if (returnInTryBlock == Boolean.TRUE && returnInCatchBlock) {
            return Boolean.TRUE;
        }
        return returnInFinallyBlock;
    }

    public Boolean visitReturn(ReturnTree tree, Stack<Tree> d) {
        if (this.exceptions == null && this.doExitPoints) {
            this.addHighlightFor((Tree)tree);
        }
        CancellableTreePathScanner.super.visitReturn(tree, d);
        return Boolean.TRUE;
    }

    public Boolean visitCatch(CatchTree tree, Stack<Tree> d) {
        TypeMirror type1 = this.info.getTrees().getTypeMirror(new TreePath(new TreePath(this.getCurrentPath(), (Tree)tree.getParameter()), tree.getParameter().getType()));
        Types t = this.info.getTypes();
        if (type1 != null) {
            HashSet<TypeMirror> toRemove = new HashSet<TypeMirror>();
            Map<TypeMirror, List<Tree>> exceptions2Highlights = this.exceptions2HighlightsStack.peek();
            if (exceptions2Highlights != null) {
                for (TypeMirror type2 : exceptions2Highlights.keySet()) {
                    if (!t.isAssignable(type2, type1)) continue;
                    toRemove.add(type2);
                }
                for (TypeMirror type : toRemove) {
                    exceptions2Highlights.remove(type);
                }
            }
        }
        this.scan((Tree)tree.getParameter(), d);
        return (Boolean)this.scan((Tree)tree.getBlock(), d);
    }

    public Boolean visitMethodInvocation(MethodInvocationTree tree, Stack<Tree> d) {
        Element el = this.info.getTrees().getElement(new TreePath(this.getCurrentPath(), (Tree)tree.getMethodSelect()));
        if (el == null) {
            System.err.println("Warning: decl == null");
            System.err.println("tree=" + (Object)tree);
        }
        if (el != null && el.getKind() == ElementKind.METHOD) {
            for (TypeMirror m : ((ExecutableElement)el).getThrownTypes()) {
                this.addToExceptionsMap(m, (Tree)tree);
            }
        }
        CancellableTreePathScanner.super.visitMethodInvocation(tree, d);
        return null;
    }

    public Boolean visitThrow(ThrowTree tree, Stack<Tree> d) {
        this.addToExceptionsMap(this.info.getTrees().getTypeMirror(new TreePath(this.getCurrentPath(), (Tree)tree.getExpression())), (Tree)tree);
        CancellableTreePathScanner.super.visitThrow(tree, d);
        return Boolean.TRUE;
    }

    public Boolean visitNewClass(NewClassTree tree, Stack<Tree> d) {
        Element el = this.info.getTrees().getElement(this.getCurrentPath());
        if (el != null && el.getKind() == ElementKind.CONSTRUCTOR) {
            for (TypeMirror m : ((ExecutableElement)el).getThrownTypes()) {
                this.addToExceptionsMap(m, (Tree)tree);
            }
        }
        return null;
    }

    public Boolean visitMethod(MethodTree node, Stack<Tree> p) {
        this.scan((Tree)node.getModifiers(), p);
        this.scan(node.getReturnType(), p);
        this.scan((Iterable)node.getTypeParameters(), p);
        this.scan((Iterable)node.getParameters(), p);
        this.scan((Iterable)node.getThrows(), p);
        return (Boolean)this.scan((Tree)node.getBody(), p);
    }

    public Boolean visitIf(IfTree node, Stack<Tree> p) {
        this.scan((Tree)node.getCondition(), p);
        Boolean thenResult = (Boolean)this.scan((Tree)node.getThenStatement(), p);
        Boolean elseResult = (Boolean)this.scan((Tree)node.getElseStatement(), p);
        if (thenResult == Boolean.TRUE && elseResult == Boolean.TRUE) {
            return Boolean.TRUE;
        }
        return null;
    }

    public Boolean visitClass(ClassTree node, Stack<Tree> p) {
        return null;
    }

    public Boolean visitLambdaExpression(LambdaExpressionTree node, Stack<Tree> p) {
        return null;
    }
}

