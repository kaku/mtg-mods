/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.javadoc.Doc
 *  com.sun.javadoc.SourcePosition
 *  com.sun.javadoc.Tag
 *  com.sun.source.tree.CompilationUnitTree
 *  org.netbeans.api.java.lexer.JavadocTokenId
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.modules.java.editor.javadoc;

import com.sun.javadoc.Doc;
import com.sun.javadoc.SourcePosition;
import com.sun.javadoc.Tag;
import com.sun.source.tree.CompilationUnitTree;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import org.netbeans.api.java.lexer.JavadocTokenId;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.java.editor.javadoc.JavadocCompletionUtils;
import org.netbeans.modules.parsing.api.Snapshot;

public final class DocPositions {
    public static final String UNCLOSED_INLINE_TAG = "@UnclosedInlineTag";
    public static final String NONAME_BLOCK_TAG = "@NonameTag";
    private Map<Tag, int[]> positions;
    private List<TagEntry> sortedTags;
    private int blockSectionStart;
    private final boolean broken;
    private Env env;
    static boolean isTestMode = false;
    String tokenSequenceDump;

    public static final DocPositions get(CompilationInfo javac, Doc javadoc, TokenSequence<JavadocTokenId> jdts) {
        return DocPositionsManager.get(javac, javadoc, jdts);
    }

    private DocPositions(Env env) {
        this.env = env;
        this.broken = false;
    }

    private DocPositions() {
        this.broken = true;
    }

    public int[] getTagSpan(Tag tag) {
        this.resolve();
        return this.positions.get((Object)tag);
    }

    public Tag getTag(int offset) {
        this.resolve();
        for (TagEntry te : this.sortedTags) {
            if (offset < te.span[0] || offset >= te.span[1]) continue;
            return te.tag();
        }
        return null;
    }

    public int getBlockSectionStart() {
        this.resolve();
        return this.blockSectionStart;
    }

    List<? extends Tag> getTags() {
        this.resolve();
        ArrayList<Tag> tags = new ArrayList<Tag>(this.sortedTags.size());
        for (TagEntry entry : this.sortedTags) {
            tags.add(entry.tag());
        }
        return tags;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void resolve() {
        block18 : {
            if (this.positions != null) {
                return;
            }
            this.positions = new WeakHashMap<Tag, int[]>();
            this.sortedTags = new ArrayList<TagEntry>();
            if (this.broken) {
                return;
            }
            try {
                this.env.prepare();
                if (this.env.javadoc == null || this.env.jdts.isEmpty()) {
                    return;
                }
                TokenSequence jdts = this.env.jdts;
                Token token = null;
                Token prev = null;
                jdts.moveStart();
                boolean isFirstLineWS = true;
                while (jdts.moveNext()) {
                    prev = token;
                    token = jdts.token();
                    if (token.id() == JavadocTokenId.TAG) {
                        if (prev == null || isFirstLineWS) {
                            isFirstLineWS = false;
                            this.addBlockTag(token, jdts.offset());
                            continue;
                        }
                        if (prev.id() != JavadocTokenId.OTHER_TEXT) continue;
                        if (JavadocCompletionUtils.isLineBreak(prev)) {
                            this.closeBlockTag(jdts.offset());
                            this.addBlockTag(token, jdts.offset());
                            continue;
                        }
                        if (!JavadocCompletionUtils.isInlineTagStart(prev)) continue;
                        this.addInlineTag();
                        token = jdts.token();
                        continue;
                    }
                    if (isFirstLineWS && token.id() == JavadocTokenId.OTHER_TEXT && JavadocCompletionUtils.isFirstWhiteSpaceAtFirstLine(token)) continue;
                    isFirstLineWS = false;
                }
                if (token != null) {
                    this.closeBlockTag(jdts.offset() + token.length());
                }
                Collections.sort(this.sortedTags);
                if (this.env.btags.length > 0) {
                    int[] span = this.positions.get((Object)this.env.btags[0]);
                    this.blockSectionStart = span == null ? 0 : span[0];
                    break block18;
                }
                this.blockSectionStart = 0;
            }
            catch (Throwable t) {
                this.tokenSequenceDump = String.valueOf((Object)this.env.jdts);
                try {
                    JavadocCompletionUtils.dumpOutOfSyncError(this.env.snapshot, this.env.jdts, this.env.javadoc, true);
                }
                catch (IllegalStateException ex) {
                    ex.initCause(t);
                    throw new IllegalStateException("" + '\'' + this.env.javadoc.getRawCommentText() + "'\n" + this.toString(), ex);
                }
            }
            finally {
                if (isTestMode) {
                    this.tokenSequenceDump = String.valueOf((Object)this.env.jdts);
                }
                this.env = null;
            }
        }
    }

    private void addInlineTag() {
        Tag tag;
        TokenSequence jdts = this.env.jdts;
        Token token = jdts.token();
        int anotherOpenBrace = 0;
        int startOffset = jdts.offset() - 1;
        int endOffset = -1;
        boolean isClosed = false;
        CharSequence tokenName = DocPositions.findTagName(jdts, jdts.index());
        block0 : while (jdts.moveNext()) {
            Token prev = token;
            token = jdts.token();
            if (token.id() == JavadocTokenId.TAG) {
                if (prev.id() != JavadocTokenId.OTHER_TEXT || !JavadocCompletionUtils.isLineBreak(prev)) continue;
                endOffset = jdts.offset();
                jdts.movePrevious();
                break;
            }
            if (token.id() != JavadocTokenId.OTHER_TEXT) continue;
            CharSequence text = token.text();
            for (int i = 0; i < text.length(); ++i) {
                char c = text.charAt(i);
                if (c == '}') {
                    if (anotherOpenBrace == 0) {
                        isClosed = true;
                        endOffset = jdts.offset() + i + 1;
                        break block0;
                    }
                    --anotherOpenBrace;
                    continue;
                }
                if (c != '{') continue;
                ++anotherOpenBrace;
            }
        }
        if (isClosed) {
            tag = this.findNextInlineAtTag();
            if (tag != null && tag.name().contentEquals(tokenName)) {
                this.addTag(tag, new int[]{startOffset, endOffset}, false);
            }
        } else {
            endOffset = endOffset < startOffset ? jdts.offset() + token.length() : endOffset;
            tag = new UnclosedTag(tokenName.toString(), "@UnclosedInlineTag", this.env.javadoc);
            ((UnclosedTag)tag).text = this.env.snapshot.getText().subSequence(startOffset, endOffset);
            this.addTag(tag, new int[]{startOffset, endOffset}, false);
        }
    }

    private Tag findNextInlineAtTag() {
        Tag tag = null;
        while (this.env.iindex < this.env.itags.length) {
            if (this.env.itags[this.env.iindex].name().startsWith("@")) {
                tag = this.env.itags[this.env.iindex++];
                break;
            }
            this.env.iindex++;
        }
        return tag;
    }

    private void addBlockTag(Token<JavadocTokenId> token, int offset) {
        assert (token.id() == JavadocTokenId.TAG);
        this.env.btag = this.env.btags[this.env.bindex];
        CharSequence tagName = DocPositions.findTagName(this.env.jdts, this.env.jdts.index());
        if (this.env.btag.name().contentEquals(tagName)) {
            ++this.env.bindex;
            this.env.iindex = 0;
            this.env.itags = this.env.btag.inlineTags();
        } else {
            this.env.btag = new UnclosedTag(token.text().toString(), "@NonameTag", this.env.javadoc);
        }
        this.addTag(this.env.btag, new int[]{offset, -1}, true);
        this.env.bscan = true;
    }

    private static CharSequence findTagName(TokenSequence<JavadocTokenId> jdts, int index) {
        jdts.moveIndex(index);
        if (!jdts.moveNext() || jdts.token().id() != JavadocTokenId.TAG) {
            throw new IllegalArgumentException("" + index + ", " + jdts.toString());
        }
        StringBuilder name = new StringBuilder(jdts.token().text());
        block0 : while (jdts.moveNext()) {
            Token token = jdts.token();
            if (token.id() != JavadocTokenId.OTHER_TEXT) {
                name.append(token.text());
                index = jdts.index();
                continue;
            }
            CharSequence text = token.text();
            for (int i = 0; i < text.length(); ++i) {
                char c = text.charAt(i);
                if (Character.isWhitespace(c)) break block0;
                name.append(c);
            }
        }
        jdts.moveIndex(index);
        jdts.moveNext();
        return name;
    }

    private void closeBlockTag(int offset) {
        if (this.env.bscan) {
            int[] span = this.positions.get((Object)this.env.btag);
            span[1] = offset;
            if ("@NonameTag" == this.env.btag.kind()) {
                ((UnclosedTag)this.env.btag).text = this.env.snapshot.getText().subSequence(span[0], span[1]);
            }
            this.env.bscan = false;
        }
    }

    private void addTag(Tag tag, int[] span, boolean isBlockTag) {
        this.positions.put(tag, span);
        this.sortedTags.add(new TagEntry(tag, span, isBlockTag));
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        if (this.positions != null) {
            for (TagEntry entry : this.sortedTags) {
                sb.append("\n ").append(entry);
            }
        } else {
            sb.append(" Not resolved yet.");
        }
        sb.append("\ntoken sequence dump: " + this.tokenSequenceDump);
        return sb.toString();
    }

    private static final class DocPositionsManager {
        private static final Map<CompilationUnitTree, Map<Doc, DocPositions>> cache = new WeakHashMap<CompilationUnitTree, Map<Doc, DocPositions>>();

        private DocPositionsManager() {
        }

        private static Map<Doc, DocPositions> getDocsCache(CompilationInfo javac) {
            CompilationUnitTree cut = javac.getCompilationUnit();
            Map<Doc, DocPositions> docsCache = cache.get((Object)cut);
            if (docsCache == null) {
                docsCache = new WeakHashMap<Doc, DocPositions>();
                cache.put(cut, docsCache);
            }
            return docsCache;
        }

        public static DocPositions get(CompilationInfo javac, Doc javadoc, TokenSequence<JavadocTokenId> jdts) {
            Map<Doc, DocPositions> docsCache = DocPositionsManager.getDocsCache(javac);
            DocPositions dp = docsCache.get((Object)javadoc);
            if (dp == null) {
                if (JavadocCompletionUtils.isInvalidDocInstance(javadoc, jdts)) {
                    JavadocCompletionUtils.dumpOutOfSyncError(javac, jdts, javadoc, false);
                    dp = new DocPositions();
                } else {
                    Snapshot snapshot = javac.getSnapshot();
                    dp = new DocPositions(new Env(jdts, snapshot, javadoc));
                }
                docsCache.put(javadoc, dp);
            }
            return dp;
        }
    }

    private static final class TagEntry
    implements Comparable<TagEntry> {
        final Reference<Tag> wtag;
        final UnclosedTag utag;
        final boolean isBlock;
        final int[] span;

        public TagEntry(Tag tag, int[] span, boolean isBlock) {
            this.wtag = new WeakReference<Tag>(tag);
            this.utag = (UnclosedTag)(tag instanceof UnclosedTag ? tag : null);
            this.isBlock = isBlock;
            assert (span.length == 2);
            this.span = span;
        }

        public Tag tag() {
            return this.wtag.get();
        }

        @Override
        public int compareTo(TagEntry te) {
            if (te == this) {
                return 0;
            }
            int res = this.span[1] - te.span[1];
            if (res == 0) {
                res = this.isBlock ? 1 : -1;
            }
            return res;
        }

        public String toString() {
            return String.format("[%1$d,%2$d], block: %3$b, %4$s", new Object[]{this.span[0], this.span[1], this.isBlock, this.tag()});
        }
    }

    private static final class UnclosedTag
    implements Tag {
        private static final Tag[] EMPTY_TAGS = new Tag[0];
        private final String name;
        private final Reference<Doc> wjavadoc;
        private final String kind;
        private CharSequence text;

        public UnclosedTag(String name, String kind, Doc javadoc) {
            this.name = name;
            this.kind = kind;
            this.wjavadoc = new WeakReference<Doc>(javadoc);
        }

        public String name() {
            return this.name;
        }

        public Doc holder() {
            return this.wjavadoc.get();
        }

        public String kind() {
            return this.kind;
        }

        public String text() {
            return this.text.toString();
        }

        public Tag[] inlineTags() {
            return EMPTY_TAGS;
        }

        public Tag[] firstSentenceTags() {
            return EMPTY_TAGS;
        }

        public SourcePosition position() {
            throw new UnsupportedOperationException();
        }

        public String toString() {
            return this.name + ":" + this.kind + ":" + this.text;
        }
    }

    private static final class Env {
        private TokenSequence<JavadocTokenId> jdts;
        private Snapshot snapshot;
        private final WeakReference<Doc> wjavadoc;
        private Doc javadoc;
        private Tag[] btags;
        private Tag btag;
        private int bindex;
        private boolean bscan = false;
        private Tag[] itags;
        private int iindex;

        public Env(TokenSequence<JavadocTokenId> jdts, Snapshot snapshot, Doc javadoc) {
            this.jdts = jdts;
            this.snapshot = snapshot;
            this.wjavadoc = new WeakReference<Doc>(javadoc);
        }

        void prepare() {
            this.javadoc = this.wjavadoc.get();
            if (this.javadoc != null) {
                this.btags = this.javadoc.tags();
                this.itags = this.javadoc.inlineTags();
            }
        }
    }

}

