/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.highlighting.support.OffsetsBag
 */
package org.netbeans.modules.java.editor.codegen;

import org.netbeans.spi.editor.highlighting.support.OffsetsBag;

public interface CodeDeleter {
    public String getDisplayName();

    public void invoke();

    public OffsetsBag getHighlight();
}

