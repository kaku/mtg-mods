/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.Fold
 *  org.netbeans.api.editor.fold.FoldHierarchy
 *  org.netbeans.api.editor.fold.FoldTemplate
 *  org.netbeans.api.editor.fold.FoldType
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.IndexingAwareParserResultTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.ParserResultTask
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.netbeans.modules.parsing.spi.SchedulerTask
 *  org.netbeans.modules.parsing.spi.TaskFactory
 *  org.netbeans.modules.parsing.spi.TaskIndexingMode
 *  org.netbeans.spi.editor.fold.FoldHierarchyTransaction
 *  org.netbeans.spi.editor.fold.FoldInfo
 *  org.netbeans.spi.editor.fold.FoldManager
 *  org.netbeans.spi.editor.fold.FoldManagerFactory
 *  org.netbeans.spi.editor.fold.FoldOperation
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.java.editor.fold;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldTemplate;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.IndexingAwareParserResultTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserResultTask;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.SchedulerTask;
import org.netbeans.modules.parsing.spi.TaskFactory;
import org.netbeans.modules.parsing.spi.TaskIndexingMode;
import org.netbeans.spi.editor.fold.FoldHierarchyTransaction;
import org.netbeans.spi.editor.fold.FoldInfo;
import org.netbeans.spi.editor.fold.FoldManager;
import org.netbeans.spi.editor.fold.FoldManagerFactory;
import org.netbeans.spi.editor.fold.FoldOperation;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;

public abstract class ParsingFoldSupport
extends TaskFactory
implements FoldManagerFactory {
    private static final RequestProcessor RP = new RequestProcessor(ParsingFoldSupport.class);
    private static final Map<String, FileManagerRegistrar> regs = new HashMap<String, FileManagerRegistrar>();

    protected ParsingFoldSupport() {
    }

    protected abstract FoldProcessor createTask(FileObject var1);

    private static FoldOperation getFoldOperation(FoldManager m) {
        if (!(m instanceof FM)) {
            return null;
        }
        return ((FM)m).operation;
    }

    public final Collection<? extends SchedulerTask> create(Snapshot snapshot) {
        FileObject f = snapshot.getSource().getFileObject();
        FileData fdata = null;
        FoldProcessor processor = null;
        if (f != null) {
            fdata = this.getRegistrar(this).getFileData(f);
            processor = fdata.createProcessor(this, f);
        }
        return f == null || processor == null ? Collections.emptyList() : Collections.singleton(this.createParserTask(f, processor));
    }

    protected ParserResultTask createParserTask(FileObject file, FoldProcessor processor) {
        FileData fd = this.getRegistrar(this).getFileData(file);
        return new ParserTask(file, fd, fd.createProcessor(this, file));
    }

    public final FoldManager createFoldManager() {
        return new FM(this.getRegistrar(this));
    }

    private static Document getDocument(FoldHierarchy h) {
        JTextComponent c = h.getComponent();
        if (c == null) {
            return null;
        }
        return c.getDocument();
    }

    private static FileObject getFileObject(FoldHierarchy h) {
        JTextComponent c = h.getComponent();
        if (c == null) {
            return null;
        }
        Document d = c.getDocument();
        Object o = d.getProperty("stream");
        if (o instanceof FileObject) {
            return (FileObject)o;
        }
        if (o instanceof DataObject) {
            return ((DataObject)o).getPrimaryFile();
        }
        return null;
    }

    private static int getCaretPos(FoldHierarchy h) {
        int caretPos = -1;
        JTextComponent c = h.getComponent();
        if (c == null) {
            return -1;
        }
        Document doc = ParsingFoldSupport.getDocument(h);
        Object od = doc.getProperty("stream");
        if (od instanceof DataObject) {
            int idx;
            DataObject d = (DataObject)od;
            EditorCookie cake = (EditorCookie)d.getCookie(EditorCookie.class);
            JEditorPane[] panes = cake.getOpenedPanes();
            int n = idx = panes == null ? -1 : Arrays.asList(panes).indexOf(c);
            if (idx != -1) {
                caretPos = c.getCaret().getDot();
            }
        }
        return caretPos;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private FileManagerRegistrar getRegistrar(ParsingFoldSupport instance) {
        Class<?> c = instance.getClass();
        Map<String, FileManagerRegistrar> map = regs;
        synchronized (map) {
            String s = c.getName();
            FileManagerRegistrar r = regs.get(s);
            if (r == null) {
                r = new FileManagerRegistrar();
                regs.put(s, r);
            }
            return r;
        }
    }

    private static class FileManagerRegistrar {
        private Map<FileObject, FileData> data = new WeakHashMap<FileObject, FileData>(5);

        private FileManagerRegistrar() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        FileData getFileData(FileObject f) {
            if (f == null) {
                return new FileData();
            }
            FileManagerRegistrar fileManagerRegistrar = this;
            synchronized (fileManagerRegistrar) {
                FileData d = this.data.get((Object)f);
                if (d == null) {
                    d = new FileData();
                    this.data.put(f, d);
                }
                return d;
            }
        }

        public FileData addFoldManager(FileObject f, FoldManager m) {
            if (f == null) {
                return null;
            }
            FileData fd = this.getFileData(f);
            fd.addManager(m);
            return fd;
        }

        public void removeFoldManager(FileObject f, FoldManager m) {
            if (f == null) {
                return;
            }
            this.getFileData(f).removeManager(m);
        }

        public Collection<FoldManager> getFoldManagers(FileObject f) {
            return this.getFileData(f).getManagers();
        }
    }

    static class FileData {
        private Collection<Reference<FoldManager>> managers = new ArrayList<Reference<FoldManager>>();
        private Reference<FoldProcessor> processor;
        private AtomicInteger stamp = new AtomicInteger();
        boolean first = true;

        FileData() {
        }

        void invalidate() {
            this.stamp.incrementAndGet();
        }

        int getStamp() {
            return this.stamp.get();
        }

        int initStamp() {
            return this.stamp.incrementAndGet();
        }

        synchronized FoldProcessor createProcessor(ParsingFoldSupport factory, FileObject f) {
            FoldProcessor p = null;
            Reference<FoldProcessor> rp = this.processor;
            if (rp != null) {
                p = rp.get();
            }
            if (p == null) {
                p = factory.createTask(f);
                if (p == null) {
                    return null;
                }
                p.fileData = this;
                this.processor = new WeakReference<FoldProcessor>(p);
            }
            return p;
        }

        synchronized void addManager(FoldManager m) {
            this.managers.add(new WeakReference<FoldManager>(m));
        }

        synchronized void removeManager(FoldManager m) {
            Iterator<Reference<FoldManager>> it = this.managers.iterator();
            while (it.hasNext()) {
                Reference<FoldManager> ref = it.next();
                FoldManager x = ref.get();
                if (x == null) {
                    it.remove();
                    continue;
                }
                if (x != m) continue;
                it.remove();
                break;
            }
        }

        synchronized List<FoldManager> getManagers() {
            ArrayList<FoldManager> live = new ArrayList<FoldManager>(this.managers.size());
            Iterator<Reference<FoldManager>> it = this.managers.iterator();
            while (it.hasNext()) {
                Reference<FoldManager> ref = it.next();
                FoldManager x = ref.get();
                if (x == null) {
                    it.remove();
                    continue;
                }
                live.add(x);
            }
            return live;
        }
    }

    private final class FM
    implements FoldManager {
        private final FileManagerRegistrar reg;
        private FoldOperation operation;
        private FileData fileData;
        private FoldProcessor processor;

        public FM(FileManagerRegistrar reg) {
            this.reg = reg;
        }

        public void init(FoldOperation operation) {
            this.operation = operation;
            FileObject f = ParsingFoldSupport.getFileObject(operation.getHierarchy());
            if (f != null) {
                this.fileData = this.reg.addFoldManager(f, this);
                this.processor = this.fileData.createProcessor(ParsingFoldSupport.this, f);
            }
        }

        private void invalidate() {
            if (this.fileData != null) {
                this.fileData.invalidate();
            }
        }

        public void initFolds(FoldHierarchyTransaction transaction) {
        }

        public void insertUpdate(DocumentEvent evt, FoldHierarchyTransaction transaction) {
            this.invalidate();
        }

        public void removeUpdate(DocumentEvent evt, FoldHierarchyTransaction transaction) {
            this.invalidate();
        }

        public void changedUpdate(DocumentEvent evt, FoldHierarchyTransaction transaction) {
        }

        public void removeEmptyNotify(Fold epmtyFold) {
        }

        public void removeDamagedNotify(Fold damagedFold) {
        }

        public void expandNotify(Fold expandedFold) {
        }

        public void release() {
            if (this.fileData != null) {
                this.fileData.removeManager(this);
            }
        }
    }

    private static class ParserTask
    extends IndexingAwareParserResultTask<Parser.Result> {
        private final FileData fileData;
        private final FileObject file;
        private final FoldProcessor processor;

        public ParserTask(FileObject file, FileData fd, FoldProcessor proc) {
            super(TaskIndexingMode.ALLOWED_DURING_SCAN);
            this.file = file;
            this.processor = proc;
            this.fileData = fd;
        }

        public void run(Parser.Result r, SchedulerEvent event) {
            Document doc = r.getSnapshot().getSource().getDocument(false);
            if (doc == null) {
                return;
            }
            Updater theUpdater = new Updater(this.fileData, doc);
            this.processor.runWith(theUpdater, r, doc);
        }

        public int getPriority() {
            return this.processor.getPriority();
        }

        public Class<? extends Scheduler> getSchedulerClass() {
            return Scheduler.EDITOR_SENSITIVE_TASK_SCHEDULER;
        }

        public void cancel() {
            this.fileData.invalidate();
        }
    }

    protected static final class Stop
    extends Error {
        protected Stop() {
        }
    }

    private static class Updater
    implements Runnable {
        private Document snapshotDoc;
        private FileData fileData;
        private final int initialStamp;
        private List<FoldInfo> foldInfos = new ArrayList<FoldInfo>();
        private List<Integer> anchors = new ArrayList<Integer>();
        private Iterator<FoldManager> mgrsToUpdate;
        private FoldOperation oper;
        private int[] caretPositions;

        public Updater(FileData fileData, Document doc) {
            this.fileData = fileData;
            this.initialStamp = fileData.initStamp();
            this.snapshotDoc = doc;
        }

        protected final FoldOperation getOperation() {
            return this.oper;
        }

        synchronized void setManagersAndCarets(List<FoldManager> mgrs, int[] poss) {
            this.mgrsToUpdate = mgrs.iterator();
            this.caretPositions = poss;
        }

        private int findFoldIndex(int caretPos, boolean useAnchors) {
            if (caretPos == -1) {
                return -1;
            }
            int expandIndex = -1;
            if (caretPos >= 0) {
                for (int i = 0; i < this.anchors.size(); ++i) {
                    FoldType ft;
                    int a = this.anchors.get(i);
                    if (a > caretPos) continue;
                    FoldInfo fi = this.foldInfos.get(i);
                    if (a == caretPos && ((ft = fi.getType()).isKindOf(FoldType.INITIAL_COMMENT) || ft.isKindOf(FoldType.COMMENT) || ft.isKindOf(FoldType.DOCUMENTATION)) || fi.getEnd() <= caretPos) continue;
                    expandIndex = i;
                    break;
                }
            }
            return expandIndex;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private synchronized void processManagers() {
            block7 : {
                do {
                    if (this.mgrsToUpdate.hasNext()) {
                        this.oper = ParsingFoldSupport.getFoldOperation(this.mgrsToUpdate.next());
                        if (this.isCancelled()) {
                            return;
                        }
                        if (this.oper == null || ParsingFoldSupport.getDocument(this.oper.getHierarchy()) != this.snapshotDoc) continue;
                        this.oper.getHierarchy().render((Runnable)this);
                        continue;
                    }
                    break block7;
                    break;
                } while (true);
                finally {
                    this.mgrsToUpdate = null;
                    this.snapshotDoc = null;
                    this.fileData = null;
                }
            }
        }

        protected final boolean isCancelled() {
            return this.fileData.getStamp() != this.initialStamp;
        }

        private FoldInfo expanded(FoldInfo info) {
            FoldInfo ex = FoldInfo.range((int)info.getStart(), (int)info.getEnd(), (FoldType)info.getType());
            if (info.getTemplate() != info.getType().getTemplate()) {
                ex = ex.withTemplate(info.getTemplate());
            }
            if (info.getDescriptionOverride() != null) {
                ex = ex.withDescription(info.getDescriptionOverride());
            }
            ex.attach(info.getExtraInfo());
            return ex.collapsed(false);
        }

        List<FoldInfo> expandCaretFold() {
            if (!this.fileData.first) {
                return this.foldInfos;
            }
            int expandIndex = this.findFoldIndex(ParsingFoldSupport.getCaretPos(this.oper.getHierarchy()), true);
            if (expandIndex == -1) {
                return this.foldInfos;
            }
            ArrayList<FoldInfo> aa = new ArrayList<FoldInfo>(this.foldInfos);
            aa.set(expandIndex, this.expanded(this.foldInfos.get(expandIndex)));
            return aa;
        }

        @Override
        public void run() {
            if (this.oper == null) {
                this.processManagers();
                return;
            }
            List<FoldInfo> infos = this.expandCaretFold();
            try {
                if (this.oper.update(infos, null, null) != null) {
                    this.fileData.first = false;
                }
            }
            catch (BadLocationException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
    }

    protected static abstract class FoldProcessor {
        private final FileObject file;
        private FileData fileData;
        private Updater updater;
        private String mimeType;
        private RequestProcessor.Task refreshTask;

        protected FoldProcessor(FileObject f, String mimeType) {
            this.refreshTask = RP.create((Runnable)new R());
            this.mimeType = mimeType;
            this.file = f;
        }

        protected FileObject getFile() {
            return this.file;
        }

        protected Runnable runInEDT() {
            return null;
        }

        protected final void performRefresh() {
            this.refreshTask.schedule(300);
        }

        protected abstract boolean processResult(Parser.Result var1);

        protected int getPriority() {
            return 0;
        }

        protected final void addFold(FoldInfo info, int anchor) {
            if (this.isCancelled() || this.updater == null) {
                throw new Stop();
            }
            this.updater.foldInfos.add(info);
            if (anchor == -1) {
                anchor = info.getStart();
            }
            this.updater.anchors.add(anchor);
        }

        protected final boolean isCancelled() {
            return this.updater == null || this.fileData.getStamp() != this.updater.initialStamp;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private synchronized void runWith(final Updater u, Parser.Result r, final Document doc) {
            if (doc == null) {
                return;
            }
            assert (this.updater == null);
            try {
                this.updater = u;
                if (!this.processResult(r)) {
                    return;
                }
                final List<FoldManager> fms = this.fileData.getManagers();
                final int[] carets = new int[fms.size()];
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        if (FoldProcessor.this.fileData.getStamp() != u.initialStamp) {
                            return;
                        }
                        int index = -1;
                        for (FoldManager fm : fms) {
                            ++index;
                            FoldOperation op = ParsingFoldSupport.getFoldOperation(fm);
                            if (op == null) continue;
                            carets[index] = ParsingFoldSupport.getCaretPos(op.getHierarchy());
                        }
                        u.setManagersAndCarets(fms, carets);
                        doc.render(u);
                    }
                });
            }
            catch (Stop stop) {}
            finally {
                this.updater = null;
            }
        }

        class R
        extends UserTask
        implements Runnable {
            R() {
            }

            @Override
            public void run() {
                try {
                    ParserManager.parse(Collections.singleton(Source.create((FileObject)FoldProcessor.this.file)), (UserTask)this);
                }
                catch (ParseException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }

            public void run(ResultIterator resultIterator) throws Exception {
                Parser.Result r = resultIterator.getParserResult();
                if (!FoldProcessor.this.mimeType.equals(r.getSnapshot().getMimeType())) {
                    return;
                }
                Document doc = r.getSnapshot().getSource().getDocument(false);
                if (doc == null) {
                    return;
                }
                Updater theUpdater = new Updater(FoldProcessor.this.fileData, doc);
                FoldProcessor.this.runWith(theUpdater, r, doc);
            }
        }

    }

}

