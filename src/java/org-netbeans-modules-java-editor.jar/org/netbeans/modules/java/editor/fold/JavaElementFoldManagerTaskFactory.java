/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.source.CancellableTask
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.JavaSource$Priority
 *  org.netbeans.api.java.source.JavaSourceTaskFactory
 *  org.netbeans.api.java.source.support.EditorAwareJavaSourceTaskFactory
 *  org.netbeans.modules.parsing.spi.TaskIndexingMode
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.java.editor.fold;

import java.util.Collection;
import org.netbeans.api.java.source.CancellableTask;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.JavaSourceTaskFactory;
import org.netbeans.api.java.source.support.EditorAwareJavaSourceTaskFactory;
import org.netbeans.modules.java.editor.fold.JavaElementFoldManager;
import org.netbeans.modules.parsing.spi.TaskIndexingMode;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public class JavaElementFoldManagerTaskFactory
extends EditorAwareJavaSourceTaskFactory {
    public JavaElementFoldManagerTaskFactory() {
        super(JavaSource.Phase.PARSED, JavaSource.Priority.NORMAL, TaskIndexingMode.ALLOWED_DURING_SCAN, new String[0]);
    }

    public CancellableTask<CompilationInfo> createTask(FileObject file) {
        final JavaElementFoldManager.JavaElementFoldTask t = JavaElementFoldManager.JavaElementFoldTask.getTask(file);
        return new CancellableTask<CompilationInfo>(){

            public void cancel() {
                t.cancel();
            }

            public void run(CompilationInfo parameter) throws Exception {
                t.run((Object)parameter);
            }
        };
    }

    public static void doRefresh(FileObject file) {
        for (JavaSourceTaskFactory f : Lookup.getDefault().lookupAll(JavaSourceTaskFactory.class)) {
            if (!(f instanceof JavaElementFoldManagerTaskFactory)) continue;
            ((JavaElementFoldManagerTaskFactory)f).reschedule(file);
        }
    }

}

