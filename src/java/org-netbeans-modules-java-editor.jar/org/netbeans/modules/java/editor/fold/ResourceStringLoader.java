/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.EditorCookie$Observable
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.java.editor.fold;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.modules.parsing.api.Source;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;

public final class ResourceStringLoader {
    private static final Logger LOG = Logger.getLogger(ResourceStringLoader.class.getName());
    private static final AtomicInteger counter = new AtomicInteger();
    private final Map<FileObject, Holder> resourceContents = new WeakHashMap<FileObject, Holder>(5);
    private final int no;
    private ChangeListener l;
    private static final RequestProcessor REFRESHER = new RequestProcessor(ResourceStringLoader.class);
    private static final Cache CACHE = new Cache();

    public ResourceStringLoader(ChangeListener l) {
        this.l = l;
        this.no = counter.incrementAndGet();
    }

    private void fireStateChanged() {
        ChangeListener l = this.l;
        if (l != null) {
            l.stateChanged(new ChangeEvent(this));
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void retainFiles(Collection<FileObject> files) {
        ResourceStringLoader resourceStringLoader = this;
        synchronized (resourceStringLoader) {
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String getMessage(FileObject f, String key) {
        Holder h;
        ResourceStringLoader resourceStringLoader = this;
        synchronized (resourceStringLoader) {
            h = this.resourceContents.get((Object)f);
            if (h == null) {
                LOG.fine("" + this.no + ": Getting contents for file " + (Object)f);
                h = CACHE.getProperties(f, this);
                this.resourceContents.put(f, h);
            }
        }
        return h.getContent().getProperty(key);
    }

    private static class Cache
    implements Runnable {
        private final Map<FileObject, Reference<Holder>> loaded = new WeakHashMap<FileObject, Reference<Holder>>(5);
        private Set<FileObject> invalid = new HashSet<FileObject>();
        public static final int CACHE_REFRESH_TIMEOUT = 500;

        private Cache() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        Holder getProperties(FileObject file, ResourceStringLoader loader) {
            Holder h;
            Map<FileObject, Reference<Holder>> map = this.loaded;
            synchronized (map) {
                Reference<Holder> refH = this.loaded.get((Object)file);
                if (refH == null || (h = refH.get()) == null) {
                    h = new Holder(this, file);
                    this.loaded.put(file, new WeakReference<Holder>(h));
                }
            }
            if (loader != null) {
                h.attach(loader);
            }
            return h;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void invalidate(FileObject f) {
            Map<FileObject, Reference<Holder>> map = this.loaded;
            synchronized (map) {
                this.invalid.add(f);
                REFRESHER.post((Runnable)this, 500);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            ArrayList<Holder> fireHolders;
            Map<FileObject, Reference<Holder>> map = this.loaded;
            synchronized (map) {
                fireHolders = new ArrayList<Holder>(this.invalid.size());
                Set<FileObject> invalidate = this.invalid;
                LOG.fine("Invalidating " + invalidate.size() + " resources");
                this.invalid = new HashSet<FileObject>();
                for (FileObject f : invalidate) {
                    Reference<Holder> ref = this.loaded.get((Object)f);
                    Holder h = ref.get();
                    if (h == null) {
                        LOG.fine("Found expired holder for " + (Object)f);
                        this.loaded.remove((Object)f);
                    }
                    fireHolders.add(h);
                }
            }
            HashSet loaders = new HashSet();
            for (Holder h : fireHolders) {
                loaders.addAll(h.collectLoaders());
            }
            LOG.fine("Invalidating " + loaders.size() + " resource loaders");
            for (ResourceStringLoader l : loaders) {
                l.fireStateChanged();
            }
        }

        private Properties loadProperties(FileObject f) {
            Source s = Source.create((FileObject)f);
            final Document doc = s.getDocument(false);
            final Properties props = new Properties();
            if (doc == null) {
                LOG.fine("Loading properties from " + (Object)f);
                try {
                    InputStream is = f.getInputStream();
                    Throwable throwable = null;
                    try {
                        props.load(is);
                    }
                    catch (Throwable x2) {
                        throwable = x2;
                        throw x2;
                    }
                    finally {
                        if (is != null) {
                            if (throwable != null) {
                                try {
                                    is.close();
                                }
                                catch (Throwable x2) {
                                    throwable.addSuppressed(x2);
                                }
                            } else {
                                is.close();
                            }
                        }
                    }
                }
                catch (IOException ex) {}
            } else {
                LOG.fine("Loading properties from document " + doc + " created from " + (Object)f);
                doc.render(new Runnable(){

                    @Override
                    public void run() {
                        int l = doc.getLength();
                        try {
                            String s = doc.getText(0, l);
                            props.load(new StringReader(s));
                        }
                        catch (BadLocationException ex) {
                            Exceptions.printStackTrace((Throwable)ex);
                        }
                        catch (IOException ex) {
                            // empty catch block
                        }
                    }
                });
            }
            return props;
        }

    }

    private static class Holder
    extends FileChangeAdapter
    implements DocumentListener,
    PropertyChangeListener {
        private final FileObject file;
        private final Cache cache;
        private final Collection<Reference<ResourceStringLoader>> toNotify = new ArrayList<Reference<ResourceStringLoader>>(5);
        private DocumentListener docWL;
        private volatile Properties content;

        Holder(Cache c, FileObject f) {
            this.cache = c;
            this.file = f;
            try {
                DataObject d = DataObject.find((FileObject)f);
                EditorCookie.Observable obs = (EditorCookie.Observable)d.getLookup().lookup(EditorCookie.Observable.class);
                if (obs != null) {
                    obs.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)obs));
                }
            }
            catch (DataObjectNotFoundException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void unregister(ResourceStringLoader ldr) {
            Holder holder = this;
            synchronized (holder) {
                this.toNotify.add(new WeakReference<ResourceStringLoader>(ldr));
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        Properties getContent() {
            Properties p = this.content;
            if (p != null) {
                return p;
            }
            Holder holder = this;
            synchronized (holder) {
                if (this.content == null) {
                    this.content = this.cache.loadProperties(this.file);
                }
                return this.content;
            }
        }

        public void fileRenamed(FileRenameEvent fe) {
            this.invalidate((FileObject)fe.getSource());
        }

        public void fileDeleted(FileEvent fe) {
            this.invalidate((FileObject)fe.getSource());
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void fileChanged(FileEvent fe) {
            Holder holder = this;
            synchronized (holder) {
                if (this.docWL == null) {
                    return;
                }
            }
            this.invalidate((FileObject)fe.getSource());
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            this.invalidate(this.extractFileObject(e.getDocument()));
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            this.invalidate(this.extractFileObject(e.getDocument()));
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
        }

        private FileObject extractFileObject(Document d) {
            if (d == null) {
                return null;
            }
            Object o = d.getProperty("stream");
            if (o instanceof FileObject) {
                return (FileObject)o;
            }
            if (o instanceof DataObject) {
                return ((DataObject)o).getPrimaryFile();
            }
            return null;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            FileObject closedFile = null;
            if (!"document".equals(evt.getPropertyName())) {
                return;
            }
            Holder holder = this;
            synchronized (holder) {
                Document nue;
                Document old = (Document)evt.getOldValue();
                if (old != null && this.docWL != null) {
                    old.removeDocumentListener(this.docWL);
                    this.docWL = null;
                }
                if ((nue = (Document)evt.getNewValue()) != null) {
                    this.docWL = WeakListeners.document((DocumentListener)this, (Object)nue);
                    nue.addDocumentListener(this.docWL);
                } else {
                    closedFile = this.extractFileObject(old);
                }
            }
            if (closedFile != null) {
                this.invalidate(closedFile);
            }
        }

        private void invalidate(FileObject myFile) {
            this.content = null;
            this.cache.invalidate(myFile);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private Collection<ResourceStringLoader> collectLoaders() {
            ArrayList<ResourceStringLoader> clients;
            Holder holder = this;
            synchronized (holder) {
                clients = new ArrayList<ResourceStringLoader>(this.toNotify.size());
                Iterator<Reference<ResourceStringLoader>> it = this.toNotify.iterator();
                while (it.hasNext()) {
                    Reference<ResourceStringLoader> ref = it.next();
                    ResourceStringLoader cl = ref.get();
                    if (cl == null) {
                        it.remove();
                        continue;
                    }
                    clients.add(cl);
                }
            }
            return clients;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void attach(ResourceStringLoader l) {
            Holder holder = this;
            synchronized (holder) {
                this.toNotify.add(new WeakReference<ResourceStringLoader>(l));
            }
        }

        public String toString() {
            return "[ResourceHolder for " + (Object)this.file + "]";
        }
    }

}

