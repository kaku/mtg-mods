/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.ui.ElementIcons
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.java.editor.codegen.ui;

import java.awt.Image;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.type.WildcardType;
import javax.lang.model.util.Elements;
import javax.swing.Action;
import javax.swing.Icon;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.ui.ElementIcons;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

public class ElementNode
extends AbstractNode {
    private Description description;
    private boolean singleSelection;
    private static final Action[] EMPTY_ACTIONS = new Action[0];
    private static String[] c = new String[]{"&", "<", ">", "\n", "\""};
    private static String[] tags = new String[]{"&amp;", "&lt;", "&gt;", "<br>", "&quot;"};

    public ElementNode(Description description) {
        this(description, false);
    }

    private ElementNode(Description description, boolean sortChildren) {
        super(description.subs == null ? Children.LEAF : new ElementChilren(description.subs, sortChildren), Lookups.singleton((Object)description));
        this.description = description;
        description.node = this;
        this.setDisplayName(description.name);
    }

    public void setSingleSelection(boolean singleSelection) {
        this.singleSelection = singleSelection;
    }

    public Image getIcon(int type) {
        if (this.description.elementHandle == null) {
            return super.getIcon(type);
        }
        return ImageUtilities.icon2Image((Icon)ElementIcons.getElementIcon((ElementKind)this.description.elementHandle.getKind(), (Collection)this.description.modifiers));
    }

    public Image getOpenedIcon(int type) {
        return this.getIcon(type);
    }

    public String getDisplayName() {
        return this.description.name;
    }

    public String getHtmlDisplayName() {
        return this.description.htmlHeader;
    }

    public Action[] getActions(boolean context) {
        return EMPTY_ACTIONS;
    }

    public void assureSingleSelection() {
        Description d;
        Node pn = this.getParentNode();
        if (pn == null && this.singleSelection) {
            this.description.deepSetSelected(false);
        } else if (pn != null && (d = (Description)pn.getLookup().lookup(Description.class)) != null) {
            d.node.assureSingleSelection();
        }
    }

    private static String translateToHTML(String input) {
        for (int cntr = 0; cntr < c.length; ++cntr) {
            input = input.replaceAll(c[cntr], tags[cntr]);
        }
        return input;
    }

    public static class Description {
        public static final Comparator<Description> ALPHA_COMPARATOR = new DescriptionComparator();
        private ElementNode node;
        private String name;
        private ElementHandle<? extends Element> elementHandle;
        private Set<Modifier> modifiers;
        private List<Description> subs;
        private String htmlHeader;
        private boolean isSelected;
        private boolean isSelectable;

        public static Description create(List<Description> subs) {
            return new Description("<root>", null, null, subs, null, false, false);
        }

        public static Description create(CompilationInfo info, Element element, List<Description> subs, boolean isSelectable, boolean isSelected) {
            boolean deprecated = info.getElements().isDeprecated(element);
            String htmlHeader = null;
            switch (element.getKind()) {
                case ANNOTATION_TYPE: 
                case CLASS: 
                case ENUM: 
                case INTERFACE: {
                    htmlHeader = Description.createHtmlHeader(deprecated, (TypeElement)element);
                    break;
                }
                case ENUM_CONSTANT: 
                case FIELD: {
                    htmlHeader = Description.createHtmlHeader(deprecated, (VariableElement)element);
                    break;
                }
                case CONSTRUCTOR: 
                case METHOD: {
                    htmlHeader = Description.createHtmlHeader(deprecated, (ExecutableElement)element);
                }
            }
            return new Description(element.getSimpleName().toString(), ElementHandle.create((Element)element), element.getModifiers(), subs, htmlHeader, isSelectable, isSelected);
        }

        private Description(String name, ElementHandle<? extends Element> elementHandle, Set<Modifier> modifiers, List<Description> subs, String htmlHeader, boolean isSelectable, boolean isSelected) {
            this.name = name;
            this.elementHandle = elementHandle;
            this.modifiers = modifiers;
            this.subs = subs;
            this.htmlHeader = htmlHeader;
            this.isSelectable = isSelectable;
            this.isSelected = isSelected;
        }

        public boolean isSelectable() {
            return this.isSelectable;
        }

        public boolean hasSelectableSubs() {
            if (null == this.subs) {
                return false;
            }
            for (Description d : this.getSubs()) {
                if (!d.isSelectable()) continue;
                return true;
            }
            return false;
        }

        public boolean isSelected() {
            return this.isSelected;
        }

        public List<Description> getSubs() {
            return this.subs;
        }

        public void setSelected(boolean selected) {
            if (selected && this.node != null) {
                this.node.assureSingleSelection();
            }
            this.isSelected = selected;
            if (this.node != null) {
                this.node.fireDisplayNameChange(null, null);
                if (this.node.getParentNode() instanceof ElementNode) {
                    ((ElementNode)this.node.getParentNode()).fireDisplayNameChange(null, null);
                }
            }
        }

        public void deepSetSelected(boolean value) {
            if (this.isSelectable() && value != this.isSelected()) {
                this.setSelected(value);
            }
            if (this.subs != null) {
                for (Description s : this.subs) {
                    s.deepSetSelected(value);
                }
            }
        }

        public ElementHandle<? extends Element> getElementHandle() {
            return this.elementHandle;
        }

        public boolean equals(Object o) {
            if (!(o instanceof Description)) {
                return false;
            }
            Description d = (Description)o;
            if (!this.name.equals(d.name)) {
                return false;
            }
            if (this.elementHandle != d.elementHandle) {
                if (this.elementHandle == null || d.elementHandle == null) {
                    return false;
                }
                if (this.elementHandle.getKind() != d.elementHandle.getKind()) {
                    return false;
                }
                if (!this.elementHandle.signatureEquals(d.elementHandle)) {
                    return false;
                }
            }
            return true;
        }

        public int hashCode() {
            int hash = 7;
            hash = 29 * hash + (this.name != null ? this.name.hashCode() : 0);
            hash = 29 * hash + (this.elementHandle != null ? this.elementHandle.getKind().hashCode() : 0);
            return hash;
        }

        public String getName() {
            return this.name;
        }

        public static Description deepCopy(Description d) {
            ArrayList<Description> subsCopy;
            if (d.subs == null) {
                subsCopy = null;
            } else {
                subsCopy = new ArrayList<Description>(d.subs.size());
                for (Description s : d.subs) {
                    subsCopy.add(Description.deepCopy(s));
                }
            }
            return new Description(d.name, d.elementHandle, d.modifiers, subsCopy, d.htmlHeader, d.isSelectable, d.isSelected);
        }

        private static String createHtmlHeader(boolean deprecated, ExecutableElement e) {
            TypeMirror rt;
            StringBuilder sb = new StringBuilder();
            sb.append("<html>");
            if (deprecated) {
                sb.append("<s>");
            }
            if (e.getKind() == ElementKind.CONSTRUCTOR) {
                sb.append(e.getEnclosingElement().getSimpleName());
            } else {
                sb.append(e.getSimpleName());
            }
            if (deprecated) {
                sb.append("</s>");
            }
            sb.append("(");
            Iterator<? extends VariableElement> it = e.getParameters().iterator();
            while (it.hasNext()) {
                VariableElement param = it.next();
                if (!it.hasNext() && e.isVarArgs() && param.asType().getKind() == TypeKind.ARRAY) {
                    sb.append(ElementNode.translateToHTML(Description.print(((ArrayType)param.asType()).getComponentType())));
                    sb.append("...");
                } else {
                    sb.append(ElementNode.translateToHTML(Description.print(param.asType())));
                }
                sb.append(" ");
                sb.append(param.getSimpleName());
                if (!it.hasNext()) continue;
                sb.append(", ");
            }
            sb.append(")");
            if (e.getKind() != ElementKind.CONSTRUCTOR && (rt = e.getReturnType()).getKind() != TypeKind.VOID) {
                sb.append(" : ");
                sb.append(ElementNode.translateToHTML(Description.print(e.getReturnType())));
            }
            return sb.toString();
        }

        private static String createHtmlHeader(boolean deprecated, VariableElement e) {
            StringBuilder sb = new StringBuilder();
            sb.append("<html>");
            if (deprecated) {
                sb.append("<s>");
            }
            sb.append(e.getSimpleName());
            if (deprecated) {
                sb.append("</s>");
            }
            if (e.getKind() != ElementKind.ENUM_CONSTANT) {
                sb.append(" : ");
                sb.append(ElementNode.translateToHTML(Description.print(e.asType())));
            }
            return sb.toString();
        }

        private static String createHtmlHeader(boolean deprecated, TypeElement e) {
            List<? extends TypeParameterElement> typeParams;
            StringBuilder sb = new StringBuilder();
            sb.append("<html>");
            if (deprecated) {
                sb.append("<s>");
            }
            sb.append(e.getSimpleName());
            if (deprecated) {
                sb.append("</s>");
            }
            if ((typeParams = e.getTypeParameters()) != null && !typeParams.isEmpty()) {
                sb.append("&lt;");
                Iterator<? extends TypeParameterElement> it = typeParams.iterator();
                while (it.hasNext()) {
                    TypeParameterElement tp = it.next();
                    sb.append(tp.getSimpleName());
                    try {
                        List<? extends TypeMirror> bounds = tp.getBounds();
                        if (!bounds.isEmpty()) {
                            sb.append(ElementNode.translateToHTML(Description.printBounds(bounds)));
                        }
                    }
                    catch (NullPointerException npe) {
                        // empty catch block
                    }
                    if (!it.hasNext()) continue;
                    sb.append(", ");
                }
                sb.append("&gt;");
            }
            return sb.toString();
        }

        private static String printBounds(List<? extends TypeMirror> bounds) {
            if (bounds.size() == 1 && "java.lang.Object".equals(bounds.get(0).toString())) {
                return "";
            }
            StringBuilder sb = new StringBuilder();
            sb.append(" extends ");
            Iterator<? extends TypeMirror> it = bounds.iterator();
            while (it.hasNext()) {
                TypeMirror bound = it.next();
                sb.append(Description.print(bound));
                if (!it.hasNext()) continue;
                sb.append(" & ");
            }
            return sb.toString();
        }

        private static String print(TypeMirror tm) {
            switch (tm.getKind()) {
                case DECLARED: {
                    DeclaredType dt = (DeclaredType)tm;
                    StringBuilder sb = new StringBuilder(dt.asElement().getSimpleName().toString());
                    List<? extends TypeMirror> typeArgs = dt.getTypeArguments();
                    if (!typeArgs.isEmpty()) {
                        sb.append("<");
                        Iterator<? extends TypeMirror> it = typeArgs.iterator();
                        while (it.hasNext()) {
                            TypeMirror ta = it.next();
                            sb.append(Description.print(ta));
                            if (!it.hasNext()) continue;
                            sb.append(", ");
                        }
                        sb.append(">");
                    }
                    return sb.toString();
                }
                case TYPEVAR: {
                    TypeVariable tv = (TypeVariable)tm;
                    StringBuilder sb = new StringBuilder(tv.asElement().getSimpleName().toString());
                    return sb.toString();
                }
                case ARRAY: {
                    ArrayType at = (ArrayType)tm;
                    StringBuilder sb = new StringBuilder(Description.print(at.getComponentType()));
                    sb.append("[]");
                    return sb.toString();
                }
                case WILDCARD: {
                    WildcardType wt = (WildcardType)tm;
                    StringBuilder sb = new StringBuilder("?");
                    if (wt.getExtendsBound() != null) {
                        sb.append(" extends ");
                        sb.append(Description.print(wt.getExtendsBound()));
                    }
                    if (wt.getSuperBound() != null) {
                        sb.append(" super ");
                        sb.append(Description.print(wt.getSuperBound()));
                    }
                    return sb.toString();
                }
            }
            return tm.toString();
        }

        private static class DescriptionComparator
        implements Comparator<Description> {
            private DescriptionComparator() {
            }

            @Override
            public int compare(Description d1, Description d2) {
                if (this.k2i(d1.elementHandle.getKind()) != this.k2i(d2.elementHandle.getKind())) {
                    return this.k2i(d1.elementHandle.getKind()) - this.k2i(d2.elementHandle.getKind());
                }
                return d1.name.compareTo(d2.name);
            }

            int k2i(ElementKind kind) {
                switch (kind) {
                    case CONSTRUCTOR: {
                        return 1;
                    }
                    case METHOD: {
                        return 2;
                    }
                    case FIELD: {
                        return 3;
                    }
                    case CLASS: {
                        return 4;
                    }
                    case INTERFACE: {
                        return 5;
                    }
                    case ENUM: {
                        return 6;
                    }
                    case ANNOTATION_TYPE: {
                        return 7;
                    }
                }
                return 100;
            }
        }

    }

    private static final class ElementChilren
    extends Children.Keys<Description> {
        public ElementChilren(List<Description> descriptions, boolean sortChildren) {
            if (sortChildren) {
                descriptions = new ArrayList<Description>(descriptions);
                Collections.sort(descriptions, Description.ALPHA_COMPARATOR);
            }
            this.setKeys(descriptions);
        }

        protected Node[] createNodes(Description key) {
            return new Node[]{new ElementNode(key, true)};
        }
    }

}

