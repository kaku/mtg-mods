/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ArrayTypeTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.modules.java.editor.semantic;

import com.sun.source.tree.ArrayTypeTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.lang.model.element.Name;
import javax.swing.text.Document;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.parsing.api.Snapshot;

public class TokenList {
    private CompilationInfo info;
    private SourcePositions sourcePositions;
    private Document doc;
    private AtomicBoolean cancel;
    private boolean topLevelIsJava;
    private TokenSequence topLevel;
    private TokenSequence ts;

    public TokenList(CompilationInfo info, final Document doc, AtomicBoolean cancel) {
        this.info = info;
        this.doc = doc;
        this.cancel = cancel;
        this.sourcePositions = info.getTrees().getSourcePositions();
        doc.render(new Runnable(){

            @Override
            public void run() {
                if (TokenList.this.cancel.get()) {
                    return;
                }
                TokenList.this.topLevel = TokenHierarchy.get((Document)doc).tokenSequence();
                TokenList.this.topLevelIsJava = TokenList.this.topLevel.language() == JavaTokenId.language();
                if (TokenList.this.topLevelIsJava) {
                    TokenList.this.ts = TokenList.this.topLevel;
                    TokenList.this.ts.moveStart();
                    TokenList.this.ts.moveNext();
                }
            }
        });
    }

    public void moveToOffset(long inputOffset) {
        final int offset = this.info.getSnapshot().getOriginalOffset((int)inputOffset);
        if (offset < 0) {
            return;
        }
        this.doc.render(new Runnable(){

            @Override
            public void run() {
                if (TokenList.this.cancel.get()) {
                    return;
                }
                if (TokenList.this.ts != null && !TokenList.this.ts.isValid()) {
                    TokenList.this.cancel.set(true);
                    return;
                }
                if (TokenList.this.topLevelIsJava) {
                    while (TokenList.this.ts.offset() < offset) {
                        if (TokenList.this.ts.moveNext()) continue;
                        return;
                    }
                } else {
                    ArrayList seqs;
                    Iterator embeddedSeqs = null;
                    if (TokenList.this.ts == null) {
                        seqs = new ArrayList(TokenList.embeddedTokenSequences(TokenHierarchy.get((Document)TokenList.this.doc), offset));
                        Collections.reverse(seqs);
                        embeddedSeqs = seqs.iterator();
                        while (embeddedSeqs.hasNext()) {
                            TokenSequence tseq = (TokenSequence)embeddedSeqs.next();
                            if (tseq.language() != JavaTokenId.language()) continue;
                            TokenList.this.ts = tseq;
                            break;
                        }
                    }
                    block2 : while (TokenList.this.ts != null && TokenList.this.ts.offset() < offset) {
                        if (TokenList.this.ts.moveNext()) continue;
                        TokenList.this.ts = null;
                        if (embeddedSeqs == null) {
                            seqs = new ArrayList(TokenList.embeddedTokenSequences(TokenHierarchy.get((Document)TokenList.this.doc), offset));
                            Collections.reverse(seqs);
                            embeddedSeqs = seqs.iterator();
                        }
                        while (embeddedSeqs.hasNext()) {
                            TokenSequence tseq = (TokenSequence)embeddedSeqs.next();
                            if (tseq.language() != JavaTokenId.language()) continue;
                            TokenList.this.ts = tseq;
                            continue block2;
                        }
                    }
                }
            }
        });
    }

    public void moveToEnd(Tree t) {
        if (t == null) {
            return;
        }
        long end = this.sourcePositions.getEndPosition(this.info.getCompilationUnit(), t);
        if (end == -1) {
            return;
        }
        if (t.getKind() == Tree.Kind.ARRAY_TYPE) {
            this.moveToEnd(((ArrayTypeTree)t).getType());
            return;
        }
        this.moveToOffset(end);
    }

    public void moveToEnd(Collection<? extends Tree> trees) {
        if (trees == null) {
            return;
        }
        for (Tree t : trees) {
            this.moveToEnd(t);
        }
    }

    public void firstIdentifier(final TreePath tp, final String name, final Map<Tree, Token> tree2Token) {
        this.doc.render(new Runnable(){

            @Override
            public void run() {
                if (TokenList.this.cancel.get()) {
                    return;
                }
                if (TokenList.this.ts != null && !TokenList.this.ts.isValid()) {
                    TokenList.this.cancel.set(true);
                    return;
                }
                if (TokenList.this.ts == null) {
                    return;
                }
                boolean next = true;
                while (TokenList.this.ts.token().id() != JavaTokenId.IDENTIFIER && (next = TokenList.this.ts.moveNext())) {
                }
                if (next && name.equals(TokenList.this.info.getTreeUtilities().decodeIdentifier(TokenList.this.ts.token().text()).toString())) {
                    tree2Token.put(tp.getLeaf(), TokenList.this.ts.token());
                }
            }
        });
    }

    public void identifierHere(final IdentifierTree tree, final Map<Tree, Token> tree2Token) {
        this.doc.render(new Runnable(){

            @Override
            public void run() {
                if (TokenList.this.cancel.get()) {
                    return;
                }
                if (TokenList.this.ts != null && !TokenList.this.ts.isValid()) {
                    TokenList.this.cancel.set(true);
                    return;
                }
                if (TokenList.this.ts == null) {
                    return;
                }
                Token t = TokenList.this.ts.token();
                if (t.id() == JavaTokenId.IDENTIFIER && tree.getName().toString().equals(TokenList.this.info.getTreeUtilities().decodeIdentifier(t.text()).toString())) {
                    tree2Token.put(tree, TokenList.this.ts.token());
                }
            }
        });
    }

    public void moveBefore(final List<? extends Tree> tArgs) {
        this.doc.render(new Runnable(){

            @Override
            public void run() {
                if (TokenList.this.cancel.get()) {
                    return;
                }
                if (TokenList.this.ts != null && !TokenList.this.ts.isValid()) {
                    TokenList.this.cancel.set(true);
                    return;
                }
                if (TokenList.this.ts == null) {
                    return;
                }
                if (!tArgs.isEmpty()) {
                    int offset = (int)TokenList.this.info.getTrees().getSourcePositions().getStartPosition(TokenList.this.info.getCompilationUnit(), (Tree)tArgs.get(0));
                    offset = TokenList.this.info.getSnapshot().getOriginalOffset(offset);
                    if (offset < 0) {
                        return;
                    }
                    while (TokenList.this.ts.offset() >= offset) {
                        if (TokenList.this.ts.movePrevious()) continue;
                        return;
                    }
                }
            }
        });
    }

    public void moveNext() {
        this.doc.render(new Runnable(){

            @Override
            public void run() {
                if (TokenList.this.cancel.get()) {
                    return;
                }
                if (TokenList.this.ts != null && !TokenList.this.ts.isValid()) {
                    TokenList.this.cancel.set(true);
                    return;
                }
                if (TokenList.this.ts == null) {
                    return;
                }
                TokenList.this.ts.moveNext();
            }
        });
    }

    private static List<TokenSequence<?>> embeddedTokenSequences(TokenHierarchy<Document> th, int offset) {
        TokenSequence embedded = th.tokenSequence();
        ArrayList sequences = new ArrayList();
        do {
            TokenSequence seq = embedded;
            embedded = null;
            seq.move(offset);
            if (!seq.moveNext()) continue;
            sequences.add((TokenSequence)seq);
            embedded = seq.embedded();
        } while (embedded != null);
        return sequences;
    }

}

