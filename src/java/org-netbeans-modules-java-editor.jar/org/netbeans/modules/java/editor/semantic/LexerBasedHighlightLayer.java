/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.spi.editor.highlighting.HighlightsSequence
 *  org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer
 */
package org.netbeans.modules.java.editor.semantic;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.java.editor.semantic.ColoringAttributes;
import org.netbeans.modules.java.editor.semantic.ColoringManager;
import org.netbeans.modules.java.editor.semantic.EmbeddedLexerBasedHighlightSequence;
import org.netbeans.modules.java.editor.semantic.LexerBasedHighlightSequence;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer;

public class LexerBasedHighlightLayer
extends AbstractHighlightsContainer {
    private Map<Token, ColoringAttributes.Coloring> colorings;
    private Map<ColoringAttributes.Coloring, AttributeSet> CACHE = new HashMap<ColoringAttributes.Coloring, AttributeSet>();
    private Document doc;

    public static LexerBasedHighlightLayer getLayer(Class id, Document doc) {
        LexerBasedHighlightLayer l = (LexerBasedHighlightLayer)((Object)doc.getProperty(id));
        if (l == null) {
            l = new LexerBasedHighlightLayer(doc);
            doc.putProperty(id, (Object)l);
        }
        return l;
    }

    private LexerBasedHighlightLayer(Document doc) {
        this.doc = doc;
        this.colorings = Collections.emptyMap();
    }

    public void setColorings(final Map<Token, ColoringAttributes.Coloring> colorings, final Set<Token> addedTokens, Set<Token> removedTokens) {
        this.doc.render(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                LexerBasedHighlightLayer lexerBasedHighlightLayer = LexerBasedHighlightLayer.this;
                synchronized (lexerBasedHighlightLayer) {
                    LexerBasedHighlightLayer.this.colorings = colorings;
                    if (!addedTokens.isEmpty()) {
                        if (addedTokens.size() < 30) {
                            int startOffset = Integer.MAX_VALUE;
                            int endOffset = -1;
                            for (Token t : addedTokens) {
                                int tOffset = t.offset(null);
                                startOffset = Math.min(tOffset, startOffset);
                                endOffset = Math.max(endOffset, tOffset + t.length());
                            }
                            LexerBasedHighlightLayer.this.fireHighlightsChange(startOffset, endOffset);
                        } else {
                            LexerBasedHighlightLayer.this.fireHighlightsChange(0, LexerBasedHighlightLayer.this.doc.getLength());
                        }
                    }
                }
            }
        });
    }

    public synchronized Map<Token, ColoringAttributes.Coloring> getColorings() {
        return this.colorings;
    }

    public synchronized HighlightsSequence getHighlights(int startOffset, int endOffset) {
        if (this.colorings.isEmpty()) {
            return HighlightsSequence.EMPTY;
        }
        TokenHierarchy th = TokenHierarchy.get((Document)this.doc);
        TokenSequence seq = th.tokenSequence();
        if (seq == null) {
            return HighlightsSequence.EMPTY;
        }
        if (seq.language() == JavaTokenId.language()) {
            return new LexerBasedHighlightSequence(this, seq.subSequence(startOffset, endOffset), this.colorings);
        }
        return new EmbeddedLexerBasedHighlightSequence(this, seq.subSequence(startOffset, endOffset), this.colorings);
    }

    public synchronized void clearColoringCache() {
        this.CACHE.clear();
    }

    synchronized AttributeSet getColoring(ColoringAttributes.Coloring c) {
        AttributeSet a = this.CACHE.get(c);
        if (a == null) {
            a = ColoringManager.getColoringImpl(c);
            this.CACHE.put(c, a);
        }
        return a;
    }

}

