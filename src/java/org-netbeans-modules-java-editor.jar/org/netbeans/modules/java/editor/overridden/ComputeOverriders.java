/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.queries.SourceForBinaryQuery
 *  org.netbeans.api.java.source.ClassIndex
 *  org.netbeans.api.java.source.ClassIndex$SearchKind
 *  org.netbeans.api.java.source.ClassIndex$SearchScope
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.TopologicalSortException
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.editor.overridden;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.queries.SourceForBinaryQuery;
import org.netbeans.api.java.source.ClassIndex;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.Task;
import org.netbeans.modules.java.editor.overridden.ElementDescription;
import org.netbeans.modules.java.editor.overridden.GoToImplementation;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.TopologicalSortException;
import org.openide.util.Utilities;

public class ComputeOverriders {
    private static final Logger LOG = Logger.getLogger(ComputeOverriders.class.getName());
    private final AtomicBoolean cancel;
    static List<URL> reverseSourceRootsInOrderOverride;
    static Map<URL, List<URL>> dependenciesOverride;
    static Map<URL, List<URL>> rootPeers;

    public ComputeOverriders(AtomicBoolean cancel) {
        this.cancel = cancel;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static Set<URL> findReverseSourceRoots(URL thisSourceRoot, Map<URL, List<URL>> sourceDeps, Map<URL, List<URL>> rootPeers, FileObject thisFile) {
        Serializable u2;
        long startTime = System.currentTimeMillis();
        try {
            List<URL> peers;
            Serializable u2;
            HashMap<URL, ArrayList<URL>> inverseDeps = new HashMap<URL, ArrayList<URL>>();
            for (Map.Entry<URL, List<URL>> entry : sourceDeps.entrySet()) {
                URL u1 = entry.getKey();
                List<URL> l1 = entry.getValue();
                for (URL u22 : l1) {
                    ArrayList<URL> l2 = (ArrayList<URL>)inverseDeps.get(u22);
                    if (l2 == null) {
                        l2 = new ArrayList<URL>();
                        inverseDeps.put(u22, l2);
                    }
                    l2.add(u1);
                }
            }
            HashSet<HashSet<URL>> result = new HashSet<HashSet<URL>>();
            LinkedList<URL> todo = new LinkedList<URL>();
            todo.add(thisSourceRoot);
            List<URL> list = peers = rootPeers != null ? rootPeers.get(thisSourceRoot) : null;
            if (peers != null) {
                todo.addAll(peers);
            }
            while (!todo.isEmpty()) {
                u2 = (URL)todo.removeFirst();
                if (result.contains(u2)) continue;
                result.add((HashSet<URL>)u2);
                List ideps = (List)inverseDeps.get(u2);
                if (ideps == null) continue;
                todo.addAll(ideps);
            }
            u2 = result;
        }
        catch (Throwable var14_14) {
            long endTime = System.currentTimeMillis();
            Logger.getLogger("TIMER").log(Level.FINE, "Find Reverse Source Roots", new Object[]{thisFile, endTime - startTime});
            throw var14_14;
        }
        long endTime = System.currentTimeMillis();
        Logger.getLogger("TIMER").log(Level.FINE, "Find Reverse Source Roots", new Object[]{thisFile, endTime - startTime});
        return u2;
    }

    private static FileObject findSourceRoot(FileObject file) {
        ClassPath cp = file != null ? ClassPath.getClassPath((FileObject)file, (String)"classpath/source") : null;
        return cp != null ? cp.findOwnerRoot(file) : null;
    }

    private Set<URL> findBinaryRootsForSourceRoot(FileObject sourceRoot, Map<URL, List<URL>> binaryDeps) {
        HashSet<URL> result = new HashSet<URL>();
        for (URL bin : binaryDeps.keySet()) {
            if (this.cancel.get()) {
                return Collections.emptySet();
            }
            for (FileObject s : SourceForBinaryQuery.findSourceRoots((URL)bin).getRoots()) {
                if (s != sourceRoot) continue;
                result.add(bin);
            }
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    Map<ElementHandle<? extends Element>, List<ElementDescription>> process(CompilationInfo info, TypeElement te, ExecutableElement ee, boolean interactive) {
        Map<ElementHandle<? extends Element>, List<ElementDescription>> map;
        long startTime = System.currentTimeMillis();
        try {
            map = this.processImpl(info, te, ee, interactive);
        }
        catch (Throwable var8_7) {
            Logger.getLogger("TIMER").log(Level.FINE, "Overridden - Total", new Object[]{info.getFileObject(), System.currentTimeMillis() - startTime});
            throw var8_7;
        }
        Logger.getLogger("TIMER").log(Level.FINE, "Overridden - Total", new Object[]{info.getFileObject(), System.currentTimeMillis() - startTime});
        return map;
    }

    private Map<ElementHandle<? extends Element>, List<ElementDescription>> processImpl(CompilationInfo info, TypeElement te, ExecutableElement ee, boolean interactive) {
        FileObject file = info.getFileObject();
        FileObject thisSourceRoot = te != null ? ComputeOverriders.findSourceRoot(SourceUtils.getFile((Element)te, (ClasspathInfo)info.getClasspathInfo())) : ComputeOverriders.findSourceRoot(file);
        if (thisSourceRoot == null) {
            return null;
        }
        HashMap<ElementHandle<TypeElement>, List<ElementHandle<ExecutableElement>>> methods = new HashMap<ElementHandle<TypeElement>, List<ElementHandle<ExecutableElement>>>();
        if (ee == null) {
            if (te == null) {
                ComputeOverriders.fillInMethods(info.getTopLevelElements(), methods);
            } else {
                methods.put((ElementHandle)ElementHandle.create((Element)te), Collections.emptyList());
            }
        } else {
            TypeElement owner = (TypeElement)ee.getEnclosingElement();
            methods.put((ElementHandle)ElementHandle.create((Element)owner), Collections.singletonList(ElementHandle.create((Element)ee)));
        }
        HashMap<ElementHandle<? extends Element>, List<ElementDescription>> overriding = new HashMap<ElementHandle<? extends Element>, List<ElementDescription>>();
        long startTime = System.currentTimeMillis();
        long[] classIndexTime = new long[1];
        Map<URL, Map<ElementHandle<TypeElement>, Set<ElementHandle<TypeElement>>>> users = this.computeUsers(info, thisSourceRoot, methods.keySet(), classIndexTime, interactive);
        long endTime = System.currentTimeMillis();
        if (users == null) {
            return null;
        }
        Logger.getLogger("TIMER").log(Level.FINE, "Overridden Candidates - Class Index", new Object[]{file, classIndexTime[0]});
        Logger.getLogger("TIMER").log(Level.FINE, "Overridden Candidates - Total", new Object[]{file, endTime - startTime});
        FileObject currentFileSourceRoot = ComputeOverriders.findSourceRoot(file);
        if (currentFileSourceRoot != null) {
            try {
                URL rootURL = currentFileSourceRoot.getURL();
                Map<ElementHandle<TypeElement>, Set<ElementHandle<TypeElement>>> overridingHandles = users.remove(rootURL);
                if (overridingHandles != null) {
                    this.computeOverridingForRoot(rootURL, overridingHandles, methods, overriding);
                }
            }
            catch (FileStateInvalidException ex) {
                LOG.log(Level.INFO, null, (Throwable)ex);
            }
        }
        for (Map.Entry<URL, Map<ElementHandle<TypeElement>, Set<ElementHandle<TypeElement>>>> data : users.entrySet()) {
            this.computeOverridingForRoot(data.getKey(), data.getValue(), methods, overriding);
        }
        if (this.cancel.get()) {
            return null;
        }
        return overriding;
    }

    private void computeOverridingForRoot(URL root, Map<ElementHandle<TypeElement>, Set<ElementHandle<TypeElement>>> overridingHandles, Map<ElementHandle<TypeElement>, List<ElementHandle<ExecutableElement>>> methods, Map<ElementHandle<? extends Element>, List<ElementDescription>> overridingResult) {
        for (Map.Entry<ElementHandle<TypeElement>, Set<ElementHandle<TypeElement>>> deps : overridingHandles.entrySet()) {
            if (this.cancel.get()) {
                return;
            }
            this.findOverriddenAnnotations(root, deps.getValue(), deps.getKey(), methods.get(deps.getKey()), overridingResult);
        }
    }

    private static void fillInMethods(Iterable<? extends TypeElement> types, Map<ElementHandle<TypeElement>, List<ElementHandle<ExecutableElement>>> methods) {
        for (TypeElement te : types) {
            LinkedList<ElementHandle> l = new LinkedList<ElementHandle>();
            for (ExecutableElement ee : ElementFilter.methodsIn(te.getEnclosedElements())) {
                l.add(ElementHandle.create((Element)ee));
            }
            methods.put((ElementHandle)ElementHandle.create((Element)te), l);
            ComputeOverriders.fillInMethods(ElementFilter.typesIn(te.getEnclosedElements()), methods);
        }
    }

    private Set<ElementHandle<TypeElement>> computeUsers(URL source, Set<ElementHandle<TypeElement>> base, long[] classIndexCumulative) {
        ClasspathInfo cpinfo = ClasspathInfo.create((ClassPath)ClassPath.EMPTY, (ClassPath)ClassPath.EMPTY, (ClassPath)ClassPathSupport.createClassPath((URL[])new URL[]{source}));
        return this.computeUsers(cpinfo, ClassIndex.SearchScope.SOURCE, base, classIndexCumulative);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    private Set<ElementHandle<TypeElement>> computeUsers(ClasspathInfo cpinfo, ClassIndex.SearchScope scope, Set<ElementHandle<TypeElement>> base, long[] classIndexCumulative) {
        long[] arrl;
        long startTime;
        Set<ElementHandle<TypeElement>> set;
        block5 : {
            long[] arrl2;
            HashSet<ElementHandle<TypeElement>> eh;
            startTime = System.currentTimeMillis();
            try {
                LinkedList<ElementHandle<TypeElement>> l = new LinkedList<ElementHandle<TypeElement>>(base);
                HashSet<ElementHandle<TypeElement>> result = new HashSet<ElementHandle<TypeElement>>();
                HashSet<HashSet<ElementHandle<TypeElement>>> seen = new HashSet<HashSet<ElementHandle<TypeElement>>>();
                while (!l.isEmpty()) {
                    if (this.cancel.get()) {
                        set = null;
                        arrl = classIndexCumulative;
                        break block5;
                    }
                    eh = l.remove(0);
                    if (!seen.add(eh)) continue;
                    result.add((ElementHandle<TypeElement>)eh);
                    Set typeElements = cpinfo.getClassIndex().getElements(eh, Collections.singleton(ClassIndex.SearchKind.IMPLEMENTORS), EnumSet.of(scope));
                    if (typeElements == null) continue;
                    l.addAll(typeElements);
                }
                eh = result;
                arrl2 = classIndexCumulative;
            }
            catch (Throwable var12_11) {
                long[] arrl3 = classIndexCumulative;
                arrl3[0] = arrl3[0] + (System.currentTimeMillis() - startTime);
                throw var12_11;
            }
            arrl2[0] = arrl2[0] + (System.currentTimeMillis() - startTime);
            return eh;
        }
        arrl[0] = arrl[0] + (System.currentTimeMillis() - startTime);
        return set;
    }

    private List<URL> reverseSourceRootsInOrder(CompilationInfo info, URL thisSourceRoot, FileObject thisSourceRootFO, Map<URL, List<URL>> sourceDeps, Map<URL, List<URL>> binaryDeps, Map<URL, List<URL>> rootPeers, boolean interactive) {
        LinkedList<URL> sourceRoots;
        Set sourceRootsSet;
        if (reverseSourceRootsInOrderOverride != null) {
            return reverseSourceRootsInOrderOverride;
        }
        if (sourceDeps.containsKey(thisSourceRoot)) {
            sourceRootsSet = ComputeOverriders.findReverseSourceRoots(thisSourceRoot, sourceDeps, rootPeers, info.getFileObject());
        } else {
            sourceRootsSet = new HashSet();
            for (URL binary : this.findBinaryRootsForSourceRoot(thisSourceRootFO, binaryDeps)) {
                List<URL> deps = binaryDeps.get(binary);
                if (deps == null) continue;
                sourceRootsSet.addAll(deps);
            }
        }
        try {
            sourceRoots = new LinkedList<URL>(Utilities.topologicalSort(sourceDeps.keySet(), sourceDeps));
        }
        catch (TopologicalSortException ex) {
            if (interactive) {
                Exceptions.attachLocalizedMessage((Throwable)ex, (String)NbBundle.getMessage(GoToImplementation.class, (String)"ERR_CycleInDependencies"));
                Exceptions.printStackTrace((Throwable)ex);
            } else {
                LOG.log(Level.FINE, null, (Throwable)ex);
            }
            return null;
        }
        sourceRoots.retainAll(sourceRootsSet);
        Collections.reverse(sourceRoots);
        return sourceRoots;
    }

    private Map<URL, Map<ElementHandle<TypeElement>, Set<ElementHandle<TypeElement>>>> computeUsers(CompilationInfo info, FileObject thisSourceRoot, Set<ElementHandle<TypeElement>> baseHandles, long[] classIndexCumulative, boolean interactive) {
        URL thisSourceRootURL;
        Map<URL, List<URL>> sourceDeps = ComputeOverriders.getDependencies(false);
        Map<URL, List<URL>> binaryDeps = ComputeOverriders.getDependencies(true);
        if (sourceDeps == null || binaryDeps == null) {
            if (interactive) {
                NotifyDescriptor.Message nd = new NotifyDescriptor.Message((Object)NbBundle.getMessage(GoToImplementation.class, (String)"ERR_NoDependencies"), 0);
                DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)nd);
            } else {
                LOG.log(Level.FINE, NbBundle.getMessage(GoToImplementation.class, (String)"ERR_NoDependencies"));
            }
            return null;
        }
        try {
            thisSourceRootURL = thisSourceRoot.getURL();
        }
        catch (FileStateInvalidException ex) {
            Exceptions.printStackTrace((Throwable)ex);
            return null;
        }
        Map<URL, List<URL>> rootPeers = ComputeOverriders.getRootPeers();
        List<URL> sourceRoots = this.reverseSourceRootsInOrder(info, thisSourceRootURL, thisSourceRoot, sourceDeps, binaryDeps, rootPeers, interactive);
        if (sourceRoots == null) {
            return null;
        }
        baseHandles = new HashSet<ElementHandle<TypeElement>>(baseHandles);
        Iterator<ElementHandle<TypeElement>> it = baseHandles.iterator();
        while (it.hasNext()) {
            if (this.cancel.get()) {
                return null;
            }
            if (!it.next().getBinaryName().contentEquals("java.lang.Object")) continue;
            it.remove();
            break;
        }
        HashMap<ElementHandle, Set<ElementHandle<TypeElement>>> auxHandles = new HashMap<ElementHandle, Set<ElementHandle<TypeElement>>>();
        if (!sourceDeps.containsKey(thisSourceRootURL)) {
            HashSet<URL> binaryRoots = new HashSet<URL>();
            for (URL sr : sourceRoots) {
                List<URL> deps = sourceDeps.get(sr);
                if (deps == null) continue;
                binaryRoots.addAll(deps);
            }
            binaryRoots.retainAll(binaryDeps.keySet());
            for (ElementHandle handle : baseHandles) {
                Set<ElementHandle<TypeElement>> types = this.computeUsers(ClasspathInfo.create((ClassPath)ClassPath.EMPTY, (ClassPath)ClassPathSupport.createClassPath((URL[])binaryRoots.toArray(new URL[0])), (ClassPath)ClassPath.EMPTY), ClassIndex.SearchScope.DEPENDENCIES, Collections.singleton(handle), classIndexCumulative);
                if (types == null || this.cancel.get()) {
                    return null;
                }
                auxHandles.put(handle, types);
            }
        }
        LinkedHashMap<URL, Map<ElementHandle<TypeElement>, Set<ElementHandle<TypeElement>>>> result = new LinkedHashMap<URL, Map<ElementHandle<TypeElement>, Set<ElementHandle<TypeElement>>>>();
        for (URL file : sourceRoots) {
            for (ElementHandle<TypeElement> base : baseHandles) {
                if (this.cancel.get()) {
                    return null;
                }
                HashSet<ElementHandle<TypeElement>> baseTypes = new HashSet<ElementHandle<TypeElement>>();
                baseTypes.add(base);
                Set aux = (Set)auxHandles.get(base);
                if (aux != null) {
                    baseTypes.addAll(aux);
                }
                for (URL dep : sourceDeps.get(file)) {
                    Map<ElementHandle<TypeElement>, Set<ElementHandle<TypeElement>>> depTypesMulti = result.get(dep);
                    Set<ElementHandle<TypeElement>> depTypes = depTypesMulti != null ? depTypesMulti.get(base) : null;
                    if (depTypes == null) continue;
                    baseTypes.addAll(depTypes);
                }
                Set<ElementHandle<TypeElement>> types = this.computeUsers(file, baseTypes, classIndexCumulative);
                if (types == null || this.cancel.get()) {
                    return null;
                }
                types.removeAll(baseTypes);
                Map<ElementHandle<TypeElement>, Set<ElementHandle<TypeElement>>> currentUsers = result.get(file);
                if (currentUsers == null) {
                    currentUsers = new LinkedHashMap<ElementHandle<TypeElement>, Set<ElementHandle<TypeElement>>>();
                    result.put(file, currentUsers);
                }
                currentUsers.put(base, types);
            }
        }
        return result;
    }

    private void findOverriddenAnnotations(URL sourceRoot, final Set<ElementHandle<TypeElement>> users, final ElementHandle<TypeElement> originalType, final List<ElementHandle<ExecutableElement>> methods, final Map<ElementHandle<? extends Element>, List<ElementDescription>> overriding) {
        if (!users.isEmpty()) {
            FileObject sourceRootFile = URLMapper.findFileObject((URL)sourceRoot);
            ClasspathInfo cpinfo = ClasspathInfo.create((FileObject)sourceRootFile);
            JavaSource js = JavaSource.create((ClasspathInfo)cpinfo, (FileObject[])new FileObject[0]);
            try {
                js.runUserActionTask((Task)new Task<CompilationController>(){

                    public void run(CompilationController controller) throws Exception {
                        HashSet<TypeElement> seenElements = new HashSet<TypeElement>();
                        Element resolvedOriginalType = originalType.resolve((CompilationInfo)controller);
                        if (resolvedOriginalType == null) {
                            return;
                        }
                        for (ElementHandle typeHandle : users) {
                            Types types;
                            if (ComputeOverriders.this.cancel.get()) {
                                return;
                            }
                            TypeElement type = (TypeElement)typeHandle.resolve((CompilationInfo)controller);
                            if (type == null || !seenElements.add(type) || !(types = controller.getTypes()).isSubtype(types.erasure(type.asType()), types.erasure(resolvedOriginalType.asType()))) continue;
                            LinkedList<ElementDescription> classOverriders = (LinkedList<ElementDescription>)overriding.get((Object)originalType);
                            if (classOverriders == null) {
                                classOverriders = new LinkedList<ElementDescription>();
                                overriding.put(originalType, classOverriders);
                            }
                            classOverriders.add(new ElementDescription((CompilationInfo)controller, type, true));
                            for (ElementHandle originalMethodHandle : methods) {
                                ExecutableElement originalMethod = (ExecutableElement)originalMethodHandle.resolve((CompilationInfo)controller);
                                if (originalMethod != null) {
                                    ExecutableElement overrider = ComputeOverriders.getImplementationOf((CompilationInfo)controller, originalMethod, type);
                                    if (overrider == null) continue;
                                    ArrayList<ElementDescription> overriddingMethods = (ArrayList<ElementDescription>)overriding.get((Object)originalMethodHandle);
                                    if (overriddingMethods == null) {
                                        overriddingMethods = new ArrayList<ElementDescription>();
                                        overriding.put(originalMethodHandle, overriddingMethods);
                                    }
                                    overriddingMethods.add(new ElementDescription((CompilationInfo)controller, overrider, true));
                                    continue;
                                }
                                Logger.getLogger("global").log(Level.SEVERE, "IsOverriddenAnnotationHandler: originalMethod == null!");
                            }
                        }
                    }
                }, true);
            }
            catch (Exception e) {
                Exceptions.printStackTrace((Throwable)e);
            }
        }
    }

    private static ExecutableElement getImplementationOf(CompilationInfo info, ExecutableElement overridee, TypeElement implementor) {
        for (ExecutableElement overrider : ElementFilter.methodsIn(implementor.getEnclosedElements())) {
            if (!info.getElements().overrides(overrider, overridee, implementor)) continue;
            return overrider;
        }
        return null;
    }

    private static Map<URL, List<URL>> getDependencies(boolean binary) {
        if (dependenciesOverride != null) {
            return dependenciesOverride;
        }
        ClassLoader l = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
        if (l == null) {
            return null;
        }
        Class clazz = null;
        String method = null;
        try {
            clazz = l.loadClass("org.netbeans.modules.parsing.impl.indexing.friendapi.IndexingController");
            method = binary ? "getBinaryRootDependencies" : "getRootDependencies";
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger(GoToImplementation.class.getName()).log(Level.FINE, null, ex);
            try {
                clazz = l.loadClass("org.netbeans.modules.parsing.impl.indexing.RepositoryUpdater");
                method = binary ? "getDependencies" : "doesnotexist";
            }
            catch (ClassNotFoundException inner) {
                Logger.getLogger(GoToImplementation.class.getName()).log(Level.FINE, null, inner);
                return null;
            }
        }
        try {
            Method getDefault = clazz.getDeclaredMethod("getDefault", new Class[0]);
            Object instance = getDefault.invoke(null, new Object[0]);
            Method dependenciesMethod = clazz.getDeclaredMethod(method, new Class[0]);
            return (Map)dependenciesMethod.invoke(instance, new Object[0]);
        }
        catch (IllegalAccessException ex) {
            Logger.getLogger(GoToImplementation.class.getName()).log(Level.FINE, null, ex);
            return null;
        }
        catch (IllegalArgumentException ex) {
            Logger.getLogger(GoToImplementation.class.getName()).log(Level.FINE, null, ex);
            return null;
        }
        catch (InvocationTargetException ex) {
            Logger.getLogger(GoToImplementation.class.getName()).log(Level.FINE, null, ex);
            return null;
        }
        catch (NoSuchMethodException ex) {
            Logger.getLogger(GoToImplementation.class.getName()).log(Level.FINE, null, ex);
            return null;
        }
        catch (SecurityException ex) {
            Logger.getLogger(GoToImplementation.class.getName()).log(Level.FINE, null, ex);
            return null;
        }
        catch (ClassCastException ex) {
            Logger.getLogger(GoToImplementation.class.getName()).log(Level.FINE, null, ex);
            return null;
        }
    }

    private static Map<URL, List<URL>> getRootPeers() {
        if (rootPeers != null) {
            return rootPeers;
        }
        ClassLoader l = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
        if (l == null) {
            return null;
        }
        Class clazz = null;
        String method = null;
        try {
            clazz = l.loadClass("org.netbeans.modules.parsing.impl.indexing.friendapi.IndexingController");
            method = "getRootPeers";
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger(GoToImplementation.class.getName()).log(Level.FINE, null, ex);
            return null;
        }
        try {
            Method getDefault = clazz.getDeclaredMethod("getDefault", new Class[0]);
            Object instance = getDefault.invoke(null, new Object[0]);
            Method peersMethod = clazz.getDeclaredMethod(method, new Class[0]);
            return (Map)peersMethod.invoke(instance, new Object[0]);
        }
        catch (IllegalAccessException ex) {
            Logger.getLogger(GoToImplementation.class.getName()).log(Level.FINE, null, ex);
            return null;
        }
        catch (IllegalArgumentException ex) {
            Logger.getLogger(GoToImplementation.class.getName()).log(Level.FINE, null, ex);
            return null;
        }
        catch (InvocationTargetException ex) {
            Logger.getLogger(GoToImplementation.class.getName()).log(Level.FINE, null, ex);
            return null;
        }
        catch (NoSuchMethodException ex) {
            Logger.getLogger(GoToImplementation.class.getName()).log(Level.FINE, null, ex);
            return null;
        }
        catch (SecurityException ex) {
            Logger.getLogger(GoToImplementation.class.getName()).log(Level.FINE, null, ex);
            return null;
        }
        catch (ClassCastException ex) {
            Logger.getLogger(GoToImplementation.class.getName()).log(Level.FINE, null, ex);
            return null;
        }
    }

}

