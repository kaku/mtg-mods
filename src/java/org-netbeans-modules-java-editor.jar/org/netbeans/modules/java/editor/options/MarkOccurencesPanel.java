/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.options;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.java.editor.options.MarkOccurencesOptionsPanelController;
import org.netbeans.modules.java.editor.options.MarkOccurencesSettings;
import org.openide.awt.Mnemonics;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class MarkOccurencesPanel
extends JPanel {
    private static final boolean DEFAULT_VALUE = true;
    private List<JCheckBox> boxes;
    private MarkOccurencesOptionsPanelController controller;
    private boolean changed = false;
    private JCheckBox breakContinueCheckBox;
    private JCheckBox constantsCheckBox;
    private JCheckBox exceptionsCheckBox;
    private JCheckBox exitCheckBox;
    private JCheckBox fieldsCheckBox;
    private JCheckBox implementsCheckBox;
    private JCheckBox keepMarks;
    private JCheckBox localVariablesCheckBox;
    private JCheckBox methodsCheckBox;
    private JCheckBox onOffCheckBox;
    private JCheckBox overridesCheckBox;
    private JCheckBox typesCheckBox;

    public MarkOccurencesPanel(MarkOccurencesOptionsPanelController controller) {
        this.initComponents();
        this.fillBoxes();
        this.addListeners();
        this.load(controller);
    }

    public void load(MarkOccurencesOptionsPanelController controller) {
        this.controller = controller;
        Preferences node = MarkOccurencesSettings.getCurrentNode();
        for (JCheckBox box : this.boxes) {
            box.setSelected(node.getBoolean(box.getActionCommand(), true));
        }
        this.componentsSetEnabled();
        this.changed = false;
    }

    public void store() {
        Preferences node = MarkOccurencesSettings.getCurrentNode();
        for (JCheckBox box : this.boxes) {
            boolean original;
            boolean value = box.isSelected();
            if (value == (original = node.getBoolean(box.getActionCommand(), true))) continue;
            node.putBoolean(box.getActionCommand(), value);
        }
        try {
            node.flush();
        }
        catch (BackingStoreException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        this.changed = false;
    }

    public boolean changed() {
        return this.changed;
    }

    private void initComponents() {
        this.onOffCheckBox = new JCheckBox();
        this.typesCheckBox = new JCheckBox();
        this.methodsCheckBox = new JCheckBox();
        this.constantsCheckBox = new JCheckBox();
        this.fieldsCheckBox = new JCheckBox();
        this.localVariablesCheckBox = new JCheckBox();
        this.exceptionsCheckBox = new JCheckBox();
        this.exitCheckBox = new JCheckBox();
        this.implementsCheckBox = new JCheckBox();
        this.overridesCheckBox = new JCheckBox();
        this.breakContinueCheckBox = new JCheckBox();
        this.keepMarks = new JCheckBox();
        this.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
        this.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((AbstractButton)this.onOffCheckBox, (String)NbBundle.getMessage(MarkOccurencesPanel.class, (String)"CTL_OnOff_CheckBox"));
        this.onOffCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.onOffCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.onOffCheckBox.setOpaque(false);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 0, 12, 0);
        this.add((Component)this.onOffCheckBox, gridBagConstraints);
        this.onOffCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(MarkOccurencesPanel.class, (String)"ACSD_OnOff_CB"));
        Mnemonics.setLocalizedText((AbstractButton)this.typesCheckBox, (String)NbBundle.getMessage(MarkOccurencesPanel.class, (String)"CTL_Types_CheckBox"));
        this.typesCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.typesCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.typesCheckBox.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 20, 8, 0);
        this.add((Component)this.typesCheckBox, gridBagConstraints);
        this.typesCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(MarkOccurencesPanel.class, (String)"ACSD_Types_CB"));
        Mnemonics.setLocalizedText((AbstractButton)this.methodsCheckBox, (String)NbBundle.getMessage(MarkOccurencesPanel.class, (String)"CTL_Methods_CheckBox"));
        this.methodsCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.methodsCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.methodsCheckBox.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 20, 8, 0);
        this.add((Component)this.methodsCheckBox, gridBagConstraints);
        this.methodsCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(MarkOccurencesPanel.class, (String)"ACSD_Methods_CB"));
        Mnemonics.setLocalizedText((AbstractButton)this.constantsCheckBox, (String)NbBundle.getMessage(MarkOccurencesPanel.class, (String)"CTL_Constants_CheckBox"));
        this.constantsCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.constantsCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.constantsCheckBox.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 20, 8, 0);
        this.add((Component)this.constantsCheckBox, gridBagConstraints);
        this.constantsCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(MarkOccurencesPanel.class, (String)"ACSD_Constants_CB"));
        Mnemonics.setLocalizedText((AbstractButton)this.fieldsCheckBox, (String)NbBundle.getMessage(MarkOccurencesPanel.class, (String)"CTL_Fields_CheckBox"));
        this.fieldsCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.fieldsCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.fieldsCheckBox.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 20, 8, 0);
        this.add((Component)this.fieldsCheckBox, gridBagConstraints);
        this.fieldsCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(MarkOccurencesPanel.class, (String)"ACSD_Fields_CB"));
        Mnemonics.setLocalizedText((AbstractButton)this.localVariablesCheckBox, (String)NbBundle.getMessage(MarkOccurencesPanel.class, (String)"CTL_LocalVariables_CheckBox"));
        this.localVariablesCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.localVariablesCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.localVariablesCheckBox.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 20, 8, 0);
        this.add((Component)this.localVariablesCheckBox, gridBagConstraints);
        this.localVariablesCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(MarkOccurencesPanel.class, (String)"ACSD_Variables_CB"));
        Mnemonics.setLocalizedText((AbstractButton)this.exceptionsCheckBox, (String)NbBundle.getMessage(MarkOccurencesPanel.class, (String)"CTL_Exceptions_CheckBox"));
        this.exceptionsCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.exceptionsCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.exceptionsCheckBox.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 20, 8, 0);
        this.add((Component)this.exceptionsCheckBox, gridBagConstraints);
        this.exceptionsCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(MarkOccurencesPanel.class, (String)"ACSD_Exceptions_CB"));
        Mnemonics.setLocalizedText((AbstractButton)this.exitCheckBox, (String)NbBundle.getMessage(MarkOccurencesPanel.class, (String)"CTL_Exit_CheckBox"));
        this.exitCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.exitCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.exitCheckBox.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 20, 8, 0);
        this.add((Component)this.exitCheckBox, gridBagConstraints);
        this.exitCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(MarkOccurencesPanel.class, (String)"ACSD_exitpoints_CB"));
        Mnemonics.setLocalizedText((AbstractButton)this.implementsCheckBox, (String)NbBundle.getMessage(MarkOccurencesPanel.class, (String)"CTL_Implements_CheckBox"));
        this.implementsCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.implementsCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.implementsCheckBox.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 20, 8, 0);
        this.add((Component)this.implementsCheckBox, gridBagConstraints);
        this.implementsCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(MarkOccurencesPanel.class, (String)"ACSD_implementing_CB"));
        Mnemonics.setLocalizedText((AbstractButton)this.overridesCheckBox, (String)NbBundle.getMessage(MarkOccurencesPanel.class, (String)"CTL_Overrides_CheckBox"));
        this.overridesCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.overridesCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.overridesCheckBox.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 20, 8, 0);
        this.add((Component)this.overridesCheckBox, gridBagConstraints);
        this.overridesCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(MarkOccurencesPanel.class, (String)"ACSD_overriding_CB"));
        Mnemonics.setLocalizedText((AbstractButton)this.breakContinueCheckBox, (String)NbBundle.getMessage(MarkOccurencesPanel.class, (String)"CTL_BreakContinue_CheckBox"));
        this.breakContinueCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.breakContinueCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.breakContinueCheckBox.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 20, 8, 0);
        this.add((Component)this.breakContinueCheckBox, gridBagConstraints);
        this.breakContinueCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(MarkOccurencesPanel.class, (String)"ACSD_Targets_CB"));
        Mnemonics.setLocalizedText((AbstractButton)this.keepMarks, (String)NbBundle.getBundle(MarkOccurencesPanel.class).getString("CTL_KeepMarks_CheckBox"));
        this.keepMarks.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.keepMarks.setMargin(new Insets(0, 0, 0, 0));
        this.keepMarks.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.gridheight = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(20, 20, 8, 0);
        this.add((Component)this.keepMarks, gridBagConstraints);
        this.keepMarks.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(MarkOccurencesPanel.class, (String)"ACSD_Marks_CB"));
    }

    private void fillBoxes() {
        this.boxes = new ArrayList<JCheckBox>();
        this.boxes.add(this.onOffCheckBox);
        this.boxes.add(this.typesCheckBox);
        this.boxes.add(this.methodsCheckBox);
        this.boxes.add(this.constantsCheckBox);
        this.boxes.add(this.fieldsCheckBox);
        this.boxes.add(this.localVariablesCheckBox);
        this.boxes.add(this.exceptionsCheckBox);
        this.boxes.add(this.exitCheckBox);
        this.boxes.add(this.implementsCheckBox);
        this.boxes.add(this.overridesCheckBox);
        this.boxes.add(this.breakContinueCheckBox);
        this.boxes.add(this.keepMarks);
        this.onOffCheckBox.setActionCommand(MarkOccurencesSettings.ON_OFF);
        this.typesCheckBox.setActionCommand(MarkOccurencesSettings.TYPES);
        this.methodsCheckBox.setActionCommand(MarkOccurencesSettings.METHODS);
        this.constantsCheckBox.setActionCommand(MarkOccurencesSettings.CONSTANTS);
        this.fieldsCheckBox.setActionCommand(MarkOccurencesSettings.FIELDS);
        this.localVariablesCheckBox.setActionCommand(MarkOccurencesSettings.LOCAL_VARIABLES);
        this.exceptionsCheckBox.setActionCommand(MarkOccurencesSettings.EXCEPTIONS);
        this.exitCheckBox.setActionCommand(MarkOccurencesSettings.EXIT);
        this.implementsCheckBox.setActionCommand(MarkOccurencesSettings.IMPLEMENTS);
        this.overridesCheckBox.setActionCommand(MarkOccurencesSettings.OVERRIDES);
        this.breakContinueCheckBox.setActionCommand(MarkOccurencesSettings.BREAK_CONTINUE);
        this.keepMarks.setActionCommand(MarkOccurencesSettings.KEEP_MARKS);
    }

    private void addListeners() {
        CheckChangeListener cl = new CheckChangeListener();
        for (JCheckBox box : this.boxes) {
            box.addChangeListener(cl);
        }
    }

    private void componentsSetEnabled() {
        for (int i = 1; i < this.boxes.size(); ++i) {
            this.boxes.get(i).setEnabled(this.onOffCheckBox.isSelected());
        }
    }

    private void fireChanged() {
        Preferences node = MarkOccurencesSettings.getCurrentNode();
        for (JCheckBox box : this.boxes) {
            if (node.getBoolean(box.getActionCommand(), true) == box.isSelected()) continue;
            this.changed = true;
            return;
        }
        this.changed = false;
    }

    private class CheckChangeListener
    implements ChangeListener {
        private CheckChangeListener() {
        }

        @Override
        public void stateChanged(ChangeEvent evt) {
            if (evt.getSource().equals(MarkOccurencesPanel.this.onOffCheckBox)) {
                MarkOccurencesPanel.this.componentsSetEnabled();
            }
            MarkOccurencesPanel.this.fireChanged();
        }
    }

}

