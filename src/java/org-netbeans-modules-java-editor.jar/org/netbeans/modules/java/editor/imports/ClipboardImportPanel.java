/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.imports;

import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ListModel;
import org.openide.util.NbBundle;

public class ClipboardImportPanel
extends JPanel {
    private final List<String> fqns;
    private JLabel jLabel1;
    private JList jList1;
    private JScrollPane jScrollPane1;

    public ClipboardImportPanel(Collection<String> fqns) {
        this.fqns = new ArrayList<String>(fqns);
        Collections.sort(this.fqns);
        this.initComponents();
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this.jList1 = new JList();
        this.jLabel1.setText(NbBundle.getMessage(ClipboardImportPanel.class, (String)"ClipboardImportPanel.jLabel1.text"));
        this.jList1.setModel(this.createImportListModel());
        this.jList1.setEnabled(false);
        this.jScrollPane1.setViewportView(this.jList1);
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false).addComponent(this.jScrollPane1, GroupLayout.Alignment.LEADING).addComponent(this.jLabel1, GroupLayout.Alignment.LEADING, -1, -1, 32767)).addContainerGap(-1, 32767)));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jLabel1).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jScrollPane1, -1, 255, 32767).addContainerGap()));
    }

    private ListModel createImportListModel() {
        DefaultListModel<String> m = new DefaultListModel<String>();
        for (String fqn : this.fqns) {
            m.addElement(fqn);
        }
        return m;
    }
}

