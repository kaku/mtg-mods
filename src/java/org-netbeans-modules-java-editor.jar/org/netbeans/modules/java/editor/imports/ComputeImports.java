/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.AnnotationTree
 *  com.sun.source.tree.AssignmentTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodInvocationTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.NewArrayTree
 *  com.sun.source.tree.NewClassTree
 *  com.sun.source.tree.ParameterizedTypeTree
 *  com.sun.source.tree.Scope
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.source.ClassIndex
 *  org.netbeans.api.java.source.ClassIndex$NameKind
 *  org.netbeans.api.java.source.ClassIndex$SearchScope
 *  org.netbeans.api.java.source.ClassIndex$Symbols
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.CompilationInfo$CacheClearPolicy
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.ElementUtilities
 *  org.netbeans.api.java.source.ElementUtilities$ElementAcceptor
 *  org.netbeans.api.java.source.TypeUtilities
 *  org.netbeans.api.java.source.TypeUtilities$TypeNameOptions
 *  org.netbeans.api.java.source.support.CancellableTreePathScanner
 *  org.openide.util.Union2
 */
package org.netbeans.modules.java.editor.imports;

import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.NewArrayTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.ParameterizedTypeTree;
import com.sun.source.tree.Scope;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ExecutableType;
import javax.lang.model.type.NullType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.source.ClassIndex;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.java.source.TypeUtilities;
import org.netbeans.api.java.source.support.CancellableTreePathScanner;
import org.netbeans.modules.editor.java.Utilities;
import org.netbeans.modules.java.editor.javadoc.JavadocImports;
import org.openide.util.Union2;

public class ComputeImports {
    private static final String ERROR = "<error>";
    private boolean cancelled;
    private static final Object IMPORT_CANDIDATES_KEY = new Object();
    private TreeVisitorImpl visitor;
    private static final String INIT = "<init>";
    private static EnumSet<TypeKind> INVALID_TYPES = EnumSet.of(TypeKind.NULL, TypeKind.NONE, TypeKind.OTHER, TypeKind.ERROR);

    public synchronized void cancel() {
        this.cancelled = true;
        if (this.visitor != null) {
            this.visitor.cancel();
        }
    }

    private synchronized boolean isCancelled() {
        return this.cancelled;
    }

    public Pair<Map<String, List<Element>>, Map<String, List<Element>>> computeCandidates(CompilationInfo info) {
        Pair<Map<String, List<Element>>, Map<String, List<Element>>> result = (Pair<Map<String, List<Element>>, Map<String, List<Element>>>)info.getCachedValue(IMPORT_CANDIDATES_KEY);
        if (result != null) {
            return result;
        }
        result = this.computeCandidates(info, Collections.emptySet());
        if (!this.isCancelled() && result != null) {
            info.putCachedValue(IMPORT_CANDIDATES_KEY, result, CompilationInfo.CacheClearPolicy.ON_CHANGE);
        }
        return result;
    }

    private synchronized void setVisitor(TreeVisitorImpl visitor) {
        this.visitor = visitor;
    }

    Pair<Map<String, List<Element>>, Map<String, List<Element>>> computeCandidates(CompilationInfo info, Set<String> forcedUnresolved) {
        HashMap<String, List<Element>> candidates = new HashMap<String, List<Element>>();
        HashMap<String, List<Element>> notFilteredCandidates = new HashMap<String, List<Element>>();
        TreeVisitorImpl v = new TreeVisitorImpl(info);
        this.setVisitor(v);
        v.scan((Tree)info.getCompilationUnit(), new HashMap());
        this.setVisitor(null);
        HashSet<String> unresolvedNames = new HashSet<String>(v.unresolved);
        unresolvedNames.addAll(forcedUnresolved);
        unresolvedNames.addAll(JavadocImports.computeUnresolvedImports(info));
        for (String unresolved : unresolvedNames) {
            if (this.isCancelled()) {
                return null;
            }
            ArrayList<Element> classes = new ArrayList<Element>();
            Set typeNames = info.getClasspathInfo().getClassIndex().getDeclaredTypes(unresolved, ClassIndex.NameKind.SIMPLE_NAME, EnumSet.allOf(ClassIndex.SearchScope.class));
            if (typeNames == null) {
                return null;
            }
            for (ElementHandle typeName : typeNames) {
                TypeElement te = info.getElements().getTypeElement(typeName.getQualifiedName());
                if (te == null) {
                    Logger.getLogger(ComputeImports.class.getName()).log(Level.INFO, "Cannot resolve type element \"" + (Object)typeName + "\".");
                    continue;
                }
                if (info.getElements().getPackageOf(te).getQualifiedName().length() == 0 || Utilities.isExcluded(te.getQualifiedName())) continue;
                classes.add(te);
            }
            Iterable simpleNames = info.getClasspathInfo().getClassIndex().getDeclaredSymbols(unresolved, ClassIndex.NameKind.SIMPLE_NAME, EnumSet.allOf(ClassIndex.SearchScope.class));
            if (simpleNames == null) {
                return null;
            }
            for (ClassIndex.Symbols p : simpleNames) {
                TypeElement te = (TypeElement)p.getEnclosingType().resolve(info);
                Set idents = p.getSymbols();
                if (te == null) continue;
                for (Element ne : te.getEnclosedElements()) {
                    if (!ne.getModifiers().contains((Object)Modifier.STATIC) || !idents.contains(this.getSimpleName(ne, te))) continue;
                    classes.add(ne);
                }
            }
            candidates.put(unresolved, new ArrayList(classes));
            notFilteredCandidates.put(unresolved, classes);
        }
        boolean wasChanged = true;
        while (wasChanged) {
            if (this.isCancelled()) {
                return new Pair<Map<String, List<Element>>, Map<String, List<Element>>>(Collections.emptyMap(), Collections.emptyMap());
            }
            wasChanged = false;
            for (Hint hint : v.hints) {
                wasChanged |= hint.filter(info, notFilteredCandidates, candidates);
            }
        }
        return new Pair<Map<String, List<Element>>, Map<String, List<Element>>>(candidates, notFilteredCandidates);
    }

    public static String displayNameForImport(@NonNull CompilationInfo info, @NonNull Element element) {
        if (element.getKind().isClass() || element.getKind().isInterface()) {
            return ((TypeElement)element).getQualifiedName().toString();
        }
        StringBuilder fqnSB = new StringBuilder();
        fqnSB.append(((TypeElement)element.getEnclosingElement()).getQualifiedName());
        fqnSB.append('.');
        fqnSB.append(element.getSimpleName());
        if (element.getKind() == ElementKind.METHOD) {
            fqnSB.append('(');
            boolean first = true;
            for (VariableElement var : ((ExecutableElement)element).getParameters()) {
                if (!first) {
                    fqnSB.append(", ");
                }
                fqnSB.append(info.getTypeUtilities().getTypeName(info.getTypes().erasure(var.asType()), new TypeUtilities.TypeNameOptions[0]));
                first = false;
            }
            fqnSB.append(')');
        }
        return fqnSB.toString();
    }

    private String getSimpleName(@NonNull Element element, @NullAllowed Element enclosingElement) {
        String result = element.getSimpleName().toString();
        if (enclosingElement != null && "<init>".equals(result)) {
            result = enclosingElement.getSimpleName().toString();
        }
        return result;
    }

    private static boolean filter(Types types, List<Element> left, List<Element> right, boolean leftReadOnly, boolean rightReadOnly) {
        boolean changed = false;
        HashMap validPairs = new HashMap();
        for (TypeElement l : ElementFilter.typesIn(left)) {
            ArrayList<TypeElement> valid = new ArrayList<TypeElement>();
            for (TypeElement r : ElementFilter.typesIn(right)) {
                TypeMirror t1 = types.erasure(l.asType());
                TypeMirror t2 = types.erasure(r.asType());
                if (!types.isAssignable(t2, t1)) continue;
                valid.add(r);
            }
            validPairs.put(l, valid);
        }
        HashSet validRights = new HashSet();
        for (TypeElement l2 : validPairs.keySet()) {
            List valid = (List)validPairs.get(l2);
            if (valid.isEmpty() && !leftReadOnly) {
                left.remove(l2);
                changed = true;
            }
            validRights.addAll(valid);
        }
        if (!rightReadOnly) {
            changed = right.retainAll(validRights) | changed;
        }
        return changed;
    }

    public static class Pair<A, B> {
        public A a;
        public B b;

        public Pair(A a, B b) {
            this.a = a;
            this.b = b;
        }
    }

    public static final class MethodParamsHint
    implements Hint {
        private final String simpleName;
        private final List<TypeMirror> paramTypes;

        public MethodParamsHint(String simpleName, List<TypeMirror> paramTypes) {
            this.simpleName = simpleName;
            this.paramTypes = paramTypes;
        }

        @Override
        public boolean filter(CompilationInfo info, Map<String, List<Element>> rawCandidates, Map<String, List<Element>> candidates) {
            List<Element> rawCands = rawCandidates.get(this.simpleName);
            List<Element> cands = candidates.get(this.simpleName);
            if (rawCands == null || cands == null) {
                return false;
            }
            boolean modified = false;
            for (Element c : new ArrayList<Element>(rawCands)) {
                if (c.getKind() != ElementKind.METHOD) {
                    rawCands.remove(c);
                    cands.remove(c);
                    modified |= true;
                    continue;
                }
                Iterator<TypeMirror> real = this.paramTypes.iterator();
                Iterator<? extends TypeMirror> formal = ((ExecutableType)c.asType()).getParameterTypes().iterator();
                boolean matches = true;
                boolean inVarArgs = false;
                TypeMirror currentFormal = null;
                while (real.hasNext() && (formal.hasNext() || inVarArgs)) {
                    TypeMirror currentReal = real.next();
                    if (!inVarArgs) {
                        currentFormal = formal.next();
                    }
                    if (info.getTypes().isAssignable(info.getTypes().erasure(currentReal), info.getTypes().erasure(currentFormal))) continue;
                    if (((ExecutableElement)c).isVarArgs() && !formal.hasNext() && currentFormal.getKind() == TypeKind.ARRAY) {
                        currentFormal = ((ArrayType)currentFormal).getComponentType();
                        if (!info.getTypes().isAssignable(info.getTypes().erasure(currentReal), info.getTypes().erasure(currentFormal))) {
                            matches = false;
                            break;
                        }
                        inVarArgs = true;
                        continue;
                    }
                    matches = false;
                    break;
                }
                if (matches &= real.hasNext() == formal.hasNext()) continue;
                rawCands.remove(c);
                cands.remove(c);
                modified |= true;
            }
            return modified;
        }
    }

    public static final class AccessibleHint
    implements Hint {
        private String simpleName;
        private Scope scope;

        public AccessibleHint(String simpleName, Scope scope) {
            this.simpleName = simpleName;
            this.scope = scope;
        }

        @Override
        public boolean filter(CompilationInfo info, Map<String, List<Element>> rawCandidates, Map<String, List<Element>> candidates) {
            List<Element> cands = rawCandidates.get(this.simpleName);
            if (cands == null || cands.isEmpty()) {
                return false;
            }
            ArrayList<Element> toRemove = new ArrayList<Element>();
            for (Element te : cands) {
                if (!(te.getKind().isClass() || te.getKind().isInterface() ? !info.getTrees().isAccessible(this.scope, (TypeElement)te) : !info.getTrees().isAccessible(this.scope, te, (DeclaredType)te.getEnclosingElement().asType()))) continue;
                toRemove.add(te);
            }
            candidates.get(this.simpleName).removeAll(toRemove);
            return cands.removeAll(toRemove);
        }
    }

    public static final class KindHint
    implements Hint {
        private String simpleName;
        private Set<ElementKind> acceptedKinds;
        private Set<ElementKind> notAcceptedKinds;

        public KindHint(String simpleName, Set<ElementKind> acceptedKinds, Set<ElementKind> notAcceptedKinds) {
            this.simpleName = simpleName;
            this.acceptedKinds = acceptedKinds;
            this.notAcceptedKinds = notAcceptedKinds;
        }

        @Override
        public boolean filter(CompilationInfo info, Map<String, List<Element>> rawCandidates, Map<String, List<Element>> candidates) {
            List<Element> cands = candidates.get(this.simpleName);
            if (cands == null || cands.isEmpty()) {
                return false;
            }
            ArrayList<TypeElement> toRemove = new ArrayList<TypeElement>();
            for (TypeElement te : ElementFilter.typesIn(cands)) {
                if (!this.acceptedKinds.isEmpty() && !this.acceptedKinds.contains((Object)te.getKind())) {
                    toRemove.add(te);
                    continue;
                }
                if (this.notAcceptedKinds.isEmpty() || !this.notAcceptedKinds.contains((Object)te.getKind())) continue;
                toRemove.add(te);
            }
            return cands.removeAll(toRemove);
        }
    }

    public static final class EnclosedHint
    implements Hint {
        private String simpleName;
        private String methodName;
        private boolean allowPrefix;

        public EnclosedHint(String simpleName, String methodName, boolean allowPrefix) {
            this.simpleName = simpleName;
            this.methodName = methodName;
            this.allowPrefix = allowPrefix;
        }

        @Override
        public boolean filter(CompilationInfo info, Map<String, List<Element>> rawCandidates, Map<String, List<Element>> candidates) {
            List<Element> cands = candidates.get(this.simpleName);
            if (cands == null || cands.isEmpty()) {
                return false;
            }
            ArrayList<TypeElement> toRemove = new ArrayList<TypeElement>();
            for (TypeElement te : ElementFilter.typesIn(cands)) {
                boolean found = false;
                for (Element e : te.getEnclosedElements()) {
                    String simpleName = e.getSimpleName().toString();
                    if (this.methodName.contentEquals(simpleName)) {
                        found = true;
                        break;
                    }
                    if (!this.allowPrefix || !simpleName.startsWith(this.methodName)) continue;
                    found = true;
                    break;
                }
                if (found) continue;
                toRemove.add(te);
            }
            return cands.removeAll(toRemove);
        }
    }

    public static final class TypeHint
    implements Hint {
        private Union2<String, DeclaredType> left;
        private Union2<String, DeclaredType> right;

        public TypeHint(Union2<String, DeclaredType> left, Union2<String, DeclaredType> right) {
            this.left = left;
            this.right = right;
        }

        @Override
        public boolean filter(CompilationInfo info, Map<String, List<Element>> rawCandidates, Map<String, List<Element>> candidates) {
            Element el;
            List<Element> left = null;
            List<Element> right = null;
            boolean leftReadOnly = false;
            boolean rightReadOnly = false;
            if (this.left.hasSecond()) {
                el = ((DeclaredType)this.left.second()).asElement();
                if (el instanceof TypeElement) {
                    left = Collections.singletonList(el);
                    leftReadOnly = true;
                }
            } else {
                left = candidates.get(this.left.first());
            }
            if (this.right.hasSecond()) {
                el = ((DeclaredType)this.right.second()).asElement();
                if (el instanceof TypeElement) {
                    right = Collections.singletonList(el);
                    rightReadOnly = true;
                }
            } else {
                right = candidates.get(this.right.first());
            }
            if (left != null && right != null && !left.isEmpty() && !right.isEmpty()) {
                return ComputeImports.filter(info.getTypes(), left, right, leftReadOnly, rightReadOnly);
            }
            return false;
        }
    }

    public static interface Hint {
        public boolean filter(CompilationInfo var1, Map<String, List<Element>> var2, Map<String, List<Element>> var3);
    }

    private static class TreeVisitorImpl
    extends CancellableTreePathScanner<Void, Map<String, Object>> {
        private final CompilationInfo info;
        private Set<String> unresolved;
        private List<Hint> hints;
        private Scope topLevelScope;

        public TreeVisitorImpl(CompilationInfo info) {
            this.info = info;
            this.unresolved = new HashSet<String>();
            this.hints = new ArrayList<Hint>();
        }

        public Void visitMemberSelect(MemberSelectTree tree, Map<String, Object> p) {
            if (tree.getExpression().getKind() == Tree.Kind.IDENTIFIER) {
                p.put("request", null);
            }
            this.scan((Tree)tree.getExpression(), p);
            Union2 leftSide = (Union2)p.remove("result");
            p.remove("request");
            if (leftSide != null && leftSide.hasFirst()) {
                boolean isMethodInvocation;
                String rightSide = tree.getIdentifier().toString();
                if ("<error>".equals(rightSide)) {
                    rightSide = "";
                }
                boolean bl = isMethodInvocation = this.getCurrentPath().getParentPath().getLeaf().getKind() == Tree.Kind.METHOD_INVOCATION;
                if (!"class".equals(rightSide)) {
                    this.hints.add(new EnclosedHint((String)leftSide.first(), rightSide, !isMethodInvocation));
                }
            }
            return null;
        }

        public Void visitVariable(VariableTree tree, Map<String, Object> p) {
            this.scan((Tree)tree.getModifiers(), p);
            if (tree.getType() != null && tree.getType().getKind() == Tree.Kind.IDENTIFIER) {
                p.put("request", null);
            }
            this.scan(tree.getType(), p);
            Union2 leftSide = (Union2)p.remove("result");
            p.remove("request");
            Union2 rightSide = null;
            if (leftSide != null && tree.getInitializer() != null) {
                TypeMirror rightType;
                Element el = this.info.getTrees().getElement(new TreePath(this.getCurrentPath(), (Tree)tree.getInitializer()));
                TypeMirror typeMirror = rightType = el != null ? el.asType() : null;
                if (rightType != null && rightType.getKind() == TypeKind.DECLARED) {
                    rightSide = Union2.createSecond((Object)((DeclaredType)rightType));
                } else if (tree.getInitializer().getKind() == Tree.Kind.NEW_CLASS || tree.getInitializer().getKind() == Tree.Kind.NEW_ARRAY) {
                    p.put("request", null);
                }
            }
            this.scan((Tree)tree.getInitializer(), p);
            rightSide = rightSide == null ? (Union2)p.remove("result") : rightSide;
            p.remove("result");
            p.remove("request");
            if (!(leftSide == null || rightSide == null || leftSide instanceof TypeMirror && rightSide instanceof TypeMirror)) {
                this.hints.add(new TypeHint(leftSide, rightSide));
            }
            return null;
        }

        public Void visitIdentifier(IdentifierTree tree, Map<String, Object> p) {
            Element el;
            MethodInvocationTree mit;
            boolean methodInvocation;
            CancellableTreePathScanner.super.visitIdentifier(tree, p);
            boolean bl = methodInvocation = this.getCurrentPath().getParentPath() != null && this.getCurrentPath().getParentPath().getLeaf().getKind() == Tree.Kind.METHOD_INVOCATION;
            if (methodInvocation && (mit = (MethodInvocationTree)this.getCurrentPath().getParentPath().getLeaf()).getMethodSelect() == tree) {
                ArrayList<TypeMirror> params = new ArrayList<TypeMirror>();
                for (ExpressionTree realParam : mit.getArguments()) {
                    TypeMirror tm = this.info.getTrees().getTypeMirror(new TreePath(this.getCurrentPath().getParentPath(), (Tree)realParam));
                    if (tm != null && tm.getKind() == TypeKind.NONE && realParam.getKind() == Tree.Kind.LAMBDA_EXPRESSION) {
                        tm = this.info.getTypes().getNullType();
                    }
                    params.add(tm);
                }
                this.hints.add(new MethodParamsHint(tree.getName().toString(), params));
            }
            if ((el = this.info.getTrees().getElement(this.getCurrentPath())) != null && (el.getKind().isClass() || el.getKind().isInterface() || el.getKind() == ElementKind.PACKAGE)) {
                TypeMirror type = el.asType();
                String simpleName = null;
                if (type != null) {
                    if (type.getKind() == TypeKind.ERROR) {
                        boolean allowImport = true;
                        if (this.getCurrentPath().getParentPath() != null && this.getCurrentPath().getParentPath().getLeaf().getKind() == Tree.Kind.ASSIGNMENT) {
                            AssignmentTree at = (AssignmentTree)this.getCurrentPath().getParentPath().getLeaf();
                            boolean bl2 = allowImport = at.getVariable() != tree;
                        }
                        if (methodInvocation) {
                            for (Scope s = this.info.getTrees().getScope((TreePath)this.getCurrentPath()); s != null; s = s.getEnclosingScope()) {
                                allowImport &= !this.info.getElementUtilities().getLocalMembersAndVars(s, new ElementUtilities.ElementAcceptor(){

                                    public boolean accept(Element e, TypeMirror type) {
                                        return e.getSimpleName().contentEquals(el.getSimpleName());
                                    }
                                }).iterator().hasNext();
                            }
                        }
                        if (allowImport) {
                            simpleName = el.getSimpleName().toString();
                        }
                    }
                    if (type != null && type.getKind() == TypeKind.PACKAGE) {
                        String s = ((PackageElement)el).getQualifiedName().toString();
                        if (this.info.getElements().getPackageElement(s) == null) {
                            simpleName = el.getSimpleName().toString();
                        }
                    }
                    if (simpleName == null || !SourceVersion.isIdentifier(simpleName) || SourceVersion.isKeyword(simpleName)) {
                        simpleName = null;
                    }
                    if (simpleName != null) {
                        this.unresolved.add(simpleName);
                        Scope currentScope = this.getScope();
                        this.hints.add(new AccessibleHint(simpleName, currentScope));
                        if (p.containsKey("request")) {
                            p.put("result", (Object)Union2.createFirst((Object)simpleName));
                        }
                    } else if (p.containsKey("request") && type.getKind() == TypeKind.DECLARED) {
                        p.put("result", (Object)Union2.createSecond((Object)((DeclaredType)type)));
                    }
                }
            }
            p.remove("request");
            return null;
        }

        public Void visitNewClass(NewClassTree node, Map<String, Object> p) {
            this.filterByNotAcceptedKind((Tree)node.getIdentifier(), ElementKind.ENUM, new ElementKind[0]);
            this.scan((Tree)node.getEnclosingExpression(), new HashMap());
            this.scan((Tree)node.getIdentifier(), p);
            this.scan((Iterable)node.getTypeArguments(), new HashMap());
            this.scan((Iterable)node.getArguments(), new HashMap());
            this.scan((Tree)node.getClassBody(), new HashMap());
            return null;
        }

        public Void visitMethodInvocation(MethodInvocationTree node, Map<String, Object> p) {
            this.scan((Iterable)node.getTypeArguments(), new HashMap());
            this.scan((Tree)node.getMethodSelect(), p);
            this.scan((Iterable)node.getArguments(), new HashMap());
            return null;
        }

        public Void visitNewArray(NewArrayTree node, Map<String, Object> p) {
            this.scan(node.getType(), p);
            this.scan((Iterable)node.getDimensions(), new HashMap());
            this.scan((Iterable)node.getInitializers(), new HashMap());
            return null;
        }

        public Void visitParameterizedType(ParameterizedTypeTree node, Map<String, Object> p) {
            this.scan(node.getType(), p);
            this.scan((Iterable)node.getTypeArguments(), new HashMap());
            return null;
        }

        public Void visitClass(ClassTree node, Map<String, Object> p) {
            if (this.getCurrentPath().getParentPath().getLeaf().getKind() != Tree.Kind.NEW_CLASS) {
                this.filterByAcceptedKind(node.getExtendsClause(), ElementKind.CLASS, new ElementKind[0]);
                for (Tree intf : node.getImplementsClause()) {
                    this.filterByAcceptedKind(intf, ElementKind.INTERFACE, ElementKind.ANNOTATION_TYPE);
                }
            }
            return (Void)CancellableTreePathScanner.super.visitClass(node, p);
        }

        public Void visitAnnotation(AnnotationTree node, Map<String, Object> p) {
            this.filterByAcceptedKind(node.getAnnotationType(), ElementKind.ANNOTATION_TYPE, new ElementKind[0]);
            return (Void)CancellableTreePathScanner.super.visitAnnotation(node, p);
        }

        private Scope getScope() {
            if (this.topLevelScope == null) {
                this.topLevelScope = this.info.getTrees().getScope(new TreePath(this.getCurrentPath().getCompilationUnit()));
            }
            return this.topLevelScope;
        }

        private /* varargs */ void filterByAcceptedKind(Tree toFilter, ElementKind acceptedKind, ElementKind ... otherAcceptedKinds) {
            this.filterByKind(toFilter, EnumSet.of(acceptedKind, otherAcceptedKinds), EnumSet.noneOf(ElementKind.class));
        }

        private /* varargs */ void filterByNotAcceptedKind(Tree toFilter, ElementKind notAcceptedKind, ElementKind ... otherNotAcceptedKinds) {
            this.filterByKind(toFilter, EnumSet.noneOf(ElementKind.class), EnumSet.of(notAcceptedKind, otherNotAcceptedKinds));
        }

        private void filterByKind(Tree toFilter, Set<ElementKind> acceptedKinds, Set<ElementKind> notAcceptedKinds) {
            if (toFilter == null) {
                return;
            }
            switch (toFilter.getKind()) {
                case IDENTIFIER: {
                    this.hints.add(new KindHint(((IdentifierTree)toFilter).getName().toString(), acceptedKinds, notAcceptedKinds));
                    break;
                }
                case PARAMETERIZED_TYPE: {
                    this.filterByKind(((ParameterizedTypeTree)toFilter).getType(), acceptedKinds, notAcceptedKinds);
                }
            }
        }

    }

}

