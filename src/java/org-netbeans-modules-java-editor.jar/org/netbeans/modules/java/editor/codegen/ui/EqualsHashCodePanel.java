/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.source.ElementHandle
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.codegen.ui;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.util.List;
import javax.accessibility.AccessibleContext;
import javax.lang.model.element.Element;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.modules.java.editor.codegen.EqualsHashCodeGenerator;
import org.netbeans.modules.java.editor.codegen.ui.ElementNode;
import org.netbeans.modules.java.editor.codegen.ui.ElementSelectorPanel;
import org.openide.util.NbBundle;

public class EqualsHashCodePanel
extends JPanel {
    private ElementSelectorPanel equalsSelector;
    private ElementSelectorPanel hashCodeSelector;
    private JLabel equalsLabel;
    private JLabel hashCodeLabel;

    public EqualsHashCodePanel(ElementNode.Description description, boolean generateEquals, boolean generateHashCode) {
        GridBagConstraints gridBagConstraints;
        assert (generateEquals || generateHashCode);
        this.initComponents();
        if (generateEquals) {
            this.equalsLabel = new JLabel(NbBundle.getMessage(EqualsHashCodeGenerator.class, (String)"LBL_equals_select"));
            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 0;
            gridBagConstraints.gridy = 0;
            gridBagConstraints.fill = 1;
            gridBagConstraints.anchor = 17;
            gridBagConstraints.weightx = 0.5;
            gridBagConstraints.insets = new Insets(12, 12, 6, 12);
            this.add((Component)this.equalsLabel, gridBagConstraints);
            this.equalsSelector = new ElementSelectorPanel(description, false);
            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 0;
            gridBagConstraints.gridy = 1;
            gridBagConstraints.fill = 1;
            gridBagConstraints.weightx = 0.5;
            gridBagConstraints.weighty = 1.0;
            gridBagConstraints.insets = new Insets(0, 12, 0, 12);
            this.add((Component)this.equalsSelector, gridBagConstraints);
            this.equalsLabel.setLabelFor(this.equalsSelector);
        }
        if (generateHashCode) {
            this.hashCodeLabel = new JLabel(NbBundle.getMessage(EqualsHashCodeGenerator.class, (String)"LBL_hashcode_select"));
            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 1;
            gridBagConstraints.gridy = 0;
            gridBagConstraints.fill = 1;
            gridBagConstraints.anchor = 17;
            gridBagConstraints.weightx = 0.5;
            gridBagConstraints.insets = new Insets(12, generateEquals ? 0 : 12, 6, 12);
            this.add((Component)this.hashCodeLabel, gridBagConstraints);
            this.hashCodeSelector = new ElementSelectorPanel(ElementNode.Description.deepCopy(description), false);
            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 1;
            gridBagConstraints.gridy = 1;
            gridBagConstraints.fill = 1;
            gridBagConstraints.weightx = 0.5;
            gridBagConstraints.weighty = 1.0;
            gridBagConstraints.insets = new Insets(0, generateEquals ? 0 : 12, 0, 12);
            this.add((Component)this.hashCodeSelector, gridBagConstraints);
            this.hashCodeLabel.setLabelFor(this.hashCodeSelector);
        }
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(EqualsHashCodeGenerator.class, (String)"A11Y_Generate_EqualsHashCode"));
    }

    public List<ElementHandle<? extends Element>> getEqualsVariables() {
        if (null == this.equalsSelector) {
            return null;
        }
        return this.equalsSelector.getSelectedElements();
    }

    public List<ElementHandle<? extends Element>> getHashCodeVariables() {
        if (null == this.hashCodeSelector) {
            return null;
        }
        return this.hashCodeSelector.getSelectedElements();
    }

    private void initComponents() {
        this.setLayout(new GridBagLayout());
    }
}

