/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.editor.fold;

public final class ResourceStringFoldInfo {
    private final String resourceName;
    private final String key;

    public ResourceStringFoldInfo(String resourceName, String key) {
        this.resourceName = resourceName;
        this.key = key;
    }

    public String getResourceName() {
        return this.resourceName;
    }

    public String getKey() {
        return this.key;
    }
}

