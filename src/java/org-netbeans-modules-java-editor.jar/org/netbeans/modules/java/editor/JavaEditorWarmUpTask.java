/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseKit
 *  org.netbeans.editor.EditorUI
 *  org.netbeans.editor.Utilities
 *  org.openide.util.Exceptions
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.java.editor;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.editor.java.JavaKit;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;

public class JavaEditorWarmUpTask
implements Runnable {
    private static final int ARTIFICIAL_DOCUMENT_LINE_COUNT = 151;
    private static final int VIEW_HIERARCHY_CREATION_COUNT = 1;
    private static final int IMAGE_WIDTH = 600;
    private static final int IMAGE_HEIGHT = 400;
    private static final int PAINT_COUNT = 1;
    private static final Logger LOG = Logger.getLogger(JavaEditorWarmUpTask.class.getName());
    private static final int STATUS_INIT = 0;
    private static final int STATUS_CREATE_PANE = 1;
    private static final int STATUS_CREATE_DOCUMENTS = 2;
    private static final int STATUS_SWITCH_DOCUMENTS = 3;
    private static final int STATUS_TRAVERSE_VIEWS = 4;
    private static final int STATUS_RENDER_FRAME = 5;
    private static final int STATUS_FINISHED = 6;
    private static final RequestProcessor RP = new RequestProcessor(JavaEditorWarmUpTask.class.getName(), 1, false, false);
    private int status = 0;
    private JEditorPane pane;
    private JFrame frame;
    private Document emptyDoc;
    private Document longDoc;
    private Document lexerDoc;
    private Graphics bGraphics;
    private BaseKit javaKit;
    private long startTime;

    @Override
    public void run() {
        if (GraphicsEnvironment.isHeadless()) {
            return;
        }
        switch (this.status) {
            case 0: {
                this.startTime = System.currentTimeMillis();
                this.javaKit = BaseKit.getKit(JavaKit.class);
                this.javaKit.getActions();
                try {
                    ((Callable)this.javaKit).call();
                }
                catch (Exception e) {
                    Exceptions.printStackTrace((Throwable)e);
                }
                this.emptyDoc = this.javaKit.createDefaultDocument();
                this.lexerDoc = this.javaKit.createDefaultDocument();
                try {
                    this.lexerDoc.insertString(0, "'c'\"s\"/**d*/", null);
                    this.lexerDoc.render(new Runnable(){

                        @Override
                        public void run() {
                            TokenHierarchy th = TokenHierarchy.get((Document)JavaEditorWarmUpTask.this.lexerDoc);
                            TokenSequence ts = th.tokenSequence(JavaTokenId.language());
                            while (ts != null && ts.moveNext()) {
                                ts.embedded();
                            }
                        }
                    });
                }
                catch (BadLocationException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "Kit instances initialized: {0}", System.currentTimeMillis() - this.startTime);
                }
                this.startTime = System.currentTimeMillis();
                if (EditorRegistry.lastFocusedComponent() != null) break;
                this.status = 1;
                SwingUtilities.invokeLater(this);
                break;
            }
            case 1: {
                assert (SwingUtilities.isEventDispatchThread());
                this.pane = new JEditorPane();
                this.pane.setEditorKit((EditorKit)this.javaKit);
                EditorUI editorUI = Utilities.getEditorUI((JTextComponent)this.pane);
                if (editorUI != null) {
                    editorUI.getExtComponent();
                }
                this.status = 2;
                RP.post((Runnable)this);
                break;
            }
            case 2: {
                this.longDoc = this.pane.getDocument();
                try {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 151; i > 0; --i) {
                        sb.append("int ident = 1; // comment\n");
                    }
                    this.longDoc.insertString(0, sb.toString(), null);
                    this.status = 3;
                    SwingUtilities.invokeLater(this);
                }
                catch (BadLocationException e) {
                    Exceptions.printStackTrace((Throwable)e);
                }
                break;
            }
            case 3: {
                for (int i = 0; i < 1; ++i) {
                    this.pane.setDocument(this.emptyDoc);
                    this.pane.setDocument(this.longDoc);
                }
                this.status = 4;
                SwingUtilities.invokeLater(this);
                break;
            }
            case 4: {
                Dimension paneSize = this.pane.getPreferredSize();
                Document doc = this.pane.getDocument();
                int docLen = doc.getLength();
                try {
                    int opCount = 10;
                    for (int i = 0; i < opCount; ++i) {
                        this.pane.modelToView(docLen * i / opCount);
                        this.pane.viewToModel(new Point(paneSize.height * i / opCount, paneSize.width * i / opCount));
                    }
                }
                catch (BadLocationException e) {
                    Exceptions.printStackTrace((Throwable)e);
                }
                this.status = 5;
                SwingUtilities.invokeLater(this);
                break;
            }
            case 5: {
                JEditorPane p = this.pane;
                this.frame = new JFrame();
                EditorUI ui = Utilities.getEditorUI((JTextComponent)p);
                JComponent mainComp = null;
                if (ui != null) {
                    mainComp = ui.getExtComponent();
                }
                if (mainComp == null) {
                    mainComp = new JScrollPane(p);
                }
                this.frame.getContentPane().add(mainComp);
                this.frame.pack();
                this.frame.paint(this.bGraphics);
                this.frame.getContentPane().removeAll();
                this.frame.dispose();
                if (p != null) {
                    p.setEditorKit(null);
                }
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "View hierarchy initialized: {0}", System.currentTimeMillis() - this.startTime);
                }
                this.startTime = System.currentTimeMillis();
                this.status = 6;
                RP.post((Runnable)this);
                break;
            }
            case 6: {
                this.pane = null;
                this.frame = null;
                this.emptyDoc = null;
                this.longDoc = null;
                this.bGraphics = null;
                this.javaKit = null;
                this.startTime = 0;
                this.status = 0;
                break;
            }
            default: {
                throw new IllegalStateException();
            }
        }
    }

    public static class Provider
    implements Runnable {
        private AtomicBoolean b = new AtomicBoolean();

        @Override
        public void run() {
            if (!this.b.compareAndSet(false, true)) {
                return;
            }
            new JavaEditorWarmUpTask().run();
        }
    }

}

