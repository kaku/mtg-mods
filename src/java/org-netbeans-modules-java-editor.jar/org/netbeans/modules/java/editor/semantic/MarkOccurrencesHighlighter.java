/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.BreakTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ContinueTree
 *  com.sun.source.tree.DoWhileLoopTree
 *  com.sun.source.tree.EnhancedForLoopTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.ForLoopTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.LabeledStatementTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.NewClassTree
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.TryTree
 *  com.sun.source.tree.WhileLoopTree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreePathScanner
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.lexer.JavadocTokenId
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.JavaParserResultTask
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.support.CaretAwareJavaSourceTaskFactory
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.NbEditorUtilities
 *  org.netbeans.modules.editor.errorstripe.privatespi.Mark
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.CursorMovedSchedulerEvent
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.netbeans.modules.parsing.spi.TaskIndexingMode
 *  org.netbeans.spi.editor.highlighting.support.OffsetsBag
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.semantic;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.BreakTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ContinueTree;
import com.sun.source.tree.DoWhileLoopTree;
import com.sun.source.tree.EnhancedForLoopTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.ForLoopTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.LabeledStatementTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TryTree;
import com.sun.source.tree.WhileLoopTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreePathScanner;
import com.sun.source.util.Trees;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.lexer.JavadocTokenId;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.JavaParserResultTask;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.support.CaretAwareJavaSourceTaskFactory;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.modules.editor.errorstripe.privatespi.Mark;
import org.netbeans.modules.java.editor.javadoc.JavadocImports;
import org.netbeans.modules.java.editor.options.MarkOccurencesSettings;
import org.netbeans.modules.java.editor.semantic.ColoringAttributes;
import org.netbeans.modules.java.editor.semantic.ColoringManager;
import org.netbeans.modules.java.editor.semantic.FindLocalUsagesQuery;
import org.netbeans.modules.java.editor.semantic.MethodExitDetector;
import org.netbeans.modules.java.editor.semantic.OccurrencesMarkProvider;
import org.netbeans.modules.java.editor.semantic.Utilities;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.CursorMovedSchedulerEvent;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.TaskIndexingMode;
import org.netbeans.spi.editor.highlighting.support.OffsetsBag;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.util.NbBundle;

public class MarkOccurrencesHighlighter
extends JavaParserResultTask {
    private FileObject file;
    public static final Color ES_COLOR = new Color(175, 172, 102);
    private static final Set<Tree.Kind> TYPE_PATH_ELEMENT = EnumSet.of(Tree.Kind.IDENTIFIER, Tree.Kind.PRIMITIVE_TYPE, Tree.Kind.PARAMETERIZED_TYPE, Tree.Kind.MEMBER_SELECT, Tree.Kind.ARRAY_TYPE);
    private boolean canceled;
    private MethodExitDetector exitDetector;
    private FindLocalUsagesQuery localUsages;
    static ColoringAttributes.Coloring MO = ColoringAttributes.add(ColoringAttributes.empty(), ColoringAttributes.MARK_OCCURRENCES);

    MarkOccurrencesHighlighter(FileObject file) {
        super(JavaSource.Phase.RESOLVED, TaskIndexingMode.ALLOWED_DURING_SCAN);
        this.file = file;
    }

    public void run(Parser.Result parseResult, SchedulerEvent event) {
        this.resume();
        CompilationInfo info = CompilationInfo.get((Parser.Result)parseResult);
        if (info == null) {
            return;
        }
        Document doc = parseResult.getSnapshot().getSource().getDocument(false);
        if (doc == null) {
            Logger.getLogger(MarkOccurrencesHighlighter.class.getName()).log(Level.FINE, "SemanticHighlighter: Cannot get document!");
            return;
        }
        int caretPosition = event instanceof CursorMovedSchedulerEvent ? ((CursorMovedSchedulerEvent)event).getCaretOffset() : CaretAwareJavaSourceTaskFactory.getLastPosition((FileObject)this.file);
        Object prop = doc.getProperty("marked-occurrence");
        if (prop != null && ((long[])prop)[0] == DocumentUtilities.getDocumentVersion((Document)doc) && ((long[])prop)[1] == (long)caretPosition) {
            return;
        }
        Preferences node = MarkOccurencesSettings.getCurrentNode();
        if (!node.getBoolean(MarkOccurencesSettings.ON_OFF, true)) {
            MarkOccurrencesHighlighter.getHighlightsBag(doc).clear();
            OccurrencesMarkProvider.get(doc).setOccurrences(Collections.emptySet());
            return;
        }
        long start = System.currentTimeMillis();
        if (this.isCancelled()) {
            return;
        }
        caretPosition = info.getSnapshot().getEmbeddedOffset(caretPosition);
        List<int[]> bag = this.processImpl(info, node, doc, caretPosition);
        if (this.isCancelled()) {
            return;
        }
        Logger.getLogger("TIMER").log(Level.FINE, "Occurrences", new Object[]{NbEditorUtilities.getFileObject((Document)doc), System.currentTimeMillis() - start});
        if (bag == null) {
            if (node.getBoolean(MarkOccurencesSettings.KEEP_MARKS, true)) {
                return;
            }
            bag = new ArrayList<int[]>();
        }
        Collections.sort(bag, new Comparator<int[]>(){

            @Override
            public int compare(int[] o1, int[] o2) {
                return o1[0] - o2[0];
            }
        });
        Iterator<int[]> it = bag.iterator();
        int[] last = it.hasNext() ? it.next() : null;
        ArrayList<int[]> result = new ArrayList<int[]>(bag.size());
        while (it.hasNext()) {
            int[] current = it.next();
            if (current[0] < last[1]) {
                last[1] = Math.max(current[1], last[1]);
                continue;
            }
            result.add(last);
            last = current;
        }
        if (last != null) {
            result.add(last);
        }
        OffsetsBag obag = new OffsetsBag(doc);
        obag.clear();
        AttributeSet attributes = ColoringManager.getColoringImpl(MO);
        for (int[] span : result) {
            int convertedStart = info.getSnapshot().getOriginalOffset(span[0]);
            int convertedEnd = info.getSnapshot().getOriginalOffset(span[1]);
            if (convertedStart == -1 || convertedEnd == -1) continue;
            obag.addHighlight(convertedStart, convertedEnd, attributes);
        }
        if (this.isCancelled()) {
            return;
        }
        MarkOccurrencesHighlighter.getHighlightsBag(doc).setHighlights(obag);
        OccurrencesMarkProvider.get(doc).setOccurrences(OccurrencesMarkProvider.createMarks(doc, bag, ES_COLOR, NbBundle.getMessage(MarkOccurrencesHighlighter.class, (String)"LBL_ES_TOOLTIP")));
    }

    private boolean isIn(CompilationUnitTree cu, SourcePositions sp, Tree tree, int position) {
        return sp.getStartPosition(cu, tree) <= (long)position && (long)position <= sp.getEndPosition(cu, tree);
    }

    private boolean isIn(int caretPosition, Token span) {
        if (span == null) {
            return false;
        }
        return span.offset(null) <= caretPosition && caretPosition <= span.offset(null) + span.length();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    List<int[]> processImpl(CompilationInfo info, Preferences node, Document doc, int caretPosition) {
        boolean insideJavadoc;
        ImportTree it;
        Element el;
        MemberSelectTree mst;
        TokenSequence cts = info.getTokenHierarchy().tokenSequence(JavaTokenId.language());
        if (cts != null) {
            cts.move(caretPosition);
            if (cts.moveNext() && cts.token().id() == JavaTokenId.IDENTIFIER && cts.offset() == caretPosition) {
                ++caretPosition;
            }
        }
        CompilationUnitTree cu = info.getCompilationUnit();
        TreePath tp = info.getTreeUtilities().pathFor(caretPosition);
        if (tp.getParentPath() != null && tp.getParentPath().getLeaf().getKind() == Tree.Kind.ANNOTATED_TYPE) {
            tp = tp.getParentPath();
        }
        TreePath typePath = MarkOccurrencesHighlighter.findTypePath(tp);
        if (this.isCancelled()) {
            return null;
        }
        if (typePath != null && typePath.getParentPath().getLeaf().getKind() == Tree.Kind.METHOD) {
            MethodTree decl = (MethodTree)typePath.getParentPath().getLeaf();
            Tree type = decl.getReturnType();
            if (node.getBoolean(MarkOccurencesSettings.EXIT, true) && this.isIn(cu, info.getTrees().getSourcePositions(), type, caretPosition)) {
                MethodExitDetector med = new MethodExitDetector();
                this.setExitDetector(med);
                try {
                    List<int[]> list = med.process(info, doc, (Tree)decl, null);
                    return list;
                }
                finally {
                    this.setExitDetector(null);
                }
            }
            for (Tree exc : decl.getThrows()) {
                if (!node.getBoolean(MarkOccurencesSettings.EXCEPTIONS, true) || !this.isIn(cu, info.getTrees().getSourcePositions(), exc, caretPosition)) continue;
                MethodExitDetector med = new MethodExitDetector();
                this.setExitDetector(med);
                try {
                    List<int[]> list = med.process(info, doc, (Tree)decl, Collections.singletonList(exc));
                    return list;
                }
                finally {
                    this.setExitDetector(null);
                }
            }
        }
        if (this.isCancelled()) {
            return null;
        }
        if (node.getBoolean(MarkOccurencesSettings.EXCEPTIONS, true) && typePath != null && (typePath.getParentPath().getLeaf().getKind() == Tree.Kind.UNION_TYPE && typePath.getParentPath().getParentPath().getLeaf().getKind() == Tree.Kind.VARIABLE && typePath.getParentPath().getParentPath().getParentPath().getLeaf().getKind() == Tree.Kind.CATCH || typePath.getParentPath().getLeaf().getKind() == Tree.Kind.VARIABLE && typePath.getParentPath().getParentPath().getLeaf().getKind() == Tree.Kind.CATCH)) {
            MethodExitDetector med = new MethodExitDetector();
            this.setExitDetector(med);
            try {
                TreePath tryPath = org.netbeans.modules.editor.java.Utilities.getPathElementOfKind(Tree.Kind.TRY, typePath);
                List<int[]> i$ = med.process(info, doc, (Tree)((TryTree)tryPath.getLeaf()).getBlock(), Collections.singletonList(typePath.getLeaf()));
                return i$;
            }
            finally {
                this.setExitDetector(null);
            }
        }
        if (this.isCancelled()) {
            return null;
        }
        if (node.getBoolean(MarkOccurencesSettings.IMPLEMENTS, true) || node.getBoolean(MarkOccurencesSettings.OVERRIDES, true)) {
            int bodyStart;
            if (typePath != null && TreeUtilities.CLASS_TREE_KINDS.contains((Object)typePath.getParentPath().getLeaf().getKind())) {
                ClassTree ctree = (ClassTree)typePath.getParentPath().getLeaf();
                int bodyStart2 = Utilities.findBodyStart((Tree)ctree, cu, info.getTrees().getSourcePositions(), doc);
                boolean isExtends = ctree.getExtendsClause() == typePath.getLeaf();
                boolean isImplements = false;
                for (Tree t : ctree.getImplementsClause()) {
                    if (t != typePath.getLeaf()) continue;
                    isImplements = true;
                    break;
                }
                if (isExtends && node.getBoolean(MarkOccurencesSettings.OVERRIDES, true) || isImplements && node.getBoolean(MarkOccurencesSettings.IMPLEMENTS, true)) {
                    Element superType = info.getTrees().getElement(typePath);
                    Element thisType = info.getTrees().getElement(typePath.getParentPath());
                    if (MarkOccurrencesHighlighter.isClass(superType) && MarkOccurrencesHighlighter.isClass(thisType)) {
                        return this.detectMethodsForClass(info, doc, typePath.getParentPath(), (TypeElement)superType, (TypeElement)thisType);
                    }
                }
            }
            if (this.isCancelled()) {
                return null;
            }
            TokenSequence ts = info.getTokenHierarchy().tokenSequence(JavaTokenId.language());
            if (ts != null && TreeUtilities.CLASS_TREE_KINDS.contains((Object)tp.getLeaf().getKind()) && caretPosition < (bodyStart = Utilities.findBodyStart(tp.getLeaf(), cu, info.getTrees().getSourcePositions(), doc))) {
                ts.move(caretPosition);
                if (ts.moveNext()) {
                    List superClasses;
                    Element thisType;
                    Tree superClass;
                    if (node.getBoolean(MarkOccurencesSettings.OVERRIDES, true) && ts.token().id() == JavaTokenId.EXTENDS && (superClass = ((ClassTree)tp.getLeaf()).getExtendsClause()) != null) {
                        Element superType = info.getTrees().getElement(new TreePath(tp, superClass));
                        thisType = info.getTrees().getElement(tp);
                        if (MarkOccurrencesHighlighter.isClass(superType) && MarkOccurrencesHighlighter.isClass(thisType)) {
                            return this.detectMethodsForClass(info, doc, tp, (TypeElement)superType, (TypeElement)thisType);
                        }
                    }
                    if (node.getBoolean(MarkOccurencesSettings.IMPLEMENTS, true) && ts.token().id() == JavaTokenId.IMPLEMENTS && (superClasses = ((ClassTree)tp.getLeaf()).getImplementsClause()) != null) {
                        ArrayList<TypeElement> superTypes = new ArrayList<TypeElement>();
                        for (Tree superTypeTree : superClasses) {
                            Element superType;
                            if (superTypeTree == null || !MarkOccurrencesHighlighter.isClass(superType = info.getTrees().getElement(new TreePath(tp, superTypeTree)))) continue;
                            superTypes.add((TypeElement)superType);
                        }
                        thisType = info.getTrees().getElement(tp);
                        if (!superTypes.isEmpty() && MarkOccurrencesHighlighter.isClass(thisType)) {
                            return this.detectMethodsForClass(info, doc, tp, superTypes, (TypeElement)thisType);
                        }
                    }
                }
            }
        }
        if (this.isCancelled()) {
            return null;
        }
        Tree tree = tp.getLeaf();
        if (node.getBoolean(MarkOccurencesSettings.BREAK_CONTINUE, true)) {
            int[] span;
            if (tree.getKind() == Tree.Kind.BREAK || tree.getKind() == Tree.Kind.CONTINUE) {
                return this.detectBreakOrContinueTarget(info, doc, tp, caretPosition);
            }
            if (tree.getKind() == Tree.Kind.LABELED_STATEMENT && (span = Utilities.findIdentifierSpan(tp, info, doc))[0] <= caretPosition && caretPosition <= span[1]) {
                List<int[]> ret = this.detectLabel(info, doc, tp);
                ret.add(span);
                return ret;
            }
        }
        boolean bl = insideJavadoc = (el = JavadocImports.findReferencedElement(info, caretPosition)) != null;
        if (this.isCancelled()) {
            return null;
        }
        if (!insideJavadoc) {
            TreePath c;
            el = tp.getParentPath() != null && tp.getParentPath().getLeaf().getKind() == Tree.Kind.NEW_CLASS ? (this.isIn(caretPosition, Utilities.findIdentifierSpan(info, doc, c = new TreePath(tp.getParentPath(), (Tree)((NewClassTree)tp.getParentPath().getLeaf()).getIdentifier()))) ? info.getTrees().getElement(tp.getParentPath()) : info.getTrees().getElement(tp)) : info.getTrees().getElement(tp);
        }
        if (el != null && (!TreeUtilities.CLASS_TREE_KINDS.contains((Object)tree.getKind()) || this.isIn(caretPosition, Utilities.findIdentifierSpan(info, doc, tp))) && !Utilities.isNonCtorKeyword(tree) && (tree.getKind() != Tree.Kind.METHOD || this.isIn(caretPosition, Utilities.findIdentifierSpan(info, doc, tp))) && MarkOccurrencesHighlighter.isEnabled(node, el) || insideJavadoc && MarkOccurrencesHighlighter.isEnabled(node, el)) {
            FindLocalUsagesQuery fluq = new FindLocalUsagesQuery();
            this.setLocalUsages(fluq);
            try {
                ArrayList<int[]> bag = new ArrayList<int[]>();
                for (Token t : fluq.findUsages(el, info, doc)) {
                    int delta = t.id() == JavadocTokenId.IDENT && t.text().charAt(0) == '<' && t.text().charAt(t.length() - 1) == '>' ? 1 : 0;
                    bag.add(new int[]{t.offset(null) + delta, t.offset(null) + t.length() - delta});
                }
                ArrayList<int[]> i$ = bag;
                return i$;
            }
            finally {
                this.setLocalUsages(null);
            }
        }
        if (tp.getParentPath() != null && tp.getParentPath().getLeaf().getKind() == Tree.Kind.IMPORT && (it = (ImportTree)tp.getParentPath().getLeaf()).isStatic() && tp.getLeaf().getKind() == Tree.Kind.MEMBER_SELECT && !"*".contentEquals((mst = (MemberSelectTree)tp.getLeaf()).getIdentifier())) {
            ArrayList<int[]> bag = new ArrayList<int[]>();
            Token<JavaTokenId> tok = Utilities.getToken(info, doc, tp);
            if (tok != null) {
                bag.add(new int[]{tok.offset(null), tok.offset(null) + tok.length()});
            }
            if ((el = info.getTrees().getElement(new TreePath(tp, (Tree)mst.getExpression()))) != null) {
                FindLocalUsagesQuery fluq = new FindLocalUsagesQuery();
                this.setLocalUsages(fluq);
                try {
                    for (Element element : el.getEnclosedElements()) {
                        if (!element.getModifiers().contains((Object)Modifier.STATIC)) continue;
                        for (Token t : fluq.findUsages(element, info, doc)) {
                            bag.add(new int[]{t.offset(null), t.offset(null) + t.length()});
                        }
                    }
                    ArrayList<int[]> i$ = bag;
                    return i$;
                }
                finally {
                    this.setLocalUsages(null);
                }
            }
        }
        return null;
    }

    private static TreePath findTypePath(TreePath tp) {
        if (!TYPE_PATH_ELEMENT.contains((Object)tp.getLeaf().getKind())) {
            return null;
        }
        while (TYPE_PATH_ELEMENT.contains((Object)tp.getParentPath().getLeaf().getKind())) {
            tp = tp.getParentPath();
        }
        return tp;
    }

    private static boolean isClass(Element el) {
        return el != null && (el.getKind().isClass() || el.getKind().isInterface());
    }

    private static boolean isEnabled(Preferences node, Element el) {
        switch (el.getKind()) {
            case ANNOTATION_TYPE: 
            case CLASS: 
            case ENUM: 
            case INTERFACE: 
            case TYPE_PARAMETER: {
                return node.getBoolean(MarkOccurencesSettings.TYPES, true);
            }
            case CONSTRUCTOR: 
            case METHOD: {
                return node.getBoolean(MarkOccurencesSettings.METHODS, true);
            }
            case ENUM_CONSTANT: {
                return node.getBoolean(MarkOccurencesSettings.CONSTANTS, true);
            }
            case FIELD: {
                if (el.getModifiers().containsAll(EnumSet.of(Modifier.STATIC, Modifier.FINAL))) {
                    return node.getBoolean(MarkOccurencesSettings.CONSTANTS, true);
                }
                return node.getBoolean(MarkOccurencesSettings.FIELDS, true);
            }
            case LOCAL_VARIABLE: 
            case RESOURCE_VARIABLE: 
            case PARAMETER: 
            case EXCEPTION_PARAMETER: {
                return node.getBoolean(MarkOccurencesSettings.LOCAL_VARIABLES, true);
            }
            case PACKAGE: {
                return false;
            }
        }
        Logger.getLogger(MarkOccurrencesHighlighter.class.getName()).log(Level.INFO, "Unknow element type: {0}.", (Object)el.getKind());
        return true;
    }

    private final synchronized void setExitDetector(MethodExitDetector detector) {
        this.exitDetector = detector;
    }

    private final synchronized void setLocalUsages(FindLocalUsagesQuery localUsages) {
        this.localUsages = localUsages;
    }

    public final synchronized void cancel() {
        this.canceled = true;
        if (this.exitDetector != null) {
            this.exitDetector.cancel();
        }
        if (this.localUsages != null) {
            this.localUsages.cancel();
        }
    }

    protected final synchronized boolean isCancelled() {
        return this.canceled;
    }

    protected final synchronized void resume() {
        this.canceled = false;
    }

    private List<int[]> detectMethodsForClass(CompilationInfo info, Document document, TreePath clazz, TypeElement superType, TypeElement thisType) {
        return this.detectMethodsForClass(info, document, clazz, Collections.singletonList(superType), thisType);
    }

    private List<int[]> detectMethodsForClass(CompilationInfo info, Document document, TreePath clazz, List<TypeElement> superTypes, TypeElement thisType) {
        ArrayList<int[]> highlights = new ArrayList<int[]>();
        ClassTree clazzTree = (ClassTree)clazz.getLeaf();
        TypeElement jlObject = info.getElements().getTypeElement("java.lang.Object");
        block0 : for (Tree member : clazzTree.getMembers()) {
            if (this.isCancelled()) {
                return null;
            }
            if (member.getKind() != Tree.Kind.METHOD) continue;
            TreePath path = new TreePath(clazz, member);
            Element el = info.getTrees().getElement(path);
            if (el.getKind() != ElementKind.METHOD) continue;
            for (TypeElement superType : superTypes) {
                for (ExecutableElement ee : ElementFilter.methodsIn(info.getElements().getAllMembers(superType))) {
                    if (!info.getElements().overrides((ExecutableElement)el, ee, thisType) || !superType.getKind().isClass() && ee.getEnclosingElement().equals(jlObject)) continue;
                    Token<JavaTokenId> t = Utilities.getToken(info, document, path);
                    if (t == null) continue block0;
                    highlights.add(new int[]{t.offset(null), t.offset(null) + t.length()});
                    continue block0;
                }
            }
        }
        return highlights;
    }

    private List<int[]> detectBreakOrContinueTarget(CompilationInfo info, Document document, TreePath breakOrContinue, int caretPosition) {
        ArrayList<int[]> result = new ArrayList<int[]>();
        StatementTree target = info.getTreeUtilities().getBreakContinueTarget(breakOrContinue);
        if (target == null) {
            return null;
        }
        TokenSequence ts = info.getTokenHierarchy().tokenSequence(JavaTokenId.language());
        ts.move((int)info.getTrees().getSourcePositions().getStartPosition(info.getCompilationUnit(), (Tree)target));
        if (ts.moveNext()) {
            result.add(new int[]{ts.offset(), ts.offset() + ts.token().length()});
        }
        StatementTree statement = target.getKind() == Tree.Kind.LABELED_STATEMENT ? ((LabeledStatementTree)target).getStatement() : target;
        StatementTree block = null;
        switch (statement.getKind()) {
            case SWITCH: {
                block = statement;
                break;
            }
            case WHILE_LOOP: {
                if (((WhileLoopTree)statement).getStatement().getKind() != Tree.Kind.BLOCK) break;
                block = ((WhileLoopTree)statement).getStatement();
                break;
            }
            case FOR_LOOP: {
                if (((ForLoopTree)statement).getStatement().getKind() != Tree.Kind.BLOCK) break;
                block = ((ForLoopTree)statement).getStatement();
                break;
            }
            case ENHANCED_FOR_LOOP: {
                if (((EnhancedForLoopTree)statement).getStatement().getKind() != Tree.Kind.BLOCK) break;
                block = ((EnhancedForLoopTree)statement).getStatement();
                break;
            }
            case DO_WHILE_LOOP: {
                if (((DoWhileLoopTree)statement).getStatement().getKind() != Tree.Kind.BLOCK) break;
                block = ((DoWhileLoopTree)statement).getStatement();
            }
        }
        if (block != null) {
            ts.move((int)info.getTrees().getSourcePositions().getEndPosition(info.getCompilationUnit(), (Tree)block));
            if (ts.movePrevious() && ts.token().id() == JavaTokenId.RBRACE) {
                result.add(new int[]{ts.offset(), ts.offset() + ts.token().length()});
            }
        }
        if (target.getKind() == Tree.Kind.LABELED_STATEMENT && this.isIn(caretPosition, Utilities.findIdentifierSpan(info, document, breakOrContinue))) {
            result.addAll(this.detectLabel(info, document, info.getTrees().getPath(info.getCompilationUnit(), (Tree)target)));
        }
        return result;
    }

    private List<int[]> detectLabel(final CompilationInfo info, final Document document, TreePath labeledStatement) {
        final ArrayList<int[]> result = new ArrayList<int[]>();
        if (labeledStatement.getLeaf().getKind() == Tree.Kind.LABELED_STATEMENT) {
            final Name label = ((LabeledStatementTree)labeledStatement.getLeaf()).getLabel();
            new TreePathScanner<Void, Void>(){

                public Void visitBreak(BreakTree node, Void p) {
                    if (node.getLabel() != null && label.contentEquals(node.getLabel())) {
                        result.add(Utilities.findIdentifierSpan(this.getCurrentPath(), info, document));
                    }
                    return (Void)TreePathScanner.super.visitBreak(node, (Object)p);
                }

                public Void visitContinue(ContinueTree node, Void p) {
                    if (node.getLabel() != null && label.contentEquals(node.getLabel())) {
                        result.add(Utilities.findIdentifierSpan(this.getCurrentPath(), info, document));
                    }
                    return (Void)TreePathScanner.super.visitContinue(node, (Object)p);
                }
            }.scan(labeledStatement, (Object)null);
        }
        return result;
    }

    static OffsetsBag getHighlightsBag(Document doc) {
        OffsetsBag bag = (OffsetsBag)doc.getProperty(MarkOccurrencesHighlighter.class);
        if (bag == null) {
            bag = new OffsetsBag(doc, false);
            doc.putProperty(MarkOccurrencesHighlighter.class, (Object)bag);
            Object stream = doc.getProperty("stream");
            final OffsetsBag bagFin = bag;
            DocumentListener l = new DocumentListener(){

                @Override
                public void insertUpdate(DocumentEvent e) {
                    bagFin.removeHighlights(e.getOffset(), e.getOffset(), false);
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    bagFin.removeHighlights(e.getOffset(), e.getOffset(), false);
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                }
            };
            doc.addDocumentListener(l);
            if (stream instanceof DataObject) {
                Logger.getLogger("TIMER").log(Level.FINE, "MarkOccurrences Highlights Bag", new Object[]{((DataObject)stream).getPrimaryFile(), bag});
                Logger.getLogger("TIMER").log(Level.FINE, "MarkOccurrences Highlights Bag Listener", new Object[]{((DataObject)stream).getPrimaryFile(), l});
            }
        }
        return bag;
    }

    public int getPriority() {
        return 100;
    }

    public Class<? extends Scheduler> getSchedulerClass() {
        return Scheduler.CURSOR_SENSITIVE_TASK_SCHEDULER;
    }

}

