/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.AnnotationTree
 *  com.sun.source.tree.AssignmentTree
 *  com.sun.source.tree.BinaryTree
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.ExpressionStatementTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.IfTree
 *  com.sun.source.tree.LiteralTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodInvocationTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.PrimitiveTypeTree
 *  com.sun.source.tree.ReturnTree
 *  com.sun.source.tree.Scope
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.TypeCastTree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreePathScanner
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.GeneratorUtilities
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.ModificationResult
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.java.source.TreeMaker
 *  org.netbeans.api.java.source.TreePathHandle
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.WorkingCopy
 *  org.netbeans.editor.Utilities
 *  org.netbeans.spi.editor.codegen.CodeGenerator
 *  org.netbeans.spi.editor.codegen.CodeGenerator$Factory
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.MapFormat
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.codegen;

import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BinaryTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.ExpressionStatementTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.PrimitiveTypeTree;
import com.sun.source.tree.ReturnTree;
import com.sun.source.tree.Scope;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TypeCastTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreePathScanner;
import com.sun.source.util.Trees;
import java.awt.Dialog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.WildcardType;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.GeneratorUtilities;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.TreeMaker;
import org.netbeans.api.java.source.TreePathHandle;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.java.editor.codegen.ConstructorGenerator;
import org.netbeans.modules.java.editor.codegen.GeneratorUtils;
import org.netbeans.modules.java.editor.codegen.ui.ElementNode;
import org.netbeans.modules.java.editor.codegen.ui.EqualsHashCodePanel;
import org.netbeans.spi.editor.codegen.CodeGenerator;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.MapFormat;
import org.openide.util.NbBundle;

public class EqualsHashCodeGenerator
implements CodeGenerator {
    private static final String ERROR = "<error>";
    private JTextComponent component;
    final ElementNode.Description description;
    final boolean generateEquals;
    final boolean generateHashCode;
    static int randomNumber = -1;
    private static final Map<Acceptor, String> EQUALS_PATTERNS = new LinkedHashMap<Acceptor, String>();
    private static final Map<Acceptor, String> HASH_CODE_PATTERNS;

    private EqualsHashCodeGenerator(JTextComponent component, ElementNode.Description description, boolean generateEquals, boolean generateHashCode) {
        this.component = component;
        this.description = description;
        this.generateEquals = generateEquals;
        this.generateHashCode = generateHashCode;
    }

    public String getDisplayName() {
        if (this.generateEquals && this.generateHashCode) {
            return NbBundle.getMessage(EqualsHashCodeGenerator.class, (String)"LBL_equals_and_hashcode");
        }
        if (!this.generateEquals) {
            return NbBundle.getMessage(EqualsHashCodeGenerator.class, (String)"LBL_hashcode");
        }
        return NbBundle.getMessage(EqualsHashCodeGenerator.class, (String)"LBL_equals");
    }

    static EqualsHashCodeGenerator createEqualsHashCodeGenerator(JTextComponent component, CompilationController cc, Element el) throws IOException {
        if (el.getKind() != ElementKind.CLASS) {
            return null;
        }
        if (el.getSimpleName() == null || el.getSimpleName().length() == 0) {
            return null;
        }
        TypeElement typeElement = (TypeElement)el;
        ExecutableElement[] equalsHashCode = EqualsHashCodeGenerator.overridesHashCodeAndEquals((CompilationInfo)cc, typeElement, null);
        ArrayList<ElementNode.Description> descriptions = new ArrayList<ElementNode.Description>();
        for (VariableElement variableElement : ElementFilter.fieldsIn(typeElement.getEnclosedElements())) {
            if ("<error>".contentEquals(variableElement.getSimpleName()) || variableElement.getModifiers().contains((Object)Modifier.STATIC)) continue;
            descriptions.add(ElementNode.Description.create((CompilationInfo)cc, variableElement, null, true, EqualsHashCodeGenerator.isUsed((CompilationInfo)cc, variableElement, equalsHashCode)));
        }
        if (descriptions.isEmpty() || equalsHashCode[0] != null && equalsHashCode[1] != null) {
            return null;
        }
        return new EqualsHashCodeGenerator(component, ElementNode.Description.create((CompilationInfo)cc, typeElement, descriptions, false, false), equalsHashCode[0] == null, equalsHashCode[1] == null);
    }

    private static /* varargs */ boolean isUsed(CompilationInfo cc, VariableElement field, ExecutableElement ... methods) {
        for (ExecutableElement e : methods) {
            class Used
            extends TreePathScanner<Void, VariableElement> {
                boolean found;

                Used() {
                }

                public Void visitIdentifier(IdentifierTree id, VariableElement what) {
                    if (id.getName().equals(what.getSimpleName())) {
                        this.found = true;
                    }
                    return (Void)TreePathScanner.super.visitIdentifier(id, (Object)what);
                }

                public Void visitMemberSelect(MemberSelectTree sel, VariableElement what) {
                    if (sel.getIdentifier().equals(what.getSimpleName())) {
                        this.found = true;
                    }
                    return (Void)TreePathScanner.super.visitMemberSelect(sel, (Object)what);
                }
            }
            if (e == null) continue;
            Trees tree = cc.getTrees();
            TreePath path = tree.getPath((Element)e);
            Used used = new Used();
            used.scan(path, (Object)field);
            if (!used.found) continue;
            return true;
        }
        return false;
    }

    public static ExecutableElement[] overridesHashCodeAndEquals(CompilationInfo compilationInfo, Element type, Cancel stop) {
        ExecutableElement[] ret = new ExecutableElement[2];
        TypeElement el = compilationInfo.getElements().getTypeElement("java.lang.Object");
        if (el == null) {
            return ret;
        }
        if (type == null || type.getKind() != ElementKind.CLASS) {
            return ret;
        }
        TypeMirror objAsType = el.asType();
        if (objAsType == null || objAsType.getKind() != TypeKind.DECLARED) {
            return ret;
        }
        ExecutableElement hashCode = null;
        ExecutableElement equals = null;
        for (ExecutableElement method : ElementFilter.methodsIn(el.getEnclosedElements())) {
            if (stop != null && stop.isCanceled()) {
                return ret;
            }
            if (method.getSimpleName().contentEquals("equals") && method.getParameters().size() == 1 && !method.getModifiers().contains((Object)Modifier.STATIC) && compilationInfo.getTypes().isSameType(objAsType, method.getParameters().get(0).asType())) {
                assert (equals == null);
                equals = method;
            }
            if (!method.getSimpleName().contentEquals("hashCode") || !method.getParameters().isEmpty() || method.getModifiers().contains((Object)Modifier.STATIC)) continue;
            assert (hashCode == null);
            hashCode = method;
        }
        if (hashCode == null || equals == null) {
            return ret;
        }
        TypeElement clazz = (TypeElement)type;
        for (Element ee : type.getEnclosedElements()) {
            if (stop != null && stop.isCanceled()) {
                return ret;
            }
            if (ee.getKind() != ElementKind.METHOD) continue;
            ExecutableElement method2 = (ExecutableElement)ee;
            if (compilationInfo.getElements().overrides(method2, hashCode, clazz)) {
                ret[1] = method2;
            }
            if (!compilationInfo.getElements().overrides(method2, equals, clazz)) continue;
            ret[0] = method2;
        }
        return ret;
    }

    public static void invokeEqualsHashCode(TreePathHandle handle, JTextComponent component) {
        JavaSource js = JavaSource.forDocument((Document)component.getDocument());
        if (js != null) {
            class FillIn
            implements Task<CompilationController> {
                EqualsHashCodeGenerator gen;
                final /* synthetic */ JTextComponent val$component;

                FillIn() {
                    this.val$component = var2_2;
                }

                public void run(CompilationController cc) throws Exception {
                    cc.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                    Element e = val$handle.resolveElement((CompilationInfo)cc);
                    this.gen = EqualsHashCodeGenerator.createEqualsHashCodeGenerator(this.val$component, cc, e);
                }

                public void invoke() {
                    if (this.gen != null) {
                        this.gen.invoke();
                    }
                }
            }
            FillIn fillIn = new FillIn(handle, component);
            try {
                js.runUserActionTask((Task)fillIn, true);
                fillIn.invoke();
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
    }

    public void invoke() {
        JavaSource js;
        final int caretOffset = this.component.getCaretPosition();
        final EqualsHashCodePanel panel = new EqualsHashCodePanel(this.description, this.generateEquals, this.generateHashCode);
        String title = NbBundle.getMessage(ConstructorGenerator.class, (String)"LBL_generate_equals_and_hashcode");
        if (!this.generateEquals) {
            title = NbBundle.getMessage(ConstructorGenerator.class, (String)"LBL_generate_hashcode");
        } else if (!this.generateHashCode) {
            title = NbBundle.getMessage(ConstructorGenerator.class, (String)"LBL_generate_equals");
        }
        DialogDescriptor dialogDescriptor = GeneratorUtils.createDialogDescriptor(panel, title);
        Dialog dialog = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
        dialog.setVisible(true);
        if (dialogDescriptor.getValue() == dialogDescriptor.getDefaultValue() && (js = JavaSource.forDocument((Document)this.component.getDocument())) != null) {
            try {
                ModificationResult mr = js.runModificationTask((Task)new Task<WorkingCopy>(){

                    public void run(WorkingCopy copy) throws IOException {
                        copy.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                        Element e = EqualsHashCodeGenerator.this.description.getElementHandle().resolve((CompilationInfo)copy);
                        TreePath path = e != null ? copy.getTrees().getPath(e) : copy.getTreeUtilities().pathFor(caretOffset);
                        path = org.netbeans.modules.editor.java.Utilities.getPathElementOfKind(TreeUtilities.CLASS_TREE_KINDS, path);
                        if (path == null) {
                            String message = NbBundle.getMessage(EqualsHashCodeGenerator.class, (String)"ERR_CannotFindOriginalClass");
                            Utilities.setStatusBoldText((JTextComponent)EqualsHashCodeGenerator.this.component, (String)message);
                        } else {
                            ArrayList<VariableElement> equalsElements = new ArrayList<VariableElement>();
                            if (EqualsHashCodeGenerator.this.generateEquals) {
                                for (ElementHandle<? extends Element> elementHandle : panel.getEqualsVariables()) {
                                    equalsElements.add((VariableElement)elementHandle.resolve((CompilationInfo)copy));
                                }
                            }
                            ArrayList<VariableElement> hashCodeElements = new ArrayList<VariableElement>();
                            if (EqualsHashCodeGenerator.this.generateHashCode) {
                                for (ElementHandle<? extends Element> elementHandle : panel.getHashCodeVariables()) {
                                    hashCodeElements.add((VariableElement)elementHandle.resolve((CompilationInfo)copy));
                                }
                            }
                            EqualsHashCodeGenerator.generateEqualsAndHashCode(copy, path, EqualsHashCodeGenerator.this.generateEquals ? equalsElements : null, EqualsHashCodeGenerator.this.generateHashCode ? hashCodeElements : null, caretOffset);
                        }
                    }
                });
                GeneratorUtils.guardedCommit(this.component, mr);
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
    }

    public static void generateEqualsAndHashCode(WorkingCopy wc, TreePath path) {
        ExecutableElement[] arr = EqualsHashCodeGenerator.overridesHashCodeAndEquals((CompilationInfo)wc, wc.getTrees().getElement(path), null);
        Set e = arr[0] == null ? Collections.emptySet() : null;
        Set h = arr[1] == null ? Collections.emptySet() : null;
        EqualsHashCodeGenerator.generateEqualsAndHashCode(wc, path, e, h, -1);
    }

    static void generateEqualsAndHashCode(WorkingCopy wc, TreePath path, Iterable<? extends VariableElement> equalsFields, Iterable<? extends VariableElement> hashCodeFields, int offset) {
        assert (TreeUtilities.CLASS_TREE_KINDS.contains((Object)path.getLeaf().getKind()));
        TypeElement te = (TypeElement)wc.getTrees().getElement(path);
        if (te != null) {
            ClassTree nue = (ClassTree)path.getLeaf();
            Scope scope = wc.getTrees().getScope(path);
            ArrayList<MethodTree> members = new ArrayList<MethodTree>();
            if (hashCodeFields != null) {
                members.add(EqualsHashCodeGenerator.createHashCodeMethod(wc, hashCodeFields, scope));
            }
            if (equalsFields != null) {
                DeclaredType dt = (DeclaredType)te.asType();
                if (!dt.getTypeArguments().isEmpty()) {
                    WildcardType wt = wc.getTypes().getWildcardType(null, null);
                    TypeMirror[] typeArgs = new TypeMirror[dt.getTypeArguments().size()];
                    for (int i = 0; i < typeArgs.length; ++i) {
                        typeArgs[i] = wt;
                    }
                    dt = dt.getEnclosingType().getKind() == TypeKind.DECLARED ? wc.getTypes().getDeclaredType((DeclaredType)dt.getEnclosingType(), te, typeArgs) : wc.getTypes().getDeclaredType(te, typeArgs);
                }
                members.add(EqualsHashCodeGenerator.createEqualsMethod(wc, equalsFields, dt, scope));
            }
            wc.rewrite((Tree)nue, (Tree)GeneratorUtils.insertClassMembers(wc, nue, members, offset));
        }
    }

    private static MethodTree createEqualsMethod(WorkingCopy wc, Iterable<? extends VariableElement> equalsFields, DeclaredType type, Scope scope) {
        TypeElement objElement;
        TreeMaker make = wc.getTreeMaker();
        EnumSet<Modifier> mods = EnumSet.of(Modifier.PUBLIC);
        List<VariableTree> params = Collections.singletonList(make.Variable(make.Modifiers(EnumSet.noneOf(Modifier.class)), (CharSequence)"obj", (objElement = wc.getElements().getTypeElement("java.lang.Object")) != null ? make.Type(objElement.asType()) : make.Identifier((CharSequence)"Object"), null));
        ArrayList<Object> statements = new ArrayList<Object>();
        statements.add((Object)make.If((ExpressionTree)make.Binary(Tree.Kind.EQUAL_TO, (ExpressionTree)make.Identifier((CharSequence)"obj"), (ExpressionTree)make.Identifier((CharSequence)"null")), (StatementTree)make.Return((ExpressionTree)make.Identifier((CharSequence)"false")), null));
        statements.add((Object)make.If((ExpressionTree)make.Binary(Tree.Kind.NOT_EQUAL_TO, (ExpressionTree)make.MethodInvocation(Collections.emptyList(), (ExpressionTree)make.Identifier((CharSequence)"getClass"), Collections.emptyList()), (ExpressionTree)make.MethodInvocation(Collections.emptyList(), (ExpressionTree)make.MemberSelect((ExpressionTree)make.Identifier((CharSequence)"obj"), (CharSequence)"getClass"), Collections.emptyList())), (StatementTree)make.Return((ExpressionTree)make.Identifier((CharSequence)"false")), null));
        statements.add((Object)make.Variable(make.Modifiers(EnumSet.of(Modifier.FINAL)), (CharSequence)"other", make.Type((TypeMirror)type), (ExpressionTree)make.TypeCast(make.Type((TypeMirror)type), (ExpressionTree)make.Identifier((CharSequence)"obj"))));
        for (VariableElement ve : equalsFields) {
            TypeMirror tm = ve.asType();
            ExpressionTree condition = EqualsHashCodeGenerator.prepareExpression(wc, EQUALS_PATTERNS, tm, ve, scope);
            statements.add((Object)make.If(condition, (StatementTree)make.Return((ExpressionTree)make.Identifier((CharSequence)"false")), null));
        }
        statements.add((Object)make.Return((ExpressionTree)make.Identifier((CharSequence)"true")));
        BlockTree body = make.Block(statements, false);
        ModifiersTree modifiers = EqualsHashCodeGenerator.prepareModifiers(wc, mods, make);
        return make.Method(modifiers, (CharSequence)"equals", (Tree)make.PrimitiveType(TypeKind.BOOLEAN), Collections.emptyList(), params, Collections.emptyList(), body, null);
    }

    private static MethodTree createHashCodeMethod(WorkingCopy wc, Iterable<? extends VariableElement> hashCodeFields, Scope scope) {
        TreeMaker make = wc.getTreeMaker();
        EnumSet<Modifier> mods = EnumSet.of(Modifier.PUBLIC);
        int startNumber = EqualsHashCodeGenerator.generatePrimeNumber(2, 10);
        int multiplyNumber = EqualsHashCodeGenerator.generatePrimeNumber(10, 100);
        ArrayList<Object> statements = new ArrayList<Object>();
        statements.add((Object)make.Variable(make.Modifiers(EnumSet.noneOf(Modifier.class)), (CharSequence)"hash", (Tree)make.PrimitiveType(TypeKind.INT), (ExpressionTree)make.Literal((Object)startNumber)));
        for (VariableElement ve : hashCodeFields) {
            TypeMirror tm = ve.asType();
            ExpressionTree variableRead = EqualsHashCodeGenerator.prepareExpression(wc, HASH_CODE_PATTERNS, tm, ve, scope);
            statements.add((Object)make.ExpressionStatement((ExpressionTree)make.Assignment((ExpressionTree)make.Identifier((CharSequence)"hash"), (ExpressionTree)make.Binary(Tree.Kind.PLUS, (ExpressionTree)make.Binary(Tree.Kind.MULTIPLY, (ExpressionTree)make.Literal((Object)multiplyNumber), (ExpressionTree)make.Identifier((CharSequence)"hash")), variableRead))));
        }
        statements.add((Object)make.Return((ExpressionTree)make.Identifier((CharSequence)"hash")));
        BlockTree body = make.Block(statements, false);
        ModifiersTree modifiers = EqualsHashCodeGenerator.prepareModifiers(wc, mods, make);
        return make.Method(modifiers, (CharSequence)"hashCode", (Tree)make.PrimitiveType(TypeKind.INT), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), body, null);
    }

    private static boolean isPrimeNumber(int n) {
        int squareRoot = (int)Math.sqrt(n) + 1;
        if (n % 2 == 0) {
            return false;
        }
        for (int cntr = 3; cntr < squareRoot; ++cntr) {
            if (n % cntr != 0) continue;
            return false;
        }
        return true;
    }

    private static int generatePrimeNumber(int lowerLimit, int higherLimit) {
        if (randomNumber > 0) {
            return randomNumber;
        }
        Random r = new Random(System.currentTimeMillis());
        int proposed = r.nextInt(higherLimit - lowerLimit) + lowerLimit;
        while (!EqualsHashCodeGenerator.isPrimeNumber(proposed)) {
            ++proposed;
        }
        if (proposed > higherLimit) {
            --proposed;
            while (!EqualsHashCodeGenerator.isPrimeNumber(proposed)) {
                --proposed;
            }
        }
        return proposed;
    }

    private static ModifiersTree prepareModifiers(WorkingCopy wc, Set<Modifier> mods, TreeMaker make) {
        TypeElement override;
        LinkedList<AnnotationTree> annotations = new LinkedList<AnnotationTree>();
        if (GeneratorUtils.supportsOverride((CompilationInfo)wc) && (override = wc.getElements().getTypeElement("java.lang.Override")) != null) {
            annotations.add(wc.getTreeMaker().Annotation((Tree)wc.getTreeMaker().QualIdent((Element)override), Collections.emptyList()));
        }
        ModifiersTree modifiers = make.Modifiers(mods, annotations);
        return modifiers;
    }

    private static KindOfType detectKind(CompilationInfo info, TypeMirror tm) {
        if (tm.getKind().isPrimitive()) {
            return KindOfType.valueOf(tm.getKind().name());
        }
        if (tm.getKind() == TypeKind.ARRAY) {
            return ((ArrayType)tm).getComponentType().getKind().isPrimitive() ? KindOfType.ARRAY_PRIMITIVE : KindOfType.ARRAY;
        }
        if (tm.getKind() == TypeKind.DECLARED) {
            Types t = info.getTypes();
            TypeElement en = info.getElements().getTypeElement("java.lang.Enum");
            if (en != null && t.isSubtype(tm, t.erasure(en.asType()))) {
                return KindOfType.ENUM;
            }
            if (((DeclaredType)tm).asElement().getKind().isClass() && ((TypeElement)((DeclaredType)tm).asElement()).getQualifiedName().contentEquals("java.lang.String")) {
                return KindOfType.STRING;
            }
        }
        return KindOfType.OTHER;
    }

    private static String choosePattern(CompilationInfo info, TypeMirror tm, Map<Acceptor, String> patterns) {
        for (Map.Entry<Acceptor, String> e : patterns.entrySet()) {
            if (!e.getKey().accept(info, tm)) continue;
            return e.getValue();
        }
        throw new IllegalStateException();
    }

    private static ExpressionTree prepareExpression(WorkingCopy wc, Map<Acceptor, String> patterns, TypeMirror tm, VariableElement ve, Scope scope) {
        String pattern = EqualsHashCodeGenerator.choosePattern((CompilationInfo)wc, tm, patterns);
        assert (pattern != null);
        String conditionText = MapFormat.format((String)pattern, Collections.singletonMap("VAR", ve.getSimpleName().toString()));
        ExpressionTree exp = wc.getTreeUtilities().parseExpression(conditionText, new SourcePositions[1]);
        exp = (ExpressionTree)GeneratorUtilities.get((WorkingCopy)wc).importFQNs((Tree)exp);
        wc.getTreeUtilities().attributeTree((Tree)exp, scope);
        return exp;
    }

    static {
        EQUALS_PATTERNS.put(new SimpleAcceptor(KindOfType.BOOLEAN, KindOfType.BYTE, KindOfType.SHORT, KindOfType.INT, KindOfType.LONG, KindOfType.CHAR), "this.{VAR} != other.{VAR}");
        EQUALS_PATTERNS.put(new SimpleAcceptor(KindOfType.FLOAT), "java.lang.Float.floatToIntBits(this.{VAR}) != java.lang.Float.floatToIntBits(other.{VAR})");
        EQUALS_PATTERNS.put(new SimpleAcceptor(KindOfType.DOUBLE), "java.lang.Double.doubleToLongBits(this.{VAR}) != java.lang.Double.doubleToLongBits(other.{VAR})");
        EQUALS_PATTERNS.put(new SimpleAcceptor(KindOfType.ENUM), "this.{VAR} != other.{VAR}");
        EQUALS_PATTERNS.put(new SimpleAcceptor(KindOfType.ARRAY_PRIMITIVE), "! java.util.Arrays.equals(this.{VAR}, other.{VAR}");
        EQUALS_PATTERNS.put(new SimpleAcceptor(KindOfType.ARRAY), "! java.util.Arrays.deepEquals(this.{VAR}, other.{VAR}");
        EQUALS_PATTERNS.put(new MethodExistsAcceptor("java.util.Objects", "equals", SourceVersion.RELEASE_7), "! java.util.Objects.equals(this.{VAR}, other.{VAR})");
        EQUALS_PATTERNS.put(new SimpleAcceptor(KindOfType.STRING), "(this.{VAR} == null) ? (other.{VAR} != null) : !this.{VAR}.equals(other.{VAR})");
        EQUALS_PATTERNS.put(new SimpleAcceptor(KindOfType.OTHER), "this.{VAR} != other.{VAR} && (this.{VAR} == null || !this.{VAR}.equals(other.{VAR}))");
        HASH_CODE_PATTERNS = new LinkedHashMap<Acceptor, String>();
        HASH_CODE_PATTERNS.put(new SimpleAcceptor(KindOfType.BYTE, KindOfType.SHORT, KindOfType.INT, KindOfType.CHAR), "this.{VAR}");
        HASH_CODE_PATTERNS.put(new SimpleAcceptor(KindOfType.LONG), "(int) (this.{VAR} ^ (this.{VAR} >>> 32))");
        HASH_CODE_PATTERNS.put(new SimpleAcceptor(KindOfType.FLOAT), "java.lang.Float.floatToIntBits(this.{VAR})");
        HASH_CODE_PATTERNS.put(new SimpleAcceptor(KindOfType.DOUBLE), "(int) (Double.doubleToLongBits(this.{VAR}) ^ (Double.doubleToLongBits(this.{VAR}) >>> 32))");
        HASH_CODE_PATTERNS.put(new SimpleAcceptor(KindOfType.BOOLEAN), "(this.{VAR} ? 1 : 0)");
        HASH_CODE_PATTERNS.put(new SimpleAcceptor(KindOfType.ARRAY_PRIMITIVE), "java.util.Arrays.hashCode(this.{VAR}");
        HASH_CODE_PATTERNS.put(new SimpleAcceptor(KindOfType.ARRAY), "java.util.Arrays.deepHashCode(this.{VAR}");
        HASH_CODE_PATTERNS.put(new MethodExistsAcceptor("java.util.Objects", "hashCode", SourceVersion.RELEASE_7), "java.util.Objects.hashCode(this.{VAR})");
        HASH_CODE_PATTERNS.put(new SimpleAcceptor(KindOfType.ENUM), "(this.{VAR} != null ? this.{VAR}.hashCode() : 0)");
        HASH_CODE_PATTERNS.put(new SimpleAcceptor(KindOfType.STRING), "(this.{VAR} != null ? this.{VAR}.hashCode() : 0)");
        HASH_CODE_PATTERNS.put(new SimpleAcceptor(KindOfType.OTHER), "(this.{VAR} != null ? this.{VAR}.hashCode() : 0)");
    }

    public static interface Cancel {
        public boolean isCanceled();
    }

    private static final class MethodExistsAcceptor
    implements Acceptor {
        private final String fqn;
        private final String methodName;
        private final SourceVersion minimalVersion;

        public MethodExistsAcceptor(String fqn, String methodName) {
            this(fqn, methodName, null);
        }

        public MethodExistsAcceptor(String fqn, String methodName, SourceVersion minimalVersion) {
            this.fqn = fqn;
            this.methodName = methodName;
            this.minimalVersion = minimalVersion;
        }

        @Override
        public boolean accept(CompilationInfo info, TypeMirror tm) {
            if (this.minimalVersion != null && this.minimalVersion.compareTo(info.getSourceVersion()) > 0) {
                return false;
            }
            TypeElement clazz = info.getElements().getTypeElement(this.fqn);
            if (clazz == null) {
                return false;
            }
            for (ExecutableElement m : ElementFilter.methodsIn(clazz.getEnclosedElements())) {
                if (!m.getSimpleName().contentEquals(this.methodName)) continue;
                return true;
            }
            return false;
        }
    }

    private static final class SimpleAcceptor
    implements Acceptor {
        private final Set<KindOfType> kinds;

        public SimpleAcceptor(KindOfType kind) {
            this.kinds = EnumSet.of(kind);
        }

        public /* varargs */ SimpleAcceptor(KindOfType kind, KindOfType ... moreKinds) {
            this.kinds = EnumSet.of(kind);
            this.kinds.addAll(Arrays.asList(moreKinds));
        }

        @Override
        public boolean accept(CompilationInfo info, TypeMirror tm) {
            return this.kinds.contains((Object)EqualsHashCodeGenerator.detectKind(info, tm));
        }
    }

    private static interface Acceptor {
        public boolean accept(CompilationInfo var1, TypeMirror var2);
    }

    private static enum KindOfType {
        BOOLEAN,
        BYTE,
        SHORT,
        INT,
        LONG,
        CHAR,
        FLOAT,
        DOUBLE,
        ENUM,
        ARRAY_PRIMITIVE,
        ARRAY,
        STRING,
        OTHER;
        

        private KindOfType() {
        }
    }

    public static class Factory
    implements CodeGenerator.Factory {
        public List<? extends CodeGenerator> create(Lookup context) {
            ArrayList<EqualsHashCodeGenerator> ret = new ArrayList<EqualsHashCodeGenerator>();
            JTextComponent component = (JTextComponent)context.lookup(JTextComponent.class);
            CompilationController controller = (CompilationController)context.lookup(CompilationController.class);
            TreePath path = (TreePath)context.lookup(TreePath.class);
            TreePath treePath = path = path != null ? org.netbeans.modules.editor.java.Utilities.getPathElementOfKind(TreeUtilities.CLASS_TREE_KINDS, path) : null;
            if (component == null || controller == null || path == null) {
                return ret;
            }
            try {
                EqualsHashCodeGenerator gen;
                controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                Element elem = controller.getTrees().getElement(path);
                if (elem != null && (gen = EqualsHashCodeGenerator.createEqualsHashCodeGenerator(component, controller, elem)) != null) {
                    ret.add(gen);
                }
            }
            catch (IOException ioe) {
                // empty catch block
            }
            return ret;
        }
    }

}

