/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseAction
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.editor.indent.api.Indent
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.codegen;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.BaseAction;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.editor.indent.api.Indent;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public final class InsertSemicolonAction
extends BaseAction {
    private final boolean withNewline;
    private final char what;

    protected InsertSemicolonAction(String name, char what, boolean withNewline) {
        super(name);
        this.withNewline = withNewline;
        this.what = what;
        this.putValue("ShortDescription", (Object)NbBundle.getMessage(InsertSemicolonAction.class, (String)name));
    }

    public InsertSemicolonAction(String name, boolean withNewline) {
        this(name, ';', withNewline);
    }

    public InsertSemicolonAction(boolean withNewLine) {
        this(withNewLine ? "complete-line-newline" : "complete-line", ';', withNewLine);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void actionPerformed(ActionEvent evt, JTextComponent target) {
        if (!target.isEditable() || !target.isEnabled()) {
            target.getToolkit().beep();
            return;
        }
        BaseDocument doc = (BaseDocument)target.getDocument();
        Indent indenter = Indent.get((Document)doc);
        indenter.lock();
        try {
            final class R
            implements Runnable {
                final /* synthetic */ JTextComponent val$target;
                final /* synthetic */ BaseDocument val$doc;
                final /* synthetic */ Indent val$indenter;

                R() {
                    this.val$target = var2_2;
                    this.val$doc = var3_3;
                    this.val$indenter = var4_4;
                }

                @Override
                public void run() {
                    try {
                        Caret caret = this.val$target.getCaret();
                        int dotpos = caret.getDot();
                        int eoloffset = Utilities.getRowEnd((JTextComponent)this.val$target, (int)dotpos);
                        this.val$doc.insertString(eoloffset, "" + this$0.what, null);
                        if (this$0.withNewline) {
                            this.val$doc.insertString(dotpos, "-", null);
                            this.val$doc.remove(dotpos, 1);
                            int eolDot = Utilities.getRowEnd((JTextComponent)this.val$target, (int)caret.getDot());
                            int newDotPos = this.val$indenter.indentNewLine(eolDot);
                            caret.setDot(newDotPos);
                        }
                    }
                    catch (BadLocationException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
            }
            doc.runAtomicAsUser((Runnable)new R(this, target, doc, indenter));
        }
        finally {
            indenter.unlock();
        }
    }

}

