/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.view.BeanTreeView
 *  org.openide.explorer.view.NodeTreeModel
 *  org.openide.explorer.view.Visualizer
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.java.editor.codegen.ui;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.InputMap;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import org.netbeans.modules.java.editor.codegen.ui.CheckRenderer;
import org.netbeans.modules.java.editor.codegen.ui.ElementNode;
import org.openide.explorer.view.BeanTreeView;
import org.openide.explorer.view.NodeTreeModel;
import org.openide.explorer.view.Visualizer;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

public class CheckTreeView
extends BeanTreeView {
    private NodeTreeModel nodeTreeModel;

    public CheckTreeView() {
        this.setFocusable(false);
        CheckListener l = new CheckListener();
        this.tree.addMouseListener(l);
        this.tree.addKeyListener(l);
        CheckRenderer check = new CheckRenderer();
        this.tree.setCellRenderer(check);
        this.tree.getSelectionModel().setSelectionMode(1);
        this.tree.setShowsRootHandles(false);
        InputMap input = this.tree.getInputMap(0);
        if (null != input) {
            input.remove(KeyStroke.getKeyStroke(10, 0));
        }
        this.setBorder(UIManager.getBorder("ScrollPane.border"));
    }

    protected NodeTreeModel createModel() {
        this.nodeTreeModel = super.createModel();
        return this.nodeTreeModel;
    }

    public void expandRow(int row) {
        this.tree.expandRow(row);
    }

    public boolean getScrollsOnExpand() {
        return this.tree.getScrollsOnExpand();
    }

    public void setScrollsOnExpand(boolean scrolls) {
        this.tree.setScrollsOnExpand(scrolls);
    }

    protected void showPath(TreePath path) {
        this.tree.expandPath(path);
        this.showPathWithoutExpansion(path);
    }

    protected void showSelection(TreePath[] treePaths) {
        this.tree.getSelectionModel().setSelectionPaths(treePaths);
        if (treePaths.length == 1) {
            this.showPathWithoutExpansion(treePaths[0]);
        }
    }

    private void showPathWithoutExpansion(TreePath path) {
        Rectangle rect = this.tree.getPathBounds(path);
        if (rect != null && this.getWidth() > 0 && this.getHeight() > 0) {
            this.tree.scrollRectToVisible(rect);
        }
    }

    class CheckListener
    implements MouseListener,
    KeyListener {
        CheckListener() {
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (!e.isPopupTrigger()) {
                TreePath path = CheckTreeView.this.tree.getPathForLocation(e.getPoint().x, e.getPoint().y);
                this.toggle(path);
            }
        }

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            TreePath path;
            JTree tree;
            if (e.getKeyCode() == 32 && e.getSource() instanceof JTree && this.toggle(path = (tree = (JTree)e.getSource()).getSelectionPath())) {
                e.consume();
            }
            if (e.getKeyCode() == 10 && !(e.getSource() instanceof JTree)) {
                TreePath path2 = CheckTreeView.this.tree.getSelectionPath();
                this.toggle(path2);
                e.consume();
            }
        }

        private boolean toggle(TreePath treePath) {
            if (treePath == null) {
                return false;
            }
            Node node = Visualizer.findNode((Object)treePath.getLastPathComponent());
            if (node == null) {
                return false;
            }
            ElementNode.Description description = (ElementNode.Description)node.getLookup().lookup(ElementNode.Description.class);
            if (description != null) {
                if (description.isSelectable()) {
                    description.setSelected(!description.isSelected());
                    return true;
                }
                boolean newState = !description.isSelected();
                description.setSelected(newState);
                this.toggleChildren(description.getSubs(), newState);
            }
            return false;
        }

        private void toggleChildren(List<ElementNode.Description> children, boolean newState) {
            if (null == children) {
                return;
            }
            for (ElementNode.Description d : children) {
                d.setSelected(newState);
                this.toggleChildren(d.getSubs(), newState);
            }
        }
    }

}

