/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.highlighting.HighlightsContainer
 *  org.netbeans.spi.editor.highlighting.HighlightsLayer
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory$Context
 *  org.netbeans.spi.editor.highlighting.ZOrder
 *  org.netbeans.spi.editor.highlighting.support.OffsetsBag
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.editor.codegen;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.java.editor.codegen.CodeDeleter;
import org.netbeans.modules.java.editor.overridden.PopupUtil;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsLayer;
import org.netbeans.spi.editor.highlighting.HighlightsLayerFactory;
import org.netbeans.spi.editor.highlighting.ZOrder;
import org.netbeans.spi.editor.highlighting.support.OffsetsBag;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class RemoveSurroundingCodePanel
extends JPanel {
    private JTextComponent component;
    public JLabel jLabel1;
    public JList jList1;
    public JScrollPane jScrollPane1;

    public RemoveSurroundingCodePanel(JTextComponent component, List<? extends CodeDeleter> deleters) {
        this.component = component;
        this.initComponents();
        this.setFocusable(false);
        this.setNextFocusableComponent(this.jList1);
        this.setBackground(this.jList1.getBackground());
        this.jScrollPane1.setBackground(this.jList1.getBackground());
        this.jList1.setModel(this.createModel(deleters));
        this.jList1.setSelectedIndex(0);
        this.jList1.setVisibleRowCount(deleters.size() > 8 ? 8 : deleters.size());
        this.jList1.setCellRenderer(new Renderer(this.jList1));
        this.jList1.grabFocus();
        this.jList1.addFocusListener(new FocusAdapter(){

            @Override
            public void focusLost(FocusEvent e) {
                PopupUtil.hidePopup();
                RemoveSurroundingCodePanel.getBag(RemoveSurroundingCodePanel.this.component.getDocument()).clear();
            }
        });
        this.jList1.addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent e) {
                RemoveSurroundingCodePanel.this.setHighlights();
            }
        });
        this.setHighlights();
    }

    private void initComponents() {
        this.jScrollPane1 = new JScrollPane();
        this.jList1 = new JList();
        this.jLabel1 = new JLabel();
        this.setBorder(BorderFactory.createLineBorder(new Color(64, 64, 64)));
        this.setLayout(new BorderLayout());
        this.jScrollPane1.setBorder(BorderFactory.createEmptyBorder(2, 4, 4, 4));
        this.jList1.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseReleased(MouseEvent evt) {
                RemoveSurroundingCodePanel.this.listMouseReleased(evt);
            }
        });
        this.jList1.addMouseMotionListener(new MouseMotionAdapter(){

            @Override
            public void mouseMoved(MouseEvent evt) {
                RemoveSurroundingCodePanel.this.listMouseMoved(evt);
            }
        });
        this.jList1.addKeyListener(new KeyAdapter(){

            @Override
            public void keyReleased(KeyEvent evt) {
                RemoveSurroundingCodePanel.this.listKeyReleased(evt);
            }
        });
        this.jScrollPane1.setViewportView(this.jList1);
        this.add((Component)this.jScrollPane1, "Center");
        this.jLabel1.setText(NbBundle.getMessage(RemoveSurroundingCodePanel.class, (String)"LBL_remove_surrounding_code"));
        this.jLabel1.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        this.jLabel1.setOpaque(true);
        this.add((Component)this.jLabel1, "First");
    }

    private void listMouseReleased(MouseEvent evt) {
        this.invokeSelected();
    }

    private void listKeyReleased(KeyEvent evt) {
        KeyStroke ks = KeyStroke.getKeyStrokeForEvent(evt);
        if (ks.getKeyCode() == 10 || ks.getKeyCode() == 32) {
            this.invokeSelected();
        }
    }

    private void listMouseMoved(MouseEvent evt) {
        int idx = this.jList1.locationToIndex(evt.getPoint());
        if (idx != this.jList1.getSelectedIndex()) {
            this.jList1.setSelectedIndex(idx);
        }
    }

    private DefaultListModel createModel(List<? extends CodeDeleter> deleters) {
        DefaultListModel<CodeDeleter> model = new DefaultListModel<CodeDeleter>();
        for (CodeDeleter generator : deleters) {
            model.addElement(generator);
        }
        return model;
    }

    private void invokeSelected() {
        Object value;
        PopupUtil.hidePopup();
        if (Utilities.isMac()) {
            this.component.requestFocus();
        }
        if ((value = this.jList1.getSelectedValue()) instanceof CodeDeleter) {
            ((CodeDeleter)value).invoke();
        }
    }

    private void setHighlights() {
        OffsetsBag bag;
        Object value = this.jList1.getSelectedValue();
        if (value instanceof CodeDeleter && (bag = ((CodeDeleter)value).getHighlight()) != null) {
            RemoveSurroundingCodePanel.getBag(this.component.getDocument()).setHighlights(bag);
        }
    }

    private static OffsetsBag getBag(Document doc) {
        OffsetsBag bag = (OffsetsBag)doc.getProperty(RemoveSurroundingCodePanel.class);
        if (bag == null) {
            bag = new OffsetsBag(doc);
            doc.putProperty(RemoveSurroundingCodePanel.class, (Object)bag);
        }
        return bag;
    }

    private static class Renderer
    extends DefaultListCellRenderer {
        private static int DARKER_COLOR_COMPONENT = 5;
        private Color fgColor;
        private Color bgColor;
        private Color bgColorDarker;
        private Color bgSelectionColor;
        private Color fgSelectionColor;

        public Renderer(JList list) {
            this.setFont(list.getFont());
            this.fgColor = list.getForeground();
            this.bgColor = list.getBackground();
            this.bgColorDarker = new Color(Math.abs(this.bgColor.getRed() - DARKER_COLOR_COMPONENT), Math.abs(this.bgColor.getGreen() - DARKER_COLOR_COMPONENT), Math.abs(this.bgColor.getBlue() - DARKER_COLOR_COMPONENT));
            this.bgSelectionColor = list.getSelectionBackground();
            this.fgSelectionColor = list.getSelectionForeground();
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean hasFocus) {
            if (isSelected) {
                this.setForeground(this.fgSelectionColor);
                this.setBackground(this.bgSelectionColor);
            } else {
                this.setForeground(this.fgColor);
                this.setBackground(index % 2 == 0 ? this.bgColor : this.bgColorDarker);
            }
            this.setText(value instanceof CodeDeleter ? ((CodeDeleter)value).getDisplayName() : value.toString());
            return this;
        }
    }

    public static class UnwrapCodeHighlightsLayerFactory
    implements HighlightsLayerFactory {
        public HighlightsLayer[] createLayers(HighlightsLayerFactory.Context context) {
            return new HighlightsLayer[]{HighlightsLayer.create((String)UnwrapCodeHighlightsLayerFactory.class.getName(), (ZOrder)ZOrder.DEFAULT_RACK, (boolean)true, (HighlightsContainer)RemoveSurroundingCodePanel.getBag(context.getDocument()))};
        }
    }

}

