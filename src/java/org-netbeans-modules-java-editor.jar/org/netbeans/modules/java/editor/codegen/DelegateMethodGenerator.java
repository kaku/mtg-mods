/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.AnnotationTree
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.ExpressionStatementTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodInvocationTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.ReturnTree
 *  com.sun.source.tree.Scope
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.ElementUtilities
 *  org.netbeans.api.java.source.ElementUtilities$ElementAcceptor
 *  org.netbeans.api.java.source.GeneratorUtilities
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.ModificationResult
 *  org.netbeans.api.java.source.ScanUtils
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.java.source.TreeMaker
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.WorkingCopy
 *  org.netbeans.api.progress.ProgressUtils
 *  org.netbeans.editor.Utilities
 *  org.netbeans.spi.editor.codegen.CodeGenerator
 *  org.netbeans.spi.editor.codegen.CodeGenerator$Factory
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.editor.codegen;

import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.ExpressionStatementTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.ReturnTree;
import com.sun.source.tree.Scope;
import com.sun.source.tree.Tree;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.awt.Dialog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.java.source.GeneratorUtilities;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.api.java.source.ScanUtils;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.TreeMaker;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.java.editor.codegen.ConstructorGenerator;
import org.netbeans.modules.java.editor.codegen.GeneratorUtils;
import org.netbeans.modules.java.editor.codegen.ui.DelegatePanel;
import org.netbeans.modules.java.editor.codegen.ui.ElementNode;
import org.netbeans.spi.editor.codegen.CodeGenerator;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class DelegateMethodGenerator
implements CodeGenerator {
    private static final String ERROR = "<error>";
    private static final Logger log = Logger.getLogger(DelegateMethodGenerator.class.getName());
    private JTextComponent component;
    private ElementHandle<TypeElement> handle;
    private ElementNode.Description description;

    private DelegateMethodGenerator(JTextComponent component, ElementHandle<TypeElement> handle, ElementNode.Description description) {
        this.component = component;
        this.handle = handle;
        this.description = description;
    }

    public String getDisplayName() {
        return NbBundle.getMessage(DelegateMethodGenerator.class, (String)"LBL_delegate_method");
    }

    public void invoke() {
        final DelegatePanel panel = new DelegatePanel(this.component, this.handle, this.description);
        DialogDescriptor dialogDescriptor = GeneratorUtils.createDialogDescriptor(panel, NbBundle.getMessage(ConstructorGenerator.class, (String)"LBL_generate_delegate"));
        Dialog dialog = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
        dialog.setVisible(true);
        if (dialogDescriptor.getValue() == dialogDescriptor.getDefaultValue()) {
            JavaSource js = JavaSource.forDocument((Document)this.component.getDocument());
            ElementHandle<? extends Element> delegateField = panel.getDelegateField();
            if (delegateField != null && delegateField.getKind() == ElementKind.CLASS) {
                return;
            }
            if (js != null) {
                try {
                    final int caretOffset = this.component.getCaretPosition();
                    ModificationResult mr = js.runModificationTask((Task)new Task<WorkingCopy>(){

                        public void run(WorkingCopy copy) throws IOException {
                            copy.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                            Element e = DelegateMethodGenerator.this.handle.resolve((CompilationInfo)copy);
                            TreePath path = e != null ? copy.getTrees().getPath(e) : copy.getTreeUtilities().pathFor(caretOffset);
                            path = org.netbeans.modules.editor.java.Utilities.getPathElementOfKind(TreeUtilities.CLASS_TREE_KINDS, path);
                            if (path == null) {
                                String message = NbBundle.getMessage(DelegateMethodGenerator.class, (String)"ERR_CannotFindOriginalClass");
                                Utilities.setStatusBoldText((JTextComponent)DelegateMethodGenerator.this.component, (String)message);
                            } else {
                                ElementHandle<? extends Element> handle = panel.getDelegateField();
                                VariableElement delegate = handle != null ? (VariableElement)handle.resolve((CompilationInfo)copy) : null;
                                ArrayList<ExecutableElement> methods = new ArrayList<ExecutableElement>();
                                for (ElementHandle<? extends Element> elementHandle : panel.getDelegateMethods()) {
                                    methods.add((ExecutableElement)elementHandle.resolve((CompilationInfo)copy));
                                }
                                DelegateMethodGenerator.generateDelegatingMethods(copy, path, delegate, methods, caretOffset);
                            }
                        }
                    });
                    GeneratorUtils.guardedCommit(this.component, mr);
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }
    }

    public static ElementNode.Description getAvailableMethods(JTextComponent component, final ElementHandle<? extends TypeElement> typeElementHandle, final ElementHandle<? extends VariableElement> fieldHandle) {
        JavaSource js;
        if (fieldHandle.getKind().isField() && (js = JavaSource.forDocument((Document)component.getDocument())) != null) {
            final int caretOffset = component.getCaretPosition();
            final ElementNode.Description[] description = new ElementNode.Description[1];
            final AtomicBoolean cancel = new AtomicBoolean();
            ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

                @Override
                public void run() {
                    try {
                        ScanUtils.waitUserActionTask((JavaSource)js, (Task)new Task<CompilationController>(){

                            public void run(CompilationController controller) throws IOException {
                                JavaSource.Phase phase;
                                if (controller.getPhase().compareTo((Enum)JavaSource.Phase.RESOLVED) < 0 && (phase = controller.toPhase(JavaSource.Phase.RESOLVED)).compareTo((Enum)JavaSource.Phase.RESOLVED) < 0) {
                                    if (log.isLoggable(Level.SEVERE)) {
                                        log.log(Level.SEVERE, "Cannot reach required phase. Leaving without action.");
                                    }
                                    return;
                                }
                                if (cancel.get()) {
                                    return;
                                }
                                description[0] = DelegateMethodGenerator.getAvailableMethods((CompilationInfo)controller, caretOffset, typeElementHandle, fieldHandle);
                            }
                        });
                    }
                    catch (IOException ioe) {
                        Exceptions.printStackTrace((Throwable)ioe);
                    }
                }

            }, (String)NbBundle.getMessage(DelegateMethodGenerator.class, (String)"LBL_Get_Available_Methods"), (AtomicBoolean)cancel, (boolean)false);
            cancel.set(true);
            return description[0];
        }
        return null;
    }

    static List<ElementNode.Description> computeUsableFieldsDescriptions(CompilationInfo info, TreePath path) {
        TypeElement cls;
        Elements elements = info.getElements();
        Trees trees = info.getTrees();
        LinkedHashMap<Element, ArrayList<ElementNode.Description>> map = new LinkedHashMap<Element, ArrayList<ElementNode.Description>>();
        for (Scope scope = trees.getScope((TreePath)path); scope != null && (cls = scope.getEnclosingClass()) != null; scope = scope.getEnclosingScope()) {
            DeclaredType type = (DeclaredType)cls.asType();
            for (VariableElement field : ElementFilter.fieldsIn(elements.getAllMembers(cls))) {
                TypeMirror fieldType = field.asType();
                if ("<error>".contentEquals(field.getSimpleName()) || fieldType.getKind().isPrimitive() || fieldType.getKind() == TypeKind.ARRAY || fieldType.getKind() == TypeKind.DECLARED && ((DeclaredType)fieldType).asElement() == cls || !trees.isAccessible(scope, (Element)field, type)) continue;
                ArrayList<ElementNode.Description> descriptions = (ArrayList<ElementNode.Description>)map.get(field.getEnclosingElement());
                if (descriptions == null) {
                    descriptions = new ArrayList<ElementNode.Description>();
                    map.put(field.getEnclosingElement(), descriptions);
                }
                descriptions.add(ElementNode.Description.create(info, field, null, false, false));
            }
        }
        ArrayList<ElementNode.Description> descriptions = new ArrayList<ElementNode.Description>();
        for (Map.Entry entry : map.entrySet()) {
            descriptions.add(ElementNode.Description.create(info, (Element)entry.getKey(), (List)entry.getValue(), false, false));
        }
        return descriptions;
    }

    static ElementNode.Description getAvailableMethods(CompilationInfo controller, int caretOffset, ElementHandle<? extends TypeElement> typeElementHandle, ElementHandle<? extends VariableElement> fieldHandle) {
        final TypeElement origin = (TypeElement)typeElementHandle.resolve(controller);
        VariableElement field = (VariableElement)ScanUtils.checkElement((CompilationInfo)controller, (Element)fieldHandle.resolve(controller));
        assert (origin != null && field != null);
        final ElementUtilities eu = controller.getElementUtilities();
        final Trees trees = controller.getTrees();
        final Scope scope = controller.getTreeUtilities().scopeFor(caretOffset);
        ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

            public boolean accept(Element e, TypeMirror type) {
                if (e.getKind() == ElementKind.METHOD && trees.isAccessible(scope, e, (DeclaredType)type)) {
                    Element impl = eu.getImplementationOf((ExecutableElement)e, origin);
                    return impl == null || !impl.getModifiers().contains((Object)Modifier.FINAL) && impl.getEnclosingElement() != origin;
                }
                return false;
            }
        };
        LinkedHashMap<Element, ArrayList<ElementNode.Description>> map = new LinkedHashMap<Element, ArrayList<ElementNode.Description>>();
        for (ExecutableElement method : ElementFilter.methodsIn(eu.getMembers(field.asType(), acceptor))) {
            ArrayList<ElementNode.Description> descriptions = (ArrayList<ElementNode.Description>)map.get(method.getEnclosingElement());
            if (descriptions == null) {
                descriptions = new ArrayList<ElementNode.Description>();
                map.put(method.getEnclosingElement(), descriptions);
            }
            descriptions.add(ElementNode.Description.create(controller, method, null, true, false));
        }
        ArrayList<ElementNode.Description> descriptions = new ArrayList<ElementNode.Description>();
        for (Map.Entry entry : map.entrySet()) {
            descriptions.add(ElementNode.Description.create(controller, (Element)entry.getKey(), (List)entry.getValue(), false, false));
        }
        if (!descriptions.isEmpty()) {
            Collections.reverse(descriptions);
        }
        return ElementNode.Description.create(descriptions);
    }

    static void generateDelegatingMethods(WorkingCopy wc, TreePath path, VariableElement delegate, Iterable<? extends ExecutableElement> methods, int offset) {
        assert (TreeUtilities.CLASS_TREE_KINDS.contains((Object)path.getLeaf().getKind()));
        TypeElement te = (TypeElement)wc.getTrees().getElement(path);
        if (te != null) {
            ClassTree clazz = (ClassTree)path.getLeaf();
            ArrayList<MethodTree> members = new ArrayList<MethodTree>();
            for (ExecutableElement executableElement : methods) {
                members.add(DelegateMethodGenerator.createDelegatingMethod(wc, delegate, executableElement, (DeclaredType)te.asType()));
            }
            wc.rewrite((Tree)clazz, (Tree)GeneratorUtils.insertClassMembers(wc, clazz, members, offset));
        }
    }

    private static MethodTree createDelegatingMethod(WorkingCopy wc, VariableElement delegate, ExecutableElement method, DeclaredType type) {
        TreeMaker make = wc.getTreeMaker();
        boolean useThisToDereference = false;
        Name delegateName = delegate.getSimpleName();
        for (VariableElement ve : method.getParameters()) {
            if (!delegateName.contentEquals(ve.getSimpleName())) continue;
            useThisToDereference = true;
            break;
        }
        ArrayList<IdentifierTree> args = new ArrayList<IdentifierTree>();
        for (VariableElement ve2 : method.getParameters()) {
            args.add(make.Identifier((CharSequence)ve2.getSimpleName()));
        }
        ExpressionTree methodSelect = method.getModifiers().contains((Object)Modifier.STATIC) ? make.QualIdent(method.getEnclosingElement()) : (useThisToDereference ? make.MemberSelect((ExpressionTree)make.Identifier((CharSequence)"this"), (CharSequence)delegateName) : make.Identifier((CharSequence)delegateName));
        MethodInvocationTree exp = make.MethodInvocation(Collections.emptyList(), (ExpressionTree)make.MemberSelect(methodSelect, (CharSequence)method.getSimpleName()), args);
        ExpressionStatementTree stmt = method.getReturnType().getKind() == TypeKind.VOID ? make.ExpressionStatement((ExpressionTree)exp) : make.Return((ExpressionTree)exp);
        BlockTree body = make.Block(Collections.singletonList(stmt), false);
        MethodTree prototype = GeneratorUtilities.get((WorkingCopy)wc).createMethod(delegate.asType().getKind() == TypeKind.DECLARED ? (DeclaredType)delegate.asType() : type, method);
        ModifiersTree mt = prototype.getModifiers();
        try {
            if (wc.getElements().getTypeElement("java.lang.Override") != null && wc.getSourceVersion().compareTo(SourceVersion.RELEASE_5) >= 0 && wc.getTypes().asMemberOf(type, method) != null) {
                mt = make.addModifiersAnnotation(mt, make.Annotation((Tree)make.Identifier((CharSequence)"Override"), Collections.emptyList()));
            }
        }
        catch (IllegalArgumentException iae) {
            // empty catch block
        }
        return make.Method(mt, (CharSequence)prototype.getName(), prototype.getReturnType(), prototype.getTypeParameters(), prototype.getParameters(), prototype.getThrows(), body, (ExpressionTree)prototype.getDefaultValue());
    }

    public static class Factory
    implements CodeGenerator.Factory {
        public List<? extends CodeGenerator> create(Lookup context) {
            ArrayList<DelegateMethodGenerator> ret = new ArrayList<DelegateMethodGenerator>();
            JTextComponent component = (JTextComponent)context.lookup(JTextComponent.class);
            CompilationController controller = (CompilationController)context.lookup(CompilationController.class);
            TreePath path = (TreePath)context.lookup(TreePath.class);
            TreePath treePath = path = path != null ? org.netbeans.modules.editor.java.Utilities.getPathElementOfKind(TreeUtilities.CLASS_TREE_KINDS, path) : null;
            if (component == null || controller == null || path == null) {
                return ret;
            }
            try {
                controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
            }
            catch (IOException ioe) {
                return ret;
            }
            TypeElement typeElement = (TypeElement)controller.getTrees().getElement(path);
            if (typeElement == null || !typeElement.getKind().isClass()) {
                return ret;
            }
            List<ElementNode.Description> descriptions = DelegateMethodGenerator.computeUsableFieldsDescriptions((CompilationInfo)controller, path);
            if (!descriptions.isEmpty()) {
                Collections.reverse(descriptions);
                ret.add(new DelegateMethodGenerator(component, ElementHandle.create((Element)typeElement), ElementNode.Description.create(descriptions)));
            }
            return ret;
        }
    }

}

