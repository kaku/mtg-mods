/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.api.editor.settings.FontColorSettings
 */
package org.netbeans.modules.java.editor.semantic;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.modules.java.editor.semantic.ColoringAttributes;
import org.netbeans.modules.java.editor.semantic.UnusedTooltipResolver;

public final class ColoringManager {
    private static final Map<Set<ColoringAttributes>, String> type2Coloring = new LinkedHashMap<Set<ColoringAttributes>, String>();

    private static /* varargs */ void put(String coloring, ColoringAttributes ... attributes) {
        EnumSet<ColoringAttributes> attribs = EnumSet.copyOf(Arrays.asList(attributes));
        type2Coloring.put(attribs, coloring);
    }

    public static AttributeSet getColoringImpl(ColoringAttributes.Coloring colorings) {
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((MimePath)MimePath.get((String)"text/x-java")).lookup(FontColorSettings.class);
        if (fcs == null) {
            return AttributesUtilities.createImmutable((AttributeSet[])new AttributeSet[0]);
        }
        assert (fcs != null);
        LinkedList<AttributeSet> attribs = new LinkedList<AttributeSet>();
        EnumSet<ColoringAttributes> es = EnumSet.noneOf(ColoringAttributes.class);
        es.addAll(colorings);
        if (colorings.contains((Object)ColoringAttributes.UNUSED)) {
            attribs.add(AttributesUtilities.createImmutable((Object[])new Object[]{EditorStyleConstants.Tooltip, new UnusedTooltipResolver()}));
            attribs.add(AttributesUtilities.createImmutable((Object[])new Object[]{"unused-browseable", Boolean.TRUE}));
        }
        for (Map.Entry<Set<ColoringAttributes>, String> attribs2Colorings : type2Coloring.entrySet()) {
            if (!es.containsAll((Collection)attribs2Colorings.getKey())) continue;
            String key = attribs2Colorings.getValue();
            es.removeAll((Collection)attribs2Colorings.getKey());
            if (key == null) continue;
            AttributeSet colors = fcs.getTokenFontColors(key);
            if (colors == null) {
                Logger.getLogger(ColoringManager.class.getName()).log(Level.SEVERE, "no colors for: {0}", key);
                continue;
            }
            attribs.add(ColoringManager.adjustAttributes(colors));
        }
        Collections.reverse(attribs);
        AttributeSet result = AttributesUtilities.createComposite((AttributeSet[])attribs.toArray(new AttributeSet[0]));
        return result;
    }

    private static AttributeSet adjustAttributes(AttributeSet as) {
        LinkedList<Object> attrs = new LinkedList<Object>();
        Enumeration e = as.getAttributeNames();
        while (e.hasMoreElements()) {
            Object key = e.nextElement();
            Object value = as.getAttribute(key);
            if (value == Boolean.FALSE) continue;
            attrs.add(key);
            attrs.add(value);
        }
        return AttributesUtilities.createImmutable((Object[])attrs.toArray());
    }

    static {
        ColoringManager.put("mark-occurrences", ColoringAttributes.MARK_OCCURRENCES);
        ColoringManager.put("mod-type-parameter-use", ColoringAttributes.TYPE_PARAMETER_USE);
        ColoringManager.put("mod-type-parameter-declaration", ColoringAttributes.TYPE_PARAMETER_DECLARATION);
        ColoringManager.put("mod-enum-declaration", ColoringAttributes.ENUM, ColoringAttributes.DECLARATION);
        ColoringManager.put("mod-annotation-type-declaration", ColoringAttributes.ANNOTATION_TYPE, ColoringAttributes.DECLARATION);
        ColoringManager.put("mod-interface-declaration", ColoringAttributes.INTERFACE, ColoringAttributes.DECLARATION);
        ColoringManager.put("mod-class-declaration", ColoringAttributes.CLASS, ColoringAttributes.DECLARATION);
        ColoringManager.put("mod-constructor-declaration", ColoringAttributes.CONSTRUCTOR, ColoringAttributes.DECLARATION);
        ColoringManager.put("mod-method-declaration", ColoringAttributes.METHOD, ColoringAttributes.DECLARATION);
        ColoringManager.put("mod-parameter-declaration", ColoringAttributes.PARAMETER, ColoringAttributes.DECLARATION);
        ColoringManager.put("mod-local-variable-declaration", ColoringAttributes.LOCAL_VARIABLE, ColoringAttributes.DECLARATION);
        ColoringManager.put("mod-field-declaration", ColoringAttributes.FIELD, ColoringAttributes.DECLARATION);
        ColoringManager.put("mod-enum", ColoringAttributes.ENUM);
        ColoringManager.put("mod-annotation-type", ColoringAttributes.ANNOTATION_TYPE);
        ColoringManager.put("mod-interface", ColoringAttributes.INTERFACE);
        ColoringManager.put("mod-class", ColoringAttributes.CLASS);
        ColoringManager.put("mod-constructor", ColoringAttributes.CONSTRUCTOR);
        ColoringManager.put("mod-method", ColoringAttributes.METHOD);
        ColoringManager.put("mod-parameter", ColoringAttributes.PARAMETER);
        ColoringManager.put("mod-local-variable", ColoringAttributes.LOCAL_VARIABLE);
        ColoringManager.put("mod-field", ColoringAttributes.FIELD);
        ColoringManager.put("mod-public", ColoringAttributes.PUBLIC);
        ColoringManager.put("mod-protected", ColoringAttributes.PROTECTED);
        ColoringManager.put("mod-package-private", ColoringAttributes.PACKAGE_PRIVATE);
        ColoringManager.put("mod-private", ColoringAttributes.PRIVATE);
        ColoringManager.put("mod-static", ColoringAttributes.STATIC);
        ColoringManager.put("mod-abstract", ColoringAttributes.ABSTRACT);
        ColoringManager.put("mod-deprecated", ColoringAttributes.DEPRECATED);
        ColoringManager.put("mod-undefined", ColoringAttributes.UNDEFINED);
        ColoringManager.put("mod-unused", ColoringAttributes.UNUSED);
        ColoringManager.put("javadoc-identifier", ColoringAttributes.JAVADOC_IDENTIFIER);
    }
}

