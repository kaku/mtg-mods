/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.JavaParserResultTask
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.whitelist.WhiteListQuery
 *  org.netbeans.api.whitelist.WhiteListQuery$Result
 *  org.netbeans.api.whitelist.WhiteListQuery$RuleDescription
 *  org.netbeans.api.whitelist.WhiteListQuery$WhiteList
 *  org.netbeans.api.whitelist.support.WhiteListSupport
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.netbeans.modules.parsing.spi.SchedulerTask
 *  org.netbeans.modules.parsing.spi.TaskFactory
 *  org.netbeans.spi.editor.hints.ErrorDescription
 *  org.netbeans.spi.editor.hints.ErrorDescriptionFactory
 *  org.netbeans.spi.editor.hints.HintsController
 *  org.netbeans.spi.editor.hints.Severity
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.java.editor.whitelist;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.Trees;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.JavaParserResultTask;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.whitelist.WhiteListQuery;
import org.netbeans.api.whitelist.support.WhiteListSupport;
import org.netbeans.modules.java.editor.whitelist.Bundle;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.SchedulerTask;
import org.netbeans.modules.parsing.spi.TaskFactory;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.netbeans.spi.editor.hints.ErrorDescriptionFactory;
import org.netbeans.spi.editor.hints.HintsController;
import org.netbeans.spi.editor.hints.Severity;
import org.openide.filesystems.FileObject;

public class WhiteListCheckTask
extends JavaParserResultTask<Parser.Result> {
    private static final String ID = "white-list-checker";
    private final AtomicBoolean canceled = new AtomicBoolean();

    private WhiteListCheckTask() {
        super(JavaSource.Phase.RESOLVED);
    }

    public void run(Parser.Result result, SchedulerEvent event) {
        this.canceled.set(false);
        CompilationInfo info = CompilationInfo.get((Parser.Result)result);
        FileObject file = info.getFileObject();
        if (file == null) {
            return;
        }
        HintsController.setErrors((FileObject)file, (String)"white-list-checker", Collections.emptyList());
        WhiteListQuery.WhiteList whiteList = WhiteListQuery.getWhiteList((FileObject)file);
        if (whiteList == null) {
            return;
        }
        CompilationUnitTree cu = info.getCompilationUnit();
        Map problems = WhiteListSupport.getWhiteListViolations((CompilationUnitTree)cu, (WhiteListQuery.WhiteList)whiteList, (Trees)info.getTrees(), (Callable)new Callable<Boolean>(){

            @Override
            public Boolean call() throws Exception {
                return WhiteListCheckTask.this.canceled.get();
            }
        });
        if (problems == null) {
            return;
        }
        SourcePositions sp = info.getTrees().getSourcePositions();
        ArrayList<ErrorDescription> errors = new ArrayList<ErrorDescription>(problems.size());
        for (Map.Entry problem : problems.entrySet()) {
            if (this.canceled.get()) {
                return;
            }
            Tree tree = (Tree)problem.getKey();
            int start = (int)sp.getStartPosition(cu, tree);
            int end = (int)sp.getEndPosition(cu, tree);
            assert (!((WhiteListQuery.Result)problem.getValue()).isAllowed());
            if (start < 0 || end < 0) continue;
            errors.add(ErrorDescriptionFactory.createErrorDescription((Severity)Severity.WARNING, (String)WhiteListCheckTask.formatViolationDescription((WhiteListQuery.Result)problem.getValue()), (FileObject)file, (int)start, (int)end));
        }
        if (this.canceled.get()) {
            return;
        }
        HintsController.setErrors((FileObject)file, (String)"white-list-checker", errors);
    }

    public int getPriority() {
        return Integer.MAX_VALUE;
    }

    public Class<? extends Scheduler> getSchedulerClass() {
        return Scheduler.EDITOR_SENSITIVE_TASK_SCHEDULER;
    }

    public void cancel() {
        this.canceled.set(true);
    }

    private static String formatViolationDescription(WhiteListQuery.Result result) {
        assert (result.getViolatedRules() != null);
        if (result.getViolatedRules().size() == 1) {
            return ((WhiteListQuery.RuleDescription)result.getViolatedRules().get(0)).getRuleDescription();
        }
        StringBuilder sb = new StringBuilder(Bundle.MSG_Violations());
        for (WhiteListQuery.RuleDescription rule : result.getViolatedRules()) {
            sb.append("\n - ");
            sb.append(rule.getRuleDescription());
        }
        return sb.toString();
    }

    public static class Factory
    extends TaskFactory {
        public Collection<? extends SchedulerTask> create(Snapshot snapshot) {
            return Collections.singleton(new WhiteListCheckTask());
        }
    }

}

