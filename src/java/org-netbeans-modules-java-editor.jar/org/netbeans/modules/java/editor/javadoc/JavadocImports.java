/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.doctree.DocCommentTree
 *  com.sun.source.doctree.DocTree
 *  com.sun.source.doctree.IdentifierTree
 *  com.sun.source.doctree.ParamTree
 *  com.sun.source.doctree.ReferenceTree
 *  com.sun.source.doctree.SeeTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.MemberReferenceTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.DocSourcePositions
 *  com.sun.source.util.DocTreePath
 *  com.sun.source.util.DocTreePathScanner
 *  com.sun.source.util.DocTrees
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreePathScanner
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.lexer.JavadocTokenId
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 */
package org.netbeans.modules.java.editor.javadoc;

import com.sun.source.doctree.DocCommentTree;
import com.sun.source.doctree.DocTree;
import com.sun.source.doctree.IdentifierTree;
import com.sun.source.doctree.ParamTree;
import com.sun.source.doctree.ReferenceTree;
import com.sun.source.doctree.SeeTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.MemberReferenceTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.DocSourcePositions;
import com.sun.source.util.DocTreePath;
import com.sun.source.util.DocTreePathScanner;
import com.sun.source.util.DocTrees;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreePathScanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.lexer.JavadocTokenId;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.java.editor.javadoc.JavadocCompletionUtils;

public final class JavadocImports {
    private static final Set<String> ALL_REF_TAG_NAMES = new HashSet<String>(Arrays.asList("@link", "@linkplain", "@value", "@see", "@throws"));

    private JavadocImports() {
    }

    public static Set<String> computeUnresolvedImports(CompilationInfo javac) {
        UnresolvedImportScanner scanner = new UnresolvedImportScanner(javac);
        scanner.scan((Tree)javac.getCompilationUnit(), (Object)null);
        return scanner.unresolved;
    }

    public static Set<TypeElement> computeReferencedElements(final CompilationInfo javac, TreePath tp) {
        final DocTrees trees = javac.getDocTrees();
        DocCommentTree docComment = trees.getDocCommentTree(tp);
        if (docComment == null) {
            return Collections.emptySet();
        }
        final HashSet<TypeElement> result = new HashSet<TypeElement>();
        new DocTreePathScanner<Void, Void>(){

            public Void visitReference(ReferenceTree node, Void p) {
                new TreePathScanner<Void, Void>(){

                    public Void visitIdentifier(com.sun.source.tree.IdentifierTree node, Void p) {
                        Element el = trees.getElement(this.getCurrentPath());
                        if (el != null && (el.getKind().isClass() || el.getKind().isInterface())) {
                            result.add((TypeElement)el);
                        }
                        return (Void)TreePathScanner.super.visitIdentifier(node, (Object)p);
                    }

                    public Void scan(Iterable<? extends TreePath> toAnalyze, Void p) {
                        for (TreePath tp : toAnalyze) {
                            this.scan(tp, (Object)p);
                        }
                        return null;
                    }
                }.scan(JavadocImports.referenceEmbeddedSourceNodes(javac, this.getCurrentPath()), null);
                return (Void)DocTreePathScanner.super.visitReference(node, (Object)p);
            }

            public Void visitSee(SeeTree node, Void p) {
                return (Void)DocTreePathScanner.super.visitSee(node, (Object)p);
            }

        }.scan(new DocTreePath(tp, docComment), (Object)null);
        return result;
    }

    public static List<Token> computeTokensOfReferencedElements(final CompilationInfo javac, final TreePath forElement, final Element toFind) {
        final DocTrees trees = javac.getDocTrees();
        final DocCommentTree docComment = javac.getDocTrees().getDocCommentTree(forElement);
        if (docComment == null) {
            return Collections.emptyList();
        }
        final ArrayList<Token> result = new ArrayList<Token>();
        new DocTreePathScanner<Void, Void>(){
            private TokenSequence<JavadocTokenId> javadoc;

            public Void visitReference(ReferenceTree node, Void p) {
                new TreePathScanner<Void, Void>(){

                    public Void visitIdentifier(com.sun.source.tree.IdentifierTree node, Void p) {
                        if (toFind.equals(trees.getElement(this.getCurrentPath()))) {
                            2.this.handleUsage((int)trees.getSourcePositions().getStartPosition(javac.getCompilationUnit(), (Tree)node));
                        }
                        return null;
                    }

                    public Void visitMemberSelect(MemberSelectTree node, Void p) {
                        if (toFind.equals(trees.getElement(this.getCurrentPath()))) {
                            int[] span = javac.getTreeUtilities().findNameSpan(node);
                            if (span != null) {
                                2.this.handleUsage(span[0]);
                            }
                            return null;
                        }
                        return (Void)TreePathScanner.super.visitMemberSelect(node, (Object)p);
                    }

                    public Void scan(Iterable<? extends TreePath> toAnalyze, Void p) {
                        for (TreePath tp : toAnalyze) {
                            this.scan(tp, (Object)p);
                        }
                        return null;
                    }
                }.scan(JavadocImports.referenceEmbeddedSourceNodes(javac, this.getCurrentPath()), null);
                if (toFind.equals(trees.getElement(this.getCurrentPath()))) {
                    int[] span = javac.getTreeUtilities().findNameSpan(docComment, node);
                    if (span != null) {
                        this.handleUsage(span[0]);
                    }
                    return null;
                }
                return (Void)DocTreePathScanner.super.visitReference(node, (Object)p);
            }

            private void handleUsage(int start) {
                if (this.javadoc == null) {
                    this.javadoc = JavadocImports.getJavadocTS(javac, start);
                    if (this.javadoc == null) {
                        return;
                    }
                }
                this.javadoc.move(start);
                if (this.javadoc.moveNext()) {
                    result.add(this.javadoc.token());
                }
            }

            public Void visitParam(ParamTree node, Void p) {
                if (node.getName() != null && toFind.equals(JavadocImports.paramElementFor(trees.getElement(forElement), node))) {
                    this.handleUsage((int)trees.getSourcePositions().getStartPosition(javac.getCompilationUnit(), docComment, (DocTree)node.getName()));
                    return null;
                }
                return (Void)DocTreePathScanner.super.visitParam(node, (Object)p);
            }

            public Void visitSee(SeeTree node, Void p) {
                return (Void)DocTreePathScanner.super.visitSee(node, (Object)p);
            }

        }.scan(new DocTreePath(forElement, docComment), (Object)null);
        return result;
    }

    private static Element paramElementFor(Element methodOrClass, ParamTree ptag) {
        ElementKind kind = methodOrClass.getKind();
        List params = Collections.emptyList();
        if (kind == ElementKind.METHOD || kind == ElementKind.CONSTRUCTOR) {
            ExecutableElement ee = (ExecutableElement)methodOrClass;
            params = ptag.isTypeParameter() ? ee.getTypeParameters() : ee.getParameters();
        } else if (kind.isClass() || kind.isInterface()) {
            TypeElement te = (TypeElement)methodOrClass;
            params = te.getTypeParameters();
        }
        for (Element param : params) {
            if (!param.getSimpleName().contentEquals(ptag.getName().getName())) continue;
            return param;
        }
        return null;
    }

    public static Element findReferencedElement(final CompilationInfo javac, final int offset) {
        final DocTrees trees = javac.getDocTrees();
        final TreePath tp = JavadocCompletionUtils.findJavadoc(javac, offset);
        if (tp == null) {
            return null;
        }
        final DocCommentTree docComment = javac.getDocTrees().getDocCommentTree(tp);
        if (docComment == null) {
            return null;
        }
        final DocSourcePositions positions = trees.getSourcePositions();
        final Element[] result = new Element[1];
        new DocTreePathScanner<Void, Void>(){

            public Void scan(DocTree node, Void p) {
                if (node != null && positions.getStartPosition(javac.getCompilationUnit(), docComment, node) <= (long)offset && positions.getEndPosition(javac.getCompilationUnit(), docComment, node) >= (long)offset) {
                    return (Void)DocTreePathScanner.super.scan(node, (Object)p);
                }
                return null;
            }

            public Void visitReference(ReferenceTree node, Void p) {
                int[] span = javac.getTreeUtilities().findNameSpan(docComment, node);
                if (span != null && span[0] <= offset && span[1] >= offset) {
                    result[0] = trees.getElement(this.getCurrentPath());
                    return null;
                }
                new TreePathScanner<Void, Void>(){

                    public Void visitIdentifier(com.sun.source.tree.IdentifierTree node, Void p) {
                        if (positions.getStartPosition(javac.getCompilationUnit(), (Tree)node) <= (long)offset && positions.getEndPosition(javac.getCompilationUnit(), (Tree)node) >= (long)offset) {
                            result[0] = trees.getElement(this.getCurrentPath());
                        }
                        return null;
                    }

                    public Void visitMemberSelect(MemberSelectTree node, Void p) {
                        int[] span = javac.getTreeUtilities().findNameSpan(node);
                        if (span != null && span[0] <= offset && span[1] >= offset) {
                            result[0] = trees.getElement(this.getCurrentPath());
                            return null;
                        }
                        return (Void)TreePathScanner.super.visitMemberSelect(node, (Object)p);
                    }

                    public Void visitMemberReference(MemberReferenceTree node, Void p) {
                        return (Void)TreePathScanner.super.visitMemberReference(node, (Object)p);
                    }

                    public Void scan(Iterable<? extends TreePath> toAnalyze, Void p) {
                        for (TreePath tp : toAnalyze) {
                            this.scan(tp, (Object)p);
                        }
                        return null;
                    }
                }.scan(JavadocImports.referenceEmbeddedSourceNodes(javac, this.getCurrentPath()), null);
                return (Void)DocTreePathScanner.super.visitReference(node, (Object)p);
            }

            public Void visitParam(ParamTree node, Void p) {
                if (node.getName() != null && positions.getStartPosition(javac.getCompilationUnit(), docComment, (DocTree)node.getName()) <= (long)offset && positions.getEndPosition(javac.getCompilationUnit(), docComment, (DocTree)node.getName()) >= (long)offset) {
                    result[0] = JavadocImports.paramElementFor(trees.getElement(tp), node);
                    return null;
                }
                return (Void)DocTreePathScanner.super.visitParam(node, (Object)p);
            }

            public Void visitSee(SeeTree node, Void p) {
                return (Void)DocTreePathScanner.super.visitSee(node, (Object)p);
            }

        }.scan(new DocTreePath(tp, docComment), (Object)null);
        return result[0];
    }

    public static Token findNameTokenOfReferencedElement(final CompilationInfo javac, final int offset) {
        DocTrees trees = javac.getDocTrees();
        TreePath tp = JavadocCompletionUtils.findJavadoc(javac, offset);
        if (tp == null) {
            return null;
        }
        final DocCommentTree docComment = javac.getDocTrees().getDocCommentTree(tp);
        if (docComment == null) {
            return null;
        }
        final DocSourcePositions positions = trees.getSourcePositions();
        final Token[] result = new Token[1];
        new DocTreePathScanner<Void, Void>(){

            public Void scan(DocTree node, Void p) {
                if (node != null && positions.getStartPosition(javac.getCompilationUnit(), docComment, node) <= (long)offset && positions.getEndPosition(javac.getCompilationUnit(), docComment, node) >= (long)offset) {
                    return (Void)DocTreePathScanner.super.scan(node, (Object)p);
                }
                return null;
            }

            public Void visitReference(ReferenceTree node, Void p) {
                int[] span = javac.getTreeUtilities().findNameSpan(docComment, node);
                if (span != null && span[0] <= offset && span[1] >= offset) {
                    this.handleUsage(offset);
                    return null;
                }
                new TreePathScanner<Void, Void>(){

                    public Void visitIdentifier(com.sun.source.tree.IdentifierTree node, Void p) {
                        if (positions.getStartPosition(javac.getCompilationUnit(), (Tree)node) <= (long)offset && positions.getEndPosition(javac.getCompilationUnit(), (Tree)node) >= (long)offset) {
                            4.this.handleUsage(offset);
                        }
                        return null;
                    }

                    public Void visitMemberSelect(MemberSelectTree node, Void p) {
                        int[] span = javac.getTreeUtilities().findNameSpan(node);
                        if (span != null && span[0] <= offset && span[1] >= offset) {
                            4.this.handleUsage(offset);
                            return null;
                        }
                        return (Void)TreePathScanner.super.visitMemberSelect(node, (Object)p);
                    }

                    public Void visitMemberReference(MemberReferenceTree node, Void p) {
                        return (Void)TreePathScanner.super.visitMemberReference(node, (Object)p);
                    }

                    public Void scan(Iterable<? extends TreePath> toAnalyze, Void p) {
                        for (TreePath tp : toAnalyze) {
                            this.scan(tp, (Object)p);
                        }
                        return null;
                    }
                }.scan(JavadocImports.referenceEmbeddedSourceNodes(javac, this.getCurrentPath()), null);
                return (Void)DocTreePathScanner.super.visitReference(node, (Object)p);
            }

            private void handleUsage(int start) {
                TokenSequence javadoc = JavadocImports.getJavadocTS(javac, start);
                if (javadoc == null) {
                    return;
                }
                javadoc.move(start);
                if (javadoc.moveNext()) {
                    result[0] = javadoc.token();
                }
            }

            public Void visitParam(ParamTree node, Void p) {
                if (node.getName() != null && positions.getStartPosition(javac.getCompilationUnit(), docComment, (DocTree)node.getName()) <= (long)offset && positions.getEndPosition(javac.getCompilationUnit(), docComment, (DocTree)node.getName()) >= (long)offset) {
                    result[0] = JavadocImports.findNameTokenOfParamTag(offset, JavadocImports.getJavadocTS(javac, offset));
                    return null;
                }
                return (Void)DocTreePathScanner.super.visitParam(node, (Object)p);
            }

            public Void visitSee(SeeTree node, Void p) {
                return (Void)DocTreePathScanner.super.visitSee(node, (Object)p);
            }

        }.scan(new DocTreePath(tp, docComment), (Object)null);
        return result[0];
    }

    private static Token<JavadocTokenId> findNameTokenOfParamTag(int startPos, TokenSequence<JavadocTokenId> jdTokenSequence) {
        Token result = null;
        if (JavadocImports.isInsideParamName(jdTokenSequence, startPos)) {
            int delta = jdTokenSequence.move(startPos);
            if (jdTokenSequence.moveNext() && (JavadocTokenId.IDENT == jdTokenSequence.token().id() || JavadocTokenId.HTML_TAG == jdTokenSequence.token().id()) || delta == 0 && jdTokenSequence.movePrevious() && (JavadocTokenId.IDENT == jdTokenSequence.token().id() || JavadocTokenId.HTML_TAG == jdTokenSequence.token().id())) {
                result = jdTokenSequence.token();
            }
        }
        return result;
    }

    public static boolean isInsideReference(TokenSequence<JavadocTokenId> jdts, int pos) {
        int delta = jdts.move(pos);
        if (jdts.moveNext() && JavadocTokenId.IDENT == jdts.token().id() || delta == 0 && jdts.movePrevious() && JavadocTokenId.IDENT == jdts.token().id()) {
            boolean isBeforeWS = false;
            block6 : while (jdts.movePrevious()) {
                Token jdt = jdts.token();
                switch ((JavadocTokenId)jdt.id()) {
                    case DOT: 
                    case HASH: 
                    case IDENT: {
                        if (!isBeforeWS) continue block6;
                        return false;
                    }
                    case OTHER_TEXT: {
                        isBeforeWS |= JavadocCompletionUtils.isWhiteSpace(jdt);
                        if (isBeforeWS |= JavadocCompletionUtils.isLineBreak(jdt)) continue block6;
                        return false;
                    }
                    case TAG: {
                        return isBeforeWS && JavadocImports.isReferenceTag(jdt);
                    }
                    case HTML_TAG: {
                        return false;
                    }
                }
                return false;
            }
        }
        return false;
    }

    public static boolean isInsideParamName(TokenSequence<JavadocTokenId> jdts, int pos) {
        int delta = jdts.move(pos);
        if ((jdts.moveNext() && (JavadocTokenId.IDENT == jdts.token().id() || JavadocTokenId.HTML_TAG == jdts.token().id()) || delta == 0 && jdts.movePrevious() && (JavadocTokenId.IDENT == jdts.token().id() || JavadocTokenId.HTML_TAG == jdts.token().id())) && jdts.movePrevious() && JavadocTokenId.OTHER_TEXT == jdts.token().id() && jdts.movePrevious() && JavadocTokenId.TAG == jdts.token().id()) {
            return "@param".contentEquals(jdts.token().text());
        }
        return false;
    }

    private static boolean isReferenceTag(Token<JavadocTokenId> tag) {
        String tagName = tag.text().toString().intern();
        return tag.id() == JavadocTokenId.TAG && ALL_REF_TAG_NAMES.contains(tagName);
    }

    private static TokenSequence<JavadocTokenId> getJavadocTS(CompilationInfo javac, int start) {
        TokenSequence javadoc = null;
        TokenSequence ts = SourceUtils.getJavaTokenSequence((TokenHierarchy)javac.getTokenHierarchy(), (int)start);
        if (ts.moveNext() && ts.token().id() == JavaTokenId.JAVADOC_COMMENT) {
            javadoc = ts.embedded(JavadocTokenId.language());
        }
        return javadoc;
    }

    private static Iterable<? extends TreePath> referenceEmbeddedSourceNodes(CompilationInfo info, DocTreePath ref) {
        List params;
        ArrayList<TreePath> result = new ArrayList<TreePath>();
        if (info.getTreeUtilities().getReferenceClass(ref) != null) {
            result.add(new TreePath(ref.getTreePath(), (Tree)info.getTreeUtilities().getReferenceClass(ref)));
        }
        if ((params = info.getTreeUtilities().getReferenceParameters(ref)) != null) {
            for (Tree et : params) {
                result.add(new TreePath(ref.getTreePath(), et));
            }
        }
        return result;
    }

    private static final class UnresolvedImportScanner
    extends TreePathScanner<Void, Void> {
        private final CompilationInfo javac;
        private Set<String> unresolved = new HashSet<String>();

        public UnresolvedImportScanner(CompilationInfo javac) {
            this.javac = javac;
        }

        public Void visitClass(ClassTree node, Void p) {
            this.resolveElement();
            return (Void)TreePathScanner.super.visitClass(node, (Object)p);
        }

        public Void visitMethod(MethodTree node, Void p) {
            this.resolveElement();
            return (Void)TreePathScanner.super.visitMethod(node, (Object)p);
        }

        public Void visitVariable(VariableTree node, Void p) {
            this.resolveElement();
            return (Void)TreePathScanner.super.visitVariable(node, (Object)p);
        }

        private void resolveElement() {
            final DocTrees trees = this.javac.getDocTrees();
            DocCommentTree dcComment = trees.getDocCommentTree(this.getCurrentPath());
            if (dcComment == null) {
                return;
            }
            new DocTreePathScanner<Void, Void>(){

                public Void visitReference(ReferenceTree node, Void p) {
                    new TreePathScanner<Void, Void>(){

                        public Void visitIdentifier(com.sun.source.tree.IdentifierTree node, Void p) {
                            Element el = trees.getElement(this.getCurrentPath());
                            if (el == null || el.asType().getKind() == TypeKind.ERROR) {
                                UnresolvedImportScanner.this.unresolved.add(node.getName().toString());
                            } else if (el.getKind() == ElementKind.PACKAGE) {
                                String s = ((PackageElement)el).getQualifiedName().toString();
                                if (UnresolvedImportScanner.this.javac.getElements().getPackageElement(s) == null) {
                                    UnresolvedImportScanner.this.unresolved.add(node.getName().toString());
                                }
                            }
                            return (Void)TreePathScanner.super.visitIdentifier(node, (Object)p);
                        }

                        public Void scan(Iterable<? extends TreePath> toAnalyze, Void p) {
                            for (TreePath tp : toAnalyze) {
                                this.scan(tp, (Object)p);
                            }
                            return null;
                        }
                    }.scan(JavadocImports.referenceEmbeddedSourceNodes(UnresolvedImportScanner.this.javac, this.getCurrentPath()), null);
                    return (Void)DocTreePathScanner.super.visitReference(node, (Object)p);
                }

            }.scan(new DocTreePath(this.getCurrentPath(), dcComment), (Object)null);
        }

    }

}

