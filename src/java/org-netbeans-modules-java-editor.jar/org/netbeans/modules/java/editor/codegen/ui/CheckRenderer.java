/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.view.Visualizer
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.java.editor.codegen.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.event.ChangeListener;
import javax.swing.tree.TreeCellRenderer;
import org.netbeans.modules.java.editor.codegen.ui.ElementNode;
import org.openide.explorer.view.Visualizer;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

class CheckRenderer
extends JPanel
implements TreeCellRenderer {
    private TristateCheckBox check;
    private JLabel label;
    private static final JList LIST_FOR_COLORS = new JList();

    public CheckRenderer() {
        this.setLayout(new BorderLayout());
        this.setOpaque(true);
        this.check = new TristateCheckBox();
        this.label = new JLabel();
        this.add((Component)this.check, "West");
        this.add((Component)this.label, "Center");
        this.check.setOpaque(false);
        this.label.setOpaque(false);
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean isSelected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.setOpaque(true);
        Node n = Visualizer.findNode((Object)value);
        ElementNode.Description description = (ElementNode.Description)n.getLookup().lookup(ElementNode.Description.class);
        if (description != null) {
            this.check.setVisible(description.isSelectable() || description.hasSelectableSubs());
            if (description.isSelectable()) {
                this.check.setSelected(description.isSelected());
            } else {
                this.check.setState(this.getCheckState(description.getSubs()));
            }
        }
        if (isSelected) {
            this.label.setForeground(LIST_FOR_COLORS.getSelectionForeground());
            panel.setOpaque(true);
            panel.setBackground(LIST_FOR_COLORS.getSelectionBackground());
        } else {
            this.label.setForeground(tree.getForeground());
            panel.setOpaque(false);
        }
        this.label.setText(n.getHtmlDisplayName());
        this.label.setIcon(new ImageIcon(n.getIcon(1)));
        panel.add((Component)this.check, "West");
        panel.add((Component)this.label, "Center");
        panel.setPreferredSize(new Dimension(this.label.getPreferredSize().width + this.check.getPreferredSize().width, panel.getPreferredSize().height));
        return panel;
    }

    private State getCheckState(List<ElementNode.Description> children) {
        if (null == children) {
            return State.OTHER;
        }
        int selCounter = 0;
        int unselCounter = 0;
        for (ElementNode.Description d : children) {
            if (!d.isSelectable()) continue;
            if (d.isSelected()) {
                ++selCounter;
            } else {
                ++unselCounter;
            }
            if (selCounter <= 0 || unselCounter <= 0) continue;
            return State.OTHER;
        }
        return selCounter > 0 ? State.SELECTED : State.NOT_SELECTED;
    }

    public Rectangle getCheckBounds() {
        return (Rectangle)this.check.getBounds().clone();
    }

    private static class TristateCheckBox
    extends JCheckBox {
        private final TristateDecorator model;

        public TristateCheckBox() {
            super(null, null);
            this.model = new TristateDecorator(this.getModel());
            this.setModel(this.model);
            this.setState(State.OTHER);
        }

        @Override
        public void addMouseListener(MouseListener l) {
        }

        public void setState(State state) {
            this.model.setState(state);
        }

        public State getState() {
            return this.model.getState();
        }

        @Override
        public void setSelected(boolean b) {
            if (b) {
                this.setState(State.SELECTED);
            } else {
                this.setState(State.NOT_SELECTED);
            }
        }

        private class TristateDecorator
        implements ButtonModel {
            private final ButtonModel other;

            private TristateDecorator(ButtonModel other) {
                this.other = other;
            }

            private void setState(State state) {
                if (state == State.NOT_SELECTED) {
                    this.other.setArmed(false);
                    this.setPressed(false);
                    this.setSelected(false);
                } else if (state == State.SELECTED) {
                    this.other.setArmed(false);
                    this.setPressed(false);
                    this.setSelected(true);
                } else {
                    this.other.setArmed(true);
                    this.setPressed(true);
                    this.setSelected(true);
                }
            }

            private State getState() {
                if (this.isSelected() && !this.isArmed()) {
                    return State.SELECTED;
                }
                if (this.isSelected() && this.isArmed()) {
                    return State.OTHER;
                }
                return State.NOT_SELECTED;
            }

            @Override
            public void setArmed(boolean b) {
            }

            @Override
            public void setEnabled(boolean b) {
                TristateCheckBox.this.setFocusable(b);
                this.other.setEnabled(b);
            }

            @Override
            public boolean isArmed() {
                return this.other.isArmed();
            }

            @Override
            public boolean isSelected() {
                return this.other.isSelected();
            }

            @Override
            public boolean isEnabled() {
                return this.other.isEnabled();
            }

            @Override
            public boolean isPressed() {
                return this.other.isPressed();
            }

            @Override
            public boolean isRollover() {
                return this.other.isRollover();
            }

            @Override
            public void setSelected(boolean b) {
                this.other.setSelected(b);
            }

            @Override
            public void setPressed(boolean b) {
                this.other.setPressed(b);
            }

            @Override
            public void setRollover(boolean b) {
                this.other.setRollover(b);
            }

            @Override
            public void setMnemonic(int key) {
                this.other.setMnemonic(key);
            }

            @Override
            public int getMnemonic() {
                return this.other.getMnemonic();
            }

            @Override
            public void setActionCommand(String s) {
                this.other.setActionCommand(s);
            }

            @Override
            public String getActionCommand() {
                return this.other.getActionCommand();
            }

            @Override
            public void setGroup(ButtonGroup group) {
                this.other.setGroup(group);
            }

            @Override
            public void addActionListener(ActionListener l) {
                this.other.addActionListener(l);
            }

            @Override
            public void removeActionListener(ActionListener l) {
                this.other.removeActionListener(l);
            }

            @Override
            public void addItemListener(ItemListener l) {
                this.other.addItemListener(l);
            }

            @Override
            public void removeItemListener(ItemListener l) {
                this.other.removeItemListener(l);
            }

            @Override
            public void addChangeListener(ChangeListener l) {
                this.other.addChangeListener(l);
            }

            @Override
            public void removeChangeListener(ChangeListener l) {
                this.other.removeChangeListener(l);
            }

            @Override
            public Object[] getSelectedObjects() {
                return this.other.getSelectedObjects();
            }
        }

    }

    private static enum State {
        SELECTED,
        NOT_SELECTED,
        OTHER;
        

        private State() {
        }
    }

}

