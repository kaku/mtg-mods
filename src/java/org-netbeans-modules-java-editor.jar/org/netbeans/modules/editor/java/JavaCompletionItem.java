/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ReturnTree
 *  com.sun.source.tree.Scope
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.ThrowTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.editor.completion.Completion
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.source.CodeStyle
 *  org.netbeans.api.java.source.CodeStyle$BracePlacement
 *  org.netbeans.api.java.source.CodeStyleUtils
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.GeneratorUtilities
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.ModificationResult
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.TypeMirrorHandle
 *  org.netbeans.api.java.source.WorkingCopy
 *  org.netbeans.api.java.source.support.ReferencesCount
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.api.progress.ProgressUtils
 *  org.netbeans.api.whitelist.WhiteListQuery
 *  org.netbeans.api.whitelist.WhiteListQuery$Operation
 *  org.netbeans.api.whitelist.WhiteListQuery$Result
 *  org.netbeans.api.whitelist.WhiteListQuery$WhiteList
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.lib.editor.codetemplates.api.CodeTemplate
 *  org.netbeans.lib.editor.codetemplates.api.CodeTemplateManager
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.indent.api.IndentUtils
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.spi.editor.completion.CompletionDocumentation
 *  org.netbeans.spi.editor.completion.CompletionItem
 *  org.netbeans.spi.editor.completion.CompletionResultSet
 *  org.netbeans.spi.editor.completion.CompletionTask
 *  org.netbeans.spi.editor.completion.support.CompletionUtilities
 *  org.netbeans.swing.plaf.LFCustoms
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.modules.editor.java;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ReturnTree;
import com.sun.source.tree.Scope;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.ThrowTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ErrorType;
import javax.lang.model.type.ExecutableType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.type.WildcardType;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import org.netbeans.api.editor.completion.Completion;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.source.CodeStyle;
import org.netbeans.api.java.source.CodeStyleUtils;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.GeneratorUtilities;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.TypeMirrorHandle;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.api.java.source.support.ReferencesCount;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.api.whitelist.WhiteListQuery;
import org.netbeans.editor.BaseDocument;
import org.netbeans.lib.editor.codetemplates.api.CodeTemplate;
import org.netbeans.lib.editor.codetemplates.api.CodeTemplateManager;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.indent.api.IndentUtils;
import org.netbeans.modules.editor.java.AutoImport;
import org.netbeans.modules.editor.java.JavaCompletionProvider;
import org.netbeans.modules.editor.java.LazySortText;
import org.netbeans.modules.editor.java.TypingCompletion;
import org.netbeans.modules.editor.java.Utilities;
import org.netbeans.modules.java.editor.codegen.GeneratorUtils;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.spi.editor.completion.CompletionDocumentation;
import org.netbeans.spi.editor.completion.CompletionItem;
import org.netbeans.spi.editor.completion.CompletionResultSet;
import org.netbeans.spi.editor.completion.CompletionTask;
import org.netbeans.spi.editor.completion.support.CompletionUtilities;
import org.netbeans.swing.plaf.LFCustoms;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.xml.XMLUtil;

public abstract class JavaCompletionItem
implements CompletionItem {
    protected static int SMART_TYPE = 1000;
    private static final String GENERATE_TEXT = NbBundle.getMessage(JavaCompletionItem.class, (String)"generate_Lbl");
    private static final Logger LOGGER = Logger.getLogger(JavaCompletionItem.class.getName());
    public static final String COLOR_END = "</font>";
    public static final String STRIKE = "<s>";
    public static final String STRIKE_END = "</s>";
    public static final String BOLD = "<b>";
    public static final String BOLD_END = "</b>";
    protected int substitutionOffset;
    private static final int PUBLIC_LEVEL = 3;
    private static final int PROTECTED_LEVEL = 2;
    private static final int PACKAGE_LEVEL = 1;
    private static final int PRIVATE_LEVEL = 0;

    public static JavaCompletionItem createKeywordItem(String kwd, String postfix, int substitutionOffset, boolean smartType) {
        return new KeywordItem(kwd, 0, postfix, substitutionOffset, smartType);
    }

    public static JavaCompletionItem createPackageItem(String pkgFQN, int substitutionOffset, boolean inPackageStatement) {
        return new PackageItem(pkgFQN, substitutionOffset, inPackageStatement);
    }

    public static JavaCompletionItem createTypeItem(CompilationInfo info, TypeElement elem, DeclaredType type, int substitutionOffset, ReferencesCount referencesCount, boolean isDeprecated, boolean insideNew, boolean addTypeVars, boolean addSimpleName, boolean smartType, boolean autoImportEnclosingType, WhiteListQuery.WhiteList whiteList) {
        switch (elem.getKind()) {
            case CLASS: {
                return new ClassItem(info, elem, type, 0, substitutionOffset, referencesCount, isDeprecated, insideNew, addTypeVars, addSimpleName, smartType, autoImportEnclosingType, whiteList);
            }
            case INTERFACE: {
                return new InterfaceItem(info, elem, type, 0, substitutionOffset, referencesCount, isDeprecated, insideNew, addTypeVars, addSimpleName, smartType, autoImportEnclosingType, whiteList);
            }
            case ENUM: {
                return new EnumItem(info, elem, type, 0, substitutionOffset, referencesCount, isDeprecated, insideNew, addSimpleName, smartType, autoImportEnclosingType, whiteList);
            }
            case ANNOTATION_TYPE: {
                return new AnnotationTypeItem(info, elem, type, 0, substitutionOffset, referencesCount, isDeprecated, insideNew, addSimpleName, smartType, autoImportEnclosingType, whiteList);
            }
        }
        throw new IllegalArgumentException("kind=" + (Object)((Object)elem.getKind()));
    }

    public static JavaCompletionItem createArrayItem(CompilationInfo info, ArrayType type, int substitutionOffset, ReferencesCount referencesCount, Elements elements, WhiteListQuery.WhiteList whiteList) {
        int dim = 0;
        TypeMirror tm = type;
        while (tm.getKind() == TypeKind.ARRAY) {
            tm = tm.getComponentType();
            ++dim;
        }
        if (tm.getKind().isPrimitive()) {
            return new KeywordItem(tm.toString(), dim, null, substitutionOffset, true);
        }
        if (tm.getKind() == TypeKind.DECLARED || tm.getKind() == TypeKind.ERROR) {
            DeclaredType dt = (DeclaredType)tm;
            TypeElement elem = (TypeElement)dt.asElement();
            switch (elem.getKind()) {
                case CLASS: {
                    return new ClassItem(info, elem, dt, dim, substitutionOffset, referencesCount, elements.isDeprecated(elem), false, false, false, true, false, whiteList);
                }
                case INTERFACE: {
                    return new InterfaceItem(info, elem, dt, dim, substitutionOffset, referencesCount, elements.isDeprecated(elem), false, false, false, true, false, whiteList);
                }
                case ENUM: {
                    return new EnumItem(info, elem, dt, dim, substitutionOffset, referencesCount, elements.isDeprecated(elem), false, false, true, false, whiteList);
                }
                case ANNOTATION_TYPE: {
                    return new AnnotationTypeItem(info, elem, dt, dim, substitutionOffset, referencesCount, elements.isDeprecated(elem), false, false, true, false, whiteList);
                }
            }
        }
        throw new IllegalArgumentException("array element kind=" + (Object)((Object)tm.getKind()));
    }

    public static JavaCompletionItem createTypeParameterItem(TypeParameterElement elem, int substitutionOffset) {
        return new TypeParameterItem(elem, substitutionOffset);
    }

    public static JavaCompletionItem createVariableItem(CompilationInfo info, VariableElement elem, TypeMirror type, int substitutionOffset, ReferencesCount referencesCount, boolean isInherited, boolean isDeprecated, boolean smartType, int assignToVarOffset, WhiteListQuery.WhiteList whiteList) {
        switch (elem.getKind()) {
            case LOCAL_VARIABLE: 
            case RESOURCE_VARIABLE: 
            case PARAMETER: 
            case EXCEPTION_PARAMETER: {
                return new VariableItem(info, type, elem.getSimpleName().toString(), substitutionOffset, false, smartType, assignToVarOffset);
            }
            case ENUM_CONSTANT: 
            case FIELD: {
                return new FieldItem(info, elem, type, substitutionOffset, referencesCount, isInherited, isDeprecated, smartType, assignToVarOffset, whiteList);
            }
        }
        throw new IllegalArgumentException("kind=" + (Object)((Object)elem.getKind()));
    }

    public static JavaCompletionItem createVariableItem(CompilationInfo info, String varName, int substitutionOffset, boolean newVarName, boolean smartType) {
        return new VariableItem(info, null, varName, substitutionOffset, newVarName, smartType, -1);
    }

    public static JavaCompletionItem createExecutableItem(CompilationInfo info, ExecutableElement elem, ExecutableType type, int substitutionOffset, ReferencesCount referencesCount, boolean isInherited, boolean isDeprecated, boolean inImport, boolean addSemicolon, boolean smartType, int assignToVarOffset, boolean memberRef, WhiteListQuery.WhiteList whiteList) {
        switch (elem.getKind()) {
            case METHOD: {
                return new MethodItem(info, elem, type, substitutionOffset, referencesCount, isInherited, isDeprecated, inImport, addSemicolon, smartType, assignToVarOffset, memberRef, whiteList);
            }
            case CONSTRUCTOR: {
                return new ConstructorItem(info, elem, type, substitutionOffset, isDeprecated, smartType, null, whiteList);
            }
        }
        throw new IllegalArgumentException("kind=" + (Object)((Object)elem.getKind()));
    }

    public static JavaCompletionItem createThisOrSuperConstructorItem(CompilationInfo info, ExecutableElement elem, ExecutableType type, int substitutionOffset, boolean isDeprecated, String name, WhiteListQuery.WhiteList whiteList) {
        if (elem.getKind() == ElementKind.CONSTRUCTOR) {
            return new ConstructorItem(info, elem, type, substitutionOffset, isDeprecated, false, name, whiteList);
        }
        throw new IllegalArgumentException("kind=" + (Object)((Object)elem.getKind()));
    }

    public static JavaCompletionItem createOverrideMethodItem(CompilationInfo info, ExecutableElement elem, ExecutableType type, int substitutionOffset, boolean implement, WhiteListQuery.WhiteList whiteList) {
        switch (elem.getKind()) {
            case METHOD: {
                return new OverrideMethodItem(info, elem, type, substitutionOffset, implement, whiteList);
            }
        }
        throw new IllegalArgumentException("kind=" + (Object)((Object)elem.getKind()));
    }

    public static JavaCompletionItem createGetterSetterMethodItem(CompilationInfo info, VariableElement elem, TypeMirror type, int substitutionOffset, String name, boolean setter) {
        switch (elem.getKind()) {
            case ENUM_CONSTANT: 
            case FIELD: {
                return new GetterSetterMethodItem(info, elem, type, substitutionOffset, name, setter);
            }
        }
        throw new IllegalArgumentException("kind=" + (Object)((Object)elem.getKind()));
    }

    public static JavaCompletionItem createDefaultConstructorItem(TypeElement elem, int substitutionOffset, boolean smartType) {
        return new DefaultConstructorItem(elem, substitutionOffset, smartType);
    }

    public static JavaCompletionItem createParametersItem(CompilationInfo info, ExecutableElement elem, ExecutableType type, int substitutionOffset, boolean isDeprecated, int activeParamIndex, String name) {
        return new ParametersItem(info, elem, type, substitutionOffset, isDeprecated, activeParamIndex, name);
    }

    public static JavaCompletionItem createAnnotationItem(CompilationInfo info, TypeElement elem, DeclaredType type, int substitutionOffset, ReferencesCount referencesCount, boolean isDeprecated, WhiteListQuery.WhiteList whiteList) {
        return new AnnotationItem(info, elem, type, substitutionOffset, referencesCount, isDeprecated, true, whiteList);
    }

    public static JavaCompletionItem createAttributeItem(CompilationInfo info, ExecutableElement elem, ExecutableType type, int substitutionOffset, boolean isDeprecated) {
        return new AttributeItem(info, elem, type, substitutionOffset, isDeprecated);
    }

    public static JavaCompletionItem createAttributeValueItem(CompilationInfo info, String value, String documentation, TypeElement element, int substitutionOffset, ReferencesCount referencesCount, WhiteListQuery.WhiteList whiteList) {
        return new AttributeValueItem(info, value, documentation, element, substitutionOffset, referencesCount, whiteList);
    }

    public static JavaCompletionItem createStaticMemberItem(CompilationInfo info, DeclaredType type, Element memberElem, TypeMirror memberType, boolean multipleVersions, int substitutionOffset, boolean isDeprecated, boolean addSemicolon, WhiteListQuery.WhiteList whiteList) {
        switch (memberElem.getKind()) {
            case ENUM_CONSTANT: 
            case FIELD: 
            case METHOD: {
                return new StaticMemberItem(info, type, memberElem, memberType, multipleVersions, substitutionOffset, isDeprecated, addSemicolon, whiteList);
            }
        }
        throw new IllegalArgumentException("kind=" + (Object)((Object)memberElem.getKind()));
    }

    public static JavaCompletionItem createChainedMembersItem(CompilationInfo info, List<? extends Element> chainedElems, List<? extends TypeMirror> chainedTypes, int substitutionOffset, boolean isDeprecated, boolean addSemicolon, WhiteListQuery.WhiteList whiteList) {
        return new ChainedMembersItem(info, chainedElems, chainedTypes, substitutionOffset, isDeprecated, addSemicolon, whiteList);
    }

    public static JavaCompletionItem createInitializeAllConstructorItem(CompilationInfo info, boolean isDefault, Iterable<? extends VariableElement> fields, ExecutableElement superConstructor, TypeElement parent, int substitutionOffset) {
        return new InitializeAllConstructorItem(info, isDefault, fields, superConstructor, parent, substitutionOffset);
    }

    protected JavaCompletionItem(int substitutionOffset) {
        this.substitutionOffset = substitutionOffset;
    }

    public void defaultAction(JTextComponent component) {
        if (component != null) {
            Completion.get().hideDocumentation();
            Completion.get().hideCompletion();
            this.process(component, '\u0000', false);
        }
    }

    public void processKeyEvent(KeyEvent evt) {
        if (evt.getID() == 400) {
            if (!(Utilities.autoPopupOnJavaIdentifierPart() && this instanceof VariableItem && ((VariableItem)this).newVarName || Utilities.getJavaCompletionSelectors().indexOf(evt.getKeyChar()) < 0 || ' ' == evt.getKeyChar() && (evt.getModifiers() & 2) != 0)) {
                if (!(evt.getKeyChar() != '(' || this instanceof AnnotationItem || this instanceof ConstructorItem || this instanceof DefaultConstructorItem || this instanceof MethodItem || this instanceof GetterSetterMethodItem || this instanceof InitializeAllConstructorItem || this instanceof OverrideMethodItem || this instanceof StaticMemberItem || this instanceof ChainedMembersItem)) {
                    return;
                }
                Completion.get().hideDocumentation();
                Completion.get().hideCompletion();
                this.process((JTextComponent)evt.getSource(), evt.getKeyChar(), false);
                if (Utilities.getJavaCompletionAutoPopupTriggers().indexOf(evt.getKeyChar()) >= 0) {
                    Completion.get().showCompletion();
                }
                evt.consume();
            }
        } else if (evt.getID() == 401 && evt.getKeyCode() == 10 && (evt.getModifiers() & 2) > 0) {
            JTextComponent component = (JTextComponent)evt.getSource();
            int caretOffset = component.getSelectionEnd();
            Document doc = component.getDocument();
            TokenSequence ts = SourceUtils.getJavaTokenSequence((TokenHierarchy)TokenHierarchy.get((Document)doc), (int)caretOffset);
            if (ts != null && (ts.moveNext() || ts.movePrevious()) && (ts.token().id() == JavaTokenId.IDENTIFIER || ((JavaTokenId)ts.token().id()).primaryCategory().startsWith("keyword") || ((JavaTokenId)ts.token().id()).primaryCategory().startsWith("string"))) {
                try {
                    doc.remove(caretOffset, ts.offset() + ts.token().length() - caretOffset);
                }
                catch (BadLocationException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        } else if (evt.getID() == 401 && evt.getKeyCode() == 10 && (evt.getModifiers() & 1) > 0) {
            JTextComponent component = (JTextComponent)evt.getSource();
            Completion.get().hideDocumentation();
            Completion.get().hideCompletion();
            this.process(component, '\u0000', true);
            evt.consume();
        }
    }

    public boolean instantSubstitution(JTextComponent component) {
        if (component != null) {
            try {
                int caretOffset = component.getSelectionEnd();
                if (caretOffset > this.substitutionOffset) {
                    String text = component.getDocument().getText(this.substitutionOffset, caretOffset - this.substitutionOffset);
                    if (!this.getInsertPrefix().toString().startsWith(text)) {
                        return false;
                    }
                }
            }
            catch (BadLocationException ble) {
                // empty catch block
            }
        }
        this.defaultAction(component);
        return true;
    }

    public CompletionTask createDocumentationTask() {
        return null;
    }

    public CompletionTask createToolTipTask() {
        return null;
    }

    public int getPreferredWidth(Graphics g, Font defaultFont) {
        return CompletionUtilities.getPreferredWidth((String)this.getLeftHtmlText(), (String)this.getRightHtmlText(), (Graphics)g, (Font)defaultFont);
    }

    public void render(Graphics g, Font defaultFont, Color defaultColor, Color backgroundColor, int width, int height, boolean selected) {
        CompletionUtilities.renderHtml((ImageIcon)this.getIcon(), (String)this.getLeftHtmlText(), (String)this.getRightHtmlText(), (Graphics)g, (Font)defaultFont, (Color)defaultColor, (int)width, (int)height, (boolean)selected);
    }

    protected ImageIcon getIcon() {
        return null;
    }

    protected String getLeftHtmlText() {
        return null;
    }

    protected String getRightHtmlText() {
        return null;
    }

    protected CharSequence getInsertPostfix(JTextComponent c) {
        return null;
    }

    protected int getAssignToVarOffset() {
        return -1;
    }

    protected CharSequence getAssignToVarText() {
        return null;
    }

    protected final void process(final JTextComponent c, char selector, boolean assignToVar) {
        int pos;
        final BaseDocument doc = (BaseDocument)c.getDocument();
        StringBuilder toAdd = new StringBuilder();
        CharSequence postfix = this.getInsertPostfix(c);
        if (postfix != null) {
            toAdd.append(postfix);
        }
        if (selector != '\u0000' && (toAdd.length() == 0 || selector != toAdd.charAt(0) && selector != toAdd.charAt(toAdd.length() - 1) && ';' != toAdd.charAt(toAdd.length() - 1))) {
            toAdd.append(selector);
            if ('[' == selector && TypingCompletion.isCompletionSettingEnabled()) {
                toAdd.append(']');
            }
        }
        int caretOffset = c.getSelectionEnd();
        Position assignToVarStartPos = null;
        Position assignToVarEndPos = null;
        if (assignToVar && this.getAssignToVarOffset() >= 0) {
            try {
                assignToVarStartPos = doc.createPosition(this.getAssignToVarOffset(), Position.Bias.Backward);
                assignToVarEndPos = doc.createPosition(caretOffset);
                if (toAdd.length() == 0 || ';' != toAdd.charAt(toAdd.length() - 1)) {
                    toAdd.append(';');
                }
            }
            catch (BadLocationException e) {
                // empty catch block
            }
        }
        Position semiPos = null;
        if (toAdd.length() > 0 && ';' == toAdd.charAt(toAdd.length() - 1) && (pos = JavaCompletionItem.findPositionForSemicolon(c)) > -2) {
            toAdd.deleteCharAt(toAdd.length() - 1);
            if (pos > -1) {
                try {
                    semiPos = doc.createPosition(pos);
                }
                catch (BadLocationException e) {
                    // empty catch block
                }
            }
        }
        CharSequence docText = DocumentUtilities.getText((Document)doc);
        int i = 0;
        int j = caretOffset;
        int length = j - this.substitutionOffset;
        if (toAdd.length() > 0) {
            boolean partialMatch = false;
            int taNL = -1;
            int docNL = -1;
            do {
                char taChar = '\u0000';
                while (i < toAdd.length() && (taChar = toAdd.charAt(i++)) <= ' ') {
                    if (taChar != '\n' || taNL >= 0) continue;
                    taNL = i - 1;
                }
                char docChar = '\u0000';
                while (j < docText.length() && (docChar = docText.charAt(j++)) <= ' ') {
                    if (docChar != '\n') continue;
                    if (taNL < 0) break;
                    if (docNL >= 0) continue;
                    docNL = j - 1;
                }
                if (taChar <= ' ' || docChar == '\n') {
                    length = j - this.substitutionOffset - (j <= docText.length() ? 1 : 0);
                    break;
                }
                if (taChar != docChar) {
                    if (!partialMatch) break;
                    if (docNL < 0) {
                        toAdd.delete(i - 1, toAdd.length());
                        length = j - this.substitutionOffset - (j <= docText.length() ? 1 : 0);
                        break;
                    }
                    toAdd.delete(taNL, toAdd.length());
                    length = docNL - this.substitutionOffset;
                    break;
                }
                partialMatch = true;
            } while (true);
        }
        CharSequence template = this.substituteText(c, this.substitutionOffset, length, this.getInsertPrefix(), toAdd);
        if (semiPos != null) {
            final Position finalSemiPos = semiPos;
            doc.runAtomic(new Runnable(){

                @Override
                public void run() {
                    try {
                        int cp = c.getCaretPosition();
                        doc.insertString(finalSemiPos.getOffset(), ";", null);
                        c.setCaretPosition(cp);
                    }
                    catch (BadLocationException e) {
                        // empty catch block
                    }
                }
            });
        }
        final StringBuilder sb = new StringBuilder();
        if (assignToVarStartPos != null && assignToVarEndPos != null) {
            sb.append(this.getAssignToVarText());
            final Position finalStartPos = assignToVarStartPos;
            final Position finalEndPos = assignToVarEndPos;
            doc.runAtomic(new Runnable(){

                @Override
                public void run() {
                    try {
                        sb.append(doc.getText(finalStartPos.getOffset(), finalEndPos.getOffset() - finalStartPos.getOffset()));
                        doc.remove(finalStartPos.getOffset(), finalEndPos.getOffset() - finalStartPos.getOffset());
                    }
                    catch (BadLocationException e) {
                        // empty catch block
                    }
                }
            });
            c.setCaretPosition(assignToVarStartPos.getOffset());
        }
        if (template != null) {
            sb.append(template);
        }
        if (sb.length() > 0) {
            CodeTemplateManager.get((Document)doc).createTemporary(sb.toString()).insert(c);
        }
    }

    protected CharSequence substituteText(final JTextComponent c, final int offset, final int length, CharSequence text, CharSequence toAdd) {
        final StringBuilder sb = new StringBuilder();
        if (text != null) {
            sb.append(text);
        }
        if (toAdd != null) {
            sb.append(toAdd);
        }
        final BaseDocument doc = (BaseDocument)c.getDocument();
        doc.runAtomic(new Runnable(){

            @Override
            public void run() {
                try {
                    String textToReplace = doc.getText(offset, length);
                    if (textToReplace.contentEquals(sb)) {
                        c.setCaretPosition(offset + length);
                    } else {
                        doc.remove(offset, length);
                        doc.insertString(offset, sb.toString(), null);
                    }
                }
                catch (BadLocationException e) {
                    // empty catch block
                }
            }
        });
        return null;
    }

    private static int getProtectionLevel(Set<Modifier> modifiers) {
        if (modifiers.contains((Object)Modifier.PUBLIC)) {
            return 3;
        }
        if (modifiers.contains((Object)Modifier.PROTECTED)) {
            return 2;
        }
        if (modifiers.contains((Object)Modifier.PRIVATE)) {
            return 0;
        }
        return 1;
    }

    private static String escape(String s) {
        if (s != null) {
            try {
                return XMLUtil.toAttributeValue((String)s);
            }
            catch (Exception ex) {
                // empty catch block
            }
        }
        return s;
    }

    private static int findPositionForSemicolon(JTextComponent c) {
        final int[] ret = new int[]{-2};
        final int offset = c.getSelectionEnd();
        final Source s = Source.create((Document)c.getDocument());
        final AtomicBoolean cancel = new AtomicBoolean();
        ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

            @Override
            public void run() {
                try {
                    ParserManager.parse(Collections.singletonList(s), (UserTask)new UserTask(){

                        public void run(ResultIterator resultIterator) throws Exception {
                            if (cancel.get()) {
                                return;
                            }
                            CompilationController controller = CompilationController.get((Parser.Result)resultIterator.getParserResult(offset));
                            controller.toPhase(JavaSource.Phase.PARSED);
                            if (cancel.get()) {
                                return;
                            }
                            int embeddedOffset = controller.getSnapshot().getEmbeddedOffset(offset);
                            Tree t = null;
                            boolean cont = true;
                            block6 : for (TreePath tp = controller.getTreeUtilities().pathFor((int)embeddedOffset); cont && tp != null; tp = tp.getParentPath()) {
                                switch (tp.getLeaf().getKind()) {
                                    case EXPRESSION_STATEMENT: 
                                    case VARIABLE: 
                                    case IMPORT: {
                                        t = tp.getLeaf();
                                        cont = false;
                                        continue block6;
                                    }
                                    case RETURN: {
                                        t = ((ReturnTree)tp.getLeaf()).getExpression();
                                        cont = false;
                                        continue block6;
                                    }
                                    case THROW: {
                                        t = ((ThrowTree)tp.getLeaf()).getExpression();
                                        cont = false;
                                        continue block6;
                                    }
                                    case MEMBER_SELECT: {
                                        cont = false;
                                    }
                                }
                            }
                            if (t != null) {
                                SourcePositions sp = controller.getTrees().getSourcePositions();
                                int endPos = (int)sp.getEndPosition(controller.getCompilationUnit(), t);
                                TokenSequence ts = JavaCompletionItem.findLastNonWhitespaceToken((CompilationInfo)controller, embeddedOffset, endPos);
                                if (ts != null) {
                                    ret[0] = ts.token().id() == JavaTokenId.SEMICOLON ? -1 : (ts.moveNext() ? (ts.token().id() == JavaTokenId.LINE_COMMENT || ts.token().id() == JavaTokenId.WHITESPACE && ts.token().text().toString().contains("\n") ? ts.offset() : offset) : ts.offset() + ts.token().length());
                                }
                            } else {
                                TokenSequence ts = controller.getTokenHierarchy().tokenSequence(JavaTokenId.language());
                                ts.move(embeddedOffset);
                                if (ts.moveNext() && ts.token().id() == JavaTokenId.SEMICOLON) {
                                    ret[0] = -1;
                                }
                            }
                        }
                    });
                }
                catch (ParseException ex) {
                    // empty catch block
                }
            }

        }, (String)NbBundle.getMessage(JavaCompletionItem.class, (String)"JCI-find_semicolon_pos"), (AtomicBoolean)cancel, (boolean)false);
        return ret[0];
    }

    private static TokenSequence<JavaTokenId> findLastNonWhitespaceToken(CompilationInfo info, int startPos, int endPos) {
        TokenSequence ts = info.getTokenHierarchy().tokenSequence(JavaTokenId.language());
        ts.move(endPos);
        block3 : while (ts.movePrevious()) {
            int offset = ts.offset();
            if (offset < startPos) {
                return null;
            }
            switch ((JavaTokenId)ts.token().id()) {
                case WHITESPACE: 
                case LINE_COMMENT: 
                case BLOCK_COMMENT: 
                case JAVADOC_COMMENT: {
                    continue block3;
                }
            }
            return ts;
        }
        return null;
    }

    private static CharSequence getIndent(JTextComponent c) {
        StringBuilder sb = new StringBuilder();
        try {
            Document doc = c.getDocument();
            CodeStyle cs = CodeStyle.getDefault((Document)doc);
            int indent = IndentUtils.lineIndent((Document)c.getDocument(), (int)IndentUtils.lineStartOffset((Document)c.getDocument(), (int)c.getCaretPosition()));
            switch (cs.getClassDeclBracePlacement()) {
                case SAME_LINE: {
                    indent = 1;
                    break;
                }
                case NEW_LINE: {
                    sb.append('\n');
                    break;
                }
                case NEW_LINE_HALF_INDENTED: {
                    sb.append('\n');
                    indent += cs.getIndentSize() / 2;
                    break;
                }
                case NEW_LINE_INDENTED: {
                    sb.append('\n');
                    indent += cs.getIndentSize();
                }
            }
            int tabSize = cs.getTabSize();
            int col = 0;
            if (!cs.expandTabToSpaces()) {
                while (col + tabSize <= indent) {
                    sb.append('\t');
                    col += tabSize;
                }
            }
            while (col < indent) {
                sb.append(' ');
                ++col;
            }
        }
        catch (BadLocationException ble) {
            // empty catch block
        }
        return sb;
    }

    private static CharSequence createAssignToVarText(CompilationInfo info, TypeMirror type, String name) {
        name = JavaCompletionItem.adjustName(name);
        StringBuilder sb = new StringBuilder();
        sb.append("${TYPE type=\"");
        sb.append(Utilities.getTypeName(info, type, true));
        sb.append("\" default=\"");
        sb.append(Utilities.getTypeName(info, type, false));
        sb.append("\" editable=false}");
        sb.append(" ${NAME newVarName=\"");
        sb.append(name);
        sb.append("\" default=\"");
        sb.append(name);
        sb.append("\"} = ");
        return sb;
    }

    private static String adjustName(String name) {
        if (name == null) {
            return null;
        }
        String shortName = null;
        if (name.startsWith("get") && name.length() > 3) {
            shortName = name.substring(3);
        }
        if (name.startsWith("is") && name.length() > 2) {
            shortName = name.substring(2);
        }
        if (shortName != null) {
            return JavaCompletionItem.firstToLower(shortName);
        }
        if (SourceVersion.isKeyword(name)) {
            return "a" + Character.toUpperCase(name.charAt(0)) + name.substring(1);
        }
        return name;
    }

    private static String firstToLower(String name) {
        if (name.length() == 0) {
            return null;
        }
        StringBuilder result = new StringBuilder();
        boolean toLower = true;
        char last = Character.toLowerCase(name.charAt(0));
        for (int i = 1; i < name.length(); ++i) {
            if (toLower && Character.isUpperCase(name.charAt(i))) {
                result.append(Character.toLowerCase(last));
            } else {
                result.append(last);
                toLower = false;
            }
            last = name.charAt(i);
        }
        result.append(last);
        if (SourceVersion.isKeyword(result)) {
            return "a" + name;
        }
        return result.toString();
    }

    static class ParamDesc {
        private final String fullTypeName;
        private final String typeName;
        private String name;

        public ParamDesc(String fullTypeName, String typeName, String name) {
            this.fullTypeName = fullTypeName;
            this.typeName = typeName;
            this.name = name;
        }
    }

    static class MemberDesc {
        private final ElementKind kind;
        private final String name;
        private final List<ParamDesc> params;

        public MemberDesc(ElementKind kind, String name, List<ParamDesc> params) {
            this.kind = kind;
            this.name = name;
            this.params = params;
        }
    }

    static class InitializeAllConstructorItem
    extends JavaCompletionItem {
        private static final String CONSTRUCTOR_PUBLIC = "org/netbeans/modules/java/editor/resources/new_constructor_16.png";
        private static final String CONSTRUCTOR_COLOR = Utilities.getHTMLColor(242, 203, 64);
        private static final String PARAMETER_NAME_COLOR = Utilities.getHTMLColor(242, 64, 242);
        private static ImageIcon icon;
        private boolean isDefault;
        private List<ElementHandle<VariableElement>> fieldHandles;
        private ElementHandle<TypeElement> parentHandle;
        private ElementHandle<ExecutableElement> superConstructorHandle;
        private String simpleName;
        private List<ParamDesc> params;
        private String sortText;
        private String leftText;

        private InitializeAllConstructorItem(CompilationInfo info, boolean isDefault, Iterable<? extends VariableElement> fields, ExecutableElement superConstructor, TypeElement parent, int substitutionOffset) {
            super(substitutionOffset);
            CodeStyle cs = null;
            try {
                cs = CodeStyle.getDefault((Document)info.getDocument());
            }
            catch (IOException ex) {
                // empty catch block
            }
            if (cs == null) {
                cs = CodeStyle.getDefault((FileObject)info.getFileObject());
            }
            this.isDefault = isDefault;
            this.fieldHandles = new ArrayList<ElementHandle<VariableElement>>();
            this.parentHandle = ElementHandle.create((Element)parent);
            this.params = new ArrayList<ParamDesc>();
            for (VariableElement ve2 : fields) {
                boolean isStatic;
                this.fieldHandles.add((ElementHandle)ElementHandle.create((Element)ve2));
                if (isDefault) continue;
                String sName = CodeStyleUtils.removePrefixSuffix((CharSequence)ve2.getSimpleName(), (String)((isStatic = ve2.getModifiers().contains((Object)Modifier.STATIC)) ? cs.getStaticFieldNamePrefix() : cs.getFieldNamePrefix()), (String)(isStatic ? cs.getStaticFieldNameSuffix() : cs.getFieldNameSuffix()));
                sName = CodeStyleUtils.addPrefixSuffix((CharSequence)sName, (String)cs.getParameterNamePrefix(), (String)cs.getParameterNameSuffix());
                this.params.add(new ParamDesc(null, Utilities.getTypeName(info, ve2.asType(), false).toString(), sName));
            }
            if (superConstructor != null) {
                this.superConstructorHandle = ElementHandle.create((Element)superConstructor);
                if (!isDefault) {
                    for (VariableElement ve : superConstructor.getParameters()) {
                        String sName = CodeStyleUtils.removePrefixSuffix((CharSequence)ve.getSimpleName(), (String)cs.getParameterNamePrefix(), (String)cs.getParameterNameSuffix());
                        sName = CodeStyleUtils.addPrefixSuffix((CharSequence)sName, (String)cs.getParameterNamePrefix(), (String)cs.getParameterNameSuffix());
                        this.params.add(new ParamDesc(null, Utilities.getTypeName(info, ve.asType(), false).toString(), sName));
                    }
                }
            } else {
                this.superConstructorHandle = null;
            }
            this.simpleName = parent.getSimpleName().toString();
        }

        public int getSortPriority() {
            return 400;
        }

        public CharSequence getSortText() {
            if (this.sortText == null) {
                StringBuilder sortParams = new StringBuilder();
                sortParams.append('(');
                int cnt = 0;
                Iterator<ParamDesc> it = this.params.iterator();
                while (it.hasNext()) {
                    ParamDesc paramDesc = it.next();
                    sortParams.append(paramDesc.typeName);
                    if (it.hasNext()) {
                        sortParams.append(',');
                    }
                    ++cnt;
                }
                sortParams.append(')');
                this.sortText = this.simpleName + "#" + (cnt < 10 ? "0" : "") + cnt + "#" + sortParams.toString();
            }
            return this.sortText;
        }

        @Override
        protected String getLeftHtmlText() {
            if (this.leftText == null) {
                StringBuilder lText = new StringBuilder();
                lText.append(CONSTRUCTOR_COLOR);
                lText.append(this.simpleName);
                lText.append("</font>");
                lText.append('(');
                Iterator<ParamDesc> it = this.params.iterator();
                while (it.hasNext()) {
                    ParamDesc paramDesc = it.next();
                    lText.append(JavaCompletionItem.escape(paramDesc.typeName));
                    lText.append(' ');
                    lText.append(PARAMETER_NAME_COLOR);
                    lText.append(paramDesc.name);
                    lText.append("</font>");
                    if (!it.hasNext()) continue;
                    lText.append(", ");
                }
                lText.append(") - ");
                lText.append(GENERATE_TEXT);
                this.leftText = lText.toString();
            }
            return this.leftText;
        }

        @Override
        protected ImageIcon getIcon() {
            if (icon == null) {
                icon = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/java/editor/resources/new_constructor_16.png", (boolean)false);
            }
            return icon;
        }

        public CharSequence getInsertPrefix() {
            return this.simpleName;
        }

        @Override
        protected CharSequence substituteText(JTextComponent c, int offset, int length, CharSequence text, CharSequence toAdd) {
            Position pos;
            BaseDocument doc = (BaseDocument)c.getDocument();
            try {
                pos = doc.createPosition(offset);
            }
            catch (BadLocationException e) {
                return null;
            }
            CharSequence cs = super.substituteText(c, offset, length, null, null);
            try {
                ModificationResult mr = ModificationResult.runModificationTask(Collections.singletonList(Source.create((Document)doc)), (UserTask)new UserTask(){

                    public void run(ResultIterator resultIterator) throws Exception {
                        TypeElement parent;
                        WorkingCopy copy = WorkingCopy.get((Parser.Result)resultIterator.getParserResult());
                        copy.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                        int embeddedOffset = copy.getSnapshot().getEmbeddedOffset(pos.getOffset());
                        TreePath tp = copy.getTreeUtilities().pathFor(embeddedOffset);
                        if (TreeUtilities.CLASS_TREE_KINDS.contains((Object)tp.getLeaf().getKind()) && (parent = (TypeElement)InitializeAllConstructorItem.this.parentHandle.resolve((CompilationInfo)copy)) != null && parent == copy.getTrees().getElement(tp)) {
                            ArrayList<VariableElement> fieldElements = new ArrayList<VariableElement>();
                            for (ElementHandle handle : InitializeAllConstructorItem.this.fieldHandles) {
                                Element fieldElement = handle.resolve((CompilationInfo)copy);
                                if (fieldElement == null || !fieldElement.getKind().isField()) continue;
                                fieldElements.add((VariableElement)fieldElement);
                            }
                            ExecutableElement superConstructor = InitializeAllConstructorItem.this.superConstructorHandle != null ? (ExecutableElement)InitializeAllConstructorItem.this.superConstructorHandle.resolve((CompilationInfo)copy) : null;
                            ClassTree clazz = (ClassTree)tp.getLeaf();
                            GeneratorUtilities gu = GeneratorUtilities.get((WorkingCopy)copy);
                            MethodTree ctor = InitializeAllConstructorItem.this.isDefault ? gu.createDefaultConstructor(parent, fieldElements, superConstructor) : gu.createConstructor(parent, fieldElements, superConstructor);
                            ClassTree decl = GeneratorUtils.insertClassMember(copy, clazz, (Tree)ctor, embeddedOffset);
                            copy.rewrite((Tree)clazz, (Tree)decl);
                        }
                    }
                });
                GeneratorUtils.guardedCommit(c, mr);
            }
            catch (Exception ex) {
                LOGGER.log(Level.FINE, null, ex);
            }
            return cs;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("public ");
            sb.append(this.simpleName);
            sb.append('(');
            Iterator<ParamDesc> it = this.params.iterator();
            while (it.hasNext()) {
                ParamDesc paramDesc = it.next();
                sb.append(paramDesc.typeName);
                sb.append(' ');
                sb.append(paramDesc.name);
                if (!it.hasNext()) continue;
                sb.append(", ");
            }
            sb.append(") - ");
            sb.append(GENERATE_TEXT);
            return sb.toString();
        }

        @Override
        public boolean instantSubstitution(JTextComponent component) {
            return false;
        }

    }

    static class ChainedMembersItem
    extends WhiteListJavaCompletionItem<Element> {
        private static final String FIELD_ST_PUBLIC = "org/netbeans/modules/editor/resources/completion/field_static_16.png";
        private static final String FIELD_ST_PROTECTED = "org/netbeans/modules/editor/resources/completion/field_static_protected_16.png";
        private static final String FIELD_ST_PACKAGE = "org/netbeans/modules/editor/resources/completion/field_static_package_private_16.png";
        private static final String FIELD_ST_PRIVATE = "org/netbeans/modules/editor/resources/completion/field_static_private_16.png";
        private static final String FIELD_COLOR = Utilities.getHTMLColor(64, 64, 242);
        private static final String METHOD_ST_PUBLIC = "org/netbeans/modules/editor/resources/completion/method_static_16.png";
        private static final String METHOD_ST_PROTECTED = "org/netbeans/modules/editor/resources/completion/method_static_protected_16.png";
        private static final String METHOD_ST_PACKAGE = "org/netbeans/modules/editor/resources/completion/method_static_package_private_16.png";
        private static final String METHOD_ST_PRIVATE = "org/netbeans/modules/editor/resources/completion/method_static_private_16.png";
        private static final String METHOD_COLOR = Utilities.getHTMLColor(188, 64, 64);
        private static final String PARAMETER_NAME_COLOR = Utilities.getHTMLColor(242, 64, 242);
        private static ImageIcon[][] icon = new ImageIcon[2][4];
        private List<MemberDesc> members;
        private String firstMemberName;
        private String lastMemberTypeName;
        private boolean isLastMethod;
        private boolean isDeprecated;
        private boolean addSemicolon;
        private Set<Modifier> modifiers;
        private String sortText;
        private String leftText;
        private String rightText;

        private ChainedMembersItem(CompilationInfo info, List<? extends Element> chainedElems, List<? extends TypeMirror> chainedTypes, int substitutionOffset, boolean isDeprecated, boolean addSemicolon, WhiteListQuery.WhiteList whiteList) {
            super(substitutionOffset, chainedElems, whiteList);
            assert (chainedElems.size() == chainedTypes.size());
            this.isDeprecated = isDeprecated;
            this.members = new ArrayList<MemberDesc>(chainedElems.size());
            Element lastMemberElem = null;
            TypeMirror lastMemberType = null;
            Iterator<? extends TypeMirror> typesIt = chainedTypes.iterator();
            for (Element element : chainedElems) {
                TypeMirror type = typesIt.next();
                String elementName = element.getSimpleName().toString();
                if (this.firstMemberName == null) {
                    this.firstMemberName = elementName;
                }
                ArrayList<ParamDesc> params = null;
                if (element.getKind() == ElementKind.METHOD) {
                    params = new ArrayList<ParamDesc>();
                    Iterator<? extends VariableElement> it = ((ExecutableElement)element).getParameters().iterator();
                    Iterator<? extends TypeMirror> tIt = ((ExecutableType)type).getParameterTypes().iterator();
                    while (it.hasNext() && tIt.hasNext()) {
                        TypeMirror tm = tIt.next();
                        params.add(new ParamDesc(tm.toString(), Utilities.getTypeName(info, tm, false, ((ExecutableElement)element).isVarArgs() && !tIt.hasNext()).toString(), it.next().getSimpleName().toString()));
                    }
                }
                this.members.add(new MemberDesc(element.getKind(), elementName, params));
                lastMemberElem = element;
                lastMemberType = type;
            }
            this.isLastMethod = lastMemberElem.getKind() == ElementKind.METHOD;
            TypeMirror mtm = this.isLastMethod ? ((ExecutableType)lastMemberType).getReturnType() : lastMemberType;
            this.lastMemberTypeName = Utilities.getTypeName(info, mtm, false).toString();
            this.addSemicolon = addSemicolon && mtm.getKind() == TypeKind.VOID;
            this.modifiers = lastMemberElem.getModifiers();
        }

        public int getSortPriority() {
            return (this.getElementHandle().getKind().isField() ? 710 : 740) - SMART_TYPE;
        }

        public CharSequence getSortText() {
            if (this.sortText == null) {
                StringBuilder sb = new StringBuilder();
                Iterator<MemberDesc> membersIt = this.members.iterator();
                while (membersIt.hasNext()) {
                    MemberDesc member = membersIt.next();
                    sb.append(member.name);
                    if (member.params != null) {
                        StringBuilder sortParams = new StringBuilder();
                        sortParams.append("#(");
                        int cnt = 0;
                        Iterator paramsIt = member.params.iterator();
                        while (paramsIt.hasNext()) {
                            ParamDesc paramDesc = (ParamDesc)paramsIt.next();
                            sortParams.append(paramDesc.typeName);
                            if (paramsIt.hasNext()) {
                                sortParams.append(',');
                            }
                            ++cnt;
                        }
                        sortParams.append(')');
                        sb.append(cnt < 10 ? "0" : "").append(cnt).append("#").append(sortParams);
                    }
                    if (!membersIt.hasNext()) continue;
                    sb.append('#');
                }
                this.sortText = sb.toString();
            }
            return this.sortText;
        }

        public CharSequence getInsertPrefix() {
            return this.firstMemberName;
        }

        @Override
        protected CharSequence getInsertPostfix(JTextComponent c) {
            StringBuilder sb = new StringBuilder();
            if (this.isLastMethod) {
                sb.append(CodeStyle.getDefault((Document)c.getDocument()).spaceBeforeMethodCallParen() ? " ()" : "()");
            }
            if (this.addSemicolon) {
                sb.append(';');
            }
            return sb;
        }

        @Override
        public CompletionTask createDocumentationTask() {
            return JavaCompletionProvider.createDocTask(this.getElementHandle());
        }

        @Override
        protected String getLeftHtmlText() {
            if (this.leftText == null) {
                StringBuilder lText = new StringBuilder();
                Iterator<MemberDesc> membersIt = this.members.iterator();
                while (membersIt.hasNext()) {
                    MemberDesc member = membersIt.next();
                    lText.append(member.kind == ElementKind.METHOD ? METHOD_COLOR : (member.kind.isField() ? FIELD_COLOR : PARAMETER_NAME_COLOR));
                    if (this.isDeprecated || this.isBlackListed()) {
                        lText.append("<s>");
                    }
                    lText.append(member.name);
                    if (this.isDeprecated || this.isBlackListed()) {
                        lText.append("</s>");
                    }
                    lText.append("</font>");
                    if (member.params != null) {
                        lText.append('(');
                        Iterator paramsIt = member.params.iterator();
                        while (paramsIt.hasNext()) {
                            ParamDesc paramDesc = (ParamDesc)paramsIt.next();
                            lText.append(JavaCompletionItem.escape(paramDesc.typeName));
                            lText.append(' ');
                            lText.append(PARAMETER_NAME_COLOR);
                            lText.append(paramDesc.name);
                            lText.append("</font>");
                            if (!paramsIt.hasNext()) continue;
                            lText.append(", ");
                        }
                        lText.append(')');
                    }
                    if (!membersIt.hasNext()) continue;
                    lText.append('.');
                }
                this.leftText = lText.toString();
            }
            return this.leftText;
        }

        @Override
        protected String getRightHtmlText() {
            if (this.rightText == null) {
                this.rightText = JavaCompletionItem.escape(this.lastMemberTypeName);
            }
            return this.rightText;
        }

        @Override
        protected ImageIcon getBaseIcon() {
            int level = JavaCompletionItem.getProtectionLevel(this.modifiers);
            boolean isField = this.getElementHandle().getKind().isField();
            ImageIcon cachedIcon = icon[isField ? 0 : 1][level];
            if (cachedIcon != null) {
                return cachedIcon;
            }
            String iconPath = null;
            if (isField) {
                switch (level) {
                    case 0: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/field_static_private_16.png";
                        break;
                    }
                    case 1: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/field_static_package_private_16.png";
                        break;
                    }
                    case 2: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/field_static_protected_16.png";
                        break;
                    }
                    case 3: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/field_static_16.png";
                    }
                }
            } else {
                switch (level) {
                    case 0: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/method_static_private_16.png";
                        break;
                    }
                    case 1: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/method_static_package_private_16.png";
                        break;
                    }
                    case 2: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/method_static_protected_16.png";
                        break;
                    }
                    case 3: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/method_static_16.png";
                    }
                }
            }
            if (iconPath == null) {
                return null;
            }
            ImageIcon newIcon = ImageUtilities.loadImageIcon((String)iconPath, (boolean)false);
            ChainedMembersItem.icon[isField != false ? 0 : 1][level] = newIcon;
            return newIcon;
        }

        @Override
        protected CharSequence substituteText(JTextComponent c, int offset, int length, CharSequence text, CharSequence toAdd) {
            String toAddText;
            int idx;
            if (toAdd.length() > 0 && toAdd.charAt(toAdd.length() - 1) == '.') {
                if (this.firstMemberName.length() == length) {
                    return WhiteListJavaCompletionItem.super.substituteText(c, offset + length, 0, ".", null);
                }
                toAdd = toAdd.subSequence(0, toAdd.length() - 1);
            }
            StringBuilder sb = new StringBuilder();
            boolean asTemplate = false;
            Iterator<MemberDesc> membersIt = this.members.iterator();
            while (membersIt.hasNext()) {
                MemberDesc member = membersIt.next();
                sb.append(member.name);
                if (member.params != null && (asTemplate || membersIt.hasNext() || !member.params.isEmpty())) {
                    sb.append(CodeStyle.getDefault((Document)c.getDocument()).spaceBeforeMethodCallParen() ? " (" : "(");
                    boolean guessArgs = Utilities.guessMethodArguments();
                    Iterator paramsIt = member.params.iterator();
                    while (paramsIt.hasNext()) {
                        ParamDesc paramDesc = (ParamDesc)paramsIt.next();
                        sb.append("${");
                        sb.append(paramDesc.name);
                        if (guessArgs) {
                            sb.append(" named instanceof=\"");
                            sb.append(paramDesc.fullTypeName);
                            sb.append("\"");
                        }
                        sb.append('}');
                        if (paramsIt.hasNext()) {
                            sb.append(", ");
                        }
                        asTemplate = true;
                    }
                    sb.append(')');
                }
                if (!membersIt.hasNext()) continue;
                sb.append('.');
            }
            if (!asTemplate) {
                return WhiteListJavaCompletionItem.super.substituteText(c, offset, length, sb, toAdd);
            }
            if (toAdd != null && (idx = (toAddText = toAdd.toString()).indexOf(41)) > 0 && toAddText.length() > idx + 1) {
                sb.append(toAddText.substring(idx + 1));
            }
            WhiteListJavaCompletionItem.super.substituteText(c, offset, length, null, null);
            return sb;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            for (Modifier mod : this.modifiers) {
                sb.append(mod.toString());
                sb.append(' ');
            }
            Iterator<MemberDesc> membersIt = this.members.iterator();
            while (membersIt.hasNext()) {
                MemberDesc member = membersIt.next();
                sb.append(member.name);
                if (member.params != null) {
                    sb.append('(');
                    Iterator paramsIt = member.params.iterator();
                    while (paramsIt.hasNext()) {
                        ParamDesc paramDesc = (ParamDesc)paramsIt.next();
                        sb.append(JavaCompletionItem.escape(paramDesc.typeName));
                        sb.append(' ');
                        sb.append(paramDesc.name);
                        if (!paramsIt.hasNext()) continue;
                        sb.append(", ");
                    }
                    sb.append(')');
                }
                if (!membersIt.hasNext()) continue;
                sb.append('.');
            }
            return sb.toString();
        }
    }

    static class StaticMemberItem
    extends WhiteListJavaCompletionItem<Element> {
        private static final String FIELD_ST_PUBLIC = "org/netbeans/modules/editor/resources/completion/field_static_16.png";
        private static final String FIELD_ST_PROTECTED = "org/netbeans/modules/editor/resources/completion/field_static_protected_16.png";
        private static final String FIELD_ST_PACKAGE = "org/netbeans/modules/editor/resources/completion/field_static_package_private_16.png";
        private static final String FIELD_ST_PRIVATE = "org/netbeans/modules/editor/resources/completion/field_static_private_16.png";
        private static final String FIELD_COLOR = Utilities.getHTMLColor(64, 64, 242);
        private static final String METHOD_ST_PUBLIC = "org/netbeans/modules/editor/resources/completion/method_static_16.png";
        private static final String METHOD_ST_PROTECTED = "org/netbeans/modules/editor/resources/completion/method_static_protected_16.png";
        private static final String METHOD_ST_PACKAGE = "org/netbeans/modules/editor/resources/completion/method_static_package_private_16.png";
        private static final String METHOD_ST_PRIVATE = "org/netbeans/modules/editor/resources/completion/method_static_private_16.png";
        private static final String METHOD_COLOR = Utilities.getHTMLColor(188, 64, 64);
        private static final String PARAMETER_NAME_COLOR = Utilities.getHTMLColor(242, 64, 242);
        private static ImageIcon[][] icon = new ImageIcon[2][4];
        private TypeMirrorHandle<DeclaredType> typeHandle;
        private boolean isDeprecated;
        private String typeName;
        private String memberName;
        private String memberTypeName;
        private boolean addSemicolon;
        private Set<Modifier> modifiers;
        private List<ParamDesc> params;
        private String sortText;
        private String leftText;
        private String rightText;

        private StaticMemberItem(CompilationInfo info, DeclaredType type, Element memberElem, TypeMirror memberType, boolean multipleVersions, int substitutionOffset, boolean isDeprecated, boolean addSemicolon, WhiteListQuery.WhiteList whiteList) {
            super(substitutionOffset, ElementHandle.create((Element)memberElem), whiteList);
            type = (DeclaredType)info.getTypes().erasure(type);
            this.typeHandle = TypeMirrorHandle.create((TypeMirror)type);
            this.isDeprecated = isDeprecated;
            this.typeName = Utilities.getTypeName(info, type, false).toString();
            this.memberName = memberElem.getSimpleName().toString();
            TypeMirror mtm = memberElem.getKind().isField() ? memberType : ((ExecutableType)memberType).getReturnType();
            this.memberTypeName = Utilities.getTypeName(info, mtm, false).toString();
            this.addSemicolon = addSemicolon && mtm.getKind() == TypeKind.VOID;
            this.modifiers = memberElem.getModifiers();
            if (!memberElem.getKind().isField() && !multipleVersions) {
                this.params = new ArrayList<ParamDesc>();
                Iterator<? extends VariableElement> it = ((ExecutableElement)memberElem).getParameters().iterator();
                Iterator<? extends TypeMirror> tIt = ((ExecutableType)memberType).getParameterTypes().iterator();
                while (it.hasNext() && tIt.hasNext()) {
                    TypeMirror tm = tIt.next();
                    this.params.add(new ParamDesc(tm.toString(), Utilities.getTypeName(info, tm, false, ((ExecutableElement)memberElem).isVarArgs() && !tIt.hasNext()).toString(), it.next().getSimpleName().toString()));
                }
            }
        }

        public int getSortPriority() {
            return (this.getElementHandle().getKind().isField() ? 720 : 750) - SMART_TYPE;
        }

        public CharSequence getSortText() {
            if (this.sortText == null) {
                if (this.getElementHandle().getKind().isField()) {
                    this.sortText = this.memberName + "#" + this.typeName;
                } else {
                    StringBuilder sortParams = new StringBuilder();
                    sortParams.append('(');
                    int cnt = 0;
                    if (this.params == null) {
                        sortParams.append("...");
                    } else {
                        Iterator<ParamDesc> it = this.params.iterator();
                        while (it.hasNext()) {
                            ParamDesc paramDesc = it.next();
                            sortParams.append(paramDesc.typeName);
                            if (it.hasNext()) {
                                sortParams.append(',');
                            }
                            ++cnt;
                        }
                    }
                    sortParams.append(')');
                    this.sortText = this.memberName + "#" + (cnt < 10 ? "0" : "") + cnt + "#" + sortParams.toString() + "#" + this.typeName;
                }
            }
            return this.sortText;
        }

        public CharSequence getInsertPrefix() {
            return this.typeName + "." + this.memberName;
        }

        @Override
        protected CharSequence getInsertPostfix(JTextComponent c) {
            return this.addSemicolon ? new StringBuilder().append(';') : null;
        }

        @Override
        public CompletionTask createDocumentationTask() {
            return JavaCompletionProvider.createDocTask(this.getElementHandle());
        }

        @Override
        protected String getLeftHtmlText() {
            if (this.leftText == null) {
                StringBuilder lText = new StringBuilder();
                lText.append(this.getElementHandle().getKind().isField() ? FIELD_COLOR : METHOD_COLOR);
                lText.append(JavaCompletionItem.escape(this.typeName));
                lText.append('.');
                if (this.isDeprecated || this.isBlackListed()) {
                    lText.append("<s>");
                }
                lText.append(this.memberName);
                if (this.isDeprecated || this.isBlackListed()) {
                    lText.append("</s>");
                }
                lText.append("</font>");
                if (!this.getElementHandle().getKind().isField()) {
                    lText.append('(');
                    if (this.params == null) {
                        lText.append("...");
                    } else {
                        Iterator<ParamDesc> it = this.params.iterator();
                        while (it.hasNext()) {
                            ParamDesc paramDesc = it.next();
                            lText.append(JavaCompletionItem.escape(paramDesc.typeName));
                            lText.append(' ');
                            lText.append(PARAMETER_NAME_COLOR);
                            lText.append(paramDesc.name);
                            lText.append("</font>");
                            if (!it.hasNext()) continue;
                            lText.append(", ");
                        }
                    }
                    lText.append(')');
                }
                this.leftText = lText.toString();
            }
            return this.leftText;
        }

        @Override
        protected String getRightHtmlText() {
            if (this.rightText == null) {
                this.rightText = JavaCompletionItem.escape(this.memberTypeName);
            }
            return this.rightText;
        }

        @Override
        protected ImageIcon getBaseIcon() {
            int level = JavaCompletionItem.getProtectionLevel(this.modifiers);
            boolean isField = this.getElementHandle().getKind().isField();
            ImageIcon cachedIcon = icon[isField ? 0 : 1][level];
            if (cachedIcon != null) {
                return cachedIcon;
            }
            String iconPath = null;
            if (isField) {
                switch (level) {
                    case 0: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/field_static_private_16.png";
                        break;
                    }
                    case 1: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/field_static_package_private_16.png";
                        break;
                    }
                    case 2: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/field_static_protected_16.png";
                        break;
                    }
                    case 3: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/field_static_16.png";
                    }
                }
            } else {
                switch (level) {
                    case 0: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/method_static_private_16.png";
                        break;
                    }
                    case 1: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/method_static_package_private_16.png";
                        break;
                    }
                    case 2: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/method_static_protected_16.png";
                        break;
                    }
                    case 3: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/method_static_16.png";
                    }
                }
            }
            if (iconPath == null) {
                return null;
            }
            ImageIcon newIcon = ImageUtilities.loadImageIcon((String)iconPath, (boolean)false);
            StaticMemberItem.icon[isField != false ? 0 : 1][level] = newIcon;
            return newIcon;
        }

        @Override
        protected CharSequence substituteText(JTextComponent c, final int offset, int length, CharSequence text, CharSequence toAdd) {
            if (toAdd.length() > 0 && toAdd.charAt(toAdd.length() - 1) == '.') {
                if (this.typeName.length() == length) {
                    return WhiteListJavaCompletionItem.super.substituteText(c, offset + length, 0, ".", null);
                }
                toAdd = toAdd.subSequence(0, toAdd.length() - 1);
            }
            WhiteListJavaCompletionItem.super.substituteText(c, offset, length, null, null);
            final BaseDocument doc = (BaseDocument)c.getDocument();
            final StringBuilder template = new StringBuilder();
            final AtomicBoolean cancel = new AtomicBoolean();
            final CharSequence finalToAdd = toAdd;
            ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

                @Override
                public void run() {
                    try {
                        ModificationResult.runModificationTask(Collections.singletonList(Source.create((Document)doc)), (UserTask)new UserTask(){

                            public void run(ResultIterator resultIterator) throws Exception {
                                if (cancel.get()) {
                                    return;
                                }
                                WorkingCopy copy = WorkingCopy.get((Parser.Result)resultIterator.getParserResult(offset));
                                copy.toPhase(JavaSource.Phase.RESOLVED);
                                if (cancel.get()) {
                                    return;
                                }
                                if (CodeStyle.getDefault((Document)doc).preferStaticImports()) {
                                    Element e = StaticMemberItem.this.getElementHandle().resolve((CompilationInfo)copy);
                                    if (e != null) {
                                        copy.rewrite((Tree)copy.getCompilationUnit(), (Tree)GeneratorUtilities.get((WorkingCopy)copy).addImports(copy.getCompilationUnit(), Collections.singleton(e)));
                                    }
                                } else {
                                    DeclaredType type = (DeclaredType)StaticMemberItem.this.typeHandle.resolve((CompilationInfo)copy);
                                    int cnt = 1;
                                    template.append("${PAR#");
                                    template.append(cnt++);
                                    template.append(" type=\"");
                                    template.append(((TypeElement)type.asElement()).getQualifiedName());
                                    template.append("\" default=\"");
                                    template.append(((TypeElement)type.asElement()).getSimpleName());
                                    template.append("\" editable=false}");
                                    Iterator<? extends TypeMirror> tas = type.getTypeArguments().iterator();
                                    if (tas.hasNext()) {
                                        template.append('<');
                                        while (tas.hasNext()) {
                                            TypeMirror ta = tas.next();
                                            template.append("${PAR#");
                                            template.append(cnt++);
                                            if (ta.getKind() == TypeKind.TYPEVAR) {
                                                template.append(" type=\"");
                                                ta = ((TypeVariable)ta).getUpperBound();
                                                template.append(Utilities.getTypeName((CompilationInfo)copy, ta, true));
                                                template.append("\" default=\"");
                                                template.append(Utilities.getTypeName((CompilationInfo)copy, ta, false));
                                                template.append("\"}");
                                            } else if (ta.getKind() == TypeKind.WILDCARD) {
                                                template.append(" type=\"");
                                                TypeMirror bound = ((WildcardType)ta).getExtendsBound();
                                                if (bound == null) {
                                                    bound = ((WildcardType)ta).getSuperBound();
                                                }
                                                template.append(bound != null ? Utilities.getTypeName((CompilationInfo)copy, bound, true) : "Object");
                                                template.append("\" default=\"");
                                                template.append(bound != null ? Utilities.getTypeName((CompilationInfo)copy, bound, false) : "Object");
                                                template.append("\"}");
                                            } else if (ta.getKind() == TypeKind.ERROR) {
                                                template.append(" default=\"");
                                                template.append(((ErrorType)ta).asElement().getSimpleName());
                                                template.append("\"}");
                                            } else {
                                                template.append(" type=\"");
                                                template.append(Utilities.getTypeName((CompilationInfo)copy, ta, true));
                                                template.append("\" default=\"");
                                                template.append(Utilities.getTypeName((CompilationInfo)copy, ta, false));
                                                template.append("\" editable=false}");
                                            }
                                            if (!tas.hasNext()) continue;
                                            template.append(", ");
                                        }
                                        template.append('>');
                                    }
                                    template.append('.');
                                }
                                template.append(StaticMemberItem.this.memberName);
                                if (!StaticMemberItem.this.getElementHandle().getKind().isField()) {
                                    template.append("(");
                                    if (StaticMemberItem.this.params == null) {
                                        template.append("${cursor completionInvoke}");
                                    } else {
                                        boolean guessArgs = Utilities.guessMethodArguments();
                                        Iterator it = StaticMemberItem.this.params.iterator();
                                        while (it.hasNext()) {
                                            ParamDesc paramDesc = (ParamDesc)it.next();
                                            template.append("${");
                                            template.append(paramDesc.name);
                                            if (guessArgs) {
                                                template.append(" named instanceof=\"");
                                                template.append(paramDesc.fullTypeName);
                                                template.append("\"");
                                            }
                                            template.append("}");
                                            if (!it.hasNext()) continue;
                                            template.append(", ");
                                        }
                                    }
                                    template.append(")");
                                }
                                if (finalToAdd != null) {
                                    template.append(finalToAdd);
                                }
                            }
                        }).commit();
                    }
                    catch (Exception ex) {
                        // empty catch block
                    }
                }

            }, (String)NbBundle.getMessage(JavaCompletionItem.class, (String)"JCI-import_resolve"), (AtomicBoolean)cancel, (boolean)false);
            return template;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            for (Modifier mod : this.modifiers) {
                sb.append(mod.toString());
                sb.append(' ');
            }
            sb.append(this.memberTypeName);
            sb.append(' ');
            sb.append(this.typeName);
            sb.append('.');
            sb.append(this.memberName);
            if (!this.getElementHandle().getKind().isField()) {
                sb.append('(');
                if (this.params == null) {
                    sb.append("...");
                } else {
                    Iterator<ParamDesc> it = this.params.iterator();
                    while (it.hasNext()) {
                        ParamDesc paramDesc = it.next();
                        sb.append(paramDesc.typeName);
                        sb.append(' ');
                        sb.append(paramDesc.name);
                        if (!it.hasNext()) continue;
                        sb.append(", ");
                    }
                }
                sb.append(')');
            }
            return sb.toString();
        }

    }

    static class AttributeValueItem
    extends WhiteListJavaCompletionItem<TypeElement> {
        private static final String ATTRIBUTE_VALUE = "org/netbeans/modules/java/editor/resources/attribute_value_16.png";
        private static final String ATTRIBUTE_VALUE_COLOR = Utilities.getHTMLColor(128, 128, 128);
        private static ImageIcon icon;
        private JavaCompletionItem delegate;
        private String value;
        private String documentation;
        private String leftText;

        private AttributeValueItem(CompilationInfo info, String value, String documentation, TypeElement element, int substitutionOffset, ReferencesCount referencesCount, WhiteListQuery.WhiteList whiteList) {
            super(substitutionOffset, element != null ? ElementHandle.create((Element)element) : null, whiteList);
            if (value.charAt(0) == '\"' && value.charAt(value.length() - 1) != '\"') {
                value = value + '\"';
            }
            this.value = value;
            this.documentation = documentation;
            if (element != null) {
                this.delegate = AttributeValueItem.createTypeItem(info, element, (DeclaredType)element.asType(), substitutionOffset, referencesCount, false, false, false, false, false, false, this.getWhiteList());
            }
        }

        public int getSortPriority() {
            return - SMART_TYPE;
        }

        public CharSequence getSortText() {
            return this.delegate != null ? this.delegate.getSortText() : this.value;
        }

        public CharSequence getInsertPrefix() {
            return this.delegate != null ? this.delegate.getInsertPrefix() : this.value;
        }

        @Override
        public CompletionTask createDocumentationTask() {
            return this.documentation == null ? null : new CompletionTask(){
                private CompletionDocumentation cd;

                public void query(CompletionResultSet resultSet) {
                    resultSet.setDocumentation(this.cd);
                    resultSet.finish();
                }

                public void refresh(CompletionResultSet resultSet) {
                    resultSet.setDocumentation(this.cd);
                    resultSet.finish();
                }

                public void cancel() {
                }

            };
        }

        @Override
        protected ImageIcon getBaseIcon() {
            if (this.delegate != null) {
                return this.delegate.getIcon();
            }
            if (icon == null) {
                icon = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/java/editor/resources/attribute_value_16.png", (boolean)false);
            }
            return icon;
        }

        @Override
        protected String getLeftHtmlText() {
            if (this.leftText == null) {
                if (this.delegate != null) {
                    this.leftText = this.delegate.getLeftHtmlText();
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append(ATTRIBUTE_VALUE_COLOR);
                    sb.append(this.value);
                    sb.append("</font>");
                    this.leftText = sb.toString();
                }
            }
            return this.leftText;
        }

        @Override
        protected CharSequence getInsertPostfix(JTextComponent c) {
            if (this.delegate != null || this.value.endsWith(".class")) {
                return ".class";
            }
            if (this.value.charAt(this.value.length() - 1) == '\"') {
                return "\"";
            }
            return null;
        }

        @Override
        protected CharSequence substituteText(JTextComponent c, int offset, int length, CharSequence text, CharSequence toAdd) {
            if (this.delegate != null) {
                return this.delegate.substituteText(c, offset, length, text, toAdd);
            }
            StringBuilder sb = new StringBuilder(text);
            if (sb.charAt(sb.length() - 1) == '\"') {
                sb.deleteCharAt(sb.length() - 1);
            } else if (sb.toString().endsWith(".class")) {
                sb.delete(sb.length() - 6, sb.length());
            }
            return WhiteListJavaCompletionItem.super.substituteText(c, offset, length, sb, toAdd);
        }

        public String toString() {
            return this.value;
        }

    }

    static class AttributeItem
    extends JavaCompletionItem {
        private static final String ATTRIBUTE = "org/netbeans/modules/java/editor/resources/attribute_16.png";
        private static final String ATTRIBUTE_COLOR = Utilities.getHTMLColor(128, 128, 128);
        private static final String VALUE_COLOR = Utilities.getHTMLColor(192, 192, 192);
        private static ImageIcon icon;
        private ElementHandle<ExecutableElement> elementHandle;
        private boolean isDeprecated;
        private String simpleName;
        private String typeName;
        private String defaultValue;
        private String leftText;
        private String rightText;

        private AttributeItem(CompilationInfo info, ExecutableElement elem, ExecutableType type, int substitutionOffset, boolean isDeprecated) {
            super(substitutionOffset);
            this.elementHandle = ElementHandle.create((Element)elem);
            this.isDeprecated = isDeprecated;
            this.simpleName = elem.getSimpleName().toString();
            this.typeName = Utilities.getTypeName(info, type.getReturnType(), false).toString();
            AnnotationValue value = elem.getDefaultValue();
            this.defaultValue = value != null ? (value.getValue() instanceof TypeMirror ? Utilities.getTypeName(info, (TypeMirror)value.getValue(), false).toString() + ".class" : value.toString()) : null;
        }

        public int getSortPriority() {
            return 100;
        }

        public CharSequence getSortText() {
            return this.simpleName;
        }

        public CharSequence getInsertPrefix() {
            return this.simpleName;
        }

        @Override
        public CompletionTask createDocumentationTask() {
            return JavaCompletionProvider.createDocTask(this.elementHandle);
        }

        @Override
        protected ImageIcon getIcon() {
            if (icon == null) {
                icon = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/java/editor/resources/attribute_16.png", (boolean)false);
            }
            return icon;
        }

        @Override
        protected String getLeftHtmlText() {
            if (this.leftText == null) {
                StringBuilder sb = new StringBuilder();
                sb.append(ATTRIBUTE_COLOR);
                if (this.defaultValue == null) {
                    sb.append("<b>");
                }
                if (this.isDeprecated) {
                    sb.append("<s>");
                }
                sb.append(this.simpleName);
                if (this.isDeprecated) {
                    sb.append("</s>");
                }
                if (this.defaultValue == null) {
                    sb.append("</b>");
                } else {
                    sb.append("</font>");
                    sb.append(VALUE_COLOR);
                    sb.append(" = ");
                    sb.append(this.defaultValue);
                }
                sb.append("</font>");
                this.leftText = sb.toString();
            }
            return this.leftText;
        }

        @Override
        protected String getRightHtmlText() {
            if (this.rightText == null) {
                this.rightText = JavaCompletionItem.escape(this.typeName);
            }
            return this.rightText;
        }

        @Override
        protected CharSequence getInsertPostfix(JTextComponent c) {
            return CodeStyle.getDefault((Document)c.getDocument()).spaceAroundAssignOps() ? " = " : "=";
        }

        public String toString() {
            return this.simpleName;
        }
    }

    static class AnnotationItem
    extends AnnotationTypeItem {
        private AnnotationItem(CompilationInfo info, TypeElement elem, DeclaredType type, int substitutionOffset, ReferencesCount referencesCount, boolean isDeprecated, boolean smartType, WhiteListQuery.WhiteList whiteList) {
            super(info, elem, type, 0, substitutionOffset, referencesCount, isDeprecated, false, false, smartType, false, whiteList);
        }

        @Override
        public CharSequence getInsertPrefix() {
            return "@" + super.getInsertPrefix();
        }

        @Override
        protected CharSequence substituteText(JTextComponent c, final int offset, int length, CharSequence text, final CharSequence toAdd) {
            final BaseDocument doc = (BaseDocument)c.getDocument();
            final StringBuilder sb = new StringBuilder();
            final AtomicBoolean cancel = new AtomicBoolean();
            ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

                @Override
                public void run() {
                    try {
                        ParserManager.parse(Collections.singletonList(Source.create((Document)doc)), (UserTask)new UserTask(){

                            public void run(ResultIterator resultIterator) throws Exception {
                                if (cancel.get()) {
                                    return;
                                }
                                CompilationController controller = CompilationController.get((Parser.Result)resultIterator.getParserResult(offset));
                                controller.toPhase(JavaSource.Phase.RESOLVED);
                                if (cancel.get()) {
                                    return;
                                }
                                DeclaredType type = (DeclaredType)AnnotationItem.this.typeHandle.resolve((CompilationInfo)controller);
                                if (type != null) {
                                    TypeElement elem = (TypeElement)type.asElement();
                                    sb.append("@${PAR");
                                    if (type.getKind() != TypeKind.ERROR && EnumSet.range(ElementKind.PACKAGE, ElementKind.INTERFACE).contains((Object)elem.getEnclosingElement().getKind())) {
                                        sb.append(" type=\"");
                                        sb.append(elem.getQualifiedName());
                                        sb.append("\" default=\"");
                                        sb.append(elem.getSimpleName());
                                    } else {
                                        sb.append(" default=\"");
                                        sb.append(elem.getQualifiedName());
                                    }
                                    sb.append("\" editable=false}");
                                    sb.append(toAdd);
                                }
                            }
                        });
                    }
                    catch (ParseException pe) {
                        // empty catch block
                    }
                }

            }, (String)NbBundle.getMessage(JavaCompletionItem.class, (String)"JCI-import_resolve"), (AtomicBoolean)cancel, (boolean)false);
            if (sb.length() == 0) {
                return super.substituteText(c, offset, length, text, toAdd);
            }
            super.substituteText(c, offset, length, null, null);
            return sb;
        }

    }

    static class ParametersItem
    extends JavaCompletionItem {
        private static final String PARAMETERS_COLOR = Utilities.getHTMLColor(192, 192, 192);
        protected ElementHandle<ExecutableElement> elementHandle;
        private boolean isDeprecated;
        private int activeParamsIndex;
        private String simpleName;
        private ArrayList<ParamDesc> params;
        private String typeName;
        private String sortText;
        private String leftText;
        private String rightText;

        private ParametersItem(CompilationInfo info, ExecutableElement elem, ExecutableType type, int substitutionOffset, boolean isDeprecated, int activeParamsIndex, String name) {
            TypeMirror tm;
            super(substitutionOffset);
            this.elementHandle = ElementHandle.create((Element)elem);
            this.isDeprecated = isDeprecated;
            this.activeParamsIndex = activeParamsIndex;
            this.simpleName = name != null ? name : (elem.getKind() == ElementKind.CONSTRUCTOR ? elem.getEnclosingElement().getSimpleName().toString() : elem.getSimpleName().toString());
            this.params = new ArrayList();
            Iterator<? extends VariableElement> it = elem.getParameters().iterator();
            Iterator<? extends TypeMirror> tIt = type.getParameterTypes().iterator();
            while (it.hasNext() && tIt.hasNext() && (tm = tIt.next()) != null) {
                this.params.add(new ParamDesc(tm.toString(), Utilities.getTypeName(info, tm, false, elem.isVarArgs() && !tIt.hasNext()).toString(), it.next().getSimpleName().toString()));
            }
            TypeMirror retType = type.getReturnType();
            this.typeName = Utilities.getTypeName(info, retType, false).toString();
        }

        public int getSortPriority() {
            return 100 - SMART_TYPE;
        }

        public CharSequence getSortText() {
            if (this.sortText == null) {
                StringBuilder sortParams = new StringBuilder();
                sortParams.append('(');
                int cnt = 0;
                Iterator<ParamDesc> it = this.params.iterator();
                while (it.hasNext()) {
                    ParamDesc param = it.next();
                    sortParams.append(param.typeName);
                    if (it.hasNext()) {
                        sortParams.append(',');
                    }
                    ++cnt;
                }
                sortParams.append(')');
                this.sortText = "#" + (cnt < 10 ? "0" : "") + cnt + "#" + sortParams.toString();
            }
            return this.sortText;
        }

        public CharSequence getInsertPrefix() {
            return "";
        }

        @Override
        protected String getLeftHtmlText() {
            if (this.leftText == null) {
                StringBuilder lText = new StringBuilder();
                lText.append(PARAMETERS_COLOR);
                if (this.isDeprecated) {
                    lText.append("<s>");
                }
                lText.append(this.simpleName);
                if (this.isDeprecated) {
                    lText.append("</s>");
                }
                lText.append('(');
                for (int i = 0; i < this.params.size(); ++i) {
                    ParamDesc paramDesc = this.params.get(i);
                    if (i == this.activeParamsIndex) {
                        lText.append("</font>").append(LFCustoms.getTextFgColorHTML()).append("<b>");
                    }
                    lText.append(JavaCompletionItem.escape(paramDesc.typeName));
                    lText.append(' ');
                    lText.append(paramDesc.name);
                    if (i < this.params.size() - 1) {
                        lText.append(", ");
                        continue;
                    }
                    lText.append("</b>").append("</font>").append(PARAMETERS_COLOR);
                }
                lText.append(')');
                lText.append("</font>");
                return lText.toString();
            }
            return this.leftText;
        }

        @Override
        protected String getRightHtmlText() {
            if (this.rightText == null) {
                this.rightText = PARAMETERS_COLOR + JavaCompletionItem.escape(this.typeName) + "</font>";
            }
            return this.rightText;
        }

        @Override
        public CompletionTask createDocumentationTask() {
            return JavaCompletionProvider.createDocTask(this.elementHandle);
        }

        @Override
        public boolean instantSubstitution(JTextComponent component) {
            return false;
        }

        @Override
        protected CharSequence getInsertPostfix(JTextComponent c) {
            return ")";
        }

        @Override
        protected CharSequence substituteText(JTextComponent c, int offset, int length, CharSequence text, CharSequence toAdd) {
            super.substituteText(c, offset, length, null, null);
            StringBuilder sb = new StringBuilder();
            boolean guessArgs = Utilities.guessMethodArguments();
            for (int i = this.activeParamsIndex; i < this.params.size(); ++i) {
                ParamDesc paramDesc = this.params.get(i);
                sb.append("${");
                sb.append(paramDesc.name);
                if (guessArgs) {
                    sb.append(" named instanceof=\"");
                    sb.append(paramDesc.fullTypeName);
                    sb.append("\"");
                }
                sb.append("}");
                if (i >= this.params.size() - 1) continue;
                sb.append(", ");
            }
            sb.append(toAdd);
            Completion.get().showToolTip();
            return sb;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(this.typeName);
            sb.append(' ');
            sb.append(this.simpleName);
            sb.append('(');
            Iterator<ParamDesc> it = this.params.iterator();
            while (it.hasNext()) {
                ParamDesc paramDesc = it.next();
                sb.append(paramDesc.typeName);
                sb.append(' ');
                sb.append(paramDesc.name);
                if (!it.hasNext()) continue;
                sb.append(", ");
            }
            sb.append(") - parameters");
            return sb.toString();
        }
    }

    static class DefaultConstructorItem
    extends JavaCompletionItem {
        private static final String CONSTRUCTOR = "org/netbeans/modules/java/editor/resources/new_constructor_16.png";
        private static final String CONSTRUCTOR_COLOR = Utilities.getHTMLColor(242, 203, 64);
        private static ImageIcon icon;
        private boolean smartType;
        private String simpleName;
        private boolean isAbstract;
        private String sortText;
        private String leftText;

        private DefaultConstructorItem(TypeElement elem, int substitutionOffset, boolean smartType) {
            super(substitutionOffset);
            this.smartType = smartType;
            this.simpleName = elem.getSimpleName().toString();
            this.isAbstract = elem.getModifiers().contains((Object)Modifier.ABSTRACT);
        }

        public CharSequence getInsertPrefix() {
            return this.simpleName;
        }

        public int getSortPriority() {
            return this.smartType ? 650 - SMART_TYPE : 650;
        }

        public CharSequence getSortText() {
            if (this.sortText == null) {
                this.sortText = this.simpleName + "#0#";
            }
            return this.sortText;
        }

        @Override
        protected String getLeftHtmlText() {
            if (this.leftText == null) {
                this.leftText = CONSTRUCTOR_COLOR + this.simpleName + "()" + "</font>";
            }
            return this.leftText;
        }

        @Override
        protected ImageIcon getIcon() {
            if (icon == null) {
                icon = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/java/editor/resources/new_constructor_16.png", (boolean)false);
            }
            return icon;
        }

        @Override
        protected CharSequence getInsertPostfix(JTextComponent c) {
            StringBuilder sb = new StringBuilder();
            sb.append(CodeStyle.getDefault((Document)c.getDocument()).spaceBeforeMethodCallParen() ? " ()" : "()");
            if ("this".equals(this.simpleName) || "super".equals(this.simpleName)) {
                sb.append(';');
            }
            if (this.isAbstract) {
                sb.append(JavaCompletionItem.getIndent(c));
                sb.append("{\n}");
            }
            return sb;
        }

        @Override
        protected CharSequence substituteText(JTextComponent c, int offset, int length, CharSequence text, CharSequence toAdd) {
            CharSequence postfix;
            Position startPos;
            BaseDocument doc = (BaseDocument)c.getDocument();
            boolean inPlace = offset == c.getCaretPosition();
            try {
                startPos = doc.createPosition(offset + (inPlace ? 0 : text.length()), Position.Bias.Backward);
            }
            catch (BadLocationException ex) {
                return null;
            }
            CharSequence cs = super.substituteText(c, startPos.getOffset(), inPlace ? length : length - text.length(), null, toAdd);
            if (toAdd != null && (postfix = this.getInsertPostfix(c)) != null) {
                int postfixLen = postfix.length();
                int toAddLen = toAdd.length();
                if (toAddLen >= postfixLen) {
                    String toAddText = toAdd.toString();
                    if (this.isAbstract) {
                        try {
                            final int off = startPos.getOffset() + toAddText.indexOf(123) + 1;
                            ModificationResult mr = ModificationResult.runModificationTask(Collections.singletonList(Source.create((Document)c.getDocument())), (UserTask)new UserTask(){

                                public void run(ResultIterator resultIterator) throws Exception {
                                    WorkingCopy copy = WorkingCopy.get((Parser.Result)resultIterator.getParserResult());
                                    copy.toPhase(JavaSource.Phase.RESOLVED);
                                    int embeddedOffset = copy.getSnapshot().getEmbeddedOffset(off);
                                    TreePath path = copy.getTreeUtilities().pathFor(embeddedOffset);
                                    while (path.getLeaf() != path.getCompilationUnit()) {
                                        Tree tree = path.getLeaf();
                                        Tree parentTree = path.getParentPath().getLeaf();
                                        if (parentTree.getKind() == Tree.Kind.NEW_CLASS && TreeUtilities.CLASS_TREE_KINDS.contains((Object)tree.getKind())) {
                                            GeneratorUtils.generateAllAbstractMethodImplementations(copy, path);
                                            break;
                                        }
                                        path = path.getParentPath();
                                    }
                                }
                            });
                            GeneratorUtils.guardedCommit(c, mr);
                        }
                        catch (Exception ex) {
                            LOGGER.log(Level.FINE, null, ex);
                        }
                    }
                }
            }
            return cs;
        }

        public String toString() {
            return this.simpleName + "()";
        }

    }

    static class ConstructorItem
    extends WhiteListJavaCompletionItem<ExecutableElement> {
        private static final String CONSTRUCTOR_PUBLIC = "org/netbeans/modules/editor/resources/completion/constructor_16.png";
        private static final String CONSTRUCTOR_PROTECTED = "org/netbeans/modules/editor/resources/completion/constructor_protected_16.png";
        private static final String CONSTRUCTOR_PACKAGE = "org/netbeans/modules/editor/resources/completion/constructor_package_private_16.png";
        private static final String CONSTRUCTOR_PRIVATE = "org/netbeans/modules/editor/resources/completion/constructor_private_16.png";
        private static final String CONSTRUCTOR_COLOR = Utilities.getHTMLColor(242, 203, 64);
        private static final String PARAMETER_NAME_COLOR = Utilities.getHTMLColor(224, 160, 65);
        private static ImageIcon[] icon = new ImageIcon[4];
        private boolean isDeprecated;
        private boolean smartType;
        private String simpleName;
        protected Set<Modifier> modifiers;
        private List<ParamDesc> params;
        private boolean isAbstract;
        private boolean insertName;
        private String sortText;
        private String leftText;

        private ConstructorItem(CompilationInfo info, ExecutableElement elem, ExecutableType type, int substitutionOffset, boolean isDeprecated, boolean smartType, String name, WhiteListQuery.WhiteList whiteList) {
            TypeMirror tm;
            super(substitutionOffset, ElementHandle.create((Element)elem), whiteList);
            this.isDeprecated = isDeprecated;
            this.smartType = smartType;
            this.simpleName = name != null ? name : elem.getEnclosingElement().getSimpleName().toString();
            this.insertName = name != null;
            this.modifiers = elem.getModifiers();
            this.params = new ArrayList<ParamDesc>();
            Iterator<? extends VariableElement> it = elem.getParameters().iterator();
            Iterator<? extends TypeMirror> tIt = type.getParameterTypes().iterator();
            while (it.hasNext() && tIt.hasNext() && (tm = tIt.next()) != null) {
                this.params.add(new ParamDesc(tm.toString(), Utilities.getTypeName(info, tm, false, elem.isVarArgs() && !tIt.hasNext()).toString(), it.next().getSimpleName().toString()));
            }
            this.isAbstract = !this.insertName && elem.getEnclosingElement().getModifiers().contains((Object)Modifier.ABSTRACT);
        }

        public int getSortPriority() {
            return this.insertName ? 550 : (this.smartType ? 650 - SMART_TYPE : 650);
        }

        public CharSequence getSortText() {
            if (this.sortText == null) {
                StringBuilder sortParams = new StringBuilder();
                sortParams.append('(');
                int cnt = 0;
                Iterator<ParamDesc> it = this.params.iterator();
                while (it.hasNext()) {
                    ParamDesc paramDesc = it.next();
                    sortParams.append(paramDesc.typeName);
                    if (it.hasNext()) {
                        sortParams.append(',');
                    }
                    ++cnt;
                }
                sortParams.append(')');
                this.sortText = this.simpleName + "#" + (cnt < 10 ? "0" : "") + cnt + "#" + sortParams.toString();
            }
            return this.sortText;
        }

        public CharSequence getInsertPrefix() {
            return this.simpleName;
        }

        @Override
        protected String getLeftHtmlText() {
            if (this.leftText == null) {
                StringBuilder lText = new StringBuilder();
                lText.append(CONSTRUCTOR_COLOR);
                lText.append("<b>");
                if (this.isDeprecated || this.isBlackListed()) {
                    lText.append("<s>");
                }
                lText.append(this.simpleName);
                if (this.isDeprecated || this.isBlackListed()) {
                    lText.append("</s>");
                }
                lText.append("</b>");
                lText.append("</font>");
                lText.append('(');
                Iterator<ParamDesc> it = this.params.iterator();
                while (it.hasNext()) {
                    ParamDesc paramDesc = it.next();
                    lText.append(JavaCompletionItem.escape(paramDesc.typeName));
                    lText.append(' ');
                    lText.append(PARAMETER_NAME_COLOR);
                    lText.append(paramDesc.name);
                    lText.append("</font>");
                    if (!it.hasNext()) continue;
                    lText.append(", ");
                }
                lText.append(')');
                this.leftText = lText.toString();
            }
            return this.leftText;
        }

        @Override
        public CompletionTask createDocumentationTask() {
            return JavaCompletionProvider.createDocTask(this.getElementHandle());
        }

        @Override
        protected ImageIcon getBaseIcon() {
            ImageIcon newIcon;
            int level = JavaCompletionItem.getProtectionLevel(this.modifiers);
            ImageIcon cachedIcon = icon[level];
            if (cachedIcon != null) {
                return cachedIcon;
            }
            String iconPath = "org/netbeans/modules/editor/resources/completion/constructor_16.png";
            switch (level) {
                case 0: {
                    iconPath = "org/netbeans/modules/editor/resources/completion/constructor_private_16.png";
                    break;
                }
                case 1: {
                    iconPath = "org/netbeans/modules/editor/resources/completion/constructor_package_private_16.png";
                    break;
                }
                case 2: {
                    iconPath = "org/netbeans/modules/editor/resources/completion/constructor_protected_16.png";
                    break;
                }
                case 3: {
                    iconPath = "org/netbeans/modules/editor/resources/completion/constructor_16.png";
                }
            }
            ConstructorItem.icon[level] = newIcon = ImageUtilities.loadImageIcon((String)iconPath, (boolean)false);
            return newIcon;
        }

        @Override
        protected CharSequence getInsertPostfix(JTextComponent c) {
            StringBuilder sb = new StringBuilder();
            sb.append(CodeStyle.getDefault((Document)c.getDocument()).spaceBeforeMethodCallParen() ? " ()" : "()");
            if ("this".equals(this.simpleName) || "super".equals(this.simpleName)) {
                sb.append(';');
            } else if (this.isAbstract) {
                sb.append(JavaCompletionItem.getIndent(c));
                sb.append("{\n}");
            }
            return sb;
        }

        @Override
        protected CharSequence substituteText(JTextComponent c, int offset, int length, CharSequence text, CharSequence toAdd) {
            CharSequence postfix;
            Position endPos;
            Position startPos;
            BaseDocument doc = (BaseDocument)c.getDocument();
            boolean inPlace = offset == c.getCaretPosition();
            try {
                startPos = doc.createPosition(this.insertName || inPlace ? offset : offset + text.length(), Position.Bias.Backward);
                endPos = doc.createPosition(offset + length);
            }
            catch (BadLocationException ex) {
                return null;
            }
            CharSequence cs = this.insertName ? WhiteListJavaCompletionItem.super.substituteText(c, startPos.getOffset(), length, text, toAdd) : WhiteListJavaCompletionItem.super.substituteText(c, startPos.getOffset(), inPlace ? length : length - text.length(), null, toAdd);
            StringBuilder sb = new StringBuilder();
            if (toAdd != null && (postfix = this.getInsertPostfix(c)) != null) {
                int postfixLen = postfix.length();
                int toAddLen = toAdd.length();
                if (toAddLen >= postfixLen) {
                    String toAddText = toAdd.toString();
                    if (this.isAbstract) {
                        try {
                            final int off = startPos.getOffset() + (this.insertName ? text.length() : 0) + toAddText.indexOf(123) + 1;
                            ModificationResult mr = ModificationResult.runModificationTask(Collections.singletonList(Source.create((Document)c.getDocument())), (UserTask)new UserTask(){

                                public void run(ResultIterator resultIterator) throws Exception {
                                    WorkingCopy copy = WorkingCopy.get((Parser.Result)resultIterator.getParserResult());
                                    copy.toPhase(JavaSource.Phase.RESOLVED);
                                    int embeddedOffset = copy.getSnapshot().getEmbeddedOffset(off);
                                    TreePath path = copy.getTreeUtilities().pathFor(embeddedOffset);
                                    while (path.getLeaf() != path.getCompilationUnit()) {
                                        Tree tree = path.getLeaf();
                                        Tree parentTree = path.getParentPath().getLeaf();
                                        if (parentTree.getKind() == Tree.Kind.NEW_CLASS && TreeUtilities.CLASS_TREE_KINDS.contains((Object)tree.getKind())) {
                                            GeneratorUtils.generateAllAbstractMethodImplementations(copy, path);
                                            break;
                                        }
                                        path = path.getParentPath();
                                    }
                                }
                            });
                            GeneratorUtils.guardedCommit(c, mr);
                        }
                        catch (Exception ex) {
                            LOGGER.log(Level.FINE, null, ex);
                        }
                    }
                    if (!this.params.isEmpty()) {
                        boolean guessArgs = Utilities.guessMethodArguments();
                        Iterator<ParamDesc> it = this.params.iterator();
                        while (it.hasNext()) {
                            ParamDesc paramDesc = it.next();
                            sb.append("${");
                            sb.append(paramDesc.name);
                            if (guessArgs) {
                                sb.append(" named instanceof=\"");
                                sb.append(paramDesc.fullTypeName);
                                sb.append("\"");
                            }
                            sb.append("}");
                            if (!it.hasNext()) continue;
                            sb.append(", ");
                        }
                        c.select(startPos.getOffset() + (this.insertName ? text.length() : 0) + toAddText.indexOf(40) + 1, endPos.getOffset());
                        sb.append(c.getSelectedText());
                    }
                }
            }
            if (sb.length() == 0) {
                return cs;
            }
            Completion.get().showToolTip();
            return sb;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            for (Modifier mod : this.modifiers) {
                sb.append(mod.toString());
                sb.append(' ');
            }
            sb.append(this.simpleName);
            sb.append('(');
            Iterator<ParamDesc> it = this.params.iterator();
            while (it.hasNext()) {
                ParamDesc paramDesc = it.next();
                sb.append(paramDesc.typeName);
                sb.append(' ');
                sb.append(paramDesc.name);
                if (!it.hasNext()) continue;
                sb.append(", ");
            }
            sb.append(')');
            return sb.toString();
        }

    }

    static class GetterSetterMethodItem
    extends JavaCompletionItem {
        private static final String METHOD_PUBLIC = "org/netbeans/modules/editor/resources/completion/method_16.png";
        private static final String GETTER_BADGE_PATH = "org/netbeans/modules/java/editor/resources/getter_badge.png";
        private static final String SETTER_BADGE_PATH = "org/netbeans/modules/java/editor/resources/setter_badge.png";
        private static final String PARAMETER_NAME_COLOR = Utilities.getHTMLColor(224, 160, 65);
        private static ImageIcon superIcon;
        private static ImageIcon[] merged_icons;
        protected ElementHandle<VariableElement> elementHandle;
        private boolean setter;
        private String paramName;
        private String name;
        private String typeName;
        private String sortText;
        private String leftText;
        private String rightText;

        private GetterSetterMethodItem(CompilationInfo info, VariableElement elem, TypeMirror type, int substitutionOffset, String name, boolean setter) {
            boolean isStatic;
            super(substitutionOffset);
            this.elementHandle = ElementHandle.create((Element)elem);
            this.setter = setter;
            CodeStyle cs = null;
            try {
                cs = CodeStyle.getDefault((Document)info.getDocument());
            }
            catch (IOException ex) {
                // empty catch block
            }
            if (cs == null) {
                cs = CodeStyle.getDefault((FileObject)info.getFileObject());
            }
            String simpleName = CodeStyleUtils.removePrefixSuffix((CharSequence)elem.getSimpleName(), (String)((isStatic = elem.getModifiers().contains((Object)Modifier.STATIC)) ? cs.getStaticFieldNamePrefix() : cs.getFieldNamePrefix()), (String)(isStatic ? cs.getStaticFieldNameSuffix() : cs.getFieldNameSuffix()));
            this.paramName = CodeStyleUtils.addPrefixSuffix((CharSequence)simpleName, (String)cs.getParameterNamePrefix(), (String)cs.getParameterNameSuffix());
            this.name = name;
            this.typeName = Utilities.getTypeName(info, type, false).toString();
        }

        public int getSortPriority() {
            return 500;
        }

        public CharSequence getSortText() {
            if (this.sortText == null) {
                StringBuilder sortParams = new StringBuilder();
                sortParams.append('(');
                if (this.setter) {
                    sortParams.append(this.typeName);
                }
                sortParams.append(')');
                this.sortText = this.name + "#" + (this.setter ? "01" : "00") + "#" + sortParams.toString();
            }
            return this.sortText;
        }

        public CharSequence getInsertPrefix() {
            return this.name;
        }

        @Override
        protected String getLeftHtmlText() {
            if (this.leftText == null) {
                StringBuilder lText = new StringBuilder();
                lText.append(LFCustoms.getTextFgColorHTML());
                lText.append("<b>");
                lText.append(this.name);
                lText.append("</b>");
                lText.append("</font>");
                lText.append('(');
                if (this.setter) {
                    lText.append(JavaCompletionItem.escape(this.typeName));
                    lText.append(' ');
                    lText.append(PARAMETER_NAME_COLOR);
                    lText.append(this.paramName);
                    lText.append("</font>");
                }
                lText.append(") - ");
                lText.append(GENERATE_TEXT);
                this.leftText = lText.toString();
            }
            return this.leftText;
        }

        @Override
        protected String getRightHtmlText() {
            if (this.rightText == null) {
                this.rightText = this.setter ? "void" : JavaCompletionItem.escape(this.typeName);
            }
            return this.rightText;
        }

        @Override
        protected ImageIcon getIcon() {
            if (merged_icons[this.setter ? 1 : 0] == null) {
                if (superIcon == null) {
                    superIcon = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/editor/resources/completion/method_16.png", (boolean)false);
                }
                if (this.setter) {
                    ImageIcon setterBadge = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/java/editor/resources/setter_badge.png", (boolean)false);
                    GetterSetterMethodItem.merged_icons[1] = new ImageIcon(ImageUtilities.mergeImages((Image)superIcon.getImage(), (Image)setterBadge.getImage(), (int)8, (int)8));
                } else {
                    ImageIcon getterBadge = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/java/editor/resources/getter_badge.png", (boolean)false);
                    GetterSetterMethodItem.merged_icons[0] = new ImageIcon(ImageUtilities.mergeImages((Image)superIcon.getImage(), (Image)getterBadge.getImage(), (int)8, (int)8));
                }
            }
            return merged_icons[this.setter ? 1 : 0];
        }

        @Override
        protected CharSequence substituteText(JTextComponent c, int offset, int length, CharSequence text, CharSequence toAdd) {
            Position pos;
            BaseDocument doc = (BaseDocument)c.getDocument();
            try {
                pos = doc.createPosition(offset);
            }
            catch (BadLocationException e) {
                return null;
            }
            CharSequence cs = super.substituteText(c, offset, length, null, null);
            try {
                ModificationResult mr = ModificationResult.runModificationTask(Collections.singletonList(Source.create((Document)doc)), (UserTask)new UserTask(){

                    public void run(ResultIterator resultIterator) throws Exception {
                        WorkingCopy copy = WorkingCopy.get((Parser.Result)resultIterator.getParserResult());
                        copy.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                        VariableElement ve = (VariableElement)GetterSetterMethodItem.this.elementHandle.resolve((CompilationInfo)copy);
                        if (ve == null) {
                            return;
                        }
                        int embeddedOffset = copy.getSnapshot().getEmbeddedOffset(pos.getOffset());
                        TreePath tp = copy.getTreeUtilities().pathFor(embeddedOffset);
                        if (TreeUtilities.CLASS_TREE_KINDS.contains((Object)tp.getLeaf().getKind())) {
                            TypeElement te;
                            if (Utilities.inAnonymousOrLocalClass(tp)) {
                                copy.toPhase(JavaSource.Phase.RESOLVED);
                            }
                            if ((te = (TypeElement)copy.getTrees().getElement(tp)) != null) {
                                GeneratorUtilities gu = GeneratorUtilities.get((WorkingCopy)copy);
                                MethodTree method = GetterSetterMethodItem.this.setter ? gu.createSetter(te, ve) : gu.createGetter(te, ve);
                                ClassTree decl = GeneratorUtils.insertClassMember(copy, (ClassTree)tp.getLeaf(), (Tree)method, embeddedOffset);
                                copy.rewrite(tp.getLeaf(), (Tree)decl);
                            }
                        }
                    }
                });
                GeneratorUtils.guardedCommit(c, mr);
            }
            catch (Exception ex) {
                LOGGER.log(Level.FINE, null, ex);
            }
            return cs;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("public ");
            sb.append(this.setter ? "void" : this.typeName);
            sb.append(' ');
            sb.append(this.name);
            sb.append('(');
            if (this.setter) {
                sb.append(this.typeName);
                sb.append(' ');
                sb.append(this.paramName);
            }
            sb.append(") - ");
            sb.append(GENERATE_TEXT);
            return sb.toString();
        }

        @Override
        public boolean instantSubstitution(JTextComponent component) {
            return false;
        }

        static {
            merged_icons = new ImageIcon[2];
        }

    }

    static class OverrideMethodItem
    extends MethodItem {
        private static final String IMPL_BADGE_PATH = "org/netbeans/modules/java/editor/resources/implement_badge.png";
        private static final String OVRD_BADGE_PATH = "org/netbeans/modules/java/editor/resources/override_badge.png";
        private static final String OVERRIDE_TEXT = NbBundle.getMessage(JavaCompletionItem.class, (String)"override_Lbl");
        private static final String IMPLEMENT_TEXT = NbBundle.getMessage(JavaCompletionItem.class, (String)"implement_Lbl");
        private static ImageIcon implementBadge = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/java/editor/resources/implement_badge.png", (boolean)false);
        private static ImageIcon overrideBadge = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/java/editor/resources/override_badge.png", (boolean)false);
        private static ImageIcon[][] merged_icon = new ImageIcon[2][4];
        private boolean implement;
        private String leftText;

        private OverrideMethodItem(CompilationInfo info, ExecutableElement elem, ExecutableType type, int substitutionOffset, boolean implement, WhiteListQuery.WhiteList whiteList) {
            super(info, elem, type, substitutionOffset, null, false, false, false, false, false, -1, false, whiteList);
            CodeStyle cs = null;
            try {
                cs = CodeStyle.getDefault((Document)info.getDocument());
            }
            catch (IOException ex) {
                // empty catch block
            }
            if (cs == null) {
                cs = CodeStyle.getDefault((FileObject)info.getFileObject());
            }
            for (ParamDesc paramDesc : this.params) {
                String name = CodeStyleUtils.removePrefixSuffix((CharSequence)paramDesc.name, (String)cs.getParameterNamePrefix(), (String)cs.getParameterNameSuffix());
                paramDesc.name = CodeStyleUtils.addPrefixSuffix((CharSequence)name, (String)cs.getParameterNamePrefix(), (String)cs.getParameterNameSuffix());
            }
            this.implement = implement;
        }

        @Override
        protected String getLeftHtmlText() {
            if (this.leftText == null) {
                this.leftText = super.getLeftHtmlText() + " - ";
                this.leftText = this.leftText + (this.implement ? IMPLEMENT_TEXT : OVERRIDE_TEXT);
            }
            return this.leftText;
        }

        @Override
        protected ImageIcon getBaseIcon() {
            int level;
            ImageIcon merged = merged_icon[this.implement ? 0 : 1][level = JavaCompletionItem.getProtectionLevel(this.modifiers)];
            if (merged != null) {
                return merged;
            }
            ImageIcon superIcon = super.getBaseIcon();
            merged = new ImageIcon(ImageUtilities.mergeImages((Image)superIcon.getImage(), (Image)(this.implement ? implementBadge.getImage() : overrideBadge.getImage()), (int)8, (int)8));
            OverrideMethodItem.merged_icon[this.implement != false ? 0 : 1][level] = merged;
            return merged;
        }

        @Override
        protected CharSequence substituteText(JTextComponent c, int offset, int length, CharSequence text, CharSequence toAdd) {
            Position pos;
            BaseDocument doc = (BaseDocument)c.getDocument();
            try {
                pos = doc.createPosition(offset);
            }
            catch (BadLocationException e) {
                return null;
            }
            CharSequence cs = super.substituteText(c, offset, length, null, null);
            try {
                ModificationResult mr = ModificationResult.runModificationTask(Collections.singletonList(Source.create((Document)doc)), (UserTask)new UserTask(){

                    public void run(ResultIterator resultIterator) throws Exception {
                        WorkingCopy copy = WorkingCopy.get((Parser.Result)resultIterator.getParserResult());
                        copy.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                        ExecutableElement ee = (ExecutableElement)OverrideMethodItem.this.getElementHandle().resolve((CompilationInfo)copy);
                        if (ee == null) {
                            return;
                        }
                        int embeddedOffset = copy.getSnapshot().getEmbeddedOffset(pos.getOffset());
                        TreePath tp = copy.getTreeUtilities().pathFor(embeddedOffset);
                        if (TreeUtilities.CLASS_TREE_KINDS.contains((Object)tp.getLeaf().getKind())) {
                            if (Utilities.inAnonymousOrLocalClass(tp)) {
                                copy.toPhase(JavaSource.Phase.RESOLVED);
                            }
                            if (OverrideMethodItem.this.implement) {
                                GeneratorUtils.generateAbstractMethodImplementation(copy, tp, ee, embeddedOffset);
                            } else {
                                GeneratorUtils.generateMethodOverride(copy, tp, ee, embeddedOffset);
                            }
                        }
                    }
                });
                GeneratorUtils.guardedCommit(c, mr);
            }
            catch (Exception ex) {
                LOGGER.log(Level.FINE, null, ex);
            }
            return cs;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(super.toString());
            sb.append(" - ");
            sb.append(this.implement ? IMPLEMENT_TEXT : OVERRIDE_TEXT);
            return sb.toString();
        }

        @Override
        public boolean instantSubstitution(JTextComponent component) {
            return false;
        }

    }

    static class MethodItem
    extends WhiteListJavaCompletionItem<ExecutableElement> {
        private static final String METHOD_PUBLIC = "org/netbeans/modules/editor/resources/completion/method_16.png";
        private static final String METHOD_PROTECTED = "org/netbeans/modules/editor/resources/completion/method_protected_16.png";
        private static final String METHOD_PACKAGE = "org/netbeans/modules/editor/resources/completion/method_package_private_16.png";
        private static final String METHOD_PRIVATE = "org/netbeans/modules/editor/resources/completion/method_private_16.png";
        private static final String METHOD_ST_PUBLIC = "org/netbeans/modules/editor/resources/completion/method_static_16.png";
        private static final String METHOD_ST_PROTECTED = "org/netbeans/modules/editor/resources/completion/method_static_protected_16.png";
        private static final String METHOD_ST_PRIVATE = "org/netbeans/modules/editor/resources/completion/method_static_private_16.png";
        private static final String METHOD_ST_PACKAGE = "org/netbeans/modules/editor/resources/completion/method_static_package_private_16.png";
        private static final String PARAMETER_NAME_COLOR = Utilities.getHTMLColor(224, 160, 65);
        private static ImageIcon[][] icon = new ImageIcon[2][4];
        private boolean isInherited;
        private boolean isDeprecated;
        private boolean inImport;
        private boolean smartType;
        private boolean memberRef;
        private String simpleName;
        protected Set<Modifier> modifiers;
        protected List<ParamDesc> params;
        private String typeName;
        private boolean addSemicolon;
        private String sortText;
        private String leftText;
        private String rightText;
        private boolean autoImportEnclosingType;
        private CharSequence enclSortText;
        private int assignToVarOffset;
        private CharSequence assignToVarText;

        private MethodItem(CompilationInfo info, ExecutableElement elem, ExecutableType type, int substitutionOffset, ReferencesCount referencesCount, boolean isInherited, boolean isDeprecated, boolean inImport, boolean addSemicolon, boolean smartType, int assignToVarOffset, boolean memberRef, WhiteListQuery.WhiteList whiteList) {
            TypeMirror tm;
            super(substitutionOffset, ElementHandle.create((Element)elem), whiteList);
            Color c = LFCustoms.getTextFgColor();
            this.isInherited = isInherited;
            this.isDeprecated = isDeprecated;
            this.inImport = inImport;
            this.smartType = smartType;
            this.memberRef = memberRef;
            this.simpleName = elem.getSimpleName().toString();
            this.modifiers = elem.getModifiers();
            this.params = new ArrayList<ParamDesc>();
            Iterator<? extends VariableElement> it = elem.getParameters().iterator();
            Iterator<? extends TypeMirror> tIt = type.getParameterTypes().iterator();
            while (it.hasNext() && tIt.hasNext() && (tm = tIt.next()) != null) {
                this.params.add(new ParamDesc(tm.toString(), Utilities.getTypeName(info, tm, false, elem.isVarArgs() && !tIt.hasNext()).toString(), it.next().getSimpleName().toString()));
            }
            TypeMirror retType = type.getReturnType();
            this.typeName = Utilities.getTypeName(info, retType, false).toString();
            this.addSemicolon = addSemicolon && retType.getKind() == TypeKind.VOID;
            this.autoImportEnclosingType = referencesCount != null;
            this.enclSortText = this.autoImportEnclosingType ? new LazySortText(elem.getEnclosingElement().getSimpleName().toString(), null, ElementHandle.create((Element)((TypeElement)elem.getEnclosingElement())), referencesCount) : "";
            this.assignToVarOffset = type.getReturnType().getKind() == TypeKind.VOID ? -1 : assignToVarOffset;
            this.assignToVarText = this.assignToVarOffset < 0 ? null : JavaCompletionItem.createAssignToVarText(info, type.getReturnType(), this.simpleName);
        }

        public int getSortPriority() {
            return this.smartType ? 500 - SMART_TYPE : 500;
        }

        public CharSequence getSortText() {
            if (this.sortText == null) {
                StringBuilder sortParams = new StringBuilder();
                sortParams.append('(');
                int cnt = 0;
                Iterator<ParamDesc> it = this.params.iterator();
                while (it.hasNext()) {
                    ParamDesc param = it.next();
                    sortParams.append(param.typeName);
                    if (it.hasNext()) {
                        sortParams.append(',');
                    }
                    ++cnt;
                }
                sortParams.append(')');
                this.sortText = this.simpleName + "#" + this.enclSortText + "#" + (cnt < 10 ? "0" : "") + cnt + "#" + sortParams.toString();
            }
            return this.sortText;
        }

        public CharSequence getInsertPrefix() {
            return this.simpleName;
        }

        @Override
        protected String getLeftHtmlText() {
            if (this.leftText == null) {
                StringBuilder lText = new StringBuilder();
                lText.append(LFCustoms.getTextFgColorHTML());
                if (!this.isInherited) {
                    lText.append("<b>");
                }
                if (this.isDeprecated || this.isBlackListed()) {
                    lText.append("<s>");
                }
                lText.append(this.simpleName);
                if (this.isDeprecated || this.isBlackListed()) {
                    lText.append("</s>");
                }
                if (!this.isInherited) {
                    lText.append("</b>");
                }
                lText.append("</font>");
                lText.append('(');
                Iterator<ParamDesc> it = this.params.iterator();
                while (it.hasNext()) {
                    ParamDesc paramDesc = it.next();
                    lText.append(JavaCompletionItem.escape(paramDesc.typeName));
                    lText.append(' ');
                    lText.append(PARAMETER_NAME_COLOR);
                    lText.append(paramDesc.name);
                    lText.append("</font>");
                    if (!it.hasNext()) continue;
                    lText.append(", ");
                }
                lText.append(')');
                return lText.toString();
            }
            return this.leftText;
        }

        @Override
        protected String getRightHtmlText() {
            if (this.rightText == null) {
                this.rightText = JavaCompletionItem.escape(this.typeName);
            }
            return this.rightText;
        }

        @Override
        public CompletionTask createDocumentationTask() {
            return JavaCompletionProvider.createDocTask(this.getElementHandle());
        }

        @Override
        protected ImageIcon getBaseIcon() {
            int level = JavaCompletionItem.getProtectionLevel(this.modifiers);
            boolean isStatic = this.modifiers.contains((Object)Modifier.STATIC);
            ImageIcon cachedIcon = icon[isStatic ? 1 : 0][level];
            if (cachedIcon != null) {
                return cachedIcon;
            }
            String iconPath = "org/netbeans/modules/editor/resources/completion/method_16.png";
            if (isStatic) {
                switch (level) {
                    case 0: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/method_static_private_16.png";
                        break;
                    }
                    case 1: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/method_static_package_private_16.png";
                        break;
                    }
                    case 2: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/method_static_protected_16.png";
                        break;
                    }
                    case 3: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/method_static_16.png";
                    }
                }
            } else {
                switch (level) {
                    case 0: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/method_private_16.png";
                        break;
                    }
                    case 1: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/method_package_private_16.png";
                        break;
                    }
                    case 2: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/method_protected_16.png";
                        break;
                    }
                    case 3: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/method_16.png";
                    }
                }
            }
            ImageIcon newIcon = ImageUtilities.loadImageIcon((String)iconPath, (boolean)false);
            MethodItem.icon[isStatic != false ? 1 : 0][level] = newIcon;
            return newIcon;
        }

        @Override
        protected CharSequence getInsertPostfix(JTextComponent c) {
            StringBuilder sb = new StringBuilder();
            if (this.inImport) {
                sb.append(';');
            } else {
                if (!this.memberRef) {
                    sb.append(CodeStyle.getDefault((Document)c.getDocument()).spaceBeforeMethodCallParen() ? " ()" : "()");
                }
                if (this.addSemicolon) {
                    sb.append(';');
                }
            }
            return sb;
        }

        @Override
        protected int getAssignToVarOffset() {
            return this.assignToVarOffset;
        }

        @Override
        public CharSequence getAssignToVarText() {
            return this.assignToVarText;
        }

        @Override
        protected CharSequence substituteText(JTextComponent c, final int offset, int length, CharSequence text, CharSequence toAdd) {
            String toAddText;
            int idx;
            final BaseDocument doc = (BaseDocument)c.getDocument();
            StringBuilder sb = new StringBuilder();
            if (!(toAdd == null || (idx = (toAddText = toAdd.toString()).indexOf(41)) <= 0 || this.params.isEmpty() && text.length() != length)) {
                sb.append(text);
                if (CodeStyle.getDefault((Document)doc).spaceBeforeMethodCallParen()) {
                    sb.append(' ');
                }
                sb.append('(');
                if (this.params.isEmpty()) {
                    sb.append("${cursor}");
                } else {
                    boolean guessArgs = Utilities.guessMethodArguments();
                    Iterator<ParamDesc> it = this.params.iterator();
                    while (it.hasNext()) {
                        ParamDesc paramDesc = it.next();
                        sb.append("${");
                        sb.append(paramDesc.name);
                        if (guessArgs) {
                            sb.append(" named instanceof=\"");
                            sb.append(paramDesc.fullTypeName);
                            sb.append("\"");
                        }
                        sb.append('}');
                        if (!it.hasNext()) continue;
                        sb.append(", ");
                    }
                }
                sb.append(')');
                if (toAddText.length() > idx + 1) {
                    sb.append(toAddText.substring(idx + 1));
                }
            }
            if (sb.length() == 0) {
                CharSequence st = WhiteListJavaCompletionItem.super.substituteText(c, offset, length, text, toAdd);
                if (st != null) {
                    sb.append(st);
                }
            } else {
                WhiteListJavaCompletionItem.super.substituteText(c, offset, length, null, null);
            }
            if (this.autoImportEnclosingType) {
                final AtomicBoolean cancel = new AtomicBoolean();
                ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

                    @Override
                    public void run() {
                        try {
                            ParserManager.parse(Collections.singletonList(Source.create((Document)doc)), (UserTask)new UserTask(){

                                public void run(ResultIterator resultIterator) throws Exception {
                                    if (cancel.get()) {
                                        return;
                                    }
                                    CompilationController controller = CompilationController.get((Parser.Result)resultIterator.getParserResult(offset));
                                    controller.toPhase(JavaSource.Phase.RESOLVED);
                                    if (cancel.get()) {
                                        return;
                                    }
                                    ExecutableElement ee = (ExecutableElement)MethodItem.this.getElementHandle().resolve((CompilationInfo)controller);
                                    if (ee != null) {
                                        TreePath tp = controller.getTreeUtilities().pathFor(controller.getSnapshot().getEmbeddedOffset(offset));
                                        AutoImport.resolveImport((CompilationInfo)controller, tp, ee.getEnclosingElement().asType());
                                    }
                                }
                            });
                        }
                        catch (ParseException pe) {
                            // empty catch block
                        }
                    }

                }, (String)NbBundle.getMessage(JavaCompletionItem.class, (String)"JCI-import_resolve"), (AtomicBoolean)cancel, (boolean)false);
            }
            Completion.get().showToolTip();
            return sb;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            for (Modifier mod : this.modifiers) {
                sb.append(mod.toString());
                sb.append(' ');
            }
            sb.append(this.typeName);
            sb.append(' ');
            sb.append(this.simpleName);
            sb.append('(');
            Iterator<ParamDesc> it = this.params.iterator();
            while (it.hasNext()) {
                ParamDesc paramDesc = it.next();
                sb.append(paramDesc.typeName);
                sb.append(' ');
                sb.append(paramDesc.name);
                if (!it.hasNext()) continue;
                sb.append(", ");
            }
            sb.append(')');
            return sb.toString();
        }

    }

    static class FieldItem
    extends WhiteListJavaCompletionItem<VariableElement> {
        private static final String FIELD_PUBLIC = "org/netbeans/modules/editor/resources/completion/field_16.png";
        private static final String FIELD_PROTECTED = "org/netbeans/modules/editor/resources/completion/field_protected_16.png";
        private static final String FIELD_PACKAGE = "org/netbeans/modules/editor/resources/completion/field_package_private_16.png";
        private static final String FIELD_PRIVATE = "org/netbeans/modules/editor/resources/completion/field_private_16.png";
        private static final String FIELD_ST_PUBLIC = "org/netbeans/modules/editor/resources/completion/field_static_16.png";
        private static final String FIELD_ST_PROTECTED = "org/netbeans/modules/editor/resources/completion/field_static_protected_16.png";
        private static final String FIELD_ST_PACKAGE = "org/netbeans/modules/editor/resources/completion/field_static_package_private_16.png";
        private static final String FIELD_ST_PRIVATE = "org/netbeans/modules/editor/resources/completion/field_static_private_16.png";
        private static final String FIELD_COLOR = Utilities.getHTMLColor(64, 198, 88);
        private static ImageIcon[][] icon = new ImageIcon[2][4];
        private boolean isInherited;
        private boolean isDeprecated;
        private boolean smartType;
        private String simpleName;
        private Set<Modifier> modifiers;
        private String typeName;
        private String leftText;
        private String rightText;
        private boolean autoImportEnclosingType;
        private CharSequence enclSortText;
        private int assignToVarOffset;
        private CharSequence assignToVarText;

        private FieldItem(CompilationInfo info, VariableElement elem, TypeMirror type, int substitutionOffset, ReferencesCount referencesCount, boolean isInherited, boolean isDeprecated, boolean smartType, int assignToVarOffset, WhiteListQuery.WhiteList whiteList) {
            super(substitutionOffset, ElementHandle.create((Element)elem), whiteList);
            this.isInherited = isInherited;
            this.isDeprecated = isDeprecated;
            this.smartType = smartType;
            this.simpleName = elem.getSimpleName().toString();
            this.modifiers = elem.getModifiers();
            this.typeName = Utilities.getTypeName(info, type, false).toString();
            this.autoImportEnclosingType = referencesCount != null;
            this.enclSortText = this.autoImportEnclosingType ? new LazySortText(elem.getEnclosingElement().getSimpleName().toString(), null, ElementHandle.create((Element)((TypeElement)elem.getEnclosingElement())), referencesCount) : "";
            this.assignToVarOffset = assignToVarOffset;
            this.assignToVarText = assignToVarOffset < 0 ? null : JavaCompletionItem.createAssignToVarText(info, type, this.simpleName);
        }

        public int getSortPriority() {
            return this.smartType ? 300 - SMART_TYPE : 300;
        }

        public CharSequence getSortText() {
            return this.simpleName + "#" + this.enclSortText;
        }

        public CharSequence getInsertPrefix() {
            return this.simpleName;
        }

        @Override
        public CompletionTask createDocumentationTask() {
            return JavaCompletionProvider.createDocTask(this.getElementHandle());
        }

        @Override
        protected String getLeftHtmlText() {
            if (this.leftText == null) {
                StringBuilder sb = new StringBuilder();
                sb.append(FIELD_COLOR);
                if (!this.isInherited) {
                    sb.append("<b>");
                }
                if (this.isDeprecated || this.isBlackListed()) {
                    sb.append("<s>");
                }
                sb.append(this.simpleName);
                if (this.isDeprecated || this.isBlackListed()) {
                    sb.append("</s>");
                }
                if (!this.isInherited) {
                    sb.append("</b>");
                }
                sb.append("</font>");
                this.leftText = sb.toString();
            }
            return this.leftText;
        }

        @Override
        protected String getRightHtmlText() {
            if (this.rightText == null) {
                this.rightText = JavaCompletionItem.escape(this.typeName);
            }
            return this.rightText;
        }

        @Override
        protected ImageIcon getBaseIcon() {
            int level = JavaCompletionItem.getProtectionLevel(this.modifiers);
            boolean isStatic = this.modifiers.contains((Object)Modifier.STATIC);
            ImageIcon cachedIcon = icon[isStatic ? 1 : 0][level];
            if (cachedIcon != null) {
                return cachedIcon;
            }
            String iconPath = "org/netbeans/modules/editor/resources/completion/field_16.png";
            if (isStatic) {
                switch (level) {
                    case 0: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/field_static_private_16.png";
                        break;
                    }
                    case 1: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/field_static_package_private_16.png";
                        break;
                    }
                    case 2: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/field_static_protected_16.png";
                        break;
                    }
                    case 3: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/field_static_16.png";
                    }
                }
            } else {
                switch (level) {
                    case 0: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/field_private_16.png";
                        break;
                    }
                    case 1: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/field_package_private_16.png";
                        break;
                    }
                    case 2: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/field_protected_16.png";
                        break;
                    }
                    case 3: {
                        iconPath = "org/netbeans/modules/editor/resources/completion/field_16.png";
                    }
                }
            }
            ImageIcon newIcon = ImageUtilities.loadImageIcon((String)iconPath, (boolean)false);
            FieldItem.icon[isStatic != false ? 1 : 0][level] = newIcon;
            return newIcon;
        }

        @Override
        protected int getAssignToVarOffset() {
            return this.assignToVarOffset;
        }

        @Override
        public CharSequence getAssignToVarText() {
            return this.assignToVarText;
        }

        @Override
        protected CharSequence substituteText(final JTextComponent c, final int offset, int length, CharSequence text, CharSequence toAdd) {
            CharSequence cs = WhiteListJavaCompletionItem.super.substituteText(c, offset, length, text, toAdd);
            if (this.autoImportEnclosingType) {
                final AtomicBoolean cancel = new AtomicBoolean();
                ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

                    @Override
                    public void run() {
                        try {
                            ParserManager.parse(Collections.singletonList(Source.create((Document)c.getDocument())), (UserTask)new UserTask(){

                                public void run(ResultIterator resultIterator) throws Exception {
                                    if (cancel.get()) {
                                        return;
                                    }
                                    CompilationController controller = CompilationController.get((Parser.Result)resultIterator.getParserResult(offset));
                                    controller.toPhase(JavaSource.Phase.RESOLVED);
                                    if (cancel.get()) {
                                        return;
                                    }
                                    VariableElement ve = (VariableElement)FieldItem.this.getElementHandle().resolve((CompilationInfo)controller);
                                    if (ve != null) {
                                        TreePath tp = controller.getTreeUtilities().pathFor(controller.getSnapshot().getEmbeddedOffset(offset));
                                        AutoImport.resolveImport((CompilationInfo)controller, tp, ve.getEnclosingElement().asType());
                                    }
                                }
                            });
                        }
                        catch (ParseException pe) {
                            // empty catch block
                        }
                    }

                }, (String)NbBundle.getMessage(JavaCompletionItem.class, (String)"JCI-import_resolve"), (AtomicBoolean)cancel, (boolean)false);
            }
            return cs;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            for (Modifier mod : this.modifiers) {
                sb.append(mod.toString());
                sb.append(' ');
            }
            sb.append(this.typeName);
            sb.append(' ');
            sb.append(this.simpleName);
            return sb.toString();
        }

    }

    static class VariableItem
    extends JavaCompletionItem {
        private static final String LOCAL_VARIABLE = "org/netbeans/modules/editor/resources/completion/localVariable.gif";
        private static final String PARAMETER_COLOR = Utilities.getHTMLColor(64, 64, 188);
        private static ImageIcon icon;
        private String varName;
        private boolean newVarName;
        private boolean smartType;
        private String typeName;
        private String leftText;
        private String rightText;
        private int assignToVarOffset;
        private CharSequence assignToVarText;

        private VariableItem(CompilationInfo info, TypeMirror type, String varName, int substitutionOffset, boolean newVarName, boolean smartType, int assignToVarOffset) {
            super(substitutionOffset);
            this.varName = varName;
            this.newVarName = newVarName;
            this.smartType = smartType;
            this.typeName = type != null ? Utilities.getTypeName(info, type, false).toString() : null;
            this.assignToVarOffset = assignToVarOffset;
            this.assignToVarText = assignToVarOffset < 0 ? null : JavaCompletionItem.createAssignToVarText(info, type, varName);
        }

        public int getSortPriority() {
            return this.smartType ? 200 - SMART_TYPE : 200;
        }

        public CharSequence getSortText() {
            return this.varName;
        }

        public CharSequence getInsertPrefix() {
            return this.varName;
        }

        @Override
        protected String getLeftHtmlText() {
            if (this.leftText == null) {
                this.leftText = PARAMETER_COLOR + "<b>" + this.varName + "</b>" + "</font>";
            }
            return this.leftText;
        }

        @Override
        protected String getRightHtmlText() {
            if (this.rightText == null) {
                this.rightText = JavaCompletionItem.escape(this.typeName);
            }
            return this.rightText;
        }

        @Override
        protected ImageIcon getIcon() {
            if (icon == null) {
                icon = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/editor/resources/completion/localVariable.gif", (boolean)false);
            }
            return icon;
        }

        @Override
        public int getAssignToVarOffset() {
            return this.assignToVarOffset;
        }

        @Override
        public CharSequence getAssignToVarText() {
            return this.assignToVarText;
        }

        public String toString() {
            return (this.typeName != null ? new StringBuilder().append(this.typeName).append(" ").toString() : "") + this.varName;
        }
    }

    static class TypeParameterItem
    extends JavaCompletionItem {
        private String simpleName;
        private String leftText;

        private TypeParameterItem(TypeParameterElement elem, int substitutionOffset) {
            super(substitutionOffset);
            this.simpleName = elem.getSimpleName().toString();
        }

        public int getSortPriority() {
            return 700;
        }

        public CharSequence getSortText() {
            return this.simpleName;
        }

        public CharSequence getInsertPrefix() {
            return this.simpleName;
        }

        @Override
        protected String getLeftHtmlText() {
            if (this.leftText == null) {
                this.leftText = LFCustoms.getTextFgColorHTML() + this.simpleName + "</font>";
            }
            return this.leftText;
        }

        public String toString() {
            return this.simpleName;
        }
    }

    static class AnnotationTypeItem
    extends ClassItem {
        private static final String ANNOTATION = "org/netbeans/modules/editor/resources/completion/annotation_type.png";
        private static ImageIcon icon;

        private AnnotationTypeItem(CompilationInfo info, TypeElement elem, DeclaredType type, int dim, int substitutionOffset, ReferencesCount referencesCount, boolean isDeprecated, boolean insideNew, boolean addSimpleName, boolean smartType, boolean autoImport, WhiteListQuery.WhiteList whiteList) {
            super(info, elem, type, dim, substitutionOffset, referencesCount, isDeprecated, insideNew, false, addSimpleName, smartType, autoImport, whiteList);
        }

        @Override
        protected ImageIcon getBaseIcon() {
            if (icon == null) {
                icon = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/editor/resources/completion/annotation_type.png", (boolean)false);
            }
            return icon;
        }
    }

    static class EnumItem
    extends ClassItem {
        private static final String ENUM = "org/netbeans/modules/editor/resources/completion/enum.png";
        private static ImageIcon icon;

        private EnumItem(CompilationInfo info, TypeElement elem, DeclaredType type, int dim, int substitutionOffset, ReferencesCount referencesCount, boolean isDeprecated, boolean insideNew, boolean addSimpleName, boolean smartType, boolean autoImport, WhiteListQuery.WhiteList whiteList) {
            super(info, elem, type, dim, substitutionOffset, referencesCount, isDeprecated, insideNew, false, addSimpleName, smartType, autoImport, whiteList);
        }

        @Override
        protected ImageIcon getBaseIcon() {
            if (icon == null) {
                icon = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/editor/resources/completion/enum.png", (boolean)false);
            }
            return icon;
        }
    }

    static class InterfaceItem
    extends ClassItem {
        private static final String INTERFACE = "org/netbeans/modules/editor/resources/completion/interface.png";
        private static final String INTERFACE_COLOR = Utilities.getHTMLColor(128, 128, 128);
        private static ImageIcon icon;

        private InterfaceItem(CompilationInfo info, TypeElement elem, DeclaredType type, int dim, int substitutionOffset, ReferencesCount referencesCount, boolean isDeprecated, boolean insideNew, boolean addTypeVars, boolean addSimpleName, boolean smartType, boolean autoImport, WhiteListQuery.WhiteList whiteList) {
            super(info, elem, type, dim, substitutionOffset, referencesCount, isDeprecated, insideNew, addTypeVars, addSimpleName, smartType, autoImport, whiteList);
        }

        @Override
        protected ImageIcon getBaseIcon() {
            if (icon == null) {
                icon = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/editor/resources/completion/interface.png", (boolean)false);
            }
            return icon;
        }

        @Override
        protected String getColor() {
            return INTERFACE_COLOR;
        }
    }

    static class ClassItem
    extends WhiteListJavaCompletionItem<TypeElement> {
        private static final String CLASS = "org/netbeans/modules/editor/resources/completion/class_16.png";
        private static final String CLASS_COLOR = Utilities.getHTMLColor(150, 64, 64);
        private static final String PKG_COLOR = Utilities.getHTMLColor(192, 192, 192);
        private static ImageIcon icon;
        protected TypeMirrorHandle<DeclaredType> typeHandle;
        private int dim;
        private boolean hasTypeArgs;
        private boolean isDeprecated;
        private boolean insideNew;
        private boolean addTypeVars;
        private boolean addSimpleName;
        private boolean smartType;
        private String simpleName;
        private String typeName;
        private String enclName;
        private CharSequence sortText;
        private String leftText;
        private boolean autoImportEnclosingType;

        private ClassItem(CompilationInfo info, TypeElement elem, DeclaredType type, int dim, int substitutionOffset, ReferencesCount referencesCount, boolean isDeprecated, boolean insideNew, boolean addTypeVars, boolean addSimpleName, boolean smartType, boolean autoImportEnclosingType, WhiteListQuery.WhiteList whiteList) {
            super(substitutionOffset, ElementHandle.create((Element)elem), whiteList);
            this.typeHandle = TypeMirrorHandle.create((TypeMirror)type);
            this.dim = dim;
            this.hasTypeArgs = addTypeVars && SourceVersion.RELEASE_5.compareTo(info.getSourceVersion()) <= 0 && !type.getTypeArguments().isEmpty();
            this.isDeprecated = isDeprecated;
            this.insideNew = insideNew;
            this.addTypeVars = addTypeVars;
            this.addSimpleName = addSimpleName;
            this.smartType = smartType;
            this.simpleName = elem.getSimpleName().toString();
            this.typeName = Utilities.getTypeName(info, type, false).toString();
            if (referencesCount != null) {
                this.enclName = Utilities.getElementName(elem.getEnclosingElement(), true).toString();
                this.sortText = new LazySortText(this.simpleName, this.enclName, this.getElementHandle(), referencesCount);
            } else {
                this.enclName = null;
                this.sortText = this.simpleName;
            }
            this.autoImportEnclosingType = autoImportEnclosingType;
        }

        public int getSortPriority() {
            return this.smartType ? 800 - SMART_TYPE : 800;
        }

        public CharSequence getSortText() {
            return this.sortText;
        }

        public CharSequence getInsertPrefix() {
            return this.simpleName;
        }

        @Override
        public boolean instantSubstitution(JTextComponent component) {
            return false;
        }

        @Override
        public CompletionTask createDocumentationTask() {
            return this.typeHandle.getKind() == TypeKind.DECLARED ? JavaCompletionProvider.createDocTask(ElementHandle.from(this.typeHandle)) : null;
        }

        @Override
        protected ImageIcon getBaseIcon() {
            if (icon == null) {
                icon = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/editor/resources/completion/class_16.png", (boolean)false);
            }
            return icon;
        }

        @Override
        protected String getLeftHtmlText() {
            if (this.leftText == null) {
                StringBuilder sb = new StringBuilder();
                sb.append(this.getColor());
                if (this.isDeprecated || this.isBlackListed()) {
                    sb.append("<s>");
                }
                sb.append(JavaCompletionItem.escape(this.typeName));
                for (int i = 0; i < this.dim; ++i) {
                    sb.append("[]");
                }
                if (this.isDeprecated || this.isBlackListed()) {
                    sb.append("</s>");
                }
                if (this.enclName != null && this.enclName.length() > 0) {
                    sb.append("</font>");
                    sb.append(PKG_COLOR);
                    sb.append(" (");
                    sb.append(this.enclName);
                    sb.append(")");
                }
                sb.append("</font>");
                this.leftText = sb.toString();
            }
            return this.leftText;
        }

        protected String getColor() {
            return CLASS_COLOR;
        }

        @Override
        protected String getInsertPostfix(JTextComponent c) {
            StringBuilder sb = new StringBuilder();
            if (this.hasTypeArgs) {
                sb.append("<>");
            }
            for (int i = 0; i < this.dim; ++i) {
                sb.append("[]");
            }
            return sb.length() > 0 ? sb.toString() : null;
        }

        @Override
        protected CharSequence substituteText(final JTextComponent c, final int offset, final int length, CharSequence text, final CharSequence toAdd) {
            final StringBuilder template = new StringBuilder();
            final AtomicBoolean cancel = new AtomicBoolean();
            ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

                @Override
                public void run() {
                    try {
                        BaseDocument doc = (BaseDocument)c.getDocument();
                        ParserManager.parse(Collections.singletonList(Source.create((Document)doc)), (UserTask)new UserTask(){

                            public void run(ResultIterator resultIterator) throws Exception {
                                TypeElement elem;
                                if (cancel.get()) {
                                    return;
                                }
                                CompilationController controller = CompilationController.get((Parser.Result)resultIterator.getParserResult(offset));
                                controller.toPhase(JavaSource.Phase.RESOLVED);
                                if (cancel.get()) {
                                    return;
                                }
                                DeclaredType type = (DeclaredType)ClassItem.this.typeHandle.resolve((CompilationInfo)controller);
                                TypeElement typeElement = elem = type != null ? (TypeElement)type.asElement() : null;
                                if (elem == null) {
                                    ClassItem.this.substituteText(c, offset, length, ClassItem.this.simpleName, toAdd);
                                    return;
                                }
                                CharSequence tail = null;
                                boolean partialMatch = false;
                                int cnt = 1;
                                if (toAdd != null) {
                                    String cs = ClassItem.this.getInsertPostfix(c);
                                    int postfixLen = cs != null ? cs.length() : 0;
                                    int toAddLen = toAdd.length();
                                    if (toAddLen >= postfixLen) {
                                        Iterator<? extends TypeMirror> tas = type != null ? type.getTypeArguments().iterator() : null;
                                        StringBuilder sb = new StringBuilder();
                                        boolean asTemplate = false;
                                        if (tas != null && tas.hasNext()) {
                                            sb.append('<');
                                            if (!ClassItem.this.insideNew || elem.getModifiers().contains((Object)Modifier.ABSTRACT) || controller.getSourceVersion().compareTo(SourceVersion.RELEASE_7) < 0 || !ClassItem.this.allowDiamond((CompilationInfo)controller, offset, type)) {
                                                while (tas.hasNext()) {
                                                    TypeMirror ta = tas.next();
                                                    sb.append("${PAR#");
                                                    sb.append(cnt++);
                                                    if (ta.getKind() == TypeKind.TYPEVAR) {
                                                        TypeVariable tv = (TypeVariable)ta;
                                                        if (ClassItem.this.smartType || elem != tv.asElement().getEnclosingElement()) {
                                                            sb.append(" editable=false default=\"");
                                                            sb.append(Utilities.getTypeName((CompilationInfo)controller, ta, true));
                                                            asTemplate = true;
                                                        } else {
                                                            sb.append(" typeVar=\"");
                                                            sb.append(tv.asElement().getSimpleName());
                                                            sb.append("\" type=\"");
                                                            ta = tv.getUpperBound();
                                                            sb.append(Utilities.getTypeName((CompilationInfo)controller, ta, true));
                                                            sb.append("\" default=\"");
                                                            sb.append(Utilities.getTypeName((CompilationInfo)controller, ta, false));
                                                            if (ClassItem.this.addTypeVars && SourceVersion.RELEASE_5.compareTo(controller.getSourceVersion()) <= 0) {
                                                                asTemplate = true;
                                                            }
                                                        }
                                                        sb.append("\"}");
                                                    } else if (ta.getKind() == TypeKind.WILDCARD) {
                                                        sb.append(" type=\"");
                                                        TypeMirror bound = ((WildcardType)ta).getExtendsBound();
                                                        if (bound == null) {
                                                            bound = ((WildcardType)ta).getSuperBound();
                                                        }
                                                        sb.append(bound != null ? Utilities.getTypeName((CompilationInfo)controller, bound, true) : "Object");
                                                        sb.append("\" default=\"");
                                                        sb.append(bound != null ? Utilities.getTypeName((CompilationInfo)controller, bound, false) : "Object");
                                                        sb.append("\"}");
                                                        asTemplate = true;
                                                    } else if (ta.getKind() == TypeKind.ERROR) {
                                                        sb.append(" default=\"");
                                                        sb.append(((ErrorType)ta).asElement().getSimpleName());
                                                        sb.append("\"}");
                                                        asTemplate = true;
                                                    } else {
                                                        sb.append(" type=\"");
                                                        sb.append(Utilities.getTypeName((CompilationInfo)controller, ta, true));
                                                        sb.append("\" default=\"");
                                                        sb.append(Utilities.getTypeName((CompilationInfo)controller, ta, false));
                                                        sb.append("\" editable=false}");
                                                        asTemplate = true;
                                                    }
                                                    if (!tas.hasNext()) continue;
                                                    sb.append(", ");
                                                }
                                            } else {
                                                asTemplate = true;
                                            }
                                            sb.append('>');
                                        }
                                        if (asTemplate) {
                                            template.append(sb);
                                        } else {
                                            for (int i = 0; i < ClassItem.this.dim; ++i) {
                                                template.append("[${PAR#");
                                                template.append(cnt++);
                                                template.append(" instanceof=\"int\" default=\"\"}]");
                                            }
                                        }
                                        if (toAddLen > postfixLen) {
                                            tail = toAdd.subSequence(postfixLen, toAddLen);
                                        }
                                    } else {
                                        partialMatch = true;
                                    }
                                }
                                if (template.length() == 0 && (ClassItem.this.addSimpleName || ClassItem.this.enclName == null)) {
                                    ClassItem.this.substituteText(c, offset, length, elem.getSimpleName(), toAdd);
                                    if (ClassItem.this.insideNew && (toAdd == null || toAdd.length() == 0)) {
                                        Completion.get().showCompletion();
                                    }
                                } else {
                                    StringBuilder sb = new StringBuilder();
                                    if (ClassItem.this.addSimpleName || ClassItem.this.enclName == null) {
                                        sb.append(elem.getSimpleName());
                                    } else if (!"text/x-java".equals(controller.getSnapshot().getMimePath().getPath())) {
                                        TreePath tp = controller.getTreeUtilities().pathFor(controller.getSnapshot().getEmbeddedOffset(offset));
                                        sb.append(AutoImport.resolveImport((CompilationInfo)controller, tp, controller.getTypes().getDeclaredType(elem, new TypeMirror[0])));
                                    } else {
                                        sb.append("${PAR#0");
                                        if ((type == null || type.getKind() != TypeKind.ERROR) && EnumSet.range(ElementKind.PACKAGE, ElementKind.INTERFACE).contains((Object)elem.getEnclosingElement().getKind())) {
                                            sb.append(" type=\"");
                                            sb.append(elem.getQualifiedName());
                                            sb.append("\" default=\"");
                                            sb.append(elem.getSimpleName());
                                        } else {
                                            sb.append(" default=\"");
                                            sb.append(elem.getQualifiedName());
                                        }
                                        sb.append("\" editable=false}");
                                    }
                                    template.insert(0, sb);
                                    if (ClassItem.this.insideNew && ClassItem.this.dim == 0 && !partialMatch) {
                                        template.append("${cursor completionInvoke}");
                                    }
                                    if (tail != null) {
                                        template.append(tail);
                                    }
                                    if (partialMatch) {
                                        template.append(toAdd);
                                    }
                                    ClassItem.this.substituteText(c, offset, length, null, null);
                                }
                                if (ClassItem.this.autoImportEnclosingType && elem != null) {
                                    TreePath tp = controller.getTreeUtilities().pathFor(controller.getSnapshot().getEmbeddedOffset(offset));
                                    AutoImport.resolveImport((CompilationInfo)controller, tp, elem.getEnclosingElement().asType());
                                }
                            }
                        });
                    }
                    catch (ParseException pe) {
                        // empty catch block
                    }
                }

            }, (String)NbBundle.getMessage(JavaCompletionItem.class, (String)"JCI-import_resolve"), (AtomicBoolean)cancel, (boolean)false);
            return template;
        }

        private boolean allowDiamond(CompilationInfo info, int offset, DeclaredType type) {
            Trees trees;
            int pos;
            TreePath path;
            TreeUtilities tu = info.getTreeUtilities();
            for (path = tu.pathFor((int)offset); path != null && !(path.getLeaf() instanceof StatementTree); path = path.getParentPath()) {
            }
            if (path != null && (pos = (int)(trees = info.getTrees()).getSourcePositions().getStartPosition(path.getCompilationUnit(), path.getLeaf().getKind() == Tree.Kind.VARIABLE ? ((VariableTree)path.getLeaf()).getType() : path.getLeaf())) >= 0) {
                Scope scope = tu.scopeFor(pos);
                String stmt = info.getText().substring(pos, offset);
                StringBuilder sb = new StringBuilder();
                sb.append('{').append(stmt).append(Utilities.getTypeName(info, type, true)).append("();}");
                SourcePositions[] sp = new SourcePositions[1];
                StatementTree st = tu.parseStatement(sb.toString(), sp);
                tu.attributeTree((Tree)st, scope);
                TreePath tp = tu.pathFor(new TreePath(path, (Tree)st), offset - pos, sp[0]);
                TypeMirror tm = tp != null ? trees.getTypeMirror(tp) : null;
                sb = new StringBuilder();
                sb.append('{').append(stmt).append(((TypeElement)type.asElement()).getQualifiedName()).append("<>();}");
                st = tu.parseStatement(sb.toString(), sp);
                tu.attributeTree((Tree)st, scope);
                tp = tu.pathFor(new TreePath(path, (Tree)st), offset - pos, sp[0]);
                TypeMirror tmd = tp != null ? trees.getTypeMirror(tp) : null;
                return tm != null && tmd != null && info.getTypes().isSameType(tm, tmd);
            }
            return false;
        }

        public String toString() {
            return this.simpleName;
        }

    }

    static class PackageItem
    extends JavaCompletionItem {
        private static final String PACKAGE = "org/netbeans/modules/java/editor/resources/package.gif";
        private static final String PACKAGE_COLOR = Utilities.getHTMLColor(64, 150, 64);
        private static ImageIcon icon;
        private boolean inPackageStatement;
        private String simpleName;
        private String sortText;
        private String leftText;

        private PackageItem(String pkgFQN, int substitutionOffset, boolean inPackageStatement) {
            super(substitutionOffset);
            this.inPackageStatement = inPackageStatement;
            int idx = pkgFQN.lastIndexOf(46);
            this.simpleName = idx < 0 ? pkgFQN : pkgFQN.substring(idx + 1);
            this.sortText = this.simpleName + "#" + pkgFQN;
        }

        public int getSortPriority() {
            return 900;
        }

        public CharSequence getSortText() {
            return this.sortText;
        }

        public CharSequence getInsertPrefix() {
            return this.simpleName;
        }

        @Override
        protected ImageIcon getIcon() {
            if (icon == null) {
                icon = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/java/editor/resources/package.gif", (boolean)false);
            }
            return icon;
        }

        @Override
        protected String getLeftHtmlText() {
            if (this.leftText == null) {
                StringBuilder sb = new StringBuilder();
                sb.append(PACKAGE_COLOR);
                sb.append(this.simpleName);
                sb.append("</font>");
                this.leftText = sb.toString();
            }
            return this.leftText;
        }

        @Override
        public void defaultAction(JTextComponent component) {
            if (component != null) {
                Completion.get().hideDocumentation();
                if (this.inPackageStatement || Utilities.getJavaCompletionAutoPopupTriggers().indexOf(46) < 0) {
                    Completion.get().hideCompletion();
                }
                this.process(component, '\u0000', false);
            }
        }

        @Override
        protected String getInsertPostfix(JTextComponent c) {
            return this.inPackageStatement ? null : ".";
        }

        public String toString() {
            return this.simpleName;
        }
    }

    static class KeywordItem
    extends JavaCompletionItem {
        private static final String JAVA_KEYWORD = "org/netbeans/modules/java/editor/resources/javakw_16.png";
        private static final String KEYWORD_COLOR = Utilities.getHTMLColor(64, 64, 217);
        private static ImageIcon icon;
        private String kwd;
        private int dim;
        private String postfix;
        private boolean smartType;
        private String leftText;

        private KeywordItem(String kwd, int dim, String postfix, int substitutionOffset, boolean smartType) {
            super(substitutionOffset);
            this.kwd = kwd;
            this.dim = dim;
            this.postfix = postfix;
            this.smartType = smartType;
        }

        public int getSortPriority() {
            return this.smartType ? 670 - SMART_TYPE : 670;
        }

        public CharSequence getSortText() {
            return this.kwd;
        }

        public CharSequence getInsertPrefix() {
            return this.kwd;
        }

        @Override
        protected ImageIcon getIcon() {
            if (icon == null) {
                icon = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/java/editor/resources/javakw_16.png", (boolean)false);
            }
            return icon;
        }

        @Override
        protected String getLeftHtmlText() {
            if (this.leftText == null) {
                StringBuilder sb = new StringBuilder();
                sb.append(KEYWORD_COLOR);
                sb.append("<b>");
                sb.append(this.kwd);
                for (int i = 0; i < this.dim; ++i) {
                    sb.append("[]");
                }
                sb.append("</b>");
                sb.append("</font>");
                this.leftText = sb.toString();
            }
            return this.leftText;
        }

        @Override
        protected CharSequence getInsertPostfix(JTextComponent c) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < this.dim; ++i) {
                sb.append("[]");
            }
            if (this.postfix != null) {
                sb.append(this.postfix);
            }
            return sb.length() > 0 ? sb.toString() : null;
        }

        @Override
        protected CharSequence substituteText(JTextComponent c, int offset, int length, CharSequence text, CharSequence toAdd) {
            CharSequence cs;
            if (toAdd != null && (cs = this.getInsertPostfix(c)) != null) {
                int postfixLen = cs.length();
                int toAddLen = toAdd.length();
                if (toAddLen >= postfixLen) {
                    StringBuilder template = new StringBuilder();
                    int cnt = 1;
                    for (int i = 0; i < this.dim; ++i) {
                        template.append("[${PAR#");
                        template.append(cnt++);
                        template.append(" instanceof=\"int\" default=\"\"}]");
                    }
                    if (template.length() > 0) {
                        super.substituteText(c, offset, length, text, null);
                        if (toAddLen > postfixLen) {
                            template.append(toAdd.subSequence(postfixLen, toAddLen));
                        }
                        return template;
                    }
                }
            }
            return super.substituteText(c, offset, length, text, toAdd);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(this.kwd);
            for (int i = 0; i < this.dim; ++i) {
                sb.append("[]");
            }
            return sb.toString();
        }
    }

    static abstract class WhiteListJavaCompletionItem<T extends Element>
    extends JavaCompletionItem {
        private static final String WARNING = "org/netbeans/modules/java/editor/resources/warning_badge.gif";
        private static ImageIcon warningIcon;
        private final WhiteListQuery.WhiteList whiteList;
        private final List<ElementHandle<? extends Element>> handles;
        private Boolean isBlackListed;

        protected WhiteListJavaCompletionItem(int substitutionOffset, ElementHandle<? extends Element> handle, WhiteListQuery.WhiteList whiteList) {
            super(substitutionOffset);
            this.handles = Collections.singletonList(handle);
            this.whiteList = whiteList;
        }

        protected WhiteListJavaCompletionItem(int substitutionOffset, List<? extends Element> elements, WhiteListQuery.WhiteList whiteList) {
            super(substitutionOffset);
            this.handles = new ArrayList<ElementHandle<? extends Element>>(elements.size());
            for (Element e : elements) {
                this.handles.add((ElementHandle)(e.getKind().isField() || e.getKind() == ElementKind.METHOD ? ElementHandle.create((Element)e) : null));
            }
            this.whiteList = whiteList;
        }

        protected final WhiteListQuery.WhiteList getWhiteList() {
            return this.whiteList;
        }

        protected final ElementHandle<T> getElementHandle() {
            return this.handles.isEmpty() ? null : this.handles.get(this.handles.size() - 1);
        }

        protected final List<ElementHandle<? extends Element>> getElementHandles() {
            return this.handles;
        }

        protected final boolean isBlackListed() {
            if (this.isBlackListed == null) {
                this.isBlackListed = this.whiteList == null ? false : !this.checkIsAllowed();
            }
            return this.isBlackListed;
        }

        private boolean checkIsAllowed() {
            for (ElementHandle<? extends Element> handle : this.handles) {
                if (handle == null || this.whiteList.check(handle, WhiteListQuery.Operation.USAGE).isAllowed()) continue;
                return false;
            }
            return true;
        }

        @Override
        public boolean instantSubstitution(JTextComponent component) {
            return this.isBlackListed() ? false : super.instantSubstitution(component);
        }

        @Override
        public final ImageIcon getIcon() {
            ImageIcon base = this.getBaseIcon();
            if (base == null || !this.isBlackListed()) {
                return base;
            }
            if (warningIcon == null) {
                warningIcon = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/java/editor/resources/warning_badge.gif", (boolean)false);
            }
            assert (warningIcon != null);
            return new ImageIcon(ImageUtilities.mergeImages((Image)base.getImage(), (Image)warningIcon.getImage(), (int)8, (int)8));
        }

        protected ImageIcon getBaseIcon() {
            return super.getIcon();
        }
    }

}

