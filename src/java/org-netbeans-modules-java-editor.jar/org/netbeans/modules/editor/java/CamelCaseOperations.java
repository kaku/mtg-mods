/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.spi.editor.typinghooks.CamelCaseInterceptor
 *  org.netbeans.spi.editor.typinghooks.CamelCaseInterceptor$Factory
 *  org.netbeans.spi.editor.typinghooks.CamelCaseInterceptor$MutableContext
 *  org.openide.util.NbPreferences
 */
package org.netbeans.modules.editor.java;

import java.util.List;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.spi.editor.typinghooks.CamelCaseInterceptor;
import org.openide.util.NbPreferences;

class CamelCaseOperations {
    CamelCaseOperations() {
    }

    static int nextCamelCasePosition(JTextComponent textComponent) throws BadLocationException {
        final int offset = textComponent.getCaretPosition();
        final Document doc = textComponent.getDocument();
        final int[] retOffset = new int[1];
        final BadLocationException[] retExc = new BadLocationException[1];
        doc.render(new Runnable(){

            @Override
            public void run() {
                try {
                    retOffset[0] = CamelCaseOperations.nextCamelCasePositionImpl(doc, offset);
                }
                catch (BadLocationException ex) {
                    retExc[0] = ex;
                }
            }
        });
        if (retExc[0] != null) {
            throw retExc[0];
        }
        return retOffset[0];
    }

    static int nextCamelCasePositionImpl(Document doc, int offset) throws BadLocationException {
        TokenSequence seq;
        Token t;
        String image;
        TokenHierarchy th = doc != null ? TokenHierarchy.get((Document)doc) : null;
        List embeddedSequences = th != null ? th.embeddedTokenSequences(offset, false) : null;
        TokenSequence tokenSequence = seq = embeddedSequences != null ? (TokenSequence)embeddedSequences.get(embeddedSequences.size() - 1) : null;
        if (seq != null) {
            seq.move(offset);
        }
        Token token = t = seq != null && seq.moveNext() ? seq.offsetToken() : null;
        if (t != null && t.id() == JavaTokenId.IDENTIFIER && (image = t.text().toString()) != null && image.length() > 0) {
            int length = image.length();
            if (offset != t.offset(th) + length) {
                int i;
                char charAtI;
                int offsetInImage = offset - t.offset(th);
                int start = offsetInImage + 1;
                if (Character.isUpperCase(image.charAt(offsetInImage))) {
                    for (i = start; i < length && Character.isUpperCase(charAtI = image.charAt(i)); ++i) {
                        ++start;
                    }
                }
                for (i = start; i < length; ++i) {
                    charAtI = image.charAt(i);
                    if (!Character.isUpperCase(charAtI)) continue;
                    return t.offset(th) + i;
                }
            }
            return t.offset(th) + image.length();
        }
        return Utilities.getNextWord((BaseDocument)((BaseDocument)doc), (int)offset);
    }

    static int previousCamelCasePosition(JTextComponent textComponent) throws BadLocationException {
        final int offset = textComponent.getCaretPosition();
        final Document doc = textComponent.getDocument();
        final int[] retOffset = new int[1];
        final BadLocationException[] retExc = new BadLocationException[1];
        doc.render(new Runnable(){

            @Override
            public void run() {
                try {
                    retOffset[0] = CamelCaseOperations.previousCamelCasePositionImpl(doc, offset);
                }
                catch (BadLocationException ex) {
                    retExc[0] = ex;
                }
            }
        });
        if (retExc[0] != null) {
            throw retExc[0];
        }
        return retOffset[0];
    }

    static int previousCamelCasePositionImpl(Document doc, int offset) throws BadLocationException {
        Token t;
        TokenSequence seq;
        TokenHierarchy th = doc != null ? TokenHierarchy.get((Document)doc) : null;
        List embeddedSequences = th != null ? th.embeddedTokenSequences(offset, false) : null;
        TokenSequence tokenSequence = seq = embeddedSequences != null && embeddedSequences.size() > 0 ? (TokenSequence)embeddedSequences.get(embeddedSequences.size() - 1) : null;
        if (seq != null) {
            seq.move(offset);
        }
        Token token = t = seq != null && seq.moveNext() ? seq.offsetToken() : null;
        if (t != null) {
            if (t.offset(th) == offset) {
                Token token2 = t = seq.movePrevious() ? seq.offsetToken() : null;
            }
            if (t != null && t.id() == JavaTokenId.IDENTIFIER) {
                String image = t.text().toString();
                if (image != null && image.length() > 0) {
                    int length = image.length();
                    int offsetInImage = offset - 1 - t.offset(th);
                    if (Character.isUpperCase(image.charAt(offsetInImage))) {
                        for (int i = offsetInImage - 1; i >= 0; --i) {
                            char charAtI = image.charAt(i);
                            if (Character.isUpperCase(charAtI)) continue;
                            return t.offset(th) + i + 1;
                        }
                        return t.offset(th);
                    }
                    for (int i = offsetInImage - 1; i >= 0; --i) {
                        char charAtI = image.charAt(i);
                        if (!Character.isUpperCase(charAtI)) continue;
                        for (int j = i; j >= 0; --j) {
                            char charAtJ = image.charAt(j);
                            if (Character.isUpperCase(charAtJ)) continue;
                            return t.offset(th) + j + 1;
                        }
                        return t.offset(th);
                    }
                    return t.offset(th);
                }
            } else if (t != null && t.id() == JavaTokenId.WHITESPACE) {
                Token whitespaceToken = t;
                while (whitespaceToken != null && whitespaceToken.id() == JavaTokenId.WHITESPACE) {
                    int wsOffset = whitespaceToken.offset(th);
                    if (wsOffset == 0) {
                        return 0;
                    }
                    whitespaceToken = seq.movePrevious() ? seq.offsetToken() : null;
                }
                if (whitespaceToken != null) {
                    return whitespaceToken.offset(th) + whitespaceToken.length();
                }
            }
        }
        return Utilities.getPreviousWord((BaseDocument)((BaseDocument)doc), (int)offset);
    }

    public static class JavaCamelCaseInterceptor
    implements CamelCaseInterceptor {
        private boolean isUsingCamelCase() {
            return NbPreferences.root().getBoolean("useCamelCaseStyleNavigation", true);
        }

        public boolean beforeChange(CamelCaseInterceptor.MutableContext context) throws BadLocationException {
            return false;
        }

        public void change(CamelCaseInterceptor.MutableContext context) throws BadLocationException {
            if (this.isUsingCamelCase()) {
                if (context.isBackward()) {
                    context.setNextWordOffset(CamelCaseOperations.previousCamelCasePosition(context.getComponent()));
                } else {
                    context.setNextWordOffset(CamelCaseOperations.nextCamelCasePosition(context.getComponent()));
                }
            }
        }

        public void afterChange(CamelCaseInterceptor.MutableContext context) throws BadLocationException {
        }

        public void cancelled(CamelCaseInterceptor.MutableContext context) {
        }

        public static class JavaFactory
        implements CamelCaseInterceptor.Factory {
            public CamelCaseInterceptor createCamelCaseInterceptor(MimePath mimePath) {
                return new JavaCamelCaseInterceptor();
            }
        }

    }

}

