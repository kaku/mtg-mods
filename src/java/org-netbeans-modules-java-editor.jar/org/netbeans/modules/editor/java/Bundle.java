/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.editor.java;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String configure_Excludes_Lbl() {
        return NbBundle.getMessage(Bundle.class, (String)"configure_Excludes_Lbl");
    }

    static String exclude_Lbl(Object arg0) {
        return NbBundle.getMessage(Bundle.class, (String)"exclude_Lbl", (Object)arg0);
    }

    private void Bundle() {
    }
}

