/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CaseTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.api.progress.ProgressUtils
 *  org.netbeans.lib.editor.codetemplates.api.CodeTemplate
 *  org.netbeans.lib.editor.codetemplates.spi.CodeTemplateFilter
 *  org.netbeans.lib.editor.codetemplates.spi.CodeTemplateFilter$ContextBasedFactory
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.editor.java;

import com.sun.source.tree.CaseTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.lib.editor.codetemplates.api.CodeTemplate;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateFilter;
import org.netbeans.modules.editor.java.JavaCodeTemplateProcessor;
import org.netbeans.modules.editor.java.Utilities;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class JavaCodeTemplateFilter
implements CodeTemplateFilter {
    private static final Logger LOG = Logger.getLogger(JavaCodeTemplateFilter.class.getName());
    private static final String EXPRESSION = "EXPRESSION";
    private static final String CLASS_HEADER = "CLASS_HEADER";
    private Tree.Kind treeKindCtx = null;
    private String stringCtx = null;

    private JavaCodeTemplateFilter(JTextComponent component, int offset) {
        if (Utilities.isJavaContext(component, offset, true)) {
            final int startOffset = offset;
            final int endOffset = component.getSelectionStart() == offset ? component.getSelectionEnd() : -1;
            final Source source = Source.create((Document)component.getDocument());
            if (source != null) {
                final AtomicBoolean cancel = new AtomicBoolean();
                ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

                    @Override
                    public void run() {
                        try {
                            ParserManager.parse(Collections.singleton(source), (UserTask)new UserTask(){

                                public void run(ResultIterator resultIterator) throws Exception {
                                    CompilationController controller;
                                    if (cancel.get()) {
                                        return;
                                    }
                                    Parser.Result result = resultIterator.getParserResult(startOffset);
                                    CompilationController compilationController = controller = result != null ? CompilationController.get((Parser.Result)result) : null;
                                    if (controller != null && JavaSource.Phase.PARSED.compareTo((Enum)controller.toPhase(JavaSource.Phase.PARSED)) <= 0) {
                                        TokenSequence ts;
                                        int delta;
                                        TreeUtilities tu = controller.getTreeUtilities();
                                        if (endOffset >= 0 && ((delta = (ts = SourceUtils.getJavaTokenSequence((TokenHierarchy)controller.getTokenHierarchy(), (int)startOffset)).move(startOffset)) == 0 || ts.moveNext() && ts.token().id() == JavaTokenId.WHITESPACE) && ((delta = ts.move(endOffset)) == 0 || ts.moveNext() && ts.token().id() == JavaTokenId.WHITESPACE)) {
                                            ExpressionTree expr;
                                            String selectedText = controller.getText().substring(startOffset, endOffset).trim();
                                            SourcePositions[] sp = new SourcePositions[1];
                                            ExpressionTree expressionTree = expr = selectedText.length() > 0 ? tu.parseExpression(selectedText, sp) : null;
                                            if (expr != null && expr.getKind() != Tree.Kind.IDENTIFIER && !Utilities.containErrors((Tree)expr) && sp[0].getEndPosition(null, (Tree)expr) >= (long)selectedText.length()) {
                                                JavaCodeTemplateFilter.this.stringCtx = "EXPRESSION";
                                            }
                                        }
                                        Tree tree = tu.pathFor(startOffset).getLeaf();
                                        if (endOffset >= 0 && startOffset != endOffset && tu.pathFor(endOffset).getLeaf() != tree) {
                                            return;
                                        }
                                        JavaCodeTemplateFilter.this.treeKindCtx = tree.getKind();
                                        if (JavaCodeTemplateFilter.this.treeKindCtx == Tree.Kind.CASE && (long)startOffset < controller.getTrees().getSourcePositions().getEndPosition(controller.getCompilationUnit(), (Tree)((CaseTree)tree).getExpression())) {
                                            JavaCodeTemplateFilter.this.treeKindCtx = null;
                                        } else if (JavaCodeTemplateFilter.this.treeKindCtx == Tree.Kind.CLASS) {
                                            int idx;
                                            String headerText;
                                            SourcePositions sp = controller.getTrees().getSourcePositions();
                                            int startPos = (int)sp.getEndPosition(controller.getCompilationUnit(), (Tree)((ClassTree)tree).getModifiers());
                                            if (startPos <= 0) {
                                                startPos = (int)sp.getStartPosition(controller.getCompilationUnit(), tree);
                                            }
                                            if ((idx = (headerText = controller.getText().substring(startPos, startOffset)).indexOf(123)) < 0) {
                                                JavaCodeTemplateFilter.this.treeKindCtx = null;
                                                JavaCodeTemplateFilter.this.stringCtx = "CLASS_HEADER";
                                            }
                                        }
                                    }
                                }
                            });
                        }
                        catch (ParseException ex) {
                            Exceptions.printStackTrace((Throwable)ex);
                        }
                    }

                }, (String)NbBundle.getMessage(JavaCodeTemplateProcessor.class, (String)"JCT-init"), (AtomicBoolean)cancel, (boolean)false);
            }
        }
    }

    public synchronized boolean accept(CodeTemplate template) {
        if (this.treeKindCtx == null && this.stringCtx == null) {
            return false;
        }
        EnumSet<Tree.Kind> treeKindContexts = EnumSet.noneOf(Tree.Kind.class);
        HashSet<String> stringContexts = new HashSet<String>();
        this.getTemplateContexts(template, treeKindContexts, stringContexts);
        return treeKindContexts.isEmpty() && stringContexts.isEmpty() || treeKindContexts.contains((Object)this.treeKindCtx) || stringContexts.contains(this.stringCtx);
    }

    private void getTemplateContexts(CodeTemplate template, EnumSet<Tree.Kind> treeKindContexts, HashSet<String> stringContexts) {
        List contexts = template.getContexts();
        if (contexts != null) {
            for (String context : contexts) {
                try {
                    treeKindContexts.add(Tree.Kind.valueOf((String)context));
                }
                catch (IllegalArgumentException iae) {
                    stringContexts.add(context);
                }
            }
        }
    }

    public static final class Factory
    implements CodeTemplateFilter.ContextBasedFactory {
        public CodeTemplateFilter createFilter(JTextComponent component, int offset) {
            return new JavaCodeTemplateFilter(component, offset);
        }

        public List<String> getSupportedContexts() {
            Tree.Kind[] values = Tree.Kind.values();
            ArrayList<String> contexts = new ArrayList<String>(values.length + 1);
            for (Tree.Kind value : values) {
                contexts.add(value.name());
            }
            contexts.add("CLASS_HEADER");
            Collections.sort(contexts);
            return contexts;
        }
    }

}

