/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.Acceptor
 *  org.netbeans.editor.AcceptorFactory
 *  org.netbeans.editor.TokenContext
 *  org.netbeans.editor.ext.java.JavaLayerTokenContext
 *  org.netbeans.editor.ext.java.JavaTokenContext
 */
package org.netbeans.modules.editor.java;

import java.util.Arrays;
import java.util.List;
import org.netbeans.editor.Acceptor;
import org.netbeans.editor.AcceptorFactory;
import org.netbeans.editor.TokenContext;
import org.netbeans.editor.ext.java.JavaLayerTokenContext;
import org.netbeans.editor.ext.java.JavaTokenContext;

public final class NbJavaSettingsInitializer {
    private static final Acceptor indentHotCharsAcceptor = new Acceptor(){

        public boolean accept(char ch) {
            switch (ch) {
                case '{': 
                case '}': {
                    return true;
                }
            }
            return false;
        }
    };

    public static Acceptor getAbbrevResetAcceptor() {
        return AcceptorFactory.NON_JAVA_IDENTIFIER;
    }

    public static Acceptor getIdentifierAcceptor() {
        return AcceptorFactory.JAVA_IDENTIFIER;
    }

    public static Acceptor getIndentHotCharsAcceptor() {
        return indentHotCharsAcceptor;
    }

    public static List getTokenContextList() {
        return Arrays.asList(new TokenContext[]{JavaTokenContext.context, JavaLayerTokenContext.context});
    }

    private NbJavaSettingsInitializer() {
    }

}

