/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.progress.ProgressUtils
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.editor.java;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.progress.ProgressUtils;
import org.openide.util.Exceptions;

public class ComputeOffAWT {
    public static <T> T computeOffAWT(Worker<T> w, String featureName, JavaSource source, JavaSource.Phase phase) {
        AtomicBoolean cancel = new AtomicBoolean();
        Compute<T> c = new Compute<T>(cancel, source, phase, w);
        ProgressUtils.runOffEventDispatchThread(c, (String)featureName, (AtomicBoolean)cancel, (boolean)false);
        return (T)c.result;
    }

    private ComputeOffAWT() {
    }

    public static interface Worker<T> {
        public T process(CompilationInfo var1);
    }

    private static final class Compute<T>
    implements Runnable,
    Task<CompilationController> {
        private final AtomicBoolean cancel;
        private final JavaSource source;
        private final JavaSource.Phase phase;
        private final Worker<T> worker;
        private T result;

        public Compute(AtomicBoolean cancel, JavaSource source, JavaSource.Phase phase, Worker<T> worker) {
            this.cancel = cancel;
            this.source = source;
            this.phase = phase;
            this.worker = worker;
        }

        @Override
        public void run() {
            try {
                this.source.runUserActionTask((Task)this, true);
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
                this.result = null;
            }
        }

        public void run(CompilationController parameter) throws Exception {
            if (this.cancel.get()) {
                return;
            }
            parameter.toPhase(this.phase);
            if (this.cancel.get()) {
                return;
            }
            T t = this.worker.process((CompilationInfo)parameter);
            if (this.cancel.get()) {
                return;
            }
            this.result = t;
        }
    }

}

