/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.AssignmentTree
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.EnhancedForLoopTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.Scope
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.TryTree
 *  com.sun.source.tree.TypeCastTree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreePathScanner
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.source.CodeStyle
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementUtilities
 *  org.netbeans.api.java.source.ElementUtilities$ElementAcceptor
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.ModificationResult
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.api.java.source.TreeMaker
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.TypeUtilities
 *  org.netbeans.api.java.source.WorkingCopy
 *  org.netbeans.api.java.source.ui.ElementHeaders
 *  org.netbeans.api.progress.ProgressUtils
 *  org.netbeans.lib.editor.codetemplates.spi.CodeTemplateInsertRequest
 *  org.netbeans.lib.editor.codetemplates.spi.CodeTemplateParameter
 *  org.netbeans.lib.editor.codetemplates.spi.CodeTemplateProcessor
 *  org.netbeans.lib.editor.codetemplates.spi.CodeTemplateProcessorFactory
 *  org.netbeans.modules.editor.NbEditorUtilities
 *  org.netbeans.modules.java.preprocessorbridge.api.JavaSourceUtil
 *  org.netbeans.modules.java.preprocessorbridge.api.JavaSourceUtil$Handle
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.editor.java;

import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.EnhancedForLoopTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.Scope;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TryTree;
import com.sun.source.tree.TypeCastTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreePathScanner;
import com.sun.source.util.Trees;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ErrorType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.WildcardType;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.java.source.CodeStyle;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.TreeMaker;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.TypeUtilities;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.api.java.source.ui.ElementHeaders;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateInsertRequest;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateParameter;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateProcessor;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateProcessorFactory;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.modules.editor.java.AutoImport;
import org.netbeans.modules.editor.java.JavaCompletionProvider;
import org.netbeans.modules.editor.java.Utilities;
import org.netbeans.modules.java.preprocessorbridge.api.JavaSourceUtil;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

public class JavaCodeTemplateProcessor
implements CodeTemplateProcessor {
    public static final String INSTANCE_OF = "instanceof";
    public static final String ARRAY = "array";
    public static final String ITERABLE = "iterable";
    public static final String TYPE = "type";
    public static final String TYPE_VAR = "typeVar";
    public static final String ITERABLE_ELEMENT_TYPE = "iterableElementType";
    public static final String LEFT_SIDE_TYPE = "leftSideType";
    public static final String RIGHT_SIDE_TYPE = "rightSideType";
    public static final String CAST = "cast";
    public static final String NEW_VAR_NAME = "newVarName";
    public static final String NAMED = "named";
    public static final String UNCAUGHT_EXCEPTION_TYPE = "uncaughtExceptionType";
    public static final String UNCAUGHT_EXCEPTION_CATCH_STATEMENTS = "uncaughtExceptionCatchStatements";
    public static final String CURRENT_CLASS_NAME = "currClassName";
    public static final String CURRENT_CLASS_FULLY_QUALIFIED_NAME = "currClassFQName";
    public static final String CURRENT_PACKAGE_NAME = "currPackageName";
    public static final String CURRENT_METHOD_NAME = "currMethodName";
    private static final String TRUE = "true";
    private static final String NULL = "null";
    private static final String ERROR = "<error>";
    private static final String CLASS = "class";
    private static final RequestProcessor RP = new RequestProcessor("Update Imports", 1, false, false);
    private CodeTemplateInsertRequest request;
    private int caretOffset;
    private CompilationInfo cInfo = null;
    private TreePath treePath = null;
    private Scope scope = null;
    private TypeElement enclClass = null;
    private List<Element> locals = null;
    private List<Element> typeVars = null;
    private Map<CodeTemplateParameter, String> param2hints = new HashMap<CodeTemplateParameter, String>();
    private Map<CodeTemplateParameter, TypeMirror> param2types = new HashMap<CodeTemplateParameter, TypeMirror>();
    private Set<String> autoImportedTypeNames = Collections.synchronizedSet(new HashSet());
    private AtomicReference<RequestProcessor.Task> task = new AtomicReference();

    private JavaCodeTemplateProcessor(CodeTemplateInsertRequest request) {
        this.request = request;
        for (CodeTemplateParameter param : request.getMasterParameters()) {
            if ("selection".equals(param.getName())) {
                this.initParsing();
                return;
            }
            for (String hint : param.getHints().keySet()) {
                if (!"uncaughtExceptionCatchStatements".equals(hint) && !"instanceof".equals(hint) && !"array".equals(hint) && !"iterable".equals(hint) && !"type".equals(hint) && !"iterableElementType".equals(hint) && !"leftSideType".equals(hint) && !"rightSideType".equals(hint) && !"cast".equals(hint) && !"newVarName".equals(hint) && !"currClassName".equals(hint) && !"currClassFQName".equals(hint) && !"currPackageName".equals(hint) && !"currMethodName".equals(hint) && !"iterableElementType".equals(hint) && !"uncaughtExceptionType".equals(hint)) continue;
                this.initParsing();
                return;
            }
        }
    }

    public void updateDefaultValues() {
        this.updateTemplateEnding();
        this.updateTemplateBasedOnCatchers();
        this.updateTemplateBasedOnSelection();
        boolean cont = true;
        while (cont) {
            cont = false;
            for (Object p : this.request.getMasterParameters()) {
                CodeTemplateParameter param = (CodeTemplateParameter)p;
                String value = this.getProposedValue(param);
                if (value == null || value.equals(param.getValue())) continue;
                param.setValue(value);
                cont = true;
            }
        }
        this.updateImports();
    }

    public void parameterValueChanged(CodeTemplateParameter masterParameter, boolean typingChange) {
        if (typingChange) {
            for (Object p : this.request.getMasterParameters()) {
                CodeTemplateParameter param = (CodeTemplateParameter)p;
                if (!param.isUserModified()) {
                    String value = this.getProposedValue(param);
                    if (value == null || value.equals(param.getValue())) continue;
                    param.setValue(value);
                    continue;
                }
                this.param2types.remove((Object)param);
            }
            this.updateImports();
        }
    }

    public void release() {
    }

    private void updateTemplateEnding() {
        String text = this.request.getParametrizedText();
        if (text.endsWith("\n")) {
            JTextComponent component = this.request.getComponent();
            int offset = component.getSelectionEnd();
            Document doc = component.getDocument();
            if (doc.getLength() > offset) {
                try {
                    if ("\n".equals(doc.getText(offset, 1))) {
                        this.request.setParametrizedText(text.substring(0, text.length() - 1));
                    }
                }
                catch (BadLocationException ble) {
                    // empty catch block
                }
            }
        }
    }

    private void updateTemplateBasedOnCatchers() {
        for (CodeTemplateParameter parameter : this.request.getAllParameters()) {
            for (String hint : parameter.getHints().keySet()) {
                if (!"uncaughtExceptionCatchStatements".equals(hint) || this.cInfo == null) continue;
                SourcePositions[] sourcePositions = new SourcePositions[1];
                TreeUtilities tu = this.cInfo.getTreeUtilities();
                StatementTree stmt = tu.parseStatement("{" + this.request.getInsertText() + "}", sourcePositions);
                if (Utilities.containErrors((Tree)stmt)) continue;
                TreePath path = tu.pathFor(new TreePath(this.treePath, (Tree)stmt), parameter.getInsertTextOffset(), sourcePositions[0]);
                if ((path = Utilities.getPathElementOfKind(Tree.Kind.TRY, path)) == null || ((TryTree)path.getLeaf()).getBlock() == null) continue;
                tu.attributeTree((Tree)stmt, this.scope);
                StringBuilder sb = new StringBuilder();
                int cnt = 0;
                for (TypeMirror tm : tu.getUncaughtExceptions(new TreePath(path, (Tree)((TryTree)path.getLeaf()).getBlock()))) {
                    sb.append("catch (");
                    sb.append("${_GEN_UCE_TYPE_" + cnt++ + " type=" + Utilities.getTypeName(this.cInfo, tm, true) + " default=" + Utilities.getTypeName(this.cInfo, tm, false) + "}");
                    sb.append(" ${_GEN_UCE_NAME_" + cnt++ + " newVarName}){}");
                }
                if (sb.length() <= 0) continue;
                StringBuilder ptBuilder = new StringBuilder(this.request.getParametrizedText());
                ptBuilder.replace(parameter.getParametrizedTextStartOffset(), parameter.getParametrizedTextEndOffset(), sb.toString());
                this.request.setParametrizedText(ptBuilder.toString());
            }
        }
    }

    private void updateTemplateBasedOnSelection() {
        block0 : for (CodeTemplateParameter parameter : this.request.getAllParameters()) {
            TreePath treePath;
            int idx;
            Tree tree;
            if (!"selection".equals(parameter.getName())) continue;
            JTextComponent component = this.request.getComponent();
            if (component.getSelectionStart() == component.getSelectionEnd() || this.cInfo == null) break;
            TreeUtilities tu = this.cInfo.getTreeUtilities();
            StatementTree stat = tu.parseStatement(this.request.getInsertText(), null);
            EnumSet<Tree.Kind[]> kinds = EnumSet.of(Tree.Kind.BLOCK, new Tree.Kind[]{Tree.Kind.DO_WHILE_LOOP, Tree.Kind.ENHANCED_FOR_LOOP, Tree.Kind.FOR_LOOP, Tree.Kind.IF, Tree.Kind.SYNCHRONIZED, Tree.Kind.TRY, Tree.Kind.WHILE_LOOP});
            if (stat == null || !kinds.contains((Object)stat.getKind()) || (tree = (treePath = tu.pathFor(component.getSelectionStart())).getLeaf()).getKind() != Tree.Kind.BLOCK || tree != tu.pathFor(component.getSelectionEnd()).getLeaf()) break;
            String selection = component.getSelectedText();
            for (idx = 0; idx < selection.length() && selection.charAt(idx) <= ' '; ++idx) {
            }
            StringBuilder selectionText = new StringBuilder(parameter.getValue().trim());
            int caretOffset = component.getSelectionStart() + idx;
            final StringBuilder sb = new StringBuilder();
            final Trees trees = this.cInfo.getTrees();
            SourcePositions sp = trees.getSourcePositions();
            final HashMap<VariableElement, VariableTree> vars = new HashMap<VariableElement, VariableTree>();
            LinkedList<VariableTree> varList = new LinkedList<VariableTree>();
            TreePathScanner scanner = new TreePathScanner(){
                private int cnt;

                public Object visitIdentifier(IdentifierTree node, Object p) {
                    VariableTree var;
                    Element e = trees.getElement(this.getCurrentPath());
                    if (e != null && (var = (VariableTree)vars.remove(e)) != null) {
                        sb.append((Object)var.getType()).append(' ').append(var.getName());
                        TypeMirror tm = ((VariableElement)e).asType();
                        switch (tm.getKind()) {
                            case ARRAY: 
                            case DECLARED: {
                                sb.append(" = ${_GEN_PARAM_" + this.cnt++ + " default=\"null\"}");
                                break;
                            }
                            case BOOLEAN: {
                                sb.append(" = ${_GEN_PARAM_" + this.cnt++ + " default=\"false\"}");
                                break;
                            }
                            case BYTE: 
                            case CHAR: 
                            case DOUBLE: 
                            case FLOAT: 
                            case INT: 
                            case LONG: 
                            case SHORT: {
                                sb.append(" = ${_GEN_PARAM_" + this.cnt++ + " default=\"0\"}");
                            }
                        }
                        sb.append(";\n");
                    }
                    return null;
                }
            };
            for (StatementTree st : ((BlockTree)tree).getStatements()) {
                if (sp.getStartPosition(this.cInfo.getCompilationUnit(), (Tree)st) < (long)component.getSelectionStart()) continue;
                if (sp.getEndPosition(this.cInfo.getCompilationUnit(), (Tree)st) <= (long)component.getSelectionEnd()) {
                    Element e;
                    if (st.getKind() != Tree.Kind.VARIABLE || (e = trees.getElement(new TreePath(treePath, (Tree)st))) == null || e.getKind() != ElementKind.LOCAL_VARIABLE) continue;
                    vars.put((VariableElement)e, (VariableTree)st);
                    varList.addFirst((VariableTree)st);
                    continue;
                }
                scanner.scan(new TreePath(treePath, (Tree)st), (Object)null);
            }
            Collection vals = vars.values();
            for (VariableTree var : varList) {
                if (vals.contains((Object)var)) continue;
                int start = (int)sp.getStartPosition(this.cInfo.getCompilationUnit(), (Tree)var) - caretOffset;
                int end = (int)sp.getEndPosition(this.cInfo.getCompilationUnit(), var.getType()) - caretOffset;
                selectionText.delete(start, end);
            }
            if (sb.length() <= 0) break;
            this.request.setParametrizedText(sb.toString() + this.request.getParametrizedText());
            for (CodeTemplateParameter p : this.request.getAllParameters()) {
                if (!"selection".equals(p.getName())) continue;
                p.setValue(selectionText.toString());
                break block0;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void updateImports() {
        RequestProcessor.Task oldTask;
        AutoImport imp = AutoImport.get(this.cInfo);
        for (Map.Entry<CodeTemplateParameter, TypeMirror> entry22 : this.param2types.entrySet()) {
            CodeTemplateParameter param = entry22.getKey();
            TypeMirror tm = this.param2types.get((Object)param);
            TreePath tp = this.cInfo.getTreeUtilities().pathFor(this.caretOffset + param.getInsertTextOffset());
            CharSequence typeName = imp.resolveImport(tp, tm);
            if ("cast".equals(this.param2hints.get((Object)param))) {
                param.setValue("(" + typeName + ")");
                continue;
            }
            if ("instanceof".equals(this.param2hints.get((Object)param))) {
                String value = param.getValue().substring(param.getValue().lastIndexOf(46) + 1);
                param.setValue(typeName + "." + value);
                continue;
            }
            param.setValue(typeName.toString());
        }
        final HashSet<String> toRemove = new HashSet<String>();
        Set<String> entry22 = this.autoImportedTypeNames;
        synchronized (entry22) {
            toRemove.addAll(this.autoImportedTypeNames);
            this.autoImportedTypeNames.addAll(imp.getAutoImportedTypes());
        }
        if (!toRemove.isEmpty() && (oldTask = this.task.getAndSet(RP.post(new Runnable(){

            @Override
            public void run() {
                try {
                    JavaCompletionProvider.JavaCompletionQuery.javadocBreak.set(true);
                    ModificationResult.runModificationTask(Collections.singleton(JavaCodeTemplateProcessor.this.cInfo.getSnapshot().getSource()), (UserTask)new UserTask(){

                        public void run(ResultIterator resultIterator) throws Exception {
                            WorkingCopy copy = WorkingCopy.get((Parser.Result)resultIterator.getParserResult());
                            copy.toPhase(JavaSource.Phase.RESOLVED);
                            for (Element usedElement : Utilities.getUsedElements((CompilationInfo)copy)) {
                                switch (usedElement.getKind()) {
                                    case CLASS: 
                                    case INTERFACE: 
                                    case ENUM: 
                                    case ANNOTATION_TYPE: {
                                        toRemove.remove(((TypeElement)usedElement).getQualifiedName().toString());
                                    }
                                }
                            }
                            TreeMaker tm = copy.getTreeMaker();
                            CompilationUnitTree cut = copy.getCompilationUnit();
                            block4 : for (String typeName : toRemove) {
                                for (ImportTree importTree : cut.getImports()) {
                                    if (importTree.isStatic() || !typeName.equals(importTree.getQualifiedIdentifier().toString())) continue;
                                    cut = tm.removeCompUnitImport(cut, importTree);
                                    continue block4;
                                }
                            }
                            copy.rewrite((Tree)copy.getCompilationUnit(), (Tree)cut);
                        }
                    }).commit();
                    JavaCodeTemplateProcessor.this.autoImportedTypeNames.removeAll(toRemove);
                }
                catch (Exception e) {
                    Exceptions.printStackTrace((Throwable)e);
                }
            }

        }))) != null) {
            oldTask.cancel();
        }
    }

    private String getProposedValue(CodeTemplateParameter param) {
        this.param2hints.remove((Object)param);
        this.param2types.remove((Object)param);
        String name = null;
        for (Map.Entry e : param.getHints().entrySet()) {
            String value;
            VariableElement ve;
            TypeMirror tm;
            Map.Entry entry = e;
            if ("instanceof".equals(entry.getKey())) {
                ve = this.instanceOf((String)entry.getValue(), name);
                if (ve != null) {
                    this.param2hints.put(param, "instanceof");
                    return ve.getSimpleName().toString();
                }
                if (name == null) continue;
                ve = this.staticInstanceOf((String)entry.getValue(), name);
                if (ve != null) {
                    TypeMirror tm2 = ve.getEnclosingElement().asType();
                    tm2 = this.cInfo.getTypes().erasure(tm2);
                    String value2 = tm2 != null ? Utilities.getTypeName(this.cInfo, tm2, true) + "." + ve.getSimpleName() : null;
                    if (value2 == null) continue;
                    this.param2hints.put(param, "instanceof");
                    if (this.containsDeclaredType(tm2)) {
                        this.param2types.put(param, tm2);
                    }
                    return value2;
                }
                return this.valueOf((String)entry.getValue());
            }
            if ("array".equals(entry.getKey())) {
                ve = this.array();
                if (ve == null) continue;
                this.param2hints.put(param, "array");
                return ve.getSimpleName().toString();
            }
            if ("iterable".equals(entry.getKey())) {
                ve = this.iterable();
                if (ve == null) continue;
                this.param2hints.put(param, "iterable");
                return ve.getSimpleName().toString();
            }
            if ("type".equals(entry.getKey())) {
                TypeParameterElement tpe;
                tm = this.type((String)entry.getValue());
                if (tm == null || tm.getKind() == TypeKind.ERROR) continue;
                if (name != null && (tpe = this.typeVar(tm, name)) != null) {
                    return tpe.getSimpleName().toString();
                }
                value = (tm = this.resolveCapturedType(tm)) != null ? Utilities.getTypeName(this.cInfo, tm, true).toString() : null;
                if (value == null) continue;
                this.param2hints.put(param, "type");
                if (this.containsDeclaredType(tm)) {
                    this.param2types.put(param, tm);
                }
                return value;
            }
            if ("typeVar".equals(entry.getKey())) {
                name = (String)entry.getValue();
                continue;
            }
            if ("iterableElementType".equals(entry.getKey())) {
                tm = this.iterableElementType(param.getInsertTextOffset() + 1);
                if (tm == null || tm.getKind() == TypeKind.ERROR || (value = (tm = this.resolveCapturedType(tm)) != null ? Utilities.getTypeName(this.cInfo, tm, true).toString() : null) == null) continue;
                this.param2hints.put(param, "iterableElementType");
                if (this.containsDeclaredType(tm)) {
                    this.param2types.put(param, tm);
                }
                return value;
            }
            if ("leftSideType".equals(entry.getKey())) {
                tm = this.assignmentSideType(param.getInsertTextOffset() + 1, true);
                if (tm == null || tm.getKind() == TypeKind.ERROR || (value = (tm = this.resolveCapturedType(tm)) != null ? Utilities.getTypeName(this.cInfo, tm, true).toString() : null) == null) continue;
                this.param2hints.put(param, "leftSideType");
                if (this.containsDeclaredType(tm)) {
                    this.param2types.put(param, tm);
                }
                return value;
            }
            if ("rightSideType".equals(entry.getKey())) {
                tm = this.assignmentSideType(param.getInsertTextOffset() + 1, false);
                if (tm == null || tm.getKind() == TypeKind.ERROR || (value = (tm = this.resolveCapturedType(tm)) != null ? Utilities.getTypeName(this.cInfo, tm, true).toString() : null) == null) continue;
                this.param2hints.put(param, "rightSideType");
                if (this.containsDeclaredType(tm)) {
                    this.param2types.put(param, tm);
                }
                return value;
            }
            if ("cast".equals(entry.getKey())) {
                tm = this.cast(param.getInsertTextOffset() + 1);
                if (tm == null) {
                    this.param2hints.put(param, "cast");
                    this.param2types.remove((Object)param);
                    return "";
                }
                if (tm.getKind() == TypeKind.ERROR || (value = (tm = this.resolveCapturedType(tm)) != null ? Utilities.getTypeName(this.cInfo, tm, true).toString() : null) == null) continue;
                this.param2hints.put(param, "cast");
                if (this.containsDeclaredType(tm)) {
                    this.param2types.put(param, tm);
                }
                return "(" + value + ")";
            }
            if ("newVarName".equals(entry.getKey())) {
                this.param2hints.put(param, "newVarName");
                Object value3 = entry.getValue();
                if (!(value3 instanceof String) || "true".equals(value3)) {
                    value3 = null;
                }
                return this.newVarName(param.getInsertTextOffset() + 1, (String)value3);
            }
            if ("currClassName".equals(entry.getKey())) {
                this.param2hints.put(param, "currClassName");
                return this.owningClassName(false);
            }
            if ("currClassFQName".equals(entry.getKey())) {
                this.param2hints.put(param, "currClassFQName");
                return this.owningClassName(true);
            }
            if ("currPackageName".equals(entry.getKey())) {
                this.param2hints.put(param, "currPackageName");
                return this.owningPackageName();
            }
            if ("currMethodName".equals(entry.getKey())) {
                this.param2hints.put(param, "currMethodName");
                return this.owningMethodName();
            }
            if ("named".equals(entry.getKey())) {
                name = param.getName();
                continue;
            }
            if (!"uncaughtExceptionType".equals(entry.getKey()) || (tm = this.uncaughtExceptionType(param.getInsertTextOffset() + 1)) == null || tm.getKind() == TypeKind.ERROR || (value = (tm = this.resolveCapturedType(tm)) != null ? Utilities.getTypeName(this.cInfo, tm, true).toString() : null) == null) continue;
            this.param2hints.put(param, "uncaughtExceptionType");
            if (this.containsDeclaredType(tm)) {
                this.param2types.put(param, tm);
            }
            return value;
        }
        return name;
    }

    private VariableElement instanceOf(String typeName, String name) {
        try {
            if (this.cInfo != null) {
                TypeMirror type = this.type(typeName);
                VariableElement closest = null;
                int distance = Integer.MAX_VALUE;
                if (type != null) {
                    Types types = this.cInfo.getTypes();
                    for (Element e : this.locals) {
                        if (!(e instanceof VariableElement) || "<error>".contentEquals(e.getSimpleName()) || e.asType().getKind() == TypeKind.ERROR || !types.isAssignable(e.asType(), type)) continue;
                        if (name == null) {
                            return (VariableElement)e;
                        }
                        int d = ElementHeaders.getDistance((String)e.getSimpleName().toString(), (String)name);
                        if (this.isSameType(e.asType(), type, types)) {
                            d -= 1000;
                        }
                        if (d >= distance) continue;
                        distance = d;
                        closest = (VariableElement)e;
                    }
                }
                return closest;
            }
        }
        catch (Exception e) {
            // empty catch block
        }
        return null;
    }

    private VariableElement staticInstanceOf(String typeName, String name) {
        try {
            if (this.cInfo != null) {
                final Trees trees = this.cInfo.getTrees();
                TypeMirror type = this.type(typeName);
                VariableElement closest = null;
                int distance = Integer.MAX_VALUE;
                if (type != null) {
                    final Types types = this.cInfo.getTypes();
                    if (type.getKind() == TypeKind.DECLARED) {
                        final DeclaredType dType = (DeclaredType)type;
                        TypeElement element = (TypeElement)dType.asElement();
                        final boolean isStatic = element.getKind().isClass() || element.getKind().isInterface();
                        ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                            public boolean accept(Element e, TypeMirror t) {
                                return e.getKind().isField() && !"<error>".contentEquals(e.getSimpleName()) && !"class".contentEquals(e.getSimpleName()) && (!isStatic || e.getModifiers().contains((Object)Modifier.STATIC)) && trees.isAccessible(JavaCodeTemplateProcessor.this.scope, e, (DeclaredType)t) && (e.getKind().isField() && e.asType().getKind() != TypeKind.ERROR && types.isAssignable(e.asType(), dType) || e.getKind() == ElementKind.METHOD && ((ExecutableElement)e).getReturnType().getKind() != TypeKind.ERROR && types.isAssignable(((ExecutableElement)e).getReturnType(), dType));
                            }
                        };
                        for (Element ee : this.cInfo.getElementUtilities().getMembers((TypeMirror)dType, acceptor)) {
                            if (name == null) {
                                return (VariableElement)ee;
                            }
                            int d = ElementHeaders.getDistance((String)ee.getSimpleName().toString(), (String)name);
                            if (ee.getKind().isField() && this.isSameType(((VariableElement)ee).asType(), dType, types) || ee.getKind() == ElementKind.METHOD && this.isSameType(((ExecutableElement)ee).getReturnType(), dType, types)) {
                                d -= 1000;
                            }
                            if (d >= distance) continue;
                            distance = d;
                            closest = (VariableElement)ee;
                        }
                    }
                }
                return closest;
            }
        }
        catch (Exception e) {
            // empty catch block
        }
        return null;
    }

    private TypeParameterElement typeVar(TypeMirror type, String name) {
        try {
            if (this.cInfo != null) {
                TypeParameterElement closest = null;
                int distance = Integer.MAX_VALUE;
                if (type != null) {
                    Types types = this.cInfo.getTypes();
                    for (Element e : this.typeVars) {
                        if (!(e instanceof TypeParameterElement) || "<error>".contentEquals(e.getSimpleName()) || !types.isAssignable(e.asType(), type)) continue;
                        int d = ElementHeaders.getDistance((String)e.getSimpleName().toString(), (String)name);
                        if (this.isSameType(e.asType(), type, types)) {
                            d -= 1000;
                        }
                        if (d >= distance) continue;
                        distance = d;
                        closest = (TypeParameterElement)e;
                    }
                }
                return closest;
            }
        }
        catch (Exception e) {
            // empty catch block
        }
        return null;
    }

    private String valueOf(String typeName) {
        try {
            TypeMirror type;
            if (this.cInfo != null && (type = this.type(typeName)) != null) {
                if (type.getKind() == TypeKind.DECLARED) {
                    return "null";
                }
                if (type.getKind() == TypeKind.BOOLEAN) {
                    return "true";
                }
            }
        }
        catch (Exception e) {
            // empty catch block
        }
        return null;
    }

    private VariableElement array() {
        if (this.cInfo != null) {
            for (Element e : this.locals) {
                if (!(e instanceof VariableElement) || "<error>".contentEquals(e.getSimpleName()) || e.asType().getKind() != TypeKind.ARRAY) continue;
                return (VariableElement)e;
            }
        }
        return null;
    }

    private VariableElement iterable() {
        TypeElement iterableTE;
        if (this.cInfo != null && (iterableTE = this.cInfo.getElements().getTypeElement("java.lang.Iterable")) != null) {
            DeclaredType iterableType = this.cInfo.getTypes().getDeclaredType(iterableTE, new TypeMirror[0]);
            for (Element e : this.locals) {
                if (!(e instanceof VariableElement) || "<error>".contentEquals(e.getSimpleName()) || e.asType().getKind() != TypeKind.ARRAY && !this.cInfo.getTypes().isAssignable(e.asType(), iterableType)) continue;
                return (VariableElement)e;
            }
        }
        return null;
    }

    private TypeMirror type(String typeName) {
        try {
            typeName = typeName.trim();
            if (this.cInfo != null && typeName.length() > 0) {
                StatementTree var;
                List stmts;
                SourcePositions[] sourcePositions = new SourcePositions[1];
                TreeUtilities tu = this.cInfo.getTreeUtilities();
                StatementTree stmt = tu.parseStatement("{" + typeName + " a;}", sourcePositions);
                if (!Utilities.containErrors((Tree)stmt) && stmt.getKind() == Tree.Kind.BLOCK && !(stmts = ((BlockTree)stmt).getStatements()).isEmpty() && (var = (StatementTree)stmts.get(0)).getKind() == Tree.Kind.VARIABLE) {
                    tu.attributeTree((Tree)stmt, this.scope);
                    TypeMirror ret = this.cInfo.getTrees().getTypeMirror(new TreePath(this.treePath, ((VariableTree)var).getType()));
                    if (ret != null) {
                        return ret;
                    }
                }
                return this.cInfo.getTreeUtilities().parseType(typeName, this.enclClass);
            }
        }
        catch (Exception e) {
            // empty catch block
        }
        return null;
    }

    private TypeMirror iterableElementType(int caretOffset) {
        try {
            if (this.cInfo != null) {
                SourcePositions[] sourcePositions = new SourcePositions[1];
                TreeUtilities tu = this.cInfo.getTreeUtilities();
                StatementTree stmt = tu.parseStatement("{" + this.request.getInsertText() + "}", sourcePositions);
                if (Utilities.containErrors((Tree)stmt)) {
                    return null;
                }
                TreePath path = tu.pathFor(new TreePath(this.treePath, (Tree)stmt), caretOffset + 1, sourcePositions[0]);
                TreePath loop = Utilities.getPathElementOfKind(Tree.Kind.ENHANCED_FOR_LOOP, path);
                if (loop != null) {
                    tu.attributeTree((Tree)stmt, this.scope);
                    TypeMirror type = this.cInfo.getTrees().getTypeMirror(new TreePath(loop, (Tree)((EnhancedForLoopTree)loop.getLeaf()).getExpression()));
                    switch (type.getKind()) {
                        case ARRAY: {
                            type = ((ArrayType)type).getComponentType();
                            return type;
                        }
                        case DECLARED: {
                            Iterator<? extends TypeMirror> types;
                            DeclaredType dt = this.findIterableType(type);
                            if (dt != null && (types = dt.getTypeArguments().iterator()).hasNext()) {
                                return types.next();
                            }
                            return this.cInfo.getElements().getTypeElement("java.lang.Object").asType();
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            // empty catch block
        }
        return null;
    }

    private TypeMirror assignmentSideType(int caretOffset, boolean left) {
        try {
            if (this.cInfo != null) {
                SourcePositions[] sourcePositions = new SourcePositions[1];
                TreeUtilities tu = this.cInfo.getTreeUtilities();
                StatementTree stmt = tu.parseStatement("{" + this.request.getInsertText() + "}", sourcePositions);
                if (Utilities.containErrors((Tree)stmt)) {
                    return null;
                }
                TreePath path = tu.pathFor(new TreePath(this.treePath, (Tree)stmt), caretOffset + 1, sourcePositions[0]);
                TreePath tree = Utilities.getPathElementOfKind(EnumSet.of(Tree.Kind.ASSIGNMENT, Tree.Kind.VARIABLE), path);
                if (tree == null) {
                    return null;
                }
                tu.attributeTree((Tree)stmt, this.scope);
                TypeMirror tm = null;
                if (tree.getLeaf().getKind() == Tree.Kind.ASSIGNMENT) {
                    AssignmentTree as = (AssignmentTree)tree.getLeaf();
                    TreePath type = new TreePath(tree, (Tree)(left ? as.getVariable() : as.getExpression()));
                    tm = this.cInfo.getTrees().getTypeMirror(type);
                } else {
                    VariableTree vd = (VariableTree)tree.getLeaf();
                    TreePath type = new TreePath(tree, left ? vd.getType() : vd.getInitializer());
                    tm = this.cInfo.getTrees().getTypeMirror(type);
                }
                if (tm != null && tm.getKind() == TypeKind.ERROR) {
                    tm = this.cInfo.getTrees().getOriginalType((ErrorType)tm);
                }
                if (tm.getKind() == TypeKind.NONE) {
                    tm = this.cInfo.getElements().getTypeElement("java.lang.Object").asType();
                }
                return tm;
            }
        }
        catch (Exception e) {
            // empty catch block
        }
        return null;
    }

    private TypeMirror cast(int caretOffset) {
        try {
            if (this.cInfo != null) {
                TypeMirror right;
                SourcePositions[] sourcePositions = new SourcePositions[1];
                TreeUtilities tu = this.cInfo.getTreeUtilities();
                StatementTree stmt = tu.parseStatement("{" + this.request.getInsertText() + "}", sourcePositions);
                if (Utilities.containErrors((Tree)stmt)) {
                    return null;
                }
                TreePath path = tu.pathFor(new TreePath(this.treePath, (Tree)stmt), caretOffset + 1, sourcePositions[0]);
                TreePath tree = Utilities.getPathElementOfKind(EnumSet.of(Tree.Kind.ASSIGNMENT, Tree.Kind.VARIABLE), path);
                if (tree == null) {
                    return null;
                }
                tu.attributeTree((Tree)stmt, this.scope);
                if (tree.getLeaf().getKind() == Tree.Kind.ASSIGNMENT) {
                    TypeMirror right2;
                    AssignmentTree as = (AssignmentTree)tree.getLeaf();
                    TypeMirror left = this.cInfo.getTrees().getTypeMirror(new TreePath(tree, (Tree)as.getVariable()));
                    if (left == null) {
                        return null;
                    }
                    TreePath exp = new TreePath(tree, (Tree)as.getExpression());
                    if (exp.getLeaf() instanceof TypeCastTree) {
                        exp = new TreePath(exp, (Tree)((TypeCastTree)exp.getLeaf()).getExpression());
                    }
                    if ((right2 = this.cInfo.getTrees().getTypeMirror(exp)) == null) {
                        return null;
                    }
                    if (right2.getKind() == TypeKind.ERROR) {
                        right2 = this.cInfo.getTrees().getOriginalType((ErrorType)right2);
                    }
                    if (this.cInfo.getTypes().isAssignable(right2, left)) {
                        return null;
                    }
                    return left;
                }
                VariableTree vd = (VariableTree)tree.getLeaf();
                TypeMirror left = this.cInfo.getTrees().getTypeMirror(new TreePath(tree, vd.getType()));
                if (left == null) {
                    return null;
                }
                TreePath exp = new TreePath(tree, (Tree)vd.getInitializer());
                if (exp.getLeaf() instanceof TypeCastTree) {
                    exp = new TreePath(exp, (Tree)((TypeCastTree)exp.getLeaf()).getExpression());
                }
                if ((right = this.cInfo.getTrees().getTypeMirror(exp)) == null) {
                    return null;
                }
                if (right.getKind() == TypeKind.ERROR) {
                    right = this.cInfo.getTrees().getOriginalType((ErrorType)right);
                }
                if (this.cInfo.getTypes().isAssignable(right, left) || !this.cInfo.getTypeUtilities().isCastable(right, left)) {
                    return null;
                }
                return left;
            }
        }
        catch (Exception e) {
            // empty catch block
        }
        return null;
    }

    private String newVarName(int caretOffset, String suggestedName) {
        try {
            if (this.cInfo != null) {
                SourcePositions[] sourcePositions = new SourcePositions[1];
                TreeUtilities tu = this.cInfo.getTreeUtilities();
                StatementTree stmt = tu.parseStatement("{" + this.request.getInsertText() + "}", sourcePositions);
                if (Utilities.containErrors((Tree)stmt)) {
                    return null;
                }
                TreePath path = tu.pathFor(new TreePath(this.treePath, (Tree)stmt), caretOffset + 1, sourcePositions[0]);
                TreePath decl = Utilities.getPathElementOfKind(Tree.Kind.VARIABLE, path);
                if (decl != null) {
                    String name;
                    final Scope s = tu.attributeTreeTo((Tree)stmt, this.scope, decl.getLeaf());
                    TypeMirror type = this.cInfo.getTrees().getTypeMirror(decl);
                    final Element element = this.cInfo.getTrees().getElement(decl);
                    final ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                        public boolean accept(Element e, TypeMirror t) {
                            switch (e.getKind()) {
                                case EXCEPTION_PARAMETER: 
                                case LOCAL_VARIABLE: 
                                case RESOURCE_VARIABLE: 
                                case PARAMETER: {
                                    return element != e;
                                }
                            }
                            return false;
                        }
                    };
                    Iterable<Element> loc = new Iterable<Element>(){

                        @Override
                        public Iterator<Element> iterator() {
                            return new Iterator<Element>(){
                                private Iterator<? extends Element> localsIt;
                                private Iterator<? extends Element> localVarsIt;

                                @Override
                                public boolean hasNext() {
                                    if (this.localsIt != null) {
                                        if (this.localsIt.hasNext()) {
                                            return true;
                                        }
                                        this.localsIt = null;
                                        this.localVarsIt = JavaCodeTemplateProcessor.this.cInfo.getElementUtilities().getLocalVars(s, acceptor).iterator();
                                    }
                                    return this.localVarsIt.hasNext();
                                }

                                @Override
                                public Element next() {
                                    return this.localsIt != null ? this.localsIt.next() : this.localVarsIt.next();
                                }

                                @Override
                                public void remove() {
                                    throw new UnsupportedOperationException("Not supported yet.");
                                }
                            };
                        }

                    };
                    Iterator<String> names = Utilities.varNamesSuggestions(type, element.getKind(), ((VariableTree)decl.getLeaf()).getModifiers().getFlags(), (name = Utilities.varNameSuggestion(decl)) != null ? name : suggestedName, null, this.cInfo.getTypes(), this.cInfo.getElements(), loc, CodeStyle.getDefault((Document)this.request.getComponent().getDocument())).iterator();
                    if (names.hasNext()) {
                        return names.next();
                    }
                }
            }
        }
        catch (Exception e) {
            // empty catch block
        }
        return null;
    }

    private String owningClassName(boolean fqn) {
        try {
            if (this.cInfo != null) {
                TreePath path = this.treePath;
                while ((path = Utilities.getPathElementOfKind(TreeUtilities.CLASS_TREE_KINDS, path)) != null) {
                    Name name;
                    Element element = this.cInfo.getTrees().getElement(path);
                    if (element == null || !element.getKind().isClass() && !element.getKind().isInterface() || (name = fqn ? ((TypeElement)element).getQualifiedName() : ((TypeElement)element).getSimpleName()) == null) continue;
                    return name.toString();
                }
            }
        }
        catch (Exception e) {
            // empty catch block
        }
        return null;
    }

    private String owningPackageName() {
        try {
            if (this.cInfo != null) {
                String result;
                ExpressionTree packageName = this.treePath.getCompilationUnit().getPackageName();
                String string = result = packageName != null ? packageName.toString() : null;
                if (result != null && !result.equals("<error>")) {
                    return result;
                }
            }
        }
        catch (Exception e) {
            // empty catch block
        }
        return null;
    }

    private String owningMethodName() {
        try {
            if (this.cInfo != null) {
                TreePath path = this.treePath;
                while ((path = Utilities.getPathElementOfKind(Tree.Kind.METHOD, path)) != null) {
                    MethodTree tree = (MethodTree)path.getLeaf();
                    String result = tree.getName().toString();
                    if (result.length() > 0) {
                        return result;
                    }
                    path = path.getParentPath();
                }
                return null;
            }
        }
        catch (Exception e) {
            // empty catch block
        }
        return null;
    }

    private TypeMirror uncaughtExceptionType(int caretOffset) {
        try {
            if (this.cInfo != null) {
                SourcePositions[] sourcePositions = new SourcePositions[1];
                TreeUtilities tu = this.cInfo.getTreeUtilities();
                StatementTree stmt = tu.parseStatement("{" + this.request.getInsertText() + "}", sourcePositions);
                if (Utilities.containErrors((Tree)stmt)) {
                    return null;
                }
                TreePath path = tu.pathFor(new TreePath(this.treePath, (Tree)stmt), caretOffset + 1, sourcePositions[0]);
                if ((path = Utilities.getPathElementOfKind(Tree.Kind.TRY, path)) != null && ((TryTree)path.getLeaf()).getBlock() != null) {
                    tu.attributeTree((Tree)stmt, this.scope);
                    Iterator excs = tu.getUncaughtExceptions(new TreePath(path, (Tree)((TryTree)path.getLeaf()).getBlock())).iterator();
                    if (excs.hasNext()) {
                        return (TypeMirror)excs.next();
                    }
                }
            }
        }
        catch (Exception e) {
            // empty catch block
        }
        return null;
    }

    private boolean containsDeclaredType(TypeMirror type) {
        switch (type.getKind()) {
            case ARRAY: {
                return this.containsDeclaredType(((ArrayType)type).getComponentType());
            }
            case DECLARED: {
                return true;
            }
        }
        return false;
    }

    private DeclaredType findIterableType(TypeMirror type) {
        if (type == null || type.getKind() != TypeKind.DECLARED) {
            return null;
        }
        TypeElement te = (TypeElement)((DeclaredType)type).asElement();
        if ("java.lang.Iterable".contentEquals(te.getQualifiedName())) {
            return (DeclaredType)type;
        }
        for (TypeMirror tm : this.cInfo.getTypes().directSupertypes(type)) {
            DeclaredType dt = this.findIterableType(tm);
            if (dt == null) continue;
            return dt;
        }
        return null;
    }

    private boolean isSameType(TypeMirror t1, TypeMirror t2, Types types) {
        if (types.isSameType(t1, t2)) {
            return true;
        }
        if (t1.getKind().isPrimitive() && types.isSameType(types.boxedClass((PrimitiveType)t1).asType(), t2)) {
            return true;
        }
        if (t2.getKind().isPrimitive() && types.isSameType(t1, types.boxedClass((PrimitiveType)t1).asType())) {
            return true;
        }
        return false;
    }

    private TypeMirror resolveCapturedType(TypeMirror type) {
        WildcardType wildcard;
        if (type.getKind() == TypeKind.TYPEVAR && (wildcard = SourceUtils.resolveCapturedType((TypeMirror)type)) != null) {
            return wildcard.getExtendsBound();
        }
        return type;
    }

    private void initParsing() {
        if (this.cInfo == null) {
            JTextComponent c = this.request.getComponent();
            this.caretOffset = c.getSelectionStart();
            Document doc = c.getDocument();
            final FileObject fo = NbEditorUtilities.getFileObject((Document)doc);
            if (fo != null) {
                final AtomicBoolean cancel = new AtomicBoolean();
                ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

                    @Override
                    public void run() {
                        try {
                            boolean isStatic;
                            Iterator it;
                            CompilationUnitTree cut;
                            if (JavaCodeTemplateProcessor.this.cInfo != null || cancel.get()) {
                                return;
                            }
                            CompilationController controller = (CompilationController)JavaSourceUtil.createControllerHandle((FileObject)fo, (JavaSourceUtil.Handle)null).getCompilationController();
                            controller.toPhase(JavaSource.Phase.RESOLVED);
                            final TreeUtilities tu = controller.getTreeUtilities();
                            JavaCodeTemplateProcessor.this.treePath = tu.pathFor(JavaCodeTemplateProcessor.this.caretOffset);
                            JavaCodeTemplateProcessor.this.scope = tu.scopeFor(JavaCodeTemplateProcessor.this.caretOffset);
                            JavaCodeTemplateProcessor.this.enclClass = JavaCodeTemplateProcessor.this.scope.getEnclosingClass();
                            boolean bl = isStatic = JavaCodeTemplateProcessor.this.enclClass != null ? tu.isStaticContext(JavaCodeTemplateProcessor.this.scope) : false;
                            if (JavaCodeTemplateProcessor.this.enclClass == null && (it = (cut = JavaCodeTemplateProcessor.this.treePath.getCompilationUnit()).getTypeDecls().iterator()).hasNext()) {
                                JavaCodeTemplateProcessor.this.enclClass = (TypeElement)controller.getTrees().getElement(TreePath.getPath((CompilationUnitTree)cut, (Tree)((Tree)it.next())));
                            }
                            Trees trees = controller.getTrees();
                            SourcePositions sp = trees.getSourcePositions();
                            Collection<? extends Element> illegalForwardRefs = Utilities.getForwardReferences(JavaCodeTemplateProcessor.this.treePath, JavaCodeTemplateProcessor.this.caretOffset, sp, trees);
                            final HashSet<Name> illegalForwardRefNames = new HashSet<Name>(illegalForwardRefs.size());
                            for (Element element : illegalForwardRefs) {
                                illegalForwardRefNames.add(element.getSimpleName());
                            }
                            final ExecutableElement method = JavaCodeTemplateProcessor.this.scope.getEnclosingMethod();
                            ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                                public boolean accept(Element e, TypeMirror t) {
                                    switch (e.getKind()) {
                                        case TYPE_PARAMETER: {
                                            return true;
                                        }
                                        case EXCEPTION_PARAMETER: 
                                        case LOCAL_VARIABLE: 
                                        case RESOURCE_VARIABLE: 
                                        case PARAMETER: {
                                            return (method == e.getEnclosingElement() || e.getModifiers().contains((Object)Modifier.FINAL)) && !illegalForwardRefNames.contains(e.getSimpleName());
                                        }
                                        case FIELD: {
                                            if (e.getSimpleName().contentEquals("this")) {
                                                return !isStatic && e.asType().getKind() == TypeKind.DECLARED && ((DeclaredType)e.asType()).asElement() == JavaCodeTemplateProcessor.this.enclClass;
                                            }
                                            if (e.getSimpleName().contentEquals("super")) {
                                                return false;
                                            }
                                            if (!illegalForwardRefNames.contains(e.getSimpleName())) break;
                                            return false;
                                        }
                                    }
                                    return (!isStatic || e.getModifiers().contains((Object)Modifier.STATIC)) && tu.isAccessible(JavaCodeTemplateProcessor.this.scope, e, (TypeMirror)((DeclaredType)t));
                                }
                            };
                            JavaCodeTemplateProcessor.this.locals = new ArrayList();
                            JavaCodeTemplateProcessor.this.typeVars = new ArrayList();
                            block6 : for (Element element2 : controller.getElementUtilities().getLocalMembersAndVars(JavaCodeTemplateProcessor.this.scope, acceptor)) {
                                switch (element2.getKind()) {
                                    case TYPE_PARAMETER: {
                                        JavaCodeTemplateProcessor.this.typeVars.add(element2);
                                        continue block6;
                                    }
                                }
                                JavaCodeTemplateProcessor.this.locals.add(element2);
                            }
                            JavaCodeTemplateProcessor.this.cInfo = (CompilationInfo)controller;
                        }
                        catch (IOException ioe) {
                            Exceptions.printStackTrace((Throwable)ioe);
                        }
                    }

                }, (String)NbBundle.getMessage(JavaCodeTemplateProcessor.class, (String)"JCT-init"), (AtomicBoolean)cancel, (boolean)false);
            }
        }
    }

    public static final class Factory
    implements CodeTemplateProcessorFactory {
        public CodeTemplateProcessor createProcessor(CodeTemplateInsertRequest request) {
            return new JavaCodeTemplateProcessor(request);
        }
    }

}

