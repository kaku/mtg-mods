/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.support.ReferencesCount
 *  org.netbeans.api.whitelist.WhiteListQuery
 *  org.netbeans.api.whitelist.WhiteListQuery$WhiteList
 *  org.netbeans.modules.parsing.api.Source
 */
package org.netbeans.modules.editor.java;

import java.util.EnumSet;
import java.util.List;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ExecutableType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.support.ReferencesCount;
import org.netbeans.api.whitelist.WhiteListQuery;
import org.netbeans.modules.editor.java.JavaCompletionItem;
import org.netbeans.modules.editor.java.LazyJavaCompletionItem;
import org.netbeans.modules.parsing.api.Source;

public interface JavaCompletionItemFactory {
    public JavaCompletionItem createKeywordItem(String var1, String var2, int var3, boolean var4);

    public JavaCompletionItem createPackageItem(String var1, int var2, boolean var3);

    public JavaCompletionItem createTypeItem(CompilationInfo var1, TypeElement var2, DeclaredType var3, int var4, ReferencesCount var5, boolean var6, boolean var7, boolean var8, boolean var9, boolean var10, boolean var11, WhiteListQuery.WhiteList var12);

    public JavaCompletionItem createTypeItem(ElementHandle<TypeElement> var1, EnumSet<ElementKind> var2, int var3, ReferencesCount var4, Source var5, boolean var6, boolean var7, boolean var8, WhiteListQuery.WhiteList var9);

    public JavaCompletionItem createArrayItem(CompilationInfo var1, ArrayType var2, int var3, ReferencesCount var4, Elements var5, WhiteListQuery.WhiteList var6);

    public JavaCompletionItem createTypeParameterItem(TypeParameterElement var1, int var2);

    public JavaCompletionItem createVariableItem(CompilationInfo var1, VariableElement var2, TypeMirror var3, int var4, ReferencesCount var5, boolean var6, boolean var7, boolean var8, int var9, WhiteListQuery.WhiteList var10);

    public JavaCompletionItem createVariableItem(CompilationInfo var1, String var2, int var3, boolean var4, boolean var5);

    public JavaCompletionItem createExecutableItem(CompilationInfo var1, ExecutableElement var2, ExecutableType var3, int var4, ReferencesCount var5, boolean var6, boolean var7, boolean var8, boolean var9, boolean var10, int var11, boolean var12, WhiteListQuery.WhiteList var13);

    public JavaCompletionItem createThisOrSuperConstructorItem(CompilationInfo var1, ExecutableElement var2, ExecutableType var3, int var4, boolean var5, String var6, WhiteListQuery.WhiteList var7);

    public JavaCompletionItem createOverrideMethodItem(CompilationInfo var1, ExecutableElement var2, ExecutableType var3, int var4, boolean var5, WhiteListQuery.WhiteList var6);

    public JavaCompletionItem createGetterSetterMethodItem(CompilationInfo var1, VariableElement var2, TypeMirror var3, int var4, String var5, boolean var6);

    public JavaCompletionItem createDefaultConstructorItem(TypeElement var1, int var2, boolean var3);

    public JavaCompletionItem createParametersItem(CompilationInfo var1, ExecutableElement var2, ExecutableType var3, int var4, boolean var5, int var6, String var7);

    public JavaCompletionItem createAnnotationItem(CompilationInfo var1, TypeElement var2, DeclaredType var3, int var4, ReferencesCount var5, boolean var6, WhiteListQuery.WhiteList var7);

    public JavaCompletionItem createAttributeItem(CompilationInfo var1, ExecutableElement var2, ExecutableType var3, int var4, boolean var5);

    public JavaCompletionItem createAttributeValueItem(CompilationInfo var1, String var2, String var3, TypeElement var4, int var5, ReferencesCount var6, WhiteListQuery.WhiteList var7);

    public JavaCompletionItem createStaticMemberItem(CompilationInfo var1, DeclaredType var2, Element var3, TypeMirror var4, boolean var5, int var6, boolean var7, boolean var8, WhiteListQuery.WhiteList var9);

    public JavaCompletionItem createStaticMemberItem(ElementHandle<TypeElement> var1, String var2, int var3, boolean var4, ReferencesCount var5, Source var6, WhiteListQuery.WhiteList var7);

    public JavaCompletionItem createChainedMembersItem(CompilationInfo var1, List<? extends Element> var2, List<? extends TypeMirror> var3, int var4, boolean var5, boolean var6, WhiteListQuery.WhiteList var7);

    public JavaCompletionItem createInitializeAllConstructorItem(CompilationInfo var1, boolean var2, Iterable<? extends VariableElement> var3, ExecutableElement var4, TypeElement var5, int var6);

    public static final class DefaultImpl
    implements JavaCompletionItemFactory {
        @Override
        public JavaCompletionItem createKeywordItem(String kwd, String postfix, int substitutionOffset, boolean smartType) {
            return JavaCompletionItem.createKeywordItem(kwd, postfix, substitutionOffset, smartType);
        }

        @Override
        public JavaCompletionItem createPackageItem(String pkgFQN, int substitutionOffset, boolean inPackageStatement) {
            return JavaCompletionItem.createPackageItem(pkgFQN, substitutionOffset, inPackageStatement);
        }

        @Override
        public JavaCompletionItem createTypeItem(CompilationInfo info, TypeElement elem, DeclaredType type, int substitutionOffset, ReferencesCount referencesCount, boolean isDeprecated, boolean insideNew, boolean addTypeVars, boolean addSimpleName, boolean smartType, boolean autoImportEnclosingType, WhiteListQuery.WhiteList whiteList) {
            return JavaCompletionItem.createTypeItem(info, elem, type, substitutionOffset, referencesCount, isDeprecated, insideNew, addTypeVars, addSimpleName, smartType, autoImportEnclosingType, whiteList);
        }

        @Override
        public JavaCompletionItem createTypeItem(ElementHandle<TypeElement> handle, EnumSet<ElementKind> kinds, int substitutionOffset, ReferencesCount referencesCount, Source source, boolean insideNew, boolean addTypeVars, boolean afterExtends, WhiteListQuery.WhiteList whiteList) {
            return LazyJavaCompletionItem.createTypeItem(handle, kinds, substitutionOffset, referencesCount, source, insideNew, addTypeVars, afterExtends, whiteList);
        }

        @Override
        public JavaCompletionItem createArrayItem(CompilationInfo info, ArrayType type, int substitutionOffset, ReferencesCount referencesCount, Elements elements, WhiteListQuery.WhiteList whiteList) {
            return JavaCompletionItem.createArrayItem(info, type, substitutionOffset, referencesCount, elements, whiteList);
        }

        @Override
        public JavaCompletionItem createTypeParameterItem(TypeParameterElement elem, int substitutionOffset) {
            return JavaCompletionItem.createTypeParameterItem(elem, substitutionOffset);
        }

        @Override
        public JavaCompletionItem createVariableItem(CompilationInfo info, VariableElement elem, TypeMirror type, int substitutionOffset, ReferencesCount referencesCount, boolean isInherited, boolean isDeprecated, boolean smartType, int assignToVarOffset, WhiteListQuery.WhiteList whiteList) {
            return JavaCompletionItem.createVariableItem(info, elem, type, substitutionOffset, referencesCount, isInherited, isDeprecated, smartType, assignToVarOffset, whiteList);
        }

        @Override
        public JavaCompletionItem createVariableItem(CompilationInfo info, String varName, int substitutionOffset, boolean newVarName, boolean smartType) {
            return JavaCompletionItem.createVariableItem(info, varName, substitutionOffset, newVarName, smartType);
        }

        @Override
        public JavaCompletionItem createExecutableItem(CompilationInfo info, ExecutableElement elem, ExecutableType type, int substitutionOffset, ReferencesCount referencesCount, boolean isInherited, boolean isDeprecated, boolean inImport, boolean addSemicolon, boolean smartType, int assignToVarOffset, boolean memberRef, WhiteListQuery.WhiteList whiteList) {
            return JavaCompletionItem.createExecutableItem(info, elem, type, substitutionOffset, referencesCount, isInherited, isDeprecated, inImport, addSemicolon, smartType, assignToVarOffset, memberRef, whiteList);
        }

        @Override
        public JavaCompletionItem createThisOrSuperConstructorItem(CompilationInfo info, ExecutableElement elem, ExecutableType type, int substitutionOffset, boolean isDeprecated, String name, WhiteListQuery.WhiteList whiteList) {
            return JavaCompletionItem.createThisOrSuperConstructorItem(info, elem, type, substitutionOffset, isDeprecated, name, whiteList);
        }

        @Override
        public JavaCompletionItem createOverrideMethodItem(CompilationInfo info, ExecutableElement elem, ExecutableType type, int substitutionOffset, boolean implement, WhiteListQuery.WhiteList whiteList) {
            return JavaCompletionItem.createOverrideMethodItem(info, elem, type, substitutionOffset, implement, whiteList);
        }

        @Override
        public JavaCompletionItem createGetterSetterMethodItem(CompilationInfo info, VariableElement elem, TypeMirror type, int substitutionOffset, String name, boolean setter) {
            return JavaCompletionItem.createGetterSetterMethodItem(info, elem, type, substitutionOffset, name, setter);
        }

        @Override
        public JavaCompletionItem createDefaultConstructorItem(TypeElement elem, int substitutionOffset, boolean smartType) {
            return JavaCompletionItem.createDefaultConstructorItem(elem, substitutionOffset, smartType);
        }

        @Override
        public JavaCompletionItem createParametersItem(CompilationInfo info, ExecutableElement elem, ExecutableType type, int substitutionOffset, boolean isDeprecated, int activeParamIndex, String name) {
            return JavaCompletionItem.createParametersItem(info, elem, type, substitutionOffset, isDeprecated, activeParamIndex, name);
        }

        @Override
        public JavaCompletionItem createAnnotationItem(CompilationInfo info, TypeElement elem, DeclaredType type, int substitutionOffset, ReferencesCount referencesCount, boolean isDeprecated, WhiteListQuery.WhiteList whiteList) {
            return JavaCompletionItem.createAnnotationItem(info, elem, type, substitutionOffset, referencesCount, isDeprecated, whiteList);
        }

        @Override
        public JavaCompletionItem createAttributeItem(CompilationInfo info, ExecutableElement elem, ExecutableType type, int substitutionOffset, boolean isDeprecated) {
            return JavaCompletionItem.createAttributeItem(info, elem, type, substitutionOffset, isDeprecated);
        }

        @Override
        public JavaCompletionItem createAttributeValueItem(CompilationInfo info, String value, String documentation, TypeElement element, int substitutionOffset, ReferencesCount referencesCount, WhiteListQuery.WhiteList whiteList) {
            return JavaCompletionItem.createAttributeValueItem(info, value, documentation, element, substitutionOffset, referencesCount, whiteList);
        }

        @Override
        public JavaCompletionItem createStaticMemberItem(CompilationInfo info, DeclaredType type, Element memberElem, TypeMirror memberType, boolean multipleVersions, int substitutionOffset, boolean isDeprecated, boolean addSemicolon, WhiteListQuery.WhiteList whiteList) {
            return JavaCompletionItem.createStaticMemberItem(info, type, memberElem, memberType, multipleVersions, substitutionOffset, isDeprecated, addSemicolon, whiteList);
        }

        @Override
        public JavaCompletionItem createStaticMemberItem(ElementHandle<TypeElement> handle, String name, int substitutionOffset, boolean addSemicolon, ReferencesCount referencesCount, Source source, WhiteListQuery.WhiteList whiteList) {
            return LazyJavaCompletionItem.createStaticMemberItem(handle, name, substitutionOffset, addSemicolon, referencesCount, source, whiteList);
        }

        @Override
        public JavaCompletionItem createChainedMembersItem(CompilationInfo info, List<? extends Element> chainedElems, List<? extends TypeMirror> chainedTypes, int substitutionOffset, boolean isDeprecated, boolean addSemicolon, WhiteListQuery.WhiteList whiteList) {
            return JavaCompletionItem.createChainedMembersItem(info, chainedElems, chainedTypes, substitutionOffset, isDeprecated, addSemicolon, whiteList);
        }

        @Override
        public JavaCompletionItem createInitializeAllConstructorItem(CompilationInfo info, boolean isDefault, Iterable<? extends VariableElement> fields, ExecutableElement superConstructor, TypeElement parent, int substitutionOffset) {
            return JavaCompletionItem.createInitializeAllConstructorItem(info, isDefault, fields, superConstructor, parent, substitutionOffset);
        }
    }

}

