/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.FoldHierarchy
 *  org.netbeans.api.editor.fold.FoldType
 *  org.netbeans.api.editor.fold.FoldUtilities
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.java.queries.SourceLevelQuery
 *  org.netbeans.api.java.source.CodeStyle
 *  org.netbeans.editor.BaseAction
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.BaseKit
 *  org.netbeans.editor.BaseTokenID
 *  org.netbeans.editor.SyntaxUpdateTokens
 *  org.netbeans.editor.SyntaxUpdateTokens$TokenInfo
 *  org.netbeans.editor.TokenContextPath
 *  org.netbeans.editor.TokenID
 *  org.netbeans.editor.Utilities
 *  org.netbeans.editor.ext.ExtKit
 *  org.netbeans.editor.ext.ExtKit$GotoDeclarationAction
 *  org.netbeans.editor.ext.ExtKit$PrefixMakerAction
 *  org.netbeans.editor.ext.ExtKit$ToggleCommentAction
 *  org.netbeans.editor.ext.java.JavaFoldManager
 *  org.netbeans.editor.ext.java.JavaTokenContext
 *  org.netbeans.lib.editor.codetemplates.api.CodeTemplateManager
 *  org.netbeans.modules.editor.MainMenuAction
 *  org.netbeans.modules.editor.NbEditorKit
 *  org.netbeans.modules.editor.NbEditorKit$GenerateFoldPopupAction
 *  org.netbeans.modules.editor.NbEditorKit$NbGenerateGoToPopupAction
 *  org.netbeans.modules.editor.NbEditorUtilities
 *  org.netbeans.modules.editor.indent.api.Indent
 *  org.netbeans.spi.editor.typinghooks.DeletedTextInterceptor
 *  org.netbeans.spi.editor.typinghooks.DeletedTextInterceptor$Context
 *  org.netbeans.spi.editor.typinghooks.DeletedTextInterceptor$Factory
 *  org.netbeans.spi.editor.typinghooks.TypedBreakInterceptor
 *  org.netbeans.spi.editor.typinghooks.TypedBreakInterceptor$Context
 *  org.netbeans.spi.editor.typinghooks.TypedBreakInterceptor$Factory
 *  org.netbeans.spi.editor.typinghooks.TypedBreakInterceptor$MutableContext
 *  org.netbeans.spi.editor.typinghooks.TypedTextInterceptor
 *  org.netbeans.spi.editor.typinghooks.TypedTextInterceptor$Context
 *  org.netbeans.spi.editor.typinghooks.TypedTextInterceptor$Factory
 *  org.netbeans.spi.editor.typinghooks.TypedTextInterceptor$MutableContext
 *  org.openide.awt.Mnemonics
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.editor.java;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;
import javax.swing.text.TextAction;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.api.editor.fold.FoldUtilities;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.java.queries.SourceLevelQuery;
import org.netbeans.api.java.source.CodeStyle;
import org.netbeans.editor.BaseAction;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.BaseTokenID;
import org.netbeans.editor.SyntaxUpdateTokens;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.ext.ExtKit;
import org.netbeans.editor.ext.java.JavaFoldManager;
import org.netbeans.editor.ext.java.JavaTokenContext;
import org.netbeans.lib.editor.codetemplates.api.CodeTemplateManager;
import org.netbeans.modules.editor.MainMenuAction;
import org.netbeans.modules.editor.NbEditorKit;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.modules.editor.indent.api.Indent;
import org.netbeans.modules.editor.java.GoToSupport;
import org.netbeans.modules.editor.java.JavaMoveCodeElementAction;
import org.netbeans.modules.editor.java.SelectCodeElementAction;
import org.netbeans.modules.editor.java.TypingCompletion;
import org.netbeans.modules.java.editor.codegen.InsertSemicolonAction;
import org.netbeans.modules.java.editor.imports.ClipboardHandler;
import org.netbeans.modules.java.editor.imports.FastImportAction;
import org.netbeans.modules.java.editor.imports.JavaFixAllImports;
import org.netbeans.modules.java.editor.overridden.GoToSuperTypeAction;
import org.netbeans.modules.java.editor.rename.InstantRenameAction;
import org.netbeans.modules.java.editor.semantic.GoToMarkOccurrencesAction;
import org.netbeans.spi.editor.typinghooks.DeletedTextInterceptor;
import org.netbeans.spi.editor.typinghooks.TypedBreakInterceptor;
import org.netbeans.spi.editor.typinghooks.TypedTextInterceptor;
import org.openide.awt.Mnemonics;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class JavaKit
extends NbEditorKit {
    public static final String JAVA_MIME_TYPE = "text/x-java";
    static final long serialVersionUID = -5445829962533684922L;
    private static final boolean INSTANT = Boolean.getBoolean("org.netbeans.modules.java.refactoring.instantRename");
    private static final String[] getSetIsPrefixes = new String[]{"get", "set", "is"};
    public static final String makeGetterAction = "make-getter";
    public static final String makeSetterAction = "make-setter";
    public static final String makeIsAction = "make-is";
    public static final String addWatchAction = "add-watch";
    public static final String toggleBreakpointAction = "toggle-breakpoint";
    public static final String abbrevDebugLineAction = "abbrev-debug-line";
    public static final String fixImportsAction = "fix-imports";
    public static final String fastImportAction = "fast-import";
    public static final String tryCatchAction = "try-catch";
    public static final String javaDocShowAction = "javadoc-show-action";
    public static final String expandAllJavadocFolds = "expand-all-javadoc-folds";
    public static final String collapseAllJavadocFolds = "collapse-all-javadoc-folds";
    public static final String expandAllCodeBlockFolds = "expand-all-code-block-folds";
    public static final String collapseAllCodeBlockFolds = "collapse-all-code-block-folds";
    public static final String selectNextElementAction = "select-element-next";
    public static final String selectPreviousElementAction = "select-element-previous";

    public String getContentType() {
        return "text/x-java";
    }

    public String getSourceLevel(BaseDocument doc) {
        DataObject dob = NbEditorUtilities.getDataObject((Document)doc);
        return dob != null ? SourceLevelQuery.getSourceLevel((FileObject)dob.getPrimaryFile()) : null;
    }

    protected void initDocument(BaseDocument doc) {
        doc.putProperty(SyntaxUpdateTokens.class, (Object)new SyntaxUpdateTokens(){
            private List tokenList;

            public void syntaxUpdateStart() {
                this.tokenList.clear();
            }

            public List syntaxUpdateEnd() {
                return this.tokenList;
            }

            public void syntaxUpdateToken(TokenID id, TokenContextPath contextPath, int offset, int length) {
                if (JavaTokenContext.LINE_COMMENT == id) {
                    this.tokenList.add(new SyntaxUpdateTokens.TokenInfo((SyntaxUpdateTokens)this, id, contextPath, offset, length));
                }
            }
        });
        CodeTemplateManager.get((Document)doc);
    }

    protected Action[] createActions() {
        Action[] superActions = super.createActions();
        BaseAction[] actions = new BaseAction[]{new ExtKit.PrefixMakerAction("make-getter", "get", getSetIsPrefixes), new ExtKit.PrefixMakerAction("make-setter", "set", getSetIsPrefixes), new ExtKit.PrefixMakerAction("make-is", "is", getSetIsPrefixes), new ExtKit.ToggleCommentAction("//"), new JavaGenerateFoldPopupAction(), new InstantRenameAction(), new InsertSemicolonAction(true), new InsertSemicolonAction(false), new SelectCodeElementAction("select-element-next", true), new SelectCodeElementAction("select-element-previous", false), new JavaMoveCodeElementAction("move-code-element-up", false), new JavaMoveCodeElementAction("move-code-element-down", true), new FastImportAction(), new GoToSuperTypeAction(), new GoToMarkOccurrencesAction(false), new GoToMarkOccurrencesAction(true), new ClipboardHandler.JavaCutAction()};
        Action[] value = TextAction.augmentList(superActions, (Action[])actions);
        return !INSTANT ? value : this.removeInstant(value);
    }

    private Action[] removeInstant(Action[] actions) {
        LinkedList<Action> value = new LinkedList<Action>();
        for (Action action : actions) {
            if (action instanceof InstantRenameAction) continue;
            value.add(action);
        }
        return value.toArray(new Action[value.size()]);
    }

    public void install(JEditorPane c) {
        super.install(c);
        ClipboardHandler.install(c);
    }

    public static class JavaGotoHelpAction
    extends BaseAction {
        public JavaGotoHelpAction() {
            super(142);
            this.putValue("helpID", (Object)JavaGotoHelpAction.class.getName());
        }

        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                GoToSupport.goToJavadoc(target.getDocument(), target.getCaretPosition());
            }
        }

        public String getPopupMenuText(JTextComponent target) {
            return NbBundle.getBundle(JavaKit.class).getString("show_javadoc");
        }
    }

    public static class JavaFixImports
    extends BaseAction {
        public JavaFixImports() {
            super(14);
            this.putValue("trimmed-text", (Object)NbBundle.getBundle(JavaKit.class).getString("fix-imports-trimmed"));
            this.putValue("ShortDescription", (Object)NbBundle.getBundle(JavaKit.class).getString("desc-fix-imports"));
            this.putValue("PopupMenuText", (Object)NbBundle.getBundle(JavaKit.class).getString("popup-fix-imports"));
        }

        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            Object source;
            Document doc;
            if (target != null && (source = (doc = target.getDocument()).getProperty("stream")) instanceof DataObject) {
                FileObject fo = ((DataObject)source).getPrimaryFile();
                JavaFixAllImports.getDefault().fixAllImports(fo, target);
            }
        }

        public static final class GlobalAction
        extends MainMenuAction {
            public GlobalAction() {
                this.postSetMenu();
            }

            protected String getMenuItemText() {
                return NbBundle.getBundle(GlobalAction.class).getString("fix-imports-main-menu-source-item");
            }

            protected String getActionName() {
                return "fix-imports";
            }
        }

    }

    public static class JavaGoToSourceAction
    extends BaseAction {
        static final long serialVersionUID = -6440495023918097760L;

        public JavaGoToSourceAction() {
            super(142);
        }

        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null && target.getDocument() instanceof BaseDocument) {
                GoToSupport.goTo((Document)((BaseDocument)target.getDocument()), target.getCaretPosition(), true);
            }
        }

        public String getPopupMenuText(JTextComponent target) {
            return NbBundle.getBundle(JavaKit.class).getString("goto_source_open_source_not_formatted");
        }

        protected Class getShortDescriptionBundleClass() {
            return BaseKit.class;
        }
    }

    @Deprecated
    public static class JavaGoToDeclarationAction
    extends ExtKit.GotoDeclarationAction {
        public boolean gotoDeclaration(JTextComponent target) {
            if (!(target.getDocument() instanceof BaseDocument)) {
                return false;
            }
            GoToSupport.goTo((Document)((BaseDocument)target.getDocument()), target.getCaretPosition(), false);
            return true;
        }
    }

    public static class JavaGenerateFoldPopupAction
    extends NbEditorKit.GenerateFoldPopupAction {
        protected void addAdditionalItems(JTextComponent target, JMenu menu) {
            this.addAction(target, menu, "collapse-all-javadoc-folds");
            this.addAction(target, menu, "expand-all-javadoc-folds");
            this.setAddSeparatorBeforeNextAction(true);
            this.addAction(target, menu, "collapse-all-code-block-folds");
            this.addAction(target, menu, "expand-all-code-block-folds");
        }
    }

    public static class CollapseAllCodeBlockFolds
    extends BaseAction {
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            FoldHierarchy hierarchy = FoldHierarchy.get((JTextComponent)target);
            ArrayList<FoldType> types = new ArrayList<FoldType>();
            types.add(JavaFoldManager.CODE_BLOCK_FOLD_TYPE);
            types.add(JavaFoldManager.IMPORTS_FOLD_TYPE);
            FoldUtilities.collapse((FoldHierarchy)hierarchy, types);
        }
    }

    public static class ExpandAllCodeBlockFolds
    extends BaseAction {
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            FoldHierarchy hierarchy = FoldHierarchy.get((JTextComponent)target);
            ArrayList<FoldType> types = new ArrayList<FoldType>();
            types.add(JavaFoldManager.CODE_BLOCK_FOLD_TYPE);
            types.add(JavaFoldManager.IMPORTS_FOLD_TYPE);
            FoldUtilities.expand((FoldHierarchy)hierarchy, types);
        }
    }

    public static class CollapseAllJavadocFolds
    extends BaseAction {
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            FoldHierarchy hierarchy = FoldHierarchy.get((JTextComponent)target);
            FoldUtilities.collapse((FoldHierarchy)hierarchy, (FoldType)JavaFoldManager.JAVADOC_FOLD_TYPE);
        }
    }

    public static class ExpandAllJavadocFolds
    extends BaseAction {
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            FoldHierarchy hierarchy = FoldHierarchy.get((JTextComponent)target);
            FoldUtilities.expand((FoldHierarchy)hierarchy, (FoldType)JavaFoldManager.JAVADOC_FOLD_TYPE);
        }
    }

    public static class JavaTypedTextInterceptor
    implements TypedTextInterceptor {
        private int caretPosition = -1;

        public boolean beforeInsert(TypedTextInterceptor.Context context) throws BadLocationException {
            return false;
        }

        public void insert(TypedTextInterceptor.MutableContext context) throws BadLocationException {
            char insertedChar = context.getText().charAt(0);
            switch (insertedChar) {
                case '(': 
                case '[': {
                    if (!TypingCompletion.isCompletionSettingEnabled()) break;
                    TypingCompletion.completeOpeningBracket(context);
                    break;
                }
                case ')': 
                case ']': {
                    if (!TypingCompletion.isCompletionSettingEnabled()) break;
                    this.caretPosition = TypingCompletion.skipClosingBracket(context);
                    break;
                }
                case ';': {
                    if (!TypingCompletion.isCompletionSettingEnabled()) break;
                    this.caretPosition = TypingCompletion.moveOrSkipSemicolon(context);
                    break;
                }
                case '\"': 
                case '\'': {
                    if (!TypingCompletion.isCompletionSettingEnabled()) break;
                    this.caretPosition = TypingCompletion.completeQuote(context);
                }
            }
        }

        public void afterInsert(TypedTextInterceptor.Context context) throws BadLocationException {
            if (this.caretPosition != -1) {
                context.getComponent().setCaretPosition(this.caretPosition);
                this.caretPosition = -1;
            }
        }

        public void cancelled(TypedTextInterceptor.Context context) {
        }

        public static class Factory
        implements TypedTextInterceptor.Factory {
            public TypedTextInterceptor createTypedTextInterceptor(MimePath mimePath) {
                return new JavaTypedTextInterceptor();
            }
        }

    }

    public static class JavaDeletedTextInterceptor
    implements DeletedTextInterceptor {
        public boolean beforeRemove(DeletedTextInterceptor.Context context) throws BadLocationException {
            return false;
        }

        public void remove(DeletedTextInterceptor.Context context) throws BadLocationException {
            char removedChar = context.getText().charAt(0);
            switch (removedChar) {
                case '(': 
                case '[': {
                    if (!TypingCompletion.isCompletionSettingEnabled()) break;
                    TypingCompletion.removeBrackets(context);
                    break;
                }
                case '\"': 
                case '\'': {
                    if (!TypingCompletion.isCompletionSettingEnabled()) break;
                    TypingCompletion.removeCompletedQuote(context);
                }
            }
        }

        public void afterRemove(DeletedTextInterceptor.Context context) throws BadLocationException {
        }

        public void cancelled(DeletedTextInterceptor.Context context) {
        }

        public static class Factory
        implements DeletedTextInterceptor.Factory {
            public DeletedTextInterceptor createDeletedTextInterceptor(MimePath mimePath) {
                return new JavaDeletedTextInterceptor();
            }
        }

    }

    public static class JavaTypedBreakInterceptor
    implements TypedBreakInterceptor {
        private boolean isJavadocTouched = false;

        public boolean beforeInsert(TypedBreakInterceptor.Context context) throws BadLocationException {
            return false;
        }

        public void insert(TypedBreakInterceptor.MutableContext context) throws BadLocationException {
            int dotPos = context.getCaretOffset();
            Document doc = context.getDocument();
            if (TypingCompletion.posWithinString(doc, dotPos)) {
                if (CodeStyle.getDefault((Document)doc).wrapAfterBinaryOps()) {
                    context.setText("\" +\n \"", 3, 6, new int[0]);
                } else {
                    context.setText("\"\n + \"", 1, 6, new int[0]);
                }
                return;
            }
            BaseDocument baseDoc = (BaseDocument)context.getDocument();
            if (TypingCompletion.isCompletionSettingEnabled() && TypingCompletion.isAddRightBrace(baseDoc, dotPos)) {
                boolean[] insert = new boolean[]{true};
                int end = TypingCompletion.getRowOrBlockEnd(baseDoc, dotPos, insert);
                if (insert[0]) {
                    doc.insertString(end, "}", null);
                    Indent.get((Document)doc).indentNewLine(end);
                }
                context.getComponent().getCaret().setDot(dotPos);
            } else {
                if (TypingCompletion.blockCommentCompletion((TypedBreakInterceptor.Context)context)) {
                    this.blockCommentComplete(doc, dotPos, context);
                }
                this.isJavadocTouched = TypingCompletion.javadocBlockCompletion((TypedBreakInterceptor.Context)context);
                if (this.isJavadocTouched) {
                    this.blockCommentComplete(doc, dotPos, context);
                }
            }
        }

        public void afterInsert(TypedBreakInterceptor.Context context) throws BadLocationException {
            if (this.isJavadocTouched) {
                Lookup.Result res = MimeLookup.getLookup((MimePath)MimePath.parse((String)"text/x-javadoc")).lookupResult(TextAction.class);
                ActionEvent newevt = new ActionEvent(context.getComponent(), 1001, "fix-javadoc");
                for (TextAction action : res.allInstances()) {
                    action.actionPerformed(newevt);
                }
                this.isJavadocTouched = false;
            }
        }

        public void cancelled(TypedBreakInterceptor.Context context) {
        }

        private void blockCommentComplete(Document doc, int dotPos, TypedBreakInterceptor.MutableContext context) throws BadLocationException {
            doc.insertString(dotPos, "*/", null);
            Indent.get((Document)doc).indentNewLine(dotPos);
            context.getComponent().getCaret().setDot(dotPos);
        }

        public static class JavaFactory
        implements TypedBreakInterceptor.Factory {
            public TypedBreakInterceptor createTypedBreakInterceptor(MimePath mimePath) {
                return new JavaTypedBreakInterceptor();
            }
        }

    }

    public static class AbbrevDebugLineAction
    extends BaseAction {
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                BaseDocument doc = (BaseDocument)target.getDocument();
                StringBuffer sb = new StringBuffer("System.out.println(\"");
                String title = (String)doc.getProperty((Object)"title");
                if (title != null) {
                    sb.append(title);
                    sb.append(':');
                }
                try {
                    sb.append(Utilities.getLineOffset((BaseDocument)doc, (int)target.getCaret().getDot()) + 1);
                }
                catch (BadLocationException e) {
                    // empty catch block
                }
                sb.append(' ');
                BaseKit kit = Utilities.getKit((JTextComponent)target);
                if (kit == null) {
                    return;
                }
                Action a = kit.getActionByName("insert-content");
                if (a != null) {
                    Utilities.performAction((Action)a, (ActionEvent)new ActionEvent(target, 1001, sb.toString()), (JTextComponent)target);
                }
            }
        }
    }

    public static class JavaGenerateGoToPopupAction
    extends NbEditorKit.NbGenerateGoToPopupAction {
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
        }

        private void addAcceleretors(Action a, JMenuItem item, JTextComponent target) {
            Keymap km = target.getKeymap();
            if (km != null) {
                KeyStroke ks;
                KeyStroke[] keys = km.getKeyStrokesForAction(a);
                if (keys != null && keys.length > 0) {
                    item.setAccelerator(keys[0]);
                } else if (a != null && (ks = (KeyStroke)a.getValue("AcceleratorKey")) != null) {
                    item.setAccelerator(ks);
                }
            }
        }

        private void addAction(JTextComponent target, JMenu menu, Action a) {
            if (a != null) {
                String actionName = (String)a.getValue("Name");
                JMenuItem item = null;
                if (a instanceof BaseAction) {
                    item = ((BaseAction)a).getPopupMenuItem(target);
                }
                if (item == null) {
                    String itemText = (String)a.getValue("trimmed-text");
                    if (itemText == null) {
                        itemText = this.getItemText(target, actionName, a);
                    }
                    if (itemText != null) {
                        item = new JMenuItem(itemText);
                        Mnemonics.setLocalizedText((AbstractButton)item, (String)itemText);
                        item.addActionListener(a);
                        this.addAcceleretors(a, item, target);
                        item.setEnabled(a.isEnabled());
                        Object helpID = a.getValue("helpID");
                        if (helpID != null && helpID instanceof String) {
                            item.putClientProperty("HelpID", helpID);
                        }
                    } else if ("goto-source".equals(actionName)) {
                        item = new JMenuItem(NbBundle.getBundle(JavaKit.class).getString("goto_source_open_source_not_formatted"));
                        this.addAcceleretors(a, item, target);
                        item.setEnabled(false);
                    }
                }
                if (item != null) {
                    menu.add(item);
                }
            }
        }

        protected void addAction(JTextComponent target, JMenu menu, String actionName) {
            BaseKit kit = Utilities.getKit((JTextComponent)target);
            if (kit == null) {
                return;
            }
            Action a = kit.getActionByName(actionName);
            if (a != null) {
                this.addAction(target, menu, a);
            } else {
                menu.addSeparator();
            }
        }

        protected String getItemText(JTextComponent target, String actionName, Action a) {
            String itemText = a instanceof BaseAction ? ((BaseAction)a).getPopupMenuText(target) : actionName;
            return itemText;
        }

        public JMenuItem getPopupMenuItem(JTextComponent target) {
            String menuText = NbBundle.getBundle(JavaKit.class).getString("generate-goto-popup");
            JMenu jm = new JMenu(menuText);
            this.addAction(target, jm, "goto-source");
            this.addAction(target, jm, "goto-declaration");
            this.addAction(target, jm, "goto-super-implementation");
            this.addAction(target, jm, "goto");
            return jm;
        }
    }

}

