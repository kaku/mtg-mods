/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenChange
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenHierarchyEvent
 *  org.netbeans.api.lexer.TokenHierarchyEventType
 *  org.netbeans.api.lexer.TokenHierarchyListener
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 */
package org.netbeans.modules.editor.java;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.text.Document;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenChange;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenHierarchyEvent;
import org.netbeans.api.lexer.TokenHierarchyEventType;
import org.netbeans.api.lexer.TokenHierarchyListener;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;

class TokenBalance
implements TokenHierarchyListener {
    private final Document doc;
    private final Map<Language<?>, LanguageHandler<?>> lang2handler;
    private boolean scanDone;

    public static <T extends TokenId> TokenBalance get(Document doc) {
        TokenBalance tb = (TokenBalance)doc.getProperty(TokenBalance.class);
        if (tb == null) {
            tb = new TokenBalance(doc);
            doc.putProperty(TokenBalance.class, tb);
        }
        return tb;
    }

    private TokenBalance(Document doc) {
        this.doc = doc;
        this.lang2handler = new HashMap();
        TokenHierarchy hi = TokenHierarchy.get((Document)doc);
        hi.addTokenHierarchyListener((TokenHierarchyListener)this);
    }

    public boolean isTracked(Language<?> language) {
        return this.handler(language, false) != null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public <T extends TokenId> void addTokenPair(Language<T> language, T left, T right) {
        Map map = this.lang2handler;
        synchronized (map) {
            this.handler(language, true).addTokenPair(left, right);
            this.scanDone = false;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void tokenHierarchyChanged(TokenHierarchyEvent evt) {
        Map map = this.lang2handler;
        synchronized (map) {
            if (evt.type() == TokenHierarchyEventType.ACTIVITY || evt.type() == TokenHierarchyEventType.REBUILD) {
                this.scanDone = false;
            } else if (this.scanDone) {
                for (LanguageHandler handler : this.lang2handler.values()) {
                    handler.handleEvent(evt);
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public <T extends TokenId> int balance(Language<T> language, T left) {
        Map map = this.lang2handler;
        synchronized (map) {
            this.checkScanDone();
            LanguageHandler<T> handler = this.handler(language, false);
            return handler != null ? handler.balance(left) : Integer.MAX_VALUE;
        }
    }

    private <T extends TokenId> LanguageHandler<T> handler(Language<T> language, boolean forceCreation) {
        LanguageHandler handler = this.lang2handler.get(language);
        if (handler == null && forceCreation) {
            handler = new LanguageHandler<T>(language);
            this.lang2handler.put(language, handler);
        }
        return handler;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void checkScanDone() {
        Map map = this.lang2handler;
        synchronized (map) {
            if (!this.scanDone) {
                TokenHierarchy hi = TokenHierarchy.get((Document)this.doc);
                for (LanguageHandler handler : this.lang2handler.values()) {
                    handler.scan(hi);
                }
                this.scanDone = true;
            }
        }
    }

    private static final class TokenIdPair<T extends TokenId> {
        T left;
        T right;
        int balance;

        public TokenIdPair(T left, T right) {
            this.left = left;
            this.right = right;
        }

        public void updateBalance(T id, int diff) {
            if (id == this.left) {
                this.balance += diff;
            } else {
                assert (id == this.right);
                this.balance -= diff;
            }
        }
    }

    private static final class LanguageHandler<T extends TokenId> {
        private final Language<T> language;
        private final Map<T, TokenIdPair<T>> id2Pair;

        LanguageHandler(Language<T> language) {
            this.language = language;
            this.id2Pair = new HashMap<T, TokenIdPair<T>>();
        }

        public final Language<T> language() {
            return this.language;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void addTokenPair(T left, T right) {
            TokenIdPair<T> pair = new TokenIdPair<T>(left, right);
            Map<T, TokenIdPair<T>> map = this.id2Pair;
            synchronized (map) {
                this.id2Pair.put(left, pair);
                this.id2Pair.put(right, pair);
            }
        }

        public void scan(TokenHierarchy hi) {
            for (TokenIdPair<T> pair : this.id2Pair.values()) {
                pair.balance = 0;
            }
            TokenSequence ts = hi.tokenSequence();
            if (ts != null) {
                this.processTokenSequence(ts, ts.tokenCount(), true, 1);
            }
        }

        public void processTokenSequence(TokenSequence<?> ts, int tokenCount, boolean checkEmbedded, int diff) {
            while (--tokenCount >= 0) {
                TokenIdPair<TokenId> pair;
                TokenId id;
                TokenSequence embeddedTS;
                boolean moved = ts.moveNext();
                assert (moved);
                if (ts.language() == this.language && (pair = this.id2Pair.get((Object)(id = ts.token().id()))) != null) {
                    pair.updateBalance(id, diff);
                }
                if (!checkEmbedded || (embeddedTS = ts.embedded()) == null) continue;
                this.processTokenSequence(embeddedTS, embeddedTS.tokenCount(), true, diff);
            }
        }

        public void handleEvent(TokenHierarchyEvent evt) {
            for (TokenChange<T> tokenChange : this.collectTokenChanges(evt.tokenChange(), new ArrayList<TokenChange<T>>())) {
                if (tokenChange.removedTokenCount() > 0) {
                    this.processTokenSequence(tokenChange.removedTokenSequence(), tokenChange.removedTokenCount(), false, -1);
                }
                if (tokenChange.addedTokenCount() <= 0) continue;
                this.processTokenSequence(tokenChange.currentTokenSequence(), tokenChange.addedTokenCount(), false, 1);
            }
        }

        public int balance(T left) {
            TokenIdPair<T> pair = this.id2Pair.get(left);
            return pair.left == left ? pair.balance : Integer.MAX_VALUE;
        }

        private List<TokenChange<T>> collectTokenChanges(TokenChange<?> change, List<TokenChange<T>> changes) {
            if (change.language() == this.language) {
                changes.add(change);
            }
            for (int i = 0; i < change.embeddedChangeCount(); ++i) {
                this.collectTokenChanges(change.embeddedChange(i), changes);
            }
            return changes;
        }
    }

}

