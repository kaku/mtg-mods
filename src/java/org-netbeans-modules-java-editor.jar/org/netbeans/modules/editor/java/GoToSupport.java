/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodInvocationTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.NewClassTree
 *  com.sun.source.tree.ParameterizedTypeTree
 *  com.sun.source.tree.Scope
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.TypeParameterTree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreePathScanner
 *  com.sun.source.util.Trees
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.api.editor.completion.Completion
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.lexer.JavadocTokenId
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.ElementUtilities
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.UiUtils
 *  org.netbeans.api.java.source.ui.ElementJavadoc
 *  org.netbeans.api.java.source.ui.ElementOpen
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.api.progress.ProgressUtils
 *  org.netbeans.editor.Utilities
 *  org.netbeans.editor.ext.ToolTipSupport
 *  org.netbeans.lib.editor.hyperlink.spi.HyperlinkType
 *  org.netbeans.lib.editor.util.StringEscapeUtils
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.spi.editor.completion.CompletionDocumentation
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.awt.StatusDisplayer
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.editor.java;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.ParameterizedTypeTree;
import com.sun.source.tree.Scope;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TypeParameterTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreePathScanner;
import com.sun.source.util.Trees;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ExecutableType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.AbstractElementVisitor6;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Types;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.editor.completion.Completion;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.lexer.JavadocTokenId;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.UiUtils;
import org.netbeans.api.java.source.ui.ElementJavadoc;
import org.netbeans.api.java.source.ui.ElementOpen;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.editor.ext.ToolTipSupport;
import org.netbeans.lib.editor.hyperlink.spi.HyperlinkType;
import org.netbeans.lib.editor.util.StringEscapeUtils;
import org.netbeans.modules.editor.java.JavaCompletionDoc;
import org.netbeans.modules.editor.java.JavaCompletionProvider;
import org.netbeans.modules.editor.java.Utilities;
import org.netbeans.modules.java.editor.javadoc.JavadocImports;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.spi.editor.completion.CompletionDocumentation;
import org.openide.awt.HtmlBrowser;
import org.openide.awt.StatusDisplayer;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class GoToSupport {
    private static final Set<JavaTokenId> USABLE_TOKEN_IDS = EnumSet.of(JavaTokenId.IDENTIFIER, JavaTokenId.THIS, JavaTokenId.SUPER);
    private static String[] c = new String[]{"&", "<", ">", "\n", "\""};
    private static String[] tags = new String[]{"&amp;", "&lt;", "&gt;", "<br>", "&quot;"};
    static UiUtilsCaller CALLER = new UiUtilsCaller(){

        @Override
        public boolean open(FileObject fo, int pos) {
            return UiUtils.open((FileObject)fo, (int)pos);
        }

        @Override
        public void beep(boolean goToSource, boolean goToJavadoc) {
            Toolkit.getDefaultToolkit().beep();
            int value = goToSource ? 1 : (goToJavadoc ? 2 : 0);
            StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(GoToSupport.class, (String)"WARN_CannotGoToGeneric", (Object)value));
        }

        @Override
        public boolean open(ClasspathInfo info, ElementHandle<?> el) {
            return ElementOpen.open((ClasspathInfo)info, el);
        }

        @Override
        public void warnCannotOpen(String displayName) {
            Toolkit.getDefaultToolkit().beep();
            StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(GoToSupport.class, (String)"WARN_CannotGoTo", (Object)displayName));
        }
    };

    private static FileObject getFileObject(Document doc) {
        DataObject od = (DataObject)doc.getProperty("stream");
        return od != null ? od.getPrimaryFile() : null;
    }

    public static String getGoToElementTooltip(final Document doc, final int offset, final boolean goToSource, final HyperlinkType type) {
        try {
            FileObject fo = GoToSupport.getFileObject(doc);
            if (fo == null) {
                return null;
            }
            final String[] result = new String[1];
            ParserManager.parse(Collections.singleton(Source.create((Document)doc)), (UserTask)new UserTask(){

                public void run(ResultIterator resultIterator) throws Exception {
                    CompilationController controller;
                    Parser.Result res = resultIterator.getParserResult(offset);
                    CompilationController compilationController = controller = res != null ? CompilationController.get((Parser.Result)res) : null;
                    if (controller == null || controller.toPhase(JavaSource.Phase.RESOLVED).compareTo((Enum)JavaSource.Phase.RESOLVED) < 0) {
                        return;
                    }
                    Context resolved = GoToSupport.resolveContext((CompilationInfo)controller, doc, offset, goToSource);
                    if (resolved != null) {
                        result[0] = GoToSupport.computeTooltip((CompilationInfo)controller, doc, resolved, type);
                    }
                }
            });
            return result[0];
        }
        catch (ParseException ex) {
            throw new IllegalStateException((Throwable)ex);
        }
    }

    private static boolean isError(Element el) {
        return el == null || el.asType() == null || el.asType().getKind() == TypeKind.ERROR;
    }

    private static void performGoTo(final Document doc, final int offset, final boolean goToSource, final boolean javadoc) {
        final AtomicBoolean cancel = new AtomicBoolean();
        ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

            @Override
            public void run() {
                GoToSupport.performGoToImpl(doc, offset, goToSource, javadoc, cancel);
            }
        }, (String)NbBundle.getMessage(GoToSupport.class, (String)(javadoc ? "LBL_GoToJavadoc" : (goToSource ? "LBL_GoToSource" : "LBL_GoToDeclaration"))), (AtomicBoolean)cancel, (boolean)false);
    }

    private static void performGoToImpl(final Document doc, final int offset, final boolean goToSource, final boolean javadoc, final AtomicBoolean cancel) {
        try {
            FileObject fo = GoToSupport.getFileObject(doc);
            if (fo == null) {
                return;
            }
            final int[] offsetToOpen = new int[]{-1};
            final ElementHandle[] elementToOpen = new ElementHandle[1];
            final String[] displayNameForError = new String[1];
            final boolean[] tryToOpen = new boolean[1];
            final ClasspathInfo[] cpInfo = new ClasspathInfo[1];
            ParserManager.parse(Collections.singleton(Source.create((Document)doc)), (UserTask)new UserTask(){

                public void run(ResultIterator resultIterator) throws Exception {
                    CompilationController controller;
                    Parser.Result res = resultIterator.getParserResult(offset);
                    if (cancel != null && cancel.get()) {
                        return;
                    }
                    CompilationController compilationController = controller = res != null ? CompilationController.get((Parser.Result)res) : null;
                    if (controller == null || controller.toPhase(JavaSource.Phase.RESOLVED).compareTo((Enum)JavaSource.Phase.RESOLVED) < 0) {
                        return;
                    }
                    cpInfo[0] = controller.getClasspathInfo();
                    Context resolved = GoToSupport.resolveContext((CompilationInfo)controller, doc, offset, goToSource);
                    if (resolved == null) {
                        GoToSupport.CALLER.beep(goToSource, javadoc);
                        return;
                    }
                    if (javadoc) {
                        URL url = SourceUtils.getPreferredJavadoc((Element)resolved.resolved);
                        if (url != null) {
                            HtmlBrowser.URLDisplayer.getDefault().showURL(url);
                        } else {
                            GoToSupport.CALLER.beep(goToSource, javadoc);
                        }
                    } else {
                        TreePath elpath = GoToSupport.getPath((CompilationInfo)controller, resolved.resolved);
                        if (elpath != null) {
                            Tree tree = elpath.getLeaf();
                            long startPos = controller.getTrees().getSourcePositions().getStartPosition(controller.getCompilationUnit(), tree);
                            if (startPos != -1) {
                                if (GoToSupport.isCaretInsideDeclarationName((CompilationInfo)controller, tree, elpath, offset)) {
                                    GoToSupport.CALLER.beep(goToSource, javadoc);
                                } else {
                                    offsetToOpen[0] = controller.getSnapshot().getOriginalOffset((int)startPos);
                                    displayNameForError[0] = Utilities.getElementName(resolved.resolved, false).toString();
                                    tryToOpen[0] = true;
                                }
                            } else {
                                GoToSupport.CALLER.beep(goToSource, javadoc);
                            }
                        } else {
                            elementToOpen[0] = ElementHandle.create((Element)resolved.resolved);
                            displayNameForError[0] = Utilities.getElementName(resolved.resolved, false).toString();
                            tryToOpen[0] = true;
                        }
                    }
                }
            });
            if (tryToOpen[0]) {
                boolean openSucceeded = false;
                if (cancel.get()) {
                    return;
                }
                if (offsetToOpen[0] >= 0) {
                    openSucceeded = CALLER.open(fo, offsetToOpen[0]);
                } else if (elementToOpen[0] != null) {
                    openSucceeded = CALLER.open(cpInfo[0], elementToOpen[0]);
                }
                if (!openSucceeded) {
                    CALLER.warnCannotOpen(displayNameForError[0]);
                }
            }
        }
        catch (ParseException ex) {
            throw new IllegalStateException((Throwable)ex);
        }
    }

    public static void goTo(Document doc, int offset, boolean goToSource) {
        GoToSupport.performGoTo(doc, offset, goToSource, false);
    }

    public static void goToJavadoc(Document doc, int offset) {
        GoToSupport.performGoTo(doc, offset, false, true);
    }

    public static Context resolveContext(CompilationInfo controller, Document doc, int offset, boolean goToSource) {
        Element el;
        TypeMirror classType;
        boolean insideImportStmt;
        Token[] token;
        token = new Token[1];
        int[] span = GoToSupport.getIdentifierSpan(doc, offset, token);
        if (span == null) {
            return null;
        }
        int exactOffset = controller.getSnapshot().getEmbeddedOffset(span[0] + 1);
        el = null;
        classType = null;
        insideImportStmt = false;
        TreePath path = controller.getTreeUtilities().pathFor(exactOffset);
        if (token[0] != null && token[0].id() == JavaTokenId.JAVADOC_COMMENT) {
            el = JavadocImports.findReferencedElement(controller, offset);
        } else {
            TreePath parent = path.getParentPath();
            if (parent != null) {
                Tree parentLeaf = parent.getLeaf();
                if (parentLeaf.getKind() == Tree.Kind.NEW_CLASS && ((NewClassTree)parentLeaf).getIdentifier() == path.getLeaf()) {
                    if (!GoToSupport.isError(controller.getTrees().getElement(path.getParentPath()))) {
                        path = path.getParentPath();
                    }
                } else if (parentLeaf.getKind() == Tree.Kind.IMPORT && ((ImportTree)parentLeaf).isStatic()) {
                    el = GoToSupport.handleStaticImport(controller, (ImportTree)parentLeaf);
                    insideImportStmt = true;
                } else if (parentLeaf.getKind() == Tree.Kind.PARAMETERIZED_TYPE && parent.getParentPath().getLeaf().getKind() == Tree.Kind.NEW_CLASS && ((ParameterizedTypeTree)parentLeaf).getType() == path.getLeaf() && !GoToSupport.isError(controller.getTrees().getElement(parent.getParentPath()))) {
                    path = parent.getParentPath();
                    classType = controller.getTrees().getTypeMirror(path);
                }
                if (el == null) {
                    el = controller.getTrees().getElement(path);
                    if (parentLeaf.getKind() == Tree.Kind.METHOD_INVOCATION && GoToSupport.isError(el)) {
                        List<ExecutableElement> ee = Utilities.fuzzyResolveMethodInvocation(controller, path.getParentPath(), new ArrayList<TypeMirror>(), new int[1]);
                        if (!ee.isEmpty()) {
                            el = ee.iterator().next();
                        } else {
                            ExpressionTree select = ((MethodInvocationTree)parentLeaf).getMethodSelect();
                            Name methodName = null;
                            switch (select.getKind()) {
                                case IDENTIFIER: {
                                    Scope s = controller.getTrees().getScope(path);
                                    el = s.getEnclosingClass();
                                    methodName = ((IdentifierTree)select).getName();
                                    break;
                                }
                                case MEMBER_SELECT: {
                                    el = controller.getTrees().getElement(new TreePath(path, (Tree)((MemberSelectTree)select).getExpression()));
                                    methodName = ((MemberSelectTree)select).getIdentifier();
                                }
                            }
                            if (el != null) {
                                for (ExecutableElement m : ElementFilter.methodsIn(el.getEnclosedElements())) {
                                    if (m.getSimpleName() != methodName) continue;
                                    el = m;
                                    break;
                                }
                            }
                        }
                    } else if (el != null && el.getKind() == ElementKind.ENUM_CONSTANT && path.getLeaf().getKind() == Tree.Kind.VARIABLE) {
                        Element e = controller.getTrees().getElement(new TreePath(path, (Tree)((VariableTree)path.getLeaf()).getInitializer()));
                        if (!controller.getElementUtilities().isSynthetic(e)) {
                            el = e;
                        }
                    }
                }
            } else {
                return null;
            }
        }
        if (GoToSupport.isError(el)) {
            return null;
        }
        if (goToSource && !insideImportStmt) {
            TypeMirror type = null;
            if (el instanceof VariableElement) {
                type = el.asType();
            }
            if (type != null && type.getKind() == TypeKind.DECLARED) {
                el = ((DeclaredType)type).asElement();
            }
        }
        if (GoToSupport.isError(el)) {
            return null;
        }
        if (controller.getElementUtilities().isSynthetic(el) && el.getKind() == ElementKind.CONSTRUCTOR) {
            el = GoToSupport.handlePossibleAnonymousInnerClass(controller, el);
        }
        if (GoToSupport.isError(el)) {
            return null;
        }
        if (el.getKind() != ElementKind.CONSTRUCTOR && (token[0].id() == JavaTokenId.SUPER || token[0].id() == JavaTokenId.THIS)) {
            return null;
        }
        return new Context(classType, el);
    }

    private static String computeTooltip(CompilationInfo controller, final Document doc, Context resolved, HyperlinkType type) {
        int overridableKind;
        DisplayNameElementVisitor v = new DisplayNameElementVisitor(controller);
        if (resolved.resolved.getKind() == ElementKind.CONSTRUCTOR && resolved.classType != null && resolved.classType.getKind() == TypeKind.DECLARED) {
            v.printExecutable((ExecutableElement)resolved.resolved, (DeclaredType)resolved.classType, true);
        } else {
            v.visit(resolved.resolved, true);
        }
        String result = null;
        try {
            class Ctrl
            implements Callable<Boolean> {
                private volatile boolean cancel;

                Ctrl() {
                }

                @Override
                public Boolean call() throws Exception {
                    return this.cancel;
                }
            }
            Ctrl control = new Ctrl();
            final ElementJavadoc jdoc = ElementJavadoc.create((CompilationInfo)controller, (Element)resolved.resolved, (Callable)control);
            Future text = jdoc != null ? jdoc.getTextAsync() : null;
            String string = result = text != null ? (String)text.get(1, TimeUnit.SECONDS) : null;
            if (result != null) {
                int idx = 0;
                for (int i = 0; i < 3 && idx >= 0; ++i) {
                    idx = result.indexOf("<p>", idx + 1);
                }
                if (idx >= 0) {
                    result = result.substring(0, idx + 3);
                    result = result + "<a href='***'>more...</a>";
                }
                if ((idx = result.indexOf("<p id=\"not-found\">")) >= 0) {
                    result = result.substring(0, idx);
                }
                doc.putProperty("TooltipResolver.hyperlinkListener", new HyperlinkListener(){

                    @Override
                    public void hyperlinkUpdate(HyperlinkEvent e) {
                        String desc;
                        if (e != null && HyperlinkEvent.EventType.ACTIVATED.equals(e.getEventType()) && (desc = e.getDescription()) != null) {
                            ElementJavadoc link;
                            ElementJavadoc elementJavadoc = link = "***".contentEquals(desc) ? jdoc : jdoc.resolveLink(desc);
                            if (link != null) {
                                ToolTipSupport tts;
                                JTextComponent comp = EditorRegistry.lastFocusedComponent();
                                if (comp != null && comp.getDocument() == doc && (tts = org.netbeans.editor.Utilities.getEditorUI((JTextComponent)comp).getToolTipSupport()) != null) {
                                    tts.setToolTipVisible(false);
                                }
                                JavaCompletionProvider.JavaCompletionQuery.outerDocumentation.set(new JavaCompletionDoc(link));
                                Completion.get().showDocumentation();
                            }
                        }
                    }
                });
            }
            control.cancel = true;
        }
        catch (Exception ex) {
            // empty catch block
        }
        if (result == null) {
            result = v.result.toString();
        }
        if ((overridableKind = GoToSupport.overridableKind(resolved.resolved)) != -1 && type != null) {
            if (type == HyperlinkType.GO_TO_DECLARATION) {
                StringBuilder sb = new StringBuilder();
                sb.append(KeyEvent.getKeyText(org.openide.util.Utilities.isMac() ? 157 : 17)).append('+').append(KeyEvent.getKeyText(18)).append('+');
                result = NbBundle.getMessage(GoToSupport.class, (String)"TP_OverriddenTooltipSugg", (Object)sb.toString(), (Object)overridableKind, (Object)result);
            } else {
                result = NbBundle.getMessage(GoToSupport.class, (String)"TP_GoToOverriddenTooltipSugg", (Object)overridableKind, (Object)result);
            }
        }
        result = "<html><body>" + result;
        return result;
    }

    public static int[] getIdentifierSpan(final Document doc, final int offset, final Token<JavaTokenId>[] token) {
        if (GoToSupport.getFileObject(doc) == null) {
            return null;
        }
        final int[][] ret = new int[][]{null};
        doc.render(new Runnable(){

            @Override
            public void run() {
                TokenHierarchy th = TokenHierarchy.get((Document)doc);
                TokenSequence ts = SourceUtils.getJavaTokenSequence((TokenHierarchy)th, (int)offset);
                if (ts == null) {
                    return;
                }
                ts.move(offset);
                if (!ts.moveNext()) {
                    return;
                }
                Token t = ts.token();
                if (JavaTokenId.JAVADOC_COMMENT == t.id()) {
                    TokenSequence jdts = ts.embedded(JavadocTokenId.language());
                    if (JavadocImports.isInsideReference(jdts, offset) || JavadocImports.isInsideParamName(jdts, offset)) {
                        jdts.move(offset);
                        jdts.moveNext();
                        if (token != null) {
                            token[0] = t;
                        }
                        ret[0] = new int[]{jdts.offset(), jdts.offset() + jdts.token().length()};
                    }
                    return;
                }
                if (!USABLE_TOKEN_IDS.contains((Object)t.id())) {
                    ts.move(offset - 1);
                    if (!ts.moveNext()) {
                        return;
                    }
                    t = ts.token();
                    if (!USABLE_TOKEN_IDS.contains((Object)t.id())) {
                        return;
                    }
                }
                if (token != null) {
                    token[0] = t;
                }
                ret[0] = new int[]{ts.offset(), ts.offset() + t.length()};
            }
        });
        return ret[0];
    }

    private static Element handlePossibleAnonymousInnerClass(CompilationInfo info, Element el) {
        Element doubleEncl;
        Element encl = el.getEnclosingElement();
        Element element = doubleEncl = encl != null ? encl.getEnclosingElement() : null;
        if (doubleEncl != null && !doubleEncl.getKind().isClass() && !doubleEncl.getKind().isInterface() && doubleEncl.getKind() != ElementKind.PACKAGE && encl.getKind() == ElementKind.CLASS) {
            Tree enclTree;
            NewClassTree nct;
            TreePath enclTreePath = info.getTrees().getPath(encl);
            Tree tree = enclTree = enclTreePath != null ? enclTreePath.getLeaf() : null;
            if (enclTree != null && TreeUtilities.CLASS_TREE_KINDS.contains((Object)enclTree.getKind()) && enclTreePath.getParentPath().getLeaf().getKind() == Tree.Kind.NEW_CLASS && (nct = (NewClassTree)enclTreePath.getParentPath().getLeaf()).getClassBody() != null) {
                Element parentElement = info.getTrees().getElement(new TreePath(enclTreePath, (Tree)nct.getIdentifier()));
                if (parentElement == null || parentElement.getKind().isInterface()) {
                    return parentElement;
                }
                TreePath superConstructorCall = (TreePath)new FindSuperConstructorCall().scan(enclTreePath, (Object)null);
                if (superConstructorCall != null) {
                    return info.getTrees().getElement(superConstructorCall);
                }
            }
            return null;
        }
        if (encl != null) {
            return encl;
        }
        return el;
    }

    private static Element handleStaticImport(CompilationInfo javac, ImportTree impt) {
        Tree clazz;
        Tree impIdent = impt.getQualifiedIdentifier();
        if (!impt.isStatic() || impIdent == null || impIdent.getKind() != Tree.Kind.MEMBER_SELECT) {
            return null;
        }
        Trees trees = javac.getTrees();
        MemberSelectTree select = (MemberSelectTree)impIdent;
        Name mName = select.getIdentifier();
        TreePath cutPath = new TreePath(javac.getCompilationUnit());
        TreePath selectPath = new TreePath(new TreePath(cutPath, (Tree)impt), (Tree)select.getExpression());
        Element selectElm = trees.getElement(selectPath);
        if (GoToSupport.isError(selectElm)) {
            return null;
        }
        TypeMirror clazzMir = null;
        TreePath clazzPath = null;
        List decls = javac.getCompilationUnit().getTypeDecls();
        if (!decls.isEmpty() && TreeUtilities.CLASS_TREE_KINDS.contains((Object)(clazz = (Tree)decls.get(0)).getKind())) {
            clazzPath = new TreePath(cutPath, clazz);
            Element clazzElm = trees.getElement(clazzPath);
            if (GoToSupport.isError(clazzElm)) {
                return null;
            }
            clazzMir = clazzElm.asType();
        }
        if (clazzMir == null) {
            return null;
        }
        Scope clazzScope = trees.getScope(clazzPath);
        for (Element member : selectElm.getEnclosedElements()) {
            if (!member.getModifiers().contains((Object)Modifier.STATIC) || !mName.contentEquals(member.getSimpleName()) || !trees.isAccessible(clazzScope, member, (DeclaredType)clazzMir)) continue;
            return member;
        }
        return null;
    }

    private static boolean isCaretInsideDeclarationName(CompilationInfo info, Tree t, TreePath path, int caret) {
        try {
            switch (t.getKind()) {
                case ANNOTATION_TYPE: 
                case CLASS: 
                case ENUM: 
                case INTERFACE: 
                case METHOD: 
                case VARIABLE: {
                    int[] span = org.netbeans.modules.java.editor.semantic.Utilities.findIdentifierSpan(path, info, info.getDocument());
                    if (span == null || span[0] == -1 || span[1] == -1) {
                        return false;
                    }
                    return span[0] <= caret && caret <= span[1];
                }
            }
            return false;
        }
        catch (IOException iOException) {
            Exceptions.printStackTrace((Throwable)iOException);
            return false;
        }
    }

    private static int overridableKind(Element el) {
        if (el.getModifiers().contains((Object)Modifier.FINAL) || el.getModifiers().contains((Object)Modifier.PRIVATE)) {
            return -1;
        }
        if (el.getKind().isClass() || el.getKind().isInterface()) {
            return el.getModifiers().contains((Object)Modifier.ABSTRACT) ? 0 : 1;
        }
        if (el.getKind() == ElementKind.METHOD && !el.getModifiers().contains((Object)Modifier.STATIC) && !el.getEnclosingElement().getModifiers().contains((Object)Modifier.FINAL)) {
            return el.getModifiers().contains((Object)Modifier.ABSTRACT) ? 2 : 3;
        }
        return -1;
    }

    private static TreePath getPath(CompilationInfo info, Element el) {
        class S
        extends TreePathScanner<Void, Void> {
            private TreePath found;
            final /* synthetic */ Element val$toFind;

            S() {
                this.val$toFind = var2_2;
            }

            public Void scan(Tree tree, Void p) {
                if (this.found != null) {
                    return null;
                }
                return (Void)TreePathScanner.super.scan(tree, (Object)p);
            }

            private boolean process() {
                Element resolved = val$info.getTrees().getElement(this.getCurrentPath());
                if (this.val$toFind.equals(resolved)) {
                    this.found = this.getCurrentPath();
                    return true;
                }
                return false;
            }

            public Void visitClass(ClassTree node, Void p) {
                if (!this.process()) {
                    TreePathScanner.super.visitClass(node, (Object)p);
                }
                return null;
            }

            public Void visitMethod(MethodTree node, Void p) {
                if (!this.process()) {
                    return (Void)TreePathScanner.super.visitMethod(node, (Object)p);
                }
                return null;
            }

            public Void visitVariable(VariableTree node, Void p) {
                if (!this.process()) {
                    return (Void)TreePathScanner.super.visitVariable(node, (Object)p);
                }
                return null;
            }

            public Void visitTypeParameter(TypeParameterTree node, Void p) {
                if (!this.process()) {
                    return (Void)TreePathScanner.super.visitTypeParameter(node, (Object)p);
                }
                return null;
            }
        }
        Element toFind = info.getElementUtilities().isSynthetic(el) ? el.getEnclosingElement() : el;
        S search = new S(info, toFind);
        search.scan((Tree)info.getCompilationUnit(), null);
        return search.found;
    }

    private static String getTypeName(CompilationInfo info, TypeMirror t, boolean fqn) {
        return GoToSupport.translate(Utilities.getTypeName(info, t, fqn).toString());
    }

    private static String translate(String input) {
        for (int cntr = 0; cntr < c.length; ++cntr) {
            input = input.replaceAll(c[cntr], tags[cntr]);
        }
        return input;
    }

    public static final class Context {
        public final TypeMirror classType;
        public final Element resolved;

        public Context(TypeMirror classType, Element resolved) {
            this.classType = classType;
            this.resolved = resolved;
        }
    }

    static interface UiUtilsCaller {
        public boolean open(FileObject var1, int var2);

        public void beep(boolean var1, boolean var2);

        public boolean open(ClasspathInfo var1, ElementHandle<?> var2);

        public void warnCannotOpen(String var1);
    }

    private static final class DisplayNameElementVisitor
    extends AbstractElementVisitor6<Void, Boolean> {
        private final CompilationInfo info;
        private StringBuffer result = new StringBuffer();

        public DisplayNameElementVisitor(CompilationInfo info) {
            this.info = info;
        }

        private void boldStartCheck(boolean highlightName) {
            if (highlightName) {
                this.result.append("<b>");
            }
        }

        private void boldStopCheck(boolean highlightName) {
            if (highlightName) {
                this.result.append("</b>");
            }
        }

        @Override
        public Void visitPackage(PackageElement e, Boolean highlightName) {
            this.boldStartCheck(highlightName);
            this.result.append(e.getQualifiedName());
            this.boldStopCheck(highlightName);
            return null;
        }

        @Override
        public Void visitType(TypeElement e, Boolean highlightName) {
            return this.printType(e, null, highlightName);
        }

        Void printType(TypeElement e, DeclaredType dt, Boolean highlightName) {
            this.modifier(e.getModifiers());
            switch (e.getKind()) {
                case CLASS: {
                    this.result.append("class ");
                    break;
                }
                case INTERFACE: {
                    this.result.append("interface ");
                    break;
                }
                case ENUM: {
                    this.result.append("enum ");
                    break;
                }
                case ANNOTATION_TYPE: {
                    this.result.append("@interface ");
                }
            }
            Element enclosing = e.getEnclosingElement();
            if (enclosing == SourceUtils.getEnclosingTypeElement((Element)e)) {
                this.result.append(((TypeElement)enclosing).getQualifiedName());
                this.result.append('.');
                this.boldStartCheck(highlightName);
                this.result.append(e.getSimpleName());
                this.boldStopCheck(highlightName);
            } else {
                this.result.append(e.getQualifiedName());
            }
            if (dt != null) {
                this.dumpRealTypeArguments(dt.getTypeArguments());
            }
            return null;
        }

        @Override
        public Void visitVariable(VariableElement e, Boolean highlightName) {
            this.modifier(e.getModifiers());
            this.result.append(GoToSupport.getTypeName(this.info, e.asType(), true));
            this.result.append(' ');
            this.boldStartCheck(highlightName);
            this.result.append(e.getSimpleName());
            this.boldStopCheck(highlightName);
            if (highlightName.booleanValue()) {
                if (e.getConstantValue() != null) {
                    this.result.append(" = ");
                    this.result.append(StringEscapeUtils.escapeHtml((String)e.getConstantValue().toString()));
                }
                Element enclosing = e.getEnclosingElement();
                if (e.getKind() != ElementKind.PARAMETER && e.getKind() != ElementKind.LOCAL_VARIABLE && e.getKind() != ElementKind.RESOURCE_VARIABLE && e.getKind() != ElementKind.EXCEPTION_PARAMETER) {
                    this.result.append(" in ");
                    this.result.append(GoToSupport.getTypeName(this.info, enclosing.asType(), true));
                }
            }
            return null;
        }

        @Override
        public Void visitExecutable(ExecutableElement e, Boolean highlightName) {
            return this.printExecutable(e, null, highlightName);
        }

        Void printExecutable(ExecutableElement e, DeclaredType dt, Boolean highlightName) {
            switch (e.getKind()) {
                case CONSTRUCTOR: {
                    this.modifier(e.getModifiers());
                    this.dumpTypeArguments(e.getTypeParameters());
                    this.result.append(' ');
                    this.boldStartCheck(highlightName);
                    this.result.append(e.getEnclosingElement().getSimpleName());
                    this.boldStopCheck(highlightName);
                    if (dt != null) {
                        this.dumpRealTypeArguments(dt.getTypeArguments());
                        this.dumpArguments(e.getParameters(), ((ExecutableType)this.info.getTypes().asMemberOf(dt, e)).getParameterTypes());
                    } else {
                        this.dumpArguments(e.getParameters(), null);
                    }
                    this.dumpThrows(e.getThrownTypes());
                    break;
                }
                case METHOD: {
                    this.modifier(e.getModifiers());
                    this.dumpTypeArguments(e.getTypeParameters());
                    this.result.append(GoToSupport.getTypeName(this.info, e.getReturnType(), true));
                    this.result.append(' ');
                    this.boldStartCheck(highlightName);
                    this.result.append(e.getSimpleName());
                    this.boldStopCheck(highlightName);
                    this.dumpArguments(e.getParameters(), null);
                    this.dumpThrows(e.getThrownTypes());
                    break;
                }
            }
            return null;
        }

        @Override
        public Void visitTypeParameter(TypeParameterElement e, Boolean highlightName) {
            return null;
        }

        private void modifier(Set<Modifier> modifiers) {
            boolean addSpace = false;
            for (Modifier m : modifiers) {
                if (addSpace) {
                    this.result.append(' ');
                }
                addSpace = true;
                this.result.append(m.toString());
            }
            if (addSpace) {
                this.result.append(' ');
            }
        }

        private void dumpTypeArguments(List<? extends TypeParameterElement> list) {
            if (list.isEmpty()) {
                return;
            }
            boolean addSpace = false;
            this.result.append("&lt;");
            for (TypeParameterElement e : list) {
                if (addSpace) {
                    this.result.append(", ");
                }
                this.result.append(GoToSupport.getTypeName(this.info, e.asType(), true));
                addSpace = true;
            }
            this.result.append("&gt;");
        }

        private void dumpRealTypeArguments(List<? extends TypeMirror> list) {
            if (list.isEmpty()) {
                return;
            }
            boolean addSpace = false;
            this.result.append("&lt;");
            for (TypeMirror t : list) {
                if (addSpace) {
                    this.result.append(", ");
                }
                this.result.append(GoToSupport.getTypeName(this.info, t, true));
                addSpace = true;
            }
            this.result.append("&gt;");
        }

        private void dumpArguments(List<? extends VariableElement> list, List<? extends TypeMirror> types) {
            Iterator<? extends TypeMirror> typesIt;
            boolean addSpace = false;
            this.result.append('(');
            Iterator<? extends VariableElement> listIt = list.iterator();
            Iterator<? extends TypeMirror> iterator = typesIt = types != null ? types.iterator() : null;
            while (listIt.hasNext()) {
                if (addSpace) {
                    this.result.append(", ");
                }
                VariableElement ve = listIt.next();
                TypeMirror type = typesIt != null ? typesIt.next() : ve.asType();
                this.result.append(GoToSupport.getTypeName(this.info, type, true));
                this.result.append(" ");
                this.result.append(ve.getSimpleName());
                addSpace = true;
            }
            this.result.append(')');
        }

        private void dumpThrows(List<? extends TypeMirror> list) {
            if (list.isEmpty()) {
                return;
            }
            boolean addSpace = false;
            this.result.append(" throws ");
            for (TypeMirror t : list) {
                if (addSpace) {
                    this.result.append(", ");
                }
                this.result.append(GoToSupport.getTypeName(this.info, t, true));
                addSpace = true;
            }
        }
    }

    private static final class FindSuperConstructorCall
    extends TreePathScanner<TreePath, Void> {
        private FindSuperConstructorCall() {
        }

        public TreePath visitMethodInvocation(MethodInvocationTree tree, Void v) {
            if (tree.getMethodSelect().getKind() == Tree.Kind.IDENTIFIER && "super".equals(((IdentifierTree)tree.getMethodSelect()).getName().toString())) {
                return this.getCurrentPath();
            }
            return null;
        }

        public TreePath reduce(TreePath first, TreePath second) {
            if (first == null) {
                return second;
            }
            return first;
        }
    }

}

