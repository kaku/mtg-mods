/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ArrayAccessTree
 *  com.sun.source.tree.AssignmentTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.EnhancedForLoopTree
 *  com.sun.source.tree.ErroneousTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.LiteralTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodInvocationTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.NewClassTree
 *  com.sun.source.tree.ParameterizedTypeTree
 *  com.sun.source.tree.Scope
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreePathScanner
 *  com.sun.source.util.TreeScanner
 *  com.sun.source.util.Trees
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.source.CodeStyle
 *  org.netbeans.api.java.source.CodeStyleUtils
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.ElementUtilities
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.TypeUtilities
 *  org.netbeans.api.java.source.TypeUtilities$TypeNameOptions
 *  org.netbeans.api.java.source.support.ReferencesCount
 *  org.netbeans.api.lexer.InputAttributes
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.modules.editor.NbEditorUtilities
 *  org.netbeans.swing.plaf.LFCustoms
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Pair
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.java;

import com.sun.source.tree.ArrayAccessTree;
import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.EnhancedForLoopTree;
import com.sun.source.tree.ErroneousTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.ParameterizedTypeTree;
import com.sun.source.tree.Scope;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreePathScanner;
import com.sun.source.util.TreeScanner;
import com.sun.source.util.Trees;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.EventListener;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ErrorType;
import javax.lang.model.type.ExecutableType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.WildcardType;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.SimpleElementVisitor8;
import javax.lang.model.util.Types;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.source.CodeStyle;
import org.netbeans.api.java.source.CodeStyleUtils;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.TypeUtilities;
import org.netbeans.api.java.source.support.ReferencesCount;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.modules.java.editor.javadoc.JavadocImports;
import org.netbeans.swing.plaf.LFCustoms;
import org.openide.filesystems.FileObject;
import org.openide.util.Pair;
import org.openide.util.WeakListeners;

public final class Utilities {
    private static final String ERROR = "<error>";
    private static boolean caseSensitive = true;
    private static boolean showDeprecatedMembers = true;
    private static boolean guessMethodArguments = true;
    private static boolean autoPopupOnJavaIdentifierPart = false;
    private static boolean javaCompletionExcluderMethods = false;
    private static boolean javaCompletionSubwords = false;
    private static String javaCompletionAutoPopupTriggers = ".";
    private static String javaCompletionSelectors = ".,;:([+-=";
    private static String javadocCompletionAutoPopupTriggers = ".#@";
    private static String javadocCompletionSelectors = ".#";
    private static final AtomicBoolean inited = new AtomicBoolean(false);
    private static Preferences preferences;
    private static final PreferenceChangeListener preferencesTracker;
    private static String cachedPrefix;
    private static Pattern cachedCamelCasePattern;
    private static Pattern cachedSubwordsPattern;
    private static final AtomicReference<Collection<String>> excludeRef;
    private static final AtomicReference<Collection<String>> includeRef;

    public static boolean startsWith(String theString, String prefix) {
        if (theString == null || theString.length() == 0 || "<error>".equals(theString)) {
            return false;
        }
        if (prefix == null || prefix.length() == 0) {
            return true;
        }
        if (javaCompletionSubwords) {
            if (!prefix.equals(cachedPrefix)) {
                cachedSubwordsPattern = null;
                cachedCamelCasePattern = null;
            }
            if (cachedSubwordsPattern == null) {
                cachedPrefix = prefix;
                String patternString = Utilities.createSubwordsPattern(prefix);
                Pattern pattern = Utilities.cachedSubwordsPattern = patternString != null ? Pattern.compile(patternString) : null;
            }
            if (cachedSubwordsPattern != null && cachedSubwordsPattern.matcher(theString).matches()) {
                return true;
            }
        }
        return Utilities.isCaseSensitive() ? theString.startsWith(prefix) : theString.toLowerCase(Locale.ENGLISH).startsWith(prefix.toLowerCase(Locale.ENGLISH));
    }

    public static String createSubwordsPattern(String prefix) {
        StringBuilder sb = new StringBuilder(3 + 8 * prefix.length());
        sb.append(".*?");
        for (int i = 0; i < prefix.length(); ++i) {
            char charAt = prefix.charAt(i);
            if (!Character.isJavaIdentifierPart(charAt)) {
                return null;
            }
            if (Character.isLowerCase(charAt)) {
                sb.append("[");
                sb.append(charAt);
                sb.append(Character.toUpperCase(charAt));
                sb.append("]");
            } else {
                sb.append(charAt);
            }
            sb.append(".*?");
        }
        return sb.toString();
    }

    public static boolean startsWithCamelCase(String theString, String prefix) {
        if (theString == null || theString.length() == 0 || prefix == null || prefix.length() == 0) {
            return false;
        }
        if (!prefix.equals(cachedPrefix)) {
            cachedSubwordsPattern = null;
            cachedCamelCasePattern = null;
        }
        if (cachedCamelCasePattern == null) {
            int index;
            StringBuilder sb = new StringBuilder();
            int lastIndex = 0;
            do {
                String token = prefix.substring(lastIndex, (index = Utilities.findNextUpper(prefix, lastIndex + 1)) == -1 ? prefix.length() : index);
                sb.append(token);
                sb.append(index != -1 ? "[\\p{javaLowerCase}\\p{Digit}_\\$]*" : ".*");
                lastIndex = index;
            } while (index != -1);
            cachedPrefix = prefix;
            cachedCamelCasePattern = Pattern.compile(sb.toString());
        }
        return cachedCamelCasePattern.matcher(theString).matches();
    }

    private static int findNextUpper(String text, int offset) {
        for (int i = offset; i < text.length(); ++i) {
            if (!Character.isUpperCase(text.charAt(i))) continue;
            return i;
        }
        return -1;
    }

    public static boolean isCaseSensitive() {
        Utilities.lazyInit();
        return caseSensitive;
    }

    public static void setCaseSensitive(boolean b) {
        Utilities.lazyInit();
        caseSensitive = b;
    }

    public static boolean isSubwordSensitive() {
        Utilities.lazyInit();
        return javaCompletionSubwords;
    }

    public static boolean isShowDeprecatedMembers() {
        Utilities.lazyInit();
        return showDeprecatedMembers;
    }

    public static void setShowDeprecatedMembers(boolean b) {
        Utilities.lazyInit();
        showDeprecatedMembers = b;
    }

    public static boolean guessMethodArguments() {
        Utilities.lazyInit();
        return guessMethodArguments;
    }

    public static boolean autoPopupOnJavaIdentifierPart() {
        Utilities.lazyInit();
        return autoPopupOnJavaIdentifierPart;
    }

    public static String getJavaCompletionAutoPopupTriggers() {
        Utilities.lazyInit();
        return javaCompletionAutoPopupTriggers;
    }

    public static String getJavaCompletionSelectors() {
        Utilities.lazyInit();
        return javaCompletionSelectors;
    }

    public static String getJavadocCompletionAutoPopupTriggers() {
        Utilities.lazyInit();
        return javadocCompletionAutoPopupTriggers;
    }

    public static String getJavadocCompletionSelectors() {
        Utilities.lazyInit();
        return javadocCompletionSelectors;
    }

    private static void updateExcluder(AtomicReference<Collection<String>> existing, String updated) {
        String[] entries;
        LinkedList<String> nue = new LinkedList<String>();
        if (updated == null || updated.length() == 0) {
            existing.set(nue);
            return;
        }
        for (String entry : entries = updated.split(",")) {
            if (entry.length() == 0) continue;
            nue.add(entry);
        }
        existing.set(nue);
    }

    public static boolean isExcludeMethods() {
        Utilities.lazyInit();
        return javaCompletionExcluderMethods;
    }

    public static boolean isExcluded(CharSequence fqn) {
        if (fqn == null || fqn.length() == 0) {
            return true;
        }
        Utilities.lazyInit();
        String s = fqn.toString();
        Collection<String> include = includeRef.get();
        Collection<String> exclude = excludeRef.get();
        if (include != null && !include.isEmpty()) {
            for (String entry : include) {
                if (!(entry.length() > fqn.length() ? entry.startsWith(s) : s.startsWith(entry))) continue;
                return false;
            }
        }
        if (exclude != null && !exclude.isEmpty()) {
            for (String entry : exclude) {
                if (entry.length() > fqn.length() || !s.startsWith(entry)) continue;
                return true;
            }
        }
        return false;
    }

    public static void exclude(CharSequence fqn) {
        if (fqn != null && fqn.length() > 0) {
            Utilities.lazyInit();
            String blacklist = preferences.get("javaCompletionBlacklist", "");
            blacklist = blacklist + (blacklist.length() > 0 ? new StringBuilder().append(",").append((Object)fqn).toString() : fqn);
            preferences.put("javaCompletionBlacklist", blacklist);
        }
    }

    private static void lazyInit() {
        if (inited.compareAndSet(false, true)) {
            preferences = (Preferences)MimeLookup.getLookup((String)"text/x-java").lookup(Preferences.class);
            preferences.addPreferenceChangeListener((PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)preferencesTracker, (Object)preferences));
            preferencesTracker.preferenceChange(null);
        }
    }

    public static int getImportanceLevel(CompilationInfo info, ReferencesCount referencesCount, @NonNull Element element) {
        boolean isType = element.getKind().isClass() || element.getKind().isInterface();
        return Utilities.getImportanceLevel(referencesCount, isType ? ElementHandle.create((Element)((TypeElement)element)) : ElementHandle.create((Element)((TypeElement)element.getEnclosingElement())));
    }

    public static int getImportanceLevel(ReferencesCount referencesCount, ElementHandle<TypeElement> handle) {
        int typeRefCount = 999 - Math.min(referencesCount.getTypeReferenceCount(handle), 999);
        int pkgRefCount = 999;
        String binaryName = SourceUtils.getJVMSignature(handle)[0];
        int idx = binaryName.lastIndexOf(46);
        if (idx > 0) {
            ElementHandle pkgElement = ElementHandle.createPackageElementHandle((String)binaryName.substring(0, idx));
            pkgRefCount -= Math.min(referencesCount.getPackageReferenceCount(pkgElement), 999);
        }
        return typeRefCount * 100000 + pkgRefCount * 100 + Utilities.getImportanceLevel(binaryName);
    }

    public static int getImportanceLevel(String fqn) {
        int weight = 50;
        if (fqn.startsWith("java.lang") || fqn.startsWith("java.util")) {
            weight -= 10;
        } else if (fqn.startsWith("org.omg") || fqn.startsWith("org.apache")) {
            weight += 10;
        } else if (fqn.startsWith("com.sun") || fqn.startsWith("com.ibm") || fqn.startsWith("com.apple")) {
            weight += 20;
        } else if (fqn.startsWith("sun") || fqn.startsWith("sunw") || fqn.startsWith("netscape")) {
            weight += 30;
        }
        return weight;
    }

    public static String getHTMLColor(int r, int g, int b) {
        Color c = LFCustoms.shiftColor((Color)new Color(r, g, b));
        return "<font color=#" + LFCustoms.getHexString((int)c.getRed()) + LFCustoms.getHexString((int)c.getGreen()) + LFCustoms.getHexString((int)c.getBlue()) + ">";
    }

    public static boolean hasAccessibleInnerClassConstructor(Element e, Scope scope, Trees trees) {
        DeclaredType dt = (DeclaredType)e.asType();
        for (TypeElement inner : ElementFilter.typesIn(e.getEnclosedElements())) {
            if (!trees.isAccessible(scope, (Element)inner, dt)) continue;
            DeclaredType innerType = (DeclaredType)inner.asType();
            for (ExecutableElement ctor : ElementFilter.constructorsIn(inner.getEnclosedElements())) {
                if (!trees.isAccessible(scope, (Element)ctor, innerType)) continue;
                return true;
            }
        }
        return false;
    }

    public static TreePath getPathElementOfKind(Tree.Kind kind, TreePath path) {
        return Utilities.getPathElementOfKind(EnumSet.of(kind), path);
    }

    public static TreePath getPathElementOfKind(Set<Tree.Kind> kinds, TreePath path) {
        while (path != null) {
            if (kinds.contains((Object)path.getLeaf().getKind())) {
                return path;
            }
            path = path.getParentPath();
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static boolean isJavaContext(JTextComponent component, int offset, boolean allowInStrings) {
        Document doc = component.getDocument();
        if (doc instanceof AbstractDocument) {
            ((AbstractDocument)doc).readLock();
        }
        try {
            if (doc.getLength() == 0 && "text/x-dialog-binding".equals(doc.getProperty("mimeType"))) {
                LanguagePath path;
                InputAttributes attributes = (InputAttributes)doc.getProperty(InputAttributes.class);
                Document d = (Document)attributes.getValue(path = LanguagePath.get((Language)((Language)MimeLookup.getLookup((String)"text/x-dialog-binding").lookup(Language.class))), (Object)"dialogBinding.document");
                if (d != null) {
                    boolean bl = "text/x-java".equals(NbEditorUtilities.getMimeType((Document)d));
                    return bl;
                }
                FileObject fo = (FileObject)attributes.getValue(path, (Object)"dialogBinding.fileObject");
                boolean bl = "text/x-java".equals(fo.getMIMEType());
                return bl;
            }
            TokenSequence ts = SourceUtils.getJavaTokenSequence((TokenHierarchy)TokenHierarchy.get((Document)doc), (int)offset);
            if (ts == null) {
                boolean path = false;
                return path;
            }
            if (!ts.moveNext() && !ts.movePrevious()) {
                boolean path = true;
                return path;
            }
            if (offset == ts.offset()) {
                boolean path = true;
                return path;
            }
            switch ((JavaTokenId)ts.token().id()) {
                case DOUBLE_LITERAL: 
                case FLOAT_LITERAL: 
                case FLOAT_LITERAL_INVALID: 
                case LONG_LITERAL: {
                    if (ts.token().text().charAt(0) == '.') break;
                }
                case CHAR_LITERAL: 
                case INT_LITERAL: 
                case INVALID_COMMENT_END: 
                case JAVADOC_COMMENT: 
                case LINE_COMMENT: 
                case BLOCK_COMMENT: {
                    boolean path = false;
                    return path;
                }
                case STRING_LITERAL: {
                    boolean path = allowInStrings;
                    return path;
                }
            }
            boolean path = true;
            return path;
        }
        finally {
            if (doc instanceof AbstractDocument) {
                ((AbstractDocument)doc).readUnlock();
            }
        }
    }

    public static CharSequence getTypeName(CompilationInfo info, TypeMirror type, boolean fqn) {
        return Utilities.getTypeName(info, type, fqn, false);
    }

    public static CharSequence getTypeName(CompilationInfo info, TypeMirror type, boolean fqn, boolean varArg) {
        EnumSet<TypeUtilities.TypeNameOptions> options = EnumSet.noneOf(TypeUtilities.TypeNameOptions.class);
        if (fqn) {
            options.add(TypeUtilities.TypeNameOptions.PRINT_FQN);
        }
        if (varArg) {
            options.add(TypeUtilities.TypeNameOptions.PRINT_AS_VARARG);
        }
        return info.getTypeUtilities().getTypeName(type, options.toArray((T[])new TypeUtilities.TypeNameOptions[0]));
    }

    public static CharSequence getElementName(Element el, boolean fqn) {
        if (el == null || el.asType().getKind() == TypeKind.NONE) {
            return "";
        }
        return (CharSequence)new ElementNameVisitor().visit(el, fqn);
    }

    public static Collection<? extends Element> getForwardReferences(TreePath path, int pos, SourcePositions sourcePositions, Trees trees) {
        HashSet<Element> refs = new HashSet<Element>();
        while (path != null) {
            switch (path.getLeaf().getKind()) {
                case BLOCK: {
                    if (path.getParentPath().getLeaf().getKind() == Tree.Kind.LAMBDA_EXPRESSION) break;
                }
                case ANNOTATION_TYPE: 
                case CLASS: 
                case ENUM: 
                case INTERFACE: {
                    return refs;
                }
                case VARIABLE: {
                    refs.add(trees.getElement(path));
                    TreePath parent = path.getParentPath();
                    if (TreeUtilities.CLASS_TREE_KINDS.contains((Object)parent.getLeaf().getKind())) {
                        boolean isStatic = ((VariableTree)path.getLeaf()).getModifiers().getFlags().contains((Object)Modifier.STATIC);
                        for (Tree member : ((ClassTree)parent.getLeaf()).getMembers()) {
                            if (member.getKind() != Tree.Kind.VARIABLE || sourcePositions.getStartPosition(path.getCompilationUnit(), member) < (long)pos || !isStatic && ((VariableTree)member).getModifiers().getFlags().contains((Object)Modifier.STATIC)) continue;
                            refs.add(trees.getElement(new TreePath(parent, member)));
                        }
                    }
                    return refs;
                }
                case ENHANCED_FOR_LOOP: {
                    EnhancedForLoopTree efl = (EnhancedForLoopTree)path.getLeaf();
                    if (sourcePositions.getEndPosition(path.getCompilationUnit(), (Tree)efl.getExpression()) < (long)pos) break;
                    refs.add(trees.getElement(new TreePath(path, (Tree)efl.getVariable())));
                }
            }
            path = path.getParentPath();
        }
        return refs;
    }

    public static List<String> varNamesSuggestions(TypeMirror type, ElementKind kind, Set<Modifier> modifiers, String suggestedName, String prefix, Types types, Elements elements, Iterable<? extends Element> locals, CodeStyle codeStyle) {
        Collection vnct;
        ArrayList<String> result = new ArrayList<String>();
        if (type == null && suggestedName == null) {
            return result;
        }
        if (suggestedName != null) {
            vnct = new LinkedHashSet<String>();
            vnct.add((String)suggestedName);
            if (type != null) {
                vnct.addAll(Utilities.varNamesForType(type, types, elements, prefix));
            }
        } else {
            vnct = Utilities.varNamesForType(type, types, elements, prefix);
        }
        boolean isConst = false;
        String namePrefix = null;
        String nameSuffix = null;
        switch (kind) {
            case FIELD: {
                if (modifiers.contains((Object)Modifier.STATIC)) {
                    if (codeStyle != null) {
                        namePrefix = codeStyle.getStaticFieldNamePrefix();
                        nameSuffix = codeStyle.getStaticFieldNameSuffix();
                    }
                    isConst = modifiers.contains((Object)Modifier.FINAL);
                    break;
                }
                if (codeStyle == null) break;
                namePrefix = codeStyle.getFieldNamePrefix();
                nameSuffix = codeStyle.getFieldNameSuffix();
                break;
            }
            case LOCAL_VARIABLE: 
            case EXCEPTION_PARAMETER: 
            case RESOURCE_VARIABLE: {
                if (codeStyle == null) break;
                namePrefix = codeStyle.getLocalVarNamePrefix();
                nameSuffix = codeStyle.getLocalVarNameSuffix();
                break;
            }
            case PARAMETER: {
                if (codeStyle == null) break;
                namePrefix = codeStyle.getParameterNamePrefix();
                nameSuffix = codeStyle.getParameterNameSuffix();
            }
        }
        if (isConst) {
            ArrayList<String> ls = new ArrayList<String>(vnct.size());
            for (String s : vnct) {
                ls.add(Utilities.getConstName(s));
            }
            vnct = ls;
        }
        if (vnct.isEmpty() && prefix != null && prefix.length() > 0 && (namePrefix != null && namePrefix.length() > 0 || nameSuffix != null && nameSuffix.length() > 0)) {
            vnct = Collections.singletonList(prefix);
        }
        String p = prefix;
        while (p != null && p.length() > 0) {
            ArrayList<String> l = new ArrayList<String>();
            for (String name : vnct) {
                if (!Utilities.startsWith(name, p)) continue;
                l.add(name);
            }
            if (l.isEmpty()) {
                p = Utilities.nextName(p);
                continue;
            }
            vnct = l;
            prefix = prefix.substring(0, prefix.length() - p.length());
            p = null;
        }
        Iterator i$ = vnct.iterator();
        while (i$.hasNext()) {
            boolean isPrimitive;
            String name = (String)i$.next();
            boolean bl = isPrimitive = type != null && type.getKind().isPrimitive();
            if (prefix != null && prefix.length() > 0) {
                name = isConst ? prefix.toUpperCase(Locale.ENGLISH) + '_' + name : prefix + name.toUpperCase(Locale.ENGLISH).charAt(0) + name.substring(1);
            }
            int cnt = 1;
            String baseName = name;
            name = CodeStyleUtils.addPrefixSuffix((CharSequence)name, (String)namePrefix, (String)nameSuffix);
            while (Utilities.isClashing(name, type, locals)) {
                if (isPrimitive) {
                    char c = name.charAt(namePrefix != null ? namePrefix.length() : 0);
                    c = (char)(c + '\u0001');
                    name = CodeStyleUtils.addPrefixSuffix((CharSequence)Character.toString(c), (String)namePrefix, (String)nameSuffix);
                    if (c != 'z' && c != 'Z') continue;
                    isPrimitive = false;
                    continue;
                }
                name = CodeStyleUtils.addPrefixSuffix((CharSequence)(baseName + cnt++), (String)namePrefix, (String)nameSuffix);
            }
            result.add(name);
        }
        return result;
    }

    public static String varNameSuggestion(TreePath path) {
        return Utilities.adjustName(Utilities.varNameForPath(path));
    }

    public static String varNameSuggestion(Tree tree) {
        return Utilities.adjustName(Utilities.varNameForTree(tree));
    }

    public static boolean inAnonymousOrLocalClass(TreePath path) {
        if (path == null) {
            return false;
        }
        TreePath parentPath = path.getParentPath();
        if (TreeUtilities.CLASS_TREE_KINDS.contains((Object)path.getLeaf().getKind()) && parentPath.getLeaf().getKind() != Tree.Kind.COMPILATION_UNIT && !TreeUtilities.CLASS_TREE_KINDS.contains((Object)parentPath.getLeaf().getKind())) {
            return true;
        }
        return Utilities.inAnonymousOrLocalClass(parentPath);
    }

    public static boolean isBoolean(TypeMirror type) {
        return type.getKind() == TypeKind.BOOLEAN;
    }

    public static Set<Element> getUsedElements(final CompilationInfo info) {
        final HashSet<Element> ret = new HashSet<Element>();
        final Trees trees = info.getTrees();
        new TreePathScanner<Void, Void>(){

            public Void visitIdentifier(IdentifierTree node, Void p) {
                this.addElement(trees.getElement(this.getCurrentPath()));
                return null;
            }

            public Void visitClass(ClassTree node, Void p) {
                for (Element element : JavadocImports.computeReferencedElements(info, this.getCurrentPath())) {
                    this.addElement(element);
                }
                return (Void)TreePathScanner.super.visitClass(node, (Object)p);
            }

            public Void visitMethod(MethodTree node, Void p) {
                for (Element element : JavadocImports.computeReferencedElements(info, this.getCurrentPath())) {
                    this.addElement(element);
                }
                return (Void)TreePathScanner.super.visitMethod(node, (Object)p);
            }

            public Void visitVariable(VariableTree node, Void p) {
                for (Element element : JavadocImports.computeReferencedElements(info, this.getCurrentPath())) {
                    this.addElement(element);
                }
                return (Void)TreePathScanner.super.visitVariable(node, (Object)p);
            }

            public Void visitCompilationUnit(CompilationUnitTree node, Void p) {
                this.scan((Iterable)node.getPackageAnnotations(), (Object)p);
                return (Void)this.scan((Iterable)node.getTypeDecls(), (Object)p);
            }

            private void addElement(Element element) {
                if (element != null) {
                    ret.add(element);
                }
            }
        }.scan((Tree)info.getCompilationUnit(), (Object)null);
        return ret;
    }

    public static boolean containErrors(Tree tree) {
        final AtomicBoolean containsErrors = new AtomicBoolean();
        new TreeScanner<Void, Void>(){

            public Void visitErroneous(ErroneousTree node, Void p) {
                containsErrors.set(true);
                return null;
            }

            public Void scan(Tree node, Void p) {
                if (containsErrors.get()) {
                    return null;
                }
                return (Void)TreeScanner.super.scan(node, (Object)p);
            }
        }.scan(tree, null);
        return containsErrors.get();
    }

    private static List<String> varNamesForType(TypeMirror type, Types types, Elements elements, String prefix) {
        switch (type.getKind()) {
            case ARRAY: {
                TypeElement iterableTE = elements.getTypeElement("java.lang.Iterable");
                DeclaredType iterable = iterableTE != null ? types.getDeclaredType(iterableTE, new TypeMirror[0]) : null;
                TypeMirror ct = ((ArrayType)type).getComponentType();
                if (ct.getKind() == TypeKind.ARRAY && iterable != null && types.isSubtype(ct, iterable)) {
                    return Utilities.varNamesForType(ct, types, elements, prefix);
                }
                ArrayList<String> vnct = new ArrayList<String>();
                for (String name : Utilities.varNamesForType(ct, types, elements, prefix)) {
                    vnct.add(name.endsWith("s") ? name + "es" : name + "s");
                }
                return vnct;
            }
            case BOOLEAN: 
            case BYTE: 
            case CHAR: 
            case DOUBLE: 
            case FLOAT: 
            case INT: 
            case LONG: 
            case SHORT: {
                String str = type.toString().substring(0, 1);
                return prefix != null && !prefix.equals(str) ? Collections.emptyList() : Collections.singletonList(str);
            }
            case TYPEVAR: {
                return Collections.singletonList(type.toString().toLowerCase(Locale.ENGLISH));
            }
            case ERROR: {
                String tn = ((ErrorType)type).asElement().getSimpleName().toString();
                if (tn.toUpperCase(Locale.ENGLISH).contentEquals(tn)) {
                    return Collections.singletonList(tn.toLowerCase(Locale.ENGLISH));
                }
                StringBuilder sb = new StringBuilder();
                ArrayList<String> al = new ArrayList<String>();
                if ("Iterator".equals(tn)) {
                    al.add("it");
                }
                while ((tn = Utilities.nextName(tn)).length() > 0) {
                    al.add(tn);
                    sb.append(tn.charAt(0));
                }
                if (sb.length() > 0) {
                    String s = sb.toString();
                    if (prefix == null || prefix.length() == 0 || s.startsWith(prefix)) {
                        al.add(s);
                    }
                }
                return al;
            }
            case DECLARED: {
                List<? extends TypeMirror> tas;
                TypeElement iterableTE = elements.getTypeElement("java.lang.Iterable");
                DeclaredType iterable = iterableTE != null ? types.getDeclaredType(iterableTE, new TypeMirror[0]) : null;
                String tn = ((DeclaredType)type).asElement().getSimpleName().toString();
                if (tn.toUpperCase(Locale.ENGLISH).contentEquals(tn)) {
                    return Collections.singletonList(tn.toLowerCase(Locale.ENGLISH));
                }
                StringBuilder sb = new StringBuilder();
                ArrayList<String> al = new ArrayList<String>();
                if ("Iterator".equals(tn)) {
                    al.add("it");
                }
                while ((tn = Utilities.nextName(tn)).length() > 0) {
                    al.add(tn);
                    sb.append(tn.charAt(0));
                }
                if (iterable != null && types.isSubtype(type, iterable) && (tas = ((DeclaredType)type).getTypeArguments()).size() > 0) {
                    TypeMirror et = tas.get(0);
                    if (et.getKind() == TypeKind.ARRAY || et.getKind() != TypeKind.WILDCARD && types.isSubtype(et, iterable)) {
                        al.addAll(Utilities.varNamesForType(et, types, elements, prefix));
                    } else {
                        for (String name : Utilities.varNamesForType(et, types, elements, prefix)) {
                            al.add(name.endsWith("s") ? name + "es" : name + "s");
                        }
                    }
                }
                if (sb.length() > 0) {
                    String s = sb.toString();
                    if (prefix == null || prefix.length() == 0 || s.startsWith(prefix)) {
                        al.add(s);
                    }
                }
                return al;
            }
            case WILDCARD: {
                TypeMirror bound = ((WildcardType)type).getExtendsBound();
                if (bound == null) {
                    bound = ((WildcardType)type).getSuperBound();
                }
                if (bound == null) break;
                return Utilities.varNamesForType(bound, types, elements, prefix);
            }
        }
        return Collections.emptyList();
    }

    private static String getConstName(String s) {
        StringBuilder sb = new StringBuilder();
        boolean prevUpper = true;
        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            if (Character.isUpperCase(c)) {
                if (!prevUpper) {
                    sb.append('_');
                }
                sb.append(c);
                prevUpper = true;
                continue;
            }
            sb.append(Character.toUpperCase(c));
            prevUpper = false;
        }
        return sb.toString();
    }

    private static String nextName(CharSequence name) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < name.length(); ++i) {
            char c = name.charAt(i);
            if (!Character.isUpperCase(c)) continue;
            char lc = Character.toLowerCase(c);
            sb.append(lc);
            sb.append(name.subSequence(i + 1, name.length()));
            break;
        }
        return sb.toString();
    }

    private static boolean isClashing(String varName, TypeMirror type, Iterable<? extends Element> locals) {
        if (SourceVersion.isKeyword(varName)) {
            return true;
        }
        if (type != null && type.getKind() == TypeKind.DECLARED && ((DeclaredType)type).asElement().getSimpleName().contentEquals(varName)) {
            return true;
        }
        for (Element e : locals) {
            if (!e.getKind().isField() && e.getKind() != ElementKind.LOCAL_VARIABLE && e.getKind() != ElementKind.RESOURCE_VARIABLE && e.getKind() != ElementKind.PARAMETER && e.getKind() != ElementKind.EXCEPTION_PARAMETER || !varName.contentEquals(e.getSimpleName())) continue;
            return true;
        }
        return false;
    }

    private static String varNameForPath(TreePath path) {
        if (path == null) {
            return null;
        }
        Tree tree = path.getLeaf();
        if (tree.getKind() == Tree.Kind.VARIABLE) {
            String name;
            if (((VariableTree)tree).getInitializer() != null && (name = Utilities.varNameForTree((Tree)((VariableTree)tree).getInitializer())) != null) {
                return name;
            }
            if (path.getParentPath().getLeaf().getKind() == Tree.Kind.ENHANCED_FOR_LOOP && ((EnhancedForLoopTree)path.getParentPath().getLeaf()).getVariable() == tree && (name = Utilities.varNameForTree((Tree)((EnhancedForLoopTree)path.getParentPath().getLeaf()).getExpression())) != null) {
                return Utilities.getSingular(name);
            }
            return null;
        }
        return Utilities.varNameForTree(path.getLeaf());
    }

    private static String varNameForTree(Tree et) {
        if (et == null) {
            return null;
        }
        switch (et.getKind()) {
            case IDENTIFIER: {
                return ((IdentifierTree)et).getName().toString();
            }
            case MEMBER_SELECT: {
                return ((MemberSelectTree)et).getIdentifier().toString();
            }
            case METHOD_INVOCATION: {
                return Utilities.varNameForTree((Tree)((MethodInvocationTree)et).getMethodSelect());
            }
            case NEW_CLASS: {
                return Utilities.firstToLower(Utilities.varNameForTree((Tree)((NewClassTree)et).getIdentifier()));
            }
            case PARAMETERIZED_TYPE: {
                return Utilities.firstToLower(Utilities.varNameForTree(((ParameterizedTypeTree)et).getType()));
            }
            case STRING_LITERAL: {
                String name = Utilities.guessLiteralName((String)((LiteralTree)et).getValue());
                if (name == null) {
                    return Utilities.firstToLower(String.class.getSimpleName());
                }
                return Utilities.firstToLower(name);
            }
            case VARIABLE: {
                return ((VariableTree)et).getName().toString();
            }
            case ARRAY_ACCESS: {
                String name = Utilities.varNameForTree((Tree)((ArrayAccessTree)et).getExpression());
                if (name != null) {
                    return Utilities.getSingular(name);
                }
                return null;
            }
            case ASSIGNMENT: {
                if (((AssignmentTree)et).getExpression() != null) {
                    return Utilities.varNameForTree((Tree)((AssignmentTree)et).getExpression());
                }
                return null;
            }
        }
        return null;
    }

    private static String getSingular(String name) {
        if (name.endsWith("ies") && name.length() > 3) {
            return name.substring(0, name.length() - 3) + 'y';
        }
        if (name.endsWith("s") && name.length() > 1) {
            return name.substring(0, name.length() - 1);
        }
        return name;
    }

    static String adjustName(String name) {
        if (name == null || "<error>".contentEquals(name)) {
            return null;
        }
        String shortName = null;
        if (name.startsWith("get") && name.length() > 3) {
            shortName = name.substring(3);
        }
        if (name.startsWith("is") && name.length() > 2) {
            shortName = name.substring(2);
        }
        if (shortName != null) {
            return Utilities.firstToLower(shortName);
        }
        if (SourceVersion.isKeyword(name)) {
            return "a" + Character.toUpperCase(name.charAt(0)) + name.substring(1);
        }
        return name;
    }

    private static String firstToLower(String name) {
        if (name.length() == 0) {
            return null;
        }
        StringBuilder result = new StringBuilder();
        boolean toLower = true;
        char last = Character.toLowerCase(name.charAt(0));
        for (int i = 1; i < name.length(); ++i) {
            if (toLower && (Character.isUpperCase(name.charAt(i)) || name.charAt(i) == '_')) {
                result.append(Character.toLowerCase(last));
            } else {
                result.append(last);
                toLower = false;
            }
            last = name.charAt(i);
        }
        result.append(toLower ? Character.toLowerCase(last) : last);
        if (SourceVersion.isKeyword(result)) {
            return "a" + name;
        }
        return result.toString();
    }

    private static String guessLiteralName(String str) {
        if (str.isEmpty()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); ++i) {
            char ch = str.charAt(i);
            if (ch == ' ') {
                sb.append('_');
            } else if (sb.length() == 0 ? Character.isJavaIdentifierStart(ch) : Character.isJavaIdentifierPart(ch)) {
                sb.append(ch);
            }
            if (sb.length() > 40) break;
        }
        if (sb.length() == 0) {
            return null;
        }
        return sb.toString();
    }

    public static TypeMirror resolveCapturedType(CompilationInfo info, TypeMirror tm) {
        TypeMirror type = Utilities.resolveCapturedTypeInt(info, tm);
        if (type.getKind() == TypeKind.WILDCARD) {
            TypeMirror tmirr = ((WildcardType)type).getExtendsBound();
            TypeMirror typeMirror = tmirr = tmirr != null ? tmirr : ((WildcardType)type).getSuperBound();
            if (tmirr != null) {
                return tmirr;
            }
            return info.getElements().getTypeElement("java.lang.Object").asType();
        }
        return type;
    }

    private static TypeMirror resolveCapturedTypeInt(CompilationInfo info, TypeMirror tm) {
        TypeMirror rct;
        TypeMirror extendsBound;
        if (tm == null) {
            return tm;
        }
        WildcardType orig = SourceUtils.resolveCapturedType((TypeMirror)tm);
        if (orig != null) {
            tm = orig;
        }
        if (tm.getKind() == TypeKind.WILDCARD && (rct = Utilities.resolveCapturedTypeInt(info, (extendsBound = ((WildcardType)tm).getExtendsBound()) != null ? extendsBound : ((WildcardType)tm).getSuperBound())) != null) {
            return rct.getKind() == TypeKind.WILDCARD ? rct : info.getTypes().getWildcardType(extendsBound != null ? rct : null, extendsBound == null ? rct : null);
        }
        if (tm.getKind() == TypeKind.DECLARED) {
            DeclaredType dt = (DeclaredType)tm;
            LinkedList<TypeMirror> typeArguments = new LinkedList<TypeMirror>();
            for (TypeMirror t : dt.getTypeArguments()) {
                typeArguments.add(Utilities.resolveCapturedTypeInt(info, t));
            }
            TypeMirror enclosingType = dt.getEnclosingType();
            if (enclosingType.getKind() == TypeKind.DECLARED) {
                return info.getTypes().getDeclaredType((DeclaredType)enclosingType, (TypeElement)dt.asElement(), typeArguments.toArray(new TypeMirror[0]));
            }
            return info.getTypes().getDeclaredType((TypeElement)dt.asElement(), typeArguments.toArray(new TypeMirror[0]));
        }
        if (tm.getKind() == TypeKind.ARRAY) {
            ArrayType at = (ArrayType)tm;
            return info.getTypes().getArrayType(Utilities.resolveCapturedTypeInt(info, at.getComponentType()));
        }
        return tm;
    }

    @NonNull
    public static List<ExecutableElement> fuzzyResolveMethodInvocation(CompilationInfo info, TreePath path, List<TypeMirror> proposed, int[] index) {
        assert (path.getLeaf().getKind() == Tree.Kind.METHOD_INVOCATION || path.getLeaf().getKind() == Tree.Kind.NEW_CLASS);
        if (path.getLeaf().getKind() == Tree.Kind.METHOD_INVOCATION) {
            String methodName;
            LinkedList<TypeMirror> actualTypes = new LinkedList<TypeMirror>();
            MethodInvocationTree mit = (MethodInvocationTree)path.getLeaf();
            for (Tree a : mit.getArguments()) {
                TreePath tp = new TreePath(path, a);
                actualTypes.add(info.getTrees().getTypeMirror(tp));
            }
            ArrayList<Pair> on = new ArrayList<Pair>();
            switch (mit.getMethodSelect().getKind()) {
                case IDENTIFIER: {
                    methodName = ((IdentifierTree)mit.getMethodSelect()).getName().toString();
                    Scope s = info.getTrees().getScope(path);
                    TypeElement enclosingClass = s.getEnclosingClass();
                    while (enclosingClass != null) {
                        on.add(Pair.of((Object)enclosingClass.asType(), (Object)false));
                        enclosingClass = info.getElementUtilities().enclosingTypeElement((Element)enclosingClass);
                    }
                    CompilationUnitTree cut = info.getCompilationUnit();
                    for (ImportTree imp : cut.getImports()) {
                        Name selected;
                        if (!imp.isStatic() || imp.getQualifiedIdentifier() == null || imp.getQualifiedIdentifier().getKind() != Tree.Kind.MEMBER_SELECT || !(selected = ((MemberSelectTree)imp.getQualifiedIdentifier()).getIdentifier()).contentEquals("*") && !selected.contentEquals(methodName)) continue;
                        TreePath tp = new TreePath(new TreePath(new TreePath(new TreePath(cut), (Tree)imp), imp.getQualifiedIdentifier()), (Tree)((MemberSelectTree)imp.getQualifiedIdentifier()).getExpression());
                        Element el = info.getTrees().getElement(tp);
                        if (el == null) continue;
                        on.add(Pair.of((Object)el.asType(), (Object)true));
                    }
                    break;
                }
                case MEMBER_SELECT: {
                    on.add(Pair.of((Object)info.getTrees().getTypeMirror(new TreePath(path, (Tree)((MemberSelectTree)mit.getMethodSelect()).getExpression())), (Object)false));
                    methodName = ((MemberSelectTree)mit.getMethodSelect()).getIdentifier().toString();
                    break;
                }
                default: {
                    throw new IllegalStateException();
                }
            }
            ArrayList<ExecutableElement> result = new ArrayList<ExecutableElement>();
            for (Pair type : on) {
                if (type.first() == null || ((TypeMirror)type.first()).getKind() != TypeKind.DECLARED) continue;
                result.addAll(Utilities.resolveMethod(info, actualTypes, (DeclaredType)type.first(), (Boolean)type.second(), false, methodName, proposed, index));
            }
            return result;
        }
        if (path.getLeaf().getKind() == Tree.Kind.NEW_CLASS) {
            LinkedList<TypeMirror> actualTypes = new LinkedList<TypeMirror>();
            NewClassTree nct = (NewClassTree)path.getLeaf();
            for (Tree a : nct.getArguments()) {
                TreePath tp = new TreePath(path, a);
                actualTypes.add(info.getTrees().getTypeMirror(tp));
            }
            TypeMirror on = info.getTrees().getTypeMirror(new TreePath(path, (Tree)nct.getIdentifier()));
            if (on == null || on.getKind() != TypeKind.DECLARED) {
                return Collections.emptyList();
            }
            return Utilities.resolveMethod(info, actualTypes, (DeclaredType)on, false, true, null, proposed, index);
        }
        return Collections.emptyList();
    }

    private static Iterable<ExecutableElement> execsIn(CompilationInfo info, TypeElement e, boolean constr, String name) {
        if (constr) {
            return ElementFilter.constructorsIn(info.getElements().getAllMembers(e));
        }
        LinkedList<ExecutableElement> result = new LinkedList<ExecutableElement>();
        for (ExecutableElement ee : ElementFilter.methodsIn(info.getElements().getAllMembers(e))) {
            if (!name.equals(ee.getSimpleName().toString())) continue;
            result.add(ee);
        }
        return result;
    }

    private static List<ExecutableElement> resolveMethod(CompilationInfo info, List<TypeMirror> foundTypes, DeclaredType on, boolean onlyStatic, boolean constr, String name, List<TypeMirror> candidateTypes, int[] index) {
        if (on.asElement() == null) {
            return Collections.emptyList();
        }
        LinkedList<ExecutableElement> found = new LinkedList<ExecutableElement>();
        block0 : for (ExecutableElement ee : Utilities.execsIn(info, (TypeElement)on.asElement(), constr, name)) {
            TypeMirror currType = ((TypeElement)ee.getEnclosingElement()).asType();
            if (!info.getTypes().isSubtype(on, currType) && !on.asElement().equals(((DeclaredType)currType).asElement()) || onlyStatic && !ee.getModifiers().contains((Object)Modifier.STATIC) || ee.getParameters().size() != foundTypes.size()) continue;
            TypeMirror innerCandidate = null;
            int innerIndex = -1;
            ExecutableType et = (ExecutableType)info.getTypes().asMemberOf(on, ee);
            Iterator<? extends TypeMirror> formal = et.getParameterTypes().iterator();
            Iterator<TypeMirror> actual = foundTypes.iterator();
            boolean mismatchFound = false;
            int i = 0;
            while (formal.hasNext() && actual.hasNext()) {
                TypeMirror currentFormal = formal.next();
                TypeMirror currentActual = actual.next();
                if (!info.getTypes().isAssignable(currentActual, currentFormal) || currentActual.getKind() == TypeKind.ERROR) {
                    if (mismatchFound) continue block0;
                    mismatchFound = true;
                    innerCandidate = currentFormal;
                    innerIndex = i;
                }
                ++i;
            }
            if (!mismatchFound) continue;
            if (candidateTypes.isEmpty()) {
                index[0] = innerIndex;
                candidateTypes.add(innerCandidate);
                found.add(ee);
                continue;
            }
            if (index[0] != innerIndex) continue;
            boolean add = true;
            for (TypeMirror tm : candidateTypes) {
                if (!info.getTypes().isSameType(tm, innerCandidate)) continue;
                add = false;
                break;
            }
            if (!add) continue;
            candidateTypes.add(innerCandidate);
            found.add(ee);
        }
        return found;
    }

    private Utilities() {
    }

    static {
        preferencesTracker = new PreferenceChangeListener(){

            @Override
            public void preferenceChange(PreferenceChangeEvent evt) {
                String settingName;
                String string = settingName = evt == null ? null : evt.getKey();
                if (settingName == null || "completion-case-sensitive".equals(settingName)) {
                    caseSensitive = preferences.getBoolean("completion-case-sensitive", false);
                }
                if (settingName == null || "show-deprecated-members".equals(settingName)) {
                    showDeprecatedMembers = preferences.getBoolean("show-deprecated-members", true);
                }
                if (settingName == null || "guessMethodArguments".equals(settingName)) {
                    guessMethodArguments = preferences.getBoolean("guessMethodArguments", true);
                }
                if (settingName == null || "javaAutoPopupOnIdentifierPart".equals(settingName)) {
                    autoPopupOnJavaIdentifierPart = preferences.getBoolean("javaAutoPopupOnIdentifierPart", false);
                }
                if (settingName == null || "javaAutoCompletionTriggers".equals(settingName)) {
                    javaCompletionAutoPopupTriggers = preferences.get("javaAutoCompletionTriggers", ".");
                }
                if (settingName == null || "javaCompletionSelectors".equals(settingName)) {
                    javaCompletionSelectors = preferences.get("javaCompletionSelectors", ".,;:([+-=");
                }
                if (settingName == null || "javadocAutoCompletionTriggers".equals(settingName)) {
                    javadocCompletionAutoPopupTriggers = preferences.get("javadocAutoCompletionTriggers", ".#@");
                }
                if (settingName == null || "javadocCompletionSelectors".equals(settingName)) {
                    javadocCompletionSelectors = preferences.get("javadocCompletionSelectors", ".#");
                }
                if (settingName == null || "javaCompletionBlacklist".equals(settingName)) {
                    String blacklist = preferences.get("javaCompletionBlacklist", "");
                    Utilities.updateExcluder(excludeRef, blacklist);
                }
                if (settingName == null || "javaCompletionWhitelist".equals(settingName)) {
                    String whitelist = preferences.get("javaCompletionWhitelist", "");
                    Utilities.updateExcluder(includeRef, whitelist);
                }
                if (settingName == null || "javaCompletionExcluderMethods".equals(settingName)) {
                    javaCompletionExcluderMethods = preferences.getBoolean("javaCompletionExcluderMethods", false);
                }
                if (settingName == null || "javaCompletionSubwords".equals(settingName)) {
                    javaCompletionSubwords = preferences.getBoolean("javaCompletionSubwords", false);
                }
            }
        };
        cachedPrefix = null;
        cachedCamelCasePattern = null;
        cachedSubwordsPattern = null;
        excludeRef = new AtomicReference();
        includeRef = new AtomicReference();
    }

    private static class ElementNameVisitor
    extends SimpleElementVisitor8<StringBuilder, Boolean> {
        private ElementNameVisitor() {
            super(new StringBuilder());
        }

        @Override
        public StringBuilder visitPackage(PackageElement e, Boolean p) {
            return ((StringBuilder)this.DEFAULT_VALUE).append((p != false ? e.getQualifiedName() : e.getSimpleName()).toString());
        }

        @Override
        public StringBuilder visitType(TypeElement e, Boolean p) {
            return ((StringBuilder)this.DEFAULT_VALUE).append((p != false ? e.getQualifiedName() : e.getSimpleName()).toString());
        }
    }

}

