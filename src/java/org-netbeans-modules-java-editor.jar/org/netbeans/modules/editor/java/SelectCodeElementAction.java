/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.source.Comment
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.progress.ProgressUtils
 *  org.netbeans.editor.BaseAction
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.openide.text.NbDocument
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.editor.java;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.MissingResourceException;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.SwingUtilities;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyledDocument;
import org.netbeans.api.java.source.Comment;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.editor.BaseAction;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.editor.java.JavaKit;
import org.openide.text.NbDocument;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

final class SelectCodeElementAction
extends BaseAction {
    private boolean selectNext;

    public SelectCodeElementAction(String name, boolean selectNext) {
        super(name);
        this.selectNext = selectNext;
        String desc = this.getShortDescription();
        if (desc != null) {
            this.putValue("ShortDescription", (Object)desc);
        }
    }

    public String getShortDescription() {
        String shortDesc;
        String name = (String)this.getValue("Name");
        if (name == null) {
            return null;
        }
        try {
            shortDesc = NbBundle.getBundle(JavaKit.class).getString(name);
        }
        catch (MissingResourceException mre) {
            shortDesc = name;
        }
        return shortDesc;
    }

    public void actionPerformed(ActionEvent evt, JTextComponent target) {
        if (target != null) {
            int selectionStartOffset = target.getSelectionStart();
            int selectionEndOffset = target.getSelectionEnd();
            if (selectionEndOffset > selectionStartOffset || this.selectNext) {
                SelectionHandler handler = (SelectionHandler)target.getClientProperty(SelectionHandler.class);
                if (handler == null) {
                    handler = new SelectionHandler(target, this.getShortDescription());
                    target.addCaretListener(handler);
                    target.putClientProperty(SelectionHandler.class, handler);
                }
                if (this.selectNext) {
                    handler.selectNext();
                } else {
                    handler.selectPrevious();
                }
            }
        }
    }

    private static final class SelectionInfo {
        private int startOffset;
        private int endOffset;

        SelectionInfo(int startOffset, int endOffset) {
            this.startOffset = startOffset;
            this.endOffset = endOffset;
        }

        public int getStartOffset() {
            return this.startOffset;
        }

        public int getEndOffset() {
            return this.endOffset;
        }

        public String toString() {
            return "<" + this.startOffset + ":" + this.endOffset + ">";
        }
    }

    private static final class SelectionHandler
    implements CaretListener,
    Runnable {
        private final JTextComponent target;
        private final String name;
        private SelectionInfo[] selectionInfos;
        private int selIndex = -1;
        private boolean ignoreNextCaretUpdate;

        SelectionHandler(JTextComponent target, String name) {
            this.target = target;
            this.name = name;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void selectNext() {
            JavaSource js;
            SelectionInfo[] si;
            SelectionHandler selectionHandler = this;
            synchronized (selectionHandler) {
                si = this.selectionInfos;
            }
            if (si == null && (js = JavaSource.forDocument((Document)this.target.getDocument())) != null) {
                final AtomicBoolean cancel = new AtomicBoolean();
                ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

                    @Override
                    public void run() {
                        try {
                            js.runUserActionTask((Task)new Task<CompilationController>(){

                                /*
                                 * WARNING - Removed try catching itself - possible behaviour change.
                                 */
                                public void run(CompilationController cc) throws Exception {
                                    try {
                                        if (cancel.get()) {
                                            return;
                                        }
                                        cc.toPhase(JavaSource.Phase.RESOLVED);
                                        if (cancel.get()) {
                                            return;
                                        }
                                        SelectionHandler selectionHandler = SelectionHandler.this;
                                        synchronized (selectionHandler) {
                                            SelectionHandler.this.selectionInfos = SelectionHandler.this.initSelectionPath(SelectionHandler.this.target, cc);
                                            if (SelectionHandler.this.selectionInfos != null && SelectionHandler.this.selectionInfos.length > 0) {
                                                SelectionHandler.this.selIndex = 0;
                                            }
                                        }
                                    }
                                    catch (IOException ex) {
                                        Exceptions.printStackTrace((Throwable)ex);
                                    }
                                }
                            }, true);
                        }
                        catch (IOException ex) {
                            Exceptions.printStackTrace((Throwable)ex);
                        }
                    }

                }, (String)this.name, (AtomicBoolean)cancel, (boolean)false);
            }
            this.run();
        }

        public synchronized void selectPrevious() {
            if (this.selIndex > 0) {
                this.select(this.selectionInfos[--this.selIndex]);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void select(SelectionInfo selectionInfo) {
            assert (SwingUtilities.isEventDispatchThread());
            Caret caret = this.target.getCaret();
            this.ignoreNextCaretUpdate = true;
            try {
                caret.setDot(selectionInfo.getEndOffset());
                caret.moveDot(selectionInfo.getStartOffset());
            }
            finally {
                this.ignoreNextCaretUpdate = false;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void caretUpdate(CaretEvent e) {
            assert (SwingUtilities.isEventDispatchThread());
            if (!this.ignoreNextCaretUpdate) {
                SelectionHandler selectionHandler = this;
                synchronized (selectionHandler) {
                    this.selectionInfos = null;
                    this.selIndex = -1;
                }
            }
        }

        private SelectionInfo[] initSelectionPath(JTextComponent target, CompilationController ci) {
            ArrayList<SelectionInfo> positions = new ArrayList<SelectionInfo>();
            int caretPos = target.getCaretPosition();
            positions.add(new SelectionInfo(caretPos, caretPos));
            if (target.getDocument() instanceof BaseDocument) {
                try {
                    int[] block = Utilities.getIdentifierBlock((BaseDocument)((BaseDocument)target.getDocument()), (int)caretPos);
                    if (block != null) {
                        positions.add(new SelectionInfo(block[0], block[1]));
                    }
                }
                catch (BadLocationException ble) {
                    // empty catch block
                }
            }
            SourcePositions sp = ci.getTrees().getSourcePositions();
            TreeUtilities treeUtilities = ci.getTreeUtilities();
            TreePath tp = treeUtilities.pathFor(caretPos);
            for (Tree tree : tp) {
                int startPos = (int)sp.getStartPosition(tp.getCompilationUnit(), tree);
                int endPos = (int)sp.getEndPosition(tp.getCompilationUnit(), tree);
                positions.add(new SelectionInfo(startPos, endPos));
                if (tree.getKind() == Tree.Kind.STRING_LITERAL) {
                    positions.add(new SelectionInfo(startPos + 1, endPos - 1));
                }
                if (tree.getKind() == Tree.Kind.BLOCK) {
                    positions.add(new SelectionInfo(startPos + 1, endPos - 1));
                }
                int docBegin = Integer.MAX_VALUE;
                for (Comment comment : treeUtilities.getComments(tree, true)) {
                    docBegin = Math.min(comment.pos(), docBegin);
                }
                int docEnd = Integer.MIN_VALUE;
                for (Comment comment2 : treeUtilities.getComments(tree, false)) {
                    docEnd = Math.max(comment2.endPos(), docEnd);
                }
                if (docBegin != Integer.MAX_VALUE && docEnd != Integer.MIN_VALUE) {
                    positions.add(new SelectionInfo(docBegin, docEnd));
                    continue;
                }
                if (docBegin == Integer.MAX_VALUE && docEnd != Integer.MIN_VALUE) {
                    positions.add(new SelectionInfo(startPos, docEnd));
                    continue;
                }
                if (docBegin == Integer.MAX_VALUE || docEnd != Integer.MIN_VALUE) continue;
                positions.add(new SelectionInfo(docBegin, endPos));
            }
            TreeSet<SelectionInfo> orderedPositions = new TreeSet<SelectionInfo>(new Comparator<SelectionInfo>(){

                @Override
                public int compare(SelectionInfo o1, SelectionInfo o2) {
                    int offsetStartDiff = o2.getStartOffset() - o1.getStartOffset();
                    if (0 == offsetStartDiff) {
                        return o1.getEndOffset() - o2.getEndOffset();
                    }
                    return offsetStartDiff;
                }
            });
            orderedPositions.addAll(positions);
            if (target.getDocument() instanceof StyledDocument) {
                SelectionInfo selectionInfo;
                StyledDocument doc = (StyledDocument)target.getDocument();
                Iterator it = positions.iterator();
                SelectionInfo selectionInfo2 = selectionInfo = it.hasNext() ? (SelectionInfo)it.next() : null;
                while (selectionInfo != null) {
                    boolean isEmptySelection;
                    int startOffset = NbDocument.findLineOffset((StyledDocument)doc, (int)NbDocument.findLineNumber((StyledDocument)doc, (int)selectionInfo.getStartOffset()));
                    int endOffset = doc.getLength();
                    try {
                        endOffset = NbDocument.findLineOffset((StyledDocument)doc, (int)(NbDocument.findLineNumber((StyledDocument)doc, (int)selectionInfo.getEndOffset()) + 1));
                    }
                    catch (IndexOutOfBoundsException ioobe) {
                        // empty catch block
                    }
                    SelectionInfo next = it.hasNext() ? (SelectionInfo)it.next() : null;
                    boolean bl = isEmptySelection = selectionInfo.startOffset == selectionInfo.endOffset;
                    if (!isEmptySelection && (next == null || startOffset >= next.startOffset && endOffset <= next.endOffset)) {
                        orderedPositions.add(new SelectionInfo(startOffset, endOffset));
                    }
                    selectionInfo = next;
                }
            }
            return orderedPositions.toArray(new SelectionInfo[orderedPositions.size()]);
        }

        @Override
        public synchronized void run() {
            if (this.selectionInfos != null && this.selIndex < this.selectionInfos.length - 1) {
                this.select(this.selectionInfos[++this.selIndex]);
            }
        }

    }

}

