/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ui.ElementJavadoc
 *  org.netbeans.spi.editor.completion.CompletionDocumentation
 */
package org.netbeans.modules.editor.java;

import java.net.URL;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import javax.lang.model.element.Element;
import javax.swing.Action;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ui.ElementJavadoc;
import org.netbeans.spi.editor.completion.CompletionDocumentation;

public class JavaCompletionDoc
implements CompletionDocumentation {
    private ElementJavadoc elementJavadoc;

    public JavaCompletionDoc(ElementJavadoc elementJavadoc) {
        this.elementJavadoc = elementJavadoc;
    }

    public JavaCompletionDoc resolveLink(String link) {
        ElementJavadoc doc = this.elementJavadoc.resolveLink(link);
        return doc != null ? new JavaCompletionDoc(doc) : null;
    }

    public URL getURL() {
        return this.elementJavadoc.getURL();
    }

    public String getText() {
        return this.elementJavadoc.getText();
    }

    public Future<String> getFutureText() {
        return this.elementJavadoc.getTextAsync();
    }

    public Action getGotoSourceAction() {
        return this.elementJavadoc.getGotoSourceAction();
    }

    public static final JavaCompletionDoc create(CompilationController controller, Element element, Callable<Boolean> callable) {
        return new JavaCompletionDoc(ElementJavadoc.create((CompilationInfo)controller, (Element)element, callable));
    }
}

