/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.util.TreePath
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.SourceUtils
 */
package org.netbeans.modules.editor.java;

import com.sun.source.util.TreePath;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ErrorType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.type.WildcardType;
import javax.lang.model.util.SimpleTypeVisitor6;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.SourceUtils;

public class AutoImport
extends SimpleTypeVisitor6<Void, Void> {
    private static final String CAPTURED_WILDCARD = "<captured wildcard>";
    private CompilationInfo info;
    private StringBuilder builder;
    private TreePath path;
    private Set<String> importedTypes = new HashSet<String>();

    private AutoImport(CompilationInfo info) {
        this.info = info;
    }

    public static AutoImport get(CompilationInfo info) {
        return new AutoImport(info);
    }

    public static CharSequence resolveImport(CompilationInfo info, TreePath treePath, TypeMirror type) {
        AutoImport imp = new AutoImport(info);
        return imp.resolveImport(treePath, type);
    }

    public CharSequence resolveImport(TreePath treePath, TypeMirror type) {
        this.builder = new StringBuilder();
        this.path = treePath;
        this.visit(type, null);
        return this.builder;
    }

    public Set<String> getAutoImportedTypes() {
        return this.importedTypes;
    }

    @Override
    public Void defaultAction(TypeMirror type, Void p) {
        this.builder.append(type);
        return null;
    }

    @Override
    public Void visitArray(ArrayType type, Void p) {
        this.visit(type.getComponentType());
        this.builder.append("[]");
        return null;
    }

    @Override
    public Void visitDeclared(DeclaredType type, Void p) {
        TypeElement element = (TypeElement)type.asElement();
        String name = element.getQualifiedName().toString();
        ElementKind kind = element.getEnclosingElement().getKind();
        if (kind.isClass() || kind.isInterface() || kind == ElementKind.PACKAGE) {
            try {
                String s = SourceUtils.resolveImport((CompilationInfo)this.info, (TreePath)this.path, (String)name);
                int idx = s.indexOf(46);
                if (idx < 0) {
                    this.importedTypes.add(name);
                } else {
                    this.importedTypes.add(name.substring(0, name.length() - s.length() + idx));
                }
                name = s;
            }
            catch (Exception e) {
                Logger.getLogger("global").log(Level.INFO, null, e);
            }
        }
        this.builder.append(name);
        Iterator<? extends TypeMirror> it = type.getTypeArguments().iterator();
        if (it.hasNext()) {
            this.builder.append('<');
            while (it.hasNext()) {
                this.visit(it.next());
                if (!it.hasNext()) continue;
                this.builder.append(", ");
            }
            this.builder.append('>');
        }
        return null;
    }

    @Override
    public Void visitTypeVariable(TypeVariable type, Void p) {
        Name name;
        Element e = type.asElement();
        if (e != null && !"<captured wildcard>".contentEquals(name = e.getSimpleName())) {
            this.builder.append(name);
            return null;
        }
        this.builder.append("?");
        TypeMirror bound = type.getLowerBound();
        if (bound != null && bound.getKind() != TypeKind.NULL) {
            this.builder.append(" super ");
            this.visit(bound);
        } else {
            bound = type.getUpperBound();
            if (bound != null && bound.getKind() != TypeKind.NULL) {
                this.builder.append(" extends ");
                if (bound.getKind() == TypeKind.TYPEVAR) {
                    bound = ((TypeVariable)bound).getLowerBound();
                }
                this.visit(bound);
            }
        }
        return null;
    }

    @Override
    public Void visitWildcard(WildcardType type, Void p) {
        this.builder.append("?");
        TypeMirror bound = type.getSuperBound();
        if (bound == null) {
            bound = type.getExtendsBound();
            if (bound != null) {
                this.builder.append(" extends ");
                if (bound.getKind() == TypeKind.WILDCARD) {
                    bound = ((WildcardType)bound).getSuperBound();
                }
                this.visit(bound);
            }
        } else {
            this.builder.append(" super ");
            this.visit(bound);
        }
        return null;
    }

    @Override
    public Void visitError(ErrorType type, Void p) {
        Element e = type.asElement();
        if (e instanceof TypeElement) {
            TypeElement te = (TypeElement)e;
            this.builder.append(te.getSimpleName());
        }
        return null;
    }
}

