/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.source.ClassIndex
 *  org.netbeans.api.java.source.ClassIndex$NameKind
 *  org.netbeans.api.java.source.ClassIndex$SearchScope
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.editor.java;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import javax.annotation.processing.Completion;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.tools.JavaFileObject;
import org.netbeans.api.java.source.ClassIndex;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;

public class SupportedAnnotationTypesCompletion
implements Processor {
    private static final Set<String> supportedAnnotationTypes = new HashSet<String>(Arrays.asList(SupportedAnnotationTypes.class.getName()));
    private Reference<ProcessingEnvironment> processingEnv;

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        return false;
    }

    @Override
    public Iterable<? extends Completion> getCompletions(Element element, AnnotationMirror annotation, ExecutableElement member, String userText) {
        FileObject owner;
        ProcessingEnvironment processingEnv = this.processingEnv.get();
        if (processingEnv == null) {
            return Collections.emptyList();
        }
        TypeElement annotationObj = processingEnv.getElementUtils().getTypeElement("java.lang.annotation.Annotation");
        if (annotationObj == null) {
            return Collections.emptyList();
        }
        Trees trees = Trees.instance((ProcessingEnvironment)processingEnv);
        TreePath path = trees.getPath(element);
        if (path == null) {
            return Collections.emptyList();
        }
        try {
            owner = URLMapper.findFileObject((URL)path.getCompilationUnit().getSourceFile().toUri().toURL());
        }
        catch (MalformedURLException ex) {
            Exceptions.printStackTrace((Throwable)ex);
            return Collections.emptyList();
        }
        ClassIndex ci = ClasspathInfo.create((FileObject)owner).getClassIndex();
        if (ci == null) {
            return Collections.emptyList();
        }
        LinkedList<CompletionImpl> result = new LinkedList<CompletionImpl>();
        for (ElementHandle eh : ci.getDeclaredTypes("", ClassIndex.NameKind.PREFIX, EnumSet.of(ClassIndex.SearchScope.DEPENDENCIES, ClassIndex.SearchScope.SOURCE))) {
            if (eh.getKind() != ElementKind.ANNOTATION_TYPE) continue;
            result.add(new CompletionImpl(eh.getQualifiedName()));
        }
        return result;
    }

    @Override
    public Set<String> getSupportedOptions() {
        return Collections.emptySet();
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return supportedAnnotationTypes;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latest();
    }

    @Override
    public void init(ProcessingEnvironment processingEnv) {
        this.processingEnv = new WeakReference<ProcessingEnvironment>(processingEnv);
    }

    private final class CompletionImpl
    implements Completion {
        private final String value;
        private final String message;

        public CompletionImpl(String value) {
            this(value, null);
        }

        public CompletionImpl(String value, String message) {
            this.value = value;
            this.message = message;
        }

        @Override
        public String getValue() {
            return this.value;
        }

        @Override
        public String getMessage() {
            return this.message;
        }
    }

}

