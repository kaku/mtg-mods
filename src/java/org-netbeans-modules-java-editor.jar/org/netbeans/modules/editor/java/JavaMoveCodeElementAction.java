/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.api.progress.ProgressUtils
 *  org.netbeans.editor.BaseAction
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.GuardedDocument
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.indent.api.Indent
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.editor.java;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.List;
import java.util.MissingResourceException;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.editor.BaseAction;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.GuardedDocument;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.indent.api.Indent;
import org.openide.util.NbBundle;

final class JavaMoveCodeElementAction
extends BaseAction {
    private boolean downward;

    public JavaMoveCodeElementAction(String name, boolean downward) {
        super(name);
        this.downward = downward;
    }

    public void actionPerformed(ActionEvent evt, final JTextComponent target) {
        if (target == null || !target.isEditable() || !target.isEnabled()) {
            target.getToolkit().beep();
            return;
        }
        final BaseDocument doc = (BaseDocument)target.getDocument();
        final JavaSource js = JavaSource.forDocument((Document)doc);
        if (js != null) {
            final AtomicBoolean cancel = new AtomicBoolean();
            ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

                @Override
                public void run() {
                    try {
                        js.runUserActionTask((Task)new Task<CompilationController>(){

                            /*
                             * WARNING - Removed try catching itself - possible behaviour change.
                             */
                            public void run(final CompilationController controller) throws Exception {
                                try {
                                    if (cancel.get()) {
                                        return;
                                    }
                                    controller.toPhase(JavaSource.Phase.PARSED);
                                    if (cancel.get()) {
                                        return;
                                    }
                                    final Indent indent = Indent.get((Document)doc);
                                    indent.lock();
                                    try {
                                        doc.runAtomicAsUser(new Runnable(){

                                            /*
                                             * WARNING - Removed try catching itself - possible behaviour change.
                                             */
                                            @Override
                                            public void run() {
                                                block12 : {
                                                    DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
                                                    try {
                                                        boolean insideBlock;
                                                        int[] currentBounds = new int[]{target.getSelectionStart(), target.getSelectionEnd()};
                                                        TreePath tp = JavaMoveCodeElementAction.this.widenToElementBounds((CompilationInfo)controller, doc, currentBounds);
                                                        if (tp == null) {
                                                            target.getToolkit().beep();
                                                            return;
                                                        }
                                                        boolean bl = insideBlock = tp.getLeaf().getKind() == Tree.Kind.BLOCK;
                                                        if (JavaMoveCodeElementAction.this.downward) {
                                                            int destinationOffset = JavaMoveCodeElementAction.this.findDestinationOffset((CompilationInfo)controller, doc, currentBounds[1], insideBlock);
                                                            if (destinationOffset < 0) {
                                                                target.getToolkit().beep();
                                                                return;
                                                            }
                                                            String text = doc.getText(currentBounds[1], destinationOffset - currentBounds[1]);
                                                            doc.remove(currentBounds[1], text.length());
                                                            doc.insertString(currentBounds[0], text, null);
                                                            indent.reindent(currentBounds[0] + text.length(), currentBounds[1] + text.length());
                                                            break block12;
                                                        }
                                                        int destinationOffset = JavaMoveCodeElementAction.this.findDestinationOffset((CompilationInfo)controller, doc, currentBounds[0] - 1, insideBlock);
                                                        if (destinationOffset < 0) {
                                                            target.getToolkit().beep();
                                                            return;
                                                        }
                                                        String text = doc.getText(destinationOffset, currentBounds[0] - destinationOffset);
                                                        doc.insertString(currentBounds[1], text, null);
                                                        doc.remove(destinationOffset, text.length());
                                                        indent.reindent(destinationOffset, currentBounds[1] - text.length());
                                                    }
                                                    catch (BadLocationException ble) {
                                                        target.getToolkit().beep();
                                                    }
                                                    finally {
                                                        DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    finally {
                                        indent.unlock();
                                    }
                                }
                                catch (IOException ioe) {
                                    target.getToolkit().beep();
                                }
                            }

                        }, true);
                    }
                    catch (IOException ioe) {
                        target.getToolkit().beep();
                    }
                }

            }, (String)this.getShortDescription(), (AtomicBoolean)cancel, (boolean)false);
        }
    }

    private TreePath widenToElementBounds(CompilationInfo cInfo, BaseDocument doc, int[] bounds) {
        TreePath tp;
        SourcePositions sp = cInfo.getTrees().getSourcePositions();
        TreeUtilities tu = cInfo.getTreeUtilities();
        int startOffset = this.getLineStart(doc, bounds[0]);
        int endOffset = this.getLineEnd(doc, bounds[1]);
        do {
            TokenSequence ts;
            if ((ts = SourceUtils.getJavaTokenSequence((TokenHierarchy)cInfo.getTokenHierarchy(), (int)startOffset)) != null && (ts.moveNext() || ts.movePrevious()) && ts.offset() < startOffset && ts.token().id() != JavaTokenId.WHITESPACE) {
                startOffset = this.getLineStart(doc, ts.offset());
                continue;
            }
            ts = SourceUtils.getJavaTokenSequence((TokenHierarchy)cInfo.getTokenHierarchy(), (int)endOffset);
            if (ts != null && (ts.moveNext() || ts.movePrevious()) && ts.offset() < endOffset && ts.token().id() != JavaTokenId.WHITESPACE) {
                endOffset = this.getLineEnd(doc, ts.offset() + ts.token().length());
                continue;
            }
            boolean finish = true;
            for (tp = tu.pathFor((int)startOffset); tp != null; tp = tp.getParentPath()) {
                Tree leaf = tp.getLeaf();
                List children = null;
                switch (leaf.getKind()) {
                    case BLOCK: {
                        if ((long)endOffset >= sp.getEndPosition(tp.getCompilationUnit(), leaf)) break;
                        children = ((BlockTree)leaf).getStatements();
                        break;
                    }
                    case CLASS: 
                    case INTERFACE: 
                    case ANNOTATION_TYPE: 
                    case ENUM: {
                        if ((long)endOffset >= sp.getEndPosition(tp.getCompilationUnit(), leaf)) break;
                        children = ((ClassTree)leaf).getMembers();
                    }
                }
                if (children == null) continue;
                for (Tree tree : children) {
                    int startPos = (int)sp.getStartPosition(tp.getCompilationUnit(), tree);
                    int endPos = (int)sp.getEndPosition(tp.getCompilationUnit(), tree);
                    if (endPos <= startOffset) continue;
                    if (startPos < startOffset) {
                        startOffset = this.getLineStart(doc, startPos);
                        finish = false;
                    }
                    if (startPos >= endOffset || endOffset >= endPos) continue;
                    endOffset = this.getLineEnd(doc, endPos);
                    finish = false;
                }
                break;
            }
            if (finish) break;
        } while (true);
        bounds[0] = startOffset;
        bounds[1] = endOffset;
        return tp;
    }

    private int findDestinationOffset(CompilationInfo cInfo, BaseDocument doc, int offset, boolean insideBlock) {
        TreeUtilities tu = cInfo.getTreeUtilities();
        SourcePositions sp = cInfo.getTrees().getSourcePositions();
        block9 : while (offset >= 0 && offset <= doc.getLength()) {
            TokenSequence ts;
            int destinationOffset;
            int n = destinationOffset = this.downward ? this.getLineEnd(doc, offset) : this.getLineStart(doc, offset);
            if (doc instanceof GuardedDocument && ((GuardedDocument)doc).isPosGuarded(destinationOffset)) {
                return -1;
            }
            if (destinationOffset < doc.getLength() && (ts = SourceUtils.getJavaTokenSequence((TokenHierarchy)cInfo.getTokenHierarchy(), (int)destinationOffset)) != null && (ts.moveNext() || ts.movePrevious()) && ts.offset() < destinationOffset && ts.token().id() != JavaTokenId.WHITESPACE) {
                offset = this.downward ? ts.offset() + ts.token().length() : ts.offset();
                continue;
            }
            TreePath destinationPath = tu.pathFor(destinationOffset);
            Tree leaf = destinationPath.getLeaf();
            if (insideBlock) {
                switch (leaf.getKind()) {
                    case COMPILATION_UNIT: {
                        return -1;
                    }
                    case BLOCK: {
                        return destinationOffset;
                    }
                    case IF: 
                    case FOR_LOOP: 
                    case ENHANCED_FOR_LOOP: 
                    case WHILE_LOOP: 
                    case DO_WHILE_LOOP: 
                    case SWITCH: 
                    case CASE: 
                    case SYNCHRONIZED: 
                    case TRY: 
                    case CATCH: {
                        offset = destinationOffset + (this.downward ? 1 : -1);
                        continue block9;
                    }
                }
                offset = this.downward ? (int)sp.getEndPosition(destinationPath.getCompilationUnit(), leaf) : (int)sp.getStartPosition(destinationPath.getCompilationUnit(), leaf);
                continue;
            }
            switch (leaf.getKind()) {
                case COMPILATION_UNIT: {
                    return -1;
                }
                case CLASS: 
                case INTERFACE: 
                case ANNOTATION_TYPE: 
                case ENUM: {
                    return destinationOffset;
                }
            }
            offset = this.downward ? (int)sp.getEndPosition(destinationPath.getCompilationUnit(), leaf) : (int)sp.getStartPosition(destinationPath.getCompilationUnit(), leaf);
        }
        return -1;
    }

    private int getLineStart(BaseDocument doc, int offset) {
        Element rootElement = doc.getDefaultRootElement();
        int lineNumber = rootElement.getElementIndex(offset);
        return lineNumber < 0 ? lineNumber : rootElement.getElement(lineNumber).getStartOffset();
    }

    private int getLineEnd(BaseDocument doc, int offset) {
        Element rootElement = doc.getDefaultRootElement();
        int lineNumber = rootElement.getElementIndex(offset);
        return lineNumber < 0 ? lineNumber : rootElement.getElement(lineNumber).getEndOffset();
    }

    private String getShortDescription() {
        String name = (String)this.getValue("Name");
        if (name != null) {
            try {
                return NbBundle.getMessage(JavaMoveCodeElementAction.class, (String)name);
            }
            catch (MissingResourceException mre) {
                // empty catch block
            }
        }
        return name;
    }

}

