/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.api.lexer.PartType
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.spi.editor.typinghooks.DeletedTextInterceptor
 *  org.netbeans.spi.editor.typinghooks.DeletedTextInterceptor$Context
 *  org.netbeans.spi.editor.typinghooks.TypedBreakInterceptor
 *  org.netbeans.spi.editor.typinghooks.TypedBreakInterceptor$Context
 *  org.netbeans.spi.editor.typinghooks.TypedTextInterceptor
 *  org.netbeans.spi.editor.typinghooks.TypedTextInterceptor$MutableContext
 */
package org.netbeans.modules.editor.java;

import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.prefs.Preferences;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.PartType;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.java.TokenBalance;
import org.netbeans.spi.editor.typinghooks.DeletedTextInterceptor;
import org.netbeans.spi.editor.typinghooks.TypedBreakInterceptor;
import org.netbeans.spi.editor.typinghooks.TypedTextInterceptor;

class TypingCompletion {
    private static Set<JavaTokenId> STOP_TOKENS_FOR_SKIP_CLOSING_BRACKET = EnumSet.of(JavaTokenId.LBRACE, JavaTokenId.RBRACE, JavaTokenId.SEMICOLON);
    private static Set<JavaTokenId> STRING_AND_COMMENT_TOKENS = EnumSet.of(JavaTokenId.STRING_LITERAL, JavaTokenId.LINE_COMMENT, JavaTokenId.JAVADOC_COMMENT, JavaTokenId.BLOCK_COMMENT);

    TypingCompletion() {
    }

    static boolean isCompletionSettingEnabled() {
        Preferences prefs = (Preferences)MimeLookup.getLookup((String)"text/x-java").lookup(Preferences.class);
        return prefs.getBoolean("pair-characters-completion", false);
    }

    static void removeCompletedQuote(DeletedTextInterceptor.Context context) throws BadLocationException {
        int caretOffset;
        TokenSequence<JavaTokenId> ts = TypingCompletion.javaTokenSequence(context, false);
        if (ts == null) {
            return;
        }
        char removedChar = context.getText().charAt(0);
        int n = caretOffset = context.isBackwardDelete() ? context.getOffset() - 1 : context.getOffset();
        if (removedChar == '\"') {
            if (ts.token().id() == JavaTokenId.STRING_LITERAL && ts.offset() == caretOffset) {
                context.getDocument().remove(caretOffset, 1);
            }
        } else if (removedChar == '\'' && ts.token().id() == JavaTokenId.CHAR_LITERAL && ts.offset() == caretOffset) {
            context.getDocument().remove(caretOffset, 1);
        }
    }

    static void removeBrackets(DeletedTextInterceptor.Context context) throws BadLocationException {
        int caretOffset = context.isBackwardDelete() ? context.getOffset() - 1 : context.getOffset();
        TokenSequence<JavaTokenId> ts = TypingCompletion.javaTokenSequence(context.getDocument(), caretOffset, false);
        if (ts == null) {
            return;
        }
        switch ((JavaTokenId)ts.token().id()) {
            case RPAREN: {
                if (TypingCompletion.tokenBalance(context.getDocument(), JavaTokenId.LPAREN) == 0) break;
                context.getDocument().remove(caretOffset, 1);
                break;
            }
            case RBRACKET: {
                if (TypingCompletion.tokenBalance(context.getDocument(), JavaTokenId.LBRACKET) == 0) break;
                context.getDocument().remove(caretOffset, 1);
            }
        }
    }

    static int skipClosingBracket(TypedTextInterceptor.MutableContext context) throws BadLocationException {
        TokenSequence<JavaTokenId> javaTS = TypingCompletion.javaTokenSequence(context, false);
        if (javaTS == null || javaTS.token().id() != JavaTokenId.RPAREN && javaTS.token().id() != JavaTokenId.RBRACKET || TypingCompletion.isStringOrComment((JavaTokenId)javaTS.token().id())) {
            return -1;
        }
        JavaTokenId bracketId = TypingCompletion.bracketCharToId(context.getText().charAt(0));
        if (TypingCompletion.isSkipClosingBracket(context, javaTS, bracketId)) {
            context.setText("", 0);
            return context.getOffset() + 1;
        }
        return -1;
    }

    static void completeOpeningBracket(TypedTextInterceptor.MutableContext context) throws BadLocationException {
        if (TypingCompletion.isStringOrComment((JavaTokenId)TypingCompletion.javaTokenSequence(context, false).token().id())) {
            return;
        }
        char chr = context.getDocument().getText(context.getOffset(), 1).charAt(0);
        if (chr == ')' || chr == ',' || chr == '\"' || chr == '\'' || chr == ' ' || chr == ']' || chr == '}' || chr == '\n' || chr == '\t' || chr == ';') {
            char insChr = context.getText().charAt(0);
            context.setText("" + insChr + TypingCompletion.matching(insChr), 1);
        }
    }

    static int moveOrSkipSemicolon(TypedTextInterceptor.MutableContext context) throws BadLocationException {
        TokenSequence<JavaTokenId> javaTS = TypingCompletion.javaTokenSequence(context, false);
        if (javaTS == null || TypingCompletion.isStringOrComment((JavaTokenId)javaTS.token().id())) {
            return -1;
        }
        if (javaTS.token().id() == JavaTokenId.SEMICOLON) {
            context.setText("", 0);
            return javaTS.offset() + 1;
        }
        int lastParenPos = context.getOffset();
        int index = javaTS.index();
        block4 : while (javaTS.moveNext() && (javaTS.token().id() != JavaTokenId.WHITESPACE || !javaTS.token().text().toString().contains("\n")) && javaTS.token().id() != JavaTokenId.RBRACE) {
            switch ((JavaTokenId)javaTS.token().id()) {
                case RPAREN: {
                    lastParenPos = javaTS.offset();
                    continue block4;
                }
                case WHITESPACE: {
                    continue block4;
                }
            }
            return -1;
        }
        javaTS.moveIndex(index);
        javaTS.moveNext();
        if (TypingCompletion.isForLoopTryWithResourcesOrLambdaSemicolon(javaTS) || TypingCompletion.posWithinAnyQuote(context, javaTS) || lastParenPos == context.getOffset() && !((JavaTokenId)javaTS.token().id()).equals((Object)JavaTokenId.RPAREN)) {
            return -1;
        }
        context.setText("", 0);
        context.getDocument().insertString(lastParenPos + 1, ";", null);
        return lastParenPos + 2;
    }

    static int completeQuote(TypedTextInterceptor.MutableContext context) throws BadLocationException {
        boolean caretInsideToken;
        boolean eol;
        if (TypingCompletion.isEscapeSequence(context)) {
            return -1;
        }
        TokenSequence<JavaTokenId> javaTS = TypingCompletion.javaTokenSequence(context, true);
        JavaTokenId id = javaTS != null ? (JavaTokenId)javaTS.token().id() : null;
        boolean bl = caretInsideToken = id != null && (javaTS.offset() + javaTS.token().length() > context.getOffset() || javaTS.token().partType() == PartType.START);
        if (caretInsideToken && (id == JavaTokenId.BLOCK_COMMENT || id == JavaTokenId.JAVADOC_COMMENT || id == JavaTokenId.LINE_COMMENT)) {
            return -1;
        }
        boolean completablePosition = TypingCompletion.isQuoteCompletablePosition(context);
        boolean insideString = caretInsideToken && (id == JavaTokenId.STRING_LITERAL || id == JavaTokenId.CHAR_LITERAL);
        int lastNonWhite = Utilities.getRowLastNonWhite((BaseDocument)((BaseDocument)context.getDocument()), (int)context.getOffset());
        boolean bl2 = eol = lastNonWhite < context.getOffset();
        if (insideString) {
            if (eol) {
                return -1;
            }
            char chr = context.getDocument().getText(context.getOffset(), 1).charAt(0);
            if (chr == context.getText().charAt(0) && context.getOffset() > 0) {
                javaTS.move(context.getOffset() - 1);
                if (javaTS.moveNext() && ((id = (JavaTokenId)javaTS.token().id()) == JavaTokenId.STRING_LITERAL || id == JavaTokenId.CHAR_LITERAL)) {
                    context.setText("", 0);
                    return context.getOffset() + 1;
                }
            }
        }
        if (completablePosition && !insideString || eol) {
            context.setText(context.getText() + context.getText(), 1);
        }
        return -1;
    }

    private static boolean isQuoteCompletablePosition(TypedTextInterceptor.MutableContext context) throws BadLocationException {
        char chr;
        if (context.getOffset() == context.getDocument().getLength()) {
            return true;
        }
        for (int i = context.getOffset(); i < context.getDocument().getLength() && (chr = context.getDocument().getText(i, 1).charAt(0)) != '\n'; ++i) {
            if (Character.isWhitespace(chr)) continue;
            return chr == ')' || chr == ',' || chr == '+' || chr == '}' || chr == ';';
        }
        return false;
    }

    private static boolean isEscapeSequence(TypedTextInterceptor.MutableContext context) throws BadLocationException {
        if (context.getOffset() <= 0) {
            return false;
        }
        int i = 2;
        while (context.getOffset() - i >= 0) {
            char[] previousChars = context.getDocument().getText(context.getOffset() - i, 2).toCharArray();
            if (previousChars[1] != '\\') {
                return false;
            }
            if (previousChars[0] != '\\') {
                return true;
            }
            i += 2;
        }
        return context.getDocument().getText(context.getOffset() - 1, 1).charAt(0) == '\\';
    }

    static boolean isAddRightBrace(BaseDocument doc, int caretOffset) throws BadLocationException {
        if (TypingCompletion.tokenBalance((Document)doc, JavaTokenId.LBRACE) <= 0) {
            return false;
        }
        int caretRowStartOffset = Utilities.getRowStart((BaseDocument)doc, (int)caretOffset);
        TokenSequence<JavaTokenId> ts = TypingCompletion.javaTokenSequence((Document)doc, caretOffset, true);
        if (ts == null) {
            return false;
        }
        boolean first = true;
        do {
            if (ts.offset() < caretRowStartOffset) {
                return false;
            }
            switch ((JavaTokenId)ts.token().id()) {
                case WHITESPACE: 
                case LINE_COMMENT: {
                    break;
                }
                case BLOCK_COMMENT: 
                case JAVADOC_COMMENT: {
                    if (!first || caretOffset <= ts.offset() || caretOffset >= ts.offset() + ts.token().length()) break;
                    return false;
                }
                case LBRACE: {
                    return true;
                }
            }
            first = false;
        } while (ts.movePrevious());
        return false;
    }

    static int getRowOrBlockEnd(BaseDocument doc, int caretOffset, boolean[] insert) throws BadLocationException {
        int rowEnd = Utilities.getRowLastNonWhite((BaseDocument)doc, (int)caretOffset);
        if (rowEnd == -1 || caretOffset >= rowEnd) {
            return caretOffset;
        }
        ++rowEnd;
        int parenBalance = 0;
        int braceBalance = 0;
        int bracketBalance = 0;
        TokenSequence<JavaTokenId> ts = TypingCompletion.javaTokenSequence((Document)doc, caretOffset, false);
        if (ts == null) {
            return caretOffset;
        }
        while (ts.offset() < rowEnd) {
            switch ((JavaTokenId)ts.token().id()) {
                case SEMICOLON: {
                    if (!TypingCompletion.isForLoopTryWithResourcesOrLambdaSemicolon(ts)) {
                        return ts.offset() + 1;
                    }
                }
                case LPAREN: {
                    ++parenBalance;
                    break;
                }
                case RPAREN: {
                    if (parenBalance-- != 0) break;
                    return ts.offset();
                }
                case LBRACE: {
                    ++braceBalance;
                    break;
                }
                case RBRACE: {
                    if (braceBalance-- != 0) break;
                    return ts.offset();
                }
                case LBRACKET: {
                    ++bracketBalance;
                    break;
                }
                case RBRACKET: {
                    if (bracketBalance-- != 0) break;
                    return ts.offset();
                }
            }
            if (ts.moveNext()) continue;
        }
        insert[0] = false;
        return rowEnd;
    }

    static boolean blockCommentCompletion(TypedBreakInterceptor.Context context) {
        return TypingCompletion.blockCommentCompletionImpl(context, false);
    }

    static boolean javadocBlockCompletion(TypedBreakInterceptor.Context context) {
        return TypingCompletion.blockCommentCompletionImpl(context, true);
    }

    private static boolean blockCommentCompletionImpl(TypedBreakInterceptor.Context context, boolean javadoc) {
        CharSequence content;
        TokenSequence<JavaTokenId> ts = TypingCompletion.javaTokenSequence(context, false);
        if (ts == null) {
            return false;
        }
        int dotPosition = context.getCaretOffset();
        ts.move(dotPosition);
        if (!ts.moveNext() && !ts.movePrevious() || ts.token().id() != (javadoc ? JavaTokenId.JAVADOC_COMMENT : JavaTokenId.BLOCK_COMMENT)) {
            return false;
        }
        int jdoffset = dotPosition - (javadoc ? 3 : 2);
        if (jdoffset >= 0 && TypingCompletion.isOpenBlockComment(content = DocumentUtilities.getText((Document)context.getDocument()), dotPosition - 1, javadoc) && !TypingCompletion.isClosedBlockComment(content, dotPosition) && TypingCompletion.isAtRowEnd(content, dotPosition)) {
            return true;
        }
        return false;
    }

    private static boolean isOpenBlockComment(CharSequence content, int pos, boolean javadoc) {
        for (int i = pos; i >= 0; --i) {
            char c = content.charAt(i);
            if (c == '*' && (javadoc ? i - 2 >= 0 && content.charAt(i - 1) == '*' && content.charAt(i - 2) == '/' : i - 1 >= 0 && content.charAt(i - 1) == '/')) {
                return true;
            }
            if (c == '\n') {
                return false;
            }
            if (c != '/' || i - 1 < 0 || content.charAt(i - 1) != '*') continue;
            return false;
        }
        return false;
    }

    private static boolean isClosedBlockComment(CharSequence txt, int pos) {
        int length = txt.length();
        int quotation = 0;
        for (int i = pos; i < length; ++i) {
            char c = txt.charAt(i);
            if (c == '*' && i < length - 1 && txt.charAt(i + 1) == '/') {
                char cc;
                if (quotation == 0 || i < length - 2) {
                    return true;
                }
                boolean isClosed = true;
                for (int j = i + 2; j < length && (cc = txt.charAt(j)) != '\n'; ++j) {
                    if (cc != '\"' || j >= length - 1 || txt.charAt(j + 1) == '\'') continue;
                    isClosed = false;
                    break;
                }
                if (!isClosed) continue;
                return true;
            }
            if (c == '/' && i < length - 1 && txt.charAt(i + 1) == '*') {
                return false;
            }
            if (c == '\n') {
                quotation = 0;
                continue;
            }
            if (c != '\"' || i >= length - 1 || txt.charAt(i + 1) == '\'') continue;
            ++quotation;
            quotation %= 2;
        }
        return false;
    }

    private static boolean isAtRowEnd(CharSequence txt, int pos) {
        int length = txt.length();
        for (int i = pos; i < length; ++i) {
            char c = txt.charAt(i);
            if (c == '\n') {
                return true;
            }
            if (Character.isWhitespace(c)) continue;
            return false;
        }
        return true;
    }

    static boolean posWithinString(Document doc, int caretOffset) {
        return TypingCompletion.posWithinQuotes(doc, caretOffset, JavaTokenId.STRING_LITERAL);
    }

    private static boolean posWithinQuotes(Document doc, int caretOffset, JavaTokenId tokenId) {
        TokenSequence<JavaTokenId> javaTS = TypingCompletion.javaTokenSequence(doc, caretOffset, false);
        if (javaTS != null) {
            if (javaTS.token().id() != tokenId) {
                return false;
            }
            if (caretOffset > javaTS.offset() && caretOffset < javaTS.offset() + javaTS.token().length()) {
                return true;
            }
            return false;
        }
        return false;
    }

    private static boolean posWithinAnyQuote(TypedTextInterceptor.MutableContext context, TokenSequence<JavaTokenId> javaTS) throws BadLocationException {
        if (javaTS.token().id() == JavaTokenId.STRING_LITERAL || javaTS.token().id() == JavaTokenId.CHAR_LITERAL) {
            char chr = context.getDocument().getText(context.getOffset(), 1).charAt(0);
            return context.getOffset() - javaTS.offset() == 1 || chr != '\"' && chr != '\'';
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static boolean isForLoopTryWithResourcesOrLambdaSemicolon(TokenSequence<JavaTokenId> ts) {
        block33 : {
            int parenDepth = 0;
            int braceDepth = 0;
            boolean semicolonFound = false;
            int tsOrigIndex = ts.index();
            do {
                if (ts.movePrevious()) {
                    switch ((JavaTokenId)ts.token().id()) {
                        case LPAREN: {
                            if (parenDepth == 0) {
                                block26 : while (ts.movePrevious()) {
                                    switch ((JavaTokenId)ts.token().id()) {
                                        case WHITESPACE: 
                                        case LINE_COMMENT: 
                                        case BLOCK_COMMENT: 
                                        case JAVADOC_COMMENT: {
                                            continue block26;
                                        }
                                        case FOR: 
                                        case TRY: {
                                            boolean bl = true;
                                            return bl;
                                        }
                                    }
                                    boolean bl = false;
                                    return bl;
                                }
                                boolean bl = false;
                                return bl;
                            }
                            --parenDepth;
                            break;
                        }
                        case RPAREN: {
                            ++parenDepth;
                            break;
                        }
                        case LBRACE: {
                            if (braceDepth == 0) {
                                if (!semicolonFound) {
                                    block27 : while (ts.movePrevious()) {
                                        switch ((JavaTokenId)ts.token().id()) {
                                            case WHITESPACE: 
                                            case LINE_COMMENT: 
                                            case BLOCK_COMMENT: 
                                            case JAVADOC_COMMENT: {
                                                continue block27;
                                            }
                                            case ARROW: {
                                                boolean bl = true;
                                                return bl;
                                            }
                                        }
                                        boolean bl = false;
                                        return bl;
                                    }
                                }
                                boolean bl = false;
                                return bl;
                            }
                            --braceDepth;
                            break;
                        }
                        case RBRACE: {
                            ++braceDepth;
                            break;
                        }
                        case SEMICOLON: {
                            if (semicolonFound) {
                                boolean bl = false;
                                return bl;
                            }
                            semicolonFound = true;
                        }
                    }
                    continue;
                }
                break block33;
                break;
            } while (true);
            finally {
                ts.moveIndex(tsOrigIndex);
                ts.moveNext();
            }
        }
        return false;
    }

    private static boolean isSkipClosingBracket(TypedTextInterceptor.MutableContext context, TokenSequence<JavaTokenId> javaTS, JavaTokenId rightBracketId) {
        if (context.getOffset() == context.getDocument().getLength()) {
            return false;
        }
        boolean skipClosingBracket = false;
        if (javaTS != null && javaTS.token().id() == rightBracketId) {
            JavaTokenId leftBracketId = TypingCompletion.matching(rightBracketId);
            do {
                boolean isPrevious;
                if (!STOP_TOKENS_FOR_SKIP_CLOSING_BRACKET.contains((Object)javaTS.token().id()) && (javaTS.token().id() != JavaTokenId.WHITESPACE || !javaTS.token().text().toString().contains("\n"))) continue;
                while (javaTS.token().id() != rightBracketId && (isPrevious = javaTS.movePrevious())) {
                }
                break;
            } while (javaTS.moveNext());
            int braceBalance = 0;
            int bracketBalance = -1;
            int numOfSemi = 0;
            boolean finished = false;
            while (!finished && javaTS.movePrevious()) {
                JavaTokenId id = (JavaTokenId)javaTS.token().id();
                switch (id) {
                    case LPAREN: 
                    case LBRACKET: {
                        if (id != leftBracketId || ++bracketBalance != 1) break;
                        if (braceBalance != 0) {
                            bracketBalance = 2;
                        }
                        finished = javaTS.offset() < context.getOffset();
                        break;
                    }
                    case RPAREN: 
                    case RBRACKET: {
                        if (id != rightBracketId) break;
                        --bracketBalance;
                        break;
                    }
                    case LBRACE: {
                        if (++braceBalance <= 0) break;
                        finished = true;
                        break;
                    }
                    case RBRACE: {
                        --braceBalance;
                        break;
                    }
                    case SEMICOLON: {
                        ++numOfSemi;
                    }
                }
            }
            if (bracketBalance == 1 && numOfSemi < 2) {
                finished = false;
                block14 : while (!finished && javaTS.movePrevious()) {
                    switch ((JavaTokenId)javaTS.token().id()) {
                        case WHITESPACE: 
                        case LINE_COMMENT: 
                        case BLOCK_COMMENT: 
                        case JAVADOC_COMMENT: {
                            continue block14;
                        }
                        case FOR: {
                            --bracketBalance;
                        }
                    }
                    finished = true;
                }
            }
            skipClosingBracket = bracketBalance != 1;
        }
        return skipClosingBracket;
    }

    private static char matching(char bracket) {
        switch (bracket) {
            case '(': {
                return ')';
            }
            case '[': {
                return ']';
            }
            case '\"': {
                return '\"';
            }
            case '\'': {
                return '\'';
            }
        }
        return ' ';
    }

    private static JavaTokenId matching(JavaTokenId id) {
        switch (id) {
            case LPAREN: {
                return JavaTokenId.RPAREN;
            }
            case LBRACKET: {
                return JavaTokenId.RBRACKET;
            }
            case RPAREN: {
                return JavaTokenId.LPAREN;
            }
            case RBRACKET: {
                return JavaTokenId.LBRACKET;
            }
        }
        return null;
    }

    private static JavaTokenId bracketCharToId(char bracket) {
        switch (bracket) {
            case '(': {
                return JavaTokenId.LPAREN;
            }
            case ')': {
                return JavaTokenId.RPAREN;
            }
            case '[': {
                return JavaTokenId.LBRACKET;
            }
            case ']': {
                return JavaTokenId.RBRACKET;
            }
            case '{': {
                return JavaTokenId.LBRACE;
            }
            case '}': {
                return JavaTokenId.RBRACE;
            }
        }
        throw new IllegalArgumentException("Not a bracket char '" + bracket + '\'');
    }

    private static int tokenBalance(Document doc, JavaTokenId leftTokenId) {
        TokenBalance tb = TokenBalance.get(doc);
        if (!tb.isTracked(JavaTokenId.language())) {
            tb.addTokenPair(JavaTokenId.language(), JavaTokenId.LPAREN, JavaTokenId.RPAREN);
            tb.addTokenPair(JavaTokenId.language(), JavaTokenId.LBRACKET, JavaTokenId.RBRACKET);
            tb.addTokenPair(JavaTokenId.language(), JavaTokenId.LBRACE, JavaTokenId.RBRACE);
        }
        int balance = tb.balance(JavaTokenId.language(), leftTokenId);
        assert (balance != Integer.MAX_VALUE);
        return balance;
    }

    private static TokenSequence<JavaTokenId> javaTokenSequence(TypedTextInterceptor.MutableContext context, boolean backwardBias) {
        return TypingCompletion.javaTokenSequence(context.getDocument(), context.getOffset(), backwardBias);
    }

    private static TokenSequence<JavaTokenId> javaTokenSequence(DeletedTextInterceptor.Context context, boolean backwardBias) {
        return TypingCompletion.javaTokenSequence(context.getDocument(), context.getOffset(), backwardBias);
    }

    private static TokenSequence<JavaTokenId> javaTokenSequence(TypedBreakInterceptor.Context context, boolean backwardBias) {
        return TypingCompletion.javaTokenSequence(context.getDocument(), context.getCaretOffset(), backwardBias);
    }

    private static TokenSequence<JavaTokenId> javaTokenSequence(Document doc, int caretOffset, boolean backwardBias) {
        TokenHierarchy hi = TokenHierarchy.get((Document)doc);
        List tsList = hi.embeddedTokenSequences(caretOffset, backwardBias);
        for (int i = tsList.size() - 1; i >= 0; --i) {
            TokenSequence ts = (TokenSequence)tsList.get(i);
            if (ts.languagePath().innerLanguage() != JavaTokenId.language()) continue;
            TokenSequence javaInnerTS = ts;
            return javaInnerTS;
        }
        return null;
    }

    private static boolean isStringOrComment(JavaTokenId javaTokenId) {
        return STRING_AND_COMMENT_TOKENS.contains((Object)javaTokenId);
    }

}

