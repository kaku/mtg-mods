/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.support.ReferencesCount
 */
package org.netbeans.modules.editor.java;

import javax.lang.model.element.TypeElement;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.support.ReferencesCount;
import org.netbeans.modules.editor.java.Utilities;

class LazySortText
implements CharSequence {
    private String simpleName;
    private String enclName;
    private ElementHandle<TypeElement> handle;
    private ReferencesCount referencesCount;
    private String importanceLevel = null;

    LazySortText(String simpleName, String enclName, ElementHandle<TypeElement> handle, ReferencesCount referencesCount) {
        this.simpleName = simpleName;
        this.enclName = enclName != null ? "" + Utilities.getImportanceLevel(enclName) + "#" + enclName : "";
        this.handle = handle;
        this.referencesCount = referencesCount;
    }

    @Override
    public int length() {
        return this.simpleName.length() + this.enclName.length() + 10;
    }

    @Override
    public char charAt(int index) {
        if (index < 0 || index >= this.length()) {
            throw new StringIndexOutOfBoundsException(index);
        }
        if (index < this.simpleName.length()) {
            return this.simpleName.charAt(index);
        }
        index -= this.simpleName.length();
        if (index-- == 0) {
            return '#';
        }
        if (index < 8) {
            return this.getImportanceLevel().charAt(index);
        }
        index -= 8;
        if (index-- == 0) {
            return '#';
        }
        return this.enclName.charAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private String getImportanceLevel() {
        if (this.importanceLevel == null) {
            this.importanceLevel = String.format("%8d", Utilities.getImportanceLevel(this.referencesCount, this.handle));
        }
        return this.importanceLevel;
    }
}

