/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IfTree
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.spi.editor.bracesmatching.BraceContext
 *  org.netbeans.spi.editor.bracesmatching.BracesMatcher
 *  org.netbeans.spi.editor.bracesmatching.BracesMatcher$ContextLocator
 *  org.netbeans.spi.editor.bracesmatching.BracesMatcherFactory
 *  org.netbeans.spi.editor.bracesmatching.MatcherContext
 *  org.netbeans.spi.editor.bracesmatching.support.BracesMatcherSupport
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.editor.java;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.io.IOException;
import java.util.List;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Position;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.spi.editor.bracesmatching.BraceContext;
import org.netbeans.spi.editor.bracesmatching.BracesMatcher;
import org.netbeans.spi.editor.bracesmatching.BracesMatcherFactory;
import org.netbeans.spi.editor.bracesmatching.MatcherContext;
import org.netbeans.spi.editor.bracesmatching.support.BracesMatcherSupport;
import org.openide.util.Exceptions;

public final class JavaBracesMatcher
implements BracesMatcher,
BracesMatcherFactory,
BracesMatcher.ContextLocator {
    private static final char[] PAIRS = new char[]{'(', ')', '[', ']', '{', '}'};
    private static final JavaTokenId[] PAIR_TOKEN_IDS = new JavaTokenId[]{JavaTokenId.LPAREN, JavaTokenId.RPAREN, JavaTokenId.LBRACKET, JavaTokenId.RBRACKET, JavaTokenId.LBRACE, JavaTokenId.RBRACE};
    private final MatcherContext context;
    private int originOffset;
    private char originChar;
    private char matchingChar;
    private boolean backward;
    private List<TokenSequence<?>> sequences;
    private int searchOffset = -1;
    private int matchStart;

    public JavaBracesMatcher() {
        this(null);
    }

    private JavaBracesMatcher(MatcherContext context) {
        this.context = context;
    }

    private JavaBracesMatcher(MatcherContext context, int searchOffset) {
        this(context);
        this.searchOffset = searchOffset;
    }

    private int getSearchOffset() {
        return this.searchOffset >= 0 ? this.searchOffset : this.context.getSearchOffset();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public int[] findOrigin() throws BadLocationException, InterruptedException {
        ((AbstractDocument)this.context.getDocument()).readLock();
        try {
            int[] origin = BracesMatcherSupport.findChar((Document)this.context.getDocument(), (int)this.getSearchOffset(), (int)this.context.getLimitOffset(), (char[])PAIRS);
            if (origin != null) {
                int[] seq;
                this.originOffset = origin[0];
                this.originChar = PAIRS[origin[1]];
                this.matchingChar = PAIRS[origin[1] + origin[2]];
                this.backward = origin[2] < 0;
                TokenHierarchy th = TokenHierarchy.get((Document)this.context.getDocument());
                this.sequences = JavaBracesMatcher.getEmbeddedTokenSequences(th, this.originOffset, this.backward, JavaTokenId.language());
                if (!this.sequences.isEmpty()) {
                    seq = this.sequences.get(this.sequences.size() - 1);
                    seq.move(this.originOffset);
                    if (seq.moveNext() && (seq.token().id() == JavaTokenId.BLOCK_COMMENT || seq.token().id() == JavaTokenId.LINE_COMMENT)) {
                        int[] arrn = null;
                        return arrn;
                    }
                }
                seq = new int[]{this.originOffset, this.originOffset + 1};
                return seq;
            }
            int[] th = null;
            return th;
        }
        finally {
            ((AbstractDocument)this.context.getDocument()).readUnlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public int[] findMatches() throws InterruptedException, BadLocationException {
        ((AbstractDocument)this.context.getDocument()).readLock();
        try {
            TokenSequence seq;
            if (!this.sequences.isEmpty()) {
                seq = this.sequences.get(this.sequences.size() - 1);
                TokenHierarchy th = TokenHierarchy.get((Document)this.context.getDocument());
                List list = this.backward ? th.tokenSequenceList(seq.languagePath(), 0, this.originOffset) : th.tokenSequenceList(seq.languagePath(), this.originOffset + 1, this.context.getDocument().getLength());
                int counter = 0;
                seq.move(this.originOffset);
                if (seq.moveNext() && seq.token().id() == JavaTokenId.STRING_LITERAL) {
                    TokenSequenceIterator tsi = new TokenSequenceIterator(list, this.backward);
                    while (tsi.hasMore()) {
                        int i;
                        int bound;
                        TokenSequence sq = tsi.getSequence();
                        if (sq.token().id() != JavaTokenId.STRING_LITERAL) continue;
                        CharSequence text = sq.token().text();
                        if (this.backward) {
                            bound = this.originOffset - sq.offset();
                            if (bound >= 0) {
                                bound = Math.min(text.length() - 1, bound);
                            }
                            for (i = bound - 1; i > 0; --i) {
                                if (this.originChar == text.charAt(i)) {
                                    ++counter;
                                    continue;
                                }
                                if (this.matchingChar != text.charAt(i)) continue;
                                if (counter == 0) {
                                    int[] arrn = new int[]{sq.offset() + i, sq.offset() + i + 1};
                                    return arrn;
                                }
                                --counter;
                            }
                            continue;
                        }
                        bound = this.originOffset - sq.offset() + 1;
                        if (bound < 0 || bound > text.length()) {
                            bound = 1;
                        }
                        for (i = bound; i < text.length() - 1; ++i) {
                            if (this.originChar == text.charAt(i)) {
                                ++counter;
                                continue;
                            }
                            if (this.matchingChar != text.charAt(i)) continue;
                            if (counter == 0) {
                                int[] arrn = new int[]{sq.offset() + i, sq.offset() + i + 1};
                                return arrn;
                            }
                            --counter;
                        }
                    }
                    tsi = null;
                    return tsi;
                }
                JavaTokenId originId = this.getTokenId(this.originChar);
                JavaTokenId lookingForId = this.getTokenId(this.matchingChar);
                TokenSequenceIterator tsi = new TokenSequenceIterator(list, this.backward);
                while (tsi.hasMore()) {
                    TokenSequence sq = tsi.getSequence();
                    if (originId == sq.token().id()) {
                        ++counter;
                        continue;
                    }
                    if (lookingForId != sq.token().id()) continue;
                    if (counter == 0) {
                        this.matchStart = sq.offset();
                        int[] i = new int[]{sq.offset(), sq.offset() + sq.token().length()};
                        return i;
                    }
                    --counter;
                }
            }
            seq = null;
            return seq;
        }
        finally {
            ((AbstractDocument)this.context.getDocument()).readUnlock();
        }
    }

    public BraceContext findContext(int originOrMatchPosition) {
        if (this.backward && this.matchingChar == '{') {
            try {
                return this.findContextBackwards(originOrMatchPosition);
            }
            catch (IOException | BadLocationException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return null;
    }

    public BraceContext findContextBackwards(int p2) throws BadLocationException, IOException {
        if (p2 != this.originOffset) {
            return null;
        }
        final int position = this.matchStart;
        JavaSource src = JavaSource.forDocument((Document)this.context.getDocument());
        if (src == null) {
            return null;
        }
        final BraceContext[] ret = new BraceContext[1];
        src.runUserActionTask((Task)new Task<CompilationController>(){

            public void run(CompilationController ctrl) throws Exception {
                StatementTree block;
                ctrl.toPhase(JavaSource.Phase.PARSED);
                TreePath path = ctrl.getTreeUtilities().pathFor(position + 1);
                if (path == null) {
                    return;
                }
                if (path.getLeaf().getKind() == Tree.Kind.BLOCK) {
                    block = (StatementTree)path.getLeaf();
                    path = path.getParentPath();
                } else {
                    block = null;
                }
                switch (path.getLeaf().getKind()) {
                    case IF: {
                        IfTree ifTree = (IfTree)path.getLeaf();
                        if (block == ifTree.getElseStatement()) {
                            final int[] elseStart = new int[]{(int)ctrl.getTrees().getSourcePositions().getStartPosition(ctrl.getCompilationUnit(), (Tree)ifTree.getElseStatement())};
                            JavaBracesMatcher.this.context.getDocument().render(new Runnable(){

                                @Override
                                public void run() {
                                    TokenHierarchy h = TokenHierarchy.get((Document)JavaBracesMatcher.this.context.getDocument());
                                    TokenSequence seq = h.tokenSequence();
                                    int off = seq.move(elseStart[0]);
                                    if (off == 0 && seq.moveNext() && seq.token().id() == JavaTokenId.LBRACE) {
                                        TokenId id;
                                        while (seq.movePrevious() && ((id = seq.token().id()) == JavaTokenId.WHITESPACE || id == JavaTokenId.BLOCK_COMMENT || id == JavaTokenId.LINE_COMMENT)) {
                                        }
                                        if (seq.token().id() == JavaTokenId.ELSE) {
                                            elseStart[0] = seq.offset();
                                        }
                                    }
                                }
                            });
                            int ifStart = (int)ctrl.getTrees().getSourcePositions().getStartPosition(ctrl.getCompilationUnit(), (Tree)ifTree);
                            int ifEnd = ifTree.getThenStatement().getKind() == Tree.Kind.BLOCK ? (int)ctrl.getTrees().getSourcePositions().getStartPosition(ctrl.getCompilationUnit(), (Tree)ifTree.getThenStatement()) : (int)ctrl.getTrees().getSourcePositions().getEndPosition(ctrl.getCompilationUnit(), (Tree)ifTree.getCondition());
                            BraceContext rel = BraceContext.create((Position)JavaBracesMatcher.this.context.getDocument().createPosition(ifStart), (Position)JavaBracesMatcher.this.context.getDocument().createPosition(ifEnd + 1));
                            ret[0] = rel.createRelated(JavaBracesMatcher.this.context.getDocument().createPosition(elseStart[0]), JavaBracesMatcher.this.context.getDocument().createPosition(position + 1));
                            return;
                        }
                    }
                    case SWITCH: 
                    case WHILE_LOOP: 
                    case METHOD: 
                    case NEW_CLASS: 
                    case CASE: {
                        long start = ctrl.getTrees().getSourcePositions().getStartPosition(ctrl.getCompilationUnit(), path.getLeaf());
                        ret[0] = BraceContext.create((Position)JavaBracesMatcher.this.context.getDocument().createPosition((int)start), (Position)JavaBracesMatcher.this.context.getDocument().createPosition(position));
                        return;
                    }
                    case CLASS: {
                        long start = ctrl.getTrees().getSourcePositions().getStartPosition(ctrl.getCompilationUnit(), (Tree)(block != null ? block : path.getLeaf()));
                        ret[0] = BraceContext.create((Position)JavaBracesMatcher.this.context.getDocument().createPosition((int)start), (Position)JavaBracesMatcher.this.context.getDocument().createPosition(position));
                        return;
                    }
                }
            }

        }, true);
        return ret[0];
    }

    private JavaTokenId getTokenId(char ch) {
        for (int i = 0; i < PAIRS.length; ++i) {
            if (PAIRS[i] != ch) continue;
            return PAIR_TOKEN_IDS[i];
        }
        return null;
    }

    public static List<TokenSequence<?>> getEmbeddedTokenSequences(TokenHierarchy<?> th, int offset, boolean backwardBias, Language<?> language) {
        TokenSequence seq;
        List sequences = th.embeddedTokenSequences(offset, backwardBias);
        for (int i = sequences.size() - 1; i >= 0 && (seq = (TokenSequence)sequences.get(i)).language() != language; --i) {
            sequences.remove(i);
        }
        return sequences;
    }

    public BracesMatcher createMatcher(MatcherContext context) {
        return new JavaBracesMatcher(context);
    }

    private static final class TokenSequenceIterator {
        private final List<TokenSequence<?>> list;
        private final boolean backward;
        private int index;

        public TokenSequenceIterator(List<TokenSequence<?>> list, boolean backward) {
            this.list = list;
            this.backward = backward;
            this.index = -1;
        }

        public boolean hasMore() {
            return this.backward ? this.hasPrevious() : this.hasNext();
        }

        public TokenSequence<?> getSequence() {
            assert (this.index >= 0 && this.index < this.list.size());
            return this.list.get(this.index);
        }

        private boolean hasPrevious() {
            boolean anotherSeq = false;
            if (this.index == -1) {
                this.index = this.list.size() - 1;
                anotherSeq = true;
            }
            while (this.index >= 0) {
                TokenSequence seq = this.list.get(this.index);
                if (anotherSeq) {
                    seq.moveEnd();
                }
                if (seq.movePrevious()) {
                    return true;
                }
                anotherSeq = true;
                --this.index;
            }
            return false;
        }

        private boolean hasNext() {
            boolean anotherSeq = false;
            if (this.index == -1) {
                this.index = 0;
                anotherSeq = true;
            }
            while (this.index < this.list.size()) {
                TokenSequence seq = this.list.get(this.index);
                if (anotherSeq) {
                    seq.moveStart();
                }
                if (seq.moveNext()) {
                    return true;
                }
                anotherSeq = true;
                ++this.index;
            }
            return false;
        }
    }

}

