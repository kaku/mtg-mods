/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.AnnotatedTypeTree
 *  com.sun.source.tree.AnnotationTree
 *  com.sun.source.tree.ArrayAccessTree
 *  com.sun.source.tree.AssignmentTree
 *  com.sun.source.tree.BinaryTree
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.CaseTree
 *  com.sun.source.tree.CatchTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.CompoundAssignmentTree
 *  com.sun.source.tree.ConditionalExpressionTree
 *  com.sun.source.tree.DoWhileLoopTree
 *  com.sun.source.tree.EnhancedForLoopTree
 *  com.sun.source.tree.ErroneousTree
 *  com.sun.source.tree.ExpressionStatementTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.ForLoopTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.IfTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.InstanceOfTree
 *  com.sun.source.tree.LabeledStatementTree
 *  com.sun.source.tree.LambdaExpressionTree
 *  com.sun.source.tree.MemberReferenceTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodInvocationTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.NewArrayTree
 *  com.sun.source.tree.NewClassTree
 *  com.sun.source.tree.ParameterizedTypeTree
 *  com.sun.source.tree.ParenthesizedTree
 *  com.sun.source.tree.PrimitiveTypeTree
 *  com.sun.source.tree.Scope
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.SwitchTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.TreeVisitor
 *  com.sun.source.tree.TryTree
 *  com.sun.source.tree.TypeCastTree
 *  com.sun.source.tree.TypeParameterTree
 *  com.sun.source.tree.UnionTypeTree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.tree.WhileLoopTree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreeScanner
 *  com.sun.source.util.Trees
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.api.editor.completion.Completion
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.source.ClassIndex
 *  org.netbeans.api.java.source.ClassIndex$NameKind
 *  org.netbeans.api.java.source.ClassIndex$SearchKind
 *  org.netbeans.api.java.source.ClassIndex$SearchScope
 *  org.netbeans.api.java.source.ClassIndex$Symbols
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.ClasspathInfo$PathKind
 *  org.netbeans.api.java.source.CodeStyle
 *  org.netbeans.api.java.source.CodeStyleUtils
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.ElementUtilities
 *  org.netbeans.api.java.source.ElementUtilities$ElementAcceptor
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.TypeUtilities
 *  org.netbeans.api.java.source.support.ReferencesCount
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.api.whitelist.WhiteListQuery
 *  org.netbeans.api.whitelist.WhiteListQuery$WhiteList
 *  org.netbeans.editor.Utilities
 *  org.netbeans.editor.ext.ToolTipSupport
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.spi.editor.completion.CompletionDocumentation
 *  org.netbeans.spi.editor.completion.CompletionItem
 *  org.netbeans.spi.editor.completion.CompletionProvider
 *  org.netbeans.spi.editor.completion.CompletionResultSet
 *  org.netbeans.spi.editor.completion.CompletionTask
 *  org.netbeans.spi.editor.completion.support.AsyncCompletionQuery
 *  org.netbeans.spi.editor.completion.support.AsyncCompletionTask
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.editor.java;

import com.sun.source.tree.AnnotatedTypeTree;
import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.ArrayAccessTree;
import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BinaryTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.CaseTree;
import com.sun.source.tree.CatchTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.CompoundAssignmentTree;
import com.sun.source.tree.ConditionalExpressionTree;
import com.sun.source.tree.DoWhileLoopTree;
import com.sun.source.tree.EnhancedForLoopTree;
import com.sun.source.tree.ErroneousTree;
import com.sun.source.tree.ExpressionStatementTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.ForLoopTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.InstanceOfTree;
import com.sun.source.tree.LabeledStatementTree;
import com.sun.source.tree.LambdaExpressionTree;
import com.sun.source.tree.MemberReferenceTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.NewArrayTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.ParameterizedTypeTree;
import com.sun.source.tree.ParenthesizedTree;
import com.sun.source.tree.PrimitiveTypeTree;
import com.sun.source.tree.Scope;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.SwitchTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TreeVisitor;
import com.sun.source.tree.TryTree;
import com.sun.source.tree.TypeCastTree;
import com.sun.source.tree.TypeParameterTree;
import com.sun.source.tree.UnionTypeTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.tree.WhileLoopTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreeScanner;
import com.sun.source.util.Trees;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ExecutableType;
import javax.lang.model.type.IntersectionType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.type.WildcardType;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.swing.JToolTip;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.editor.completion.Completion;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.source.ClassIndex;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CodeStyle;
import org.netbeans.api.java.source.CodeStyleUtils;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.TypeUtilities;
import org.netbeans.api.java.source.support.ReferencesCount;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.api.whitelist.WhiteListQuery;
import org.netbeans.editor.ext.ToolTipSupport;
import org.netbeans.modules.editor.java.JavaCompletionDoc;
import org.netbeans.modules.editor.java.JavaCompletionItem;
import org.netbeans.modules.editor.java.JavaCompletionItemFactory;
import org.netbeans.modules.editor.java.MethodParamsTipPaintComponent;
import org.netbeans.modules.editor.java.Utilities;
import org.netbeans.modules.java.editor.codegen.GeneratorUtils;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.spi.editor.completion.CompletionDocumentation;
import org.netbeans.spi.editor.completion.CompletionItem;
import org.netbeans.spi.editor.completion.CompletionProvider;
import org.netbeans.spi.editor.completion.CompletionResultSet;
import org.netbeans.spi.editor.completion.CompletionTask;
import org.netbeans.spi.editor.completion.support.AsyncCompletionQuery;
import org.netbeans.spi.editor.completion.support.AsyncCompletionTask;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class JavaCompletionProvider
implements CompletionProvider {
    public int getAutoQueryTypes(JTextComponent component, String typedText) {
        if (typedText != null && typedText.length() == 1 && (Utilities.getJavaCompletionAutoPopupTriggers().indexOf(typedText.charAt(0)) >= 0 || Utilities.autoPopupOnJavaIdentifierPart() && JavaCompletionQuery.isJavaIdentifierPart(typedText)) && Utilities.isJavaContext(component, component.getSelectionStart() - 1, true)) {
            return 1;
        }
        return 0;
    }

    public CompletionTask createTask(int type, JTextComponent component) {
        if ((type & 1) != 0 || type == 4 || type == 2) {
            return new AsyncCompletionTask((AsyncCompletionQuery)new JavaCompletionQuery(type, component.getSelectionStart(), true), component);
        }
        return null;
    }

    static CompletionTask createDocTask(ElementHandle element) {
        JavaCompletionQuery query = new JavaCompletionQuery(2, -1, true);
        query.element = element;
        return new AsyncCompletionTask((AsyncCompletionQuery)query, EditorRegistry.lastFocusedComponent());
    }

    public static List<? extends CompletionItem> query(Source source, int queryType, int offset, int substitutionOffset, JavaCompletionItemFactory factory) throws Exception {
        assert (source != null);
        assert ((queryType & 1) != 0);
        JavaCompletionQuery query = new JavaCompletionQuery(queryType, offset, false);
        query.javaCompletionItemFactory = factory;
        ParserManager.parse(Collections.singletonList(source), (UserTask)query.getTask());
        if (offset != substitutionOffset) {
            for (JavaCompletionItem jci : query.results) {
                jci.substitutionOffset += substitutionOffset - offset;
            }
        }
        return query.results;
    }

    static final class JavaCompletionQuery
    extends AsyncCompletionQuery {
        static final AtomicBoolean javadocBreak = new AtomicBoolean();
        static AtomicReference<CompletionDocumentation> outerDocumentation = new AtomicReference();
        private static final String ERROR = "<error>";
        private static final String INIT = "<init>";
        private static final String SPACE = " ";
        private static final String COLON = ":";
        private static final String SEMI = ";";
        private static final String EMPTY = "";
        private static final String ABSTRACT_KEYWORD = "abstract";
        private static final String ASSERT_KEYWORD = "assert";
        private static final String BOOLEAN_KEYWORD = "boolean";
        private static final String BREAK_KEYWORD = "break";
        private static final String BYTE_KEYWORD = "byte";
        private static final String CASE_KEYWORD = "case";
        private static final String CATCH_KEYWORD = "catch";
        private static final String CHAR_KEYWORD = "char";
        private static final String CLASS_KEYWORD = "class";
        private static final String CONTINUE_KEYWORD = "continue";
        private static final String DEFAULT_KEYWORD = "default";
        private static final String DO_KEYWORD = "do";
        private static final String DOUBLE_KEYWORD = "double";
        private static final String ELSE_KEYWORD = "else";
        private static final String ENUM_KEYWORD = "enum";
        private static final String EXTENDS_KEYWORD = "extends";
        private static final String FALSE_KEYWORD = "false";
        private static final String FINAL_KEYWORD = "final";
        private static final String FINALLY_KEYWORD = "finally";
        private static final String FLOAT_KEYWORD = "float";
        private static final String FOR_KEYWORD = "for";
        private static final String IF_KEYWORD = "if";
        private static final String IMPLEMENTS_KEYWORD = "implements";
        private static final String IMPORT_KEYWORD = "import";
        private static final String INSTANCEOF_KEYWORD = "instanceof";
        private static final String INT_KEYWORD = "int";
        private static final String INTERFACE_KEYWORD = "interface";
        private static final String LONG_KEYWORD = "long";
        private static final String NATIVE_KEYWORD = "native";
        private static final String NEW_KEYWORD = "new";
        private static final String NULL_KEYWORD = "null";
        private static final String PACKAGE_KEYWORD = "package";
        private static final String PRIVATE_KEYWORD = "private";
        private static final String PROTECTED_KEYWORD = "protected";
        private static final String PUBLIC_KEYWORD = "public";
        private static final String RETURN_KEYWORD = "return";
        private static final String SHORT_KEYWORD = "short";
        private static final String STATIC_KEYWORD = "static";
        private static final String STRICT_KEYWORD = "strictfp";
        private static final String SUPER_KEYWORD = "super";
        private static final String SWITCH_KEYWORD = "switch";
        private static final String SYNCHRONIZED_KEYWORD = "synchronized";
        private static final String THIS_KEYWORD = "this";
        private static final String THROW_KEYWORD = "throw";
        private static final String THROWS_KEYWORD = "throws";
        private static final String TRANSIENT_KEYWORD = "transient";
        private static final String TRUE_KEYWORD = "true";
        private static final String TRY_KEYWORD = "try";
        private static final String VOID_KEYWORD = "void";
        private static final String VOLATILE_KEYWORD = "volatile";
        private static final String WHILE_KEYWORD = "while";
        private static final String JAVA_LANG_CLASS = "java.lang.Class";
        private static final String JAVA_LANG_OBJECT = "java.lang.Object";
        private static final String JAVA_LANG_ITERABLE = "java.lang.Iterable";
        private static final String[] PRIM_KEYWORDS = new String[]{"boolean", "byte", "char", "double", "float", "int", "long", "short"};
        private static final String[] STATEMENT_KEYWORDS = new String[]{"do", "if", "for", "switch", "synchronized", "try", "void", "while"};
        private static final String[] STATEMENT_SPACE_KEYWORDS = new String[]{"assert", "new", "throw"};
        private static final String[] BLOCK_KEYWORDS = new String[]{"assert", "class", "final", "new", "throw"};
        private static final String[] CLASS_BODY_KEYWORDS = new String[]{"abstract", "class", "enum", "final", "interface", "native", "private", "protected", "public", "static", "strictfp", "synchronized", "transient", "void", "volatile"};
        private static final String SKIP_ACCESSIBILITY_CHECK = "org.netbeans.modules.editor.java.JavaCompletionProvider.skipAccessibilityCheck";
        private ArrayList<JavaCompletionItem> results;
        private JavaCompletionItemFactory javaCompletionItemFactory;
        private byte hasAdditionalItems = 0;
        private MethodParamsTipPaintComponent toolTip;
        private CompletionDocumentation documentation;
        private int anchorOffset;
        private int toolTipOffset;
        private JTextComponent component;
        private int queryType;
        private int caretOffset;
        private String filterPrefix;
        private ElementHandle element;
        private boolean hasTask;

        private JavaCompletionQuery(int queryType, int caretOffset, boolean hasTask) {
            this.queryType = queryType;
            this.caretOffset = caretOffset;
            this.hasTask = hasTask;
        }

        protected void preQueryUpdate(JTextComponent component) {
            int newCaretOffset = component.getSelectionStart();
            if (newCaretOffset >= this.caretOffset) {
                try {
                    if (JavaCompletionQuery.isJavaIdentifierPart(component.getDocument().getText(this.caretOffset, newCaretOffset - this.caretOffset))) {
                        return;
                    }
                }
                catch (BadLocationException e) {
                    // empty catch block
                }
            }
            Completion.get().hideCompletion();
        }

        protected void prepareQuery(JTextComponent component) {
            this.component = component;
            if (this.queryType == 4) {
                this.toolTip = new MethodParamsTipPaintComponent(component);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected void query(CompletionResultSet resultSet, Document doc, int caretOffset) {
            block27 : {
                try {
                    ClasspathInfo cpInfo;
                    FileObject fo;
                    this.caretOffset = caretOffset;
                    CompletionDocumentation outerDoc = outerDocumentation.getAndSet(null);
                    if (this.queryType == 2 && outerDoc != null) {
                        int offset;
                        MouseEvent lme;
                        resultSet.setDocumentation(outerDoc);
                        ToolTipSupport tts = org.netbeans.editor.Utilities.getEditorUI((JTextComponent)this.component).getToolTipSupport();
                        if (tts != null && (lme = tts.getLastMouseEvent()) != null && (offset = this.component.viewToModel(lme.getPoint())) > -1) {
                            resultSet.setAnchorOffset(offset);
                        }
                        break block27;
                    }
                    if (this.queryType != 4 && !Utilities.isJavaContext(this.component, caretOffset, true)) break block27;
                    this.results = null;
                    this.documentation = null;
                    if (this.toolTip != null) {
                        this.toolTip.clearData();
                    }
                    this.anchorOffset = -1;
                    Source source = null;
                    if (this.queryType == 2 && this.element != null && (cpInfo = ClasspathInfo.create((Document)doc)) != null && (fo = SourceUtils.getFile((ElementHandle)this.element, (ClasspathInfo)cpInfo)) != null) {
                        source = Source.create((FileObject)fo);
                    }
                    if (source == null) {
                        source = Source.create((Document)doc);
                    }
                    if (source == null) break block27;
                    ParserManager.parse(Collections.singletonList(source), (UserTask)this.getTask());
                    if ((this.queryType & 1) != 0) {
                        if (this.results != null) {
                            resultSet.addAllItems(this.results);
                        }
                        resultSet.setHasAdditionalItems(this.hasAdditionalItems > 0);
                        if (this.hasAdditionalItems == 1) {
                            resultSet.setHasAdditionalItemsText(NbBundle.getMessage(JavaCompletionProvider.class, (String)"JCP-imported-items"));
                        }
                        if (this.hasAdditionalItems == 2) {
                            resultSet.setHasAdditionalItemsText(NbBundle.getMessage(JavaCompletionProvider.class, (String)"JCP-instance-members"));
                        }
                    } else if (this.queryType == 4) {
                        if (this.toolTip != null && this.toolTip.hasData()) {
                            resultSet.setToolTip((JToolTip)this.toolTip);
                        }
                    } else if (this.queryType == 2) {
                        if (this.documentation instanceof JavaCompletionDoc) {
                            while (!this.isTaskCancelled()) {
                                try {
                                    if (javadocBreak.getAndSet(false)) {
                                        Completion c = Completion.get();
                                        c.hideDocumentation();
                                        c.showDocumentation();
                                    }
                                    ((JavaCompletionDoc)this.documentation).getFutureText().get(250, TimeUnit.MILLISECONDS);
                                    resultSet.setDocumentation(this.documentation);
                                    break;
                                }
                                catch (TimeoutException timeOut) {
                                    continue;
                                }
                            }
                        } else if (this.documentation != null) {
                            resultSet.setDocumentation(this.documentation);
                        }
                    }
                    if (this.anchorOffset > -1) {
                        resultSet.setAnchorOffset(this.anchorOffset);
                    }
                }
                catch (Exception e) {
                    Exceptions.printStackTrace((Throwable)e);
                }
                finally {
                    resultSet.finish();
                }
            }
        }

        protected boolean canFilter(JTextComponent component) {
            this.filterPrefix = null;
            int newOffset = component.getSelectionStart();
            final Document doc = component.getDocument();
            if ((this.queryType & 1) != 0) {
                final int offset = Math.min(this.anchorOffset, this.caretOffset);
                if (offset > -1) {
                    if (newOffset < offset) {
                        return true;
                    }
                    if (newOffset >= this.caretOffset) {
                        try {
                            final int len = newOffset - offset;
                            if (len == 0) {
                                this.filterPrefix = "";
                            } else if (len > 0) {
                                doc.render(new Runnable(){

                                    @Override
                                    public void run() {
                                        TokenSequence ts = SourceUtils.getJavaTokenSequence((TokenHierarchy)TokenHierarchy.get((Document)doc), (int)offset);
                                        if (ts != null && ts.move(offset) == 0 && ts.moveNext() && (ts.token().id() == JavaTokenId.IDENTIFIER || ((JavaTokenId)ts.token().id()).primaryCategory().startsWith("keyword") || ((JavaTokenId)ts.token().id()).primaryCategory().startsWith("string") || ((JavaTokenId)ts.token().id()).primaryCategory().equals("literal")) && ts.token().length() >= len) {
                                            JavaCompletionQuery.this.filterPrefix = ts.token().text().toString().substring(0, len);
                                        }
                                    }
                                });
                            }
                            if (this.filterPrefix == null) {
                                String prefix = doc.getText(offset, newOffset - offset);
                                if (prefix.length() > 0 && Utilities.getJavaCompletionAutoPopupTriggers().indexOf(prefix.charAt(prefix.length() - 1)) >= 0) {
                                    return false;
                                }
                            } else if (this.filterPrefix.length() == 0) {
                                this.anchorOffset = newOffset;
                            }
                        }
                        catch (BadLocationException e) {
                            // empty catch block
                        }
                        return true;
                    }
                }
                return false;
            }
            if (this.queryType == 4) {
                try {
                    if (newOffset == this.caretOffset) {
                        this.filterPrefix = "";
                    } else if (newOffset - this.caretOffset > 0) {
                        this.filterPrefix = doc.getText(this.caretOffset, newOffset - this.caretOffset);
                    } else if (newOffset - this.caretOffset < 0) {
                        this.filterPrefix = newOffset > this.toolTipOffset ? doc.getText(newOffset, this.caretOffset - newOffset) : null;
                    }
                }
                catch (BadLocationException ex) {
                    // empty catch block
                }
                return this.filterPrefix != null && this.filterPrefix.indexOf(44) == -1 && this.filterPrefix.indexOf(40) == -1 && this.filterPrefix.indexOf(41) == -1;
            }
            return false;
        }

        protected void filter(CompletionResultSet resultSet) {
            try {
                if ((this.queryType & 1) != 0) {
                    if (this.results != null) {
                        if (this.filterPrefix != null) {
                            resultSet.addAllItems(this.getFilteredData(this.results, this.filterPrefix));
                            resultSet.setHasAdditionalItems(this.hasAdditionalItems > 0);
                        } else {
                            Completion.get().hideDocumentation();
                            Completion.get().hideCompletion();
                        }
                    }
                } else if (this.queryType == 4) {
                    resultSet.setToolTip((JToolTip)(this.toolTip != null && this.toolTip.hasData() ? this.toolTip : null));
                }
                resultSet.setAnchorOffset(this.anchorOffset);
            }
            catch (Exception ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            resultSet.finish();
        }

        public void run(CompilationController controller) throws Exception {
            if (!this.hasTask || !this.isTaskCancelled()) {
                if ((this.queryType & 1) != 0) {
                    if (this.component != null) {
                        this.component.putClientProperty("completion-active", Boolean.TRUE);
                    }
                    this.resolveCompletion(controller);
                    if (this.component != null && this.isTaskCancelled()) {
                        this.component.putClientProperty("completion-active", Boolean.FALSE);
                    }
                } else if (this.queryType == 4) {
                    this.resolveToolTip(controller);
                } else if (this.queryType == 2) {
                    this.resolveDocumentation(controller);
                }
            }
        }

        private UserTask getTask() {
            return new Task();
        }

        private void resolveToolTip(CompilationController controller) throws IOException {
            Env env = this.getCompletionEnvironment(controller, this.queryType);
            if (env == null) {
                return;
            }
            Tree lastTree = null;
            int offset = env.getOffset();
            for (TreePath path = env.getPath(); path != null; path = path.getParentPath()) {
                int startPos;
                SourcePositions sourcePositions;
                CompilationUnitTree root;
                Tree tree = path.getLeaf();
                if (tree.getKind() == Tree.Kind.METHOD_INVOCATION) {
                    MethodInvocationTree mi = (MethodInvocationTree)tree;
                    root = env.getRoot();
                    sourcePositions = env.getSourcePositions();
                    startPos = lastTree != null ? (int)sourcePositions.getStartPosition(root, lastTree) : offset;
                    List<Tree> argTypes = this.getArgumentsUpToPos(env, mi.getArguments(), (int)sourcePositions.getEndPosition(root, (Tree)mi.getMethodSelect()), startPos, false);
                    if (argTypes != null) {
                        controller.toPhase(JavaSource.Phase.RESOLVED);
                        TypeMirror[] types = new TypeMirror[argTypes.size()];
                        int j = 0;
                        for (Tree t : argTypes) {
                            types[j++] = controller.getTrees().getTypeMirror(new TreePath(path, t));
                        }
                        List<List<String>> params = null;
                        ExpressionTree mid = mi.getMethodSelect();
                        path = new TreePath(path, (Tree)mid);
                        switch (mid.getKind()) {
                            case MEMBER_SELECT: {
                                ExpressionTree exp = ((MemberSelectTree)mid).getExpression();
                                path = new TreePath(path, (Tree)exp);
                                final Trees trees = controller.getTrees();
                                TypeMirror type = trees.getTypeMirror(path);
                                Element element = trees.getElement(path);
                                final boolean isStatic = element != null && (element.getKind().isClass() || element.getKind().isInterface() || element.getKind() == ElementKind.TYPE_PARAMETER);
                                final boolean isSuperCall = element != null && element.getKind().isField() && element.getSimpleName().contentEquals("super");
                                final Scope scope = env.getScope();
                                TypeElement enclClass = scope.getEnclosingClass();
                                final TypeMirror enclType = enclClass != null ? enclClass.asType() : null;
                                ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                                    public boolean accept(Element e, TypeMirror t) {
                                        return !(isStatic && !e.getModifiers().contains((Object)Modifier.STATIC) && e.getKind() != ElementKind.CONSTRUCTOR || t.getKind() == TypeKind.DECLARED && !trees.isAccessible(scope, e, (DeclaredType)(isSuperCall && enclType != null ? enclType : t)));
                                    }
                                };
                                params = this.getMatchingParams((CompilationInfo)controller, type, controller.getElementUtilities().getMembers(type, acceptor), ((MemberSelectTree)mid).getIdentifier().toString(), types, controller.getTypes());
                                break;
                            }
                            case IDENTIFIER: {
                                final Scope scope = env.getScope();
                                TreeUtilities tu = controller.getTreeUtilities();
                                final Trees trees = controller.getTrees();
                                TypeElement enclClass = scope.getEnclosingClass();
                                final boolean isStatic = enclClass != null ? tu.isStaticContext(scope) || env.getPath().getLeaf().getKind() == Tree.Kind.BLOCK && ((BlockTree)env.getPath().getLeaf()).isStatic() : false;
                                ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                                    public boolean accept(Element e, TypeMirror t) {
                                        switch (e.getKind()) {
                                            case CONSTRUCTOR: {
                                                return !e.getModifiers().contains((Object)Modifier.PRIVATE);
                                            }
                                            case METHOD: {
                                                return (!isStatic || e.getModifiers().contains((Object)Modifier.STATIC)) && trees.isAccessible(scope, e, (DeclaredType)t);
                                            }
                                        }
                                        return false;
                                    }
                                };
                                String name = ((IdentifierTree)mid).getName().toString();
                                if ("super".equals(name) && enclClass != null) {
                                    TypeMirror superclass = enclClass.getSuperclass();
                                    params = this.getMatchingParams((CompilationInfo)controller, superclass, controller.getElementUtilities().getMembers(superclass, acceptor), "<init>", types, controller.getTypes());
                                    break;
                                }
                                if ("this".equals(name) && enclClass != null) {
                                    TypeMirror thisclass = enclClass.asType();
                                    params = this.getMatchingParams((CompilationInfo)controller, thisclass, controller.getElementUtilities().getMembers(thisclass, acceptor), "<init>", types, controller.getTypes());
                                    break;
                                }
                                params = this.getMatchingParams((CompilationInfo)controller, enclClass != null ? enclClass.asType() : null, controller.getElementUtilities().getLocalMembersAndVars(scope, acceptor), name, types, controller.getTypes());
                                break;
                            }
                        }
                        if (params != null) {
                            this.toolTip.setData(params, types.length);
                        }
                        startPos = (int)sourcePositions.getEndPosition(env.getRoot(), (Tree)mi.getMethodSelect());
                        String text = controller.getText().substring(startPos, offset);
                        int idx = text.indexOf(40);
                        this.anchorOffset = idx < 0 ? startPos : startPos + controller.getSnapshot().getOriginalOffset(idx);
                        idx = text.lastIndexOf(44);
                        int n = this.toolTipOffset = idx < 0 ? startPos : startPos + controller.getSnapshot().getOriginalOffset(idx);
                        if (this.toolTipOffset < this.anchorOffset) {
                            this.toolTipOffset = this.anchorOffset;
                        }
                        return;
                    }
                } else if (tree.getKind() == Tree.Kind.NEW_CLASS) {
                    NewClassTree nc = (NewClassTree)tree;
                    root = env.getRoot();
                    sourcePositions = env.getSourcePositions();
                    startPos = lastTree != null ? (int)sourcePositions.getStartPosition(root, lastTree) : offset;
                    int pos = (int)sourcePositions.getEndPosition(root, (Tree)nc.getIdentifier());
                    List<Tree> argTypes = this.getArgumentsUpToPos(env, nc.getArguments(), pos, startPos, false);
                    if (argTypes != null) {
                        int idx;
                        String text;
                        controller.toPhase(JavaSource.Phase.RESOLVED);
                        TypeMirror[] types = new TypeMirror[argTypes.size()];
                        int j = 0;
                        for (Tree t : argTypes) {
                            types[j++] = controller.getTrees().getTypeMirror(new TreePath(path, t));
                        }
                        path = new TreePath(path, (Tree)nc.getIdentifier());
                        final Trees trees = controller.getTrees();
                        TypeMirror type = trees.getTypeMirror(path);
                        if (type != null && type.getKind() == TypeKind.ERROR && path.getLeaf().getKind() == Tree.Kind.PARAMETERIZED_TYPE) {
                            path = new TreePath(path, ((ParameterizedTypeTree)path.getLeaf()).getType());
                            type = trees.getTypeMirror(path);
                        }
                        Element el = trees.getElement(path);
                        final Scope scope = env.getScope();
                        final boolean isAnonymous = nc.getClassBody() != null || el.getKind().isInterface() || el.getModifiers().contains((Object)Modifier.ABSTRACT);
                        ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                            public boolean accept(Element e, TypeMirror t) {
                                return e.getKind() == ElementKind.CONSTRUCTOR && (trees.isAccessible(scope, e, (DeclaredType)t) || isAnonymous && e.getModifiers().contains((Object)Modifier.PROTECTED));
                            }
                        };
                        List<List<String>> params = this.getMatchingParams((CompilationInfo)controller, type, controller.getElementUtilities().getMembers(type, acceptor), "<init>", types, controller.getTypes());
                        if (params != null) {
                            this.toolTip.setData(params, types.length);
                        }
                        if (pos < 0) {
                            path = path.getParentPath();
                            pos = (int)sourcePositions.getStartPosition(root, path.getLeaf());
                        }
                        this.anchorOffset = (idx = (text = controller.getText().substring(pos, offset)).indexOf(40)) < 0 ? pos : pos + controller.getSnapshot().getOriginalOffset(idx);
                        idx = text.lastIndexOf(44);
                        int n = this.toolTipOffset = idx < 0 ? pos : pos + controller.getSnapshot().getOriginalOffset(idx);
                        if (this.toolTipOffset < this.anchorOffset) {
                            this.toolTipOffset = this.anchorOffset;
                        }
                        return;
                    }
                }
                lastTree = tree;
            }
        }

        private void resolveDocumentation(CompilationController controller) throws IOException {
            controller.toPhase(JavaSource.Phase.RESOLVED);
            Element el = null;
            if (this.element != null) {
                el = this.element.resolve((CompilationInfo)controller);
            } else {
                Env e = this.getCompletionEnvironment(controller, this.queryType);
                if (e != null) {
                    el = controller.getTrees().getElement(e.getPath());
                }
            }
            if (el != null) {
                switch (el.getKind()) {
                    case ANNOTATION_TYPE: 
                    case CLASS: 
                    case ENUM: 
                    case INTERFACE: {
                        if (el.asType().getKind() == TypeKind.ERROR) break;
                    }
                    case CONSTRUCTOR: 
                    case METHOD: 
                    case ENUM_CONSTANT: 
                    case FIELD: {
                        this.documentation = JavaCompletionDoc.create(controller, el, new Callable<Boolean>(){

                            @Override
                            public Boolean call() {
                                if (JavaCompletionQuery.this.isTaskCancelled()) {
                                    return true;
                                }
                                if (JavaCompletionQuery.javadocBreak.getAndSet(false)) {
                                    Completion c = Completion.get();
                                    c.hideDocumentation();
                                    c.showDocumentation();
                                }
                                return false;
                            }
                        });
                    }
                }
            }
        }

        private void resolveCompletion(CompilationController controller) throws IOException {
            Env env = this.getCompletionEnvironment(controller, this.queryType);
            if (env == null) {
                return;
            }
            this.results = new ArrayList();
            if (this.javaCompletionItemFactory == null) {
                this.javaCompletionItemFactory = new JavaCompletionItemFactory.DefaultImpl();
            }
            this.anchorOffset = controller.getSnapshot().getOriginalOffset(env.getOffset());
            TreePath path = env.getPath();
            switch (path.getLeaf().getKind()) {
                case COMPILATION_UNIT: {
                    this.insideCompilationUnit(env);
                    break;
                }
                case IMPORT: {
                    this.insideImport(env);
                    break;
                }
                case ANNOTATION_TYPE: 
                case CLASS: 
                case ENUM: 
                case INTERFACE: {
                    this.insideClass(env);
                    break;
                }
                case VARIABLE: {
                    this.insideVariable(env);
                    break;
                }
                case METHOD: {
                    this.insideMethod(env);
                    break;
                }
                case MODIFIERS: {
                    this.insideModifiers(env, path);
                    break;
                }
                case ANNOTATION: 
                case TYPE_ANNOTATION: {
                    this.insideAnnotation(env);
                    break;
                }
                case ANNOTATED_TYPE: {
                    this.insideAnnotatedType(env);
                    break;
                }
                case TYPE_PARAMETER: {
                    this.insideTypeParameter(env);
                    break;
                }
                case PARAMETERIZED_TYPE: {
                    this.insideParameterizedType(env, path);
                    break;
                }
                case UNBOUNDED_WILDCARD: 
                case EXTENDS_WILDCARD: 
                case SUPER_WILDCARD: {
                    TreePath parentPath = path.getParentPath();
                    if (parentPath.getLeaf().getKind() != Tree.Kind.PARAMETERIZED_TYPE) break;
                    this.insideParameterizedType(env, parentPath);
                    break;
                }
                case BLOCK: {
                    this.insideBlock(env);
                    break;
                }
                case MEMBER_SELECT: {
                    this.insideMemberSelect(env);
                    break;
                }
                case MEMBER_REFERENCE: {
                    this.insideMemberReference(env);
                    break;
                }
                case LAMBDA_EXPRESSION: {
                    this.insideLambdaExpression(env);
                    break;
                }
                case METHOD_INVOCATION: {
                    this.insideMethodInvocation(env);
                    break;
                }
                case NEW_CLASS: {
                    this.insideNewClass(env);
                    break;
                }
                case ASSERT: 
                case RETURN: 
                case THROW: {
                    this.localResult(env);
                    this.addValueKeywords(env);
                    break;
                }
                case TRY: {
                    this.insideTry(env);
                    break;
                }
                case CATCH: {
                    this.insideCatch(env);
                    break;
                }
                case UNION_TYPE: {
                    this.insideUnionType(env);
                    break;
                }
                case IF: {
                    this.insideIf(env);
                    break;
                }
                case WHILE_LOOP: {
                    this.insideWhile(env);
                    break;
                }
                case DO_WHILE_LOOP: {
                    this.insideDoWhile(env);
                    break;
                }
                case FOR_LOOP: {
                    this.insideFor(env);
                    break;
                }
                case ENHANCED_FOR_LOOP: {
                    this.insideForEach(env);
                    break;
                }
                case SWITCH: {
                    this.insideSwitch(env);
                    break;
                }
                case CASE: {
                    this.insideCase(env);
                    break;
                }
                case LABELED_STATEMENT: {
                    this.localResult(env);
                    this.addKeywordsForStatement(env);
                    break;
                }
                case PARENTHESIZED: {
                    this.insideParens(env);
                    break;
                }
                case TYPE_CAST: {
                    this.insideExpression(env, path);
                    break;
                }
                case INSTANCE_OF: {
                    this.insideTypeCheck(env);
                    break;
                }
                case ARRAY_ACCESS: {
                    this.insideArrayAccess(env);
                    break;
                }
                case NEW_ARRAY: {
                    this.insideNewArray(env);
                    break;
                }
                case ASSIGNMENT: {
                    this.insideAssignment(env);
                    break;
                }
                case MULTIPLY_ASSIGNMENT: 
                case DIVIDE_ASSIGNMENT: 
                case REMAINDER_ASSIGNMENT: 
                case PLUS_ASSIGNMENT: 
                case MINUS_ASSIGNMENT: 
                case LEFT_SHIFT_ASSIGNMENT: 
                case RIGHT_SHIFT_ASSIGNMENT: 
                case UNSIGNED_RIGHT_SHIFT_ASSIGNMENT: 
                case AND_ASSIGNMENT: 
                case XOR_ASSIGNMENT: 
                case OR_ASSIGNMENT: {
                    this.insideCompoundAssignment(env);
                    break;
                }
                case PREFIX_INCREMENT: 
                case PREFIX_DECREMENT: 
                case UNARY_PLUS: 
                case UNARY_MINUS: 
                case BITWISE_COMPLEMENT: 
                case LOGICAL_COMPLEMENT: {
                    this.localResult(env);
                    break;
                }
                case AND: 
                case CONDITIONAL_AND: 
                case CONDITIONAL_OR: 
                case DIVIDE: 
                case EQUAL_TO: 
                case GREATER_THAN: 
                case GREATER_THAN_EQUAL: 
                case LEFT_SHIFT: 
                case LESS_THAN: 
                case LESS_THAN_EQUAL: 
                case MINUS: 
                case MULTIPLY: 
                case NOT_EQUAL_TO: 
                case OR: 
                case PLUS: 
                case REMAINDER: 
                case RIGHT_SHIFT: 
                case UNSIGNED_RIGHT_SHIFT: 
                case XOR: {
                    this.insideBinaryTree(env);
                    break;
                }
                case CONDITIONAL_EXPRESSION: {
                    this.insideConditionalExpression(env);
                    break;
                }
                case EXPRESSION_STATEMENT: {
                    this.insideExpressionStatement(env);
                    break;
                }
                case BREAK: {
                    this.insideBreak(env);
                    break;
                }
                case STRING_LITERAL: {
                    this.insideStringLiteral(env);
                }
            }
        }

        private void insideCompilationUnit(Env env) throws IOException {
            int offset = env.getOffset();
            SourcePositions sourcePositions = env.getSourcePositions();
            CompilationUnitTree root = env.getRoot();
            ExpressionTree pkg = root.getPackageName();
            if (pkg == null || (long)offset <= sourcePositions.getStartPosition(root, (Tree)root)) {
                this.addKeywordsForCU(env);
                return;
            }
            if ((long)offset <= sourcePositions.getStartPosition(root, (Tree)pkg)) {
                this.addPackages(env, null, true);
            } else {
                TokenSequence<JavaTokenId> first = this.findFirstNonWhitespaceToken(env, (int)sourcePositions.getEndPosition(root, (Tree)pkg), offset);
                if (first != null && first.token().id() == JavaTokenId.SEMICOLON) {
                    this.addKeywordsForCU(env);
                }
            }
        }

        private void insideImport(Env env) {
            CompilationUnitTree root;
            int offset = env.getOffset();
            String prefix = env.getPrefix();
            ImportTree im = (ImportTree)env.getPath().getLeaf();
            SourcePositions sourcePositions = env.getSourcePositions();
            if ((long)offset <= sourcePositions.getStartPosition(root = env.getRoot(), im.getQualifiedIdentifier())) {
                TokenSequence<JavaTokenId> last = this.findLastNonWhitespaceToken(env, (Tree)im, offset);
                if (last != null && last.token().id() == JavaTokenId.IMPORT && Utilities.startsWith("static", prefix)) {
                    this.addKeyword(env, "static", " ", false);
                }
                this.addPackages(env, null, false);
            }
        }

        private void insideClass(Env env) throws IOException {
            String headerText;
            int tpPos;
            Tree impl;
            int extPos;
            int implPos;
            int idx;
            TypeParameterTree tp;
            int offset = env.getOffset();
            env.insideClass(true);
            TreePath path = env.getPath();
            ClassTree cls = (ClassTree)path.getLeaf();
            CompilationController controller = env.getController();
            SourcePositions sourcePositions = env.getSourcePositions();
            CompilationUnitTree root = env.getRoot();
            int startPos = (int)sourcePositions.getEndPosition(root, (Tree)cls.getModifiers());
            if (startPos <= 0) {
                startPos = (int)sourcePositions.getStartPosition(root, (Tree)cls);
            }
            if ((idx = (headerText = controller.getText().substring(startPos, offset)).indexOf(123)) >= 0) {
                this.addKeywordsForClassBody(env);
                this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.TYPE_PARAMETER), null);
                this.addElementCreators(env);
                return;
            }
            TreeUtilities tu = controller.getTreeUtilities();
            Tree lastImpl = null;
            Iterator i$ = cls.getImplementsClause().iterator();
            while (i$.hasNext() && (long)(implPos = (int)sourcePositions.getEndPosition(root, impl = (Tree)i$.next())) != -1 && offset > implPos) {
                lastImpl = impl;
                startPos = implPos;
            }
            if (lastImpl != null) {
                TokenSequence<JavaTokenId> last = this.findLastNonWhitespaceToken(env, startPos, offset);
                if (last != null && last.token().id() == JavaTokenId.COMMA) {
                    controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                    env.addToExcludes(controller.getTrees().getElement(path));
                    this.addTypes(env, EnumSet.of(ElementKind.INTERFACE, ElementKind.ANNOTATION_TYPE), null);
                }
                return;
            }
            Tree ext = cls.getExtendsClause();
            if (ext != null && (long)(extPos = (int)sourcePositions.getEndPosition(root, ext)) != -1 && offset > extPos) {
                TokenSequence<JavaTokenId> last = this.findLastNonWhitespaceToken(env, extPos + 1, offset);
                if (last != null && last.token().id() == JavaTokenId.IMPLEMENTS) {
                    controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                    env.addToExcludes(controller.getTrees().getElement(path));
                    this.addTypes(env, EnumSet.of(ElementKind.INTERFACE, ElementKind.ANNOTATION_TYPE), null);
                } else {
                    this.addKeyword(env, "implements", " ", false);
                }
                return;
            }
            TypeParameterTree lastTypeParam = null;
            Iterator i$2 = cls.getTypeParameters().iterator();
            while (i$2.hasNext() && (long)(tpPos = (int)sourcePositions.getEndPosition(root, (Tree)(tp = (TypeParameterTree)i$2.next()))) != -1 && offset > tpPos) {
                lastTypeParam = tp;
                startPos = tpPos;
            }
            if (lastTypeParam != null) {
                TokenSequence<JavaTokenId> first = this.findFirstNonWhitespaceToken(env, startPos, offset);
                if (first != null && first.token().id() == JavaTokenId.GT) {
                    if ((first = this.nextNonWhitespaceToken(first)) != null && first.offset() < offset) {
                        if (first.token().id() == JavaTokenId.EXTENDS) {
                            controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                            env.afterExtends();
                            env.addToExcludes(controller.getTrees().getElement(path));
                            this.addTypes(env, tu.isInterface(cls) ? EnumSet.of(ElementKind.INTERFACE, ElementKind.ANNOTATION_TYPE) : EnumSet.of(ElementKind.CLASS), null);
                            return;
                        }
                        if (first.token().id() == JavaTokenId.IMPLEMENTS) {
                            controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                            env.addToExcludes(controller.getTrees().getElement(path));
                            this.addTypes(env, EnumSet.of(ElementKind.INTERFACE, ElementKind.ANNOTATION_TYPE), null);
                            return;
                        }
                    }
                    if (!tu.isAnnotation(cls)) {
                        if (!tu.isEnum(cls)) {
                            this.addKeyword(env, "extends", " ", false);
                        }
                        if (!tu.isInterface(cls)) {
                            this.addKeyword(env, "implements", " ", false);
                        }
                    }
                } else if (lastTypeParam.getBounds().isEmpty()) {
                    this.addKeyword(env, "extends", " ", false);
                }
                return;
            }
            TokenSequence<JavaTokenId> lastNonWhitespaceToken = this.findLastNonWhitespaceToken(env, startPos, offset);
            if (lastNonWhitespaceToken != null) {
                switch ((JavaTokenId)lastNonWhitespaceToken.token().id()) {
                    case EXTENDS: {
                        controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                        env.afterExtends();
                        env.addToExcludes(controller.getTrees().getElement(path));
                        this.addTypes(env, tu.isInterface(cls) ? EnumSet.of(ElementKind.INTERFACE, ElementKind.ANNOTATION_TYPE) : EnumSet.of(ElementKind.CLASS), null);
                        break;
                    }
                    case IMPLEMENTS: {
                        controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                        env.addToExcludes(controller.getTrees().getElement(path));
                        this.addTypes(env, EnumSet.of(ElementKind.INTERFACE, ElementKind.ANNOTATION_TYPE), null);
                        break;
                    }
                    case IDENTIFIER: {
                        if (tu.isAnnotation(cls)) break;
                        if (!tu.isEnum(cls)) {
                            this.addKeyword(env, "extends", " ", false);
                        }
                        if (tu.isInterface(cls)) break;
                        this.addKeyword(env, "implements", " ", false);
                    }
                }
                return;
            }
            lastNonWhitespaceToken = this.findLastNonWhitespaceToken(env, (int)sourcePositions.getStartPosition(root, (Tree)cls), offset);
            if (lastNonWhitespaceToken != null && lastNonWhitespaceToken.token().id() == JavaTokenId.AT) {
                this.addKeyword(env, "interface", " ", false);
                this.addTypes(env, EnumSet.of(ElementKind.ANNOTATION_TYPE), null);
            } else if (path.getParentPath().getLeaf().getKind() == Tree.Kind.COMPILATION_UNIT) {
                this.addClassModifiers(env, cls.getModifiers().getFlags());
            } else {
                this.addMemberModifiers(env, cls.getModifiers().getFlags(), false);
                this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.TYPE_PARAMETER), null);
            }
        }

        private void insideVariable(Env env) throws IOException {
            int typePos;
            int offset = env.getOffset();
            TreePath path = env.getPath();
            VariableTree var = (VariableTree)path.getLeaf();
            SourcePositions sourcePositions = env.getSourcePositions();
            CompilationUnitTree root = env.getRoot();
            Tree type = var.getType();
            int n = typePos = type.getKind() == Tree.Kind.ERRONEOUS && ((ErroneousTree)type).getErrorTrees().isEmpty() ? (int)sourcePositions.getEndPosition(root, type) : (int)sourcePositions.getStartPosition(root, type);
            if (offset <= typePos) {
                Tree parent = path.getParentPath().getLeaf();
                if (parent.getKind() == Tree.Kind.CATCH) {
                    TypeElement te;
                    CompilationController controller = env.getController();
                    if (this.queryType == 1) {
                        TreePath tryPath = Utilities.getPathElementOfKind(Tree.Kind.TRY, path);
                        Set exs = controller.getTreeUtilities().getUncaughtExceptions(tryPath);
                        Elements elements = controller.getElements();
                        for (TypeMirror ex : exs) {
                            if (ex.getKind() != TypeKind.DECLARED || !this.startsWith(env, ((DeclaredType)ex).asElement().getSimpleName().toString()) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(((DeclaredType)ex).asElement())) continue;
                            env.addToExcludes(((DeclaredType)ex).asElement());
                            this.results.add(this.javaCompletionItemFactory.createTypeItem((CompilationInfo)controller, (TypeElement)((DeclaredType)ex).asElement(), (DeclaredType)ex, this.anchorOffset, env.getReferencesCount(), elements.isDeprecated(((DeclaredType)ex).asElement()), false, false, false, true, false, env.getWhiteList()));
                        }
                    }
                    if ((te = controller.getElements().getTypeElement("java.lang.Throwable")) != null) {
                        this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.TYPE_PARAMETER), controller.getTypes().getDeclaredType(te, new TypeMirror[0]));
                    }
                } else if (parent.getKind() == Tree.Kind.TRY) {
                    CompilationController controller = env.getController();
                    TypeElement te = controller.getElements().getTypeElement("java.lang.AutoCloseable");
                    if (te != null) {
                        this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.TYPE_PARAMETER), controller.getTypes().getDeclaredType(te, new TypeMirror[0]));
                    }
                } else {
                    boolean isLocal = !TreeUtilities.CLASS_TREE_KINDS.contains((Object)parent.getKind());
                    this.addMemberModifiers(env, var.getModifiers().getFlags(), isLocal);
                    this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.TYPE_PARAMETER), null);
                    ModifiersTree mods = var.getModifiers();
                    if (mods.getFlags().isEmpty() && mods.getAnnotations().isEmpty()) {
                        this.addElementCreators(env);
                    }
                }
                return;
            }
            Tree init = this.unwrapErrTree((Tree)var.getInitializer());
            if (init == null) {
                TokenSequence<JavaTokenId> last = this.findLastNonWhitespaceToken(env, (int)sourcePositions.getEndPosition(root, type), offset);
                if (last == null || last.token().id() == JavaTokenId.COMMA) {
                    this.insideExpression(env, new TreePath(path, type));
                } else if (last.token().id() == JavaTokenId.EQ) {
                    this.localResult(env);
                    this.addValueKeywords(env);
                }
            } else {
                int pos = (int)sourcePositions.getStartPosition(root, init);
                if (pos < 0) {
                    return;
                }
                if (offset <= pos) {
                    TokenSequence<JavaTokenId> last = this.findLastNonWhitespaceToken(env, (int)sourcePositions.getEndPosition(root, type), offset);
                    if (last == null) {
                        this.insideExpression(env, new TreePath(path, type));
                    } else if (last.token().id() == JavaTokenId.EQ) {
                        this.localResult(env);
                        this.addValueKeywords(env);
                    }
                } else {
                    this.insideExpression(env, new TreePath(path, init));
                }
            }
        }

        private void insideMethod(Env env) throws IOException {
            int modsPos;
            int retPos;
            TokenSequence<JavaTokenId> lastToken;
            int thrPos;
            Tree thr;
            ModifiersTree mods;
            Tree retType;
            int offset = env.getOffset();
            TreePath path = env.getPath();
            MethodTree mth = (MethodTree)path.getLeaf();
            CompilationController controller = env.getController();
            SourcePositions sourcePositions = env.getSourcePositions();
            CompilationUnitTree root = env.getRoot();
            int startPos = (int)sourcePositions.getStartPosition(root, (Tree)mth);
            Tree lastTree = null;
            int state = 0;
            Iterator i$ = mth.getThrows().iterator();
            while (i$.hasNext() && (long)(thrPos = (int)sourcePositions.getEndPosition(root, thr = (Tree)i$.next())) != -1 && offset > thrPos) {
                lastTree = thr;
                startPos = thrPos;
                state = 4;
            }
            if (lastTree == null) {
                VariableTree param;
                int parPos;
                i$ = mth.getParameters().iterator();
                while (i$.hasNext() && (long)(parPos = (int)sourcePositions.getEndPosition(root, (Tree)(param = (VariableTree)i$.next()))) != -1 && offset > parPos) {
                    lastTree = param;
                    startPos = parPos;
                    state = 3;
                }
            }
            if (lastTree == null && (retType = mth.getReturnType()) != null && (long)(retPos = (int)sourcePositions.getEndPosition(root, retType)) != -1 && offset > retPos) {
                lastTree = retType;
                startPos = retPos;
                state = 2;
            }
            if (lastTree == null) {
                int tpPos;
                TypeParameterTree tp;
                i$ = mth.getTypeParameters().iterator();
                while (i$.hasNext() && (long)(tpPos = (int)sourcePositions.getEndPosition(root, (Tree)(tp = (TypeParameterTree)i$.next()))) != -1 && offset > tpPos) {
                    lastTree = tp;
                    startPos = tpPos;
                    state = 1;
                }
            }
            if (lastTree == null && (mods = mth.getModifiers()) != null && (long)(modsPos = (int)sourcePositions.getEndPosition(root, (Tree)mods)) != -1 && offset > modsPos) {
                lastTree = mods;
                startPos = modsPos;
            }
            if ((lastToken = this.findLastNonWhitespaceToken(env, startPos, offset)) != null) {
                block0 : switch ((JavaTokenId)lastToken.token().id()) {
                    case LPAREN: {
                        this.addMemberModifiers(env, Collections.emptySet(), true);
                        this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.TYPE_PARAMETER), null);
                        break;
                    }
                    case RPAREN: {
                        Tree mthParent = path.getParentPath().getLeaf();
                        switch (mthParent.getKind()) {
                            case ANNOTATION_TYPE: {
                                this.addKeyword(env, "default", " ", false);
                                break block0;
                            }
                        }
                        this.addKeyword(env, "throws", " ", false);
                        break;
                    }
                    case THROWS: {
                        TypeElement te;
                        if (this.queryType == 1 && mth.getBody() != null) {
                            controller.toPhase(JavaSource.Phase.RESOLVED);
                            Set exs = controller.getTreeUtilities().getUncaughtExceptions(new TreePath(path, (Tree)mth.getBody()));
                            Elements elements = controller.getElements();
                            for (TypeMirror ex : exs) {
                                if (ex.getKind() != TypeKind.DECLARED || !this.startsWith(env, ((DeclaredType)ex).asElement().getSimpleName().toString()) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(((DeclaredType)ex).asElement())) continue;
                                env.addToExcludes(((DeclaredType)ex).asElement());
                                this.results.add(this.javaCompletionItemFactory.createTypeItem((CompilationInfo)env.getController(), (TypeElement)((DeclaredType)ex).asElement(), (DeclaredType)ex, this.anchorOffset, env.getReferencesCount(), elements.isDeprecated(((DeclaredType)ex).asElement()), false, false, false, true, false, env.getWhiteList()));
                            }
                        }
                        if ((te = controller.getElements().getTypeElement("java.lang.Throwable")) == null) break;
                        this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.TYPE_PARAMETER), controller.getTypes().getDeclaredType(te, new TypeMirror[0]));
                        break;
                    }
                    case DEFAULT: {
                        this.addLocalConstantsAndTypes(env);
                        break;
                    }
                    case GT: 
                    case GTGT: 
                    case GTGTGT: {
                        this.addPrimitiveTypeKeywords(env);
                        this.addKeyword(env, "void", " ", false);
                        this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.TYPE_PARAMETER), null);
                        break;
                    }
                    case COMMA: {
                        switch (state) {
                            case 3: {
                                this.addMemberModifiers(env, Collections.emptySet(), true);
                                this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.TYPE_PARAMETER), null);
                                break block0;
                            }
                            case 4: {
                                TypeElement te;
                                if (this.queryType == 1 && mth.getBody() != null) {
                                    controller.toPhase(JavaSource.Phase.RESOLVED);
                                    Set exs = controller.getTreeUtilities().getUncaughtExceptions(new TreePath(path, (Tree)mth.getBody()));
                                    Trees trees = controller.getTrees();
                                    Types types = controller.getTypes();
                                    for (ExpressionTree thr2 : mth.getThrows()) {
                                        TypeMirror t = trees.getTypeMirror(new TreePath(path, (Tree)thr2));
                                        Iterator it = exs.iterator();
                                        while (it.hasNext()) {
                                            if (!types.isSubtype((TypeMirror)it.next(), t)) continue;
                                            it.remove();
                                        }
                                        if (thr2 != lastTree) continue;
                                        break;
                                    }
                                    Elements elements = controller.getElements();
                                    for (TypeMirror ex : exs) {
                                        if (ex.getKind() != TypeKind.DECLARED || !this.startsWith(env, ((DeclaredType)ex).asElement().getSimpleName().toString()) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(((DeclaredType)ex).asElement())) continue;
                                        env.addToExcludes(((DeclaredType)ex).asElement());
                                        this.results.add(this.javaCompletionItemFactory.createTypeItem((CompilationInfo)env.getController(), (TypeElement)((DeclaredType)ex).asElement(), (DeclaredType)ex, this.anchorOffset, env.getReferencesCount(), elements.isDeprecated(((DeclaredType)ex).asElement()), false, false, false, true, false, env.getWhiteList()));
                                    }
                                }
                                if ((te = controller.getElements().getTypeElement("java.lang.Throwable")) == null) break block0;
                                this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.TYPE_PARAMETER), controller.getTypes().getDeclaredType(te, new TypeMirror[0]));
                            }
                        }
                    }
                }
                return;
            }
            switch (state) {
                case 0: {
                    this.addMemberModifiers(env, mth.getModifiers().getFlags(), false);
                    this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.TYPE_PARAMETER), null);
                    break;
                }
                case 1: {
                    if (!((TypeParameterTree)lastTree).getBounds().isEmpty()) break;
                    this.addKeyword(env, "extends", " ", false);
                    break;
                }
                case 2: {
                    this.insideExpression(env, new TreePath(path, lastTree));
                }
            }
        }

        private void insideModifiers(Env env, TreePath modPath) throws IOException {
            Tree grandParent;
            int offset = env.getOffset();
            ModifiersTree mods = (ModifiersTree)modPath.getLeaf();
            EnumSet<Modifier> m = EnumSet.noneOf(Modifier.class);
            TokenSequence ts = env.getController().getTreeUtilities().tokensFor((Tree)mods, env.getSourcePositions());
            JavaTokenId lastNonWhitespaceTokenId = null;
            while (ts.moveNext() && ts.offset() < offset) {
                lastNonWhitespaceTokenId = (JavaTokenId)ts.token().id();
                switch (lastNonWhitespaceTokenId) {
                    case PUBLIC: {
                        m.add(Modifier.PUBLIC);
                        break;
                    }
                    case PROTECTED: {
                        m.add(Modifier.PROTECTED);
                        break;
                    }
                    case PRIVATE: {
                        m.add(Modifier.PRIVATE);
                        break;
                    }
                    case STATIC: {
                        m.add(Modifier.STATIC);
                        break;
                    }
                    case DEFAULT: {
                        m.add(Modifier.DEFAULT);
                        break;
                    }
                    case ABSTRACT: {
                        m.add(Modifier.ABSTRACT);
                        break;
                    }
                    case FINAL: {
                        m.add(Modifier.FINAL);
                        break;
                    }
                    case SYNCHRONIZED: {
                        m.add(Modifier.SYNCHRONIZED);
                        break;
                    }
                    case NATIVE: {
                        m.add(Modifier.NATIVE);
                        break;
                    }
                    case STRICTFP: {
                        m.add(Modifier.STRICTFP);
                        break;
                    }
                    case TRANSIENT: {
                        m.add(Modifier.TRANSIENT);
                        break;
                    }
                    case VOLATILE: {
                        m.add(Modifier.VOLATILE);
                    }
                }
            }
            if (lastNonWhitespaceTokenId == JavaTokenId.AT) {
                this.addKeyword(env, "interface", " ", false);
                this.addTypes(env, EnumSet.of(ElementKind.ANNOTATION_TYPE), null);
                return;
            }
            TreePath parentPath = modPath.getParentPath();
            Tree parent = parentPath.getLeaf();
            TreePath grandParentPath = parentPath.getParentPath();
            Tree tree = grandParent = grandParentPath != null ? grandParentPath.getLeaf() : null;
            if (this.isTopLevelClass(parent, env.getRoot())) {
                this.addClassModifiers(env, m);
            } else if (parent.getKind() != Tree.Kind.VARIABLE || grandParent == null || TreeUtilities.CLASS_TREE_KINDS.contains((Object)grandParent.getKind())) {
                this.addMemberModifiers(env, m, false);
                this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.TYPE_PARAMETER), null);
            } else if (parent.getKind() == Tree.Kind.VARIABLE && grandParent.getKind() == Tree.Kind.METHOD) {
                this.addMemberModifiers(env, m, true);
                this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.TYPE_PARAMETER), null);
            } else {
                this.localResult(env);
                this.addKeywordsForBlock(env);
            }
        }

        private void insideAnnotation(Env env) throws IOException {
            CompilationUnitTree root;
            int offset = env.getOffset();
            TreePath path = env.getPath();
            AnnotationTree ann = (AnnotationTree)path.getLeaf();
            CompilationController controller = env.getController();
            SourcePositions sourcePositions = env.getSourcePositions();
            int typeEndPos = (int)sourcePositions.getEndPosition(root = env.getRoot(), ann.getAnnotationType());
            if (offset <= typeEndPos) {
                TreePath parentPath = path.getParentPath();
                if (parentPath.getLeaf().getKind() == Tree.Kind.MODIFIERS && (parentPath.getParentPath().getLeaf().getKind() != Tree.Kind.VARIABLE || parentPath.getParentPath().getParentPath().getLeaf().getKind() == Tree.Kind.CLASS)) {
                    this.addKeyword(env, "interface", " ", false);
                }
                if (this.queryType == 1) {
                    controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                    Set<? extends TypeMirror> smarts = env.getSmartTypes();
                    if (smarts != null) {
                        Elements elements = controller.getElements();
                        for (TypeMirror smart : smarts) {
                            TypeElement elem;
                            if (smart.getKind() != TypeKind.DECLARED || (elem = (TypeElement)((DeclaredType)smart).asElement()).getKind() != ElementKind.ANNOTATION_TYPE || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(elem)) continue;
                            this.results.add(this.javaCompletionItemFactory.createTypeItem((CompilationInfo)env.getController(), elem, (DeclaredType)smart, this.anchorOffset, env.getReferencesCount(), elements.isDeprecated(elem), false, false, false, true, false, env.getWhiteList()));
                        }
                    }
                }
                this.addTypes(env, EnumSet.of(ElementKind.ANNOTATION_TYPE), null);
                return;
            }
            TokenSequence<JavaTokenId> ts = this.findLastNonWhitespaceToken(env, (Tree)ann, offset);
            if (ts == null || ts.token().id() != JavaTokenId.LPAREN && ts.token().id() != JavaTokenId.COMMA) {
                return;
            }
            controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
            Trees trees = controller.getTrees();
            Element annTypeElement = trees.getElement(new TreePath(path, ann.getAnnotationType()));
            if (annTypeElement != null && annTypeElement.getKind() == ElementKind.ANNOTATION_TYPE) {
                HashSet<String> names = new HashSet<String>();
                for (ExpressionTree arg : ann.getArguments()) {
                    ExpressionTree var;
                    if (arg.getKind() != Tree.Kind.ASSIGNMENT || sourcePositions.getEndPosition(root, (Tree)arg) >= (long)offset || (var = ((AssignmentTree)arg).getVariable()).getKind() != Tree.Kind.IDENTIFIER) continue;
                    names.add(((IdentifierTree)var).getName().toString());
                }
                Elements elements = controller.getElements();
                ExecutableElement valueElement = null;
                for (Element e : ((TypeElement)annTypeElement).getEnclosedElements()) {
                    if (e.getKind() != ElementKind.METHOD) continue;
                    String name = e.getSimpleName().toString();
                    if ("value".equals(name)) {
                        valueElement = (ExecutableElement)e;
                    } else if (((ExecutableElement)e).getDefaultValue() == null) {
                        valueElement = null;
                    }
                    if (names.contains(name) || !this.startsWith(env, name) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(e)) continue;
                    this.results.add(this.javaCompletionItemFactory.createAttributeItem((CompilationInfo)env.getController(), (ExecutableElement)e, (ExecutableType)e.asType(), this.anchorOffset, elements.isDeprecated(e)));
                }
                if (valueElement != null && names.isEmpty()) {
                    Element el = null;
                    TreePath pPath = path.getParentPath();
                    if (pPath.getLeaf().getKind() == Tree.Kind.COMPILATION_UNIT) {
                        el = trees.getElement(pPath);
                    } else {
                        pPath = pPath.getParentPath();
                        Tree.Kind pKind = pPath.getLeaf().getKind();
                        if (TreeUtilities.CLASS_TREE_KINDS.contains((Object)pKind) || pKind == Tree.Kind.METHOD || pKind == Tree.Kind.VARIABLE) {
                            el = trees.getElement(pPath);
                        }
                    }
                    if (el != null) {
                        AnnotationMirror annotation = null;
                        for (AnnotationMirror am : el.getAnnotationMirrors()) {
                            if (annTypeElement != am.getAnnotationType().asElement()) continue;
                            annotation = am;
                            break;
                        }
                        if (annotation != null) {
                            this.addAttributeValues(env, el, annotation, valueElement);
                        }
                    }
                    this.addLocalConstantsAndTypes(env);
                }
            }
        }

        private void insideAnnotatedType(Env env) throws IOException {
            CompilationUnitTree root;
            int offset = env.getOffset();
            AnnotatedTypeTree att = (AnnotatedTypeTree)env.getPath().getLeaf();
            SourcePositions sourcePositions = env.getSourcePositions();
            int pos = (int)sourcePositions.getStartPosition(root = env.getRoot(), (Tree)att.getUnderlyingType());
            if (pos >= 0 && pos < offset) {
                this.insideExpression(env, new TreePath(env.getPath(), (Tree)att.getUnderlyingType()));
            } else {
                this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.TYPE_PARAMETER), null);
            }
        }

        private void insideAnnotationAttribute(Env env, TreePath annotationPath, Name attributeName) throws IOException {
            CompilationController controller = env.getController();
            controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
            Trees trees = controller.getTrees();
            AnnotationTree at = (AnnotationTree)annotationPath.getLeaf();
            Element annTypeElement = trees.getElement(new TreePath(annotationPath, at.getAnnotationType()));
            Element el = null;
            TreePath pPath = annotationPath.getParentPath();
            if (pPath.getLeaf().getKind() == Tree.Kind.COMPILATION_UNIT) {
                el = trees.getElement(pPath);
            } else {
                Tree.Kind pKind = (pPath = pPath.getParentPath()).getLeaf().getKind();
                if (TreeUtilities.CLASS_TREE_KINDS.contains((Object)pKind) || pKind == Tree.Kind.METHOD || pKind == Tree.Kind.VARIABLE) {
                    el = trees.getElement(pPath);
                }
            }
            if (el != null && annTypeElement != null && annTypeElement.getKind() == ElementKind.ANNOTATION_TYPE) {
                ExecutableElement memberElement = null;
                for (Element e : ((TypeElement)annTypeElement).getEnclosedElements()) {
                    if (e.getKind() != ElementKind.METHOD || !attributeName.contentEquals(e.getSimpleName())) continue;
                    memberElement = (ExecutableElement)e;
                    break;
                }
                if (memberElement != null) {
                    AnnotationMirror annotation = null;
                    for (AnnotationMirror am : el.getAnnotationMirrors()) {
                        if (annTypeElement != am.getAnnotationType().asElement()) continue;
                        annotation = am;
                        break;
                    }
                    if (annotation != null) {
                        this.addAttributeValues(env, el, annotation, memberElement);
                    }
                }
            }
        }

        private void insideTypeParameter(Env env) throws IOException {
            int offset = env.getOffset();
            TreePath path = env.getPath();
            TypeParameterTree tp = (TypeParameterTree)path.getLeaf();
            CompilationController controller = env.getController();
            TokenSequence<JavaTokenId> ts = this.findLastNonWhitespaceToken(env, (Tree)tp, offset);
            if (ts != null) {
                switch ((JavaTokenId)ts.token().id()) {
                    case EXTENDS: {
                        controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                        env.addToExcludes(controller.getTrees().getElement(path.getParentPath()));
                        this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ANNOTATION_TYPE), null);
                        break;
                    }
                    case AMP: {
                        controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                        env.addToExcludes(controller.getTrees().getElement(path.getParentPath()));
                        this.addTypes(env, EnumSet.of(ElementKind.INTERFACE, ElementKind.ANNOTATION_TYPE), null);
                        break;
                    }
                    case IDENTIFIER: {
                        if ((long)ts.offset() != env.getSourcePositions().getStartPosition(env.getRoot(), (Tree)tp)) break;
                        this.addKeyword(env, "extends", " ", false);
                    }
                }
            }
        }

        private void insideParameterizedType(Env env, TreePath ptPath) throws IOException {
            int offset = env.getOffset();
            ParameterizedTypeTree ta = (ParameterizedTypeTree)ptPath.getLeaf();
            TokenSequence<JavaTokenId> ts = this.findLastNonWhitespaceToken(env, (Tree)ta, offset);
            if (ts != null) {
                switch ((JavaTokenId)ts.token().id()) {
                    case EXTENDS: 
                    case COMMA: 
                    case SUPER: 
                    case LT: {
                        if (this.queryType == 1) {
                            int parPos;
                            TypeElement te;
                            List<? extends TypeParameterElement> typeParams;
                            Set<? extends TypeMirror> smarts;
                            Tree arg;
                            CompilationController controller = env.getController();
                            SourcePositions sourcePositions = env.getSourcePositions();
                            CompilationUnitTree root = env.getRoot();
                            int index = 0;
                            Iterator i$ = ta.getTypeArguments().iterator();
                            while (i$.hasNext() && (long)(parPos = (int)sourcePositions.getEndPosition(root, arg = (Tree)i$.next())) != -1 && offset > parPos) {
                                ++index;
                            }
                            Elements elements = controller.getElements();
                            Types types = controller.getTypes();
                            TypeMirror tm = controller.getTrees().getTypeMirror(new TreePath(ptPath, ta.getType()));
                            List<? extends TypeMirror> bounds = null;
                            if (tm.getKind() == TypeKind.DECLARED && index < (typeParams = (te = (TypeElement)((DeclaredType)tm).asElement()).getTypeParameters()).size()) {
                                TypeParameterElement typeParam = typeParams.get(index);
                                bounds = typeParam.getBounds();
                            }
                            if ((smarts = env.getSmartTypes()) != null) {
                                for (TypeMirror smart : smarts) {
                                    List<? extends TypeMirror> typeArgs;
                                    if (smart == null || smart.getKind() != TypeKind.DECLARED || !types.isSubtype(tm, types.erasure(smart)) || index >= (typeArgs = ((DeclaredType)smart).getTypeArguments()).size()) continue;
                                    TypeMirror lowerBound = typeArgs.get(index);
                                    TypeMirror upperBound = null;
                                    if (lowerBound.getKind() == TypeKind.WILDCARD) {
                                        upperBound = ((WildcardType)lowerBound).getSuperBound();
                                        lowerBound = ((WildcardType)lowerBound).getExtendsBound();
                                    }
                                    if (lowerBound != null && lowerBound.getKind() == TypeKind.TYPEVAR) {
                                        lowerBound = ((TypeVariable)lowerBound).getUpperBound();
                                    }
                                    if (upperBound != null && upperBound.getKind() == TypeKind.TYPEVAR) {
                                        upperBound = ((TypeVariable)upperBound).getUpperBound();
                                    }
                                    if (upperBound != null && upperBound.getKind() == TypeKind.DECLARED) {
                                        while (upperBound.getKind() == TypeKind.DECLARED) {
                                            TypeElement elem = (TypeElement)((DeclaredType)upperBound).asElement();
                                            if (this.startsWith(env, elem.getSimpleName().toString()) && this.withinBounds(env, upperBound, bounds) && (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(elem))) {
                                                this.results.add(this.javaCompletionItemFactory.createTypeItem((CompilationInfo)env.getController(), elem, (DeclaredType)upperBound, this.anchorOffset, env.getReferencesCount(), elements.isDeprecated(elem), false, true, false, true, false, env.getWhiteList()));
                                            }
                                            env.addToExcludes(elem);
                                            upperBound = elem.getSuperclass();
                                        }
                                        continue;
                                    }
                                    if (lowerBound == null || lowerBound.getKind() != TypeKind.DECLARED) continue;
                                    for (DeclaredType subtype : this.getSubtypesOf(env, (DeclaredType)lowerBound)) {
                                        TypeElement elem = (TypeElement)subtype.asElement();
                                        if (this.withinBounds(env, subtype, bounds) && (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(elem))) {
                                            this.results.add(this.javaCompletionItemFactory.createTypeItem((CompilationInfo)env.getController(), elem, subtype, this.anchorOffset, env.getReferencesCount(), elements.isDeprecated(elem), false, true, false, true, false, env.getWhiteList()));
                                        }
                                        env.addToExcludes(elem);
                                    }
                                }
                            } else if (bounds != null && !bounds.isEmpty()) {
                                TypeMirror lowerBound = (TypeMirror)bounds.get(0);
                                bounds = bounds.subList(0, bounds.size());
                                for (DeclaredType subtype : this.getSubtypesOf(env, (DeclaredType)lowerBound)) {
                                    TypeElement elem = (TypeElement)subtype.asElement();
                                    if (this.withinBounds(env, subtype, bounds) && (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(elem))) {
                                        this.results.add(this.javaCompletionItemFactory.createTypeItem((CompilationInfo)env.getController(), elem, subtype, this.anchorOffset, env.getReferencesCount(), elements.isDeprecated(elem), false, true, false, true, false, env.getWhiteList()));
                                    }
                                    env.addToExcludes(elem);
                                }
                            }
                        }
                        this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.TYPE_PARAMETER), null);
                        break;
                    }
                    case QUESTION: {
                        this.addKeyword(env, "extends", " ", false);
                        this.addKeyword(env, "super", " ", false);
                    }
                }
            }
        }

        private void insideBlock(Env env) throws IOException {
            StatementTree stat;
            int pos;
            int offset = env.getOffset();
            BlockTree bl = (BlockTree)env.getPath().getLeaf();
            SourcePositions sourcePositions = env.getSourcePositions();
            CompilationUnitTree root = env.getRoot();
            int blockPos = (int)sourcePositions.getStartPosition(root, (Tree)bl);
            String text = env.getController().getText().substring(blockPos, offset);
            if (text.indexOf(123) < 0) {
                this.addMemberModifiers(env, Collections.singleton(Modifier.STATIC), false);
                this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.TYPE_PARAMETER), null);
                return;
            }
            StatementTree last = null;
            Iterator i$ = bl.getStatements().iterator();
            while (i$.hasNext() && (long)(pos = (int)sourcePositions.getStartPosition(root, (Tree)(stat = (StatementTree)i$.next()))) != -1 && offset > pos) {
                last = stat;
            }
            if (last == null) {
                ExecutableElement enclMethod = env.getScope().getEnclosingMethod();
                if (enclMethod != null && enclMethod.getKind() == ElementKind.CONSTRUCTOR) {
                    String prefix = env.getPrefix();
                    if (Utilities.startsWith("this", prefix)) {
                        Element element = enclMethod.getEnclosingElement();
                        this.addThisOrSuperConstructor(env, element.asType(), element, "this", enclMethod);
                    }
                    if (Utilities.startsWith("super", prefix)) {
                        Element element = enclMethod.getEnclosingElement();
                        element = ((DeclaredType)((TypeElement)element).getSuperclass()).asElement();
                        this.addThisOrSuperConstructor(env, element.asType(), element, "super", enclMethod);
                    }
                }
            } else if (last.getKind() == Tree.Kind.TRY) {
                if (((TryTree)last).getFinallyBlock() == null) {
                    this.addKeyword(env, "catch", null, false);
                    this.addKeyword(env, "finally", null, false);
                    if (((TryTree)last).getCatches().isEmpty() && ((TryTree)last).getResources().isEmpty()) {
                        return;
                    }
                }
            } else if (last.getKind() == Tree.Kind.IF && ((IfTree)last).getElseStatement() == null) {
                this.addKeyword(env, "else", null, false);
            }
            this.localResult(env);
            this.addKeywordsForBlock(env);
        }

        /*
         * Enabled force condition propagation
         * Lifted jumps to return sites
         */
        private void insideMemberSelect(Env env) throws IOException {
            PackageElement pe;
            int offset = env.getOffset();
            String prefix = env.getPrefix();
            TreePath path = env.getPath();
            MemberSelectTree fa = (MemberSelectTree)path.getLeaf();
            CompilationController controller = env.getController();
            CompilationUnitTree root = env.getRoot();
            SourcePositions sourcePositions = env.getSourcePositions();
            int expEndPos = (int)sourcePositions.getEndPosition(root, (Tree)fa.getExpression());
            boolean afterDot = false;
            boolean afterLt = false;
            int openLtNum = 0;
            JavaTokenId lastNonWhitespaceTokenId = null;
            TokenSequence ts = controller.getTokenHierarchy().tokenSequence(JavaTokenId.language());
            ts.move(expEndPos);
            block19 : while (ts.moveNext() && ts.offset() < offset) {
                switch ((JavaTokenId)ts.token().id()) {
                    case DOUBLE_LITERAL: 
                    case FLOAT_LITERAL: 
                    case FLOAT_LITERAL_INVALID: 
                    case LONG_LITERAL: 
                    case ELLIPSIS: {
                        if (ts.offset() != expEndPos || ts.token().text().charAt(0) != '.') break;
                    }
                    case DOT: {
                        afterDot = true;
                        break;
                    }
                    case LT: {
                        afterLt = true;
                        ++openLtNum;
                        break;
                    }
                    case GT: {
                        --openLtNum;
                        break;
                    }
                    case GTGT: {
                        openLtNum -= 2;
                        break;
                    }
                    case GTGTGT: {
                        openLtNum -= 3;
                    }
                }
                switch ((JavaTokenId)ts.token().id()) {
                    case WHITESPACE: 
                    case LINE_COMMENT: 
                    case BLOCK_COMMENT: 
                    case JAVADOC_COMMENT: {
                        continue block19;
                    }
                }
                lastNonWhitespaceTokenId = (JavaTokenId)ts.token().id();
            }
            if (!afterDot) {
                if (expEndPos > offset) return;
                this.insideExpression(env, new TreePath(path, (Tree)fa.getExpression()));
                return;
            }
            if (openLtNum > 0) {
                switch (lastNonWhitespaceTokenId) {
                    case QUESTION: {
                        this.addKeyword(env, "extends", " ", false);
                        this.addKeyword(env, "super", " ", false);
                        return;
                    }
                    case EXTENDS: 
                    case COMMA: 
                    case SUPER: 
                    case LT: {
                        this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.TYPE_PARAMETER), null);
                    }
                }
                return;
            }
            if (lastNonWhitespaceTokenId == JavaTokenId.STAR) return;
            controller.toPhase(JavaSource.Phase.RESOLVED);
            TreePath parentPath = path.getParentPath();
            Tree parent = parentPath != null ? parentPath.getLeaf() : null;
            TreePath grandParentPath = parentPath != null ? parentPath.getParentPath() : null;
            Tree grandParent = grandParentPath != null ? grandParentPath.getLeaf() : null;
            ExpressionTree exp = fa.getExpression();
            TreePath expPath = new TreePath(path, (Tree)exp);
            TypeMirror type = controller.getTrees().getTypeMirror(expPath);
            if (type != null) {
                TypeElement te;
                EnumSet<ElementKind> kinds;
                DeclaredType baseType = null;
                Set exs = null;
                boolean inImport = false;
                boolean insideNew = false;
                if (TreeUtilities.CLASS_TREE_KINDS.contains((Object)parent.getKind()) && ((ClassTree)parent).getExtendsClause() == fa) {
                    kinds = EnumSet.of(ElementKind.CLASS);
                    env.afterExtends();
                } else if (TreeUtilities.CLASS_TREE_KINDS.contains((Object)parent.getKind()) && ((ClassTree)parent).getImplementsClause().contains((Object)fa)) {
                    kinds = EnumSet.of(ElementKind.INTERFACE);
                } else if (parent.getKind() == Tree.Kind.IMPORT) {
                    inImport = true;
                    kinds = ((ImportTree)parent).isStatic() ? EnumSet.of(ElementKind.CLASS, new ElementKind[]{ElementKind.ENUM, ElementKind.INTERFACE, ElementKind.ANNOTATION_TYPE, ElementKind.FIELD, ElementKind.METHOD, ElementKind.ENUM_CONSTANT}) : EnumSet.of(ElementKind.CLASS, ElementKind.ANNOTATION_TYPE, ElementKind.ENUM, ElementKind.INTERFACE);
                } else if (parent.getKind() == Tree.Kind.NEW_CLASS && ((NewClassTree)parent).getIdentifier() == fa) {
                    insideNew = true;
                    kinds = EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ANNOTATION_TYPE);
                    if (grandParent.getKind() == Tree.Kind.THROW && (te = controller.getElements().getTypeElement("java.lang.Throwable")) != null) {
                        baseType = controller.getTypes().getDeclaredType(te, new TypeMirror[0]);
                    }
                } else if (parent.getKind() == Tree.Kind.PARAMETERIZED_TYPE && ((ParameterizedTypeTree)parent).getTypeArguments().contains((Object)fa)) {
                    kinds = EnumSet.of(ElementKind.CLASS, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.INTERFACE);
                } else if (parent.getKind() == Tree.Kind.ANNOTATION || parent.getKind() == Tree.Kind.TYPE_ANNOTATION) {
                    if (((AnnotationTree)parent).getAnnotationType() == fa) {
                        kinds = EnumSet.of(ElementKind.ANNOTATION_TYPE);
                    } else {
                        ExpressionTree et;
                        Iterator it = ((AnnotationTree)parent).getArguments().iterator();
                        if (it.hasNext() && ((et = (ExpressionTree)it.next()) == fa || et.getKind() == Tree.Kind.ASSIGNMENT && ((AssignmentTree)et).getExpression() == fa)) {
                            void el22;
                            Element el22 = controller.getTrees().getElement(expPath);
                            if (type.getKind() == TypeKind.ERROR && el22.getKind().isClass()) {
                                PackageElement el22 = controller.getElements().getPackageElement(((TypeElement)el22).getQualifiedName());
                            }
                            if (el22 instanceof PackageElement) {
                                this.addPackageContent(env, (PackageElement)el22, EnumSet.of(ElementKind.CLASS, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.INTERFACE), null, false, false);
                                return;
                            } else {
                                if (type.getKind() != TypeKind.DECLARED) return;
                                this.addMemberConstantsAndTypes(env, (DeclaredType)type, (Element)el22);
                            }
                            return;
                        }
                        kinds = EnumSet.of(ElementKind.CLASS, new ElementKind[]{ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.INTERFACE, ElementKind.FIELD, ElementKind.METHOD, ElementKind.ENUM_CONSTANT});
                    }
                } else {
                    Trees trees;
                    if (parent.getKind() == Tree.Kind.ASSIGNMENT && ((AssignmentTree)parent).getExpression() == fa && grandParent != null && grandParent.getKind() == Tree.Kind.ANNOTATION) {
                        Element el = controller.getTrees().getElement(expPath);
                        if (type.getKind() == TypeKind.ERROR && el.getKind().isClass()) {
                            el = controller.getElements().getPackageElement(((TypeElement)el).getQualifiedName());
                        }
                        if (el instanceof PackageElement) {
                            this.addPackageContent(env, (PackageElement)el, EnumSet.of(ElementKind.CLASS, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.INTERFACE), null, false, false);
                            return;
                        } else {
                            if (type.getKind() != TypeKind.DECLARED) return;
                            this.addMemberConstantsAndTypes(env, (DeclaredType)type, el);
                        }
                        return;
                    }
                    if (parent.getKind() == Tree.Kind.VARIABLE && ((VariableTree)parent).getType() == fa) {
                        if (grandParent.getKind() == Tree.Kind.CATCH) {
                            kinds = EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE);
                            if (this.queryType == 1) {
                                exs = controller.getTreeUtilities().getUncaughtExceptions(grandParentPath.getParentPath());
                            }
                            if ((te = controller.getElements().getTypeElement("java.lang.Throwable")) != null) {
                                baseType = controller.getTypes().getDeclaredType(te, new TypeMirror[0]);
                            }
                        } else {
                            kinds = EnumSet.of(ElementKind.CLASS, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.INTERFACE);
                        }
                    } else if (parent.getKind() == Tree.Kind.METHOD && ((MethodTree)parent).getThrows().contains((Object)fa)) {
                        Types types = controller.getTypes();
                        if (this.queryType == 1 && ((MethodTree)parent).getBody() != null) {
                            ExpressionTree thr;
                            controller.toPhase(JavaSource.Phase.RESOLVED);
                            exs = controller.getTreeUtilities().getUncaughtExceptions(new TreePath(path, (Tree)((MethodTree)parent).getBody()));
                            trees = controller.getTrees();
                            Iterator i$ = ((MethodTree)parent).getThrows().iterator();
                            while (i$.hasNext() && sourcePositions.getEndPosition(root, (Tree)(thr = (ExpressionTree)i$.next())) < (long)offset) {
                                TypeMirror t = trees.getTypeMirror(new TreePath(path, (Tree)thr));
                                Iterator it = exs.iterator();
                                while (it.hasNext()) {
                                    if (!types.isSubtype((TypeMirror)it.next(), t)) continue;
                                    it.remove();
                                }
                            }
                        }
                        kinds = EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE);
                        TypeElement te2 = controller.getElements().getTypeElement("java.lang.Throwable");
                        if (te2 != null) {
                            baseType = controller.getTypes().getDeclaredType(te2, new TypeMirror[0]);
                        }
                    } else {
                        if (parent.getKind() == Tree.Kind.METHOD && ((MethodTree)parent).getDefaultValue() == fa) {
                            Element el = controller.getTrees().getElement(expPath);
                            if (type.getKind() == TypeKind.ERROR && el.getKind().isClass()) {
                                el = controller.getElements().getPackageElement(((TypeElement)el).getQualifiedName());
                            }
                            if (el instanceof PackageElement) {
                                this.addPackageContent(env, (PackageElement)el, EnumSet.of(ElementKind.CLASS, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.INTERFACE), null, false, false);
                                return;
                            } else {
                                if (type.getKind() != TypeKind.DECLARED) return;
                                this.addMemberConstantsAndTypes(env, (DeclaredType)type, el);
                            }
                            return;
                        }
                        if (parent.getKind() == Tree.Kind.TYPE_PARAMETER) {
                            Tree bound;
                            int pos;
                            TypeParameterTree tpt = (TypeParameterTree)parent;
                            trees = controller.getTrees();
                            boolean first = true;
                            Iterator i$ = tpt.getBounds().iterator();
                            while (i$.hasNext() && offset > (pos = (int)sourcePositions.getEndPosition(root, bound = (Tree)i$.next()))) {
                                first = false;
                                env.addToExcludes(trees.getElement(new TreePath(parentPath, bound)));
                            }
                            kinds = first ? EnumSet.of(ElementKind.CLASS, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.INTERFACE) : EnumSet.of(ElementKind.ANNOTATION_TYPE, ElementKind.INTERFACE);
                        } else if (parent.getKind() == Tree.Kind.AND) {
                            TypeMirror tm = controller.getTrees().getTypeMirror(new TreePath(path, (Tree)((BinaryTree)parent).getLeftOperand()));
                            if (tm != null && tm.getKind() == TypeKind.DECLARED) {
                                env.addToExcludes(((DeclaredType)tm).asElement());
                                kinds = EnumSet.of(ElementKind.INTERFACE, ElementKind.ANNOTATION_TYPE);
                            } else if (tm != null && tm.getKind() == TypeKind.INTERSECTION) {
                                for (TypeMirror bound : ((IntersectionType)tm).getBounds()) {
                                    if (bound.getKind() != TypeKind.DECLARED) continue;
                                    env.addToExcludes(((DeclaredType)bound).asElement());
                                }
                                kinds = EnumSet.of(ElementKind.INTERFACE, ElementKind.ANNOTATION_TYPE);
                            } else {
                                kinds = EnumSet.of(ElementKind.CLASS, new ElementKind[]{ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.INTERFACE, ElementKind.FIELD, ElementKind.METHOD, ElementKind.ENUM_CONSTANT});
                            }
                        } else if (afterLt) {
                            kinds = EnumSet.of(ElementKind.METHOD);
                        } else if (parent.getKind() == Tree.Kind.ENHANCED_FOR_LOOP && ((EnhancedForLoopTree)parent).getExpression() == fa) {
                            env.insideForEachExpression();
                            kinds = EnumSet.of(ElementKind.CLASS, new ElementKind[]{ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.INTERFACE, ElementKind.FIELD, ElementKind.METHOD, ElementKind.ENUM_CONSTANT});
                        } else {
                            kinds = EnumSet.of(ElementKind.CLASS, new ElementKind[]{ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.INTERFACE, ElementKind.FIELD, ElementKind.METHOD, ElementKind.ENUM_CONSTANT});
                        }
                    }
                }
                switch (type.getKind()) {
                    TypeMirror tm;
                    Element el;
                    case TYPEVAR: {
                        while (type != null && type.getKind() == TypeKind.TYPEVAR) {
                            type = ((TypeVariable)type).getUpperBound();
                        }
                        if (type == null) {
                            return;
                        }
                        type = controller.getTypes().capture(type);
                    }
                    case ARRAY: 
                    case DECLARED: 
                    case UNION: 
                    case BOOLEAN: 
                    case BYTE: 
                    case CHAR: 
                    case DOUBLE: 
                    case FLOAT: 
                    case INT: 
                    case LONG: 
                    case SHORT: 
                    case VOID: {
                        boolean b;
                        boolean bl = b = exp.getKind() == Tree.Kind.PARENTHESIZED || exp.getKind() == Tree.Kind.TYPE_CAST;
                        while (b) {
                            if (exp.getKind() == Tree.Kind.PARENTHESIZED) {
                                exp = ((ParenthesizedTree)exp).getExpression();
                                expPath = new TreePath(expPath, (Tree)exp);
                                continue;
                            }
                            if (exp.getKind() == Tree.Kind.TYPE_CAST) {
                                exp = ((TypeCastTree)exp).getExpression();
                                expPath = new TreePath(expPath, (Tree)exp);
                                continue;
                            }
                            b = false;
                        }
                        el = controller.getTrees().getElement(expPath);
                        if (el != null && (el.getKind().isClass() || el.getKind().isInterface()) && parent.getKind() == Tree.Kind.NEW_CLASS && ((NewClassTree)parent).getIdentifier() == fa && prefix != null) {
                            String typeName = Utilities.getElementName(el, true) + "." + prefix;
                            tm = controller.getTreeUtilities().parseType(typeName, env.getScope().getEnclosingClass());
                            if (tm != null && tm.getKind() == TypeKind.DECLARED) {
                                this.addMembers(env, tm, ((DeclaredType)tm).asElement(), EnumSet.of(ElementKind.CONSTRUCTOR), null, inImport, insideNew, false);
                            }
                        }
                        if (exs != null && !exs.isEmpty()) {
                            Elements elements = controller.getElements();
                            for (TypeMirror ex : exs) {
                                Element e;
                                if (ex.getKind() != TypeKind.DECLARED || (e = ((DeclaredType)ex).asElement()).getEnclosingElement() != el || !this.startsWith(env, e.getSimpleName().toString()) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(e)) continue;
                                env.addToExcludes(e);
                                this.results.add(this.javaCompletionItemFactory.createTypeItem((CompilationInfo)env.getController(), (TypeElement)e, (DeclaredType)ex, this.anchorOffset, null, elements.isDeprecated(e), insideNew, insideNew || env.isInsideClass(), true, true, false, env.getWhiteList()));
                            }
                            return;
                        }
                        if (el == null) {
                            if (exp.getKind() == Tree.Kind.ARRAY_TYPE) {
                                TypeMirror tm2 = type;
                                while (tm2.getKind() == TypeKind.ARRAY) {
                                    tm2 = ((ArrayType)tm2).getComponentType();
                                }
                                if (tm2.getKind().isPrimitive()) {
                                    el = controller.getTypes().boxedClass((PrimitiveType)tm2);
                                } else if (tm2.getKind() == TypeKind.DECLARED) {
                                    el = ((DeclaredType)tm2).asElement();
                                }
                            } else if (exp.getKind() == Tree.Kind.PRIMITIVE_TYPE) {
                                el = controller.getTypes().boxedClass((PrimitiveType)type);
                            }
                        }
                        this.addMembers(env, type, el, kinds, baseType, inImport, insideNew, false);
                        return;
                    }
                    default: {
                        el = controller.getTrees().getElement(expPath);
                        if (type.getKind() == TypeKind.ERROR && el != null && el.getKind().isClass()) {
                            el = controller.getElements().getPackageElement(((TypeElement)el).getQualifiedName());
                        }
                        if (el == null || el.getKind() != ElementKind.PACKAGE) return;
                        if (parent.getKind() == Tree.Kind.NEW_CLASS && ((NewClassTree)parent).getIdentifier() == fa && prefix != null) {
                            String typeName = Utilities.getElementName(el, true) + "." + prefix;
                            tm = controller.getTreeUtilities().parseType(typeName, env.getScope().getEnclosingClass());
                            if (tm != null && tm.getKind() == TypeKind.DECLARED) {
                                this.addMembers(env, tm, ((DeclaredType)tm).asElement(), EnumSet.of(ElementKind.CONSTRUCTOR), null, inImport, insideNew, false);
                            }
                        }
                        if (exs != null && !exs.isEmpty()) {
                            Elements elements = controller.getElements();
                            for (TypeMirror ex : exs) {
                                Element e;
                                if (ex.getKind() != TypeKind.DECLARED || (e = ((DeclaredType)ex).asElement()).getEnclosingElement() != el || !this.startsWith(env, e.getSimpleName().toString()) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(e)) continue;
                                env.addToExcludes(e);
                                this.results.add(this.javaCompletionItemFactory.createTypeItem((CompilationInfo)env.getController(), (TypeElement)e, (DeclaredType)ex, this.anchorOffset, env.getReferencesCount(), elements.isDeprecated(e), false, env.isInsideClass(), true, true, false, env.getWhiteList()));
                            }
                        }
                        this.addPackageContent(env, (PackageElement)el, kinds, baseType, insideNew, false);
                        if (!this.results.isEmpty() || ((PackageElement)el).getQualifiedName() != el.getSimpleName()) return;
                        ClassIndex ci = controller.getClasspathInfo().getClassIndex();
                        if (!el.getEnclosedElements().isEmpty() || !ci.getPackageNames(el.getSimpleName() + ".", true, EnumSet.allOf(ClassIndex.SearchScope.class)).isEmpty()) return;
                        Trees trees = controller.getTrees();
                        Scope scope = env.getScope();
                        for (ElementHandle teHandle : ci.getDeclaredTypes(el.getSimpleName().toString(), ClassIndex.NameKind.SIMPLE_NAME, EnumSet.allOf(ClassIndex.SearchScope.class))) {
                            TypeElement te3 = (TypeElement)teHandle.resolve((CompilationInfo)controller);
                            if (te3 == null || !trees.isAccessible(scope, te3)) continue;
                            this.addMembers(env, te3.asType(), te3, kinds, baseType, inImport, insideNew, true);
                        }
                        return;
                    }
                }
            }
            if (parent.getKind() != Tree.Kind.COMPILATION_UNIT || ((CompilationUnitTree)parent).getPackageName() != fa || (pe = controller.getElements().getPackageElement(this.fullName((Tree)exp))) == null) return;
            this.addPackageContent(env, pe, EnumSet.of(ElementKind.PACKAGE), null, false, true);
        }

        private void insideMemberReference(Env env) throws IOException {
            TreePath path = env.getPath();
            MemberReferenceTree mr = (MemberReferenceTree)path.getLeaf();
            TokenSequence<JavaTokenId> ts = this.findLastNonWhitespaceToken(env, (Tree)mr, env.getOffset());
            if (ts != null) {
                switch ((JavaTokenId)ts.token().id()) {
                    case GT: 
                    case GTGT: 
                    case GTGTGT: 
                    case COLONCOLON: {
                        CompilationController controller = env.getController();
                        ExpressionTree exp = mr.getQualifierExpression();
                        TreePath expPath = new TreePath(path, (Tree)exp);
                        Trees trees = controller.getTrees();
                        TypeMirror type = trees.getTypeMirror(expPath);
                        if (type != null && type.getKind() == TypeKind.TYPEVAR) {
                            while (type != null && type.getKind() == TypeKind.TYPEVAR) {
                                type = ((TypeVariable)type).getUpperBound();
                            }
                            if (type != null) {
                                type = controller.getTypes().capture(type);
                            }
                        }
                        if (type == null || type.getKind() != TypeKind.DECLARED && type.getKind() != TypeKind.ARRAY && type.getKind() != TypeKind.TYPEVAR) break;
                        Element e = trees.getElement(expPath);
                        this.addMethodReferences(env, type, e);
                        if (e != null && !e.getKind().isClass() && !e.getKind().isInterface()) break;
                        this.addKeyword(env, "new", " ", false);
                        break;
                    }
                    case COMMA: 
                    case LT: {
                        this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.TYPE_PARAMETER), null);
                    }
                }
            }
        }

        private void insideLambdaExpression(Env env) throws IOException {
            TreePath path = env.getPath();
            LambdaExpressionTree let = (LambdaExpressionTree)path.getLeaf();
            TokenSequence<JavaTokenId> ts = this.findLastNonWhitespaceToken(env, (Tree)let, env.getOffset());
            if (ts != null) {
                switch ((JavaTokenId)ts.token().id()) {
                    case ARROW: {
                        this.localResult(env);
                        this.addValueKeywords(env);
                        break;
                    }
                    case COMMA: {
                        if (!let.getParameters().isEmpty() && env.getController().getTrees().getSourcePositions().getStartPosition(path.getCompilationUnit(), ((VariableTree)let.getParameters().get(0)).getType()) < 0) break;
                        this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.TYPE_PARAMETER), null);
                        this.addPrimitiveTypeKeywords(env);
                        this.addKeyword(env, "final", " ", false);
                    }
                }
            }
        }

        private void insideMethodInvocation(Env env) throws IOException {
            int offset;
            TreePath path = env.getPath();
            MethodInvocationTree mi = (MethodInvocationTree)path.getLeaf();
            TokenSequence<JavaTokenId> ts = this.findLastNonWhitespaceToken(env, (Tree)mi, offset = env.getOffset());
            if (ts == null || ts.token().id() != JavaTokenId.LPAREN && ts.token().id() != JavaTokenId.COMMA) {
                SourcePositions sp = env.getSourcePositions();
                CompilationUnitTree root = env.getRoot();
                int lastTokenEndOffset = ts.offset() + ts.token().length();
                for (ExpressionTree arg : mi.getArguments()) {
                    int pos = (int)sp.getEndPosition(root, (Tree)arg);
                    if (lastTokenEndOffset == pos) {
                        this.insideExpression(env, new TreePath(path, (Tree)arg));
                        break;
                    }
                    if (offset > pos) continue;
                    break;
                }
                return;
            }
            String prefix = env.getPrefix();
            if (prefix == null || prefix.length() == 0) {
                this.addMethodArguments(env, mi);
            }
            this.addLocalMembersAndVars(env);
            this.addValueKeywords(env);
            this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.TYPE_PARAMETER), null);
            this.addPrimitiveTypeKeywords(env);
        }

        private void insideNewClass(Env env) throws IOException {
            TreePath path = env.getPath();
            NewClassTree nc = (NewClassTree)path.getLeaf();
            TokenSequence<JavaTokenId> ts = this.findLastNonWhitespaceToken(env, (Tree)nc, env.getOffset());
            if (ts != null) {
                switch ((JavaTokenId)ts.token().id()) {
                    case NEW: {
                        Set<? extends TypeMirror> smarts;
                        TypeMirror tm;
                        String prefix = env.getPrefix();
                        CompilationController controller = env.getController();
                        controller.toPhase(JavaSource.Phase.RESOLVED);
                        TypeElement tel = controller.getElements().getTypeElement("java.lang.Throwable");
                        DeclaredType base = path.getParentPath().getLeaf().getKind() == Tree.Kind.THROW && tel != null ? controller.getTypes().getDeclaredType(tel, new TypeMirror[0]) : null;
                        TypeElement toExclude = null;
                        if (nc.getIdentifier().getKind() == Tree.Kind.IDENTIFIER && prefix != null && (tm = controller.getTreeUtilities().parseType(prefix, env.getScope().getEnclosingClass())) != null && tm.getKind() == TypeKind.DECLARED) {
                            TypeElement te = (TypeElement)((DeclaredType)tm).asElement();
                            this.addMembers(env, tm, te, EnumSet.of(ElementKind.CONSTRUCTOR), base, false, true, false);
                            if ((te.getTypeParameters().isEmpty() || SourceVersion.RELEASE_5.compareTo(controller.getSourceVersion()) > 0) && !Utilities.hasAccessibleInnerClassConstructor(te, env.getScope(), controller.getTrees())) {
                                toExclude = te;
                            }
                        }
                        boolean insideNew = true;
                        ExpressionTree encl = nc.getEnclosingExpression();
                        if (this.queryType == 1 && (smarts = env.getSmartTypes()) != null) {
                            Elements elements = env.getController().getElements();
                            for (TypeMirror smart : smarts) {
                                if (smart == null) continue;
                                if (smart.getKind() == TypeKind.DECLARED) {
                                    if (encl != null) continue;
                                    for (DeclaredType subtype : this.getSubtypesOf(env, (DeclaredType)smart)) {
                                        TypeElement elem = (TypeElement)subtype.asElement();
                                        if (toExclude != elem && (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(elem))) {
                                            this.results.add(this.javaCompletionItemFactory.createTypeItem((CompilationInfo)env.getController(), elem, (DeclaredType)Utilities.resolveCapturedType((CompilationInfo)controller, subtype), this.anchorOffset, env.getReferencesCount(), elements.isDeprecated(elem), true, true, false, true, false, env.getWhiteList()));
                                        }
                                        env.addToExcludes(elem);
                                    }
                                    continue;
                                }
                                if (smart.getKind() != TypeKind.ARRAY) continue;
                                insideNew = false;
                                try {
                                    TypeMirror tm2 = smart;
                                    while (tm2.getKind() == TypeKind.ARRAY) {
                                        tm2 = ((ArrayType)tm2).getComponentType();
                                    }
                                    if (tm2.getKind().isPrimitive() && this.startsWith(env, tm2.toString())) {
                                        this.results.add(this.javaCompletionItemFactory.createArrayItem((CompilationInfo)env.getController(), (ArrayType)smart, this.anchorOffset, env.getReferencesCount(), env.getController().getElements(), env.getWhiteList()));
                                        continue;
                                    }
                                    if (tm2.getKind() != TypeKind.DECLARED && tm2.getKind() != TypeKind.ERROR || !this.startsWith(env, ((DeclaredType)tm2).asElement().getSimpleName().toString())) continue;
                                    this.results.add(this.javaCompletionItemFactory.createArrayItem((CompilationInfo)env.getController(), (ArrayType)smart, this.anchorOffset, env.getReferencesCount(), env.getController().getElements(), env.getWhiteList()));
                                }
                                catch (IllegalArgumentException iae) {
                                }
                            }
                        }
                        if (toExclude != null) {
                            env.addToExcludes(toExclude);
                        }
                        if (insideNew) {
                            env.insideNew();
                        }
                        if (encl == null) {
                            this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE), base);
                            break;
                        }
                        TypeMirror enclType = controller.getTrees().getTypeMirror(new TreePath(path, (Tree)nc.getEnclosingExpression()));
                        if (enclType == null || enclType.getKind() != TypeKind.DECLARED) break;
                        this.addMembers(env, enclType, ((DeclaredType)enclType).asElement(), EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE), base, false, insideNew, false);
                        break;
                    }
                    case LPAREN: 
                    case COMMA: {
                        String prefix = env.getPrefix();
                        if (prefix == null || prefix.length() == 0) {
                            this.addConstructorArguments(env, nc);
                        }
                        this.addLocalMembersAndVars(env);
                        this.addValueKeywords(env);
                        this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.TYPE_PARAMETER), null);
                        this.addPrimitiveTypeKeywords(env);
                        break;
                    }
                    case GT: 
                    case GTGT: 
                    case GTGTGT: {
                        CompilationController controller = env.getController();
                        TypeMirror tm = controller.getTrees().getTypeMirror(new TreePath(path, (Tree)nc.getIdentifier()));
                        this.addMembers(env, tm, ((DeclaredType)tm).asElement(), EnumSet.of(ElementKind.CONSTRUCTOR), null, false, false, false);
                    }
                }
            }
        }

        private void insideTry(Env env) throws IOException {
            CompilationController controller = env.getController();
            this.addKeyword(env, "final", " ", false);
            TypeElement te = controller.getElements().getTypeElement("java.lang.AutoCloseable");
            if (te != null) {
                this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.TYPE_PARAMETER), controller.getTypes().getDeclaredType(te, new TypeMirror[0]));
            }
        }

        private void insideCatch(Env env) throws IOException {
            TreePath path = env.getPath();
            CatchTree ct = (CatchTree)path.getLeaf();
            CompilationController controller = env.getController();
            TokenSequence<JavaTokenId> last = this.findLastNonWhitespaceToken(env, (Tree)ct, env.getOffset());
            if (last != null && last.token().id() == JavaTokenId.LPAREN) {
                TypeElement te;
                this.addKeyword(env, "final", " ", false);
                if (this.queryType == 1) {
                    TreePath tryPath = Utilities.getPathElementOfKind(Tree.Kind.TRY, path);
                    Set exs = controller.getTreeUtilities().getUncaughtExceptions(tryPath);
                    Elements elements = controller.getElements();
                    for (TypeMirror ex : exs) {
                        if (ex.getKind() != TypeKind.DECLARED || !this.startsWith(env, ((DeclaredType)ex).asElement().getSimpleName().toString()) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(((DeclaredType)ex).asElement())) continue;
                        env.addToExcludes(((DeclaredType)ex).asElement());
                        this.results.add(this.javaCompletionItemFactory.createTypeItem((CompilationInfo)env.getController(), (TypeElement)((DeclaredType)ex).asElement(), (DeclaredType)ex, this.anchorOffset, env.getReferencesCount(), elements.isDeprecated(((DeclaredType)ex).asElement()), false, false, false, true, false, env.getWhiteList()));
                    }
                }
                if ((te = controller.getElements().getTypeElement("java.lang.Throwable")) != null) {
                    this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.TYPE_PARAMETER), controller.getTypes().getDeclaredType(te, new TypeMirror[0]));
                }
            }
        }

        private void insideUnionType(Env env) throws IOException {
            TreePath path = env.getPath();
            UnionTypeTree dtt = (UnionTypeTree)path.getLeaf();
            CompilationController controller = env.getController();
            TokenSequence<JavaTokenId> last = this.findLastNonWhitespaceToken(env, (Tree)dtt, env.getOffset());
            if (last != null && last.token().id() == JavaTokenId.BAR) {
                TypeElement te;
                if (this.queryType == 1) {
                    TreePath tryPath = Utilities.getPathElementOfKind(Tree.Kind.TRY, path);
                    Set exs = controller.getTreeUtilities().getUncaughtExceptions(tryPath);
                    if (!exs.isEmpty()) {
                        Trees trees = controller.getTrees();
                        Types types = controller.getTypes();
                        for (Tree t : dtt.getTypeAlternatives()) {
                            TypeMirror tm = trees.getTypeMirror(new TreePath(path, t));
                            if (tm == null || tm.getKind() == TypeKind.ERROR) continue;
                            Iterator it = exs.iterator();
                            while (it.hasNext()) {
                                if (!types.isSubtype(tm, (TypeMirror)it.next())) continue;
                                it.remove();
                            }
                        }
                        Elements elements = controller.getElements();
                        for (TypeMirror ex : exs) {
                            if (ex.getKind() != TypeKind.DECLARED || !this.startsWith(env, ((DeclaredType)ex).asElement().getSimpleName().toString()) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(((DeclaredType)ex).asElement())) continue;
                            env.addToExcludes(((DeclaredType)ex).asElement());
                            this.results.add(this.javaCompletionItemFactory.createTypeItem((CompilationInfo)env.getController(), (TypeElement)((DeclaredType)ex).asElement(), (DeclaredType)ex, this.anchorOffset, env.getReferencesCount(), elements.isDeprecated(((DeclaredType)ex).asElement()), false, false, false, true, false, env.getWhiteList()));
                        }
                    }
                }
                if ((te = controller.getElements().getTypeElement("java.lang.Throwable")) != null) {
                    this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.TYPE_PARAMETER), controller.getTypes().getDeclaredType(te, new TypeMirror[0]));
                }
            }
        }

        private void insideIf(Env env) throws IOException {
            TokenSequence<JavaTokenId> last;
            IfTree iff = (IfTree)env.getPath().getLeaf();
            if (env.getSourcePositions().getEndPosition(env.getRoot(), (Tree)iff.getCondition()) <= (long)env.getOffset() && (last = this.findLastNonWhitespaceToken(env, (Tree)iff, env.getOffset())) != null && (last.token().id() == JavaTokenId.RPAREN || last.token().id() == JavaTokenId.ELSE)) {
                this.localResult(env);
                this.addKeywordsForStatement(env);
            }
        }

        private void insideWhile(Env env) throws IOException {
            TokenSequence<JavaTokenId> last;
            WhileLoopTree wlt = (WhileLoopTree)env.getPath().getLeaf();
            if (env.getSourcePositions().getEndPosition(env.getRoot(), (Tree)wlt.getCondition()) <= (long)env.getOffset() && (last = this.findLastNonWhitespaceToken(env, (Tree)wlt, env.getOffset())) != null && last.token().id() == JavaTokenId.RPAREN) {
                this.localResult(env);
                this.addKeywordsForStatement(env);
            }
        }

        private void insideDoWhile(Env env) throws IOException {
            TokenSequence<JavaTokenId> last;
            DoWhileLoopTree dwlt = (DoWhileLoopTree)env.getPath().getLeaf();
            if (env.getSourcePositions().getEndPosition(env.getRoot(), (Tree)dwlt.getStatement()) <= (long)env.getOffset() && (last = this.findLastNonWhitespaceToken(env, (Tree)dwlt, env.getOffset())) != null && (last.token().id() == JavaTokenId.RBRACE || last.token().id() == JavaTokenId.SEMICOLON)) {
                this.addKeyword(env, "while", null, false);
            }
        }

        private void insideFor(Env env) throws IOException {
            Tree update;
            int pos;
            TokenSequence<JavaTokenId> last;
            int pos2;
            int offset = env.getOffset();
            TreePath path = env.getPath();
            ForLoopTree fl = (ForLoopTree)path.getLeaf();
            SourcePositions sourcePositions = env.getSourcePositions();
            CompilationUnitTree root = env.getRoot();
            Tree lastTree = null;
            int lastTreePos = offset;
            Iterator i$ = fl.getUpdate().iterator();
            while (i$.hasNext() && (long)(pos2 = (int)sourcePositions.getEndPosition(root, update = (Tree)i$.next())) != -1 && offset > pos2) {
                lastTree = update;
                lastTreePos = pos2;
            }
            if (lastTree == null && (long)(pos = (int)sourcePositions.getEndPosition(root, (Tree)fl.getCondition())) != -1 && pos < offset) {
                lastTree = fl.getCondition();
                lastTreePos = pos;
            }
            if (lastTree == null) {
                Tree init;
                i$ = fl.getInitializer().iterator();
                while (i$.hasNext() && (long)(pos2 = (int)sourcePositions.getEndPosition(root, init = (Tree)i$.next())) != -1 && offset > pos2) {
                    lastTree = init;
                    lastTreePos = pos2;
                }
            }
            if (lastTree == null) {
                last = this.findLastNonWhitespaceToken(env, (Tree)fl, offset);
                if (last != null && last.token().id() == JavaTokenId.LPAREN) {
                    this.addLocalFieldsAndVars(env);
                    this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.TYPE_PARAMETER), null);
                    this.addPrimitiveTypeKeywords(env);
                }
            } else {
                last = this.findLastNonWhitespaceToken(env, lastTreePos, offset);
                if (last != null && last.token().id() == JavaTokenId.SEMICOLON) {
                    this.localResult(env);
                    this.addValueKeywords(env);
                } else if (last != null && last.token().id() == JavaTokenId.RPAREN) {
                    this.localResult(env);
                    this.addKeywordsForStatement(env);
                } else {
                    switch (lastTree.getKind()) {
                        case VARIABLE: {
                            ExpressionTree var = ((VariableTree)lastTree).getInitializer();
                            if (var == null) break;
                            this.insideExpression(env, new TreePath(new TreePath(path, lastTree), (Tree)var));
                            break;
                        }
                        case EXPRESSION_STATEMENT: {
                            Tree exp = this.unwrapErrTree((Tree)((ExpressionStatementTree)lastTree).getExpression());
                            if (exp == null) break;
                            this.insideExpression(env, new TreePath(new TreePath(path, lastTree), exp));
                            break;
                        }
                        default: {
                            this.insideExpression(env, new TreePath(path, lastTree));
                        }
                    }
                }
            }
        }

        private void insideForEach(Env env) throws IOException {
            CompilationUnitTree root;
            int offset = env.getOffset();
            TreePath path = env.getPath();
            EnhancedForLoopTree efl = (EnhancedForLoopTree)path.getLeaf();
            SourcePositions sourcePositions = env.getSourcePositions();
            if (sourcePositions.getStartPosition(root = env.getRoot(), (Tree)efl.getExpression()) >= (long)offset) {
                TokenSequence<JavaTokenId> last = this.findLastNonWhitespaceToken(env, (int)sourcePositions.getEndPosition(root, (Tree)efl.getVariable()), offset);
                if (last != null && last.token().id() == JavaTokenId.COLON) {
                    env.insideForEachExpression();
                    this.addKeyword(env, "new", " ", false);
                    this.localResult(env);
                }
                return;
            }
            TokenSequence<JavaTokenId> last = this.findLastNonWhitespaceToken(env, (int)sourcePositions.getEndPosition(root, (Tree)efl.getExpression()), offset);
            if (last != null && last.token().id() == JavaTokenId.RPAREN) {
                this.addKeywordsForStatement(env);
            } else {
                env.insideForEachExpression();
                this.addKeyword(env, "new", " ", false);
            }
            this.localResult(env);
        }

        private void insideSwitch(Env env) throws IOException {
            CompilationUnitTree root;
            int offset = env.getOffset();
            TreePath path = env.getPath();
            SwitchTree st = (SwitchTree)path.getLeaf();
            SourcePositions sourcePositions = env.getSourcePositions();
            if (sourcePositions.getStartPosition(root = env.getRoot(), (Tree)st.getExpression()) < (long)offset) {
                CaseTree t;
                int pos;
                CaseTree lastCase = null;
                Iterator i$ = st.getCases().iterator();
                while (i$.hasNext() && (long)(pos = (int)sourcePositions.getStartPosition(root, (Tree)(t = (CaseTree)i$.next()))) != -1 && offset > pos) {
                    lastCase = t;
                }
                if (lastCase != null) {
                    int pos2;
                    StatementTree stat;
                    StatementTree last = null;
                    Iterator i$2 = lastCase.getStatements().iterator();
                    while (i$2.hasNext() && (long)(pos2 = (int)sourcePositions.getStartPosition(root, (Tree)(stat = (StatementTree)i$2.next()))) != -1 && offset > pos2) {
                        last = stat;
                    }
                    if (last != null) {
                        if (last.getKind() == Tree.Kind.TRY) {
                            if (((TryTree)last).getFinallyBlock() == null) {
                                this.addKeyword(env, "catch", null, false);
                                this.addKeyword(env, "finally", null, false);
                                if (((TryTree)last).getCatches().size() == 0) {
                                    return;
                                }
                            }
                        } else if (last.getKind() == Tree.Kind.IF && ((IfTree)last).getElseStatement() == null) {
                            this.addKeyword(env, "else", null, false);
                        }
                    }
                    this.localResult(env);
                    this.addKeywordsForBlock(env);
                } else {
                    TokenSequence<JavaTokenId> ts = this.findLastNonWhitespaceToken(env, (Tree)st, offset);
                    if (ts != null && ts.token().id() == JavaTokenId.LBRACE) {
                        this.addKeyword(env, "case", " ", false);
                        this.addKeyword(env, "default", ":", false);
                    }
                }
            }
        }

        private void insideCase(Env env) throws IOException {
            int offset = env.getOffset();
            TreePath path = env.getPath();
            CaseTree cst = (CaseTree)path.getLeaf();
            SourcePositions sourcePositions = env.getSourcePositions();
            CompilationUnitTree root = env.getRoot();
            CompilationController controller = env.getController();
            if (cst.getExpression() != null && (sourcePositions.getStartPosition(root, (Tree)cst.getExpression()) >= (long)offset || cst.getExpression().getKind() == Tree.Kind.ERRONEOUS && ((ErroneousTree)cst.getExpression()).getErrorTrees().isEmpty() && sourcePositions.getEndPosition(root, (Tree)cst.getExpression()) >= (long)offset)) {
                TreePath path1 = path.getParentPath();
                if (path1.getLeaf().getKind() == Tree.Kind.SWITCH) {
                    TypeMirror tm = controller.getTrees().getTypeMirror(new TreePath(path1, (Tree)((SwitchTree)path1.getLeaf()).getExpression()));
                    if (tm.getKind() == TypeKind.DECLARED && ((DeclaredType)tm).asElement().getKind() == ElementKind.ENUM) {
                        this.addEnumConstants(env, (TypeElement)((DeclaredType)tm).asElement());
                    } else {
                        this.addLocalConstantsAndTypes(env);
                    }
                }
            } else {
                TokenSequence<JavaTokenId> ts = this.findLastNonWhitespaceToken(env, (Tree)cst, offset);
                if (ts != null && ts.token().id() != JavaTokenId.DEFAULT) {
                    this.localResult(env);
                    this.addKeywordsForBlock(env);
                }
            }
        }

        private void insideParens(Env env) throws IOException {
            TreePath path = env.getPath();
            ParenthesizedTree pa = (ParenthesizedTree)path.getLeaf();
            SourcePositions sourcePositions = env.getSourcePositions();
            CompilationUnitTree root = env.getRoot();
            Tree exp = this.unwrapErrTree((Tree)pa.getExpression());
            if (exp == null || (long)env.getOffset() <= sourcePositions.getStartPosition(root, exp)) {
                Set<? extends TypeMirror> smarts;
                if (this.queryType == 1 && path.getParentPath().getLeaf().getKind() != Tree.Kind.SWITCH && (smarts = env.getSmartTypes()) != null) {
                    Elements elements = env.getController().getElements();
                    for (TypeMirror smart : smarts) {
                        if (smart == null) continue;
                        if (smart.getKind() == TypeKind.DECLARED) {
                            for (DeclaredType subtype : this.getSubtypesOf(env, (DeclaredType)smart)) {
                                TypeElement elem = (TypeElement)subtype.asElement();
                                if (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(elem)) {
                                    this.results.add(this.javaCompletionItemFactory.createTypeItem((CompilationInfo)env.getController(), elem, subtype, this.anchorOffset, env.getReferencesCount(), elements.isDeprecated(elem), false, false, false, true, false, env.getWhiteList()));
                                }
                                env.addToExcludes(elem);
                            }
                            continue;
                        }
                        if (smart.getKind() != TypeKind.ARRAY) continue;
                        try {
                            TypeMirror tm = smart;
                            while (tm.getKind() == TypeKind.ARRAY) {
                                tm = ((ArrayType)tm).getComponentType();
                            }
                            if (tm.getKind().isPrimitive() && this.startsWith(env, tm.toString())) {
                                this.results.add(this.javaCompletionItemFactory.createArrayItem((CompilationInfo)env.getController(), (ArrayType)smart, this.anchorOffset, env.getReferencesCount(), env.getController().getElements(), env.getWhiteList()));
                                continue;
                            }
                            if (tm.getKind() != TypeKind.DECLARED && tm.getKind() != TypeKind.ERROR || !this.startsWith(env, ((DeclaredType)tm).asElement().getSimpleName().toString())) continue;
                            this.results.add(this.javaCompletionItemFactory.createArrayItem((CompilationInfo)env.getController(), (ArrayType)smart, this.anchorOffset, env.getReferencesCount(), env.getController().getElements(), env.getWhiteList()));
                        }
                        catch (IllegalArgumentException iae) {
                        }
                    }
                }
                this.addLocalMembersAndVars(env);
                this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.TYPE_PARAMETER), null);
                this.addPrimitiveTypeKeywords(env);
                this.addValueKeywords(env);
            } else {
                this.insideExpression(env, new TreePath(path, exp));
            }
        }

        private void insideTypeCheck(Env env) throws IOException {
            InstanceOfTree iot = (InstanceOfTree)env.getPath().getLeaf();
            TokenSequence<JavaTokenId> ts = this.findLastNonWhitespaceToken(env, (Tree)iot, env.getOffset());
            if (ts != null && ts.token().id() == JavaTokenId.INSTANCEOF) {
                this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.TYPE_PARAMETER), null);
            }
        }

        private void insideArrayAccess(Env env) throws IOException {
            CompilationUnitTree root;
            Tree expr;
            int bPos;
            String aatText;
            int offset = env.getOffset();
            ArrayAccessTree aat = (ArrayAccessTree)env.getPath().getLeaf();
            SourcePositions sourcePositions = env.getSourcePositions();
            int aaTextStart = (int)sourcePositions.getEndPosition(root = env.getRoot(), (Tree)aat.getExpression());
            if ((long)aaTextStart != -1 && ((expr = this.unwrapErrTree((Tree)aat.getIndex())) == null || offset <= (int)sourcePositions.getStartPosition(root, expr)) && (bPos = (aatText = env.getController().getText().substring(aaTextStart, offset)).indexOf(91)) > -1) {
                this.localResult(env);
                this.addValueKeywords(env);
            }
        }

        private void insideNewArray(Env env) throws IOException {
            int offset = env.getOffset();
            TreePath path = env.getPath();
            NewArrayTree nat = (NewArrayTree)path.getLeaf();
            if (nat.getInitializers() != null) {
                Tree init;
                int pos;
                SourcePositions sourcePositions = env.getSourcePositions();
                CompilationUnitTree root = env.getRoot();
                Tree last = null;
                int lastPos = offset;
                Iterator i$ = nat.getInitializers().iterator();
                while (i$.hasNext() && (long)(pos = (int)sourcePositions.getEndPosition(root, init = (Tree)i$.next())) != -1 && offset > pos) {
                    last = init;
                    lastPos = pos;
                }
                if (last != null) {
                    TokenSequence<JavaTokenId> ts = this.findLastNonWhitespaceToken(env, lastPos, offset);
                    if (ts != null && ts.token().id() == JavaTokenId.COMMA) {
                        TreePath parentPath = path.getParentPath();
                        TreePath gparentPath = parentPath.getParentPath();
                        if (gparentPath.getLeaf().getKind() == Tree.Kind.ANNOTATION && parentPath.getLeaf().getKind() == Tree.Kind.ASSIGNMENT) {
                            ExpressionTree var = ((AssignmentTree)parentPath.getLeaf()).getVariable();
                            if (var.getKind() == Tree.Kind.IDENTIFIER) {
                                this.insideAnnotationAttribute(env, gparentPath, ((IdentifierTree)var).getName());
                                this.addLocalConstantsAndTypes(env);
                            }
                        } else {
                            this.localResult(env);
                            this.addValueKeywords(env);
                        }
                    }
                    return;
                }
            }
            TokenSequence<JavaTokenId> ts = this.findLastNonWhitespaceToken(env, (Tree)nat, offset);
            switch ((JavaTokenId)ts.token().id()) {
                case LBRACKET: 
                case LBRACE: {
                    TreePath parentPath = path.getParentPath();
                    TreePath gparentPath = parentPath.getParentPath();
                    if (gparentPath.getLeaf().getKind() == Tree.Kind.ANNOTATION && parentPath.getLeaf().getKind() == Tree.Kind.ASSIGNMENT) {
                        ExpressionTree var = ((AssignmentTree)parentPath.getLeaf()).getVariable();
                        if (var.getKind() != Tree.Kind.IDENTIFIER) break;
                        this.insideAnnotationAttribute(env, gparentPath, ((IdentifierTree)var).getName());
                        this.addLocalConstantsAndTypes(env);
                        break;
                    }
                    this.localResult(env);
                    this.addValueKeywords(env);
                    break;
                }
                case RBRACKET: {
                    if (nat.getDimensions().size() <= 0) break;
                    this.insideExpression(env, path);
                }
            }
        }

        private void insideAssignment(Env env) throws IOException {
            CompilationUnitTree root;
            int offset = env.getOffset();
            TreePath path = env.getPath();
            AssignmentTree as = (AssignmentTree)path.getLeaf();
            SourcePositions sourcePositions = env.getSourcePositions();
            int asTextStart = (int)sourcePositions.getEndPosition(root = env.getRoot(), (Tree)as.getVariable());
            if ((long)asTextStart != -1) {
                Tree expr = this.unwrapErrTree((Tree)as.getExpression());
                if (expr == null || offset <= (int)sourcePositions.getStartPosition(root, expr)) {
                    CompilationController controller = env.getController();
                    String asText = controller.getText().substring(asTextStart, offset);
                    int eqPos = asText.indexOf(61);
                    if (eqPos > -1) {
                        TreePath parentPath = path.getParentPath();
                        if (parentPath.getLeaf().getKind() != Tree.Kind.ANNOTATION) {
                            this.localResult(env);
                            this.addValueKeywords(env);
                        } else if (as.getVariable().getKind() == Tree.Kind.IDENTIFIER) {
                            this.insideAnnotationAttribute(env, parentPath, ((IdentifierTree)as.getVariable()).getName());
                            this.addLocalConstantsAndTypes(env);
                        }
                    }
                } else {
                    this.insideExpression(env, new TreePath(path, expr));
                }
            }
        }

        private void insideCompoundAssignment(Env env) throws IOException {
            CompilationUnitTree root;
            String catText;
            int eqPos;
            Tree expr;
            int offset = env.getOffset();
            CompoundAssignmentTree cat = (CompoundAssignmentTree)env.getPath().getLeaf();
            SourcePositions sourcePositions = env.getSourcePositions();
            int catTextStart = (int)sourcePositions.getEndPosition(root = env.getRoot(), (Tree)cat.getVariable());
            if ((long)catTextStart != -1 && ((expr = this.unwrapErrTree((Tree)cat.getExpression())) == null || offset <= (int)sourcePositions.getStartPosition(root, expr)) && (eqPos = (catText = env.getController().getText().substring(catTextStart, offset)).indexOf(61)) > -1) {
                this.localResult(env);
                this.addValueKeywords(env);
            }
        }

        private void insideStringLiteral(Env env) throws IOException {
            ExpressionTree var;
            TreePath path = env.getPath();
            TreePath parentPath = path.getParentPath();
            TreePath grandParentPath = parentPath.getParentPath();
            if (grandParentPath != null && grandParentPath.getLeaf().getKind() == Tree.Kind.ANNOTATION && parentPath.getLeaf().getKind() == Tree.Kind.ASSIGNMENT && ((AssignmentTree)parentPath.getLeaf()).getExpression() == path.getLeaf() && (var = ((AssignmentTree)parentPath.getLeaf()).getVariable()).getKind() == Tree.Kind.IDENTIFIER) {
                this.insideAnnotationAttribute(env, grandParentPath, ((IdentifierTree)var).getName());
            }
        }

        private void insideBinaryTree(Env env) throws IOException {
            CompilationUnitTree root;
            TokenSequence<JavaTokenId> last;
            int offset = env.getOffset();
            TreePath path = env.getPath();
            BinaryTree bi = (BinaryTree)path.getLeaf();
            SourcePositions sourcePositions = env.getSourcePositions();
            int pos = (int)sourcePositions.getEndPosition(root = env.getRoot(), (Tree)bi.getRightOperand());
            if ((long)pos != -1 && pos < offset) {
                return;
            }
            pos = (int)sourcePositions.getEndPosition(root, (Tree)bi.getLeftOperand());
            if ((long)pos != -1 && (last = this.findLastNonWhitespaceToken(env, pos, offset)) != null) {
                TypeMirror tm;
                CompilationController controller = env.getController();
                controller.toPhase(JavaSource.Phase.RESOLVED);
                TypeMirror typeMirror = tm = last.token().id() == JavaTokenId.AMP ? controller.getTrees().getTypeMirror(new TreePath(path, (Tree)bi.getLeftOperand())) : null;
                if (tm != null && tm.getKind() == TypeKind.DECLARED) {
                    env.addToExcludes(((DeclaredType)tm).asElement());
                    this.addTypes(env, EnumSet.of(ElementKind.INTERFACE, ElementKind.ANNOTATION_TYPE), null);
                } else if (tm != null && tm.getKind() == TypeKind.INTERSECTION) {
                    for (TypeMirror bound : ((IntersectionType)tm).getBounds()) {
                        if (bound.getKind() != TypeKind.DECLARED) continue;
                        env.addToExcludes(((DeclaredType)bound).asElement());
                    }
                    this.addTypes(env, EnumSet.of(ElementKind.INTERFACE, ElementKind.ANNOTATION_TYPE), null);
                } else {
                    this.localResult(env);
                    this.addValueKeywords(env);
                }
            }
        }

        private void insideConditionalExpression(Env env) throws IOException {
            TokenSequence<JavaTokenId> last;
            CompilationUnitTree root;
            ConditionalExpressionTree co = (ConditionalExpressionTree)env.getPath().getLeaf();
            SourcePositions sourcePositions = env.getSourcePositions();
            int coTextStart = (int)sourcePositions.getStartPosition(root = env.getRoot(), (Tree)co);
            if ((long)coTextStart != -1 && (last = this.findLastNonWhitespaceToken(env, coTextStart, env.getOffset())) != null && (last.token().id() == JavaTokenId.QUESTION || last.token().id() == JavaTokenId.COLON)) {
                this.localResult(env);
                this.addValueKeywords(env);
            }
        }

        private void insideExpressionStatement(Env env) throws IOException {
            TreePath path = env.getPath();
            ExpressionStatementTree est = (ExpressionStatementTree)path.getLeaf();
            CompilationController controller = env.getController();
            ExpressionTree t = est.getExpression();
            if (t.getKind() == Tree.Kind.ERRONEOUS) {
                Iterator it = ((ErroneousTree)t).getErrorTrees().iterator();
                if (it.hasNext()) {
                    t = (Tree)it.next();
                } else {
                    this.localResult(env);
                    Tree parentTree = path.getParentPath().getLeaf();
                    switch (parentTree.getKind()) {
                        case FOR_LOOP: {
                            if (((ForLoopTree)parentTree).getStatement() == est) {
                                this.addKeywordsForStatement(env);
                                break;
                            }
                            this.addValueKeywords(env);
                            break;
                        }
                        case ENHANCED_FOR_LOOP: {
                            if (((EnhancedForLoopTree)parentTree).getStatement() == est) {
                                this.addKeywordsForStatement(env);
                                break;
                            }
                            this.addValueKeywords(env);
                            break;
                        }
                        case VARIABLE: {
                            this.addValueKeywords(env);
                            break;
                        }
                        case LAMBDA_EXPRESSION: {
                            this.addValueKeywords(env);
                            break;
                        }
                        default: {
                            this.addKeywordsForStatement(env);
                        }
                    }
                    return;
                }
            }
            TreePath tPath = new TreePath(path, (Tree)t);
            if (t.getKind() == Tree.Kind.MODIFIERS) {
                this.insideModifiers(env, tPath);
            } else if (t.getKind() == Tree.Kind.MEMBER_SELECT && "<error>".contentEquals(((MemberSelectTree)t).getIdentifier())) {
                controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                TreePath expPath = new TreePath(tPath, (Tree)((MemberSelectTree)t).getExpression());
                TypeMirror type = controller.getTrees().getTypeMirror(expPath);
                switch (type.getKind()) {
                    case TYPEVAR: {
                        type = ((TypeVariable)type).getUpperBound();
                        if (type == null) {
                            return;
                        }
                        type = controller.getTypes().capture(type);
                    }
                    case ARRAY: 
                    case DECLARED: 
                    case BOOLEAN: 
                    case BYTE: 
                    case CHAR: 
                    case DOUBLE: 
                    case FLOAT: 
                    case INT: 
                    case LONG: 
                    case SHORT: 
                    case VOID: {
                        this.addMembers(env, type, controller.getTrees().getElement(expPath), EnumSet.of(ElementKind.CLASS, new ElementKind[]{ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.INTERFACE, ElementKind.FIELD, ElementKind.METHOD, ElementKind.ENUM_CONSTANT}), null, false, false, false);
                        break;
                    }
                    default: {
                        Element el = controller.getTrees().getElement(expPath);
                        if (el instanceof PackageElement) {
                            this.addPackageContent(env, (PackageElement)el, EnumSet.of(ElementKind.CLASS, new ElementKind[]{ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.INTERFACE, ElementKind.FIELD, ElementKind.METHOD, ElementKind.ENUM_CONSTANT}), null, false, false);
                            break;
                        } else {
                            break;
                        }
                    }
                }
            } else {
                this.insideExpression(env, tPath);
            }
        }

        private void insideExpression(Env env, TreePath exPath) throws IOException {
            Tree t;
            Element e;
            TokenSequence<JavaTokenId> last;
            int offset = env.getOffset();
            String prefix = env.getPrefix();
            Tree et = exPath.getLeaf();
            Tree parent = exPath.getParentPath().getLeaf();
            CompilationController controller = env.getController();
            int endPos = (int)env.getSourcePositions().getEndPosition(env.getRoot(), et);
            if ((long)endPos != -1 && endPos < offset && (last = this.findLastNonWhitespaceToken(env, endPos, offset)) != null && last.token().id() != JavaTokenId.COMMA) {
                return;
            }
            controller.toPhase(JavaSource.Phase.RESOLVED);
            ElementKind varKind = ElementKind.LOCAL_VARIABLE;
            Set varMods = EnumSet.noneOf(Modifier.class);
            if (parent.getKind() == Tree.Kind.VARIABLE) {
                varMods = ((VariableTree)parent).getModifiers().getFlags();
                Element varEl = controller.getTrees().getElement(exPath.getParentPath());
                if (varEl != null) {
                    varKind = varEl.getKind();
                }
            }
            if (et.getKind() == Tree.Kind.ANNOTATED_TYPE) {
                et = ((AnnotatedTypeTree)et).getUnderlyingType();
                exPath = new TreePath(exPath, et);
            }
            if (!(parent != null && parent.getKind() == Tree.Kind.PARENTHESIZED || et.getKind() != Tree.Kind.PRIMITIVE_TYPE && et.getKind() != Tree.Kind.ARRAY_TYPE && et.getKind() != Tree.Kind.PARAMETERIZED_TYPE)) {
                TypeMirror tm = controller.getTrees().getTypeMirror(exPath);
                final Collection<? extends Element> illegalForwardRefs = env.getForwardReferences();
                Scope scope = env.getScope();
                final ExecutableElement method = scope.getEnclosingMethod();
                ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                    public boolean accept(Element e, TypeMirror t) {
                        return (method == null || method == e.getEnclosingElement() || e.getModifiers().contains((Object)Modifier.FINAL)) && !illegalForwardRefs.contains(e);
                    }
                };
                String suggestedName = Utilities.varNameSuggestion(Utilities.getPathElementOfKind(EnumSet.of(Tree.Kind.ASSIGNMENT, Tree.Kind.VARIABLE), env.getOriginalPath()));
                for (String name : Utilities.varNamesSuggestions(tm, varKind, varMods, suggestedName, prefix, controller.getTypes(), controller.getElements(), controller.getElementUtilities().getLocalMembersAndVars(scope, acceptor), CodeStyle.getDefault((Document)controller.getDocument()))) {
                    this.results.add(this.javaCompletionItemFactory.createVariableItem((CompilationInfo)env.getController(), name, this.anchorOffset, true, false));
                }
                return;
            }
            if (et.getKind() == Tree.Kind.UNION_TYPE) {
                Iterator i$ = ((UnionTypeTree)et).getTypeAlternatives().iterator();
                while (i$.hasNext()) {
                    et = t = (Tree)i$.next();
                    exPath = new TreePath(exPath, t);
                }
            }
            if (et.getKind() == Tree.Kind.IDENTIFIER) {
                Element e2 = controller.getTrees().getElement(exPath);
                if (e2 == null) {
                    return;
                }
                TypeMirror tm = controller.getTrees().getTypeMirror(exPath);
                switch (e2.getKind()) {
                    case ANNOTATION_TYPE: 
                    case CLASS: 
                    case ENUM: 
                    case INTERFACE: 
                    case PACKAGE: {
                        VariableElement ve;
                        if (parent == null || parent.getKind() != Tree.Kind.PARENTHESIZED || env.getController().getSourceVersion().compareTo(SourceVersion.RELEASE_8) >= 0) {
                            final Collection<? extends Element> illegalForwardRefs = env.getForwardReferences();
                            Scope scope = env.getScope();
                            final ExecutableElement method = scope.getEnclosingMethod();
                            ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                                public boolean accept(Element e, TypeMirror t) {
                                    return (method == null || method == e.getEnclosingElement() || e.getModifiers().contains((Object)Modifier.FINAL)) && !illegalForwardRefs.contains(e);
                                }
                            };
                            String suggestedName = Utilities.varNameSuggestion(Utilities.getPathElementOfKind(EnumSet.of(Tree.Kind.ASSIGNMENT, Tree.Kind.VARIABLE), env.getOriginalPath()));
                            for (String name : Utilities.varNamesSuggestions(tm, varKind, varMods, suggestedName, prefix, controller.getTypes(), controller.getElements(), controller.getElementUtilities().getLocalMembersAndVars(scope, acceptor), CodeStyle.getDefault((Document)controller.getDocument()))) {
                                this.results.add(this.javaCompletionItemFactory.createVariableItem((CompilationInfo)env.getController(), name, this.anchorOffset, true, false));
                            }
                        }
                        if ((ve = this.getFieldOrVar(env, e2.getSimpleName().toString())) == null) break;
                        this.addKeyword(env, "instanceof", " ", false);
                        break;
                    }
                    case ENUM_CONSTANT: 
                    case FIELD: 
                    case EXCEPTION_PARAMETER: 
                    case LOCAL_VARIABLE: 
                    case RESOURCE_VARIABLE: 
                    case PARAMETER: {
                        TypeElement te;
                        if (tm != null && (tm.getKind() == TypeKind.DECLARED || tm.getKind() == TypeKind.ARRAY || tm.getKind() == TypeKind.ERROR)) {
                            this.addKeyword(env, "instanceof", " ", false);
                        }
                        if ((te = this.getTypeElement(env, e2.getSimpleName().toString())) == null) break;
                        final Collection<? extends Element> illegalForwardRefs = env.getForwardReferences();
                        Scope scope = env.getScope();
                        final ExecutableElement method = scope.getEnclosingMethod();
                        ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                            public boolean accept(Element e, TypeMirror t) {
                                return (method == null || method == e.getEnclosingElement() || e.getModifiers().contains((Object)Modifier.FINAL)) && !illegalForwardRefs.contains(e);
                            }
                        };
                        String suggestedName = Utilities.varNameSuggestion(Utilities.getPathElementOfKind(EnumSet.of(Tree.Kind.ASSIGNMENT, Tree.Kind.VARIABLE), env.getOriginalPath()));
                        for (String name : Utilities.varNamesSuggestions(controller.getTypes().getDeclaredType(te, new TypeMirror[0]), varKind, varMods, suggestedName, prefix, controller.getTypes(), controller.getElements(), controller.getElementUtilities().getLocalMembersAndVars(scope, acceptor), CodeStyle.getDefault((Document)controller.getDocument()))) {
                            this.results.add(this.javaCompletionItemFactory.createVariableItem((CompilationInfo)env.getController(), name, this.anchorOffset, true, false));
                        }
                        break;
                    }
                }
                return;
            }
            ExpressionTree exp = null;
            if (et.getKind() == Tree.Kind.PARENTHESIZED) {
                exp = ((ParenthesizedTree)et).getExpression();
            } else if (et.getKind() == Tree.Kind.TYPE_CAST) {
                if (env.getSourcePositions().getEndPosition(env.getRoot(), ((TypeCastTree)et).getType()) <= (long)offset) {
                    exp = ((TypeCastTree)et).getType();
                }
            } else if (et.getKind() == Tree.Kind.ASSIGNMENT && (t = ((AssignmentTree)et).getExpression()).getKind() == Tree.Kind.PARENTHESIZED && env.getSourcePositions().getEndPosition(env.getRoot(), t) < (long)offset) {
                exp = ((ParenthesizedTree)t).getExpression();
            }
            if (exp != null) {
                exPath = new TreePath(exPath, (Tree)exp);
                if (exp.getKind() == Tree.Kind.PRIMITIVE_TYPE || exp.getKind() == Tree.Kind.ARRAY_TYPE || exp.getKind() == Tree.Kind.PARAMETERIZED_TYPE) {
                    this.localResult(env);
                    this.addValueKeywords(env);
                    return;
                }
                e = controller.getTrees().getElement(exPath);
                if (e == null) {
                    if (exp.getKind() == Tree.Kind.TYPE_CAST) {
                        this.addKeyword(env, "instanceof", " ", false);
                    }
                    return;
                }
                TypeMirror tm = controller.getTrees().getTypeMirror(exPath);
                switch (e.getKind()) {
                    case ANNOTATION_TYPE: 
                    case CLASS: 
                    case ENUM: 
                    case INTERFACE: 
                    case PACKAGE: {
                        if (exp.getKind() == Tree.Kind.IDENTIFIER) {
                            VariableElement ve = this.getFieldOrVar(env, e.getSimpleName().toString());
                            if (ve != null) {
                                this.addKeyword(env, "instanceof", " ", false);
                            }
                            if (ve != null && tm != null && tm.getKind() == TypeKind.ERROR) break;
                            this.localResult(env);
                            this.addValueKeywords(env);
                            break;
                        }
                        if (exp.getKind() == Tree.Kind.MEMBER_SELECT) {
                            if (tm != null && (tm.getKind() == TypeKind.ERROR || tm.getKind() == TypeKind.PACKAGE)) {
                                this.addKeyword(env, "instanceof", " ", false);
                            }
                            this.localResult(env);
                            this.addValueKeywords(env);
                            break;
                        }
                        if (exp.getKind() != Tree.Kind.PARENTHESIZED || tm == null || tm.getKind() != TypeKind.DECLARED && tm.getKind() != TypeKind.ARRAY) break;
                        this.addKeyword(env, "instanceof", " ", false);
                        break;
                    }
                    case ENUM_CONSTANT: 
                    case FIELD: 
                    case EXCEPTION_PARAMETER: 
                    case LOCAL_VARIABLE: 
                    case RESOURCE_VARIABLE: 
                    case PARAMETER: {
                        TypeElement te;
                        if (tm != null && (tm.getKind() == TypeKind.DECLARED || tm.getKind() == TypeKind.ARRAY || tm.getKind() == TypeKind.ERROR)) {
                            this.addKeyword(env, "instanceof", " ", false);
                        }
                        if ((te = this.getTypeElement(env, e.getSimpleName().toString())) == null && exp.getKind() != Tree.Kind.MEMBER_SELECT) break;
                        this.localResult(env);
                        this.addValueKeywords(env);
                        break;
                    }
                    case CONSTRUCTOR: 
                    case METHOD: {
                        if (tm == null || tm.getKind() != TypeKind.DECLARED && tm.getKind() != TypeKind.ARRAY && tm.getKind() != TypeKind.ERROR) break;
                        this.addKeyword(env, "instanceof", " ", false);
                    }
                }
                return;
            }
            e = controller.getTrees().getElement(exPath);
            TypeMirror tm = controller.getTrees().getTypeMirror(exPath);
            if (e == null) {
                if (tm != null && (tm.getKind() == TypeKind.DECLARED || tm.getKind() == TypeKind.ARRAY)) {
                    this.addKeyword(env, "instanceof", " ", false);
                }
                return;
            }
            switch (e.getKind()) {
                case ANNOTATION_TYPE: 
                case CLASS: 
                case ENUM: 
                case INTERFACE: 
                case PACKAGE: {
                    final Collection<? extends Element> illegalForwardRefs = env.getForwardReferences();
                    Scope scope = env.getScope();
                    final ExecutableElement method = scope.getEnclosingMethod();
                    ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                        public boolean accept(Element e, TypeMirror t) {
                            return (method == null || method == e.getEnclosingElement() || e.getModifiers().contains((Object)Modifier.FINAL)) && !illegalForwardRefs.contains(e);
                        }
                    };
                    String suggestedName = Utilities.varNameSuggestion(Utilities.getPathElementOfKind(EnumSet.of(Tree.Kind.ASSIGNMENT, Tree.Kind.VARIABLE), env.getOriginalPath()));
                    for (String name : Utilities.varNamesSuggestions(tm, varKind, varMods, suggestedName, prefix, controller.getTypes(), controller.getElements(), controller.getElementUtilities().getLocalMembersAndVars(scope, acceptor), CodeStyle.getDefault((Document)controller.getDocument()))) {
                        this.results.add(this.javaCompletionItemFactory.createVariableItem((CompilationInfo)env.getController(), name, this.anchorOffset, true, false));
                    }
                    break;
                }
                case CONSTRUCTOR: 
                case METHOD: 
                case ENUM_CONSTANT: 
                case FIELD: 
                case EXCEPTION_PARAMETER: 
                case LOCAL_VARIABLE: 
                case RESOURCE_VARIABLE: 
                case PARAMETER: {
                    if (tm == null || tm.getKind() != TypeKind.DECLARED && tm.getKind() != TypeKind.ARRAY && tm.getKind() != TypeKind.ERROR) break;
                    this.addKeyword(env, "instanceof", " ", false);
                }
            }
        }

        private void insideBreak(Env env) throws IOException {
            TreePath path = env.getPath();
            TokenSequence<JavaTokenId> ts = this.findLastNonWhitespaceToken(env, path.getLeaf(), env.getOffset());
            if (ts != null && ts.token().id() == JavaTokenId.BREAK) {
                while (path != null) {
                    if (path.getLeaf().getKind() == Tree.Kind.LABELED_STATEMENT) {
                        this.results.add(this.javaCompletionItemFactory.createVariableItem((CompilationInfo)env.getController(), ((LabeledStatementTree)path.getLeaf()).getLabel().toString(), this.anchorOffset, false, false));
                    }
                    path = path.getParentPath();
                }
            }
        }

        private void localResult(Env env) throws IOException {
            this.addLocalMembersAndVars(env);
            this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.TYPE_PARAMETER), null);
            this.addPrimitiveTypeKeywords(env);
        }

        private void addLocalConstantsAndTypes(Env env) throws IOException {
            String prefix = env.getPrefix();
            CompilationController controller = env.getController();
            Elements elements = controller.getElements();
            final Types types = controller.getTypes();
            final Trees trees = controller.getTrees();
            final Scope scope = env.getScope();
            Set<? extends TypeMirror> smartTypes = null;
            boolean smartType = false;
            if (this.queryType == 1 && (smartTypes = env.getSmartTypes()) != null) {
                Iterator<? extends TypeMirror> i$ = smartTypes.iterator();
                while (i$.hasNext()) {
                    TypeMirror st = i$.next();
                    if (st.getKind() == TypeKind.BOOLEAN) {
                        smartType = true;
                    }
                    if (st.getKind().isPrimitive()) {
                        st = types.boxedClass((PrimitiveType)st).asType();
                    }
                    if (st.getKind() != TypeKind.DECLARED) continue;
                    final DeclaredType type = (DeclaredType)st;
                    TypeElement element = (TypeElement)type.asElement();
                    if (element.getKind() == ElementKind.ANNOTATION_TYPE && (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(element))) {
                        this.results.add(this.javaCompletionItemFactory.createAnnotationItem((CompilationInfo)env.getController(), element, type, this.anchorOffset, env.getReferencesCount(), elements.isDeprecated(element), env.getWhiteList()));
                    }
                    if ("java.lang.Class".contentEquals(element.getQualifiedName())) {
                        this.addTypeDotClassMembers(env, type);
                    }
                    if (!this.startsWith(env, element.getSimpleName().toString(), prefix)) continue;
                    final boolean isStatic = element.getKind().isClass() || element.getKind().isInterface();
                    ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                        public boolean accept(Element e, TypeMirror t) {
                            return (e.getKind() == ElementKind.ENUM_CONSTANT || e.getKind() == ElementKind.FIELD && ((VariableElement)e).getConstantValue() != null) && (!isStatic || e.getModifiers().contains((Object)Modifier.STATIC)) && trees.isAccessible(scope, e, (DeclaredType)t) && types.isAssignable(((VariableElement)e).asType(), type);
                        }
                    };
                    for (Element ee : controller.getElementUtilities().getMembers((TypeMirror)type, acceptor)) {
                        if (!Utilities.isShowDeprecatedMembers() && elements.isDeprecated(ee)) continue;
                        this.results.add(this.javaCompletionItemFactory.createStaticMemberItem((CompilationInfo)env.getController(), type, ee, this.asMemberOf(ee, type, types), false, this.anchorOffset, elements.isDeprecated(ee), false, env.getWhiteList()));
                    }
                }
            }
            if (env.getPath().getLeaf().getKind() != Tree.Kind.CASE) {
                if (Utilities.startsWith("false", prefix)) {
                    this.results.add(this.javaCompletionItemFactory.createKeywordItem("false", null, this.anchorOffset, smartType));
                }
                if (Utilities.startsWith("true", prefix)) {
                    this.results.add(this.javaCompletionItemFactory.createKeywordItem("true", null, this.anchorOffset, smartType));
                }
            }
            TypeElement enclClass = scope.getEnclosingClass();
            for (Element e : this.getLocalMembersAndVars(env)) {
                switch (e.getKind()) {
                    case FIELD: {
                        if (((VariableElement)e).getConstantValue() == null) break;
                        TypeMirror tm = this.asMemberOf(e, enclClass != null ? enclClass.asType() : null, types);
                        this.results.add(this.javaCompletionItemFactory.createVariableItem((CompilationInfo)env.getController(), (VariableElement)e, tm, this.anchorOffset, null, env.getScope().getEnclosingClass() != e.getEnclosingElement(), elements.isDeprecated(e), this.isOfSmartType(env, tm, smartTypes), env.assignToVarPos(), env.getWhiteList()));
                        break;
                    }
                    case ENUM_CONSTANT: 
                    case EXCEPTION_PARAMETER: 
                    case LOCAL_VARIABLE: 
                    case RESOURCE_VARIABLE: 
                    case PARAMETER: {
                        if (((VariableElement)e).getConstantValue() == null) break;
                        this.results.add(this.javaCompletionItemFactory.createVariableItem((CompilationInfo)env.getController(), (VariableElement)e, e.asType(), this.anchorOffset, null, env.getScope().getEnclosingClass() != e.getEnclosingElement(), elements.isDeprecated(e), this.isOfSmartType(env, e.asType(), smartTypes), env.assignToVarPos(), env.getWhiteList()));
                    }
                }
            }
            this.addTypes(env, EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE, ElementKind.TYPE_PARAMETER), null);
        }

        private void addLocalMembersAndVars(final Env env) throws IOException {
            CompilationController controller = env.getController();
            Elements elements = controller.getElements();
            Types types = controller.getTypes();
            final Trees trees = controller.getTrees();
            final Scope scope = env.getScope();
            Iterable<? extends Element> locals = this.getLocalMembersAndVars(env);
            Set<? extends TypeMirror> smartTypes = null;
            if (this.queryType == 1) {
                smartTypes = env.getSmartTypes();
                if (smartTypes != null) {
                    Iterator<? extends TypeMirror> i$ = smartTypes.iterator();
                    while (i$.hasNext()) {
                        boolean withinScope;
                        TypeMirror st = i$.next();
                        if (st.getKind().isPrimitive()) {
                            st = types.boxedClass((PrimitiveType)st).asType();
                        }
                        if (st.getKind() != TypeKind.DECLARED) continue;
                        DeclaredType type = (DeclaredType)st;
                        TypeElement element = (TypeElement)type.asElement();
                        if ("java.lang.Class".contentEquals(element.getQualifiedName())) {
                            this.addTypeDotClassMembers(env, type);
                        }
                        if (!this.startsWith(env, element.getSimpleName().toString()) || (withinScope = this.withinScope(env, element)) && scope.getEnclosingClass() == element) continue;
                        final boolean isStatic = element.getKind().isClass() || element.getKind().isInterface();
                        final Set<? extends TypeMirror> finalSmartTypes = smartTypes;
                        ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                            public boolean accept(Element e, TypeMirror t) {
                                return (!e.getSimpleName().contentEquals("class") && !withinScope && (!isStatic || e.getModifiers().contains((Object)Modifier.STATIC)) || withinScope && e.getSimpleName().contentEquals("this")) && trees.isAccessible(scope, e, (DeclaredType)t) && (e.getKind().isField() && JavaCompletionQuery.this.isOfSmartType(env, ((VariableElement)e).asType(), finalSmartTypes) || e.getKind() == ElementKind.METHOD && JavaCompletionQuery.this.isOfSmartType(env, ((ExecutableElement)e).getReturnType(), finalSmartTypes));
                            }
                        };
                        for (Element ee : controller.getElementUtilities().getMembers((TypeMirror)type, acceptor)) {
                            if (!Utilities.isShowDeprecatedMembers() && elements.isDeprecated(ee)) continue;
                            this.results.add(this.javaCompletionItemFactory.createStaticMemberItem((CompilationInfo)env.getController(), type, ee, this.asMemberOf(ee, type, types), false, this.anchorOffset, elements.isDeprecated(ee), env.addSemicolon(), env.getWhiteList()));
                        }
                    }
                }
            } else {
                this.addChainedMembers(env, locals);
                this.addAllStaticMemberNames(env);
            }
            TypeElement enclClass = scope.getEnclosingClass();
            for (Element e : locals) {
                switch (e.getKind()) {
                    case ENUM_CONSTANT: 
                    case EXCEPTION_PARAMETER: 
                    case LOCAL_VARIABLE: 
                    case RESOURCE_VARIABLE: 
                    case PARAMETER: {
                        this.results.add(this.javaCompletionItemFactory.createVariableItem((CompilationInfo)env.getController(), (VariableElement)e, e.asType(), this.anchorOffset, null, env.getScope().getEnclosingClass() != e.getEnclosingElement(), elements.isDeprecated(e), this.isOfSmartType(env, e.asType(), smartTypes), env.assignToVarPos(), env.getWhiteList()));
                        break;
                    }
                    case FIELD: {
                        String name = e.getSimpleName().toString();
                        if ("this".equals(name) || "super".equals(name)) {
                            this.results.add(this.javaCompletionItemFactory.createKeywordItem(name, null, this.anchorOffset, this.isOfSmartType(env, e.asType(), smartTypes)));
                            break;
                        }
                        TypeMirror tm = this.asMemberOf(e, enclClass != null ? enclClass.asType() : null, types);
                        this.results.add(this.javaCompletionItemFactory.createVariableItem((CompilationInfo)env.getController(), (VariableElement)e, tm, this.anchorOffset, null, env.getScope().getEnclosingClass() != e.getEnclosingElement(), elements.isDeprecated(e), this.isOfSmartType(env, tm, smartTypes), env.assignToVarPos(), env.getWhiteList()));
                        break;
                    }
                    case METHOD: {
                        ExecutableType et = (ExecutableType)this.asMemberOf(e, enclClass != null ? enclClass.asType() : null, types);
                        this.results.add(this.javaCompletionItemFactory.createExecutableItem((CompilationInfo)env.getController(), (ExecutableElement)e, et, this.anchorOffset, null, env.getScope().getEnclosingClass() != e.getEnclosingElement(), elements.isDeprecated(e), false, env.addSemicolon(), this.isOfSmartType(env, this.getCorrectedReturnType(env, et, (ExecutableElement)e, enclClass.asType()), smartTypes), env.assignToVarPos(), false, env.getWhiteList()));
                    }
                }
            }
        }

        private void addLocalFieldsAndVars(Env env) throws IOException {
            CompilationController controller = env.getController();
            Elements elements = controller.getElements();
            Types types = controller.getTypes();
            Scope scope = env.getScope();
            Set<? extends TypeMirror> smartTypes = this.queryType == 1 ? env.getSmartTypes() : null;
            TypeElement enclClass = scope.getEnclosingClass();
            for (Element e : this.getLocalMembersAndVars(env)) {
                switch (e.getKind()) {
                    case ENUM_CONSTANT: 
                    case EXCEPTION_PARAMETER: 
                    case LOCAL_VARIABLE: 
                    case RESOURCE_VARIABLE: 
                    case PARAMETER: {
                        this.results.add(this.javaCompletionItemFactory.createVariableItem((CompilationInfo)env.getController(), (VariableElement)e, e.asType(), this.anchorOffset, null, env.getScope().getEnclosingClass() != e.getEnclosingElement(), elements.isDeprecated(e), this.isOfSmartType(env, e.asType(), smartTypes), env.assignToVarPos(), env.getWhiteList()));
                        break;
                    }
                    case FIELD: {
                        String name = e.getSimpleName().toString();
                        if ("this".equals(name) || "super".equals(name)) {
                            this.results.add(this.javaCompletionItemFactory.createKeywordItem(name, null, this.anchorOffset, this.isOfSmartType(env, e.asType(), smartTypes)));
                            break;
                        }
                        TypeMirror tm = this.asMemberOf(e, enclClass != null ? enclClass.asType() : null, types);
                        this.results.add(this.javaCompletionItemFactory.createVariableItem((CompilationInfo)env.getController(), (VariableElement)e, tm, this.anchorOffset, null, env.getScope().getEnclosingClass() != e.getEnclosingElement(), elements.isDeprecated(e), this.isOfSmartType(env, tm, smartTypes), env.assignToVarPos(), env.getWhiteList()));
                    }
                }
            }
        }

        private Iterable<? extends Element> getLocalMembersAndVars(final Env env) throws IOException {
            final String prefix = env.getPrefix();
            CompilationController controller = env.getController();
            final Elements elements = controller.getElements();
            final Trees trees = controller.getTrees();
            TreeUtilities tu = controller.getTreeUtilities();
            final ElementUtilities eu = controller.getElementUtilities();
            final Scope scope = env.getScope();
            final TypeElement enclClass = scope.getEnclosingClass();
            final boolean enclStatic = enclClass != null && enclClass.getModifiers().contains((Object)Modifier.STATIC);
            final boolean ctxStatic = enclClass != null && (tu.isStaticContext(scope) || env.getPath().getLeaf().getKind() == Tree.Kind.BLOCK && ((BlockTree)env.getPath().getLeaf()).isStatic());
            final Collection<? extends Element> illegalForwardRefs = env.getForwardReferences();
            final ExecutableElement method = scope.getEnclosingMethod() != null && scope.getEnclosingMethod().getEnclosingElement() == enclClass ? scope.getEnclosingMethod() : null;
            ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                public boolean accept(Element e, TypeMirror t) {
                    boolean isStatic = ctxStatic || t != null && t.getKind() == TypeKind.DECLARED && ((DeclaredType)t).asElement() != enclClass && enclStatic;
                    switch (e.getKind()) {
                        case CONSTRUCTOR: {
                            return false;
                        }
                        case EXCEPTION_PARAMETER: 
                        case LOCAL_VARIABLE: 
                        case RESOURCE_VARIABLE: 
                        case PARAMETER: {
                            return JavaCompletionQuery.this.startsWith(env, e.getSimpleName().toString()) && (method == e.getEnclosingElement() || eu.isEffectivelyFinal((VariableElement)e) || method == null && (e.getEnclosingElement().getKind() == ElementKind.INSTANCE_INIT || e.getEnclosingElement().getKind() == ElementKind.STATIC_INIT || e.getEnclosingElement().getKind() == ElementKind.METHOD && e.getEnclosingElement().getEnclosingElement().getKind() == ElementKind.FIELD)) && !illegalForwardRefs.contains(e);
                        }
                        case FIELD: {
                            if (e.getSimpleName().contentEquals("this") ? e.asType().getKind() == TypeKind.DECLARED && ((DeclaredType)e.asType()).asElement() == enclClass : e.getSimpleName().contentEquals("super")) {
                                return Utilities.startsWith(e.getSimpleName().toString(), prefix) && !isStatic;
                            }
                        }
                        case ENUM_CONSTANT: {
                            return !(!JavaCompletionQuery.this.startsWith(env, e.getSimpleName().toString()) || illegalForwardRefs.contains(e) || isStatic && !e.getModifiers().contains((Object)Modifier.STATIC) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(e) || !trees.isAccessible(scope, e, (DeclaredType)t));
                        }
                        case METHOD: {
                            String sn = e.getSimpleName().toString();
                            return !(!JavaCompletionQuery.this.startsWith(env, sn) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(e) || isStatic && !e.getModifiers().contains((Object)Modifier.STATIC) || !trees.isAccessible(scope, e, (DeclaredType)t) || Utilities.isExcludeMethods() && Utilities.isExcluded(Utilities.getElementName(e.getEnclosingElement(), true) + "." + sn));
                        }
                    }
                    return false;
                }
            };
            return controller.getElementUtilities().getLocalMembersAndVars(scope, acceptor);
        }

        private void addTypeDotClassMembers(Env env, DeclaredType type) throws IOException {
            CompilationController controller = env.getController();
            Elements elements = controller.getElements();
            Types types = controller.getTypes();
            Iterator<? extends TypeMirror> it = type.getTypeArguments().iterator();
            TypeMirror tm = it.hasNext() ? it.next() : elements.getTypeElement("java.lang.Object").asType();
            Collection dts = null;
            if (tm.getKind() == TypeKind.WILDCARD) {
                TypeMirror bound = ((WildcardType)tm).getSuperBound();
                if (bound != null) {
                    if (bound.getKind() == TypeKind.DECLARED) {
                        dts = this.getSupertypesOf(env, (DeclaredType)bound);
                    }
                } else {
                    bound = ((WildcardType)tm).getExtendsBound();
                    if (bound != null) {
                        if (bound.getKind() == TypeKind.DECLARED) {
                            dts = "java.lang.Object".contentEquals(((TypeElement)((DeclaredType)bound).asElement()).getQualifiedName()) ? Collections.singleton((DeclaredType)elements.getTypeElement("java.lang.Object").asType()) : this.getSubtypesOf(env, (DeclaredType)bound);
                        }
                    } else {
                        dts = Collections.singleton((DeclaredType)elements.getTypeElement("java.lang.Object").asType());
                    }
                }
            } else if (tm.getKind() == TypeKind.DECLARED) {
                dts = Collections.singleton((DeclaredType)tm);
            }
            if (dts != null) {
                ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                    public boolean accept(Element e, TypeMirror t) {
                        return e.getKind() == ElementKind.FIELD && e.getSimpleName().contentEquals("class");
                    }
                };
                for (DeclaredType dt : dts) {
                    if (!this.startsWith(env, dt.asElement().getSimpleName().toString())) continue;
                    for (Element ee : controller.getElementUtilities().getMembers((TypeMirror)dt, acceptor)) {
                        this.results.add(this.javaCompletionItemFactory.createStaticMemberItem((CompilationInfo)env.getController(), dt, ee, this.asMemberOf(ee, dt, types), false, this.anchorOffset, elements.isDeprecated(ee), env.addSemicolon(), env.getWhiteList()));
                    }
                }
            }
        }

        private void addChainedMembers(final Env env, Iterable<? extends Element> locals) throws IOException {
            final Set<? extends TypeMirror> smartTypes = env.getSmartTypes();
            if (smartTypes != null && !smartTypes.isEmpty()) {
                CompilationController controller = env.getController();
                final Scope scope = env.getScope();
                TypeElement enclClass = scope.getEnclosingClass();
                Elements elements = controller.getElements();
                final Types types = controller.getTypes();
                final Trees trees = controller.getTrees();
                ElementUtilities eu = controller.getElementUtilities();
                for (Element localElement : locals) {
                    TypeMirror localElementType = null;
                    TypeMirror type = null;
                    switch (localElement.getKind()) {
                        case ENUM_CONSTANT: 
                        case EXCEPTION_PARAMETER: 
                        case LOCAL_VARIABLE: 
                        case RESOURCE_VARIABLE: 
                        case PARAMETER: {
                            type = localElementType = localElement.asType();
                            break;
                        }
                        case FIELD: {
                            String name = localElement.getSimpleName().toString();
                            if ("this".equals(name) || "super".equals(name)) break;
                            type = localElementType = this.asMemberOf(localElement, enclClass != null ? enclClass.asType() : null, types);
                            break;
                        }
                        case METHOD: {
                            localElementType = this.asMemberOf(localElement, enclClass != null ? enclClass.asType() : null, types);
                            type = ((ExecutableType)localElementType).getReturnType();
                        }
                    }
                    if (type == null || type.getKind() != TypeKind.DECLARED || this.isOfSmartType(env, type, smartTypes)) continue;
                    ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                        public boolean accept(Element e, TypeMirror t) {
                            switch (e.getKind()) {
                                case FIELD: {
                                    if (e.getSimpleName().contentEquals("this") || e.getSimpleName().contentEquals("super")) {
                                        return false;
                                    }
                                }
                                case ENUM_CONSTANT: {
                                    return trees.isAccessible(scope, e, (DeclaredType)t) && JavaCompletionQuery.this.isOfSmartType(env, JavaCompletionQuery.this.asMemberOf(e, t, types), smartTypes);
                                }
                                case METHOD: {
                                    return trees.isAccessible(scope, e, (DeclaredType)t) && JavaCompletionQuery.this.isOfSmartType(env, ((ExecutableType)JavaCompletionQuery.this.asMemberOf(e, t, types)).getReturnType(), smartTypes);
                                }
                            }
                            return false;
                        }
                    };
                    for (Element e : eu.getMembers(type, acceptor)) {
                        if (!Utilities.isShowDeprecatedMembers() && elements.isDeprecated(e)) continue;
                        ArrayList<Element> chainedElements = new ArrayList<Element>(2);
                        chainedElements.add(localElement);
                        chainedElements.add(e);
                        ArrayList<TypeMirror> chainedTypes = new ArrayList<TypeMirror>(2);
                        chainedTypes.add(localElementType);
                        chainedTypes.add(this.asMemberOf(e, type, types));
                        this.results.add(this.javaCompletionItemFactory.createChainedMembersItem((CompilationInfo)env.getController(), chainedElements, chainedTypes, this.anchorOffset, elements.isDeprecated(localElement) || elements.isDeprecated(e), env.addSemicolon(), env.getWhiteList()));
                    }
                }
            }
        }

        private void addAllStaticMemberNames(Env env) {
            String prefix = env.getPrefix();
            if (prefix != null && prefix.length() > 0) {
                CompilationController controller = env.getController();
                Set<? extends Element> excludes = env.getExcludes();
                HashSet<ElementHandle> excludeHandles = null;
                if (excludes != null) {
                    excludeHandles = new HashSet<ElementHandle>(excludes.size());
                    for (Element el : excludes) {
                        excludeHandles.add(ElementHandle.create((Element)el));
                    }
                }
                ClassIndex.NameKind kind = Utilities.isCaseSensitive() ? ClassIndex.NameKind.PREFIX : ClassIndex.NameKind.CASE_INSENSITIVE_PREFIX;
                Iterable declaredSymbols = controller.getClasspathInfo().getClassIndex().getDeclaredSymbols(prefix, kind, EnumSet.allOf(ClassIndex.SearchScope.class));
                for (ClassIndex.Symbols symbols : declaredSymbols) {
                    if (Utilities.isExcludeMethods() && Utilities.isExcluded(symbols.getEnclosingType().getQualifiedName()) || excludeHandles != null && excludeHandles.contains((Object)symbols.getEnclosingType()) || JavaCompletionQuery.isAnnonInner(symbols.getEnclosingType())) continue;
                    for (String name : symbols.getSymbols()) {
                        this.results.add(this.javaCompletionItemFactory.createStaticMemberItem(symbols.getEnclosingType(), name, this.anchorOffset, env.addSemicolon(), env.getReferencesCount(), controller.getSnapshot().getSource(), env.getWhiteList()));
                    }
                }
            }
        }

        private void addMemberConstantsAndTypes(final Env env, TypeMirror type, Element elem) throws IOException {
            Set<? extends TypeMirror> smartTypes = this.queryType == 1 ? env.getSmartTypes() : null;
            CompilationController controller = env.getController();
            final Elements elements = controller.getElements();
            Types types = controller.getTypes();
            final Trees trees = controller.getTrees();
            TypeElement typeElem = type.getKind() == TypeKind.DECLARED ? (TypeElement)((DeclaredType)type).asElement() : null;
            final boolean isStatic = elem != null && (elem.getKind().isClass() || elem.getKind().isInterface() || elem.getKind() == ElementKind.TYPE_PARAMETER);
            final boolean isSuperCall = elem != null && elem.getKind().isField() && elem.getSimpleName().contentEquals("super");
            final Scope scope = env.getScope();
            TypeElement enclClass = scope.getEnclosingClass();
            final TypeMirror enclType = enclClass != null ? enclClass.asType() : null;
            ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                public boolean accept(Element e, TypeMirror t) {
                    if (!JavaCompletionQuery.this.startsWith(env, e.getSimpleName().toString()) || isStatic && !e.getModifiers().contains((Object)Modifier.STATIC)) {
                        return false;
                    }
                    switch (e.getKind()) {
                        case FIELD: {
                            if (((VariableElement)e).getConstantValue() == null && !"class".contentEquals(e.getSimpleName())) {
                                return false;
                            }
                        }
                        case ENUM_CONSTANT: {
                            return (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(e)) && trees.isAccessible(scope, e, (DeclaredType)(isSuperCall && enclType != null ? enclType : t));
                        }
                        case CLASS: 
                        case ENUM: 
                        case INTERFACE: {
                            return (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(e)) && trees.isAccessible(scope, e, (DeclaredType)t);
                        }
                    }
                    return false;
                }
            };
            for (Element e : controller.getElementUtilities().getMembers(type, acceptor)) {
                switch (e.getKind()) {
                    case ENUM_CONSTANT: 
                    case FIELD: {
                        String name = e.getSimpleName().toString();
                        if ("class".equals(name)) {
                            this.results.add(this.javaCompletionItemFactory.createKeywordItem(name, null, this.anchorOffset, false));
                            break;
                        }
                        TypeMirror tm = this.asMemberOf(e, type, types);
                        this.results.add(this.javaCompletionItemFactory.createVariableItem((CompilationInfo)env.getController(), (VariableElement)e, tm, this.anchorOffset, null, typeElem != e.getEnclosingElement(), elements.isDeprecated(e), this.isOfSmartType(env, tm, smartTypes), env.assignToVarPos(), env.getWhiteList()));
                        break;
                    }
                    case CLASS: 
                    case ENUM: 
                    case INTERFACE: {
                        DeclaredType dt = (DeclaredType)this.asMemberOf(e, type, types);
                        this.results.add(this.javaCompletionItemFactory.createTypeItem((CompilationInfo)env.getController(), (TypeElement)e, dt, this.anchorOffset, null, elements.isDeprecated(e), false, env.isInsideClass(), true, false, false, env.getWhiteList()));
                    }
                }
            }
        }

        private void addMethodReferences(final Env env, TypeMirror type, Element elem) throws IOException {
            Set<? extends TypeMirror> smartTypes = env.getSmartTypes();
            final String prefix = env.getPrefix();
            CompilationController controller = env.getController();
            final Elements elements = controller.getElements();
            Types types = controller.getTypes();
            TreeUtilities tu = controller.getTreeUtilities();
            TypeElement typeElem = type.getKind() == TypeKind.DECLARED ? (TypeElement)((DeclaredType)type).asElement() : null;
            boolean isThisCall = elem != null && elem.getKind().isField() && elem.getSimpleName().contentEquals("this");
            final boolean isSuperCall = elem != null && elem.getKind().isField() && elem.getSimpleName().contentEquals("super");
            final Scope scope = env.getScope();
            if ((isThisCall || isSuperCall) && tu.isStaticContext(scope)) {
                return;
            }
            ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                public boolean accept(Element e, TypeMirror t) {
                    switch (e.getKind()) {
                        case METHOD: {
                            String sn = e.getSimpleName().toString();
                            return !(!JavaCompletionQuery.this.startsWith(env, sn, prefix) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(e) || !env.isAccessible(scope, e, t, isSuperCall) || Utilities.isExcludeMethods() && Utilities.isExcluded(Utilities.getElementName(e.getEnclosingElement(), true) + "." + sn));
                        }
                    }
                    return false;
                }
            };
            for (Element e : controller.getElementUtilities().getMembers(type, acceptor)) {
                switch (e.getKind()) {
                    case METHOD: {
                        ExecutableType et = (ExecutableType)this.asMemberOf(e, type, types);
                        this.results.add(this.javaCompletionItemFactory.createExecutableItem((CompilationInfo)env.getController(), (ExecutableElement)e, et, this.anchorOffset, null, typeElem != e.getEnclosingElement(), elements.isDeprecated(e), false, false, this.isOfSmartType(env, et, smartTypes), env.assignToVarPos(), true, env.getWhiteList()));
                    }
                }
            }
        }

        private void addMembers(final Env env, TypeMirror type, final Element elem, final EnumSet<ElementKind> kinds, final DeclaredType baseType, final boolean inImport, boolean insideNew, boolean autoImport) throws IOException {
            Set<? extends TypeMirror> smartTypes = env.getSmartTypes();
            CompilationController controller = env.getController();
            final Trees trees = controller.getTrees();
            final Elements elements = controller.getElements();
            final ElementUtilities eu = controller.getElementUtilities();
            final Types types = controller.getTypes();
            TreeUtilities tu = controller.getTreeUtilities();
            TypeElement typeElem = type.getKind() == TypeKind.DECLARED ? (TypeElement)((DeclaredType)type).asElement() : null;
            final boolean isStatic = elem != null && (elem.getKind().isClass() || elem.getKind().isInterface() || elem.getKind() == ElementKind.TYPE_PARAMETER) && elem.asType().getKind() != TypeKind.ERROR;
            boolean isThisCall = elem != null && elem.getKind().isField() && elem.getSimpleName().contentEquals("this");
            final boolean isSuperCall = elem != null && elem.getKind().isField() && elem.getSimpleName().contentEquals("super");
            final Scope scope = env.getScope();
            if ((isThisCall || isSuperCall) && tu.isStaticContext(scope)) {
                return;
            }
            final boolean[] ctorSeen = new boolean[]{false};
            final boolean[] nestedClassSeen = new boolean[]{false};
            final TypeElement enclClass = scope.getEnclosingClass();
            ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                public boolean accept(Element e, TypeMirror t) {
                    switch (e.getKind()) {
                        case FIELD: {
                            if (!JavaCompletionQuery.this.startsWith(env, e.getSimpleName().toString())) {
                                return false;
                            }
                            if (e.getSimpleName().contentEquals("this") || e.getSimpleName().contentEquals("super")) {
                                TypeElement cls = enclClass;
                                while (cls != null) {
                                    if (cls == elem) {
                                        return JavaCompletionQuery.this.isOfKindAndType(JavaCompletionQuery.this.asMemberOf(e, t, types), e, kinds, baseType, scope, trees, types);
                                    }
                                    TypeElement outer = eu.enclosingTypeElement((Element)cls);
                                    cls = !cls.getModifiers().contains((Object)Modifier.STATIC) ? outer : null;
                                }
                                return false;
                            }
                            if (isStatic) {
                                if (!e.getModifiers().contains((Object)Modifier.STATIC) || e.getSimpleName().contentEquals("class") && elem.getKind() == ElementKind.TYPE_PARAMETER) {
                                    return false;
                                }
                            } else if (JavaCompletionQuery.this.queryType == 1 && e.getModifiers().contains((Object)Modifier.STATIC)) {
                                if ((Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(e)) && JavaCompletionQuery.this.isOfKindAndType(JavaCompletionQuery.this.asMemberOf(e, t, types), e, kinds, baseType, scope, trees, types) && env.isAccessible(scope, e, t, isSuperCall) && (isStatic && !inImport || !e.getSimpleName().contentEquals("class"))) {
                                    JavaCompletionQuery.this.hasAdditionalItems = 2;
                                }
                                return false;
                            }
                            return (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(e)) && JavaCompletionQuery.this.isOfKindAndType(JavaCompletionQuery.this.asMemberOf(e, t, types), e, kinds, baseType, scope, trees, types) && env.isAccessible(scope, e, t, isSuperCall) && (isStatic && !inImport || !e.getSimpleName().contentEquals("class"));
                        }
                        case ENUM_CONSTANT: 
                        case EXCEPTION_PARAMETER: 
                        case LOCAL_VARIABLE: 
                        case RESOURCE_VARIABLE: 
                        case PARAMETER: {
                            return JavaCompletionQuery.this.startsWith(env, e.getSimpleName().toString()) && (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(e)) && JavaCompletionQuery.this.isOfKindAndType(JavaCompletionQuery.this.asMemberOf(e, t, types), e, kinds, baseType, scope, trees, types) && env.isAccessible(scope, e, t, isSuperCall);
                        }
                        case METHOD: {
                            String sn = e.getSimpleName().toString();
                            if (isStatic) {
                                if (!e.getModifiers().contains((Object)Modifier.STATIC)) {
                                    return false;
                                }
                            } else if (JavaCompletionQuery.this.queryType == 1 && e.getModifiers().contains((Object)Modifier.STATIC)) {
                                if (!(!JavaCompletionQuery.this.startsWith(env, sn) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(e) || !JavaCompletionQuery.this.isOfKindAndType(((ExecutableType)JavaCompletionQuery.this.asMemberOf(e, t, types)).getReturnType(), e, kinds, baseType, scope, trees, types) || !env.isAccessible(scope, e, t, isSuperCall) || Utilities.isExcludeMethods() && Utilities.isExcluded(Utilities.getElementName(e.getEnclosingElement(), true) + "." + sn))) {
                                    JavaCompletionQuery.this.hasAdditionalItems = 2;
                                }
                                return false;
                            }
                            return !(!JavaCompletionQuery.this.startsWith(env, sn) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(e) || !JavaCompletionQuery.this.isOfKindAndType(((ExecutableType)JavaCompletionQuery.this.asMemberOf(e, t, types)).getReturnType(), e, kinds, baseType, scope, trees, types) || !env.isAccessible(scope, e, t, isSuperCall) || Utilities.isExcludeMethods() && Utilities.isExcluded(Utilities.getElementName(e.getEnclosingElement(), true) + "." + sn));
                        }
                        case ANNOTATION_TYPE: 
                        case CLASS: 
                        case ENUM: 
                        case INTERFACE: {
                            if (!e.getModifiers().contains((Object)Modifier.STATIC)) {
                                nestedClassSeen[0] = true;
                            }
                            return !(!JavaCompletionQuery.this.startsWith(env, e.getSimpleName().toString()) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(e) || !JavaCompletionQuery.this.isOfKindAndType(e.asType(), e, kinds, baseType, scope, trees, types) || env.isAfterExtends() && !JavaCompletionQuery.this.containsAccessibleNonFinalType(e, scope, trees) || !env.isAccessible(scope, e, t, isSuperCall) || !isStatic);
                        }
                        case CONSTRUCTOR: {
                            ctorSeen[0] = true;
                            return (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(e)) && JavaCompletionQuery.this.isOfKindAndType(e.getEnclosingElement().asType(), e, kinds, baseType, scope, trees, types) && (env.isAccessible(scope, e, t, isSuperCall) || elem.getModifiers().contains((Object)Modifier.ABSTRACT) && !e.getModifiers().contains((Object)Modifier.PRIVATE)) && isStatic;
                        }
                    }
                    return false;
                }
            };
            for (Element e : controller.getElementUtilities().getMembers(type, acceptor)) {
                switch (e.getKind()) {
                    ExecutableType et;
                    case ENUM_CONSTANT: 
                    case FIELD: 
                    case EXCEPTION_PARAMETER: 
                    case LOCAL_VARIABLE: 
                    case RESOURCE_VARIABLE: 
                    case PARAMETER: {
                        String name = e.getSimpleName().toString();
                        if ("this".equals(name) || "class".equals(name) || "super".equals(name)) {
                            this.results.add(this.javaCompletionItemFactory.createKeywordItem(name, null, this.anchorOffset, this.isOfSmartType(env, e.asType(), smartTypes)));
                            break;
                        }
                        TypeMirror tm = this.asMemberOf(e, type, types);
                        this.results.add(this.javaCompletionItemFactory.createVariableItem((CompilationInfo)env.getController(), (VariableElement)e, tm, this.anchorOffset, autoImport ? env.getReferencesCount() : null, typeElem != e.getEnclosingElement(), elements.isDeprecated(e), this.isOfSmartType(env, tm, smartTypes), env.assignToVarPos(), env.getWhiteList()));
                        break;
                    }
                    case CONSTRUCTOR: {
                        et = (ExecutableType)this.asMemberOf(e, type, types);
                        this.results.add(this.javaCompletionItemFactory.createExecutableItem((CompilationInfo)env.getController(), (ExecutableElement)e, et, this.anchorOffset, autoImport ? env.getReferencesCount() : null, typeElem != e.getEnclosingElement(), elements.isDeprecated(e), inImport, false, this.isOfSmartType(env, type, smartTypes), env.assignToVarPos(), false, env.getWhiteList()));
                        break;
                    }
                    case METHOD: {
                        et = (ExecutableType)this.asMemberOf(e, type, types);
                        this.results.add(this.javaCompletionItemFactory.createExecutableItem((CompilationInfo)env.getController(), (ExecutableElement)e, et, this.anchorOffset, autoImport ? env.getReferencesCount() : null, typeElem != e.getEnclosingElement(), elements.isDeprecated(e), inImport, env.addSemicolon(), this.isOfSmartType(env, this.getCorrectedReturnType(env, et, (ExecutableElement)e, type), smartTypes), env.assignToVarPos(), false, env.getWhiteList()));
                        break;
                    }
                    case ANNOTATION_TYPE: 
                    case CLASS: 
                    case ENUM: 
                    case INTERFACE: {
                        DeclaredType dt = (DeclaredType)this.asMemberOf(e, type, types);
                        this.results.add(this.javaCompletionItemFactory.createTypeItem((CompilationInfo)env.getController(), (TypeElement)e, dt, this.anchorOffset, null, elements.isDeprecated(e), insideNew, insideNew || env.isInsideClass(), true, this.isOfSmartType(env, dt, smartTypes), autoImport, env.getWhiteList()));
                    }
                }
            }
            if (!ctorSeen[0] && kinds.contains((Object)ElementKind.CONSTRUCTOR) && elem.getKind().isInterface()) {
                this.results.add(this.javaCompletionItemFactory.createDefaultConstructorItem((TypeElement)elem, this.anchorOffset, this.isOfSmartType(env, type, smartTypes)));
            }
            if (isStatic && enclClass != null && elem.getKind().isInterface() && env.getController().getSourceVersion().compareTo(SourceVersion.RELEASE_8) >= 0) {
                for (TypeMirror iface : enclClass.getInterfaces()) {
                    if (((DeclaredType)iface).asElement() != elem) continue;
                    this.results.add(this.javaCompletionItemFactory.createKeywordItem("super", null, this.anchorOffset, this.isOfSmartType(env, type, smartTypes)));
                    break;
                }
            }
            if (!isStatic && nestedClassSeen[0]) {
                this.addKeyword(env, "new", " ", false);
            }
        }

        private void addThisOrSuperConstructor(Env env, TypeMirror type, final Element elem, String name, final ExecutableElement toExclude) throws IOException {
            CompilationController controller = env.getController();
            final Elements elements = controller.getElements();
            Types types = controller.getTypes();
            final Trees trees = controller.getTrees();
            final Scope scope = env.getScope();
            final boolean[] ctorSeen = new boolean[]{false};
            ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                public boolean accept(Element e, TypeMirror t) {
                    switch (e.getKind()) {
                        case CONSTRUCTOR: {
                            ctorSeen[0] = true;
                            return toExclude != e && (Utilities.isShowDeprecatedMembers() || !elements.isDeprecated(e)) && (trees.isAccessible(scope, e, (DeclaredType)t) || elem.getModifiers().contains((Object)Modifier.ABSTRACT) && !e.getModifiers().contains((Object)Modifier.PRIVATE));
                        }
                    }
                    return false;
                }
            };
            for (Element e : controller.getElementUtilities().getMembers(type, acceptor)) {
                if (e.getKind() != ElementKind.CONSTRUCTOR) continue;
                ExecutableType et = (ExecutableType)this.asMemberOf(e, type, types);
                this.results.add(this.javaCompletionItemFactory.createThisOrSuperConstructorItem((CompilationInfo)env.getController(), (ExecutableElement)e, et, this.anchorOffset, elements.isDeprecated(e), name, env.getWhiteList()));
            }
        }

        private void addEnumConstants(Env env, TypeElement elem) {
            Elements elements = env.getController().getElements();
            for (Element e : elem.getEnclosedElements()) {
                String name;
                if (e.getKind() != ElementKind.ENUM_CONSTANT || !this.startsWith(env, name = e.getSimpleName().toString()) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(e)) continue;
                this.results.add(this.javaCompletionItemFactory.createVariableItem((CompilationInfo)env.getController(), (VariableElement)e, e.asType(), this.anchorOffset, null, false, elements.isDeprecated(e), false, env.assignToVarPos(), env.getWhiteList()));
            }
        }

        private void addPackageContent(Env env, PackageElement pe, EnumSet<ElementKind> kinds, DeclaredType baseType, boolean insideNew, boolean insidePkgStmt) throws IOException {
            Set<? extends TypeMirror> smartTypes = this.queryType == 1 ? env.getSmartTypes() : null;
            CompilationController controller = env.getController();
            Elements elements = controller.getElements();
            Types types = controller.getTypes();
            Trees trees = controller.getTrees();
            Scope scope = env.getScope();
            for (Element e : pe.getEnclosedElements()) {
                if (!e.getKind().isClass() && !e.getKind().isInterface()) continue;
                String name = e.getSimpleName().toString();
                if (env.getExcludes() != null && env.getExcludes().contains(e) || !this.startsWith(env, name) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(e) || !trees.isAccessible(scope, (TypeElement)e) || !this.isOfKindAndType(e.asType(), e, kinds, baseType, scope, trees, types) || Utilities.isExcluded(Utilities.getElementName(e, true))) continue;
                this.results.add(this.javaCompletionItemFactory.createTypeItem((CompilationInfo)env.getController(), (TypeElement)e, (DeclaredType)e.asType(), this.anchorOffset, null, elements.isDeprecated(e), insideNew, insideNew || env.isInsideClass(), true, this.isOfSmartType(env, e.asType(), smartTypes), false, env.getWhiteList()));
            }
            String pkgName = pe.getQualifiedName() + ".";
            this.addPackages(env, pkgName, insidePkgStmt);
        }

        private void addPackages(Env env, String fqnPrefix, boolean inPkgStmt) {
            if (fqnPrefix == null) {
                fqnPrefix = "";
            }
            String prefix = env.getPrefix() != null ? fqnPrefix + env.getPrefix() : null;
            for (String pkgName : env.getController().getClasspathInfo().getClassIndex().getPackageNames(fqnPrefix, true, EnumSet.allOf(ClassIndex.SearchScope.class))) {
                if (!this.startsWith(env, pkgName, prefix) || Utilities.isExcluded(pkgName + ".")) continue;
                this.results.add(this.javaCompletionItemFactory.createPackageItem(pkgName, this.anchorOffset, inPkgStmt));
            }
        }

        private void addTypes(Env env, EnumSet<ElementKind> kinds, DeclaredType baseType) throws IOException {
            if (this.queryType == 9) {
                if (baseType == null) {
                    this.addAllTypes(env, kinds);
                } else {
                    Elements elements = env.getController().getElements();
                    Set<? extends Element> excludes = env.getExcludes();
                    for (DeclaredType subtype : this.getSubtypesOf(env, baseType)) {
                        TypeElement elem = (TypeElement)subtype.asElement();
                        if (excludes != null && excludes.contains(elem) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(elem) || env.isAfterExtends() && elem.getModifiers().contains((Object)Modifier.FINAL)) continue;
                        this.results.add(this.javaCompletionItemFactory.createTypeItem((CompilationInfo)env.getController(), elem, subtype, this.anchorOffset, env.getReferencesCount(), elements.isDeprecated(elem), env.isInsideNew(), env.isInsideNew() || env.isInsideClass(), false, true, false, env.getWhiteList()));
                    }
                }
            } else {
                this.addLocalAndImportedTypes(env, kinds, baseType);
                this.hasAdditionalItems = 1;
            }
            this.addPackages(env, null, false);
        }

        private void addLocalAndImportedTypes(final Env env, final EnumSet<ElementKind> kinds, final DeclaredType baseType) throws IOException {
            CompilationController controller = env.getController();
            final Trees trees = controller.getTrees();
            final Elements elements = controller.getElements();
            final Types types = controller.getTypes();
            TreeUtilities tu = controller.getTreeUtilities();
            final Scope scope = env.getScope();
            final ExecutableElement enclMethod = scope.getEnclosingMethod();
            TypeElement enclClass = scope.getEnclosingClass();
            final boolean isStatic = enclClass == null ? false : tu.isStaticContext(scope) || env.getPath().getLeaf().getKind() == Tree.Kind.BLOCK && ((BlockTree)env.getPath().getLeaf()).isStatic();
            Object acceptor = new ElementUtilities.ElementAcceptor(){

                public boolean accept(Element e, TypeMirror t) {
                    if (!(env.getExcludes() != null && env.getExcludes().contains(e) || !e.getKind().isClass() && !e.getKind().isInterface() && e.getKind() != ElementKind.TYPE_PARAMETER || env.isAfterExtends() && !JavaCompletionQuery.this.containsAccessibleNonFinalType(e, scope, trees))) {
                        String name = e.getSimpleName().toString();
                        return !(name.length() <= 0 || Character.isDigit(name.charAt(0)) || !JavaCompletionQuery.this.startsWith(env, name) || isStatic && !e.getModifiers().contains((Object)Modifier.STATIC) && e.getEnclosingElement() != enclMethod || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(e) || !JavaCompletionQuery.this.isOfKindAndType(e.asType(), e, kinds, baseType, scope, trees, types));
                    }
                    return false;
                }
            };
            for (Element e2 : controller.getElementUtilities().getLocalMembersAndVars(scope, acceptor)) {
                switch (e2.getKind()) {
                    case ANNOTATION_TYPE: 
                    case CLASS: 
                    case ENUM: 
                    case INTERFACE: {
                        this.results.add(this.javaCompletionItemFactory.createTypeItem((CompilationInfo)env.getController(), (TypeElement)e2, (DeclaredType)e2.asType(), this.anchorOffset, null, elements.isDeprecated(e2), env.isInsideNew(), env.isInsideNew() || env.isInsideClass(), false, false, false, env.getWhiteList()));
                        env.addToExcludes(e2);
                        break;
                    }
                    case TYPE_PARAMETER: {
                        this.results.add(this.javaCompletionItemFactory.createTypeParameterItem((TypeParameterElement)e2, this.anchorOffset));
                    }
                }
            }
            acceptor = new ElementUtilities.ElementAcceptor(){

                public boolean accept(Element e, TypeMirror t) {
                    if (e.getKind().isClass() || e.getKind().isInterface()) {
                        return !(env.getExcludes() != null && env.getExcludes().contains(e) || !JavaCompletionQuery.this.startsWith(env, e.getSimpleName().toString()) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(e) || !trees.isAccessible(scope, (TypeElement)e) || !JavaCompletionQuery.this.isOfKindAndType(e.asType(), e, kinds, baseType, scope, trees, types) || env.isAfterExtends() && !JavaCompletionQuery.this.containsAccessibleNonFinalType(e, scope, trees));
                    }
                    return false;
                }
            };
            for (Element e2 : controller.getElementUtilities().getGlobalTypes(acceptor)) {
                this.results.add(this.javaCompletionItemFactory.createTypeItem((CompilationInfo)env.getController(), (TypeElement)e2, (DeclaredType)e2.asType(), this.anchorOffset, null, elements.isDeprecated(e2), env.isInsideNew(), env.isInsideNew() || env.isInsideClass(), false, false, false, env.getWhiteList()));
            }
        }

        private void addAllTypes(Env env, EnumSet<ElementKind> kinds) {
            String prefix = env.getPrefix();
            CompilationController controller = env.getController();
            Set<? extends Element> excludes = env.getExcludes();
            HashSet<ElementHandle> excludeHandles = null;
            if (excludes != null) {
                excludeHandles = new HashSet<ElementHandle>(excludes.size());
                for (Element el : excludes) {
                    excludeHandles.add(ElementHandle.create((Element)el));
                }
            }
            if (!kinds.contains((Object)ElementKind.CLASS) && !kinds.contains((Object)ElementKind.INTERFACE)) {
                Set declaredTypes = controller.getClasspathInfo().getClassIndex().getDeclaredTypes("", ClassIndex.NameKind.PREFIX, EnumSet.allOf(ClassIndex.SearchScope.class));
                HashMap<String, ElementHandle> removed = new HashMap<String, ElementHandle>(declaredTypes.size());
                HashSet<String> doNotRemove = new HashSet<String>();
                block1 : for (ElementHandle name : declaredTypes) {
                    int idx;
                    if (excludeHandles != null && excludeHandles.contains((Object)name) || JavaCompletionQuery.isAnnonInner(name)) continue;
                    if (!kinds.contains((Object)name.getKind()) && !doNotRemove.contains(name.getQualifiedName())) {
                        removed.put(name.getQualifiedName(), name);
                        continue;
                    }
                    String qName = name.getQualifiedName();
                    String sName = null;
                    while ((idx = qName.lastIndexOf(46)) > 0) {
                        if (sName == null) {
                            sName = qName.substring(idx + 1);
                            if (sName.length() <= 0 || !this.startsWith(env, sName, prefix)) continue block1;
                            this.results.add(this.javaCompletionItemFactory.createTypeItem(name, kinds, this.anchorOffset, env.getReferencesCount(), controller.getSnapshot().getSource(), env.isInsideNew(), env.isInsideNew() || env.isInsideClass(), env.afterExtends, env.getWhiteList()));
                        }
                        qName = qName.substring(0, idx);
                        doNotRemove.add(qName);
                        ElementHandle r = (ElementHandle)removed.remove(qName);
                        if (r == null) continue;
                        this.results.add(this.javaCompletionItemFactory.createTypeItem(r, kinds, this.anchorOffset, env.getReferencesCount(), controller.getSnapshot().getSource(), env.isInsideNew(), env.isInsideNew() || env.isInsideClass(), env.afterExtends, env.getWhiteList()));
                    }
                }
            } else {
                ClassIndex.NameKind kind;
                String subwordsPattern = null;
                if (prefix != null && !env.isCamelCasePrefix() && Utilities.isSubwordSensitive()) {
                    subwordsPattern = Utilities.createSubwordsPattern(prefix);
                }
                ClassIndex.NameKind nameKind = env.isCamelCasePrefix() ? (Utilities.isCaseSensitive() ? ClassIndex.NameKind.CAMEL_CASE : ClassIndex.NameKind.CAMEL_CASE_INSENSITIVE) : (subwordsPattern != null ? ClassIndex.NameKind.REGEXP : (kind = Utilities.isCaseSensitive() ? ClassIndex.NameKind.PREFIX : ClassIndex.NameKind.CASE_INSENSITIVE_PREFIX));
                Set declaredTypes = controller.getClasspathInfo().getClassIndex().getDeclaredTypes(subwordsPattern != null ? subwordsPattern : (prefix != null ? prefix : ""), kind, EnumSet.allOf(ClassIndex.SearchScope.class));
                this.results.ensureCapacity(this.results.size() + declaredTypes.size());
                for (ElementHandle name : declaredTypes) {
                    if (excludeHandles != null && excludeHandles.contains((Object)name) || JavaCompletionQuery.isAnnonInner(name)) continue;
                    this.results.add(this.javaCompletionItemFactory.createTypeItem(name, kinds, this.anchorOffset, env.getReferencesCount(), controller.getSnapshot().getSource(), env.isInsideNew(), env.isInsideNew() || env.isInsideClass(), env.afterExtends, env.getWhiteList()));
                }
            }
        }

        private Set<DeclaredType> getSupertypesOf(Env env, DeclaredType type) {
            LinkedList<DeclaredType> bases = new LinkedList<DeclaredType>();
            bases.add(type);
            HashSet<DeclaredType> ret = new HashSet<DeclaredType>();
            while (!bases.isEmpty()) {
                TypeMirror sup;
                DeclaredType head = (DeclaredType)bases.remove();
                TypeElement elem = (TypeElement)head.asElement();
                if (this.startsWith(env, elem.getSimpleName().toString())) {
                    ret.add(head);
                }
                if ((sup = elem.getSuperclass()).getKind() == TypeKind.DECLARED) {
                    bases.add((DeclaredType)sup);
                }
                for (TypeMirror iface : elem.getInterfaces()) {
                    if (iface.getKind() != TypeKind.DECLARED) continue;
                    bases.add((DeclaredType)iface);
                }
            }
            return ret;
        }

        private List<DeclaredType> getSubtypesOf(Env env, DeclaredType baseType) throws IOException {
            if (((TypeElement)baseType.asElement()).getQualifiedName().contentEquals("java.lang.Object")) {
                return Collections.emptyList();
            }
            LinkedList<DeclaredType> subtypes = new LinkedList<DeclaredType>();
            String prefix = env.getPrefix();
            CompilationController controller = env.getController();
            Types types = controller.getTypes();
            Trees trees = controller.getTrees();
            Scope scope = env.getScope();
            if (prefix != null && prefix.length() > 2 && baseType.getTypeArguments().isEmpty()) {
                String subwordsPattern = null;
                if (prefix != null && !env.isCamelCasePrefix() && Utilities.isSubwordSensitive()) {
                    subwordsPattern = Utilities.createSubwordsPattern(prefix);
                }
                ClassIndex.NameKind kind = env.isCamelCasePrefix() ? (Utilities.isCaseSensitive() ? ClassIndex.NameKind.CAMEL_CASE : ClassIndex.NameKind.CAMEL_CASE_INSENSITIVE) : (subwordsPattern != null ? ClassIndex.NameKind.REGEXP : (Utilities.isCaseSensitive() ? ClassIndex.NameKind.PREFIX : ClassIndex.NameKind.CASE_INSENSITIVE_PREFIX));
                for (ElementHandle handle : controller.getClasspathInfo().getClassIndex().getDeclaredTypes(subwordsPattern != null ? subwordsPattern : prefix, kind, EnumSet.allOf(ClassIndex.SearchScope.class))) {
                    TypeElement te = (TypeElement)handle.resolve((CompilationInfo)controller);
                    if (te == null || !trees.isAccessible(scope, te) || !types.isSubtype(types.getDeclaredType(te, new TypeMirror[0]), baseType)) continue;
                    subtypes.add(types.getDeclaredType(te, new TypeMirror[0]));
                }
            } else {
                HashSet<TypeElement> elems = new HashSet<TypeElement>();
                LinkedList<DeclaredType> bases = new LinkedList<DeclaredType>();
                bases.add(baseType);
                ClassIndex index = controller.getClasspathInfo().getClassIndex();
                while (!bases.isEmpty()) {
                    List<? extends TypeMirror> tas;
                    DeclaredType head = (DeclaredType)bases.remove();
                    TypeElement elem = (TypeElement)head.asElement();
                    if (!elems.add(elem)) continue;
                    if (this.startsWith(env, elem.getSimpleName().toString())) {
                        subtypes.add(head);
                    }
                    boolean isRaw = !(tas = head.getTypeArguments()).iterator().hasNext();
                    block2 : for (ElementHandle eh : index.getElements(ElementHandle.create((Element)elem), EnumSet.of(ClassIndex.SearchKind.IMPLEMENTORS), EnumSet.allOf(ClassIndex.SearchScope.class))) {
                        TypeElement e = (TypeElement)eh.resolve((CompilationInfo)controller);
                        if (e != null) {
                            if (!trees.isAccessible(scope, e)) continue;
                            if (isRaw) {
                                DeclaredType dt = types.getDeclaredType(e, new TypeMirror[0]);
                                bases.add(dt);
                                continue;
                            }
                            HashMap<Element, TypeMirror> map = new HashMap<Element, TypeMirror>();
                            TypeMirror sup = e.getSuperclass();
                            if (sup.getKind() == TypeKind.DECLARED && ((DeclaredType)sup).asElement() == elem) {
                                DeclaredType dt = (DeclaredType)sup;
                                Iterator<? extends TypeMirror> ittas = tas.iterator();
                                Iterator<? extends TypeMirror> it = dt.getTypeArguments().iterator();
                                while (it.hasNext() && ittas.hasNext()) {
                                    TypeMirror stm;
                                    TypeMirror basetm = ittas.next();
                                    if (basetm == (stm = it.next())) continue;
                                    if (stm.getKind() != TypeKind.TYPEVAR) continue block2;
                                    map.put(((TypeVariable)stm).asElement(), basetm);
                                }
                                if (it.hasNext() != ittas.hasNext()) {
                                    continue;
                                }
                            } else {
                                for (TypeMirror tm : e.getInterfaces()) {
                                    if (((DeclaredType)tm).asElement() != elem) continue;
                                    DeclaredType dt = (DeclaredType)tm;
                                    Iterator<? extends TypeMirror> ittas = tas.iterator();
                                    Iterator<? extends TypeMirror> it = dt.getTypeArguments().iterator();
                                    while (it.hasNext() && ittas.hasNext()) {
                                        TypeMirror stm;
                                        TypeMirror basetm = ittas.next();
                                        if (basetm == (stm = it.next())) continue;
                                        if (stm.getKind() != TypeKind.TYPEVAR) continue block2;
                                        map.put(((TypeVariable)stm).asElement(), basetm);
                                    }
                                    if (it.hasNext() != ittas.hasNext()) continue block2;
                                }
                            }
                            bases.add(this.getDeclaredType(e, map, types));
                            continue;
                        }
                        Logger.getLogger("global").log(Level.FINE, String.format("Cannot resolve: %s on bootpath: %s classpath: %s sourcepath: %s\n", new Object[]{eh.toString(), controller.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.BOOT), controller.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.COMPILE), controller.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.SOURCE)}));
                    }
                }
            }
            return subtypes;
        }

        private void addMethodArguments(Env env, MethodInvocationTree mit) throws IOException {
            CompilationController controller = env.getController();
            TreePath path = env.getPath();
            CompilationUnitTree root = env.getRoot();
            SourcePositions sourcePositions = env.getSourcePositions();
            List<Tree> argTypes = this.getArgumentsUpToPos(env, mit.getArguments(), (int)sourcePositions.getEndPosition(root, (Tree)mit.getMethodSelect()), env.getOffset(), true);
            if (argTypes != null) {
                controller.toPhase(JavaSource.Phase.RESOLVED);
                TypeMirror[] types = new TypeMirror[argTypes.size()];
                int j = 0;
                for (Tree t : argTypes) {
                    types[j++] = controller.getTrees().getTypeMirror(new TreePath(path, t));
                }
                List<Pair<ExecutableElement, ExecutableType>> methods = null;
                String name = null;
                ExpressionTree mid = mit.getMethodSelect();
                path = new TreePath(path, (Tree)mid);
                switch (mid.getKind()) {
                    boolean isStatic;
                    case MEMBER_SELECT: {
                        ExpressionTree exp = ((MemberSelectTree)mid).getExpression();
                        path = new TreePath(path, (Tree)exp);
                        final Trees trees = controller.getTrees();
                        TypeMirror type = trees.getTypeMirror(path);
                        Element element = trees.getElement(path);
                        isStatic = element != null && (element.getKind().isClass() || element.getKind().isInterface() || element.getKind() == ElementKind.TYPE_PARAMETER);
                        final boolean isSuperCall = element != null && element.getKind().isField() && element.getSimpleName().contentEquals("super");
                        final Scope scope = env.getScope();
                        TypeElement enclClass = scope.getEnclosingClass();
                        final TypeMirror enclType = enclClass != null ? enclClass.asType() : null;
                        ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                            public boolean accept(Element e, TypeMirror t) {
                                return !(isStatic && !e.getModifiers().contains((Object)Modifier.STATIC) && e.getKind() != ElementKind.CONSTRUCTOR || t.getKind() == TypeKind.DECLARED && !trees.isAccessible(scope, e, (DeclaredType)(isSuperCall && enclType != null ? enclType : t)));
                            }
                        };
                        methods = this.getMatchingExecutables(type, controller.getElementUtilities().getMembers(type, acceptor), ((MemberSelectTree)mid).getIdentifier().toString(), types, controller.getTypes());
                        break;
                    }
                    case IDENTIFIER: {
                        final Scope scope = env.getScope();
                        TreeUtilities tu = controller.getTreeUtilities();
                        final Trees trees = controller.getTrees();
                        TypeElement enclClass = scope.getEnclosingClass();
                        isStatic = enclClass != null ? tu.isStaticContext(scope) || env.getPath().getLeaf().getKind() == Tree.Kind.BLOCK && ((BlockTree)env.getPath().getLeaf()).isStatic() : false;
                        final Collection<? extends Element> illegalForwardRefs = env.getForwardReferences();
                        final ExecutableElement method = scope.getEnclosingMethod();
                        ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                            public boolean accept(Element e, TypeMirror t) {
                                switch (e.getKind()) {
                                    case EXCEPTION_PARAMETER: 
                                    case LOCAL_VARIABLE: 
                                    case RESOURCE_VARIABLE: 
                                    case PARAMETER: {
                                        return (method == e.getEnclosingElement() || e.getModifiers().contains((Object)Modifier.FINAL)) && !illegalForwardRefs.contains(e);
                                    }
                                    case FIELD: {
                                        if (illegalForwardRefs.contains(e)) {
                                            return false;
                                        }
                                        if (!e.getSimpleName().contentEquals("this") && !e.getSimpleName().contentEquals("super")) break;
                                        return !isStatic;
                                    }
                                }
                                return (!isStatic || e.getModifiers().contains((Object)Modifier.STATIC)) && trees.isAccessible(scope, e, (DeclaredType)t);
                            }
                        };
                        name = ((IdentifierTree)mid).getName().toString();
                        if ("super".equals(name) && enclClass != null) {
                            TypeMirror superclass = enclClass.getSuperclass();
                            methods = this.getMatchingExecutables(superclass, controller.getElementUtilities().getMembers(superclass, acceptor), "<init>", types, controller.getTypes());
                            break;
                        }
                        if ("this".equals(name) && enclClass != null) {
                            TypeMirror thisclass = enclClass.asType();
                            methods = this.getMatchingExecutables(thisclass, controller.getElementUtilities().getMembers(thisclass, acceptor), "<init>", types, controller.getTypes());
                            break;
                        }
                        methods = this.getMatchingExecutables(enclClass != null ? enclClass.asType() : null, controller.getElementUtilities().getLocalMembersAndVars(scope, acceptor), name, types, controller.getTypes());
                        name = null;
                        break;
                    }
                }
                if (methods != null) {
                    Elements elements = controller.getElements();
                    for (Pair method : methods) {
                        if (!Utilities.isShowDeprecatedMembers() && elements.isDeprecated((Element)method.a)) continue;
                        this.results.add(this.javaCompletionItemFactory.createParametersItem((CompilationInfo)env.getController(), (ExecutableElement)method.a, (ExecutableType)method.b, this.anchorOffset, elements.isDeprecated((Element)method.a), types.length, name));
                    }
                }
            }
        }

        private void addConstructorArguments(Env env, NewClassTree nct) throws IOException {
            CompilationController controller = env.getController();
            TreePath path = env.getPath();
            CompilationUnitTree root = env.getRoot();
            SourcePositions sourcePositions = env.getSourcePositions();
            List<Tree> argTypes = this.getArgumentsUpToPos(env, nct.getArguments(), (int)sourcePositions.getEndPosition(root, (Tree)nct.getIdentifier()), env.getOffset(), true);
            if (argTypes != null) {
                controller.toPhase(JavaSource.Phase.RESOLVED);
                TypeMirror[] types = new TypeMirror[argTypes.size()];
                int j = 0;
                for (Tree t : argTypes) {
                    types[j++] = controller.getTrees().getTypeMirror(new TreePath(path, t));
                }
                path = new TreePath(path, (Tree)nct.getIdentifier());
                final Trees trees = controller.getTrees();
                TypeMirror type = trees.getTypeMirror(path);
                Element el = trees.getElement(path);
                final Scope scope = env.getScope();
                final boolean isAnonymous = nct.getClassBody() != null || el != null && (el.getKind().isInterface() || el.getModifiers().contains((Object)Modifier.ABSTRACT));
                ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                    public boolean accept(Element e, TypeMirror t) {
                        return e.getKind() == ElementKind.CONSTRUCTOR && (trees.isAccessible(scope, e, (DeclaredType)t) || isAnonymous && e.getModifiers().contains((Object)Modifier.PROTECTED));
                    }
                };
                List<Pair<ExecutableElement, ExecutableType>> ctors = this.getMatchingExecutables(type, controller.getElementUtilities().getMembers(type, acceptor), "<init>", types, controller.getTypes());
                Elements elements = controller.getElements();
                for (Pair<ExecutableElement, ExecutableType> ctor : ctors) {
                    if (!Utilities.isShowDeprecatedMembers() && elements.isDeprecated((Element)ctor.a)) continue;
                    this.results.add(this.javaCompletionItemFactory.createParametersItem((CompilationInfo)env.getController(), (ExecutableElement)ctor.a, (ExecutableType)ctor.b, this.anchorOffset, elements.isDeprecated((Element)ctor.a), types.length, null));
                }
            }
        }

        private void addAttributeValues(Env env, Element element, AnnotationMirror annotation, ExecutableElement member) throws IOException {
            CompilationController controller = env.getController();
            TreeUtilities tu = controller.getTreeUtilities();
            ElementUtilities eu = controller.getElementUtilities();
            for (javax.annotation.processing.Completion completion : SourceUtils.getAttributeValueCompletions((CompilationInfo)controller, (Element)element, (AnnotationMirror)annotation, (ExecutableElement)member, (String)env.getPrefix())) {
                Name fqn;
                String value = completion.getValue().trim();
                if (value.length() <= 0 || !this.startsWith(env, value)) continue;
                TypeMirror type = member.getReturnType();
                TypeElement typeElement = null;
                while (type.getKind() == TypeKind.ARRAY) {
                    type = ((ArrayType)type).getComponentType();
                }
                if (type.getKind() == TypeKind.DECLARED && "java.lang.Class".contentEquals(fqn = ((TypeElement)((DeclaredType)type).asElement()).getQualifiedName())) {
                    String name = value.endsWith(".class") ? value.substring(0, value.length() - 6) : value;
                    TypeMirror tm = tu.parseType(name, eu.outermostTypeElement(element));
                    TypeElement typeElement2 = typeElement = tm != null && tm.getKind() == TypeKind.DECLARED ? (TypeElement)((DeclaredType)tm).asElement() : null;
                    if (typeElement != null && this.startsWith(env, typeElement.getSimpleName().toString())) {
                        env.addToExcludes(typeElement);
                    }
                }
                this.results.add(this.javaCompletionItemFactory.createAttributeValueItem((CompilationInfo)env.getController(), value, completion.getMessage(), typeElement, this.anchorOffset, env.getReferencesCount(), env.getWhiteList()));
            }
        }

        private void addKeyword(Env env, String kw, String postfix, boolean smartType) {
            if (Utilities.startsWith(kw, env.getPrefix())) {
                this.results.add(this.javaCompletionItemFactory.createKeywordItem(kw, postfix, this.anchorOffset, smartType));
            }
        }

        private void addKeywordsForCU(Env env) {
            ArrayList<String> kws = new ArrayList<String>();
            int offset = env.getOffset();
            String prefix = env.getPrefix();
            CompilationUnitTree cu = env.getRoot();
            SourcePositions sourcePositions = env.getSourcePositions();
            kws.add("abstract");
            kws.add("class");
            kws.add("enum");
            kws.add("final");
            kws.add("interface");
            boolean beforeAnyClass = true;
            boolean beforePublicClass = true;
            for (Tree t : cu.getTypeDecls()) {
                int pos;
                if (!TreeUtilities.CLASS_TREE_KINDS.contains((Object)t.getKind()) || (long)(pos = (int)sourcePositions.getEndPosition(cu, t)) == -1 || offset < pos) continue;
                beforeAnyClass = false;
                if (!((ClassTree)t).getModifiers().getFlags().contains((Object)Modifier.PUBLIC)) continue;
                beforePublicClass = false;
                break;
            }
            if (beforePublicClass) {
                kws.add("public");
            }
            if (beforeAnyClass) {
                ExpressionTree pd;
                kws.add("import");
                Tree firstImport = null;
                Iterator i$ = cu.getImports().iterator();
                if (i$.hasNext()) {
                    Tree t2;
                    firstImport = t2 = (Tree)i$.next();
                }
                if ((pd = cu.getPackageName()) != null && (long)offset <= sourcePositions.getStartPosition(cu, (Tree)cu) || pd == null && (firstImport == null || sourcePositions.getStartPosition(cu, firstImport) >= (long)offset)) {
                    kws.add("package");
                }
            }
            for (String kw : kws) {
                if (!Utilities.startsWith(kw, prefix)) continue;
                this.results.add(this.javaCompletionItemFactory.createKeywordItem(kw, " ", this.anchorOffset, false));
            }
        }

        private void addKeywordsForClassBody(Env env) {
            String prefix = env.getPrefix();
            for (String kw : CLASS_BODY_KEYWORDS) {
                if (!Utilities.startsWith(kw, prefix)) continue;
                this.results.add(this.javaCompletionItemFactory.createKeywordItem(kw, " ", this.anchorOffset, false));
            }
            if (env.getController().getSourceVersion().compareTo(SourceVersion.RELEASE_8) >= 0 && Utilities.startsWith("default", prefix) && Utilities.getPathElementOfKind(Tree.Kind.INTERFACE, env.getPath()) != null) {
                this.results.add(this.javaCompletionItemFactory.createKeywordItem("default", " ", this.anchorOffset, false));
            }
            this.addPrimitiveTypeKeywords(env);
        }

        private void addKeywordsForBlock(Env env) {
            String prefix = env.getPrefix();
            for (String kw2 : STATEMENT_KEYWORDS) {
                if (!Utilities.startsWith(kw2, prefix)) continue;
                this.results.add(this.javaCompletionItemFactory.createKeywordItem(kw2, null, this.anchorOffset, false));
            }
            for (String kw2 : BLOCK_KEYWORDS) {
                if (!Utilities.startsWith(kw2, prefix)) continue;
                this.results.add(this.javaCompletionItemFactory.createKeywordItem(kw2, " ", this.anchorOffset, false));
            }
            if (Utilities.startsWith("return", prefix)) {
                TreePath tp = Utilities.getPathElementOfKind(EnumSet.of(Tree.Kind.METHOD, Tree.Kind.LAMBDA_EXPRESSION), env.getPath());
                String postfix = " ";
                if (tp != null) {
                    if (tp.getLeaf().getKind() == Tree.Kind.METHOD) {
                        Tree rt = ((MethodTree)tp.getLeaf()).getReturnType();
                        if (rt == null || rt.getKind() == Tree.Kind.PRIMITIVE_TYPE && ((PrimitiveTypeTree)rt).getPrimitiveTypeKind() == TypeKind.VOID) {
                            postfix = ";";
                        }
                    } else {
                        ExecutableType dt;
                        TypeMirror tm = env.getController().getTrees().getTypeMirror(tp);
                        if (tm != null && tm.getKind() == TypeKind.DECLARED && (dt = env.getController().getTypeUtilities().getDescriptorType((DeclaredType)tm)) != null && dt.getReturnType().getKind() == TypeKind.VOID) {
                            postfix = ";";
                        }
                    }
                }
                this.results.add(this.javaCompletionItemFactory.createKeywordItem("return", postfix, this.anchorOffset, false));
            }
            boolean caseAdded = false;
            boolean breakAdded = false;
            boolean continueAdded = false;
            block6 : for (TreePath tp = env.getPath(); tp != null; tp = tp.getParentPath()) {
                switch (tp.getLeaf().getKind()) {
                    case SWITCH: {
                        CaseTree t;
                        CaseTree lastCase = null;
                        CompilationUnitTree root = env.getRoot();
                        SourcePositions sourcePositions = env.getSourcePositions();
                        Iterator i$ = ((SwitchTree)tp.getLeaf()).getCases().iterator();
                        while (i$.hasNext() && sourcePositions.getStartPosition(root, (Tree)(t = (CaseTree)i$.next())) < (long)env.getOffset()) {
                            lastCase = t;
                        }
                        if (!(caseAdded || lastCase != null && lastCase.getExpression() == null)) {
                            caseAdded = true;
                            if (Utilities.startsWith("case", prefix)) {
                                this.results.add(this.javaCompletionItemFactory.createKeywordItem("case", " ", this.anchorOffset, false));
                            }
                            if (Utilities.startsWith("default", prefix)) {
                                this.results.add(this.javaCompletionItemFactory.createKeywordItem("default", ":", this.anchorOffset, false));
                            }
                        }
                        if (breakAdded || !Utilities.startsWith("break", prefix)) continue block6;
                        breakAdded = true;
                        this.results.add(this.javaCompletionItemFactory.createKeywordItem("break", this.withinLabeledStatement(env) ? null : ";", this.anchorOffset, false));
                        continue block6;
                    }
                    case WHILE_LOOP: 
                    case DO_WHILE_LOOP: 
                    case FOR_LOOP: 
                    case ENHANCED_FOR_LOOP: {
                        if (!breakAdded && Utilities.startsWith("break", prefix)) {
                            breakAdded = true;
                            this.results.add(this.javaCompletionItemFactory.createKeywordItem("break", this.withinLabeledStatement(env) ? null : ";", this.anchorOffset, false));
                        }
                        if (continueAdded || !Utilities.startsWith("continue", prefix)) continue block6;
                        continueAdded = true;
                        this.results.add(this.javaCompletionItemFactory.createKeywordItem("continue", this.withinLabeledStatement(env) ? null : ";", this.anchorOffset, false));
                    }
                }
            }
        }

        private void addKeywordsForStatement(Env env) {
            TreePath tp;
            String prefix = env.getPrefix();
            for (String kw2 : STATEMENT_KEYWORDS) {
                if (!Utilities.startsWith(kw2, prefix)) continue;
                this.results.add(this.javaCompletionItemFactory.createKeywordItem(kw2, null, this.anchorOffset, false));
            }
            for (String kw2 : STATEMENT_SPACE_KEYWORDS) {
                if (!Utilities.startsWith(kw2, prefix)) continue;
                this.results.add(this.javaCompletionItemFactory.createKeywordItem(kw2, " ", this.anchorOffset, false));
            }
            if (Utilities.startsWith("return", prefix)) {
                tp = Utilities.getPathElementOfKind(EnumSet.of(Tree.Kind.METHOD, Tree.Kind.LAMBDA_EXPRESSION), env.getPath());
                String postfix = " ";
                if (tp != null) {
                    if (tp.getLeaf().getKind() == Tree.Kind.METHOD) {
                        Tree rt = ((MethodTree)tp.getLeaf()).getReturnType();
                        if (rt == null || rt.getKind() == Tree.Kind.PRIMITIVE_TYPE && ((PrimitiveTypeTree)rt).getPrimitiveTypeKind() == TypeKind.VOID) {
                            postfix = ";";
                        }
                    } else {
                        ExecutableType dt;
                        TypeMirror tm = env.getController().getTrees().getTypeMirror(tp);
                        if (tm != null && tm.getKind() == TypeKind.DECLARED && (dt = env.getController().getTypeUtilities().getDescriptorType((DeclaredType)tm)) != null && dt.getReturnType().getKind() == TypeKind.VOID) {
                            postfix = ";";
                        }
                    }
                }
                this.results.add(this.javaCompletionItemFactory.createKeywordItem("return", postfix, this.anchorOffset, false));
            }
            boolean cAdded = false;
            boolean bAdded = false;
            block6 : for (tp = env.getPath(); !(tp == null || cAdded && bAdded); tp = tp.getParentPath()) {
                switch (tp.getLeaf().getKind()) {
                    case WHILE_LOOP: 
                    case DO_WHILE_LOOP: 
                    case FOR_LOOP: 
                    case ENHANCED_FOR_LOOP: {
                        if (!cAdded && Utilities.startsWith("continue", prefix)) {
                            this.results.add(this.javaCompletionItemFactory.createKeywordItem("continue", ";", this.anchorOffset, false));
                            cAdded = true;
                        }
                    }
                    case SWITCH: {
                        if (bAdded || !Utilities.startsWith("break", prefix)) continue block6;
                        this.results.add(this.javaCompletionItemFactory.createKeywordItem("break", ";", this.anchorOffset, false));
                        bAdded = true;
                    }
                }
            }
        }

        private void addValueKeywords(Env env) throws IOException {
            Set<? extends TypeMirror> smartTypes;
            String prefix = env.getPrefix();
            boolean smartType = false;
            if (this.queryType == 1 && (smartTypes = env.getSmartTypes()) != null && !smartTypes.isEmpty()) {
                for (TypeMirror st : smartTypes) {
                    if (st.getKind() != TypeKind.BOOLEAN) continue;
                    smartType = true;
                    break;
                }
            }
            if (Utilities.startsWith("false", prefix)) {
                this.results.add(this.javaCompletionItemFactory.createKeywordItem("false", null, this.anchorOffset, smartType));
            }
            if (Utilities.startsWith("true", prefix)) {
                this.results.add(this.javaCompletionItemFactory.createKeywordItem("true", null, this.anchorOffset, smartType));
            }
            if (Utilities.startsWith("null", prefix)) {
                this.results.add(this.javaCompletionItemFactory.createKeywordItem("null", null, this.anchorOffset, false));
            }
            if (Utilities.startsWith("new", prefix)) {
                this.results.add(this.javaCompletionItemFactory.createKeywordItem("new", " ", this.anchorOffset, false));
            }
        }

        private void addPrimitiveTypeKeywords(Env env) {
            String prefix = env.getPrefix();
            for (String kw : PRIM_KEYWORDS) {
                if (!Utilities.startsWith(kw, prefix)) continue;
                this.results.add(this.javaCompletionItemFactory.createKeywordItem(kw, null, this.anchorOffset, false));
            }
        }

        private void addClassModifiers(Env env, Set<Modifier> modifiers) {
            String prefix = env.getPrefix();
            ArrayList<String> kws = new ArrayList<String>();
            if (!modifiers.contains((Object)Modifier.PUBLIC) && !modifiers.contains((Object)Modifier.PRIVATE)) {
                kws.add("public");
            }
            if (!modifiers.contains((Object)Modifier.FINAL) && !modifiers.contains((Object)Modifier.ABSTRACT)) {
                kws.add("abstract");
                kws.add("final");
            }
            kws.add("class");
            kws.add("interface");
            kws.add("enum");
            for (String kw : kws) {
                if (!Utilities.startsWith(kw, prefix)) continue;
                this.results.add(this.javaCompletionItemFactory.createKeywordItem(kw, " ", this.anchorOffset, false));
            }
        }

        private void addMemberModifiers(Env env, Set<Modifier> modifiers, boolean isLocal) {
            String prefix = env.getPrefix();
            ArrayList<String> kws = new ArrayList<String>();
            if (isLocal) {
                if (!modifiers.contains((Object)Modifier.FINAL)) {
                    kws.add("final");
                }
            } else {
                if (!(modifiers.contains((Object)Modifier.PUBLIC) || modifiers.contains((Object)Modifier.PROTECTED) || modifiers.contains((Object)Modifier.PRIVATE))) {
                    kws.add("public");
                    kws.add("protected");
                    kws.add("private");
                }
                if (!(env.getController().getSourceVersion().compareTo(SourceVersion.RELEASE_8) < 0 || Utilities.getPathElementOfKind(Tree.Kind.INTERFACE, env.getPath()) == null || modifiers.contains((Object)Modifier.STATIC) || modifiers.contains((Object)Modifier.ABSTRACT) || modifiers.contains((Object)Modifier.DEFAULT))) {
                    kws.add("default");
                }
                if (!(modifiers.contains((Object)Modifier.FINAL) || modifiers.contains((Object)Modifier.ABSTRACT) || modifiers.contains((Object)Modifier.VOLATILE))) {
                    kws.add("final");
                }
                if (!(modifiers.contains((Object)Modifier.FINAL) || modifiers.contains((Object)Modifier.ABSTRACT) || modifiers.contains((Object)Modifier.DEFAULT) || modifiers.contains((Object)Modifier.NATIVE) || modifiers.contains((Object)Modifier.SYNCHRONIZED))) {
                    kws.add("abstract");
                }
                if (!modifiers.contains((Object)Modifier.STATIC) && !modifiers.contains((Object)Modifier.DEFAULT)) {
                    kws.add("static");
                }
                if (!modifiers.contains((Object)Modifier.ABSTRACT) && !modifiers.contains((Object)Modifier.NATIVE)) {
                    kws.add("native");
                }
                if (!modifiers.contains((Object)Modifier.STRICTFP)) {
                    kws.add("strictfp");
                }
                if (!modifiers.contains((Object)Modifier.SYNCHRONIZED) && !modifiers.contains((Object)Modifier.ABSTRACT)) {
                    kws.add("synchronized");
                }
                if (!modifiers.contains((Object)Modifier.TRANSIENT)) {
                    kws.add("transient");
                }
                if (!modifiers.contains((Object)Modifier.FINAL) && !modifiers.contains((Object)Modifier.VOLATILE)) {
                    kws.add("volatile");
                }
                kws.add("void");
                kws.add("class");
                kws.add("interface");
                kws.add("enum");
            }
            for (String kw : kws) {
                if (!Utilities.startsWith(kw, prefix)) continue;
                this.results.add(this.javaCompletionItemFactory.createKeywordItem(kw, " ", this.anchorOffset, false));
            }
            for (String kw2 : PRIM_KEYWORDS) {
                if (!Utilities.startsWith(kw2, prefix)) continue;
                this.results.add(this.javaCompletionItemFactory.createKeywordItem(kw2, " ", this.anchorOffset, false));
            }
        }

        private void addElementCreators(Env env) throws IOException {
            Trees trees;
            TypeElement te;
            TypeMirror tm;
            CompilationController controller = env.getController();
            controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
            TreePath clsPath = Utilities.getPathElementOfKind(TreeUtilities.CLASS_TREE_KINDS, env.getPath());
            if (clsPath == null) {
                return;
            }
            ClassTree cls = (ClassTree)clsPath.getLeaf();
            CompilationUnitTree root = env.getRoot();
            SourcePositions sourcePositions = env.getSourcePositions();
            Tree currentMember = null;
            int nextMemberPos = -1;
            for (Tree member : cls.getMembers()) {
                int pos = (int)sourcePositions.getStartPosition(root, member);
                if (pos >= this.caretOffset) {
                    nextMemberPos = pos;
                    break;
                }
                pos = (int)sourcePositions.getEndPosition(root, member);
                if (this.caretOffset >= pos) continue;
                currentMember = member;
                nextMemberPos = pos;
                break;
            }
            if (nextMemberPos > this.caretOffset) {
                String text = controller.getText().substring(this.caretOffset, nextMemberPos);
                int idx = text.indexOf(10);
                if (idx >= 0) {
                    text = text.substring(0, idx);
                }
                if (text.trim().length() > 0) {
                    return;
                }
            }
            if ((te = (TypeElement)(trees = controller.getTrees()).getElement(clsPath)) == null) {
                return;
            }
            String prefix = env.getPrefix();
            Types types = controller.getTypes();
            DeclaredType clsType = (DeclaredType)te.asType();
            if (te.getKind().isClass() || te.getKind().isInterface() && SourceVersion.RELEASE_8.compareTo(controller.getSourceVersion()) <= 0) {
                for (ExecutableElement ee : GeneratorUtils.findUndefs((CompilationInfo)controller, te)) {
                    if (!this.startsWith(env, ee.getSimpleName().toString()) || (tm = this.asMemberOf(ee, clsType, types)).getKind() != TypeKind.EXECUTABLE) continue;
                    this.results.add(this.javaCompletionItemFactory.createOverrideMethodItem((CompilationInfo)env.getController(), ee, (ExecutableType)tm, this.anchorOffset, true, env.getWhiteList()));
                }
            }
            if (te.getKind().isClass() || te.getKind().isInterface()) {
                for (ExecutableElement ee : GeneratorUtils.findOverridable((CompilationInfo)controller, te)) {
                    if (!this.startsWith(env, ee.getSimpleName().toString()) || (tm = this.asMemberOf(ee, clsType, types)).getKind() != TypeKind.EXECUTABLE) continue;
                    this.results.add(this.javaCompletionItemFactory.createOverrideMethodItem((CompilationInfo)env.getController(), ee, (ExecutableType)tm, this.anchorOffset, false, env.getWhiteList()));
                }
            }
            if (!te.getKind().isClass()) {
                return;
            }
            if (prefix == null || this.startsWith(env, "get") || this.startsWith(env, "set") || this.startsWith(env, "is") || this.startsWith(env, prefix, "get") || this.startsWith(env, prefix, "set") || this.startsWith(env, prefix, "is")) {
                List<? extends Element> members = controller.getElements().getAllMembers(te);
                HashMap<String, List<ExecutableElement>> methods = new HashMap<String, List<ExecutableElement>>();
                for (ExecutableElement method : ElementFilter.methodsIn(members)) {
                    ArrayList<ExecutableElement> l = (ArrayList<ExecutableElement>)methods.get(method.getSimpleName().toString());
                    if (l == null) {
                        l = new ArrayList<ExecutableElement>();
                        methods.put(method.getSimpleName().toString(), l);
                    }
                    l.add(method);
                }
                CodeStyle codeStyle = CodeStyle.getDefault((Document)controller.getDocument());
                for (VariableElement variableElement : ElementFilter.fieldsIn(members)) {
                    Name name = variableElement.getSimpleName();
                    if (name.contentEquals("<error>")) continue;
                    boolean isStatic = variableElement.getModifiers().contains((Object)Modifier.STATIC);
                    String setterName = CodeStyleUtils.computeSetterName((CharSequence)name, (boolean)isStatic, (CodeStyle)codeStyle);
                    String getterName = CodeStyleUtils.computeGetterName((CharSequence)name, (boolean)Utilities.isBoolean(variableElement.asType()), (boolean)isStatic, (CodeStyle)codeStyle);
                    if ((prefix == null || this.startsWith(env, getterName)) && !GeneratorUtils.hasGetter((CompilationInfo)controller, te, variableElement, methods, codeStyle)) {
                        this.results.add(this.javaCompletionItemFactory.createGetterSetterMethodItem((CompilationInfo)env.getController(), variableElement, this.asMemberOf(variableElement, clsType, types), this.anchorOffset, getterName, false));
                    }
                    if (prefix != null && !this.startsWith(env, setterName) || variableElement.getModifiers().contains((Object)Modifier.FINAL) || GeneratorUtils.hasSetter((CompilationInfo)controller, te, variableElement, methods, codeStyle)) continue;
                    this.results.add(this.javaCompletionItemFactory.createGetterSetterMethodItem((CompilationInfo)env.getController(), variableElement, this.asMemberOf(variableElement, clsType, types), this.anchorOffset, setterName, true));
                }
            }
            if (this.startsWith(env, te.getSimpleName().toString())) {
                Element e;
                LinkedHashSet<VariableElement> initializedFields = new LinkedHashSet<VariableElement>();
                LinkedHashSet<VariableElement> uninitializedFields = new LinkedHashSet<VariableElement>();
                ArrayList<ExecutableElement> constructors = new ArrayList<ExecutableElement>();
                if (currentMember != null && currentMember.getKind() == Tree.Kind.VARIABLE && (e = trees.getElement(new TreePath(clsPath, currentMember))).getKind().isField()) {
                    initializedFields.add((VariableElement)e);
                }
                Element dctor2generate = null;
                LinkedHashMap<ExecutableElement, boolean[]> ctors2generate = new LinkedHashMap<ExecutableElement, boolean[]>();
                GeneratorUtils.scanForFieldsAndConstructors((CompilationInfo)controller, clsPath, initializedFields, uninitializedFields, constructors);
                LinkedHashSet<VariableElement> uninitializedFinalFields = new LinkedHashSet<VariableElement>();
                for (VariableElement ve : uninitializedFields) {
                    if (!ve.getModifiers().contains((Object)Modifier.FINAL)) continue;
                    uninitializedFinalFields.add(ve);
                }
                int ufSize = uninitializedFields.size();
                int uffSize = uninitializedFinalFields.size();
                if (cls.getKind() != Tree.Kind.ENUM && te.getSuperclass().getKind() == TypeKind.DECLARED) {
                    DeclaredType superType = (DeclaredType)te.getSuperclass();
                    Scope scope = env.getScope();
                    for (ExecutableElement ctor : ElementFilter.constructorsIn(superType.asElement().getEnclosedElements())) {
                        if (!trees.isAccessible(scope, (Element)ctor, superType)) continue;
                        if (dctor2generate == null || ((ExecutableElement)dctor2generate).getParameters().size() > ctor.getParameters().size()) {
                            dctor2generate = ctor;
                        }
                        boolean[] arrbl = new boolean[2];
                        arrbl[0] = true;
                        arrbl[1] = ufSize > 0 && uffSize < ufSize;
                        ctors2generate.put(ctor, arrbl);
                    }
                } else {
                    dctor2generate = te;
                    boolean[] arrbl = new boolean[2];
                    arrbl[0] = true;
                    arrbl[1] = ufSize > 0 && uffSize < ufSize;
                    ctors2generate.put(null, arrbl);
                }
                for (ExecutableElement ee : constructors) {
                    if (controller.getElementUtilities().isSynthetic((Element)ee)) continue;
                    List<? extends VariableElement> parameters = ee.getParameters();
                    if (parameters.isEmpty()) {
                        dctor2generate = null;
                    }
                    for (Map.Entry entry : ctors2generate.entrySet()) {
                        Iterator<? extends VariableElement> original;
                        boolean same;
                        Iterator proposed;
                        List params = entry.getKey() != null ? ((ExecutableElement)entry.getKey()).getParameters() : Collections.emptyList();
                        int paramSize = params.size();
                        if (parameters.size() == paramSize + uffSize) {
                            proposed = uninitializedFinalFields.iterator();
                            original = parameters.iterator();
                            for (same = true; same && proposed.hasNext() && original.hasNext(); same &= controller.getTypes().isSameType((TypeMirror)((VariableElement)proposed.next()).asType(), (TypeMirror)original.next().asType())) {
                            }
                            if (same) {
                                proposed = params.iterator();
                                while (same && proposed.hasNext() && original.hasNext()) {
                                    same &= controller.getTypes().isSameType(((VariableElement)proposed.next()).asType(), original.next().asType());
                                }
                                if (same) {
                                    ((boolean[])entry.getValue())[0] = false;
                                }
                            }
                        }
                        if (ufSize <= 0 || uffSize >= ufSize || parameters.size() != paramSize + ufSize) continue;
                        proposed = uninitializedFields.iterator();
                        original = parameters.iterator();
                        for (same = true; same && proposed.hasNext() && original.hasNext(); same &= controller.getTypes().isSameType((TypeMirror)((VariableElement)proposed.next()).asType(), (TypeMirror)original.next().asType())) {
                        }
                        if (!same) continue;
                        proposed = params.iterator();
                        while (same && proposed.hasNext() && original.hasNext()) {
                            same &= controller.getTypes().isSameType(((VariableElement)proposed.next()).asType(), original.next().asType());
                        }
                        if (!same) continue;
                        ((boolean[])entry.getValue())[1] = false;
                    }
                }
                if (dctor2generate != null) {
                    this.results.add(this.javaCompletionItemFactory.createInitializeAllConstructorItem((CompilationInfo)env.getController(), true, uninitializedFinalFields, dctor2generate.getKind() == ElementKind.CONSTRUCTOR ? (ExecutableElement)dctor2generate : null, te, this.anchorOffset));
                }
                for (Map.Entry entry : ctors2generate.entrySet()) {
                    if ((entry.getKey() != dctor2generate || !((ExecutableType)dctor2generate.asType()).getParameterTypes().isEmpty()) && ((boolean[])entry.getValue())[0]) {
                        this.results.add(this.javaCompletionItemFactory.createInitializeAllConstructorItem((CompilationInfo)env.getController(), false, uninitializedFinalFields, (ExecutableElement)entry.getKey(), te, this.anchorOffset));
                    }
                    if (!((boolean[])entry.getValue())[1]) continue;
                    this.results.add(this.javaCompletionItemFactory.createInitializeAllConstructorItem((CompilationInfo)env.getController(), false, uninitializedFields, (ExecutableElement)entry.getKey(), te, this.anchorOffset));
                }
            }
        }

        private TypeElement getTypeElement(Env env, final String simpleName) throws IOException {
            if (simpleName == null || simpleName.length() == 0) {
                return null;
            }
            CompilationController controller = env.getController();
            TreeUtilities tu = controller.getTreeUtilities();
            final Trees trees = controller.getTrees();
            final Scope scope = env.getScope();
            TypeElement enclClass = scope.getEnclosingClass();
            final boolean isStatic = enclClass == null ? false : tu.isStaticContext(scope) || env.getPath().getLeaf().getKind() == Tree.Kind.BLOCK && ((BlockTree)env.getPath().getLeaf()).isStatic();
            Object acceptor = new ElementUtilities.ElementAcceptor(){

                public boolean accept(Element e, TypeMirror t) {
                    return !(!e.getKind().isClass() && !e.getKind().isInterface() || !e.getSimpleName().contentEquals(simpleName) || isStatic && !e.getModifiers().contains((Object)Modifier.STATIC) || !trees.isAccessible(scope, e, (DeclaredType)t));
                }
            };
            Iterator i$ = controller.getElementUtilities().getLocalMembersAndVars(scope, acceptor).iterator();
            if (i$.hasNext()) {
                Element e = (Element)i$.next();
                return (TypeElement)e;
            }
            acceptor = new ElementUtilities.ElementAcceptor(){

                public boolean accept(Element e, TypeMirror t) {
                    return e.getSimpleName().contentEquals(simpleName) && trees.isAccessible(scope, (TypeElement)e);
                }
            };
            for (TypeElement e : controller.getElementUtilities().getGlobalTypes(acceptor)) {
                if (!simpleName.contentEquals(e.getSimpleName())) continue;
                return e;
            }
            return null;
        }

        private VariableElement getFieldOrVar(Env env, final String simpleName) throws IOException {
            if (simpleName == null || simpleName.length() == 0) {
                return null;
            }
            CompilationController controller = env.getController();
            Scope scope = env.getScope();
            TypeElement enclClass = scope.getEnclosingClass();
            final boolean isStatic = enclClass == null ? false : controller.getTreeUtilities().isStaticContext(scope) || env.getPath().getLeaf().getKind() == Tree.Kind.BLOCK && ((BlockTree)env.getPath().getLeaf()).isStatic();
            final Collection<? extends Element> illegalForwardRefs = env.getForwardReferences();
            final ExecutableElement method = scope.getEnclosingMethod();
            ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                public boolean accept(Element e, TypeMirror t) {
                    if (!e.getSimpleName().contentEquals(simpleName)) {
                        return false;
                    }
                    switch (e.getKind()) {
                        case LOCAL_VARIABLE: 
                        case RESOURCE_VARIABLE: {
                            if (isStatic && (e.getSimpleName().contentEquals("this") || e.getSimpleName().contentEquals("super"))) {
                                return false;
                            }
                        }
                        case EXCEPTION_PARAMETER: 
                        case PARAMETER: {
                            return (method == e.getEnclosingElement() || e.getModifiers().contains((Object)Modifier.FINAL)) && !illegalForwardRefs.contains(e);
                        }
                        case FIELD: {
                            if (e.getSimpleName().contentEquals("this") || e.getSimpleName().contentEquals("super")) {
                                return !isStatic;
                            }
                        }
                        case ENUM_CONSTANT: {
                            return !illegalForwardRefs.contains(e);
                        }
                    }
                    return false;
                }
            };
            Iterator i$ = controller.getElementUtilities().getLocalMembersAndVars(scope, acceptor).iterator();
            if (i$.hasNext()) {
                Element e = (Element)i$.next();
                return (VariableElement)e;
            }
            return null;
        }

        private TypeMirror getCorrectedReturnType(Env env, ExecutableType et, ExecutableElement el, TypeMirror site) {
            TypeMirror type = et.getReturnType();
            if (site.getKind() == TypeKind.DECLARED && "getClass".contentEquals(el.getSimpleName()) && et.getParameterTypes().isEmpty() && type.getKind() == TypeKind.DECLARED && "java.lang.Class".contentEquals(((TypeElement)((DeclaredType)type).asElement()).getQualifiedName()) && ((TypeElement)((DeclaredType)type).asElement()).getTypeParameters().size() == 1) {
                Types types = env.getController().getTypes();
                type = types.getDeclaredType((TypeElement)((DeclaredType)type).asElement(), types.getWildcardType(site, null));
            }
            return type;
        }

        /*
         * Enabled force condition propagation
         * Lifted jumps to return sites
         */
        private boolean isOfSmartType(Env env, TypeMirror type, Set<? extends TypeMirror> smartTypes) {
            if (smartTypes == null || smartTypes.isEmpty()) {
                return false;
            }
            if (env.isInsideForEachExpression()) {
                if (type.getKind() == TypeKind.ARRAY) {
                    type = ((ArrayType)type).getComponentType();
                } else {
                    if (type.getKind() != TypeKind.DECLARED) return false;
                    Elements elements = env.getController().getElements();
                    Types types = env.getController().getTypes();
                    TypeElement iterableTE = elements.getTypeElement("java.lang.Iterable");
                    if (iterableTE == null) {
                        return false;
                    }
                    DeclaredType declaredType = types.getDeclaredType(iterableTE, new TypeMirror[0]);
                    DeclaredType iterable = declaredType;
                    if (iterable == null || !types.isSubtype(type, iterable)) return false;
                    Iterator<? extends TypeMirror> it = ((DeclaredType)type).getTypeArguments().iterator();
                    type = it.hasNext() ? it.next() : elements.getTypeElement("java.lang.Object").asType();
                }
            } else if (type.getKind() == TypeKind.EXECUTABLE) {
                Types types = env.getController().getTypes();
                TypeUtilities tu = env.getController().getTypeUtilities();
                for (TypeMirror smartType : smartTypes) {
                    ExecutableType descriptorType;
                    if (smartType.getKind() != TypeKind.DECLARED || (descriptorType = tu.getDescriptorType((DeclaredType)smartType)) == null || !types.isSubsignature((ExecutableType)type, descriptorType) || !types.isSubtype(((ExecutableType)type).getReturnType(), descriptorType.getReturnType())) continue;
                    return true;
                }
                return false;
            }
            for (TypeMirror smartType : smartTypes) {
                if (!SourceUtils.checkTypesAssignable((CompilationInfo)env.getController(), (TypeMirror)type, (TypeMirror)smartType)) continue;
                return true;
            }
            return false;
        }

        private boolean isTopLevelClass(Tree tree, CompilationUnitTree root) {
            if (TreeUtilities.CLASS_TREE_KINDS.contains((Object)tree.getKind()) || tree.getKind() == Tree.Kind.EXPRESSION_STATEMENT && ((ExpressionStatementTree)tree).getExpression().getKind() == Tree.Kind.ERRONEOUS) {
                for (Tree t : root.getTypeDecls()) {
                    if (tree != t) continue;
                    return true;
                }
            }
            return false;
        }

        private static boolean isAnnonInner(ElementHandle<TypeElement> elem) {
            String name = elem.getQualifiedName();
            int idx = name.lastIndexOf(46);
            String simpleName = idx > -1 ? name.substring(idx + 1) : name;
            return simpleName.length() == 0 || Character.isDigit(simpleName.charAt(0));
        }

        private static boolean isJavaIdentifierPart(String text) {
            for (int i = 0; i < text.length(); ++i) {
                if (Character.isJavaIdentifierPart(text.charAt(i))) continue;
                return false;
            }
            return true;
        }

        private Collection getFilteredData(Collection<JavaCompletionItem> data, String prefix) {
            if (prefix.length() == 0) {
                return data;
            }
            ArrayList<CompletionItem> ret = new ArrayList<CompletionItem>();
            boolean camelCase = JavaCompletionQuery.isCamelCasePrefix(prefix);
            for (CompletionItem itm : data) {
                if (!Utilities.startsWith(itm.getInsertPrefix().toString(), prefix) && (!camelCase || !Utilities.startsWithCamelCase(itm.getInsertPrefix().toString(), prefix))) continue;
                ret.add(itm);
            }
            return ret;
        }

        private boolean isOfKindAndType(TypeMirror type, Element e, EnumSet<ElementKind> kinds, TypeMirror base, Scope scope, Trees trees, Types types) {
            if (type.getKind() != TypeKind.ERROR && kinds.contains((Object)e.getKind())) {
                if (base == null) {
                    return true;
                }
                if (types.isSubtype(type, base)) {
                    return true;
                }
            }
            if ((e.getKind().isClass() || e.getKind().isInterface()) && (kinds.contains((Object)ElementKind.ANNOTATION_TYPE) || kinds.contains((Object)ElementKind.CLASS) || kinds.contains((Object)ElementKind.ENUM) || kinds.contains((Object)ElementKind.INTERFACE))) {
                DeclaredType dt = (DeclaredType)e.asType();
                for (Element ee : e.getEnclosedElements()) {
                    if (!trees.isAccessible(scope, ee, dt) || !this.isOfKindAndType(ee.asType(), ee, kinds, base, scope, trees, types)) continue;
                    return true;
                }
            }
            return false;
        }

        private boolean containsAccessibleNonFinalType(Element e, Scope scope, Trees trees) {
            if (e.getKind().isClass() || e.getKind().isInterface()) {
                if (!e.getModifiers().contains((Object)Modifier.FINAL)) {
                    return true;
                }
                DeclaredType dt = (DeclaredType)e.asType();
                for (Element ee : e.getEnclosedElements()) {
                    if (!trees.isAccessible(scope, ee, dt) || !this.containsAccessibleNonFinalType(ee, scope, trees)) continue;
                    return true;
                }
            }
            return false;
        }

        private Set<? extends TypeMirror> getSmartTypes(Env env) throws IOException {
            int offset = env.getOffset();
            CompilationController controller = env.getController();
            Tree lastTree = null;
            int dim = 0;
            for (TreePath path = controller.getTreeUtilities().pathFor((int)offset); path != null; path = path.getParentPath()) {
                Tree tree = path.getLeaf();
                switch (tree.getKind()) {
                    CompilationUnitTree root;
                    Element el;
                    TypeMirror type;
                    CompoundAssignmentTree cat;
                    List<Tree> argTypes;
                    BinaryTree bt;
                    String text;
                    ExpressionTree exp;
                    SourcePositions sourcePositions;
                    case VARIABLE: {
                        type = controller.getTrees().getTypeMirror(new TreePath(path, ((VariableTree)tree).getType()));
                        if (type == null) {
                            return null;
                        }
                        while (dim-- > 0) {
                            if (type.getKind() == TypeKind.ARRAY) {
                                type = ((ArrayType)type).getComponentType();
                                continue;
                            }
                            return null;
                        }
                        return type != null ? Collections.singleton(type) : null;
                    }
                    case ASSIGNMENT: {
                        type = controller.getTrees().getTypeMirror(new TreePath(path, (Tree)((AssignmentTree)tree).getVariable()));
                        if (type == null) {
                            return null;
                        }
                        TreePath parentPath = path.getParentPath();
                        if (parentPath != null && parentPath.getLeaf().getKind() == Tree.Kind.ANNOTATION && type.getKind() == TypeKind.EXECUTABLE) {
                            type = ((ExecutableType)type).getReturnType();
                            while (dim-- > 0) {
                                if (type.getKind() == TypeKind.ARRAY) {
                                    type = ((ArrayType)type).getComponentType();
                                    continue;
                                }
                                return null;
                            }
                            if (type.getKind() == TypeKind.ARRAY) {
                                type = ((ArrayType)type).getComponentType();
                            }
                        }
                        return type != null ? Collections.singleton(type) : null;
                    }
                    case RETURN: {
                        ExecutableType descType;
                        TreePath methodOrLambdaPath = Utilities.getPathElementOfKind(EnumSet.of(Tree.Kind.METHOD, Tree.Kind.LAMBDA_EXPRESSION), path);
                        if (methodOrLambdaPath == null) {
                            return null;
                        }
                        if (methodOrLambdaPath.getLeaf().getKind() == Tree.Kind.METHOD) {
                            Tree retTree = ((MethodTree)methodOrLambdaPath.getLeaf()).getReturnType();
                            if (retTree == null) {
                                return null;
                            }
                            type = controller.getTrees().getTypeMirror(new TreePath(methodOrLambdaPath, retTree));
                            if (type == null && JavaSource.Phase.RESOLVED.compareTo((Enum)controller.getPhase()) > 0) {
                                controller.toPhase(JavaSource.Phase.RESOLVED);
                                type = controller.getTrees().getTypeMirror(new TreePath(methodOrLambdaPath, retTree));
                            }
                            return type != null ? Collections.singleton(type) : null;
                        }
                        type = controller.getTrees().getTypeMirror(methodOrLambdaPath);
                        if (type == null || type.getKind() != TypeKind.DECLARED || (descType = controller.getTypeUtilities().getDescriptorType((DeclaredType)type)) == null) break;
                        return Collections.singleton(descType.getReturnType());
                    }
                    case THROW: {
                        TreePath methodPath = Utilities.getPathElementOfKind(Tree.Kind.METHOD, path);
                        if (methodPath == null) {
                            return null;
                        }
                        HashSet<TypeMirror> ret = new HashSet<TypeMirror>();
                        Trees trees = controller.getTrees();
                        for (ExpressionTree thr : ((MethodTree)methodPath.getLeaf()).getThrows()) {
                            type = trees.getTypeMirror(new TreePath(methodPath, (Tree)thr));
                            if (type == null && JavaSource.Phase.RESOLVED.compareTo((Enum)controller.getPhase()) > 0) {
                                controller.toPhase(JavaSource.Phase.RESOLVED);
                                type = trees.getTypeMirror(new TreePath(methodPath, (Tree)thr));
                            }
                            if (type == null) continue;
                            ret.add(type);
                        }
                        return ret;
                    }
                    case TRY: {
                        TypeElement te = controller.getElements().getTypeElement("java.lang.AutoCloseable");
                        return te != null ? Collections.singleton(controller.getTypes().getDeclaredType(te, new TypeMirror[0])) : null;
                    }
                    case IF: {
                        IfTree iff = (IfTree)tree;
                        return iff.getCondition() == lastTree ? Collections.singleton(controller.getTypes().getPrimitiveType(TypeKind.BOOLEAN)) : null;
                    }
                    case WHILE_LOOP: {
                        WhileLoopTree wl = (WhileLoopTree)tree;
                        return wl.getCondition() == lastTree ? Collections.singleton(controller.getTypes().getPrimitiveType(TypeKind.BOOLEAN)) : null;
                    }
                    case DO_WHILE_LOOP: {
                        DoWhileLoopTree dwl = (DoWhileLoopTree)tree;
                        return dwl.getCondition() == lastTree ? Collections.singleton(controller.getTypes().getPrimitiveType(TypeKind.BOOLEAN)) : null;
                    }
                    case FOR_LOOP: {
                        ForLoopTree fl = (ForLoopTree)tree;
                        ExpressionTree cond = fl.getCondition();
                        if (lastTree != null) {
                            Iterator itt;
                            if (cond instanceof ErroneousTree && (itt = ((ErroneousTree)cond).getErrorTrees().iterator()).hasNext()) {
                                cond = (Tree)itt.next();
                            }
                            return cond == lastTree ? Collections.singleton(controller.getTypes().getPrimitiveType(TypeKind.BOOLEAN)) : null;
                        }
                        sourcePositions = env.getSourcePositions();
                        root = env.getRoot();
                        if (cond != null && sourcePositions.getEndPosition(root, (Tree)cond) < (long)offset) {
                            return null;
                        }
                        Tree lastInit = null;
                        for (Tree init : fl.getInitializer()) {
                            if (sourcePositions.getEndPosition(root, init) >= (long)offset) {
                                return null;
                            }
                            lastInit = init;
                        }
                        text = null;
                        if (lastInit == null) {
                            text = controller.getText().substring((int)sourcePositions.getStartPosition(root, (Tree)fl), offset).trim();
                            int idx = text.indexOf(40);
                            if (idx >= 0) {
                                text = text.substring(idx + 1);
                            }
                        } else {
                            text = controller.getText().substring((int)sourcePositions.getEndPosition(root, lastInit), offset).trim();
                        }
                        return ";".equals(text) ? Collections.singleton(controller.getTypes().getPrimitiveType(TypeKind.BOOLEAN)) : null;
                    }
                    case ENHANCED_FOR_LOOP: {
                        EnhancedForLoopTree efl = (EnhancedForLoopTree)tree;
                        ExpressionTree expr = efl.getExpression();
                        if (lastTree != null) {
                            Iterator itt;
                            if (expr instanceof ErroneousTree && (itt = ((ErroneousTree)expr).getErrorTrees().iterator()).hasNext()) {
                                expr = (Tree)itt.next();
                            }
                            if (expr != lastTree) {
                                return null;
                            }
                        } else {
                            sourcePositions = env.getSourcePositions();
                            root = env.getRoot();
                            text = null;
                            if (efl.getVariable() == null || sourcePositions.getEndPosition(root, (Tree)efl.getVariable()) > (long)offset) {
                                text = controller.getText().substring((int)sourcePositions.getStartPosition(root, (Tree)efl), offset).trim();
                                int idx = text.indexOf(40);
                                if (idx >= 0) {
                                    text = text.substring(idx + 1);
                                }
                            } else {
                                text = controller.getText().substring((int)sourcePositions.getEndPosition(root, (Tree)efl.getVariable()), offset).trim();
                            }
                            if (!":".equals(text)) {
                                return null;
                            }
                        }
                        TypeMirror var = efl.getVariable() != null ? controller.getTrees().getTypeMirror(new TreePath(path, (Tree)efl.getVariable())) : null;
                        return var != null ? Collections.singleton(var) : null;
                    }
                    case SWITCH: {
                        SwitchTree sw = (SwitchTree)tree;
                        if (sw.getExpression() != lastTree) {
                            return null;
                        }
                        HashSet<TypeMirror> ret = new HashSet<TypeMirror>();
                        Types types = controller.getTypes();
                        ret.add(controller.getTypes().getPrimitiveType(TypeKind.INT));
                        TypeElement te = controller.getElements().getTypeElement("java.lang.Enum");
                        if (te != null) {
                            ret.add(types.getDeclaredType(te, new TypeMirror[0]));
                        }
                        if (controller.getSourceVersion().compareTo(SourceVersion.RELEASE_7) >= 0 && (te = controller.getElements().getTypeElement("java.lang.String")) != null) {
                            ret.add(types.getDeclaredType(te, new TypeMirror[0]));
                        }
                        return ret;
                    }
                    case METHOD_INVOCATION: {
                        MethodInvocationTree mi = (MethodInvocationTree)tree;
                        sourcePositions = env.getSourcePositions();
                        root = env.getRoot();
                        argTypes = this.getArgumentsUpToPos(env, mi.getArguments(), (int)sourcePositions.getEndPosition(root, (Tree)mi.getMethodSelect()), lastTree != null ? (int)sourcePositions.getStartPosition(root, lastTree) : offset, true);
                        if (argTypes == null) break;
                        TypeMirror[] args = new TypeMirror[argTypes.size()];
                        int j = 0;
                        for (Tree t : argTypes) {
                            args[j++] = controller.getTrees().getTypeMirror(new TreePath(path, t));
                        }
                        TypeMirror[] targs = null;
                        if (!mi.getTypeArguments().isEmpty()) {
                            targs = new TypeMirror[mi.getTypeArguments().size()];
                            j = 0;
                            for (Tree t2 : mi.getTypeArguments()) {
                                TypeMirror ta = controller.getTrees().getTypeMirror(new TreePath(path, t2));
                                if (ta == null) {
                                    return null;
                                }
                                targs[j++] = ta;
                            }
                        }
                        ExpressionTree mid = mi.getMethodSelect();
                        path = new TreePath(path, (Tree)mid);
                        TypeMirror typeMirror = controller.getTrees().getTypeMirror(path);
                        ExecutableType midTM = typeMirror != null && typeMirror.getKind() == TypeKind.EXECUTABLE ? (ExecutableType)typeMirror : null;
                        ExecutableElement midEl = midTM == null ? null : (ExecutableElement)controller.getTrees().getElement(path);
                        switch (mid.getKind()) {
                            String name;
                            case MEMBER_SELECT: {
                                name = ((MemberSelectTree)mid).getIdentifier().toString();
                                exp = ((MemberSelectTree)mid).getExpression();
                                path = new TreePath(path, (Tree)exp);
                                TypeMirror tm = controller.getTrees().getTypeMirror(path);
                                el = controller.getTrees().getElement(path);
                                final Trees trs = controller.getTrees();
                                if (el != null && tm.getKind() == TypeKind.DECLARED) {
                                    final boolean isStatic = el.getKind().isClass() || el.getKind().isInterface() || el.getKind() == ElementKind.TYPE_PARAMETER;
                                    final boolean isSuperCall = el != null && el.getKind().isField() && el.getSimpleName().contentEquals("super");
                                    final Scope scope = env.getScope();
                                    TypeElement enclClass = scope.getEnclosingClass();
                                    final TypeMirror enclType = enclClass != null ? enclClass.asType() : null;
                                    ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                                        public boolean accept(Element e, TypeMirror t) {
                                            return e.getKind() == ElementKind.METHOD && (!isStatic || e.getModifiers().contains((Object)Modifier.STATIC)) && trs.isAccessible(scope, e, (DeclaredType)(isSuperCall && enclType != null ? enclType : t));
                                        }
                                    };
                                    return this.getMatchingArgumentTypes(tm, controller.getElementUtilities().getMembers(tm, acceptor), name, args, targs, midEl, midTM, controller.getTypes(), controller.getTypeUtilities());
                                }
                                return null;
                            }
                            case IDENTIFIER: {
                                boolean isStatic;
                                name = ((IdentifierTree)mid).getName().toString();
                                final Scope scope = env.getScope();
                                TreeUtilities tu = controller.getTreeUtilities();
                                final Trees trs = controller.getTrees();
                                TypeElement enclClass = scope.getEnclosingClass();
                                boolean bl = enclClass != null ? tu.isStaticContext(scope) || env.getPath().getLeaf().getKind() == Tree.Kind.BLOCK && ((BlockTree)env.getPath().getLeaf()).isStatic() : (isStatic = false);
                                if ("super".equals(name) && enclClass != null) {
                                    ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                                        public boolean accept(Element e, TypeMirror t) {
                                            return e.getKind() == ElementKind.CONSTRUCTOR && trs.isAccessible(scope, e, (DeclaredType)t);
                                        }
                                    };
                                    TypeMirror superclass = enclClass.getSuperclass();
                                    return this.getMatchingArgumentTypes(superclass, controller.getElementUtilities().getMembers(superclass, acceptor), "<init>", args, targs, midEl, midTM, controller.getTypes(), controller.getTypeUtilities());
                                }
                                ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                                    public boolean accept(Element e, TypeMirror t) {
                                        return e.getKind() == ElementKind.METHOD && (!isStatic || e.getModifiers().contains((Object)Modifier.STATIC)) && trs.isAccessible(scope, e, (DeclaredType)t);
                                    }
                                };
                                return this.getMatchingArgumentTypes(enclClass != null ? enclClass.asType() : null, controller.getElementUtilities().getLocalMembersAndVars(scope, acceptor), "this".equals(name) ? "<init>" : name, args, targs, midEl, midTM, controller.getTypes(), controller.getTypeUtilities());
                            }
                        }
                        break;
                    }
                    case NEW_CLASS: {
                        Element elem;
                        NewClassTree nc = (NewClassTree)tree;
                        sourcePositions = env.getSourcePositions();
                        root = env.getRoot();
                        int idEndPos = (int)sourcePositions.getEndPosition(root, (Tree)nc.getIdentifier());
                        if (idEndPos < 0) {
                            idEndPos = (int)sourcePositions.getStartPosition(root, (Tree)nc);
                        }
                        if (idEndPos < 0 || idEndPos >= offset || controller.getText().substring(idEndPos, offset).indexOf(40) < 0 || (argTypes = this.getArgumentsUpToPos(env, nc.getArguments(), idEndPos, lastTree != null ? (int)sourcePositions.getStartPosition(root, lastTree) : offset, true)) == null) break;
                        Trees trees = controller.getTrees();
                        TypeMirror[] args = new TypeMirror[argTypes.size()];
                        int j = 0;
                        for (Tree t : argTypes) {
                            args[j++] = trees.getTypeMirror(new TreePath(path, t));
                        }
                        TypeMirror[] targs = null;
                        if (!nc.getTypeArguments().isEmpty()) {
                            targs = new TypeMirror[nc.getTypeArguments().size()];
                            j = 0;
                            for (Tree t3 : nc.getTypeArguments()) {
                                TypeMirror ta = trees.getTypeMirror(new TreePath(path, t3));
                                if (ta == null) {
                                    return null;
                                }
                                targs[j++] = ta;
                            }
                        }
                        ExecutableElement ncElem = (elem = controller.getTrees().getElement(path)) != null && elem.getKind() == ElementKind.CONSTRUCTOR ? (ExecutableElement)elem : null;
                        TypeMirror ncTM = ncElem != null ? ncElem.asType() : null;
                        ExecutableType ncType = ncTM != null && ncTM.getKind() == TypeKind.EXECUTABLE ? (ExecutableType)ncTM : null;
                        ExpressionTree mid = nc.getIdentifier();
                        TypeMirror tm = trees.getTypeMirror(path = new TreePath(path, (Tree)mid));
                        if (tm != null && tm.getKind() == TypeKind.ERROR && path.getLeaf().getKind() == Tree.Kind.PARAMETERIZED_TYPE) {
                            path = new TreePath(path, ((ParameterizedTypeTree)path.getLeaf()).getType());
                            tm = trees.getTypeMirror(path);
                        }
                        Element el2 = controller.getTrees().getElement(path);
                        final Trees trs = controller.getTrees();
                        if (el2 != null && tm.getKind() == TypeKind.DECLARED) {
                            final Scope scope = env.getScope();
                            final boolean isAnonymous = nc.getClassBody() != null || el2.getKind().isInterface() || el2.getModifiers().contains((Object)Modifier.ABSTRACT);
                            ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                                public boolean accept(Element e, TypeMirror t) {
                                    return e.getKind() == ElementKind.CONSTRUCTOR && (trs.isAccessible(scope, e, (DeclaredType)t) || isAnonymous && e.getModifiers().contains((Object)Modifier.PROTECTED));
                                }
                            };
                            return this.getMatchingArgumentTypes(tm, controller.getElementUtilities().getMembers(tm, acceptor), "<init>", args, targs, ncElem, ncType, controller.getTypes(), controller.getTypeUtilities());
                        }
                        return null;
                    }
                    case NEW_ARRAY: {
                        NewArrayTree nat = (NewArrayTree)tree;
                        Tree arrayType = nat.getType();
                        if (arrayType == null) {
                            ++dim;
                            break;
                        }
                        sourcePositions = env.getSourcePositions();
                        int typeEndPos = (int)sourcePositions.getEndPosition(root = env.getRoot(), arrayType);
                        if (typeEndPos > offset) break;
                        text = controller.getText().substring(typeEndPos, offset);
                        if (text.indexOf(123) >= 0) {
                            type = controller.getTrees().getTypeMirror(new TreePath(path, arrayType));
                            while (dim-- > 0) {
                                if (type.getKind() == TypeKind.ARRAY) {
                                    type = ((ArrayType)type).getComponentType();
                                    continue;
                                }
                                return null;
                            }
                            return type != null ? Collections.singleton(type) : null;
                        }
                        if (text.trim().endsWith("[")) {
                            return Collections.singleton(controller.getTypes().getPrimitiveType(TypeKind.INT));
                        }
                        return null;
                    }
                    case LAMBDA_EXPRESSION: {
                        ExecutableType descType;
                        LambdaExpressionTree let = (LambdaExpressionTree)tree;
                        int pos = (int)env.getSourcePositions().getStartPosition(env.getRoot(), let.getBody());
                        if (offset <= pos && this.findLastNonWhitespaceToken(env, tree, offset).token().id() != JavaTokenId.ARROW || lastTree != null && lastTree.getKind() == Tree.Kind.BLOCK || (type = controller.getTrees().getTypeMirror(path)) == null || type.getKind() != TypeKind.DECLARED || (descType = controller.getTypeUtilities().getDescriptorType((DeclaredType)type)) == null) break;
                        return Collections.singleton(descType.getReturnType());
                    }
                    case CASE: {
                        TreePath parentPath;
                        CaseTree ct = (CaseTree)tree;
                        exp = ct.getExpression();
                        if (exp != null && env.getSourcePositions().getEndPosition(env.getRoot(), (Tree)exp) >= (long)offset && (parentPath = path.getParentPath()).getLeaf().getKind() == Tree.Kind.SWITCH) {
                            exp = ((SwitchTree)parentPath.getLeaf()).getExpression();
                            type = controller.getTrees().getTypeMirror(new TreePath(parentPath, (Tree)exp));
                            return type != null ? Collections.singleton(type) : null;
                        }
                        return null;
                    }
                    case ANNOTATION: {
                        AnnotationTree ann = (AnnotationTree)tree;
                        int pos = (int)env.getSourcePositions().getStartPosition(env.getRoot(), ann.getAnnotationType());
                        if (offset <= pos || offset < (pos = (int)env.getSourcePositions().getEndPosition(env.getRoot(), ann.getAnnotationType()))) break;
                        text = controller.getText().substring(pos, offset).trim();
                        if (("(".equals(text) || text.endsWith("{") || text.endsWith(",")) && (el = (TypeElement)controller.getTrees().getElement(new TreePath(path, ann.getAnnotationType()))) != null) {
                            for (Element ee : el.getEnclosedElements()) {
                                if (ee.getKind() != ElementKind.METHOD || !"value".contentEquals(ee.getSimpleName())) continue;
                                type = ((ExecutableElement)ee).getReturnType();
                                while (dim-- > 0) {
                                    if (type.getKind() == TypeKind.ARRAY) {
                                        type = ((ArrayType)type).getComponentType();
                                        continue;
                                    }
                                    return null;
                                }
                                if (type.getKind() == TypeKind.ARRAY) {
                                    type = ((ArrayType)type).getComponentType();
                                }
                                return type != null ? Collections.singleton(type) : null;
                            }
                        }
                        return null;
                    }
                    case REMAINDER_ASSIGNMENT: 
                    case LEFT_SHIFT_ASSIGNMENT: 
                    case RIGHT_SHIFT_ASSIGNMENT: 
                    case UNSIGNED_RIGHT_SHIFT_ASSIGNMENT: 
                    case AND_ASSIGNMENT: 
                    case XOR_ASSIGNMENT: 
                    case OR_ASSIGNMENT: {
                        cat = (CompoundAssignmentTree)tree;
                        int pos = (int)env.getSourcePositions().getEndPosition(env.getRoot(), (Tree)cat.getVariable());
                        if (offset <= pos) break;
                        HashSet<PrimitiveType> ret = new HashSet<PrimitiveType>();
                        Types types = controller.getTypes();
                        ret.add(types.getPrimitiveType(TypeKind.BYTE));
                        ret.add(types.getPrimitiveType(TypeKind.CHAR));
                        ret.add(types.getPrimitiveType(TypeKind.INT));
                        ret.add(types.getPrimitiveType(TypeKind.LONG));
                        ret.add(types.getPrimitiveType(TypeKind.SHORT));
                        return ret;
                    }
                    case AND: 
                    case LEFT_SHIFT: 
                    case OR: 
                    case REMAINDER: 
                    case RIGHT_SHIFT: 
                    case UNSIGNED_RIGHT_SHIFT: 
                    case XOR: {
                        bt = (BinaryTree)tree;
                        int pos = (int)env.getSourcePositions().getEndPosition(env.getRoot(), (Tree)bt.getLeftOperand());
                        if (offset <= pos) break;
                    }
                    case BITWISE_COMPLEMENT: {
                        HashSet<PrimitiveType> ret = new HashSet<PrimitiveType>();
                        Types types = controller.getTypes();
                        ret.add(types.getPrimitiveType(TypeKind.BYTE));
                        ret.add(types.getPrimitiveType(TypeKind.CHAR));
                        ret.add(types.getPrimitiveType(TypeKind.INT));
                        ret.add(types.getPrimitiveType(TypeKind.LONG));
                        ret.add(types.getPrimitiveType(TypeKind.SHORT));
                        return ret;
                    }
                    case CONDITIONAL_AND: 
                    case CONDITIONAL_OR: {
                        bt = (BinaryTree)tree;
                        int pos = (int)env.getSourcePositions().getEndPosition(env.getRoot(), (Tree)bt.getLeftOperand());
                        if (offset <= pos) break;
                    }
                    case LOGICAL_COMPLEMENT: {
                        return Collections.singleton(controller.getTypes().getPrimitiveType(TypeKind.BOOLEAN));
                    }
                    case EQUAL_TO: 
                    case NOT_EQUAL_TO: 
                    case PLUS: {
                        bt = (BinaryTree)tree;
                        int pos = (int)env.getSourcePositions().getEndPosition(env.getRoot(), (Tree)bt.getLeftOperand());
                        if (offset <= pos) break;
                        TypeMirror tm = controller.getTrees().getTypeMirror(new TreePath(path, (Tree)bt.getLeftOperand()));
                        if (tm == null) {
                            return null;
                        }
                        if (tm.getKind().isPrimitive()) {
                            HashSet<PrimitiveType> ret = new HashSet<PrimitiveType>();
                            Types types = controller.getTypes();
                            ret.add(types.getPrimitiveType(TypeKind.BYTE));
                            ret.add(types.getPrimitiveType(TypeKind.CHAR));
                            ret.add(types.getPrimitiveType(TypeKind.DOUBLE));
                            ret.add(types.getPrimitiveType(TypeKind.FLOAT));
                            ret.add(types.getPrimitiveType(TypeKind.INT));
                            ret.add(types.getPrimitiveType(TypeKind.LONG));
                            ret.add(types.getPrimitiveType(TypeKind.SHORT));
                            return ret;
                        }
                        return Collections.singleton(tm);
                    }
                    case PLUS_ASSIGNMENT: {
                        cat = (CompoundAssignmentTree)tree;
                        int pos = (int)env.getSourcePositions().getEndPosition(env.getRoot(), (Tree)cat.getVariable());
                        if (offset <= pos) break;
                        TypeMirror tm = controller.getTrees().getTypeMirror(new TreePath(path, (Tree)cat.getVariable()));
                        if (tm == null) {
                            return null;
                        }
                        if (tm.getKind().isPrimitive()) {
                            HashSet<PrimitiveType> ret = new HashSet<PrimitiveType>();
                            Types types = controller.getTypes();
                            ret.add(types.getPrimitiveType(TypeKind.BYTE));
                            ret.add(types.getPrimitiveType(TypeKind.CHAR));
                            ret.add(types.getPrimitiveType(TypeKind.DOUBLE));
                            ret.add(types.getPrimitiveType(TypeKind.FLOAT));
                            ret.add(types.getPrimitiveType(TypeKind.INT));
                            ret.add(types.getPrimitiveType(TypeKind.LONG));
                            ret.add(types.getPrimitiveType(TypeKind.SHORT));
                            return ret;
                        }
                        return Collections.singleton(tm);
                    }
                    case MULTIPLY_ASSIGNMENT: 
                    case DIVIDE_ASSIGNMENT: 
                    case MINUS_ASSIGNMENT: {
                        cat = (CompoundAssignmentTree)tree;
                        int pos = (int)env.getSourcePositions().getEndPosition(env.getRoot(), (Tree)cat.getVariable());
                        if (offset <= pos) break;
                        HashSet<PrimitiveType> ret = new HashSet<PrimitiveType>();
                        Types types = controller.getTypes();
                        ret.add(types.getPrimitiveType(TypeKind.BYTE));
                        ret.add(types.getPrimitiveType(TypeKind.CHAR));
                        ret.add(types.getPrimitiveType(TypeKind.DOUBLE));
                        ret.add(types.getPrimitiveType(TypeKind.FLOAT));
                        ret.add(types.getPrimitiveType(TypeKind.INT));
                        ret.add(types.getPrimitiveType(TypeKind.LONG));
                        ret.add(types.getPrimitiveType(TypeKind.SHORT));
                        return ret;
                    }
                    case DIVIDE: 
                    case GREATER_THAN: 
                    case GREATER_THAN_EQUAL: 
                    case LESS_THAN: 
                    case LESS_THAN_EQUAL: 
                    case MINUS: 
                    case MULTIPLY: {
                        bt = (BinaryTree)tree;
                        int pos = (int)env.getSourcePositions().getEndPosition(env.getRoot(), (Tree)bt.getLeftOperand());
                        if (offset <= pos) break;
                    }
                    case PREFIX_INCREMENT: 
                    case PREFIX_DECREMENT: 
                    case UNARY_PLUS: 
                    case UNARY_MINUS: {
                        HashSet<PrimitiveType> ret = new HashSet<PrimitiveType>();
                        Types types = controller.getTypes();
                        ret.add(types.getPrimitiveType(TypeKind.BYTE));
                        ret.add(types.getPrimitiveType(TypeKind.CHAR));
                        ret.add(types.getPrimitiveType(TypeKind.DOUBLE));
                        ret.add(types.getPrimitiveType(TypeKind.FLOAT));
                        ret.add(types.getPrimitiveType(TypeKind.INT));
                        ret.add(types.getPrimitiveType(TypeKind.LONG));
                        ret.add(types.getPrimitiveType(TypeKind.SHORT));
                        return ret;
                    }
                    case EXPRESSION_STATEMENT: {
                        exp = ((ExpressionStatementTree)tree).getExpression();
                        if (exp.getKind() != Tree.Kind.PARENTHESIZED || !(text = controller.getText().substring((int)env.getSourcePositions().getStartPosition(env.getRoot(), (Tree)exp), offset).trim()).endsWith(")")) break;
                        return null;
                    }
                }
                lastTree = tree;
            }
            return null;
        }

        private TokenSequence<JavaTokenId> findFirstNonWhitespaceToken(Env env, int startPos, int endPos) {
            TokenSequence<JavaTokenId> ts = env.getController().getTokenHierarchy().tokenSequence(JavaTokenId.language());
            ts.move(startPos);
            ts = this.nextNonWhitespaceToken(ts);
            if (ts == null || ts.offset() >= endPos) {
                return null;
            }
            return ts;
        }

        private TokenSequence<JavaTokenId> nextNonWhitespaceToken(TokenSequence<JavaTokenId> ts) {
            block3 : while (ts.moveNext()) {
                switch ((JavaTokenId)ts.token().id()) {
                    case WHITESPACE: 
                    case LINE_COMMENT: 
                    case BLOCK_COMMENT: 
                    case JAVADOC_COMMENT: {
                        continue block3;
                    }
                }
                return ts;
            }
            return null;
        }

        private TokenSequence<JavaTokenId> findLastNonWhitespaceToken(Env env, Tree tree, int position) {
            int startPos = (int)env.getSourcePositions().getStartPosition(env.getRoot(), tree);
            return this.findLastNonWhitespaceToken(env, startPos, position);
        }

        private TokenSequence<JavaTokenId> findLastNonWhitespaceToken(Env env, int startPos, int endPos) {
            TokenSequence<JavaTokenId> ts = env.getController().getTokenHierarchy().tokenSequence(JavaTokenId.language());
            ts.move(endPos);
            ts = this.previousNonWhitespaceToken(ts);
            if (ts == null || ts.offset() < startPos) {
                return null;
            }
            return ts;
        }

        private TokenSequence<JavaTokenId> previousNonWhitespaceToken(TokenSequence<JavaTokenId> ts) {
            block3 : while (ts.movePrevious()) {
                switch ((JavaTokenId)ts.token().id()) {
                    case WHITESPACE: 
                    case LINE_COMMENT: 
                    case BLOCK_COMMENT: 
                    case JAVADOC_COMMENT: {
                        continue block3;
                    }
                }
                return ts;
            }
            return null;
        }

        private List<Tree> getArgumentsUpToPos(Env env, Iterable<? extends ExpressionTree> args, int startPos, int position, boolean strict) {
            ExpressionTree e;
            int pos;
            ArrayList<Tree> ret = new ArrayList<Tree>();
            CompilationUnitTree root = env.getRoot();
            SourcePositions sourcePositions = env.getSourcePositions();
            if (args == null) {
                return null;
            }
            Iterator<? extends ExpressionTree> i$ = args.iterator();
            while (i$.hasNext() && (long)(pos = (int)sourcePositions.getEndPosition(root, (Tree)(e = i$.next()))) != -1 && (position > pos || !strict && position == pos)) {
                startPos = pos;
                ret.add((Tree)e);
            }
            if (startPos < 0) {
                return ret;
            }
            if (position >= startPos) {
                TokenSequence<JavaTokenId> last = this.findLastNonWhitespaceToken(env, startPos, position);
                if (last == null) {
                    if (!strict && !ret.isEmpty()) {
                        ret.remove(ret.size() - 1);
                        return ret;
                    }
                } else if (last.token().id() == JavaTokenId.LPAREN || last.token().id() == JavaTokenId.COMMA) {
                    return ret;
                }
            }
            return null;
        }

        private List<Pair<ExecutableElement, ExecutableType>> getMatchingExecutables(TypeMirror type, Iterable<? extends Element> elements, String name, TypeMirror[] argTypes, Types types) {
            ArrayList<Pair<ExecutableElement, ExecutableType>> ret = new ArrayList<Pair<ExecutableElement, ExecutableType>>();
            block0 : for (Element e : elements) {
                if (e.getKind() != ElementKind.CONSTRUCTOR && e.getKind() != ElementKind.METHOD || !name.contentEquals(e.getSimpleName())) continue;
                List<? extends VariableElement> params = ((ExecutableElement)e).getParameters();
                int parSize = params.size();
                boolean varArgs = ((ExecutableElement)e).isVarArgs();
                if (!varArgs && parSize < argTypes.length) continue;
                ExecutableType eType = (ExecutableType)this.asMemberOf(e, type, types);
                if (parSize == 0) {
                    ret.add(new Pair(e, eType));
                    continue;
                }
                Iterator<? extends TypeMirror> parIt = eType.getParameterTypes().iterator();
                TypeMirror param = null;
                for (int i = 0; i <= argTypes.length; ++i) {
                    if (parIt.hasNext()) {
                        param = parIt.next();
                        if (!parIt.hasNext() && param.getKind() == TypeKind.ARRAY) {
                            param = ((ArrayType)param).getComponentType();
                        }
                    } else if (!varArgs) continue block0;
                    if (i == argTypes.length) {
                        ret.add(new Pair(e, eType));
                        continue block0;
                    }
                    if (argTypes[i] == null || !types.isAssignable(argTypes[i], param)) continue block0;
                }
            }
            return ret;
        }

        private List<List<String>> getMatchingParams(CompilationInfo info, TypeMirror type, Iterable<? extends Element> elements, String name, TypeMirror[] argTypes, Types types) {
            ArrayList<List<String>> ret = new ArrayList<List<String>>();
            block0 : for (Element e : elements) {
                if (e.getKind() != ElementKind.CONSTRUCTOR && e.getKind() != ElementKind.METHOD || !name.contentEquals(e.getSimpleName())) continue;
                List<? extends VariableElement> params = ((ExecutableElement)e).getParameters();
                int parSize = params.size();
                boolean varArgs = ((ExecutableElement)e).isVarArgs();
                if (!varArgs && parSize < argTypes.length) continue;
                if (parSize == 0) {
                    ret.add(Collections.singletonList(NbBundle.getMessage(JavaCompletionProvider.class, (String)"JCP-no-parameters")));
                    continue;
                }
                ExecutableType eType = (ExecutableType)this.asMemberOf(e, type, types);
                Iterator<? extends TypeMirror> parIt = eType.getParameterTypes().iterator();
                TypeMirror param = null;
                for (int i = 0; i <= argTypes.length; ++i) {
                    if (parIt.hasNext()) {
                        param = parIt.next();
                        if (!parIt.hasNext() && param.getKind() == TypeKind.ARRAY) {
                            param = ((ArrayType)param).getComponentType();
                        }
                    } else if (!varArgs) continue block0;
                    if (i == argTypes.length) {
                        ArrayList<String> paramStrings = new ArrayList<String>(parSize);
                        Iterator<? extends TypeMirror> tIt = eType.getParameterTypes().iterator();
                        Iterator<? extends VariableElement> it = params.iterator();
                        while (it.hasNext()) {
                            Name veName;
                            VariableElement ve = it.next();
                            StringBuffer sb = new StringBuffer();
                            sb.append(Utilities.getTypeName(info, tIt.next(), false));
                            if (varArgs && !tIt.hasNext()) {
                                sb.delete(sb.length() - 2, sb.length()).append("...");
                            }
                            if ((veName = ve.getSimpleName()) != null && veName.length() > 0) {
                                sb.append(" ");
                                sb.append(veName);
                            }
                            if (it.hasNext()) {
                                sb.append(", ");
                            }
                            paramStrings.add(sb.toString());
                        }
                        ret.add(paramStrings);
                        continue block0;
                    }
                    if (argTypes[i] == null || argTypes[i].getKind() != TypeKind.ERROR && !types.isAssignable(argTypes[i], param)) continue block0;
                }
            }
            return ret.isEmpty() ? null : ret;
        }

        private Set<TypeMirror> getMatchingArgumentTypes(TypeMirror type, Iterable<? extends Element> elements, String name, TypeMirror[] argTypes, TypeMirror[] typeArgTypes, ExecutableElement prototypeSym, ExecutableType prototype, Types types, TypeUtilities tu) {
            HashSet<TypeMirror> ret = new HashSet<TypeMirror>();
            List<TypeMirror> tatList = typeArgTypes != null ? Arrays.asList(typeArgTypes) : null;
            block0 : for (Element e : elements) {
                if (e.getKind() != ElementKind.CONSTRUCTOR && e.getKind() != ElementKind.METHOD || !name.contentEquals(e.getSimpleName())) continue;
                List<? extends VariableElement> params = ((ExecutableElement)e).getParameters();
                int parSize = params.size();
                boolean varArgs = ((ExecutableElement)e).isVarArgs();
                if (!varArgs && parSize <= argTypes.length) continue;
                ExecutableType meth = e == prototypeSym && prototype != null ? prototype : (ExecutableType)this.asMemberOf(e, type, types);
                Iterator<? extends TypeMirror> parIt = meth.getParameterTypes().iterator();
                TypeMirror param = null;
                HashMap<TypeVariable, TypeMirror> table = new HashMap<TypeVariable, TypeMirror>();
                for (int i = 0; i <= argTypes.length; ++i) {
                    if (parIt.hasNext()) {
                        param = parIt.next();
                    } else if (!varArgs) continue block0;
                    if (tatList != null && param.getKind() == TypeKind.DECLARED && tatList.size() == meth.getTypeVariables().size()) {
                        param = tu.substitute(param, meth.getTypeVariables(), tatList);
                    }
                    if (i == argTypes.length) {
                        TypeMirror toAdd = null;
                        if (i < parSize) {
                            toAdd = param;
                        }
                        if (varArgs && !parIt.hasNext() && param.getKind() == TypeKind.ARRAY) {
                            toAdd = ((ArrayType)param).getComponentType();
                        }
                        if (toAdd == null || !ret.add(toAdd) || toAdd.getKind() == TypeKind.TYPEVAR) continue block0;
                        TypeMirror toRemove = null;
                        for (TypeMirror tm : ret) {
                            if (tm == toAdd) continue;
                            TypeMirror tmErasure = types.erasure(tm);
                            TypeMirror toAddErasure = types.erasure(toAdd);
                            if (types.isSubtype(toAddErasure, tmErasure)) {
                                toRemove = toAdd;
                                break;
                            }
                            if (!types.isSubtype(tmErasure, toAddErasure)) continue;
                            toRemove = tm;
                            break;
                        }
                        if (toRemove == null || toRemove.getKind().isPrimitive() || "java.lang.String".equals(toRemove.toString()) || "char[]".equals(toRemove.toString())) continue block0;
                        ret.remove(toRemove);
                        continue block0;
                    }
                    if (argTypes[i] == null) continue block0;
                    if (varArgs && !parIt.hasNext() && param.getKind() == TypeKind.ARRAY) {
                        if (types.isAssignable(argTypes[i], param)) {
                            varArgs = false;
                            continue;
                        }
                        if (argTypes[i].getKind() == TypeKind.ERROR || types.isAssignable(argTypes[i], ((ArrayType)param).getComponentType())) continue;
                        continue block0;
                    }
                    if (argTypes[i].getKind() == TypeKind.ERROR || types.isAssignable(argTypes[i], param)) continue;
                    if (tatList != null || param.getKind() != TypeKind.DECLARED || argTypes[i].getKind() != TypeKind.DECLARED || !types.isAssignable(types.erasure(argTypes[i]), types.erasure(param))) continue block0;
                    Iterator<? extends TypeMirror> argTypeTAs = ((DeclaredType)argTypes[i]).getTypeArguments().iterator();
                    for (TypeMirror paramTA : ((DeclaredType)param).getTypeArguments()) {
                        if (!argTypeTAs.hasNext() || paramTA.getKind() != TypeKind.TYPEVAR) break;
                        table.put((TypeVariable)paramTA, argTypeTAs.next());
                    }
                    if (table.size() != meth.getTypeVariables().size()) continue;
                    tatList = new ArrayList<TypeMirror>(meth.getTypeVariables().size());
                    for (TypeVariable tv : meth.getTypeVariables()) {
                        tatList.add((TypeMirror)table.get(tv));
                    }
                }
            }
            return ret.isEmpty() ? null : ret;
        }

        private TypeMirror asMemberOf(Element element, TypeMirror type, Types types) {
            TypeMirror ret = element.asType();
            TypeMirror enclType = element.getEnclosingElement().asType();
            if (enclType.getKind() == TypeKind.DECLARED) {
                enclType = types.erasure(enclType);
            }
            while (type != null && type.getKind() == TypeKind.DECLARED) {
                if ((enclType.getKind() != TypeKind.DECLARED || ((DeclaredType)enclType).asElement().getSimpleName().length() > 0) && types.isSubtype(type, enclType)) {
                    ret = types.asMemberOf((DeclaredType)type, element);
                    break;
                }
                type = ((DeclaredType)type).getEnclosingType();
            }
            return ret;
        }

        private Tree unwrapErrTree(Tree tree) {
            if (tree != null && tree.getKind() == Tree.Kind.ERRONEOUS) {
                Iterator it = ((ErroneousTree)tree).getErrorTrees().iterator();
                tree = it.hasNext() ? (Tree)it.next() : null;
            }
            return tree;
        }

        private boolean withinScope(Env env, TypeElement e) throws IOException {
            for (Element encl = env.getScope().getEnclosingClass(); encl != null; encl = encl.getEnclosingElement()) {
                if (e != encl) continue;
                return true;
            }
            return false;
        }

        private boolean withinLabeledStatement(Env env) {
            for (TreePath path = env.getPath(); path != null; path = path.getParentPath()) {
                if (path.getLeaf().getKind() != Tree.Kind.LABELED_STATEMENT) continue;
                return true;
            }
            return false;
        }

        private String fullName(Tree tree) {
            switch (tree.getKind()) {
                case IDENTIFIER: {
                    return ((IdentifierTree)tree).getName().toString();
                }
                case MEMBER_SELECT: {
                    String sname = this.fullName((Tree)((MemberSelectTree)tree).getExpression());
                    return sname == null ? null : sname + '.' + ((MemberSelectTree)tree).getIdentifier();
                }
            }
            return null;
        }

        private DeclaredType getDeclaredType(TypeElement e, HashMap<? extends Element, ? extends TypeMirror> map, Types types) {
            List<? extends TypeParameterElement> tpes = e.getTypeParameters();
            TypeMirror[] targs = new TypeMirror[tpes.size()];
            int i = 0;
            for (TypeParameterElement tpe : tpes) {
                TypeMirror t = map.get(tpe);
                targs[i++] = t != null ? t : tpe.asType();
            }
            Element encl = e.getEnclosingElement();
            if ((encl.getKind().isClass() || encl.getKind().isInterface()) && !((TypeElement)encl).getTypeParameters().isEmpty()) {
                return types.getDeclaredType(this.getDeclaredType((TypeElement)encl, map, types), e, targs);
            }
            return types.getDeclaredType(e, targs);
        }

        /*
         * Enabled force condition propagation
         * Lifted jumps to return sites
         */
        private Env getCompletionEnvironment(CompilationController controller, int queryType) throws IOException {
            controller.toPhase(JavaSource.Phase.PARSED);
            int offset = controller.getSnapshot().getEmbeddedOffset(this.caretOffset);
            if (offset < 0 || offset > controller.getText().length()) {
                return null;
            }
            String prefix = null;
            if (offset > 0) {
                TokenSequence ts;
                if (queryType != 2) {
                    int len;
                    ts = controller.getTokenHierarchy().tokenSequence(JavaTokenId.language());
                    if (ts.move(offset) == 0 || !ts.moveNext()) {
                        ts.movePrevious();
                    }
                    if ((len = offset - ts.offset()) > 0 && ts.token().length() >= len) {
                        if (ts.token().id() == JavaTokenId.IDENTIFIER || ((JavaTokenId)ts.token().id()).primaryCategory().startsWith("keyword") || ((JavaTokenId)ts.token().id()).primaryCategory().startsWith("string") || ((JavaTokenId)ts.token().id()).primaryCategory().equals("literal")) {
                            prefix = ts.token().text().toString().substring(0, len);
                            offset = ts.offset();
                        } else if ((ts.token().id() == JavaTokenId.DOUBLE_LITERAL || ts.token().id() == JavaTokenId.FLOAT_LITERAL || ts.token().id() == JavaTokenId.FLOAT_LITERAL_INVALID || ts.token().id() == JavaTokenId.LONG_LITERAL) && ts.token().text().charAt(0) == '.') {
                            prefix = ts.token().text().toString().substring(1, len);
                            offset = ts.offset() + 1;
                        }
                    }
                } else {
                    ts = controller.getTokenHierarchy().tokenSequence(JavaTokenId.language());
                    ts.move(offset);
                    if (!ts.moveNext()) {
                        ts.movePrevious();
                    }
                    if (ts.offset() == offset && ts.token().length() > 0 && (ts.token().id() == JavaTokenId.IDENTIFIER || ((JavaTokenId)ts.token().id()).primaryCategory().startsWith("keyword") || ((JavaTokenId)ts.token().id()).primaryCategory().startsWith("string") || ((JavaTokenId)ts.token().id()).primaryCategory().equals("literal"))) {
                        ++offset;
                    }
                }
            }
            offset = Math.min(offset, controller.getText().length());
            TreePath path = controller.getTreeUtilities().pathFor(offset);
            if (queryType != 2) {
                for (TreePath treePath = path; treePath != null; treePath = treePath.getParentPath()) {
                    TreePath pPath;
                    TreePath gpPath = (pPath = treePath.getParentPath()) != null ? pPath.getParentPath() : null;
                    Env env = this.getEnvImpl(controller, path, treePath, pPath, gpPath, offset, prefix, true);
                    if (env == null) continue;
                    return env;
                }
                return new Env(offset, prefix, controller, path, path, controller.getTrees().getSourcePositions(), null);
            } else {
                if (JavaSource.Phase.RESOLVED.compareTo((Enum)controller.getPhase()) <= 0) return new Env(offset, prefix, controller, path, path, controller.getTrees().getSourcePositions(), null);
                LinkedList<TreePath> reversePath = new LinkedList<TreePath>();
                for (TreePath treePath = path; treePath != null; treePath = treePath.getParentPath()) {
                    reversePath.addFirst(treePath);
                }
                Iterator i$ = reversePath.iterator();
                while (i$.hasNext()) {
                    TreePath pPath;
                    TreePath tp;
                    TreePath gpPath = (pPath = (tp = (TreePath)i$.next()).getParentPath()) != null ? pPath.getParentPath() : null;
                    Env env = this.getEnvImpl(controller, path, tp, pPath, gpPath, offset, prefix, false);
                    if (env == null) continue;
                    return env;
                }
            }
            return new Env(offset, prefix, controller, path, path, controller.getTrees().getSourcePositions(), null);
        }

        private Env getEnvImpl(CompilationController controller, TreePath orig, TreePath path, TreePath pPath, TreePath gpPath, int offset, String prefix, boolean upToOffset) throws IOException {
            Tree tree = path != null ? path.getLeaf() : null;
            Tree parent = pPath != null ? pPath.getLeaf() : null;
            Tree grandParent = gpPath != null ? gpPath.getLeaf() : null;
            SourcePositions sourcePositions = controller.getTrees().getSourcePositions();
            CompilationUnitTree root = controller.getCompilationUnit();
            TreeUtilities tu = controller.getTreeUtilities();
            if (upToOffset && TreeUtilities.CLASS_TREE_KINDS.contains((Object)tree.getKind())) {
                controller.toPhase(Utilities.inAnonymousOrLocalClass(path) ? JavaSource.Phase.RESOLVED : JavaSource.Phase.ELEMENTS_RESOLVED);
                return new Env(offset, prefix, controller, orig, orig, sourcePositions, null);
            }
            if (parent != null && tree.getKind() == Tree.Kind.BLOCK && (parent.getKind() == Tree.Kind.METHOD || TreeUtilities.CLASS_TREE_KINDS.contains((Object)parent.getKind()))) {
                BlockTree block;
                controller.toPhase(Utilities.inAnonymousOrLocalClass(path) ? JavaSource.Phase.RESOLVED : JavaSource.Phase.ELEMENTS_RESOLVED);
                int blockPos = (int)sourcePositions.getStartPosition(root, tree);
                String blockText = controller.getText().substring(blockPos, upToOffset ? offset : (int)sourcePositions.getEndPosition(root, tree));
                SourcePositions[] sp = new SourcePositions[1];
                BlockTree blockTree = block = ((BlockTree)tree).isStatic() ? tu.parseStaticBlock(blockText, sp) : tu.parseStatement(blockText, sp);
                if (block == null) {
                    return null;
                }
                sourcePositions = new SourcePositionsImpl((Tree)block, sourcePositions, sp[0], blockPos, upToOffset ? offset : -1);
                Scope scope = controller.getTrees().getScope(path);
                path = tu.pathFor(new TreePath(pPath, (Tree)block), offset, sourcePositions);
                if (upToOffset) {
                    Tree last = path.getLeaf();
                    List stmts = null;
                    switch (path.getLeaf().getKind()) {
                        case BLOCK: {
                            stmts = ((BlockTree)path.getLeaf()).getStatements();
                            break;
                        }
                        case FOR_LOOP: {
                            stmts = ((ForLoopTree)path.getLeaf()).getInitializer();
                            break;
                        }
                        case ENHANCED_FOR_LOOP: {
                            stmts = Collections.singletonList(((EnhancedForLoopTree)path.getLeaf()).getStatement());
                            break;
                        }
                        case METHOD: {
                            stmts = ((MethodTree)path.getLeaf()).getParameters();
                            break;
                        }
                        case SWITCH: {
                            CaseTree lastCase = null;
                            Iterator i$ = ((SwitchTree)path.getLeaf()).getCases().iterator();
                            while (i$.hasNext()) {
                                CaseTree caseTree;
                                lastCase = caseTree = (CaseTree)i$.next();
                            }
                            if (lastCase == null) break;
                            stmts = lastCase.getStatements();
                            break;
                        }
                        case CASE: {
                            stmts = ((CaseTree)path.getLeaf()).getStatements();
                        }
                    }
                    if (stmts != null) {
                        for (StatementTree st : stmts) {
                            if (sourcePositions.getEndPosition(root, (Tree)st) > (long)offset) continue;
                            last = st;
                        }
                    }
                    scope = tu.reattributeTreeTo((Tree)block, scope, last);
                } else {
                    tu.reattributeTreeTo((Tree)block, scope, (Tree)block);
                }
                return new Env(offset, prefix, controller, path, orig, sourcePositions, scope);
            }
            if (tree.getKind() == Tree.Kind.LAMBDA_EXPRESSION) {
                SourcePositions[] sp;
                int bodyPos;
                controller.toPhase(JavaSource.Phase.RESOLVED);
                Tree lambdaBody = ((LambdaExpressionTree)tree).getBody();
                Scope scope = null;
                for (TreePath blockPath = path.getParentPath(); blockPath != null; blockPath = blockPath.getParentPath()) {
                    BlockTree block;
                    if (blockPath.getLeaf().getKind() != Tree.Kind.BLOCK || blockPath.getParentPath().getLeaf().getKind() != Tree.Kind.METHOD && !TreeUtilities.CLASS_TREE_KINDS.contains((Object)blockPath.getParentPath().getLeaf().getKind())) continue;
                    int blockPos = (int)sourcePositions.getStartPosition(root, blockPath.getLeaf());
                    String blockText = controller.getText().substring(blockPos, (int)sourcePositions.getEndPosition(root, blockPath.getLeaf()));
                    sp = new SourcePositions[1];
                    BlockTree blockTree = block = ((BlockTree)blockPath.getLeaf()).isStatic() ? tu.parseStaticBlock(blockText, sp) : tu.parseStatement(blockText, sp);
                    if (block == null) {
                        return null;
                    }
                    sourcePositions = new SourcePositionsImpl((Tree)block, sourcePositions, sp[0], blockPos, -1);
                    scope = controller.getTrees().getScope(blockPath);
                    path = Utilities.getPathElementOfKind(Tree.Kind.LAMBDA_EXPRESSION, tu.pathFor(new TreePath(blockPath.getParentPath(), (Tree)block), offset, sourcePositions));
                    lambdaBody = ((LambdaExpressionTree)path.getLeaf()).getBody();
                    scope = tu.reattributeTreeTo((Tree)block, scope, lambdaBody);
                    break;
                }
                if (scope == null) {
                    scope = controller.getTrees().getScope(new TreePath(path, lambdaBody));
                }
                if ((bodyPos = (int)sourcePositions.getStartPosition(root, lambdaBody)) >= offset) {
                    TokenSequence ts = controller.getTokenHierarchy().tokenSequence(JavaTokenId.language());
                    ts.move(offset);
                    block23 : while (ts.movePrevious()) {
                        switch ((JavaTokenId)ts.token().id()) {
                            case WHITESPACE: 
                            case LINE_COMMENT: 
                            case BLOCK_COMMENT: 
                            case JAVADOC_COMMENT: {
                                continue block23;
                            }
                            case ARROW: {
                                return new Env(offset, prefix, controller, path, orig, sourcePositions, scope);
                            }
                        }
                        return null;
                    }
                }
                String bodyText = controller.getText().substring(bodyPos, upToOffset ? offset : (int)sourcePositions.getEndPosition(root, lambdaBody));
                sp = new SourcePositions[1];
                StatementTree body = bodyText.charAt(0) == '{' ? tu.parseStatement(bodyText, sp) : tu.parseExpression(bodyText, sp);
                ExpressionStatementTree fake = body instanceof ExpressionTree ? new ExpressionStatementTree((Tree)body){
                    final /* synthetic */ Tree val$body;

                    public Object accept(TreeVisitor v, Object p) {
                        return v.visitExpressionStatement((ExpressionStatementTree)this, p);
                    }

                    public ExpressionTree getExpression() {
                        return (ExpressionTree)this.val$body;
                    }

                    public Tree.Kind getKind() {
                        return Tree.Kind.EXPRESSION_STATEMENT;
                    }
                } : body;
                sourcePositions = new SourcePositionsImpl((Tree)fake, sourcePositions, sp[0], bodyPos, upToOffset ? offset : -1);
                path = tu.pathFor(new TreePath(path, (Tree)fake), offset, sourcePositions);
                if (upToOffset && !(body instanceof ExpressionTree)) {
                    Tree last = path.getLeaf();
                    List stmts = null;
                    switch (path.getLeaf().getKind()) {
                        case BLOCK: {
                            stmts = ((BlockTree)path.getLeaf()).getStatements();
                            break;
                        }
                        case FOR_LOOP: {
                            stmts = ((ForLoopTree)path.getLeaf()).getInitializer();
                            break;
                        }
                        case ENHANCED_FOR_LOOP: {
                            stmts = Collections.singletonList(((EnhancedForLoopTree)path.getLeaf()).getStatement());
                            break;
                        }
                        case METHOD: {
                            stmts = ((MethodTree)path.getLeaf()).getParameters();
                            break;
                        }
                        case SWITCH: {
                            CaseTree lastCase = null;
                            Iterator i$ = ((SwitchTree)path.getLeaf()).getCases().iterator();
                            while (i$.hasNext()) {
                                CaseTree caseTree;
                                lastCase = caseTree = (CaseTree)i$.next();
                            }
                            if (lastCase == null) break;
                            stmts = lastCase.getStatements();
                            break;
                        }
                        case CASE: {
                            stmts = ((CaseTree)path.getLeaf()).getStatements();
                        }
                    }
                    if (stmts != null) {
                        for (StatementTree st : stmts) {
                            if (sourcePositions.getEndPosition(root, (Tree)st) > (long)offset) continue;
                            last = st;
                        }
                    }
                    scope = tu.reattributeTreeTo((Tree)body, scope, last);
                } else {
                    scope = tu.reattributeTreeTo((Tree)body, scope, (Tree)body);
                }
                return new Env(offset, prefix, controller, path, orig, sourcePositions, scope);
            }
            if (grandParent != null && TreeUtilities.CLASS_TREE_KINDS.contains((Object)grandParent.getKind()) && parent != null && parent.getKind() == Tree.Kind.VARIABLE && this.unwrapErrTree((Tree)((VariableTree)parent).getInitializer()) == tree) {
                if (tu.isEnum((ClassTree)grandParent)) {
                    controller.toPhase(JavaSource.Phase.RESOLVED);
                    return null;
                }
                controller.toPhase(Utilities.inAnonymousOrLocalClass(path) ? JavaSource.Phase.RESOLVED : JavaSource.Phase.ELEMENTS_RESOLVED);
                Scope scope = controller.getTrees().getScope(path);
                int initPos = (int)sourcePositions.getStartPosition(root, tree);
                String initText = controller.getText().substring(initPos, upToOffset ? offset : (int)sourcePositions.getEndPosition(root, tree));
                if (initText.length() > 0) {
                    SourcePositions[] sp = new SourcePositions[1];
                    final ExpressionTree init = tu.parseVariableInitializer(initText, sp);
                    ExpressionStatementTree fake = new ExpressionStatementTree(){

                        public Object accept(TreeVisitor v, Object p) {
                            return v.visitExpressionStatement((ExpressionStatementTree)this, p);
                        }

                        public ExpressionTree getExpression() {
                            return init;
                        }

                        public Tree.Kind getKind() {
                            return Tree.Kind.EXPRESSION_STATEMENT;
                        }
                    };
                    sourcePositions = new SourcePositionsImpl((Tree)fake, sourcePositions, sp[0], initPos, upToOffset ? offset : -1);
                    path = tu.pathFor(new TreePath(pPath, (Tree)fake), offset, sourcePositions);
                    if (upToOffset && sp[0].getEndPosition(root, (Tree)init) + (long)initPos > (long)offset) {
                        scope = tu.reattributeTreeTo((Tree)init, scope, path.getLeaf());
                    } else {
                        tu.reattributeTree((Tree)init, scope);
                    }
                }
                return new Env(offset, prefix, controller, path, orig, sourcePositions, scope);
            }
            if (parent != null && TreeUtilities.CLASS_TREE_KINDS.contains((Object)parent.getKind()) && tree.getKind() == Tree.Kind.VARIABLE && ((VariableTree)tree).getInitializer() != null && orig == path && sourcePositions.getStartPosition(root, (Tree)((VariableTree)tree).getInitializer()) >= 0 && sourcePositions.getStartPosition(root, (Tree)((VariableTree)tree).getInitializer()) <= (long)offset) {
                controller.toPhase(Utilities.inAnonymousOrLocalClass(path) ? JavaSource.Phase.RESOLVED : JavaSource.Phase.ELEMENTS_RESOLVED);
                tree = ((VariableTree)tree).getInitializer();
                Scope scope = controller.getTrees().getScope(new TreePath(path, tree));
                int initPos = (int)sourcePositions.getStartPosition(root, tree);
                String initText = controller.getText().substring(initPos, offset);
                if (initText.length() > 0) {
                    SourcePositions[] sp = new SourcePositions[1];
                    final ExpressionTree init = tu.parseVariableInitializer(initText, sp);
                    ExpressionStatementTree fake = new ExpressionStatementTree(){

                        public Object accept(TreeVisitor v, Object p) {
                            return v.visitExpressionStatement((ExpressionStatementTree)this, p);
                        }

                        public ExpressionTree getExpression() {
                            return init;
                        }

                        public Tree.Kind getKind() {
                            return Tree.Kind.EXPRESSION_STATEMENT;
                        }
                    };
                    sourcePositions = new SourcePositionsImpl((Tree)fake, sourcePositions, sp[0], initPos, offset);
                    path = tu.pathFor(new TreePath(path, (Tree)fake), offset, sourcePositions);
                    tu.reattributeTree((Tree)init, scope);
                }
                return new Env(offset, prefix, controller, path, orig, sourcePositions, scope);
            }
            return null;
        }

        private boolean startsWith(Env env, String theString) {
            String prefix = env.getPrefix();
            return this.startsWith(env, theString, prefix);
        }

        private boolean startsWith(Env env, String theString, String prefix) {
            return env.isCamelCasePrefix() ? (Utilities.isCaseSensitive() ? Utilities.startsWithCamelCase(theString, prefix) : Utilities.startsWithCamelCase(theString, prefix) || Utilities.startsWith(theString, prefix)) : Utilities.startsWith(theString, prefix);
        }

        private boolean withinBounds(Env env, TypeMirror type, List<? extends TypeMirror> bounds) {
            if (bounds != null) {
                Types types = env.getController().getTypes();
                for (TypeMirror bound : bounds) {
                    if (types.isSubtype(type, bound)) continue;
                    return false;
                }
            }
            return true;
        }

        private static boolean isCamelCasePrefix(String prefix) {
            if (prefix == null || prefix.length() < 2 || prefix.charAt(0) == '\"') {
                return false;
            }
            for (int i = 1; i < prefix.length(); ++i) {
                if (!Character.isUpperCase(prefix.charAt(i))) continue;
                return true;
            }
            return false;
        }

        private static class Pair<A, B> {
            private A a;
            private B b;

            private Pair(A a, B b) {
                this.a = a;
                this.b = b;
            }
        }

        private class Env {
            private int offset;
            private String prefix;
            private boolean isCamelCasePrefix;
            private CompilationController controller;
            private TreePath path;
            private TreePath originalPath;
            private SourcePositions sourcePositions;
            private Scope scope;
            private ReferencesCount referencesCount;
            private Collection<? extends Element> refs;
            private boolean afterExtends;
            private boolean insideNew;
            private boolean insideForEachExpression;
            private boolean insideClass;
            private Set<? extends TypeMirror> smartTypes;
            private Set<Element> excludes;
            private boolean checkAccessibility;
            private boolean addSemicolon;
            private boolean checkAddSemicolon;
            private int assignToVarPos;
            private WhiteListQuery.WhiteList[] whiteList;

            private Env(int offset, String prefix, CompilationController controller, TreePath path, TreePath originalPath, SourcePositions sourcePositions, Scope scope) {
                this.refs = null;
                this.afterExtends = false;
                this.insideNew = false;
                this.insideForEachExpression = false;
                this.insideClass = false;
                this.smartTypes = null;
                this.excludes = null;
                this.addSemicolon = false;
                this.checkAddSemicolon = true;
                this.assignToVarPos = -2;
                this.offset = offset;
                this.prefix = prefix;
                this.isCamelCasePrefix = JavaCompletionQuery.isCamelCasePrefix(prefix);
                this.controller = controller;
                this.path = path;
                this.originalPath = originalPath;
                this.sourcePositions = sourcePositions;
                this.scope = scope;
                Object prop = JavaCompletionQuery.this.component != null ? JavaCompletionQuery.this.component.getDocument().getProperty("org.netbeans.modules.editor.java.JavaCompletionProvider.skipAccessibilityCheck") : null;
                this.checkAccessibility = !(prop instanceof String) || !Boolean.parseBoolean((String)prop);
            }

            public WhiteListQuery.WhiteList getWhiteList() {
                if (this.whiteList == null) {
                    FileObject file = this.controller.getFileObject();
                    WhiteListQuery.WhiteList[] arrwhiteList = new WhiteListQuery.WhiteList[1];
                    arrwhiteList[0] = file == null ? null : WhiteListQuery.getWhiteList((FileObject)file);
                    this.whiteList = arrwhiteList;
                }
                return this.whiteList[0];
            }

            public int getOffset() {
                return this.offset;
            }

            public String getPrefix() {
                return this.prefix;
            }

            public boolean isCamelCasePrefix() {
                return this.isCamelCasePrefix;
            }

            public CompilationController getController() {
                return this.controller;
            }

            public CompilationUnitTree getRoot() {
                return this.path.getCompilationUnit();
            }

            public TreePath getPath() {
                return this.path;
            }

            public TreePath getOriginalPath() {
                return this.originalPath;
            }

            public SourcePositions getSourcePositions() {
                return this.sourcePositions;
            }

            public Scope getScope() throws IOException {
                if (this.scope == null) {
                    this.controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                    this.scope = this.controller.getTreeUtilities().scopeFor(this.offset);
                }
                return this.scope;
            }

            public ReferencesCount getReferencesCount() {
                if (this.referencesCount == null) {
                    this.referencesCount = ReferencesCount.get((ClasspathInfo)this.controller.getClasspathInfo());
                }
                return this.referencesCount;
            }

            public Collection<? extends Element> getForwardReferences() {
                if (this.refs == null) {
                    this.refs = Utilities.getForwardReferences(this.path, this.offset, this.sourcePositions, this.controller.getTrees());
                }
                return this.refs;
            }

            public boolean isAfterExtends() {
                return this.afterExtends;
            }

            public void afterExtends() {
                this.afterExtends = true;
            }

            public void insideForEachExpression() {
                this.insideForEachExpression = true;
            }

            public boolean isInsideForEachExpression() {
                return this.insideForEachExpression;
            }

            public void insideNew() {
                this.insideNew = true;
            }

            public boolean isInsideNew() {
                return this.insideNew;
            }

            public void insideClass(boolean insideClass) {
                this.insideClass = insideClass;
            }

            public boolean isInsideClass() {
                return this.insideClass;
            }

            public Set<? extends TypeMirror> getSmartTypes() throws IOException {
                if (this.smartTypes == null) {
                    this.controller.toPhase(JavaSource.Phase.RESOLVED);
                    this.smartTypes = JavaCompletionQuery.this.getSmartTypes(this);
                    if (this.smartTypes != null) {
                        Iterator<? extends TypeMirror> it = this.smartTypes.iterator();
                        TypeMirror err = null;
                        if (it.hasNext()) {
                            err = it.next();
                            if (it.hasNext() || err.getKind() != TypeKind.ERROR) {
                                err = null;
                            }
                        }
                        if (err != null) {
                            HashSet<? extends TypeMirror> st = new HashSet<TypeMirror>();
                            Types types = this.controller.getTypes();
                            TypeElement te = (TypeElement)((DeclaredType)err).asElement();
                            if (te.getQualifiedName() == te.getSimpleName()) {
                                ClassIndex ci = this.controller.getClasspathInfo().getClassIndex();
                                for (ElementHandle eh : ci.getDeclaredTypes(te.getSimpleName().toString(), ClassIndex.NameKind.SIMPLE_NAME, EnumSet.allOf(ClassIndex.SearchScope.class))) {
                                    te = (TypeElement)eh.resolve((CompilationInfo)this.controller);
                                    if (te == null) continue;
                                    st.add(types.erasure(te.asType()));
                                }
                            }
                            this.smartTypes = st;
                        }
                    }
                }
                return this.smartTypes;
            }

            public void addToExcludes(Element toExclude) {
                if (toExclude != null) {
                    if (this.excludes == null) {
                        this.excludes = new HashSet<Element>();
                    }
                    this.excludes.add(toExclude);
                }
            }

            public Set<? extends Element> getExcludes() {
                return this.excludes;
            }

            public boolean isAccessible(Scope scope, Element member, TypeMirror type, boolean selectSuper) {
                if (!this.checkAccessibility) {
                    return true;
                }
                if (type.getKind() != TypeKind.DECLARED) {
                    return member.getModifiers().contains((Object)Modifier.PUBLIC);
                }
                if (this.getController().getTrees().isAccessible(scope, member, (DeclaredType)type)) {
                    return true;
                }
                return selectSuper && member.getModifiers().contains((Object)Modifier.PROTECTED) && !member.getModifiers().contains((Object)Modifier.STATIC) && !member.getKind().isClass() && !member.getKind().isInterface() && this.getController().getTrees().isAccessible(scope, (TypeElement)((DeclaredType)type).asElement()) && (member.getKind() != ElementKind.METHOD || this.getController().getElementUtilities().getImplementationOf((ExecutableElement)member, (TypeElement)((DeclaredType)type).asElement()) == member);
            }

            public boolean addSemicolon() {
                if (this.checkAddSemicolon) {
                    TreePath tp = this.getPath();
                    Tree tree = tp.getLeaf();
                    if ((tree.getKind() == Tree.Kind.IDENTIFIER || tree.getKind() == Tree.Kind.PRIMITIVE_TYPE) && (tp = tp.getParentPath()).getLeaf().getKind() == Tree.Kind.VARIABLE && ((VariableTree)tp.getLeaf()).getType() == tree) {
                        this.addSemicolon = true;
                    }
                    if (tp.getLeaf().getKind() == Tree.Kind.MEMBER_SELECT || tp.getLeaf().getKind() == Tree.Kind.METHOD_INVOCATION && ((MethodInvocationTree)tp.getLeaf()).getMethodSelect() == tree || tp.getLeaf().getKind() == Tree.Kind.VARIABLE) {
                        tp = tp.getParentPath();
                    }
                    if (tp.getLeaf().getKind() == Tree.Kind.EXPRESSION_STATEMENT && tp.getParentPath().getLeaf().getKind() != Tree.Kind.LAMBDA_EXPRESSION || tp.getLeaf().getKind() == Tree.Kind.BLOCK || tp.getLeaf().getKind() == Tree.Kind.RETURN) {
                        this.addSemicolon = true;
                    }
                    this.checkAddSemicolon = false;
                }
                return this.addSemicolon;
            }

            public int assignToVarPos() {
                if (this.assignToVarPos < -1) {
                    TreePath tp = this.getPath();
                    Tree tree = tp.getLeaf();
                    if (tp.getLeaf().getKind() == Tree.Kind.MEMBER_SELECT || tp.getLeaf().getKind() == Tree.Kind.METHOD_INVOCATION && ((MethodInvocationTree)tp.getLeaf()).getMethodSelect() == tree) {
                        tp = tp.getParentPath();
                    }
                    this.assignToVarPos = tp.getLeaf().getKind() == Tree.Kind.EXPRESSION_STATEMENT ? this.getController().getSnapshot().getOriginalOffset((int)this.getSourcePositions().getStartPosition(this.getRoot(), tree)) : (tp.getLeaf().getKind() == Tree.Kind.BLOCK ? this.getController().getSnapshot().getOriginalOffset(this.offset) : -1);
                }
                return this.assignToVarPos;
            }
        }

        private class Task
        extends UserTask {
            private Task() {
            }

            public void run(ResultIterator resultIterator) throws Exception {
                CompilationController controller;
                Parser.Result result = resultIterator.getParserResult(JavaCompletionQuery.this.caretOffset);
                CompilationController compilationController = controller = result != null ? CompilationController.get((Parser.Result)result) : null;
                if (controller != null) {
                    JavaCompletionQuery.this.run(controller);
                }
            }
        }

        private class SourcePositionsImpl
        extends TreeScanner<Void, Tree>
        implements SourcePositions {
            private Tree root;
            private SourcePositions original;
            private SourcePositions modified;
            private int startOffset;
            private int endOffset;
            private boolean found;

            private SourcePositionsImpl(Tree root, SourcePositions original, SourcePositions modified, int startOffset, int endOffset) {
                this.root = root;
                this.original = original;
                this.modified = modified;
                this.startOffset = startOffset;
                this.endOffset = endOffset;
            }

            public long getStartPosition(CompilationUnitTree compilationUnitTree, Tree tree) {
                if (tree == this.root) {
                    return this.startOffset;
                }
                this.found = false;
                this.scan(this.root, tree);
                return this.found ? this.modified.getStartPosition(compilationUnitTree, tree) + (long)this.startOffset : this.original.getStartPosition(compilationUnitTree, tree);
            }

            public long getEndPosition(CompilationUnitTree compilationUnitTree, Tree tree) {
                if (tree == this.root) {
                    return this.endOffset;
                }
                this.found = false;
                this.scan(this.root, tree);
                return this.found ? this.modified.getEndPosition(compilationUnitTree, tree) + (long)this.startOffset : this.original.getEndPosition(compilationUnitTree, tree);
            }

            public Void scan(Tree node, Tree p) {
                if (node == p) {
                    this.found = true;
                } else {
                    TreeScanner.super.scan(node, (Object)p);
                }
                return null;
            }
        }

    }

}

