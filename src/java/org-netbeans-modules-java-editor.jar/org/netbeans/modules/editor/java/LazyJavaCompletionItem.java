/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.Scope
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.editor.completion.Completion
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.api.java.source.support.ReferencesCount
 *  org.netbeans.api.options.OptionsDisplayer
 *  org.netbeans.api.whitelist.WhiteListQuery
 *  org.netbeans.api.whitelist.WhiteListQuery$WhiteList
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.spi.editor.completion.CompletionItem
 *  org.netbeans.spi.editor.completion.CompletionTask
 *  org.netbeans.spi.editor.completion.CompositeCompletionItem
 *  org.netbeans.spi.editor.completion.LazyCompletionItem
 *  org.netbeans.spi.editor.completion.support.CompletionUtilities
 *  org.openide.util.ImageUtilities
 */
package org.netbeans.modules.editor.java;

import com.sun.source.tree.Scope;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.swing.ImageIcon;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.completion.Completion;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.support.ReferencesCount;
import org.netbeans.api.options.OptionsDisplayer;
import org.netbeans.api.whitelist.WhiteListQuery;
import org.netbeans.modules.editor.java.Bundle;
import org.netbeans.modules.editor.java.JavaCompletionItem;
import org.netbeans.modules.editor.java.JavaCompletionProvider;
import org.netbeans.modules.editor.java.LazySortText;
import org.netbeans.modules.editor.java.Utilities;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.spi.editor.completion.CompletionItem;
import org.netbeans.spi.editor.completion.CompletionTask;
import org.netbeans.spi.editor.completion.CompositeCompletionItem;
import org.netbeans.spi.editor.completion.LazyCompletionItem;
import org.netbeans.spi.editor.completion.support.CompletionUtilities;
import org.openide.util.ImageUtilities;

public abstract class LazyJavaCompletionItem<T extends Element>
extends JavaCompletionItem.WhiteListJavaCompletionItem<T>
implements LazyCompletionItem {
    private Source source;
    private JavaCompletionItem delegate = null;

    public static JavaCompletionItem createTypeItem(ElementHandle<TypeElement> handle, EnumSet<ElementKind> kinds, int substitutionOffset, ReferencesCount referencesCount, Source source, boolean insideNew, boolean addTypeVars, boolean afterExtends, WhiteListQuery.WhiteList whiteList) {
        return new TypeItem(handle, kinds, substitutionOffset, referencesCount, source, insideNew, addTypeVars, afterExtends, whiteList);
    }

    public static JavaCompletionItem createStaticMemberItem(ElementHandle<TypeElement> handle, String name, int substitutionOffset, boolean addSemicolon, ReferencesCount referencesCount, Source source, WhiteListQuery.WhiteList whiteList) {
        return new StaticMemberItem(handle, name, substitutionOffset, addSemicolon, referencesCount, source, whiteList);
    }

    private static CompletionItem createExcludeItem(CharSequence name) {
        if (name == null) {
            ExcludeFromCompletionItem item;
            ExcludeFromCompletionItem excludeFromCompletionItem = item = CONFIGURE_ITEM != null ? (ExcludeFromCompletionItem)CONFIGURE_ITEM.get() : null;
            if (item == null) {
                item = new ExcludeFromCompletionItem(name);
                CONFIGURE_ITEM = new WeakReference<ExcludeFromCompletionItem>(item);
            }
            return item;
        }
        return new ExcludeFromCompletionItem(name);
    }

    private LazyJavaCompletionItem(int substitutionOffset, ElementHandle<? extends Element> handle, Source source, WhiteListQuery.WhiteList whiteList) {
        super(substitutionOffset, handle, whiteList);
        this.source = source;
    }

    public boolean accept() {
        if (this.delegate == null && this.getElementHandle() != null) {
            try {
                JavaCompletionProvider.JavaCompletionQuery.javadocBreak.set(true);
                ParserManager.parse(Collections.singletonList(this.source), (UserTask)new UserTask(){

                    public void run(ResultIterator resultIterator) throws Exception {
                        CompilationController controller = CompilationController.get((Parser.Result)resultIterator.getParserResult(LazyJavaCompletionItem.this.substitutionOffset));
                        controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                        Element t = LazyJavaCompletionItem.this.getElementHandle().resolve((CompilationInfo)controller);
                        if (t != null) {
                            LazyJavaCompletionItem.this.delegate = LazyJavaCompletionItem.this.getDelegate((CompilationInfo)controller, t);
                        }
                    }
                });
            }
            catch (ParseException t) {
                // empty catch block
            }
        }
        return this.delegate != null;
    }

    protected abstract JavaCompletionItem getDelegate(CompilationInfo var1, T var2);

    @Override
    public void defaultAction(JTextComponent component) {
        if (this.delegate != null) {
            this.delegate.defaultAction(component);
        }
    }

    @Override
    public void processKeyEvent(KeyEvent evt) {
        if (this.delegate != null) {
            this.delegate.processKeyEvent(evt);
        }
    }

    @Override
    public int getPreferredWidth(Graphics g, Font defaultFont) {
        if (this.delegate != null) {
            return this.delegate.getPreferredWidth(g, defaultFont);
        }
        return 0;
    }

    @Override
    public void render(Graphics g, Font defaultFont, Color defaultColor, Color backgroundColor, int width, int height, boolean selected) {
        if (this.delegate != null) {
            this.delegate.render(g, defaultFont, defaultColor, backgroundColor, width, height, selected);
        }
    }

    @Override
    public CompletionTask createDocumentationTask() {
        if (this.delegate != null) {
            return this.delegate.createDocumentationTask();
        }
        return null;
    }

    @Override
    public CompletionTask createToolTipTask() {
        if (this.delegate != null) {
            return this.delegate.createToolTipTask();
        }
        return null;
    }

    private static class ExcludeFromCompletionItem
    implements CompletionItem {
        private static final ImageIcon icon = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/editor/hints/resources/suggestion.gif", (boolean)false);
        private static WeakReference<ExcludeFromCompletionItem> CONFIGURE_ITEM;
        private CharSequence name;
        private String text;

        private ExcludeFromCompletionItem(CharSequence name) {
            this.name = name;
            this.text = name == null ? Bundle.configure_Excludes_Lbl() : Bundle.exclude_Lbl(name);
        }

        public void defaultAction(JTextComponent component) {
            Completion.get().hideAll();
            if (this.name == null) {
                OptionsDisplayer.getDefault().open("Editor/CodeCompletion/text/x-java");
            } else {
                Utilities.exclude(this.name);
                Completion.get().showCompletion();
            }
        }

        public void processKeyEvent(KeyEvent evt) {
        }

        public int getPreferredWidth(Graphics g, Font defaultFont) {
            return CompletionUtilities.getPreferredWidth((String)this.text, (String)null, (Graphics)g, (Font)defaultFont);
        }

        public void render(Graphics g, Font defaultFont, Color defaultColor, Color backgroundColor, int width, int height, boolean selected) {
            CompletionUtilities.renderHtml((ImageIcon)icon, (String)this.text, (String)null, (Graphics)g, (Font)defaultFont, (Color)defaultColor, (int)width, (int)height, (boolean)selected);
        }

        public CompletionTask createDocumentationTask() {
            return null;
        }

        public CompletionTask createToolTipTask() {
            return null;
        }

        public boolean instantSubstitution(JTextComponent component) {
            return false;
        }

        public int getSortPriority() {
            return 10;
        }

        public CharSequence getSortText() {
            return this.text;
        }

        public CharSequence getInsertPrefix() {
            return null;
        }
    }

    private static class StaticMemberItem
    extends LazyJavaCompletionItem<TypeElement> {
        private boolean addSemicolon;
        private String name;
        private CharSequence sortText;

        private StaticMemberItem(ElementHandle<TypeElement> handle, String name, int substitutionOffset, boolean addSemicolon, ReferencesCount referencesCount, Source source, WhiteListQuery.WhiteList whiteList) {
            super(substitutionOffset, handle, source, whiteList);
            this.name = name;
            this.sortText = new LazySortText(this.name, handle.getQualifiedName(), handle, referencesCount);
            this.addSemicolon = addSemicolon;
        }

        @Override
        protected JavaCompletionItem getDelegate(CompilationInfo info, TypeElement te) {
            Elements elements = info.getElements();
            Trees trees = info.getTrees();
            Scope scope = info.getTrees().getScope(info.getTreeUtilities().pathFor(this.substitutionOffset));
            if (te != null) {
                Element element = null;
                boolean multiVersion = false;
                for (Element e : te.getEnclosedElements()) {
                    if (!e.getKind().isField() && e.getKind() != ElementKind.METHOD || !this.name.contentEquals(Utilities.isCaseSensitive() ? e.getSimpleName() : e.getSimpleName().toString().toLowerCase()) || !e.getModifiers().contains((Object)Modifier.STATIC) || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(e) || !trees.isAccessible(scope, e, (DeclaredType)te.asType())) continue;
                    if (element != null) {
                        multiVersion = true;
                        break;
                    }
                    element = e;
                }
                if (element != null) {
                    this.name = element.getSimpleName().toString();
                    return StaticMemberItem.createStaticMemberItem(info, (DeclaredType)te.asType(), element, element.asType(), multiVersion, this.substitutionOffset, elements.isDeprecated(element), this.addSemicolon, this.getWhiteList());
                }
            }
            return null;
        }

        public int getSortPriority() {
            return 690;
        }

        public CharSequence getSortText() {
            return this.sortText;
        }

        public CharSequence getInsertPrefix() {
            return this.name;
        }
    }

    static class TypeItem
    extends LazyJavaCompletionItem<TypeElement>
    implements CompositeCompletionItem {
        private EnumSet<ElementKind> kinds;
        private boolean insideNew;
        private boolean addTypeVars;
        private boolean afterExtends;
        private String name;
        private String simpleName;
        private String pkgName;
        private CharSequence sortText;
        private ReferencesCount referencesCount;
        private List<CompletionItem> subItems = new ArrayList<CompletionItem>();

        private TypeItem(ElementHandle<TypeElement> handle, EnumSet<ElementKind> kinds, int substitutionOffset, ReferencesCount referencesCount, Source source, boolean insideNew, boolean addTypeVars, boolean afterExtends, WhiteListQuery.WhiteList whiteList) {
            super(substitutionOffset, handle, source, whiteList);
            this.kinds = kinds;
            this.insideNew = insideNew;
            this.addTypeVars = addTypeVars;
            this.afterExtends = afterExtends;
            this.name = handle.getQualifiedName();
            int idx = this.name.lastIndexOf(46);
            this.simpleName = idx > -1 ? this.name.substring(idx + 1) : this.name;
            this.pkgName = idx > -1 ? this.name.substring(0, idx) : "";
            this.sortText = new LazySortText(this.simpleName, this.pkgName, handle, referencesCount);
            this.referencesCount = referencesCount;
        }

        @Override
        protected JavaCompletionItem getDelegate(CompilationInfo info, TypeElement te) {
            Trees trees = info.getTrees();
            Elements elements = info.getElements();
            Scope scope = trees.getScope(info.getTreeUtilities().pathFor(this.substitutionOffset));
            if (!(te == null || !Utilities.isShowDeprecatedMembers() && elements.isDeprecated(te) || !trees.isAccessible(scope, te) || !this.isOfKind(te, this.kinds) || this.afterExtends && te.getModifiers().contains((Object)Modifier.FINAL) || this.isInDefaultPackage(te) && !this.isInDefaultPackage(scope.getEnclosingClass()) || Utilities.isExcluded(te.getQualifiedName()))) {
                this.subItems.add(LazyJavaCompletionItem.createExcludeItem(te.getQualifiedName()));
                this.subItems.add(LazyJavaCompletionItem.createExcludeItem(elements.getPackageOf(te).getQualifiedName()));
                this.subItems.add(LazyJavaCompletionItem.createExcludeItem(null));
                return TypeItem.createTypeItem(info, te, (DeclaredType)te.asType(), this.substitutionOffset, this.referencesCount, elements.isDeprecated(te), this.insideNew, this.addTypeVars, false, false, false, this.getWhiteList());
            }
            return null;
        }

        public int getSortPriority() {
            return 700;
        }

        public CharSequence getSortText() {
            return this.sortText;
        }

        public CharSequence getInsertPrefix() {
            return this.simpleName;
        }

        public List<? extends CompletionItem> getSubItems() {
            return this.subItems;
        }

        public String toString() {
            return this.name;
        }

        private boolean isOfKind(Element e, EnumSet<ElementKind> kinds) {
            if (kinds.contains((Object)e.getKind())) {
                return true;
            }
            for (Element ee : e.getEnclosedElements()) {
                if (!this.isOfKind(ee, kinds)) continue;
                return true;
            }
            return false;
        }

        private boolean isInDefaultPackage(Element e) {
            while (e != null && e.getKind() != ElementKind.PACKAGE) {
                e = e.getEnclosingElement();
            }
            return e != null && e.getSimpleName().length() == 0;
        }
    }

}

