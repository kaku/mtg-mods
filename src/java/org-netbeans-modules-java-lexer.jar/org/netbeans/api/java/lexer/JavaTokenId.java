/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.InputAttributes
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.api.lexer.PartType
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.spi.lexer.LanguageEmbedding
 *  org.netbeans.spi.lexer.LanguageHierarchy
 *  org.netbeans.spi.lexer.Lexer
 *  org.netbeans.spi.lexer.LexerRestartInfo
 */
package org.netbeans.api.java.lexer;

import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.api.java.lexer.JavaStringTokenId;
import org.netbeans.api.java.lexer.JavadocTokenId;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.PartType;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.java.lexer.JavaCharacterTokenId;
import org.netbeans.lib.java.lexer.JavaLexer;
import org.netbeans.spi.lexer.LanguageEmbedding;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;

public enum JavaTokenId implements TokenId
{
    ERROR(null, "error"),
    IDENTIFIER(null, "identifier"),
    ABSTRACT("abstract", "keyword"),
    ASSERT("assert", "keyword-directive"),
    BOOLEAN("boolean", "keyword"),
    BREAK("break", "keyword-directive"),
    BYTE("byte", "keyword"),
    CASE("case", "keyword-directive"),
    CATCH("catch", "keyword-directive"),
    CHAR("char", "keyword"),
    CLASS("class", "keyword"),
    CONST("const", "keyword"),
    CONTINUE("continue", "keyword-directive"),
    DEFAULT("default", "keyword-directive"),
    DO("do", "keyword-directive"),
    DOUBLE("double", "keyword"),
    ELSE("else", "keyword-directive"),
    ENUM("enum", "keyword"),
    EXTENDS("extends", "keyword"),
    FINAL("final", "keyword"),
    FINALLY("finally", "keyword-directive"),
    FLOAT("float", "keyword"),
    FOR("for", "keyword-directive"),
    GOTO("goto", "keyword-directive"),
    IF("if", "keyword-directive"),
    IMPLEMENTS("implements", "keyword"),
    IMPORT("import", "keyword"),
    INSTANCEOF("instanceof", "keyword"),
    INT("int", "keyword"),
    INTERFACE("interface", "keyword"),
    LONG("long", "keyword"),
    NATIVE("native", "keyword"),
    NEW("new", "keyword"),
    PACKAGE("package", "keyword"),
    PRIVATE("private", "keyword"),
    PROTECTED("protected", "keyword"),
    PUBLIC("public", "keyword"),
    RETURN("return", "keyword-directive"),
    SHORT("short", "keyword"),
    STATIC("static", "keyword"),
    STRICTFP("strictfp", "keyword"),
    SUPER("super", "keyword"),
    SWITCH("switch", "keyword-directive"),
    SYNCHRONIZED("synchronized", "keyword"),
    THIS("this", "keyword"),
    THROW("throw", "keyword-directive"),
    THROWS("throws", "keyword"),
    TRANSIENT("transient", "keyword"),
    TRY("try", "keyword-directive"),
    VOID("void", "keyword"),
    VOLATILE("volatile", "keyword"),
    WHILE("while", "keyword-directive"),
    INT_LITERAL(null, "number"),
    LONG_LITERAL(null, "number"),
    FLOAT_LITERAL(null, "number"),
    DOUBLE_LITERAL(null, "number"),
    CHAR_LITERAL(null, "character"),
    STRING_LITERAL(null, "string"),
    TRUE("true", "literal"),
    FALSE("false", "literal"),
    NULL("null", "literal"),
    LPAREN("(", "separator"),
    RPAREN(")", "separator"),
    LBRACE("{", "separator"),
    RBRACE("}", "separator"),
    LBRACKET("[", "separator"),
    RBRACKET("]", "separator"),
    SEMICOLON(";", "separator"),
    COMMA(",", "separator"),
    DOT(".", "separator"),
    COLONCOLON("::", "separator"),
    EQ("=", "operator"),
    GT(">", "operator"),
    LT("<", "operator"),
    BANG("!", "operator"),
    TILDE("~", "operator"),
    QUESTION("?", "operator"),
    COLON(":", "operator"),
    EQEQ("==", "operator"),
    LTEQ("<=", "operator"),
    GTEQ(">=", "operator"),
    BANGEQ("!=", "operator"),
    AMPAMP("&&", "operator"),
    BARBAR("||", "operator"),
    PLUSPLUS("++", "operator"),
    MINUSMINUS("--", "operator"),
    PLUS("+", "operator"),
    MINUS("-", "operator"),
    STAR("*", "operator"),
    SLASH("/", "operator"),
    AMP("&", "operator"),
    BAR("|", "operator"),
    CARET("^", "operator"),
    PERCENT("%", "operator"),
    LTLT("<<", "operator"),
    GTGT(">>", "operator"),
    GTGTGT(">>>", "operator"),
    PLUSEQ("+=", "operator"),
    MINUSEQ("-=", "operator"),
    STAREQ("*=", "operator"),
    SLASHEQ("/=", "operator"),
    AMPEQ("&=", "operator"),
    BAREQ("|=", "operator"),
    CARETEQ("^=", "operator"),
    PERCENTEQ("%=", "operator"),
    LTLTEQ("<<=", "operator"),
    GTGTEQ(">>=", "operator"),
    GTGTGTEQ(">>>=", "operator"),
    ARROW("->", "operator"),
    ELLIPSIS("...", "special"),
    AT("@", "special"),
    WHITESPACE(null, "whitespace"),
    LINE_COMMENT(null, "comment"),
    BLOCK_COMMENT(null, "comment"),
    JAVADOC_COMMENT(null, "comment"),
    INVALID_COMMENT_END("*/", "error"),
    FLOAT_LITERAL_INVALID(null, "number");
    
    private final String fixedText;
    private final String primaryCategory;
    private static final Language<JavaTokenId> language;

    private JavaTokenId(String fixedText, String primaryCategory) {
        this.fixedText = fixedText;
        this.primaryCategory = primaryCategory;
    }

    public String fixedText() {
        return this.fixedText;
    }

    public String primaryCategory() {
        return this.primaryCategory;
    }

    public static Language<JavaTokenId> language() {
        return language;
    }

    static {
        language = new LanguageHierarchy<JavaTokenId>(){

            protected String mimeType() {
                return "text/x-java";
            }

            protected Collection<JavaTokenId> createTokenIds() {
                return EnumSet.allOf(JavaTokenId.class);
            }

            protected Map<String, Collection<JavaTokenId>> createTokenCategories() {
                HashMap<String, Collection<JavaTokenId>> cats = new HashMap<String, Collection<JavaTokenId>>();
                cats.put("error", EnumSet.of(JavaTokenId.FLOAT_LITERAL_INVALID));
                EnumSet<JavaTokenId> l = EnumSet.of(JavaTokenId.INT_LITERAL, JavaTokenId.LONG_LITERAL, JavaTokenId.FLOAT_LITERAL, JavaTokenId.DOUBLE_LITERAL, JavaTokenId.CHAR_LITERAL);
                l.add(JavaTokenId.STRING_LITERAL);
                cats.put("literal", l);
                return cats;
            }

            protected Lexer<JavaTokenId> createLexer(LexerRestartInfo<JavaTokenId> info) {
                return new JavaLexer(info);
            }

            protected LanguageEmbedding<?> embedding(Token<JavaTokenId> token, LanguagePath languagePath, InputAttributes inputAttributes) {
                switch ((JavaTokenId)token.id()) {
                    case JAVADOC_COMMENT: {
                        return LanguageEmbedding.create(JavadocTokenId.language(), (int)3, (int)(token.partType() == PartType.COMPLETE ? 2 : 0));
                    }
                    case STRING_LITERAL: {
                        return LanguageEmbedding.create(JavaStringTokenId.language(), (int)1, (int)(token.partType() == PartType.COMPLETE ? 1 : 0));
                    }
                    case CHAR_LITERAL: {
                        return LanguageEmbedding.create(JavaCharacterTokenId.language(), (int)1, (int)(token.partType() == PartType.COMPLETE ? 1 : 0));
                    }
                }
                return null;
            }
        }.language();
    }

}

