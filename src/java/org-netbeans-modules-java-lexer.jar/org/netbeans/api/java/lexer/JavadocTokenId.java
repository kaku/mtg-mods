/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.spi.lexer.LanguageHierarchy
 *  org.netbeans.spi.lexer.Lexer
 *  org.netbeans.spi.lexer.LexerRestartInfo
 */
package org.netbeans.api.java.lexer;

import java.util.Collection;
import java.util.EnumSet;
import java.util.Map;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.java.lexer.JavadocLexer;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;

public enum JavadocTokenId implements TokenId
{
    IDENT("comment"),
    TAG("javadoc-tag"),
    HTML_TAG("html-tag"),
    DOT("comment"),
    HASH("comment"),
    OTHER_TEXT("comment");
    
    private final String primaryCategory;
    private static final Language<JavadocTokenId> language;

    private JavadocTokenId() {
        this(null);
    }

    private JavadocTokenId(String primaryCategory) {
        this.primaryCategory = primaryCategory;
    }

    public String primaryCategory() {
        return this.primaryCategory;
    }

    public static Language<JavadocTokenId> language() {
        return language;
    }

    static {
        language = new LanguageHierarchy<JavadocTokenId>(){

            protected Collection<JavadocTokenId> createTokenIds() {
                return EnumSet.allOf(JavadocTokenId.class);
            }

            protected Map<String, Collection<JavadocTokenId>> createTokenCategories() {
                return null;
            }

            protected Lexer<JavadocTokenId> createLexer(LexerRestartInfo<JavadocTokenId> info) {
                return new JavadocLexer(info);
            }

            protected String mimeType() {
                return "text/x-javadoc";
            }
        }.language();
    }

}

