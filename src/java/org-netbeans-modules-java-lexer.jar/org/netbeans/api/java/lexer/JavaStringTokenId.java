/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.spi.lexer.LanguageHierarchy
 *  org.netbeans.spi.lexer.Lexer
 *  org.netbeans.spi.lexer.LexerRestartInfo
 */
package org.netbeans.api.java.lexer;

import java.util.Collection;
import java.util.EnumSet;
import java.util.Map;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.java.lexer.JavaStringLexer;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;

public enum JavaStringTokenId implements TokenId
{
    TEXT("string"),
    BACKSPACE("string-escape"),
    FORM_FEED("string-escape"),
    NEWLINE("string-escape"),
    CR("string-escape"),
    TAB("string-escape"),
    SINGLE_QUOTE("string-escape"),
    DOUBLE_QUOTE("string-escape"),
    BACKSLASH("string-escape"),
    OCTAL_ESCAPE("string-escape"),
    OCTAL_ESCAPE_INVALID("string-escape-invalid"),
    UNICODE_ESCAPE("string-escape"),
    UNICODE_ESCAPE_INVALID("string-escape-invalid"),
    ESCAPE_SEQUENCE_INVALID("string-escape-invalid");
    
    private final String primaryCategory;
    private static final Language<JavaStringTokenId> language;

    private JavaStringTokenId() {
        this(null);
    }

    private JavaStringTokenId(String primaryCategory) {
        this.primaryCategory = primaryCategory;
    }

    public String primaryCategory() {
        return this.primaryCategory;
    }

    public static Language<JavaStringTokenId> language() {
        return language;
    }

    static {
        language = new LanguageHierarchy<JavaStringTokenId>(){

            protected Collection<JavaStringTokenId> createTokenIds() {
                return EnumSet.allOf(JavaStringTokenId.class);
            }

            protected Map<String, Collection<JavaStringTokenId>> createTokenCategories() {
                return null;
            }

            protected Lexer<JavaStringTokenId> createLexer(LexerRestartInfo<JavaStringTokenId> info) {
                return new JavaStringLexer<JavaStringTokenId>(info, true);
            }

            protected String mimeType() {
                return "text/x-java-string";
            }
        }.language();
    }

}

