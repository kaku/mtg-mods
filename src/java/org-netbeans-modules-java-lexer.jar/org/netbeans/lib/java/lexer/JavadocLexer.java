/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.spi.lexer.Lexer
 *  org.netbeans.spi.lexer.LexerInput
 *  org.netbeans.spi.lexer.LexerRestartInfo
 *  org.netbeans.spi.lexer.TokenFactory
 *  org.netbeans.spi.lexer.TokenPropertyProvider
 */
package org.netbeans.lib.java.lexer;

import org.netbeans.api.java.lexer.JavadocTokenId;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerInput;
import org.netbeans.spi.lexer.LexerRestartInfo;
import org.netbeans.spi.lexer.TokenFactory;
import org.netbeans.spi.lexer.TokenPropertyProvider;

public class JavadocLexer
implements Lexer<JavadocTokenId> {
    private static final int EOF = -1;
    private LexerInput input;
    private TokenFactory<JavadocTokenId> tokenFactory;
    private Integer state = null;

    public JavadocLexer(LexerRestartInfo<JavadocTokenId> info) {
        this.input = info.input();
        this.tokenFactory = info.tokenFactory();
        this.state = (Integer)info.state();
    }

    public Object state() {
        return this.state;
    }

    public Token<JavadocTokenId> nextToken() {
        int ch = this.input.read();
        if (ch == -1) {
            return null;
        }
        if (Character.isJavaIdentifierStart(ch)) {
            while (Character.isJavaIdentifierPart(this.input.read())) {
            }
            this.input.backup(1);
            if (this.state != null && this.state == 2) {
                this.state = 1;
                return this.token(JavadocTokenId.IDENT, "javadoc-identifier");
            }
            if (this.state == null) {
                this.state = 1;
            }
            return this.token(JavadocTokenId.IDENT);
        }
        switch (ch) {
            case 64: {
                if (this.state != null) {
                    return this.otherText(ch);
                }
                String tag = "";
                do {
                    if (!Character.isLetter(ch = this.input.read())) {
                        this.state = "param".equals(tag) ? 2 : ("code".equals(tag) || "literal".equals(tag) ? 3 : 1);
                        this.input.backup(1);
                        return this.tokenFactory.createToken((TokenId)JavadocTokenId.TAG, this.input.readLength());
                    }
                    tag = tag + new String(Character.toChars(ch));
                } while (true);
            }
            case 60: {
                if (this.state != null && this.state == 3) {
                    return this.otherText(ch);
                }
                int backupCounter = 0;
                boolean newline = false;
                boolean asterisk = false;
                do {
                    ch = this.input.read();
                    ++backupCounter;
                    if (ch == -1) {
                        this.state = null;
                        return this.token(JavadocTokenId.HTML_TAG);
                    }
                    if (ch == 62) {
                        if (this.state != null && this.state == 2) {
                            this.state = 1;
                            return this.token(JavadocTokenId.IDENT, "javadoc-identifier");
                        }
                        this.state = 1;
                        return this.token(JavadocTokenId.HTML_TAG);
                    }
                    if (ch == 60) {
                        this.state = 1;
                        this.input.backup(1);
                        return this.token(JavadocTokenId.HTML_TAG);
                    }
                    if (ch == 10) {
                        this.state = null;
                        backupCounter = 1;
                        newline = true;
                        asterisk = false;
                        continue;
                    }
                    if (newline && ch == 64) {
                        this.input.backup(backupCounter);
                        return this.token(JavadocTokenId.HTML_TAG);
                    }
                    if (newline && !asterisk && ch == 42) {
                        asterisk = true;
                        continue;
                    }
                    if (!newline || Character.isWhitespace(ch)) continue;
                    newline = false;
                } while (true);
            }
            case 46: {
                if (this.state == null) {
                    this.state = 1;
                }
                return this.token(JavadocTokenId.DOT);
            }
            case 35: {
                if (this.state == null) {
                    this.state = 1;
                }
                return this.token(JavadocTokenId.HASH);
            }
        }
        return this.otherText(ch);
    }

    private Token<JavadocTokenId> otherText(int ch) {
        boolean newline = this.state == null;
        boolean leftbr = false;
        do {
            if (Character.isJavaIdentifierStart(ch)) {
                if ((newline || leftbr) && this.state != null && this.state != 3) {
                    this.state = null;
                }
                this.input.backup(1);
                return this.token(JavadocTokenId.OTHER_TEXT);
            }
            switch (ch) {
                case 60: {
                    if (this.state != null && this.state == 3) {
                        leftbr = false;
                        newline = false;
                        break;
                    }
                }
                case 35: 
                case 46: {
                    this.input.backup(1);
                }
                case -1: {
                    return this.token(JavadocTokenId.OTHER_TEXT);
                }
                case 64: {
                    if ((newline || leftbr) && (this.state == null || this.state != 3)) {
                        this.state = null;
                        this.input.backup(1);
                        return this.token(JavadocTokenId.OTHER_TEXT);
                    }
                    leftbr = false;
                    newline = false;
                    break;
                }
                case 123: {
                    leftbr = true;
                    newline = false;
                    break;
                }
                case 10: {
                    newline = true;
                    break;
                }
                case 125: {
                    if (this.state != null && this.state == 3) {
                        this.state = 1;
                        if (this.input.readLength() > 1) {
                            this.input.backup(1);
                        }
                        return this.token(JavadocTokenId.OTHER_TEXT);
                    }
                    leftbr = false;
                    newline = false;
                    break;
                }
                case 42: {
                    if (newline) break;
                }
                default: {
                    if (Character.isWhitespace(ch)) break;
                    leftbr = false;
                    newline = false;
                }
            }
            ch = this.input.read();
        } while (true);
    }

    private Token<JavadocTokenId> token(JavadocTokenId id) {
        return this.tokenFactory.createToken((TokenId)id);
    }

    private Token<JavadocTokenId> token(JavadocTokenId id, final Object property) {
        return this.tokenFactory.createPropertyToken((TokenId)id, this.input.readLength(), (TokenPropertyProvider)new TokenPropertyProvider<JavadocTokenId>(){

            public Object getValue(Token<JavadocTokenId> token, Object key) {
                if (property.equals(key)) {
                    return true;
                }
                return null;
            }
        });
    }

    public void release() {
    }

}

