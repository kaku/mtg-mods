/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.spi.lexer.Lexer
 *  org.netbeans.spi.lexer.LexerInput
 *  org.netbeans.spi.lexer.LexerRestartInfo
 *  org.netbeans.spi.lexer.TokenFactory
 */
package org.netbeans.lib.java.lexer;

import org.netbeans.api.java.lexer.JavaStringTokenId;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.java.lexer.JavaCharacterTokenId;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerInput;
import org.netbeans.spi.lexer.LexerRestartInfo;
import org.netbeans.spi.lexer.TokenFactory;

public class JavaStringLexer<T extends TokenId>
implements Lexer<T> {
    private static final int EOF = -1;
    private LexerInput input;
    private TokenFactory<T> tokenFactory;
    private boolean isJavaStringTokenId;

    public JavaStringLexer(LexerRestartInfo<T> info, boolean isJavaStringTokenId) {
        this.input = info.input();
        this.tokenFactory = info.tokenFactory();
        this.isJavaStringTokenId = isJavaStringTokenId;
        assert (info.state() == null);
    }

    public Object state() {
        return null;
    }

    public Token<T> nextToken() {
        do {
            int ch = this.input.read();
            switch (ch) {
                case -1: {
                    if (this.input.readLength() > 0) {
                        return this.token((T)((Object)(this.isStringTokenId() ? JavaStringTokenId.TEXT : JavaCharacterTokenId.TEXT)));
                    }
                    return null;
                }
                case 92: {
                    if (this.input.readLength() > 1) {
                        this.input.backup(1);
                        return this.tokenFactory.createToken((TokenId)(this.isStringTokenId() ? JavaStringTokenId.TEXT : JavaCharacterTokenId.TEXT), this.input.readLength());
                    }
                    ch = this.input.read();
                    switch (ch) {
                        case 98: {
                            return this.token((T)((Object)(this.isStringTokenId() ? JavaStringTokenId.BACKSPACE : JavaCharacterTokenId.BACKSPACE)));
                        }
                        case 102: {
                            return this.token((T)((Object)(this.isStringTokenId() ? JavaStringTokenId.FORM_FEED : JavaCharacterTokenId.FORM_FEED)));
                        }
                        case 110: {
                            return this.token((T)((Object)(this.isStringTokenId() ? JavaStringTokenId.NEWLINE : JavaCharacterTokenId.NEWLINE)));
                        }
                        case 114: {
                            return this.token((T)((Object)(this.isStringTokenId() ? JavaStringTokenId.CR : JavaCharacterTokenId.CR)));
                        }
                        case 116: {
                            return this.token((T)((Object)(this.isStringTokenId() ? JavaStringTokenId.TAB : JavaCharacterTokenId.TAB)));
                        }
                        case 39: {
                            return this.token((T)((Object)(this.isStringTokenId() ? JavaStringTokenId.SINGLE_QUOTE : JavaCharacterTokenId.SINGLE_QUOTE)));
                        }
                        case 34: {
                            return this.token((T)((Object)(this.isStringTokenId() ? JavaStringTokenId.DOUBLE_QUOTE : JavaCharacterTokenId.DOUBLE_QUOTE)));
                        }
                        case 92: {
                            return this.token((T)((Object)(this.isStringTokenId() ? JavaStringTokenId.BACKSLASH : JavaCharacterTokenId.BACKSLASH)));
                        }
                        case 117: {
                            while (117 == (ch = this.input.read())) {
                            }
                            int i = 0;
                            do {
                                if (!((ch = Character.toLowerCase(ch)) >= 48 && ch <= 57 || ch >= 97 && ch <= 102)) {
                                    this.input.backup(1);
                                    return this.token((T)((Object)(this.isStringTokenId() ? JavaStringTokenId.UNICODE_ESCAPE_INVALID : JavaCharacterTokenId.UNICODE_ESCAPE_INVALID)));
                                }
                                if (i == 3) {
                                    return this.token((T)((Object)(this.isStringTokenId() ? JavaStringTokenId.UNICODE_ESCAPE : JavaCharacterTokenId.UNICODE_ESCAPE)));
                                }
                                ch = this.input.read();
                                ++i;
                            } while (true);
                        }
                        case 48: 
                        case 49: 
                        case 50: 
                        case 51: {
                            switch (this.input.read()) {
                                case 48: 
                                case 49: 
                                case 50: 
                                case 51: 
                                case 52: 
                                case 53: 
                                case 54: 
                                case 55: {
                                    switch (this.input.read()) {
                                        case 48: 
                                        case 49: 
                                        case 50: 
                                        case 51: 
                                        case 52: 
                                        case 53: 
                                        case 54: 
                                        case 55: {
                                            return this.token((T)((Object)(this.isStringTokenId() ? JavaStringTokenId.OCTAL_ESCAPE : JavaCharacterTokenId.OCTAL_ESCAPE)));
                                        }
                                    }
                                    this.input.backup(1);
                                    return this.token((T)((Object)(this.isStringTokenId() ? JavaStringTokenId.OCTAL_ESCAPE_INVALID : JavaCharacterTokenId.OCTAL_ESCAPE_INVALID)));
                                }
                            }
                            this.input.backup(1);
                            return this.token((T)((Object)(this.isStringTokenId() ? JavaStringTokenId.OCTAL_ESCAPE_INVALID : JavaCharacterTokenId.OCTAL_ESCAPE_INVALID)));
                        }
                    }
                    this.input.backup(1);
                    return this.token((T)((Object)(this.isStringTokenId() ? JavaStringTokenId.ESCAPE_SEQUENCE_INVALID : JavaCharacterTokenId.ESCAPE_SEQUENCE_INVALID)));
                }
            }
        } while (true);
    }

    private Token<T> token(T id) {
        return this.tokenFactory.createToken(id);
    }

    public void release() {
    }

    private boolean isStringTokenId() {
        return this.isJavaStringTokenId;
    }
}

