/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.PartType
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.spi.lexer.Lexer
 *  org.netbeans.spi.lexer.LexerInput
 *  org.netbeans.spi.lexer.LexerRestartInfo
 *  org.netbeans.spi.lexer.TokenFactory
 */
package org.netbeans.lib.java.lexer;

import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.lexer.PartType;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerInput;
import org.netbeans.spi.lexer.LexerRestartInfo;
import org.netbeans.spi.lexer.TokenFactory;

public class JavaLexer
implements Lexer<JavaTokenId> {
    private static final int EOF = -1;
    private final LexerInput input;
    private final TokenFactory<JavaTokenId> tokenFactory;
    private final int version;
    int previousLength = -1;
    int currentLength = -1;

    public JavaLexer(LexerRestartInfo<JavaTokenId> info) {
        this.input = info.input();
        this.tokenFactory = info.tokenFactory();
        assert (info.state() == null);
        Integer ver = (Integer)info.getAttributeValue((Object)"version");
        this.version = ver != null ? ver : 8;
    }

    public Object state() {
        return null;
    }

    public int nextChar() {
        int first;
        this.previousLength = this.currentLength;
        int backupReadLength = this.input.readLength();
        int c = this.input.read();
        if (c != 92) {
            this.currentLength = 1;
            return c;
        }
        boolean wasU = false;
        while ((first = this.input.read()) == 117) {
            wasU = true;
        }
        if (!wasU) {
            this.input.backup(this.input.readLengthEOF() - backupReadLength);
            this.currentLength = 1;
            return this.input.read();
        }
        int second = this.input.read();
        int third = this.input.read();
        int fourth = this.input.read();
        if (fourth == -1) {
            this.input.backup(this.input.readLengthEOF() - backupReadLength);
            this.currentLength = 1;
            return this.input.read();
        }
        first = Character.digit(first, 16);
        second = Character.digit(second, 16);
        third = Character.digit(third, 16);
        fourth = Character.digit(fourth, 16);
        if (first == -1 || second == -1 || third == -1 || fourth == -1) {
            this.input.backup(this.input.readLengthEOF() - backupReadLength);
            this.currentLength = 1;
            return this.input.read();
        }
        this.currentLength = this.input.readLength() - backupReadLength;
        return ((first * 16 + second) * 16 + third) * 16 + fourth;
    }

    public void backup(int howMany) {
        switch (howMany) {
            case 1: {
                assert (this.currentLength != -1);
                this.input.backup(this.currentLength);
                this.currentLength = this.previousLength;
                this.previousLength = -1;
                break;
            }
            case 2: {
                assert (this.currentLength != -1 && this.previousLength != -1);
                this.input.backup(this.currentLength + this.previousLength);
                this.previousLength = -1;
                this.currentLength = -1;
                break;
            }
            default: {
                assert (false);
                break;
            }
        }
    }

    public void consumeNewline() {
        if (this.nextChar() != 10) {
            this.backup(1);
        }
    }

    public Token<JavaTokenId> nextToken() {
        int c = this.nextChar();
        JavaTokenId lookupId = null;
        switch (c) {
            case 35: {
                return this.token(JavaTokenId.ERROR);
            }
            case 34: {
                if (lookupId == null) {
                    lookupId = JavaTokenId.STRING_LITERAL;
                }
                do {
                    switch (this.nextChar()) {
                        case 34: {
                            return this.token(lookupId);
                        }
                        case 92: {
                            this.nextChar();
                            break;
                        }
                        case 13: {
                            this.consumeNewline();
                        }
                        case -1: 
                        case 10: {
                            return this.tokenFactory.createToken((TokenId)lookupId, this.input.readLength(), PartType.START);
                        }
                    }
                } while (true);
            }
            case 39: {
                do {
                    switch (this.nextChar()) {
                        case 39: {
                            return this.token(JavaTokenId.CHAR_LITERAL);
                        }
                        case 92: {
                            this.nextChar();
                            break;
                        }
                        case 13: {
                            this.consumeNewline();
                        }
                        case -1: 
                        case 10: {
                            return this.tokenFactory.createToken((TokenId)JavaTokenId.CHAR_LITERAL, this.input.readLength(), PartType.START);
                        }
                    }
                } while (true);
            }
            case 47: {
                switch (this.nextChar()) {
                    case 47: {
                        do {
                            switch (this.nextChar()) {
                                case 13: {
                                    this.consumeNewline();
                                }
                                case -1: 
                                case 10: {
                                    return this.token(JavaTokenId.LINE_COMMENT);
                                }
                            }
                        } while (true);
                    }
                    case 61: {
                        return this.token(JavaTokenId.SLASHEQ);
                    }
                    case 42: {
                        c = this.nextChar();
                        if (c == 42) {
                            c = this.nextChar();
                            if (c == 47) {
                                return this.token(JavaTokenId.BLOCK_COMMENT);
                            }
                            do {
                                if (c == 42) {
                                    c = this.nextChar();
                                    if (c == 47) {
                                        return this.token(JavaTokenId.JAVADOC_COMMENT);
                                    }
                                    if (c != -1) continue;
                                    return this.tokenFactory.createToken((TokenId)JavaTokenId.JAVADOC_COMMENT, this.input.readLength(), PartType.START);
                                }
                                if (c == -1) {
                                    return this.tokenFactory.createToken((TokenId)JavaTokenId.JAVADOC_COMMENT, this.input.readLength(), PartType.START);
                                }
                                c = this.nextChar();
                            } while (true);
                        }
                        do {
                            c = this.nextChar();
                            while (c == 42) {
                                c = this.nextChar();
                                if (c == 47) {
                                    return this.token(JavaTokenId.BLOCK_COMMENT);
                                }
                                if (c != -1) continue;
                                return this.tokenFactory.createToken((TokenId)JavaTokenId.BLOCK_COMMENT, this.input.readLength(), PartType.START);
                            }
                        } while (c != -1);
                        return this.tokenFactory.createToken((TokenId)JavaTokenId.BLOCK_COMMENT, this.input.readLength(), PartType.START);
                    }
                }
                this.backup(1);
                return this.token(JavaTokenId.SLASH);
            }
            case 61: {
                if (this.nextChar() == 61) {
                    return this.token(JavaTokenId.EQEQ);
                }
                this.backup(1);
                return this.token(JavaTokenId.EQ);
            }
            case 62: {
                switch (this.nextChar()) {
                    case 62: {
                        c = this.nextChar();
                        switch (c) {
                            case 62: {
                                if (this.nextChar() == 61) {
                                    return this.token(JavaTokenId.GTGTGTEQ);
                                }
                                this.backup(1);
                                return this.token(JavaTokenId.GTGTGT);
                            }
                            case 61: {
                                return this.token(JavaTokenId.GTGTEQ);
                            }
                        }
                        this.backup(1);
                        return this.token(JavaTokenId.GTGT);
                    }
                    case 61: {
                        return this.token(JavaTokenId.GTEQ);
                    }
                }
                this.backup(1);
                return this.token(JavaTokenId.GT);
            }
            case 60: {
                switch (this.nextChar()) {
                    case 60: {
                        if (this.nextChar() == 61) {
                            return this.token(JavaTokenId.LTLTEQ);
                        }
                        this.backup(1);
                        return this.token(JavaTokenId.LTLT);
                    }
                    case 61: {
                        return this.token(JavaTokenId.LTEQ);
                    }
                }
                this.backup(1);
                return this.token(JavaTokenId.LT);
            }
            case 43: {
                switch (this.nextChar()) {
                    case 43: {
                        return this.token(JavaTokenId.PLUSPLUS);
                    }
                    case 61: {
                        return this.token(JavaTokenId.PLUSEQ);
                    }
                }
                this.backup(1);
                return this.token(JavaTokenId.PLUS);
            }
            case 45: {
                switch (this.nextChar()) {
                    case 45: {
                        return this.token(JavaTokenId.MINUSMINUS);
                    }
                    case 61: {
                        return this.token(JavaTokenId.MINUSEQ);
                    }
                    case 62: {
                        return this.token(JavaTokenId.ARROW);
                    }
                }
                this.backup(1);
                return this.token(JavaTokenId.MINUS);
            }
            case 42: {
                switch (this.nextChar()) {
                    case 47: {
                        return this.token(JavaTokenId.INVALID_COMMENT_END);
                    }
                    case 61: {
                        return this.token(JavaTokenId.STAREQ);
                    }
                }
                this.backup(1);
                return this.token(JavaTokenId.STAR);
            }
            case 124: {
                switch (this.nextChar()) {
                    case 124: {
                        return this.token(JavaTokenId.BARBAR);
                    }
                    case 61: {
                        return this.token(JavaTokenId.BAREQ);
                    }
                }
                this.backup(1);
                return this.token(JavaTokenId.BAR);
            }
            case 38: {
                switch (this.nextChar()) {
                    case 38: {
                        return this.token(JavaTokenId.AMPAMP);
                    }
                    case 61: {
                        return this.token(JavaTokenId.AMPEQ);
                    }
                }
                this.backup(1);
                return this.token(JavaTokenId.AMP);
            }
            case 37: {
                if (this.nextChar() == 61) {
                    return this.token(JavaTokenId.PERCENTEQ);
                }
                this.backup(1);
                return this.token(JavaTokenId.PERCENT);
            }
            case 94: {
                if (this.nextChar() == 61) {
                    return this.token(JavaTokenId.CARETEQ);
                }
                this.backup(1);
                return this.token(JavaTokenId.CARET);
            }
            case 33: {
                if (this.nextChar() == 61) {
                    return this.token(JavaTokenId.BANGEQ);
                }
                this.backup(1);
                return this.token(JavaTokenId.BANG);
            }
            case 46: {
                c = this.nextChar();
                if (c == 46) {
                    if (this.nextChar() == 46) {
                        return this.token(JavaTokenId.ELLIPSIS);
                    }
                    this.backup(2);
                } else {
                    if (48 <= c && c <= 57) {
                        return this.finishNumberLiteral(this.nextChar(), true);
                    }
                    this.backup(1);
                }
                return this.token(JavaTokenId.DOT);
            }
            case 126: {
                return this.token(JavaTokenId.TILDE);
            }
            case 44: {
                return this.token(JavaTokenId.COMMA);
            }
            case 59: {
                return this.token(JavaTokenId.SEMICOLON);
            }
            case 58: {
                if (this.nextChar() == 58) {
                    return this.token(JavaTokenId.COLONCOLON);
                }
                this.backup(1);
                return this.token(JavaTokenId.COLON);
            }
            case 63: {
                return this.token(JavaTokenId.QUESTION);
            }
            case 40: {
                return this.token(JavaTokenId.LPAREN);
            }
            case 41: {
                return this.token(JavaTokenId.RPAREN);
            }
            case 91: {
                return this.token(JavaTokenId.LBRACKET);
            }
            case 93: {
                return this.token(JavaTokenId.RBRACKET);
            }
            case 123: {
                return this.token(JavaTokenId.LBRACE);
            }
            case 125: {
                return this.token(JavaTokenId.RBRACE);
            }
            case 64: {
                return this.token(JavaTokenId.AT);
            }
            case 48: {
                c = this.nextChar();
                if (c == 120 || c == 88) {
                    boolean inFraction = false;
                    boolean afterDigit = false;
                    block229 : do {
                        switch (this.nextChar()) {
                            case 48: 
                            case 49: 
                            case 50: 
                            case 51: 
                            case 52: 
                            case 53: 
                            case 54: 
                            case 55: 
                            case 56: 
                            case 57: 
                            case 65: 
                            case 66: 
                            case 67: 
                            case 68: 
                            case 69: 
                            case 70: 
                            case 97: 
                            case 98: 
                            case 99: 
                            case 100: 
                            case 101: 
                            case 102: {
                                afterDigit = true;
                                continue block229;
                            }
                            case 46: {
                                if (!inFraction) {
                                    inFraction = true;
                                    afterDigit = false;
                                    continue block229;
                                }
                                return this.token(JavaTokenId.FLOAT_LITERAL_INVALID);
                            }
                            case 80: 
                            case 112: {
                                return this.finishFloatExponent();
                            }
                            case 76: 
                            case 108: {
                                return this.token(JavaTokenId.LONG_LITERAL);
                            }
                            case 95: {
                                if (this.version < 7 || !afterDigit) break block229;
                                int cc = this.nextChar();
                                this.backup(1);
                                if (!(cc >= 48 && cc <= 57 || cc >= 97 && cc <= 102 || cc >= 65 && cc <= 70) && cc != 95) break block229;
                                continue block229;
                            }
                        }
                        break;
                    } while (true);
                    this.backup(1);
                    return this.token(inFraction ? JavaTokenId.FLOAT_LITERAL_INVALID : JavaTokenId.INT_LITERAL);
                }
                if (this.version >= 7 && (c == 98 || c == 66)) {
                    boolean afterDigit = false;
                    block230 : do {
                        switch (this.nextChar()) {
                            case 48: 
                            case 49: {
                                afterDigit = true;
                                continue block230;
                            }
                            case 76: 
                            case 108: {
                                return this.token(JavaTokenId.LONG_LITERAL);
                            }
                            case 95: {
                                if (!afterDigit) break block230;
                                int cc = this.nextChar();
                                this.backup(1);
                                if (cc != 48 && cc != 49 && cc != 95) break block230;
                                continue block230;
                            }
                        }
                        break;
                    } while (true);
                    this.backup(1);
                    return this.token(JavaTokenId.INT_LITERAL);
                }
                return this.finishNumberLiteral(c, false);
            }
            case 49: 
            case 50: 
            case 51: 
            case 52: 
            case 53: 
            case 54: 
            case 55: 
            case 56: 
            case 57: {
                return this.finishNumberLiteral(this.nextChar(), false);
            }
            case 97: {
                c = this.nextChar();
                switch (c) {
                    case 98: {
                        c = this.nextChar();
                        if (c != 115 || (c = this.nextChar()) != 116 || (c = this.nextChar()) != 114 || (c = this.nextChar()) != 97 || (c = this.nextChar()) != 99 || (c = this.nextChar()) != 116) break;
                        return this.keywordOrIdentifier(JavaTokenId.ABSTRACT);
                    }
                    case 115: {
                        c = this.nextChar();
                        if (c != 115 || (c = this.nextChar()) != 101 || (c = this.nextChar()) != 114 || (c = this.nextChar()) != 116) break;
                        return this.version >= 4 ? this.keywordOrIdentifier(JavaTokenId.ASSERT) : this.finishIdentifier();
                    }
                }
                return this.finishIdentifier(c);
            }
            case 98: {
                c = this.nextChar();
                switch (c) {
                    case 111: {
                        c = this.nextChar();
                        if (c != 111 || (c = this.nextChar()) != 108 || (c = this.nextChar()) != 101 || (c = this.nextChar()) != 97 || (c = this.nextChar()) != 110) break;
                        return this.keywordOrIdentifier(JavaTokenId.BOOLEAN);
                    }
                    case 114: {
                        c = this.nextChar();
                        if (c != 101 || (c = this.nextChar()) != 97 || (c = this.nextChar()) != 107) break;
                        return this.keywordOrIdentifier(JavaTokenId.BREAK);
                    }
                    case 121: {
                        c = this.nextChar();
                        if (c != 116 || (c = this.nextChar()) != 101) break;
                        return this.keywordOrIdentifier(JavaTokenId.BYTE);
                    }
                }
                return this.finishIdentifier(c);
            }
            case 99: {
                c = this.nextChar();
                block127 : switch (c) {
                    case 97: {
                        c = this.nextChar();
                        switch (c) {
                            case 115: {
                                c = this.nextChar();
                                if (c != 101) break;
                                return this.keywordOrIdentifier(JavaTokenId.CASE);
                            }
                            case 116: {
                                c = this.nextChar();
                                if (c != 99 || (c = this.nextChar()) != 104) break;
                                return this.keywordOrIdentifier(JavaTokenId.CATCH);
                            }
                        }
                        break;
                    }
                    case 104: {
                        c = this.nextChar();
                        if (c != 97 || (c = this.nextChar()) != 114) break;
                        return this.keywordOrIdentifier(JavaTokenId.CHAR);
                    }
                    case 108: {
                        c = this.nextChar();
                        if (c != 97 || (c = this.nextChar()) != 115 || (c = this.nextChar()) != 115) break;
                        return this.keywordOrIdentifier(JavaTokenId.CLASS);
                    }
                    case 111: {
                        c = this.nextChar();
                        if (c != 110) break;
                        c = this.nextChar();
                        switch (c) {
                            case 115: {
                                c = this.nextChar();
                                if (c != 116) break block127;
                                return this.keywordOrIdentifier(JavaTokenId.CONST);
                            }
                            case 116: {
                                c = this.nextChar();
                                if (c != 105 || (c = this.nextChar()) != 110 || (c = this.nextChar()) != 117 || (c = this.nextChar()) != 101) break block127;
                                return this.keywordOrIdentifier(JavaTokenId.CONTINUE);
                            }
                        }
                    }
                }
                return this.finishIdentifier(c);
            }
            case 100: {
                c = this.nextChar();
                block141 : switch (c) {
                    case 101: {
                        c = this.nextChar();
                        if (c != 102 || (c = this.nextChar()) != 97 || (c = this.nextChar()) != 117 || (c = this.nextChar()) != 108 || (c = this.nextChar()) != 116) break;
                        return this.keywordOrIdentifier(JavaTokenId.DEFAULT);
                    }
                    case 111: {
                        c = this.nextChar();
                        switch (c) {
                            case 117: {
                                c = this.nextChar();
                                if (c != 98 || (c = this.nextChar()) != 108 || (c = this.nextChar()) != 101) break block141;
                                return this.keywordOrIdentifier(JavaTokenId.DOUBLE);
                            }
                        }
                        return this.keywordOrIdentifier(JavaTokenId.DO, c);
                    }
                }
                return this.finishIdentifier(c);
            }
            case 101: {
                c = this.nextChar();
                switch (c) {
                    case 108: {
                        c = this.nextChar();
                        if (c != 115 || (c = this.nextChar()) != 101) break;
                        return this.keywordOrIdentifier(JavaTokenId.ELSE);
                    }
                    case 110: {
                        c = this.nextChar();
                        if (c != 117 || (c = this.nextChar()) != 109) break;
                        return this.version >= 5 ? this.keywordOrIdentifier(JavaTokenId.ENUM) : this.finishIdentifier();
                    }
                    case 120: {
                        c = this.nextChar();
                        if (c != 116 || (c = this.nextChar()) != 101 || (c = this.nextChar()) != 110 || (c = this.nextChar()) != 100 || (c = this.nextChar()) != 115) break;
                        return this.keywordOrIdentifier(JavaTokenId.EXTENDS);
                    }
                }
                return this.finishIdentifier(c);
            }
            case 102: {
                c = this.nextChar();
                block153 : switch (c) {
                    case 97: {
                        c = this.nextChar();
                        if (c != 108 || (c = this.nextChar()) != 115 || (c = this.nextChar()) != 101) break;
                        return this.keywordOrIdentifier(JavaTokenId.FALSE);
                    }
                    case 105: {
                        c = this.nextChar();
                        if (c != 110 || (c = this.nextChar()) != 97 || (c = this.nextChar()) != 108) break;
                        c = this.nextChar();
                        switch (c) {
                            case 108: {
                                c = this.nextChar();
                                if (c != 121) break block153;
                                return this.keywordOrIdentifier(JavaTokenId.FINALLY);
                            }
                        }
                        return this.keywordOrIdentifier(JavaTokenId.FINAL, c);
                    }
                    case 108: {
                        c = this.nextChar();
                        if (c != 111 || (c = this.nextChar()) != 97 || (c = this.nextChar()) != 116) break;
                        return this.keywordOrIdentifier(JavaTokenId.FLOAT);
                    }
                    case 111: {
                        c = this.nextChar();
                        if (c != 114) break;
                        return this.keywordOrIdentifier(JavaTokenId.FOR);
                    }
                }
                return this.finishIdentifier(c);
            }
            case 103: {
                c = this.nextChar();
                if (c == 111 && (c = this.nextChar()) == 116 && (c = this.nextChar()) == 111) {
                    return this.keywordOrIdentifier(JavaTokenId.GOTO);
                }
                return this.finishIdentifier(c);
            }
            case 105: {
                c = this.nextChar();
                block162 : switch (c) {
                    case 102: {
                        return this.keywordOrIdentifier(JavaTokenId.IF);
                    }
                    case 109: {
                        c = this.nextChar();
                        if (c != 112) break;
                        c = this.nextChar();
                        switch (c) {
                            case 108: {
                                c = this.nextChar();
                                if (c != 101 || (c = this.nextChar()) != 109 || (c = this.nextChar()) != 101 || (c = this.nextChar()) != 110 || (c = this.nextChar()) != 116 || (c = this.nextChar()) != 115) break;
                                return this.keywordOrIdentifier(JavaTokenId.IMPLEMENTS);
                            }
                            case 111: {
                                c = this.nextChar();
                                if (c != 114 || (c = this.nextChar()) != 116) break;
                                return this.keywordOrIdentifier(JavaTokenId.IMPORT);
                            }
                        }
                        break;
                    }
                    case 110: {
                        c = this.nextChar();
                        switch (c) {
                            case 115: {
                                c = this.nextChar();
                                if (c != 116 || (c = this.nextChar()) != 97 || (c = this.nextChar()) != 110 || (c = this.nextChar()) != 99 || (c = this.nextChar()) != 101 || (c = this.nextChar()) != 111 || (c = this.nextChar()) != 102) break block162;
                                return this.keywordOrIdentifier(JavaTokenId.INSTANCEOF);
                            }
                            case 116: {
                                c = this.nextChar();
                                switch (c) {
                                    case 101: {
                                        c = this.nextChar();
                                        if (c != 114 || (c = this.nextChar()) != 102 || (c = this.nextChar()) != 97 || (c = this.nextChar()) != 99 || (c = this.nextChar()) != 101) break block162;
                                        return this.keywordOrIdentifier(JavaTokenId.INTERFACE);
                                    }
                                }
                                return this.keywordOrIdentifier(JavaTokenId.INT, c);
                            }
                        }
                    }
                }
                return this.finishIdentifier(c);
            }
            case 108: {
                c = this.nextChar();
                if (c == 111 && (c = this.nextChar()) == 110 && (c = this.nextChar()) == 103) {
                    return this.keywordOrIdentifier(JavaTokenId.LONG);
                }
                return this.finishIdentifier(c);
            }
            case 110: {
                c = this.nextChar();
                switch (c) {
                    case 97: {
                        c = this.nextChar();
                        if (c != 116 || (c = this.nextChar()) != 105 || (c = this.nextChar()) != 118 || (c = this.nextChar()) != 101) break;
                        return this.keywordOrIdentifier(JavaTokenId.NATIVE);
                    }
                    case 101: {
                        c = this.nextChar();
                        if (c != 119) break;
                        return this.keywordOrIdentifier(JavaTokenId.NEW);
                    }
                    case 117: {
                        c = this.nextChar();
                        if (c != 108 || (c = this.nextChar()) != 108) break;
                        return this.keywordOrIdentifier(JavaTokenId.NULL);
                    }
                }
                return this.finishIdentifier(c);
            }
            case 112: {
                c = this.nextChar();
                switch (c) {
                    case 97: {
                        c = this.nextChar();
                        if (c != 99 || (c = this.nextChar()) != 107 || (c = this.nextChar()) != 97 || (c = this.nextChar()) != 103 || (c = this.nextChar()) != 101) break;
                        return this.keywordOrIdentifier(JavaTokenId.PACKAGE);
                    }
                    case 114: {
                        c = this.nextChar();
                        switch (c) {
                            case 105: {
                                c = this.nextChar();
                                if (c != 118 || (c = this.nextChar()) != 97 || (c = this.nextChar()) != 116 || (c = this.nextChar()) != 101) break;
                                return this.keywordOrIdentifier(JavaTokenId.PRIVATE);
                            }
                            case 111: {
                                c = this.nextChar();
                                if (c != 116 || (c = this.nextChar()) != 101 || (c = this.nextChar()) != 99 || (c = this.nextChar()) != 116 || (c = this.nextChar()) != 101 || (c = this.nextChar()) != 100) break;
                                return this.keywordOrIdentifier(JavaTokenId.PROTECTED);
                            }
                        }
                        break;
                    }
                    case 117: {
                        c = this.nextChar();
                        if (c != 98 || (c = this.nextChar()) != 108 || (c = this.nextChar()) != 105 || (c = this.nextChar()) != 99) break;
                        return this.keywordOrIdentifier(JavaTokenId.PUBLIC);
                    }
                }
                return this.finishIdentifier(c);
            }
            case 114: {
                c = this.nextChar();
                if (c == 101 && (c = this.nextChar()) == 116 && (c = this.nextChar()) == 117 && (c = this.nextChar()) == 114 && (c = this.nextChar()) == 110) {
                    return this.keywordOrIdentifier(JavaTokenId.RETURN);
                }
                return this.finishIdentifier(c);
            }
            case 115: {
                c = this.nextChar();
                switch (c) {
                    case 104: {
                        c = this.nextChar();
                        if (c != 111 || (c = this.nextChar()) != 114 || (c = this.nextChar()) != 116) break;
                        return this.keywordOrIdentifier(JavaTokenId.SHORT);
                    }
                    case 116: {
                        c = this.nextChar();
                        switch (c) {
                            case 97: {
                                c = this.nextChar();
                                if (c != 116 || (c = this.nextChar()) != 105 || (c = this.nextChar()) != 99) break;
                                return this.keywordOrIdentifier(JavaTokenId.STATIC);
                            }
                            case 114: {
                                c = this.nextChar();
                                if (c != 105 || (c = this.nextChar()) != 99 || (c = this.nextChar()) != 116 || (c = this.nextChar()) != 102 || (c = this.nextChar()) != 112) break;
                                return this.keywordOrIdentifier(JavaTokenId.STRICTFP);
                            }
                        }
                        break;
                    }
                    case 117: {
                        c = this.nextChar();
                        if (c != 112 || (c = this.nextChar()) != 101 || (c = this.nextChar()) != 114) break;
                        return this.keywordOrIdentifier(JavaTokenId.SUPER);
                    }
                    case 119: {
                        c = this.nextChar();
                        if (c != 105 || (c = this.nextChar()) != 116 || (c = this.nextChar()) != 99 || (c = this.nextChar()) != 104) break;
                        return this.keywordOrIdentifier(JavaTokenId.SWITCH);
                    }
                    case 121: {
                        c = this.nextChar();
                        if (c != 110 || (c = this.nextChar()) != 99 || (c = this.nextChar()) != 104 || (c = this.nextChar()) != 114 || (c = this.nextChar()) != 111 || (c = this.nextChar()) != 110 || (c = this.nextChar()) != 105 || (c = this.nextChar()) != 122 || (c = this.nextChar()) != 101 || (c = this.nextChar()) != 100) break;
                        return this.keywordOrIdentifier(JavaTokenId.SYNCHRONIZED);
                    }
                }
                return this.finishIdentifier(c);
            }
            case 116: {
                c = this.nextChar();
                block203 : switch (c) {
                    case 104: {
                        c = this.nextChar();
                        switch (c) {
                            case 105: {
                                c = this.nextChar();
                                if (c != 115) break;
                                return this.keywordOrIdentifier(JavaTokenId.THIS);
                            }
                            case 114: {
                                c = this.nextChar();
                                if (c != 111 || (c = this.nextChar()) != 119) break;
                                c = this.nextChar();
                                switch (c) {
                                    case 115: {
                                        return this.keywordOrIdentifier(JavaTokenId.THROWS);
                                    }
                                }
                                return this.keywordOrIdentifier(JavaTokenId.THROW, c);
                            }
                        }
                        break;
                    }
                    case 114: {
                        c = this.nextChar();
                        switch (c) {
                            case 97: {
                                c = this.nextChar();
                                if (c != 110 || (c = this.nextChar()) != 115 || (c = this.nextChar()) != 105 || (c = this.nextChar()) != 101 || (c = this.nextChar()) != 110 || (c = this.nextChar()) != 116) break block203;
                                return this.keywordOrIdentifier(JavaTokenId.TRANSIENT);
                            }
                            case 117: {
                                c = this.nextChar();
                                if (c != 101) break block203;
                                return this.keywordOrIdentifier(JavaTokenId.TRUE);
                            }
                            case 121: {
                                return this.keywordOrIdentifier(JavaTokenId.TRY);
                            }
                        }
                    }
                }
                return this.finishIdentifier(c);
            }
            case 118: {
                c = this.nextChar();
                if (c == 111) {
                    c = this.nextChar();
                    switch (c) {
                        case 105: {
                            c = this.nextChar();
                            if (c != 100) break;
                            return this.keywordOrIdentifier(JavaTokenId.VOID);
                        }
                        case 108: {
                            c = this.nextChar();
                            if (c != 97 || (c = this.nextChar()) != 116 || (c = this.nextChar()) != 105 || (c = this.nextChar()) != 108 || (c = this.nextChar()) != 101) break;
                            return this.keywordOrIdentifier(JavaTokenId.VOLATILE);
                        }
                    }
                }
                return this.finishIdentifier(c);
            }
            case 119: {
                c = this.nextChar();
                if (c == 104 && (c = this.nextChar()) == 105 && (c = this.nextChar()) == 108 && (c = this.nextChar()) == 101) {
                    return this.keywordOrIdentifier(JavaTokenId.WHILE);
                }
                return this.finishIdentifier(c);
            }
            case 36: 
            case 65: 
            case 66: 
            case 67: 
            case 68: 
            case 69: 
            case 70: 
            case 71: 
            case 72: 
            case 73: 
            case 74: 
            case 75: 
            case 76: 
            case 77: 
            case 78: 
            case 79: 
            case 80: 
            case 81: 
            case 82: 
            case 83: 
            case 84: 
            case 85: 
            case 86: 
            case 87: 
            case 88: 
            case 89: 
            case 90: 
            case 95: 
            case 104: 
            case 106: 
            case 107: 
            case 109: 
            case 111: 
            case 113: 
            case 117: 
            case 120: 
            case 121: 
            case 122: {
                return this.finishIdentifier();
            }
            case 9: 
            case 10: 
            case 11: 
            case 12: 
            case 13: 
            case 28: 
            case 29: 
            case 30: 
            case 31: {
                return this.finishWhitespace();
            }
            case 32: {
                c = this.nextChar();
                if (c == -1 || !Character.isWhitespace(c)) {
                    this.backup(1);
                    return this.input.readLength() == 1 ? this.tokenFactory.getFlyweightToken((TokenId)JavaTokenId.WHITESPACE, " ") : this.tokenFactory.createToken((TokenId)JavaTokenId.WHITESPACE);
                }
                return this.finishWhitespace();
            }
            case -1: {
                return null;
            }
        }
        if (c >= 128) {
            if (Character.isJavaIdentifierStart(c = this.translateSurrogates(c))) {
                return this.finishIdentifier();
            }
            if (Character.isWhitespace(c)) {
                return this.finishWhitespace();
            }
        }
        return this.token(JavaTokenId.ERROR);
    }

    private int translateSurrogates(int c) {
        if (Character.isHighSurrogate((char)c)) {
            int lowSurr = this.nextChar();
            if (lowSurr != -1 && Character.isLowSurrogate((char)lowSurr)) {
                c = Character.toCodePoint((char)c, (char)lowSurr);
            } else {
                this.backup(1);
            }
        }
        return c;
    }

    private Token<JavaTokenId> finishWhitespace() {
        int c;
        while ((c = this.nextChar()) != -1 && Character.isWhitespace(c)) {
        }
        this.backup(1);
        return this.tokenFactory.createToken((TokenId)JavaTokenId.WHITESPACE);
    }

    private Token<JavaTokenId> finishIdentifier() {
        return this.finishIdentifier(this.nextChar());
    }

    private Token<JavaTokenId> finishIdentifier(int c) {
        do {
            if (c == -1 || !Character.isJavaIdentifierPart(c = this.translateSurrogates(c))) {
                this.backup(c >= 65536 ? 2 : 1);
                return this.tokenFactory.createToken((TokenId)JavaTokenId.IDENTIFIER);
            }
            c = this.nextChar();
        } while (true);
    }

    private Token<JavaTokenId> keywordOrIdentifier(JavaTokenId keywordId) {
        return this.keywordOrIdentifier(keywordId, this.nextChar());
    }

    private Token<JavaTokenId> keywordOrIdentifier(JavaTokenId keywordId, int c) {
        if (c == -1 || !Character.isJavaIdentifierPart(c = this.translateSurrogates(c))) {
            this.backup(c >= 65536 ? 2 : 1);
            return this.token(keywordId);
        }
        return this.finishIdentifier();
    }

    private Token<JavaTokenId> finishNumberLiteral(int c, boolean inFraction) {
        boolean afterDigit = true;
        do {
            switch (c) {
                case 46: {
                    if (!inFraction) {
                        inFraction = true;
                        afterDigit = false;
                        break;
                    }
                    return this.token(JavaTokenId.FLOAT_LITERAL_INVALID);
                }
                case 76: 
                case 108: {
                    return this.token(JavaTokenId.LONG_LITERAL);
                }
                case 68: 
                case 100: {
                    return this.token(JavaTokenId.DOUBLE_LITERAL);
                }
                case 70: 
                case 102: {
                    return this.token(JavaTokenId.FLOAT_LITERAL);
                }
                case 48: 
                case 49: 
                case 50: 
                case 51: 
                case 52: 
                case 53: 
                case 54: 
                case 55: 
                case 56: 
                case 57: {
                    afterDigit = true;
                    break;
                }
                case 69: 
                case 101: {
                    return this.finishFloatExponent();
                }
                case 95: {
                    if (this.version >= 7 && afterDigit) {
                        int cc = this.nextChar();
                        this.backup(1);
                        if (cc >= 48 && cc <= 57 || cc == 95) break;
                    }
                }
                default: {
                    this.backup(1);
                    return this.token(inFraction ? JavaTokenId.DOUBLE_LITERAL : JavaTokenId.INT_LITERAL);
                }
            }
            c = this.nextChar();
        } while (true);
    }

    private Token<JavaTokenId> finishFloatExponent() {
        int c = this.nextChar();
        if (c == 43 || c == 45) {
            c = this.nextChar();
        }
        if (c < 48 || 57 < c) {
            return this.token(JavaTokenId.FLOAT_LITERAL_INVALID);
        }
        while (48 <= (c = this.nextChar()) && c <= 57) {
        }
        switch (c) {
            case 68: 
            case 100: {
                return this.token(JavaTokenId.DOUBLE_LITERAL);
            }
            case 70: 
            case 102: {
                return this.token(JavaTokenId.FLOAT_LITERAL);
            }
        }
        this.backup(1);
        return this.token(JavaTokenId.DOUBLE_LITERAL);
    }

    private Token<JavaTokenId> token(JavaTokenId id) {
        String fixedText = id.fixedText();
        return fixedText != null && fixedText.length() == this.input.readLength() ? this.tokenFactory.getFlyweightToken((TokenId)id, fixedText) : this.tokenFactory.createToken((TokenId)id);
    }

    public void release() {
    }
}

