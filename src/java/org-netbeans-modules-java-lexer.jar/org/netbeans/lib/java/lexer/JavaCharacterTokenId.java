/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.spi.lexer.LanguageHierarchy
 *  org.netbeans.spi.lexer.Lexer
 *  org.netbeans.spi.lexer.LexerRestartInfo
 */
package org.netbeans.lib.java.lexer;

import java.util.Collection;
import java.util.EnumSet;
import java.util.Map;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.java.lexer.JavaStringLexer;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;

public enum JavaCharacterTokenId implements TokenId
{
    TEXT("character"),
    BACKSPACE("character-escape"),
    FORM_FEED("character-escape"),
    NEWLINE("character-escape"),
    CR("character-escape"),
    TAB("character-escape"),
    SINGLE_QUOTE("character-escape"),
    DOUBLE_QUOTE("character-escape"),
    BACKSLASH("character-escape"),
    OCTAL_ESCAPE("character-escape"),
    OCTAL_ESCAPE_INVALID("character-escape-invalid"),
    UNICODE_ESCAPE("character-escape"),
    UNICODE_ESCAPE_INVALID("character-escape-invalid"),
    ESCAPE_SEQUENCE_INVALID("character-escape-invalid");
    
    private final String primaryCategory;
    private static final Language<JavaCharacterTokenId> language;

    private JavaCharacterTokenId() {
        this(null);
    }

    private JavaCharacterTokenId(String primaryCategory) {
        this.primaryCategory = primaryCategory;
    }

    public String primaryCategory() {
        return this.primaryCategory;
    }

    public static Language<JavaCharacterTokenId> language() {
        return language;
    }

    static {
        language = new LanguageHierarchy<JavaCharacterTokenId>(){

            protected Collection<JavaCharacterTokenId> createTokenIds() {
                return EnumSet.allOf(JavaCharacterTokenId.class);
            }

            protected Map<String, Collection<JavaCharacterTokenId>> createTokenCategories() {
                return null;
            }

            protected Lexer<JavaCharacterTokenId> createLexer(LexerRestartInfo<JavaCharacterTokenId> info) {
                return new JavaStringLexer<JavaCharacterTokenId>(info, false);
            }

            protected String mimeType() {
                return "text/x-java-character";
            }
        }.language();
    }

}

