/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.tools.javac.api.JavacTrees
 *  com.sun.tools.javac.api.JavacTrees$Copier
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.TreeMaker
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Factory
 */
package org.netbeans.lib.nbjavac.services;

import com.sun.source.tree.ClassTree;
import com.sun.tools.javac.api.JavacTrees;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.Context;
import org.netbeans.lib.nbjavac.services.NBTreeMaker;

public class NBJavacTrees
extends JavacTrees {
    public static void preRegister(Context context) {
        context.put(JavacTrees.class, (Context.Factory)new Context.Factory<JavacTrees>(){

            public JavacTrees make(Context c) {
                return new NBJavacTrees(c);
            }
        });
    }

    protected NBJavacTrees(Context context) {
        super(context);
    }

    protected JavacTrees.Copier createCopier(TreeMaker make) {
        return new JavacTrees.Copier(make){

            public JCTree visitClass(ClassTree node, JCTree p) {
                JCTree result = super.visitClass(node, (Object)p);
                if (node instanceof NBTreeMaker.IndexedClassDecl && result instanceof NBTreeMaker.IndexedClassDecl) {
                    ((NBTreeMaker.IndexedClassDecl)result).index = ((NBTreeMaker.IndexedClassDecl)node).index;
                }
                return result;
            }
        };
    }

}

