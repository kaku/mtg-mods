/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.comp.Enter
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCClassDecl
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Factory
 *  com.sun.tools.javac.util.Context$Key
 */
package org.netbeans.lib.nbjavac.services;

import com.sun.tools.javac.comp.Enter;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.Context;
import org.netbeans.lib.nbjavac.services.CancelService;
import org.netbeans.lib.nbjavac.services.NBTreeMaker;

public class NBEnter
extends Enter {
    private final CancelService cancelService;

    public static void preRegister(Context context) {
        context.put(enterKey, (Context.Factory)new Context.Factory<Enter>(){

            public Enter make(Context c) {
                return new NBEnter(c);
            }
        });
    }

    public NBEnter(Context context) {
        super(context);
        this.cancelService = CancelService.instance(context);
    }

    public void visitClassDef(JCTree.JCClassDecl tree) {
        this.cancelService.abortIfCanceled();
        super.visitClassDef(tree);
    }

    protected int getIndex(JCTree.JCClassDecl clazz) {
        return clazz instanceof NBTreeMaker.IndexedClassDecl ? ((NBTreeMaker.IndexedClassDecl)clazz).index : -1;
    }

}

