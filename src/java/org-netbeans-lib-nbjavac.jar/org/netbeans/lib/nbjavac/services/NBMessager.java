/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Factory
 *  com.sun.tools.javac.util.Context$Key
 *  com.sun.tools.javac.util.JCDiagnostic
 *  com.sun.tools.javac.util.JCDiagnostic$DiagnosticPosition
 *  com.sun.tools.javac.util.Log
 *  com.sun.tools.javadoc.Messager
 */
package org.netbeans.lib.nbjavac.services;

import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.JCDiagnostic;
import com.sun.tools.javac.util.Log;
import com.sun.tools.javadoc.Messager;
import java.io.PrintWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.tools.JavaFileObject;

public final class NBMessager
extends Messager {
    private static final String ERR_NOT_IN_PROFILE = "not.in.profile";
    private final Map<URI, Collection<Symbol.ClassSymbol>> notInProfiles = new HashMap<URI, Collection<Symbol.ClassSymbol>>();

    private NBMessager(Context context, String programName, PrintWriter errWriter, PrintWriter warnWriter, PrintWriter noticeWriter) {
        super(context, programName, errWriter, warnWriter, noticeWriter);
    }

    public static NBMessager instance(Context context) {
        Log log = Log.instance((Context)context);
        if (!(log instanceof NBMessager)) {
            throw new InternalError("No NBMessager instance!");
        }
        return (NBMessager)log;
    }

    public static void preRegister(Context context, final String programName, final PrintWriter errWriter, final PrintWriter warnWriter, final PrintWriter noticeWriter) {
        context.put(logKey, (Context.Factory)new Context.Factory<Log>(){

            public Log make(Context c) {
                return new NBMessager(c, programName, errWriter, warnWriter, noticeWriter);
            }
        });
    }

    public /* varargs */ void error(JCDiagnostic.DiagnosticPosition pos, String key, Object ... args) {
        JavaFileObject currentFile;
        if ("not.in.profile".equals(key) && (currentFile = this.currentSourceFile()) != null) {
            URI uri = currentFile.toUri();
            Symbol.ClassSymbol type = (Symbol.ClassSymbol)args[0];
            Collection<Symbol.ClassSymbol> types = this.notInProfiles.get(uri);
            if (types == null) {
                types = new ArrayList<Symbol.ClassSymbol>();
                this.notInProfiles.put(uri, types);
            }
            types.add(type);
        }
        super.error(pos, key, args);
    }

    Collection<? extends Symbol.ClassSymbol> removeNotInProfile(URI uri) {
        return uri == null ? null : this.notInProfiles.remove(uri);
    }

    protected int getDefaultMaxWarnings() {
        return Integer.MAX_VALUE;
    }

    protected int getDefaultMaxErrors() {
        return Integer.MAX_VALUE;
    }

}

