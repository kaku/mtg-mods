/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Key
 */
package org.netbeans.lib.nbjavac.services;

import com.sun.tools.javac.util.Context;
import org.netbeans.lib.nbjavac.services.CancelAbort;

public class CancelService {
    protected static final Context.Key<CancelService> cancelServiceKey = new Context.Key();

    public static CancelService instance(Context context) {
        CancelService instance = (CancelService)context.get(cancelServiceKey);
        if (instance == null) {
            instance = new CancelService();
            context.put(cancelServiceKey, (Object)instance);
        }
        return instance;
    }

    protected CancelService() {
    }

    public boolean isCanceled() {
        return false;
    }

    public final void abortIfCanceled() {
        if (this.isCanceled()) {
            throw new CancelAbort();
        }
    }
}

