/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.comp.Attr
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCBlock
 *  com.sun.tools.javac.tree.JCTree$JCClassDecl
 *  com.sun.tools.javac.tree.JCTree$JCMethodDecl
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Factory
 *  com.sun.tools.javac.util.Context$Key
 */
package org.netbeans.lib.nbjavac.services;

import com.sun.tools.javac.comp.Attr;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.Context;
import org.netbeans.lib.nbjavac.services.CancelService;

public class NBAttr
extends Attr {
    private final CancelService cancelService;

    public static void preRegister(Context context) {
        context.put(attrKey, (Context.Factory)new Context.Factory<Attr>(){

            public Attr make(Context c) {
                return new NBAttr(c);
            }
        });
    }

    public NBAttr(Context context) {
        super(context);
        this.cancelService = CancelService.instance(context);
    }

    public void visitClassDef(JCTree.JCClassDecl tree) {
        this.cancelService.abortIfCanceled();
        super.visitClassDef(tree);
    }

    public void visitMethodDef(JCTree.JCMethodDecl tree) {
        this.cancelService.abortIfCanceled();
        super.visitMethodDef(tree);
    }

    public void visitBlock(JCTree.JCBlock tree) {
        this.cancelService.abortIfCanceled();
        super.visitBlock(tree);
    }

}

