/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.code.Attribute
 *  com.sun.tools.javac.code.Attribute$Compound
 *  com.sun.tools.javac.code.Attribute$RetentionPolicy
 *  com.sun.tools.javac.code.Attribute$TypeCompound
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.code.Symbol$MethodSymbol
 *  com.sun.tools.javac.code.Symbol$VarSymbol
 *  com.sun.tools.javac.code.TargetType
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.code.TypeAnnotationPosition
 *  com.sun.tools.javac.code.Types
 *  com.sun.tools.javac.jvm.ClassWriter
 *  com.sun.tools.javac.jvm.Code
 *  com.sun.tools.javac.jvm.Pool
 *  com.sun.tools.javac.jvm.Target
 *  com.sun.tools.javac.util.ByteBuffer
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Factory
 *  com.sun.tools.javac.util.Context$Key
 *  com.sun.tools.javac.util.List
 *  com.sun.tools.javac.util.ListBuffer
 *  com.sun.tools.javac.util.Name
 */
package org.netbeans.lib.nbjavac.services;

import com.sun.tools.javac.code.Attribute;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.TargetType;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.TypeAnnotationPosition;
import com.sun.tools.javac.code.Types;
import com.sun.tools.javac.jvm.ClassWriter;
import com.sun.tools.javac.jvm.Code;
import com.sun.tools.javac.jvm.Pool;
import com.sun.tools.javac.jvm.Target;
import com.sun.tools.javac.util.ByteBuffer;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.ListBuffer;
import com.sun.tools.javac.util.Name;
import java.net.URI;
import java.util.Collection;
import javax.tools.JavaFileObject;
import org.netbeans.lib.nbjavac.services.NBMessager;
import org.netbeans.lib.nbjavac.services.NBNames;

public class NBClassWriter
extends ClassWriter {
    private final NBNames nbNames;
    private final NBMessager nbMessager;
    private final Target target;
    private final Types types;

    public static void preRegister(Context context) {
        context.put(classWriterKey, (Context.Factory)new Context.Factory<ClassWriter>(){

            public ClassWriter make(Context c) {
                return new NBClassWriter(c);
            }
        });
    }

    protected NBClassWriter(Context context) {
        super(context);
        this.nbNames = NBNames.instance(context);
        this.nbMessager = NBMessager.instance(context);
        this.target = Target.instance((Context)context);
        this.types = Types.instance((Context)context);
    }

    protected int writeExtraClassAttributes(Symbol.ClassSymbol c) {
        Collection<? extends Symbol.ClassSymbol> nip;
        if (c.sourcefile != null && (nip = this.nbMessager.removeNotInProfile(c.sourcefile.toUri())) != null) {
            for (Symbol.ClassSymbol s : nip) {
                this.pool.put((Object)s.type);
            }
        }
        if (!this.target.hasEnclosingMethodAttribute()) {
            return this.writeEnclosingMethodAttribute(this.nbNames._org_netbeans_EnclosingMethod, c);
        }
        return 0;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected int writeExtraMemberAttributes(Symbol sym) {
        int attrCount = 0;
        if (sym.externalType(this.types).isErroneous()) {
            int rsIdx = this.writeAttr(this.nbNames._org_netbeans_TypeSignature);
            try {
                this.preserveErrors = true;
                this.databuf.appendChar(this.pool.put((Object)this.typeSig(sym.type)));
            }
            finally {
                this.preserveErrors = false;
            }
            this.endAttr(rsIdx);
            ++attrCount;
        }
        return attrCount;
    }

    protected int writeExtraParameterAttributes(Symbol.MethodSymbol m) {
        boolean hasSourceLevel = false;
        if (m.params != null) {
            block0 : for (Symbol.VarSymbol s : m.params) {
                for (Attribute.Compound a : s.getRawAttributes()) {
                    if (this.types.getRetention(a) != Attribute.RetentionPolicy.SOURCE) continue;
                    hasSourceLevel = true;
                    continue block0;
                }
            }
        }
        int attrCount = 0;
        if (hasSourceLevel) {
            int attrIndex = this.writeAttr(this.nbNames._org_netbeans_SourceLevelParameterAnnotations);
            this.databuf.appendByte(m.params.length());
            for (Symbol.VarSymbol s : m.params) {
                ListBuffer buf = new ListBuffer();
                for (Attribute.Compound a2 : s.getRawAttributes()) {
                    if (this.types.getRetention(a2) != Attribute.RetentionPolicy.SOURCE) continue;
                    buf.append((Object)a2);
                }
                this.databuf.appendChar(buf.length());
                for (Attribute.Compound a2 : buf) {
                    this.writeCompoundAttribute(a2);
                }
            }
            this.endAttr(attrIndex);
            ++attrCount;
        }
        if (m.code == null && m.params != null && m.params.nonEmpty()) {
            int attrIndex = this.writeAttr(this.nbNames._org_netbeans_ParameterNames);
            for (Symbol.VarSymbol s : m.params) {
                this.databuf.appendChar(this.pool.put((Object)s.name));
            }
            this.endAttr(attrIndex);
            ++attrCount;
        }
        return attrCount;
    }

    protected int writeExtraJavaAnnotations(List<Attribute.Compound> attrs) {
        ListBuffer sourceLevel = new ListBuffer();
        for (Attribute.Compound a : attrs) {
            if (this.types.getRetention(a) != Attribute.RetentionPolicy.SOURCE) continue;
            sourceLevel.append((Object)a);
        }
        int attrCount = 0;
        if (sourceLevel.nonEmpty()) {
            int attrIndex = this.writeAttr(this.nbNames._org_netbeans_SourceLevelAnnotations);
            this.databuf.appendChar(sourceLevel.length());
            for (Attribute.Compound a2 : sourceLevel) {
                this.writeCompoundAttribute(a2);
            }
            this.endAttr(attrIndex);
            ++attrCount;
        }
        return attrCount;
    }

    protected int writeExtraTypeAnnotations(List<Attribute.TypeCompound> attrs) {
        ListBuffer sourceLevel = new ListBuffer();
        for (Attribute.TypeCompound tc : attrs) {
            boolean fixed;
            if (tc.hasUnknownPosition() && !(fixed = tc.tryFixPosition()) || tc.position.type.isLocal() || !tc.position.emitToClassfile() || this.types.getRetention((Attribute.Compound)tc) != Attribute.RetentionPolicy.SOURCE) continue;
            sourceLevel.append((Object)tc);
        }
        int attrCount = 0;
        if (sourceLevel.nonEmpty()) {
            int attrIndex = this.writeAttr(this.nbNames._org_netbeans_SourceLevelTypeAnnotations);
            this.databuf.appendChar(sourceLevel.length());
            for (Attribute.TypeCompound p : sourceLevel) {
                this.writeTypeAnnotation(p);
            }
            this.endAttr(attrIndex);
            ++attrCount;
        }
        return attrCount;
    }

}

