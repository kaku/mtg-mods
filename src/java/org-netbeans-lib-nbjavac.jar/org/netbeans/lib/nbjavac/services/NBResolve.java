/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$TypeSymbol
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.comp.AttrContext
 *  com.sun.tools.javac.comp.Env
 *  com.sun.tools.javac.comp.Resolve
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Factory
 *  com.sun.tools.javac.util.Context$Key
 */
package org.netbeans.lib.nbjavac.services;

import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.comp.AttrContext;
import com.sun.tools.javac.comp.Env;
import com.sun.tools.javac.comp.Resolve;
import com.sun.tools.javac.util.Context;

public class NBResolve
extends Resolve {
    private boolean accessibleOverride;

    public static NBResolve instance(Context context) {
        Resolve instance = (Resolve)context.get(resolveKey);
        if (instance == null) {
            instance = new NBResolve(context);
        }
        return (NBResolve)instance;
    }

    public static void preRegister(Context context) {
        context.put(resolveKey, (Context.Factory)new Context.Factory<Resolve>(){

            public Resolve make(Context c) {
                return new NBResolve(c);
            }
        });
    }

    protected NBResolve(Context ctx) {
        super(ctx);
    }

    public void disableAccessibilityChecks() {
        this.accessibleOverride = true;
    }

    public void restoreAccessbilityChecks() {
        this.accessibleOverride = false;
    }

    public boolean isAccessible(Env<AttrContext> env, Type site, Symbol sym, boolean checkInner) {
        if (this.accessibleOverride) {
            return true;
        }
        return super.isAccessible(env, site, sym, checkInner);
    }

    public boolean isAccessible(Env<AttrContext> env, Symbol.TypeSymbol c, boolean checkInner) {
        if (this.accessibleOverride) {
            return true;
        }
        return super.isAccessible(env, c, checkInner);
    }

}

