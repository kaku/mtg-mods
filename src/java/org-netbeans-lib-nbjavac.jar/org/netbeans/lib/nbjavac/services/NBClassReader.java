/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$MethodSymbol
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.jvm.ClassFile
 *  com.sun.tools.javac.jvm.ClassFile$Version
 *  com.sun.tools.javac.jvm.ClassReader
 *  com.sun.tools.javac.jvm.ClassReader$AttributeKind
 *  com.sun.tools.javac.jvm.ClassReader$AttributeReader
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Factory
 *  com.sun.tools.javac.util.Context$Key
 *  com.sun.tools.javac.util.List
 *  com.sun.tools.javac.util.Name
 *  com.sun.tools.javac.util.Names
 *  com.sun.tools.javadoc.JavadocClassReader
 */
package org.netbeans.lib.nbjavac.services;

import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.jvm.ClassFile;
import com.sun.tools.javac.jvm.ClassReader;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;
import com.sun.tools.javadoc.JavadocClassReader;
import java.util.Map;
import java.util.Set;
import org.netbeans.lib.nbjavac.services.NBNames;

public class NBClassReader
extends JavadocClassReader {
    private final Names names;
    private final NBNames nbNames;

    public static void preRegister(Context context, final boolean loadDocEnv) {
        context.put(classReaderKey, (Context.Factory)new Context.Factory<ClassReader>(){

            public ClassReader make(Context c) {
                return new NBClassReader(c, loadDocEnv);
            }
        });
    }

    public NBClassReader(Context context, boolean loadDocEnv) {
        NBAttributeReader[] readers;
        super(context, loadDocEnv);
        this.names = Names.instance((Context)context);
        this.nbNames = NBNames.instance(context);
        for (NBAttributeReader r : readers = new NBAttributeReader[]{new NBAttributeReader(this.nbNames._org_netbeans_EnclosingMethod, ClassFile.Version.V45_3, this.CLASS_OR_MEMBER_ATTRIBUTE){

            public void read(Symbol sym, int attrLen) {
                int newbp = NBClassReader.this.bp + attrLen;
                NBClassReader.this.readEnclosingMethodAttr(sym);
                NBClassReader.this.bp = newbp;
            }
        }, new NBAttributeReader(this.nbNames._org_netbeans_TypeSignature, ClassFile.Version.V49, this.CLASS_OR_MEMBER_ATTRIBUTE){

            protected void read(Symbol sym, int attrLen) {
                sym.type = NBClassReader.this.readType(NBClassReader.this.nextChar());
            }
        }, new NBAttributeReader(this.nbNames._org_netbeans_ParameterNames, ClassFile.Version.V49, this.CLASS_OR_MEMBER_ATTRIBUTE){

            protected void read(Symbol sym, int attrLen) {
                List parameterTypes;
                int newbp = NBClassReader.this.bp + attrLen;
                List parameterNames = List.nil();
                int numParams = 0;
                if (sym.type != null && (parameterTypes = sym.type.getParameterTypes()) != null) {
                    numParams = parameterTypes.length();
                }
                for (int i = 0; i < numParams; ++i) {
                    if (NBClassReader.this.bp >= newbp - 1) continue;
                    parameterNames = parameterNames.prepend((Object)NBClassReader.this.readName(NBClassReader.this.nextChar()));
                }
                parameterNames = parameterNames.reverse();
                while (parameterNames.length() < numParams) {
                    parameterNames = parameterNames.prepend((Object)NBClassReader.access$1000((NBClassReader)NBClassReader.this).empty);
                }
                ((Symbol.MethodSymbol)sym).savedParameterNames = parameterNames;
            }
        }, new NBAttributeReader(this.nbNames._org_netbeans_SourceLevelAnnotations, ClassFile.Version.V49, this.CLASS_OR_MEMBER_ATTRIBUTE){

            protected void read(Symbol sym, int attrLen) {
                NBClassReader.this.attachAnnotations(sym);
            }
        }, new NBAttributeReader(this.nbNames._org_netbeans_SourceLevelParameterAnnotations, ClassFile.Version.V49, this.CLASS_OR_MEMBER_ATTRIBUTE){

            protected void read(Symbol sym, int attrLen) {
                NBClassReader.this.attachParameterAnnotations(sym);
            }
        }, new NBAttributeReader(this.nbNames._org_netbeans_SourceLevelTypeAnnotations, ClassFile.Version.V52, this.CLASS_OR_MEMBER_ATTRIBUTE){

            protected void read(Symbol sym, int attrLen) {
                NBClassReader.this.attachTypeAnnotations(sym);
            }
        }}) {
            this.attributeReaders.put(r.getName(), r);
        }
    }

    static /* synthetic */ Names access$1000(NBClassReader x0) {
        return x0.names;
    }

    private abstract class NBAttributeReader
    extends ClassReader.AttributeReader {
        private NBAttributeReader(Name name, ClassFile.Version version, Set<ClassReader.AttributeKind> kinds) {
            super((ClassReader)NBClassReader.this, name, version, kinds);
        }

        private Name getName() {
            return this.name;
        }
    }

}

