/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.comp.MemberEnter
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCCompilationUnit
 *  com.sun.tools.javac.tree.JCTree$JCImport
 *  com.sun.tools.javac.tree.JCTree$JCMethodDecl
 *  com.sun.tools.javac.tree.JCTree$JCVariableDecl
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Factory
 *  com.sun.tools.javac.util.Context$Key
 *  com.sun.tools.javadoc.JavadocMemberEnter
 */
package org.netbeans.lib.nbjavac.services;

import com.sun.tools.javac.comp.MemberEnter;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javadoc.JavadocMemberEnter;
import org.netbeans.lib.nbjavac.services.CancelService;

public class NBJavadocMemberEnter
extends JavadocMemberEnter {
    private final CancelService cancelService;

    public static void preRegister(Context context) {
        context.put(memberEnterKey, (Context.Factory)new Context.Factory<MemberEnter>(){

            public MemberEnter make(Context c) {
                return new NBJavadocMemberEnter(c);
            }
        });
    }

    public NBJavadocMemberEnter(Context context) {
        super(context);
        this.cancelService = CancelService.instance(context);
    }

    public void visitTopLevel(JCTree.JCCompilationUnit tree) {
        this.cancelService.abortIfCanceled();
        super.visitTopLevel(tree);
    }

    public void visitImport(JCTree.JCImport tree) {
        this.cancelService.abortIfCanceled();
        super.visitImport(tree);
    }

    public void visitMethodDef(JCTree.JCMethodDecl tree) {
        this.cancelService.abortIfCanceled();
        super.visitMethodDef(tree);
    }

    public void visitVarDef(JCTree.JCVariableDecl tree) {
        this.cancelService.abortIfCanceled();
        super.visitVarDef(tree);
    }

}

