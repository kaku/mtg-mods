/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.code.Symtab
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.comp.Attr
 *  com.sun.tools.javac.comp.Env
 *  com.sun.tools.javac.comp.Flow
 *  com.sun.tools.javac.comp.MemberEnter
 *  com.sun.tools.javac.parser.JavacParser
 *  com.sun.tools.javac.parser.LazyDocCommentTable
 *  com.sun.tools.javac.parser.LazyDocCommentTable$Entry
 *  com.sun.tools.javac.tree.DocCommentTable
 *  com.sun.tools.javac.tree.EndPosTable
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCBlock
 *  com.sun.tools.javac.tree.JCTree$JCClassDecl
 *  com.sun.tools.javac.tree.JCTree$JCCompilationUnit
 *  com.sun.tools.javac.tree.JCTree$JCExpressionStatement
 *  com.sun.tools.javac.tree.JCTree$JCMethodDecl
 *  com.sun.tools.javac.tree.JCTree$JCModifiers
 *  com.sun.tools.javac.tree.JCTree$JCStatement
 *  com.sun.tools.javac.tree.TreeInfo
 *  com.sun.tools.javac.tree.TreeMaker
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Key
 *  com.sun.tools.javac.util.JCDiagnostic
 *  com.sun.tools.javac.util.JCDiagnostic$DiagnosticPosition
 *  com.sun.tools.javac.util.List
 *  com.sun.tools.javac.util.Log
 *  com.sun.tools.javac.util.Name
 *  com.sun.tools.javac.util.Names
 */
package org.netbeans.lib.nbjavac.services;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Symtab;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.comp.Attr;
import com.sun.tools.javac.comp.Env;
import com.sun.tools.javac.comp.Flow;
import com.sun.tools.javac.comp.MemberEnter;
import com.sun.tools.javac.parser.JavacParser;
import com.sun.tools.javac.parser.LazyDocCommentTable;
import com.sun.tools.javac.tree.DocCommentTable;
import com.sun.tools.javac.tree.EndPosTable;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeInfo;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.JCDiagnostic;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Log;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;
import java.nio.CharBuffer;
import java.util.Map;
import org.netbeans.lib.nbjavac.services.NBParserFactory;

public class PartialReparser {
    protected static final Context.Key<PartialReparser> partialReparserKey = new Context.Key();
    private final Context context;

    public static PartialReparser instance(Context ctx) {
        PartialReparser res = (PartialReparser)ctx.get(partialReparserKey);
        if (res == null) {
            res = new PartialReparser(ctx);
            ctx.put(partialReparserKey, (Object)res);
        }
        return res;
    }

    public PartialReparser(Context context) {
        this.context = context;
    }

    public JCTree.JCBlock reparseMethodBody(CompilationUnitTree topLevel, MethodTree methodToReparse, String newBodyText, int annonIndex, Map<? super JCTree, ? super LazyDocCommentTable.Entry> docComments) {
        NBParserFactory parserFactory = (NBParserFactory)NBParserFactory.instance((Context)this.context);
        CharBuffer buf = CharBuffer.wrap((newBodyText + "\u0000").toCharArray(), 0, newBodyText.length());
        JavacParser parser = parserFactory.newParser(buf, ((JCTree.JCBlock)methodToReparse.getBody()).pos, ((JCTree.JCCompilationUnit)topLevel).endPositions);
        JCTree.JCStatement statement = parser.parseStatement();
        NBParserFactory.assignAnonymousClassIndices(Names.instance((Context)this.context), (JCTree)statement, Names.instance((Context)this.context).empty, annonIndex);
        if (statement.getKind() == Tree.Kind.BLOCK) {
            if (docComments != null) {
                docComments.putAll(((LazyDocCommentTable)parser.getDocComments()).table);
            }
            return (JCTree.JCBlock)statement;
        }
        return null;
    }

    public BlockTree reattrMethodBody(MethodTree methodToReparse, BlockTree block) {
        Attr attr = Attr.instance((Context)this.context);
        assert (((JCTree.JCMethodDecl)methodToReparse).localEnv != null);
        JCTree.JCMethodDecl tree = (JCTree.JCMethodDecl)methodToReparse;
        Names names = Names.instance((Context)this.context);
        Symtab syms = Symtab.instance((Context)this.context);
        MemberEnter memberEnter = MemberEnter.instance((Context)this.context);
        Log log = Log.instance((Context)this.context);
        TreeMaker make = TreeMaker.instance((Context)this.context);
        Env env = attr.dupLocalEnv(((JCTree.JCMethodDecl)methodToReparse).localEnv);
        Symbol.ClassSymbol owner = env.enclClass.sym;
        if (tree.name == names.init && !owner.type.isErroneous() && owner.type != syms.objectType) {
            JCTree.JCBlock body = tree.body;
            if (body.stats.isEmpty() || !TreeInfo.isSelfCall((JCTree)((JCTree)body.stats.head))) {
                body.stats = body.stats.prepend((Object)memberEnter.SuperCall(make.at(body.pos), List.nil(), List.nil(), false));
            } else if ((env.enclClass.sym.flags() & 16384) != 0 && (tree.mods.flags & 0x1000000000L) == 0 && TreeInfo.isSuperCall((JCTree)((JCTree)body.stats.head))) {
                log.error(((JCTree.JCStatement)tree.body.stats.head).pos(), "call.to.super.not.allowed.in.enum.ctor", new Object[]{env.enclClass.sym});
            }
        }
        attr.attribStat((JCTree)((JCTree.JCBlock)block), env);
        return block;
    }

    public BlockTree reflowMethodBody(CompilationUnitTree topLevel, ClassTree ownerClass, MethodTree methodToReparse) {
        Flow flow = Flow.instance((Context)this.context);
        TreeMaker make = TreeMaker.instance((Context)this.context);
        flow.reanalyzeMethod(make.forToplevel((JCTree.JCCompilationUnit)topLevel), (JCTree.JCClassDecl)ownerClass);
        return methodToReparse.getBody();
    }
}

