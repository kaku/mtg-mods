/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.comp.Enter
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCClassDecl
 *  com.sun.tools.javac.tree.JCTree$JCCompilationUnit
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Factory
 *  com.sun.tools.javac.util.Context$Key
 *  com.sun.tools.javac.util.JCDiagnostic
 *  com.sun.tools.javac.util.JCDiagnostic$DiagnosticPosition
 *  com.sun.tools.javac.util.List
 *  com.sun.tools.javac.util.Name
 *  com.sun.tools.javadoc.JavadocEnter
 *  com.sun.tools.javadoc.Messager
 */
package org.netbeans.lib.nbjavac.services;

import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.comp.Enter;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.JCDiagnostic;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javadoc.JavadocEnter;
import com.sun.tools.javadoc.Messager;
import org.netbeans.lib.nbjavac.services.CancelService;
import org.netbeans.lib.nbjavac.services.NBTreeMaker;

public class NBJavadocEnter
extends JavadocEnter {
    private final Messager messager;
    private final CancelService cancelService;

    public static void preRegister(Context context) {
        context.put(enterKey, (Context.Factory)new Context.Factory<Enter>(){

            public Enter make(Context c) {
                return new NBJavadocEnter(c);
            }
        });
    }

    protected NBJavadocEnter(Context context) {
        super(context);
        this.messager = Messager.instance0((Context)context);
        this.cancelService = CancelService.instance(context);
    }

    public void main(List<JCTree.JCCompilationUnit> trees) {
        this.complete(trees, null);
    }

    protected void duplicateClass(JCDiagnostic.DiagnosticPosition pos, Symbol.ClassSymbol c) {
        this.messager.error(pos, "duplicate.class", new Object[]{c.fullname});
    }

    public void visitClassDef(JCTree.JCClassDecl tree) {
        this.cancelService.abortIfCanceled();
        super.visitClassDef(tree);
    }

    protected int getIndex(JCTree.JCClassDecl clazz) {
        return clazz instanceof NBTreeMaker.IndexedClassDecl ? ((NBTreeMaker.IndexedClassDecl)clazz).index : -1;
    }

}

