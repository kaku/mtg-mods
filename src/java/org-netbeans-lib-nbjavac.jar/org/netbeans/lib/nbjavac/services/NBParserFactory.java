/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.parser.JavacParser
 *  com.sun.tools.javac.parser.JavacParser$AbstractEndPosTable
 *  com.sun.tools.javac.parser.JavacParser$SimpleEndPosTable
 *  com.sun.tools.javac.parser.Lexer
 *  com.sun.tools.javac.parser.ParserFactory
 *  com.sun.tools.javac.parser.Scanner
 *  com.sun.tools.javac.parser.ScannerFactory
 *  com.sun.tools.javac.parser.Tokens
 *  com.sun.tools.javac.parser.Tokens$Comment
 *  com.sun.tools.javac.parser.Tokens$Token
 *  com.sun.tools.javac.tree.EndPosTable
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCBlock
 *  com.sun.tools.javac.tree.JCTree$JCClassDecl
 *  com.sun.tools.javac.tree.JCTree$JCCompilationUnit
 *  com.sun.tools.javac.tree.JCTree$JCExpression
 *  com.sun.tools.javac.tree.JCTree$JCMethodInvocation
 *  com.sun.tools.javac.tree.JCTree$JCModifiers
 *  com.sun.tools.javac.tree.JCTree$JCTypeParameter
 *  com.sun.tools.javac.tree.TreeScanner
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Factory
 *  com.sun.tools.javac.util.Context$Key
 *  com.sun.tools.javac.util.List
 *  com.sun.tools.javac.util.Name
 *  com.sun.tools.javac.util.Names
 */
package org.netbeans.lib.nbjavac.services;

import com.sun.tools.javac.parser.JavacParser;
import com.sun.tools.javac.parser.Lexer;
import com.sun.tools.javac.parser.ParserFactory;
import com.sun.tools.javac.parser.Scanner;
import com.sun.tools.javac.parser.ScannerFactory;
import com.sun.tools.javac.parser.Tokens;
import com.sun.tools.javac.tree.EndPosTable;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeScanner;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import org.netbeans.lib.nbjavac.services.CancelService;
import org.netbeans.lib.nbjavac.services.NBTreeMaker;

public class NBParserFactory
extends ParserFactory {
    private final ScannerFactory scannerFactory;
    private final Names names;
    private final CancelService cancelService;

    public static void preRegister(Context context) {
        context.put(parserFactoryKey, (Context.Factory)new Context.Factory<ParserFactory>(){

            public ParserFactory make(Context c) {
                return new NBParserFactory(c);
            }
        });
    }

    protected NBParserFactory(Context context) {
        super(context);
        this.scannerFactory = ScannerFactory.instance((Context)context);
        this.names = Names.instance((Context)context);
        this.cancelService = CancelService.instance(context);
    }

    public JavacParser newParser(CharSequence input, boolean keepDocComments, boolean keepEndPos, boolean keepLineMap) {
        Scanner lexer = this.scannerFactory.newScanner(input, keepDocComments);
        return new NBJavacParser(this, (Lexer)lexer, keepDocComments, keepLineMap, keepEndPos, this.cancelService);
    }

    public JavacParser newParser(CharSequence input, int startPos, final EndPosTable endPos) {
        Scanner lexer = this.scannerFactory.newScanner(input, true);
        lexer.seek(startPos);
        ((NBJavacParser.EndPosTableImpl)endPos).resetErrorEndPos();
        return new NBJavacParser(this, (Lexer)lexer, true, false, true, this.cancelService){

            @Override
            protected JavacParser.AbstractEndPosTable newEndPosTable(boolean keepEndPositions) {
                return new JavacParser.AbstractEndPosTable(this){

                    public void storeEnd(JCTree tree, int endpos) {
                        ((NBJavacParser.EndPosTableImpl)endPos).storeEnd(tree, endpos);
                    }

                    protected <T extends JCTree> T to(T t) {
                        this.storeEnd((JCTree)t, .access$100(()2.this).endPos);
                        return t;
                    }

                    protected <T extends JCTree> T toP(T t) {
                        this.storeEnd((JCTree)t, .access$200(()2.this).prevToken().endPos);
                        return t;
                    }

                    public int getEndPos(JCTree tree) {
                        return endPos.getEndPos(tree);
                    }

                    public int replaceTree(JCTree oldtree, JCTree newtree) {
                        return endPos.replaceTree(oldtree, newtree);
                    }

                    protected void setErrorEndPos(int errPos) {
                        super.setErrorEndPos(errPos);
                        ((NBJavacParser.EndPosTableImpl)endPos).setErrorEndPos(errPos);
                    }
                };
            }

            static /* synthetic */ Tokens.Token access$100( x0) {
                return x0.token;
            }

            static /* synthetic */ Lexer access$200( x0) {
                return x0.S;
            }

        };
    }

    public static void assignAnonymousClassIndices(Names names, JCTree tree, Name name, int startNumber) {
        AssignAnonymousIndices aai = new AssignAnonymousIndices(names);
        if (name != null) {
            aai.newAnonScope(name, startNumber);
        }
        aai.scan(tree);
    }

    private static final class AssignAnonymousIndices
    extends TreeScanner {
        private final Names names;
        private final Map<Name, AnonScope> anonScopeMap = new HashMap<Name, AnonScope>();
        private final Stack<AnonScope> anonScopes = new Stack();

        public AssignAnonymousIndices(Names names) {
            this.names = names;
        }

        void newAnonScope(Name name) {
            this.newAnonScope(name, 1);
        }

        public void newAnonScope(Name name, int startNumber) {
            AnonScope parent = this.anonScopes.isEmpty() ? null : this.anonScopes.peek();
            Name fqn = parent != null && parent.parentDecl != this.names.empty ? parent.parentDecl.append('.', name) : name;
            AnonScope scope = this.anonScopeMap.get((Object)fqn);
            if (scope == null) {
                scope = new AnonScope(name, startNumber);
                this.anonScopeMap.put(fqn, scope);
            }
            this.anonScopes.push(scope);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void visitClassDef(JCTree.JCClassDecl tree) {
            if (tree.name == this.names.empty) {
                ((NBTreeMaker.IndexedClassDecl)tree).index = this.anonScopes.peek().assignNumber();
            }
            this.newAnonScope(tree.name);
            try {
                super.visitClassDef(tree);
            }
            finally {
                this.anonScopes.pop();
            }
            if (!this.anonScopes.isEmpty() && this.anonScopes.peek().localClass && tree.name != this.names.empty) {
                ((NBTreeMaker.IndexedClassDecl)tree).index = this.anonScopes.peek().assignLocalNumber(tree.name);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void visitBlock(JCTree.JCBlock tree) {
            AnonScope as = this.anonScopes.peek();
            boolean old = as.localClass;
            as.localClass = true;
            try {
                super.visitBlock(tree);
            }
            finally {
                as.localClass = old;
            }
        }

        public void visitApply(JCTree.JCMethodInvocation tree) {
            this.scan(tree.args);
            this.scan((JCTree)tree.meth);
        }

        private static class AnonScope {
            public boolean localClass;
            private final Name parentDecl;
            private int currentNumber;
            private Map<Name, Integer> localClasses;

            private AnonScope(Name name, int startNumber) {
                assert (name != null);
                this.parentDecl = name;
                this.currentNumber = startNumber;
            }

            public int assignNumber() {
                int ret = this.currentNumber;
                if (this.currentNumber != -1) {
                    ++this.currentNumber;
                }
                return ret;
            }

            public int assignLocalNumber(Name name) {
                Integer num;
                if (this.localClasses == null) {
                    this.localClasses = new HashMap<Name, Integer>();
                }
                num = (num = this.localClasses.get((Object)name)) == null ? Integer.valueOf(1) : Integer.valueOf(num + 1);
                this.localClasses.put(name, num);
                return num;
            }

            public String toString() {
                return String.format("%s : %d", this.parentDecl.toString(), this.currentNumber);
            }
        }

    }

    public static class NBJavacParser
    extends JavacParser {
        private final Names names;
        private final CancelService cancelService;

        public NBJavacParser(NBParserFactory fac, Lexer S, boolean keepDocComments, boolean keepLineMap, boolean keepEndPos, CancelService cancelService) {
            super((ParserFactory)fac, S, keepDocComments, keepLineMap, keepEndPos);
            this.names = fac.names;
            this.cancelService = cancelService;
        }

        protected JavacParser.AbstractEndPosTable newEndPosTable(boolean keepEndPositions) {
            return keepEndPositions ? new EndPosTableImpl(this) : super.newEndPosTable(keepEndPositions);
        }

        protected JCTree.JCClassDecl classDeclaration(JCTree.JCModifiers mods, Tokens.Comment dc) {
            if (this.cancelService != null) {
                this.cancelService.abortIfCanceled();
            }
            return super.classDeclaration(mods, dc);
        }

        protected JCTree.JCClassDecl interfaceDeclaration(JCTree.JCModifiers mods, Tokens.Comment dc) {
            if (this.cancelService != null) {
                this.cancelService.abortIfCanceled();
            }
            return super.interfaceDeclaration(mods, dc);
        }

        protected JCTree.JCClassDecl enumDeclaration(JCTree.JCModifiers mods, Tokens.Comment dc) {
            if (this.cancelService != null) {
                this.cancelService.abortIfCanceled();
            }
            return super.enumDeclaration(mods, dc);
        }

        protected JCTree methodDeclaratorRest(int pos, JCTree.JCModifiers mods, JCTree.JCExpression type, Name name, List<JCTree.JCTypeParameter> typarams, boolean isInterface, boolean isVoid, Tokens.Comment dc) {
            if (this.cancelService != null) {
                this.cancelService.abortIfCanceled();
            }
            return super.methodDeclaratorRest(pos, mods, type, name, typarams, isInterface, isVoid, dc);
        }

        public JCTree.JCCompilationUnit parseCompilationUnit() {
            JCTree.JCCompilationUnit toplevel = super.parseCompilationUnit();
            NBParserFactory.assignAnonymousClassIndices(this.names, (JCTree)toplevel, null, -1);
            return toplevel;
        }

        public final class EndPosTableImpl
        extends JavacParser.SimpleEndPosTable {
            private EndPosTableImpl(JavacParser parser) {
                super(parser);
            }

            private void resetErrorEndPos() {
                this.errorEndPos = -1;
            }

            public void storeEnd(JCTree tree, int endpos) {
                if (endpos >= 0) {
                    super.storeEnd(tree, endpos);
                }
            }

            protected void setErrorEndPos(int errPos) {
                super.setErrorEndPos(errPos);
            }
        }

    }

}

