/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.code.Symtab
 *  com.sun.tools.javac.code.Types
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCClassDecl
 *  com.sun.tools.javac.tree.JCTree$JCCompilationUnit
 *  com.sun.tools.javac.tree.JCTree$JCExpression
 *  com.sun.tools.javac.tree.JCTree$JCModifiers
 *  com.sun.tools.javac.tree.JCTree$JCTypeParameter
 *  com.sun.tools.javac.tree.TreeMaker
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Factory
 *  com.sun.tools.javac.util.Context$Key
 *  com.sun.tools.javac.util.List
 *  com.sun.tools.javac.util.Name
 *  com.sun.tools.javac.util.Names
 */
package org.netbeans.lib.nbjavac.services;

import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Symtab;
import com.sun.tools.javac.code.Types;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;

public class NBTreeMaker
extends TreeMaker {
    private final Names names;
    private final Types types;
    private final Symtab syms;

    public static void preRegister(Context context) {
        context.put(treeMakerKey, (Context.Factory)new Context.Factory<TreeMaker>(){

            public TreeMaker make(Context c) {
                return new NBTreeMaker(c);
            }
        });
    }

    protected NBTreeMaker(Context context) {
        super(context);
        this.names = Names.instance((Context)context);
        this.types = Types.instance((Context)context);
        this.syms = Symtab.instance((Context)context);
    }

    protected NBTreeMaker(JCTree.JCCompilationUnit toplevel, Names names, Types types, Symtab syms) {
        super(toplevel, names, types, syms);
        this.names = names;
        this.types = types;
        this.syms = syms;
    }

    public TreeMaker forToplevel(JCTree.JCCompilationUnit toplevel) {
        return new NBTreeMaker(toplevel, this.names, this.types, this.syms);
    }

    public IndexedClassDecl ClassDef(JCTree.JCModifiers mods, Name name, List<JCTree.JCTypeParameter> typarams, JCTree.JCExpression extending, List<JCTree.JCExpression> implementing, List<JCTree> defs) {
        IndexedClassDecl result = new IndexedClassDecl(mods, name, typarams, extending, implementing, defs, null);
        result.pos = this.pos;
        return result;
    }

    public static class IndexedClassDecl
    extends JCTree.JCClassDecl {
        public int index = -1;

        protected IndexedClassDecl(JCTree.JCModifiers mods, Name name, List<JCTree.JCTypeParameter> typarams, JCTree.JCExpression extending, List<JCTree.JCExpression> implementing, List<JCTree> defs, Symbol.ClassSymbol sym) {
            super(mods, name, typarams, extending, implementing, defs, sym);
        }
    }

}

