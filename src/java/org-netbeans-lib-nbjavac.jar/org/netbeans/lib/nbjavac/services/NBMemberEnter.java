/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.comp.MemberEnter
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCCompilationUnit
 *  com.sun.tools.javac.tree.JCTree$JCImport
 *  com.sun.tools.javac.tree.JCTree$JCMethodDecl
 *  com.sun.tools.javac.tree.JCTree$JCVariableDecl
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Factory
 */
package org.netbeans.lib.nbjavac.services;

import com.sun.tools.javac.comp.MemberEnter;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.Context;
import org.netbeans.lib.nbjavac.services.CancelService;

public class NBMemberEnter
extends MemberEnter {
    private final CancelService cancelService;

    public static void preRegister(Context context) {
        context.put(MemberEnter.class, (Context.Factory)new Context.Factory<MemberEnter>(){

            public MemberEnter make(Context c) {
                return new NBMemberEnter(c);
            }
        });
    }

    public NBMemberEnter(Context context) {
        super(context);
        this.cancelService = CancelService.instance(context);
    }

    public void visitTopLevel(JCTree.JCCompilationUnit tree) {
        this.cancelService.abortIfCanceled();
        super.visitTopLevel(tree);
    }

    public void visitImport(JCTree.JCImport tree) {
        this.cancelService.abortIfCanceled();
        super.visitImport(tree);
    }

    public void visitMethodDef(JCTree.JCMethodDecl tree) {
        this.cancelService.abortIfCanceled();
        super.visitMethodDef(tree);
    }

    public void visitVarDef(JCTree.JCVariableDecl tree) {
        this.cancelService.abortIfCanceled();
        super.visitVarDef(tree);
    }

}

