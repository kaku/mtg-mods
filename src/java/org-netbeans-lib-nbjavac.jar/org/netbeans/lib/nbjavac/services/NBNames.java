/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Factory
 *  com.sun.tools.javac.util.Context$Key
 *  com.sun.tools.javac.util.Name
 *  com.sun.tools.javac.util.Names
 */
package org.netbeans.lib.nbjavac.services;

import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;

public class NBNames {
    public static final Context.Key<NBNames> nbNamesKey = new Context.Key();
    public final Name _org_netbeans_EnclosingMethod;
    public final Name _org_netbeans_TypeSignature;
    public final Name _org_netbeans_ParameterNames;
    public final Name _org_netbeans_SourceLevelAnnotations;
    public final Name _org_netbeans_SourceLevelParameterAnnotations;
    public final Name _org_netbeans_SourceLevelTypeAnnotations;

    public static void preRegister(Context context) {
        context.put(nbNamesKey, (Context.Factory)new Context.Factory<NBNames>(){

            public NBNames make(Context c) {
                return new NBNames(c);
            }
        });
    }

    public static NBNames instance(Context context) {
        NBNames instance = (NBNames)context.get(nbNamesKey);
        if (instance == null) {
            instance = new NBNames(context);
        }
        return instance;
    }

    protected NBNames(Context context) {
        Names n = Names.instance((Context)context);
        this._org_netbeans_EnclosingMethod = n.fromString("org.netbeans.EnclosingMethod");
        this._org_netbeans_TypeSignature = n.fromString("org.netbeans.TypeSignature");
        this._org_netbeans_ParameterNames = n.fromString("org.netbeans.ParameterNames");
        this._org_netbeans_SourceLevelAnnotations = n.fromString("org.netbeans.SourceLevelAnnotations");
        this._org_netbeans_SourceLevelParameterAnnotations = n.fromString("org.netbeans.SourceLevelParameterAnnotations");
        this._org_netbeans_SourceLevelTypeAnnotations = n.fromString("org.netbeans.SourceLevelTypeAnnotations");
    }

}

