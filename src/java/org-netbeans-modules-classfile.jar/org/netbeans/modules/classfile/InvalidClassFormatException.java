/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import java.io.IOException;

public final class InvalidClassFormatException
extends IOException {
    private static final long serialVersionUID = -7043855006167696889L;

    InvalidClassFormatException() {
    }

    InvalidClassFormatException(String s) {
        super(s);
    }

    InvalidClassFormatException(Throwable cause) {
        super(cause.getLocalizedMessage());
        this.initCause(cause);
    }
}

