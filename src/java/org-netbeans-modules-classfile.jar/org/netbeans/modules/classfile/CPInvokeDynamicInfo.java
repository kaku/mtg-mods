/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.ConstantPool;

public class CPInvokeDynamicInfo
extends CPEntry {
    int iBootstrapMethod;
    int iNameAndType;

    CPInvokeDynamicInfo(ConstantPool pool, int iBootstrapMethod, int iNameAndType) {
        super(pool);
        this.iBootstrapMethod = iBootstrapMethod;
        this.iNameAndType = iNameAndType;
    }

    @Override
    public int getTag() {
        return 18;
    }

    public int getBootstrapMethod() {
        return this.iBootstrapMethod;
    }

    public int getNameAndType() {
        return this.iNameAndType;
    }

    public String toString() {
        return this.getClass().getName() + ": bootstrapMethod=" + this.iBootstrapMethod + ", nameAndType=" + this.iNameAndType;
    }
}

