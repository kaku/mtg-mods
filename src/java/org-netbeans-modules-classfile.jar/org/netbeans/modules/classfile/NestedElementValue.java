/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.Annotation;
import org.netbeans.modules.classfile.ConstantPool;
import org.netbeans.modules.classfile.ElementValue;

public final class NestedElementValue
extends ElementValue {
    Annotation value;

    NestedElementValue(ConstantPool pool, Annotation value) {
        this.value = value;
    }

    public final Annotation getNestedValue() {
        return this.value;
    }

    public String toString() {
        return "nested value=" + this.value;
    }
}

