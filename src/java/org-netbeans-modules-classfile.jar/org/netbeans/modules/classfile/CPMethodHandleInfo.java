/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.ConstantPool;

public class CPMethodHandleInfo
extends CPEntry {
    ReferenceKind referenceKind;
    int iReference;

    CPMethodHandleInfo(ConstantPool pool, int referenceKind, int iReference) {
        super(pool);
        this.referenceKind = ReferenceKind.from(referenceKind);
        this.iReference = iReference;
    }

    @Override
    public int getTag() {
        return 15;
    }

    public ReferenceKind getReferenceKind() {
        return this.referenceKind;
    }

    public int getReference() {
        return this.iReference;
    }

    public String toString() {
        return this.getClass().getName() + ": kind=" + (Object)((Object)this.referenceKind) + ", index=" + this.iReference;
    }

    public static enum ReferenceKind {
        getField(1),
        getStatic(2),
        putField(3),
        putStatic(4),
        invokeVirtual(5),
        invokeStatic(6),
        invokeSpecial(7),
        newInvokeSpecial(8),
        invokeInterface(9);
        
        private final int kindInt;

        private ReferenceKind(int kindInt) {
            this.kindInt = kindInt;
        }

        static ReferenceKind from(int referenceKind) {
            for (ReferenceKind k : ReferenceKind.values()) {
                if (k.kindInt != referenceKind) continue;
                return k;
            }
            throw new IllegalStateException("Unknown ref kind: " + referenceKind);
        }
    }

}

