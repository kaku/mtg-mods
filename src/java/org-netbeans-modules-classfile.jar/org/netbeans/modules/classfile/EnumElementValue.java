/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.CPName;
import org.netbeans.modules.classfile.ConstantPool;
import org.netbeans.modules.classfile.ElementValue;

public final class EnumElementValue
extends ElementValue {
    String enumType;
    String enumName;

    EnumElementValue(ConstantPool pool, int iEnumType, int iEnumName) {
        this.enumType = ((CPName)pool.get(iEnumType)).getName();
        this.enumName = ((CPName)pool.get(iEnumName)).getName();
    }

    EnumElementValue(String type, String name) {
        this.enumType = type;
        this.enumName = name;
    }

    public final String getEnumType() {
        return this.enumType;
    }

    public final String getEnumName() {
        return this.enumName;
    }

    public String toString() {
        return this.enumType + "." + this.enumName;
    }
}

