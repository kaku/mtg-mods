/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import java.io.DataInputStream;
import java.io.IOException;
import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.CPUTF8Info;
import org.netbeans.modules.classfile.ConstantPool;
import org.netbeans.modules.classfile.InvalidClassFormatException;

public final class LocalVariableTypeTableEntry {
    int startPC;
    int length;
    String name;
    String signature;
    int index;

    static LocalVariableTypeTableEntry[] loadLocalVariableTypeTable(DataInputStream in, ConstantPool pool) throws IOException {
        int n = in.readUnsignedShort();
        LocalVariableTypeTableEntry[] entries = new LocalVariableTypeTableEntry[n];
        for (int i = 0; i < n; ++i) {
            entries[i] = new LocalVariableTypeTableEntry(in, pool);
        }
        return entries;
    }

    LocalVariableTypeTableEntry(DataInputStream in, ConstantPool pool) throws IOException {
        this.loadLocalVariableEntry(in, pool);
    }

    private void loadLocalVariableEntry(DataInputStream in, ConstantPool pool) throws IOException {
        this.startPC = in.readUnsignedShort();
        this.length = in.readUnsignedShort();
        CPEntry o = pool.get(in.readUnsignedShort());
        if (!(o instanceof CPUTF8Info)) {
            throw new InvalidClassFormatException();
        }
        CPUTF8Info entry = (CPUTF8Info)o;
        this.name = entry.getName();
        o = pool.get(in.readUnsignedShort());
        if (!(o instanceof CPUTF8Info)) {
            throw new InvalidClassFormatException();
        }
        entry = (CPUTF8Info)o;
        this.signature = entry.getName();
        this.index = in.readUnsignedShort();
    }

    public final int getStartPC() {
        return this.startPC;
    }

    public final int getLength() {
        return this.length;
    }

    public final String getName() {
        return this.name;
    }

    public final String getSignature() {
        return this.signature;
    }

    public final int getIndex() {
        return this.index;
    }
}

