/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.netbeans.modules.classfile.CPClassInfo;
import org.netbeans.modules.classfile.CPDoubleInfo;
import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.CPFieldInfo;
import org.netbeans.modules.classfile.CPFloatInfo;
import org.netbeans.modules.classfile.CPIntegerInfo;
import org.netbeans.modules.classfile.CPInterfaceMethodInfo;
import org.netbeans.modules.classfile.CPInvokeDynamicInfo;
import org.netbeans.modules.classfile.CPLongInfo;
import org.netbeans.modules.classfile.CPMethodHandleInfo;
import org.netbeans.modules.classfile.CPMethodInfo;
import org.netbeans.modules.classfile.CPMethodTypeInfo;
import org.netbeans.modules.classfile.CPNameAndTypeInfo;
import org.netbeans.modules.classfile.CPStringInfo;
import org.netbeans.modules.classfile.CPUTF8Info;
import org.netbeans.modules.classfile.ClassName;
import org.netbeans.modules.classfile.ConstantPoolReader;
import org.netbeans.modules.classfile.InvalidClassFormatException;

public final class ConstantPool {
    private static final int CONSTANT_POOL_START = 1;
    static final int CONSTANT_Utf8 = 1;
    static final int CONSTANT_Integer = 3;
    static final int CONSTANT_Float = 4;
    static final int CONSTANT_Long = 5;
    static final int CONSTANT_Double = 6;
    static final int CONSTANT_Class = 7;
    static final int CONSTANT_String = 8;
    static final int CONSTANT_FieldRef = 9;
    static final int CONSTANT_MethodRef = 10;
    static final int CONSTANT_InterfaceMethodRef = 11;
    static final int CONSTANT_NameAndType = 12;
    static final int CONSTANT_MethodHandle = 15;
    static final int CONSTANT_MethodType = 16;
    static final int CONSTANT_InvokeDynamic = 18;
    CPEntry[] cpEntries;
    int constantPoolCount = 0;

    ConstantPool(int size, InputStream bytes) throws IOException {
        if (size < 0) {
            throw new IllegalArgumentException("size cannot be negative");
        }
        if (bytes == null) {
            throw new IllegalArgumentException("byte stream not specified");
        }
        this.constantPoolCount = size;
        this.cpEntries = new CPEntry[this.constantPoolCount];
        this.load(bytes);
    }

    ConstantPool() {
        this.constantPoolCount = 1;
        this.cpEntries = new CPEntry[this.constantPoolCount];
    }

    public final CPEntry get(int index) {
        if (index <= 0 || index >= this.cpEntries.length) {
            throw new IndexOutOfBoundsException(Integer.toString(index));
        }
        return this.cpEntries[index];
    }

    public final CPClassInfo getClass(int index) {
        if (index <= 0) {
            throw new IndexOutOfBoundsException(Integer.toString(index));
        }
        return (CPClassInfo)this.get(index);
    }

    public final <T extends CPEntry> Collection<? extends T> getAllConstants(Class<T> classType) {
        return Collections.unmodifiableCollection(this.getAllConstantsImpl(classType));
    }

    private <T extends CPEntry> Collection<? extends T> getAllConstantsImpl(Class<T> classType) {
        int n = this.cpEntries.length;
        ArrayList<T> c = new ArrayList<T>(n);
        for (int i = 1; i < n; ++i) {
            if (this.cpEntries[i] == null || !this.cpEntries[i].getClass().equals(classType)) continue;
            c.add(classType.cast(this.cpEntries[i]));
        }
        return c;
    }

    public final Set<ClassName> getAllClassNames() {
        HashSet<ClassName> set = new HashSet<ClassName>();
        Collection<CPClassInfo> c = this.getAllConstantsImpl(CPClassInfo.class);
        for (CPClassInfo ci : c) {
            set.add(ci.getClassName());
        }
        return Collections.unmodifiableSet(set);
    }

    final String getString(int index) {
        CPUTF8Info utf = (CPUTF8Info)this.cpEntries[index];
        return utf.getName();
    }

    private void load(InputStream cpBytes) throws IOException {
        try {
            int i;
            ConstantPoolReader cpr = new ConstantPoolReader(cpBytes);
            for (i = 1; i < this.constantPoolCount; ++i) {
                CPEntry newEntry;
                this.cpEntries[i] = newEntry = this.getConstantPoolEntry(cpr);
                if (!newEntry.usesTwoSlots()) continue;
                ++i;
            }
            for (i = 1; i < this.constantPoolCount; ++i) {
                CPEntry entry = this.cpEntries[i];
                if (entry == null) continue;
                entry.resolve(this.cpEntries);
            }
        }
        catch (IllegalArgumentException ioe) {
            throw new InvalidClassFormatException(ioe);
        }
        catch (IndexOutOfBoundsException iobe) {
            throw new InvalidClassFormatException(iobe);
        }
    }

    private CPEntry getConstantPoolEntry(ConstantPoolReader cpr) throws IOException {
        CPEntry newEntry2;
        byte type = cpr.readByte();
        switch (type) {
            CPEntry newEntry2;
            case 1: {
                newEntry2 = new CPUTF8Info(this, cpr.readRawUTF());
                break;
            }
            case 3: {
                newEntry2 = new CPIntegerInfo(this, cpr.readInt());
                break;
            }
            case 4: {
                newEntry2 = new CPFloatInfo(this, cpr.readFloat());
                break;
            }
            case 5: {
                newEntry2 = new CPLongInfo(this, cpr.readLong());
                break;
            }
            case 6: {
                newEntry2 = new CPDoubleInfo(this, cpr.readDouble());
                break;
            }
            case 7: {
                int nameIndex = cpr.readUnsignedShort();
                newEntry2 = new CPClassInfo(this, nameIndex);
                break;
            }
            case 8: {
                int nameIndex = cpr.readUnsignedShort();
                newEntry2 = new CPStringInfo(this, nameIndex);
                break;
            }
            case 9: {
                int classIndex = cpr.readUnsignedShort();
                int natIndex = cpr.readUnsignedShort();
                newEntry2 = new CPFieldInfo(this, classIndex, natIndex);
                break;
            }
            case 10: {
                int classIndex = cpr.readUnsignedShort();
                int natIndex = cpr.readUnsignedShort();
                newEntry2 = new CPMethodInfo(this, classIndex, natIndex);
                break;
            }
            case 11: {
                int classIndex = cpr.readUnsignedShort();
                int natIndex = cpr.readUnsignedShort();
                newEntry2 = new CPInterfaceMethodInfo(this, classIndex, natIndex);
                break;
            }
            case 12: {
                int nameIndex = cpr.readUnsignedShort();
                int descIndex = cpr.readUnsignedShort();
                newEntry2 = new CPNameAndTypeInfo(this, nameIndex, descIndex);
                break;
            }
            case 15: {
                int kind = cpr.readUnsignedByte();
                int index = cpr.readUnsignedShort();
                newEntry2 = new CPMethodHandleInfo(this, kind, index);
                break;
            }
            case 16: {
                int index = cpr.readUnsignedShort();
                newEntry2 = new CPMethodTypeInfo(this, index);
                break;
            }
            case 18: {
                int bootstrapMethod = cpr.readUnsignedShort();
                int nameAndType = cpr.readUnsignedShort();
                newEntry2 = new CPInvokeDynamicInfo(this, bootstrapMethod, nameAndType);
                break;
            }
            default: {
                throw new IllegalArgumentException("invalid constant pool type: " + type);
            }
        }
        return newEntry2;
    }
}

