/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.ConstantPool;

abstract class CPName
extends CPEntry {
    static final int INVALID_INDEX = -1;
    int index;
    private String name;

    CPName(ConstantPool pool, int index) {
        super(pool);
        this.index = index;
    }

    CPName(ConstantPool pool) {
        super(pool);
        this.index = -1;
    }

    public String getName() {
        if (this.index == -1) {
            return null;
        }
        if (this.name == null) {
            this.name = ((CPName)this.pool.cpEntries[this.index]).getName();
        }
        return this.name;
    }

    @Override
    public Object getValue() {
        return this.getName();
    }

    void setNameIndex(int index) {
        this.index = index;
        this.name = null;
    }

    public String toString() {
        return this.getClass().getName() + ": name=" + (this.index == -1 ? "<unresolved>" : ((CPName)this.pool.cpEntries[this.index]).getName());
    }
}

