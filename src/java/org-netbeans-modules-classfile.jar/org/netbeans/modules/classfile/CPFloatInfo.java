/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.ConstantPool;

public final class CPFloatInfo
extends CPEntry {
    CPFloatInfo(ConstantPool pool, float v) {
        super(pool);
        this.value = new Float(v);
    }

    @Override
    public final int getTag() {
        return 4;
    }

    public String toString() {
        return this.getClass().getName() + ": value=" + this.value;
    }
}

