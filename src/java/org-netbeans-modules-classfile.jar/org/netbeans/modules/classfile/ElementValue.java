/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import java.io.DataInputStream;
import java.io.IOException;
import org.netbeans.modules.classfile.Annotation;
import org.netbeans.modules.classfile.ArrayElementValue;
import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.CPFieldInfo;
import org.netbeans.modules.classfile.ClassElementValue;
import org.netbeans.modules.classfile.ClassName;
import org.netbeans.modules.classfile.ConstantPool;
import org.netbeans.modules.classfile.EnumElementValue;
import org.netbeans.modules.classfile.NestedElementValue;
import org.netbeans.modules.classfile.PrimitiveElementValue;

public abstract class ElementValue {
    static ElementValue load(DataInputStream in, ConstantPool pool, boolean runtimeVisible) throws IOException {
        char tag = (char)in.readByte();
        switch (tag) {
            case 'e': {
                return ElementValue.loadEnumValue(in, pool);
            }
            case 'c': {
                int classType = in.readUnsignedShort();
                return new ClassElementValue(pool, classType);
            }
            case '@': {
                Annotation value = Annotation.loadAnnotation(in, pool, runtimeVisible);
                return new NestedElementValue(pool, value);
            }
            case '[': {
                ElementValue[] values = new ElementValue[in.readUnsignedShort()];
                for (int i = 0; i < values.length; ++i) {
                    values[i] = ElementValue.load(in, pool, runtimeVisible);
                }
                return new ArrayElementValue(pool, values);
            }
        }
        assert ("BCDFIJSZs".indexOf(tag) >= 0);
        return new PrimitiveElementValue(pool, in.readUnsignedShort());
    }

    private static ElementValue loadEnumValue(DataInputStream in, ConstantPool pool) throws IOException {
        int type = in.readUnsignedShort();
        CPEntry cpe = pool.get(type);
        if (cpe.getTag() == 9) {
            CPFieldInfo fe = (CPFieldInfo)cpe;
            String enumType = fe.getClassName().getInternalName();
            String enumName = fe.getFieldName();
            return new EnumElementValue(enumType, enumName);
        }
        int name = in.readUnsignedShort();
        return new EnumElementValue(pool, type, name);
    }

    ElementValue() {
    }
}

