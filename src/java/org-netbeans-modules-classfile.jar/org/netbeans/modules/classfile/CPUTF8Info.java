/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.CPName;
import org.netbeans.modules.classfile.ConstantPool;
import org.netbeans.modules.classfile.ConstantPoolReader;

public final class CPUTF8Info
extends CPName {
    String name;
    byte[] utf;

    CPUTF8Info(ConstantPool pool, String name) {
        super(pool);
        this.name = name;
    }

    CPUTF8Info(ConstantPool pool, byte[] utf) {
        super(pool);
        this.utf = utf;
    }

    @Override
    public final String getName() {
        if (this.name == null) {
            this.name = ConstantPoolReader.readUTF(this.utf, this.utf.length);
            this.utf = null;
        }
        return this.name;
    }

    @Override
    public final Object getValue() {
        return this.getName();
    }

    @Override
    public final int getTag() {
        return 1;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + ": name=" + this.getName();
    }
}

