/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.ConstantPool;

public abstract class CPEntry {
    ConstantPool pool;
    Object value;

    CPEntry(ConstantPool pool) {
        this.pool = pool;
    }

    void resolve(CPEntry[] pool) {
    }

    boolean usesTwoSlots() {
        return false;
    }

    public Object getValue() {
        return this.value;
    }

    public abstract int getTag();
}

