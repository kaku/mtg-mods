/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.CPFieldMethodInfo;
import org.netbeans.modules.classfile.ConstantPool;

public final class CPFieldInfo
extends CPFieldMethodInfo {
    CPFieldInfo(ConstantPool pool, int iClass, int iNameAndType) {
        super(pool, iClass, iNameAndType);
    }

    @Override
    public final int getTag() {
        return 9;
    }
}

