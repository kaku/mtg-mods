/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

public class Access {
    public static final int PUBLIC = 1;
    public static final int PRIVATE = 2;
    public static final int PROTECTED = 4;
    public static final int STATIC = 8;
    public static final int FINAL = 16;
    public static final int SYNCHRONIZED = 32;
    public static final int SUPER = 32;
    public static final int VOLATILE = 64;
    public static final int BRIDGE = 64;
    public static final int TRANSIENT = 128;
    public static final int VARARGS = 128;
    public static final int NATIVE = 256;
    public static final int INTERFACE = 512;
    public static final int ABSTRACT = 1024;
    public static final int STRICT = 2048;
    public static final int SYNTHETIC = 4096;
    public static final int ANNOTATION = 8192;
    public static final int ENUM = 16384;

    public static String toString(int access) {
        StringBuffer sb = new StringBuffer();
        if ((access & 1) == 1) {
            sb.append("public ");
        }
        if ((access & 2) == 2) {
            sb.append("private ");
        }
        if ((access & 4) == 4) {
            sb.append("protected ");
        }
        if ((access & 7) == 0) {
            sb.append("package private ");
        }
        if ((access & 8) == 8) {
            sb.append("static ");
        }
        if ((access & 16) == 16) {
            sb.append("final ");
        }
        if ((access & 32) == 32) {
            sb.append("synchronized ");
        }
        if ((access & 64) == 64) {
            sb.append("volatile ");
        }
        if ((access & 128) == 128) {
            sb.append("transient ");
        }
        if ((access & 256) == 256) {
            sb.append("native ");
        }
        if ((access & 1024) == 1024) {
            sb.append("abstract ");
        }
        if ((access & 2048) == 2048) {
            sb.append("strict ");
        }
        return sb.substring(0, sb.length() - 1);
    }

    public static boolean isStatic(int access) {
        return (access & 8) == 8;
    }

    public static final boolean isPublic(int access) {
        return (access & 1) == 1;
    }

    public static final boolean isProtected(int access) {
        return (access & 4) == 4;
    }

    public static final boolean isPackagePrivate(int access) {
        return (access & 7) == 0;
    }

    public static final boolean isPrivate(int access) {
        return (access & 2) == 2;
    }

    private Access() {
    }
}

