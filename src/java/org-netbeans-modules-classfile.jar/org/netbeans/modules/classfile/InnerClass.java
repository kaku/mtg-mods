/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import java.io.DataInputStream;
import java.io.IOException;
import org.netbeans.modules.classfile.CPClassInfo;
import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.CPUTF8Info;
import org.netbeans.modules.classfile.ClassName;
import org.netbeans.modules.classfile.ConstantPool;

public final class InnerClass {
    ClassName name;
    ClassName outerClassName;
    String simpleName;
    int access;

    static InnerClass[] loadInnerClasses(DataInputStream in, ConstantPool pool) throws IOException {
        int n = in.readUnsignedShort();
        InnerClass[] innerClasses = new InnerClass[n];
        for (int i = 0; i < n; ++i) {
            innerClasses[i] = new InnerClass(in, pool);
        }
        return innerClasses;
    }

    InnerClass(DataInputStream in, ConstantPool pool) throws IOException {
        this.loadInnerClass(in, pool);
    }

    private void loadInnerClass(DataInputStream in, ConstantPool pool) throws IOException {
        int index = in.readUnsignedShort();
        this.name = index > 0 ? pool.getClass(index).getClassName() : null;
        index = in.readUnsignedShort();
        this.outerClassName = index > 0 ? pool.getClass(index).getClassName() : null;
        index = in.readUnsignedShort();
        if (index > 0) {
            CPUTF8Info entry = (CPUTF8Info)pool.get(index);
            this.simpleName = entry.getName();
        }
        this.access = in.readUnsignedShort();
    }

    public final ClassName getName() {
        return this.name;
    }

    public final ClassName getOuterClassName() {
        return this.outerClassName;
    }

    public final String getSimpleName() {
        return this.simpleName;
    }

    public final int getAccess() {
        return this.access;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("innerclass=");
        sb.append(this.name);
        if (this.simpleName != null) {
            sb.append(" (");
            sb.append(this.simpleName);
            sb.append(')');
        }
        sb.append(", outerclass=");
        sb.append(this.outerClassName);
        return sb.toString();
    }
}

