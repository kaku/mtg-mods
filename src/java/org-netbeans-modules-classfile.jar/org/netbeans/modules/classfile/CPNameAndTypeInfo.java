/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.CPName;
import org.netbeans.modules.classfile.ConstantPool;

public class CPNameAndTypeInfo
extends CPEntry {
    int iName;
    int iDesc;

    CPNameAndTypeInfo(ConstantPool pool, int iName, int iDesc) {
        super(pool);
        this.iName = iName;
        this.iDesc = iDesc;
    }

    protected CPNameAndTypeInfo(ConstantPool pool) {
        super(pool);
        this.iName = -1;
        this.iDesc = -1;
    }

    public final String getName() {
        return ((CPName)this.pool.cpEntries[this.iName]).getName();
    }

    void setNameIndex(int index) {
        this.iName = index;
    }

    public final String getDescriptor() {
        return ((CPName)this.pool.cpEntries[this.iDesc]).getName();
    }

    void setDescriptorIndex(int index) {
        this.iDesc = index;
    }

    @Override
    public int getTag() {
        return 12;
    }

    public String toString() {
        return this.getClass().getName() + ": name=" + this.getName() + ", descriptor=" + this.getDescriptor();
    }
}

