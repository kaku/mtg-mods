/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.ConstantPool;
import org.netbeans.modules.classfile.ElementValue;

public final class ArrayElementValue
extends ElementValue {
    ElementValue[] values;

    ArrayElementValue(ConstantPool pool, ElementValue[] values) {
        this.values = values;
    }

    public ElementValue[] getValues() {
        return (ElementValue[])this.values.clone();
    }

    public String toString() {
        StringBuffer sb = new StringBuffer("[");
        int n = this.values.length;
        for (int i = 0; i < n; ++i) {
            sb.append(this.values[i]);
            if (i + 1 >= n) continue;
            sb.append(',');
        }
        sb.append(']');
        return sb.toString();
    }
}

