/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import java.io.DataInputStream;
import java.io.IOException;
import org.netbeans.modules.classfile.ConstantPool;

public final class BootstrapMethod {
    int methodRef;
    int[] arguments;

    static BootstrapMethod[] loadBootstrapMethod(DataInputStream in, ConstantPool pool) throws IOException {
        int n = in.readUnsignedShort();
        BootstrapMethod[] innerClasses = new BootstrapMethod[n];
        for (int i = 0; i < n; ++i) {
            innerClasses[i] = new BootstrapMethod(in, pool);
        }
        return innerClasses;
    }

    BootstrapMethod(DataInputStream in, ConstantPool pool) throws IOException {
        this.methodRef = in.readUnsignedShort();
        int args = in.readUnsignedShort();
        this.arguments = new int[args];
        for (int i = 0; i < args; ++i) {
            this.arguments[i] = in.readUnsignedShort();
        }
    }

    public int getMethodRef() {
        return this.methodRef;
    }

    public int[] getArguments() {
        return (int[])this.arguments.clone();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("bootstrapmethod=");
        sb.append(this.methodRef);
        sb.append("(");
        for (int i = 0; i < this.arguments.length; ++i) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(this.arguments[i]);
        }
        sb.append(")");
        return sb.toString();
    }
}

