/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import org.netbeans.modules.classfile.Annotation;
import org.netbeans.modules.classfile.AttributeMap;
import org.netbeans.modules.classfile.CPFieldMethodInfo;
import org.netbeans.modules.classfile.ClassFile;
import org.netbeans.modules.classfile.Code;
import org.netbeans.modules.classfile.ConstantPool;
import org.netbeans.modules.classfile.Field;
import org.netbeans.modules.classfile.InvalidClassFileAttributeException;
import org.netbeans.modules.classfile.LocalVariableTableEntry;
import org.netbeans.modules.classfile.Method;

public final class Parameter
extends Field {
    static Parameter[] makeParams(Method method) {
        ArrayList paramList = new ArrayList();
        ParamIterator it = new ParamIterator(method);
        while (it.hasNext()) {
            paramList.add(it.next());
        }
        return paramList.toArray(new Parameter[paramList.size()]);
    }

    private static Parameter createParameter(String name, String type, ClassFile classFile, DataInputStream visibleAnnotations, DataInputStream invisibleAnnotations) {
        return new Parameter(name, type, classFile, visibleAnnotations, invisibleAnnotations);
    }

    private Parameter(String name, String type, ClassFile classFile, DataInputStream visibleAnnotations, DataInputStream invisibleAnnotations) {
        super(name, type, classFile);
        this.loadParameterAnnotations(visibleAnnotations, invisibleAnnotations);
    }

    private void loadParameterAnnotations(DataInputStream visible, DataInputStream invisible) {
        super.loadAnnotations();
        if (this.annotations == null && (visible != null || invisible != null)) {
            this.annotations = new HashMap(2);
        }
        try {
            if (visible != null && visible.available() > 0) {
                Annotation.load(visible, this.classFile.getConstantPool(), true, this.annotations);
            }
        }
        catch (IOException e) {
            throw new InvalidClassFileAttributeException("invalid RuntimeVisibleParameterAnnotations attribute", e);
        }
        try {
            if (invisible != null && invisible.available() > 0) {
                Annotation.load(invisible, this.classFile.getConstantPool(), false, this.annotations);
            }
        }
        catch (IOException e) {
            throw new InvalidClassFileAttributeException("invalid RuntimeInvisibleParameterAnnotations attribute", e);
        }
    }

    @Override
    public final String getDeclaration() {
        StringBuffer sb = new StringBuffer();
        sb.append(CPFieldMethodInfo.getSignature(this.getDescriptor(), false));
        String name = this.getName();
        if (name != null) {
            sb.append(' ');
            sb.append(name);
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("name=");
        sb.append(this.getName());
        sb.append(" type=");
        sb.append(this.getDescriptor());
        if (this.getTypeSignature() != null) {
            sb.append(", signature=");
            sb.append(this.typeSignature);
        }
        this.loadAnnotations();
        if (this.annotations.size() > 0) {
            Iterator iter = this.annotations.values().iterator();
            sb.append(", annotations={ ");
            while (iter.hasNext()) {
                sb.append(((Annotation)iter.next()).toString());
                if (!iter.hasNext()) continue;
                sb.append(", ");
            }
            sb.append(" }");
        }
        return sb.toString();
    }

    private static class ParamIterator
    implements Iterator<Parameter> {
        ClassFile classFile;
        String signature;
        LocalVariableTableEntry[] localVars;
        int ivar;
        int isig;
        DataInputStream visibleAnnotations;
        DataInputStream invisibleAnnotations;

        ParamIterator(Method method) {
            this.classFile = method.getClassFile();
            this.signature = method.getDescriptor();
            assert (this.signature.charAt(0) == '(');
            this.isig = 1;
            this.ivar = method.isStatic() ? 0 : 1;
            Code code = method.getCode();
            this.localVars = code != null ? code.getLocalVariableTable() : new LocalVariableTableEntry[]{};
            AttributeMap attrs = method.getAttributes();
            try {
                this.visibleAnnotations = this.getParamAttr(attrs, "RuntimeVisibleParameterAnnotations");
            }
            catch (IOException e) {
                throw new InvalidClassFileAttributeException("invalid RuntimeVisibleParameterAnnotations attribute", e);
            }
            try {
                this.invisibleAnnotations = this.getParamAttr(attrs, "RuntimeInvisibleParameterAnnotations");
            }
            catch (IOException e) {
                throw new InvalidClassFileAttributeException("invalid RuntimeInvisibleParameterAnnotations attribute", e);
            }
        }

        private DataInputStream getParamAttr(AttributeMap attrs, String name) throws IOException {
            DataInputStream in = attrs.getStream(name);
            if (in != null) {
                in.readByte();
            }
            return in;
        }

        @Override
        public boolean hasNext() {
            return this.signature.charAt(this.isig) != ')';
        }

        @Override
        public Parameter next() {
            if (this.hasNext()) {
                String name = "";
                for (int i = 0; i < this.localVars.length; ++i) {
                    LocalVariableTableEntry lvte = this.localVars[i];
                    if (lvte.index != this.ivar || lvte.startPC != 0) continue;
                    name = this.localVars[i].getName();
                    break;
                }
                ++this.ivar;
                int sigStart = this.isig;
                while (this.isig < this.signature.length()) {
                    char ch = this.signature.charAt(this.isig);
                    switch (ch) {
                        case '[': {
                            ++this.isig;
                            break;
                        }
                        case 'B': 
                        case 'C': 
                        case 'F': 
                        case 'I': 
                        case 'S': 
                        case 'V': 
                        case 'Z': {
                            String type = this.signature.substring(sigStart, ++this.isig);
                            return Parameter.createParameter(name, type, this.classFile, this.visibleAnnotations, this.invisibleAnnotations);
                        }
                        case 'D': 
                        case 'J': {
                            ++this.ivar;
                            String type = this.signature.substring(sigStart, ++this.isig);
                            return Parameter.createParameter(name, type, this.classFile, this.visibleAnnotations, this.invisibleAnnotations);
                        }
                        case 'L': {
                            int end = this.signature.indexOf(59, this.isig) + 1;
                            String type = this.signature.substring(this.isig, end);
                            this.isig = end;
                            return Parameter.createParameter(name, type, this.classFile, this.visibleAnnotations, this.invisibleAnnotations);
                        }
                    }
                }
            }
            throw new NoSuchElementException();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

}

