/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import java.io.DataInputStream;
import java.io.IOException;
import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.ConstantPool;
import org.netbeans.modules.classfile.InvalidClassFormatException;

public abstract class VerificationTypeInfo {
    private int tag;
    public static final int ITEM_Top = 0;
    public static final int ITEM_Integer = 1;
    public static final int ITEM_Float = 2;
    public static final int ITEM_Double = 3;
    public static final int ITEM_Long = 4;
    public static final int ITEM_Null = 5;
    public static final int ITEM_UninitializedThis = 6;
    public static final int ITEM_Object = 7;
    public static final int ITEM_Uninitialized = 8;

    static VerificationTypeInfo loadVerificationTypeInfo(DataInputStream in, ConstantPool pool) throws IOException {
        int tag = in.readUnsignedByte();
        switch (tag) {
            case 0: {
                return new TopVariableInfo();
            }
            case 1: {
                return new IntegerVariableInfo();
            }
            case 2: {
                return new FloatVariableInfo();
            }
            case 4: {
                return new LongVariableInfo();
            }
            case 3: {
                return new DoubleVariableInfo();
            }
            case 5: {
                return new NullVariableInfo();
            }
            case 6: {
                return new UninitializedThisVariableInfo();
            }
            case 7: {
                int cpool_index = in.readUnsignedShort();
                return new ObjectVariableInfo(pool.get(cpool_index));
            }
            case 8: {
                int offset = in.readUnsignedShort();
                return new UninitializedVariableInfo(offset);
            }
        }
        throw new InvalidClassFormatException("invalid verification_type_info tag: " + tag);
    }

    VerificationTypeInfo(int tag) {
        this.tag = tag;
    }

    public int getTag() {
        return this.tag;
    }

    public static final class UninitializedVariableInfo
    extends VerificationTypeInfo {
        int offset;

        UninitializedVariableInfo(int offset) {
            super(7);
            this.offset = offset;
        }

        public int getOffset() {
            return this.offset;
        }
    }

    public static final class ObjectVariableInfo
    extends VerificationTypeInfo {
        CPEntry cpEntry;

        ObjectVariableInfo(CPEntry entry) {
            super(7);
            this.cpEntry = entry;
        }

        public CPEntry getConstantPoolEntry() {
            return this.cpEntry;
        }
    }

    public static final class UninitializedThisVariableInfo
    extends VerificationTypeInfo {
        UninitializedThisVariableInfo() {
            super(6);
        }
    }

    public static final class NullVariableInfo
    extends VerificationTypeInfo {
        NullVariableInfo() {
            super(5);
        }
    }

    public static final class DoubleVariableInfo
    extends VerificationTypeInfo {
        DoubleVariableInfo() {
            super(3);
        }
    }

    public static final class LongVariableInfo
    extends VerificationTypeInfo {
        LongVariableInfo() {
            super(4);
        }
    }

    public static final class FloatVariableInfo
    extends VerificationTypeInfo {
        FloatVariableInfo() {
            super(2);
        }
    }

    public static final class IntegerVariableInfo
    extends VerificationTypeInfo {
        IntegerVariableInfo() {
            super(1);
        }
    }

    public static final class TopVariableInfo
    extends VerificationTypeInfo {
        TopVariableInfo() {
            super(0);
        }
    }

}

