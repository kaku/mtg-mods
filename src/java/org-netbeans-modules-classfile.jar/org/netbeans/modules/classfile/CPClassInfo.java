/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.CPName;
import org.netbeans.modules.classfile.ClassName;
import org.netbeans.modules.classfile.ConstantPool;

public final class CPClassInfo
extends CPName {
    CPClassInfo(ConstantPool pool, int index) {
        super(pool, index);
    }

    public ClassName getClassName() {
        String name = super.getName();
        return ClassName.getClassName(name);
    }

    @Override
    public final int getTag() {
        return 7;
    }

    @Override
    public String toString() {
        return this.getClassName().toString();
    }
}

