/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

public final class InvalidClassFileAttributeException
extends RuntimeException {
    private static final long serialVersionUID = -2988920220798200016L;

    InvalidClassFileAttributeException(String message, Throwable cause) {
        super(message, cause);
    }
}

