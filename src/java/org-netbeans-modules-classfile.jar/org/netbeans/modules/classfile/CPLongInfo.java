/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.ConstantPool;

public final class CPLongInfo
extends CPEntry {
    CPLongInfo(ConstantPool pool, long v) {
        super(pool);
        this.value = new Long(v);
    }

    @Override
    boolean usesTwoSlots() {
        return true;
    }

    @Override
    public final int getTag() {
        return 5;
    }

    public String toString() {
        return this.getClass().getName() + ": value=" + this.value;
    }
}

