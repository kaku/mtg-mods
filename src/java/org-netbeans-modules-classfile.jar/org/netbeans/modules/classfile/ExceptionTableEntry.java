/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import java.io.DataInputStream;
import java.io.IOException;
import org.netbeans.modules.classfile.CPClassInfo;
import org.netbeans.modules.classfile.ConstantPool;
import org.netbeans.modules.classfile.InvalidClassFileAttributeException;

public final class ExceptionTableEntry {
    int startPC;
    int endPC;
    int handlerPC;
    CPClassInfo catchType;

    static ExceptionTableEntry[] loadExceptionTable(DataInputStream in, ConstantPool pool) throws IOException {
        int n = in.readUnsignedShort();
        ExceptionTableEntry[] exceptions = new ExceptionTableEntry[n];
        for (int i = 0; i < n; ++i) {
            exceptions[i] = new ExceptionTableEntry(in, pool);
        }
        return exceptions;
    }

    ExceptionTableEntry(DataInputStream in, ConstantPool pool) throws IOException {
        this.loadExceptionEntry(in, pool);
    }

    private void loadExceptionEntry(DataInputStream in, ConstantPool pool) throws IOException {
        this.startPC = in.readUnsignedShort();
        this.endPC = in.readUnsignedShort();
        this.handlerPC = in.readUnsignedShort();
        int typeIndex = in.readUnsignedShort();
        if (typeIndex != 0) {
            try {
                this.catchType = pool.getClass(typeIndex);
            }
            catch (IndexOutOfBoundsException e) {
                throw new InvalidClassFileAttributeException("invalid catchType (" + typeIndex + ") in exception table entry", e);
            }
        }
    }

    public final int getStartPC() {
        return this.startPC;
    }

    public final int getEndPC() {
        return this.endPC;
    }

    public final int getHandlerPC() {
        return this.handlerPC;
    }

    public final CPClassInfo getCatchType() {
        return this.catchType;
    }
}

