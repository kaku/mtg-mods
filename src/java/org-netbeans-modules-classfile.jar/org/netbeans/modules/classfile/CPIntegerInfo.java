/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.ConstantPool;

public final class CPIntegerInfo
extends CPEntry {
    CPIntegerInfo(ConstantPool pool, int v) {
        super(pool);
        this.value = new Integer(v);
    }

    @Override
    public final int getTag() {
        return 3;
    }

    public String toString() {
        return this.getClass().getName() + ": value=" + this.value;
    }
}

