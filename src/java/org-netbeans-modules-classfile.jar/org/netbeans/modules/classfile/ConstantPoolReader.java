/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import java.io.DataInput;
import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;

public final class ConstantPoolReader
extends FilterInputStream
implements DataInput {
    private char[] lineBuffer;
    static char[] str = new char[1024];
    byte[] bytearr = new byte[1024];

    public ConstantPoolReader(InputStream in) {
        super(in);
    }

    @Override
    public void readFully(byte[] b) throws IOException {
        this.readFully(b, 0, b.length);
    }

    @Override
    public void readFully(byte[] b, int off, int len) throws IOException {
        int count;
        InputStream input = this.in;
        for (int n = 0; n < len; n += count) {
            count = input.read(b, off + n, len - n);
            if (count >= 0) continue;
            throw new EOFException();
        }
    }

    @Override
    public int skipBytes(int n) throws IOException {
        int total;
        InputStream input = this.in;
        int cur = 0;
        for (total = 0; total < n && (cur = (int)input.skip(n - total)) > 0; total += cur) {
        }
        return total;
    }

    @Override
    public boolean readBoolean() throws IOException {
        int ch = this.in.read();
        if (ch < 0) {
            throw new EOFException();
        }
        return ch != 0;
    }

    @Override
    public byte readByte() throws IOException {
        int ch = this.in.read();
        if (ch < 0) {
            throw new EOFException();
        }
        return (byte)ch;
    }

    @Override
    public int readUnsignedByte() throws IOException {
        int ch = this.in.read();
        if (ch < 0) {
            throw new EOFException();
        }
        return ch;
    }

    @Override
    public short readShort() throws IOException {
        int ch2;
        InputStream input = this.in;
        int ch1 = input.read();
        if ((ch1 | (ch2 = input.read())) < 0) {
            throw new EOFException();
        }
        return (short)((ch1 << 8) + ch2);
    }

    @Override
    public int readUnsignedShort() throws IOException {
        int ch2;
        InputStream input = this.in;
        int ch1 = input.read();
        if ((ch1 | (ch2 = input.read())) < 0) {
            throw new EOFException();
        }
        return (ch1 << 8) + ch2;
    }

    @Override
    public char readChar() throws IOException {
        return (char)this.readUnsignedShort();
    }

    @Override
    public int readInt() throws IOException {
        int ch4;
        int ch2;
        int ch3;
        InputStream input = this.in;
        int ch1 = input.read();
        if ((ch1 | (ch2 = input.read()) | (ch3 = input.read()) | (ch4 = input.read())) < 0) {
            throw new EOFException();
        }
        return (ch1 << 24) + (ch2 << 16) + (ch3 << 8) + ch4;
    }

    @Override
    public long readLong() throws IOException {
        return ((long)this.readInt() << 32) + ((long)this.readInt() & 0xFFFFFFFFL);
    }

    @Override
    public float readFloat() throws IOException {
        return Float.intBitsToFloat(this.readInt());
    }

    @Override
    public double readDouble() throws IOException {
        return Double.longBitsToDouble(this.readLong());
    }

    @Override
    public String readLine() throws IOException {
        int c;
        InputStream in = this.in;
        char[] buf = this.lineBuffer;
        if (buf == null) {
            buf = this.lineBuffer = new char[128];
        }
        int room = buf.length;
        int offset = 0;
        block4 : do {
            c = in.read();
            switch (c) {
                case -1: 
                case 10: {
                    break block4;
                }
                case 13: {
                    int c2 = in.read();
                    if (c2 == 10 || c2 == -1) break block4;
                    if (!(in instanceof PushbackInputStream)) {
                        in = this.in = new PushbackInputStream(in);
                    }
                    ((PushbackInputStream)in).unread(c2);
                    break block4;
                }
                default: {
                    if (--room < 0) {
                        buf = new char[offset + 128];
                        room = buf.length - offset - 1;
                        System.arraycopy(this.lineBuffer, 0, buf, 0, offset);
                        this.lineBuffer = buf;
                    }
                    buf[offset++] = (char)c;
                    continue block4;
                }
            }
            break;
        } while (true);
        if (c == -1 && offset == 0) {
            return null;
        }
        return String.copyValueOf(buf, 0, offset);
    }

    @Override
    public String readUTF() throws IOException {
        int utflen = this.readUnsignedShort();
        if (utflen > this.bytearr.length) {
            this.bytearr = new byte[utflen];
        }
        this.readFully(this.bytearr, 0, utflen);
        return ConstantPoolReader.readUTF(this.bytearr, utflen);
    }

    byte[] readRawUTF() throws IOException {
        int utflen = this.readUnsignedShort();
        byte[] buf = new byte[utflen];
        this.readFully(buf, 0, utflen);
        return buf;
    }

    static synchronized String readUTF(byte[] src, int utflen) {
        int i = 0;
        int strlen = 0;
        if (utflen > str.length) {
            str = new char[utflen];
        }
        while (i < utflen) {
            int b;
            if ((b = src[i++] & 255) >= 224) {
                b = (b & 15) << 12;
                b |= (src[i++] & 63) << 6;
                b |= src[i++] & 63;
            } else if (b >= 192) {
                b = (b & 31) << 6;
                b |= src[i++] & 63;
            }
            ConstantPoolReader.str[strlen++] = (char)b;
        }
        return new String(str, 0, strlen);
    }
}

