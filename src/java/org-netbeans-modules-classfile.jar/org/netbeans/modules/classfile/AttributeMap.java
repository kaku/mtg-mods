/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.CPUTF8Info;
import org.netbeans.modules.classfile.ConstantPool;
import org.netbeans.modules.classfile.InvalidClassFormatException;

public final class AttributeMap {
    Map<String, byte[]> map;

    static AttributeMap load(DataInputStream in, ConstantPool pool) throws IOException {
        return AttributeMap.load(in, pool, false);
    }

    static AttributeMap load(DataInputStream in, ConstantPool pool, boolean includeCode) throws IOException {
        int count = in.readUnsignedShort();
        HashMap<String, byte[]> map = new HashMap<String, byte[]>(count + 1, 1.0f);
        for (int i = 0; i < count; ++i) {
            int len;
            CPEntry o = pool.get(in.readUnsignedShort());
            if (!(o instanceof CPUTF8Info)) {
                throw new InvalidClassFormatException();
            }
            CPUTF8Info entry = (CPUTF8Info)o;
            String name = entry.getName();
            if (!includeCode && "Code".equals(name)) {
                int n;
                for (len = in.readInt(); (n = (int)in.skip(len)) > 0 && n < len; len -= n) {
                }
                continue;
            }
            byte[] attr = new byte[len];
            in.readFully(attr);
            map.put(name, attr);
        }
        return new AttributeMap(map);
    }

    AttributeMap(Map<String, byte[]> attributes) {
        this.map = attributes;
    }

    DataInputStream getStream(String name) {
        byte[] attr = this.map.get(name);
        return attr != null ? new DataInputStream(new ByteArrayInputStream(attr)) : null;
    }

    byte[] get(String name) {
        return this.map.get(name);
    }

    public int size() {
        return this.map.size();
    }

    public boolean isEmpty() {
        return this.map.isEmpty();
    }

    public boolean containsAttribute(String key) {
        return this.map.containsKey(key);
    }

    public Set<String> keySet() {
        return this.map.keySet();
    }
}

