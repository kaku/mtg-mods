/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.CPClassInfo;
import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.CPNameAndTypeInfo;
import org.netbeans.modules.classfile.ClassName;
import org.netbeans.modules.classfile.ConstantPool;

public final class EnclosingMethod {
    final CPClassInfo classInfo;
    final CPNameAndTypeInfo methodInfo;

    EnclosingMethod(ConstantPool pool, CPClassInfo classInfo, int iMethod) {
        this.classInfo = classInfo;
        this.methodInfo = iMethod > 0 ? (CPNameAndTypeInfo)pool.get(iMethod) : null;
    }

    public ClassName getClassName() {
        return this.classInfo.getClassName();
    }

    public CPClassInfo getClassInfo() {
        return this.classInfo;
    }

    public boolean hasMethod() {
        return this.methodInfo != null;
    }

    public CPNameAndTypeInfo getMethodInfo() {
        return this.methodInfo;
    }

    public String toString() {
        String methodString = this.methodInfo != null ? this.methodInfo.toString() : "<no method>";
        return "enclosing method: class=" + this.getClassName() + ", method=" + methodString;
    }
}

