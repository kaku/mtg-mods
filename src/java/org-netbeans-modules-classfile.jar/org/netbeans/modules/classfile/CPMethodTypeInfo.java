/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.ConstantPool;

public class CPMethodTypeInfo
extends CPEntry {
    int iDescriptor;

    CPMethodTypeInfo(ConstantPool pool, int iDescriptor) {
        super(pool);
        this.iDescriptor = iDescriptor;
    }

    @Override
    public int getTag() {
        return 16;
    }

    public int getDescriptor() {
        return this.iDescriptor;
    }

    public String toString() {
        return this.getClass().getName() + ": descriptor=" + this.iDescriptor;
    }
}

