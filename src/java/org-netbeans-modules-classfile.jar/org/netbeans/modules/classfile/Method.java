/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import org.netbeans.modules.classfile.AttributeMap;
import org.netbeans.modules.classfile.CPClassInfo;
import org.netbeans.modules.classfile.CPFieldMethodInfo;
import org.netbeans.modules.classfile.CPMethodInfo;
import org.netbeans.modules.classfile.ClassFile;
import org.netbeans.modules.classfile.Code;
import org.netbeans.modules.classfile.ConstantPool;
import org.netbeans.modules.classfile.ElementValue;
import org.netbeans.modules.classfile.Field;
import org.netbeans.modules.classfile.InvalidClassFileAttributeException;
import org.netbeans.modules.classfile.Parameter;

public final class Method
extends Field {
    private Code code;
    private CPClassInfo[] exceptions;
    private Parameter[] parameters;
    private ElementValue annotationDefault = notloadedAnnotationDefault;
    private static final ElementValue notloadedAnnotationDefault = new ElementValue(){};

    static Method[] loadMethods(DataInputStream in, ConstantPool pool, ClassFile cls, boolean includeCode) throws IOException {
        int count = in.readUnsignedShort();
        Method[] methods = new Method[count];
        for (int i = 0; i < count; ++i) {
            methods[i] = new Method(in, pool, cls, includeCode);
        }
        return methods;
    }

    Method(DataInputStream in, ConstantPool pool, ClassFile cls, boolean includeCode) throws IOException {
        super(in, pool, cls, includeCode);
    }

    public final Code getCode() {
        DataInputStream in;
        if (this.code == null && (in = this.attributes.getStream("Code")) != null) {
            try {
                this.code = new Code(in, this.classFile.constantPool);
                in.close();
            }
            catch (IOException e) {
                throw new InvalidClassFileAttributeException("invalid Code attribute", e);
            }
        }
        return this.code;
    }

    public final CPClassInfo[] getExceptionClasses() {
        if (this.exceptions == null) {
            DataInputStream in = this.attributes.getStream("Exceptions");
            if (in != null) {
                try {
                    this.exceptions = ClassFile.getCPClassList(in, this.classFile.constantPool);
                    in.close();
                }
                catch (IOException e) {
                    throw new InvalidClassFileAttributeException("invalid Exceptions attribute", e);
                }
            }
            if (this.exceptions == null) {
                this.exceptions = new CPClassInfo[0];
            }
        }
        return (CPClassInfo[])this.exceptions.clone();
    }

    public final boolean isBridge() {
        return (this.access & 64) == 64;
    }

    public final boolean isVarArgs() {
        return (this.access & 128) == 128;
    }

    public final boolean isSynchronized() {
        return (this.access & 32) == 32;
    }

    public final boolean isNative() {
        return (this.access & 256) == 256;
    }

    public final boolean isAbstract() {
        return (this.access & 1024) == 1024;
    }

    public final List<Parameter> getParameters() {
        if (this.parameters == null) {
            this.parameters = Parameter.makeParams(this);
        }
        return Arrays.asList(this.parameters);
    }

    public final String getReturnType() {
        String desc = this.getDescriptor();
        int i = desc.indexOf(41) + 1;
        return desc.substring(i);
    }

    public final String getReturnSignature() {
        String type = this.getReturnType();
        return CPFieldMethodInfo.getSignature(type, true);
    }

    public ElementValue getAnnotationDefault() {
        if (this.annotationDefault == notloadedAnnotationDefault) {
            this.annotationDefault = null;
            DataInputStream in = this.attributes.getStream("AnnotationDefault");
            if (in != null) {
                try {
                    this.annotationDefault = ElementValue.load(in, this.classFile.constantPool, false);
                    in.close();
                }
                catch (IOException e) {
                    throw new InvalidClassFileAttributeException("invalid AnnotationDefault attribute", e);
                }
            }
        }
        return this.annotationDefault;
    }

    @Override
    public String toString() {
        Code c;
        StringBuffer sb = new StringBuffer(super.toString());
        sb.append(", params (");
        this.getParameters();
        for (int i = 0; i < this.parameters.length; ++i) {
            sb.append(this.parameters[i].toString());
            if (i + 1 >= this.parameters.length) continue;
            sb.append(", ");
        }
        sb.append("), returns ");
        sb.append(this.getReturnSignature());
        CPClassInfo[] ec = this.getExceptionClasses();
        if (ec.length > 0) {
            sb.append(", throws");
            for (int i2 = 0; i2 < ec.length; ++i2) {
                sb.append(' ');
                sb.append(ec[i2].getName());
            }
        }
        if (this.getAnnotationDefault() != null) {
            sb.append(", default \"");
            sb.append(this.annotationDefault.toString());
            sb.append("\" ");
        }
        if ((c = this.getCode()) != null) {
            sb.append(' ');
            sb.append(c.toString());
        }
        return sb.toString();
    }

    @Override
    public final String getDeclaration() {
        return CPMethodInfo.getFullMethodName(this.getName(), this.getDescriptor());
    }

}

