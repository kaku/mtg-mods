/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.CPMethodInfo;
import org.netbeans.modules.classfile.ConstantPool;

public final class CPInterfaceMethodInfo
extends CPMethodInfo {
    CPInterfaceMethodInfo(ConstantPool pool, int iClass, int iNameAndType) {
        super(pool, iClass, iNameAndType);
    }

    @Override
    public final int getTag() {
        return 11;
    }
}

