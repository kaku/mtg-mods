/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.netbeans.modules.classfile.Access;
import org.netbeans.modules.classfile.Annotation;
import org.netbeans.modules.classfile.AttributeMap;
import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.CPUTF8Info;
import org.netbeans.modules.classfile.ClassFile;
import org.netbeans.modules.classfile.ClassName;
import org.netbeans.modules.classfile.ConstantPool;
import org.netbeans.modules.classfile.InvalidClassFileAttributeException;

public abstract class Field {
    private int iName;
    private int iType;
    private String _name;
    private String _type;
    int access;
    ClassFile classFile;
    Map<ClassName, Annotation> annotations;
    String typeSignature;
    AttributeMap attributes;

    Field(DataInputStream in, ConstantPool pool, ClassFile classFile, boolean includeCode) throws IOException {
        this.access = in.readUnsignedShort();
        this.iName = in.readUnsignedShort();
        this.iType = in.readUnsignedShort();
        this.classFile = classFile;
        this.attributes = AttributeMap.load(in, pool, includeCode);
    }

    Field(String name, String type, ClassFile classFile) {
        this.access = 0;
        this._name = name;
        this._type = type;
        this.classFile = classFile;
        this.attributes = new AttributeMap(new HashMap<String, byte[]>(1));
    }

    public final String getName() {
        if (this._name == null && this.iName != 0) {
            CPUTF8Info utfName = (CPUTF8Info)this.classFile.constantPool.get(this.iName);
            this._name = utfName.getName();
        }
        return this._name;
    }

    public final String getDescriptor() {
        if (this._type == null && this.iType != 0) {
            CPUTF8Info utfType = (CPUTF8Info)this.classFile.constantPool.get(this.iType);
            this._type = utfType.getName();
        }
        return this._type;
    }

    public abstract String getDeclaration();

    public final int getAccess() {
        return this.access;
    }

    public final boolean isStatic() {
        return Access.isStatic(this.access);
    }

    public final boolean isPublic() {
        return Access.isPublic(this.access);
    }

    public final boolean isProtected() {
        return Access.isProtected(this.access);
    }

    public final boolean isPackagePrivate() {
        return Access.isPackagePrivate(this.access);
    }

    public final boolean isPrivate() {
        return Access.isPrivate(this.access);
    }

    public final boolean isDeprecated() {
        return this.attributes.get("Deprecated") != null;
    }

    public final boolean isSynthetic() {
        return this.attributes.get("Synthetic") != null;
    }

    public final ClassFile getClassFile() {
        return this.classFile;
    }

    public String getTypeSignature() {
        DataInputStream in;
        if (this.typeSignature == null && (in = this.attributes.getStream("Signature")) != null) {
            try {
                int index = in.readUnsignedShort();
                CPUTF8Info entry = (CPUTF8Info)this.classFile.constantPool.get(index);
                this.typeSignature = entry.getName();
                in.close();
            }
            catch (IOException e) {
                throw new InvalidClassFileAttributeException("invalid Signature attribute", e);
            }
        }
        return this.typeSignature;
    }

    void setTypeSignature(String sig) {
        this.typeSignature = sig;
    }

    public final Collection<Annotation> getAnnotations() {
        this.loadAnnotations();
        return this.annotations.values();
    }

    public final Annotation getAnnotation(ClassName annotationClass) {
        this.loadAnnotations();
        return this.annotations.get(annotationClass);
    }

    public final boolean isAnnotationPresent(ClassName annotationClass) {
        this.loadAnnotations();
        return this.annotations.get(annotationClass) != null;
    }

    public final AttributeMap getAttributes() {
        return this.attributes;
    }

    void loadAnnotations() {
        if (this.annotations == null) {
            this.annotations = ClassFile.buildAnnotationMap(this.classFile.constantPool, this.attributes);
        }
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        String name = this.getName();
        if (name != null) {
            sb.append(this.getName());
            sb.append(' ');
        }
        if (this.isSynthetic()) {
            sb.append("(synthetic)");
        }
        if (this.isDeprecated()) {
            sb.append("(deprecated)");
        }
        sb.append("type=");
        sb.append(this.getDescriptor());
        if (this.getTypeSignature() != null) {
            sb.append(", signature=");
            sb.append(this.typeSignature);
        }
        sb.append(", access=");
        sb.append(Access.toString(this.access));
        this.loadAnnotations();
        if (this.annotations.size() > 0) {
            Iterator<Annotation> iter = this.annotations.values().iterator();
            sb.append(", annotations={ ");
            while (iter.hasNext()) {
                sb.append(iter.next().toString());
                if (!iter.hasNext()) continue;
                sb.append(", ");
            }
            sb.append(" }");
        }
        return sb.toString();
    }
}

