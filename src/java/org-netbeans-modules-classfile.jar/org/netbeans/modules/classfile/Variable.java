/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import java.io.DataInputStream;
import java.io.IOException;
import org.netbeans.modules.classfile.AttributeMap;
import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.CPFieldMethodInfo;
import org.netbeans.modules.classfile.ClassFile;
import org.netbeans.modules.classfile.ConstantPool;
import org.netbeans.modules.classfile.Field;
import org.netbeans.modules.classfile.InvalidClassFileAttributeException;

public final class Variable
extends Field {
    private Object constValue = notLoadedConstValue;
    private static final Object notLoadedConstValue = new Object();

    static Variable[] loadFields(DataInputStream in, ConstantPool pool, ClassFile cls) throws IOException {
        int count = in.readUnsignedShort();
        Variable[] variables = new Variable[count];
        for (int i = 0; i < count; ++i) {
            variables[i] = new Variable(in, pool, cls);
        }
        return variables;
    }

    Variable(DataInputStream in, ConstantPool pool, ClassFile cls) throws IOException {
        super(in, pool, cls, false);
    }

    public final boolean isConstant() {
        return this.attributes.get("ConstantValue") != null;
    }

    @Deprecated
    public final Object getValue() {
        return this.getConstantValue();
    }

    public final Object getConstantValue() {
        DataInputStream in;
        if (this.constValue == notLoadedConstValue && (in = this.attributes.getStream("ConstantValue")) != null) {
            try {
                int index = in.readUnsignedShort();
                CPEntry cpe = this.classFile.constantPool.get(index);
                this.constValue = cpe.getValue();
            }
            catch (IOException e) {
                throw new InvalidClassFileAttributeException("invalid ConstantValue attribute", e);
            }
        }
        return this.constValue;
    }

    @Override
    public final String getDeclaration() {
        StringBuffer sb = new StringBuffer();
        sb.append(CPFieldMethodInfo.getSignature(this.getDescriptor(), false));
        sb.append(' ');
        sb.append(this.getName());
        return sb.toString();
    }

    public final boolean isEnumConstant() {
        return (this.access & 16384) == 16384;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer(super.toString());
        if (this.isConstant()) {
            sb.append(", const value=");
            sb.append(this.getValue());
        }
        return sb.toString();
    }
}

