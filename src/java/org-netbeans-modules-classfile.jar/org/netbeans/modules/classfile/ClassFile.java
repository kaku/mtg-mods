/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.modules.classfile.Access;
import org.netbeans.modules.classfile.Annotation;
import org.netbeans.modules.classfile.AttributeMap;
import org.netbeans.modules.classfile.BootstrapMethod;
import org.netbeans.modules.classfile.CPClassInfo;
import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.CPUTF8Info;
import org.netbeans.modules.classfile.ClassName;
import org.netbeans.modules.classfile.ConstantPool;
import org.netbeans.modules.classfile.EnclosingMethod;
import org.netbeans.modules.classfile.InnerClass;
import org.netbeans.modules.classfile.InvalidClassFileAttributeException;
import org.netbeans.modules.classfile.InvalidClassFormatException;
import org.netbeans.modules.classfile.Method;
import org.netbeans.modules.classfile.Variable;

public class ClassFile {
    ConstantPool constantPool;
    int classAccess;
    CPClassInfo classInfo;
    CPClassInfo superClassInfo;
    CPClassInfo[] interfaces;
    Variable[] variables;
    Method[] methods;
    String sourceFileName;
    InnerClass[] innerClasses;
    BootstrapMethod[] bootstrapMethods;
    private AttributeMap attributes;
    private Map<ClassName, Annotation> annotations;
    short majorVersion;
    short minorVersion;
    String typeSignature;
    EnclosingMethod enclosingMethod;
    private boolean includeCode = false;
    private static final int BUFFER_SIZE = 4096;
    private static final Set<String> badNonJavaClassNames = new HashSet<String>(Arrays.asList(";", "[", "."));

    public ClassFile(InputStream classData) throws IOException {
        this(classData, true);
    }

    public ClassFile(String classFileName) throws IOException {
        this(classFileName, true);
    }

    public ClassFile(File file, boolean includeCode) throws IOException {
        InputStream is = null;
        this.includeCode = includeCode;
        if (file == null || !file.exists()) {
            throw new FileNotFoundException(file != null ? file.getPath() : "null");
        }
        try {
            is = new BufferedInputStream(new FileInputStream(file), 4096);
            this.load(is);
        }
        catch (InvalidClassFormatException e) {
            throw new InvalidClassFormatException(file.getPath() + '(' + e.getMessage() + ')');
        }
        finally {
            if (is != null) {
                is.close();
            }
        }
    }

    public ClassFile(InputStream classData, boolean includeCode) throws IOException {
        if (classData == null) {
            throw new IOException("input stream not specified");
        }
        this.includeCode = includeCode;
        try {
            this.load(classData);
        }
        catch (IndexOutOfBoundsException e) {
            throw new InvalidClassFormatException("invalid classfile format");
        }
    }

    public ClassFile(String classFileName, boolean includeCode) throws IOException {
        InputStream in = null;
        this.includeCode = includeCode;
        try {
            if (classFileName == null) {
                throw new IOException("input stream not specified");
            }
            in = new BufferedInputStream(new FileInputStream(classFileName), 4096);
            this.load(in);
        }
        catch (InvalidClassFormatException e) {
            throw new InvalidClassFormatException(classFileName + '(' + e.getMessage() + ')');
        }
        finally {
            if (in != null) {
                in.close();
            }
        }
    }

    public final ConstantPool getConstantPool() {
        return this.constantPool;
    }

    private void load(InputStream classData) throws IOException {
        try {
            DataInputStream in = new DataInputStream(classData);
            this.constantPool = this.loadClassHeader(in);
            this.interfaces = ClassFile.getCPClassList(in, this.constantPool);
            this.variables = Variable.loadFields(in, this.constantPool, this);
            this.methods = Method.loadMethods(in, this.constantPool, this, this.includeCode);
            this.attributes = AttributeMap.load(in, this.constantPool);
        }
        catch (IOException ioe) {
            throw new InvalidClassFormatException(ioe);
        }
        catch (ClassCastException cce) {
            throw new InvalidClassFormatException(cce);
        }
    }

    private ConstantPool loadClassHeader(DataInputStream in) throws IOException {
        int magic = in.readInt();
        if (magic != -889275714) {
            throw new InvalidClassFormatException();
        }
        this.minorVersion = in.readShort();
        this.majorVersion = in.readShort();
        int count = in.readUnsignedShort();
        ConstantPool pool = new ConstantPool(count, in);
        this.classAccess = in.readUnsignedShort();
        this.classInfo = pool.getClass(in.readUnsignedShort());
        if (this.classInfo == null) {
            throw new InvalidClassFormatException();
        }
        if (ClassFile.isBadNonJavaClassName(this.classInfo.getName())) {
            throw new InvalidClassFormatException(String.format("Invalid non java class name: %s", this.classInfo.getName()));
        }
        int index = in.readUnsignedShort();
        if (index != 0) {
            this.superClassInfo = pool.getClass(index);
        }
        return pool;
    }

    static CPClassInfo[] getCPClassList(DataInputStream in, ConstantPool pool) throws IOException {
        int count = in.readUnsignedShort();
        CPClassInfo[] classes = new CPClassInfo[count];
        for (int i = 0; i < count; ++i) {
            classes[i] = pool.getClass(in.readUnsignedShort());
        }
        return classes;
    }

    public final int getAccess() {
        return this.classAccess;
    }

    public final ClassName getName() {
        return this.classInfo.getClassName();
    }

    public final ClassName getSuperClass() {
        if (this.superClassInfo == null) {
            return null;
        }
        return this.superClassInfo.getClassName();
    }

    public final Collection<ClassName> getInterfaces() {
        ArrayList<ClassName> l = new ArrayList<ClassName>();
        int n = this.interfaces.length;
        for (int i = 0; i < n; ++i) {
            l.add(this.interfaces[i].getClassName());
        }
        return l;
    }

    public final Variable getVariable(String name) {
        for (Variable v : this.variables) {
            if (!v.getName().equals(name)) continue;
            return v;
        }
        return null;
    }

    public final Collection<Variable> getVariables() {
        return Arrays.asList(this.variables);
    }

    public final int getVariableCount() {
        return this.variables.length;
    }

    public final Method getMethod(String name, String signature) {
        for (Method m : this.methods) {
            if (!m.getName().equals(name) || !m.getDescriptor().equals(signature)) continue;
            return m;
        }
        return null;
    }

    public final Collection<Method> getMethods() {
        return Arrays.asList(this.methods);
    }

    public final int getMethodCount() {
        return this.methods.length;
    }

    public final String getSourceFileName() {
        DataInputStream in;
        if (this.sourceFileName == null && (in = this.attributes.getStream("SourceFile")) != null) {
            try {
                int ipool = in.readUnsignedShort();
                CPUTF8Info entry = (CPUTF8Info)this.constantPool.get(ipool);
                this.sourceFileName = entry.getName();
                in.close();
            }
            catch (IOException e) {
                throw new InvalidClassFileAttributeException("invalid SourceFile attribute", e);
            }
        }
        return this.sourceFileName;
    }

    public final boolean isDeprecated() {
        return this.attributes.get("Deprecated") != null;
    }

    public final boolean isSynthetic() {
        return (this.classAccess & 4096) == 4096 || this.attributes.get("Synthetic") != null;
    }

    public final boolean isAnnotation() {
        return (this.classAccess & 8192) == 8192;
    }

    public final boolean isEnum() {
        return (this.classAccess & 16384) == 16384;
    }

    public final AttributeMap getAttributes() {
        return this.attributes;
    }

    public final Collection<InnerClass> getInnerClasses() {
        if (this.innerClasses == null) {
            DataInputStream in = this.attributes.getStream("InnerClasses");
            if (in != null) {
                try {
                    this.innerClasses = InnerClass.loadInnerClasses(in, this.constantPool);
                    in.close();
                }
                catch (IOException e) {
                    throw new InvalidClassFileAttributeException("invalid InnerClasses attribute", e);
                }
            } else {
                this.innerClasses = new InnerClass[0];
            }
        }
        return Arrays.asList(this.innerClasses);
    }

    public final List<BootstrapMethod> getBootstrapMethods() {
        if (this.bootstrapMethods == null) {
            DataInputStream in = this.attributes.getStream("BootstrapMethods");
            if (in != null) {
                try {
                    this.bootstrapMethods = BootstrapMethod.loadBootstrapMethod(in, this.constantPool);
                    in.close();
                }
                catch (IOException e) {
                    throw new InvalidClassFileAttributeException("invalid InnerClasses attribute", e);
                }
            } else {
                this.bootstrapMethods = new BootstrapMethod[0];
            }
        }
        return Arrays.asList(this.bootstrapMethods);
    }

    public int getMajorVersion() {
        return this.majorVersion;
    }

    public int getMinorVersion() {
        return this.minorVersion;
    }

    public String getTypeSignature() {
        DataInputStream in;
        if (this.typeSignature == null && (in = this.attributes.getStream("Signature")) != null) {
            try {
                CPUTF8Info entry = (CPUTF8Info)this.constantPool.get(in.readUnsignedShort());
                this.typeSignature = entry.getName();
                in.close();
            }
            catch (IOException e) {
                throw new InvalidClassFileAttributeException("invalid Signature attribute", e);
            }
        }
        return this.typeSignature;
    }

    public EnclosingMethod getEnclosingMethod() {
        DataInputStream in;
        if (this.enclosingMethod == null && (in = this.attributes.getStream("EnclosingMethod")) != null) {
            try {
                int classIndex = in.readUnsignedShort();
                int natIndex = in.readUnsignedShort();
                CPEntry entry = this.constantPool.get(classIndex);
                if (entry.getTag() == 7) {
                    this.enclosingMethod = new EnclosingMethod(this.constantPool, (CPClassInfo)entry, natIndex);
                }
                in.close();
            }
            catch (IOException e) {
                throw new InvalidClassFileAttributeException("invalid EnclosingMethod attribute", e);
            }
        }
        return this.enclosingMethod;
    }

    private void loadAnnotations() {
        if (this.annotations == null) {
            this.annotations = ClassFile.buildAnnotationMap(this.constantPool, this.attributes);
        }
    }

    static Map<ClassName, Annotation> buildAnnotationMap(ConstantPool pool, AttributeMap attrs) {
        HashMap<ClassName, Annotation> annotations = new HashMap<ClassName, Annotation>(2);
        DataInputStream in = attrs.getStream("RuntimeVisibleAnnotations");
        if (in != null) {
            try {
                Annotation.load(in, pool, true, annotations);
                in.close();
            }
            catch (IOException e) {
                throw new InvalidClassFileAttributeException("invalid RuntimeVisibleAnnotations attribute", e);
            }
        }
        if ((in = attrs.getStream("RuntimeInvisibleAnnotations")) != null) {
            try {
                Annotation.load(in, pool, false, annotations);
                in.close();
            }
            catch (IOException e) {
                throw new InvalidClassFileAttributeException("invalid RuntimeInvisibleAnnotations attribute", e);
            }
        }
        return annotations;
    }

    public final Collection<Annotation> getAnnotations() {
        this.loadAnnotations();
        return this.annotations.values();
    }

    public final Annotation getAnnotation(ClassName annotationClass) {
        this.loadAnnotations();
        return this.annotations.get(annotationClass);
    }

    public final boolean isAnnotationPresent(ClassName annotationClass) {
        this.loadAnnotations();
        return this.annotations.get(annotationClass) != null;
    }

    public final Set<ClassName> getAllClassNames() {
        int i;
        HashSet<ClassName> set = new HashSet<ClassName>();
        Collection<CPClassInfo> c = this.constantPool.getAllConstants(CPClassInfo.class);
        for (CPClassInfo ci : c) {
            set.add(ci.getClassName());
        }
        for (i = 0; i < this.variables.length; ++i) {
            this.addClassNames(set, this.variables[i].getDescriptor());
        }
        for (i = 0; i < this.methods.length; ++i) {
            this.addClassNames(set, this.methods[i].getDescriptor());
        }
        return Collections.unmodifiableSet(set);
    }

    private void addClassNames(Set<ClassName> set, String type) {
        int j;
        int i = 0;
        while ((i = type.indexOf(76, i)) != -1 && (j = type.indexOf(59, i)) > i) {
            String classType = type.substring(i + 1, j);
            set.add(ClassName.getClassName(classType));
            i = j + 1;
        }
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("ClassFile: ");
        sb.append(Access.toString(this.classAccess));
        sb.append(' ');
        sb.append(this.classInfo);
        if (this.isSynthetic()) {
            sb.append(" (synthetic)");
        }
        if (this.isDeprecated()) {
            sb.append(" (deprecated)");
        }
        sb.append("\n   source: ");
        sb.append(this.getSourceFileName());
        sb.append("\n   super: ");
        sb.append(this.superClassInfo);
        if (this.getTypeSignature() != null) {
            sb.append("\n   signature: ");
            sb.append(this.typeSignature);
        }
        if (this.getEnclosingMethod() != null) {
            sb.append("\n   enclosing method: ");
            sb.append(this.enclosingMethod);
        }
        sb.append("\n   ");
        this.loadAnnotations();
        if (this.annotations.size() > 0) {
            Iterator<Annotation> iter = this.annotations.values().iterator();
            sb.append("annotations: ");
            while (iter.hasNext()) {
                sb.append("\n      ");
                sb.append(iter.next().toString());
            }
            sb.append("\n   ");
        }
        if (this.interfaces.length > 0) {
            sb.append(this.arrayToString("interfaces", this.interfaces));
            sb.append("\n   ");
        }
        if (this.getInnerClasses().size() > 0) {
            sb.append(this.arrayToString("innerclasses", this.innerClasses));
            sb.append("\n   ");
        }
        if (this.variables.length > 0) {
            sb.append(this.arrayToString("variables", this.variables));
            sb.append("\n   ");
        }
        if (this.methods.length > 0) {
            sb.append(this.arrayToString("methods", this.methods));
        }
        return sb.toString();
    }

    private String arrayToString(String name, Object[] array) {
        StringBuffer sb = new StringBuffer();
        sb.append(name);
        sb.append(": ");
        int n = array.length;
        if (n > 0) {
            int i = 0;
            do {
                sb.append("\n      ");
                sb.append(array[i++].toString());
            } while (i < n);
        } else {
            sb.append("none");
        }
        return sb.toString();
    }

    private static boolean isBadNonJavaClassName(String name) {
        return name.length() == 1 && badNonJavaClassNames.contains(name);
    }
}

