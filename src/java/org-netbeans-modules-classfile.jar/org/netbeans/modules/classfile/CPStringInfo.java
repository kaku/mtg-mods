/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.CPName;
import org.netbeans.modules.classfile.ConstantPool;

public final class CPStringInfo
extends CPName {
    CPStringInfo(ConstantPool pool, int index) {
        super(pool, index);
    }

    @Override
    public final int getTag() {
        return 8;
    }
}

