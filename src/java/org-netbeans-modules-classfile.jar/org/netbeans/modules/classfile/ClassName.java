/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import java.io.ObjectStreamException;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.Comparator;
import java.util.WeakHashMap;

public final class ClassName
implements Comparable<ClassName>,
Comparator<ClassName>,
Serializable {
    static final long serialVersionUID = -8444469778945723553L;
    private final String type;
    private final transient String internalName;
    private volatile transient String externalName;
    private volatile transient String packageName;
    private volatile transient String simpleName;
    private static final WeakHashMap<String, WeakReference<ClassName>> cache = new WeakHashMap();

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static ClassName getClassName(String classType) {
        if (classType == null || classType.length() == 0) {
            return null;
        }
        ClassName cn = ClassName.getCacheEntry(classType);
        WeakHashMap<String, WeakReference<ClassName>> weakHashMap = cache;
        synchronized (weakHashMap) {
            cn = ClassName.getCacheEntry(classType);
            if (cn == null) {
                String _type;
                int i = classType.indexOf(76);
                char lastChar = classType.charAt(classType.length() - 1);
                if (i != -1 && lastChar == ';') {
                    _type = classType.substring(i + 1, classType.length() - 1);
                    if (i > 0) {
                        _type = classType.substring(0, i) + _type;
                    }
                    if ((cn = ClassName.getCacheEntry(_type)) != null) {
                        return cn;
                    }
                } else {
                    _type = classType;
                }
                cn = new ClassName(_type);
                cache.put(_type, new WeakReference<ClassName>(cn));
            }
        }
        return cn;
    }

    private static ClassName getCacheEntry(String key) {
        WeakReference<ClassName> ref = cache.get(key);
        return ref != null ? ref.get() : null;
    }

    private ClassName(String type) {
        this.type = type;
        int i = type.lastIndexOf(91);
        this.internalName = i > -1 ? type.substring(i + 1) : type;
    }

    public String getType() {
        return this.type;
    }

    public String getInternalName() {
        return this.internalName;
    }

    public String getExternalName() {
        return this.getExternalName(false);
    }

    public String getExternalName(boolean suppressArrays) {
        int i;
        this.initExternalName();
        if (suppressArrays && (i = this.externalName.indexOf(91)) != -1) {
            return this.externalName.substring(0, i);
        }
        return this.externalName;
    }

    private synchronized void initExternalName() {
        if (this.externalName == null) {
            this.externalName = this.externalizeClassName();
        }
    }

    public String getPackage() {
        if (this.packageName == null) {
            this.initPackage();
        }
        return this.packageName;
    }

    private synchronized void initPackage() {
        int i = this.internalName.lastIndexOf(47);
        this.packageName = i != -1 ? this.internalName.substring(0, i).replace('/', '.') : "";
    }

    public String getSimpleName() {
        if (this.simpleName == null) {
            this.initSimpleName();
        }
        return this.simpleName;
    }

    private synchronized void initSimpleName() {
        String pkg = this.getPackage();
        int i = pkg.length();
        String extName = this.getExternalName();
        this.simpleName = i == 0 ? extName : extName.substring(i + 1);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj instanceof ClassName && this.type.equals(((ClassName)obj).type);
    }

    @Override
    public int compareTo(ClassName obj) {
        return this.type.compareTo(obj.type);
    }

    @Override
    public int compare(ClassName o1, ClassName o2) {
        return o1.compareTo(o2);
    }

    public int hashCode() {
        return this.type.hashCode();
    }

    public String toString() {
        return this.getExternalName();
    }

    private String externalizeClassName() {
        int i;
        StringBuffer sb = new StringBuffer(this.type);
        int arrays = 0;
        boolean atBeginning = true;
        int length = sb.length();
        block4 : for (i = 0; i < length; ++i) {
            char ch = sb.charAt(i);
            switch (ch) {
                case '[': {
                    if (!atBeginning) continue block4;
                    ++arrays;
                    continue block4;
                }
                case '$': 
                case '/': {
                    sb.setCharAt(i, '.');
                    atBeginning = false;
                    continue block4;
                }
                default: {
                    atBeginning = false;
                }
            }
        }
        if (arrays > 0) {
            sb.delete(0, arrays);
            for (i = 0; i < arrays; ++i) {
                sb.append("[]");
            }
        }
        return sb.toString();
    }

    static void clearCache() {
        cache.clear();
    }

    private Object readResolve() throws ObjectStreamException {
        return ClassName.getClassName(this.internalName);
    }
}

