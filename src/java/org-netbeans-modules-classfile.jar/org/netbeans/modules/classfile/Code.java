/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import java.io.DataInputStream;
import java.io.IOException;
import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.CPUTF8Info;
import org.netbeans.modules.classfile.ConstantPool;
import org.netbeans.modules.classfile.ExceptionTableEntry;
import org.netbeans.modules.classfile.InvalidClassFileAttributeException;
import org.netbeans.modules.classfile.InvalidClassFormatException;
import org.netbeans.modules.classfile.LocalVariableTableEntry;
import org.netbeans.modules.classfile.LocalVariableTypeTableEntry;
import org.netbeans.modules.classfile.StackMapFrame;

public final class Code {
    private static final boolean debug = false;
    private int maxStack;
    private int maxLocals;
    private byte[] byteCodes;
    private ExceptionTableEntry[] exceptionTable;
    private int[] lineNumberTable;
    private LocalVariableTableEntry[] localVariableTable;
    private LocalVariableTypeTableEntry[] localVariableTypeTable;
    private StackMapFrame[] stackMapTable;

    Code(DataInputStream in, ConstantPool pool) throws IOException {
        if (in == null) {
            throw new IllegalArgumentException("input stream not specified");
        }
        if (pool == null) {
            throw new IllegalArgumentException("constant pool not specified");
        }
        try {
            this.loadCode(in, pool);
        }
        catch (IndexOutOfBoundsException e) {
            throw new InvalidClassFileAttributeException("invalid code attribute", e);
        }
    }

    private void loadCode(DataInputStream in, ConstantPool pool) throws IOException {
        this.maxStack = in.readUnsignedShort();
        this.maxLocals = in.readUnsignedShort();
        int len = in.readInt();
        this.byteCodes = new byte[len];
        in.readFully(this.byteCodes);
        this.exceptionTable = ExceptionTableEntry.loadExceptionTable(in, pool);
        this.loadCodeAttributes(in, pool);
    }

    private void loadCodeAttributes(DataInputStream in, ConstantPool pool) throws IOException {
        int count = in.readUnsignedShort();
        for (int i = 0; i < count; ++i) {
            int n;
            CPEntry o = pool.get(in.readUnsignedShort());
            if (!(o instanceof CPUTF8Info)) {
                throw new InvalidClassFormatException();
            }
            CPUTF8Info entry = (CPUTF8Info)o;
            String name = entry.getName();
            if (name.equals("LineNumberTable")) {
                this.loadLineNumberTable(in, pool);
                continue;
            }
            if (name.equals("LocalVariableTable")) {
                this.localVariableTable = LocalVariableTableEntry.loadLocalVariableTable(in, pool);
                continue;
            }
            if (name.equals("LocalVariableTypeTable")) {
                this.localVariableTypeTable = LocalVariableTypeTableEntry.loadLocalVariableTypeTable(in, pool);
                continue;
            }
            if (name.equals("StackMapTable")) {
                this.stackMapTable = StackMapFrame.loadStackMapTable(in, pool);
                continue;
            }
            for (int len = in.readInt(); (n = (int)in.skip(len)) > 0 && n < len; len -= n) {
            }
        }
        if (this.lineNumberTable == null) {
            this.lineNumberTable = new int[0];
        }
        if (this.localVariableTable == null) {
            this.localVariableTable = new LocalVariableTableEntry[0];
        }
        if (this.localVariableTypeTable == null) {
            this.localVariableTypeTable = new LocalVariableTypeTableEntry[0];
        }
        if (this.stackMapTable == null) {
            this.stackMapTable = new StackMapFrame[0];
        }
    }

    private void loadLineNumberTable(DataInputStream in, ConstantPool pool) throws IOException {
        int n = in.readUnsignedShort();
        this.lineNumberTable = new int[n * 2];
        for (int i = 0; i < n; ++i) {
            this.lineNumberTable[i * 2] = in.readUnsignedShort();
            this.lineNumberTable[i * 2 + 1] = in.readUnsignedShort();
        }
    }

    public final int getMaxStack() {
        return this.maxStack;
    }

    public final int getMaxLocals() {
        return this.maxLocals;
    }

    public final byte[] getByteCodes() {
        return (byte[])this.byteCodes.clone();
    }

    public final ExceptionTableEntry[] getExceptionTable() {
        return (ExceptionTableEntry[])this.exceptionTable.clone();
    }

    public final int[] getLineNumberTable() {
        return (int[])this.lineNumberTable.clone();
    }

    public final LocalVariableTableEntry[] getLocalVariableTable() {
        return (LocalVariableTableEntry[])this.localVariableTable.clone();
    }

    public final LocalVariableTypeTableEntry[] getLocalVariableTypeTable() {
        return (LocalVariableTypeTableEntry[])this.localVariableTypeTable.clone();
    }

    public final StackMapFrame[] getStackMapTable() {
        return (StackMapFrame[])this.stackMapTable.clone();
    }

    public String toString() {
        StringBuffer sb = new StringBuffer("Code: bytes=");
        sb.append(this.byteCodes.length);
        sb.append(", stack=");
        sb.append(this.maxStack);
        sb.append(", locals=");
        sb.append(this.maxLocals);
        return sb.toString();
    }
}

