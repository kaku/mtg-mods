/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.CPName;
import org.netbeans.modules.classfile.CPNameAndTypeInfo;
import org.netbeans.modules.classfile.ClassName;
import org.netbeans.modules.classfile.ConstantPool;

abstract class CPFieldMethodInfo
extends CPNameAndTypeInfo {
    int iClass;
    int iNameAndType;

    CPFieldMethodInfo(ConstantPool pool, int iClass, int iNameAndType) {
        super(pool);
        this.iClass = iClass;
        this.iNameAndType = iNameAndType;
    }

    public final int getClassID() {
        return this.iClass;
    }

    public final int getFieldID() {
        return this.iNameAndType;
    }

    public final ClassName getClassName() {
        return ClassName.getClassName(((CPName)this.pool.cpEntries[this.iClass]).getName());
    }

    void setClassNameIndex(int index) {
        this.iClass = index;
    }

    public final String getFieldName() {
        return ((CPNameAndTypeInfo)this.pool.cpEntries[this.iNameAndType]).getName();
    }

    @Override
    public String toString() {
        return this.getClass().getName() + ": class=" + this.getClassName() + ", name=" + this.getName() + ", descriptor=" + this.getDescriptor();
    }

    public final String getSignature() {
        return CPFieldMethodInfo.getSignature(this.getDescriptor(), true);
    }

    static String getSignature(String s, boolean fullName) {
        StringBuffer sb = new StringBuffer();
        int arrays = 0;
        int i = 0;
        block13 : while (i < s.length()) {
            char ch = s.charAt(i++);
            switch (ch) {
                case 'B': {
                    sb.append("byte");
                    continue block13;
                }
                case 'C': {
                    sb.append("char");
                    continue block13;
                }
                case 'D': {
                    sb.append("double");
                    continue block13;
                }
                case 'F': {
                    sb.append("float");
                    continue block13;
                }
                case 'I': {
                    sb.append("int");
                    continue block13;
                }
                case 'J': {
                    sb.append("long");
                    continue block13;
                }
                case 'S': {
                    sb.append("short");
                    continue block13;
                }
                case 'Z': {
                    sb.append("boolean");
                    continue block13;
                }
                case 'V': {
                    sb.append("void");
                    continue block13;
                }
                case 'L': {
                    int idx;
                    int l = s.indexOf(59);
                    String cls = s.substring(1, l).replace('/', '.');
                    if (!fullName && (idx = cls.lastIndexOf(46)) >= 0) {
                        cls = cls.substring(idx + 1);
                    }
                    sb.append(cls);
                    i = l + 1;
                    continue block13;
                }
                case '[': {
                    ++arrays;
                    continue block13;
                }
            }
        }
        while (arrays-- > 0) {
            sb.append("[]");
        }
        return sb.toString();
    }

    @Override
    void resolve(CPEntry[] cpEntries) {
        CPNameAndTypeInfo nati = (CPNameAndTypeInfo)cpEntries[this.iNameAndType];
        this.setNameIndex(nati.iName);
        this.setDescriptorIndex(nati.iDesc);
    }
}

