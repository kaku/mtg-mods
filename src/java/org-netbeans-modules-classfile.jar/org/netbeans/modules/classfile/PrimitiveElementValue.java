/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.ConstantPool;
import org.netbeans.modules.classfile.ElementValue;

public final class PrimitiveElementValue
extends ElementValue {
    CPEntry value;

    PrimitiveElementValue(ConstantPool pool, int iValue) {
        this.value = pool.get(iValue);
    }

    public final CPEntry getValue() {
        return this.value;
    }

    public String toString() {
        return "const=" + this.value.getValue();
    }
}

