/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import java.io.DataInputStream;
import java.io.IOException;
import org.netbeans.modules.classfile.ConstantPool;
import org.netbeans.modules.classfile.InvalidClassFormatException;
import org.netbeans.modules.classfile.VerificationTypeInfo;

public abstract class StackMapFrame {
    int frameType;

    static StackMapFrame[] loadStackMapTable(DataInputStream in, ConstantPool pool) throws IOException {
        int n = in.readUnsignedShort();
        StackMapFrame[] entries = new StackMapFrame[n];
        for (int i = 0; i < n; ++i) {
            StackMapFrame frame2;
            StackMapFrame frame2;
            int tag = in.readUnsignedByte();
            if (tag >= 0 && tag <= 63) {
                frame2 = new SameFrame(tag);
            } else if (tag >= 64 && tag <= 127) {
                VerificationTypeInfo typeInfo = VerificationTypeInfo.loadVerificationTypeInfo(in, pool);
                frame2 = new SameLocals1StackItemFrame(tag, typeInfo);
            } else {
                if (tag >= 128 && tag <= 246) {
                    throw new InvalidClassFormatException("reserved stack map frame tag used: " + tag);
                }
                if (tag == 247) {
                    int offset = in.readUnsignedShort();
                    VerificationTypeInfo typeInfo = VerificationTypeInfo.loadVerificationTypeInfo(in, pool);
                    frame2 = new SameLocals1StackItemFrameExtended(tag, offset, typeInfo);
                } else if (tag >= 248 && tag <= 250) {
                    int offset = in.readUnsignedShort();
                    frame2 = new ChopFrame(tag, offset);
                } else if (tag == 251) {
                    int offset = in.readUnsignedShort();
                    frame2 = new SameFrameExtended(tag, offset);
                } else {
                    frame2 = tag >= 252 && tag <= 254 ? StackMapFrame.makeAppendFrame(tag, in, pool) : StackMapFrame.makeFullFrame(in, pool);
                }
            }
            entries[i] = frame2;
        }
        return entries;
    }

    private static AppendFrame makeAppendFrame(int tag, DataInputStream in, ConstantPool pool) throws IOException {
        int offset = in.readUnsignedShort();
        VerificationTypeInfo[] locals = new VerificationTypeInfo[tag - 251];
        for (int i = 0; i < locals.length; ++i) {
            locals[i] = VerificationTypeInfo.loadVerificationTypeInfo(in, pool);
        }
        return new AppendFrame(tag, offset, locals);
    }

    private static FullFrame makeFullFrame(DataInputStream in, ConstantPool pool) throws IOException {
        int offset = in.readUnsignedShort();
        int n = in.readUnsignedShort();
        VerificationTypeInfo[] locals = new VerificationTypeInfo[n];
        for (int i = 0; i < locals.length; ++i) {
            locals[i] = VerificationTypeInfo.loadVerificationTypeInfo(in, pool);
        }
        n = in.readUnsignedShort();
        VerificationTypeInfo[] stackItems = new VerificationTypeInfo[n];
        for (int i2 = 0; i2 < stackItems.length; ++i2) {
            stackItems[i2] = VerificationTypeInfo.loadVerificationTypeInfo(in, pool);
        }
        return new FullFrame(255, offset, locals, stackItems);
    }

    StackMapFrame(int tag) {
        this.frameType = tag;
    }

    public final int getFrameType() {
        return this.frameType;
    }

    public abstract int getOffsetDelta();

    public static final class FullFrame
    extends StackMapFrame {
        int offset;
        VerificationTypeInfo[] locals;
        VerificationTypeInfo[] stackItems;

        FullFrame(int tag, int offset, VerificationTypeInfo[] locals, VerificationTypeInfo[] stackItems) {
            super(tag);
            this.offset = offset;
            this.locals = locals;
            this.stackItems = stackItems;
        }

        @Override
        public int getOffsetDelta() {
            return this.offset;
        }

        public VerificationTypeInfo[] getLocals() {
            return (VerificationTypeInfo[])this.locals.clone();
        }

        public VerificationTypeInfo[] getStackItems() {
            return (VerificationTypeInfo[])this.stackItems.clone();
        }
    }

    public static final class AppendFrame
    extends StackMapFrame {
        int offset;
        VerificationTypeInfo[] locals;

        AppendFrame(int tag, int offset, VerificationTypeInfo[] locals) {
            super(tag);
            this.offset = offset;
            this.locals = locals;
        }

        @Override
        public int getOffsetDelta() {
            return this.offset;
        }

        public VerificationTypeInfo[] getLocals() {
            return (VerificationTypeInfo[])this.locals.clone();
        }
    }

    public static final class SameFrameExtended
    extends StackMapFrame {
        int offset;

        SameFrameExtended(int tag, int offset) {
            super(tag);
            this.offset = offset;
        }

        @Override
        public int getOffsetDelta() {
            return this.offset;
        }
    }

    public static final class ChopFrame
    extends StackMapFrame {
        int offset;

        ChopFrame(int tag, int offset) {
            super(tag);
            this.offset = offset;
        }

        @Override
        public int getOffsetDelta() {
            return this.offset;
        }
    }

    public static final class SameLocals1StackItemFrameExtended
    extends StackMapFrame {
        int offset;
        VerificationTypeInfo typeInfo;

        SameLocals1StackItemFrameExtended(int tag, int offset, VerificationTypeInfo typeInfo) {
            super(tag);
            this.offset = offset;
            this.typeInfo = typeInfo;
        }

        @Override
        public int getOffsetDelta() {
            return this.offset;
        }

        public VerificationTypeInfo getVerificationTypeInfo() {
            return this.typeInfo;
        }
    }

    public static final class SameLocals1StackItemFrame
    extends StackMapFrame {
        VerificationTypeInfo typeInfo;

        SameLocals1StackItemFrame(int tag, VerificationTypeInfo typeInfo) {
            super(tag);
            this.typeInfo = typeInfo;
        }

        @Override
        public int getOffsetDelta() {
            return this.frameType - 64;
        }

        public VerificationTypeInfo getVerificationTypeInfo() {
            return this.typeInfo;
        }
    }

    public static final class SameFrame
    extends StackMapFrame {
        SameFrame(int tag) {
            super(tag);
        }

        @Override
        public int getOffsetDelta() {
            return this.frameType;
        }
    }

}

