/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.ConstantPool;

public final class CPDoubleInfo
extends CPEntry {
    CPDoubleInfo(ConstantPool pool, double v) {
        super(pool);
        this.value = new Double(v);
    }

    @Override
    boolean usesTwoSlots() {
        return true;
    }

    @Override
    public final int getTag() {
        return 6;
    }

    public String toString() {
        return this.getClass().getName() + ": value=" + this.value;
    }
}

