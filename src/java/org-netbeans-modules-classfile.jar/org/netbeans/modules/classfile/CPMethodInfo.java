/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.CPFieldMethodInfo;
import org.netbeans.modules.classfile.ConstantPool;

public class CPMethodInfo
extends CPFieldMethodInfo {
    CPMethodInfo(ConstantPool pool, int iClass, int iNameAndType) {
        super(pool, iClass, iNameAndType);
    }

    public final String getMethodName() {
        return this.getFieldName();
    }

    public final String getFullMethodName() {
        return CPMethodInfo.getFullMethodName(this.getMethodName(), this.getDescriptor());
    }

    static String getFullMethodName(String name, String signature) {
        StringBuffer sb = new StringBuffer();
        int index = signature.indexOf(41);
        String params = signature.substring(1, index);
        if (!"<init>".equals(name) && !"<clinit>".equals(name)) {
            String ret = signature.substring(index + 1);
            if ((ret = CPFieldMethodInfo.getSignature(ret, false)).length() > 0) {
                sb.append(ret);
                sb.append(' ');
            }
        }
        sb.append(name);
        sb.append('(');
        index = 0;
        int paramsLength = params.length();
        while (index < paramsLength) {
            StringBuffer p = new StringBuffer();
            char ch = params.charAt(index++);
            while (ch == '[') {
                p.append(ch);
                ch = params.charAt(index++);
            }
            p.append(ch);
            if (ch == 'L') {
                do {
                    ch = params.charAt(index++);
                    p.append(ch);
                } while (ch != ';');
            }
            sb.append(CPFieldMethodInfo.getSignature(p.toString(), false));
            if (index >= paramsLength) continue;
            sb.append(',');
        }
        sb.append(')');
        return sb.toString();
    }

    @Override
    public int getTag() {
        return 10;
    }
}

