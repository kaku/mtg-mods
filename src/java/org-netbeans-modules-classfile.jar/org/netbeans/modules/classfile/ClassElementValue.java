/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.CPName;
import org.netbeans.modules.classfile.ClassName;
import org.netbeans.modules.classfile.ConstantPool;
import org.netbeans.modules.classfile.ElementValue;

public final class ClassElementValue
extends ElementValue {
    String name;

    ClassElementValue(ConstantPool pool, int iValue) {
        this.name = ((CPName)pool.get(iValue)).getName();
    }

    public final ClassName getClassName() {
        return ClassName.getClassName(this.name);
    }

    public String toString() {
        return "class=" + this.name;
    }
}

