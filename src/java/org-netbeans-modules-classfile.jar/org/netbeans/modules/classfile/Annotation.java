/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import org.netbeans.modules.classfile.AnnotationComponent;
import org.netbeans.modules.classfile.CPClassInfo;
import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.CPName;
import org.netbeans.modules.classfile.ClassName;
import org.netbeans.modules.classfile.ConstantPool;

public class Annotation {
    ClassName type;
    AnnotationComponent[] components;
    boolean runtimeVisible;

    static void load(DataInputStream in, ConstantPool pool, boolean visible, Map<ClassName, Annotation> map) throws IOException {
        int nattrs = in.readUnsignedShort();
        for (int i = 0; i < nattrs; ++i) {
            Annotation ann = Annotation.loadAnnotation(in, pool, visible);
            map.put(ann.getType(), ann);
        }
    }

    static Annotation loadAnnotation(DataInputStream in, ConstantPool pool, boolean visible) throws IOException {
        ClassName type;
        CPEntry entry = pool.get(in.readUnsignedShort());
        if (entry.getTag() == 7) {
            type = ((CPClassInfo)entry).getClassName();
        } else {
            String s = ((CPName)entry).getName();
            type = ClassName.getClassName(s);
        }
        int npairs = in.readUnsignedShort();
        ArrayList<AnnotationComponent> pairList = new ArrayList<AnnotationComponent>();
        for (int j = 0; j < npairs; ++j) {
            pairList.add(AnnotationComponent.load(in, pool, visible));
        }
        AnnotationComponent[] acs = new AnnotationComponent[pairList.size()];
        pairList.toArray(acs);
        return new Annotation(pool, type, acs, visible);
    }

    Annotation(ConstantPool pool, ClassName type, AnnotationComponent[] components, boolean runtimeVisible) {
        this.type = type;
        this.components = components;
        this.runtimeVisible = runtimeVisible;
    }

    public final ClassName getType() {
        return this.type;
    }

    public final AnnotationComponent[] getComponents() {
        return (AnnotationComponent[])this.components.clone();
    }

    public final AnnotationComponent getComponent(String name) {
        for (int i = 0; i < this.components.length; ++i) {
            AnnotationComponent comp = this.components[i];
            if (!comp.getName().equals(name)) continue;
            return comp;
        }
        return null;
    }

    public boolean isRuntimeVisible() {
        return this.runtimeVisible;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer("@");
        sb.append(this.type);
        sb.append(" runtimeVisible=");
        sb.append(this.runtimeVisible);
        int n = this.components.length;
        if (n > 0) {
            sb.append(" { ");
            for (int i = 0; i < n; ++i) {
                sb.append(this.components[i]);
                if (i >= n - 1) continue;
                sb.append(", ");
            }
            sb.append(" }");
        }
        return sb.toString();
    }
}

