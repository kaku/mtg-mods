/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.classfile;

import java.io.DataInputStream;
import java.io.IOException;
import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.CPName;
import org.netbeans.modules.classfile.ConstantPool;
import org.netbeans.modules.classfile.ElementValue;

public class AnnotationComponent {
    String name;
    ElementValue value;

    static AnnotationComponent load(DataInputStream in, ConstantPool pool, boolean runtimeVisible) throws IOException {
        int iName = in.readUnsignedShort();
        String name = ((CPName)pool.get(iName)).getName();
        ElementValue value = ElementValue.load(in, pool, runtimeVisible);
        return new AnnotationComponent(name, value);
    }

    AnnotationComponent(String name, ElementValue value) {
        this.name = name;
        this.value = value;
    }

    public final String getName() {
        return this.name;
    }

    public final ElementValue getValue() {
        return this.value;
    }

    public String toString() {
        return "name=" + this.name + ", value=" + this.value;
    }
}

