/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.api.java.queries;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String NAME_Compact1() {
        return NbBundle.getMessage(Bundle.class, (String)"NAME_Compact1");
    }

    static String NAME_Compact2() {
        return NbBundle.getMessage(Bundle.class, (String)"NAME_Compact2");
    }

    static String NAME_Compact3() {
        return NbBundle.getMessage(Bundle.class, (String)"NAME_Compact3");
    }

    static String NAME_FullJRE() {
        return NbBundle.getMessage(Bundle.class, (String)"NAME_FullJRE");
    }

    private void Bundle() {
    }
}

