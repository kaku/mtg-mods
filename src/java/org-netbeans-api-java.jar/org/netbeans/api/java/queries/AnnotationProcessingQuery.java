/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 *  org.openide.util.Parameters
 */
package org.netbeans.api.java.queries;

import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.spi.java.queries.AnnotationProcessingQueryImplementation;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;
import org.openide.util.Parameters;

public class AnnotationProcessingQuery {
    private static final Result EMPTY = new Result(){

        @Override
        public Set<? extends Trigger> annotationProcessingEnabled() {
            return EnumSet.noneOf(Trigger.class);
        }

        @Override
        public Iterable<? extends String> annotationProcessorsToRun() {
            return null;
        }

        @Override
        public URL sourceOutputDirectory() {
            return null;
        }

        @Override
        public Map<? extends String, ? extends String> processorOptions() {
            return Collections.emptyMap();
        }

        @Override
        public void addChangeListener(ChangeListener l) {
        }

        @Override
        public void removeChangeListener(ChangeListener l) {
        }
    };

    @NonNull
    public static Result getAnnotationProcessingOptions(@NonNull FileObject file) {
        Parameters.notNull((CharSequence)"file", (Object)file);
        for (AnnotationProcessingQueryImplementation i : Lookup.getDefault().lookupAll(AnnotationProcessingQueryImplementation.class)) {
            Result r = i.getAnnotationProcessingOptions(file);
            if (r == null) continue;
            return r;
        }
        return EMPTY;
    }

    private AnnotationProcessingQuery() {
    }

    public static enum Trigger {
        ON_SCAN,
        IN_EDITOR;
        

        private Trigger() {
        }
    }

    public static interface Result {
        @NonNull
        public Set<? extends Trigger> annotationProcessingEnabled();

        @CheckForNull
        public Iterable<? extends String> annotationProcessorsToRun();

        @CheckForNull
        public URL sourceOutputDirectory();

        @NonNull
        public Map<? extends String, ? extends String> processorOptions();

        public void addChangeListener(@NonNull ChangeListener var1);

        public void removeChangeListener(@NonNull ChangeListener var1);
    }

}

