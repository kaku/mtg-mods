/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.filesystems.FileObject
 *  org.openide.modules.SpecificationVersion
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Parameters
 *  org.openide.util.Union2
 *  org.openide.util.WeakListeners
 */
package org.netbeans.api.java.queries;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.queries.Bundle;
import org.netbeans.spi.java.queries.SourceLevelQueryImplementation;
import org.netbeans.spi.java.queries.SourceLevelQueryImplementation2;
import org.openide.filesystems.FileObject;
import org.openide.modules.SpecificationVersion;
import org.openide.util.ChangeSupport;
import org.openide.util.Lookup;
import org.openide.util.Parameters;
import org.openide.util.Union2;
import org.openide.util.WeakListeners;

public class SourceLevelQuery {
    private static final Logger LOGGER = Logger.getLogger(SourceLevelQuery.class.getName());
    private static final Pattern SOURCE_LEVEL = Pattern.compile("\\d+\\.\\d+");
    private static final Pattern SYNONYM = Pattern.compile("\\d+");
    private static final SpecificationVersion JDK8 = new SpecificationVersion("1.8");
    private static final Lookup.Result<? extends SourceLevelQueryImplementation> implementations = Lookup.getDefault().lookupResult(SourceLevelQueryImplementation.class);
    private static final Lookup.Result<? extends SourceLevelQueryImplementation2> implementations2 = Lookup.getDefault().lookupResult(SourceLevelQueryImplementation2.class);

    private SourceLevelQuery() {
    }

    public static String getSourceLevel(FileObject javaFile) {
        for (Object sqi2 : implementations2.allInstances()) {
            String s;
            SourceLevelQueryImplementation2.Result result = sqi2.getSourceLevel(javaFile);
            if (result == null || (s = SourceLevelQuery.normalize(result.getSourceLevel())) == null) continue;
            if (!SOURCE_LEVEL.matcher(s).matches()) {
                LOGGER.log(Level.WARNING, "#83994: Ignoring bogus source level {0} for {1} from {2}", new Object[]{s, javaFile, sqi2});
                continue;
            }
            LOGGER.log(Level.FINE, "Found source level {0} for {1} from {2}", new Object[]{s, javaFile, sqi2});
            return s;
        }
        for (Object sqi2 : implementations.allInstances()) {
            String s = SourceLevelQuery.normalize(sqi2.getSourceLevel(javaFile));
            if (s == null) continue;
            if (!SOURCE_LEVEL.matcher(s).matches()) {
                LOGGER.log(Level.WARNING, "#83994: Ignoring bogus source level {0} for {1} from {2}", new Object[]{s, javaFile, sqi2});
                continue;
            }
            LOGGER.log(Level.FINE, "Found source level {0} for {1} from {2}", new Object[]{s, javaFile, sqi2});
            return s;
        }
        LOGGER.log(Level.FINE, "No source level found for {0}", (Object)javaFile);
        return null;
    }

    @NonNull
    public static Result getSourceLevel2(@NonNull FileObject javaFile) {
        for (SourceLevelQueryImplementation2 sqi : implementations2.allInstances()) {
            SourceLevelQueryImplementation2.Result result = sqi.getSourceLevel(javaFile);
            if (result == null) continue;
            LOGGER.log(Level.FINE, "Found source level {0} for {1} from {2}", new Object[]{result, javaFile, sqi});
            return new Result(result);
        }
        LOGGER.log(Level.FINE, "No source level found for {0}", (Object)javaFile);
        return new Result(javaFile);
    }

    @CheckForNull
    private static String normalize(@NullAllowed String sourceLevel) {
        if (sourceLevel != null && SYNONYM.matcher(sourceLevel).matches()) {
            sourceLevel = MessageFormat.format("1.{0}", sourceLevel);
        }
        return sourceLevel;
    }

    static /* synthetic */ SpecificationVersion access$200() {
        return JDK8;
    }

    public static final class Result {
        @NonNull
        private final Union2<SourceLevelQueryImplementation2.Result, FileObject> delegate;
        private final ChangeSupport cs;
        private ChangeListener spiListener;

        private Result(@NonNull SourceLevelQueryImplementation2.Result delegate) {
            this.cs = new ChangeSupport((Object)this);
            Parameters.notNull((CharSequence)"delegate", (Object)delegate);
            this.delegate = Union2.createFirst((Object)delegate);
        }

        private Result(@NonNull FileObject javaFile) {
            this.cs = new ChangeSupport((Object)this);
            Parameters.notNull((CharSequence)"sourceLevel", (Object)javaFile);
            this.delegate = Union2.createSecond((Object)javaFile);
        }

        @CheckForNull
        public String getSourceLevel() {
            if (this.delegate.hasFirst()) {
                String sourceLevel = SourceLevelQuery.normalize(((SourceLevelQueryImplementation2.Result)this.delegate.first()).getSourceLevel());
                if (sourceLevel != null && !SOURCE_LEVEL.matcher(sourceLevel).matches()) {
                    LOGGER.log(Level.WARNING, "#83994: Ignoring bogus source level {0} from {2}", new Object[]{sourceLevel, this.delegate.first()});
                    sourceLevel = null;
                }
                return sourceLevel;
            }
            return SourceLevelQuery.getSourceLevel((FileObject)this.delegate.second());
        }

        @NonNull
        public Profile getProfile() {
            SourceLevelQueryImplementation2.Result delegate = this.getDelegate();
            if (!(delegate instanceof SourceLevelQueryImplementation2.Result2)) {
                return Profile.DEFAULT;
            }
            Profile result = ((SourceLevelQueryImplementation2.Result2)delegate).getProfile();
            assert (result != null);
            return result;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void addChangeListener(@NonNull ChangeListener listener) {
            Parameters.notNull((CharSequence)"listener", (Object)listener);
            SourceLevelQueryImplementation2.Result _delegate = this.getDelegate();
            if (_delegate == null) {
                return;
            }
            this.cs.addChangeListener(listener);
            Result result = this;
            synchronized (result) {
                if (this.spiListener == null) {
                    this.spiListener = new ChangeListener(){

                        @Override
                        public void stateChanged(ChangeEvent e) {
                            Result.this.cs.fireChange();
                        }
                    };
                    _delegate.addChangeListener(WeakListeners.change((ChangeListener)this.spiListener, (Object)_delegate));
                }
            }
        }

        public void removeChangeListener(@NonNull ChangeListener listener) {
            Parameters.notNull((CharSequence)"listener", (Object)listener);
            SourceLevelQueryImplementation2.Result _delegate = this.getDelegate();
            if (_delegate == null) {
                return;
            }
            this.cs.removeChangeListener(listener);
        }

        public boolean supportsChanges() {
            return this.getDelegate() != null;
        }

        private SourceLevelQueryImplementation2.Result getDelegate() {
            return this.delegate.hasFirst() ? (SourceLevelQueryImplementation2.Result)this.delegate.first() : null;
        }

    }

    public static class Profile
    extends Enum<Profile> {
        public static final /* enum */ Profile COMPACT1 = new Profile("compact1", Bundle.NAME_Compact1(), SourceLevelQuery.access$200());
        public static final /* enum */ Profile COMPACT2 = new Profile("compact2", Bundle.NAME_Compact2(), SourceLevelQuery.access$200());
        public static final /* enum */ Profile COMPACT3 = new Profile("compact3", Bundle.NAME_Compact3(), SourceLevelQuery.access$200());
        public static final /* enum */ Profile DEFAULT = new Profile("DEFAULT", 3, Bundle.NAME_FullJRE()){

            @Override
            public boolean isSupportedIn(@NonNull String sourceLevel) {
                return true;
            }
        };
        private static final Map<String, Profile> profilesByName;
        private final String name;
        private final String displayName;
        private final SpecificationVersion supportedFrom;
        private static final /* synthetic */ Profile[] $VALUES;

        public static Profile[] values() {
            return (Profile[])$VALUES.clone();
        }

        public static Profile valueOf(String name) {
            return Enum.valueOf(Profile.class, name);
        }

        private Profile(@NonNull String name, String displayName, SpecificationVersion supportedFrom) {
            super(string, n);
            assert (name != null);
            assert (displayName != null);
            assert (supportedFrom != null);
            this.name = name;
            this.displayName = displayName;
            this.supportedFrom = supportedFrom;
        }

        private Profile(String displayName) {
            super(string, n);
            assert (displayName != null);
            this.name = "";
            this.displayName = displayName;
            this.supportedFrom = null;
        }

        @NonNull
        public String getName() {
            return this.name;
        }

        @NonNull
        public String getDisplayName() {
            return this.displayName;
        }

        public boolean isSupportedIn(@NonNull String sourceLevel) {
            Parameters.notNull((CharSequence)"sourceLevel", (Object)sourceLevel);
            sourceLevel = SourceLevelQuery.normalize(sourceLevel);
            if (!SOURCE_LEVEL.matcher(sourceLevel).matches()) {
                throw new IllegalArgumentException(sourceLevel);
            }
            return this.supportedFrom.compareTo((Object)new SpecificationVersion(sourceLevel)) <= 0;
        }

        @CheckForNull
        public static Profile forName(@NullAllowed String profileName) {
            if (profileName == null) {
                profileName = DEFAULT.getName();
            }
            return profilesByName.get(profileName);
        }

        static {
            $VALUES = new Profile[]{COMPACT1, COMPACT2, COMPACT3, DEFAULT};
            profilesByName = new HashMap<String, Profile>();
            for (Profile sp : Profile.values()) {
                profilesByName.put(sp.getName(), sp);
            }
        }

    }

}

