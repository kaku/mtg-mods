/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.openide.ErrorManager
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 */
package org.netbeans.api.java.queries;

import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import javax.swing.event.ChangeListener;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.netbeans.spi.java.queries.JavadocForBinaryQueryImplementation;
import org.openide.ErrorManager;
import org.openide.util.Lookup;

public class JavadocForBinaryQuery {
    private static final ErrorManager ERR = ErrorManager.getDefault().getInstance(JavadocForBinaryQuery.class.getName());
    private static final Lookup.Result<? extends JavadocForBinaryQueryImplementation> implementations = Lookup.getDefault().lookupResult(JavadocForBinaryQueryImplementation.class);
    private static final Result EMPTY_RESULT = new EmptyResult();

    private JavadocForBinaryQuery() {
    }

    public static Result findJavadoc(URL binary) {
        ClassPathSupport.createResource((URL)binary);
        boolean log = ERR.isLoggable(1);
        if (log) {
            ERR.log("JFBQ.findJavadoc: " + binary);
        }
        for (JavadocForBinaryQueryImplementation impl : implementations.allInstances()) {
            Result r = impl.findJavadoc(binary);
            if (r != null) {
                if (log) {
                    ERR.log("  got result " + Arrays.asList(r.getRoots()) + " from " + impl);
                }
                return r;
            }
            if (!log) continue;
            ERR.log("  got no result from " + impl);
        }
        if (log) {
            ERR.log("  got no results from any impl");
        }
        return EMPTY_RESULT;
    }

    private static final class EmptyResult
    implements Result {
        private static final URL[] NO_ROOTS = new URL[0];

        EmptyResult() {
        }

        @Override
        public URL[] getRoots() {
            return NO_ROOTS;
        }

        @Override
        public void addChangeListener(ChangeListener l) {
        }

        @Override
        public void removeChangeListener(ChangeListener l) {
        }
    }

    public static interface Result {
        public URL[] getRoots();

        public void addChangeListener(ChangeListener var1);

        public void removeChangeListener(ChangeListener var1);
    }

}

