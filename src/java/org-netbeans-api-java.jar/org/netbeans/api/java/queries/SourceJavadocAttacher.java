/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Parameters
 */
package org.netbeans.api.java.queries;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.spi.java.queries.SourceJavadocAttacherImplementation;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Parameters;

public final class SourceJavadocAttacher {
    private static final Logger LOG = Logger.getLogger(SourceJavadocAttacher.class.getName());

    private SourceJavadocAttacher() {
    }

    public static void attachSources(@NonNull URL root, @NullAllowed AttachmentListener listener) {
        SourceJavadocAttacher.attach(root, listener, 0);
    }

    public static void attachJavadoc(@NonNull URL root, @NullAllowed AttachmentListener listener) {
        SourceJavadocAttacher.attach(root, listener, 1);
    }

    private static void attach(URL root, @NullAllowed AttachmentListener listener, int mode) {
        Parameters.notNull((CharSequence)"root", (Object)root);
        if (listener == null) {
            listener = new AttachmentListener(){

                @Override
                public void attachmentSucceeded() {
                }

                @Override
                public void attachmentFailed() {
                }
            };
        }
        try {
            for (SourceJavadocAttacherImplementation attacher : Lookup.getDefault().lookupAll(SourceJavadocAttacherImplementation.class)) {
                boolean handles = mode == 0 ? attacher.attachSources(root, listener) : attacher.attachJavadoc(root, listener);
                if (!handles) continue;
                Object[] arrobject = new Object[3];
                arrobject[0] = mode == 0 ? "sources" : "javadoc";
                arrobject[1] = root;
                arrobject[2] = attacher.getClass().getName();
                LOG.log(Level.FINE, "Attaching of {0} to root: {1} handled by: {2}", arrobject);
                return;
            }
        }
        catch (IOException ioe) {
            Exceptions.printStackTrace((Throwable)ioe);
        }
        if (listener != null) {
            listener.attachmentFailed();
        }
        Object[] arrobject = new Object[2];
        arrobject[0] = mode == 0 ? "sources" : "javadoc";
        arrobject[1] = root;
        LOG.log(Level.FINE, "No provider handled attaching of {0} to root: {1}", arrobject);
    }

    public static interface AttachmentListener {
        public void attachmentSucceeded();

        public void attachmentFailed();
    }

}

