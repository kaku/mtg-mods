/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 */
package org.netbeans.api.java.queries;

import java.net.URL;
import java.util.Collection;
import org.netbeans.spi.java.queries.MultipleRootsUnitTestForSourceQueryImplementation;
import org.netbeans.spi.java.queries.UnitTestForSourceQueryImplementation;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public class UnitTestForSourceQuery {
    private static final Lookup.Result<? extends UnitTestForSourceQueryImplementation> implementations = Lookup.getDefault().lookupResult(UnitTestForSourceQueryImplementation.class);
    private static final Lookup.Result<? extends MultipleRootsUnitTestForSourceQueryImplementation> mrImplementations = Lookup.getDefault().lookupResult(MultipleRootsUnitTestForSourceQueryImplementation.class);

    private UnitTestForSourceQuery() {
    }

    public static URL findUnitTest(FileObject source) {
        URL[] result = UnitTestForSourceQuery.findUnitTests(source);
        return result.length == 0 ? null : result[0];
    }

    public static URL[] findUnitTests(FileObject source) {
        if (source == null) {
            throw new IllegalArgumentException("Parameter source cannot be null");
        }
        for (Object query2 : mrImplementations.allInstances()) {
            URL[] urls = query2.findUnitTests(source);
            if (urls == null) continue;
            return urls;
        }
        for (Object query2 : implementations.allInstances()) {
            URL u = query2.findUnitTest(source);
            if (u == null) continue;
            return new URL[]{u};
        }
        return new URL[0];
    }

    public static URL findSource(FileObject unitTest) {
        URL[] result = UnitTestForSourceQuery.findSources(unitTest);
        return result.length == 0 ? null : result[0];
    }

    public static URL[] findSources(FileObject unitTest) {
        if (unitTest == null) {
            throw new IllegalArgumentException("Parameter unitTest cannot be null");
        }
        for (Object query2 : mrImplementations.allInstances()) {
            URL[] urls = query2.findSources(unitTest);
            if (urls == null) continue;
            return urls;
        }
        for (Object query2 : implementations.allInstances()) {
            URL u = query2.findSource(unitTest);
            if (u == null) continue;
            return new URL[]{u};
        }
        return new URL[0];
    }
}

