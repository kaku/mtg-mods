/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 */
package org.netbeans.api.java.queries;

import java.util.Collection;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.netbeans.spi.java.queries.AccessibilityQueryImplementation;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public class AccessibilityQuery {
    private static final Lookup.Result<? extends AccessibilityQueryImplementation> implementations = Lookup.getDefault().lookupResult(AccessibilityQueryImplementation.class);

    private AccessibilityQuery() {
    }

    @SuppressWarnings(value={"NP_BOOLEAN_RETURN_NULL"})
    @CheckForNull
    public static Boolean isPubliclyAccessible(@NonNull FileObject pkg) {
        if (!pkg.isFolder()) {
            throw new IllegalArgumentException("Not a folder: " + (Object)pkg);
        }
        for (AccessibilityQueryImplementation aqi : implementations.allInstances()) {
            Boolean b = aqi.isPubliclyAccessible(pkg);
            if (b == null) continue;
            return b;
        }
        return null;
    }
}

