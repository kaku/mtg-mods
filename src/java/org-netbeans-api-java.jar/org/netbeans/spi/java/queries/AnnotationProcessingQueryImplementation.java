/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.java.queries;

import org.netbeans.api.java.queries.AnnotationProcessingQuery;
import org.openide.filesystems.FileObject;

public interface AnnotationProcessingQueryImplementation {
    public AnnotationProcessingQuery.Result getAnnotationProcessingOptions(FileObject var1);
}

