/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.java.queries;

import org.openide.filesystems.FileObject;

@Deprecated
public interface SourceLevelQueryImplementation {
    public String getSourceLevel(FileObject var1);
}

