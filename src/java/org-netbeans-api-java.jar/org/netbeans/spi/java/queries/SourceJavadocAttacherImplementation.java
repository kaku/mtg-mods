/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.spi.java.queries;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.Callable;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.queries.SourceJavadocAttacher;

public interface SourceJavadocAttacherImplementation {
    public boolean attachSources(@NonNull URL var1, @NonNull SourceJavadocAttacher.AttachmentListener var2) throws IOException;

    public boolean attachJavadoc(@NonNull URL var1, @NonNull SourceJavadocAttacher.AttachmentListener var2) throws IOException;

    public static interface Definer {
        @NonNull
        public String getDisplayName();

        @NonNull
        public String getDescription();

        @NonNull
        public List<? extends URL> getSources(@NonNull URL var1, @NonNull Callable<Boolean> var2);

        @NonNull
        public List<? extends URL> getJavadoc(@NonNull URL var1, @NonNull Callable<Boolean> var2);
    }

}

