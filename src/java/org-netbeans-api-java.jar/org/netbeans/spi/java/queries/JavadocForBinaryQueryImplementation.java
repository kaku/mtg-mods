/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.java.queries;

import java.net.URL;
import org.netbeans.api.java.queries.JavadocForBinaryQuery;

public interface JavadocForBinaryQueryImplementation {
    public JavadocForBinaryQuery.Result findJavadoc(URL var1);
}

