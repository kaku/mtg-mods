/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.java.queries;

import java.net.URL;
import org.openide.filesystems.FileObject;

public interface MultipleRootsUnitTestForSourceQueryImplementation {
    public URL[] findUnitTests(FileObject var1);

    public URL[] findSources(FileObject var1);
}

