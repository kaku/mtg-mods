/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.java.queries;

import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.queries.SourceLevelQuery;
import org.openide.filesystems.FileObject;

public interface SourceLevelQueryImplementation2 {
    public Result getSourceLevel(FileObject var1);

    public static interface Result2
    extends Result {
        @NonNull
        public SourceLevelQuery.Profile getProfile();
    }

    public static interface Result {
        @CheckForNull
        public String getSourceLevel();

        public void addChangeListener(@NonNull ChangeListener var1);

        public void removeChangeListener(@NonNull ChangeListener var1);
    }

}

