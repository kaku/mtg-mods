/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.parsing.spi.indexing.Indexable
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.java.preprocessorbridge.spi;

import com.sun.source.tree.CompilationUnitTree;
import java.net.URL;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public interface JavaIndexerPlugin {
    public void process(@NonNull CompilationUnitTree var1, @NonNull Indexable var2, @NonNull Lookup var3);

    public void delete(@NonNull Indexable var1);

    public void finish();

    public static interface Factory {
        @CheckForNull
        public JavaIndexerPlugin create(@NonNull URL var1, @NonNull FileObject var2);
    }

}

