/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.preprocessorbridge.spi;

import java.io.File;
import java.util.Set;

public interface VirtualSourceProvider {
    public Set<String> getSupportedExtensions();

    public boolean index();

    public void translate(Iterable<File> var1, File var2, Result var3);

    public static interface Result {
        public void add(File var1, String var2, String var3, CharSequence var4);
    }

}

