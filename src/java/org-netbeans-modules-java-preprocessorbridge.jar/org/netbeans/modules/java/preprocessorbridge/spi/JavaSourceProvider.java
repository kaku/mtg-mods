/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.java.preprocessorbridge.spi;

import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.openide.filesystems.FileObject;

public interface JavaSourceProvider {
    public PositionTranslatingJavaFileFilterImplementation forFileObject(FileObject var1);

    public static interface PositionTranslatingJavaFileFilterImplementation
    extends JavaFileFilterImplementation {
        public int getOriginalPosition(int var1);

        public int getJavaSourcePosition(int var1);
    }

}

