/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.preprocessorbridge.spi;

import javax.swing.text.Document;

public interface ImportProcessor {
    public void addImport(Document var1, String var2);
}

