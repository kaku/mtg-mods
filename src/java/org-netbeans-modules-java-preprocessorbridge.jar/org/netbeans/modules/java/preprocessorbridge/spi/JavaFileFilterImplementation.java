/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.preprocessorbridge.spi;

import java.io.Reader;
import java.io.Writer;
import javax.swing.event.ChangeListener;

public interface JavaFileFilterImplementation {
    public Reader filterReader(Reader var1);

    public CharSequence filterCharSequence(CharSequence var1);

    public Writer filterWriter(Writer var1);

    public void addChangeListener(ChangeListener var1);

    public void removeChangeListener(ChangeListener var1);
}

