/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.java.preprocessorbridge;

import java.io.IOException;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaSourceUtilImpl;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public abstract class JavaSourceUtilImplAccessor {
    private static volatile JavaSourceUtilImplAccessor impl;

    public static void setInstance(JavaSourceUtilImplAccessor _impl) {
        assert (_impl != null);
        impl = _impl;
    }

    public static synchronized JavaSourceUtilImplAccessor getInstance() {
        if (impl == null) {
            try {
                Class.forName(JavaSourceUtilImpl.class.getName(), true, JavaSourceUtilImpl.class.getClassLoader());
            }
            catch (ClassNotFoundException cnfe) {
                Exceptions.printStackTrace((Throwable)cnfe);
            }
        }
        return impl;
    }

    public abstract long createTaggedCompilationController(JavaSourceUtilImpl var1, FileObject var2, long var3, Object[] var5) throws IOException;
}

