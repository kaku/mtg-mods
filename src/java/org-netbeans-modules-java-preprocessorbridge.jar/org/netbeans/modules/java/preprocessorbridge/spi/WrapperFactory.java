/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.util.Trees
 */
package org.netbeans.modules.java.preprocessorbridge.spi;

import com.sun.source.util.Trees;

public interface WrapperFactory {
    public Trees wrapTrees(Trees var1);
}

