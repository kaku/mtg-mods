/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 */
package org.netbeans.modules.java.preprocessorbridge.api;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import org.netbeans.modules.java.preprocessorbridge.JavaSourceUtilImplAccessor;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaSourceUtilImpl;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public class JavaSourceUtil {
    private static final Lookup.Result<JavaSourceUtilImpl> result = Lookup.getDefault().lookupResult(JavaSourceUtilImpl.class);

    private JavaSourceUtil() {
    }

    public static Handle createControllerHandle(FileObject file, Handle handle) throws IOException {
        assert (file != null);
        JavaSourceUtilImpl impl = JavaSourceUtil.getSPI();
        assert (impl != null);
        long id = handle == null ? -1 : handle.id;
        Object[] arrobject = new Object[1];
        arrobject[0] = handle == null ? null : handle.compilationController;
        Object[] param = arrobject;
        long newId = JavaSourceUtilImplAccessor.getInstance().createTaggedCompilationController(impl, file, id, param);
        if (newId == id) {
            return handle;
        }
        return new Handle(param[0], newId);
    }

    private static JavaSourceUtilImpl getSPI() {
        Collection instances = result.allInstances();
        int size = instances.size();
        assert (size < 2);
        return size == 0 ? null : (JavaSourceUtilImpl)instances.iterator().next();
    }

    public static class Handle {
        private final long id;
        private final Object compilationController;

        private Handle(Object compilaionController, long id) {
            this.compilationController = compilaionController;
            this.id = id;
        }

        public Object getCompilationController() {
            return this.compilationController;
        }
    }

}

