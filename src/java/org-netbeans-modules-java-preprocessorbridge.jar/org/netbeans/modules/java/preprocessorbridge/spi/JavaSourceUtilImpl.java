/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.java.preprocessorbridge.spi;

import java.io.IOException;
import org.netbeans.modules.java.preprocessorbridge.JavaSourceUtilImplAccessor;
import org.openide.filesystems.FileObject;

public abstract class JavaSourceUtilImpl {
    private static final String EXPECTED_PACKAGE = "org.netbeans.modules.java.source";

    protected JavaSourceUtilImpl() {
        String implPackage = this.getClass().getPackage().getName();
        if (!"org.netbeans.modules.java.source".equals(implPackage)) {
            throw new IllegalArgumentException();
        }
    }

    protected abstract long createTaggedCompilationController(FileObject var1, long var2, Object[] var4) throws IOException;

    static {
        JavaSourceUtilImplAccessor.setInstance(new MyAccessor());
    }

    private static class MyAccessor
    extends JavaSourceUtilImplAccessor {
        private MyAccessor() {
        }

        @Override
        public long createTaggedCompilationController(JavaSourceUtilImpl spi, FileObject fo, long currentTag, Object[] out) throws IOException {
            assert (spi != null);
            return spi.createTaggedCompilationController(fo, currentTag, out);
        }
    }

}

