/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.whitelist;

import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.spi.whitelist.WhiteListQueryImplementation;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public class ProjectWhiteListQueryImpl
implements WhiteListQueryImplementation {
    @Override
    public WhiteListQueryImplementation.WhiteListImplementation getWhiteList(FileObject file) {
        WhiteListQueryImplementation delegate;
        Project project = FileOwnerQuery.getOwner((FileObject)file);
        if (project != null && (delegate = (WhiteListQueryImplementation)project.getLookup().lookup(WhiteListQueryImplementation.class)) != null) {
            return delegate.getWhiteList(file);
        }
        return null;
    }
}

