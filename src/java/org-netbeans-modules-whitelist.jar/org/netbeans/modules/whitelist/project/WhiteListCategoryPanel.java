/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.project.Project
 *  org.netbeans.spi.project.ui.support.ProjectCustomizer
 *  org.netbeans.spi.project.ui.support.ProjectCustomizer$Category
 *  org.netbeans.spi.project.ui.support.ProjectCustomizer$CompositeCategoryProvider
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.whitelist.project;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultCellEditor;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle;
import javax.swing.event.TableModelListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.netbeans.api.project.Project;
import org.netbeans.modules.whitelist.project.WhiteListLookupProvider;
import org.netbeans.spi.project.ui.support.ProjectCustomizer;
import org.netbeans.spi.whitelist.WhiteListQueryImplementation;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class WhiteListCategoryPanel
extends JPanel
implements ActionListener {
    private Project p;
    private JLabel jLabel1;
    private JScrollPane jScrollPane1;
    private JTable jTable1;

    public WhiteListCategoryPanel(Project p) {
        this.p = p;
        this.initComponents();
        WhiteListsModel model = new WhiteListsModel(this.getTableContent());
        this.jTable1.setModel(model);
        this.jTable1.getTableHeader().setVisible(false);
        this.jTable1.getTableHeader().setPreferredSize(new Dimension(0, 0));
        this.jTable1.getColumnModel().getColumn(0).setCellEditor(new DefaultCellEditor(new JCheckBox()));
        this.jTable1.getColumnModel().getColumn(0).setMaxWidth(25);
        this.jTable1.getColumnModel().getColumn(0).setMinWidth(25);
    }

    private List<Desc> getTableContent() {
        ArrayList<Desc> l = new ArrayList<Desc>();
        for (WhiteListQueryImplementation.UserSelectable impl : WhiteListLookupProvider.getUserSelectableWhiteLists()) {
            l.add(new Desc(impl, WhiteListLookupProvider.isWhiteListEnabledInProject(this.p, impl.getId())));
        }
        return l;
    }

    public static ProjectCustomizer.CompositeCategoryProvider createWhiteListCategoryProvider(Map attrs) {
        return new Factory(Boolean.TRUE.equals((Boolean)attrs.get("show")));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        for (Desc d : ((WhiteListsModel)this.jTable1.getModel()).whitelists) {
            WhiteListLookupProvider.enableWhiteListInProject(this.p, d.w.getId(), d.active);
        }
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this.jTable1 = new JTable();
        this.jLabel1.setText(NbBundle.getMessage(WhiteListCategoryPanel.class, (String)"WhiteListCategoryPanel.jLabel1.text"));
        this.jScrollPane1.setViewportView(this.jTable1);
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.jLabel1).addContainerGap(283, 32767)).addComponent(this.jScrollPane1, -1, 400, 32767));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.jLabel1).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jScrollPane1, -1, 263, 32767)));
    }

    private static class WhiteListsModel
    implements TableModel {
        private List<Desc> whitelists;
        private String[] header = new String[]{"Enabled", "Whitelist"};
        private Class[] headerClass = new Class[]{Boolean.class, String.class};

        public WhiteListsModel(List<Desc> whitelists) {
            assert (whitelists.size() > 0);
            this.whitelists = new ArrayList<Desc>(whitelists);
        }

        @Override
        public int getRowCount() {
            return this.whitelists.size();
        }

        @Override
        public int getColumnCount() {
            return 2;
        }

        @Override
        public String getColumnName(int columnIndex) {
            return this.header[columnIndex];
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return this.headerClass[columnIndex];
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return columnIndex == 0;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Desc srd = this.whitelists.get(rowIndex);
            if (columnIndex == 0) {
                return srd.active;
            }
            return srd.w.getDisplayName();
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            if (aValue instanceof Boolean) {
                this.whitelists.get(rowIndex).active = (Boolean)aValue;
            }
        }

        @Override
        public void addTableModelListener(TableModelListener l) {
        }

        @Override
        public void removeTableModelListener(TableModelListener l) {
        }
    }

    private static class Desc {
        private WhiteListQueryImplementation.UserSelectable w;
        private boolean active;

        public Desc(WhiteListQueryImplementation.UserSelectable w, boolean active) {
            this.w = w;
            this.active = active;
        }
    }

    public static class Factory
    implements ProjectCustomizer.CompositeCategoryProvider {
        private static final String CATEGORY_WHITELIST = "WhiteList";
        private final boolean alwaysShowWhiteListPanel;

        public Factory(boolean showWhiteListPanel) {
            this.alwaysShowWhiteListPanel = showWhiteListPanel;
        }

        public ProjectCustomizer.Category createCategory(Lookup context) {
            Project p = (Project)context.lookup(Project.class);
            if (p == null) {
                return null;
            }
            if (WhiteListLookupProvider.getUserSelectableWhiteLists().isEmpty()) {
                return null;
            }
            if (!WhiteListLookupProvider.isWhiteListPanelEnabled(p) && !this.alwaysShowWhiteListPanel) {
                return null;
            }
            return ProjectCustomizer.Category.create((String)"WhiteList", (String)NbBundle.getMessage(WhiteListCategoryPanel.class, (String)"LBL_CategoryWhitelist"), (Image)null, (ProjectCustomizer.Category[])new ProjectCustomizer.Category[0]);
        }

        public JComponent createComponent(ProjectCustomizer.Category category, Lookup context) {
            Project p = (Project)context.lookup(Project.class);
            assert (p != null);
            WhiteListCategoryPanel customizerPanel = new WhiteListCategoryPanel(p);
            category.setStoreListener((ActionListener)customizerPanel);
            return customizerPanel;
        }
    }

}

