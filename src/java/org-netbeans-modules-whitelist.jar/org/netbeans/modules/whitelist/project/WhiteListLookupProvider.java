/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ProjectManager
 *  org.netbeans.api.project.ProjectUtils
 *  org.netbeans.spi.project.LookupProvider
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.modules.whitelist.project;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.prefs.Preferences;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.whitelist.WhiteListQuery;
import org.netbeans.spi.project.LookupProvider;
import org.netbeans.spi.whitelist.WhiteListQueryImplementation;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

public class WhiteListLookupProvider
implements LookupProvider {
    private static final String WHITELISTS_PATH = "org-netbeans-api-java/whitelists/";
    private static final String PROP_WHITELIST_ENABLED = "whitelist-enabled";
    private static final String PROP_WHITELIST = "whitelist-";
    private static final Map<Project, Reference<WhiteListLookup>> lookupCache = Collections.synchronizedMap(new WeakHashMap());

    @NonNull
    public Lookup createAdditionalLookup(Lookup baseContext) {
        Project p = (Project)baseContext.lookup(Project.class);
        assert (p != null);
        return WhiteListLookupProvider.getEnabledUserSelectableWhiteLists(p);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @NonNull
    static Lookup getEnabledUserSelectableWhiteLists(@NonNull Project p) {
        Map<Project, Reference<WhiteListLookup>> map = lookupCache;
        synchronized (map) {
            WhiteListLookup lkp;
            Reference<WhiteListLookup> lkpRef = lookupCache.get((Object)p);
            if (lkpRef == null || (lkp = lkpRef.get()) == null) {
                lkp = new WhiteListLookup(p);
                lookupCache.put(p, new WeakReference<WhiteListLookup>(lkp));
            }
            return lkp;
        }
    }

    static Collection<? extends WhiteListQueryImplementation.UserSelectable> getUserSelectableWhiteLists() {
        return Lookups.forPath((String)"org-netbeans-api-java/whitelists/").lookupResult(WhiteListQueryImplementation.UserSelectable.class).allInstances();
    }

    static boolean isWhiteListPanelEnabled(@NonNull Project p) {
        Preferences prefs = ProjectUtils.getPreferences((Project)p, WhiteListQuery.class, (boolean)true);
        return prefs.getBoolean("whitelist-enabled", false);
    }

    public static void enableWhiteListInProject(@NonNull Project p, final @NonNull String whiteListId, final boolean enable) {
        WhiteListLookup lkp;
        final Preferences prefs = ProjectUtils.getPreferences((Project)p, WhiteListQuery.class, (boolean)true);
        ProjectManager.mutex().writeAccess(new Runnable(){

            @Override
            public void run() {
                prefs.putBoolean("whitelist-" + whiteListId, enable);
                if (enable) {
                    prefs.putBoolean("whitelist-enabled", true);
                }
            }
        });
        Reference<WhiteListLookup> lkpRef = lookupCache.get((Object)p);
        if (lkpRef != null && (lkp = lkpRef.get()) != null) {
            lkp.updateLookup();
        }
    }

    public static boolean isWhiteListEnabledInProject(@NonNull Project p, @NonNull String whiteListId) {
        Preferences prefs = ProjectUtils.getPreferences((Project)p, WhiteListQuery.class, (boolean)true);
        return prefs.getBoolean("whitelist-" + whiteListId, false);
    }

    private static class WhiteListLookup
    extends ProxyLookup {
        private Project p;
        private final AtomicBoolean initialized = new AtomicBoolean();

        public WhiteListLookup(Project p) {
            this.p = p;
        }

        protected void beforeLookup(Lookup.Template<?> template) {
            if (WhiteListQueryImplementation.class.isAssignableFrom(template.getType()) && !this.initialized.get()) {
                WhiteListQueryImplementation.UserSelectable[] queries = this.createQueries();
                if (!this.initialized.get()) {
                    this.setLookups(new Lookup[]{Lookups.fixed((Object[])queries)});
                    this.initialized.set(true);
                }
            }
            super.beforeLookup(template);
        }

        private void updateLookup() {
            this.setLookups(new Lookup[]{Lookups.fixed((Object[])this.createQueries())});
        }

        @NonNull
        private WhiteListQueryImplementation.UserSelectable[] createQueries() {
            ArrayList<WhiteListQueryImplementation.UserSelectable> impls = new ArrayList<WhiteListQueryImplementation.UserSelectable>();
            for (WhiteListQueryImplementation.UserSelectable w : Lookups.forPath((String)"org-netbeans-api-java/whitelists/").lookupAll(WhiteListQueryImplementation.UserSelectable.class)) {
                if (!WhiteListLookupProvider.isWhiteListEnabledInProject(this.p, w.getId())) continue;
                impls.add(w);
            }
            return impls.toArray(new WhiteListQueryImplementation.UserSelectable[impls.size()]);
        }
    }

}

