/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.java.source.ElementHandle
 *  org.openide.filesystems.FileObject
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.whitelist;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventListener;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.whitelist.WhiteListQuery;
import org.netbeans.spi.whitelist.WhiteListQueryImplementation;
import org.openide.filesystems.FileObject;
import org.openide.util.ChangeSupport;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.WeakListeners;

public class WhiteListQueryImplementationMerged
implements WhiteListQueryImplementation {
    private Lookup lkp;
    private final Map<FileObject, Reference<WhiteListQueryImplementation.WhiteListImplementation>> canonicalCache;

    public WhiteListQueryImplementationMerged(Lookup lkp) {
        this.lkp = lkp;
        this.canonicalCache = new WeakHashMap<FileObject, Reference<WhiteListQueryImplementation.WhiteListImplementation>>();
    }

    @Override
    public synchronized WhiteListQueryImplementation.WhiteListImplementation getWhiteList(FileObject file) {
        WhiteListQueryImplementation.WhiteListImplementation wl;
        Reference<WhiteListQueryImplementation.WhiteListImplementation> ref = this.canonicalCache.get((Object)file);
        WhiteListQueryImplementation.WhiteListImplementation whiteListImplementation = wl = ref == null ? null : ref.get();
        if (wl != null) {
            return wl;
        }
        Lookup.Result lr = this.lkp.lookupResult(WhiteListQueryImplementation.class);
        boolean empty = true;
        for (WhiteListQueryImplementation impl : lr.allInstances()) {
            WhiteListQueryImplementation.WhiteListImplementation i = impl.getWhiteList(file);
            if (i == null) continue;
            empty = false;
            break;
        }
        if (empty) {
            return null;
        }
        wl = new WhiteListImplementationMerged(lr, file);
        this.canonicalCache.put(file, new WeakReference<WhiteListQueryImplementation.WhiteListImplementation>(wl));
        return wl;
    }

    private static class WhiteListImplementationMerged
    implements WhiteListQueryImplementation.WhiteListImplementation,
    ChangeListener,
    LookupListener {
        private final Lookup.Result<WhiteListQueryImplementation> lr;
        private final FileObject file;
        private final ChangeSupport changeSupport;
        private Map<WhiteListQueryImplementation.WhiteListImplementation, ChangeListener> cache;

        public WhiteListImplementationMerged(@NonNull Lookup.Result<WhiteListQueryImplementation> lr, @NonNull FileObject file) {
            this.lr = lr;
            this.file = file;
            this.changeSupport = new ChangeSupport((Object)this);
            this.lr.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this, this.lr));
        }

        @NonNull
        @Override
        public WhiteListQuery.Result check(@NonNull ElementHandle<?> element, @NonNull WhiteListQuery.Operation operation) {
            ArrayList<? extends WhiteListQuery.RuleDescription> rules = new ArrayList<WhiteListQuery.RuleDescription>();
            for (WhiteListQueryImplementation.WhiteListImplementation impl : this.getWhiteLists()) {
                WhiteListQuery.Result r = impl.check(element, operation);
                if (r == null || r.isAllowed()) continue;
                rules.addAll(r.getViolatedRules());
            }
            if (rules.isEmpty()) {
                return new WhiteListQuery.Result();
            }
            return new WhiteListQuery.Result(rules);
        }

        @Override
        public void addChangeListener(@NonNull ChangeListener listener) {
            this.changeSupport.addChangeListener(listener);
        }

        @Override
        public void removeChangeListener(@NonNull ChangeListener listener) {
            this.changeSupport.removeChangeListener(listener);
        }

        @Override
        public void stateChanged(ChangeEvent event) {
            this.changeSupport.fireChange();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void resultChanged(LookupEvent ev) {
            WhiteListImplementationMerged whiteListImplementationMerged = this;
            synchronized (whiteListImplementationMerged) {
                this.cache = null;
            }
            this.changeSupport.fireChange();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private Iterable<WhiteListQueryImplementation.WhiteListImplementation> getWhiteLists() {
            WhiteListImplementationMerged whiteListImplementationMerged = this;
            synchronized (whiteListImplementationMerged) {
                if (this.cache != null) {
                    return this.cache.keySet();
                }
            }
            IdentityHashMap<WhiteListQueryImplementation.WhiteListImplementation, ChangeListener> map = new IdentityHashMap<WhiteListQueryImplementation.WhiteListImplementation, ChangeListener>();
            for (WhiteListQueryImplementation wlq : this.lr.allInstances()) {
                WhiteListQueryImplementation.WhiteListImplementation wl = wlq.getWhiteList(this.file);
                if (wl == null) continue;
                ChangeListener cl = WeakListeners.change((ChangeListener)this, (Object)wl);
                wl.addChangeListener(cl);
                map.put(wl, cl);
            }
            WhiteListImplementationMerged i$ = this;
            synchronized (i$) {
                if (this.cache == null) {
                    this.cache = map;
                }
                return this.cache.keySet();
            }
        }
    }

}

