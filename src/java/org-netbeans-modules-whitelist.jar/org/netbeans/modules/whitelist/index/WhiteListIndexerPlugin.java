/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.LineMap
 *  com.sun.source.tree.Tree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.Trees
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaIndexerPlugin
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaIndexerPlugin$Factory
 *  org.netbeans.modules.parsing.lucene.support.DocumentIndex
 *  org.netbeans.modules.parsing.lucene.support.Index
 *  org.netbeans.modules.parsing.lucene.support.Index$Status
 *  org.netbeans.modules.parsing.lucene.support.IndexDocument
 *  org.netbeans.modules.parsing.lucene.support.IndexManager
 *  org.netbeans.modules.parsing.lucene.support.IndexManager$Action
 *  org.netbeans.modules.parsing.lucene.support.Queries
 *  org.netbeans.modules.parsing.lucene.support.Queries$QueryKind
 *  org.netbeans.modules.parsing.spi.indexing.Indexable
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.whitelist.index;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.LineMap;
import com.sun.source.tree.Tree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.Trees;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.whitelist.WhiteListQuery;
import org.netbeans.api.whitelist.index.WhiteListIndex;
import org.netbeans.api.whitelist.support.WhiteListSupport;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaIndexerPlugin;
import org.netbeans.modules.parsing.lucene.support.DocumentIndex;
import org.netbeans.modules.parsing.lucene.support.Index;
import org.netbeans.modules.parsing.lucene.support.IndexDocument;
import org.netbeans.modules.parsing.lucene.support.IndexManager;
import org.netbeans.modules.parsing.lucene.support.Queries;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.netbeans.modules.whitelist.index.WhiteListIndexAccessor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public class WhiteListIndexerPlugin
implements JavaIndexerPlugin {
    private static final String WHITE_LIST_INDEX = "whitelist";
    private static final String RULE_MSG = "msg";
    private static final String RULE_NAME = "name";
    private static final String WHITE_LIST_ID = "whitelist";
    private static final String LINE = "line";
    private static Map<URL, File> roots2whiteListDirs = new ConcurrentHashMap<URL, File>();
    private final URL root;
    private final File whiteListDir;
    private final WhiteListQuery.WhiteList whiteList;
    private final DocumentIndex index;

    private WhiteListIndexerPlugin(@NonNull URL root, @NonNull WhiteListQuery.WhiteList whiteList, @NonNull File whiteListDir) throws IOException {
        assert (root != null);
        assert (whiteList != null);
        assert (whiteListDir != null);
        this.root = root;
        this.whiteList = whiteList;
        this.whiteListDir = whiteListDir;
        this.index = IndexManager.createDocumentIndex((File)whiteListDir);
    }

    public void process(@NonNull CompilationUnitTree toProcess, @NonNull Indexable indexable, @NonNull Lookup services) {
        Trees trees = (Trees)services.lookup(Trees.class);
        assert (trees != null);
        Map<? extends Tree, ? extends WhiteListQuery.Result> problems = WhiteListSupport.getWhiteListViolations(toProcess, this.whiteList, trees, null);
        assert (problems != null);
        LineMap lm = toProcess.getLineMap();
        SourcePositions sp = trees.getSourcePositions();
        for (Map.Entry<? extends Tree, ? extends WhiteListQuery.Result> p : problems.entrySet()) {
            int ln;
            assert (!p.getValue().isAllowed());
            int start = (int)sp.getStartPosition(toProcess, p.getKey());
            if (start < 0 || (ln = (int)lm.getLineNumber((long)start)) < 0) continue;
            for (WhiteListQuery.RuleDescription rule : p.getValue().getViolatedRules()) {
                IndexDocument doc = IndexManager.createDocument((String)indexable.getRelativePath());
                String wlID = rule.getWhiteListID();
                if (wlID != null) {
                    doc.addPair("whitelist", wlID, true, true);
                }
                doc.addPair("name", rule.getRuleName(), true, true);
                doc.addPair("msg", rule.getRuleDescription(), false, true);
                doc.addPair("line", Integer.toString(ln), false, true);
                this.index.addDocument(doc);
            }
        }
    }

    public void delete(@NonNull Indexable indexable) {
        this.index.removeDocument(indexable.getRelativePath());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void finish() {
        try {
            this.index.store(true);
            roots2whiteListDirs.put(this.root, this.whiteListDir);
            WhiteListIndexAccessor.getInstance().refresh(this.root);
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        finally {
            try {
                this.index.close();
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
    }

    @CheckForNull
    private static DocumentIndex getIndex(@NonNull FileObject root) {
        try {
            File whiteListFolder = roots2whiteListDirs.get(root.getURL());
            if (whiteListFolder != null) {
                DocumentIndex index = IndexManager.createDocumentIndex((File)whiteListFolder);
                return index.getStatus() == Index.Status.VALID ? index : null;
            }
        }
        catch (IOException ioe) {
            Exceptions.printStackTrace((Throwable)ioe);
        }
        return null;
    }

    @NonNull
    public static Collection<? extends WhiteListIndex.Problem> getWhiteListViolations(final @NonNull FileObject root, final @NullAllowed FileObject resource) {
        final ArrayList result = new ArrayList();
        try {
            IndexManager.priorityAccess((IndexManager.Action)new IndexManager.Action<Void>(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                public Void run() throws IOException, InterruptedException {
                    DocumentIndex index = WhiteListIndexerPlugin.getIndex(root);
                    if (index != null) {
                        try {
                            for (IndexDocument doc : index.findByPrimaryKey(resource == null ? "" : FileUtil.getRelativePath((FileObject)root, (FileObject)resource), Queries.QueryKind.PREFIX, new String[0])) {
                                try {
                                    String key = doc.getPrimaryKey();
                                    String wlName = doc.getValue("whitelist");
                                    String ruleName = doc.getValue("name");
                                    assert (ruleName != null);
                                    String ruleDesc = doc.getValue("msg");
                                    assert (ruleDesc != null);
                                    int line = Integer.parseInt(doc.getValue("line"));
                                    WhiteListQuery.Result wr = new WhiteListQuery.Result(Collections.singletonList(new WhiteListQuery.RuleDescription(ruleName, ruleDesc, wlName)));
                                    result.add(WhiteListIndexAccessor.getInstance().createProblem(wr, root, key, line));
                                }
                                catch (ArithmeticException ae) {
                                    Exceptions.printStackTrace((Throwable)ae);
                                }
                            }
                        }
                        finally {
                            index.close();
                        }
                    }
                    return null;
                }
            });
        }
        catch (IOException e) {
            Exceptions.printStackTrace((Throwable)e);
        }
        catch (InterruptedException e) {
            Exceptions.printStackTrace((Throwable)e);
        }
        return result;
    }

    public static class Factory
    implements JavaIndexerPlugin.Factory {
        public JavaIndexerPlugin create(URL root, FileObject cacheFolder) {
            try {
                FileObject whiteListFolder;
                File whiteListDir = (File)roots2whiteListDirs.get(root);
                if (whiteListDir == null && (whiteListDir = FileUtil.toFile((FileObject)(whiteListFolder = FileUtil.createFolder((FileObject)cacheFolder, (String)"whitelist")))) == null) {
                    return null;
                }
                FileObject rootFo = URLMapper.findFileObject((URL)root);
                if (rootFo == null) {
                    Factory.delete(whiteListDir);
                    return null;
                }
                WhiteListQuery.WhiteList wl = WhiteListQuery.getWhiteList(rootFo);
                if (wl == null) {
                    return null;
                }
                return new WhiteListIndexerPlugin(root, wl, whiteListDir);
            }
            catch (IOException ioe) {
                Exceptions.printStackTrace((Throwable)ioe);
                return null;
            }
        }

        private static void delete(final @NonNull File folder) throws IOException {
            try {
                IndexManager.writeAccess((IndexManager.Action)new IndexManager.Action<Void>(){

                    public Void run() throws IOException, InterruptedException {
                        Factory.deleteImpl(folder);
                        return null;
                    }
                });
            }
            catch (InterruptedException ex) {
                throw new IOException(ex);
            }
        }

        private static void deleteImpl(File folder) {
            File[] children = folder.listFiles();
            if (children != null) {
                for (File child : children) {
                    if (child.isDirectory()) {
                        Factory.deleteImpl(child);
                    }
                    child.delete();
                }
            }
        }

    }

}

