/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.whitelist.index;

import java.net.URL;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.whitelist.WhiteListQuery;
import org.netbeans.api.whitelist.index.WhiteListIndex;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.Parameters;

public abstract class WhiteListIndexAccessor {
    private static volatile WhiteListIndexAccessor instance;

    @NonNull
    public static synchronized WhiteListIndexAccessor getInstance() {
        if (instance == null) {
            try {
                Class.forName(WhiteListIndex.class.getName(), true, WhiteListIndexAccessor.class.getClassLoader());
                assert (instance != null);
            }
            catch (ClassNotFoundException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return instance;
    }

    public static void setInstance(@NonNull WhiteListIndexAccessor _instance) {
        Parameters.notNull((CharSequence)"_instance", (Object)_instance);
        instance = _instance;
    }

    public abstract void refresh(@NonNull URL var1);

    public abstract WhiteListIndex.Problem createProblem(@NonNull WhiteListQuery.Result var1, @NonNull FileObject var2, @NonNull String var3, int var4);
}

