/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.project.LookupMerger
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.whitelist;

import org.netbeans.modules.whitelist.WhiteListQueryImplementationMerged;
import org.netbeans.spi.project.LookupMerger;
import org.netbeans.spi.whitelist.WhiteListQueryImplementation;
import org.openide.util.Lookup;

public class WhiteListQueryMerger
implements LookupMerger<WhiteListQueryImplementation> {
    public Class<WhiteListQueryImplementation> getMergeableClass() {
        return WhiteListQueryImplementation.class;
    }

    public WhiteListQueryImplementation merge(Lookup lookup) {
        return new WhiteListQueryImplementationMerged(lookup);
    }
}

