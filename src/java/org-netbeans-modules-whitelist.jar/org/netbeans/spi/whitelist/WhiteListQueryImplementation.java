/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.java.source.ElementHandle
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.whitelist;

import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.whitelist.WhiteListQuery;
import org.openide.filesystems.FileObject;

public interface WhiteListQueryImplementation {
    @CheckForNull
    public WhiteListImplementation getWhiteList(@NonNull FileObject var1);

    public static interface WhiteListImplementation {
        @NonNull
        public WhiteListQuery.Result check(@NonNull ElementHandle<?> var1, @NonNull WhiteListQuery.Operation var2);

        public void addChangeListener(@NonNull ChangeListener var1);

        public void removeChangeListener(@NonNull ChangeListener var1);
    }

    public static interface UserSelectable
    extends WhiteListQueryImplementation {
        public String getDisplayName();

        public String getId();
    }

}

