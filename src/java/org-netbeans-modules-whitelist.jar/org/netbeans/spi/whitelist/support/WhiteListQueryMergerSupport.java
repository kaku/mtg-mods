/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.project.LookupMerger
 */
package org.netbeans.spi.whitelist.support;

import org.netbeans.modules.whitelist.WhiteListQueryMerger;
import org.netbeans.spi.project.LookupMerger;
import org.netbeans.spi.whitelist.WhiteListQueryImplementation;

public class WhiteListQueryMergerSupport {
    public static LookupMerger<WhiteListQueryImplementation> createWhiteListQueryMerger() {
        return new WhiteListQueryMerger();
    }
}

