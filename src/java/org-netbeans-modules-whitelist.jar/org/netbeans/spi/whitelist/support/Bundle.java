/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.spi.whitelist.support;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String DESC_Class(Object arg0, Object arg1) {
        return NbBundle.getMessage(Bundle.class, (String)"DESC_Class", (Object)arg0, (Object)arg1);
    }

    static String DESC_Cons(Object arg0, Object arg1) {
        return NbBundle.getMessage(Bundle.class, (String)"DESC_Cons", (Object)arg0, (Object)arg1);
    }

    static String DESC_Meth(Object arg0, Object arg1) {
        return NbBundle.getMessage(Bundle.class, (String)"DESC_Meth", (Object)arg0, (Object)arg1);
    }

    static String RULE_Class() {
        return NbBundle.getMessage(Bundle.class, (String)"RULE_Class");
    }

    static String RULE_Cons() {
        return NbBundle.getMessage(Bundle.class, (String)"RULE_Cons");
    }

    static String RULE_Meth() {
        return NbBundle.getMessage(Bundle.class, (String)"RULE_Meth");
    }

    static String TXT_UnknownWhiteList() {
        return NbBundle.getMessage(Bundle.class, (String)"TXT_UnknownWhiteList");
    }

    private void Bundle() {
    }
}

