/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.SourceUtils
 *  org.openide.util.Parameters
 *  org.openide.util.Union2
 */
package org.netbeans.spi.whitelist.support;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.whitelist.WhiteListQuery;
import org.netbeans.spi.whitelist.WhiteListQueryImplementation;
import org.netbeans.spi.whitelist.support.Bundle;
import org.openide.util.Parameters;
import org.openide.util.Union2;

final class WhiteListImplementationBuilder {
    private static final byte INVOKE = 1;
    private static final byte OVERRIDE = 2;
    private Model model = new Model();

    private WhiteListImplementationBuilder() {
    }

    @NonNull
    public WhiteListImplementationBuilder setDisplayName(@NonNull String displayName) {
        Parameters.notNull((CharSequence)"displayName", (Object)displayName);
        this.checkPreconditions();
        this.model.setDisplayName(displayName);
        return this;
    }

    @NonNull
    public WhiteListImplementationBuilder addCheckedPackage(@NonNull String checkedPackage) {
        Parameters.notNull((CharSequence)"checkedPackage", (Object)checkedPackage);
        this.checkPreconditions();
        this.model.addCheckedPackage(checkedPackage);
        return this;
    }

    @NonNull
    public WhiteListImplementationBuilder addInvocableClass(@NonNull String classBinaryName) {
        Parameters.notNull((CharSequence)"classBinaryName", (Object)classBinaryName);
        this.checkPreconditions();
        this.model.addClass(classBinaryName, 1);
        return this;
    }

    @NonNull
    public /* varargs */ WhiteListImplementationBuilder addInvocableMethod(@NonNull String classBinaryName, @NonNull String methodName, @NonNull String ... argumentTypes) {
        Parameters.notNull((CharSequence)"classBinaryName", (Object)classBinaryName);
        Parameters.notNull((CharSequence)"methodName", (Object)methodName);
        Parameters.notNull((CharSequence)"argumentTypes", (Object)argumentTypes);
        this.checkPreconditions();
        this.model.addMethod(classBinaryName, methodName, argumentTypes, 1);
        return this;
    }

    @NonNull
    public WhiteListImplementationBuilder addSubclassableClass(@NonNull String classBinaryName) {
        Parameters.notNull((CharSequence)"classBinaryName", (Object)classBinaryName);
        this.checkPreconditions();
        this.model.addClass(classBinaryName, 2);
        return this;
    }

    @NonNull
    public /* varargs */ WhiteListImplementationBuilder addOverridableMethod(@NonNull String classBinaryName, @NonNull String methodName, @NonNull String ... argumentTypes) {
        Parameters.notNull((CharSequence)"classBinaryName", (Object)classBinaryName);
        Parameters.notNull((CharSequence)"methodName", (Object)methodName);
        Parameters.notNull((CharSequence)"argumentTypes", (Object)argumentTypes);
        this.checkPreconditions();
        this.model.addMethod(classBinaryName, methodName, argumentTypes, 2);
        return this;
    }

    @NonNull
    public WhiteListImplementationBuilder addAllowedPackage(@NonNull String packageName, boolean includingSubpackages) {
        Parameters.notNull((CharSequence)"checkedPackage", (Object)packageName);
        this.checkPreconditions();
        this.model.addAllowedPackage(includingSubpackages ? packageName + "." : packageName);
        return this;
    }

    @NonNull
    public WhiteListImplementationBuilder addDisallowedPackage(@NonNull String packageName, boolean includingSubpackages) {
        Parameters.notNull((CharSequence)"checkedPackage", (Object)packageName);
        this.checkPreconditions();
        this.model.addDisallowedPackage(includingSubpackages ? packageName + "." : packageName);
        return this;
    }

    @NonNull
    public WhiteListQueryImplementation.WhiteListImplementation build() {
        WhiteList result = new WhiteList(this.model.build());
        this.model = null;
        return result;
    }

    @NonNull
    public static WhiteListImplementationBuilder create() {
        return new WhiteListImplementationBuilder();
    }

    private void checkPreconditions() {
        if (this.model == null) {
            throw new IllegalStateException("Modifying already built builder, create a new one");
        }
    }

    static class PackedNames
    implements Names {
        private static final int DEF_SLOTS = 1024;
        private static final int DEF_INIT_BYTES = 65536;
        private final int hashMask;
        private Entry[] slots = new Entry[1024];
        private byte[] storage;
        private int pos;

        PackedNames() {
            this.hashMask = this.slots.length - 1;
            this.storage = new byte[65536];
        }

        @Override
        public Integer putName(@NonNull String name) {
            assert (name != null);
            int hc = name.hashCode() & this.hashMask;
            byte[] sbytes = this.decode(name);
            Entry entry = this.slots[hc];
            while (entry != null && !this.contentEquals(entry, sbytes)) {
                entry = entry.next;
            }
            if (entry == null) {
                if (this.storage.length < this.pos + sbytes.length) {
                    int newStorSize;
                    for (newStorSize = this.storage.length; newStorSize < this.pos + sbytes.length; newStorSize <<= 1) {
                    }
                    byte[] tmpStorage = new byte[newStorSize];
                    System.arraycopy(this.storage, 0, tmpStorage, 0, this.storage.length);
                    this.storage = tmpStorage;
                }
                System.arraycopy(sbytes, 0, this.storage, this.pos, sbytes.length);
                this.slots[hc] = entry = new Entry(this.pos, sbytes.length, this.slots[hc]);
                this.pos += sbytes.length == 0 ? 1 : sbytes.length;
            }
            return entry.pos;
        }

        @Override
        public Integer getName(@NonNull String name) {
            assert (name != null);
            int hc = name.hashCode() & this.hashMask;
            byte[] sbytes = this.decode(name);
            Entry entry = this.slots[hc];
            while (entry != null && !this.contentEquals(entry, sbytes)) {
                entry = entry.next;
            }
            return entry == null ? null : Integer.valueOf(entry.pos);
        }

        private boolean contentEquals(@NonNull Entry entry, @NonNull byte[] content) {
            assert (entry != null);
            assert (content != null);
            if (entry.length != content.length) {
                return false;
            }
            for (int i = 0; i < entry.length; ++i) {
                if (content[i] == this.storage[entry.pos + i]) continue;
                return false;
            }
            return true;
        }

        private byte[] decode(String str) {
            try {
                return str.getBytes("UTF-8");
            }
            catch (UnsupportedEncodingException e) {
                throw new IllegalStateException("No UTF-8 supported");
            }
        }

        private static class Entry {
            private int pos;
            private int length;
            private Entry next;

            private Entry(int pos, int length, @NullAllowed Entry next) {
                this.pos = pos;
                this.length = length;
                this.next = next;
            }
        }

    }

    static class SimpleNames
    implements Names {
        private final Map<String, Integer> names = new HashMap<String, Integer>();
        private int counter = Integer.MIN_VALUE;

        SimpleNames() {
        }

        @NonNull
        @Override
        public Integer putName(@NonNull String name) {
            assert (name != null);
            Integer result = this.names.get(name);
            if (result == null) {
                result = this.counter++;
                this.names.put(name, result);
            }
            return result;
        }

        @CheckForNull
        @Override
        public Integer getName(@NonNull String name) {
            assert (name != null);
            return this.names.get(name);
        }
    }

    private static interface Names {
        @NonNull
        public Integer putName(@NonNull String var1);

        @CheckForNull
        public Integer getName(@NonNull String var1);
    }

    private static final class Model {
        private static final String DEF_NAMES = PackedNames.class.getName();
        private String whiteListName;
        private Union2<StringBuilder, Pattern> checkedPkgs;
        private List<String> allowedPackages;
        private List<String> disallowedPackages;
        private final Names names;
        private final IntermediateCacheNode<IntermediateCacheNode<IntermediateCacheNode<IntermediateCacheNode<CacheNode>>>> root = new IntermediateCacheNode();

        private Model() {
            try {
                this.names = (Names)Class.forName(System.getProperty("WhiteListBuilder.names", DEF_NAMES)).newInstance();
            }
            catch (InstantiationException ex) {
                throw new IllegalStateException("Cannot instantiate names", ex);
            }
            catch (IllegalAccessException ex) {
                throw new IllegalStateException("Cannot instantiate names", ex);
            }
            catch (ClassNotFoundException ex) {
                throw new IllegalStateException("Cannot instantiate names", ex);
            }
            this.checkedPkgs = Union2.createFirst((Object)new StringBuilder());
            this.allowedPackages = new ArrayList<String>();
            this.disallowedPackages = new ArrayList<String>();
        }

        void addCheckedPackage(@NonNull String pkg) {
            if (((StringBuilder)this.checkedPkgs.first()).length() > 0) {
                ((StringBuilder)this.checkedPkgs.first()).append('|');
            }
            ((StringBuilder)this.checkedPkgs.first()).append(Pattern.quote(pkg + '.')).append(".*");
        }

        void addAllowedPackage(@NonNull String pkg) {
            this.allowedPackages.add(pkg);
        }

        void addDisallowedPackage(@NonNull String pkg) {
            this.disallowedPackages.add(pkg);
        }

        void addClass(@NonNull String binaryName, byte mode) {
            String[] pkgNamePair = this.splitName(binaryName, '/');
            Integer pkgId = this.names.putName(Model.folderToPackage(pkgNamePair[0]));
            Integer clsId = this.names.putName(new String(pkgNamePair[1]));
            IntermediateCacheNode pkgNode = this.root.putIfAbsent(pkgId, new IntermediateCacheNode());
            IntermediateCacheNode clsNode = pkgNode.putIfAbsent(clsId, new IntermediateCacheNode());
            clsNode.state = (byte)(clsNode.state | mode);
        }

        void addMethod(@NonNull String clsBinaryName, @NonNull String methodName, @NonNull String[] argTypes, @NonNull byte mode) {
            String[] pkgNamePair = this.splitName(clsBinaryName, '/');
            Integer pkgId = this.names.putName(Model.folderToPackage(pkgNamePair[0]));
            Integer clsId = this.names.putName(new String(pkgNamePair[1]));
            Integer methodNameId = this.names.putName(methodName);
            Integer metodSigId = this.names.putName(this.vmSignature(argTypes));
            IntermediateCacheNode pkgNode = this.root.putIfAbsent(pkgId, new IntermediateCacheNode());
            IntermediateCacheNode clsNode = pkgNode.putIfAbsent(clsId, new IntermediateCacheNode());
            IntermediateCacheNode<CacheNode> methodNameNode = clsNode.putIfAbsent(methodNameId, new IntermediateCacheNode());
            CacheNode methodSigNode = methodNameNode.putIfAbsent(metodSigId, new CacheNode());
            methodSigNode.state = (byte)(methodSigNode.state | mode);
        }

        void setDisplayName(String name) {
            this.whiteListName = name;
        }

        Model build() {
            this.checkedPkgs = Union2.createSecond((Object)Pattern.compile(((StringBuilder)this.checkedPkgs.first()).toString()));
            if (this.whiteListName == null) {
                this.whiteListName = Bundle.TXT_UnknownWhiteList();
            }
            return this;
        }

        private boolean isThere(List<String> packages, String pkg) {
            String pkg2 = pkg + ".";
            for (String s : packages) {
                if (!pkg2.startsWith(s)) continue;
                if (s.endsWith(".")) {
                    return true;
                }
                if (!pkg.equals(s)) continue;
                return true;
            }
            return false;
        }

        boolean isAllowed(@NonNull ElementHandle<?> element, byte mode) {
            String[] vmSignatures = SourceUtils.getJVMSignature(element);
            String[] pkgNamePair = this.splitName(vmSignatures[0], '.');
            if (this.isThere(this.allowedPackages, pkgNamePair[0])) {
                return true;
            }
            if (this.isThere(this.disallowedPackages, pkgNamePair[0])) {
                return false;
            }
            if (!((Pattern)this.checkedPkgs.second()).matcher(pkgNamePair[0] + '.').matches()) {
                return true;
            }
            Integer pkgId = this.names.getName(pkgNamePair[0]);
            Integer clsId = this.names.getName(pkgNamePair[1]);
            IntermediateCacheNode<IntermediateCacheNode<IntermediateCacheNode<CacheNode>>> pkgNode = this.root.get(pkgId);
            if (pkgNode == null) {
                return false;
            }
            IntermediateCacheNode<IntermediateCacheNode<CacheNode>> clsNode = pkgNode.get(clsId);
            if (clsNode == null) {
                return false;
            }
            if ((clsNode.state & mode) == mode) {
                return true;
            }
            if (element.getKind() == ElementKind.METHOD || element.getKind() == ElementKind.CONSTRUCTOR) {
                Integer methodNameId = this.names.getName(vmSignatures[1]);
                Integer methodSigId = this.names.getName(this.paramsOnly(vmSignatures[2]));
                IntermediateCacheNode<CacheNode> methodNameNode = clsNode.get(methodNameId);
                if (methodNameNode == null) {
                    return false;
                }
                CacheNode methodSigNode = methodNameNode.get(methodSigId);
                if (methodSigNode == null) {
                    return false;
                }
                return (methodSigNode.state & mode) == mode;
            }
            if ((element.getKind().isClass() || element.getKind().isInterface()) && clsNode.hasChildren()) {
                return true;
            }
            return false;
        }

        String getDisplayName() {
            return this.whiteListName;
        }

        @NonNull
        private String[] splitName(@NonNull String qName, char separator) {
            String name;
            String pkg;
            int index = qName.lastIndexOf(separator);
            if (index == -1) {
                pkg = "";
                name = qName;
            } else {
                pkg = qName.substring(0, index);
                name = qName.substring(index + 1);
            }
            return new String[]{pkg, name};
        }

        @NonNull
        private String paramsOnly(@NonNull String name) {
            assert (name.charAt(0) == '(');
            int index = name.lastIndexOf(41);
            assert (index > 0);
            return name.substring(1, index);
        }

        @NonNull
        private String vmSignature(@NonNull String[] types) {
            StringBuilder sb = new StringBuilder();
            for (String type : types) {
                this.encodeType(type, sb);
            }
            return sb.toString();
        }

        private void encodeType(@NonNull String type, @NonNull StringBuilder sb) {
            assert (type != null);
            assert (sb != null);
            if ("void".equals(type)) {
                sb.append('V');
            } else if ("boolean".equals(type)) {
                sb.append('Z');
            } else if ("byte".equals(type)) {
                sb.append('B');
            } else if ("short".equals(type)) {
                sb.append('S');
            } else if ("int".equals(type)) {
                sb.append('I');
            } else if ("long".equals(type)) {
                sb.append('J');
            } else if ("char".equals(type)) {
                sb.append('C');
            } else if ("float".equals(type)) {
                sb.append('F');
            } else if ("double".equals(type)) {
                sb.append('D');
            } else if (type.charAt(type.length() - 1) == ']') {
                sb.append('[');
                this.encodeType(type.substring(0, type.length() - 2), sb);
            } else {
                sb.append('L');
                sb.append(type);
                sb.append(';');
            }
        }

        @NonNull
        private static String folderToPackage(@NonNull String folder) {
            return folder.replace('/', '.');
        }

        private static class IntermediateCacheNode<T extends CacheNode>
        extends CacheNode {
            private Map<Integer, T> nodes = new HashMap<Integer, T>();

            private IntermediateCacheNode() {
                super();
            }

            @NonNull
            final T putIfAbsent(@NonNull Integer id, @NonNull T node) {
                assert (id != null);
                assert (node != null);
                CacheNode result = (CacheNode)this.nodes.get(id);
                if (result == null) {
                    result = node;
                    this.nodes.put(id, (CacheNode)result);
                }
                return (T)result;
            }

            @CheckForNull
            final T get(@NullAllowed Integer id) {
                return (T)(id == null ? null : (CacheNode)this.nodes.get(id));
            }

            final boolean hasChildren() {
                return !this.nodes.isEmpty();
            }
        }

        private static class CacheNode {
            byte state;

            private CacheNode() {
            }
        }

    }

    private static final class WhiteList
    implements WhiteListQueryImplementation.WhiteListImplementation {
        private final Model model;

        private WhiteList(@NonNull Model model) {
            assert (model != null);
            this.model = model;
        }

        @Override
        public WhiteListQuery.Result check(@NonNull ElementHandle<?> element, @NonNull WhiteListQuery.Operation operation) {
            assert (element != null);
            assert (operation != null);
            boolean b = this.model.isAllowed(element, 1);
            String ruleName = null;
            String ruleDesc = null;
            if (!b) {
                if (element.getKind().isClass() || element.getKind().isInterface()) {
                    ruleName = Bundle.RULE_Class();
                    ruleDesc = Bundle.DESC_Class(WhiteList.displayName(element), this.model.getDisplayName());
                } else if (element.getKind() == ElementKind.CONSTRUCTOR) {
                    ruleName = Bundle.RULE_Cons();
                    ruleDesc = Bundle.DESC_Cons(WhiteList.displayName(element), this.model.getDisplayName());
                } else {
                    ruleName = Bundle.RULE_Meth();
                    ruleDesc = Bundle.DESC_Meth(WhiteList.displayName(element), this.model.getDisplayName());
                }
                return new WhiteListQuery.Result(Collections.singletonList(new WhiteListQuery.RuleDescription(ruleName, ruleDesc, null)));
            }
            return new WhiteListQuery.Result();
        }

        @Override
        public void addChangeListener(@NonNull ChangeListener listener) {
        }

        @Override
        public void removeChangeListener(@NonNull ChangeListener listener) {
        }

        @NonNull
        private static String displayName(@NonNull ElementHandle<? extends Element> handle) {
            ElementKind kind = handle.getKind();
            String[] vmSig = SourceUtils.getJVMSignature(handle);
            if (kind.isClass() || kind.isInterface()) {
                assert (vmSig.length == 1);
                return WhiteList.cannonicalName(vmSig[0]);
            }
            if (kind == ElementKind.CONSTRUCTOR) {
                assert (vmSig.length == 3);
                StringBuilder sb = new StringBuilder();
                int index = vmSig[2].lastIndexOf(41);
                assert (index > 0);
                sb.append(WhiteList.simpleName(vmSig[0]));
                WhiteList.appendParams(vmSig[2], index, sb);
                return sb.toString();
            }
            if (kind == ElementKind.METHOD) {
                assert (vmSig.length == 3);
                StringBuilder sb = new StringBuilder();
                int index = vmSig[2].lastIndexOf(41);
                assert (index > 0);
                WhiteList.cannonicalName(vmSig[2].substring(index + 1), sb);
                sb.append(' ');
                sb.append(vmSig[1]);
                WhiteList.appendParams(vmSig[2], index, sb);
                return sb.toString();
            }
            throw new UnsupportedOperationException(kind.name());
        }

        private static void appendParams(@NonNull String methodSig, @NonNull int paramsEndIndex, @NonNull StringBuilder sb) {
            sb.append('(');
            int state = 0;
            boolean first = true;
            int i = 1;
            int j = 1;
            block4 : while (j < paramsEndIndex) {
                char la = methodSig.charAt(j);
                switch (state) {
                    case 0: {
                        if (la == 'L') {
                            state = 1;
                            ++j;
                            continue block4;
                        }
                        if (la == '[') {
                            ++j;
                            continue block4;
                        }
                        ++j;
                        if (first) {
                            first = false;
                        } else {
                            sb.append(", ");
                        }
                        WhiteList.cannonicalName(methodSig.substring(i, j), sb);
                        i = j;
                        continue block4;
                    }
                    case 1: {
                        if (la == ';') {
                            ++j;
                            if (first) {
                                first = false;
                            } else {
                                sb.append(", ");
                            }
                            WhiteList.cannonicalName(methodSig.substring(i, j), sb);
                            i = j;
                            state = 0;
                            continue block4;
                        }
                        ++j;
                        continue block4;
                    }
                }
                throw new IllegalStateException();
            }
            sb.append(')');
        }

        @NonNull
        private static String cannonicalName(@NonNull String binaryName) {
            return binaryName.replace('$', '.').replace('/', '.');
        }

        @NonNull
        private static String simpleName(@NonNull String binaryName) {
            int index = Math.max(binaryName.lastIndexOf(46), binaryName.lastIndexOf(36));
            return index == -1 ? binaryName : binaryName.substring(index + 1);
        }

        private static void cannonicalName(@NonNull String type, @NonNull StringBuilder into) {
            char la = type.charAt(0);
            switch (la) {
                case 'V': {
                    into.append("void");
                    break;
                }
                case 'Z': {
                    into.append("boolean");
                    break;
                }
                case 'B': {
                    into.append("byte");
                    break;
                }
                case 'S': {
                    into.append("short");
                    break;
                }
                case 'I': {
                    into.append("int");
                    break;
                }
                case 'J': {
                    into.append("long");
                    break;
                }
                case 'C': {
                    into.append("char");
                    break;
                }
                case 'F': {
                    into.append("float");
                    break;
                }
                case 'D': {
                    into.append("double");
                    break;
                }
                case 'L': {
                    into.append(WhiteList.cannonicalName(type.substring(1, type.length() - 1)));
                    break;
                }
                case '[': {
                    WhiteList.cannonicalName(type.substring(1), into);
                    into.append("[]");
                    break;
                }
                default: {
                    throw new IllegalStateException();
                }
            }
        }
    }

}

