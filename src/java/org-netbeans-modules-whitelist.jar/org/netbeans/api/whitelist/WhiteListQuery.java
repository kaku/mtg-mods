/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.project.Project
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 *  org.openide.util.Parameters
 */
package org.netbeans.api.whitelist;

import java.util.Collections;
import java.util.List;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.project.Project;
import org.netbeans.modules.whitelist.WhiteListQueryImplementationMerged;
import org.netbeans.modules.whitelist.project.WhiteListLookupProvider;
import org.netbeans.spi.whitelist.WhiteListQueryImplementation;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;
import org.openide.util.Parameters;

public final class WhiteListQuery {
    private static final WhiteListQueryImplementation mergedGlobalWhiteLists = new WhiteListQueryImplementationMerged(Lookup.getDefault());

    private WhiteListQuery() {
    }

    @CheckForNull
    public static WhiteList getWhiteList(@NonNull FileObject file) {
        Parameters.notNull((CharSequence)"file", (Object)file);
        WhiteListQueryImplementation.WhiteListImplementation whiteListImpl = mergedGlobalWhiteLists.getWhiteList(file);
        if (whiteListImpl != null) {
            return new WhiteList(whiteListImpl);
        }
        return null;
    }

    public static void enableWhiteListInProject(@NonNull Project project, @NonNull String whiteListId, boolean enable) {
        WhiteListLookupProvider.enableWhiteListInProject(project, whiteListId, enable);
    }

    public static boolean isWhiteListEnabledInProject(@NonNull Project project, @NonNull String whiteListId) {
        return WhiteListLookupProvider.isWhiteListEnabledInProject(project, whiteListId);
    }

    public static final class RuleDescription {
        private final String ruleName;
        private final String ruleDescription;
        private final String whiteListID;

        public RuleDescription(@NonNull String ruleName, @NonNull String ruleDescription, @NullAllowed String whiteListID) {
            this.ruleName = ruleName;
            this.ruleDescription = ruleDescription;
            this.whiteListID = whiteListID;
        }

        @NonNull
        public String getRuleDescription() {
            return this.ruleDescription;
        }

        @NonNull
        public String getRuleName() {
            return this.ruleName;
        }

        @CheckForNull
        public String getWhiteListID() {
            return this.whiteListID;
        }
    }

    public static final class Result {
        private final boolean allowed;
        private final List<? extends RuleDescription> violatedRules;

        public Result() {
            this.allowed = true;
            this.violatedRules = Collections.emptyList();
        }

        public Result(@NonNull List<? extends RuleDescription> violatedRules) {
            this.allowed = false;
            this.violatedRules = violatedRules;
        }

        public boolean isAllowed() {
            return this.allowed;
        }

        @NonNull
        public List<? extends RuleDescription> getViolatedRules() {
            return this.violatedRules;
        }
    }

    public static enum Operation {
        USAGE;
        

        private Operation() {
        }
    }

    public static final class WhiteList {
        private final WhiteListQueryImplementation.WhiteListImplementation impl;

        private WhiteList(@NonNull WhiteListQueryImplementation.WhiteListImplementation impl) {
            Parameters.notNull((CharSequence)"impl", (Object)impl);
            this.impl = impl;
        }

        @NonNull
        public final Result check(@NonNull ElementHandle<?> element, @NonNull Operation operation) {
            Parameters.notNull((CharSequence)"element", element);
            Parameters.notNull((CharSequence)"operation", (Object)((Object)operation));
            return this.impl.check(element, operation);
        }

        public void addChangeListener(@NonNull ChangeListener listener) {
            Parameters.notNull((CharSequence)"listener", (Object)listener);
            this.impl.addChangeListener(listener);
        }

        public void removeChangeListener(@NonNull ChangeListener listener) {
            Parameters.notNull((CharSequence)"listener", (Object)listener);
            this.impl.removeChangeListener(listener);
        }
    }

}

