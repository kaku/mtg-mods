/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodInvocationTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.NewClassTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreePathScanner
 *  com.sun.source.util.Trees
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.source.ElementHandle
 *  org.openide.util.Exceptions
 *  org.openide.util.Parameters
 */
package org.netbeans.api.whitelist.support;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreePathScanner;
import com.sun.source.util.Trees;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.whitelist.WhiteListQuery;
import org.openide.util.Exceptions;
import org.openide.util.Parameters;

public final class WhiteListSupport {
    private static final Logger LOG = Logger.getLogger(WhiteListSupport.class.getName());

    private WhiteListSupport() {
    }

    @CheckForNull
    public static Map<? extends Tree, ? extends WhiteListQuery.Result> getWhiteListViolations(@NonNull CompilationUnitTree unit, @NonNull WhiteListQuery.WhiteList whitelist, @NonNull Trees trees, @NullAllowed Callable<Boolean> cancel) {
        Parameters.notNull((CharSequence)"tree", (Object)unit);
        Parameters.notNull((CharSequence)"whitelist", (Object)whitelist);
        Parameters.notNull((CharSequence)"trees", (Object)trees);
        HashMap result = new HashMap();
        WhiteListScanner scanner = new WhiteListScanner(trees, whitelist, cancel);
        try {
            scanner.scan((Tree)unit, result);
            return result;
        }
        catch (WhiteListScanner.Cancel ce) {
            return null;
        }
    }

    private static class WhiteListScanner
    extends TreePathScanner<Void, Map<Tree, WhiteListQuery.Result>> {
        private final Trees trees;
        private final Callable<Boolean> cancel;
        private final WhiteListQuery.WhiteList whiteList;
        private final ArrayDeque<MethodInvocationTree> methodInvocation;

        WhiteListScanner(Trees trees, WhiteListQuery.WhiteList whiteList, Callable<Boolean> cancel) {
            this.trees = trees;
            this.whiteList = whiteList;
            this.cancel = cancel;
            this.methodInvocation = new ArrayDeque();
        }

        public Void visitMethod(MethodTree node, Map<Tree, WhiteListQuery.Result> p) {
            LOG.log(Level.FINEST, "Visiting {0}", (Object)node);
            this.checkCancel();
            return (Void)TreePathScanner.super.visitMethod(node, p);
        }

        public Void visitClass(ClassTree node, Map<Tree, WhiteListQuery.Result> p) {
            LOG.log(Level.FINEST, "Visiting {0}", (Object)node);
            this.checkCancel();
            return (Void)TreePathScanner.super.visitClass(node, p);
        }

        public Void visitIdentifier(IdentifierTree node, Map<Tree, WhiteListQuery.Result> p) {
            this.handleNode((Tree)node, p);
            return (Void)TreePathScanner.super.visitIdentifier(node, p);
        }

        public Void visitMemberSelect(MemberSelectTree node, Map<Tree, WhiteListQuery.Result> p) {
            this.handleNode((Tree)node, p);
            return (Void)TreePathScanner.super.visitMemberSelect(node, p);
        }

        public Void visitNewClass(NewClassTree node, Map<Tree, WhiteListQuery.Result> p) {
            WhiteListQuery.Result res;
            Element e = this.trees.getElement(this.getCurrentPath());
            if (e != null && !(res = this.whiteList.check(ElementHandle.create((Element)e), WhiteListQuery.Operation.USAGE)).isAllowed()) {
                p.put((Tree)node, res);
            }
            this.scan((Iterable)node.getTypeArguments(), p);
            this.scan((Iterable)node.getArguments(), p);
            this.scan((Tree)node.getClassBody(), p);
            return null;
        }

        public Void visitMethodInvocation(MethodInvocationTree node, Map<Tree, WhiteListQuery.Result> p) {
            this.methodInvocation.offerFirst(node);
            TreePathScanner.super.visitMethodInvocation(node, p);
            this.methodInvocation.removeFirst();
            return null;
        }

        private void handleNode(Tree node, Map<Tree, WhiteListQuery.Result> p) {
            WhiteListQuery.Result res;
            Element e = this.trees.getElement(this.getCurrentPath());
            if (e == null) {
                return;
            }
            ElementKind k = e.getKind();
            ElementHandle eh = null;
            Tree toReport = null;
            if (k.isClass() || k.isInterface()) {
                TypeMirror type = e.asType();
                if (type != null && (type = this.findComponentType(type)).getKind() == TypeKind.DECLARED) {
                    eh = ElementHandle.create((Element)((DeclaredType)type).asElement());
                    toReport = node;
                }
            } else if (!(k != ElementKind.METHOD && k != ElementKind.CONSTRUCTOR || this.methodInvocation.isEmpty())) {
                toReport = (Tree)this.methodInvocation.peekFirst();
                eh = ElementHandle.create((Element)e);
            }
            if (toReport != null && !(res = this.whiteList.check(eh, WhiteListQuery.Operation.USAGE)).isAllowed()) {
                p.put(toReport, res);
            }
        }

        @NonNull
        private TypeMirror findComponentType(@NonNull TypeMirror type) {
            if (type.getKind() != TypeKind.ARRAY) {
                return type;
            }
            return this.findComponentType(((ArrayType)type).getComponentType());
        }

        private void checkCancel() {
            if (this.cancel != null) {
                Boolean vote = null;
                try {
                    vote = this.cancel.call();
                }
                catch (Exception e) {
                    Exceptions.printStackTrace((Throwable)e);
                }
                if (vote == Boolean.TRUE) {
                    throw new Cancel();
                }
            }
        }

        static final class Cancel
        extends RuntimeException {
            Cancel() {
            }
        }

    }

}

