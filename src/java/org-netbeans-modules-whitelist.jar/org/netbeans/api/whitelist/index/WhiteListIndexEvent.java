/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 */
package org.netbeans.api.whitelist.index;

import java.net.URL;
import java.util.EventObject;
import org.netbeans.api.annotations.common.NonNull;
import org.openide.util.Parameters;

public class WhiteListIndexEvent
extends EventObject {
    private final URL root;

    WhiteListIndexEvent(@NonNull Object source, @NonNull URL root) {
        super(source);
        Parameters.notNull((CharSequence)"root", (Object)root);
        this.root = root;
    }

    @NonNull
    public URL getRoot() {
        return this.root;
    }
}

