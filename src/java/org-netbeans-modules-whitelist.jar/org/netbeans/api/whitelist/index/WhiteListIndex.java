/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Parameters
 */
package org.netbeans.api.whitelist.index;

import java.net.URL;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.whitelist.WhiteListQuery;
import org.netbeans.api.whitelist.index.WhiteListIndexEvent;
import org.netbeans.api.whitelist.index.WhiteListIndexListener;
import org.netbeans.modules.whitelist.index.WhiteListIndexAccessor;
import org.netbeans.modules.whitelist.index.WhiteListIndexerPlugin;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Parameters;

public final class WhiteListIndex {
    private static WhiteListIndex instance;
    private final List<WhiteListIndexListener> listeners = new CopyOnWriteArrayList<WhiteListIndexListener>();

    private WhiteListIndex() {
    }

    @NonNull
    public /* varargs */ Collection<? extends Problem> getWhiteListViolations(@NonNull FileObject root, @NullAllowed FileObject file, @NonNull String ... whitelists) throws IllegalArgumentException, UnsupportedOperationException {
        Parameters.notNull((CharSequence)"scope", (Object)root);
        Parameters.notNull((CharSequence)"whitelists", (Object)whitelists);
        if (file != null && !root.equals((Object)file) && !FileUtil.isParentOf((FileObject)root, (FileObject)file)) {
            throw new IllegalArgumentException("The file: " + FileUtil.getFileDisplayName((FileObject)file) + " has to be inside the root: " + FileUtil.getFileDisplayName((FileObject)root));
        }
        if (file != null && !file.isData()) {
            throw new IllegalArgumentException("The file: " + FileUtil.getFileDisplayName((FileObject)file) + " has to be file.");
        }
        return WhiteListIndexerPlugin.getWhiteListViolations(root, file);
    }

    public void addWhiteListIndexListener(@NonNull WhiteListIndexListener listener) {
        Parameters.notNull((CharSequence)"listener", (Object)listener);
        this.listeners.add(listener);
    }

    public void removeWhiteListIndexListener(@NonNull WhiteListIndexListener listener) {
        Parameters.notNull((CharSequence)"listener", (Object)listener);
        this.listeners.remove(listener);
    }

    public static synchronized WhiteListIndex getDefault() {
        if (instance == null) {
            instance = new WhiteListIndex();
        }
        return instance;
    }

    private void fireIndexChange(URL root) {
        WhiteListIndexEvent event = new WhiteListIndexEvent(this, root);
        for (WhiteListIndexListener l : this.listeners) {
            l.indexChanged(event);
        }
    }

    static {
        WhiteListIndexAccessor.setInstance(new WhiteListIndexAccessorImpl());
    }

    private static final class WhiteListIndexAccessorImpl
    extends WhiteListIndexAccessor {
        private WhiteListIndexAccessorImpl() {
        }

        @Override
        public void refresh(@NonNull URL root) {
            WhiteListIndex.getDefault().fireIndexChange(root);
        }

        @NonNull
        @Override
        public Problem createProblem(@NonNull WhiteListQuery.Result result, @NonNull FileObject root, @NonNull String key, int line) {
            return new Problem(result, root, key, line);
        }
    }

    public static final class Problem {
        private final WhiteListQuery.Result result;
        private final FileObject root;
        private final String relPath;
        private final int line;

        private Problem(@NonNull WhiteListQuery.Result result, @NonNull FileObject root, @NonNull String relPath, int line) {
            Parameters.notNull((CharSequence)"result", (Object)result);
            Parameters.notNull((CharSequence)"root", (Object)root);
            Parameters.notNull((CharSequence)"relPath", (Object)relPath);
            this.result = result;
            this.root = root;
            this.relPath = relPath;
            this.line = line;
        }

        @NonNull
        public WhiteListQuery.Result getResult() {
            return this.result;
        }

        @CheckForNull
        public FileObject getFile() {
            return this.root.getFileObject(this.relPath);
        }

        public int getLine() {
            return this.line;
        }
    }

}

