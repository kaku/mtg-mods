/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.api.whitelist.index;

import java.util.EventListener;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.whitelist.index.WhiteListIndexEvent;

public interface WhiteListIndexListener
extends EventListener {
    public void indexChanged(@NonNull WhiteListIndexEvent var1);
}

