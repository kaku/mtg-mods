/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.FoldTemplate
 *  org.netbeans.api.editor.fold.FoldType
 *  org.netbeans.spi.editor.fold.FoldManager
 */
package org.netbeans.editor.ext.java;

import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.editor.ext.java.Bundle;
import org.netbeans.spi.editor.fold.FoldManager;

public abstract class JavaFoldManager
implements FoldManager {
    public static final FoldType INITIAL_COMMENT_FOLD_TYPE = FoldType.INITIAL_COMMENT;
    public static final FoldType IMPORTS_FOLD_TYPE = FoldType.create((String)"import", (String)Bundle.FoldType_Imports(), (org.netbeans.api.editor.fold.FoldTemplate)new org.netbeans.api.editor.fold.FoldTemplate(0, 0, "..."));
    public static final FoldType JAVADOC_FOLD_TYPE = FoldType.DOCUMENTATION.derive("javadoc", Bundle.FoldType_Javadoc(), new org.netbeans.api.editor.fold.FoldTemplate(3, 2, "/**...*/"));
    public static final FoldType CODE_BLOCK_FOLD_TYPE = FoldType.MEMBER.derive("method", Bundle.FoldType_Methods(), new org.netbeans.api.editor.fold.FoldTemplate(1, 1, "{...}"));
    public static final FoldType INNERCLASS_TYPE = FoldType.NESTED.derive("innerclass", "Inner Classes", new org.netbeans.api.editor.fold.FoldTemplate(1, 1, "{...}"));
    private static final String IMPORTS_FOLD_DESCRIPTION = "...";
    private static final String COMMENT_FOLD_DESCRIPTION = "/*...*/";
    private static final String JAVADOC_FOLD_DESCRIPTION = "/**...*/";
    private static final String CODE_BLOCK_FOLD_DESCRIPTION = "{...}";
    @Deprecated
    public static final FoldTemplate INITIAL_COMMENT_FOLD_TEMPLATE = new FoldTemplate(INITIAL_COMMENT_FOLD_TYPE, "/*...*/", 2, 2);
    @Deprecated
    public static final FoldTemplate IMPORTS_FOLD_TEMPLATE = new FoldTemplate(IMPORTS_FOLD_TYPE, "...", 0, 0);
    @Deprecated
    public static final FoldTemplate JAVADOC_FOLD_TEMPLATE = new FoldTemplate(JAVADOC_FOLD_TYPE, "/**...*/", 3, 2);
    @Deprecated
    public static final FoldTemplate CODE_BLOCK_FOLD_TEMPLATE = new FoldTemplate(CODE_BLOCK_FOLD_TYPE, "{...}", 1, 1);
    @Deprecated
    public static final FoldTemplate INNER_CLASS_FOLD_TEMPLATE = new FoldTemplate(INNERCLASS_TYPE, "{...}", 1, 1);

    protected static final class FoldTemplate {
        private FoldType type;
        private String description;
        private int startGuardedLength;
        private int endGuardedLength;

        protected FoldTemplate(FoldType type, String description, int startGuardedLength, int endGuardedLength) {
            this.type = type;
            this.description = description;
            this.startGuardedLength = startGuardedLength;
            this.endGuardedLength = endGuardedLength;
        }

        public FoldType getType() {
            return this.type;
        }

        public String getDescription() {
            return this.description;
        }

        public int getStartGuardedLength() {
            return this.startGuardedLength;
        }

        public int getEndGuardedLength() {
            return this.endGuardedLength;
        }
    }

}

