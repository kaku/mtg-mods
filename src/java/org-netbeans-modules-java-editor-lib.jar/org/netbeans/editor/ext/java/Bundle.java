/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.editor.ext.java;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String FoldType_Imports() {
        return NbBundle.getMessage(Bundle.class, (String)"FoldType_Imports");
    }

    static String FoldType_InnerClasses() {
        return NbBundle.getMessage(Bundle.class, (String)"FoldType_InnerClasses");
    }

    static String FoldType_Javadoc() {
        return NbBundle.getMessage(Bundle.class, (String)"FoldType_Javadoc");
    }

    static String FoldType_Methods() {
        return NbBundle.getMessage(Bundle.class, (String)"FoldType_Methods");
    }

    private void Bundle() {
    }
}

