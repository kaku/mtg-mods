/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseTokenID
 *  org.netbeans.editor.TokenContext
 *  org.netbeans.editor.TokenContextPath
 *  org.netbeans.editor.Utilities
 */
package org.netbeans.editor.ext.java;

import org.netbeans.editor.BaseTokenID;
import org.netbeans.editor.TokenContext;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.Utilities;

public class JavaLayerTokenContext
extends TokenContext {
    public static final int METHOD_ID = 1;
    public static final BaseTokenID METHOD = new BaseTokenID("method", 1);
    public static final JavaLayerTokenContext context = new JavaLayerTokenContext();
    public static final TokenContextPath contextPath = context.getContextPath();

    private JavaLayerTokenContext() {
        super("java-layer-");
        try {
            this.addDeclaredTokenIDs();
        }
        catch (Exception e) {
            Utilities.annotateLoggable((Throwable)e);
        }
    }
}

