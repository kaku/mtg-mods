/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseImageTokenID
 *  org.netbeans.editor.BaseTokenCategory
 *  org.netbeans.editor.BaseTokenID
 *  org.netbeans.editor.TokenCategory
 *  org.netbeans.editor.TokenContext
 *  org.netbeans.editor.TokenContextPath
 *  org.netbeans.editor.TokenID
 *  org.netbeans.editor.Utilities
 */
package org.netbeans.editor.ext.java;

import java.util.HashMap;
import org.netbeans.editor.BaseImageTokenID;
import org.netbeans.editor.BaseTokenCategory;
import org.netbeans.editor.BaseTokenID;
import org.netbeans.editor.TokenCategory;
import org.netbeans.editor.TokenContext;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;
import org.netbeans.editor.Utilities;

public class JavaTokenContext
extends TokenContext {
    public static final int KEYWORDS_ID = 1;
    public static final int OPERATORS_ID = 2;
    public static final int NUMERIC_LITERALS_ID = 3;
    public static final int ERRORS_ID = 4;
    public static final int WHITESPACE_ID = 5;
    public static final int IDENTIFIER_ID = 6;
    public static final int LINE_COMMENT_ID = 7;
    public static final int BLOCK_COMMENT_ID = 8;
    public static final int CHAR_LITERAL_ID = 9;
    public static final int STRING_LITERAL_ID = 10;
    public static final int INT_LITERAL_ID = 11;
    public static final int LONG_LITERAL_ID = 12;
    public static final int HEX_LITERAL_ID = 13;
    public static final int OCTAL_LITERAL_ID = 14;
    public static final int FLOAT_LITERAL_ID = 15;
    public static final int DOUBLE_LITERAL_ID = 16;
    public static final int EQ_ID = 17;
    public static final int LT_ID = 18;
    public static final int GT_ID = 19;
    public static final int LSHIFT_ID = 20;
    public static final int RSSHIFT_ID = 21;
    public static final int RUSHIFT_ID = 22;
    public static final int PLUS_ID = 23;
    public static final int MINUS_ID = 24;
    public static final int MUL_ID = 25;
    public static final int DIV_ID = 26;
    public static final int AND_ID = 27;
    public static final int OR_ID = 28;
    public static final int XOR_ID = 29;
    public static final int MOD_ID = 30;
    public static final int NOT_ID = 31;
    public static final int NEG_ID = 32;
    public static final int EQ_EQ_ID = 33;
    public static final int LT_EQ_ID = 34;
    public static final int GT_EQ_ID = 35;
    public static final int LSHIFT_EQ_ID = 36;
    public static final int RSSHIFT_EQ_ID = 37;
    public static final int RUSHIFT_EQ_ID = 38;
    public static final int PLUS_EQ_ID = 39;
    public static final int MINUS_EQ_ID = 40;
    public static final int MUL_EQ_ID = 41;
    public static final int DIV_EQ_ID = 42;
    public static final int AND_EQ_ID = 43;
    public static final int OR_EQ_ID = 44;
    public static final int XOR_EQ_ID = 45;
    public static final int MOD_EQ_ID = 46;
    public static final int NOT_EQ_ID = 47;
    public static final int DOT_ID = 48;
    public static final int COMMA_ID = 49;
    public static final int COLON_ID = 50;
    public static final int SEMICOLON_ID = 51;
    public static final int QUESTION_ID = 52;
    public static final int LPAREN_ID = 53;
    public static final int RPAREN_ID = 54;
    public static final int LBRACKET_ID = 55;
    public static final int RBRACKET_ID = 56;
    public static final int LBRACE_ID = 57;
    public static final int RBRACE_ID = 58;
    public static final int PLUS_PLUS_ID = 59;
    public static final int MINUS_MINUS_ID = 60;
    public static final int AND_AND_ID = 61;
    public static final int OR_OR_ID = 62;
    public static final int BOOLEAN_ID = 63;
    public static final int BYTE_ID = 64;
    public static final int CHAR_ID = 65;
    public static final int DOUBLE_ID = 66;
    public static final int FLOAT_ID = 67;
    public static final int INT_ID = 68;
    public static final int LONG_ID = 69;
    public static final int SHORT_ID = 70;
    public static final int VOID_ID = 71;
    public static final int ABSTRACT_ID = 72;
    public static final int ASSERT_ID = 73;
    public static final int BREAK_ID = 74;
    public static final int CASE_ID = 75;
    public static final int CATCH_ID = 76;
    public static final int CLASS_ID = 77;
    public static final int CONST_ID = 78;
    public static final int CONTINUE_ID = 79;
    public static final int DEFAULT_ID = 80;
    public static final int DO_ID = 81;
    public static final int ELSE_ID = 82;
    public static final int ENUM_ID = 83;
    public static final int EXTENDS_ID = 84;
    public static final int FALSE_ID = 85;
    public static final int FINAL_ID = 86;
    public static final int FINALLY_ID = 87;
    public static final int FOR_ID = 88;
    public static final int GOTO_ID = 89;
    public static final int IF_ID = 90;
    public static final int IMPLEMENTS_ID = 91;
    public static final int IMPORT_ID = 92;
    public static final int INSTANCEOF_ID = 93;
    public static final int INTERFACE_ID = 94;
    public static final int NATIVE_ID = 95;
    public static final int NEW_ID = 96;
    public static final int NULL_ID = 97;
    public static final int PACKAGE_ID = 98;
    public static final int PRIVATE_ID = 99;
    public static final int PROTECTED_ID = 100;
    public static final int PUBLIC_ID = 101;
    public static final int RETURN_ID = 102;
    public static final int STATIC_ID = 103;
    public static final int STRICTFP_ID = 104;
    public static final int SUPER_ID = 105;
    public static final int SWITCH_ID = 106;
    public static final int SYNCHRONIZED_ID = 107;
    public static final int THIS_ID = 108;
    public static final int THROW_ID = 109;
    public static final int THROWS_ID = 110;
    public static final int TRANSIENT_ID = 111;
    public static final int TRUE_ID = 112;
    public static final int TRY_ID = 113;
    public static final int VOLATILE_ID = 114;
    public static final int WHILE_ID = 115;
    public static final int INCOMPLETE_STRING_LITERAL_ID = 116;
    public static final int INCOMPLETE_CHAR_LITERAL_ID = 117;
    public static final int INCOMPLETE_HEX_LITERAL_ID = 118;
    public static final int INVALID_CHAR_ID = 119;
    public static final int INVALID_OPERATOR_ID = 120;
    public static final int INVALID_OCTAL_LITERAL_ID = 121;
    public static final int INVALID_COMMENT_END_ID = 122;
    public static final int ANNOTATION_ID = 123;
    public static final int ELLIPSIS_ID = 124;
    public static final BaseTokenCategory KEYWORDS = new BaseTokenCategory("keywords", 1);
    public static final BaseTokenCategory OPERATORS = new BaseTokenCategory("operators", 2);
    public static final BaseTokenCategory NUMERIC_LITERALS = new BaseTokenCategory("numeric-literals", 3);
    public static final BaseTokenCategory ERRORS = new BaseTokenCategory("errors", 4);
    public static final BaseTokenID WHITESPACE = new BaseTokenID("whitespace", 5);
    public static final BaseTokenID IDENTIFIER = new BaseTokenID("identifier", 6);
    public static final BaseTokenID LINE_COMMENT = new BaseTokenID("line-comment", 7);
    public static final BaseTokenID BLOCK_COMMENT = new BaseTokenID("block-comment", 8);
    public static final BaseTokenID CHAR_LITERAL = new BaseTokenID("char-literal", 9);
    public static final BaseTokenID STRING_LITERAL = new BaseTokenID("string-literal", 10);
    public static final BaseTokenID INT_LITERAL = new BaseTokenID("int-literal", 11, (TokenCategory)NUMERIC_LITERALS);
    public static final BaseTokenID LONG_LITERAL = new BaseTokenID("long-literal", 12, (TokenCategory)NUMERIC_LITERALS);
    public static final BaseTokenID HEX_LITERAL = new BaseTokenID("hex-literal", 13, (TokenCategory)NUMERIC_LITERALS);
    public static final BaseTokenID OCTAL_LITERAL = new BaseTokenID("octal-literal", 14, (TokenCategory)NUMERIC_LITERALS);
    public static final BaseTokenID FLOAT_LITERAL = new BaseTokenID("float-literal", 15, (TokenCategory)NUMERIC_LITERALS);
    public static final BaseTokenID DOUBLE_LITERAL = new BaseTokenID("double-literal", 16, (TokenCategory)NUMERIC_LITERALS);
    public static final BaseImageTokenID EQ = new BaseImageTokenID("eq", 17, (TokenCategory)OPERATORS, "=");
    public static final BaseImageTokenID LT = new BaseImageTokenID("lt", 18, (TokenCategory)OPERATORS, "<");
    public static final BaseImageTokenID GT = new BaseImageTokenID("gt", 19, (TokenCategory)OPERATORS, ">");
    public static final BaseImageTokenID LSHIFT = new BaseImageTokenID("lshift", 20, (TokenCategory)OPERATORS, "<<");
    public static final BaseImageTokenID RSSHIFT = new BaseImageTokenID("rsshift", 21, (TokenCategory)OPERATORS, ">>");
    public static final BaseImageTokenID RUSHIFT = new BaseImageTokenID("rushift", 22, (TokenCategory)OPERATORS, ">>>");
    public static final BaseImageTokenID PLUS = new BaseImageTokenID("plus", 23, (TokenCategory)OPERATORS, "+");
    public static final BaseImageTokenID MINUS = new BaseImageTokenID("minus", 24, (TokenCategory)OPERATORS, "-");
    public static final BaseImageTokenID MUL = new BaseImageTokenID("mul", 25, (TokenCategory)OPERATORS, "*");
    public static final BaseImageTokenID DIV = new BaseImageTokenID("div", 26, (TokenCategory)OPERATORS, "/");
    public static final BaseImageTokenID AND = new BaseImageTokenID("and", 27, (TokenCategory)OPERATORS, "&");
    public static final BaseImageTokenID OR = new BaseImageTokenID("or", 28, (TokenCategory)OPERATORS, "|");
    public static final BaseImageTokenID XOR = new BaseImageTokenID("xor", 29, (TokenCategory)OPERATORS, "^");
    public static final BaseImageTokenID MOD = new BaseImageTokenID("mod", 30, (TokenCategory)OPERATORS, "%");
    public static final BaseImageTokenID NOT = new BaseImageTokenID("not", 31, (TokenCategory)OPERATORS, "!");
    public static final BaseImageTokenID NEG = new BaseImageTokenID("neg", 32, (TokenCategory)OPERATORS, "~");
    public static final BaseImageTokenID EQ_EQ = new BaseImageTokenID("eq-eq", 33, (TokenCategory)OPERATORS, "==");
    public static final BaseImageTokenID LT_EQ = new BaseImageTokenID("le", 34, (TokenCategory)OPERATORS, "<=");
    public static final BaseImageTokenID GT_EQ = new BaseImageTokenID("ge", 35, (TokenCategory)OPERATORS, ">=");
    public static final BaseImageTokenID LSHIFT_EQ = new BaseImageTokenID("lshift-eq", 36, (TokenCategory)OPERATORS, "<<=");
    public static final BaseImageTokenID RSSHIFT_EQ = new BaseImageTokenID("rsshift-eq", 37, (TokenCategory)OPERATORS, ">>=");
    public static final BaseImageTokenID RUSHIFT_EQ = new BaseImageTokenID("rushift-eq", 38, (TokenCategory)OPERATORS, ">>>=");
    public static final BaseImageTokenID PLUS_EQ = new BaseImageTokenID("plus-eq", 39, (TokenCategory)OPERATORS, "+=");
    public static final BaseImageTokenID MINUS_EQ = new BaseImageTokenID("minus-eq", 40, (TokenCategory)OPERATORS, "-=");
    public static final BaseImageTokenID MUL_EQ = new BaseImageTokenID("mul-eq", 41, (TokenCategory)OPERATORS, "*=");
    public static final BaseImageTokenID DIV_EQ = new BaseImageTokenID("div-eq", 42, (TokenCategory)OPERATORS, "/=");
    public static final BaseImageTokenID AND_EQ = new BaseImageTokenID("and-eq", 43, (TokenCategory)OPERATORS, "&=");
    public static final BaseImageTokenID OR_EQ = new BaseImageTokenID("or-eq", 44, (TokenCategory)OPERATORS, "|=");
    public static final BaseImageTokenID XOR_EQ = new BaseImageTokenID("xor-eq", 45, (TokenCategory)OPERATORS, "^=");
    public static final BaseImageTokenID MOD_EQ = new BaseImageTokenID("mod-eq", 46, (TokenCategory)OPERATORS, "%=");
    public static final BaseImageTokenID NOT_EQ = new BaseImageTokenID("not-eq", 47, (TokenCategory)OPERATORS, "!=");
    public static final BaseImageTokenID DOT = new BaseImageTokenID("dot", 48, (TokenCategory)OPERATORS, ".");
    public static final BaseImageTokenID COMMA = new BaseImageTokenID("comma", 49, (TokenCategory)OPERATORS, ",");
    public static final BaseImageTokenID COLON = new BaseImageTokenID("colon", 50, (TokenCategory)OPERATORS, ":");
    public static final BaseImageTokenID SEMICOLON = new BaseImageTokenID("semicolon", 51, (TokenCategory)OPERATORS, ";");
    public static final BaseImageTokenID QUESTION = new BaseImageTokenID("question", 52, (TokenCategory)OPERATORS, "?");
    public static final BaseImageTokenID LPAREN = new BaseImageTokenID("lparen", 53, (TokenCategory)OPERATORS, "(");
    public static final BaseImageTokenID RPAREN = new BaseImageTokenID("rparen", 54, (TokenCategory)OPERATORS, ")");
    public static final BaseImageTokenID LBRACKET = new BaseImageTokenID("lbracket", 55, (TokenCategory)OPERATORS, "[");
    public static final BaseImageTokenID RBRACKET = new BaseImageTokenID("rbracket", 56, (TokenCategory)OPERATORS, "]");
    public static final BaseImageTokenID LBRACE = new BaseImageTokenID("lbrace", 57, (TokenCategory)OPERATORS, "{");
    public static final BaseImageTokenID RBRACE = new BaseImageTokenID("rbrace", 58, (TokenCategory)OPERATORS, "}");
    public static final BaseImageTokenID PLUS_PLUS = new BaseImageTokenID("plus-plus", 59, (TokenCategory)OPERATORS, "++");
    public static final BaseImageTokenID MINUS_MINUS = new BaseImageTokenID("minus-minus", 60, (TokenCategory)OPERATORS, "--");
    public static final BaseImageTokenID AND_AND = new BaseImageTokenID("and-and", 61, (TokenCategory)OPERATORS, "&&");
    public static final BaseImageTokenID OR_OR = new BaseImageTokenID("or-or", 62, (TokenCategory)OPERATORS, "||");
    public static final BaseImageTokenID BOOLEAN = new BaseImageTokenID("boolean", 63, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID BYTE = new BaseImageTokenID("byte", 64, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID CHAR = new BaseImageTokenID("char", 65, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID DOUBLE = new BaseImageTokenID("double", 66, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID FLOAT = new BaseImageTokenID("float", 67, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID INT = new BaseImageTokenID("int", 68, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID LONG = new BaseImageTokenID("long", 69, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID SHORT = new BaseImageTokenID("short", 70, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID VOID = new BaseImageTokenID("void", 71, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID ABSTRACT = new BaseImageTokenID("abstract", 72, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID ASSERT = new BaseImageTokenID("assert", 73, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID BREAK = new BaseImageTokenID("break", 74, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID CASE = new BaseImageTokenID("case", 75, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID CATCH = new BaseImageTokenID("catch", 76, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID CLASS = new BaseImageTokenID("class", 77, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID CONST = new BaseImageTokenID("const", 78, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID CONTINUE = new BaseImageTokenID("continue", 79, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID DEFAULT = new BaseImageTokenID("default", 80, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID DO = new BaseImageTokenID("do", 81, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID ELSE = new BaseImageTokenID("else", 82, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID ENUM = new BaseImageTokenID("enum", 83, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID EXTENDS = new BaseImageTokenID("extends", 84, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID FALSE = new BaseImageTokenID("false", 85, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID FINAL = new BaseImageTokenID("final", 86, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID FINALLY = new BaseImageTokenID("finally", 87, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID FOR = new BaseImageTokenID("for", 88, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID GOTO = new BaseImageTokenID("goto", 89, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID IF = new BaseImageTokenID("if", 90, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID IMPLEMENTS = new BaseImageTokenID("implements", 91, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID IMPORT = new BaseImageTokenID("import", 92, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID INSTANCEOF = new BaseImageTokenID("instanceof", 93, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID INTERFACE = new BaseImageTokenID("interface", 94, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID NATIVE = new BaseImageTokenID("native", 95, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID NEW = new BaseImageTokenID("new", 96, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID NULL = new BaseImageTokenID("null", 97, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID PACKAGE = new BaseImageTokenID("package", 98, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID PRIVATE = new BaseImageTokenID("private", 99, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID PROTECTED = new BaseImageTokenID("protected", 100, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID PUBLIC = new BaseImageTokenID("public", 101, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID RETURN = new BaseImageTokenID("return", 102, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID STATIC = new BaseImageTokenID("static", 103, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID STRICTFP = new BaseImageTokenID("strictfp", 104, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID SUPER = new BaseImageTokenID("super", 105, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID SWITCH = new BaseImageTokenID("switch", 106, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID SYNCHRONIZED = new BaseImageTokenID("synchronized", 107, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID THIS = new BaseImageTokenID("this", 108, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID THROW = new BaseImageTokenID("throw", 109, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID THROWS = new BaseImageTokenID("throws", 110, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID TRANSIENT = new BaseImageTokenID("transient", 111, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID TRUE = new BaseImageTokenID("true", 112, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID TRY = new BaseImageTokenID("try", 113, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID VOLATILE = new BaseImageTokenID("volatile", 114, (TokenCategory)KEYWORDS);
    public static final BaseImageTokenID WHILE = new BaseImageTokenID("while", 115, (TokenCategory)KEYWORDS);
    public static final BaseTokenID INCOMPLETE_STRING_LITERAL = new BaseTokenID("incomplete-string-literal", 116, (TokenCategory)ERRORS);
    public static final BaseTokenID INCOMPLETE_CHAR_LITERAL = new BaseTokenID("incomplete-char-literal", 117, (TokenCategory)ERRORS);
    public static final BaseTokenID INCOMPLETE_HEX_LITERAL = new BaseTokenID("incomplete-hex-literal", 118, (TokenCategory)ERRORS);
    public static final BaseTokenID INVALID_CHAR = new BaseTokenID("invalid-char", 119, (TokenCategory)ERRORS);
    public static final BaseTokenID INVALID_OPERATOR = new BaseTokenID("invalid-operator", 120, (TokenCategory)ERRORS);
    public static final BaseTokenID INVALID_OCTAL_LITERAL = new BaseTokenID("invalid-octal-literal", 121, (TokenCategory)ERRORS);
    public static final BaseTokenID INVALID_COMMENT_END = new BaseTokenID("invalid-comment-end", 122, (TokenCategory)ERRORS);
    public static final BaseTokenID ANNOTATION = new BaseTokenID("annotation", 123);
    public static final BaseImageTokenID ELLIPSIS = new BaseImageTokenID("ellipsis", 124, (TokenCategory)OPERATORS, "...");
    public static final JavaTokenContext context = new JavaTokenContext();
    public static final TokenContextPath contextPath = context.getContextPath();
    private static final HashMap str2kwd = new HashMap();

    public static boolean isType(TokenID keywordTokenID) {
        int numID = keywordTokenID != null ? keywordTokenID.getNumericID() : -1;
        return numID >= 63 && numID < 71;
    }

    public static boolean isType(String s) {
        return JavaTokenContext.isType((TokenID)str2kwd.get(s));
    }

    public static boolean isTypeOrVoid(TokenID keywordTokenID) {
        int numID = keywordTokenID != null ? keywordTokenID.getNumericID() : -1;
        return numID >= 63 && numID <= 71;
    }

    public static boolean isTypeOrVoid(String s) {
        return JavaTokenContext.isTypeOrVoid((TokenID)str2kwd.get(s));
    }

    public static TokenID getKeyword(String s) {
        return (TokenID)str2kwd.get(s);
    }

    private JavaTokenContext() {
        super("java-");
        try {
            this.addDeclaredTokenIDs();
        }
        catch (Exception e) {
            Utilities.annotateLoggable((Throwable)e);
        }
    }

    static {
        BaseImageTokenID[] kwds = new BaseImageTokenID[]{ABSTRACT, ASSERT, BREAK, CASE, CATCH, CLASS, CONST, CONTINUE, DEFAULT, DO, ELSE, ENUM, EXTENDS, FALSE, FINAL, FINALLY, FOR, GOTO, IF, IMPLEMENTS, IMPORT, INSTANCEOF, INTERFACE, NATIVE, NEW, NULL, PACKAGE, PRIVATE, PROTECTED, PUBLIC, RETURN, STATIC, STRICTFP, SUPER, SWITCH, SYNCHRONIZED, THIS, THROW, THROWS, TRANSIENT, TRUE, TRY, VOLATILE, WHILE, BOOLEAN, BYTE, CHAR, DOUBLE, FLOAT, INT, LONG, SHORT, VOID};
        for (int i = kwds.length - 1; i >= 0; --i) {
            str2kwd.put(kwds[i].getImage(), kwds[i]);
        }
    }
}

