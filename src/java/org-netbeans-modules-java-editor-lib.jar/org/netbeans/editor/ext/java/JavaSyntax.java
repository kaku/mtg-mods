/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseImageTokenID
 *  org.netbeans.editor.BaseTokenID
 *  org.netbeans.editor.Syntax
 *  org.netbeans.editor.TokenContextPath
 *  org.netbeans.editor.TokenID
 */
package org.netbeans.editor.ext.java;

import org.netbeans.editor.BaseImageTokenID;
import org.netbeans.editor.BaseTokenID;
import org.netbeans.editor.Syntax;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;
import org.netbeans.editor.ext.java.JavaTokenContext;

public class JavaSyntax
extends Syntax {
    private static final int ISI_WHITESPACE = 2;
    private static final int ISI_LINE_COMMENT = 4;
    private static final int ISI_BLOCK_COMMENT = 5;
    private static final int ISI_STRING = 6;
    private static final int ISI_STRING_A_BSLASH = 7;
    private static final int ISI_CHAR = 8;
    private static final int ISI_CHAR_A_BSLASH = 9;
    private static final int ISI_IDENTIFIER = 10;
    private static final int ISA_SLASH = 11;
    private static final int ISA_EQ = 12;
    private static final int ISA_GT = 13;
    private static final int ISA_GTGT = 14;
    private static final int ISA_GTGTGT = 15;
    private static final int ISA_LT = 16;
    private static final int ISA_LTLT = 17;
    private static final int ISA_PLUS = 18;
    private static final int ISA_MINUS = 19;
    private static final int ISA_STAR = 20;
    private static final int ISA_STAR_I_BLOCK_COMMENT = 21;
    private static final int ISA_PIPE = 22;
    private static final int ISA_PERCENT = 23;
    private static final int ISA_AND = 24;
    private static final int ISA_XOR = 25;
    private static final int ISA_EXCLAMATION = 26;
    private static final int ISA_ZERO = 27;
    private static final int ISI_INT = 28;
    private static final int ISI_OCTAL = 29;
    private static final int ISI_DOUBLE = 30;
    private static final int ISI_DOUBLE_EXP = 31;
    private static final int ISI_HEX = 32;
    private static final int ISA_DOT = 33;
    private boolean isJava15 = true;
    private boolean useInJsp = false;

    public JavaSyntax() {
        this.tokenContextPath = JavaTokenContext.contextPath;
    }

    public JavaSyntax(String sourceLevel) {
        this();
        if (sourceLevel != null) {
            try {
                this.isJava15 = (double)Float.parseFloat(sourceLevel) >= 1.5;
            }
            catch (NumberFormatException e) {
                // empty catch block
            }
        }
    }

    public JavaSyntax(String sourceLevel, boolean useInJsp) {
        this(sourceLevel);
        this.useInJsp = useInJsp;
    }

    protected TokenID parseToken() {
        while (this.offset < this.stopOffset) {
            char actChar = this.buffer[this.offset];
            block0 : switch (this.state) {
                case -1: {
                    switch (actChar) {
                        case '\"': {
                            this.state = 6;
                            break block0;
                        }
                        case '\'': {
                            this.state = 8;
                            break block0;
                        }
                        case '/': {
                            this.state = 11;
                            break block0;
                        }
                        case '=': {
                            this.state = 12;
                            break block0;
                        }
                        case '>': {
                            this.state = 13;
                            break block0;
                        }
                        case '<': {
                            this.state = 16;
                            break block0;
                        }
                        case '+': {
                            this.state = 18;
                            break block0;
                        }
                        case '-': {
                            this.state = 19;
                            break block0;
                        }
                        case '*': {
                            this.state = 20;
                            break block0;
                        }
                        case '|': {
                            this.state = 22;
                            break block0;
                        }
                        case '%': {
                            this.state = 23;
                            break block0;
                        }
                        case '&': {
                            this.state = 24;
                            break block0;
                        }
                        case '^': {
                            this.state = 25;
                            break block0;
                        }
                        case '~': {
                            ++this.offset;
                            return JavaTokenContext.NEG;
                        }
                        case '!': {
                            this.state = 26;
                            break block0;
                        }
                        case '0': {
                            this.state = 27;
                            break block0;
                        }
                        case '.': {
                            this.state = 33;
                            break block0;
                        }
                        case ',': {
                            ++this.offset;
                            return JavaTokenContext.COMMA;
                        }
                        case ';': {
                            ++this.offset;
                            return JavaTokenContext.SEMICOLON;
                        }
                        case ':': {
                            ++this.offset;
                            return JavaTokenContext.COLON;
                        }
                        case '?': {
                            ++this.offset;
                            return JavaTokenContext.QUESTION;
                        }
                        case '(': {
                            ++this.offset;
                            return JavaTokenContext.LPAREN;
                        }
                        case ')': {
                            ++this.offset;
                            return JavaTokenContext.RPAREN;
                        }
                        case '[': {
                            ++this.offset;
                            return JavaTokenContext.LBRACKET;
                        }
                        case ']': {
                            ++this.offset;
                            return JavaTokenContext.RBRACKET;
                        }
                        case '{': {
                            ++this.offset;
                            return JavaTokenContext.LBRACE;
                        }
                        case '}': {
                            ++this.offset;
                            return JavaTokenContext.RBRACE;
                        }
                        case '@': {
                            ++this.offset;
                            return JavaTokenContext.ANNOTATION;
                        }
                    }
                    if (Character.isWhitespace(actChar)) {
                        this.state = 2;
                        break;
                    }
                    if (Character.isDigit(actChar)) {
                        this.state = 28;
                        break;
                    }
                    if (Character.isJavaIdentifierStart(actChar)) {
                        this.state = 10;
                        break;
                    }
                    ++this.offset;
                    return JavaTokenContext.INVALID_CHAR;
                }
                case 2: {
                    if (Character.isWhitespace(actChar)) break;
                    this.state = -1;
                    return JavaTokenContext.WHITESPACE;
                }
                case 4: {
                    switch (actChar) {
                        case '\n': {
                            this.state = -1;
                            return JavaTokenContext.LINE_COMMENT;
                        }
                    }
                    break;
                }
                case 5: {
                    switch (actChar) {
                        case '*': {
                            this.state = 21;
                            break block0;
                        }
                        case '\n': {
                            if (!this.useInJsp) break;
                            ++this.offset;
                            return JavaTokenContext.BLOCK_COMMENT;
                        }
                    }
                    break;
                }
                case 6: {
                    switch (actChar) {
                        case '\\': {
                            this.state = 7;
                            break block0;
                        }
                        case '\n': {
                            this.state = -1;
                            this.supposedTokenID = JavaTokenContext.STRING_LITERAL;
                            return this.supposedTokenID;
                        }
                        case '\"': {
                            ++this.offset;
                            this.state = -1;
                            return JavaTokenContext.STRING_LITERAL;
                        }
                    }
                    break;
                }
                case 7: {
                    switch (actChar) {
                        case '\"': 
                        case '\\': {
                            break;
                        }
                        default: {
                            --this.offset;
                        }
                    }
                    this.state = 6;
                    break;
                }
                case 8: {
                    switch (actChar) {
                        case '\\': {
                            this.state = 9;
                            break block0;
                        }
                        case '\n': {
                            this.state = -1;
                            this.supposedTokenID = JavaTokenContext.CHAR_LITERAL;
                            return this.supposedTokenID;
                        }
                        case '\'': {
                            ++this.offset;
                            this.state = -1;
                            return JavaTokenContext.CHAR_LITERAL;
                        }
                    }
                    break;
                }
                case 9: {
                    switch (actChar) {
                        case '\'': 
                        case '\\': {
                            break;
                        }
                        default: {
                            --this.offset;
                        }
                    }
                    this.state = 8;
                    break;
                }
                case 10: {
                    if (Character.isJavaIdentifierPart(actChar)) break;
                    this.state = -1;
                    TokenID tid = this.matchKeyword(this.buffer, this.tokenOffset, this.offset - this.tokenOffset);
                    return tid != null ? tid : JavaTokenContext.IDENTIFIER;
                }
                case 11: {
                    switch (actChar) {
                        case '=': {
                            ++this.offset;
                            this.state = -1;
                            return JavaTokenContext.DIV_EQ;
                        }
                        case '/': {
                            this.state = 4;
                            break block0;
                        }
                        case '*': {
                            this.state = 5;
                            break block0;
                        }
                    }
                    this.state = -1;
                    return JavaTokenContext.DIV;
                }
                case 12: {
                    switch (actChar) {
                        case '=': {
                            ++this.offset;
                            return JavaTokenContext.EQ_EQ;
                        }
                    }
                    this.state = -1;
                    return JavaTokenContext.EQ;
                }
                case 13: {
                    switch (actChar) {
                        case '>': {
                            this.state = 14;
                            break block0;
                        }
                        case '=': {
                            ++this.offset;
                            return JavaTokenContext.GT_EQ;
                        }
                    }
                    this.state = -1;
                    return JavaTokenContext.GT;
                }
                case 14: {
                    switch (actChar) {
                        case '>': {
                            this.state = 15;
                            break block0;
                        }
                        case '=': {
                            ++this.offset;
                            return JavaTokenContext.RSSHIFT_EQ;
                        }
                    }
                    this.state = -1;
                    return JavaTokenContext.RSSHIFT;
                }
                case 15: {
                    switch (actChar) {
                        case '=': {
                            ++this.offset;
                            return JavaTokenContext.RUSHIFT_EQ;
                        }
                    }
                    this.state = -1;
                    return JavaTokenContext.RUSHIFT;
                }
                case 16: {
                    switch (actChar) {
                        case '<': {
                            this.state = 17;
                            break block0;
                        }
                        case '=': {
                            ++this.offset;
                            return JavaTokenContext.LT_EQ;
                        }
                    }
                    this.state = -1;
                    return JavaTokenContext.LT;
                }
                case 17: {
                    switch (actChar) {
                        case '<': {
                            this.state = -1;
                            ++this.offset;
                            return JavaTokenContext.INVALID_OPERATOR;
                        }
                        case '=': {
                            ++this.offset;
                            return JavaTokenContext.LSHIFT_EQ;
                        }
                    }
                    this.state = -1;
                    return JavaTokenContext.LSHIFT;
                }
                case 18: {
                    switch (actChar) {
                        case '+': {
                            ++this.offset;
                            return JavaTokenContext.PLUS_PLUS;
                        }
                        case '=': {
                            ++this.offset;
                            return JavaTokenContext.PLUS_EQ;
                        }
                    }
                    this.state = -1;
                    return JavaTokenContext.PLUS;
                }
                case 19: {
                    switch (actChar) {
                        case '-': {
                            ++this.offset;
                            return JavaTokenContext.MINUS_MINUS;
                        }
                        case '=': {
                            ++this.offset;
                            return JavaTokenContext.MINUS_EQ;
                        }
                    }
                    this.state = -1;
                    return JavaTokenContext.MINUS;
                }
                case 20: {
                    switch (actChar) {
                        case '=': {
                            ++this.offset;
                            return JavaTokenContext.MUL_EQ;
                        }
                        case '/': {
                            ++this.offset;
                            this.state = -1;
                            return JavaTokenContext.INVALID_COMMENT_END;
                        }
                    }
                    this.state = -1;
                    return JavaTokenContext.MUL;
                }
                case 21: {
                    switch (actChar) {
                        case '/': {
                            ++this.offset;
                            this.state = -1;
                            return JavaTokenContext.BLOCK_COMMENT;
                        }
                    }
                    --this.offset;
                    this.state = 5;
                    break;
                }
                case 22: {
                    switch (actChar) {
                        case '=': {
                            ++this.offset;
                            this.state = -1;
                            return JavaTokenContext.OR_EQ;
                        }
                        case '|': {
                            ++this.offset;
                            this.state = -1;
                            return JavaTokenContext.OR_OR;
                        }
                    }
                    this.state = -1;
                    return JavaTokenContext.OR;
                }
                case 23: {
                    switch (actChar) {
                        case '=': {
                            ++this.offset;
                            this.state = -1;
                            return JavaTokenContext.MOD_EQ;
                        }
                    }
                    this.state = -1;
                    return JavaTokenContext.MOD;
                }
                case 24: {
                    switch (actChar) {
                        case '=': {
                            ++this.offset;
                            this.state = -1;
                            return JavaTokenContext.AND_EQ;
                        }
                        case '&': {
                            ++this.offset;
                            this.state = -1;
                            return JavaTokenContext.AND_AND;
                        }
                    }
                    this.state = -1;
                    return JavaTokenContext.AND;
                }
                case 25: {
                    switch (actChar) {
                        case '=': {
                            ++this.offset;
                            this.state = -1;
                            return JavaTokenContext.XOR_EQ;
                        }
                    }
                    this.state = -1;
                    return JavaTokenContext.XOR;
                }
                case 26: {
                    switch (actChar) {
                        case '=': {
                            ++this.offset;
                            this.state = -1;
                            return JavaTokenContext.NOT_EQ;
                        }
                    }
                    this.state = -1;
                    return JavaTokenContext.NOT;
                }
                case 27: {
                    switch (actChar) {
                        case '.': {
                            this.state = 30;
                            break block0;
                        }
                        case 'X': 
                        case 'x': {
                            this.state = 32;
                            break block0;
                        }
                        case 'L': 
                        case 'l': {
                            ++this.offset;
                            this.state = -1;
                            return JavaTokenContext.LONG_LITERAL;
                        }
                        case 'F': 
                        case 'f': {
                            ++this.offset;
                            this.state = -1;
                            return JavaTokenContext.FLOAT_LITERAL;
                        }
                        case 'D': 
                        case 'd': {
                            ++this.offset;
                            this.state = -1;
                            return JavaTokenContext.DOUBLE_LITERAL;
                        }
                        case '8': 
                        case '9': {
                            this.state = -1;
                            ++this.offset;
                            return JavaTokenContext.INVALID_OCTAL_LITERAL;
                        }
                        case 'E': 
                        case 'e': {
                            this.state = 31;
                            break block0;
                        }
                    }
                    if (Character.isDigit(actChar)) {
                        this.state = 29;
                        break;
                    }
                    this.state = -1;
                    return JavaTokenContext.INT_LITERAL;
                }
                case 28: {
                    switch (actChar) {
                        case 'L': 
                        case 'l': {
                            ++this.offset;
                            this.state = -1;
                            return JavaTokenContext.LONG_LITERAL;
                        }
                        case '.': {
                            this.state = 30;
                            break block0;
                        }
                        case 'F': 
                        case 'f': {
                            ++this.offset;
                            this.state = -1;
                            return JavaTokenContext.FLOAT_LITERAL;
                        }
                        case 'D': 
                        case 'd': {
                            ++this.offset;
                            this.state = -1;
                            return JavaTokenContext.DOUBLE_LITERAL;
                        }
                        case 'E': 
                        case 'e': {
                            this.state = 31;
                            break block0;
                        }
                    }
                    if (actChar >= '0' && actChar <= '9') break;
                    this.state = -1;
                    return JavaTokenContext.INT_LITERAL;
                }
                case 29: {
                    if (actChar >= '0' && actChar <= '7') break;
                    this.state = -1;
                    return JavaTokenContext.OCTAL_LITERAL;
                }
                case 30: {
                    switch (actChar) {
                        case 'F': 
                        case 'f': {
                            ++this.offset;
                            this.state = -1;
                            return JavaTokenContext.FLOAT_LITERAL;
                        }
                        case 'D': 
                        case 'd': {
                            ++this.offset;
                            this.state = -1;
                            return JavaTokenContext.DOUBLE_LITERAL;
                        }
                        case 'E': 
                        case 'e': {
                            this.state = 31;
                            break block0;
                        }
                    }
                    if (actChar >= '0' && actChar <= '9' || actChar == '.') break;
                    this.state = -1;
                    return JavaTokenContext.DOUBLE_LITERAL;
                }
                case 31: {
                    switch (actChar) {
                        case 'F': 
                        case 'f': {
                            ++this.offset;
                            this.state = -1;
                            return JavaTokenContext.FLOAT_LITERAL;
                        }
                        case 'D': 
                        case 'd': {
                            ++this.offset;
                            this.state = -1;
                            return JavaTokenContext.DOUBLE_LITERAL;
                        }
                    }
                    if (Character.isDigit(actChar) || actChar == '-' || actChar == '+') break;
                    this.state = -1;
                    return JavaTokenContext.DOUBLE_LITERAL;
                }
                case 32: {
                    if (actChar >= 'a' && actChar <= 'f' || actChar >= 'A' && actChar <= 'F' || Character.isDigit(actChar)) break;
                    this.state = -1;
                    return JavaTokenContext.HEX_LITERAL;
                }
                case 33: {
                    if (Character.isDigit(actChar)) {
                        this.state = 30;
                        break;
                    }
                    if (actChar == '.' && this.offset + 1 < this.stopOffset && this.buffer[this.offset + 1] == '.') {
                        this.offset += 2;
                        this.state = -1;
                        return JavaTokenContext.ELLIPSIS;
                    }
                    this.state = -1;
                    return JavaTokenContext.DOT;
                }
            }
            ++this.offset;
        }
        if (this.lastBuffer) {
            switch (this.state) {
                case 2: {
                    this.state = -1;
                    return JavaTokenContext.WHITESPACE;
                }
                case 10: {
                    this.state = -1;
                    TokenID kwd = this.matchKeyword(this.buffer, this.tokenOffset, this.offset - this.tokenOffset);
                    return kwd != null ? kwd : JavaTokenContext.IDENTIFIER;
                }
                case 4: {
                    return JavaTokenContext.LINE_COMMENT;
                }
                case 5: 
                case 21: {
                    return JavaTokenContext.BLOCK_COMMENT;
                }
                case 6: 
                case 7: {
                    return JavaTokenContext.STRING_LITERAL;
                }
                case 8: 
                case 9: {
                    return JavaTokenContext.CHAR_LITERAL;
                }
                case 27: 
                case 28: {
                    this.state = -1;
                    return JavaTokenContext.INT_LITERAL;
                }
                case 29: {
                    this.state = -1;
                    return JavaTokenContext.OCTAL_LITERAL;
                }
                case 30: 
                case 31: {
                    this.state = -1;
                    return JavaTokenContext.DOUBLE_LITERAL;
                }
                case 32: {
                    this.state = -1;
                    return JavaTokenContext.HEX_LITERAL;
                }
                case 33: {
                    this.state = -1;
                    return JavaTokenContext.DOT;
                }
                case 11: {
                    this.state = -1;
                    return JavaTokenContext.DIV;
                }
                case 12: {
                    this.state = -1;
                    return JavaTokenContext.EQ;
                }
                case 13: {
                    this.state = -1;
                    return JavaTokenContext.GT;
                }
                case 14: {
                    this.state = -1;
                    return JavaTokenContext.RSSHIFT;
                }
                case 15: {
                    this.state = -1;
                    return JavaTokenContext.RUSHIFT;
                }
                case 16: {
                    this.state = -1;
                    return JavaTokenContext.LT;
                }
                case 17: {
                    this.state = -1;
                    return JavaTokenContext.LSHIFT;
                }
                case 18: {
                    this.state = -1;
                    return JavaTokenContext.PLUS;
                }
                case 19: {
                    this.state = -1;
                    return JavaTokenContext.MINUS;
                }
                case 20: {
                    this.state = -1;
                    return JavaTokenContext.MUL;
                }
                case 22: {
                    this.state = -1;
                    return JavaTokenContext.OR;
                }
                case 23: {
                    this.state = -1;
                    return JavaTokenContext.MOD;
                }
                case 24: {
                    this.state = -1;
                    return JavaTokenContext.AND;
                }
                case 25: {
                    this.state = -1;
                    return JavaTokenContext.XOR;
                }
                case 26: {
                    this.state = -1;
                    return JavaTokenContext.NOT;
                }
            }
        }
        switch (this.state) {
            case 2: {
                return JavaTokenContext.WHITESPACE;
            }
        }
        return null;
    }

    public String getStateName(int stateNumber) {
        switch (stateNumber) {
            case 2: {
                return "ISI_WHITESPACE";
            }
            case 4: {
                return "ISI_LINE_COMMENT";
            }
            case 5: {
                return "ISI_BLOCK_COMMENT";
            }
            case 6: {
                return "ISI_STRING";
            }
            case 7: {
                return "ISI_STRING_A_BSLASH";
            }
            case 8: {
                return "ISI_CHAR";
            }
            case 9: {
                return "ISI_CHAR_A_BSLASH";
            }
            case 10: {
                return "ISI_IDENTIFIER";
            }
            case 11: {
                return "ISA_SLASH";
            }
            case 12: {
                return "ISA_EQ";
            }
            case 13: {
                return "ISA_GT";
            }
            case 14: {
                return "ISA_GTGT";
            }
            case 15: {
                return "ISA_GTGTGT";
            }
            case 16: {
                return "ISA_LT";
            }
            case 17: {
                return "ISA_LTLT";
            }
            case 18: {
                return "ISA_PLUS";
            }
            case 19: {
                return "ISA_MINUS";
            }
            case 20: {
                return "ISA_STAR";
            }
            case 21: {
                return "ISA_STAR_I_BLOCK_COMMENT";
            }
            case 22: {
                return "ISA_PIPE";
            }
            case 23: {
                return "ISA_PERCENT";
            }
            case 24: {
                return "ISA_AND";
            }
            case 25: {
                return "ISA_XOR";
            }
            case 26: {
                return "ISA_EXCLAMATION";
            }
            case 27: {
                return "ISA_ZERO";
            }
            case 28: {
                return "ISI_INT";
            }
            case 29: {
                return "ISI_OCTAL";
            }
            case 30: {
                return "ISI_DOUBLE";
            }
            case 31: {
                return "ISI_DOUBLE_EXP";
            }
            case 32: {
                return "ISI_HEX";
            }
            case 33: {
                return "ISA_DOT";
            }
        }
        return super.getStateName(stateNumber);
    }

    public TokenID matchKeyword(char[] buffer, int offset, int len) {
        if (len > 12) {
            return null;
        }
        if (len <= 1) {
            return null;
        }
        switch (buffer[offset++]) {
            case 'a': {
                if (len <= 5) {
                    return null;
                }
                switch (buffer[offset++]) {
                    case 'b': {
                        return len == 8 && buffer[offset++] == 's' && buffer[offset++] == 't' && buffer[offset++] == 'r' && buffer[offset++] == 'a' && buffer[offset++] == 'c' && buffer[offset++] == 't' ? JavaTokenContext.ABSTRACT : null;
                    }
                    case 's': {
                        return len == 6 && buffer[offset++] == 's' && buffer[offset++] == 'e' && buffer[offset++] == 'r' && buffer[offset++] == 't' ? JavaTokenContext.ASSERT : null;
                    }
                }
                return null;
            }
            case 'b': {
                if (len <= 3) {
                    return null;
                }
                switch (buffer[offset++]) {
                    case 'o': {
                        return len == 7 && buffer[offset++] == 'o' && buffer[offset++] == 'l' && buffer[offset++] == 'e' && buffer[offset++] == 'a' && buffer[offset++] == 'n' ? JavaTokenContext.BOOLEAN : null;
                    }
                    case 'r': {
                        return len == 5 && buffer[offset++] == 'e' && buffer[offset++] == 'a' && buffer[offset++] == 'k' ? JavaTokenContext.BREAK : null;
                    }
                    case 'y': {
                        return len == 4 && buffer[offset++] == 't' && buffer[offset++] == 'e' ? JavaTokenContext.BYTE : null;
                    }
                }
                return null;
            }
            case 'c': {
                if (len <= 3) {
                    return null;
                }
                switch (buffer[offset++]) {
                    case 'a': {
                        switch (buffer[offset++]) {
                            case 's': {
                                return len == 4 && buffer[offset++] == 'e' ? JavaTokenContext.CASE : null;
                            }
                            case 't': {
                                return len == 5 && buffer[offset++] == 'c' && buffer[offset++] == 'h' ? JavaTokenContext.CATCH : null;
                            }
                        }
                        return null;
                    }
                    case 'h': {
                        return len == 4 && buffer[offset++] == 'a' && buffer[offset++] == 'r' ? JavaTokenContext.CHAR : null;
                    }
                    case 'l': {
                        return len == 5 && buffer[offset++] == 'a' && buffer[offset++] == 's' && buffer[offset++] == 's' ? JavaTokenContext.CLASS : null;
                    }
                    case 'o': {
                        if (len <= 4) {
                            return null;
                        }
                        if (buffer[offset++] != 'n') {
                            return null;
                        }
                        switch (buffer[offset++]) {
                            case 's': {
                                return len == 5 && buffer[offset++] == 't' ? JavaTokenContext.CONST : null;
                            }
                            case 't': {
                                return len == 8 && buffer[offset++] == 'i' && buffer[offset++] == 'n' && buffer[offset++] == 'u' && buffer[offset++] == 'e' ? JavaTokenContext.CONTINUE : null;
                            }
                        }
                        return null;
                    }
                }
                return null;
            }
            case 'd': {
                switch (buffer[offset++]) {
                    case 'e': {
                        return len == 7 && buffer[offset++] == 'f' && buffer[offset++] == 'a' && buffer[offset++] == 'u' && buffer[offset++] == 'l' && buffer[offset++] == 't' ? JavaTokenContext.DEFAULT : null;
                    }
                    case 'o': {
                        if (len == 2) {
                            return JavaTokenContext.DO;
                        }
                        switch (buffer[offset++]) {
                            case 'u': {
                                return len == 6 && buffer[offset++] == 'b' && buffer[offset++] == 'l' && buffer[offset++] == 'e' ? JavaTokenContext.DOUBLE : null;
                            }
                        }
                        return null;
                    }
                }
                return null;
            }
            case 'e': {
                if (len <= 3) {
                    return null;
                }
                switch (buffer[offset++]) {
                    case 'l': {
                        return len == 4 && buffer[offset++] == 's' && buffer[offset++] == 'e' ? JavaTokenContext.ELSE : null;
                    }
                    case 'n': {
                        return len == 4 && buffer[offset++] == 'u' && buffer[offset++] == 'm' ? (this.isJava15 ? JavaTokenContext.ENUM : null) : null;
                    }
                    case 'x': {
                        return len == 7 && buffer[offset++] == 't' && buffer[offset++] == 'e' && buffer[offset++] == 'n' && buffer[offset++] == 'd' && buffer[offset++] == 's' ? JavaTokenContext.EXTENDS : null;
                    }
                }
                return null;
            }
            case 'f': {
                if (len <= 2) {
                    return null;
                }
                switch (buffer[offset++]) {
                    case 'a': {
                        return len == 5 && buffer[offset++] == 'l' && buffer[offset++] == 's' && buffer[offset++] == 'e' ? JavaTokenContext.FALSE : null;
                    }
                    case 'i': {
                        if (len <= 4) {
                            return null;
                        }
                        if (buffer[offset++] != 'n' || buffer[offset++] != 'a' || buffer[offset++] != 'l') {
                            return null;
                        }
                        if (len == 5) {
                            return JavaTokenContext.FINAL;
                        }
                        if (len <= 6) {
                            return null;
                        }
                        if (buffer[offset++] != 'l' || buffer[offset++] != 'y') {
                            return null;
                        }
                        if (len == 7) {
                            return JavaTokenContext.FINALLY;
                        }
                        return null;
                    }
                    case 'l': {
                        return len == 5 && buffer[offset++] == 'o' && buffer[offset++] == 'a' && buffer[offset++] == 't' ? JavaTokenContext.FLOAT : null;
                    }
                    case 'o': {
                        return len == 3 && buffer[offset++] == 'r' ? JavaTokenContext.FOR : null;
                    }
                }
                return null;
            }
            case 'g': {
                return len == 4 && buffer[offset++] == 'o' && buffer[offset++] == 't' && buffer[offset++] == 'o' ? JavaTokenContext.GOTO : null;
            }
            case 'i': {
                switch (buffer[offset++]) {
                    case 'f': {
                        return len == 2 ? JavaTokenContext.IF : null;
                    }
                    case 'm': {
                        if (len <= 5) {
                            return null;
                        }
                        if (buffer[offset++] != 'p') {
                            return null;
                        }
                        switch (buffer[offset++]) {
                            case 'l': {
                                return len == 10 && buffer[offset++] == 'e' && buffer[offset++] == 'm' && buffer[offset++] == 'e' && buffer[offset++] == 'n' && buffer[offset++] == 't' && buffer[offset++] == 's' ? JavaTokenContext.IMPLEMENTS : null;
                            }
                            case 'o': {
                                return len == 6 && buffer[offset++] == 'r' && buffer[offset++] == 't' ? JavaTokenContext.IMPORT : null;
                            }
                        }
                        return null;
                    }
                    case 'n': {
                        if (len <= 2) {
                            return null;
                        }
                        switch (buffer[offset++]) {
                            case 's': {
                                return len == 10 && buffer[offset++] == 't' && buffer[offset++] == 'a' && buffer[offset++] == 'n' && buffer[offset++] == 'c' && buffer[offset++] == 'e' && buffer[offset++] == 'o' && buffer[offset++] == 'f' ? JavaTokenContext.INSTANCEOF : null;
                            }
                            case 't': {
                                if (len == 3) {
                                    return JavaTokenContext.INT;
                                }
                                switch (buffer[offset++]) {
                                    case 'e': {
                                        return len == 9 && buffer[offset++] == 'r' && buffer[offset++] == 'f' && buffer[offset++] == 'a' && buffer[offset++] == 'c' && buffer[offset++] == 'e' ? JavaTokenContext.INTERFACE : null;
                                    }
                                }
                                return null;
                            }
                        }
                        return null;
                    }
                }
                return null;
            }
            case 'l': {
                return len == 4 && buffer[offset++] == 'o' && buffer[offset++] == 'n' && buffer[offset++] == 'g' ? JavaTokenContext.LONG : null;
            }
            case 'n': {
                if (len <= 2) {
                    return null;
                }
                switch (buffer[offset++]) {
                    case 'a': {
                        return len == 6 && buffer[offset++] == 't' && buffer[offset++] == 'i' && buffer[offset++] == 'v' && buffer[offset++] == 'e' ? JavaTokenContext.NATIVE : null;
                    }
                    case 'e': {
                        return len == 3 && buffer[offset++] == 'w' ? JavaTokenContext.NEW : null;
                    }
                    case 'u': {
                        return len == 4 && buffer[offset++] == 'l' && buffer[offset++] == 'l' ? JavaTokenContext.NULL : null;
                    }
                }
                return null;
            }
            case 'p': {
                if (len <= 5) {
                    return null;
                }
                switch (buffer[offset++]) {
                    case 'a': {
                        return len == 7 && buffer[offset++] == 'c' && buffer[offset++] == 'k' && buffer[offset++] == 'a' && buffer[offset++] == 'g' && buffer[offset++] == 'e' ? JavaTokenContext.PACKAGE : null;
                    }
                    case 'r': {
                        if (len <= 6) {
                            return null;
                        }
                        switch (buffer[offset++]) {
                            case 'i': {
                                return len == 7 && buffer[offset++] == 'v' && buffer[offset++] == 'a' && buffer[offset++] == 't' && buffer[offset++] == 'e' ? JavaTokenContext.PRIVATE : null;
                            }
                            case 'o': {
                                return len == 9 && buffer[offset++] == 't' && buffer[offset++] == 'e' && buffer[offset++] == 'c' && buffer[offset++] == 't' && buffer[offset++] == 'e' && buffer[offset++] == 'd' ? JavaTokenContext.PROTECTED : null;
                            }
                        }
                        return null;
                    }
                    case 'u': {
                        return len == 6 && buffer[offset++] == 'b' && buffer[offset++] == 'l' && buffer[offset++] == 'i' && buffer[offset++] == 'c' ? JavaTokenContext.PUBLIC : null;
                    }
                }
                return null;
            }
            case 'r': {
                return len == 6 && buffer[offset++] == 'e' && buffer[offset++] == 't' && buffer[offset++] == 'u' && buffer[offset++] == 'r' && buffer[offset++] == 'n' ? JavaTokenContext.RETURN : null;
            }
            case 's': {
                if (len <= 4) {
                    return null;
                }
                switch (buffer[offset++]) {
                    case 'h': {
                        return len == 5 && buffer[offset++] == 'o' && buffer[offset++] == 'r' && buffer[offset++] == 't' ? JavaTokenContext.SHORT : null;
                    }
                    case 't': {
                        if (len <= 5) {
                            return null;
                        }
                        switch (buffer[offset++]) {
                            case 'a': {
                                return len == 6 && buffer[offset++] == 't' && buffer[offset++] == 'i' && buffer[offset++] == 'c' ? JavaTokenContext.STATIC : null;
                            }
                            case 'r': {
                                return len == 8 && buffer[offset++] == 'i' && buffer[offset++] == 'c' && buffer[offset++] == 't' && buffer[offset++] == 'f' && buffer[offset++] == 'p' ? JavaTokenContext.STRICTFP : null;
                            }
                        }
                        return null;
                    }
                    case 'u': {
                        return len == 5 && buffer[offset++] == 'p' && buffer[offset++] == 'e' && buffer[offset++] == 'r' ? JavaTokenContext.SUPER : null;
                    }
                    case 'w': {
                        return len == 6 && buffer[offset++] == 'i' && buffer[offset++] == 't' && buffer[offset++] == 'c' && buffer[offset++] == 'h' ? JavaTokenContext.SWITCH : null;
                    }
                    case 'y': {
                        return len == 12 && buffer[offset++] == 'n' && buffer[offset++] == 'c' && buffer[offset++] == 'h' && buffer[offset++] == 'r' && buffer[offset++] == 'o' && buffer[offset++] == 'n' && buffer[offset++] == 'i' && buffer[offset++] == 'z' && buffer[offset++] == 'e' && buffer[offset++] == 'd' ? JavaTokenContext.SYNCHRONIZED : null;
                    }
                }
                return null;
            }
            case 't': {
                if (len <= 2) {
                    return null;
                }
                switch (buffer[offset++]) {
                    case 'h': {
                        if (len <= 3) {
                            return null;
                        }
                        switch (buffer[offset++]) {
                            case 'i': {
                                return len == 4 && buffer[offset++] == 's' ? JavaTokenContext.THIS : null;
                            }
                            case 'r': {
                                if (len <= 4) {
                                    return null;
                                }
                                if (buffer[offset++] != 'o' || buffer[offset++] != 'w') {
                                    return null;
                                }
                                if (len == 5) {
                                    return JavaTokenContext.THROW;
                                }
                                if (buffer[offset++] != 's') {
                                    return null;
                                }
                                if (len == 6) {
                                    return JavaTokenContext.THROWS;
                                }
                                return null;
                            }
                        }
                        return null;
                    }
                    case 'r': {
                        switch (buffer[offset++]) {
                            case 'a': {
                                return len == 9 && buffer[offset++] == 'n' && buffer[offset++] == 's' && buffer[offset++] == 'i' && buffer[offset++] == 'e' && buffer[offset++] == 'n' && buffer[offset++] == 't' ? JavaTokenContext.TRANSIENT : null;
                            }
                            case 'u': {
                                return len == 4 && buffer[offset++] == 'e' ? JavaTokenContext.TRUE : null;
                            }
                            case 'y': {
                                return len == 3 ? JavaTokenContext.TRY : null;
                            }
                        }
                        return null;
                    }
                }
                return null;
            }
            case 'v': {
                if (len <= 3) {
                    return null;
                }
                if (buffer[offset++] != 'o') {
                    return null;
                }
                switch (buffer[offset++]) {
                    case 'i': {
                        return len == 4 && buffer[offset++] == 'd' ? JavaTokenContext.VOID : null;
                    }
                    case 'l': {
                        return len == 8 && buffer[offset++] == 'a' && buffer[offset++] == 't' && buffer[offset++] == 'i' && buffer[offset++] == 'l' && buffer[offset++] == 'e' ? JavaTokenContext.VOLATILE : null;
                    }
                }
                return null;
            }
            case 'w': {
                return len == 5 && buffer[offset++] == 'h' && buffer[offset++] == 'i' && buffer[offset++] == 'l' && buffer[offset++] == 'e' ? JavaTokenContext.WHILE : null;
            }
        }
        return null;
    }
}

