/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.BaseImageTokenID
 *  org.netbeans.editor.BaseTokenID
 *  org.netbeans.editor.Syntax
 *  org.netbeans.editor.TokenContextPath
 *  org.netbeans.editor.TokenID
 *  org.netbeans.editor.TokenItem
 *  org.netbeans.editor.Utilities
 *  org.netbeans.editor.ext.AbstractFormatLayer
 *  org.netbeans.editor.ext.ExtFormatter
 *  org.netbeans.editor.ext.FormatLayer
 *  org.netbeans.editor.ext.FormatSupport
 *  org.netbeans.editor.ext.FormatTokenPosition
 *  org.netbeans.editor.ext.FormatWriter
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.editor.ext.java;

import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseImageTokenID;
import org.netbeans.editor.BaseTokenID;
import org.netbeans.editor.Syntax;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;
import org.netbeans.editor.TokenItem;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.ext.AbstractFormatLayer;
import org.netbeans.editor.ext.ExtFormatter;
import org.netbeans.editor.ext.FormatLayer;
import org.netbeans.editor.ext.FormatSupport;
import org.netbeans.editor.ext.FormatTokenPosition;
import org.netbeans.editor.ext.FormatWriter;
import org.netbeans.editor.ext.java.JavaFormatSupport;
import org.netbeans.editor.ext.java.JavaSyntax;
import org.netbeans.editor.ext.java.JavaTokenContext;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;

public class JavaFormatter
extends ExtFormatter {
    public JavaFormatter(Class kitClass) {
        super(kitClass);
    }

    protected boolean acceptSyntax(Syntax syntax) {
        return syntax instanceof JavaSyntax;
    }

    public int[] getReformatBlock(JTextComponent target, String typedText) {
        int[] ret;
        ret = null;
        BaseDocument doc = Utilities.getDocument((JTextComponent)target);
        int dotPos = target.getCaret().getDot();
        if (doc != null) {
            if ("e".equals(typedText)) {
                try {
                    int fnw = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)dotPos);
                    if (fnw >= 0 && fnw + 4 == dotPos && CharSequenceUtilities.textEquals((CharSequence)"else", (CharSequence)DocumentUtilities.getText((Document)doc, (int)fnw, (int)4))) {
                        ret = new int[]{fnw, fnw + 4};
                    }
                }
                catch (BadLocationException e) {}
            } else if (":".equals(typedText)) {
                try {
                    int fnw = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)dotPos);
                    if (fnw >= 0 && fnw + 4 <= doc.getLength() && CharSequenceUtilities.textEquals((CharSequence)"case", (CharSequence)DocumentUtilities.getText((Document)doc, (int)fnw, (int)4))) {
                        ret = new int[]{fnw, fnw + 4};
                    } else if (fnw >= 0 & fnw + 7 <= doc.getLength() && CharSequenceUtilities.textEquals((CharSequence)"default", (CharSequence)DocumentUtilities.getText((Document)doc, (int)fnw, (int)7))) {
                        ret = new int[]{fnw, fnw + 7};
                    }
                }
                catch (BadLocationException e) {}
            } else {
                ret = super.getReformatBlock(target, typedText);
            }
        }
        return ret;
    }

    protected void initFormatLayers() {
        this.addFormatLayer((FormatLayer)new StripEndWhitespaceLayer());
        this.addFormatLayer((FormatLayer)new JavaLayer());
    }

    public FormatSupport createFormatSupport(FormatWriter fw) {
        return new JavaFormatSupport(fw);
    }

    public class JavaLayer
    extends AbstractFormatLayer {
        public JavaLayer() {
            super("java-layer");
        }

        protected FormatSupport createFormatSupport(FormatWriter fw) {
            return new JavaFormatSupport(fw);
        }

        public void format(FormatWriter fw) {
            block6 : {
                try {
                    JavaFormatSupport jfs = (JavaFormatSupport)this.createFormatSupport(fw);
                    FormatTokenPosition pos = jfs.getFormatStartPosition();
                    if (jfs.isIndentOnly()) {
                        jfs.indentLine(pos);
                        break block6;
                    }
                    while (pos != null) {
                        jfs.indentLine(pos);
                        this.formatLine(jfs, pos);
                        FormatTokenPosition pos2 = jfs.findLineEnd(pos);
                        if (pos2 != null && pos2.getToken() != null && (pos = jfs.getNextPosition(pos2, Position.Bias.Forward)) != pos2 && pos != null && pos.getToken() != null) {
                            FormatTokenPosition fnw = jfs.findLineFirstNonWhitespace(pos);
                            if (fnw != null) {
                                pos = fnw;
                                continue;
                            }
                            pos = jfs.findLineStart(pos);
                            continue;
                        }
                        break;
                    }
                }
                catch (IllegalStateException e) {
                    // empty catch block
                }
            }
        }

        private void removeLineBeforeToken(TokenItem token, JavaFormatSupport jfs, boolean checkRBraceBefore) {
            FormatTokenPosition ftpos;
            FormatTokenPosition endOfPreviousLine;
            FormatTokenPosition ftp;
            FormatTokenPosition tokenPos = jfs.getPosition(token, 0);
            if (jfs.findNonWhitespace(tokenPos, null, true, true) != null) {
                return;
            }
            if (checkRBraceBefore && ((ftpos = jfs.findNonWhitespace(tokenPos, null, false, true)) == null || ftpos.getToken().getTokenID().getNumericID() != 58)) {
                return;
            }
            if (jfs.getNextPosition(tokenPos) != null && (ftp = jfs.findImportant(jfs.getNextPosition(tokenPos), null, true, false)) != null) {
                this.insertNewLineBeforeToken(ftp.getToken(), jfs);
            }
            if ((endOfPreviousLine = jfs.getPreviousPosition(ftp = jfs.findLineStart(tokenPos))) == null || endOfPreviousLine.getToken().getTokenID() != JavaTokenContext.WHITESPACE) {
                return;
            }
            ftp = jfs.findLineStart(endOfPreviousLine);
            if ((ftp = jfs.findImportant(tokenPos, ftp, false, true)) == null) {
                return;
            }
            ftp = jfs.findNonWhitespace(endOfPreviousLine, null, true, true);
            if (ftp.getToken().getTokenID() == JavaTokenContext.LINE_COMMENT || ftp.getToken().getTokenID() == JavaTokenContext.LBRACE) {
                return;
            }
            boolean remove = true;
            while (remove) {
                if (token.getPrevious() == endOfPreviousLine.getToken()) {
                    remove = false;
                }
                if (jfs.canRemoveToken(token.getPrevious())) {
                    jfs.removeToken(token.getPrevious());
                    continue;
                }
                return;
            }
            if (jfs.canInsertToken(token)) {
                jfs.insertSpaces(token, 1);
            }
        }

        private void insertNewLineBeforeToken(TokenItem token, JavaFormatSupport jfs) {
            FormatTokenPosition elsePos = jfs.getPosition(token, 0);
            FormatTokenPosition imp = jfs.findImportant(elsePos, null, true, true);
            if (imp != null && imp.getToken().getTokenContextPath() == jfs.getTokenContextPath() && jfs.canInsertToken(token)) {
                jfs.insertToken(token, jfs.getValidWhitespaceTokenID(), jfs.getValidWhitespaceTokenContextPath(), "\n");
                jfs.removeLineEndWhitespace(imp);
                jfs.indentLine(elsePos);
            }
        }

        protected void formatLine(JavaFormatSupport jfs, FormatTokenPosition pos) {
            block15 : for (TokenItem token = jfs.findLineStart((FormatTokenPosition)pos).getToken(); token != null; token = token.getNext()) {
                if (token.getTokenContextPath() != jfs.getTokenContextPath()) continue;
                switch (token.getTokenID().getNumericID()) {
                    case 82: {
                        if (jfs.getFormatNewlineBeforeBrace()) {
                            this.insertNewLineBeforeToken(token, jfs);
                            continue block15;
                        }
                        this.removeLineBeforeToken(token, jfs, true);
                        continue block15;
                    }
                    case 76: {
                        if (jfs.getFormatNewlineBeforeBrace()) {
                            this.insertNewLineBeforeToken(token, jfs);
                            continue block15;
                        }
                        this.removeLineBeforeToken(token, jfs, true);
                        continue block15;
                    }
                    case 87: {
                        if (jfs.getFormatNewlineBeforeBrace()) {
                            this.insertNewLineBeforeToken(token, jfs);
                            continue block15;
                        }
                        this.removeLineBeforeToken(token, jfs, true);
                        continue block15;
                    }
                    case 57: {
                        if (jfs.isIndentOnly()) continue block15;
                        if (jfs.getFormatNewlineBeforeBrace()) {
                            FormatTokenPosition lbracePos = jfs.getPosition(token, 0);
                            FormatTokenPosition imp = jfs.findImportant(lbracePos, null, true, true);
                            if (imp == null || imp.getToken().getTokenContextPath() != jfs.getTokenContextPath()) continue block15;
                            switch (imp.getToken().getTokenID().getNumericID()) {
                                case 7: 
                                case 8: {
                                    continue block15;
                                }
                                case 56: {
                                    continue block15;
                                }
                                case 17: 
                                case 49: 
                                case 57: {
                                    continue block15;
                                }
                            }
                            FormatTokenPosition next = jfs.findImportant(lbracePos, null, true, false);
                            if (next != null && next.getToken() != null && next.getToken().getTokenID() == JavaTokenContext.RBRACE || !jfs.canInsertToken(token)) continue block15;
                            jfs.insertToken(token, jfs.getValidWhitespaceTokenID(), jfs.getValidWhitespaceTokenContextPath(), "\n");
                            jfs.removeLineEndWhitespace(imp);
                            jfs.indentLine(lbracePos);
                            continue block15;
                        }
                        FormatTokenPosition tokenPos = jfs.getPosition(token, 0);
                        FormatTokenPosition ftpos = jfs.findNonWhitespace(tokenPos, null, false, true);
                        if (ftpos == null) continue block15;
                        switch (ftpos.getToken().getTokenID().getNumericID()) {
                            case 6: 
                            case 54: 
                            case 82: 
                            case 87: 
                            case 113: {
                                this.removeLineBeforeToken(token, jfs, false);
                            }
                        }
                        continue block15;
                    }
                    case 53: {
                        TokenItem prevprevToken;
                        TokenItem prevToken;
                        if (jfs.getFormatSpaceBeforeParenthesis()) {
                            prevToken = token.getPrevious();
                            if (prevToken == null || prevToken.getTokenID() != JavaTokenContext.IDENTIFIER && prevToken.getTokenID() != JavaTokenContext.THIS && prevToken.getTokenID() != JavaTokenContext.SUPER || !jfs.canInsertToken(token)) continue block15;
                            jfs.insertToken(token, jfs.getWhitespaceTokenID(), jfs.getWhitespaceTokenContextPath(), " ");
                            continue block15;
                        }
                        prevToken = token.getPrevious();
                        if (prevToken == null || prevToken.getTokenID() != JavaTokenContext.WHITESPACE || prevToken.getImage().length() != 1 || (prevprevToken = prevToken.getPrevious()) == null || prevprevToken.getTokenID() != JavaTokenContext.IDENTIFIER && prevprevToken.getTokenID() != JavaTokenContext.THIS && prevprevToken.getTokenID() != JavaTokenContext.SUPER || !jfs.canRemoveToken(prevToken)) continue block15;
                        jfs.removeToken(prevToken);
                    }
                }
            }
        }
    }

    public class StripEndWhitespaceLayer
    extends AbstractFormatLayer {
        public StripEndWhitespaceLayer() {
            super("java-strip-whitespace-at-line-end");
        }

        protected FormatSupport createFormatSupport(FormatWriter fw) {
            return new JavaFormatSupport(fw);
        }

        public void format(FormatWriter fw) {
            JavaFormatSupport jfs = (JavaFormatSupport)this.createFormatSupport(fw);
            FormatTokenPosition pos = jfs.getFormatStartPosition();
            if (!jfs.isIndentOnly()) {
                while (pos.getToken() != null) {
                    FormatTokenPosition startPos = pos;
                    if ((pos = jfs.removeLineEndWhitespace(pos)).getToken() != null) {
                        pos = jfs.getNextPosition(pos);
                    }
                    if (!startPos.equals((Object)pos)) continue;
                    break;
                }
            }
        }
    }

}

