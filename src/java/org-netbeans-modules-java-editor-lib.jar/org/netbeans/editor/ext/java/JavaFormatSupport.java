/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.editor.BaseImageTokenID
 *  org.netbeans.editor.BaseTokenID
 *  org.netbeans.editor.ImageTokenID
 *  org.netbeans.editor.TokenContextPath
 *  org.netbeans.editor.TokenID
 *  org.netbeans.editor.TokenItem
 *  org.netbeans.editor.ext.ExtFormatSupport
 *  org.netbeans.editor.ext.FormatTokenPosition
 *  org.netbeans.editor.ext.FormatWriter
 *  org.netbeans.modules.editor.indent.api.IndentUtils
 *  org.openide.util.Lookup
 */
package org.netbeans.editor.ext.java;

import java.util.prefs.Preferences;
import javax.swing.text.Document;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.editor.BaseImageTokenID;
import org.netbeans.editor.BaseTokenID;
import org.netbeans.editor.ImageTokenID;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;
import org.netbeans.editor.TokenItem;
import org.netbeans.editor.ext.ExtFormatSupport;
import org.netbeans.editor.ext.FormatTokenPosition;
import org.netbeans.editor.ext.FormatWriter;
import org.netbeans.editor.ext.java.JavaTokenContext;
import org.netbeans.modules.editor.indent.api.IndentUtils;
import org.openide.util.Lookup;

public class JavaFormatSupport
extends ExtFormatSupport {
    private TokenContextPath tokenContextPath;

    public JavaFormatSupport(FormatWriter formatWriter) {
        this(formatWriter, JavaTokenContext.contextPath);
    }

    public JavaFormatSupport(FormatWriter formatWriter, TokenContextPath tokenContextPath) {
        super(formatWriter);
        this.tokenContextPath = tokenContextPath;
    }

    public TokenContextPath getTokenContextPath() {
        return this.tokenContextPath;
    }

    public boolean isComment(TokenItem token, int offset) {
        TokenID tokenID = token.getTokenID();
        return token.getTokenContextPath() == this.tokenContextPath && (tokenID == JavaTokenContext.LINE_COMMENT || tokenID == JavaTokenContext.BLOCK_COMMENT);
    }

    public boolean isMultiLineComment(TokenItem token) {
        return token.getTokenID() == JavaTokenContext.BLOCK_COMMENT;
    }

    public boolean isMultiLineComment(FormatTokenPosition pos) {
        TokenItem token = pos.getToken();
        return token == null ? false : this.isMultiLineComment(token);
    }

    public boolean isJavaDocComment(TokenItem token) {
        return this.isMultiLineComment(token) && token.getImage().startsWith("/**");
    }

    public TokenID getWhitespaceTokenID() {
        return JavaTokenContext.WHITESPACE;
    }

    public TokenContextPath getWhitespaceTokenContextPath() {
        return this.tokenContextPath;
    }

    public boolean canModifyWhitespace(TokenItem inToken) {
        if (inToken.getTokenContextPath() == JavaTokenContext.contextPath) {
            switch (inToken.getTokenID().getNumericID()) {
                case 5: 
                case 8: {
                    return true;
                }
            }
        }
        return false;
    }

    public TokenItem findStatement(TokenItem token) {
        TokenItem lit = null;
        for (TokenItem t = this.getPreviousToken((TokenItem)token); t != null; t = t.getPrevious()) {
            if (t.getTokenContextPath() != this.tokenContextPath) continue;
            switch (t.getTokenID().getNumericID()) {
                case 51: {
                    if (this.isForLoopSemicolon(t)) break;
                    return lit != null ? lit : t;
                }
                case 57: 
                case 82: {
                    return lit != null ? lit : t;
                }
                case 58: {
                    if (!this.isArrayInitializationBraceBlock(t, null)) {
                        return lit != null ? lit : t;
                    }
                    t = this.findMatchingToken(t, null, (ImageTokenID)JavaTokenContext.LBRACE, true);
                    break;
                }
                case 50: {
                    TokenItem tt = this.findAnyToken(t, null, new TokenID[]{JavaTokenContext.CASE, JavaTokenContext.DEFAULT, JavaTokenContext.FOR, JavaTokenContext.QUESTION, JavaTokenContext.ASSERT}, t.getTokenContextPath(), true);
                    if (tt == null) break;
                    switch (tt.getTokenID().getNumericID()) {
                        case 75: 
                        case 80: 
                        case 88: {
                            return lit != null ? lit : t;
                        }
                    }
                    break;
                }
                case 75: 
                case 80: 
                case 81: 
                case 106: {
                    return t;
                }
                case 88: 
                case 90: 
                case 115: {
                    TokenItem mt;
                    if (lit != null && lit.getTokenID() == JavaTokenContext.LPAREN && (mt = this.findMatchingToken(lit, token, (ImageTokenID)JavaTokenContext.RPAREN, false)) != null && mt.getNext() != null && (mt = this.findImportantToken(mt.getNext(), token, false)) != null) {
                        return mt;
                    }
                    return t;
                }
            }
            if (!this.isImportant(t, 0)) continue;
            lit = t;
        }
        return lit;
    }

    public TokenItem findIf(TokenItem elseToken) {
        if (elseToken == null || !this.tokenEquals(elseToken, (TokenID)JavaTokenContext.ELSE, this.tokenContextPath)) {
            throw new IllegalArgumentException("Only accept 'else'.");
        }
        int braceDepth = 0;
        int elseDepth = 0;
        block7 : while ((elseToken = this.findStatement(elseToken)) != null) {
            switch (elseToken.getTokenID().getNumericID()) {
                case 57: {
                    if (--braceDepth >= 0) break;
                    return null;
                }
                case 58: {
                    ++braceDepth;
                    break;
                }
                case 82: {
                    if (braceDepth != 0) break;
                    ++elseDepth;
                    break;
                }
                case 50: 
                case 51: 
                case 75: 
                case 80: 
                case 81: 
                case 88: 
                case 115: {
                    break;
                }
                case 90: {
                    if (braceDepth != 0 || elseDepth-- != 0) continue block7;
                    return elseToken;
                }
            }
        }
        return null;
    }

    public TokenItem findSwitch(TokenItem caseToken) {
        if (caseToken == null || !this.tokenEquals(caseToken, (TokenID)JavaTokenContext.CASE, this.tokenContextPath) && !this.tokenEquals(caseToken, (TokenID)JavaTokenContext.DEFAULT, this.tokenContextPath)) {
            throw new IllegalArgumentException("Only accept 'case' or 'default'.");
        }
        int braceDepth = 1;
        while ((caseToken = this.findStatement(caseToken)) != null) {
            switch (caseToken.getTokenID().getNumericID()) {
                case 57: {
                    if (--braceDepth >= 0) break;
                    return null;
                }
                case 58: {
                    ++braceDepth;
                    break;
                }
                case 80: 
                case 106: {
                    if (braceDepth != 0) break;
                    return caseToken;
                }
            }
        }
        return null;
    }

    public TokenItem findTry(TokenItem catchToken) {
        if (catchToken == null || !this.tokenEquals(catchToken, (TokenID)JavaTokenContext.CATCH, this.tokenContextPath)) {
            throw new IllegalArgumentException("Only accept 'catch'.");
        }
        int braceDepth = 0;
        while ((catchToken = this.findStatement(catchToken)) != null) {
            switch (catchToken.getTokenID().getNumericID()) {
                case 57: {
                    if (--braceDepth >= 0) break;
                    return null;
                }
                case 58: {
                    ++braceDepth;
                    break;
                }
                case 113: {
                    if (braceDepth != 0) break;
                    return catchToken;
                }
            }
        }
        return null;
    }

    public TokenItem findStatementStart(TokenItem token) {
        return this.findStatementStart(token, true);
    }

    public TokenItem findStatementStart(TokenItem token, boolean outermost) {
        TokenItem t = this.findStatement(token);
        if (t != null) {
            switch (t.getTokenID().getNumericID()) {
                case 51: {
                    TokenItem scss = this.findStatement(t);
                    if (scss == null) {
                        return token;
                    }
                    switch (scss.getTokenID().getNumericID()) {
                        case 50: 
                        case 51: 
                        case 57: 
                        case 58: 
                        case 75: 
                        case 80: {
                            return t;
                        }
                        case 81: 
                        case 88: 
                        case 90: 
                        case 107: 
                        case 115: {
                            return this.findStatementStart(t, outermost);
                        }
                        case 82: {
                            TokenItem ifss = this.findIf(scss);
                            if (ifss != null) {
                                return this.findStatementStart(ifss, outermost);
                            }
                            return scss;
                        }
                    }
                    TokenItem bscss = this.findStatement(scss);
                    if (bscss != null) {
                        switch (bscss.getTokenID().getNumericID()) {
                            case 50: 
                            case 51: 
                            case 57: 
                            case 58: {
                                return scss;
                            }
                            case 81: 
                            case 88: 
                            case 90: 
                            case 107: 
                            case 115: {
                                return this.findStatementStart(bscss, outermost);
                            }
                            case 82: {
                                TokenItem ifss = this.findIf(bscss);
                                if (ifss != null) {
                                    return this.findStatementStart(ifss, outermost);
                                }
                                return bscss;
                            }
                        }
                    }
                    return scss;
                }
                case 57: {
                    return token;
                }
                case 58: {
                    TokenItem lbss;
                    TokenItem lb = this.findMatchingToken(t, null, (ImageTokenID)JavaTokenContext.LBRACE, true);
                    if (lb != null && (lbss = this.findStatement(lb)) != null) {
                        switch (lbss.getTokenID().getNumericID()) {
                            case 82: {
                                TokenItem ifss = this.findIf(lbss);
                                if (ifss != null) {
                                    return this.findStatementStart(ifss, outermost);
                                }
                                return lbss;
                            }
                            case 76: {
                                TokenItem tryss = this.findTry(lbss);
                                if (tryss != null) {
                                    return this.findStatementStart(tryss, outermost);
                                }
                                return lbss;
                            }
                            case 81: 
                            case 88: 
                            case 90: 
                            case 107: 
                            case 115: {
                                return this.findStatementStart(lbss, outermost);
                            }
                        }
                        if (lbss.getTokenID().getNumericID() == 57) {
                            return t;
                        }
                        return lbss;
                    }
                    return t;
                }
                case 50: 
                case 75: 
                case 80: {
                    return token;
                }
                case 82: {
                    TokenItem ifss = this.findIf(t);
                    return ifss != null ? this.findStatementStart(ifss, outermost) : t;
                }
                case 81: 
                case 88: 
                case 90: 
                case 107: 
                case 115: {
                    if (!outermost) {
                        return t;
                    }
                    return this.findStatementStart(t, outermost);
                }
                case 6: {
                    return t;
                }
            }
            return t;
        }
        return token;
    }

    public int getTokenIndent(TokenItem token, boolean forceFirstNonWhitespace) {
        FormatTokenPosition tp = this.getPosition(token, 0);
        FormatTokenPosition fnw = forceFirstNonWhitespace ? this.findLineFirstNonWhitespace(tp) : this.findLineFirstNonWhitespaceAndNonLeftBrace(tp);
        if (fnw != null) {
            tp = fnw;
        }
        return this.getVisualColumnOffset(tp);
    }

    public int getTokenIndent(TokenItem token) {
        return this.getTokenIndent(token, false);
    }

    public int findIndent(TokenItem token) {
        TokenItem t;
        int indent = -1;
        if (token != null) {
            block0 : switch (token.getTokenID().getNumericID()) {
                case 82: {
                    TokenItem ifss = this.findIf(token);
                    if (ifss == null) break;
                    indent = this.getTokenIndent(ifss);
                    break;
                }
                case 57: {
                    TokenItem stmt = this.findStatement(token);
                    if (stmt == null) {
                        indent = 0;
                        break;
                    }
                    switch (stmt.getTokenID().getNumericID()) {
                        case 81: 
                        case 82: 
                        case 88: 
                        case 90: 
                        case 115: {
                            indent = this.getTokenIndent(stmt);
                            break block0;
                        }
                        case 57: {
                            indent = this.getTokenIndent(stmt) + this.getShiftWidth();
                            break block0;
                        }
                    }
                    stmt = this.findStatementStart(token);
                    if (stmt == null) {
                        indent = 0;
                        break;
                    }
                    if (stmt == token) {
                        stmt = this.findStatement(token);
                        indent = stmt != null ? (indent = this.getTokenIndent(stmt)) : 0;
                        break;
                    }
                    indent = this.getTokenIndent(stmt);
                    switch (stmt.getTokenID().getNumericID()) {
                        case 57: {
                            indent += this.getShiftWidth();
                        }
                    }
                    break;
                }
                case 58: {
                    TokenItem rbmt = this.findMatchingToken(token, null, (ImageTokenID)JavaTokenContext.LBRACE, true);
                    if (rbmt != null) {
                        TokenItem t2 = this.findStatement(rbmt);
                        boolean forceFirstNonWhitespace = false;
                        if (t2 == null) {
                            t2 = rbmt;
                        } else {
                            switch (t2.getTokenID().getNumericID()) {
                                case 51: 
                                case 57: 
                                case 58: {
                                    t2 = rbmt;
                                    forceFirstNonWhitespace = true;
                                }
                            }
                        }
                        indent = this.getTokenIndent(t2, forceFirstNonWhitespace);
                        break;
                    }
                    indent = this.getTokenIndent(token);
                    break;
                }
                case 75: 
                case 80: {
                    TokenItem swss = this.findSwitch(token);
                    if (swss == null) break;
                    indent = this.getFormatOptionBoolean("indentCasesFromSwitch", true) ? this.getTokenIndent(swss) + this.getShiftWidth() : this.getTokenIndent(swss);
                }
            }
        }
        if (indent < 0 && (t = this.findImportantToken(token, null, true)) != null) {
            if (t.getTokenContextPath() != this.tokenContextPath) {
                return this.getTokenIndent(t);
            }
            switch (t.getTokenID().getNumericID()) {
                case 51: {
                    TokenItem tt = this.findStatementStart(token);
                    indent = this.getTokenIndent(tt);
                    break;
                }
                case 57: {
                    TokenItem lbss = this.findStatementStart(t, false);
                    if (lbss == null) {
                        lbss = t;
                    }
                    indent = this.getTokenIndent(lbss) + this.getShiftWidth();
                    break;
                }
                case 58: {
                    TokenItem t3 = this.findStatementStart(token);
                    indent = this.getTokenIndent(t3);
                    break;
                }
                case 50: {
                    TokenItem ttt = this.findAnyToken(t, null, new TokenID[]{JavaTokenContext.CASE, JavaTokenContext.DEFAULT, JavaTokenContext.FOR, JavaTokenContext.QUESTION, JavaTokenContext.ASSERT}, t.getTokenContextPath(), true);
                    if (ttt != null && ttt.getTokenID().getNumericID() == 52) {
                        indent = this.getTokenIndent(ttt) + this.getShiftWidth();
                        break;
                    }
                    indent = this.getTokenIndent(t) + this.getShiftWidth();
                    break;
                }
                case 52: 
                case 81: 
                case 82: {
                    indent = this.getTokenIndent(t) + this.getShiftWidth();
                    break;
                }
                case 54: {
                    TokenItem rpmt = this.findMatchingToken(t, null, (ImageTokenID)JavaTokenContext.LPAREN, true);
                    if (rpmt != null && (rpmt = this.findImportantToken(rpmt, null, true)) != null && rpmt.getTokenContextPath() == this.tokenContextPath) {
                        switch (rpmt.getTokenID().getNumericID()) {
                            case 88: 
                            case 90: 
                            case 115: {
                                indent = this.getTokenIndent(rpmt) + this.getShiftWidth();
                            }
                        }
                    }
                    if (indent >= 0) break;
                    indent = this.computeStatementIndent(t);
                    break;
                }
                case 49: {
                    if (this.isEnumComma(t)) {
                        indent = this.getTokenIndent(t);
                        break;
                    }
                }
                default: {
                    indent = this.computeStatementIndent(t);
                }
            }
            if (indent < 0) {
                indent = this.getTokenIndent(t);
            }
        }
        if (indent < 0) {
            indent = 0;
        }
        return indent;
    }

    private int computeStatementIndent(TokenItem t) {
        TokenItem stmtStart = this.findStatementStart(t);
        int indent = this.getTokenIndent(stmtStart);
        int tindent = this.getTokenIndent(t);
        if (tindent > indent) {
            return tindent;
        }
        if (stmtStart != null) {
            TokenItem maybeAnno;
            FormatTokenPosition pos;
            if (t != null && this.tokenEquals(t, (TokenID)JavaTokenContext.COMMA, this.tokenContextPath) && this.isArrayInitializationBraceBlock(t, null) && !this.isInsideParens(t, stmtStart)) {
                indent -= this.getFormatStatementContinuationIndent();
            }
            if (t != null && (pos = this.findLineFirstNonWhitespace(this.getPosition(t, 0))) != null && (maybeAnno = pos.getToken()) != null && maybeAnno.getTokenID() == JavaTokenContext.ANNOTATION) {
                indent -= this.getFormatStatementContinuationIndent();
            }
            indent += this.getFormatStatementContinuationIndent();
        }
        return indent;
    }

    public FormatTokenPosition indentLine(FormatTokenPosition pos) {
        int indent = 0;
        FormatTokenPosition firstNWS = this.findLineFirstNonWhitespace(pos);
        if (firstNWS != null) {
            if (this.isComment(firstNWS)) {
                if (this.isMultiLineComment(firstNWS) && firstNWS.getOffset() != 0) {
                    indent = this.getLineIndent(this.getPosition(firstNWS.getToken(), 0), true) + 1;
                    if (!this.isIndentOnly()) {
                        if (this.getChar(firstNWS) != '*') {
                            if (this.isJavaDocComment(firstNWS.getToken())) {
                                if (this.getFormatLeadingStarInComment()) {
                                    this.insertString(firstNWS, "* ");
                                }
                            } else {
                                indent = this.getLineIndent(pos, true);
                            }
                        } else if (this.isJavaDocComment(firstNWS.getToken()) && !this.getFormatLeadingStarInComment()) {
                            int len = -1;
                            if (firstNWS.getOffset() + 1 < firstNWS.getToken().getImage().length()) {
                                FormatTokenPosition nextCharPos = this.getPosition(firstNWS.getToken(), firstNWS.getOffset() + 1);
                                char nextChar = this.getChar(nextCharPos);
                                if (nextChar != '/') {
                                    len = this.getChar(nextCharPos) == ' ' ? 2 : 1;
                                }
                            } else {
                                len = 1;
                            }
                            if (len != -1) {
                                this.remove(firstNWS, len);
                            }
                        }
                    } else if (this.getChar(firstNWS) != '*' && this.isJavaDocComment(firstNWS.getToken()) && this.getFormatLeadingStarInComment()) {
                        this.insertString(firstNWS, "* ");
                        this.setIndentShift(2);
                    }
                } else {
                    indent = !this.isMultiLineComment(firstNWS) ? this.findIndent(firstNWS.getToken()) : (this.isJavaDocComment(firstNWS.getToken()) ? this.findIndent(firstNWS.getToken()) : (firstNWS.getToken().getImage().indexOf(10) == -1 ? this.findIndent(firstNWS.getToken()) : this.getLineIndent(firstNWS, true)));
                }
            } else {
                indent = this.findIndent(firstNWS.getToken());
            }
        } else {
            TokenItem token = pos.getToken();
            if (token == null && (token = this.findLineStart(pos).getToken()) == null) {
                token = this.getLastToken();
            }
            if (token != null && this.isMultiLineComment(token)) {
                if (this.getFormatLeadingStarInComment() && (this.isIndentOnly() || this.isJavaDocComment(token))) {
                    this.insertString(pos, "* ");
                    this.setIndentShift(2);
                }
                indent = this.getVisualColumnOffset(this.getPosition(token, 0)) + 1;
            } else {
                indent = this.findIndent(pos.getToken());
            }
        }
        return this.changeLineIndent(pos, indent);
    }

    public String getIndentString(int indent) {
        return IndentUtils.createIndentString((Document)this.getFormatWriter().getDocument(), (int)indent);
    }

    public boolean isForLoopSemicolon(TokenItem token) {
        if (token == null || !this.tokenEquals(token, (TokenID)JavaTokenContext.SEMICOLON, this.tokenContextPath)) {
            throw new IllegalArgumentException("Only accept ';'.");
        }
        int parDepth = 0;
        int braceDepth = 0;
        boolean semicolonFound = false;
        for (token = token.getPrevious(); token != null; token = token.getPrevious()) {
            if (this.tokenEquals(token, (TokenID)JavaTokenContext.LPAREN, this.tokenContextPath)) {
                if (parDepth == 0) {
                    FormatTokenPosition tp = this.getPosition(token, 0);
                    if ((tp = this.findImportant(tp, null, false, true)) != null && this.tokenEquals(tp.getToken(), (TokenID)JavaTokenContext.FOR, this.tokenContextPath)) {
                        return true;
                    }
                    return false;
                }
                --parDepth;
                continue;
            }
            if (this.tokenEquals(token, (TokenID)JavaTokenContext.RPAREN, this.tokenContextPath)) {
                ++parDepth;
                continue;
            }
            if (this.tokenEquals(token, (TokenID)JavaTokenContext.LBRACE, this.tokenContextPath)) {
                if (braceDepth == 0) {
                    return false;
                }
                --braceDepth;
                continue;
            }
            if (this.tokenEquals(token, (TokenID)JavaTokenContext.RBRACE, this.tokenContextPath)) {
                ++braceDepth;
                continue;
            }
            if (!this.tokenEquals(token, (TokenID)JavaTokenContext.SEMICOLON, this.tokenContextPath)) continue;
            if (semicolonFound) {
                return false;
            }
            semicolonFound = true;
        }
        return false;
    }

    private boolean isInsideParens(TokenItem token, TokenItem limitToken) {
        int depth = 0;
        for (token = token.getPrevious(); token != null && token != limitToken; token = token.getPrevious()) {
            if (this.tokenEquals(token, (TokenID)JavaTokenContext.LPAREN, this.tokenContextPath)) {
                if (--depth >= 0) continue;
                return true;
            }
            if (!this.tokenEquals(token, (TokenID)JavaTokenContext.RPAREN, this.tokenContextPath)) continue;
            ++depth;
        }
        return false;
    }

    private boolean isArrayInitializationBraceBlock(TokenItem token, TokenItem limitToken) {
        int depth = 0;
        block5 : for (token = token.getPrevious(); token != null && token != limitToken && token.getTokenContextPath() == this.tokenContextPath; token = token.getPrevious()) {
            switch (token.getTokenID().getNumericID()) {
                case 58: {
                    ++depth;
                    continue block5;
                }
                case 57: {
                    if (--depth >= 0) continue block5;
                    TokenItem prev = this.findImportantToken(token, limitToken, true);
                    return prev != null && prev.getTokenContextPath() == this.tokenContextPath && (JavaTokenContext.RBRACKET.equals((Object)prev.getTokenID()) || JavaTokenContext.EQ.equals((Object)prev.getTokenID()));
                }
                case 51: 
                case 81: 
                case 88: 
                case 90: 
                case 115: {
                    if (depth != 0) continue block5;
                    return false;
                }
            }
        }
        return false;
    }

    public boolean isEnumComma(TokenItem token) {
        TokenItem itm;
        TokenItem startItem;
        while (token != null && this.tokenEquals(token, (TokenID)JavaTokenContext.COMMA, this.tokenContextPath) && (itm = this.findStatementStart(token)) != token) {
            token = itm;
        }
        if (token != null && this.tokenEquals(token, (TokenID)JavaTokenContext.IDENTIFIER, this.tokenContextPath) && (itm = this.findImportantToken(token, null, true)) != null && this.tokenEquals(itm, (TokenID)JavaTokenContext.LBRACE, this.tokenContextPath) && (startItem = this.findStatementStart(itm)) != null && this.findToken(startItem, itm, (TokenID)JavaTokenContext.ENUM, this.tokenContextPath, null, false) != null) {
            return true;
        }
        return false;
    }

    public boolean getFormatSpaceBeforeParenthesis() {
        return this.getFormatOptionBoolean("spaceBeforeMethodDeclParen", false);
    }

    public boolean getFormatSpaceAfterComma() {
        return this.getFormatOptionBoolean("spaceAfterComma", true);
    }

    public boolean getFormatNewlineBeforeBrace() {
        String s;
        Preferences p = this.getFormatOptions();
        String string = s = p == null ? null : p.get("methodDeclBracePlacement", null);
        if (s != null && s.equals("NEW_LINE")) {
            return true;
        }
        return false;
    }

    public boolean getFormatLeadingSpaceInComment() {
        return false;
    }

    public boolean getFormatLeadingStarInComment() {
        return this.getFormatOptionBoolean("addLeadingStarInComment", true);
    }

    private int getFormatStatementContinuationIndent() {
        return this.getFormatOptionInt("continuationIndentSize", 8);
    }

    private boolean getFormatOptionBoolean(String optionName, boolean def) {
        Preferences p = this.getFormatOptions();
        if (p == null) {
            return def;
        }
        return p.getBoolean(optionName, def);
    }

    private int getFormatOptionInt(String optionName, int def) {
        Preferences p = this.getFormatOptions();
        if (p == null) {
            return def;
        }
        return p.getInt(optionName, def);
    }

    private Preferences getFormatOptions() {
        Lookup l = MimeLookup.getLookup((String)"text/x-java");
        return (Preferences)l.lookup(Preferences.class);
    }

    public FormatTokenPosition findLineFirstNonWhitespaceAndNonLeftBrace(FormatTokenPosition pos) {
        FormatTokenPosition eolp;
        FormatTokenPosition ftp = super.findLineFirstNonWhitespace(pos);
        if (ftp == null) {
            return null;
        }
        if (!ftp.getToken().getImage().startsWith("{")) {
            return ftp;
        }
        TokenItem rbmt = this.findMatchingToken(ftp.getToken(), (eolp = this.findNextEOL(ftp)) != null ? eolp.getToken() : null, (ImageTokenID)JavaTokenContext.RBRACE, false);
        if (rbmt != null) {
            return ftp;
        }
        FormatTokenPosition ftp_next = this.getNextPosition(ftp);
        if (ftp_next == null) {
            return ftp;
        }
        FormatTokenPosition ftp2 = this.findImportant(ftp_next, null, true, false);
        if (ftp2 != null) {
            return ftp2;
        }
        return ftp;
    }
}

