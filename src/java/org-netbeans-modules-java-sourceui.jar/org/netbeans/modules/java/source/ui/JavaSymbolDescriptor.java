/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ProjectInformation
 *  org.netbeans.modules.java.source.parsing.FileObjects
 *  org.netbeans.modules.java.source.usages.ClassIndexImpl
 *  org.netbeans.modules.java.ui.Icons
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.netbeans.spi.jumpto.symbol.SymbolDescriptor
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.java.source.ui;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.swing.Icon;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.ui.ElementOpen;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;
import org.netbeans.modules.java.ui.Icons;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.netbeans.spi.jumpto.symbol.SymbolDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public class JavaSymbolDescriptor
extends SymbolDescriptor {
    private final String displayName;
    private final ElementHandle<TypeElement> owner;
    private final ElementHandle<?> me;
    private final ElementKind kind;
    private final Set<Modifier> modifiers;
    private final FileObject root;
    private final Project project;
    private final ClassIndexImpl ci;
    private FileObject cachedFo;
    private volatile String cachedPath;

    public JavaSymbolDescriptor(@NonNull String displayName, @NonNull ElementKind kind, @NonNull Set<Modifier> modifiers, @NonNull ElementHandle<TypeElement> owner, @NonNull ElementHandle<?> me, @NullAllowed Project project, @NonNull FileObject root, @NonNull ClassIndexImpl ci) {
        assert (displayName != null);
        assert (kind != null);
        assert (modifiers != null);
        assert (owner != null);
        assert (me != null);
        assert (root != null);
        assert (ci != null);
        this.displayName = displayName;
        this.kind = kind;
        this.modifiers = modifiers;
        this.owner = owner;
        this.me = me;
        this.root = root;
        this.project = project;
        this.ci = ci;
    }

    public Icon getIcon() {
        return Icons.getElementIcon((ElementKind)this.kind, this.modifiers);
    }

    public String getSymbolName() {
        return this.displayName;
    }

    public String getOwnerName() {
        return this.owner.getQualifiedName();
    }

    public FileObject getFileObject() {
        if (this.cachedFo == null) {
            ClasspathInfo cpInfo = ClasspathInfo.create((ClassPath)ClassPath.EMPTY, (ClassPath)ClassPath.EMPTY, (ClassPath)ClassPathSupport.createClassPath((FileObject[])new FileObject[]{this.root}));
            this.cachedFo = SourceUtils.getFile(this.owner, (ClasspathInfo)cpInfo);
        }
        return this.cachedFo;
    }

    @NonNull
    public String getFileDisplayPath() {
        String res = this.cachedPath;
        if (res == null) {
            File rootFile = FileUtil.toFile((FileObject)this.root);
            if (rootFile != null) {
                try {
                    String binaryName = this.owner.getBinaryName();
                    String relativePath = this.ci.getSourceName(binaryName);
                    if (relativePath == null) {
                        relativePath = binaryName;
                        int lastDot = relativePath.lastIndexOf(46);
                        int csIndex = relativePath.indexOf(36, lastDot);
                        if (csIndex > 0 && csIndex < relativePath.length() - 1) {
                            relativePath = binaryName.substring(0, csIndex);
                        }
                        relativePath = String.format("%s.%s", FileObjects.convertPackage2Folder((String)relativePath, (char)File.separatorChar), "java");
                    }
                    res = new File(rootFile, relativePath).getAbsolutePath();
                }
                catch (IOException | InterruptedException e) {
                    Exceptions.printStackTrace((Throwable)e);
                }
            }
            if (res == null) {
                FileObject fo = this.getFileObject();
                res = fo == null ? "" : FileUtil.getFileDisplayName((FileObject)fo);
            }
            this.cachedPath = res;
        }
        return res;
    }

    public void open() {
        FileObject file = this.getFileObject();
        if (file != null) {
            ClasspathInfo cpInfo = ClasspathInfo.create((FileObject)file);
            ElementOpen.open(cpInfo, this.me);
        }
    }

    public String getProjectName() {
        ProjectInformation info = this.getProjectInfo();
        return info == null ? "" : info.getDisplayName();
    }

    public Icon getProjectIcon() {
        ProjectInformation info = this.getProjectInfo();
        return info == null ? null : info.getIcon();
    }

    public int getOffset() {
        return -1;
    }

    @NonNull
    public ElementKind getElementKind() {
        return this.kind;
    }

    @NonNull
    public Set<? extends Modifier> getModifiers() {
        return this.modifiers;
    }

    @CheckForNull
    private ProjectInformation getProjectInfo() {
        return this.project == null ? null : (ProjectInformation)this.project.getLookup().lookup(ProjectInformation.class);
    }
}

