/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.ModuleInstall
 */
package org.netbeans.modules.java.source.ui;

import org.netbeans.modules.java.source.ui.OpenProjectFastIndex;
import org.openide.modules.ModuleInstall;

public class JavaSourceUIModule
extends ModuleInstall {
    public void restored() {
        super.restored();
        OpenProjectFastIndex.getDefault();
    }
}

