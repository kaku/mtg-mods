/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.modules.java.ui.Icons
 *  org.netbeans.spi.jumpto.support.NameMatcherFactory
 *  org.netbeans.spi.jumpto.type.SearchType
 *  org.netbeans.spi.jumpto.type.TypeDescriptor
 *  org.netbeans.spi.jumpto.type.TypeProvider
 *  org.netbeans.spi.jumpto.type.TypeProvider$Context
 *  org.netbeans.spi.jumpto.type.TypeProvider$Result
 *  org.openide.cookies.EditCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.source.ui;

import java.awt.Toolkit;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.lang.model.element.ElementKind;
import javax.swing.Icon;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.modules.java.source.ui.JavaTypeProvider;
import org.netbeans.modules.java.source.ui.OpenProjectFastIndex;
import org.netbeans.modules.java.ui.Icons;
import org.netbeans.spi.jumpto.support.NameMatcherFactory;
import org.netbeans.spi.jumpto.type.SearchType;
import org.netbeans.spi.jumpto.type.TypeDescriptor;
import org.netbeans.spi.jumpto.type.TypeProvider;
import org.openide.cookies.EditCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public final class FastTypeProvider
implements TypeProvider {
    private static final Logger LOG = Logger.getLogger(FastTypeProvider.class.getName());
    private AtomicBoolean cancel = new AtomicBoolean();
    private OpenProjectFastIndex fastIndex;
    private Icon classIcon;

    public FastTypeProvider() {
        this(OpenProjectFastIndex.getDefault());
    }

    FastTypeProvider(OpenProjectFastIndex fastIndex) {
        this.fastIndex = fastIndex;
    }

    private Icon getClassIcon() {
        if (this.classIcon == null) {
            this.classIcon = Icons.getElementIcon((ElementKind)ElementKind.CLASS, (Collection)null);
        }
        return this.classIcon;
    }

    public void cancel() {
        this.cancel.set(true);
    }

    public void cleanup() {
    }

    public void computeTypeNames(TypeProvider.Context context, TypeProvider.Result result) {
        StringBuilder pattern = new StringBuilder();
        boolean sensitive = true;
        String quotedText = Pattern.quote(context.getText());
        switch (context.getSearchType()) {
            case CASE_INSENSITIVE_EXACT_NAME: {
                sensitive = false;
            }
            case CAMEL_CASE: {
                pattern.append(FastTypeProvider.createCamelCaseRegExp(context.getText(), sensitive));
                break;
            }
            case EXACT_NAME: {
                pattern.append("^").append(quotedText).append("$");
                break;
            }
            case CASE_INSENSITIVE_PREFIX: {
                sensitive = false;
            }
            case PREFIX: {
                pattern.append("^").append(quotedText);
                break;
            }
            case CASE_INSENSITIVE_REGEXP: {
                sensitive = false;
            }
            case REGEXP: {
                pattern.append(NameMatcherFactory.wildcardsToRegexp((String)JavaTypeProvider.removeNonJavaChars(context.getText()), (boolean)false));
            }
        }
        Pattern searchPattern = Pattern.compile(pattern.toString(), 8 + (sensitive ? 0 : 2));
        for (Map.Entry<FileObject, OpenProjectFastIndex.NameIndex> one : this.fastIndex.copyIndexes().entrySet()) {
            FileObject root = one.getKey();
            Project p = FileOwnerQuery.getOwner((FileObject)root);
            if (context.getProject() != null && !context.getProject().equals((Object)p)) continue;
            OpenProjectFastIndex.NameIndex fileIndex = one.getValue();
            Matcher m = searchPattern.matcher(fileIndex.files());
            while (m.find()) {
                if (this.cancel.get()) {
                    LOG.fine("Search canceled");
                    return;
                }
                if (m.start() == m.end()) continue;
                CharSequence f = fileIndex.getFilename(m.start(), m.end());
                String pkg = fileIndex.findPath(m.start());
                SimpleDescriptor desc = new SimpleDescriptor(p, root, f, pkg);
                result.addResult((TypeDescriptor)desc);
            }
        }
    }

    public String getDisplayName() {
        return NbBundle.getMessage(FastTypeProvider.class, (String)"LBL_FastJavaIndex");
    }

    public String name() {
        return "fastJavaIndex";
    }

    private static String createCamelCaseRegExp(String camel, boolean caseSensitive) {
        int index;
        StringBuilder sb = new StringBuilder();
        int lastIndex = 0;
        do {
            String token = camel.substring(lastIndex, (index = FastTypeProvider.findNextUpper(camel, lastIndex + 1)) == -1 ? camel.length() : index);
            sb.append(Pattern.quote(caseSensitive ? token : token.toLowerCase()));
            sb.append(index != -1 ? "[\\p{javaLowerCase}\\p{Digit}_\\$]*" : ".*");
            lastIndex = index;
        } while (index != -1);
        return sb.toString();
    }

    private static int findNextUpper(String text, int offset) {
        for (int i = offset; i < text.length(); ++i) {
            if (!Character.isUpperCase(text.charAt(i))) continue;
            return i;
        }
        return -1;
    }

    private class SimpleDescriptor
    extends TypeDescriptor {
        public static final String JAVA_EXTENSION = ".java";
        private FileObject root;
        private String simpleName;
        private String pkgName;
        private Project project;

        public SimpleDescriptor(Project project, FileObject root, CharSequence simpleName, CharSequence pkgName) {
            this.root = root;
            this.simpleName = simpleName.toString();
            this.pkgName = pkgName.toString();
            this.project = project;
        }

        public String getContextName() {
            return NbBundle.getMessage(FastTypeProvider.class, (String)"FMT_TypeContextName", (Object)(this.pkgName == null ? NbBundle.getMessage(FastTypeProvider.class, (String)"LBL_DefaultPackage") : this.pkgName));
        }

        @CheckForNull
        public FileObject getFileObject() {
            String s = this.simpleName;
            if (this.pkgName != null && !"".equals(this.pkgName)) {
                StringBuilder sb = new StringBuilder();
                s = sb.append(this.pkgName).append('.').append(this.simpleName).toString().replaceAll("\\.", "/");
            }
            return this.root.getFileObject(s + ".java");
        }

        public Icon getIcon() {
            return FastTypeProvider.this.getClassIcon();
        }

        public int getOffset() {
            return -1;
        }

        public String getOuterName() {
            return null;
        }

        public Icon getProjectIcon() {
            return FastTypeProvider.this.fastIndex.getProjectIcon(this.project);
        }

        public String getProjectName() {
            return FastTypeProvider.this.fastIndex.getProjectName(this.project);
        }

        public String getSimpleName() {
            return this.simpleName.toString();
        }

        public String getTypeName() {
            return this.simpleName;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(this.simpleName).append(" (");
            if (this.pkgName == null || "".equals(this.pkgName)) {
                sb.append("Default Package");
            } else {
                sb.append(this.pkgName);
            }
            sb.append(")");
            return sb.toString();
        }

        public int hashCode() {
            int hc = 17;
            hc = hc * 31 + (this.root == null ? 0 : this.root.hashCode());
            hc = hc * 31 + (this.pkgName == null ? 0 : this.pkgName.hashCode());
            hc = hc * 31 + (this.simpleName == null ? 0 : this.simpleName.hashCode());
            return hc;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SimpleDescriptor)) {
                return false;
            }
            SimpleDescriptor other = (SimpleDescriptor)((Object)obj);
            return this.root == null ? other.root == null : (this.root.equals((Object)other.root) && this.pkgName == null ? other.pkgName == null : (this.pkgName.equals(other.pkgName) && this.simpleName == null ? other.simpleName == null : this.simpleName.equals(other.simpleName)));
        }

        public void open() {
            boolean success = false;
            try {
                EditCookie cake;
                DataObject d;
                FileObject fo = this.getFileObject();
                if (fo != null && (cake = (EditCookie)(d = DataObject.find((FileObject)fo)).getCookie(EditCookie.class)) != null) {
                    cake.edit();
                    success = true;
                }
            }
            catch (DataObjectNotFoundException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            if (!success) {
                Toolkit.getDefaultToolkit().beep();
            }
        }
    }

}

