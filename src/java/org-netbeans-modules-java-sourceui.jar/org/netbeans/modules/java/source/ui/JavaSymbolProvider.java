/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.source.ClassIndex
 *  org.netbeans.api.java.source.ClassIndex$NameKind
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 *  org.netbeans.modules.java.source.indexing.TransactionContext
 *  org.netbeans.modules.java.source.indexing.TransactionContext$Service
 *  org.netbeans.modules.java.source.parsing.FileManagerTransaction
 *  org.netbeans.modules.java.source.parsing.ProcessorGenerated
 *  org.netbeans.modules.java.source.usages.ClassIndexImpl
 *  org.netbeans.modules.java.source.usages.ClassIndexManager
 *  org.netbeans.modules.java.source.usages.ClasspathInfoAccessor
 *  org.netbeans.modules.java.source.usages.DocumentUtil
 *  org.netbeans.modules.parsing.lucene.support.Convertor
 *  org.netbeans.modules.parsing.lucene.support.IndexManager
 *  org.netbeans.modules.parsing.lucene.support.IndexManager$Action
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport
 *  org.netbeans.spi.jumpto.support.NameMatcher
 *  org.netbeans.spi.jumpto.support.NameMatcherFactory
 *  org.netbeans.spi.jumpto.symbol.SymbolDescriptor
 *  org.netbeans.spi.jumpto.symbol.SymbolProvider
 *  org.netbeans.spi.jumpto.symbol.SymbolProvider$Context
 *  org.netbeans.spi.jumpto.symbol.SymbolProvider$Result
 *  org.netbeans.spi.jumpto.type.SearchType
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.Pair
 */
package org.netbeans.modules.java.source.ui;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.QualifiedNameable;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ErrorType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.type.WildcardType;
import javax.lang.model.util.SimpleTypeVisitor6;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.source.ClassIndex;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.source.indexing.TransactionContext;
import org.netbeans.modules.java.source.parsing.FileManagerTransaction;
import org.netbeans.modules.java.source.parsing.ProcessorGenerated;
import org.netbeans.modules.java.source.ui.JavaSymbolDescriptor;
import org.netbeans.modules.java.source.ui.JavaTypeProvider;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;
import org.netbeans.modules.java.source.usages.ClassIndexManager;
import org.netbeans.modules.java.source.usages.ClasspathInfoAccessor;
import org.netbeans.modules.java.source.usages.DocumentUtil;
import org.netbeans.modules.parsing.lucene.support.Convertor;
import org.netbeans.modules.parsing.lucene.support.IndexManager;
import org.netbeans.modules.parsing.spi.indexing.support.QuerySupport;
import org.netbeans.spi.jumpto.support.NameMatcher;
import org.netbeans.spi.jumpto.support.NameMatcherFactory;
import org.netbeans.spi.jumpto.symbol.SymbolDescriptor;
import org.netbeans.spi.jumpto.symbol.SymbolProvider;
import org.netbeans.spi.jumpto.type.SearchType;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.Pair;

public class JavaSymbolProvider
implements SymbolProvider {
    private static final Logger LOGGER = Logger.getLogger(JavaSymbolProvider.class.getName());
    private static final String CAPTURED_WILDCARD = "<captured wildcard>";
    private static final String UNKNOWN = "<unknown>";
    private static final String INIT = "<init>";
    private volatile boolean canceled;

    public String name() {
        return "java symbols";
    }

    public String getDisplayName() {
        return NbBundle.getMessage(JavaTypeProvider.class, (String)"MSG_JavaSymbols");
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Lifted jumps to return sites
     */
    public void computeSymbolNames(SymbolProvider.Context context, final SymbolProvider.Result result) {
        try {
            st = context.getSearchType();
            textToSearch = context.getText();
            prefix = null;
            dotIndex = textToSearch.lastIndexOf(46);
            if (dotIndex > 0 && dotIndex != textToSearch.length() - 1) {
                prefix = textToSearch.substring(0, dotIndex);
                textToSearch = textToSearch.substring(dotIndex + 1);
            }
            _ident = new String[]{textToSearch};
            switch (2.$SwitchMap$org$netbeans$spi$jumpto$type$SearchType[st.ordinal()]) {
                case 1: {
                    _kind = ClassIndex.NameKind.PREFIX;
                    _caseSensitive = true;
                    ** break;
                }
                case 2: {
                    _kind = ClassIndex.NameKind.REGEXP;
                    _ident[0] = JavaSymbolProvider.removeNonJavaChars(_ident[0]);
                    _ident[0] = NameMatcherFactory.wildcardsToRegexp((String)_ident[0], (boolean)true);
                    _caseSensitive = true;
                    ** break;
                }
                case 3: {
                    _ident = JavaSymbolProvider.createCamelCase(_ident);
                    _kind = ClassIndex.NameKind.CAMEL_CASE;
                    _caseSensitive = true;
                    ** break;
                }
                case 4: {
                    _kind = ClassIndex.NameKind.SIMPLE_NAME;
                    _caseSensitive = true;
                    ** break;
                }
                case 5: {
                    _kind = ClassIndex.NameKind.CASE_INSENSITIVE_PREFIX;
                    _caseSensitive = false;
                    ** break;
                }
                case 6: {
                    _kind = ClassIndex.NameKind.CASE_INSENSITIVE_REGEXP;
                    _caseSensitive = false;
                    ** break;
                }
                case 7: {
                    _kind = ClassIndex.NameKind.CASE_INSENSITIVE_REGEXP;
                    _ident[0] = JavaSymbolProvider.removeNonJavaChars(_ident[0]);
                    _ident[0] = NameMatcherFactory.wildcardsToRegexp((String)_ident[0], (boolean)true);
                    _caseSensitive = false;
                    ** break;
                }
            }
            throw new IllegalArgumentException();
lbl45: // 7 sources:
            ident = _ident;
            kind = _kind;
            caseSensitive = _caseSensitive;
            if (prefix != null) {
                restriction = JavaSymbolProvider.compileName(prefix, caseSensitive);
                result.setHighlightText(textToSearch);
            } else {
                restriction = null;
            }
            try {
                manager = ClassIndexManager.getDefault();
                roots = QuerySupport.findRoots((Project)null, Collections.singleton("classpath/source"), Collections.emptySet(), Collections.emptySet());
                rootUrls = new HashSet<URL>();
                for (FileObject root : roots) {
                    if (this.canceled) {
                        return;
                    }
                    rootUrls.add(root.toURL());
                }
                if (JavaSymbolProvider.LOGGER.isLoggable(Level.FINE)) {
                    JavaSymbolProvider.LOGGER.log(Level.FINE, "Querying following roots:");
                    for (URL url : rootUrls) {
                        JavaSymbolProvider.LOGGER.log(Level.FINE, "  {0}", url);
                    }
                    JavaSymbolProvider.LOGGER.log(Level.FINE, "-------------------------");
                }
                IndexManager.priorityAccess((IndexManager.Action)new IndexManager.Action<Void>(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    public Void run() throws IOException, InterruptedException {
                        for (URL url : rootUrls) {
                            if (JavaSymbolProvider.this.canceled) {
                                return null;
                            }
                            final FileObject root = URLMapper.findFileObject((URL)url);
                            if (root == null) continue;
                            final Project project = FileOwnerQuery.getOwner((FileObject)root);
                            final ClassIndexImpl impl = manager.getUsagesQuery(root.toURL(), true);
                            if (impl == null) continue;
                            final HashMap r = new HashMap();
                            for (String currentIdent : ident) {
                                impl.getDeclaredElements(currentIdent, kind, DocumentUtil.elementHandleConvertor(), r);
                            }
                            if (r.isEmpty()) continue;
                            TransactionContext.beginTrans().register(FileManagerTransaction.class, (TransactionContext.Service)FileManagerTransaction.read()).register(ProcessorGenerated.class, (TransactionContext.Service)ProcessorGenerated.nullWrite());
                            try {
                                ClasspathInfo cpInfo = ClasspathInfoAccessor.getINSTANCE().create(root, null, true, true, false, false);
                                JavaSource js = JavaSource.create((ClasspathInfo)cpInfo, (FileObject[])new FileObject[0]);
                                js.runUserActionTask((Task)new Task<CompilationController>(){

                                    public void run(CompilationController controller) {
                                        for (Map.Entry p : r.entrySet()) {
                                            ElementHandle owner = (ElementHandle)p.getKey();
                                            TypeElement te = (TypeElement)owner.resolve((CompilationInfo)controller);
                                            Set idents = (Set)p.getValue();
                                            if (te == null) continue;
                                            if (idents.contains(this.getSimpleName(te, null)) && JavaSymbolProvider.this.matchesRestrictions(te, restriction)) {
                                                result.addResult((SymbolDescriptor)new JavaSymbolDescriptor(te.getSimpleName().toString(), te.getKind(), te.getModifiers(), owner, ElementHandle.create((Element)te), project, root, impl));
                                            }
                                            for (Element ne : te.getEnclosedElements()) {
                                                if (!idents.contains(this.getSimpleName(ne, te)) || !JavaSymbolProvider.this.matchesRestrictions(ne, restriction)) continue;
                                                result.addResult((SymbolDescriptor)new JavaSymbolDescriptor(JavaSymbolProvider.getDisplayName(ne, te), ne.getKind(), ne.getModifiers(), owner, ElementHandle.create((Element)ne), project, root, impl));
                                            }
                                        }
                                    }

                                    private String getSimpleName(@NonNull Element element, @NullAllowed Element enclosingElement) {
                                        String result = element.getSimpleName().toString();
                                        if (enclosingElement != null && "<init>".equals(result)) {
                                            result = enclosingElement.getSimpleName().toString();
                                        }
                                        if (!caseSensitive) {
                                            result = result.toLowerCase();
                                        }
                                        return result;
                                    }
                                }, true);
                                continue;
                            }
                            finally {
                                TransactionContext.get().commit();
                                continue;
                            }
                        }
                        return null;
                    }

                });
                return;
            }
            catch (IOException ioe) {
                Exceptions.printStackTrace((Throwable)ioe);
                return;
            }
            catch (InterruptedException ie) {
                this.cleanup();
                return;
            }
        }
        finally {
            this.cleanup();
        }
    }

    private boolean matchesRestrictions(@NonNull Element e, @NullAllowed Pair<NameMatcher, Boolean> restriction) {
        if (restriction == null) {
            return true;
        }
        Element owner = e.getEnclosingElement();
        if (owner == null) {
            return false;
        }
        Name n = (Boolean)restriction.second() != false && owner instanceof QualifiedNameable ? ((QualifiedNameable)owner).getQualifiedName() : owner.getSimpleName();
        return ((NameMatcher)restriction.first()).accept(n.toString());
    }

    private static Pair<NameMatcher, Boolean> compileName(@NonNull String prefix, boolean caseSensitive) {
        boolean fqn;
        boolean bl = fqn = prefix.indexOf(46) > 0;
        SearchType searchType = JavaSymbolProvider.containsWildCard(prefix) ? (caseSensitive ? SearchType.REGEXP : SearchType.CASE_INSENSITIVE_REGEXP) : (caseSensitive ? SearchType.PREFIX : SearchType.CASE_INSENSITIVE_PREFIX);
        return Pair.of((Object)NameMatcherFactory.createNameMatcher((String)prefix, (SearchType)searchType), (Object)fqn);
    }

    private static boolean containsWildCard(String text) {
        for (int i = 0; i < text.length(); ++i) {
            if (text.charAt(i) != '?' && text.charAt(i) != '*') continue;
            return true;
        }
        return false;
    }

    private static String getDisplayName(@NonNull Element e, @NonNull Element enclosingElement) {
        assert (e != null);
        if (e.getKind() == ElementKind.METHOD || e.getKind() == ElementKind.CONSTRUCTOR) {
            StringBuilder sb = new StringBuilder();
            if (e.getKind() == ElementKind.CONSTRUCTOR) {
                sb.append(enclosingElement.getSimpleName());
            } else {
                sb.append(e.getSimpleName());
            }
            sb.append('(');
            ExecutableElement ee = (ExecutableElement)e;
            List<? extends VariableElement> vl = ee.getParameters();
            Iterator<? extends VariableElement> it = vl.iterator();
            while (it.hasNext()) {
                VariableElement v = it.next();
                TypeMirror tm = v.asType();
                sb.append(JavaSymbolProvider.getTypeName(tm, false, true));
                if (!it.hasNext()) continue;
                sb.append(", ");
            }
            sb.append(')');
            return sb.toString();
        }
        return e.getSimpleName().toString();
    }

    private static String[] createCamelCase(String[] text) {
        if (text[0].length() == 0) {
            return text;
        }
        return new String[]{text[0], "" + Character.toLowerCase(text[0].charAt(0)) + text[0].substring(1)};
    }

    private static CharSequence getTypeName(TypeMirror type, boolean fqn, boolean varArg) {
        if (type == null) {
            return "";
        }
        return (CharSequence)new TypeNameVisitor(varArg).visit(type, fqn);
    }

    private static String removeNonJavaChars(String text) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < text.length(); ++i) {
            char c = text.charAt(i);
            if (!Character.isJavaIdentifierPart(c) && c != '*' && c != '?') continue;
            sb.append(c);
        }
        return sb.toString();
    }

    public void cancel() {
        this.canceled = true;
    }

    public void cleanup() {
        this.canceled = false;
    }

    static class 2 {
        static final /* synthetic */ int[] $SwitchMap$org$netbeans$spi$jumpto$type$SearchType;

        static {
            $SwitchMap$org$netbeans$spi$jumpto$type$SearchType = new int[SearchType.values().length];
            try {
                2.$SwitchMap$org$netbeans$spi$jumpto$type$SearchType[SearchType.PREFIX.ordinal()] = 1;
            }
            catch (NoSuchFieldError ex) {
                // empty catch block
            }
            try {
                2.$SwitchMap$org$netbeans$spi$jumpto$type$SearchType[SearchType.REGEXP.ordinal()] = 2;
            }
            catch (NoSuchFieldError ex) {
                // empty catch block
            }
            try {
                2.$SwitchMap$org$netbeans$spi$jumpto$type$SearchType[SearchType.CAMEL_CASE.ordinal()] = 3;
            }
            catch (NoSuchFieldError ex) {
                // empty catch block
            }
            try {
                2.$SwitchMap$org$netbeans$spi$jumpto$type$SearchType[SearchType.EXACT_NAME.ordinal()] = 4;
            }
            catch (NoSuchFieldError ex) {
                // empty catch block
            }
            try {
                2.$SwitchMap$org$netbeans$spi$jumpto$type$SearchType[SearchType.CASE_INSENSITIVE_PREFIX.ordinal()] = 5;
            }
            catch (NoSuchFieldError ex) {
                // empty catch block
            }
            try {
                2.$SwitchMap$org$netbeans$spi$jumpto$type$SearchType[SearchType.CASE_INSENSITIVE_EXACT_NAME.ordinal()] = 6;
            }
            catch (NoSuchFieldError ex) {
                // empty catch block
            }
            try {
                2.$SwitchMap$org$netbeans$spi$jumpto$type$SearchType[SearchType.CASE_INSENSITIVE_REGEXP.ordinal()] = 7;
            }
            catch (NoSuchFieldError ex) {
                // empty catch block
            }
        }
    }

    private static class TypeNameVisitor
    extends SimpleTypeVisitor6<StringBuilder, Boolean> {
        private boolean varArg;
        private boolean insideCapturedWildcard = false;

        private TypeNameVisitor(boolean varArg) {
            super(new StringBuilder());
            this.varArg = varArg;
        }

        @Override
        public StringBuilder defaultAction(TypeMirror t, Boolean p) {
            return ((StringBuilder)this.DEFAULT_VALUE).append(t);
        }

        @Override
        public StringBuilder visitDeclared(DeclaredType t, Boolean p) {
            Element e = t.asElement();
            if (e instanceof TypeElement) {
                TypeElement te = (TypeElement)e;
                ((StringBuilder)this.DEFAULT_VALUE).append((p != false ? te.getQualifiedName() : te.getSimpleName()).toString());
                Iterator<? extends TypeMirror> it = t.getTypeArguments().iterator();
                if (it.hasNext()) {
                    ((StringBuilder)this.DEFAULT_VALUE).append("<");
                    while (it.hasNext()) {
                        this.visit(it.next(), p);
                        if (!it.hasNext()) continue;
                        ((StringBuilder)this.DEFAULT_VALUE).append(", ");
                    }
                    ((StringBuilder)this.DEFAULT_VALUE).append(">");
                }
                return (StringBuilder)this.DEFAULT_VALUE;
            }
            return ((StringBuilder)this.DEFAULT_VALUE).append("<unknown>");
        }

        @Override
        public StringBuilder visitArray(ArrayType t, Boolean p) {
            boolean isVarArg = this.varArg;
            this.varArg = false;
            this.visit(t.getComponentType(), p);
            return ((StringBuilder)this.DEFAULT_VALUE).append(isVarArg ? "..." : "[]");
        }

        @Override
        public StringBuilder visitTypeVariable(TypeVariable t, Boolean p) {
            String name;
            Element e = t.asElement();
            if (e != null && !"<captured wildcard>".equals(name = e.getSimpleName().toString())) {
                return ((StringBuilder)this.DEFAULT_VALUE).append(name);
            }
            ((StringBuilder)this.DEFAULT_VALUE).append("?");
            if (!this.insideCapturedWildcard) {
                this.insideCapturedWildcard = true;
                TypeMirror bound = t.getLowerBound();
                if (bound != null && bound.getKind() != TypeKind.NULL) {
                    ((StringBuilder)this.DEFAULT_VALUE).append(" super ");
                    this.visit(bound, p);
                } else {
                    bound = t.getUpperBound();
                    if (bound != null && bound.getKind() != TypeKind.NULL) {
                        ((StringBuilder)this.DEFAULT_VALUE).append(" extends ");
                        if (bound.getKind() == TypeKind.TYPEVAR) {
                            bound = ((TypeVariable)bound).getLowerBound();
                        }
                        this.visit(bound, p);
                    }
                }
                this.insideCapturedWildcard = false;
            }
            return (StringBuilder)this.DEFAULT_VALUE;
        }

        @Override
        public StringBuilder visitWildcard(WildcardType t, Boolean p) {
            int len = ((StringBuilder)this.DEFAULT_VALUE).length();
            ((StringBuilder)this.DEFAULT_VALUE).append("?");
            TypeMirror bound = t.getSuperBound();
            if (bound == null) {
                bound = t.getExtendsBound();
                if (bound != null) {
                    ((StringBuilder)this.DEFAULT_VALUE).append(" extends ");
                    if (bound.getKind() == TypeKind.WILDCARD) {
                        bound = ((WildcardType)bound).getSuperBound();
                    }
                    this.visit(bound, p);
                } else if (!(len != 0 || (bound = SourceUtils.getBound((WildcardType)t)) == null || bound.getKind() == TypeKind.DECLARED && ((TypeElement)((DeclaredType)bound).asElement()).getQualifiedName().contentEquals("java.lang.Object"))) {
                    ((StringBuilder)this.DEFAULT_VALUE).append(" extends ");
                    this.visit(bound, p);
                }
            } else {
                ((StringBuilder)this.DEFAULT_VALUE).append(" super ");
                this.visit(bound, p);
            }
            return (StringBuilder)this.DEFAULT_VALUE;
        }

        @Override
        public StringBuilder visitError(ErrorType t, Boolean p) {
            Element e = t.asElement();
            if (e instanceof TypeElement) {
                TypeElement te = (TypeElement)e;
                return ((StringBuilder)this.DEFAULT_VALUE).append((p != false ? te.getQualifiedName() : te.getSimpleName()).toString());
            }
            return (StringBuilder)this.DEFAULT_VALUE;
        }
    }

}

