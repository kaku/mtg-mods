/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.document.Document
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.api.java.queries.SourceForBinaryQuery
 *  org.netbeans.api.java.queries.SourceForBinaryQuery$Result
 *  org.netbeans.api.java.source.ClassIndex
 *  org.netbeans.api.java.source.ClassIndex$NameKind
 *  org.netbeans.api.java.source.ClassIndex$SearchScope
 *  org.netbeans.api.java.source.ClassIndex$SearchScopeType
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.ClasspathInfo$PathKind
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ProjectInformation
 *  org.netbeans.modules.java.BinaryElementOpen
 *  org.netbeans.modules.java.source.usages.ClassIndexImpl
 *  org.netbeans.modules.java.source.usages.ClassIndexImplEvent
 *  org.netbeans.modules.java.source.usages.ClassIndexImplListener
 *  org.netbeans.modules.java.source.usages.ClassIndexManager
 *  org.netbeans.modules.java.source.usages.DocumentUtil
 *  org.netbeans.modules.parsing.lucene.support.Convertor
 *  org.netbeans.modules.parsing.lucene.support.Index
 *  org.netbeans.modules.parsing.lucene.support.Index$IndexClosedException
 *  org.netbeans.modules.parsing.lucene.support.IndexManager
 *  org.netbeans.modules.parsing.lucene.support.IndexManager$Action
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.netbeans.spi.jumpto.support.NameMatcherFactory
 *  org.netbeans.spi.jumpto.type.SearchType
 *  org.netbeans.spi.jumpto.type.TypeProvider
 *  org.netbeans.spi.jumpto.type.TypeProvider$Context
 *  org.netbeans.spi.jumpto.type.TypeProvider$Result
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.java.source.ui;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.lang.model.element.TypeElement;
import javax.swing.Icon;
import org.apache.lucene.document.Document;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.queries.SourceForBinaryQuery;
import org.netbeans.api.java.source.ClassIndex;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.ui.TypeElementFinder;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.netbeans.modules.java.BinaryElementOpen;
import org.netbeans.modules.java.source.ui.JavaTypeDescription;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;
import org.netbeans.modules.java.source.usages.ClassIndexImplEvent;
import org.netbeans.modules.java.source.usages.ClassIndexImplListener;
import org.netbeans.modules.java.source.usages.ClassIndexManager;
import org.netbeans.modules.java.source.usages.DocumentUtil;
import org.netbeans.modules.parsing.lucene.support.Convertor;
import org.netbeans.modules.parsing.lucene.support.Index;
import org.netbeans.modules.parsing.lucene.support.IndexManager;
import org.netbeans.modules.parsing.spi.indexing.support.QuerySupport;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.netbeans.spi.jumpto.support.NameMatcherFactory;
import org.netbeans.spi.jumpto.type.SearchType;
import org.netbeans.spi.jumpto.type.TypeProvider;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Parameters;

public class JavaTypeProvider
implements TypeProvider {
    private static final Logger LOGGER = Logger.getLogger(JavaTypeProvider.class.getName());
    private static final Level LEVEL = Level.FINE;
    private static final Collection<? extends JavaTypeDescription> ACTIVE = Collections.unmodifiableSet(new HashSet());
    private Map<URI, CacheItem> rootCache;
    private volatile boolean isCanceled = false;
    private ClasspathInfo cpInfo;
    private final TypeElementFinder.Customizer customizer;
    private static Pattern camelCasePattern = Pattern.compile("(?:\\p{javaUpperCase}(?:\\p{javaLowerCase}|\\p{Digit}|\\.|\\$)*){2,}");

    public String name() {
        return "java";
    }

    public String getDisplayName() {
        return "Java Classes";
    }

    public void cleanup() {
        this.isCanceled = false;
        DataCache.clear();
        this.setRootCache(null);
    }

    public void cancel() {
        this.isCanceled = true;
    }

    public JavaTypeProvider() {
        this(null, null);
    }

    public JavaTypeProvider(ClasspathInfo cpInfo, TypeElementFinder.Customizer customizer) {
        this.cpInfo = cpInfo;
        this.customizer = customizer;
    }

    public void computeTypeNames(TypeProvider.Context context, TypeProvider.Result res) {
        Map<URI, CacheItem> c;
        Pattern packageName;
        boolean isFullyQualifiedName;
        ClassIndex.NameKind nameKind;
        String typeName;
        this.isCanceled = false;
        String originalText = context.getText();
        SearchType searchType = context.getSearchType();
        final DataCache dataCache = DataCache.forText(originalText, searchType);
        assert (dataCache != null);
        CacheItem.DataCacheCallback callBack = new CacheItem.DataCacheCallback(){

            @Override
            public void handleDataCacheChange(@NonNull CacheItem ci) {
                assert (ci != null);
                dataCache.put(ci, null);
            }
        };
        boolean hasBinaryOpen = Lookup.getDefault().lookup(BinaryElementOpen.class) != null;
        switch (searchType) {
            case EXACT_NAME: {
                nameKind = ClassIndex.NameKind.SIMPLE_NAME;
                break;
            }
            case CASE_INSENSITIVE_EXACT_NAME: {
                nameKind = ClassIndex.NameKind.CASE_INSENSITIVE_REGEXP;
                break;
            }
            case PREFIX: {
                nameKind = ClassIndex.NameKind.PREFIX;
                break;
            }
            case CASE_INSENSITIVE_PREFIX: {
                nameKind = ClassIndex.NameKind.CASE_INSENSITIVE_PREFIX;
                break;
            }
            case REGEXP: {
                nameKind = ClassIndex.NameKind.REGEXP;
                break;
            }
            case CASE_INSENSITIVE_REGEXP: {
                nameKind = ClassIndex.NameKind.CASE_INSENSITIVE_REGEXP;
                break;
            }
            case CAMEL_CASE: {
                nameKind = ClassIndex.NameKind.CAMEL_CASE;
                break;
            }
            default: {
                throw new RuntimeException("Unexpected search type: " + (Object)searchType);
            }
        }
        if (this.getRootCache() == null) {
            HashMap<URI, CacheItem> sources = null;
            if (this.cpInfo == null) {
                sources = new HashMap<URI, CacheItem>();
                Collection srcRoots = QuerySupport.findRoots((Project)null, Collections.singleton("classpath/source"), Collections.emptySet(), Collections.emptySet());
                for (FileObject root : srcRoots) {
                    if (this.isCanceled) {
                        return;
                    }
                    URL rootUrl = root.toURL();
                    if (this.isCanceled) {
                        return;
                    }
                    try {
                        sources.put(rootUrl.toURI(), new CacheItem(rootUrl, "classpath/source", callBack));
                    }
                    catch (URISyntaxException ex) {
                        LOGGER.log(Level.INFO, "Cannot convert root {0} into URI, ignoring.", rootUrl);
                    }
                }
                Collection binRoots = QuerySupport.findRoots((Project)null, Collections.emptySet(), Collections.emptySet(), Arrays.asList("classpath/compile", "classpath/boot"));
                for (FileObject root2 : binRoots) {
                    SourceForBinaryQuery.Result result;
                    if (this.isCanceled) {
                        return;
                    }
                    URL rootUrl = root2.toURL();
                    if (!hasBinaryOpen && (result = SourceForBinaryQuery.findSourceRoots((URL)rootUrl)).getRoots().length == 0) continue;
                    if (this.isCanceled) {
                        return;
                    }
                    try {
                        URI rootURI = rootUrl.toURI();
                        if (sources.containsKey(rootURI)) continue;
                        sources.put(rootURI, new CacheItem(rootUrl, "classpath/boot", callBack));
                    }
                    catch (URISyntaxException ex) {
                        LOGGER.log(Level.INFO, "Cannot convert root {0} into URI, ignoring.", rootUrl);
                    }
                }
            } else {
                List bootRoots = this.cpInfo.getClassPath(ClasspathInfo.PathKind.BOOT).entries();
                List compileRoots = this.cpInfo.getClassPath(ClasspathInfo.PathKind.COMPILE).entries();
                List sourceRoots = this.cpInfo.getClassPath(ClasspathInfo.PathKind.SOURCE).entries();
                sources = new HashMap(bootRoots.size() + compileRoots.size() + sourceRoots.size());
                for (ClassPath.Entry entry22 : bootRoots) {
                    if (this.isCanceled) {
                        return;
                    }
                    try {
                        sources.put(entry22.getURL().toURI(), new CacheItem(entry22.getURL(), "classpath/boot", callBack));
                    }
                    catch (URISyntaxException ex) {
                        LOGGER.log(Level.INFO, "Cannot convert root {0} into URI, ignoring.", entry22.getURL());
                    }
                }
                for (ClassPath.Entry entry22 : compileRoots) {
                    if (this.isCanceled) {
                        return;
                    }
                    try {
                        sources.put(entry22.getURL().toURI(), new CacheItem(entry22.getURL(), "classpath/compile", callBack));
                    }
                    catch (URISyntaxException ex) {
                        LOGGER.log(Level.INFO, "Cannot convert root {0} into URI, ignoring.", entry22.getURL());
                    }
                }
                for (ClassPath.Entry entry22 : sourceRoots) {
                    if (this.isCanceled) {
                        return;
                    }
                    try {
                        sources.put(entry22.getURL().toURI(), new CacheItem(entry22.getURL(), "classpath/source", callBack));
                    }
                    catch (URISyntaxException ex) {
                        LOGGER.log(Level.INFO, "Cannot convert root {0} into URI, ignoring.", entry22.getURL());
                    }
                }
            }
            if (!this.isCanceled) {
                if (LOGGER.isLoggable(LEVEL)) {
                    LOGGER.log(LEVEL, "Querying following roots:");
                    for (CacheItem ci : sources.values()) {
                        LOGGER.log(LEVEL, "  {0}; binary={1}", new Object[]{ci.getRoot().toURI(), ci.isBinary()});
                    }
                    LOGGER.log(LEVEL, "-------------------------");
                }
                this.setRootCache(sources);
            } else {
                return;
            }
        }
        if ((c = this.getRootCache()) == null) {
            return;
        }
        final ArrayList<? extends JavaTypeDescription> types = new ArrayList<JavaTypeDescription>(c.size() * 20);
        boolean scanInProgress = SourceUtils.isScanInProgress();
        if (scanInProgress) {
            String message = NbBundle.getMessage(JavaTypeProvider.class, (String)"LBL_ScanInProgress_warning");
            res.setMessage(message);
        } else {
            res.setMessage(null);
        }
        int lastIndexOfDot = originalText.lastIndexOf(".");
        boolean bl = isFullyQualifiedName = -1 != lastIndexOfDot;
        if (isFullyQualifiedName) {
            packageName = JavaTypeProvider.createPackageRegExp(originalText.substring(0, lastIndexOfDot));
            typeName = originalText.substring(lastIndexOfDot + 1);
            res.setHighlightText(typeName);
        } else {
            packageName = null;
            typeName = originalText;
        }
        final String textForQuery = JavaTypeProvider.getTextForQuery(typeName, nameKind, context.getSearchType());
        LOGGER.log(Level.FINE, "Text For Query ''{0}''.", originalText);
        if (this.customizer != null) {
            Iterator<CacheItem> i$ = c.values().iterator();
            while (i$.hasNext()) {
                CacheItem ci;
                HashSet<ElementHandle<TypeElement>> names = new HashSet<ElementHandle<TypeElement>>(this.customizer.query(ci.getClasspathInfo(), textForQuery, nameKind, EnumSet.of((ci = i$.next()).isBinary ? ClassIndex.SearchScope.DEPENDENCIES : ClassIndex.SearchScope.SOURCE)));
                if (nameKind == ClassIndex.NameKind.CAMEL_CASE) {
                    names.addAll(this.customizer.query(ci.getClasspathInfo(), textForQuery, ClassIndex.NameKind.CASE_INSENSITIVE_PREFIX, EnumSet.of(ci.isBinary ? ClassIndex.SearchScope.DEPENDENCIES : ClassIndex.SearchScope.SOURCE)));
                }
                for (ElementHandle<TypeElement> name : names) {
                    ci.initIndex();
                    JavaTypeDescription td = new JavaTypeDescription(ci, name);
                    types.add(td);
                    if (!this.isCanceled) continue;
                    return;
                }
            }
        } else {
            final ArrayDeque<CacheItem> nonCached = new ArrayDeque<CacheItem>(c.size());
            for (CacheItem ci : c.values()) {
                Collection<? extends JavaTypeDescription> cacheLine = dataCache.get(ci);
                if (cacheLine != null) {
                    types.addAll(cacheLine);
                    continue;
                }
                nonCached.add(ci);
            }
            if (!nonCached.isEmpty()) {
                try {
                    IndexManager.priorityAccess((IndexManager.Action)new IndexManager.Action<Void>(){

                        /*
                         * WARNING - Removed try catching itself - possible behaviour change.
                         */
                        public Void run() throws IOException, InterruptedException {
                            for (CacheItem ci : nonCached) {
                                if (JavaTypeProvider.this.isCanceled) {
                                    return null;
                                }
                                try {
                                    block10 : {
                                        Collection ct = new ArrayList();
                                        boolean exists = false;
                                        dataCache.put(ci, ACTIVE);
                                        try {
                                            exists = ci.collectDeclaredTypes(packageName, textForQuery, nameKind, ct);
                                            if (exists) {
                                                types.addAll(ct);
                                            }
                                            if (!exists) break block10;
                                            dataCache.compareAndSet(ci, ACTIVE, ct.isEmpty() ? Collections.emptySet() : ct);
                                        }
                                        catch (Throwable var5_7) {
                                            if (exists) {
                                                dataCache.compareAndSet(ci, ACTIVE, ct.isEmpty() ? Collections.emptySet() : ct);
                                            } else {
                                                dataCache.put(ci, null);
                                            }
                                            throw var5_7;
                                        }
                                        continue;
                                    }
                                    dataCache.put(ci, null);
                                    continue;
                                }
                                catch (IOException ioe) {
                                    Exceptions.printStackTrace((Throwable)ioe);
                                    continue;
                                }
                                catch (InterruptedException ie) {
                                    throw new AssertionError(ie);
                                }
                            }
                            return null;
                        }
                    });
                }
                catch (IOException ex) {
                    throw new AssertionError(ex);
                }
                catch (InterruptedException ex) {
                    throw new AssertionError(ex);
                }
            }
            if (this.isCanceled) {
                return;
            }
            if (scanInProgress) {
                res.pendingResult();
            }
        }
        if (!this.isCanceled) {
            res.addResult(types);
        }
    }

    static String removeNonJavaChars(String text) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < text.length(); ++i) {
            char c = text.charAt(i);
            if (!Character.isJavaIdentifierPart(c) && c != '*' && c != '?') continue;
            sb.append(c);
        }
        return sb.toString();
    }

    @CheckForNull
    private Map<URI, CacheItem> getRootCache() {
        if (LOGGER.isLoggable(LEVEL) && this.rootCache == null) {
            LOGGER.log(LEVEL, "Returning null cache entries.", new Exception());
        }
        return this.rootCache == null ? null : Collections.unmodifiableMap(this.rootCache);
    }

    private void setRootCache(@NullAllowed Map<URI, CacheItem> cache) {
        if (LOGGER.isLoggable(LEVEL)) {
            LOGGER.log(LEVEL, "Setting cache entries from " + this.rootCache + " to " + cache + ".", new Exception());
        }
        if (this.rootCache != null) {
            for (CacheItem ci : this.rootCache.values()) {
                ci.dispose();
            }
        }
        this.rootCache = cache;
    }

    private static String getTextForQuery(String text, ClassIndex.NameKind nameKind, SearchType searchType) {
        String textForQuery;
        switch (nameKind) {
            case REGEXP: 
            case CASE_INSENSITIVE_REGEXP: {
                textForQuery = NameMatcherFactory.wildcardsToRegexp((String)JavaTypeProvider.removeNonJavaChars(text), (boolean)(searchType != SearchType.CASE_INSENSITIVE_EXACT_NAME));
                break;
            }
            default: {
                textForQuery = text;
            }
        }
        return textForQuery;
    }

    @NonNull
    private static Pattern createPackageRegExp(@NonNull String pkgName) {
        StringBuilder sb = new StringBuilder();
        sb.append("(.*\\.)?");
        for (int i = 0; i < pkgName.length(); ++i) {
            char c = pkgName.charAt(i);
            if (Character.isJavaIdentifierPart(c)) {
                sb.append(c);
                continue;
            }
            if (c != '.') continue;
            sb.append(".*\\.");
        }
        sb.append(".*(\\..*)?");
        LOGGER.log(Level.FINE, "Package pattern: {0}", sb);
        return Pattern.compile(sb.toString());
    }

    @NonNull
    private static ClassIndex.NameKind translateSearchType(@NonNull String simpleName, @NonNull ClassIndex.NameKind originalSearchType) {
        if (originalSearchType == ClassIndex.NameKind.SIMPLE_NAME || originalSearchType == ClassIndex.NameKind.CASE_INSENSITIVE_REGEXP) {
            return originalSearchType;
        }
        if (JavaTypeProvider.isAllUpper(simpleName) && simpleName.length() > 1 || JavaTypeProvider.isCamelCase(simpleName)) {
            return ClassIndex.NameKind.CAMEL_CASE;
        }
        if (JavaTypeProvider.containsWildCard(simpleName) != -1) {
            return JavaTypeProvider.isCaseSensitive(originalSearchType) ? ClassIndex.NameKind.REGEXP : ClassIndex.NameKind.CASE_INSENSITIVE_REGEXP;
        }
        return JavaTypeProvider.isCaseSensitive(originalSearchType) ? ClassIndex.NameKind.PREFIX : ClassIndex.NameKind.CASE_INSENSITIVE_PREFIX;
    }

    private static boolean isCaseSensitive(@NonNull ClassIndex.NameKind originalNameKind) {
        switch (originalNameKind) {
            case CASE_INSENSITIVE_REGEXP: 
            case CAMEL_CASE_INSENSITIVE: 
            case CASE_INSENSITIVE_PREFIX: {
                return false;
            }
        }
        return true;
    }

    private static int containsWildCard(@NonNull String text) {
        for (int i = 0; i < text.length(); ++i) {
            if (text.charAt(i) != '?' && text.charAt(i) != '*') continue;
            return i;
        }
        return -1;
    }

    private static boolean isAllUpper(@NonNull String text) {
        for (int i = 0; i < text.length(); ++i) {
            if (Character.isUpperCase(text.charAt(i))) continue;
            return false;
        }
        return true;
    }

    private static boolean isCamelCase(String text) {
        return camelCasePattern.matcher(text).matches();
    }

    private static final class DataCache {
        private static String forText;
        private static final Map<SearchType, DataCache> instances;
        private final Map<CacheItem, Collection<? extends JavaTypeDescription>> dataCache = new HashMap<CacheItem, Collection<? extends JavaTypeDescription>>();

        private DataCache() {
        }

        @CheckForNull
        synchronized Collection<? extends JavaTypeDescription> get(@NonNull CacheItem item) {
            return this.dataCache.get(item);
        }

        synchronized void put(@NonNull CacheItem item, @NullAllowed Collection<? extends JavaTypeDescription> data) {
            this.dataCache.put(item, data);
        }

        synchronized boolean compareAndSet(@NonNull CacheItem item, @NullAllowed Collection<? extends JavaTypeDescription> expected, @NullAllowed Collection<? extends JavaTypeDescription> update) {
            if (this.dataCache.get(item) == expected) {
                this.dataCache.put(item, update);
                return true;
            }
            return false;
        }

        static synchronized void clear() {
            forText = null;
            instances.clear();
        }

        @NonNull
        static synchronized DataCache forText(@NonNull String text, @NonNull SearchType searchType) {
            DataCache cacheInstance;
            Parameters.notNull((CharSequence)"text", (Object)text);
            Parameters.notNull((CharSequence)"searchType", (Object)searchType);
            if (!text.equals(forText)) {
                DataCache.clear();
                forText = text;
            }
            if ((cacheInstance = instances.get((Object)searchType)) == null) {
                cacheInstance = new DataCache();
                instances.put(searchType, cacheInstance);
            }
            return cacheInstance;
        }

        static {
            instances = new EnumMap<SearchType, DataCache>(SearchType.class);
        }
    }

    static final class CacheItem
    implements ClassIndexImplListener {
        private final URI rootURI;
        private final boolean isBinary;
        private final String cpType;
        private DataCacheCallback callBack;
        private String projectName;
        private Icon projectIcon;
        private ClasspathInfo cpInfo;
        private ClassIndexImpl index;
        private FileObject cachedRoot;

        public CacheItem(@NullAllowed URL root, @NullAllowed String cpType, @NullAllowed DataCacheCallback callBack) throws URISyntaxException {
            this.cpType = cpType;
            this.isBinary = "classpath/boot".equals(cpType) || "classpath/compile".equals(cpType);
            this.rootURI = root == null ? null : root.toURI();
            this.callBack = callBack;
        }

        public int hashCode() {
            return this.rootURI == null ? 0 : this.rootURI.hashCode();
        }

        public boolean equals(Object other) {
            if (other == this) {
                return true;
            }
            if (other instanceof CacheItem) {
                CacheItem otherItem = (CacheItem)other;
                return this.rootURI == null ? otherItem.rootURI == null : this.rootURI.equals(otherItem.rootURI);
            }
            return false;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public FileObject getRoot() {
            CacheItem cacheItem = this;
            synchronized (cacheItem) {
                if (this.cachedRoot != null) {
                    return this.cachedRoot;
                }
            }
            URL root = CacheItem.toURL(this.rootURI);
            FileObject _tmp = root == null ? null : URLMapper.findFileObject((URL)root);
            CacheItem cacheItem2 = this;
            synchronized (cacheItem2) {
                if (this.cachedRoot == null) {
                    this.cachedRoot = _tmp;
                }
            }
            return _tmp;
        }

        public boolean isBinary() {
            return this.isBinary;
        }

        public synchronized String getProjectName() {
            if (!this.isBinary && this.projectName == null) {
                this.initProjectInfo();
            }
            return this.projectName;
        }

        public synchronized Icon getProjectIcon() {
            if (!this.isBinary && this.projectIcon == null) {
                this.initProjectInfo();
            }
            return this.projectIcon;
        }

        public ClasspathInfo getClasspathInfo() {
            if (this.cpInfo == null) {
                ClassPath cp = ClassPathSupport.createClassPath((URL[])new URL[]{CacheItem.toURL(this.rootURI)});
                this.cpInfo = this.isBinary ? ("classpath/boot".equals(this.cpType) ? ClasspathInfo.create((ClassPath)cp, (ClassPath)ClassPath.EMPTY, (ClassPath)ClassPath.EMPTY) : ClasspathInfo.create((ClassPath)ClassPath.EMPTY, (ClassPath)cp, (ClassPath)ClassPath.EMPTY)) : ClasspathInfo.create((ClassPath)ClassPath.EMPTY, (ClassPath)ClassPath.EMPTY, (ClassPath)cp);
            }
            return this.cpInfo;
        }

        private boolean initIndex() {
            if (this.index == null) {
                URL root = CacheItem.toURL(this.rootURI);
                ClassIndexImpl classIndexImpl = this.index = root == null ? null : ClassIndexManager.getDefault().getUsagesQuery(root, true);
                if (this.index == null) {
                    return false;
                }
                this.index.addClassIndexImplListener((ClassIndexImplListener)this);
            }
            return true;
        }

        public boolean collectDeclaredTypes(@NullAllowed Pattern packageName, @NonNull String typeName, @NonNull ClassIndex.NameKind kind, @NonNull Collection<? super JavaTypeDescription> collector) throws IOException, InterruptedException {
            ClassIndex.SearchScope baseSearchScope;
            ClassIndex.SearchScope searchScope;
            if (!this.initIndex()) {
                return false;
            }
            ClassIndex.SearchScope searchScope2 = baseSearchScope = this.isBinary ? ClassIndex.SearchScope.DEPENDENCIES : ClassIndex.SearchScope.SOURCE;
            if (packageName != null) {
                HashSet allPackages = new HashSet();
                this.index.getPackageNames("", false, allPackages);
                Set<? extends String> packages = this.filterPackages(packageName, allPackages);
                searchScope = ClassIndex.createPackageSearchScope((ClassIndex.SearchScopeType)baseSearchScope, (String[])packages.toArray(new String[packages.size()]));
                kind = JavaTypeProvider.translateSearchType(typeName, kind);
            } else {
                searchScope = baseSearchScope;
            }
            try {
                this.index.getDeclaredTypes(typeName, kind, Collections.unmodifiableSet(Collections.singleton(searchScope)), (Convertor)new JavaTypeDescriptionConvertor(this), collector);
            }
            catch (Index.IndexClosedException ice) {
                // empty catch block
            }
            return true;
        }

        public void typesAdded(@NonNull ClassIndexImplEvent event) {
            if (this.callBack != null) {
                this.callBack.handleDataCacheChange(this);
            }
        }

        public void typesRemoved(@NonNull ClassIndexImplEvent event) {
            if (this.callBack != null) {
                this.callBack.handleDataCacheChange(this);
            }
        }

        public void typesChanged(@NonNull ClassIndexImplEvent event) {
            if (this.callBack != null) {
                this.callBack.handleDataCacheChange(this);
            }
        }

        @CheckForNull
        URI getRootURI() {
            return this.rootURI;
        }

        @CheckForNull
        ClassIndexImpl getClassIndex() {
            return this.index;
        }

        private void initProjectInfo() {
            Project p = FileOwnerQuery.getOwner((URI)this.rootURI);
            if (p != null) {
                ProjectInformation pi = (ProjectInformation)p.getLookup().lookup(ProjectInformation.class);
                this.projectName = pi == null ? p.getProjectDirectory().getNameExt() : pi.getDisplayName();
                this.projectIcon = pi == null ? null : pi.getIcon();
            }
        }

        @NonNull
        private Set<? extends String> filterPackages(@NonNull Pattern packageName, @NonNull Set<? extends String> basePackages) {
            HashSet<String> result = new HashSet<String>();
            for (String pkg : basePackages) {
                if (!packageName.matcher(pkg).matches()) continue;
                result.add(pkg);
            }
            return result;
        }

        private void dispose() {
            this.callBack = null;
            if (this.index != null) {
                this.index.removeClassIndexImplListener((ClassIndexImplListener)this);
            }
        }

        @CheckForNull
        private static URL toURL(@NullAllowed URI uri) {
            try {
                return uri == null ? null : uri.toURL();
            }
            catch (MalformedURLException ex) {
                LOGGER.log(Level.FINE, "Cannot convert URI to URL", ex);
                return null;
            }
        }

        private static class JavaTypeDescriptionConvertor
        implements Convertor<Document, JavaTypeDescription> {
            private static final Pattern ANONYMOUS = Pattern.compile(".*\\$\\d+(C|I|E|A|\\$.+)");
            private final CacheItem ci;
            private final Convertor<Document, ElementHandle<TypeElement>> delegate;

            JavaTypeDescriptionConvertor(@NonNull CacheItem ci) {
                this.ci = ci;
                this.delegate = DocumentUtil.elementHandleConvertor();
            }

            public JavaTypeDescription convert(Document p) {
                String binName = DocumentUtil.getSimpleBinaryName((Document)p);
                if (binName == null || ANONYMOUS.matcher(binName).matches()) {
                    return null;
                }
                ElementHandle eh = (ElementHandle)this.delegate.convert((Object)p);
                return eh == null ? null : new JavaTypeDescription(this.ci, eh);
            }
        }

        static interface DataCacheCallback {
            public void handleDataCacheChange(@NonNull CacheItem var1);
        }

    }

}

