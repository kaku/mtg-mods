/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.GlobalPathRegistry
 *  org.netbeans.api.java.classpath.GlobalPathRegistryEvent
 *  org.netbeans.api.java.classpath.GlobalPathRegistryListener
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ProjectInformation
 *  org.netbeans.api.project.ProjectUtils
 *  org.netbeans.api.project.ui.OpenProjects
 *  org.netbeans.modules.java.source.indexing.JavaIndex
 *  org.netbeans.modules.java.source.usages.ClassIndexManager
 *  org.netbeans.modules.java.source.usages.ClassIndexManagerEvent
 *  org.netbeans.modules.java.source.usages.ClassIndexManagerListener
 *  org.netbeans.modules.parsing.spi.indexing.PathRecognizer
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.java.source.ui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.classpath.GlobalPathRegistry;
import org.netbeans.api.java.classpath.GlobalPathRegistryEvent;
import org.netbeans.api.java.classpath.GlobalPathRegistryListener;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.ui.OpenProjects;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.usages.ClassIndexManager;
import org.netbeans.modules.java.source.usages.ClassIndexManagerEvent;
import org.netbeans.modules.java.source.usages.ClassIndexManagerListener;
import org.netbeans.modules.parsing.spi.indexing.PathRecognizer;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;

class OpenProjectFastIndex
implements ClassIndexManagerListener {
    private static final Logger LOG = Logger.getLogger(OpenProjectFastIndex.class.getName());
    private static final Strategy STRATEGY;
    private static final OpenProjectFastIndex DEFAULT;
    private static final RequestProcessor RP;
    private final Lookup.Result<PathRecognizer> recognizers = Lookup.getDefault().lookupResult(PathRecognizer.class);
    private volatile Set<String> sourcePathIds;
    private ProjectOpenWatcher watcher;
    private final GlobalPathRegistry globalRegistry = GlobalPathRegistry.getDefault();
    private final Map<FileObject, NameIndex> indexedRoots = new HashMap<FileObject, NameIndex>();
    private Reference<Set<FileObject>> removedRoots;
    private final Map<Project, PI> projectInfoCache = new HashMap<Project, PI>();
    private int watchCount;
    private final LookupListener weakLookupL;
    private final GlobalPathRegistryListener weakGlobalL;
    private final LookupListener lookupL;
    private final GlobalPathRegistryListener globalL;

    public static OpenProjectFastIndex getDefault() {
        return DEFAULT;
    }

    OpenProjectFastIndex() {
        this(false);
        this.recognizers.addLookupListener(this.weakLookupL);
        this.globalRegistry.addGlobalPathRegistryListener(this.weakGlobalL);
        ClassIndexManager.getDefault().addClassIndexManagerListener((ClassIndexManagerListener)WeakListeners.create(ClassIndexManagerListener.class, (EventListener)((Object)this), (Object)ClassIndexManager.getDefault()));
    }

    OpenProjectFastIndex(boolean register) {
        this.updateSourceIds();
        this.lookupL = new LookupListener(){

            public void resultChanged(LookupEvent ev) {
                if (ev.getSource() == OpenProjectFastIndex.this.recognizers) {
                    OpenProjectFastIndex.this.updateSourceIds();
                }
            }
        };
        this.weakLookupL = (LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this.lookupL, this.recognizers);
        this.globalL = new GlobalPathRegistryListener(){

            public void pathsAdded(GlobalPathRegistryEvent event) {
                if (STRATEGY.isEnabled()) {
                    Future projects = OpenProjects.getDefault().openProjects();
                    if (!OpenProjectFastIndex.this.sourcePathIds.contains(event.getId())) {
                        LOG.log(Level.FINE, "Non-source paths added: {0}", event.getId());
                        return;
                    }
                    if (projects.isDone()) {
                        LOG.log(Level.FINE, "Paths added, no project open in progress: {0}", event.getChangedPaths());
                        return;
                    }
                    OpenProjectFastIndex.this.getWatcher(projects).addChangedPaths(event.getChangedPaths());
                }
            }

            public void pathsRemoved(GlobalPathRegistryEvent event) {
                if (STRATEGY.isEnabled()) {
                    if (!OpenProjectFastIndex.this.sourcePathIds.contains(event.getId())) {
                        LOG.log(Level.FINE, "Non-source removed: {0}", event.getId());
                        return;
                    }
                    Set roots = OpenProjectFastIndex.getRoots(event.getChangedPaths());
                    LOG.log(Level.FINE, "Paths removed: {0}", roots);
                    OpenProjectFastIndex.this.removeRoots(roots);
                }
            }
        };
        this.weakGlobalL = (GlobalPathRegistryListener)WeakListeners.create(GlobalPathRegistryListener.class, (EventListener)this.globalL, (Object)this.globalRegistry);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private synchronized void removeRoots(Collection<FileObject> roots) {
        Set<FileObject> c;
        Set<FileObject> set = c = this.removedRoots == null ? null : this.removedRoots.get();
        if (c != null) {
            c.addAll(roots);
        }
        LOG.log(Level.FINE, "Removing roots: {0}", roots);
        this.indexedRoots.keySet().removeAll(roots);
        ArrayList<Project> retainProjects = new ArrayList<Project>(this.indexedRoots.size());
        for (NameIndex ni : this.indexedRoots.values()) {
            retainProjects.add(ni.getProject());
        }
        Map<Project, PI> i$ = this.projectInfoCache;
        synchronized (i$) {
            Set<Project> ks = this.projectInfoCache.keySet();
            ks.retainAll(retainProjects);
            LOG.log(Level.FINEST, "Retained project caches: {0}", ks);
        }
    }

    public void classIndexAdded(ClassIndexManagerEvent event) {
        Collection<FileObject> c = this.getFileRoots(event.getRoots());
        LOG.log(Level.FINE, "Index updated, removing {0} ", c);
        this.removeRoots(c);
    }

    public void classIndexRemoved(ClassIndexManagerEvent event) {
        Collection<FileObject> c = this.getFileRoots(event.getRoots());
        LOG.log(Level.FINE, "Roots removed from ClassIndexes, removing {0} ", c);
        this.removeRoots(c);
    }

    private Collection<FileObject> getFileRoots(Iterable<? extends URL> roots) {
        ArrayList<FileObject> c = new ArrayList<FileObject>(5);
        for (URL rootURL : roots) {
            FileObject fo = URLMapper.findFileObject((URL)rootURL);
            if (fo == null) continue;
            c.add(fo);
        }
        return c;
    }

    private void updateSourceIds() {
        HashSet<String> ids = new HashSet<String>();
        for (PathRecognizer r : this.recognizers.allInstances()) {
            Set rids = r.getSourcePathIds();
            if (rids == null) continue;
            ids.addAll(rids);
        }
        this.sourcePathIds = ids;
    }

    private synchronized ProjectOpenWatcher getWatcher(Future f) {
        Set<FileObject> removed;
        if (this.watcher != null) {
            return this.watcher;
        }
        Set<FileObject> set = removed = this.removedRoots == null ? null : this.removedRoots.get();
        if (removed == null) {
            removed = new HashSet<FileObject>();
            this.removedRoots = new WeakReference<HashSet<FileObject>>((HashSet<FileObject>)removed);
        }
        this.watcher = new ProjectOpenWatcher(f, removed);
        ++this.watchCount;
        LOG.fine("Starting project open watcher");
        RP.post((Runnable)this.watcher);
        return this.watcher;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private synchronized void releaseWatcher(ProjectOpenWatcher w) {
        if (--this.watchCount == 0) {
            LOG.fine("Last watcher finished, flushing everything");
            this.removedRoots = null;
            Map<Project, PI> map = this.projectInfoCache;
            synchronized (map) {
                this.projectInfoCache.clear();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void updateNameIndexes(Project project, Collection<FileObject> roots) {
        IndexBuilder b = new IndexBuilder(project, roots, (Collection)this.removedRoots.get());
        Map<FileObject, NameIndex> nameIndexes = b.build();
        Collection c = this.removedRoots.get();
        assert (c != null);
        LOG.log(Level.FINE, "Adding roots: {0}", roots);
        OpenProjectFastIndex openProjectFastIndex = this;
        synchronized (openProjectFastIndex) {
            nameIndexes.keySet().removeAll(c);
            this.indexedRoots.putAll(nameIndexes);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Map<FileObject, NameIndex> copyIndexes() {
        OpenProjectFastIndex openProjectFastIndex = this;
        synchronized (openProjectFastIndex) {
            return new HashMap<FileObject, NameIndex>(this.indexedRoots);
        }
    }

    private static Set<FileObject> getRoots(Set<ClassPath> paths) {
        HashSet<FileObject> sroots = new HashSet<FileObject>();
        for (ClassPath cp : paths) {
            sroots.addAll(Arrays.asList(cp.getRoots()));
        }
        return sroots;
    }

    public String getProjectName(Project p) {
        if (p == null) {
            return null;
        }
        return this.getCacheInfo((Project)p).projectName;
    }

    public Icon getProjectIcon(Project p) {
        if (p == null) {
            return null;
        }
        return this.getCacheInfo((Project)p).projectIcon;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private PI getCacheInfo(Project p) {
        PI pi;
        Map<Project, PI> map = this.projectInfoCache;
        synchronized (map) {
            pi = this.projectInfoCache.get((Object)p);
            if (pi != null) {
                return pi;
            }
        }
        ProjectInformation info = ProjectUtils.getInformation((Project)p);
        pi = new PI();
        pi.projectName = info.getDisplayName();
        pi.projectIcon = info.getIcon();
        Map<Project, PI> map2 = this.projectInfoCache;
        synchronized (map2) {
            this.projectInfoCache.put(p, pi);
        }
        return pi;
    }

    static {
        Strategy s = Strategy.NBFS;
        String propValue = System.getProperty("OpenProjectFastIndex.impl");
        if (propValue != null) {
            try {
                s = Strategy.valueOf(propValue);
            }
            catch (IllegalArgumentException iae) {
                LOG.log(Level.WARNING, "Wrong impl: {0}", propValue);
            }
        }
        STRATEGY = s;
        DEFAULT = new OpenProjectFastIndex();
        RP = new RequestProcessor("Project prescan initiator", 1);
    }

    private static enum Strategy {
        NONE{

            @Override
            boolean isEnabled() {
                return false;
            }
        }
        ,
        NBFS{

            @Override
            boolean isEnabled() {
                return true;
            }
        };
        

        private Strategy() {
        }

        abstract boolean isEnabled();

    }

    static class IndexBuilder {
        private Collection<FileObject> rootsToScan;
        private StringBuilder filenames;
        private Collection<FileObject> removedRoots;
        private Project project;
        private Map<FileObject, NameIndex> nameIndexes = new HashMap<FileObject, NameIndex>();
        private ArrayList dirPositions;
        private int charPtr;

        public IndexBuilder(Project project, Collection<FileObject> rootsToScan, Collection<FileObject> removedRoots) {
            this.rootsToScan = rootsToScan;
            this.removedRoots = removedRoots;
            this.project = project;
        }

        private void reset() {
            this.filenames = new StringBuilder();
            this.dirPositions = new ArrayList();
            this.charPtr = 0;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Map<FileObject, NameIndex> build() {
            for (FileObject root : this.rootsToScan) {
                Collection<FileObject> collection = this.removedRoots;
                synchronized (collection) {
                    if (this.removedRoots.contains((Object)root)) {
                        continue;
                    }
                }
                this.reset();
                this.scanDir(root, -1);
                NameIndex ni = new NameIndex(this.project, root, this.filenames.toString(), this.dirPositions);
                this.nameIndexes.put(root, ni);
            }
            return this.nameIndexes;
        }

        private void scanDir(FileObject d, int parentIndex) {
            FileObject f;
            int myIndex = this.dirPositions.size();
            this.dirPositions.add(new Object[]{d.getNameExt(), this.charPtr, parentIndex});
            boolean someFile = false;
            Enumeration en = d.getData(false);
            while (en.hasMoreElements()) {
                f = (FileObject)en.nextElement();
                String fn = f.getName();
                if (!Utilities.isJavaIdentifier((String)fn) || !"text/x-java".equals(f.getMIMEType())) continue;
                someFile = true;
                this.filenames.append(fn).append("\n");
                this.charPtr += fn.length() + 1;
            }
            if (!someFile) {
                this.filenames.append("\n");
                ++this.charPtr;
            }
            en = d.getFolders(false);
            while (en.hasMoreElements()) {
                f = (FileObject)en.nextElement();
                if (!Utilities.isJavaIdentifier((String)f.getNameExt())) continue;
                this.scanDir(f, myIndex);
            }
        }
    }

    static class NameIndex {
        private final Reference<FileObject> root;
        private final String fileNames;
        private final int[] dirStartPositions;
        private final String[] dirNames;
        private final int size;
        private final Project project;

        NameIndex(Project p, FileObject root, String files, ArrayList indices) {
            this.project = p;
            this.root = new WeakReference<FileObject>(root);
            this.size = indices.size();
            this.fileNames = files;
            this.dirStartPositions = new int[this.size * 2];
            this.dirNames = new String[this.size];
            for (int i = this.size - 1; i >= 0; --i) {
                Object[] data = (Object[])indices.get(i);
                this.dirStartPositions[i] = (Integer)data[1];
                this.dirStartPositions[i + this.size] = (Integer)data[2];
                this.dirNames[i] = (String)data[0];
            }
        }

        public CharSequence files() {
            return this.fileNames;
        }

        public CharSequence getFilename(int matchFrom, int matchTo) {
            int startLine = this.fileNames.lastIndexOf(10, matchFrom);
            while (this.fileNames.charAt(++startLine) == ' ') {
            }
            int endLine = this.fileNames.indexOf(10, matchTo);
            return this.fileNames.subSequence(startLine, endLine);
        }

        public String findPath(int namePos) {
            int atIndex = Arrays.binarySearch(this.dirStartPositions, 0, this.size, namePos);
            if (atIndex < 0) {
                atIndex = - atIndex + 1 - 1;
            }
            return this.appendParents(new StringBuilder(), atIndex).toString();
        }

        private StringBuilder appendParents(StringBuilder sb, int dirIndex) {
            int parentIndex = this.dirStartPositions[this.size + dirIndex];
            if (parentIndex > 0) {
                this.appendParents(sb, parentIndex).append(".");
            }
            sb.append(this.dirNames[dirIndex]);
            return sb;
        }

        public FileObject getRoot() {
            return this.root.get();
        }

        public Project getProject() {
            return this.project;
        }
    }

    private static final class PI {
        Icon projectIcon;
        String projectName;

        private PI() {
        }
    }

    class ProjectOpenWatcher
    implements Runnable,
    PropertyChangeListener {
        private int state;
        private final Future waitFor;
        private volatile Project mainProject;
        private Set<FileObject> rootsToIndex;
        private Collection<Project> newProjects;
        private Collection<FileObject> removedRoots;

        public ProjectOpenWatcher(Future waitFor, Collection<FileObject> removedRoots) {
            this.rootsToIndex = new HashSet<FileObject>();
            this.waitFor = waitFor;
            this.removedRoots = removedRoots;
            OpenProjects.getDefault().addPropertyChangeListener((PropertyChangeListener)this);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            int state;
            Object object = this;
            synchronized (object) {
                state = this.state++;
            }
            if (state == 0) {
                try {
                    this.waitFor.get();
                    object = OpenProjectFastIndex.this;
                    synchronized (object) {
                        OpenProjectFastIndex.this.watcher = null;
                        ProjectOpenWatcher projectOpenWatcher = this;
                        synchronized (projectOpenWatcher) {
                            ++this.state;
                        }
                    }
                    RP.schedule((Runnable)this, 200, TimeUnit.MILLISECONDS);
                }
                catch (InterruptedException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
                catch (ExecutionException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            } else {
                OpenProjects.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
                try {
                    Project selected = this.selectProject(this.newProjects);
                    this.processProject(selected);
                }
                finally {
                    OpenProjectFastIndex.this.releaseWatcher(this);
                }
            }
        }

        void processProject(Project selected) {
            if (selected == null) {
                return;
            }
            Iterator<FileObject> it = this.rootsToIndex.iterator();
            while (it.hasNext()) {
                FileObject f = it.next();
                if (JavaIndex.isIndexed((URL)f.toURL())) {
                    it.remove();
                    continue;
                }
                Project p = FileOwnerQuery.getOwner((FileObject)f);
                if (p == selected) continue;
                it.remove();
            }
            if (this.rootsToIndex.isEmpty()) {
                LOG.log(Level.FINE, "Nothing to preindex");
                return;
            }
            OpenProjectFastIndex.this.updateNameIndexes(selected, this.rootsToIndex);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public boolean addChangedPaths(Set<ClassPath> paths) {
            Set sroots = OpenProjectFastIndex.getRoots(paths);
            ProjectOpenWatcher projectOpenWatcher = this;
            synchronized (projectOpenWatcher) {
                if (this.state > 1) {
                    return false;
                }
                LOG.log(Level.FINE, "Added source paths: {0}", paths);
                this.rootsToIndex.addAll(sroots);
            }
            return true;
        }

        private void matchOpenProjects(Project[] old, Project[] current) {
            HashMap<FileObject, Project> projects = new HashMap<FileObject, Project>();
            for (Project p2 : current) {
                projects.put(p2.getProjectDirectory(), p2);
            }
            for (Project p2 : old) {
                projects.remove((Object)p2.getProjectDirectory());
            }
            this.newProjects = projects.values();
        }

        private Project selectProject(Collection<Project> newlyOpenedProjects) {
            if (newlyOpenedProjects.size() == 1) {
                return newlyOpenedProjects.iterator().next();
            }
            if (newlyOpenedProjects.contains((Object)this.mainProject)) {
                return this.mainProject;
            }
            return null;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("openProjects".equals(evt.getPropertyName())) {
                this.matchOpenProjects((Project[])evt.getOldValue(), (Project[])evt.getNewValue());
            } else if ("MainProject".equals(evt.getPropertyName())) {
                this.mainProject = (Project)evt.getNewValue();
            }
        }
    }

}

