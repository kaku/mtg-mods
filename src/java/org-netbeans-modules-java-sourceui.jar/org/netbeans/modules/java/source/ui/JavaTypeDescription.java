/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.platform.JavaPlatform
 *  org.netbeans.api.java.platform.JavaPlatformManager
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.modules.java.source.parsing.FileObjects
 *  org.netbeans.modules.java.source.usages.ClassIndexImpl
 *  org.netbeans.modules.java.ui.Icons
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.netbeans.spi.jumpto.type.TypeDescriptor
 *  org.openide.awt.StatusDisplayer
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.source.ui;

import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.swing.Icon;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.api.java.platform.JavaPlatformManager;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.ui.ElementOpen;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.ui.JavaTypeProvider;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;
import org.netbeans.modules.java.ui.Icons;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.netbeans.spi.jumpto.type.TypeDescriptor;
import org.openide.awt.StatusDisplayer;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class JavaTypeDescription
extends TypeDescriptor {
    private static final Logger LOG = Logger.getLogger(JavaTypeDescription.class.getName());
    private Icon icon;
    private final JavaTypeProvider.CacheItem cacheItem;
    private final ElementHandle<TypeElement> handle;
    private String simpleName;
    private String outerName;
    private String packageName;
    private volatile String cachedPath;

    JavaTypeDescription(@NonNull JavaTypeProvider.CacheItem cacheItem, @NonNull ElementHandle<TypeElement> handle) {
        this.cacheItem = cacheItem;
        this.handle = handle;
        this.init();
    }

    public void open() {
        ClasspathInfo ci;
        FileObject root = this.cacheItem.getRoot();
        if (root == null) {
            String message = NbBundle.getMessage(JavaTypeDescription.class, (String)"LBL_JavaTypeDescription_nosource", (Object)this.handle.getQualifiedName());
            StatusDisplayer.getDefault().setStatusText(message);
            Toolkit.getDefaultToolkit().beep();
            return;
        }
        ClassPath bootPath = ClassPath.getClassPath((FileObject)root, (String)"classpath/boot");
        if (bootPath == null) {
            bootPath = JavaPlatformManager.getDefault().getDefaultPlatform().getBootstrapLibraries();
        }
        if (this.cacheItem.isBinary()) {
            ClassPath compilePath = ClassPathSupport.createClassPath((FileObject[])new FileObject[]{root});
            ci = ClasspathInfo.create((ClassPath)bootPath, (ClassPath)compilePath, (ClassPath)ClassPath.EMPTY);
        } else {
            ClassPath sourcePath = ClassPathSupport.createClassPath((FileObject[])new FileObject[]{root});
            ci = ClasspathInfo.create((ClassPath)bootPath, (ClassPath)ClassPath.EMPTY, (ClassPath)sourcePath);
        }
        if (this.cacheItem.isBinary()) {
            ElementHandle<TypeElement> eh = this.handle;
            if (!ElementOpen.open(ci, eh)) {
                String message = NbBundle.getMessage(JavaTypeDescription.class, (String)"LBL_JavaTypeDescription_nosource", (Object)eh.getQualifiedName());
                StatusDisplayer.getDefault().setStatusText(message);
                Toolkit.getDefaultToolkit().beep();
            }
        } else {
            FileObject file = SourceUtils.getFile(this.handle, (ClasspathInfo)ci);
            boolean opened = false;
            if (file != null) {
                opened = ElementOpen.open(file, this.handle);
            }
            if (!opened) {
                StringBuilder name = new StringBuilder();
                if (this.packageName != null) {
                    name.append(this.packageName);
                    name.append('.');
                }
                if (this.outerName != null) {
                    name.append(this.outerName);
                } else {
                    name.append(this.simpleName);
                }
                String message = NbBundle.getMessage(JavaTypeDescription.class, (String)"LBL_JavaTypeDescription_nosource", (Object)name.toString());
                StatusDisplayer.getDefault().setStatusText(message);
                Toolkit.getDefaultToolkit().beep();
            }
        }
    }

    public String getSimpleName() {
        return this.simpleName;
    }

    public String getOuterName() {
        return this.outerName;
    }

    public FileObject getFileObject() {
        FileObject root = this.cacheItem.getRoot();
        String relativePath = JavaTypeDescription.getRelativePath(this.handle.getBinaryName(), this.cacheItem.getClassIndex(), this.cacheItem.isBinary(), this.cacheItem.getRootURI());
        return root == null ? null : root.getFileObject(relativePath);
    }

    public String getFileDisplayPath() {
        String path = this.cachedPath;
        if (path == null) {
            URI uri = this.cacheItem.getRootURI();
            assert (uri != null);
            try {
                File rootFile = Utilities.toFile((URI)uri);
                String relativePath = JavaTypeDescription.getRelativePath(this.handle.getBinaryName(), this.cacheItem.getClassIndex(), this.cacheItem.isBinary(), uri);
                path = new File(rootFile, relativePath).getAbsolutePath();
            }
            catch (IllegalArgumentException e) {
                path = FileUtil.getFileDisplayName((FileObject)this.cacheItem.getRoot());
            }
            this.cachedPath = path;
        }
        return path;
    }

    public String getTypeName() {
        StringBuilder sb = new StringBuilder(this.simpleName);
        if (this.outerName != null) {
            sb.append(" in ").append(this.outerName);
        }
        return sb.toString();
    }

    public String getContextName() {
        StringBuilder sb = new StringBuilder();
        sb.append(" (").append(this.packageName == null ? "Default Package" : this.packageName).append(")");
        return sb.toString();
    }

    public String getProjectName() {
        String projectName = this.cacheItem.getProjectName();
        return projectName == null ? "" : projectName;
    }

    public Icon getProjectIcon() {
        return this.cacheItem.getProjectIcon();
    }

    public synchronized Icon getIcon() {
        return this.icon;
    }

    public int getOffset() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(this.simpleName);
        if (this.outerName != null) {
            sb.append(" in ").append(this.outerName);
        }
        sb.append(" (").append(this.packageName == null ? "Default Package" : this.packageName).append(")");
        if (this.cacheItem.getProjectName() != null) {
            sb.append(" [").append(this.cacheItem.getProjectName()).append("]");
        }
        return sb.toString();
    }

    public int hashCode() {
        int hc = 17;
        hc = hc * 31 + this.handle.hashCode();
        hc = hc * 31 + this.handle.hashCode();
        return hc;
    }

    public boolean equals(@NullAllowed Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof JavaTypeDescription)) {
            return false;
        }
        JavaTypeDescription otherJTD = (JavaTypeDescription)((Object)other);
        return this.handle.equals(otherJTD.handle) && this.cacheItem.equals(otherJTD.cacheItem);
    }

    public ElementHandle<TypeElement> getHandle() {
        return this.handle;
    }

    private void init() {
        String typeName = this.handle.getBinaryName();
        int lastDot = typeName.lastIndexOf(46);
        int lastDollar = typeName.lastIndexOf(36);
        if (lastDot == -1) {
            if (lastDollar == -1) {
                this.simpleName = typeName;
            } else {
                this.simpleName = typeName.substring(lastDollar + 1);
                this.outerName = typeName.substring(0, lastDollar).replace('$', '.');
            }
        } else {
            this.packageName = typeName.substring(0, lastDot);
            if (lastDollar < lastDot) {
                this.simpleName = typeName.substring(lastDot + 1).replace('$', '.');
            } else {
                this.simpleName = typeName.substring(lastDollar + 1);
                this.outerName = typeName.substring(lastDot + 1, lastDollar).replace('$', '.');
            }
        }
        this.icon = Icons.getElementIcon((ElementKind)this.handle.getKind(), (Collection)null);
    }

    private static String getRelativePath(@NonNull String binaryName, @NullAllowed ClassIndexImpl ci, boolean isBinary, @NullAllowed URI root) {
        String relativePath = null;
        if (ci == null) {
            LOG.log(Level.WARNING, "No ClassIndex for {0} in {1}", new Object[]{binaryName, root});
        } else {
            try {
                relativePath = ci.getSourceName(binaryName);
            }
            catch (IOException | InterruptedException ex) {
                LOG.log(Level.WARNING, "Broken ClassIndex for {0} in {1}", new Object[]{binaryName, root});
            }
        }
        if (relativePath == null) {
            relativePath = binaryName;
            int lastDot = relativePath.lastIndexOf(46);
            int csIndex = relativePath.indexOf(36, lastDot);
            if (csIndex > 0 && csIndex < relativePath.length() - 1) {
                relativePath = binaryName.substring(0, csIndex);
            }
            Object[] arrobject = new Object[2];
            arrobject[0] = FileObjects.convertPackage2Folder((String)relativePath, (char)File.separatorChar);
            arrobject[1] = isBinary ? "class" : "java";
            relativePath = String.format("%s.%s", arrobject);
        }
        return relativePath;
    }
}

