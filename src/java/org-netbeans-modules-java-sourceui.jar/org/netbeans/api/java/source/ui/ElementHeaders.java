/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.TreeUtilities
 *  org.netbeans.modules.java.ui.ElementHeaderFormater
 */
package org.netbeans.api.java.source.ui;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.util.Set;
import javax.lang.model.element.Element;
import javax.lang.model.element.Name;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.modules.java.ui.ElementHeaderFormater;

public final class ElementHeaders {
    public static final String ANNOTATIONS = "%annotations%";
    public static final String NAME = "%name%";
    public static final String TYPE = "%type%";
    public static final String THROWS = "%throws%";
    public static final String IMPLEMENTS = "%implements%";
    public static final String EXTENDS = "%extends%";
    public static final String TYPEPARAMETERS = "%typeparameters%";
    public static final String FLAGS = "%flags%";
    public static final String PARAMETERS = "%parameters%";

    private ElementHeaders() {
    }

    public static String getHeader(TreePath treePath, CompilationInfo info, String formatString) {
        assert (info != null);
        assert (treePath != null);
        Element element = info.getTrees().getElement(treePath);
        if (element != null) {
            return ElementHeaders.getHeader(element, info, formatString);
        }
        return null;
    }

    public static String getHeader(Element element, CompilationInfo info, String formatString) {
        assert (element != null);
        assert (info != null);
        assert (formatString != null);
        TreePath tp = info.getTrees().getPath(element);
        if (tp != null) {
            Tree tree = tp.getLeaf();
            if (tree.getKind() == Tree.Kind.METHOD) {
                while (tp != null && !TreeUtilities.CLASS_TREE_KINDS.contains((Object)tp.getLeaf().getKind())) {
                    tp = tp.getParentPath();
                }
                Name enclosingClassName = tp != null ? ((ClassTree)tp.getLeaf()).getSimpleName() : null;
                return ElementHeaderFormater.getMethodHeader((MethodTree)((MethodTree)tree), (Name)enclosingClassName, (CompilationInfo)info, (String)formatString);
            }
            if (TreeUtilities.CLASS_TREE_KINDS.contains((Object)tree.getKind())) {
                return ElementHeaderFormater.getClassHeader((ClassTree)((ClassTree)tree), (CompilationInfo)info, (String)formatString);
            }
            if (tree.getKind() == Tree.Kind.VARIABLE) {
                return ElementHeaderFormater.getVariableHeader((VariableTree)((VariableTree)tree), (CompilationInfo)info, (String)formatString);
            }
        }
        return formatString.replaceAll("%name%", element.getSimpleName().toString()).replaceAll("%[a-z]*%", "");
    }

    public static int getDistance(String s, String t) {
        int n = s.length();
        int m = t.length();
        if (n == 0) {
            return m;
        }
        if (m == 0) {
            return n;
        }
        int[][] d = new int[n + 1][m + 1];
        int i = 0;
        while (i <= n) {
            d[i][0] = i++;
        }
        int j = 0;
        while (j <= m) {
            d[0][j] = j++;
        }
        for (i = 1; i <= n; ++i) {
            char s_i = s.charAt(i - 1);
            for (j = 1; j <= m; ++j) {
                char t_j = t.charAt(j - 1);
                int cost = s_i == t_j ? 0 : 1;
                d[i][j] = ElementHeaders.min(d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + cost);
            }
        }
        return d[n][m];
    }

    private static int min(int a, int b, int c) {
        int mi = a;
        if (b < mi) {
            mi = b;
        }
        if (c < mi) {
            mi = c;
        }
        return mi;
    }
}

