/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.source.ClassIndex
 *  org.netbeans.api.java.source.ClassIndex$NameKind
 *  org.netbeans.api.java.source.ClassIndex$SearchScope
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.jumpto.type.TypeBrowser
 *  org.netbeans.api.jumpto.type.TypeBrowser$Filter
 *  org.netbeans.spi.jumpto.type.TypeDescriptor
 *  org.netbeans.spi.jumpto.type.TypeProvider
 *  org.openide.util.NbBundle
 */
package org.netbeans.api.java.source.ui;

import java.util.Set;
import javax.lang.model.element.TypeElement;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.source.ClassIndex;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.jumpto.type.TypeBrowser;
import org.netbeans.modules.java.source.ui.JavaTypeDescription;
import org.netbeans.modules.java.source.ui.JavaTypeProvider;
import org.netbeans.spi.jumpto.type.TypeDescriptor;
import org.netbeans.spi.jumpto.type.TypeProvider;
import org.openide.util.NbBundle;

public final class TypeElementFinder {
    @CheckForNull
    public static ElementHandle<TypeElement> find(@NullAllowed ClasspathInfo cpInfo, @NullAllowed Customizer customizer) {
        return TypeElementFinder.find(cpInfo, null, customizer);
    }

    @CheckForNull
    public static ElementHandle<TypeElement> find(@NullAllowed ClasspathInfo cpInfo, @NullAllowed String initialText, final @NullAllowed Customizer customizer) {
        TypeBrowser.Filter typeBrowserFilter = null;
        if (customizer != null) {
            typeBrowserFilter = new TypeBrowser.Filter(){

                public boolean accept(TypeDescriptor typeDescriptor) {
                    JavaTypeDescription javaTypeDesc = TypeElementFinder.toJavaTypeDescription(typeDescriptor);
                    if (customizer != null && javaTypeDesc != null) {
                        return customizer.accept(javaTypeDesc.getHandle());
                    }
                    return true;
                }
            };
        }
        TypeProvider[] arrtypeProvider = new TypeProvider[1];
        arrtypeProvider[0] = new JavaTypeProvider(cpInfo, customizer == null ? null : customizer);
        TypeDescriptor typeDescriptor = TypeBrowser.browse((String)NbBundle.getMessage(TypeElementFinder.class, (String)"DLG_FindType"), (String)initialText, (TypeBrowser.Filter)typeBrowserFilter, (TypeProvider[])arrtypeProvider);
        JavaTypeDescription javaTypeDesc = TypeElementFinder.toJavaTypeDescription(typeDescriptor);
        return javaTypeDesc == null ? null : javaTypeDesc.getHandle();
    }

    private static JavaTypeDescription toJavaTypeDescription(TypeDescriptor typeDescriptor) {
        if (typeDescriptor instanceof JavaTypeDescription) {
            return (JavaTypeDescription)typeDescriptor;
        }
        return null;
    }

    public static interface Customizer {
        public Set<ElementHandle<TypeElement>> query(ClasspathInfo var1, String var2, ClassIndex.NameKind var3, Set<ClassIndex.SearchScope> var4);

        public boolean accept(ElementHandle<TypeElement> var1);
    }

}

