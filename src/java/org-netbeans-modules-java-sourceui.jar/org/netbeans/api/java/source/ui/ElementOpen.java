/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreePathScanner
 *  com.sun.source.util.Trees
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.editor.fold.Fold
 *  org.netbeans.api.editor.fold.FoldHierarchy
 *  org.netbeans.api.editor.fold.FoldUtilities
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.java.source.UiUtils
 *  org.netbeans.api.progress.ProgressUtils
 *  org.netbeans.modules.java.BinaryElementOpen
 *  org.netbeans.modules.java.source.JavaSourceAccessor
 *  org.openide.awt.StatusDisplayer
 *  org.openide.awt.StatusDisplayer$Message
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.Parameters
 */
package org.netbeans.api.java.source.ui;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreePathScanner;
import com.sun.source.util.Trees;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldUtilities;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.UiUtils;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.modules.java.BinaryElementOpen;
import org.netbeans.modules.java.source.JavaSourceAccessor;
import org.openide.awt.StatusDisplayer;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Parameters;

public final class ElementOpen {
    private static Logger log = Logger.getLogger(ElementOpen.class.getName());
    private static final int AWT_TIMEOUT = 1000;
    private static final int NON_AWT_TIMEOUT = 2000;

    private ElementOpen() {
    }

    public static boolean open(final ClasspathInfo cpInfo, final ElementHandle<? extends Element> el) {
        final AtomicBoolean cancel = new AtomicBoolean();
        if (SwingUtilities.isEventDispatchThread() && !JavaSourceAccessor.holdsParserLock()) {
            final boolean[] result = new boolean[1];
            ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

                @Override
                public void run() {
                    result[0] = ElementOpen.open(cpInfo, el, cancel);
                }
            }, (String)NbBundle.getMessage(ElementOpen.class, (String)"TXT_CalculatingDeclPos"), (AtomicBoolean)cancel, (boolean)false);
            return result[0];
        }
        return ElementOpen.open(cpInfo, el, cancel);
    }

    private static boolean open(ClasspathInfo cpInfo, ElementHandle<? extends Element> el, AtomicBoolean cancel) {
        Object[] openInfo;
        FileObject fo = SourceUtils.getFile(el, (ClasspathInfo)cpInfo);
        if (fo != null && fo.isFolder()) {
            fo = fo.getFileObject("package-info.java");
        }
        Object[] arrobject = openInfo = fo != null ? ElementOpen.getOpenInfo(fo, el, cancel) : null;
        if (cancel.get()) {
            return false;
        }
        if (openInfo != null) {
            assert (openInfo[0] instanceof FileObject);
            return ElementOpen.doOpen((FileObject)openInfo[0], (Integer)openInfo[1], (Integer)openInfo[2]);
        }
        BinaryElementOpen beo = (BinaryElementOpen)Lookup.getDefault().lookup(BinaryElementOpen.class);
        if (beo != null) {
            return beo.open(cpInfo, el, cancel);
        }
        return false;
    }

    public static boolean open(ClasspathInfo cpInfo, Element el) {
        return ElementOpen.open(cpInfo, ElementHandle.create((Element)el));
    }

    public static boolean open(final @NonNull FileObject toSearch, final @NonNull ElementHandle<? extends Element> toOpen) {
        final AtomicBoolean cancel = new AtomicBoolean();
        if (SwingUtilities.isEventDispatchThread() && !JavaSourceAccessor.holdsParserLock()) {
            final boolean[] result = new boolean[1];
            ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

                @Override
                public void run() {
                    result[0] = ElementOpen.open(toSearch, toOpen, cancel);
                }
            }, (String)NbBundle.getMessage(ElementOpen.class, (String)"TXT_CalculatingDeclPos"), (AtomicBoolean)cancel, (boolean)false);
            return result[0];
        }
        return ElementOpen.open(toSearch, toOpen, cancel);
    }

    private static boolean open(@NonNull FileObject toSearch, @NonNull ElementHandle<? extends Element> toOpen, @NonNull AtomicBoolean cancel) {
        Parameters.notNull((CharSequence)"toSearch", (Object)toSearch);
        Parameters.notNull((CharSequence)"toOpen", toOpen);
        Object[] openInfo = ElementOpen.getOpenInfo(toSearch, toOpen, cancel);
        if (cancel.get()) {
            return false;
        }
        if (openInfo != null) {
            assert (openInfo[0] instanceof FileObject);
            return ElementOpen.doOpen((FileObject)openInfo[0], (Integer)openInfo[1], (Integer)openInfo[2]);
        }
        BinaryElementOpen beo = (BinaryElementOpen)Lookup.getDefault().lookup(BinaryElementOpen.class);
        if (beo != null) {
            return beo.open(ClasspathInfo.create((FileObject)toSearch), toOpen, cancel);
        }
        return false;
    }

    private static Object[] getOpenInfo(FileObject fo, ElementHandle<? extends Element> handle, AtomicBoolean cancel) {
        assert (fo != null);
        try {
            int[] offset = ElementOpen.getOffset(fo, handle, cancel);
            return new Object[]{fo, offset[0], offset[1]};
        }
        catch (IOException e) {
            Exceptions.printStackTrace((Throwable)e);
            return null;
        }
    }

    private static boolean doOpen(FileObject fo, final int offsetA, final int offsetB) {
        DataObject od;
        boolean success;
        if (offsetA == -1) {
            StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(ElementOpen.class, (String)"WARN_ElementNotFound"), 1000);
        }
        if (!(success = UiUtils.open((FileObject)fo, (int)offsetA))) {
            return false;
        }
        try {
            od = DataObject.find((FileObject)fo);
        }
        catch (DataObjectNotFoundException ex) {
            return success;
        }
        final EditorCookie ec = (EditorCookie)od.getLookup().lookup(EditorCookie.class);
        if (ec != null && offsetA != -1 && offsetB != -1) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    JEditorPane pane;
                    Fold f;
                    FoldHierarchy fh;
                    JEditorPane[] panes = ec.getOpenedPanes();
                    if (offsetB >= 0 && panes != null && panes.length > 0 && (f = FoldUtilities.findNearestFold((FoldHierarchy)(fh = FoldHierarchy.get((JTextComponent)(pane = panes[0]))), (int)offsetA)) != null && f.getStartOffset() >= offsetA && f.getEndOffset() <= offsetB) {
                        fh.expand(f);
                    }
                }
            });
        }
        return success;
    }

    private static int[] getOffset(final FileObject fo, final ElementHandle<? extends Element> handle, final AtomicBoolean cancel) throws IOException {
        final int[] result = new int[]{-1, -1};
        JavaSource js = JavaSource.forFileObject((FileObject)fo);
        if (js != null) {
            Task<CompilationController> t = new Task<CompilationController>(){

                public void run(CompilationController info) throws IOException {
                    if (cancel.get()) {
                        return;
                    }
                    try {
                        info.toPhase(JavaSource.Phase.RESOLVED);
                    }
                    catch (IOException ioe) {
                        Exceptions.printStackTrace((Throwable)ioe);
                    }
                    Element el = handle.resolve((CompilationInfo)info);
                    if (el == null) {
                        if (!SourceUtils.isScanInProgress()) {
                            log.severe("Cannot resolve " + (Object)handle + ". " + (Object)info.getClasspathInfo());
                        } else {
                            Level l = Level.FINE;
                            assert ((l = Level.INFO) != null);
                            log.log(l, "Cannot resolve {0} ({1})", new Object[]{handle, info.getClasspathInfo()});
                        }
                        return;
                    }
                    if (el.getKind() == ElementKind.PACKAGE) {
                        Matcher m = Pattern.compile("(?m)^package (.+);$").matcher(fo.asText());
                        if (m.find()) {
                            result[0] = m.start();
                        }
                        return;
                    }
                    FindDeclarationVisitor v = new FindDeclarationVisitor(el, (CompilationInfo)info);
                    CompilationUnitTree cu = info.getCompilationUnit();
                    v.scan((Tree)cu, (Object)null);
                    Tree elTree = v.declTree;
                    if (elTree != null) {
                        result[0] = (int)info.getTrees().getSourcePositions().getStartPosition(cu, elTree);
                        result[1] = (int)info.getTrees().getSourcePositions().getEndPosition(cu, elTree);
                    }
                }
            };
            js.runUserActionTask((Task)t, true);
        }
        return result;
    }

    private static class FindDeclarationVisitor
    extends TreePathScanner<Void, Void> {
        private Element element;
        private Tree declTree;
        private CompilationInfo info;

        public FindDeclarationVisitor(Element element, CompilationInfo info) {
            this.element = element;
            this.info = info;
        }

        public Void visitClass(ClassTree tree, Void d) {
            this.handleDeclaration();
            TreePathScanner.super.visitClass(tree, (Object)d);
            return null;
        }

        public Void visitMethod(MethodTree tree, Void d) {
            this.handleDeclaration();
            TreePathScanner.super.visitMethod(tree, (Object)d);
            return null;
        }

        public Void visitVariable(VariableTree tree, Void d) {
            this.handleDeclaration();
            TreePathScanner.super.visitVariable(tree, (Object)d);
            return null;
        }

        public void handleDeclaration() {
            Element found = this.info.getTrees().getElement(this.getCurrentPath());
            if (this.element.equals(found)) {
                this.declTree = this.getCurrentPath().getLeaf();
            }
        }
    }

}

