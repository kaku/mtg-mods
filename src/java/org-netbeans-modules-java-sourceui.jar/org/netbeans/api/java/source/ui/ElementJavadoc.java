/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.javadoc.AnnotationDesc
 *  com.sun.javadoc.AnnotationDesc$ElementValuePair
 *  com.sun.javadoc.AnnotationTypeDoc
 *  com.sun.javadoc.AnnotationTypeElementDoc
 *  com.sun.javadoc.AnnotationValue
 *  com.sun.javadoc.ClassDoc
 *  com.sun.javadoc.Doc
 *  com.sun.javadoc.ExecutableMemberDoc
 *  com.sun.javadoc.FieldDoc
 *  com.sun.javadoc.MemberDoc
 *  com.sun.javadoc.MethodDoc
 *  com.sun.javadoc.PackageDoc
 *  com.sun.javadoc.ParamTag
 *  com.sun.javadoc.Parameter
 *  com.sun.javadoc.ParameterizedType
 *  com.sun.javadoc.ProgramElementDoc
 *  com.sun.javadoc.SeeTag
 *  com.sun.javadoc.Tag
 *  com.sun.javadoc.ThrowsTag
 *  com.sun.javadoc.Type
 *  com.sun.javadoc.TypeVariable
 *  com.sun.javadoc.WildcardType
 *  com.sun.source.tree.Tree
 *  com.sun.source.util.Trees
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.queries.JavadocForBinaryQuery
 *  org.netbeans.api.java.queries.SourceJavadocAttacher
 *  org.netbeans.api.java.queries.SourceJavadocAttacher$AttachmentListener
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.ClasspathInfo$PathKind
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.ElementUtilities
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.netbeans.modules.java.preprocessorbridge.api.JavaSourceUtil
 *  org.netbeans.modules.java.preprocessorbridge.api.JavaSourceUtil$Handle
 *  org.netbeans.modules.java.source.JavadocHelper
 *  org.netbeans.modules.java.source.JavadocHelper$RemoteJavadocException
 *  org.netbeans.modules.java.source.JavadocHelper$RemoteJavadocPolicy
 *  org.netbeans.modules.java.source.JavadocHelper$TextStream
 *  org.netbeans.modules.java.source.parsing.FileObjects
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.Pair
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.api.java.source.ui;

import com.sun.javadoc.AnnotationDesc;
import com.sun.javadoc.AnnotationTypeDoc;
import com.sun.javadoc.AnnotationTypeElementDoc;
import com.sun.javadoc.AnnotationValue;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.Doc;
import com.sun.javadoc.ExecutableMemberDoc;
import com.sun.javadoc.FieldDoc;
import com.sun.javadoc.MemberDoc;
import com.sun.javadoc.MethodDoc;
import com.sun.javadoc.PackageDoc;
import com.sun.javadoc.ParamTag;
import com.sun.javadoc.Parameter;
import com.sun.javadoc.ParameterizedType;
import com.sun.javadoc.ProgramElementDoc;
import com.sun.javadoc.SeeTag;
import com.sun.javadoc.Tag;
import com.sun.javadoc.ThrowsTag;
import com.sun.javadoc.Type;
import com.sun.javadoc.TypeVariable;
import com.sun.javadoc.WildcardType;
import com.sun.source.tree.Tree;
import com.sun.source.util.Trees;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.SwingUtilities;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.queries.JavadocForBinaryQuery;
import org.netbeans.api.java.queries.SourceJavadocAttacher;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.ui.ElementOpen;
import org.netbeans.api.java.source.ui.HTMLJavadocParser;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.modules.java.preprocessorbridge.api.JavaSourceUtil;
import org.netbeans.modules.java.source.JavadocHelper;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.Pair;
import org.openide.util.RequestProcessor;
import org.openide.xml.XMLUtil;

public class ElementJavadoc {
    private static final String ASSOCIATE_JDOC = "associate-javadoc:";
    private static final String API = "/api";
    private static final Set<String> LANGS;
    private static final RequestProcessor RP;
    private final ClasspathInfo cpInfo;
    private final ElementHandle<? extends Element> handle;
    private volatile Future<String> content;
    private final Callable<Boolean> cancel;
    private Hashtable<String, ElementHandle<? extends Element>> links = new Hashtable();
    private int linkCounter = 0;
    private volatile URL docURL = null;
    private volatile AbstractAction goToSource = null;
    private static final String PARAM_TAG = "@param";
    private static final String RETURN_TAG = "@return";
    private static final String THROWS_TAG = "@throws";
    private static final String SEE_TAG = "@see";
    private static final String SINCE_TAG = "@since";
    private static final String INHERIT_DOC_TAG = "@inheritDoc";
    private static final String LINKPLAIN_TAG = "@linkplain";
    private static final String CODE_TAG = "@code";
    private static final String LITERAL_TAG = "@literal";
    private static final String DEPRECATED_TAG = "@deprecated";
    private static final String VALUE_TAG = "@value";

    public static final ElementJavadoc create(CompilationInfo compilationInfo, Element element) {
        return ElementJavadoc.create(compilationInfo, element, null);
    }

    public static final ElementJavadoc create(CompilationInfo compilationInfo, Element element, Callable<Boolean> cancel) {
        return new ElementJavadoc(compilationInfo, element, null, cancel);
    }

    public String getText() {
        try {
            Future<String> tmp = this.content;
            return tmp != null ? tmp.get() : null;
        }
        catch (InterruptedException ex) {
            return null;
        }
        catch (ExecutionException ex) {
            return null;
        }
    }

    public Future<String> getTextAsync() {
        return this.content;
    }

    public URL getURL() {
        return this.docURL;
    }

    public ElementJavadoc resolveLink(String link) {
        if (link.startsWith("associate-javadoc:")) {
            String root = link.substring("associate-javadoc:".length());
            try {
                final CountDownLatch latch = new CountDownLatch(1);
                final AtomicBoolean success = new AtomicBoolean();
                SourceJavadocAttacher.attachJavadoc((URL)new URL(root), (SourceJavadocAttacher.AttachmentListener)new SourceJavadocAttacher.AttachmentListener(){

                    public void attachmentSucceeded() {
                        success.set(true);
                        latch.countDown();
                    }

                    public void attachmentFailed() {
                        latch.countDown();
                    }
                });
                if (!SwingUtilities.isEventDispatchThread()) {
                    latch.await();
                    return success.get() ? this.resolveElement(this.handle, link) : new ElementJavadoc(NbBundle.getMessage(ElementJavadoc.class, (String)"javadoc_attaching_failed"), this.cancel);
                }
            }
            catch (MalformedURLException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            catch (InterruptedException ie) {
                Exceptions.printStackTrace((Throwable)ie);
            }
            return null;
        }
        ElementHandle<? extends Element> linkDoc = this.links.get(link);
        return this.resolveElement(linkDoc, link);
    }

    public String toString() {
        return String.format("ElementJavadoc[url=%s, handle=%s]", new Object[]{this.docURL, this.handle});
    }

    private ElementJavadoc resolveElement(final ElementHandle<?> handle, final String link) {
        ElementJavadoc[] ret;
        ret = new ElementJavadoc[1];
        try {
            JavaSource js;
            FileObject fo;
            FileObject fileObject = fo = handle != null ? SourceUtils.getFile(handle, (ClasspathInfo)this.cpInfo) : null;
            if (fo != null && fo.isFolder() && handle.getKind() == ElementKind.PACKAGE) {
                fo = fo.getFileObject("package-info", "java");
            }
            if (this.cpInfo == null && fo == null) {
                try {
                    URL u = this.docURL != null ? new URL(this.docURL, link) : new URL(link);
                    ret[0] = new ElementJavadoc(u, this.cancel);
                }
                catch (MalformedURLException ex) {
                    // empty catch block
                }
                return ret[0];
            }
            JavaSource javaSource = js = fo != null ? JavaSource.forFileObject((FileObject)fo) : JavaSource.create((ClasspathInfo)this.cpInfo, (FileObject[])new FileObject[0]);
            if (js != null) {
                js.runUserActionTask((Task)new Task<CompilationController>(){

                    public void run(CompilationController controller) throws IOException {
                        controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                        if (handle != null) {
                            ret[0] = new ElementJavadoc((CompilationInfo)controller, handle.resolve((CompilationInfo)controller), null, ElementJavadoc.this.cancel);
                        } else {
                            int idx = link.indexOf(35);
                            URI uri = null;
                            try {
                                Element e;
                                uri = URI.create(idx < 0 ? link : link.substring(0, idx));
                                if (!uri.isAbsolute() && ElementJavadoc.this.handle != null && (e = ElementJavadoc.this.handle.resolve((CompilationInfo)controller)) != null) {
                                    PackageElement pe = controller.getElements().getPackageOf(e);
                                    uri = URI.create(FileObjects.getRelativePath((String)pe.getQualifiedName().toString(), (String)uri.getPath()));
                                }
                            }
                            catch (IllegalArgumentException iae) {
                                // empty catch block
                            }
                            if (uri != null) {
                                int startIdx;
                                String path;
                                if (!uri.isAbsolute()) {
                                    uri = uri.normalize();
                                }
                                startIdx = (startIdx = (path = uri.toString()).lastIndexOf("..")) < 0 ? 0 : startIdx + 3;
                                int endIdx = path.lastIndexOf(46);
                                if (endIdx >= 0) {
                                    path = path.substring(startIdx, endIdx);
                                }
                                String clsName = path.replace('/', '.');
                                Element e = controller.getElements().getTypeElement(clsName);
                                if (e != null) {
                                    if (idx >= 0) {
                                        String fragment = link.substring(idx + 1);
                                        idx = fragment.indexOf(40);
                                        String name = idx < 0 ? fragment : fragment.substring(0, idx);
                                        for (Element member : e.getEnclosedElements()) {
                                            if (!member.getSimpleName().contentEquals(name) || !fragment.contentEquals(ElementJavadoc.this.getFragment(member))) continue;
                                            e = member;
                                            break;
                                        }
                                    }
                                    ret[0] = new ElementJavadoc((CompilationInfo)controller, e, new URL(ElementJavadoc.this.docURL, link), ElementJavadoc.this.cancel);
                                } else if (uri.isAbsolute()) {
                                    ret[0] = new ElementJavadoc(uri.toURL(), ElementJavadoc.this.cancel);
                                } else if (ElementJavadoc.this.docURL != null) {
                                    try {
                                        ret[0] = new ElementJavadoc(new URL(ElementJavadoc.this.docURL, link), ElementJavadoc.this.cancel);
                                    }
                                    catch (MalformedURLException ex) {
                                        // empty catch block
                                    }
                                }
                            }
                        }
                    }
                }, true);
            }
        }
        catch (IOException ioe) {
            Exceptions.printStackTrace((Throwable)ioe);
        }
        if (ret[0] != null) {
            try {
                while (this.cancel != null && !this.cancel.call().booleanValue()) {
                    try {
                        ret[0].getTextAsync().get(250, TimeUnit.MILLISECONDS);
                        break;
                    }
                    catch (TimeoutException timeOut) {
                        continue;
                    }
                }
            }
            catch (Exception ex) {
                // empty catch block
            }
        }
        return ret[0];
    }

    public Action getGotoSourceAction() {
        return this.goToSource;
    }

    private ElementJavadoc(CompilationInfo compilationInfo, Element element, final URL url, final Callable<Boolean> cancel) {
        Pair context = Pair.of((Object)compilationInfo.getTrees(), (Object)compilationInfo.getElementUtilities());
        this.cpInfo = compilationInfo.getClasspathInfo();
        this.handle = element == null ? null : ElementHandle.create((Element)element);
        this.cancel = cancel;
        Doc doc = ((ElementUtilities)context.second()).javaDocFor(element);
        boolean localized = false;
        boolean remote = false;
        StringBuilder content = new StringBuilder();
        JavadocHelper.TextStream page = null;
        try {
            if (element != null) {
                List pages = JavadocHelper.getJavadoc((Element)element, (JavadocHelper.RemoteJavadocPolicy)JavadocHelper.RemoteJavadocPolicy.SPECULATIVE, cancel);
                for (JavadocHelper.TextStream ts : pages) {
                    localized |= ElementJavadoc.isLocalized(ts.getLocation(), element);
                    remote |= ts.isRemote();
                }
                if (remote && pages.size() > 1) {
                    throw new JavadocHelper.RemoteJavadocException(null);
                }
                page = pages.isEmpty() ? null : (JavadocHelper.TextStream)pages.get(0);
                URL uRL = this.docURL = page == null ? null : page.getLocation();
                if (!localized) {
                    this.assignSource(element, compilationInfo, url, content);
                }
            }
            this.content = this.prepareContent(content, doc, localized, page, cancel, true, context);
        }
        catch (JavadocHelper.RemoteJavadocException re) {
            final FileObject fo = compilationInfo.getFileObject();
            if (fo == null || JavaSource.forFileObject((FileObject)fo) == null) {
                StringBuilder sb = new StringBuilder(content);
                if (sb.indexOf("<p>", sb.length() - 3) == sb.length() - 3) {
                    sb.delete(sb.length() - 3, sb.length());
                }
                sb.append(this.noJavadocFound());
                this.content = new Now(sb.toString());
                return;
            }
            final StringBuilder contentFin = content;
            final boolean localizedFin = localized;
            this.content = new FutureTask<String>(new Callable<String>(){

                @Override
                public String call() throws Exception {
                    JavaSourceUtil.Handle ch = JavaSourceUtil.createControllerHandle((FileObject)fo, (JavaSourceUtil.Handle)null);
                    CompilationController c = (CompilationController)ch.getCompilationController();
                    c.toPhase(JavaSource.Phase.RESOLVED);
                    Element element = ElementJavadoc.this.handle.resolve((CompilationInfo)c);
                    JavadocHelper.TextStream page = JavadocHelper.getJavadoc((Element)element, (boolean)true, (Callable)cancel);
                    ElementJavadoc.this.docURL = page == null ? null : page.getLocation();
                    if (!ElementJavadoc.isLocalized(ElementJavadoc.this.docURL, element)) {
                        ElementJavadoc.this.assignSource(element, (CompilationInfo)c, url, contentFin);
                    }
                    Pair context = Pair.of((Object)c.getTrees(), (Object)c.getElementUtilities());
                    Doc doc = ((ElementUtilities)context.second()).javaDocFor(element);
                    return (String)ElementJavadoc.this.prepareContent(contentFin, doc, localizedFin, page, cancel, false, context).get();
                }
            });
            RP.post(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    ProgressHandle progress = ProgressHandleFactory.createHandle((String)NbBundle.getMessage(ElementJavadoc.class, (String)"LBL_HTTPJavadocDownload"));
                    progress.start();
                    try {
                        ((Runnable)((Object)ElementJavadoc.this.content)).run();
                    }
                    finally {
                        progress.finish();
                    }
                }
            });
        }
    }

    private ElementJavadoc(URL url, Callable<Boolean> cancel) {
        assert (url != null);
        this.content = null;
        this.docURL = url;
        this.handle = null;
        this.cpInfo = null;
        this.cancel = cancel;
    }

    private ElementJavadoc(String message, Callable<Boolean> cancel) {
        assert (message != null);
        this.content = new Now(message);
        this.docURL = null;
        this.handle = null;
        this.cpInfo = null;
        this.cancel = cancel;
    }

    private static boolean isLocalized(URL docURL, Element element) {
        if (docURL == null) {
            return false;
        }
        Element pkg = element;
        while (pkg.getKind() != ElementKind.PACKAGE) {
            if ((pkg = pkg.getEnclosingElement()) != null) continue;
            return false;
        }
        String pkgBinName = ((PackageElement)pkg).getQualifiedName().toString();
        String surl = docURL.toString();
        int index = surl.lastIndexOf(47);
        if (index < 0) {
            return false;
        }
        if ((index -= pkgBinName.length() + 1) < 0) {
            return false;
        }
        if ((index -= "/api".length()) < 0 || !surl.regionMatches(index, "/api", 0, "/api".length())) {
            return false;
        }
        int index2 = surl.lastIndexOf(47, index - 1);
        if (index2 < 0) {
            return false;
        }
        String lang = surl.substring(index2 + 1, index);
        return LANGS.contains(lang);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Future<String> prepareContent(StringBuilder header, Doc doc, boolean useJavadoc, final JavadocHelper.TextStream page, Callable<Boolean> cancel, boolean sync, Pair<Trees, ElementUtilities> ctx) throws JavadocHelper.RemoteJavadocException {
        try {
            final StringBuilder sb = new StringBuilder(header);
            if (doc != null) {
                ArrayList<Tag> inheritedTags;
                if (doc instanceof ProgramElementDoc) {
                    sb.append(this.getContainingClassOrPackageHeader((ProgramElementDoc)doc, ctx));
                }
                if (doc.isMethod() || doc.isConstructor() || doc.isAnnotationTypeElement()) {
                    sb.append(this.getMethodHeader((ExecutableMemberDoc)doc, ctx));
                } else if (doc.isField() || doc.isEnumConstant()) {
                    sb.append(this.getFieldHeader((FieldDoc)doc, ctx));
                } else if (doc.isClass() || doc.isInterface() || doc.isAnnotationType()) {
                    sb.append(this.getClassHeader((ClassDoc)doc, ctx));
                } else if (doc instanceof PackageDoc) {
                    sb.append(this.getPackageHeader((PackageDoc)doc, ctx));
                }
                sb.append("<p>");
                if (!useJavadoc) {
                    Tag[] inlineTags = doc.inlineTags();
                    if (doc.isMethod()) {
                        ArrayList<Tag> tags;
                        String s;
                        MethodDoc mdoc = (MethodDoc)doc;
                        inheritedTags = null;
                        if (inlineTags.length == 0) {
                            inheritedTags = new ArrayList();
                        } else {
                            for (Tag tag : inlineTags) {
                                if (!"@inheritDoc".equals(tag.kind()) || inheritedTags != null) continue;
                                inheritedTags = new ArrayList<Tag>();
                            }
                        }
                        Tag[] returnTags = mdoc.tags("@return");
                        ArrayList<Tag> inheritedReturnTags = null;
                        if (!"void".equals(mdoc.returnType().typeName())) {
                            if (returnTags.length == 0) {
                                inheritedReturnTags = new ArrayList<Tag>();
                            } else {
                                ArrayList<Tag> tags2 = new ArrayList<Tag>();
                                for (Tag tag : returnTags) {
                                    for (Tag t : tag.inlineTags()) {
                                        if ("@inheritDoc".equals(t.kind()) && inheritedReturnTags == null) {
                                            inheritedReturnTags = new ArrayList();
                                        }
                                        tags2.add(t);
                                    }
                                }
                                returnTags = tags2.toArray((T[])new Tag[tags2.size()]);
                            }
                        }
                        HashSet<Integer> paramPos = new HashSet<Integer>();
                        LinkedHashMap<Integer, List<Tag>> paramTags = null;
                        LinkedHashMap<Integer, ParamTag> inheritedParamTags = null;
                        LinkedHashMap<Integer, List<Tag>> inheritedParamInlineTags = null;
                        Parameter[] parameters = mdoc.parameters();
                        if (parameters.length > 0) {
                            paramTags = new LinkedHashMap<Integer, List<Tag>>();
                            for (int i = 0; i < parameters.length; ++i) {
                                paramPos.add(i);
                            }
                        }
                        for (ParamTag tag : mdoc.paramTags()) {
                            Integer pos = this.paramPos(mdoc, tag);
                            if (!paramPos.remove(pos)) continue;
                            tags = new ArrayList<Tag>();
                            paramTags.put(pos, tags);
                            for (Tag t : tag.inlineTags()) {
                                if ("@inheritDoc".equals(t.kind())) {
                                    if (inheritedParamTags == null) {
                                        inheritedParamTags = new LinkedHashMap();
                                    }
                                    if (inheritedParamInlineTags == null) {
                                        inheritedParamInlineTags = new LinkedHashMap();
                                    }
                                    paramPos.add(pos);
                                    continue;
                                }
                                tags.add(t);
                            }
                        }
                        if (!paramPos.isEmpty()) {
                            if (inheritedParamTags == null) {
                                inheritedParamTags = new LinkedHashMap<Integer, ParamTag>();
                            }
                            if (inheritedParamInlineTags == null) {
                                inheritedParamInlineTags = new LinkedHashMap<Integer, List<Tag>>();
                            }
                        }
                        HashSet<String> throwsTypes = new HashSet<String>();
                        ArrayList<ThrowsTag> throwsTags = new ArrayList<ThrowsTag>();
                        HashMap<String, List<Tag>> throwsInlineTags = new HashMap<String, List<Tag>>();
                        LinkedHashMap inheritedThrowsTags = null;
                        HashMap inheritedThrowsInlineTags = null;
                        for (Type exc : mdoc.thrownExceptionTypes()) {
                            throwsTypes.add(exc.qualifiedTypeName());
                        }
                        for (Type tag2 : mdoc.throwsTags()) {
                            Type exceptionType = tag2.exceptionType();
                            String exceptionName = exceptionType != null ? exceptionType.qualifiedTypeName() : tag2.exceptionName();
                            throwsTypes.remove(exceptionName);
                            ArrayList<Tag> tags3 = new ArrayList<Tag>();
                            throwsTags.add((ThrowsTag)tag2);
                            throwsInlineTags.put(exceptionName, tags3);
                            for (Tag t : tag2.inlineTags()) {
                                if ("@inheritDoc".equals(t.kind())) {
                                    if (inheritedThrowsTags == null) {
                                        inheritedThrowsTags = new LinkedHashMap();
                                    }
                                    if (inheritedThrowsInlineTags == null) {
                                        inheritedThrowsInlineTags = new HashMap();
                                    }
                                    throwsTypes.add(exceptionName);
                                    continue;
                                }
                                tags3.add(t);
                            }
                        }
                        if (!throwsTypes.isEmpty()) {
                            if (inheritedThrowsTags == null) {
                                inheritedThrowsTags = new LinkedHashMap<String, ThrowsTag>();
                            }
                            if (inheritedThrowsInlineTags == null) {
                                inheritedThrowsInlineTags = new HashMap<String, List<Tag>>();
                            }
                        }
                        if ((inheritedTags != null && inheritedTags.isEmpty() || inheritedReturnTags != null && inheritedReturnTags.isEmpty() || paramPos != null && !paramPos.isEmpty() || throwsTypes != null && !throwsTypes.isEmpty()) && (s = this.inheritedDocFor(mdoc, mdoc.containingClass(), inheritedTags, inheritedReturnTags, paramPos, inheritedParamTags, inheritedParamInlineTags, throwsTypes, inheritedThrowsTags, inheritedThrowsInlineTags, cancel, sync, ctx)) != null) {
                            sb.append(s);
                            Now len$ = new Now(sb.toString());
                            return len$;
                        }
                        if (inheritedTags != null && !inheritedTags.isEmpty()) {
                            if (inlineTags.length == 0) {
                                inlineTags = inheritedTags.toArray((T[])new Tag[inheritedTags.size()]);
                            } else {
                                tags = new ArrayList();
                                for (Tag tag3 : inlineTags) {
                                    if ("@inheritDoc".equals(tag3.kind())) {
                                        tags.addAll(inheritedTags);
                                        continue;
                                    }
                                    tags.add(tag3);
                                }
                                inlineTags = tags.toArray((T[])new Tag[tags.size()]);
                            }
                        }
                        if (inheritedReturnTags != null && !inheritedReturnTags.isEmpty()) {
                            if (returnTags.length == 0) {
                                returnTags = inheritedReturnTags.toArray((T[])new Tag[inheritedReturnTags.size()]);
                            } else {
                                tags = new ArrayList();
                                for (Tag tag3 : returnTags) {
                                    for (Tag t : tag3.inlineTags()) {
                                        if ("@inheritDoc".equals(t.kind())) {
                                            tags.addAll(inheritedReturnTags);
                                            continue;
                                        }
                                        tags.add(t);
                                    }
                                }
                                returnTags = tags.toArray((T[])new Tag[tags.size()]);
                            }
                        }
                        ArrayList<Integer> ppos = new ArrayList<Integer>();
                        if (inheritedParamTags != null && !inheritedParamTags.isEmpty()) {
                            for (Integer pos : paramTags.keySet()) {
                                ppos.add(pos);
                                ParamTag paramTag = inheritedParamTags.remove(pos);
                                List<Tag> tags4 = inheritedParamInlineTags.get(pos);
                                if (tags4 == null || tags4.isEmpty()) continue;
                                List<Tag> inTags = paramTags.get(pos);
                                inTags.clear();
                                for (Tag tag4 : paramTag.inlineTags()) {
                                    if ("@inheritDoc".equals(tag4.kind())) {
                                        inTags.addAll(tags4);
                                        continue;
                                    }
                                    inTags.add(tag4);
                                }
                            }
                            for (Integer pos2 : inheritedParamTags.keySet()) {
                                ppos.add(pos2);
                                List<Tag> tags5 = inheritedParamInlineTags.get(pos2);
                                if (tags5 == null || tags5.isEmpty()) continue;
                                paramTags.put(pos2, tags5);
                            }
                        }
                        if (inheritedThrowsTags != null && !inheritedThrowsTags.isEmpty()) {
                            for (ThrowsTag throwsTag : throwsTags) {
                                Type exceptionType = throwsTag.exceptionType();
                                String exceptionName = exceptionType != null ? exceptionType.qualifiedTypeName() : throwsTag.exceptionName();
                                inheritedThrowsTags.remove(exceptionName);
                                List<Tag> tags6 = inheritedThrowsInlineTags.get(exceptionName);
                                if (tags6 == null || tags6.isEmpty()) continue;
                                List<Tag> inTags = throwsInlineTags.get(exceptionName);
                                inTags.clear();
                                for (Tag tag5 : throwsTag.inlineTags()) {
                                    if ("@inheritDoc".equals(tag5.kind())) {
                                        inTags.addAll(tags6);
                                        continue;
                                    }
                                    inTags.add(tag5);
                                }
                            }
                            for (Map.Entry entry : inheritedThrowsTags.entrySet()) {
                                throwsTags.add((ThrowsTag)entry.getValue());
                                List<Tag> tags7 = inheritedThrowsInlineTags.get(entry.getKey());
                                if (tags7 == null || tags7.isEmpty()) continue;
                                throwsInlineTags.put((String)entry.getKey(), tags7);
                            }
                        }
                        if (inlineTags.length > 0 || doc.tags().length > 0) {
                            sb.append(this.getDeprecatedTag(doc, ctx));
                            sb.append(this.inlineTags(doc, inlineTags, ctx));
                            sb.append("<p>");
                            sb.append(this.getMethodTags(mdoc, returnTags, paramTags, throwsTags, throwsInlineTags, ctx));
                            Now i$ = new Now(sb.toString());
                            return i$;
                        }
                    } else if (inlineTags.length > 0 || doc.tags().length > 0) {
                        sb.append(this.getDeprecatedTag(doc, ctx));
                        sb.append(this.inlineTags(doc, inlineTags, ctx));
                        sb.append("<p>");
                        sb.append(this.getTags(doc, ctx));
                        Now mdoc = new Now(sb.toString());
                        return mdoc;
                    }
                }
                Callable<String> call = new Callable<String>(){

                    @Override
                    public String call() throws Exception {
                        String jdText;
                        String string = page != null ? HTMLJavadocParser.getJavadocText(page, false) : (jdText = ElementJavadoc.this.docURL != null ? HTMLJavadocParser.getJavadocText(ElementJavadoc.this.docURL, false) : null);
                        if (jdText != null) {
                            sb.append(jdText);
                        } else {
                            if (sb.indexOf("<p>", sb.length() - 3) == sb.length() - 3) {
                                sb.delete(sb.length() - 3, sb.length());
                            }
                            sb.append(ElementJavadoc.this.noJavadocFound());
                        }
                        return sb.toString();
                    }
                };
                FutureTask task = new FutureTask(call);
                if (sync) {
                    RP.post(task);
                } else {
                    task.run();
                }
                inheritedTags = task;
                return inheritedTags;
            }
            if (sb.indexOf("<p>", sb.length() - 3) == sb.length() - 3) {
                sb.delete(sb.length() - 3, sb.length());
            }
            sb.append(this.noJavadocFound());
            Now call = new Now(sb.toString());
            return call;
        }
        finally {
            if (page != null) {
                page.close();
            }
        }
    }

    private String noJavadocFound() {
        if (this.handle != null) {
            FileObject resource;
            ArrayList<ClassPath> cps = new ArrayList<ClassPath>(2);
            ClassPath cp = this.cpInfo.getClassPath(ClasspathInfo.PathKind.BOOT);
            if (cp != null) {
                cps.add(cp);
            }
            if ((cp = this.cpInfo.getClassPath(ClasspathInfo.PathKind.COMPILE)) != null) {
                cps.add(cp);
            }
            cp = ClassPathSupport.createProxyClassPath((ClassPath[])cps.toArray((T[])new ClassPath[cps.size()]));
            String toSearch = SourceUtils.getJVMSignature(this.handle)[0].replace('.', '/');
            if (this.handle.getKind() != ElementKind.PACKAGE) {
                toSearch = toSearch + ".class";
            }
            if ((resource = cp.findResource(toSearch)) != null) {
                FileObject root = cp.findOwnerRoot(resource);
                try {
                    URL rootURL = root.getURL();
                    if (JavadocForBinaryQuery.findJavadoc((URL)rootURL).getRoots().length == 0) {
                        FileObject userRoot = FileUtil.getArchiveFile((FileObject)root);
                        if (userRoot == null) {
                            userRoot = root;
                        }
                        return NbBundle.getMessage(ElementJavadoc.class, (String)"javadoc_content_not_found_attach", (Object)rootURL.toExternalForm(), (Object)FileUtil.getFileDisplayName((FileObject)userRoot));
                    }
                }
                catch (FileStateInvalidException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }
        return NbBundle.getMessage(ElementJavadoc.class, (String)"javadoc_content_not_found");
    }

    private CharSequence getContainingClassOrPackageHeader(ProgramElementDoc peDoc, Pair<Trees, ElementUtilities> ctx) {
        PackageDoc pkg;
        StringBuilder sb = new StringBuilder();
        ClassDoc cls = peDoc.containingClass();
        if (cls != null) {
            Element e = ((ElementUtilities)ctx.second()).elementFor((Doc)cls);
            if (e != null) {
                switch (e.getEnclosingElement().getKind()) {
                    case ANNOTATION_TYPE: 
                    case CLASS: 
                    case ENUM: 
                    case INTERFACE: 
                    case PACKAGE: {
                        if (cls.containingClass() == null && cls.containingPackage() == null) break;
                        sb.append("<font size='+0'><b>");
                        this.createLink(sb, e, this.makeNameLineBreakable(cls.qualifiedName()));
                        sb.append("</b></font>");
                    }
                }
            }
        } else if ((peDoc.isClass() || peDoc.isInterface() || peDoc.isEnum() || peDoc.isAnnotationType()) && (pkg = peDoc.containingPackage()) != null) {
            sb.append("<font size='+0'><b>");
            this.createLink(sb, ((ElementUtilities)ctx.second()).elementFor((Doc)pkg), this.makeNameLineBreakable(pkg.name()));
            sb.append("</b></font>");
        }
        return sb;
    }

    private String makeNameLineBreakable(String name) {
        return name.replace(".", ".&#x200B;");
    }

    private CharSequence getMethodHeader(ExecutableMemberDoc mdoc, Pair<Trees, ElementUtilities> ctx) {
        int i;
        Type[] exs;
        StringBuilder sb = new StringBuilder();
        sb.append("<p><tt>");
        sb.append(this.getAnnotations(mdoc.annotations(), ctx));
        int len = sb.length();
        sb.append(Modifier.toString(mdoc.modifierSpecifier() & -257));
        len = sb.length() - len;
        TypeVariable[] tvars = mdoc.typeParameters();
        if (tvars.length > 0) {
            if (len > 0) {
                sb.append(' ');
                ++len;
            }
            sb.append("&lt;");
            for (int i2 = 0; i2 < tvars.length; ++i2) {
                len += this.appendType(sb, (Type)tvars[i2], false, true, false, ctx);
                if (i2 >= tvars.length - 1) continue;
                sb.append(",");
                ++len;
            }
            sb.append("&gt;");
            len += 2;
        }
        if (!mdoc.isConstructor()) {
            if (len > 0) {
                sb.append(' ');
                ++len;
            }
            len += this.appendType(sb, ((MethodDoc)mdoc).returnType(), false, false, false, ctx);
        }
        if (mdoc.containingClass() != null) {
            String name = mdoc.name();
            len += name.length();
            sb.append(" <b>").append(name).append("</b>");
        }
        if (!mdoc.isAnnotationTypeElement()) {
            sb.append('(');
            ++len;
            Parameter[] params = mdoc.parameters();
            for (i = 0; i < params.length; ++i) {
                sb.append(this.getAnnotations(params[i].annotations(), ctx));
                boolean varArg = i == params.length - 1 && mdoc.isVarArgs();
                this.appendType(sb, params[i].type(), varArg, false, false, ctx);
                sb.append(' ').append(params[i].name());
                String dim = params[i].type().dimension();
                if (dim.length() > 0 && varArg) {
                    dim = dim.substring(2) + "...";
                }
                if (i >= params.length - 1) continue;
                sb.append(", ");
                this.appendSpace(sb, len);
            }
            sb.append(')');
        }
        if ((exs = mdoc.thrownExceptionTypes()).length > 0) {
            sb.append(" throws ");
            for (i = 0; i < exs.length; ++i) {
                this.appendType(sb, exs[i], false, false, false, ctx);
                if (i >= exs.length - 1) continue;
                sb.append(", ");
            }
        }
        sb.append("</tt>");
        return sb;
    }

    private CharSequence getFieldHeader(FieldDoc fdoc, Pair<Trees, ElementUtilities> ctx) {
        StringBuilder sb = new StringBuilder();
        sb.append("<p><tt>");
        sb.append(this.getAnnotations(fdoc.annotations(), ctx));
        int len = sb.length();
        sb.append(fdoc.modifiers());
        len = sb.length() - len;
        if (len > 0) {
            sb.append(' ');
        }
        this.appendType(sb, fdoc.type(), false, false, false, ctx);
        sb.append(" <b>").append(fdoc.name()).append("</b>");
        String val = fdoc.constantValueExpression();
        if (val != null && val.length() > 0) {
            sb.append(" = ").append(val);
        }
        sb.append("</tt>");
        return sb;
    }

    private CharSequence getClassHeader(ClassDoc cdoc, Pair<Trees, ElementUtilities> ctx) {
        StringBuilder sb = new StringBuilder();
        sb.append("<p><tt>");
        sb.append(this.getAnnotations(cdoc.annotations(), ctx));
        int mods = cdoc.modifierSpecifier() & -513;
        if (cdoc.isEnum()) {
            mods &= -17;
        }
        sb.append(Modifier.toString(mods));
        if (sb.length() > 0) {
            sb.append(' ');
        }
        if (cdoc.isAnnotationType()) {
            sb.append("@interface ");
        } else if (cdoc.isEnum()) {
            sb.append("enum ");
        } else if (cdoc.isInterface()) {
            sb.append("interface ");
        } else {
            sb.append("class ");
        }
        sb.append("<b>").append(cdoc.simpleTypeName());
        TypeVariable[] tvars = cdoc.typeParameters();
        if (tvars.length > 0) {
            sb.append("&lt;");
            for (int i = 0; i < tvars.length; ++i) {
                this.appendType(sb, (Type)tvars[i], false, true, false, ctx);
                if (i >= tvars.length - 1) continue;
                sb.append(",");
            }
            sb.append("&gt;");
        }
        sb.append("</b>");
        if (!cdoc.isAnnotationType()) {
            Type[] ifaces;
            Type supercls;
            if (cdoc.isClass() && (supercls = cdoc.superclassType()) != null) {
                sb.append(" extends ");
                this.appendType(sb, supercls, false, false, false, ctx);
            }
            if ((ifaces = cdoc.interfaceTypes()).length > 0) {
                sb.append(cdoc.isInterface() ? " extends " : " implements ");
                for (int i = 0; i < ifaces.length; ++i) {
                    this.appendType(sb, ifaces[i], false, false, false, ctx);
                    if (i >= ifaces.length - 1) continue;
                    sb.append(", ");
                }
            }
        }
        sb.append("</tt>");
        return sb;
    }

    private CharSequence getPackageHeader(PackageDoc pdoc, Pair<Trees, ElementUtilities> ctx) {
        StringBuilder sb = new StringBuilder();
        sb.append("<p><tt>");
        sb.append(this.getAnnotations(pdoc.annotations(), ctx));
        sb.append("package <b>").append(pdoc.name()).append("</b>");
        sb.append("</tt>");
        return sb;
    }

    private CharSequence getAnnotations(AnnotationDesc[] annotations, Pair<Trees, ElementUtilities> ctx) {
        StringBuilder sb = new StringBuilder();
        for (AnnotationDesc annotationDesc : annotations) {
            AnnotationTypeDoc annotationType = annotationDesc.annotationType();
            if (annotationType == null || !this.isDocumented(annotationType)) continue;
            this.appendType(sb, (Type)annotationType, false, false, true, ctx);
            AnnotationDesc.ElementValuePair[] pairs = annotationDesc.elementValues();
            if (pairs.length > 0) {
                sb.append('(');
                for (int i = 0; i < pairs.length; ++i) {
                    AnnotationTypeElementDoc ated = pairs[i].element();
                    this.createLink(sb, ((ElementUtilities)ctx.second()).elementFor((Doc)ated), ated.name());
                    sb.append('=');
                    this.appendAnnotationValue(sb, pairs[i].value(), ctx);
                    if (i >= pairs.length - 1) continue;
                    sb.append(",");
                }
                sb.append(')');
            }
            sb.append("<br>");
        }
        return sb;
    }

    private boolean isDocumented(AnnotationTypeDoc annotationType) {
        for (AnnotationDesc annotationDesc : annotationType.annotations()) {
            if (!"java.lang.annotation.Documented".equals(annotationDesc.annotationType().qualifiedTypeName())) continue;
            return true;
        }
        return false;
    }

    private void appendAnnotationValue(StringBuilder sb, AnnotationValue av, Pair<Trees, ElementUtilities> ctx) {
        Object value = av.value();
        if (value instanceof AnnotationValue[]) {
            int length = ((AnnotationValue[])value).length;
            if (length > 1) {
                sb.append('{');
            }
            for (int i = 0; i < ((AnnotationValue[])value).length; ++i) {
                this.appendAnnotationValue(sb, ((AnnotationValue[])value)[i], ctx);
                if (i >= ((AnnotationValue[])value).length - 1) continue;
                sb.append(",");
            }
            if (length > 1) {
                sb.append('}');
            }
        } else if (value instanceof Doc) {
            this.createLink(sb, ((ElementUtilities)ctx.second()).elementFor((Doc)value), ((Doc)value).name());
        } else {
            sb.append(value.toString());
        }
    }

    private CharSequence getMethodTags(MethodDoc doc, Tag[] returnTags, Map<Integer, List<Tag>> paramInlineTags, List<ThrowsTag> throwsTags, Map<String, List<Tag>> throwsInlineTags, Pair<Trees, ElementUtilities> ctx) {
        int length;
        StringBuilder ret = new StringBuilder();
        if (returnTags.length > 0) {
            ret.append(this.inlineTags((Doc)doc, returnTags, ctx));
            ret.append("<br>");
        }
        StringBuilder par = new StringBuilder();
        if (paramInlineTags != null) {
            Parameter[] parameters = doc.parameters();
            for (Integer pos : paramInlineTags.keySet()) {
                CharSequence cs;
                par.append("<code>").append(parameters[pos].name()).append("</code>");
                List<Tag> tags = paramInlineTags.get(pos);
                Tag[] its = tags.toArray((T[])new Tag[tags.size()]);
                if (its.length > 0 && (cs = this.inlineTags((Doc)doc, its, ctx)).length() > 0) {
                    par.append(" - ");
                    par.append(cs);
                }
                par.append("<br>");
            }
        }
        StringBuilder tpar = new StringBuilder();
        ParamTag[] tpTags = doc.typeParamTags();
        if (tpTags.length > 0) {
            for (ParamTag pTag : tpTags) {
                CharSequence cs;
                tpar.append("<code>").append(pTag.parameterName()).append("</code>");
                Tag[] its = pTag.inlineTags();
                if (its.length > 0 && (cs = this.inlineTags((Doc)doc, its, ctx)).length() > 0) {
                    tpar.append(" - ");
                    tpar.append(cs);
                }
                tpar.append("<br>");
            }
        }
        StringBuilder thr = new StringBuilder();
        if (throwsTags != null) {
            for (ThrowsTag throwsTag : throwsTags) {
                CharSequence cs;
                Tag[] its;
                thr.append("<code>");
                Type exType = throwsTag.exceptionType();
                if (exType != null) {
                    this.createLink(thr, ((ElementUtilities)ctx.second()).elementFor((Doc)exType.asClassDoc()), exType.simpleTypeName());
                } else {
                    thr.append(throwsTag.exceptionName());
                }
                thr.append("</code>");
                List<Tag> tags = throwsInlineTags.get(exType != null ? exType.qualifiedTypeName() : throwsTag.exceptionName());
                Tag[] arrtag = its = tags == null ? throwsTag.inlineTags() : tags.toArray((T[])new Tag[tags.size()]);
                if (its.length > 0 && (cs = this.inlineTags((Doc)doc, its, ctx)).length() > 0) {
                    thr.append(" - ");
                    thr.append(cs);
                }
                thr.append("<br>");
            }
        }
        StringBuilder see = new StringBuilder();
        StringBuilder since = null;
        for (Tag tag : doc.tags()) {
            if ("@see".equals(tag.kind())) {
                SeeTag stag = (SeeTag)tag;
                ClassDoc refClass = stag.referencedClass();
                PackageDoc refPackage = stag.referencedPackage();
                String className = stag.referencedClassName();
                String memberName = stag.referencedMemberName();
                String label = stag.label();
                if (memberName != null) {
                    if (refClass != null) {
                        this.createLink(see, ((ElementUtilities)ctx.second()).elementFor((Doc)stag.referencedMember()), "<code>" + (label != null && label.length() > 0 ? label : new StringBuilder().append(refClass.simpleTypeName()).append(".").append(memberName).toString()) + "</code>");
                    } else {
                        see.append(className);
                        see.append('.');
                        see.append(memberName);
                    }
                    see.append(", ");
                    continue;
                }
                if (className != null) {
                    if (refClass != null) {
                        this.createLink(see, ((ElementUtilities)ctx.second()).elementFor((Doc)refClass), "<code>" + (label != null && label.length() > 0 ? label : refClass.simpleTypeName()) + "</code>");
                    } else if (refPackage != null) {
                        this.createLink(see, ((ElementUtilities)ctx.second()).elementFor((Doc)refPackage), "<code>" + (label != null && label.length() > 0 ? label : refPackage.name()) + "</code>");
                    } else {
                        see.append(className);
                    }
                    see.append(", ");
                    continue;
                }
                see.append(stag.text()).append(", ");
                continue;
            }
            if (!"@since".equals(tag.kind())) continue;
            if (since == null) {
                since = new StringBuilder(tag.text());
                continue;
            }
            since.append(", ").append(tag.text());
        }
        StringBuilder sb = new StringBuilder();
        if (par.length() > 0) {
            sb.append("<b>").append(NbBundle.getMessage(ElementJavadoc.class, (String)"JCD-params")).append("</b><blockquote>").append(par).append("</blockquote>");
        }
        if (tpar.length() > 0) {
            sb.append("<b>").append(NbBundle.getMessage(ElementJavadoc.class, (String)"JCD-typeparams")).append("</b><blockquote>").append(tpar).append("</blockquote>");
        }
        if (ret.length() > 0) {
            sb.append("<b>").append(NbBundle.getMessage(ElementJavadoc.class, (String)"JCD-returns")).append("</b><blockquote>").append(ret).append("</blockquote>");
        }
        if (thr.length() > 0) {
            sb.append("<b>").append(NbBundle.getMessage(ElementJavadoc.class, (String)"JCD-throws")).append("</b><blockquote>").append(thr).append("</blockquote>");
        }
        if (since != null) {
            sb.append("<b>").append(NbBundle.getMessage(ElementJavadoc.class, (String)"JCD-since")).append("</b><blockquote>").append(since).append("</blockquote>");
        }
        if ((length = see.length()) > 0) {
            sb.append("<b>").append(NbBundle.getMessage(ElementJavadoc.class, (String)"JCD-see")).append("</b><blockquote>").append(see.delete(length - 2, length)).append("</blockquote>");
        }
        return sb;
    }

    private CharSequence getTags(Doc doc, Pair<Trees, ElementUtilities> ctx) {
        int length;
        StringBuilder see = new StringBuilder();
        StringBuilder par = new StringBuilder();
        StringBuilder thr = new StringBuilder();
        StringBuilder ret = new StringBuilder();
        StringBuilder since = null;
        for (Tag tag : doc.tags()) {
            if ("@param".equals(tag.kind()) && !doc.isMethod()) {
                par.append("<code>").append(((ParamTag)tag).parameterName()).append("</code>");
                Tag[] its = tag.inlineTags();
                if (its.length > 0) {
                    par.append(" - ");
                    par.append(this.inlineTags(doc, its, ctx));
                }
                par.append("<br>");
                continue;
            }
            if ("@throws".equals(tag.kind()) && !doc.isMethod()) {
                thr.append("<code>");
                Type exType = ((ThrowsTag)tag).exceptionType();
                if (exType != null) {
                    this.createLink(thr, ((ElementUtilities)ctx.second()).elementFor((Doc)exType.asClassDoc()), exType.simpleTypeName());
                } else {
                    thr.append(((ThrowsTag)tag).exceptionName());
                }
                thr.append("</code>");
                Tag[] its = tag.inlineTags();
                if (its.length > 0) {
                    thr.append(" - ");
                    thr.append(this.inlineTags(doc, its, ctx));
                }
                thr.append("<br>");
                continue;
            }
            if ("@return".equals(tag.kind()) && !doc.isMethod()) {
                ret.append(this.inlineTags(doc, tag.inlineTags(), ctx));
                ret.append("<br>");
                continue;
            }
            if ("@see".equals(tag.kind())) {
                SeeTag stag = (SeeTag)tag;
                ClassDoc refClass = stag.referencedClass();
                PackageDoc refPackage = stag.referencedPackage();
                String className = stag.referencedClassName();
                String memberName = stag.referencedMemberName();
                String label = stag.label();
                if (memberName != null) {
                    if (refClass != null) {
                        this.createLink(see, ((ElementUtilities)ctx.second()).elementFor((Doc)stag.referencedMember()), "<code>" + (label != null && label.length() > 0 ? label : new StringBuilder().append(refClass.simpleTypeName()).append(".").append(memberName).toString()) + "</code>");
                    } else {
                        see.append(className);
                        see.append('.');
                        see.append(memberName);
                    }
                    see.append(", ");
                    continue;
                }
                if (className != null) {
                    if (refClass != null) {
                        this.createLink(see, ((ElementUtilities)ctx.second()).elementFor((Doc)refClass), "<code>" + (label != null && label.length() > 0 ? label : refClass.simpleTypeName()) + "</code>");
                    } else if (refPackage != null) {
                        this.createLink(see, ((ElementUtilities)ctx.second()).elementFor((Doc)refPackage), "<code>" + (label != null && label.length() > 0 ? label : refPackage.name()) + "</code>");
                    } else {
                        see.append(className);
                    }
                    see.append(", ");
                    continue;
                }
                see.append(stag.text()).append(", ");
                continue;
            }
            if (!"@since".equals(tag.kind())) continue;
            if (since == null) {
                since = new StringBuilder(tag.text());
                continue;
            }
            since.append(", ").append(tag.text());
        }
        StringBuilder sb = new StringBuilder();
        if (par.length() > 0) {
            sb.append("<b>").append(NbBundle.getMessage(ElementJavadoc.class, (String)"JCD-params")).append("</b><blockquote>").append(par).append("</blockquote>");
        }
        if (ret.length() > 0) {
            sb.append("<b>").append(NbBundle.getMessage(ElementJavadoc.class, (String)"JCD-returns")).append("</b><blockquote>").append(ret).append("</blockquote>");
        }
        if (thr.length() > 0) {
            sb.append("<b>").append(NbBundle.getMessage(ElementJavadoc.class, (String)"JCD-throws")).append("</b><blockquote>").append(thr).append("</blockquote>");
        }
        if (since != null) {
            sb.append("<b>").append(NbBundle.getMessage(ElementJavadoc.class, (String)"JCD-since")).append("</b><blockquote>").append(since).append("</blockquote>");
        }
        if ((length = see.length()) > 0) {
            sb.append("<b>").append(NbBundle.getMessage(ElementJavadoc.class, (String)"JCD-see")).append("</b><blockquote>").append(see.delete(length - 2, length)).append("</blockquote>");
        }
        return sb;
    }

    private CharSequence getDeprecatedTag(Doc doc, Pair<Trees, ElementUtilities> ctx) {
        StringBuilder sb = new StringBuilder();
        for (Tag tag : doc.tags()) {
            if (!"@deprecated".equals(tag.kind())) continue;
            sb.append("<b>").append(NbBundle.getMessage(ElementJavadoc.class, (String)"JCD-deprecated")).append("</b> <i>").append(this.inlineTags(doc, tag.inlineTags(), ctx)).append("</i><p>");
            break;
        }
        return sb;
    }

    private CharSequence inlineTags(Doc doc, Tag[] tags, Pair<Trees, ElementUtilities> ctx) {
        StringBuilder sb = new StringBuilder();
        for (Tag tag : tags) {
            if ("@see".equals(tag.kind())) {
                SeeTag stag = (SeeTag)tag;
                if ("@value".equals(tag.name())) {
                    MemberDoc mdoc = stag.referencedMember();
                    if (mdoc == null && tag.text().length() == 0) {
                        mdoc = stag.holder();
                    }
                    if (mdoc == null || !mdoc.isField()) continue;
                    try {
                        sb.append(XMLUtil.toElementContent((String)((FieldDoc)mdoc).constantValueExpression()));
                    }
                    catch (IOException ioe) {}
                    continue;
                }
                ClassDoc refClass = stag.referencedClass();
                String memberName = stag.referencedMemberName();
                PackageDoc refPackage = stag.referencedPackage();
                String label = stag.label();
                boolean plain = "@linkplain".equals(stag.name());
                if (memberName != null) {
                    if (refClass != null) {
                        this.createLink(sb, ((ElementUtilities)ctx.second()).elementFor((Doc)stag.referencedMember()), (plain ? "" : "<code>") + (label != null && label.length() > 0 ? label : new StringBuilder().append(refClass.simpleTypeName()).append(".").append(memberName).toString()) + (plain ? "" : "</code>"));
                        continue;
                    }
                    sb.append(stag.referencedClassName());
                    sb.append('.');
                    sb.append(memberName);
                    continue;
                }
                if (refClass != null) {
                    this.createLink(sb, ((ElementUtilities)ctx.second()).elementFor((Doc)refClass), (plain ? "" : "<code>") + (label != null && label.length() > 0 ? label : refClass.simpleTypeName()) + (plain ? "" : "</code>"));
                    continue;
                }
                if (refPackage != null) {
                    this.createLink(sb, ((ElementUtilities)ctx.second()).elementFor((Doc)refPackage), (plain ? "" : "<code>") + (label != null && label.length() > 0 ? label : refPackage.name()) + (plain ? "" : "</code>"));
                    continue;
                }
                String className = stag.referencedClassName();
                sb.append(className != null ? className : stag.text());
                continue;
            }
            if ("@inheritDoc".equals(tag.kind())) {
                MethodDoc mdoc;
                if (!doc.isMethod() || (mdoc = ((MethodDoc)doc).overriddenMethod()) == null) continue;
                sb.append(this.inlineTags((Doc)mdoc, mdoc.inlineTags(), ctx));
                continue;
            }
            if ("@literal".equals(tag.kind())) {
                try {
                    sb.append(XMLUtil.toElementContent((String)tag.text()));
                }
                catch (IOException ioe) {}
                continue;
            }
            if ("@code".equals(tag.kind())) {
                sb.append("<code>");
                try {
                    sb.append(XMLUtil.toElementContent((String)tag.text()));
                }
                catch (IOException ioe) {
                    // empty catch block
                }
                sb.append("</code>");
                continue;
            }
            sb.append(tag.text());
        }
        return sb;
    }

    private CharSequence getFragment(Element e) {
        StringBuilder sb = new StringBuilder();
        if (!e.getKind().isClass() && !e.getKind().isInterface()) {
            if (e.getKind() == ElementKind.CONSTRUCTOR) {
                sb.append(e.getEnclosingElement().getSimpleName());
            } else {
                sb.append(e.getSimpleName());
            }
            if (e.getKind() == ElementKind.METHOD || e.getKind() == ElementKind.CONSTRUCTOR) {
                ExecutableElement ee = (ExecutableElement)e;
                sb.append('(');
                Iterator<? extends VariableElement> it = ee.getParameters().iterator();
                while (it.hasNext()) {
                    VariableElement param = it.next();
                    this.appendType(sb, param.asType(), ee.isVarArgs() && !it.hasNext());
                    if (!it.hasNext()) continue;
                    sb.append(", ");
                }
                sb.append(')');
            }
        }
        return sb;
    }

    private void appendType(StringBuilder sb, TypeMirror type, boolean varArg) {
        switch (type.getKind()) {
            case ARRAY: {
                this.appendType(sb, ((ArrayType)type).getComponentType(), false);
                sb.append(varArg ? "..." : "[]");
                break;
            }
            case DECLARED: {
                sb.append(((TypeElement)((DeclaredType)type).asElement()).getQualifiedName());
                break;
            }
            default: {
                sb.append(type);
            }
        }
    }

    private void appendSpace(StringBuilder sb, int length) {
        while (length-- >= 0) {
            sb.append(' ');
        }
    }

    private int appendType(StringBuilder sb, Type type, boolean varArg, boolean typeVar, boolean annotation, Pair<Trees, ElementUtilities> ctx) {
        int len = 0;
        WildcardType wt = type.asWildcardType();
        if (wt != null) {
            sb.append('?');
            ++len;
            Type[] bounds = wt.extendsBounds();
            if (bounds != null && bounds.length > 0) {
                sb.append(" extends ");
                len += 9;
                len += this.appendType(sb, bounds[0], false, false, false, ctx);
            }
            if ((bounds = wt.superBounds()) != null && bounds.length > 0) {
                sb.append(" super ");
                len += 7;
                len += this.appendType(sb, bounds[0], false, false, false, ctx);
            }
        } else {
            TypeVariable tv = type.asTypeVariable();
            if (tv != null) {
                len += this.createLink(sb, null, tv.simpleTypeName());
                Type[] bounds = tv.bounds();
                if (typeVar && bounds != null && bounds.length > 0) {
                    sb.append(" extends ");
                    len += 9;
                    for (int i = 0; i < bounds.length; ++i) {
                        len += this.appendType(sb, bounds[i], false, false, false, ctx);
                        if (i >= bounds.length - 1) continue;
                        sb.append(" & ");
                        len += 3;
                    }
                }
            } else {
                Type[] targs;
                String tName;
                ClassDoc cd = type.asClassDoc();
                String string = tName = cd != null ? cd.name() : type.simpleTypeName();
                if (cd != null && cd.isAnnotationType() && annotation) {
                    tName = "@" + tName;
                }
                len += this.createLink(sb, ((ElementUtilities)ctx.second()).elementFor((Doc)type.asClassDoc()), tName);
                ParameterizedType pt = type.asParameterizedType();
                if (pt != null && (targs = pt.typeArguments()).length > 0) {
                    sb.append("&lt;");
                    for (int j = 0; j < targs.length; ++j) {
                        len += this.appendType(sb, targs[j], false, false, false, ctx);
                        if (j >= targs.length - 1) continue;
                        sb.append(",");
                        ++len;
                    }
                    sb.append("&gt;");
                    len += 2;
                }
            }
        }
        String dim = type.dimension();
        if (dim.length() > 0) {
            if (varArg) {
                dim = dim.substring(2) + "...";
            }
            sb.append(dim);
            len += dim.length();
        }
        return len;
    }

    private int createLink(StringBuilder sb, Element e, String text) {
        if (e != null && e.asType().getKind() != TypeKind.ERROR) {
            String link = "*" + this.linkCounter++;
            this.links.put(link, (ElementHandle)ElementHandle.create((Element)e));
            sb.append("<a href='").append(link).append("'>");
        }
        sb.append(text);
        if (e != null) {
            sb.append("</a>");
        }
        return text.length();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private String inheritedDocFor(MethodDoc mdoc, ClassDoc cdoc, List<Tag> inlineTags, List<Tag> returnTags, Set<Integer> paramPos, Map<Integer, ParamTag> paramTags, Map<Integer, List<Tag>> paramInlineTags, Set<String> throwsTypes, Map<String, ThrowsTag> throwsTags, Map<String, List<Tag>> throwsInlineTags, Callable<Boolean> cancel, boolean stopByRemoteJdoc, Pair<Trees, ElementUtilities> ctx) throws JavadocHelper.RemoteJavadocException {
        JavadocHelper.TextStream inheritedPage = null;
        try {
            String jdText;
            ClassDoc superclass;
            for (ClassDoc ifaceDoc : cdoc.interfaces()) {
                for (MethodDoc methodDoc : ifaceDoc.methods(false)) {
                    void inheritedThrowsTags22;
                    if (!mdoc.overrides(methodDoc)) continue;
                    Element e = ((ElementUtilities)ctx.second()).elementFor((Doc)methodDoc);
                    boolean isLocalized = false;
                    if (e != null) {
                        inheritedPage = JavadocHelper.getJavadoc((Element)e, cancel);
                        if (inheritedPage != null) {
                            this.docURL = inheritedPage.getLocation();
                        }
                        if (!(isLocalized = ElementJavadoc.isLocalized(this.docURL, e))) {
                            ((Trees)ctx.first()).getTree(e);
                        }
                    }
                    if (isLocalized) break;
                    ArrayList<Tag> inheritedInlineTags = null;
                    if (inlineTags != null && inlineTags.isEmpty()) {
                        for (Tag tag : methodDoc.inlineTags()) {
                            if ("@inheritDoc".equals(tag.kind())) {
                                if (inheritedInlineTags != null) continue;
                                inheritedInlineTags = new ArrayList<Tag>();
                                continue;
                            }
                            inlineTags.add(tag);
                        }
                    }
                    ArrayList inheritedReturnTags = null;
                    if (returnTags != null && returnTags.isEmpty()) {
                        for (Tag tag : methodDoc.tags("@return")) {
                            for (Tag t : tag.inlineTags()) {
                                if ("@inheritDoc".equals(t.kind())) {
                                    if (inheritedReturnTags != null) continue;
                                    inheritedReturnTags = new ArrayList();
                                    continue;
                                }
                                returnTags.add(t);
                            }
                        }
                    }
                    HashSet<Integer> inheritedParamPos = null;
                    LinkedHashMap inheritedParamTags = null;
                    LinkedHashMap inheritedParamInlineTags = null;
                    if (paramTags != null && paramPos != null && !paramPos.isEmpty()) {
                        for (ParamTag tag : methodDoc.paramTags()) {
                            Integer pos = this.paramPos(methodDoc, tag);
                            if (!paramPos.remove(pos)) continue;
                            ArrayList<Tag> tags = new ArrayList<Tag>();
                            paramTags.put(pos, tag);
                            paramInlineTags.put(pos, tags);
                            for (Tag t : tag.inlineTags()) {
                                if ("@inheritDoc".equals(t.kind())) {
                                    if (inheritedParamPos == null) {
                                        inheritedParamPos = new HashSet<Integer>();
                                    }
                                    if (inheritedParamTags == null) {
                                        inheritedParamTags = new LinkedHashMap();
                                    }
                                    if (inheritedParamInlineTags == null) {
                                        inheritedParamInlineTags = new LinkedHashMap();
                                    }
                                    inheritedParamPos.add(pos);
                                    continue;
                                }
                                tags.add(t);
                            }
                        }
                    }
                    HashSet<String> inheritedThrowsTypes = null;
                    Object inheritedThrowsTags22 = null;
                    HashMap inheritedThrowsInlineTags = null;
                    if (throwsTags != null && throwsTypes != null && !throwsTypes.isEmpty()) {
                        for (ThrowsTag tag : methodDoc.throwsTags()) {
                            String exceptionName;
                            Type exceptionType = tag.exceptionType();
                            String string = exceptionName = exceptionType != null ? exceptionType.qualifiedTypeName() : tag.exceptionName();
                            if (!throwsTypes.remove(exceptionName)) continue;
                            ArrayList<Tag> tags = new ArrayList<Tag>();
                            throwsTags.put(exceptionName, tag);
                            throwsInlineTags.put(exceptionName, tags);
                            for (Tag t : tag.inlineTags()) {
                                if ("@inheritDoc".equals(t.kind())) {
                                    if (inheritedThrowsTypes == null) {
                                        inheritedThrowsTypes = new HashSet<String>();
                                    }
                                    if (inheritedThrowsTags22 == null) {
                                        LinkedHashMap inheritedThrowsTags22 = new LinkedHashMap();
                                    }
                                    if (inheritedThrowsInlineTags == null) {
                                        inheritedThrowsInlineTags = new HashMap();
                                    }
                                    inheritedThrowsTypes.add(exceptionName);
                                    continue;
                                }
                                tags.add(t);
                            }
                        }
                    }
                    if (inheritedInlineTags != null || inheritedReturnTags != null || inheritedParamPos != null && inheritedParamTags != null) {
                        this.inheritedDocFor(mdoc, ifaceDoc, (List<Tag>)inheritedInlineTags, inheritedReturnTags, (Set<Integer>)inheritedParamPos, inheritedParamTags, inheritedParamInlineTags, (Set<String>)inheritedThrowsTypes, (Map<String, ThrowsTag>)inheritedThrowsTags22, inheritedThrowsInlineTags, cancel, stopByRemoteJdoc, ctx);
                    }
                    if (inheritedInlineTags != null && !inheritedInlineTags.isEmpty()) {
                        inlineTags.clear();
                        for (Tag tag : methodDoc.inlineTags()) {
                            if ("@inheritDoc".equals(tag.kind())) {
                                inlineTags.addAll(inheritedInlineTags);
                                continue;
                            }
                            inlineTags.add(tag);
                        }
                    }
                    if (inheritedReturnTags != null && !inheritedReturnTags.isEmpty()) {
                        returnTags.clear();
                        for (Tag tag : methodDoc.tags("@return")) {
                            for (Tag t : tag.inlineTags()) {
                                if ("@inheritDoc".equals(t.kind())) {
                                    returnTags.addAll(inheritedReturnTags);
                                    continue;
                                }
                                returnTags.add(t);
                            }
                        }
                    }
                    if (inheritedParamTags != null && !inheritedParamTags.isEmpty()) {
                        for (Integer pos : inheritedParamTags.keySet()) {
                            List<Tag> tags = paramInlineTags.get(pos);
                            tags.clear();
                            for (Tag t : paramTags.get(pos).inlineTags()) {
                                if ("@inheritDoc".equals(t.kind())) {
                                    tags.addAll((Collection)inheritedParamInlineTags.get(pos));
                                    continue;
                                }
                                tags.add(t);
                            }
                        }
                    }
                    if (inheritedThrowsTags22 == null || inheritedThrowsTags22.isEmpty()) break;
                    for (String param : inheritedThrowsTags22.keySet()) {
                        List<Tag> tags = throwsInlineTags.get(param);
                        tags.clear();
                        for (Tag t : throwsTags.get(param).inlineTags()) {
                            if ("@inheritDoc".equals(t.kind())) {
                                tags.addAll((Collection)inheritedThrowsInlineTags.get(param));
                                continue;
                            }
                            tags.add(t);
                        }
                    }
                    break;
                }
                if (inlineTags != null && inlineTags.isEmpty() || returnTags != null && returnTags.isEmpty() || paramPos != null && !paramPos.isEmpty() || throwsTypes != null && !throwsTypes.isEmpty()) continue;
                MethodDoc[] arr$ = null;
                return arr$;
            }
            if (stopByRemoteJdoc && inheritedPage != null && ElementJavadoc.isRemote(inheritedPage, this.docURL)) {
                throw new JavadocHelper.RemoteJavadocException(null);
            }
            String string = jdText = inheritedPage != null ? HTMLJavadocParser.getJavadocText(inheritedPage, false) : null;
            if (jdText != null) {
                String len$ = jdText;
                return len$;
            }
            for (ClassDoc ifaceDoc2 : cdoc.interfaces()) {
                jdText = this.inheritedDocFor(mdoc, ifaceDoc2, inlineTags, returnTags, paramPos, paramTags, paramInlineTags, throwsTypes, throwsTags, throwsInlineTags, cancel, stopByRemoteJdoc, ctx);
                if (jdText != null) {
                    String len$ = jdText;
                    return len$;
                }
                if (inlineTags != null && inlineTags.isEmpty() || returnTags != null && returnTags.isEmpty() || paramPos != null && !paramPos.isEmpty() || throwsTypes != null && !throwsTypes.isEmpty()) continue;
                String len$ = null;
                return len$;
            }
            if (inheritedPage != null) {
                inheritedPage.close();
                inheritedPage = null;
            }
            if ((superclass = cdoc.superclass()) != null) {
                for (MethodDoc methodDoc : superclass.methods(false)) {
                    if (!mdoc.overrides(methodDoc)) continue;
                    Element e = ((ElementUtilities)ctx.second()).elementFor((Doc)methodDoc);
                    boolean isLocalized = false;
                    if (e != null) {
                        inheritedPage = JavadocHelper.getJavadoc((Element)e, cancel);
                        if (inheritedPage != null) {
                            this.docURL = inheritedPage.getLocation();
                        }
                        if (!(isLocalized = ElementJavadoc.isLocalized(this.docURL, e))) {
                            ((Trees)ctx.first()).getTree(e);
                        }
                    }
                    if (isLocalized) break;
                    ArrayList<Tag> inheritedInlineTags = null;
                    if (inlineTags != null && inlineTags.isEmpty()) {
                        for (Tag tag2 : methodDoc.inlineTags()) {
                            if ("@inheritDoc".equals(tag2.kind())) {
                                if (inheritedInlineTags != null) continue;
                                inheritedInlineTags = new ArrayList<Tag>();
                                continue;
                            }
                            inlineTags.add(tag2);
                        }
                    }
                    ArrayList inheritedReturnTags = null;
                    if (returnTags != null && returnTags.isEmpty()) {
                        for (Tag tag3 : methodDoc.tags("@return")) {
                            for (Tag t : tag3.inlineTags()) {
                                if ("@inheritDoc".equals(t.kind())) {
                                    if (inheritedReturnTags != null) continue;
                                    inheritedReturnTags = new ArrayList();
                                    continue;
                                }
                                returnTags.add(t);
                            }
                        }
                    }
                    HashSet<Integer> inheritedParamNames = null;
                    LinkedHashMap inheritedParamTags = null;
                    LinkedHashMap inheritedParamInlineTags = null;
                    if (paramTags != null && paramPos != null && !paramPos.isEmpty()) {
                        for (ParamTag tag4 : methodDoc.paramTags()) {
                            Integer pos = this.paramPos(methodDoc, tag4);
                            if (!paramPos.remove(pos)) continue;
                            ArrayList<Tag> tags = new ArrayList<Tag>();
                            paramTags.put(pos, tag4);
                            paramInlineTags.put(pos, tags);
                            for (Tag t : tag4.inlineTags()) {
                                if ("@inheritDoc".equals(t.kind())) {
                                    if (inheritedParamNames == null) {
                                        inheritedParamNames = new HashSet<Integer>();
                                    }
                                    if (inheritedParamTags == null) {
                                        inheritedParamTags = new LinkedHashMap();
                                    }
                                    if (inheritedParamInlineTags == null) {
                                        inheritedParamInlineTags = new LinkedHashMap();
                                    }
                                    inheritedParamNames.add(pos);
                                    continue;
                                }
                                tags.add(t);
                            }
                        }
                    }
                    HashSet<String> inheritedThrowsTypes = null;
                    LinkedHashMap inheritedThrowsTags = null;
                    HashMap inheritedThrowsInlineTags = null;
                    if (throwsTags != null && throwsTypes != null && !throwsTypes.isEmpty()) {
                        ThrowsTag[] arr$ = methodDoc.throwsTags();
                        int len$ = arr$.length;
                        boolean i$ = false;
                        while (++i$ < len$) {
                            String exceptionName;
                            ThrowsTag tag5 = arr$[i$];
                            Type exceptionType = tag5.exceptionType();
                            String string2 = exceptionName = exceptionType != null ? exceptionType.qualifiedTypeName() : tag5.exceptionName();
                            if (throwsTypes.remove(exceptionName)) {
                                ArrayList<Tag> tags = new ArrayList<Tag>();
                                throwsTags.put(exceptionName, tag5);
                                throwsInlineTags.put(exceptionName, tags);
                                for (Tag t : tag5.inlineTags()) {
                                    if ("@inheritDoc".equals(t.kind())) {
                                        if (inheritedThrowsTypes == null) {
                                            inheritedThrowsTypes = new HashSet<String>();
                                        }
                                        if (inheritedThrowsTags == null) {
                                            inheritedThrowsTags = new LinkedHashMap();
                                        }
                                        if (inheritedThrowsInlineTags == null) {
                                            inheritedThrowsInlineTags = new HashMap();
                                        }
                                        inheritedThrowsTypes.add(exceptionName);
                                        continue;
                                    }
                                    tags.add(t);
                                }
                            }
                        }
                    }
                    if (inheritedInlineTags != null || inheritedReturnTags != null || inheritedParamNames != null && inheritedParamTags != null) {
                        this.inheritedDocFor(mdoc, superclass, inheritedInlineTags, inheritedReturnTags, inheritedParamNames, inheritedParamTags, inheritedParamInlineTags, inheritedThrowsTypes, inheritedThrowsTags, inheritedThrowsInlineTags, cancel, stopByRemoteJdoc, ctx);
                    }
                    if (inheritedInlineTags != null && !inheritedInlineTags.isEmpty()) {
                        inlineTags.clear();
                        for (Tag tag : methodDoc.inlineTags()) {
                            if ("@inheritDoc".equals(tag.kind())) {
                                inlineTags.addAll(inheritedInlineTags);
                                continue;
                            }
                            inlineTags.add(tag);
                        }
                    }
                    if (inheritedReturnTags != null && !inheritedReturnTags.isEmpty()) {
                        returnTags.clear();
                        for (Tag tag : methodDoc.tags("@return")) {
                            for (Tag t : tag.inlineTags()) {
                                if ("@inheritDoc".equals(t.kind())) {
                                    returnTags.addAll(inheritedReturnTags);
                                    continue;
                                }
                                returnTags.add(t);
                            }
                        }
                    }
                    if (inheritedParamTags != null && !inheritedParamTags.isEmpty()) {
                        for (Integer pos : inheritedParamTags.keySet()) {
                            List<Tag> tags = paramInlineTags.get(pos);
                            tags.clear();
                            for (Tag t : paramTags.get(pos).inlineTags()) {
                                if ("@inheritDoc".equals(t.kind())) {
                                    tags.addAll((Collection)inheritedParamInlineTags.get(pos));
                                    continue;
                                }
                                tags.add(t);
                            }
                        }
                    }
                    if (inheritedThrowsTags == null || inheritedThrowsTags.isEmpty()) break;
                    for (String param : inheritedThrowsTags.keySet()) {
                        List<Tag> tags = throwsInlineTags.get(param);
                        tags.clear();
                        for (Tag t : throwsTags.get(param).inlineTags()) {
                            if ("@inheritDoc".equals(t.kind())) {
                                tags.addAll((Collection)inheritedThrowsInlineTags.get(param));
                                continue;
                            }
                            tags.add(t);
                        }
                    }
                    break;
                }
                if (inlineTags != null && inlineTags.isEmpty() || returnTags != null && returnTags.isEmpty() || paramPos != null && !paramPos.isEmpty() || throwsTypes != null && !throwsTypes.isEmpty()) {
                    if (stopByRemoteJdoc && inheritedPage != null && ElementJavadoc.isRemote(inheritedPage, this.docURL)) {
                        throw new JavadocHelper.RemoteJavadocException(null);
                    }
                    jdText = inheritedPage != null ? HTMLJavadocParser.getJavadocText(inheritedPage, false) : null;
                    String arr$ = jdText != null ? jdText : this.inheritedDocFor(mdoc, superclass, inlineTags, returnTags, paramPos, paramTags, paramInlineTags, throwsTypes, throwsTags, throwsInlineTags, cancel, stopByRemoteJdoc, ctx);
                    return arr$;
                }
            }
            String arr$ = null;
            return arr$;
        }
        finally {
            if (inheritedPage != null) {
                inheritedPage.close();
            }
        }
    }

    private int paramPos(MethodDoc methodDoc, ParamTag paramTag) {
        Parameter[] parameters = methodDoc.parameters();
        for (int i = 0; i < parameters.length; ++i) {
            Parameter parameter = parameters[i];
            if (!parameter.name().equals(paramTag.parameterName())) continue;
            return i;
        }
        return -1;
    }

    private static boolean isRemote(JavadocHelper.TextStream page, URL url) {
        return page != null ? page.getLocation().toString().startsWith("http") : (url != null ? url.toString().startsWith("http") : false);
    }

    private void assignSource(@NonNull Element element, @NonNull CompilationInfo compilationInfo, @NullAllowed URL url, @NonNull StringBuilder content) {
        final FileObject fo = SourceUtils.getFile((Element)element, (ClasspathInfo)compilationInfo.getClasspathInfo());
        if (fo != null) {
            this.goToSource = new AbstractAction(){

                @Override
                public void actionPerformed(ActionEvent evt) {
                    ElementOpen.open(fo, ElementJavadoc.this.handle);
                }
            };
            if (this.docURL == null) {
                content.append("<base href=\"").append(fo.toURL()).append("\"></base>");
            }
        }
        if (url != null) {
            this.docURL = url;
        }
    }

    static {
        RP = new RequestProcessor(ElementJavadoc.class);
        Locale[] availableLocales = Locale.getAvailableLocales();
        HashSet<String> locNames = new HashSet<String>((int)((float)availableLocales.length / 0.75f) + 1);
        for (Locale locale : availableLocales) {
            locNames.add(locale.toString());
        }
        LANGS = Collections.unmodifiableSet(locNames);
    }

    private static final class Now
    implements Future<String> {
        private final String value;

        Now(String value) {
            this.value = value;
        }

        @Override
        public boolean cancel(boolean mayInterruptIfRunning) {
            return false;
        }

        @Override
        public boolean isCancelled() {
            return false;
        }

        @Override
        public boolean isDone() {
            return true;
        }

        @Override
        public String get() throws InterruptedException, ExecutionException {
            return this.value;
        }

        @Override
        public String get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            return this.value;
        }
    }

}

