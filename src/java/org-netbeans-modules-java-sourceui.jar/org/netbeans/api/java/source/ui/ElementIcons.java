/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.source.UiUtils
 */
package org.netbeans.api.java.source.ui;

import java.util.Collection;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.swing.Icon;
import org.netbeans.api.java.source.UiUtils;

public class ElementIcons {
    private ElementIcons() {
    }

    public static Icon getElementIcon(ElementKind elementKind, Collection<Modifier> modifiers) {
        return UiUtils.getElementIcon((ElementKind)elementKind, modifiers);
    }
}

