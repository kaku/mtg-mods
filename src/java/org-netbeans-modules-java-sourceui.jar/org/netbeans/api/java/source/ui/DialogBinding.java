/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.DialogBinding
 *  org.netbeans.api.java.source.JavaSource
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.api.java.source.ui;

import javax.swing.text.JTextComponent;
import org.netbeans.api.java.source.JavaSource;
import org.openide.filesystems.FileObject;

@Deprecated
public final class DialogBinding {
    private DialogBinding() {
    }

    @Deprecated
    public static JavaSource bindComponentToFile(FileObject fileObject, int offset, int length, JTextComponent component) throws IllegalArgumentException {
        org.netbeans.api.editor.DialogBinding.bindComponentToFile((FileObject)fileObject, (int)offset, (int)length, (JTextComponent)component);
        return null;
    }
}

