/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.platform.JavaPlatform
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 */
package org.netbeans.api.java.source.ui;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.Future;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.Task;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

public class ScanDialog {
    private ScanDialog() {
    }

    public static boolean runWhenScanFinished(final Runnable runnable, String actionName) {
        assert (runnable != null);
        assert (actionName != null);
        assert (SwingUtilities.isEventDispatchThread());
        if (SourceUtils.isScanInProgress()) {
            class AL
            implements ActionListener {
                private Dialog dialog;
                private Future<Void> monitor;

                AL() {
                }

                public synchronized void start(Future<Void> monitor) {
                    assert (monitor != null);
                    this.monitor = monitor;
                    if (this.dialog != null) {
                        this.dialog.setVisible(true);
                    }
                }

                @Override
                public void actionPerformed(ActionEvent e) {
                    this.monitor.cancel(false);
                    this.close();
                }

                synchronized void close() {
                    if (this.dialog != null) {
                        this.dialog.setVisible(false);
                        this.dialog.dispose();
                        this.dialog = null;
                    }
                }
            }
            final AL listener = new AL();
            JLabel label = new JLabel(NbBundle.getMessage(ScanDialog.class, (String)"MSG_WaitScan"), UIManager.getIcon("OptionPane.informationIcon"), 2);
            label.setBorder(new EmptyBorder(12, 12, 11, 11));
            DialogDescriptor dd = new DialogDescriptor((Object)label, actionName, true, new Object[]{NbBundle.getMessage(ScanDialog.class, (String)"LBL_CancelAction", (Object)actionName)}, (Object)null, 0, null, (ActionListener)listener);
            listener.dialog = DialogDisplayer.getDefault().createDialog(dd);
            listener.dialog.pack();
            ClasspathInfo info = ClasspathInfo.create((ClassPath)JavaPlatform.getDefault().getBootstrapLibraries(), (ClassPath)ClassPathSupport.createClassPath((URL[])new URL[0]), (ClassPath)ClassPathSupport.createClassPath((URL[])new URL[0]));
            JavaSource js = JavaSource.create((ClasspathInfo)info, (FileObject[])new FileObject[0]);
            try {
                Future monitor = js.runWhenScanFinished((Task)new Task<CompilationController>(){

                    public void run(CompilationController parameter) throws Exception {
                        Runnable r = new Runnable(){

                            @Override
                            public void run() {
                                listener.close();
                                runnable.run();
                            }
                        };
                        if (SwingUtilities.isEventDispatchThread()) {
                            r.run();
                        } else {
                            SwingUtilities.invokeLater(r);
                        }
                    }

                }, true);
                if (!monitor.isDone()) {
                    listener.start(monitor);
                }
                return monitor.isCancelled();
            }
            catch (IOException e) {
                Exceptions.printStackTrace((Throwable)e);
                return true;
            }
        }
        runnable.run();
        return false;
    }

}

