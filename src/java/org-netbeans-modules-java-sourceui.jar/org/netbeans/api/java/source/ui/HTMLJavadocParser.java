/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.java.source.JavadocHelper
 *  org.netbeans.modules.java.source.JavadocHelper$TextStream
 */
package org.netbeans.api.java.source.ui;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.ChangedCharSetException;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;
import org.netbeans.modules.java.source.JavadocHelper;

class HTMLJavadocParser {
    public static final Logger LOG = Logger.getLogger(HTMLJavadocParser.class.getName());

    HTMLJavadocParser() {
    }

    public static String getJavadocText(URL url, boolean pkg) {
        return HTMLJavadocParser.getJavadocText(new JavadocHelper.TextStream(url), pkg);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static String getJavadocText(JavadocHelper.TextStream page, boolean pkg) {
        if (page == null) {
            return null;
        }
        InputStream is = null;
        String charset = null;
        do {
            ParserDelegator parser;
            try {
                Object urls;
                InputStreamReader reader;
                is = page.openStream();
                parser = new ParserDelegator();
                String urlStr = URLDecoder.decode(page.getLocation().toString(), "UTF-8");
                int[] offsets = null;
                InputStreamReader inputStreamReader = reader = charset == null ? new InputStreamReader(is) : new InputStreamReader(is, charset);
                if (pkg) {
                    offsets = HTMLJavadocParser.parsePackage(reader, parser, charset != null);
                } else if (urlStr.indexOf(35) > 0) {
                    urls = page.getLocations();
                    HashSet<String> possibleNames = new HashSet<String>(urls.size());
                    Iterator i$ = urls.iterator();
                    while (i$.hasNext()) {
                        URL nameUrl = (URL)i$.next();
                        urlStr = URLDecoder.decode(nameUrl.toString(), "UTF-8");
                        String memberName = urlStr.substring(urlStr.indexOf(35) + 1);
                        if (memberName.isEmpty()) continue;
                        possibleNames.add(memberName);
                    }
                    if (!possibleNames.isEmpty()) {
                        offsets = HTMLJavadocParser.parseMember(reader, possibleNames, parser, charset != null);
                    }
                } else {
                    offsets = HTMLJavadocParser.parseClass(reader, parser, charset != null);
                }
                if (offsets == null) break;
                urls = HTMLJavadocParser.getTextFromURLStream(page, offsets, charset);
                return urls;
            }
            catch (ChangedCharSetException e) {
                if (charset == null) {
                    charset = HTMLJavadocParser.getCharSet(e);
                    continue;
                }
                e.printStackTrace();
            }
            catch (FileNotFoundException x) {}
            catch (IOException ioe) {
                ioe.printStackTrace();
            }
            finally {
                parser = null;
                if (is == null) continue;
                try {
                    is.close();
                    continue;
                }
                catch (IOException ioe) {
                    ioe.printStackTrace();
                }
                continue;
            }
            break;
        } while (true);
        return null;
    }

    private static String getCharSet(ChangedCharSetException e) {
        String spec = e.getCharSetSpec();
        if (e.keyEqualsCharSet()) {
            return spec;
        }
        int index = spec.indexOf(";");
        if (index != -1) {
            spec = spec.substring(index + 1);
        }
        spec = spec.toLowerCase();
        StringTokenizer st = new StringTokenizer(spec, " \t=", true);
        boolean foundCharSet = false;
        boolean foundEquals = false;
        while (st.hasMoreTokens()) {
            String token = st.nextToken();
            if (token.equals(" ") || token.equals("\t")) continue;
            if (!foundCharSet && !foundEquals && token.equals("charset")) {
                foundCharSet = true;
                continue;
            }
            if (!foundEquals && token.equals("=")) {
                foundEquals = true;
                continue;
            }
            if (foundEquals && foundCharSet) {
                return token;
            }
            foundCharSet = false;
            foundEquals = false;
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static String getTextFromURLStream(JavadocHelper.TextStream page, int[] offsets, String charset) throws IOException {
        if (page == null) {
            return null;
        }
        InputStream fis = null;
        InputStreamReader fisreader = null;
        try {
            fis = page.openStream();
            fisreader = charset == null ? new InputStreamReader(fis) : new InputStreamReader(fis, charset);
            StringBuilder sb = new StringBuilder();
            int offset = 0;
            for (int i2 = 0; i2 < offsets.length - 1; i2 += 2) {
                int count;
                int startOffset = offsets[i2];
                int endOffset = offsets[i2 + 1];
                if (startOffset < 0 || endOffset < 0) continue;
                if (startOffset > endOffset) {
                    LOG.log(Level.WARNING, "Was not able to correctly parse javadoc: {0}, startOffset={1}, endOffset={2}.", new Object[]{page.getLocation(), startOffset, endOffset});
                    String string = null;
                    return string;
                }
                int len = endOffset - startOffset;
                char[] buffer = new char[len];
                int bytesToSkip = startOffset - offset;
                long bytesSkipped = 0;
                do {
                    bytesSkipped = fisreader.skip(bytesToSkip);
                } while ((bytesToSkip = (int)((long)bytesToSkip - bytesSkipped)) > 0 && bytesSkipped > 0);
                int bytesAlreadyRead = 0;
                while ((count = fisreader.read(buffer, bytesAlreadyRead, len - bytesAlreadyRead)) >= 0 && (bytesAlreadyRead += count) < len) {
                }
                sb.append(buffer);
                offset = endOffset;
            }
            String i2 = sb.toString();
            return i2;
        }
        finally {
            if (fisreader != null) {
                fisreader.close();
            }
        }
    }

    private static int[] parseClass(Reader reader, HTMLEditorKit.Parser parser, boolean ignoreCharset) throws IOException {
        boolean INIT = false;
        boolean CLASS_DATA_START = true;
        int TEXT_START = 2;
        int INSIDE_DIV = 3;
        int AFTER_DIV = 4;
        final int[] state = new int[]{0};
        final int[] offset = new int[]{-1, -1, -1, -1};
        HTMLEditorKit.ParserCallback callback = new HTMLEditorKit.ParserCallback(){
            int div_counter;
            int li_counter;
            int nextHRPos;
            int lastHRPos;

            @Override
            public void handleSimpleTag(HTML.Tag t, MutableAttributeSet a, int pos) {
                if (t == HTML.Tag.HR) {
                    if (state[0] == 2) {
                        this.nextHRPos = pos;
                    }
                    this.lastHRPos = pos;
                }
            }

            @Override
            public void handleStartTag(HTML.Tag t, MutableAttributeSet a, int pos) {
                String attrName;
                if (t == HTML.Tag.P && state[0] == 1) {
                    if (offset[0] != -1 && offset[1] == -1) {
                        offset[1] = pos + 3;
                    } else {
                        state[0] = 2;
                    }
                } else if (t == HTML.Tag.DIV) {
                    if (state[0] == 1 && a.containsAttribute(HTML.Attribute.CLASS, "block")) {
                        state[0] = 3;
                        if (offset[2] == -1) {
                            offset[2] = pos;
                        }
                    }
                    if (state[0] == 3) {
                        ++this.div_counter;
                    }
                } else if (t == HTML.Tag.LI && state[0] == 4) {
                    ++this.li_counter;
                } else if (t == HTML.Tag.A && state[0] == 2 && (attrName = (String)a.getAttribute(HTML.Attribute.NAME)) != null && attrName.length() > 0) {
                    offset[3] = this.nextHRPos != -1 && this.nextHRPos > offset[2] ? this.nextHRPos : pos;
                    state[0] = 0;
                }
            }

            @Override
            public void handleEndTag(HTML.Tag t, int pos) {
                if (t == HTML.Tag.DIV && state[0] == 3) {
                    if (--this.div_counter == 0) {
                        if (offset[0] > -1 && offset[1] == -1) {
                            state[0] = 1;
                            offset[1] = pos;
                        } else {
                            state[0] = 4;
                        }
                    }
                } else if (t == HTML.Tag.LI && state[0] == 4 && --this.li_counter < 0) {
                    offset[3] = pos;
                    state[0] = 0;
                }
            }

            @Override
            public void handleComment(char[] data, int pos) {
                String comment = String.valueOf(data);
                if (comment != null) {
                    if (comment.indexOf("START OF CLASS DATA") > 0) {
                        state[0] = 1;
                    } else if (comment.indexOf("NESTED CLASS SUMMARY") > 0 && offset[3] == -1) {
                        offset[3] = this.lastHRPos != -1 && this.lastHRPos > offset[2] ? this.lastHRPos : pos;
                    }
                }
            }

            @Override
            public void handleText(char[] data, int pos) {
                if (state[0] == 1 && "Deprecated.".equals(new String(data))) {
                    offset[0] = this.lastHRPos + 4;
                } else if (state[0] == 3 && "Deprecated.".equals(new String(data))) {
                    offset[0] = offset[2];
                    offset[2] = -1;
                } else if (state[0] == 2 && offset[2] < 0) {
                    offset[2] = pos;
                }
            }
        };
        parser.parse(reader, callback, ignoreCharset);
        callback = null;
        return offset;
    }

    private static int[] parseMember(Reader reader, final Collection<? extends String> names, HTMLEditorKit.Parser parser, boolean ignoreCharset) throws IOException {
        boolean INIT = false;
        boolean A_OPEN = true;
        int A_CLOSE = 2;
        int PRE_CLOSE = 3;
        int INSIDE_DIV = 4;
        final int[] state = new int[1];
        final int[] offset = new int[]{-1, -1};
        state[0] = 0;
        HTMLEditorKit.ParserCallback callback = new HTMLEditorKit.ParserCallback(){
            int div_counter;
            int dl_counter;
            int li_counter;
            int hrPos;
            boolean startWithNextText;

            @Override
            public void handleSimpleTag(HTML.Tag t, MutableAttributeSet a, int pos) {
                if (t == HTML.Tag.HR && state[0] == 3) {
                    this.hrPos = pos;
                }
            }

            @Override
            public void handleStartTag(HTML.Tag t, MutableAttributeSet a, int pos) {
                if (t == HTML.Tag.A) {
                    String attrName = (String)a.getAttribute(HTML.Attribute.NAME);
                    if (names.contains(attrName)) {
                        state[0] = 1;
                    } else if (state[0] == 3 && attrName != null && this.hrPos != -1) {
                        state[0] = 0;
                        offset[1] = this.hrPos;
                    }
                } else if (t == HTML.Tag.DL && state[0] == 3) {
                    ++this.dl_counter;
                } else if (t == HTML.Tag.LI && state[0] == 3) {
                    ++this.li_counter;
                } else if (t == HTML.Tag.DD && state[0] == 3 && offset[0] < 0) {
                    offset[0] = pos;
                } else if (t == HTML.Tag.DIV && (state[0] == 3 || state[0] == 2 || state[0] == 4)) {
                    state[0] = 4;
                    ++this.div_counter;
                    if (offset[0] < 0) {
                        if (this.div_counter == 2) {
                            offset[0] = pos;
                        } else if (this.div_counter == 1 && a.containsAttribute(HTML.Attribute.CLASS, "block")) {
                            this.startWithNextText = true;
                        }
                    }
                }
            }

            @Override
            public void handleEndTag(HTML.Tag t, int pos) {
                if (t == HTML.Tag.A && state[0] == 1) {
                    state[0] = 2;
                } else if (t == HTML.Tag.PRE && state[0] == 2) {
                    state[0] = 3;
                } else if (t == HTML.Tag.DL && state[0] == 3) {
                    if (--this.dl_counter == 0) {
                        this.hrPos = pos;
                    }
                } else if (t == HTML.Tag.LI && state[0] == 3) {
                    if (--this.li_counter < 0) {
                        this.hrPos = pos;
                    }
                } else if (t == HTML.Tag.DIV && state[0] == 4 && --this.div_counter == 0) {
                    state[0] = 3;
                    this.hrPos = pos;
                }
            }

            @Override
            public void handleText(char[] data, int pos) {
                if (this.startWithNextText) {
                    this.startWithNextText = false;
                    if (offset[0] < 0) {
                        offset[0] = pos;
                    }
                }
            }

            @Override
            public void handleComment(char[] data, int pos) {
                String comment = String.valueOf(data);
                if (comment != null && comment.indexOf("END OF CLASS DATA") > 0 && state[0] == 3 && this.hrPos != -1) {
                    state[0] = 0;
                    offset[1] = this.hrPos;
                }
            }
        };
        parser.parse(reader, callback, ignoreCharset);
        callback = null;
        return offset;
    }

    private static int[] parsePackage(Reader reader, HTMLEditorKit.Parser parser, boolean ignoreCharset) throws IOException {
        String name = "package_description";
        boolean INIT = false;
        boolean A_OPEN = true;
        final int[] state = new int[1];
        final int[] offset = new int[]{-1, -1};
        state[0] = 0;
        HTMLEditorKit.ParserCallback callback = new HTMLEditorKit.ParserCallback(){
            int hrPos;

            @Override
            public void handleSimpleTag(HTML.Tag t, MutableAttributeSet a, int pos) {
                if (t == HTML.Tag.HR && state[0] != 0 && state[0] == 1) {
                    this.hrPos = pos;
                    offset[1] = pos;
                }
            }

            @Override
            public void handleStartTag(HTML.Tag t, MutableAttributeSet a, int pos) {
                if (t == HTML.Tag.A) {
                    String attrName = (String)a.getAttribute(HTML.Attribute.NAME);
                    if ("package_description".equals(attrName)) {
                        state[0] = 1;
                        offset[0] = pos;
                    } else if (state[0] == 1 && attrName != null) {
                        state[0] = 0;
                        offset[1] = this.hrPos != -1 ? this.hrPos : pos;
                    }
                }
            }
        };
        parser.parse(reader, callback, ignoreCharset);
        callback = null;
        return offset;
    }

}

