/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.parsing.spi.IndexingAwareParserResultTask
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.TaskIndexingMode
 *  org.openide.util.Parameters
 */
package org.netbeans.api.java.source;

import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.modules.parsing.spi.IndexingAwareParserResultTask;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.TaskIndexingMode;
import org.openide.util.Parameters;

public abstract class JavaParserResultTask<T extends Parser.Result>
extends IndexingAwareParserResultTask<T> {
    private final JavaSource.Phase phase;

    protected JavaParserResultTask(@NonNull JavaSource.Phase phase) {
        this(phase, TaskIndexingMode.DISALLOWED_DURING_SCAN);
    }

    protected JavaParserResultTask(@NonNull JavaSource.Phase phase, @NonNull TaskIndexingMode taskIndexingMode) {
        super(taskIndexingMode);
        Parameters.notNull((CharSequence)"phase", (Object)((Object)phase));
        this.phase = phase;
    }

    @NonNull
    public final JavaSource.Phase getPhase() {
        return this.phase;
    }
}

