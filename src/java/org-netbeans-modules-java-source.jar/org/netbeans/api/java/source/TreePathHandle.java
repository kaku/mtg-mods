/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.TreeVisitor
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreeScanner
 *  com.sun.source.util.Trees
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.util.Name
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.OpenCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.EditorSupport
 *  org.openide.text.PositionRef
 *  org.openide.util.Exceptions
 *  org.openide.util.Parameters
 */
package org.netbeans.api.java.source;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TreeVisitor;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreeScanner;
import com.sun.source.util.Trees;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.Name;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.swing.text.Position;
import javax.tools.JavaFileObject;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.EditorSupport;
import org.openide.text.PositionRef;
import org.openide.util.Exceptions;
import org.openide.util.Parameters;

public final class TreePathHandle {
    private static Logger log = Logger.getLogger(TreePathHandle.class.getName());
    private final Delegate delegate;

    private TreePathHandle(Delegate d) {
        if (d == null) {
            throw new IllegalArgumentException();
        }
        this.delegate = d;
    }

    @CheckForNull
    public FileObject getFileObject() {
        return this.delegate.getFileObject();
    }

    public TreePath resolve(CompilationInfo compilationInfo) throws IllegalArgumentException {
        TreePath result = this.delegate.resolve(compilationInfo);
        if (result == null) {
            Logger.getLogger(TreePathHandle.class.getName()).info("Cannot resolve: " + this.toString());
        }
        return result;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof TreePathHandle)) {
            return false;
        }
        if (this.delegate.getClass() != ((TreePathHandle)obj).delegate.getClass()) {
            return false;
        }
        return this.delegate.equalsHandle(((TreePathHandle)obj).delegate);
    }

    public int hashCode() {
        return this.delegate.hashCode();
    }

    public Element resolveElement(CompilationInfo info) {
        Parameters.notNull((CharSequence)"info", (Object)info);
        Element result = this.delegate.resolveElement(info);
        if (result == null) {
            Logger.getLogger(TreePathHandle.class.getName()).info("Cannot resolve: " + this.toString());
        }
        return result;
    }

    @CheckForNull
    public ElementHandle getElementHandle() {
        return this.delegate.getElementHandle();
    }

    public Tree.Kind getKind() {
        return this.delegate.getKind();
    }

    public static TreePathHandle create(TreePath treePath, CompilationInfo info) throws IllegalArgumentException {
        TreePath current;
        FileObject file;
        Parameters.notNull((CharSequence)"treePath", (Object)treePath);
        Parameters.notNull((CharSequence)"info", (Object)info);
        try {
            URL url = treePath.getCompilationUnit().getSourceFile().toUri().toURL();
            file = URLMapper.findFileObject((URL)url);
            if (file == null) {
                log.log(Level.INFO, "There is no fileobject for source: " + url + ". Was this file removed?");
                return new TreePathHandle(new EmptyDelegate(url, treePath.getLeaf().getKind()));
            }
        }
        catch (MalformedURLException e) {
            throw (RuntimeException)new RuntimeException().initCause(e);
        }
        int position = ((JCTree)treePath.getLeaf()).pos;
        if (position == -1) {
            int index = TreePathHandle.listChildren(treePath.getParentPath().getLeaf()).indexOf((Object)treePath.getLeaf());
            assert (index != -1);
            return new TreePathHandle(new CountingDelegate(TreePathHandle.create(treePath.getParentPath(), info), index, treePath.getLeaf().getKind()));
        }
        PositionRef pos = TreePathHandle.createPositionRef(file, position, Position.Bias.Forward);
        Element correspondingElement = info.getTrees().getElement(current);
        Element element = null;
        for (current = treePath; current != null; current = current.getParentPath()) {
            Element loc;
            if (!TreeUtilities.CLASS_TREE_KINDS.contains((Object)current.getLeaf().getKind()) && current.getLeaf().getKind() != Tree.Kind.VARIABLE && current.getLeaf().getKind() != Tree.Kind.METHOD || !TreePathHandle.isSupported(loc = info.getTrees().getElement(current))) continue;
            element = loc;
            break;
        }
        return new TreePathHandle(new TreeDelegate(pos, new TreeDelegate.KindPath(treePath), file, element != null ? ElementHandle.create(element) : null, correspondingElement != null && TreePathHandle.isSupported(correspondingElement) ? ElementHandle.create(correspondingElement) : null));
    }

    public static TreePathHandle create(Element element, CompilationInfo info) throws IllegalArgumentException {
        URL u = null;
        String qualName = null;
        Symbol.ClassSymbol clsSym = element instanceof Symbol.ClassSymbol ? (Symbol.ClassSymbol)element : (Symbol.ClassSymbol)SourceUtils.getEnclosingTypeElement(element);
        if (clsSym != null && (clsSym.classfile != null || clsSym.sourcefile != null)) {
            try {
                if (clsSym.sourcefile != null && clsSym.sourcefile.getKind() == JavaFileObject.Kind.SOURCE && clsSym.sourcefile.toUri().isAbsolute()) {
                    u = clsSym.sourcefile.toUri().toURL();
                } else if (clsSym.classfile != null) {
                    u = clsSym.classfile.toUri().toURL();
                }
            }
            catch (MalformedURLException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            qualName = clsSym.getEnclosingElement().getQualifiedName().toString();
        }
        return new TreePathHandle(new ElementDelegate(ElementHandle.create(element), u, qualName, info.getClasspathInfo()));
    }

    private static boolean isSupported(Element el) {
        if (el == null) {
            return false;
        }
        switch (el.getKind()) {
            case PACKAGE: 
            case CLASS: 
            case INTERFACE: 
            case ENUM: 
            case METHOD: 
            case CONSTRUCTOR: 
            case INSTANCE_INIT: 
            case STATIC_INIT: 
            case FIELD: 
            case ANNOTATION_TYPE: 
            case ENUM_CONSTANT: {
                return true;
            }
        }
        return false;
    }

    private static PositionRef createPositionRef(FileObject file, int position, Position.Bias bias) {
        try {
            DataObject dob = DataObject.find((FileObject)file);
            Node.Cookie obj = dob.getCookie(OpenCookie.class);
            if (obj instanceof CloneableEditorSupport) {
                return ((CloneableEditorSupport)obj).createPositionRef(position, bias);
            }
            obj = dob.getCookie(EditorCookie.class);
            if (obj instanceof CloneableEditorSupport) {
                return ((CloneableEditorSupport)obj).createPositionRef(position, bias);
            }
            EditorSupport es = (EditorSupport)dob.getCookie(EditorSupport.class);
            if (es != null) {
                return es.createPositionRef(position, bias);
            }
        }
        catch (DataObjectNotFoundException ex) {
            ex.printStackTrace();
        }
        throw new IllegalStateException("Cannot create PositionRef for file " + file.getPath() + ". CloneableEditorSupport not found");
    }

    @NonNull
    public static TreePathHandle from(@NonNull ElementHandle<?> handle, @NonNull ClasspathInfo cpInfo) {
        Parameters.notNull((CharSequence)"handle", handle);
        Parameters.notNull((CharSequence)"cpInfo", (Object)cpInfo);
        return new TreePathHandle(new ElementDelegate(handle, null, null, cpInfo));
    }

    public String toString() {
        return "TreePathHandle[delegate:" + this.delegate + "]";
    }

    private static List<Tree> listChildren(@NonNull Tree t) {
        final LinkedList<Tree> result = new LinkedList<Tree>();
        t.accept((TreeVisitor)new TreeScanner<Void, Void>(){

            public Void scan(Tree node, Void p) {
                result.add(node);
                return null;
            }
        }, (Object)null);
        return result;
    }

    private static final class CountingDelegate
    implements Delegate {
        private final TreePathHandle parent;
        private final int index;
        private final Tree.Kind kind;

        public CountingDelegate(TreePathHandle parent, int index, Tree.Kind kind) {
            this.parent = parent;
            this.index = index;
            this.kind = kind;
        }

        @Override
        public FileObject getFileObject() {
            return this.parent.getFileObject();
        }

        @Override
        public TreePath resolve(CompilationInfo compilationInfo) throws IllegalArgumentException {
            Tree t;
            TreePath p = this.parent.resolve(compilationInfo);
            if (p == null) {
                return null;
            }
            List children = TreePathHandle.listChildren(p.getLeaf());
            if (this.index < children.size() && (t = (Tree)children.get(this.index)).getKind() == this.kind) {
                return new TreePath(p, t);
            }
            return null;
        }

        @Override
        public boolean equalsHandle(Delegate obj) {
            return this == obj;
        }

        @Override
        public Element resolveElement(CompilationInfo info) {
            return this.parent.resolveElement(info);
        }

        @Override
        public Tree.Kind getKind() {
            return this.kind;
        }

        @Override
        public ElementHandle getElementHandle() {
            return this.parent.getElementHandle();
        }
    }

    private static final class EmptyDelegate
    implements Delegate {
        private final URL source;
        private final Tree.Kind kind;

        public EmptyDelegate(URL source, Tree.Kind kind) {
            this.source = source;
            this.kind = kind;
        }

        @Override
        public FileObject getFileObject() {
            return URLMapper.findFileObject((URL)this.source);
        }

        @Override
        public TreePath resolve(CompilationInfo compilationInfo) throws IllegalArgumentException {
            return null;
        }

        @Override
        public boolean equalsHandle(Delegate obj) {
            return this == obj;
        }

        @Override
        public Element resolveElement(CompilationInfo info) {
            return null;
        }

        @Override
        public Tree.Kind getKind() {
            return this.kind;
        }

        @Override
        public ElementHandle getElementHandle() {
            return null;
        }
    }

    private static final class ElementDelegate
    implements Delegate {
        private final ElementHandle<? extends Element> el;
        private final URL source;
        private final String qualName;
        private final ClasspathInfo cpInfo;

        public ElementDelegate(ElementHandle<? extends Element> el, URL source, String qualName, ClasspathInfo cpInfo) {
            this.el = el;
            this.source = source;
            this.qualName = qualName;
            this.cpInfo = cpInfo;
        }

        @Override
        public FileObject getFileObject() {
            FileObject file = SourceUtils.getFile(this.el, this.cpInfo);
            if (file == null && this.source != null) {
                FileObject fo = URLMapper.findFileObject((URL)this.source);
                if (fo == null) {
                    log.log(Level.INFO, "There is no fileobject for source: " + this.source + ". Was this file removed?");
                    return file;
                }
                file = fo;
                if (fo.getNameExt().endsWith("sig")) {
                    String pkgName = FileObjects.convertPackage2Folder(this.qualName);
                    StringTokenizer tk = new StringTokenizer(pkgName, "/");
                    for (int i = 0; fo != null && i <= tk.countTokens(); fo = fo.getParent(), ++i) {
                    }
                    if (fo != null) {
                        try {
                            URL url = fo.getURL();
                            URL sourceRoot = null;
                            if (sourceRoot != null) {
                                FileObject root = URLMapper.findFileObject((URL)sourceRoot);
                                String resourceName = FileUtil.getRelativePath((FileObject)fo, (FileObject)URLMapper.findFileObject((URL)this.source));
                                file = root.getFileObject(resourceName.replace(".sig", ".class"));
                            } else {
                                Logger.getLogger(TreePathHandle.class.getName()).fine("Index.getSourceRootForClassFolder(url) returned null for url=" + url);
                            }
                        }
                        catch (FileStateInvalidException ex) {
                            Exceptions.printStackTrace((Throwable)ex);
                        }
                    }
                }
            }
            return file;
        }

        @Override
        public TreePath resolve(CompilationInfo compilationInfo) throws IllegalArgumentException {
            Element e = this.resolveElement(compilationInfo);
            if (e == null) {
                return null;
            }
            return compilationInfo.getTrees().getPath(e);
        }

        @Override
        public Element resolveElement(CompilationInfo info) {
            return this.el.resolve(info);
        }

        @Override
        public Tree.Kind getKind() {
            switch (this.el.getKind()) {
                case PACKAGE: {
                    return Tree.Kind.COMPILATION_UNIT;
                }
                case CLASS: 
                case INTERFACE: 
                case ENUM: 
                case ANNOTATION_TYPE: {
                    return Tree.Kind.CLASS;
                }
                case FIELD: 
                case ENUM_CONSTANT: 
                case PARAMETER: 
                case LOCAL_VARIABLE: 
                case RESOURCE_VARIABLE: 
                case EXCEPTION_PARAMETER: {
                    return Tree.Kind.VARIABLE;
                }
                case METHOD: 
                case CONSTRUCTOR: {
                    return Tree.Kind.METHOD;
                }
                case INSTANCE_INIT: 
                case STATIC_INIT: {
                    return Tree.Kind.BLOCK;
                }
                case TYPE_PARAMETER: {
                    return Tree.Kind.TYPE_PARAMETER;
                }
            }
            return Tree.Kind.OTHER;
        }

        @Override
        public boolean equalsHandle(Delegate obj) {
            ElementDelegate other = (ElementDelegate)obj;
            return this.el.signatureEquals((Element)((Object)other.el)) && this.cpInfo.equals(other.cpInfo);
        }

        @Override
        public int hashCode() {
            return Arrays.hashCode(this.el.getSignature());
        }

        public String toString() {
            return this.getClass().getSimpleName() + "[elementHandle:" + this.el + ", url:" + this.source + "]";
        }

        @Override
        public ElementHandle getElementHandle() {
            return this.el;
        }
    }

    private static final class TreeDelegate
    implements Delegate {
        private final PositionRef position;
        private final KindPath kindPath;
        private final FileObject file;
        private final ElementHandle enclosingElement;
        private final ElementHandle correspondingEl;
        private final Tree.Kind kind;

        private TreeDelegate(PositionRef position, KindPath kindPath, FileObject file, ElementHandle element, ElementHandle correspondingEl) {
            this.kindPath = kindPath;
            this.position = position;
            this.file = file;
            this.enclosingElement = element;
            this.correspondingEl = correspondingEl;
            if (kindPath != null) {
                this.kind = (Tree.Kind)kindPath.kindPath.get(0);
            } else if (correspondingEl != null) {
                ElementKind k = correspondingEl.getKind();
                switch (k) {
                    case ANNOTATION_TYPE: {
                        this.kind = Tree.Kind.ANNOTATION_TYPE;
                        break;
                    }
                    case CLASS: {
                        this.kind = Tree.Kind.CLASS;
                        break;
                    }
                    case ENUM: {
                        this.kind = Tree.Kind.ENUM;
                        break;
                    }
                    case INTERFACE: {
                        this.kind = Tree.Kind.INTERFACE;
                        break;
                    }
                    case FIELD: 
                    case ENUM_CONSTANT: {
                        this.kind = Tree.Kind.VARIABLE;
                        break;
                    }
                    case METHOD: 
                    case CONSTRUCTOR: {
                        this.kind = Tree.Kind.METHOD;
                        break;
                    }
                    default: {
                        this.kind = null;
                        break;
                    }
                }
            } else {
                this.kind = null;
            }
        }

        @Override
        public FileObject getFileObject() {
            return this.file;
        }

        @Override
        public TreePath resolve(CompilationInfo compilationInfo) throws IllegalArgumentException {
            assert (compilationInfo != null);
            if (!compilationInfo.getFileObject().equals((Object)this.getFileObject())) {
                StringBuilder debug = new StringBuilder();
                FileObject mine = this.getFileObject();
                FileObject remote = compilationInfo.getFileObject();
                debug.append("TreePathHandle [" + FileUtil.getFileDisplayName((FileObject)mine) + "] was not created from " + FileUtil.getFileDisplayName((FileObject)remote));
                debug.append("\n");
                try {
                    debug.append("mine: id=").append((Object)mine).append(", url=").append(mine.getURL().toExternalForm());
                }
                catch (FileStateInvalidException ex) {
                    debug.append(ex.getMessage());
                }
                debug.append("\n");
                try {
                    debug.append("remote: id=").append((Object)remote).append(", url=").append(remote.getURL().toExternalForm());
                }
                catch (FileStateInvalidException ex) {
                    debug.append(ex.getMessage());
                }
                throw new IllegalArgumentException(debug.toString());
            }
            Element element = this.enclosingElement != null ? (Element)this.enclosingElement.resolve(compilationInfo) : null;
            TreePath tp = null;
            if (element != null) {
                TreePath startPath = compilationInfo.getTrees().getPath(element);
                if (startPath == null) {
                    Logger.getLogger(TreePathHandle.class.getName()).fine("compilationInfo.getTrees().getPath(element) returned null for element %s " + element + "(" + this.file.getPath() + ")");
                } else {
                    tp = compilationInfo.getTreeUtilities().pathFor(startPath, this.position.getOffset() + 1);
                }
            }
            if (tp != null && new KindPath(tp).equals(this.kindPath)) {
                return tp;
            }
            int pos = this.position.getOffset();
            tp = this.resolvePathForPos(compilationInfo, pos + 1);
            if (tp != null) {
                return tp;
            }
            tp = this.resolvePathForPos(compilationInfo, pos);
            return tp;
        }

        private TreePath resolvePathForPos(CompilationInfo compilationInfo, int pos) {
            for (TreePath tp = compilationInfo.getTreeUtilities().pathFor((int)pos); tp != null; tp = tp.getParentPath()) {
                KindPath kindPath1 = new KindPath(tp);
                this.kindPath.getList().remove((Object)Tree.Kind.ERRONEOUS);
                if (!kindPath1.equals(this.kindPath)) continue;
                return tp;
            }
            return null;
        }

        @Override
        public boolean equalsHandle(Delegate obj) {
            TreeDelegate other = (TreeDelegate)obj;
            try {
                if (!(this.correspondingEl == other.correspondingEl || this.correspondingEl != null && this.correspondingEl.equals(other.correspondingEl))) {
                    return false;
                }
                if (!(this.enclosingElement == other.enclosingElement || this.enclosingElement != null && this.enclosingElement.equals(other.enclosingElement))) {
                    return false;
                }
                if (this.position == null && other.position == null) {
                    return true;
                }
                if (this.position.getPosition().getOffset() != other.position.getPosition().getOffset()) {
                    return false;
                }
                if (!(this.file == other.file || this.file != null && this.file.equals((Object)other.file))) {
                    return false;
                }
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            if (this.position == null) {
                return 553 + this.enclosingElement.hashCode();
            }
            int hash = 7;
            hash = 79 * hash + this.position.getOffset();
            hash = 79 * hash + (this.file != null ? this.file.hashCode() : 0);
            return hash;
        }

        @Override
        public Element resolveElement(CompilationInfo info) {
            if (this.correspondingEl != null) {
                return this.correspondingEl.resolve(info);
            }
            if (this.file != null && info.getFileObject() != null && info.getFileObject().equals((Object)this.file) && this.position != null) {
                TreePath tp = this.resolve(info);
                if (tp == null) {
                    return null;
                }
                Element el = info.getTrees().getElement(tp);
                if (el == null) {
                    Logger.getLogger(TreePathHandle.class.toString()).fine("info.getTrees().getElement(tp) returned null for " + (Object)tp);
                    Element staticallyImported = this.getStaticallyImportedElement(tp, info);
                    if (staticallyImported != null) {
                        return staticallyImported;
                    }
                } else {
                    return el;
                }
            }
            return null;
        }

        private Element getStaticallyImportedElement(TreePath treePath, CompilationInfo info) {
            javax.lang.model.element.Name simpleName;
            if (treePath.getLeaf().getKind() != Tree.Kind.MEMBER_SELECT) {
                return null;
            }
            MemberSelectTree memberSelectTree = (MemberSelectTree)treePath.getLeaf();
            for (TreePath tp = treePath; tp != null; tp = tp.getParentPath()) {
                Tree.Kind treeKind = tp.getLeaf().getKind();
                if (treeKind == Tree.Kind.IMPORT) {
                    if (((ImportTree)tp.getLeaf()).isStatic()) break;
                    return null;
                }
                if (treeKind == Tree.Kind.MEMBER_SELECT || treeKind == Tree.Kind.IDENTIFIER) {
                    continue;
                }
                return null;
            }
            if ((simpleName = memberSelectTree.getIdentifier()) == null) {
                return null;
            }
            TreePath declPath = new TreePath(new TreePath(treePath, (Tree)memberSelectTree), (Tree)memberSelectTree.getExpression());
            TypeElement decl = (TypeElement)info.getTrees().getElement(declPath);
            if (decl == null) {
                return null;
            }
            for (Element e : info.getElements().getAllMembers(decl)) {
                if (!e.getModifiers().contains((Object)Modifier.STATIC) || !e.getSimpleName().equals(simpleName)) continue;
                return e;
            }
            return null;
        }

        @Override
        public Tree.Kind getKind() {
            return this.kind;
        }

        public String toString() {
            return this.getClass().getSimpleName() + "[kind:" + (Object)this.kind + ", enclosingElement:" + this.enclosingElement + ", file:" + (Object)this.file + "]";
        }

        @Override
        public ElementHandle getElementHandle() {
            return this.correspondingEl;
        }

        static class KindPath {
            private ArrayList<Tree.Kind> kindPath = new ArrayList();

            KindPath(TreePath treePath) {
                while (treePath != null) {
                    this.kindPath.add(treePath.getLeaf().getKind());
                    treePath = treePath.getParentPath();
                }
            }

            public int hashCode() {
                return this.kindPath.hashCode();
            }

            public boolean equals(Object object) {
                if (object instanceof KindPath) {
                    return this.kindPath.equals(((KindPath)object).kindPath);
                }
                return false;
            }

            public ArrayList<Tree.Kind> getList() {
                return this.kindPath;
            }
        }

    }

    static interface Delegate {
        public FileObject getFileObject();

        public TreePath resolve(CompilationInfo var1) throws IllegalArgumentException;

        public boolean equalsHandle(Delegate var1);

        public int hashCode();

        public Element resolveElement(CompilationInfo var1);

        public Tree.Kind getKind();

        public ElementHandle getElementHandle();
    }

}

