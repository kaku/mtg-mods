/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.util.TreePath
 */
package org.netbeans.api.java.source.matching;

import com.sun.source.util.TreePath;
import java.util.Collection;
import java.util.Map;
import javax.lang.model.element.Element;

public final class Occurrence {
    private final TreePath occurrenceRoot;
    private final Map<String, TreePath> variables;
    private final Map<String, Collection<? extends TreePath>> multiVariables;
    private final Map<String, String> variables2Names;
    private final Map<Element, Element> variablesRemapToElement;
    private final Map<Element, TreePath> variablesRemapToTrees;

    Occurrence(TreePath occurrenceRoot, Map<String, TreePath> variables, Map<String, Collection<? extends TreePath>> multiVariables, Map<String, String> variables2Names, Map<Element, Element> variablesRemapToElement, Map<Element, TreePath> variablesRemapToTrees) {
        this.occurrenceRoot = occurrenceRoot;
        this.variables = variables;
        this.multiVariables = multiVariables;
        this.variables2Names = variables2Names;
        this.variablesRemapToElement = variablesRemapToElement;
        this.variablesRemapToTrees = variablesRemapToTrees;
    }

    public TreePath getOccurrenceRoot() {
        return this.occurrenceRoot;
    }

    public Map<String, TreePath> getVariables() {
        return this.variables;
    }

    public Map<String, Collection<? extends TreePath>> getMultiVariables() {
        return this.multiVariables;
    }

    public Map<String, String> getVariables2Names() {
        return this.variables2Names;
    }

    public Map<Element, Element> getVariablesRemapToElement() {
        return this.variablesRemapToElement;
    }

    public Map<Element, TreePath> getVariablesRemapToTrees() {
        return this.variablesRemapToTrees;
    }
}

