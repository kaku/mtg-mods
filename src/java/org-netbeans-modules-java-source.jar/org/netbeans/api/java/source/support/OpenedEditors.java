/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.util.Parameters
 *  org.openide.util.WeakSet
 */
package org.netbeans.api.java.source.support;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.modules.java.source.JavaSourceSupportAccessor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.util.Parameters;
import org.openide.util.WeakSet;

class OpenedEditors
implements PropertyChangeListener {
    private Set<JTextComponent> visibleEditors = new WeakSet();
    private Map<JTextComponent, DataObject> visibleEditors2Files = new WeakHashMap<JTextComponent, DataObject>();
    private List<ChangeListener> listeners = new ArrayList<ChangeListener>();
    private static OpenedEditors DEFAULT;

    private OpenedEditors() {
        EditorRegistry.addPropertyChangeListener((PropertyChangeListener)new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                OpenedEditors.this.stateChanged();
            }
        });
    }

    public static synchronized OpenedEditors getDefault() {
        if (DEFAULT == null) {
            DEFAULT = new OpenedEditors();
        }
        return DEFAULT;
    }

    public synchronized void addChangeListener(ChangeListener l) {
        this.listeners.add(l);
    }

    public synchronized void removeChangeListener(ChangeListener l) {
        this.listeners.remove(l);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fireChangeEvent() {
        ChangeEvent e = new ChangeEvent(this);
        ArrayList<ChangeListener> listenersCopy = null;
        OpenedEditors openedEditors = this;
        synchronized (openedEditors) {
            listenersCopy = new ArrayList<ChangeListener>(this.listeners);
        }
        for (ChangeListener l : listenersCopy) {
            l.stateChanged(e);
        }
    }

    public synchronized List<JTextComponent> getVisibleEditors() {
        LinkedList<JTextComponent> result = new LinkedList<JTextComponent>();
        for (JTextComponent c : this.visibleEditors) {
            if (this.visibleEditors2Files.get(c) == null) continue;
            result.add(c);
        }
        return Collections.unmodifiableList(result);
    }

    public synchronized Collection<FileObject> getVisibleEditorsFiles() {
        LinkedHashSet<FileObject> result = new LinkedHashSet<FileObject>();
        for (DataObject file : this.visibleEditors2Files.values()) {
            if (file == null) continue;
            result.add(file.getPrimaryFile());
        }
        return Collections.unmodifiableCollection(result);
    }

    public synchronized void stateChanged() {
        DataObject file;
        for (JTextComponent c : this.visibleEditors) {
            c.removePropertyChangeListener(this);
            DataObject file2 = this.visibleEditors2Files.remove(c);
            if (file2 == null) continue;
            file2.removePropertyChangeListener((PropertyChangeListener)this);
        }
        this.visibleEditors.clear();
        JTextComponent editor = EditorRegistry.lastFocusedComponent();
        DataObject dataObject = file = editor != null ? OpenedEditors.getDataObject(editor) : null;
        if (editor instanceof JEditorPane && file != null && JavaSource.forFileObject(file.getPrimaryFile()) != null) {
            this.visibleEditors.add(editor);
        }
        for (JTextComponent c2 : this.visibleEditors) {
            c2.addPropertyChangeListener(this);
            DataObject cFile = OpenedEditors.getDataObject(c2);
            cFile.addPropertyChangeListener((PropertyChangeListener)this);
            this.visibleEditors2Files.put(c2, cFile);
        }
        this.fireChangeEvent();
    }

    @Override
    public synchronized void propertyChange(PropertyChangeEvent evt) {
        if (evt.getSource() instanceof DataObject && "primaryFile".equals(evt.getPropertyName())) {
            this.fireChangeEvent();
            return;
        }
        if (evt.getSource() instanceof JTextComponent && this.visibleEditors.contains(evt.getSource())) {
            DataObject nueFile;
            JTextComponent c = (JTextComponent)evt.getSource();
            DataObject originalFile = this.visibleEditors2Files.get(c);
            if (originalFile != (nueFile = OpenedEditors.getDataObject(c))) {
                this.visibleEditors2Files.put(c, nueFile);
                this.fireChangeEvent();
            }
            return;
        }
    }

    private static DataObject getDataObject(JTextComponent pane) {
        Object source = pane.getDocument().getProperty("stream");
        if (!(source instanceof DataObject)) {
            return null;
        }
        return (DataObject)source;
    }

    static FileObject getFileObject(JTextComponent pane) {
        DataObject od = OpenedEditors.getDataObject(pane);
        return od != null ? od.getPrimaryFile() : null;
    }

    public static /* varargs */ boolean isSupported(FileObject file, String ... mimeTypes) throws NullPointerException {
        Parameters.notNull((CharSequence)"files", (Object)file);
        return !OpenedEditors.filterSupportedMIMETypes(Collections.singletonList(file), mimeTypes).isEmpty();
    }

    public static /* varargs */ List<FileObject> filterSupportedMIMETypes(Collection<FileObject> files, String ... mimeTypes) throws NullPointerException {
        Parameters.notNull((CharSequence)"files", files);
        boolean allowJavaExtension = false;
        if (mimeTypes == null) {
            mimeTypes = new String[]{"text/x-java"};
            allowJavaExtension = true;
        }
        List<String> mimeTypesList = Arrays.asList(mimeTypes);
        boolean allowAll = mimeTypesList.contains("*");
        LinkedList<FileObject> result = new LinkedList<FileObject>();
        Logger.getLogger(OpenedEditors.class.getName()).log(Level.FINER, "mimeTypesList={0}", mimeTypesList);
        block0 : for (FileObject f : files) {
            int plus;
            int slash;
            Logger.getLogger(OpenedEditors.class.getName()).log(Level.FINER, "analyzing={0}", (Object)f);
            if (JavaSource.forFileObject(f) == null) continue;
            if (allowAll) {
                result.add(f);
                continue;
            }
            if (allowJavaExtension && "java".equals(f.getExt())) {
                result.add(f);
                continue;
            }
            String fileMimeType = FileUtil.getMIMEType((FileObject)f);
            Logger.getLogger(OpenedEditors.class.getName()).log(Level.FINER, "fileMimeType={0}", fileMimeType);
            if (fileMimeType == null) continue;
            if (mimeTypesList.contains(fileMimeType)) {
                result.add(f);
                continue;
            }
            String shorterMimeType = fileMimeType;
            while ((slash = shorterMimeType.indexOf(47)) != -1 && (plus = shorterMimeType.indexOf(43, slash)) != -1) {
                if (!mimeTypesList.contains(shorterMimeType = shorterMimeType.substring(0, slash + 1) + shorterMimeType.substring(plus + 1))) continue;
                result.add(f);
                continue block0;
            }
        }
        Logger.getLogger(OpenedEditors.class.getName()).log(Level.FINE, "filter({0}, {1})={2}", new Object[]{files, mimeTypesList, result});
        return result;
    }

    static {
        JavaSourceSupportAccessor.ACCESSOR = new JavaSourceSupportAccessor(){

            @Override
            public Collection<FileObject> getVisibleEditorsFiles() {
                return OpenedEditors.getDefault().getVisibleEditorsFiles();
            }
        };
    }

}

