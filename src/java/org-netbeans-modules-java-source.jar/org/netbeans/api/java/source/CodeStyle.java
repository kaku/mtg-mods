/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.VariableTree
 *  org.netbeans.api.project.Project
 *  org.netbeans.modules.editor.indent.spi.CodeStylePreferences
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.api.java.source;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.Set;
import java.util.prefs.Preferences;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.swing.text.Document;
import org.netbeans.api.project.Project;
import org.netbeans.modules.editor.indent.spi.CodeStylePreferences;
import org.netbeans.modules.java.ui.FmtOptions;
import org.openide.filesystems.FileObject;

public final class CodeStyle {
    private Preferences preferences;

    private CodeStyle(Preferences preferences) {
        this.preferences = preferences;
    }

    public static CodeStyle getDefault(Project project) {
        return CodeStyle.getDefault(project.getProjectDirectory());
    }

    public static synchronized CodeStyle getDefault(FileObject file) {
        Preferences prefs = CodeStylePreferences.get((FileObject)file, (String)"text/x-java").getPreferences();
        return FmtOptions.codeStyleProducer.create(prefs);
    }

    public static synchronized CodeStyle getDefault(Document doc) {
        Preferences prefs = CodeStylePreferences.get((Document)doc, (String)"text/x-java").getPreferences();
        return FmtOptions.codeStyleProducer.create(prefs);
    }

    public boolean expandTabToSpaces() {
        return this.preferences.getBoolean("expand-tabs", FmtOptions.getDefaultAsBoolean("expand-tabs"));
    }

    public int getTabSize() {
        return this.preferences.getInt("tab-size", FmtOptions.getDefaultAsInt("tab-size"));
    }

    public int getIndentSize() {
        int indentLevel = this.preferences.getInt("indent-shift-width", FmtOptions.getDefaultAsInt("indent-shift-width"));
        if (indentLevel <= 0) {
            boolean expandTabs = this.preferences.getBoolean("expand-tabs", FmtOptions.getDefaultAsBoolean("expand-tabs"));
            indentLevel = expandTabs ? this.preferences.getInt("spaces-per-tab", FmtOptions.getDefaultAsInt("spaces-per-tab")) : this.preferences.getInt("tab-size", FmtOptions.getDefaultAsInt("tab-size"));
        }
        return indentLevel;
    }

    public int getContinuationIndentSize() {
        return this.preferences.getInt("continuationIndentSize", FmtOptions.getDefaultAsInt("continuationIndentSize"));
    }

    public int getLabelIndent() {
        return this.preferences.getInt("labelIndent", FmtOptions.getDefaultAsInt("labelIndent"));
    }

    public boolean absoluteLabelIndent() {
        return this.preferences.getBoolean("absoluteLabelIndent", FmtOptions.getDefaultAsBoolean("absoluteLabelIndent"));
    }

    public boolean indentTopLevelClassMembers() {
        return this.preferences.getBoolean("indentTopLevelClassMembers", FmtOptions.getDefaultAsBoolean("indentTopLevelClassMembers"));
    }

    public boolean indentCasesFromSwitch() {
        return this.preferences.getBoolean("indentCasesFromSwitch", FmtOptions.getDefaultAsBoolean("indentCasesFromSwitch"));
    }

    public int getRightMargin() {
        return this.preferences.getInt("text-limit-width", FmtOptions.getDefaultAsInt("text-limit-width"));
    }

    public boolean addLeadingStarInComment() {
        return this.preferences.getBoolean("addLeadingStarInComment", FmtOptions.getDefaultAsBoolean("addLeadingStarInComment"));
    }

    public boolean preferLongerNames() {
        return this.preferences.getBoolean("preferLongerNames", FmtOptions.getDefaultAsBoolean("preferLongerNames"));
    }

    public String getFieldNamePrefix() {
        return this.preferences.get("fieldNamePrefix", FmtOptions.getDefaultAsString("fieldNamePrefix"));
    }

    public String getFieldNameSuffix() {
        return this.preferences.get("fieldNameSuffix", FmtOptions.getDefaultAsString("fieldNameSuffix"));
    }

    public String getStaticFieldNamePrefix() {
        return this.preferences.get("staticFieldNamePrefix", FmtOptions.getDefaultAsString("staticFieldNamePrefix"));
    }

    public String getStaticFieldNameSuffix() {
        return this.preferences.get("staticFieldNameSuffix", FmtOptions.getDefaultAsString("staticFieldNameSuffix"));
    }

    public String getParameterNamePrefix() {
        return this.preferences.get("parameterNamePrefix", FmtOptions.getDefaultAsString("parameterNamePrefix"));
    }

    public String getParameterNameSuffix() {
        return this.preferences.get("parameterNameSuffix", FmtOptions.getDefaultAsString("parameterNameSuffix"));
    }

    public String getLocalVarNamePrefix() {
        return this.preferences.get("localVarNamePrefix", FmtOptions.getDefaultAsString("localVarNamePrefix"));
    }

    public String getLocalVarNameSuffix() {
        return this.preferences.get("localVarNameSuffix", FmtOptions.getDefaultAsString("localVarNameSuffix"));
    }

    public boolean qualifyFieldAccess() {
        return this.preferences.getBoolean("qualifyFieldAccess", FmtOptions.getDefaultAsBoolean("qualifyFieldAccess"));
    }

    public boolean useIsForBooleanGetters() {
        return this.preferences.getBoolean("useIsForBooleanGetters", FmtOptions.getDefaultAsBoolean("useIsForBooleanGetters"));
    }

    public boolean addOverrideAnnotation() {
        return this.preferences.getBoolean("addOverrideAnnotation", FmtOptions.getDefaultAsBoolean("addOverrideAnnotation"));
    }

    public boolean makeLocalVarsFinal() {
        return this.preferences.getBoolean("makeLocalVarsFinal", FmtOptions.getDefaultAsBoolean("makeLocalVarsFinal"));
    }

    public boolean makeParametersFinal() {
        return this.preferences.getBoolean("makeParametersFinal", FmtOptions.getDefaultAsBoolean("makeParametersFinal"));
    }

    public MemberGroups getClassMemberGroups() {
        return new MemberGroups(this.preferences.get("classMembersOrder", FmtOptions.getDefaultAsString("classMembersOrder")), this.preferences.getBoolean("sortMembersByVisibility", FmtOptions.getDefaultAsBoolean("sortMembersByVisibility")) ? this.preferences.get("visibilityOrder", FmtOptions.getDefaultAsString("visibilityOrder")) : null);
    }

    public boolean keepGettersAndSettersTogether() {
        return this.preferences.getBoolean("keepGettersAndSettersTogether", FmtOptions.getDefaultAsBoolean("keepGettersAndSettersTogether"));
    }

    public boolean sortMembersInGroupsAlphabetically() {
        return this.preferences.getBoolean("sortMembersInGroups", FmtOptions.getDefaultAsBoolean("sortMembersInGroups"));
    }

    public InsertionPoint getClassMemberInsertionPoint() {
        String point = this.preferences.get("classMemberInsertionPoint", FmtOptions.getDefaultAsString("classMemberInsertionPoint"));
        return InsertionPoint.valueOf(point);
    }

    public BracePlacement getClassDeclBracePlacement() {
        String placement = this.preferences.get("classDeclBracePlacement", FmtOptions.getDefaultAsString("classDeclBracePlacement"));
        return BracePlacement.valueOf(placement);
    }

    public BracePlacement getMethodDeclBracePlacement() {
        String placement = this.preferences.get("methodDeclBracePlacement", FmtOptions.getDefaultAsString("methodDeclBracePlacement"));
        return BracePlacement.valueOf(placement);
    }

    public BracePlacement getOtherBracePlacement() {
        String placement = this.preferences.get("otherBracePlacement", FmtOptions.getDefaultAsString("otherBracePlacement"));
        return BracePlacement.valueOf(placement);
    }

    public boolean specialElseIf() {
        return this.preferences.getBoolean("specialElseIf", FmtOptions.getDefaultAsBoolean("specialElseIf"));
    }

    public BracesGenerationStyle redundantIfBraces() {
        String redundant = this.preferences.get("redundantIfBraces", FmtOptions.getDefaultAsString("redundantIfBraces"));
        return BracesGenerationStyle.valueOf(redundant);
    }

    public BracesGenerationStyle redundantForBraces() {
        String redundant = this.preferences.get("redundantForBraces", FmtOptions.getDefaultAsString("redundantForBraces"));
        return BracesGenerationStyle.valueOf(redundant);
    }

    public BracesGenerationStyle redundantWhileBraces() {
        String redundant = this.preferences.get("redundantWhileBraces", FmtOptions.getDefaultAsString("redundantWhileBraces"));
        return BracesGenerationStyle.valueOf(redundant);
    }

    public BracesGenerationStyle redundantDoWhileBraces() {
        String redundant = this.preferences.get("redundantDoWhileBraces", FmtOptions.getDefaultAsString("redundantDoWhileBraces"));
        return BracesGenerationStyle.valueOf(redundant);
    }

    public boolean alignMultilineMethodParams() {
        return this.preferences.getBoolean("alignMultilineMethodParams", FmtOptions.getDefaultAsBoolean("alignMultilineMethodParams"));
    }

    public boolean alignMultilineLambdaParams() {
        return this.preferences.getBoolean("alignMultilineLambdaParams", FmtOptions.getDefaultAsBoolean("alignMultilineLambdaParams"));
    }

    public boolean alignMultilineCallArgs() {
        return this.preferences.getBoolean("alignMultilineCallArgs", FmtOptions.getDefaultAsBoolean("alignMultilineCallArgs"));
    }

    public boolean alignMultilineAnnotationArgs() {
        return this.preferences.getBoolean("alignMultilineAnnotationArgs", FmtOptions.getDefaultAsBoolean("alignMultilineAnnotationArgs"));
    }

    public boolean alignMultilineImplements() {
        return this.preferences.getBoolean("alignMultilineImplements", FmtOptions.getDefaultAsBoolean("alignMultilineImplements"));
    }

    public boolean alignMultilineThrows() {
        return this.preferences.getBoolean("alignMultilineThrows", FmtOptions.getDefaultAsBoolean("alignMultilineThrows"));
    }

    public boolean alignMultilineParenthesized() {
        return this.preferences.getBoolean("alignMultilineParenthesized", FmtOptions.getDefaultAsBoolean("alignMultilineParenthesized"));
    }

    public boolean alignMultilineBinaryOp() {
        return this.preferences.getBoolean("alignMultilineBinaryOp", FmtOptions.getDefaultAsBoolean("alignMultilineBinaryOp"));
    }

    public boolean alignMultilineTernaryOp() {
        return this.preferences.getBoolean("alignMultilineTernaryOp", FmtOptions.getDefaultAsBoolean("alignMultilineTernaryOp"));
    }

    public boolean alignMultilineAssignment() {
        return this.preferences.getBoolean("alignMultilineAssignment", FmtOptions.getDefaultAsBoolean("alignMultilineAssignment"));
    }

    public boolean alignMultilineTryResources() {
        return this.preferences.getBoolean("alignMultilineTryResources", FmtOptions.getDefaultAsBoolean("alignMultilineTryResources"));
    }

    public boolean alignMultilineDisjunctiveCatchTypes() {
        return this.preferences.getBoolean("alignMultilineDisjunctiveCatchTypes", FmtOptions.getDefaultAsBoolean("alignMultilineDisjunctiveCatchTypes"));
    }

    public boolean alignMultilineFor() {
        return this.preferences.getBoolean("alignMultilineFor", FmtOptions.getDefaultAsBoolean("alignMultilineFor"));
    }

    public boolean alignMultilineArrayInit() {
        return this.preferences.getBoolean("alignMultilineArrayInit", FmtOptions.getDefaultAsBoolean("alignMultilineArrayInit"));
    }

    public boolean placeElseOnNewLine() {
        return this.preferences.getBoolean("placeElseOnNewLine", FmtOptions.getDefaultAsBoolean("placeElseOnNewLine"));
    }

    public boolean placeWhileOnNewLine() {
        return this.preferences.getBoolean("placeWhileOnNewLine", FmtOptions.getDefaultAsBoolean("placeWhileOnNewLine"));
    }

    public boolean placeCatchOnNewLine() {
        return this.preferences.getBoolean("placeCatchOnNewLine", FmtOptions.getDefaultAsBoolean("placeCatchOnNewLine"));
    }

    public boolean placeFinallyOnNewLine() {
        return this.preferences.getBoolean("placeFinallyOnNewLine", FmtOptions.getDefaultAsBoolean("placeFinallyOnNewLine"));
    }

    public boolean placeNewLineAfterModifiers() {
        return this.preferences.getBoolean("placeNewLineAfterModifiers", FmtOptions.getDefaultAsBoolean("placeNewLineAfterModifiers"));
    }

    public WrapStyle wrapExtendsImplementsKeyword() {
        String wrap = this.preferences.get("wrapExtendsImplementsKeyword", FmtOptions.getDefaultAsString("wrapExtendsImplementsKeyword"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapExtendsImplementsList() {
        String wrap = this.preferences.get("wrapExtendsImplementsList", FmtOptions.getDefaultAsString("wrapExtendsImplementsList"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapMethodParams() {
        String wrap = this.preferences.get("wrapMethodParams", FmtOptions.getDefaultAsString("wrapMethodParams"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapLambdaParams() {
        String wrap = this.preferences.get("wrapLambdaParams", FmtOptions.getDefaultAsString("wrapLambdaParams"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapLambdaArrow() {
        String wrap = this.preferences.get("wrapLambdaArrow", FmtOptions.getDefaultAsString("wrapLambdaArrow"));
        return WrapStyle.valueOf(wrap);
    }

    public boolean wrapAfterLambdaArrow() {
        return this.preferences.getBoolean("wrapAfterLambdaArrow", FmtOptions.getDefaultAsBoolean("wrapAfterLambdaArrow"));
    }

    public WrapStyle wrapThrowsKeyword() {
        String wrap = this.preferences.get("wrapThrowsKeyword", FmtOptions.getDefaultAsString("wrapThrowsKeyword"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapThrowsList() {
        String wrap = this.preferences.get("wrapThrowsList", FmtOptions.getDefaultAsString("wrapThrowsList"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapMethodCallArgs() {
        String wrap = this.preferences.get("wrapMethodCallArgs", FmtOptions.getDefaultAsString("wrapMethodCallArgs"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapAnnotationArgs() {
        String wrap = this.preferences.get("wrapAnnotationArgs", FmtOptions.getDefaultAsString("wrapAnnotationArgs"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapChainedMethodCalls() {
        String wrap = this.preferences.get("wrapChainedMethodCalls", FmtOptions.getDefaultAsString("wrapChainedMethodCalls"));
        return WrapStyle.valueOf(wrap);
    }

    public boolean wrapAfterDotInChainedMethodCalls() {
        return this.preferences.getBoolean("wrapAfterDotInChainedMethodCalls", FmtOptions.getDefaultAsBoolean("wrapAfterDotInChainedMethodCalls"));
    }

    public WrapStyle wrapArrayInit() {
        String wrap = this.preferences.get("wrapArrayInit", FmtOptions.getDefaultAsString("wrapArrayInit"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapTryResources() {
        String wrap = this.preferences.get("wrapTryResources", FmtOptions.getDefaultAsString("wrapTryResources"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapDisjunctiveCatchTypes() {
        String wrap = this.preferences.get("wrapDisjunctiveCatchTypes", FmtOptions.getDefaultAsString("wrapDisjunctiveCatchTypes"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapFor() {
        String wrap = this.preferences.get("wrapFor", FmtOptions.getDefaultAsString("wrapFor"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapForStatement() {
        String wrap = this.preferences.get("wrapForStatement", FmtOptions.getDefaultAsString("wrapForStatement"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapIfStatement() {
        String wrap = this.preferences.get("wrapIfStatement", FmtOptions.getDefaultAsString("wrapIfStatement"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapWhileStatement() {
        String wrap = this.preferences.get("wrapWhileStatement", FmtOptions.getDefaultAsString("wrapWhileStatement"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapDoWhileStatement() {
        String wrap = this.preferences.get("wrapDoWhileStatement", FmtOptions.getDefaultAsString("wrapDoWhileStatement"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapCaseStatements() {
        String wrap = this.preferences.get("wrapCaseStatements", FmtOptions.getDefaultAsString("wrapCaseStatements"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapAssert() {
        String wrap = this.preferences.get("wrapAssert", FmtOptions.getDefaultAsString("wrapAssert"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapEnumConstants() {
        String wrap = this.preferences.get("wrapEnumConstants", FmtOptions.getDefaultAsString("wrapEnumConstants"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapAnnotations() {
        String wrap = this.preferences.get("wrapAnnotations", FmtOptions.getDefaultAsString("wrapAnnotations"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapBinaryOps() {
        String wrap = this.preferences.get("wrapBinaryOps", FmtOptions.getDefaultAsString("wrapBinaryOps"));
        return WrapStyle.valueOf(wrap);
    }

    public boolean wrapAfterBinaryOps() {
        return this.preferences.getBoolean("wrapAfterBinaryOps", FmtOptions.getDefaultAsBoolean("wrapAfterBinaryOps"));
    }

    public WrapStyle wrapTernaryOps() {
        String wrap = this.preferences.get("wrapTernaryOps", FmtOptions.getDefaultAsString("wrapTernaryOps"));
        return WrapStyle.valueOf(wrap);
    }

    public boolean wrapAfterTernaryOps() {
        return this.preferences.getBoolean("wrapAfterTernaryOps", FmtOptions.getDefaultAsBoolean("wrapAfterTernaryOps"));
    }

    public WrapStyle wrapAssignOps() {
        String wrap = this.preferences.get("wrapAssignOps", FmtOptions.getDefaultAsString("wrapAssignOps"));
        return WrapStyle.valueOf(wrap);
    }

    public boolean wrapAfterAssignOps() {
        return this.preferences.getBoolean("wrapAfterAssignOps", FmtOptions.getDefaultAsBoolean("wrapAfterAssignOps"));
    }

    public int getMaximumBlankLinesInDeclarations() {
        return this.preferences.getInt("blankLinesInDeclarations", FmtOptions.getDefaultAsInt("blankLinesInDeclarations"));
    }

    public int getMaximumBlankLinesInCode() {
        return this.preferences.getInt("blankLinesInCode", FmtOptions.getDefaultAsInt("blankLinesInCode"));
    }

    public int getBlankLinesBeforePackage() {
        return this.preferences.getInt("blankLinesBeforePackage", FmtOptions.getDefaultAsInt("blankLinesBeforePackage"));
    }

    public int getBlankLinesAfterPackage() {
        return this.preferences.getInt("blankLinesAfterPackage", FmtOptions.getDefaultAsInt("blankLinesAfterPackage"));
    }

    public int getBlankLinesBeforeImports() {
        return this.preferences.getInt("blankLinesBeforeImports", FmtOptions.getDefaultAsInt("blankLinesBeforeImports"));
    }

    public int getBlankLinesAfterImports() {
        return this.preferences.getInt("blankLinesAfterImports", FmtOptions.getDefaultAsInt("blankLinesAfterImports"));
    }

    public int getBlankLinesBeforeClass() {
        return this.preferences.getInt("blankLinesBeforeClass", FmtOptions.getDefaultAsInt("blankLinesBeforeClass"));
    }

    public int getBlankLinesAfterClass() {
        return this.preferences.getInt("blankLinesAfterClass", FmtOptions.getDefaultAsInt("blankLinesAfterClass"));
    }

    public int getBlankLinesAfterClassHeader() {
        return this.preferences.getInt("blankLinesAfterClassHeader", FmtOptions.getDefaultAsInt("blankLinesAfterClassHeader"));
    }

    public int getBlankLinesAfterAnonymousClassHeader() {
        return this.preferences.getInt("blankLinesAfterAnonymousClassHeader", FmtOptions.getDefaultAsInt("blankLinesAfterAnonymousClassHeader"));
    }

    public int getBlankLinesBeforeClassClosingBrace() {
        return this.preferences.getInt("blankLinesBeforeClassClosingBrace", FmtOptions.getDefaultAsInt("blankLinesBeforeClassClosingBrace"));
    }

    public int getBlankLinesBeforeAnonymousClassClosingBrace() {
        return this.preferences.getInt("blankLinesBeforeAnonymousClassClosingBrace", FmtOptions.getDefaultAsInt("blankLinesBeforeAnonymousClassClosingBrace"));
    }

    public int getBlankLinesBeforeFields() {
        return this.preferences.getInt("blankLinesBeforeFields", FmtOptions.getDefaultAsInt("blankLinesBeforeFields"));
    }

    public int getBlankLinesAfterFields() {
        return this.preferences.getInt("blankLinesAfterFields", FmtOptions.getDefaultAsInt("blankLinesAfterFields"));
    }

    public int getBlankLinesBeforeMethods() {
        return this.preferences.getInt("blankLinesBeforeMethods", FmtOptions.getDefaultAsInt("blankLinesBeforeMethods"));
    }

    public int getBlankLinesAfterMethods() {
        return this.preferences.getInt("blankLinesAfterMethods", FmtOptions.getDefaultAsInt("blankLinesAfterMethods"));
    }

    public boolean spaceBeforeWhile() {
        return this.preferences.getBoolean("spaceBeforeWhile", FmtOptions.getDefaultAsBoolean("spaceBeforeWhile"));
    }

    public boolean spaceBeforeElse() {
        return this.preferences.getBoolean("spaceBeforeElse", FmtOptions.getDefaultAsBoolean("spaceBeforeElse"));
    }

    public boolean spaceBeforeCatch() {
        return this.preferences.getBoolean("spaceBeforeCatch", FmtOptions.getDefaultAsBoolean("spaceBeforeCatch"));
    }

    public boolean spaceBeforeFinally() {
        return this.preferences.getBoolean("spaceBeforeFinally", FmtOptions.getDefaultAsBoolean("spaceBeforeFinally"));
    }

    public boolean spaceBeforeMethodDeclParen() {
        return this.preferences.getBoolean("spaceBeforeMethodDeclParen", FmtOptions.getDefaultAsBoolean("spaceBeforeMethodDeclParen"));
    }

    public boolean spaceBeforeMethodCallParen() {
        return this.preferences.getBoolean("spaceBeforeMethodCallParen", FmtOptions.getDefaultAsBoolean("spaceBeforeMethodCallParen"));
    }

    public boolean spaceBeforeIfParen() {
        return this.preferences.getBoolean("spaceBeforeIfParen", FmtOptions.getDefaultAsBoolean("spaceBeforeIfParen"));
    }

    public boolean spaceBeforeForParen() {
        return this.preferences.getBoolean("spaceBeforeForParen", FmtOptions.getDefaultAsBoolean("spaceBeforeForParen"));
    }

    public boolean spaceBeforeWhileParen() {
        return this.preferences.getBoolean("spaceBeforeWhileParen", FmtOptions.getDefaultAsBoolean("spaceBeforeWhileParen"));
    }

    public boolean spaceBeforeTryParen() {
        return this.preferences.getBoolean("spaceBeforeTryParen", FmtOptions.getDefaultAsBoolean("spaceBeforeTryParen"));
    }

    public boolean spaceBeforeCatchParen() {
        return this.preferences.getBoolean("spaceBeforeCatchParen", FmtOptions.getDefaultAsBoolean("spaceBeforeCatchParen"));
    }

    public boolean spaceBeforeSwitchParen() {
        return this.preferences.getBoolean("spaceBeforeSwitchParen", FmtOptions.getDefaultAsBoolean("spaceBeforeSwitchParen"));
    }

    public boolean spaceBeforeSynchronizedParen() {
        return this.preferences.getBoolean("spaceBeforeSynchronizedParen", FmtOptions.getDefaultAsBoolean("spaceBeforeSynchronizedParen"));
    }

    public boolean spaceBeforeAnnotationParen() {
        return this.preferences.getBoolean("spaceBeforeAnnotationParen", FmtOptions.getDefaultAsBoolean("spaceBeforeAnnotationParen"));
    }

    public boolean spaceAroundUnaryOps() {
        return this.preferences.getBoolean("spaceAroundUnaryOps", FmtOptions.getDefaultAsBoolean("spaceAroundUnaryOps"));
    }

    public boolean spaceAroundBinaryOps() {
        return this.preferences.getBoolean("spaceAroundBinaryOps", FmtOptions.getDefaultAsBoolean("spaceAroundBinaryOps"));
    }

    public boolean spaceAroundTernaryOps() {
        return this.preferences.getBoolean("spaceAroundTernaryOps", FmtOptions.getDefaultAsBoolean("spaceAroundTernaryOps"));
    }

    public boolean spaceAroundAssignOps() {
        return this.preferences.getBoolean("spaceAroundAssignOps", FmtOptions.getDefaultAsBoolean("spaceAroundAssignOps"));
    }

    public boolean spaceAroundAnnotationValueAssignOps() {
        return this.preferences.getBoolean("spaceAroundAnnotationValueAssignOps", FmtOptions.getDefaultAsBoolean("spaceAroundAnnotationValueAssignOps"));
    }

    public boolean spaceAroundLambdaArrow() {
        return this.preferences.getBoolean("spaceAroundLambdaArrow", FmtOptions.getDefaultAsBoolean("spaceAroundLambdaArrow"));
    }

    public boolean spaceAroundMethodReferenceDoubleColon() {
        return this.preferences.getBoolean("spaceAroundMethodReferenceDoubleColon", FmtOptions.getDefaultAsBoolean("spaceAroundMethodReferenceDoubleColon"));
    }

    public boolean spaceBeforeClassDeclLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeClassDeclLeftBrace", FmtOptions.getDefaultAsBoolean("spaceBeforeClassDeclLeftBrace"));
    }

    public boolean spaceBeforeMethodDeclLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeMethodDeclLeftBrace", FmtOptions.getDefaultAsBoolean("spaceBeforeMethodDeclLeftBrace"));
    }

    public boolean spaceBeforeIfLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeIfLeftBrace", FmtOptions.getDefaultAsBoolean("spaceBeforeIfLeftBrace"));
    }

    public boolean spaceBeforeElseLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeElseLeftBrace", FmtOptions.getDefaultAsBoolean("spaceBeforeElseLeftBrace"));
    }

    public boolean spaceBeforeWhileLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeWhileLeftBrace", FmtOptions.getDefaultAsBoolean("spaceBeforeWhileLeftBrace"));
    }

    public boolean spaceBeforeForLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeForLeftBrace", FmtOptions.getDefaultAsBoolean("spaceBeforeForLeftBrace"));
    }

    public boolean spaceBeforeDoLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeDoLeftBrace", FmtOptions.getDefaultAsBoolean("spaceBeforeDoLeftBrace"));
    }

    public boolean spaceBeforeSwitchLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeSwitchLeftBrace", FmtOptions.getDefaultAsBoolean("spaceBeforeSwitchLeftBrace"));
    }

    public boolean spaceBeforeTryLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeTryLeftBrace", FmtOptions.getDefaultAsBoolean("spaceBeforeTryLeftBrace"));
    }

    public boolean spaceBeforeCatchLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeCatchLeftBrace", FmtOptions.getDefaultAsBoolean("spaceBeforeCatchLeftBrace"));
    }

    public boolean spaceBeforeFinallyLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeFinallyLeftBrace", FmtOptions.getDefaultAsBoolean("spaceBeforeFinallyLeftBrace"));
    }

    public boolean spaceBeforeSynchronizedLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeSynchronizedLeftBrace", FmtOptions.getDefaultAsBoolean("spaceBeforeSynchronizedLeftBrace"));
    }

    public boolean spaceBeforeStaticInitLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeStaticInitLeftBrace", FmtOptions.getDefaultAsBoolean("spaceBeforeStaticInitLeftBrace"));
    }

    public boolean spaceBeforeArrayInitLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeArrayInitLeftBrace", FmtOptions.getDefaultAsBoolean("spaceBeforeArrayInitLeftBrace"));
    }

    public boolean spaceWithinParens() {
        return this.preferences.getBoolean("spaceWithinParens", FmtOptions.getDefaultAsBoolean("spaceWithinParens"));
    }

    public boolean spaceWithinMethodDeclParens() {
        return this.preferences.getBoolean("spaceWithinMethodDeclParens", FmtOptions.getDefaultAsBoolean("spaceWithinMethodDeclParens"));
    }

    public boolean spaceWithinLambdaParens() {
        return this.preferences.getBoolean("spaceWithinLambdaParens", FmtOptions.getDefaultAsBoolean("spaceWithinLambdaParens"));
    }

    public boolean spaceWithinMethodCallParens() {
        return this.preferences.getBoolean("spaceWithinMethodCallParens", FmtOptions.getDefaultAsBoolean("spaceWithinMethodCallParens"));
    }

    public boolean spaceWithinIfParens() {
        return this.preferences.getBoolean("spaceWithinIfParens", FmtOptions.getDefaultAsBoolean("spaceWithinIfParens"));
    }

    public boolean spaceWithinForParens() {
        return this.preferences.getBoolean("spaceWithinForParens", FmtOptions.getDefaultAsBoolean("spaceWithinForParens"));
    }

    public boolean spaceWithinWhileParens() {
        return this.preferences.getBoolean("spaceWithinWhileParens", FmtOptions.getDefaultAsBoolean("spaceWithinWhileParens"));
    }

    public boolean spaceWithinSwitchParens() {
        return this.preferences.getBoolean("spaceWithinSwitchParens", FmtOptions.getDefaultAsBoolean("spaceWithinSwitchParens"));
    }

    public boolean spaceWithinTryParens() {
        return this.preferences.getBoolean("spaceWithinTryParens", FmtOptions.getDefaultAsBoolean("spaceWithinTryParens"));
    }

    public boolean spaceWithinCatchParens() {
        return this.preferences.getBoolean("spaceWithinCatchParens", FmtOptions.getDefaultAsBoolean("spaceWithinCatchParens"));
    }

    public boolean spaceWithinSynchronizedParens() {
        return this.preferences.getBoolean("spaceWithinSynchronizedParens", FmtOptions.getDefaultAsBoolean("spaceWithinSynchronizedParens"));
    }

    public boolean spaceWithinTypeCastParens() {
        return this.preferences.getBoolean("spaceWithinTypeCastParens", FmtOptions.getDefaultAsBoolean("spaceWithinTypeCastParens"));
    }

    public boolean spaceWithinAnnotationParens() {
        return this.preferences.getBoolean("spaceWithinAnnotationParens", FmtOptions.getDefaultAsBoolean("spaceWithinAnnotationParens"));
    }

    public boolean spaceWithinBraces() {
        return this.preferences.getBoolean("spaceWithinBraces", FmtOptions.getDefaultAsBoolean("spaceWithinBraces"));
    }

    public boolean spaceWithinArrayInitBrackets() {
        return this.preferences.getBoolean("spaceWithinArrayInitBrackets", FmtOptions.getDefaultAsBoolean("spaceWithinArrayInitBrackets"));
    }

    public boolean spaceWithinArrayIndexBrackets() {
        return this.preferences.getBoolean("spaceWithinArrayIndexBrackets", FmtOptions.getDefaultAsBoolean("spaceWithinArrayIndexBrackets"));
    }

    public boolean spaceBeforeComma() {
        return this.preferences.getBoolean("spaceBeforeComma", FmtOptions.getDefaultAsBoolean("spaceBeforeComma"));
    }

    public boolean spaceAfterComma() {
        return this.preferences.getBoolean("spaceAfterComma", FmtOptions.getDefaultAsBoolean("spaceAfterComma"));
    }

    public boolean spaceBeforeSemi() {
        return this.preferences.getBoolean("spaceBeforeSemi", FmtOptions.getDefaultAsBoolean("spaceBeforeSemi"));
    }

    public boolean spaceAfterSemi() {
        return this.preferences.getBoolean("spaceAfterSemi", FmtOptions.getDefaultAsBoolean("spaceAfterSemi"));
    }

    public boolean spaceBeforeColon() {
        return this.preferences.getBoolean("spaceBeforeColon", FmtOptions.getDefaultAsBoolean("spaceBeforeColon"));
    }

    public boolean spaceAfterColon() {
        return this.preferences.getBoolean("spaceAfterColon", FmtOptions.getDefaultAsBoolean("spaceAfterColon"));
    }

    public boolean spaceAfterTypeCast() {
        return this.preferences.getBoolean("spaceAfterTypeCast", FmtOptions.getDefaultAsBoolean("spaceAfterTypeCast"));
    }

    public boolean useSingleClassImport() {
        return this.preferences.getBoolean("useSingleClassImport", FmtOptions.getDefaultAsBoolean("useSingleClassImport"));
    }

    public boolean useFQNs() {
        return this.preferences.getBoolean("useFQNs", FmtOptions.getDefaultAsBoolean("useFQNs"));
    }

    public boolean importInnerClasses() {
        return this.preferences.getBoolean("importInnerClasses", FmtOptions.getDefaultAsBoolean("importInnerClasses"));
    }

    public boolean preferStaticImports() {
        return this.preferences.getBoolean("preferStaticImports", FmtOptions.getDefaultAsBoolean("preferStaticImports"));
    }

    public int countForUsingStarImport() {
        boolean allow = this.preferences.getBoolean("allowConvertToStarImport", FmtOptions.getDefaultAsBoolean("allowConvertToStarImport"));
        return allow ? this.preferences.getInt("countForUsingStarImport", FmtOptions.getDefaultAsInt("countForUsingStarImport")) : Integer.MAX_VALUE;
    }

    public int countForUsingStaticStarImport() {
        boolean allow = this.preferences.getBoolean("allowConvertToStaticStarImport", FmtOptions.getDefaultAsBoolean("allowConvertToStaticStarImport"));
        return allow ? this.preferences.getInt("countForUsingStaticStarImport", FmtOptions.getDefaultAsInt("countForUsingStaticStarImport")) : Integer.MAX_VALUE;
    }

    public String[] getPackagesForStarImport() {
        String pkgs = this.preferences.get("packagesForStarImport", FmtOptions.getDefaultAsString("packagesForStarImport"));
        if (pkgs == null || pkgs.length() == 0) {
            return new String[0];
        }
        return pkgs.trim().split("\\s*[,;]\\s*");
    }

    public ImportGroups getImportGroups() {
        return new ImportGroups(this.preferences.get("importGroupsOrder", FmtOptions.getDefaultAsString("importGroupsOrder")));
    }

    public boolean separateImportGroups() {
        return this.preferences.getBoolean("separateImportGroups", FmtOptions.getDefaultAsBoolean("separateImportGroups"));
    }

    public boolean enableBlockCommentFormatting() {
        return this.preferences.getBoolean("enableCommentFormatting", FmtOptions.getDefaultAsBoolean("enableCommentFormatting")) && this.preferences.getBoolean("enableBlockCommentFormatting", FmtOptions.getDefaultAsBoolean("enableBlockCommentFormatting"));
    }

    public boolean enableJavadocFormatting() {
        return this.preferences.getBoolean("enableCommentFormatting", FmtOptions.getDefaultAsBoolean("enableCommentFormatting"));
    }

    public boolean wrapCommentText() {
        return this.preferences.getBoolean("wrapCommentText", FmtOptions.getDefaultAsBoolean("wrapCommentText"));
    }

    public boolean wrapOneLineComments() {
        return this.preferences.getBoolean("wrapOneLineComment", FmtOptions.getDefaultAsBoolean("wrapOneLineComment"));
    }

    public boolean preserveNewLinesInComments() {
        return this.preferences.getBoolean("preserveNewLinesInComments", FmtOptions.getDefaultAsBoolean("preserveNewLinesInComments"));
    }

    public boolean blankLineAfterJavadocDescription() {
        return this.preferences.getBoolean("blankLineAfterJavadocDescription", FmtOptions.getDefaultAsBoolean("blankLineAfterJavadocDescription"));
    }

    public boolean blankLineAfterJavadocParameterDescriptions() {
        return this.preferences.getBoolean("blankLineAfterJavadocParameterDescriptions", FmtOptions.getDefaultAsBoolean("blankLineAfterJavadocParameterDescriptions"));
    }

    public boolean blankLineAfterJavadocReturnTag() {
        return this.preferences.getBoolean("blankLineAfterJavadocReturnTag", FmtOptions.getDefaultAsBoolean("blankLineAfterJavadocReturnTag"));
    }

    public boolean generateParagraphTagOnBlankLines() {
        return this.preferences.getBoolean("generateParagraphTagOnBlankLines", FmtOptions.getDefaultAsBoolean("generateParagraphTagOnBlankLines"));
    }

    public boolean alignJavadocParameterDescriptions() {
        return this.preferences.getBoolean("alignJavadocParameterDescriptions", FmtOptions.getDefaultAsBoolean("alignJavadocParameterDescriptions"));
    }

    public boolean alignJavadocReturnDescription() {
        return this.preferences.getBoolean("alignJavadocReturnDescription", FmtOptions.getDefaultAsBoolean("alignJavadocReturnDescription"));
    }

    public boolean alignJavadocExceptionDescriptions() {
        return this.preferences.getBoolean("alignJavadocExceptionDescriptions", FmtOptions.getDefaultAsBoolean("alignJavadocExceptionDescriptions"));
    }

    static {
        FmtOptions.codeStyleProducer = new Producer();
    }

    private static class Producer
    implements FmtOptions.CodeStyleProducer {
        private Producer() {
        }

        @Override
        public CodeStyle create(Preferences preferences) {
            return new CodeStyle(preferences);
        }
    }

    public static final class MemberGroups {
        private Info[] infos;

        private MemberGroups(String groups, String visibility) {
            if (groups == null || groups.length() == 0) {
                this.infos = new Info[0];
            } else {
                String[] order = groups.trim().split("\\s*[,;]\\s*");
                String[] visibilityOrder = visibility != null ? visibility.trim().split("\\s*[,;]\\s*") : new String[1];
                this.infos = new Info[order.length * visibilityOrder.length];
                for (int i = 0; i < order.length; ++i) {
                    String o = order[i];
                    boolean isStatic = false;
                    if (o.startsWith("STATIC ")) {
                        isStatic = true;
                        o = o.substring(7);
                    }
                    ElementKind kind = ElementKind.valueOf(o);
                    for (int j = 0; j < visibilityOrder.length; ++j) {
                        int idx = i * visibilityOrder.length + j;
                        String vo = visibilityOrder[j];
                        Info info = new Info(idx);
                        info.ignoreVisibility = vo == null || !"DEFAULT".equals(vo);
                        info.mods = vo != null && !"DEFAULT".equals(vo) ? EnumSet.of(Modifier.valueOf(vo)) : EnumSet.noneOf(Modifier.class);
                        if (isStatic) {
                            info.mods.add(Modifier.STATIC);
                        }
                        info.kind = kind;
                        this.infos[idx] = info;
                    }
                }
            }
        }

        public int getGroupId(Tree tree) {
            ElementKind kind = ElementKind.OTHER;
            Set modifiers = null;
            switch (tree.getKind()) {
                case ANNOTATION_TYPE: 
                case CLASS: 
                case ENUM: 
                case INTERFACE: {
                    kind = ElementKind.CLASS;
                    modifiers = ((ClassTree)tree).getModifiers().getFlags();
                    break;
                }
                case METHOD: {
                    MethodTree mt = (MethodTree)tree;
                    kind = mt.getName().contentEquals("<init>") ? ElementKind.CONSTRUCTOR : ElementKind.METHOD;
                    modifiers = mt.getModifiers().getFlags();
                    break;
                }
                case VARIABLE: {
                    kind = ElementKind.FIELD;
                    modifiers = ((VariableTree)tree).getModifiers().getFlags();
                    break;
                }
                case BLOCK: {
                    kind = ((BlockTree)tree).isStatic() ? ElementKind.STATIC_INIT : ElementKind.INSTANCE_INIT;
                }
            }
            for (Info info : this.infos) {
                if (!info.check(kind, modifiers)) continue;
                return info.groupId;
            }
            return this.infos.length;
        }

        public int getGroupId(Element element) {
            int i$ = 0;
            Info[] arr$ = this.infos;
            int len$ = arr$.length;
            if (i$ < len$) {
                Info info = arr$[i$];
                ElementKind kind = element.getKind();
                if (kind == ElementKind.ANNOTATION_TYPE || kind == ElementKind.ENUM || kind == ElementKind.INSTANCE_INIT) {
                    kind = ElementKind.CLASS;
                }
                if (info.check(kind, element.getModifiers())) {
                    // empty if block
                }
                return info.groupId;
            }
            return this.infos.length;
        }

        private static final class Info {
            private int groupId;
            private boolean ignoreVisibility;
            private Set<Modifier> mods;
            private ElementKind kind;

            private Info(int id) {
                this.groupId = id;
            }

            private boolean check(ElementKind kind, Set<Modifier> modifiers) {
                if (this.kind != kind) {
                    return false;
                }
                if (modifiers == null || modifiers.isEmpty()) {
                    return this.mods.isEmpty();
                }
                if (!modifiers.containsAll(this.mods)) {
                    return false;
                }
                EnumSet<Modifier> copy = EnumSet.copyOf(modifiers);
                copy.removeAll(this.mods);
                copy.retainAll(this.ignoreVisibility ? EnumSet.of(Modifier.STATIC) : EnumSet.of(Modifier.STATIC, Modifier.PUBLIC, Modifier.PRIVATE, Modifier.PROTECTED));
                return copy.isEmpty();
            }
        }

    }

    public static final class ImportGroups {
        private Info[] infos;
        private boolean separateStatic;

        private ImportGroups(String groups) {
            if (groups == null || groups.length() == 0) {
                this.infos = new Info[0];
            } else {
                String[] order = groups.trim().split("\\s*[,;]\\s*");
                this.infos = new Info[order.length];
                for (int i = 0; i < order.length; ++i) {
                    String imp = order[i];
                    Info info = new Info(i);
                    if (imp.startsWith("static ")) {
                        info.isStatic = true;
                        this.separateStatic = true;
                        imp = imp.substring(7);
                    }
                    info.prefix = imp.length() > 0 && !"*".equals(imp) ? imp + '.' : "";
                    this.infos[i] = info;
                }
                Arrays.sort(this.infos, new Comparator<Info>(){

                    @Override
                    public int compare(Info o1, Info o2) {
                        int bal = o2.prefix.length() - o1.prefix.length();
                        return bal == 0 ? o1.prefix.compareTo(o2.prefix) : bal;
                    }
                });
            }
        }

        public int getGroupId(String name, boolean isStatic) {
            for (Info info : this.infos) {
                if (!(this.separateStatic ? info.check(name, isStatic) : info.check(name))) continue;
                return info.groupId;
            }
            return this.infos.length;
        }

        private static final class Info {
            private int groupId;
            private boolean isStatic;
            private String prefix;

            private Info(int id) {
                this.groupId = id;
            }

            private boolean check(String s) {
                return s.startsWith(this.prefix);
            }

            private boolean check(String s, boolean b) {
                return this.isStatic == b && this.check(s);
            }
        }

    }

    public static enum InsertionPoint {
        LAST_IN_CATEGORY,
        FIRST_IN_CATEGORY,
        ORDERED_IN_CATEGORY,
        CARET_LOCATION;
        

        private InsertionPoint() {
        }
    }

    public static enum WrapStyle {
        WRAP_ALWAYS,
        WRAP_IF_LONG,
        WRAP_NEVER;
        

        private WrapStyle() {
        }
    }

    public static enum BracesGenerationStyle {
        GENERATE,
        LEAVE_ALONE,
        ELIMINATE;
        

        private BracesGenerationStyle() {
        }
    }

    public static enum BracePlacement {
        SAME_LINE,
        NEW_LINE,
        NEW_LINE_HALF_INDENTED,
        NEW_LINE_INDENTED;
        

        private BracePlacement() {
        }
    }

}

