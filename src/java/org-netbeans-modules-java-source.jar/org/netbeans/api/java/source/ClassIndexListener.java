/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.java.source;

import java.util.EventListener;
import org.netbeans.api.java.source.RootsEvent;
import org.netbeans.api.java.source.TypesEvent;

public interface ClassIndexListener
extends EventListener {
    public void typesAdded(TypesEvent var1);

    public void typesRemoved(TypesEvent var1);

    public void typesChanged(TypesEvent var1);

    public void rootsAdded(RootsEvent var1);

    public void rootsRemoved(RootsEvent var1);
}

