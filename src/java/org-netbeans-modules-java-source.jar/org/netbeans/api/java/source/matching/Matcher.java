/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.util.TreePath
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Cancellable
 */
package org.netbeans.api.java.source.matching;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.util.TreePath;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.lang.model.element.Element;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.matching.Occurrence;
import org.netbeans.api.java.source.matching.Pattern;
import org.netbeans.modules.java.source.matching.CopyFinder;
import org.openide.util.Cancellable;

public class Matcher
implements Cancellable {
    private final CompilationInfo info;
    private AtomicBoolean givenCancel;
    private final AtomicBoolean privateCancel = new AtomicBoolean();
    private TreePath root;
    private Set<CopyFinder.Options> options = EnumSet.noneOf(CopyFinder.Options.class);
    private Map<String, TreePath> variables;
    private Map<String, Collection<? extends TreePath>> multiVariables;
    private Map<String, String> variables2Names;

    @NonNull
    public static Matcher create(@NonNull CompilationInfo info) {
        return new Matcher(info);
    }

    private Matcher(CompilationInfo info) {
        this.info = info;
        this.root = new TreePath(info.getCompilationUnit());
        this.options.add(CopyFinder.Options.ALLOW_GO_DEEPER);
    }

    @NonNull
    public Matcher setSearchRoot(@NonNull TreePath root) {
        this.root = root;
        return this;
    }

    @NonNull
    public Matcher setTreeTopSearch() {
        this.options.remove((Object)CopyFinder.Options.ALLOW_GO_DEEPER);
        return this;
    }

    @NonNull
    public Matcher setUntypedMatching() {
        this.options.add(CopyFinder.Options.NO_ELEMENT_VERIFY);
        return this;
    }

    @NonNull
    public Matcher setPresetVariable(@NonNull Map<String, TreePath> variables, @NonNull Map<String, Collection<? extends TreePath>> multiVariables, @NonNull Map<String, String> variables2Names) {
        this.variables = variables;
        this.multiVariables = multiVariables;
        this.variables2Names = variables2Names;
        return this;
    }

    @NonNull
    public Matcher setCancel(@NonNull AtomicBoolean cancel) {
        this.givenCancel = cancel;
        return this;
    }

    @NonNull
    public Collection<? extends Occurrence> match(Pattern pattern) {
        EnumSet<CopyFinder.Options> opts = EnumSet.noneOf(CopyFinder.Options.class);
        opts.addAll(this.options);
        if (pattern.variable2Type != null) {
            opts.add(CopyFinder.Options.ALLOW_VARIABLES_IN_PATTERN);
        }
        if (pattern.allowRemapToTrees) {
            opts.add(CopyFinder.Options.ALLOW_REMAP_VARIABLE_TO_EXPRESSION);
        }
        ArrayList<Occurrence> result = new ArrayList<Occurrence>();
        CopyFinder.State preinitializeState = this.variables != null ? CopyFinder.State.from(this.variables, this.multiVariables, this.variables2Names) : null;
        CopyFinder.Cancel cancel = new CopyFinder.Cancel(){

            @Override
            public boolean isCancelled() {
                return Matcher.this.privateCancel.get() || Matcher.this.givenCancel != null && Matcher.this.givenCancel.get();
            }
        };
        for (Map.Entry<TreePath, CopyFinder.VariableAssignments> e : CopyFinder.internalComputeDuplicates(this.info, pattern.pattern, this.root, preinitializeState, pattern.remappable, cancel, pattern.variable2Type, opts.toArray((T[])new CopyFinder.Options[opts.size()])).entrySet()) {
            result.add(new Occurrence(e.getKey(), e.getValue().variables, e.getValue().multiVariables, e.getValue().variables2Names, e.getValue().variablesRemapToElement, e.getValue().variablesRemapToTrees));
        }
        return Collections.unmodifiableCollection(result);
    }

    public boolean cancel() {
        this.privateCancel.set(true);
        return true;
    }

}

