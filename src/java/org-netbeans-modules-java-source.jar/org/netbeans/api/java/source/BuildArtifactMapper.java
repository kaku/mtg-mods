/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.api.java.source;

import java.io.File;
import java.net.URL;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.java.source.usages.BuildArtifactMapperImpl;

public class BuildArtifactMapper {
    public static void addArtifactsUpdatedListener(@NonNull URL sourceRoot, @NonNull ArtifactsUpdated listener) {
        BuildArtifactMapperImpl.addArtifactsUpdatedListener(sourceRoot, listener);
    }

    public static void removeArtifactsUpdatedListener(@NonNull URL sourceRoot, @NonNull ArtifactsUpdated listener) {
        BuildArtifactMapperImpl.removeArtifactsUpdatedListener(sourceRoot, listener);
    }

    public static interface ArtifactsUpdated {
        public void artifactsUpdated(@NonNull Iterable<File> var1);
    }

}

