/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.javadoc.Doc
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.Scope
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  com.sun.tools.javac.api.JavacScope
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.api.JavacTrees
 *  com.sun.tools.javac.code.Source
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.code.Symbol$MethodSymbol
 *  com.sun.tools.javac.code.Symbol$PackageSymbol
 *  com.sun.tools.javac.code.Symbol$TypeSymbol
 *  com.sun.tools.javac.code.Symbol$VarSymbol
 *  com.sun.tools.javac.code.Symtab
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.code.Type$ClassType
 *  com.sun.tools.javac.code.Type$MethodType
 *  com.sun.tools.javac.code.Types
 *  com.sun.tools.javac.comp.AttrContext
 *  com.sun.tools.javac.comp.Enter
 *  com.sun.tools.javac.comp.Env
 *  com.sun.tools.javac.comp.Resolve
 *  com.sun.tools.javac.model.JavacElements
 *  com.sun.tools.javac.model.JavacTypes
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.List
 *  com.sun.tools.javac.util.Name
 *  com.sun.tools.javac.util.Names
 *  com.sun.tools.javadoc.AnnotationTypeElementDocImpl
 *  com.sun.tools.javadoc.ClassDocImpl
 *  com.sun.tools.javadoc.ConstructorDocImpl
 *  com.sun.tools.javadoc.DocEnv
 *  com.sun.tools.javadoc.FieldDocImpl
 *  com.sun.tools.javadoc.MethodDocImpl
 *  com.sun.tools.javadoc.PackageDocImpl
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 */
package org.netbeans.api.java.source;

import com.sun.javadoc.Doc;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.Scope;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import com.sun.tools.javac.api.JavacScope;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.api.JavacTrees;
import com.sun.tools.javac.code.Source;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Symtab;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.comp.AttrContext;
import com.sun.tools.javac.comp.Enter;
import com.sun.tools.javac.comp.Env;
import com.sun.tools.javac.comp.Resolve;
import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.model.JavacTypes;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;
import com.sun.tools.javadoc.AnnotationTypeElementDocImpl;
import com.sun.tools.javadoc.ClassDocImpl;
import com.sun.tools.javadoc.ConstructorDocImpl;
import com.sun.tools.javadoc.DocEnv;
import com.sun.tools.javadoc.FieldDocImpl;
import com.sun.tools.javadoc.MethodDocImpl;
import com.sun.tools.javadoc.PackageDocImpl;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Set;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ExecutableType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.source.ClassIndex;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.modules.java.source.JavadocEnv;
import org.netbeans.modules.java.source.builder.ElementsService;
import org.netbeans.modules.java.source.parsing.CompilationInfoImpl;

public final class ElementUtilities {
    private final Context ctx;
    private final ElementsService delegate;
    private final CompilationInfo info;

    ElementUtilities(@NonNull CompilationInfo info) {
        this(info.impl.getJavacTask(), info);
    }

    ElementUtilities(@NonNull JavacTaskImpl jt) {
        this(jt, null);
    }

    private ElementUtilities(@NonNull JavacTaskImpl jt, @NullAllowed CompilationInfo info) {
        this.ctx = jt.getContext();
        this.delegate = ElementsService.instance(this.ctx);
        this.info = info;
    }

    public TypeElement enclosingTypeElement(Element element) throws IllegalArgumentException {
        return ElementUtilities.enclosingTypeElementImpl(element);
    }

    static TypeElement enclosingTypeElementImpl(Element element) throws IllegalArgumentException {
        if (element.getKind() == ElementKind.PACKAGE) {
            throw new IllegalArgumentException();
        }
        if ((element = element.getEnclosingElement()).getKind() == ElementKind.PACKAGE) {
            return null;
        }
        while (element != null && !element.getKind().isClass() && !element.getKind().isInterface()) {
            element = element.getEnclosingElement();
        }
        return (TypeElement)element;
    }

    public TypeElement outermostTypeElement(Element element) {
        return this.delegate.outermostTypeElement(element);
    }

    public Element getImplementationOf(ExecutableElement method, TypeElement origin) {
        return this.delegate.getImplementationOf(method, origin);
    }

    public boolean isSynthetic(Element element) {
        return (((Symbol)element).flags() & 4096) != 0 || (((Symbol)element).flags() & 0x1000000000L) != 0;
    }

    public boolean overridesMethod(ExecutableElement element) {
        return this.delegate.overridesMethod(element);
    }

    public static String getBinaryName(TypeElement element) throws IllegalArgumentException {
        if (element instanceof Symbol.TypeSymbol) {
            return ((Symbol.TypeSymbol)element).flatName().toString();
        }
        throw new IllegalArgumentException();
    }

    public Doc javaDocFor(Element element) {
        if (element != null) {
            DocEnv env = DocEnv.instance((Context)this.ctx);
            switch (element.getKind()) {
                case ANNOTATION_TYPE: 
                case CLASS: 
                case ENUM: 
                case INTERFACE: {
                    return env.getClassDoc((Symbol.ClassSymbol)element);
                }
                case ENUM_CONSTANT: 
                case FIELD: {
                    return env.getFieldDoc((Symbol.VarSymbol)element);
                }
                case METHOD: {
                    if (((Symbol.MethodSymbol)element).enclClass().getKind() == ElementKind.ANNOTATION_TYPE) {
                        return env.getAnnotationTypeElementDoc((Symbol.MethodSymbol)element);
                    }
                    return env.getMethodDoc((Symbol.MethodSymbol)element);
                }
                case CONSTRUCTOR: {
                    return env.getConstructorDoc((Symbol.MethodSymbol)element);
                }
                case PACKAGE: {
                    return env.getPackageDoc((Symbol.PackageSymbol)element);
                }
            }
        }
        return null;
    }

    public Element elementFor(Doc doc) {
        return doc instanceof JavadocEnv.ElementHolder ? ((JavadocEnv.ElementHolder)doc).getElement() : null;
    }

    public Iterable<? extends Element> getMembers(TypeMirror type, ElementAcceptor acceptor) {
        ArrayList<Element> members = new ArrayList<Element>();
        if (type != null) {
            JavacElements elements = JavacElements.instance((Context)this.ctx);
            JavacTypes types = JavacTypes.instance((Context)this.ctx);
            switch (type.getKind()) {
                case DECLARED: 
                case UNION: {
                    TypeElement te = (TypeElement)((DeclaredType)type).asElement();
                    if (te == null) break;
                    for (Element member : elements.getAllMembers(te)) {
                        if (acceptor != null && !acceptor.accept(member, type) || this.isHidden(member, members, (Elements)elements, (Types)types)) continue;
                        members.add(member);
                    }
                    if (te.getKind().isClass() || te.getKind().isInterface() && Source.instance((Context)this.ctx).allowDefaultMethods()) {
                        Symbol.VarSymbol thisPseudoMember = new Symbol.VarSymbol(262160, Names.instance((Context)this.ctx)._this, (Type)((Type.ClassType)te.asType()), (Symbol)((Symbol.ClassSymbol)te));
                        if (acceptor == null || acceptor.accept((Element)thisPseudoMember, type)) {
                            members.add((Element)thisPseudoMember);
                        }
                        if (te.getSuperclass().getKind() == TypeKind.DECLARED) {
                            Symbol.VarSymbol superPseudoMember = new Symbol.VarSymbol(262160, Names.instance((Context)this.ctx)._super, (Type)((Type.ClassType)te.getSuperclass()), (Symbol)((Symbol.ClassSymbol)te));
                            if (acceptor == null || acceptor.accept((Element)superPseudoMember, type)) {
                                members.add((Element)superPseudoMember);
                            }
                        }
                    }
                }
                case BOOLEAN: 
                case BYTE: 
                case CHAR: 
                case DOUBLE: 
                case FLOAT: 
                case INT: 
                case LONG: 
                case SHORT: 
                case VOID: {
                    Type t = Symtab.instance((Context)this.ctx).classType;
                    List typeargs = Source.instance((Context)this.ctx).allowGenerics() ? List.of((Object)((Type)type)) : List.nil();
                    t = new Type.ClassType(t.getEnclosingType(), typeargs, t.tsym);
                    Symbol.VarSymbol classPseudoMember = new Symbol.VarSymbol(25, Names.instance((Context)this.ctx)._class, t, (Symbol)((Type)type).tsym);
                    if (acceptor != null && !acceptor.accept((Element)classPseudoMember, type)) break;
                    members.add((Element)classPseudoMember);
                    break;
                }
                case ARRAY: {
                    for (Element member : elements.getAllMembers((TypeElement)((Type)type).tsym)) {
                        if (acceptor != null && !acceptor.accept(member, type)) continue;
                        members.add(member);
                    }
                    Type t = Symtab.instance((Context)this.ctx).classType;
                    List typeargs = Source.instance((Context)this.ctx).allowGenerics() ? List.of((Object)((Type)type)) : List.nil();
                    t = new Type.ClassType(t.getEnclosingType(), typeargs, t.tsym);
                    Symbol.VarSymbol classPseudoMember = new Symbol.VarSymbol(25, Names.instance((Context)this.ctx)._class, t, (Symbol)((Type)type).tsym);
                    if (acceptor != null && !acceptor.accept((Element)classPseudoMember, type)) break;
                    members.add((Element)classPseudoMember);
                }
            }
        }
        return members;
    }

    public Iterable<? extends Element> getLocalMembersAndVars(Scope scope, ElementAcceptor acceptor) {
        ArrayList<Element> members = new ArrayList<Element>();
        JavacElements elements = JavacElements.instance((Context)this.ctx);
        JavacTypes types = JavacTypes.instance((Context)this.ctx);
        while (scope != null) {
            TypeElement cls = scope.getEnclosingClass();
            if (cls != null) {
                for (Element local : scope.getLocalElements()) {
                    if (acceptor != null && !acceptor.accept(local, null) || this.isHidden(local, members, (Elements)elements, (Types)types)) continue;
                    members.add(local);
                }
                TypeMirror type = cls.asType();
                for (Element member : elements.getAllMembers(cls)) {
                    if (acceptor != null && !acceptor.accept(member, type) || this.isHidden(member, members, (Elements)elements, (Types)types)) continue;
                    members.add(member);
                }
            } else {
                for (Element local : scope.getLocalElements()) {
                    if (local.getKind().isClass() || local.getKind().isInterface() || acceptor != null && (local.getEnclosingElement() == null || !acceptor.accept(local, local.getEnclosingElement().asType())) || this.isHidden(local, members, (Elements)elements, (Types)types)) continue;
                    members.add(local);
                }
            }
            scope = scope.getEnclosingScope();
        }
        return members;
    }

    public Iterable<? extends Element> getLocalVars(Scope scope, ElementAcceptor acceptor) {
        ArrayList<Element> members = new ArrayList<Element>();
        JavacElements elements = JavacElements.instance((Context)this.ctx);
        JavacTypes types = JavacTypes.instance((Context)this.ctx);
        while (scope != null && scope.getEnclosingClass() != null) {
            for (Element local : scope.getLocalElements()) {
                if (acceptor != null && !acceptor.accept(local, null) || this.isHidden(local, members, (Elements)elements, (Types)types)) continue;
                members.add(local);
            }
            scope = scope.getEnclosingScope();
        }
        return members;
    }

    public Iterable<? extends TypeElement> getGlobalTypes(ElementAcceptor acceptor) {
        ArrayList<TypeElement> members = new ArrayList<TypeElement>();
        JavacTrees trees = JavacTrees.instance((Context)this.ctx);
        JavacElements elements = JavacElements.instance((Context)this.ctx);
        JavacTypes types = JavacTypes.instance((Context)this.ctx);
        for (CompilationUnitTree unit : Collections.singletonList(this.info.getCompilationUnit())) {
            Scope scope;
            TreePath path = new TreePath(unit);
            for (scope = trees.getScope((TreePath)path); scope != null && scope instanceof JavacScope && !((JavacScope)scope).isStarImportScope(); scope = scope.getEnclosingScope()) {
                for (Element local : scope.getLocalElements()) {
                    if (!local.getKind().isClass() && !local.getKind().isInterface() || this.isHidden(local, members, (Elements)elements, (Types)types) || acceptor != null && !acceptor.accept(local, null)) continue;
                    members.add((TypeElement)local);
                }
            }
            Element element = trees.getElement(path);
            if (element != null && element.getKind() == ElementKind.PACKAGE) {
                for (Element member : element.getEnclosedElements()) {
                    if (this.isHidden(member, members, (Elements)elements, (Types)types) || acceptor != null && !acceptor.accept(member, null)) continue;
                    members.add((TypeElement)member);
                }
            }
            while (scope != null) {
                for (Element local : scope.getLocalElements()) {
                    if (!local.getKind().isClass() && !local.getKind().isInterface() || this.isHidden(local, members, (Elements)elements, (Types)types) || acceptor != null && !acceptor.accept(local, null)) continue;
                    members.add((TypeElement)local);
                }
                scope = scope.getEnclosingScope();
            }
        }
        return members;
    }

    private boolean isHidden(Element member, java.util.List<? extends Element> members, Elements elements, Types types) {
        ListIterator<? extends Element> it = members.listIterator();
        while (it.hasNext()) {
            Element hider = it.next();
            if (hider == member) {
                return true;
            }
            if (hider.getSimpleName() != member.getSimpleName()) continue;
            if (elements.hides(member, hider)) {
                it.remove();
                continue;
            }
            TypeMirror memberType = member.asType();
            TypeMirror hiderType = hider.asType();
            if (memberType.getKind() == TypeKind.EXECUTABLE && hiderType.getKind() == TypeKind.EXECUTABLE) {
                if (!types.isSubsignature((ExecutableType)hiderType, (ExecutableType)memberType)) continue;
                return true;
            }
            return false;
        }
        return false;
    }

    public boolean isLocal(Element element) {
        return this.delegate.isLocal(element);
    }

    public boolean alreadyDefinedIn(CharSequence name, ExecutableType method, TypeElement enclClass) {
        return this.delegate.alreadyDefinedIn(name, method, enclClass);
    }

    public boolean isMemberOf(Element e, TypeElement type) {
        return this.delegate.isMemberOf(e, type);
    }

    public ExecutableElement getOverriddenMethod(ExecutableElement method) {
        return this.delegate.getOverriddenMethod(method);
    }

    public boolean implementsMethod(ExecutableElement element) {
        return this.delegate.implementsMethod(element);
    }

    public java.util.List<? extends ExecutableElement> findUnimplementedMethods(TypeElement impl) {
        return this.findUnimplementedMethods(impl, impl);
    }

    public boolean isErroneous(@NullAllowed Element e) {
        if (e == null) {
            return true;
        }
        TypeMirror type = e.asType();
        if (type == null) {
            return false;
        }
        if (type.getKind() == TypeKind.ERROR || type.getKind() == TypeKind.OTHER) {
            return true;
        }
        if (type instanceof Type && ((Type)type).isErroneous()) {
            return true;
        }
        return false;
    }

    public boolean isEffectivelyFinal(VariableElement e) {
        return (((Symbol)e).flags() & 2199023255568L) != 0;
    }

    @CheckForNull
    public Element findElement(@NonNull String description) {
        TypeElement el;
        if (description.contains("(")) {
            String methodFullName = description.substring(0, description.indexOf(40));
            String className = methodFullName.substring(0, methodFullName.lastIndexOf(46));
            TypeElement clazz = this.info.getElements().getTypeElement(className);
            if (clazz == null) {
                return null;
            }
            String methodSimpleName = methodFullName.substring(methodFullName.lastIndexOf(46) + 1);
            boolean constructor = clazz.getSimpleName().contentEquals(methodSimpleName);
            String parameters = description.substring(description.indexOf(40) + 1, description.lastIndexOf(41) + 1);
            int lastParamStart = 0;
            int angleDepth = 0;
            ArrayList<TypeMirror> types = new ArrayList<TypeMirror>();
            block6 : for (int paramIndex = 0; paramIndex < parameters.length(); ++paramIndex) {
                switch (parameters.charAt(paramIndex)) {
                    case '<': {
                        ++angleDepth;
                        continue block6;
                    }
                    case '>': {
                        --angleDepth;
                        continue block6;
                    }
                    case ',': {
                        if (angleDepth > 0) continue block6;
                    }
                    case ')': {
                        if (paramIndex <= lastParamStart) continue block6;
                        String type = parameters.substring(lastParamStart, paramIndex).replace("...", "[]");
                        types.add(this.info.getTypes().erasure(this.info.getTreeUtilities().parseType(type, this.info.getTopLevelElements().get(0))));
                        lastParamStart = paramIndex + 1;
                    }
                }
            }
            block7 : for (ExecutableElement ee : constructor ? ElementFilter.constructorsIn(clazz.getEnclosedElements()) : ElementFilter.methodsIn(clazz.getEnclosedElements())) {
                if (!constructor && !ee.getSimpleName().contentEquals(methodSimpleName) || ee.getParameters().size() != types.size()) continue;
                Iterator<? extends TypeMirror> real = ((ExecutableType)this.info.getTypes().erasure(ee.asType())).getParameterTypes().iterator();
                Iterator expected = types.iterator();
                while (real.hasNext() && expected.hasNext()) {
                    if (this.info.getTypes().isSameType(real.next(), (TypeMirror)expected.next())) continue;
                    continue block7;
                }
                assert (real.hasNext() == expected.hasNext());
                return ee;
            }
        }
        if ((el = this.info.getElements().getTypeElement(description)) != null) {
            return el;
        }
        int dot = description.lastIndexOf(46);
        if (dot != -1) {
            String simpleName = description.substring(dot + 1);
            el = this.info.getElements().getTypeElement(description.substring(0, dot));
            if (el != null) {
                for (VariableElement var : ElementFilter.fieldsIn(el.getEnclosedElements())) {
                    if (!var.getSimpleName().contentEquals(simpleName)) continue;
                    return var;
                }
            }
        }
        return null;
    }

    private java.util.List<? extends ExecutableElement> findUnimplementedMethods(TypeElement impl, TypeElement element) {
        ArrayList<ExecutableElement> undef = new ArrayList<ExecutableElement>();
        JavacTypes types = JavacTypes.instance((Context)this.ctx);
        com.sun.tools.javac.code.Types implTypes = com.sun.tools.javac.code.Types.instance((Context)this.ctx);
        DeclaredType implType = (DeclaredType)impl.asType();
        if (element.getKind().isInterface() || element.getModifiers().contains((Object)Modifier.ABSTRACT)) {
            for (Element e : element.getEnclosedElements()) {
                Element eeImpl;
                ExecutableElement ee;
                if (e.getKind() != ElementKind.METHOD || !e.getModifiers().contains((Object)Modifier.ABSTRACT) || (eeImpl = this.getImplementationOf(ee = (ExecutableElement)e, impl)) != null && (eeImpl != ee || impl == element) || implTypes.asSuper((Type)implType, (Symbol)ee.getEnclosingElement()) == null) continue;
                undef.add(ee);
            }
        }
        for (TypeMirror t : types.directSupertypes(element.asType())) {
            for (ExecutableElement ee : this.findUnimplementedMethods(impl, (TypeElement)((DeclaredType)t).asElement())) {
                boolean exists = false;
                ExecutableType eeType = (ExecutableType)types.asMemberOf(implType, ee);
                for (ExecutableElement existing : undef) {
                    TypeMirror eeReturnType;
                    ExecutableType existingType;
                    if (!existing.getSimpleName().contentEquals(ee.getSimpleName()) || !types.isSubsignature(existingType = (ExecutableType)types.asMemberOf(implType, existing), eeType)) continue;
                    TypeMirror existingReturnType = existingType.getReturnType();
                    if (!types.isSubtype(existingReturnType, eeReturnType = eeType.getReturnType())) {
                        if (types.isSubtype(eeReturnType, existingReturnType)) {
                            undef.remove(existing);
                            undef.add(ee);
                        } else if (existingReturnType.getKind() == TypeKind.DECLARED && eeReturnType.getKind() == TypeKind.DECLARED) {
                            DeclaredType subType;
                            Env env = Enter.instance((Context)this.ctx).getClassEnv((Symbol.TypeSymbol)impl);
                            DeclaredType declaredType = subType = env != null ? this.findCommonSubtype((DeclaredType)existingReturnType, (DeclaredType)eeReturnType, env) : null;
                            if (subType != null) {
                                Type mt;
                                undef.remove(existing);
                                Symbol.MethodSymbol ms = ((Symbol.MethodSymbol)existing).clone((Symbol)impl);
                                ms.type = mt = implTypes.createMethodTypeWithReturn((Type)((Type.MethodType)ms.type), (Type)subType);
                                undef.add((ExecutableElement)ms);
                            }
                        }
                    }
                    exists = true;
                    break;
                }
                if (exists) continue;
                undef.add(ee);
            }
        }
        return undef;
    }

    private DeclaredType findCommonSubtype(DeclaredType type1, DeclaredType type2, Env<AttrContext> env) {
        java.util.List<DeclaredType> subtypes1 = this.getSubtypes(type1, env);
        java.util.List<DeclaredType> subtypes2 = this.getSubtypes(type2, env);
        if (subtypes1 == null || subtypes2 == null) {
            return null;
        }
        Types types = this.info.getTypes();
        for (DeclaredType subtype1 : subtypes1) {
            for (DeclaredType subtype2 : subtypes2) {
                if (types.isSubtype(subtype1, subtype2)) {
                    return subtype1;
                }
                if (!types.isSubtype(subtype2, subtype1)) continue;
                return subtype2;
            }
        }
        return null;
    }

    private java.util.List<DeclaredType> getSubtypes(DeclaredType baseType, Env<AttrContext> env) {
        LinkedList<DeclaredType> subtypes = new LinkedList<DeclaredType>();
        HashSet<TypeElement> elems = new HashSet<TypeElement>();
        LinkedList<DeclaredType> bases = new LinkedList<DeclaredType>();
        bases.add(baseType);
        ClassIndex index = this.info.getClasspathInfo().getClassIndex();
        Trees trees = this.info.getTrees();
        Types types = this.info.getTypes();
        Resolve resolve = Resolve.instance((Context)this.ctx);
        while (!bases.isEmpty()) {
            DeclaredType head = (DeclaredType)bases.remove();
            TypeElement elem = (TypeElement)head.asElement();
            if (!elems.add(elem)) continue;
            subtypes.add(head);
            java.util.List<? extends TypeMirror> tas = head.getTypeArguments();
            boolean isRaw = !tas.iterator().hasNext();
            Set<ElementHandle<TypeElement>> implementors = index.getElements(ElementHandle.create(elem), EnumSet.of(ClassIndex.SearchKind.IMPLEMENTORS), EnumSet.allOf(ClassIndex.SearchScope.class));
            if (implementors == null) {
                return null;
            }
            block1 : for (ElementHandle<TypeElement> eh : implementors) {
                TypeElement e = eh.resolve(this.info);
                if (e == null || !resolve.isAccessible(env, (Symbol.TypeSymbol)e)) continue;
                if (isRaw) {
                    DeclaredType dt = types.getDeclaredType(e, new TypeMirror[0]);
                    bases.add(dt);
                    continue;
                }
                HashMap<Element, TypeMirror> map = new HashMap<Element, TypeMirror>();
                TypeMirror sup = e.getSuperclass();
                if (sup.getKind() == TypeKind.DECLARED && ((DeclaredType)sup).asElement() == elem) {
                    DeclaredType dt = (DeclaredType)sup;
                    Iterator<? extends TypeMirror> ittas = tas.iterator();
                    Iterator<? extends TypeMirror> it = dt.getTypeArguments().iterator();
                    while (it.hasNext() && ittas.hasNext()) {
                        TypeMirror stm;
                        TypeMirror basetm = ittas.next();
                        if (basetm == (stm = it.next())) continue;
                        if (stm.getKind() != TypeKind.TYPEVAR) continue block1;
                        map.put(((TypeVariable)stm).asElement(), basetm);
                    }
                    if (it.hasNext() != ittas.hasNext()) {
                        continue;
                    }
                } else {
                    for (TypeMirror tm : e.getInterfaces()) {
                        if (((DeclaredType)tm).asElement() != elem) continue;
                        DeclaredType dt = (DeclaredType)tm;
                        Iterator<? extends TypeMirror> ittas = tas.iterator();
                        Iterator<? extends TypeMirror> it = dt.getTypeArguments().iterator();
                        while (it.hasNext() && ittas.hasNext()) {
                            TypeMirror stm;
                            TypeMirror basetm = ittas.next();
                            if (basetm == (stm = it.next())) continue;
                            if (stm.getKind() != TypeKind.TYPEVAR) continue block1;
                            map.put(((TypeVariable)stm).asElement(), basetm);
                        }
                        if (it.hasNext() != ittas.hasNext()) continue block1;
                    }
                }
                bases.add(this.getDeclaredType(e, map, types));
            }
        }
        return subtypes;
    }

    private DeclaredType getDeclaredType(TypeElement e, HashMap<? extends Element, ? extends TypeMirror> map, Types types) {
        java.util.List<? extends TypeParameterElement> tpes = e.getTypeParameters();
        TypeMirror[] targs = new TypeMirror[tpes.size()];
        int i = 0;
        for (TypeParameterElement tpe : tpes) {
            TypeMirror t = map.get(tpe);
            targs[i++] = t != null ? t : tpe.asType();
        }
        Element encl = e.getEnclosingElement();
        if ((encl.getKind().isClass() || encl.getKind().isInterface()) && !((TypeElement)encl).getTypeParameters().isEmpty()) {
            return types.getDeclaredType(this.getDeclaredType((TypeElement)encl, map, types), e, targs);
        }
        return types.getDeclaredType(e, targs);
    }

    public static interface ElementAcceptor {
        public boolean accept(Element var1, TypeMirror var2);
    }

}

