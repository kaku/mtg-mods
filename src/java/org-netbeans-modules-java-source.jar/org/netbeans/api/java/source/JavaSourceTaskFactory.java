/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.parsing.spi.TaskIndexingMode
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.api.java.source;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.CancellableTask;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.modules.java.source.JavaSourceAccessor;
import org.netbeans.modules.java.source.JavaSourceTaskFactoryManager;
import org.netbeans.modules.parsing.spi.TaskIndexingMode;
import org.openide.filesystems.FileObject;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;

public abstract class JavaSourceTaskFactory {
    private static final Logger LOG = Logger.getLogger(JavaSourceTaskFactory.class.getName());
    static final String BEFORE_ADDING_REMOVING_TASKS = "beforeAddingRemovingTasks";
    static final String FILEOBJECTS_COMPUTATION = "fileObjectsComputation";
    private final JavaSource.Phase phase;
    private final JavaSource.Priority priority;
    private final TaskIndexingMode taskIndexingMode;
    static boolean SYNCHRONOUS_EVENTS = false;
    private final Map<FileObject, CancellableTask<CompilationInfo>> file2Task;
    private final Map<FileObject, JavaSource> file2JS;
    private final Object filesLock = new Object();
    private static RequestProcessor WORKER = new RequestProcessor("JavaSourceTaskFactory", 1, false, false);
    static Accessor2 ACCESSOR2;

    protected JavaSourceTaskFactory(@NonNull JavaSource.Phase phase, @NonNull JavaSource.Priority priority) {
        this.phase = phase;
        this.priority = priority;
        this.taskIndexingMode = TaskIndexingMode.DISALLOWED_DURING_SCAN;
        this.file2Task = new HashMap<FileObject, CancellableTask<CompilationInfo>>();
        this.file2JS = new HashMap<FileObject, JavaSource>();
    }

    protected JavaSourceTaskFactory(@NonNull JavaSource.Phase phase, @NonNull JavaSource.Priority priority, @NonNull TaskIndexingMode taskIndexingMode) {
        Parameters.notNull((CharSequence)"phase", (Object)((Object)phase));
        Parameters.notNull((CharSequence)"priority", (Object)((Object)priority));
        Parameters.notNull((CharSequence)"taskIndexingMode", (Object)taskIndexingMode);
        this.phase = phase;
        this.priority = priority;
        this.taskIndexingMode = taskIndexingMode;
        this.file2Task = new HashMap<FileObject, CancellableTask<CompilationInfo>>();
        this.file2JS = new HashMap<FileObject, JavaSource>();
    }

    @NonNull
    protected abstract CancellableTask<CompilationInfo> createTask(FileObject var1);

    @NonNull
    protected abstract Collection<FileObject> getFileObjects();

    protected final void fileObjectsChanged() {
        LOG.log(Level.FINEST, "fileObjectsComputation");
        final ArrayList<FileObject> currentFiles = new ArrayList<FileObject>(this.getFileObjects());
        if (SYNCHRONOUS_EVENTS) {
            this.stateChangedImpl(currentFiles);
        } else {
            WORKER.post(new Runnable(){

                @Override
                public void run() {
                    JavaSourceTaskFactory.this.stateChangedImpl(currentFiles);
                }
            });
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void stateChangedImpl(List<FileObject> currentFiles) {
        LOG.log(Level.FINEST, "beforeAddingRemovingTasks");
        Object object = this.filesLock;
        synchronized (object) {
            ArrayList<FileObject> addedFiles = new ArrayList<FileObject>(currentFiles);
            ArrayList<FileObject> removedFiles = new ArrayList<FileObject>(this.file2Task.keySet());
            addedFiles.removeAll(this.file2Task.keySet());
            removedFiles.removeAll(currentFiles);
            for (FileObject r : removedFiles) {
                JavaSource source = this.file2JS.remove((Object)r);
                if (source == null) continue;
                ACCESSOR2.removePhaseCompletionTask(source, this.file2Task.remove((Object)r));
            }
            for (FileObject a : addedFiles) {
                JavaSource js;
                if (a == null || !a.isValid() || (js = JavaSource.forFileObject(a)) == null) continue;
                CancellableTask<CompilationInfo> task = this.createTask(a);
                if (task == null) {
                    throw new IllegalStateException("createTask(FileObject) returned null for factory: " + this.getClass().getName());
                }
                ACCESSOR2.addPhaseCompletionTask(js, task, this.phase, this.priority, this.taskIndexingMode);
                this.file2Task.put(a, task);
                this.file2JS.put(a, js);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected final void reschedule(FileObject file) throws IllegalArgumentException {
        Object object = this.filesLock;
        synchronized (object) {
            JavaSource source = this.file2JS.get((Object)file);
            if (source == null) {
                return;
            }
            CancellableTask<CompilationInfo> task = this.file2Task.get((Object)file);
            if (task == null) {
                return;
            }
            ACCESSOR2.rescheduleTask(source, task);
        }
    }

    static {
        JavaSourceTaskFactoryManager.ACCESSOR = new JavaSourceTaskFactoryManager.Accessor(){

            @Override
            public void fireChangeEvent(JavaSourceTaskFactory f) {
                f.fileObjectsChanged();
            }
        };
        ACCESSOR2 = new Accessor2(){

            @Override
            public void addPhaseCompletionTask(JavaSource js, CancellableTask<CompilationInfo> task, JavaSource.Phase phase, JavaSource.Priority priority, TaskIndexingMode taskIndexingMode) {
                JavaSourceAccessor.getINSTANCE().addPhaseCompletionTask(js, task, phase, priority, taskIndexingMode);
            }

            @Override
            public void removePhaseCompletionTask(JavaSource js, CancellableTask<CompilationInfo> task) {
                JavaSourceAccessor.getINSTANCE().removePhaseCompletionTask(js, task);
            }

            @Override
            public void rescheduleTask(JavaSource js, CancellableTask<CompilationInfo> task) {
                JavaSourceAccessor.getINSTANCE().rescheduleTask(js, task);
            }
        };
    }

    static interface Accessor2 {
        public void addPhaseCompletionTask(JavaSource var1, CancellableTask<CompilationInfo> var2, JavaSource.Phase var3, JavaSource.Priority var4, TaskIndexingMode var5);

        public void removePhaseCompletionTask(JavaSource var1, CancellableTask<CompilationInfo> var2);

        public void rescheduleTask(JavaSource var1, CancellableTask<CompilationInfo> var2);
    }

}

