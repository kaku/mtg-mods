/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.Scope
 *  com.sun.source.tree.Tree
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.code.Scope
 *  com.sun.tools.javac.code.Scope$ImportScope
 *  com.sun.tools.javac.code.Scope$StarImportScope
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.code.Symbol$MethodSymbol
 *  com.sun.tools.javac.code.Symbol$PackageSymbol
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.code.Type$CapturedType
 *  com.sun.tools.javac.code.Type$TypeVar
 *  com.sun.tools.javac.code.Type$WildcardType
 *  com.sun.tools.javac.code.Types
 *  com.sun.tools.javac.comp.Check
 *  com.sun.tools.javac.model.FilteredMemberList
 *  com.sun.tools.javac.model.JavacElements
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCCompilationUnit
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.JCDiagnostic
 *  com.sun.tools.javac.util.JCDiagnostic$DiagnosticPosition
 *  com.sun.tools.javac.util.List
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.queries.SourceForBinaryQuery
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.modules.java.preprocessorbridge.spi.ImportProcessor
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.api.indexing.IndexingManager
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Pair
 *  org.openide.util.Parameters
 *  org.openide.util.Utilities
 */
package org.netbeans.api.java.source;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.code.Scope;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.comp.Check;
import com.sun.tools.javac.model.FilteredMemberList;
import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.JCDiagnostic;
import com.sun.tools.javac.util.List;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.processing.Completion;
import javax.annotation.processing.Processor;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.QualifiedNameable;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.WildcardType;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.ElementScanner6;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.swing.SwingUtilities;
import javax.swing.text.Document;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.queries.SourceForBinaryQuery;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CodeStyle;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.java.source.GeneratorUtilities;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.api.java.source.matching.Matcher;
import org.netbeans.api.java.source.matching.Occurrence;
import org.netbeans.api.java.source.matching.Pattern;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.java.preprocessorbridge.spi.ImportProcessor;
import org.netbeans.modules.java.source.ElementHandleAccessor;
import org.netbeans.modules.java.source.JavadocHelper;
import org.netbeans.modules.java.source.indexing.JavaCustomIndexer;
import org.netbeans.modules.java.source.parsing.ClasspathInfoProvider;
import org.netbeans.modules.java.source.parsing.CompilationInfoImpl;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.save.DiffContext;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;
import org.netbeans.modules.java.source.usages.ClassIndexManager;
import org.netbeans.modules.java.source.usages.ClasspathInfoAccessor;
import org.netbeans.modules.java.source.usages.ExecutableFilesIndex;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.api.indexing.IndexingManager;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.indexing.support.QuerySupport;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Pair;
import org.openide.util.Parameters;
import org.openide.util.Utilities;

public class SourceUtils {
    private static final Logger LOG = Logger.getLogger(SourceUtils.class.getName());
    private static final Map<URI, Integer> jdocCache = new ConcurrentHashMap<URI, Integer>();
    private static final Set<String> docLet1 = Collections.unmodifiableSet(new HashSet<String>(Arrays.asList("constructor_summary", "method_summary", "field_detail", "constructor_detail", "method_detail")));
    private static final Set<String> docLet2 = Collections.unmodifiableSet(new HashSet<String>(Arrays.asList("constructor.summary", "method.summary", "field.detail", "constructor.detail", "method.detail")));
    private static final int MAX_LEN = 6;
    private static final char[] VOWELS = new char[]{'a', 'e', 'i', 'o', 'u', 'y', '\u00e9', '\u00ea', '\u00e8', '\u00e1', '\u00e2', '\u00e6', '\u00e0', '\u03b1', '\u00e3', '\u00e5', '\u00e4', '\u00eb', '\u00f3', '\u00f4', '\u0153', '\u00f2', '\u03bf', '\u00f5', '\u00f6', '\u00ed', '\u00ee', '\u00ec', '\u03b9', '\u00ef', '\u00fa', '\u00fb', '\u00f9', '\u03d2', '\u03c5', '\u00fc', '\u0430', '\u043e', '\u044f', '\u0438', '\u0439', '\u0435', '\u044b', '\u044d', '\u0443', '\u044e'};

    private SourceUtils() {
    }

    public static TokenSequence<JavaTokenId> getJavaTokenSequence(TokenHierarchy hierarchy, int offset) {
        if (hierarchy != null) {
            for (TokenSequence ts = hierarchy.tokenSequence(); ts != null && (offset == 0 || ts.moveNext()); ts = ts.embedded()) {
                ts.move(offset);
                if (ts.language() == JavaTokenId.language()) {
                    return ts;
                }
                if (ts.moveNext() || ts.movePrevious()) continue;
                return null;
            }
        }
        return null;
    }

    public static Set<TreePath> computeDuplicates(CompilationInfo info, TreePath searchingFor, TreePath scope, AtomicBoolean cancel) {
        HashSet<TreePath> result = new HashSet<TreePath>();
        for (Occurrence od : Matcher.create(info).setCancel(cancel).setSearchRoot(scope).match(Pattern.createSimplePattern(searchingFor))) {
            result.add(od.getOccurrenceRoot());
        }
        return result;
    }

    public static boolean checkTypesAssignable(CompilationInfo info, TypeMirror from, TypeMirror to) {
        Context c = info.impl.getJavacTask().getContext();
        if (from.getKind() == TypeKind.TYPEVAR) {
            com.sun.tools.javac.code.Types types = com.sun.tools.javac.code.Types.instance((Context)c);
            Type.TypeVar t = types.substBound((Type.TypeVar)from, List.of((Object)((Type)from)), List.of((Object)types.boxedTypeOrType((Type)to)));
            return info.getTypes().isAssignable((TypeMirror)t.getUpperBound(), to) || info.getTypes().isAssignable(to, (TypeMirror)t.getUpperBound());
        }
        if (from.getKind() == TypeKind.WILDCARD) {
            from = com.sun.tools.javac.code.Types.instance((Context)c).wildUpperBound((Type)from);
        }
        return Check.instance((Context)c).checkType(null, (Type)from, (Type)to).getKind() != TypeKind.ERROR;
    }

    public static TypeMirror getBound(WildcardType wildcardType) {
        Type.TypeVar bound = ((Type.WildcardType)wildcardType).bound;
        return bound != null ? bound.bound : null;
    }

    public static java.util.List<? extends Completion> getAttributeValueCompletions(CompilationInfo info, Element element, AnnotationMirror annotation, ExecutableElement member, String userText) {
        LinkedList<Completion> completions = new LinkedList<Completion>();
        if (info.getPhase().compareTo(JavaSource.Phase.ELEMENTS_RESOLVED) >= 0) {
            String fqn = ((TypeElement)annotation.getAnnotationType().asElement()).getQualifiedName().toString();
            Iterable processors = info.impl.getJavacTask().getProcessors();
            if (processors != null) {
                for (Processor processor : processors) {
                    boolean match = false;
                    for (String sat : processor.getSupportedAnnotationTypes()) {
                        if ("*".equals(sat)) {
                            match = true;
                            break;
                        }
                        if (sat.endsWith(".*")) {
                            sat = sat.substring(0, sat.length() - 1);
                            if (!fqn.startsWith(sat)) continue;
                            match = true;
                            break;
                        }
                        if (!fqn.equals(sat)) continue;
                        match = true;
                        break;
                    }
                    if (!match) continue;
                    try {
                        for (Completion c : processor.getCompletions(element, annotation, member, userText)) {
                            completions.add(c);
                        }
                        continue;
                    }
                    catch (Exception e) {
                        Logger.getLogger(processor.getClass().getName()).log(Level.INFO, e.getMessage(), e);
                        continue;
                    }
                }
            }
        }
        return completions;
    }

    @Deprecated
    public static TypeElement getEnclosingTypeElement(Element element) throws IllegalArgumentException {
        return ElementUtilities.enclosingTypeElementImpl(element);
    }

    public static TypeElement getOutermostEnclosingTypeElement(Element element) {
        Element ec = SourceUtils.getEnclosingTypeElement(element);
        if (ec == null) {
            ec = element;
        }
        while (ec.getEnclosingElement().getKind().isClass() || ec.getEnclosingElement().getKind().isInterface()) {
            ec = ec.getEnclosingElement();
        }
        return (TypeElement)ec;
    }

    @NonNull
    public static String[] getJVMSignature(@NonNull ElementHandle<?> handle) {
        Parameters.notNull((CharSequence)"handle", handle);
        return ElementHandleAccessor.getInstance().getJVMSignature(handle);
    }

    public static String resolveImport(final CompilationInfo info, TreePath context, String fqn) throws NullPointerException, IOException {
        if (info == null) {
            throw new NullPointerException();
        }
        if (context == null) {
            throw new NullPointerException();
        }
        if (fqn == null) {
            throw new NullPointerException();
        }
        CodeStyle cs = DiffContext.getCodeStyle(info);
        if (cs.useFQNs()) {
            return fqn;
        }
        CompilationUnitTree cut = info.getCompilationUnit();
        final Trees trees = info.getTrees();
        final com.sun.source.tree.Scope scope = trees.getScope(context);
        String qName = fqn;
        StringBuilder sqName = new StringBuilder();
        boolean clashing = false;
        ElementUtilities eu = info.getElementUtilities();
        ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

            @Override
            public boolean accept(Element e, TypeMirror type) {
                return (e.getKind().isClass() || e.getKind().isInterface()) && trees.isAccessible(scope, (TypeElement)e);
            }
        };
        Element toImport = null;
        while (qName != null && qName.length() > 0) {
            int lastDot = qName.lastIndexOf(46);
            Element element = info.getElements().getTypeElement(qName);
            if (element != null) {
                clashing = false;
                String simple = qName.substring(lastDot < 0 ? 0 : lastDot + 1);
                if (sqName.length() > 0) {
                    sqName.insert(0, '.');
                }
                sqName.insert(0, simple);
                if (cs.useSingleClassImport() && (toImport == null || !cs.importInnerClasses())) {
                    toImport = element;
                }
                boolean matchFound = false;
                for (Element e2 : eu.getLocalMembersAndVars(scope, acceptor)) {
                    if (!simple.contentEquals(e2.getSimpleName())) continue;
                    if (qName.contentEquals(((TypeElement)e2).getQualifiedName())) {
                        return sqName.toString();
                    }
                    clashing = true;
                    matchFound = true;
                    break;
                }
                if (!matchFound) {
                    for (TypeElement e : eu.getGlobalTypes(acceptor)) {
                        if (!simple.contentEquals(e.getSimpleName())) continue;
                        if (qName.contentEquals(e.getQualifiedName())) {
                            return sqName.toString();
                        }
                        clashing = true;
                        break;
                    }
                }
                if (cs.importInnerClasses()) {
                    break;
                }
            } else {
                element = info.getElements().getPackageElement(qName);
                if (element != null) {
                    if (toImport != null && !GeneratorUtilities.checkPackagesForStarImport(qName, cs)) break;
                    toImport = element;
                    break;
                }
            }
            qName = lastDot < 0 ? null : qName.substring(0, lastDot);
        }
        if (clashing || toImport == null) {
            return fqn;
        }
        String topLevelLanguageMIMEType = info.getFileObject().getMIMEType();
        if ("text/x-java".equals(topLevelLanguageMIMEType)) {
            Scope.StarImportScope importScope;
            if (info instanceof WorkingCopy) {
                CompilationUnitTree nue = (CompilationUnitTree)((WorkingCopy)info).resolveRewriteTarget((Tree)cut);
                ((WorkingCopy)info).rewrite((Tree)info.getCompilationUnit(), (Tree)GeneratorUtilities.get((WorkingCopy)info).addImports(nue, Collections.singleton(toImport)));
            } else {
                final ElementHandle<Object> handle = ElementHandle.create(toImport);
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        try {
                            ModificationResult.runModificationTask(Collections.singletonList(info.getSnapshot().getSource()), new UserTask(){

                                public void run(ResultIterator resultIterator) throws Exception {
                                    WorkingCopy copy = WorkingCopy.get(resultIterator.getParserResult());
                                    copy.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                                    Object elementToImport = handle.resolve(copy);
                                    copy.rewrite((Tree)copy.getCompilationUnit(), (Tree)GeneratorUtilities.get(copy).addImports(copy.getCompilationUnit(), Collections.singleton(elementToImport)));
                                }
                            }).commit();
                        }
                        catch (Exception e) {
                            Exceptions.printStackTrace((Throwable)e);
                        }
                    }

                });
            }
            JCTree.JCCompilationUnit unit = (JCTree.JCCompilationUnit)info.getCompilationUnit();
            if (toImport.getKind() == ElementKind.PACKAGE) {
                importScope = new Scope.StarImportScope(unit.starImportScope.owner);
                importScope.importAll((Scope)unit.starImportScope);
                importScope.importAll(((Symbol.PackageSymbol)toImport).members());
                unit.starImportScope = importScope;
            } else {
                importScope = new Scope.ImportScope(unit.namedImportScope.owner);
                for (Symbol symbol : unit.namedImportScope.getElements()) {
                    importScope.enter(symbol);
                }
                importScope.enterIfAbsent((Symbol)toImport);
                unit.namedImportScope = importScope;
            }
        } else {
            Lookup lookup = MimeLookup.getLookup((MimePath)MimePath.get((String)topLevelLanguageMIMEType));
            Collection instances = lookup.lookupAll(ImportProcessor.class);
            for (ImportProcessor importsProcesor : instances) {
                importsProcesor.addImport(info.getDocument(), fqn);
            }
        }
        return sqName.toString();
    }

    public static FileObject getFile(Element element, ClasspathInfo cpInfo) {
        Element prev;
        Parameters.notNull((CharSequence)"element", (Object)element);
        Parameters.notNull((CharSequence)"cpInfo", (Object)cpInfo);
        Element element2 = prev = element.getKind() == ElementKind.PACKAGE ? element : null;
        while (element.getKind() != ElementKind.PACKAGE) {
            prev = element;
            element = element.getEnclosingElement();
        }
        ElementKind kind = prev.getKind();
        if (!kind.isClass() && !kind.isInterface() && kind != ElementKind.PACKAGE) {
            return null;
        }
        ElementHandle<Element> handle = ElementHandle.create(prev);
        return SourceUtils.getFile(handle, cpInfo);
    }

    public static FileObject getFile(ElementHandle<? extends Element> handle, ClasspathInfo cpInfo) {
        Parameters.notNull((CharSequence)"handle", handle);
        Parameters.notNull((CharSequence)"cpInfo", (Object)cpInfo);
        try {
            String pkgName;
            boolean pkg = handle.getKind() == ElementKind.PACKAGE;
            String[] signature = handle.getSignature();
            assert (signature.length >= 1);
            ClassPath[] cps = new ClassPath[]{cpInfo.getClassPath(ClasspathInfo.PathKind.SOURCE), SourceUtils.createClassPath(cpInfo, ClasspathInfo.PathKind.OUTPUT), SourceUtils.createClassPath(cpInfo, ClasspathInfo.PathKind.BOOT), SourceUtils.createClassPath(cpInfo, ClasspathInfo.PathKind.COMPILE)};
            String className = null;
            if (pkg) {
                pkgName = FileObjects.convertPackage2Folder(signature[0]);
            } else {
                int index = signature[0].lastIndexOf(46);
                if (index < 0) {
                    pkgName = "";
                    className = signature[0];
                } else {
                    pkgName = FileObjects.convertPackage2Folder(signature[0].substring(0, index));
                    className = signature[0].substring(index + 1);
                }
            }
            java.util.List<Pair<FileObject, ClassPath>> fos = SourceUtils.findAllResources(pkgName, cps);
            for (Pair<FileObject, ClassPath> pair : fos) {
                FileObject foundFo;
                FileObject root = ((ClassPath)pair.second()).findOwnerRoot((FileObject)pair.first());
                if (root == null) continue;
                FileObject[] sourceRoots = SourceForBinaryQuery.findSourceRoots((URL)root.toURL()).getRoots();
                ClassPath sourcePath = ClassPathSupport.createClassPath((FileObject[])sourceRoots);
                LinkedList<FileObject> folders = new LinkedList<FileObject>(sourcePath.findAllResources(pkgName));
                if (pkg) {
                    return folders.isEmpty() ? (FileObject)pair.first() : (FileObject)folders.get(0);
                }
                boolean caseSensitive = SourceUtils.isCaseSensitive();
                Object fnames = SourceUtils.getSourceFileNames(className);
                folders.addFirst((FileObject)pair.first());
                if (fnames instanceof String) {
                    FileObject match = SourceUtils.findMatchingChild((String)fnames, folders, caseSensitive);
                    if (match != null) {
                        return match;
                    }
                } else {
                    for (String candidate : (java.util.List)fnames) {
                        FileObject match = SourceUtils.findMatchingChild(candidate, folders, caseSensitive);
                        if (match == null) continue;
                        return match;
                    }
                }
                if ((foundFo = sourceRoots.length == 0 ? SourceUtils.findSource(signature[0], root) : SourceUtils.findSource(signature[0], sourceRoots)) == null) continue;
                return foundFo;
            }
        }
        catch (IOException e) {
            Exceptions.printStackTrace((Throwable)e);
        }
        return null;
    }

    private static FileObject findMatchingChild(String sourceFileName, Collection<FileObject> folders, boolean caseSensitive) {
        Match matchSet = caseSensitive ? new CaseSensitiveMatch(sourceFileName) : new CaseInsensitiveMatch(sourceFileName);
        for (FileObject folder : folders) {
            for (FileObject child : folder.getChildren()) {
                if (!matchSet.apply(child)) continue;
                return child;
            }
        }
        return null;
    }

    @NonNull
    private static java.util.List<Pair<FileObject, ClassPath>> findAllResources(@NonNull String resourceName, @NonNull ClassPath[] cps) {
        ArrayList<Pair<FileObject, ClassPath>> result = new ArrayList<Pair<FileObject, ClassPath>>();
        for (ClassPath cp : cps) {
            for (FileObject fo : cp.findAllResources(resourceName)) {
                result.add((Pair)Pair.of((Object)fo, (Object)cp));
            }
        }
        return result;
    }

    private static /* varargs */ FileObject findSource(String binaryName, FileObject ... fos) throws IOException {
        ClassIndexManager cim = ClassIndexManager.getDefault();
        try {
            for (FileObject fo : fos) {
                FileObject result;
                String sourceName;
                ClassIndexImpl ci = cim.getUsagesQuery(fo.toURL(), true);
                if (ci == null || (sourceName = ci.getSourceName(binaryName)) == null || (result = fo.getFileObject(sourceName)) == null) continue;
                return result;
            }
        }
        catch (InterruptedException e) {
            // empty catch block
        }
        return null;
    }

    @Deprecated
    public static URL getJavadoc(Element element, ClasspathInfo cpInfo) {
        Collection<? extends URL> res = SourceUtils.getJavadoc(element);
        return res.isEmpty() ? null : res.iterator().next();
    }

    /*
     * Exception decompiling
     */
    @CheckForNull
    public static URL getPreferredJavadoc(@NonNull Element element) {
        // This method has failed to decompile.  When submitting a bug report, please provide this stack trace, and (if you hold appropriate legal rights) the relevant class file.
        // org.benf.cfr.reader.util.ConfusedCFRException: Tried to end blocks [1[TRYBLOCK]], but top level block is 9[CATCHBLOCK]
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.processEndingBlocks(Op04StructuredStatement.java:397)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.buildNestedBlocks(Op04StructuredStatement.java:449)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op03SimpleStatement.createInitialStructuredBlock(Op03SimpleStatement.java:2877)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisInner(CodeAnalyser.java:825)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisOrWrapFail(CodeAnalyser.java:217)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysis(CodeAnalyser.java:162)
        // org.benf.cfr.reader.entities.attributes.AttributeCode.analyse(AttributeCode.java:95)
        // org.benf.cfr.reader.entities.Method.analyse(Method.java:355)
        // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:768)
        // org.benf.cfr.reader.entities.ClassFile.analyseTop(ClassFile.java:700)
        // org.benf.cfr.reader.Main.doJar(Main.java:134)
        // org.benf.cfr.reader.Main.main(Main.java:189)
        throw new IllegalStateException("Decompilation failed");
    }

    @NonNull
    public static Collection<? extends URL> getJavadoc(@NonNull Element element) {
        Parameters.notNull((CharSequence)"element", (Object)element);
        JavadocHelper.TextStream page = JavadocHelper.getJavadoc(element);
        if (page == null) {
            return Collections.emptySet();
        }
        page.close();
        return page.getLocations();
    }

    public static boolean isScanInProgress() {
        return IndexingManager.getDefault().isIndexing();
    }

    public static void waitScanFinished() throws InterruptedException {
        try {
            class T
            extends UserTask
            implements ClasspathInfoProvider {
                private final ClassPath EMPTY_PATH = ClassPathSupport.createClassPath((URL[])new URL[0]);
                private final ClasspathInfo cpinfo = ClasspathInfo.create(this.EMPTY_PATH, this.EMPTY_PATH, this.EMPTY_PATH);

                T() {
                }

                public void run(ResultIterator resultIterator) throws Exception {
                }

                @Override
                public ClasspathInfo getClasspathInfo() {
                    return this.cpinfo;
                }
            }
            Future f = ParserManager.parseWhenScanFinished((String)"text/x-java", (UserTask)new T());
            if (!f.isDone()) {
                f.get();
            }
        }
        catch (Exception ex) {
            // empty catch block
        }
    }

    @NonNull
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    public static Set<URL> getDependentRoots(@NonNull URL root) {
        return SourceUtils.getDependentRoots(root, true);
    }

    @NonNull
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    public static Set<URL> getDependentRoots(@NonNull URL root, boolean filterNonOpenedProjects) {
        FileObject rootFO = URLMapper.findFileObject((URL)root);
        if (rootFO != null) {
            return SourceUtils.mapToURLs(QuerySupport.findDependentRoots((FileObject)rootFO, (boolean)filterNonOpenedProjects));
        }
        return Collections.singleton(root);
    }

    public static Collection<ElementHandle<TypeElement>> getMainClasses(@NonNull FileObject fo) {
        Parameters.notNull((CharSequence)"fo", (Object)fo);
        if (!fo.isValid()) {
            throw new IllegalArgumentException("FileObject : " + FileUtil.getFileDisplayName((FileObject)fo) + " is not valid.");
        }
        if (fo.isVirtual()) {
            throw new IllegalArgumentException("FileObject : " + FileUtil.getFileDisplayName((FileObject)fo) + " is virtual.");
        }
        JavaSource js = JavaSource.forFileObject(fo);
        if (js == null) {
            throw new IllegalArgumentException();
        }
        try {
            final LinkedHashSet<ElementHandle<TypeElement>> result = new LinkedHashSet<ElementHandle<TypeElement>>();
            js.runUserActionTask(new Task<CompilationController>(){

                @Override
                public void run(CompilationController control) throws Exception {
                    if (control.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED).compareTo(JavaSource.Phase.ELEMENTS_RESOLVED) >= 0) {
                        final ArrayList types = new ArrayList();
                        ElementScanner6<Void, Void> visitor = new ElementScanner6<Void, Void>(){

                            @Override
                            public Void visitType(TypeElement e, Void p) {
                                if (e.getEnclosingElement().getKind() == ElementKind.PACKAGE || e.getModifiers().contains((Object)Modifier.STATIC)) {
                                    types.add(e);
                                    return (Void)ElementScanner6.super.visitType(e, p);
                                }
                                return null;
                            }
                        };
                        visitor.scan(control.getTopLevelElements(), null);
                        for (TypeElement type : types) {
                            for (ExecutableElement exec : ElementFilter.methodsIn(control.getElements().getAllMembers(type))) {
                                if (!SourceUtils.isMainMethod(exec)) continue;
                                result.add(ElementHandle.create(type));
                            }
                        }
                    }
                }

            }, true);
            return result;
        }
        catch (IOException ioe) {
            Exceptions.printStackTrace((Throwable)ioe);
            return Collections.emptySet();
        }
    }

    public static boolean isMainClass(String qualifiedName, ClasspathInfo cpInfo) {
        return SourceUtils.isMainClass(qualifiedName, cpInfo, false);
    }

    public static boolean isMainClass(final String qualifiedName, ClasspathInfo cpInfo, boolean optimistic) {
        if (qualifiedName == null || cpInfo == null) {
            throw new IllegalArgumentException();
        }
        block6 : for (ClassPath.Entry entry : cpInfo.getClassPath(ClasspathInfo.PathKind.SOURCE).entries()) {
            Iterable<? extends URL> mainClasses = ExecutableFilesIndex.DEFAULT.getMainClasses(entry.getURL());
            try {
                URI root = entry.getURL().toURI();
                for (URL mainClass : mainClasses) {
                    try {
                        URI relative = root.relativize(mainClass.toURI());
                        String resourceNameNoExt = FileObjects.stripExtension(relative.getPath());
                        String ffqn = FileObjects.convertFolder2Package(resourceNameNoExt, '/');
                        if (!qualifiedName.equals(ffqn)) continue;
                        ClassPath bootCp = cpInfo.getClassPath(ClasspathInfo.PathKind.BOOT);
                        if (bootCp.findResource(resourceNameNoExt + '.' + "class") != null) continue block6;
                        return true;
                    }
                    catch (URISyntaxException e) {
                        LOG.log(Level.INFO, "Ignoring fast check for file: {0} due to: {1}", new Object[]{mainClass.toString(), e.getMessage()});
                        continue;
                    }
                }
                continue;
            }
            catch (URISyntaxException e) {
                LOG.log(Level.INFO, "Ignoring fast check for root: {0} due to: {1}", new Object[]{entry.getURL().toString(), e.getMessage()});
                continue;
            }
        }
        final boolean[] result = new boolean[]{false};
        if (!optimistic) {
            JavaSource js = JavaSource.create(cpInfo, new FileObject[0]);
            try {
                js.runUserActionTask(new Task<CompilationController>(){

                    @Override
                    public void run(CompilationController control) throws Exception {
                        JavacElements elms = (JavacElements)control.getElements();
                        Symbol.ClassSymbol type = elms.getTypeElementByBinaryName((CharSequence)qualifiedName);
                        if (type == null) {
                            return;
                        }
                        java.util.List<ExecutableElement> methods = ElementFilter.methodsIn(elms.getAllMembers((TypeElement)type));
                        for (ExecutableElement method : methods) {
                            if (!SourceUtils.isMainMethod(method)) continue;
                            result[0] = true;
                            break;
                        }
                    }
                }, true);
            }
            catch (IOException ioe) {
                Exceptions.printStackTrace((Throwable)ioe);
            }
        }
        return result[0];
    }

    public static boolean isMainMethod(ExecutableElement method) {
        if (!"main".contentEquals(method.getSimpleName())) {
            return false;
        }
        long flags = ((Symbol.MethodSymbol)method).flags();
        if ((flags & 1) == 0 || (flags & 8) == 0) {
            return false;
        }
        if (method.getReturnType().getKind() != TypeKind.VOID) {
            return false;
        }
        java.util.List<? extends VariableElement> params = method.getParameters();
        if (params.size() != 1) {
            return false;
        }
        TypeMirror param = params.get(0).asType();
        if (param.getKind() != TypeKind.ARRAY) {
            return false;
        }
        ArrayType array = (ArrayType)param;
        TypeMirror compound = array.getComponentType();
        if (compound.getKind() != TypeKind.DECLARED) {
            return false;
        }
        if (!"java.lang.String".contentEquals(((TypeElement)((DeclaredType)compound).asElement()).getQualifiedName())) {
            return false;
        }
        return true;
    }

    public static Collection<ElementHandle<TypeElement>> getMainClasses(FileObject[] sourceRoots) {
        final LinkedList<ElementHandle<TypeElement>> result = new LinkedList<ElementHandle<TypeElement>>();
        for (final FileObject root : sourceRoots) {
            try {
                final File rootFile = FileUtil.toFile((FileObject)root);
                ClassPath bootPath = ClassPath.getClassPath((FileObject)root, (String)"classpath/boot");
                ClassPath compilePath = ClassPath.getClassPath((FileObject)root, (String)"classpath/compile");
                ClassPath srcPath = ClassPathSupport.createClassPath((FileObject[])new FileObject[]{root});
                ClasspathInfo cpInfo = ClasspathInfo.create(bootPath, compilePath, srcPath);
                JavaSource js = JavaSource.create(cpInfo, new FileObject[0]);
                js.runUserActionTask(new Task<CompilationController>(){

                    @Override
                    public void run(CompilationController control) throws Exception {
                        URL rootURL = root.toURL();
                        Iterable<? extends URL> mainClasses = ExecutableFilesIndex.DEFAULT.getMainClasses(rootURL);
                        LinkedList<? extends ElementHandle<TypeElement>> classes = new LinkedList<ElementHandle<TypeElement>>();
                        for (URL mainClass : mainClasses) {
                            File mainFo = Utilities.toFile((URI)URI.create(mainClass.toExternalForm()));
                            if (!mainFo.exists()) continue;
                            classes.addAll(JavaCustomIndexer.getRelatedTypes(mainFo, rootFile));
                        }
                        block1 : for (ElementHandle cls : classes) {
                            TypeElement te = (TypeElement)cls.resolve(control);
                            if (te == null) continue;
                            java.util.List<ExecutableElement> methods = ElementFilter.methodsIn(te.getEnclosedElements());
                            for (ExecutableElement method : methods) {
                                if (!SourceUtils.isMainMethod(method)) continue;
                                if (!SourceUtils.isIncluded(cls, control.getClasspathInfo())) continue block1;
                                result.add(cls);
                                continue block1;
                            }
                        }
                    }
                }, false);
                continue;
            }
            catch (IOException ioe) {
                Exceptions.printStackTrace((Throwable)ioe);
                return Collections.emptySet();
            }
        }
        return result;
    }

    private static boolean isIncluded(ElementHandle<TypeElement> element, ClasspathInfo cpInfo) {
        FileObject fobj = SourceUtils.getFile(element, cpInfo);
        if (fobj == null) {
            return true;
        }
        ClassPath sourcePath = cpInfo.getClassPath(ClasspathInfo.PathKind.SOURCE);
        for (ClassPath.Entry e : sourcePath.entries()) {
            FileObject root = e.getRoot();
            if (root == null || !FileUtil.isParentOf((FileObject)root, (FileObject)fobj)) continue;
            return e.includes(fobj);
        }
        return true;
    }

    private static boolean isCaseSensitive() {
        return !new File("a").equals(new File("A"));
    }

    private static Object getSourceFileNames(String classFileName) {
        int max = classFileName.length() - 1;
        int index = classFileName.indexOf(36);
        if (index == -1) {
            return classFileName;
        }
        ArrayList<String> ll = new ArrayList<String>(3);
        do {
            ll.add(classFileName.substring(0, index));
        } while (index < max && (index = classFileName.indexOf(36, index + 1)) >= 0);
        ll.add(classFileName);
        return ll;
    }

    public static WildcardType resolveCapturedType(TypeMirror type) {
        if (type instanceof Type.CapturedType) {
            return ((Type.CapturedType)type).wildcard;
        }
        return null;
    }

    private static ClassPath createClassPath(ClasspathInfo cpInfo, ClasspathInfo.PathKind kind) throws MalformedURLException {
        return ClasspathInfoAccessor.getINSTANCE().getCachedClassPath(cpInfo, kind);
    }

    @NonNull
    static String generateReadableParameterName(@NonNull String typeName, @NonNull Set<String> used) {
        boolean arr = typeName.indexOf("[") > 0 || typeName.endsWith("...");
        typeName = SourceUtils.trimToSimpleName(typeName);
        String result = typeName.toLowerCase();
        if (typeName.endsWith("Listener")) {
            result = "" + Character.toLowerCase(typeName.charAt(0)) + "l";
        } else if ("Object".equals(typeName)) {
            result = "o";
        } else if ("Class".equals(typeName)) {
            result = "type";
        } else if ("InputStream".equals(typeName)) {
            result = "in";
        } else if ("OutputStream".equals(typeName)) {
            result = "out";
        } else if ("Runnable".equals(typeName)) {
            result = "r";
        } else if ("Lookup".equals(typeName)) {
            result = "lkp";
        } else if (typeName.endsWith("Stream")) {
            result = "stream";
        } else if (typeName.endsWith("Writer")) {
            result = "writer";
        } else if (typeName.endsWith("Reader")) {
            result = "reader";
        } else if (typeName.endsWith("Panel")) {
            result = "pnl";
        } else if (typeName.endsWith("Action")) {
            result = "action";
        }
        if (result.length() > 6 && (result = SourceUtils.tryToMakeAcronym(typeName)).length() > 6 && (result = SourceUtils.elideVowelsAndRepetitions(result)).length() > 6) {
            result = ("" + result.charAt(0)).toLowerCase();
        }
        if (result.trim().length() == 0) {
            result = "value";
        }
        if (arr) {
            result = result + "s";
        }
        if (SourceUtils.isPrimitiveTypeName(result) || !Utilities.isJavaIdentifier((String)result)) {
            StringBuilder sb = new StringBuilder();
            sb.append(result.charAt(0));
            result = sb.toString();
        }
        String test = result;
        int revs = 0;
        while (used.contains(test)) {
            test = result + ++revs;
        }
        result = test;
        used.add(result);
        return result;
    }

    private static String trimToSimpleName(String typeName) {
        String result = typeName;
        int ix = result.indexOf("<");
        if (ix > 0 && ix != typeName.length() - 1) {
            result = typeName.substring(0, ix);
        }
        if (result.endsWith("...")) {
            result = result.substring(0, result.length() - 3);
        }
        if ((ix = result.lastIndexOf("$")) > 0 && ix != result.length() - 1) {
            result = result.substring(ix + 1);
        } else {
            ix = result.lastIndexOf(".");
            if (ix > 0 && ix != result.length() - 1) {
                result = result.substring(ix + 1);
            }
        }
        ix = result.indexOf("[");
        if (ix > 0) {
            result = result.substring(0, ix);
        }
        return result;
    }

    private static String elideVowelsAndRepetitions(String name) {
        char[] chars = name.toCharArray();
        StringBuilder sb = new StringBuilder();
        char last = '\u0000';
        char lastUsed = '\u0000';
        for (int i = 0; i < chars.length; ++i) {
            char c = chars[i];
            if (Character.isDigit(c)) continue;
            if (i == 0 || Character.isUpperCase(c)) {
                if (lastUsed != c) {
                    sb.append(c);
                    lastUsed = c;
                }
            } else if (c != last && !SourceUtils.isVowel(c) && lastUsed != c) {
                sb.append(c);
                lastUsed = c;
            }
            last = c;
        }
        return sb.toString();
    }

    private static boolean isVowel(char c) {
        return Arrays.binarySearch(VOWELS, c) >= 0;
    }

    private static boolean isPrimitiveTypeName(String typeName) {
        return "void".equals(typeName) || "int".equals(typeName) || "long".equals(typeName) || "float".equals(typeName) || "double".equals(typeName) || "short".equals(typeName) || "char".equals(typeName) || "boolean".equals(typeName);
    }

    private static String tryToMakeAcronym(String s) {
        char[] c = s.toCharArray();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < c.length; ++i) {
            if (!Character.isUpperCase(c[i])) continue;
            sb.append(c[i]);
        }
        if (sb.length() > 1) {
            return sb.toString().toLowerCase();
        }
        return s.toLowerCase();
    }

    @NonNull
    private static Set<URL> mapToURLs(@NonNull Collection<? extends FileObject> fos) {
        HashSet<URL> res = new HashSet<URL>(fos.size());
        for (FileObject fo : fos) {
            res.add(fo.toURL());
        }
        return res;
    }

    @CheckForNull
    private static URI findJavadocRoot(@NonNull URL javadocURL, @NonNull Element element) {
        String protocol = javadocURL.getProtocol();
        String host = javadocURL.getHost();
        int port = javadocURL.getPort();
        String path = javadocURL.getPath();
        while (element.getKind() != ElementKind.PACKAGE && !element.getKind().isClass() && !element.getKind().isInterface()) {
            element = element.getEnclosingElement();
        }
        int count = ((QualifiedNameable)element).getQualifiedName().toString().split("\\.").length;
        String[] parts = path.split("/");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < parts.length - count; ++i) {
            sb.append(parts[i]).append('/');
        }
        try {
            URL rootURL = new URL(protocol, host, port, sb.toString());
            return rootURL.toURI();
        }
        catch (MalformedURLException | URISyntaxException e) {
            LOG.log(Level.WARNING, "Invalid javadoc URL: {0}, protocol: {1}, host: {2}, port: {3}, path: {4}", new Object[]{javadocURL, protocol, host, port, sb.toString()});
            return null;
        }
    }

    private static class CaseInsensitiveMatch
    extends Match {
        CaseInsensitiveMatch(String name) {
            super(name);
        }

        @Override
        protected boolean match(String name1, String name2) {
            return name1.equalsIgnoreCase(name2);
        }
    }

    private static class CaseSensitiveMatch
    extends Match {
        CaseSensitiveMatch(String name) {
            super(name);
        }

        @Override
        protected boolean match(String name1, String name2) {
            return name1.equals(name2);
        }
    }

    private static abstract class Match {
        private final String name;

        Match(String names) {
            this.name = names;
        }

        final boolean apply(FileObject fo) {
            String foName = fo.getName();
            return this.match(foName, this.name) && this.isJava(fo);
        }

        protected abstract boolean match(String var1, String var2);

        private boolean isJava(FileObject fo) {
            return "java".equalsIgnoreCase(fo.getExt()) && fo.isData();
        }
    }

}

