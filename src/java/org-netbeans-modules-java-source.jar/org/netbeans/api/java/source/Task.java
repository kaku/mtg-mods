/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.java.source;

public interface Task<P> {
    public void run(P var1) throws Exception;
}

