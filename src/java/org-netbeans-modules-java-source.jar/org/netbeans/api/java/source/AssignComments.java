/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreeScanner
 *  com.sun.source.util.Trees
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.util.Context
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 */
package org.netbeans.api.java.source;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreeScanner;
import com.sun.source.util.Trees;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.Context;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.source.Comment;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.java.source.builder.CommentHandlerService;
import org.netbeans.modules.java.source.builder.CommentSetImpl;
import org.netbeans.modules.java.source.parsing.CompilationInfoImpl;
import org.netbeans.modules.java.source.query.CommentHandler;
import org.netbeans.modules.java.source.query.CommentSet;

class AssignComments
extends TreeScanner<Void, Void> {
    private final CompilationInfo info;
    private final CompilationUnitTree unit;
    private final Tree commentMapTarget;
    private final TokenSequence<JavaTokenId> seq;
    private final CommentHandlerService commentService;
    private final SourcePositions positions;
    private int tokenIndexAlreadyAdded = -1;
    private boolean mapComments;
    private Tree parent = null;
    private static Logger log = Logger.getLogger(AssignComments.class.getName());
    private int lastWhiteNewline;

    public AssignComments(CompilationInfo info, Tree commentMapTarget, TokenSequence<JavaTokenId> seq, CompilationUnitTree cut) {
        this(info, commentMapTarget, seq, cut, info.getTrees().getSourcePositions());
    }

    public AssignComments(CompilationInfo info, Tree commentMapTarget, TokenSequence<JavaTokenId> seq, SourcePositions positions) {
        this(info, commentMapTarget, seq, info.getCompilationUnit(), positions);
    }

    private AssignComments(CompilationInfo info, Tree commentMapTarget, TokenSequence<JavaTokenId> seq, CompilationUnitTree cut, SourcePositions positions) {
        this.info = info;
        this.unit = cut;
        this.seq = seq;
        this.commentMapTarget = commentMapTarget;
        this.commentService = CommentHandlerService.instance(info.impl.getJavacTask().getContext());
        this.positions = positions;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Void scan(Tree tree, Void p) {
        if (tree == null) {
            return null;
        }
        boolean oldMapComments = this.mapComments;
        try {
            this.mapComments |= tree == this.commentMapTarget;
            if (this.commentMapTarget != null && this.info.getTreeUtilities().isSynthetic(new TreePath(new TreePath(this.unit), tree))) {
                Void void_ = null;
                return void_;
            }
            if (this.commentMapTarget != null) {
                this.mapComments2(tree, true, false);
            }
            Tree oldParent = this.parent;
            try {
                this.parent = tree;
                TreeScanner.super.scan(tree, (Object)p);
            }
            finally {
                this.parent = oldParent;
            }
            if (this.commentMapTarget != null) {
                this.mapComments2(tree, false, tree.getKind() != Tree.Kind.BLOCK || this.parent == null || this.parent.getKind() != Tree.Kind.METHOD);
                if (this.mapComments) {
                    ((CommentSetImpl)this.createCommentSet(this.commentService, tree)).commentsMapped();
                }
            }
            Void void_ = null;
            return void_;
        }
        finally {
            this.mapComments = oldMapComments;
        }
    }

    private void mapComments2(Tree tree, boolean preceding, boolean trailing) {
        if (((JCTree)tree).pos <= 0) {
            return;
        }
        this.collect(tree, preceding, trailing);
    }

    private void collect(Tree tree, boolean preceding, boolean trailing) {
        if (this.isEvil(tree)) {
            return;
        }
        if (preceding) {
            int pos = this.findInterestingStart((JCTree)tree);
            if (pos >= 0) {
                BlockTree blockTree;
                this.seq.move(pos);
                this.lookForPreceedings(this.seq, tree);
                if (tree instanceof BlockTree && (blockTree = (BlockTree)tree).getStatements().isEmpty()) {
                    this.lookWithinEmptyBlock(this.seq, blockTree);
                }
            }
        } else {
            this.lookForInline(this.seq, tree);
            if (trailing) {
                this.lookForTrailing(this.seq, tree);
            }
        }
        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "T: " + (Object)tree + "\nC: " + this.commentService.getComments(tree));
        }
    }

    private void lookForInline(TokenSequence<JavaTokenId> seq, Tree tree) {
        seq.move((int)this.positions.getEndPosition(this.unit, tree));
        CommentsCollection result = new CommentsCollection();
        while (seq.moveNext()) {
            if (seq.token().id() == JavaTokenId.WHITESPACE) {
                if (this.numberOfNL(seq.token()) <= 0) continue;
                break;
            }
            if (!this.isComment((JavaTokenId)seq.token().id())) break;
            if (seq.index() > this.tokenIndexAlreadyAdded) {
                result.add(seq.token());
            }
            this.tokenIndexAlreadyAdded = seq.index();
            if (seq.token().id() != JavaTokenId.LINE_COMMENT) continue;
        }
        if (!result.isEmpty()) {
            CommentSet.RelativePosition position = CommentSet.RelativePosition.INLINE;
            this.attachComments(tree, result, position);
        }
    }

    private void attachComments(Tree tree, CommentsCollection result, CommentSet.RelativePosition position) {
        if (!this.mapComments) {
            return;
        }
        CommentSetImpl cs = this.commentService.getComments(tree);
        for (Token<JavaTokenId> token : result) {
            this.attachComment(position, cs, token);
        }
    }

    private boolean isEvil(Tree tree) {
        Tree.Kind kind = tree.getKind();
        switch (kind) {
            case COMPILATION_UNIT: {
                CompilationUnitTree cut = (CompilationUnitTree)tree;
                return cut.getPackageName() == null;
            }
            case MODIFIERS: 
            case PRIMITIVE_TYPE: {
                return true;
            }
        }
        return false;
    }

    private boolean parentEatsTralingComment(TokenSequence<JavaTokenId> seq, Tree t) {
        boolean ok;
        if (this.parent == null || this.lastWhiteNewline == -1) {
            return false;
        }
        int commentIndent = seq.offset() - this.lastWhiteNewline;
        Tree.Kind k = this.parent.getKind();
        boolean bl = ok = k == Tree.Kind.WHILE_LOOP || k == Tree.Kind.DO_WHILE_LOOP || k == Tree.Kind.IF;
        if (!ok) {
            return false;
        }
        int treeIndent = this.countIndent(seq, t);
        return treeIndent < commentIndent;
    }

    private int countIndent(TokenSequence<JavaTokenId> seq, Tree tree) {
        int st = (int)this.positions.getStartPosition(this.unit, tree);
        int save = seq.offset();
        int nl = -1;
        seq.move(st);
        while (seq.movePrevious()) {
            Token tukac = seq.token();
            if (tukac.id() != JavaTokenId.WHITESPACE) {
                if (tukac.id() != JavaTokenId.LINE_COMMENT) break;
                nl = seq.offset() + tukac.length();
                break;
            }
            nl = tukac.text().toString().lastIndexOf(10);
            if (nl == -1) continue;
            break;
        }
        seq.move(save);
        if (!seq.moveNext()) {
            seq.movePrevious();
        }
        return nl == -1 ? -1 : st - nl;
    }

    private void lookForTrailing(TokenSequence<JavaTokenId> seq, Tree tree) {
        seq.move((int)this.positions.getEndPosition(this.unit, tree));
        LinkedList<TrailingCommentsDataHolder> comments = new LinkedList<TrailingCommentsDataHolder>();
        int maxLines = 0;
        int newlines = 0;
        int lastIndex = -1;
        while (seq.moveNext()) {
            Token t;
            if (lastIndex == -1) {
                lastIndex = seq.index();
            }
            if ((t = seq.token()).id() == JavaTokenId.WHITESPACE) {
                int nls = this.numberOfNL(t, seq.offset());
                newlines += nls;
                continue;
            }
            if (this.isComment((JavaTokenId)t.id())) {
                if (newlines > 0 && this.parentEatsTralingComment(seq, tree)) {
                    return;
                }
                if (seq.index() > this.tokenIndexAlreadyAdded) {
                    comments.add(new TrailingCommentsDataHolder(newlines, t, lastIndex));
                }
                maxLines = Math.max(maxLines, newlines);
                if (t.id() == JavaTokenId.LINE_COMMENT) {
                    newlines = 1;
                    this.lastWhiteNewline = seq.offset() + t.length();
                } else {
                    newlines = 0;
                    this.lastWhiteNewline = -1;
                }
                lastIndex = -1;
                continue;
            }
            if (t.id() != JavaTokenId.RBRACE && t.id() != JavaTokenId.ELSE) break;
            maxLines = Integer.MAX_VALUE;
            break;
        }
        int index = seq.index() - 1;
        maxLines = Math.max(maxLines, newlines);
        for (TrailingCommentsDataHolder h : comments) {
            if (h.newlines < maxLines) {
                this.attachComments(Collections.singleton(h.comment), tree, this.commentService, CommentSet.RelativePosition.TRAILING);
                continue;
            }
            index = h.index - 1;
            break;
        }
        this.tokenIndexAlreadyAdded = index;
    }

    private void lookWithinEmptyBlock(TokenSequence<JavaTokenId> seq, BlockTree tree) {
        if (this.moveTo(seq, JavaTokenId.LBRACE, true)) {
            int idx = -1;
            if (seq.moveNext()) {
                JavaTokenId id = (JavaTokenId)seq.token().id();
                idx = seq.index();
                if (id == JavaTokenId.WHITESPACE || this.isComment(id)) {
                    CommentsCollection cc = this.getCommentsCollection(seq, Integer.MAX_VALUE);
                    this.attachComments((Tree)tree, cc, CommentSet.RelativePosition.INNER);
                }
            }
            if (this.tokenIndexAlreadyAdded < idx) {
                this.tokenIndexAlreadyAdded = idx;
            }
        } else {
            int end = (int)this.positions.getEndPosition(this.unit, (Tree)tree);
            seq.move(end);
            seq.moveNext();
        }
    }

    private boolean moveTo(TokenSequence<JavaTokenId> seq, JavaTokenId toToken, boolean forward) {
        if (seq.token() == null) {
            return false;
        }
        do {
            if (toToken == seq.token().id()) {
                return true;
            }
            if (forward) {
                if (!seq.moveNext()) break;
                continue;
            }
            if (!seq.movePrevious()) break;
        } while (true);
        return false;
    }

    private void lookForPreceedings(TokenSequence<JavaTokenId> seq, Tree tree) {
        int reset = ((JCTree)tree).pos;
        CommentsCollection cc = null;
        while (seq.moveNext() && seq.offset() < reset) {
            JavaTokenId id = (JavaTokenId)seq.token().id();
            if (!this.isComment(id)) continue;
            if (cc == null) {
                cc = this.getCommentsCollection(seq, Integer.MAX_VALUE);
                continue;
            }
            cc.merge(this.getCommentsCollection(seq, Integer.MAX_VALUE));
        }
        this.attachComments(cc, tree, this.commentService, CommentSet.RelativePosition.PRECEDING);
        seq.move(reset);
        seq.moveNext();
        this.tokenIndexAlreadyAdded = seq.index();
    }

    private int findInterestingStart(JCTree tree) {
        boolean previousSucceeded;
        int pos = (int)this.positions.getStartPosition(this.unit, (Tree)tree);
        if (pos <= 0) {
            return 0;
        }
        this.seq.move(pos);
        boolean ranOnce = false;
        block4 : while (previousSucceeded = this.seq.movePrevious() && this.tokenIndexAlreadyAdded < this.seq.index()) {
            ranOnce = true;
            switch ((JavaTokenId)this.seq.token().id()) {
                case WHITESPACE: 
                case LINE_COMMENT: 
                case JAVADOC_COMMENT: 
                case BLOCK_COMMENT: {
                    continue block4;
                }
                case LBRACE: {
                    return this.seq.offset() + this.seq.token().length();
                }
            }
            return this.seq.offset() + this.seq.token().length();
        }
        if (!previousSucceeded && !ranOnce) {
            return -1;
        }
        return this.seq.offset() + (this.tokenIndexAlreadyAdded >= this.seq.index() ? this.seq.token().length() : 0);
    }

    private void attachComments(Iterable<? extends Token<JavaTokenId>> foundComments, Tree tree, CommentHandler ch, CommentSet.RelativePosition positioning) {
        if (foundComments == null || !foundComments.iterator().hasNext() || !this.mapComments) {
            return;
        }
        CommentSetImpl set = (CommentSetImpl)this.createCommentSet(ch, tree);
        if (set.areCommentsMapped()) {
            return;
        }
        for (Token<JavaTokenId> comment : foundComments) {
            this.attachComment(positioning, set, comment);
        }
    }

    private void attachComment(CommentSet.RelativePosition positioning, CommentSet set, Token<JavaTokenId> comment) {
        Comment c = Comment.create(this.getStyle((JavaTokenId)comment.id()), comment.offset(null), this.getEndPos(comment), -2, this.getText(comment));
        set.addComment(positioning, c);
    }

    private String getText(Token<JavaTokenId> comment) {
        return String.valueOf(comment.text());
    }

    private int getEndPos(Token<JavaTokenId> comment) {
        return comment.offset(null) + comment.length();
    }

    private Comment.Style getStyle(JavaTokenId id) {
        switch (id) {
            case JAVADOC_COMMENT: {
                return Comment.Style.JAVADOC;
            }
            case LINE_COMMENT: {
                return Comment.Style.LINE;
            }
            case BLOCK_COMMENT: {
                return Comment.Style.BLOCK;
            }
        }
        return Comment.Style.WHITESPACE;
    }

    private int numberOfNL(Token<JavaTokenId> t) {
        return this.numberOfNL(t, -1);
    }

    private int numberOfNL(Token<JavaTokenId> t, int offset) {
        int count = 0;
        CharSequence charSequence = t.text();
        for (int i = 0; i < charSequence.length(); ++i) {
            char a = charSequence.charAt(i);
            if ('\n' != a) continue;
            this.lastWhiteNewline = offset + i;
            ++count;
        }
        if (offset == -1) {
            this.lastWhiteNewline = -1;
        }
        return count;
    }

    private CommentsCollection getCommentsCollection(TokenSequence<JavaTokenId> ts, int maxTension) {
        CommentsCollection result = new CommentsCollection();
        Token t = ts.token();
        result.add(t);
        boolean isLC = t.id() == JavaTokenId.LINE_COMMENT;
        int lastCommentIndex = ts.index();
        int start = ts.offset();
        int end = ts.offset() + ts.token().length();
        while (ts.moveNext()) {
            if (ts.index() < this.tokenIndexAlreadyAdded) continue;
            t = ts.token();
            if (this.isComment((JavaTokenId)t.id())) {
                result.add(t);
                start = Math.min(ts.offset(), start);
                end = Math.max(ts.offset() + t.length(), end);
                isLC = t.id() == JavaTokenId.LINE_COMMENT;
                lastCommentIndex = ts.index();
                continue;
            }
            if (t.id() == JavaTokenId.WHITESPACE && this.numberOfNL(t) + (isLC ? 1 : 0) <= maxTension) continue;
        }
        ts.moveIndex(lastCommentIndex);
        ts.moveNext();
        this.tokenIndexAlreadyAdded = ts.index();
        result.setBounds(new int[]{start, end});
        return result;
    }

    private CommentSet createCommentSet(CommentHandler ch, Tree lastTree) {
        return ch.getComments(lastTree);
    }

    private boolean isComment(JavaTokenId tid) {
        switch (tid) {
            case LINE_COMMENT: 
            case JAVADOC_COMMENT: 
            case BLOCK_COMMENT: {
                return true;
            }
        }
        return false;
    }

    private static class CommentsCollection
    implements Iterable<Token<JavaTokenId>> {
        private final int[] bounds = new int[]{-2, -2};
        private final List<Token<JavaTokenId>> comments = new LinkedList<Token<JavaTokenId>>();

        private CommentsCollection() {
        }

        void add(Token<JavaTokenId> comment) {
            this.comments.add(comment);
        }

        boolean isEmpty() {
            return this.comments.isEmpty();
        }

        @Override
        public Iterator<Token<JavaTokenId>> iterator() {
            return this.comments.iterator();
        }

        void setBounds(int[] bounds) {
            this.bounds[0] = bounds[0];
            this.bounds[1] = bounds[1];
        }

        public int[] getBounds() {
            return (int[])this.bounds.clone();
        }

        public void merge(CommentsCollection cc) {
            this.comments.addAll(cc.comments);
            this.bounds[0] = Math.min(this.bounds[0], cc.bounds[0]);
            this.bounds[1] = Math.max(this.bounds[1], cc.bounds[1]);
        }
    }

    private static final class TrailingCommentsDataHolder {
        private final int newlines;
        private final Token<JavaTokenId> comment;
        private final int index;

        public TrailingCommentsDataHolder(int newlines, Token<JavaTokenId> comment, int index) {
            this.newlines = newlines;
            this.comment = comment;
            this.index = index;
        }
    }

}

