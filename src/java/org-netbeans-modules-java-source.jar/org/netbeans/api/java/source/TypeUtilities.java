/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$TypeSymbol
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.code.Types
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.List
 *  org.netbeans.api.annotations.common.CheckReturnValue
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 */
package org.netbeans.api.java.source;

import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.Types;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Iterator;
import javax.lang.model.element.Element;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ErrorType;
import javax.lang.model.type.ExecutableType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.type.WildcardType;
import javax.lang.model.util.SimpleTypeVisitor6;
import org.netbeans.api.annotations.common.CheckReturnValue;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.modules.java.source.parsing.CompilationInfoImpl;

public final class TypeUtilities {
    private final CompilationInfo info;
    private static final String UNKNOWN = "<unknown>";
    private static final String CAPTURED_WILDCARD = "<captured wildcard>";

    TypeUtilities(CompilationInfo info) {
        assert (info != null);
        this.info = info;
    }

    public boolean isCastable(TypeMirror t1, TypeMirror t2) {
        switch (t1.getKind()) {
            case EXECUTABLE: 
            case PACKAGE: 
            case NONE: 
            case OTHER: {
                throw new IllegalArgumentException();
            }
        }
        return Types.instance((Context)this.info.impl.getJavacTask().getContext()).isCastable((Type)t1, (Type)t2);
    }

    public TypeMirror substitute(TypeMirror type, java.util.List<? extends TypeMirror> from, java.util.List<? extends TypeMirror> to) {
        if (from.size() != to.size()) {
            throw new IllegalArgumentException();
        }
        List l1 = List.nil();
        for (TypeMirror typeMirror : from) {
            l1 = l1.prepend((Object)((Type)typeMirror));
        }
        List l2 = List.nil();
        for (TypeMirror typeMirror2 : to) {
            l2 = l2.prepend((Object)((Type)typeMirror2));
        }
        return Types.instance((Context)this.info.impl.getJavacTask().getContext()).subst((Type)type, l1, l2);
    }

    public ExecutableType getDescriptorType(DeclaredType origin) {
        Type dt;
        Types types = Types.instance((Context)this.info.impl.getJavacTask().getContext());
        if (types.isFunctionalInterface(((Type)origin).tsym) && (dt = types.findDescriptorType((Type)origin)) != null && dt.getKind() == TypeKind.EXECUTABLE) {
            return (ExecutableType)dt;
        }
        return null;
    }

    @NonNull
    @CheckReturnValue
    public /* varargs */ CharSequence getTypeName(@NullAllowed TypeMirror type, @NonNull TypeNameOptions ... options) {
        if (type == null) {
            return "";
        }
        EnumSet<TypeNameOptions> opt = EnumSet.noneOf(TypeNameOptions.class);
        opt.addAll(Arrays.asList(options));
        return ((StringBuilder)new TypeNameVisitor(opt.contains((Object)TypeNameOptions.PRINT_AS_VARARG)).visit(type, opt.contains((Object)TypeNameOptions.PRINT_FQN))).toString();
    }

    private static class TypeNameVisitor
    extends SimpleTypeVisitor6<StringBuilder, Boolean> {
        private boolean varArg;
        private boolean insideCapturedWildcard = false;

        private TypeNameVisitor(boolean varArg) {
            super(new StringBuilder());
            this.varArg = varArg;
        }

        @Override
        public StringBuilder defaultAction(TypeMirror t, Boolean p) {
            return ((StringBuilder)this.DEFAULT_VALUE).append(t);
        }

        @Override
        public StringBuilder visitDeclared(DeclaredType t, Boolean p) {
            Element e = t.asElement();
            if (e instanceof TypeElement) {
                TypeElement te = (TypeElement)e;
                ((StringBuilder)this.DEFAULT_VALUE).append((p != false ? te.getQualifiedName() : te.getSimpleName()).toString());
                Iterator<? extends TypeMirror> it = t.getTypeArguments().iterator();
                if (it.hasNext()) {
                    ((StringBuilder)this.DEFAULT_VALUE).append("<");
                    while (it.hasNext()) {
                        this.visit(it.next(), p);
                        if (!it.hasNext()) continue;
                        ((StringBuilder)this.DEFAULT_VALUE).append(", ");
                    }
                    ((StringBuilder)this.DEFAULT_VALUE).append(">");
                }
                return (StringBuilder)this.DEFAULT_VALUE;
            }
            return ((StringBuilder)this.DEFAULT_VALUE).append("<unknown>");
        }

        @Override
        public StringBuilder visitArray(ArrayType t, Boolean p) {
            boolean isVarArg = this.varArg;
            this.varArg = false;
            this.visit(t.getComponentType(), p);
            return ((StringBuilder)this.DEFAULT_VALUE).append(isVarArg ? "..." : "[]");
        }

        @Override
        public StringBuilder visitTypeVariable(TypeVariable t, Boolean p) {
            String name;
            Element e = t.asElement();
            if (e != null && !"<captured wildcard>".equals(name = e.getSimpleName().toString())) {
                return ((StringBuilder)this.DEFAULT_VALUE).append(name);
            }
            ((StringBuilder)this.DEFAULT_VALUE).append("?");
            if (!this.insideCapturedWildcard) {
                this.insideCapturedWildcard = true;
                TypeMirror bound = t.getLowerBound();
                if (bound != null && bound.getKind() != TypeKind.NULL) {
                    ((StringBuilder)this.DEFAULT_VALUE).append(" super ");
                    this.visit(bound, p);
                } else {
                    bound = t.getUpperBound();
                    if (bound != null && bound.getKind() != TypeKind.NULL) {
                        ((StringBuilder)this.DEFAULT_VALUE).append(" extends ");
                        if (bound.getKind() == TypeKind.TYPEVAR) {
                            bound = ((TypeVariable)bound).getLowerBound();
                        }
                        this.visit(bound, p);
                    }
                }
                this.insideCapturedWildcard = false;
            }
            return (StringBuilder)this.DEFAULT_VALUE;
        }

        @Override
        public StringBuilder visitWildcard(WildcardType t, Boolean p) {
            int len = ((StringBuilder)this.DEFAULT_VALUE).length();
            ((StringBuilder)this.DEFAULT_VALUE).append("?");
            TypeMirror bound = t.getSuperBound();
            if (bound == null) {
                bound = t.getExtendsBound();
                if (bound != null) {
                    ((StringBuilder)this.DEFAULT_VALUE).append(" extends ");
                    if (bound.getKind() == TypeKind.WILDCARD) {
                        bound = ((WildcardType)bound).getSuperBound();
                    }
                    this.visit(bound, p);
                } else if (!(len != 0 || (bound = SourceUtils.getBound(t)) == null || bound.getKind() == TypeKind.DECLARED && ((TypeElement)((DeclaredType)bound).asElement()).getQualifiedName().contentEquals("java.lang.Object"))) {
                    ((StringBuilder)this.DEFAULT_VALUE).append(" extends ");
                    this.visit(bound, p);
                }
            } else {
                ((StringBuilder)this.DEFAULT_VALUE).append(" super ");
                this.visit(bound, p);
            }
            return (StringBuilder)this.DEFAULT_VALUE;
        }

        @Override
        public StringBuilder visitError(ErrorType t, Boolean p) {
            Element e = t.asElement();
            if (e instanceof TypeElement) {
                TypeElement te = (TypeElement)e;
                return ((StringBuilder)this.DEFAULT_VALUE).append((p != false ? te.getQualifiedName() : te.getSimpleName()).toString());
            }
            return (StringBuilder)this.DEFAULT_VALUE;
        }

        @Override
        public StringBuilder visitUnknown(TypeMirror t, Boolean p) {
            return ((StringBuilder)this.DEFAULT_VALUE).append("<unknown>");
        }
    }

    public static enum TypeNameOptions {
        PRINT_FQN,
        PRINT_AS_VARARG;
        

        private TypeNameOptions() {
        }
    }

}

