/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.doctree.DocCommentTree
 *  com.sun.source.doctree.DocTree
 *  com.sun.source.doctree.DocTree$Kind
 *  com.sun.source.doctree.DocTreeVisitor
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.util.DocTreePath
 *  com.sun.source.util.DocTreePathScanner
 *  com.sun.source.util.DocTreeScanner
 *  com.sun.source.util.DocTrees
 *  com.sun.source.util.TreePath
 *  com.sun.tools.javac.tree.DCTree
 *  com.sun.tools.javac.tree.DCTree$DCDocComment
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.OpenCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.EditorSupport
 *  org.openide.text.PositionRef
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Parameters
 */
package org.netbeans.api.java.source;

import com.sun.source.doctree.DocCommentTree;
import com.sun.source.doctree.DocTree;
import com.sun.source.doctree.DocTreeVisitor;
import com.sun.source.tree.Tree;
import com.sun.source.util.DocTreePath;
import com.sun.source.util.DocTreePathScanner;
import com.sun.source.util.DocTreeScanner;
import com.sun.source.util.DocTrees;
import com.sun.source.util.TreePath;
import com.sun.tools.javac.tree.DCTree;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.swing.text.Position;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.TreePathHandle;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.EditorSupport;
import org.openide.text.PositionRef;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Parameters;

public final class DocTreePathHandle {
    private static Logger log = Logger.getLogger(DocTreePathHandle.class.getName());
    private final Delegate delegate;

    private DocTreePathHandle(Delegate d) {
        if (d == null) {
            throw new IllegalArgumentException();
        }
        this.delegate = d;
    }

    public DocTreePath resolve(CompilationInfo compilationInfo) throws IllegalArgumentException {
        DocTreePath result = this.delegate.resolve(compilationInfo);
        if (result == null) {
            Logger.getLogger(DocTreePathHandle.class.getName()).info("Cannot resolve: " + this.toString());
        }
        return result;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof DocTreePathHandle)) {
            return false;
        }
        if (this.delegate.getClass() != ((DocTreePathHandle)obj).delegate.getClass()) {
            return false;
        }
        return this.delegate.equalsHandle(((DocTreePathHandle)obj).delegate);
    }

    public int hashCode() {
        return this.delegate.hashCode();
    }

    public TreePathHandle getTreePathHandle() {
        return this.delegate.getTreePathHandle();
    }

    public DocTree.Kind getKind() {
        return this.delegate.getKind();
    }

    public static DocTreePathHandle create(DocTreePath docTreePath, CompilationInfo javac) throws IllegalArgumentException {
        Parameters.notNull((CharSequence)"docTreePath", (Object)docTreePath);
        Parameters.notNull((CharSequence)"javac", (Object)javac);
        TreePathHandle treePathHandle = TreePathHandle.create(docTreePath.getTreePath(), javac);
        if (treePathHandle.getFileObject() == null) {
            return null;
        }
        int position = (int)((DCTree)docTreePath.getLeaf()).getSourcePosition((DCTree.DCDocComment)docTreePath.getDocComment());
        if (position == -1) {
            DocTree docTree = docTreePath.getLeaf();
            if (docTree == docTreePath.getDocComment()) {
                return new DocTreePathHandle(new DocCommentDelegate(treePathHandle));
            }
            int index = DocTreePathHandle.listChildren(docTreePath.getParentPath().getLeaf()).indexOf((Object)docTree);
            assert (index != -1);
            return new DocTreePathHandle(new CountingDelegate(treePathHandle, index, docTreePath.getLeaf().getKind()));
        }
        PositionRef pos = DocTreePathHandle.createPositionRef(treePathHandle.getFileObject(), position, Position.Bias.Forward);
        return new DocTreePathHandle(new DocTreeDelegate(pos, new DocTreeDelegate.KindPath(docTreePath), treePathHandle));
    }

    private static boolean isSupported(Element el) {
        switch (el.getKind()) {
            case PACKAGE: 
            case CLASS: 
            case INTERFACE: 
            case ENUM: 
            case METHOD: 
            case CONSTRUCTOR: 
            case INSTANCE_INIT: 
            case STATIC_INIT: 
            case FIELD: 
            case ANNOTATION_TYPE: 
            case ENUM_CONSTANT: {
                return true;
            }
        }
        return false;
    }

    private static List<DocTree> listChildren(@NonNull DocTree t) {
        final LinkedList<DocTree> result = new LinkedList<DocTree>();
        t.accept((DocTreeVisitor)new DocTreeScanner<Void, Void>(){

            public Void scan(DocTree node, Void p) {
                result.add(node);
                return null;
            }
        }, (Object)null);
        return result;
    }

    private static PositionRef createPositionRef(FileObject file, int position, Position.Bias bias) {
        try {
            DataObject dob = DataObject.find((FileObject)file);
            Object obj = dob.getLookup().lookup(OpenCookie.class);
            if (obj instanceof CloneableEditorSupport) {
                return ((CloneableEditorSupport)obj).createPositionRef(position, bias);
            }
            obj = dob.getLookup().lookup(EditorCookie.class);
            if (obj instanceof CloneableEditorSupport) {
                return ((CloneableEditorSupport)obj).createPositionRef(position, bias);
            }
            EditorSupport es = (EditorSupport)dob.getLookup().lookup(EditorSupport.class);
            if (es != null) {
                return es.createPositionRef(position, bias);
            }
        }
        catch (DataObjectNotFoundException ex) {
            ex.printStackTrace();
        }
        throw new IllegalStateException("Cannot create PositionRef for file " + file.getPath() + ". CloneableEditorSupport not found");
    }

    public String toString() {
        return "DocTreePathHandle[delegate:" + this.delegate + "]";
    }

    private static DocTreePath getChild(@NonNull DocCommentTree t, final int index) {
        final DocTreePath[] result = new DocTreePath[1];
        t.accept((DocTreeVisitor)new DocTreePathScanner<DocTreePath, Void>(){
            int count;

            public DocTreePath scan(DocTree node, Void p) {
                if (index == this.count) {
                    result[0] = this.getCurrentPath();
                }
                return null;
            }
        }, (Object)null);
        return result[0];
    }

    private static final class CountingDelegate
    implements Delegate {
        private final TreePathHandle parent;
        private final int index;
        private final DocTree.Kind kind;

        public CountingDelegate(TreePathHandle parent, int index, DocTree.Kind kind) {
            this.parent = parent;
            this.index = index;
            this.kind = kind;
        }

        @Override
        public DocTreePath resolve(CompilationInfo javac) throws IllegalArgumentException {
            TreePath p = this.parent.resolve(javac);
            if (p == null) {
                return null;
            }
            DocCommentTree docCommentTree = javac.getDocTrees().getDocCommentTree(p);
            return DocTreePathHandle.getChild(docCommentTree, this.index);
        }

        @Override
        public boolean equalsHandle(Delegate obj) {
            return this == obj;
        }

        @Override
        public DocTree.Kind getKind() {
            return this.kind;
        }

        @Override
        public TreePathHandle getTreePathHandle() {
            return this.parent;
        }
    }

    private static final class DocCommentDelegate
    implements Delegate {
        private final TreePathHandle parent;

        public DocCommentDelegate(TreePathHandle parent) {
            this.parent = parent;
        }

        @Override
        public DocTreePath resolve(CompilationInfo javac) throws IllegalArgumentException {
            TreePath p = this.parent.resolve(javac);
            if (p == null) {
                return null;
            }
            DocCommentTree docCommentTree = javac.getDocTrees().getDocCommentTree(p);
            return new DocTreePath(p, docCommentTree);
        }

        @Override
        public boolean equalsHandle(Delegate obj) {
            return this == obj;
        }

        @Override
        public TreePathHandle getTreePathHandle() {
            return this.parent;
        }

        @Override
        public DocTree.Kind getKind() {
            return DocTree.Kind.DOC_COMMENT;
        }
    }

    private static final class DocTreeDelegate
    implements Delegate {
        private final PositionRef position;
        private final KindPath kindPath;
        private final TreePathHandle treePathHandle;
        private final DocTree.Kind kind;

        private DocTreeDelegate(PositionRef position, KindPath kindPath, TreePathHandle treePathHandle) {
            this.kindPath = kindPath;
            this.position = position;
            this.treePathHandle = treePathHandle;
            this.kind = kindPath != null ? (DocTree.Kind)kindPath.kindPath.get(0) : null;
        }

        @Override
        public DocTreePath resolve(CompilationInfo javac) throws IllegalArgumentException {
            int pos;
            assert (javac != null);
            TreePath treePath = this.treePathHandle.resolve(javac);
            if (treePath == null) {
                throw new IllegalArgumentException("treePathHandle.resolve(compilationInfo) returned null for treePathHandle " + this.treePathHandle);
            }
            DocTreePath tp = null;
            DocCommentTree doc = javac.getDocTrees().getDocCommentTree(treePath);
            tp = this.resolvePathForPos(javac, treePath, doc, (pos = this.position.getOffset()) + 1);
            if (tp != null) {
                return tp;
            }
            tp = this.resolvePathForPos(javac, treePath, doc, pos);
            return tp;
        }

        private DocTreePath resolvePathForPos(CompilationInfo javac, TreePath treePath, DocCommentTree doc, int pos) {
            for (DocTreePath tp = javac.getTreeUtilities().pathFor((TreePath)treePath, (DocCommentTree)doc, (int)pos); tp != null; tp = tp.getParentPath()) {
                KindPath kindPath1 = new KindPath(tp);
                this.kindPath.getList().remove((Object)Tree.Kind.ERRONEOUS);
                if (!kindPath1.equals(this.kindPath)) continue;
                return tp;
            }
            return null;
        }

        @Override
        public boolean equalsHandle(Delegate obj) {
            DocTreeDelegate other = (DocTreeDelegate)obj;
            try {
                if (this.position.getPosition().getOffset() != this.position.getPosition().getOffset()) {
                    return false;
                }
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
                return false;
            }
            return other.getTreePathHandle().equals(this.treePathHandle);
        }

        @Override
        public int hashCode() {
            if (this.position == null) {
                return 553 + this.treePathHandle.hashCode();
            }
            int hash = 7;
            hash = 79 * hash + this.position.getOffset();
            hash = 79 * hash + this.treePathHandle.hashCode();
            return hash;
        }

        @Override
        public DocTree.Kind getKind() {
            return this.kind;
        }

        public String toString() {
            return this.getClass().getSimpleName() + "[kind:" + (Object)this.kind + ", treepathHandle:" + this.treePathHandle + "]";
        }

        @Override
        public TreePathHandle getTreePathHandle() {
            return this.treePathHandle;
        }

        static class KindPath {
            private ArrayList<DocTree.Kind> kindPath = new ArrayList();

            KindPath(DocTreePath treePath) {
                while (treePath != null) {
                    this.kindPath.add(treePath.getLeaf().getKind());
                    treePath = treePath.getParentPath();
                }
            }

            public int hashCode() {
                return this.kindPath.hashCode();
            }

            public boolean equals(Object object) {
                if (object instanceof KindPath) {
                    return this.kindPath.equals(((KindPath)object).kindPath);
                }
                return false;
            }

            public ArrayList<DocTree.Kind> getList() {
                return this.kindPath;
            }
        }

    }

    static interface Delegate {
        public DocTreePath resolve(CompilationInfo var1) throws IllegalArgumentException;

        public boolean equalsHandle(Delegate var1);

        public int hashCode();

        public DocTree.Kind getKind();

        public TreePathHandle getTreePathHandle();
    }

}

