/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.lexer.TokenSequence
 */
package org.netbeans.api.java.source;

import com.sun.source.tree.CompilationUnitTree;
import java.io.IOException;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.GeneratorUtilities;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.api.lexer.TokenSequence;

@Deprecated
public final class CommentCollector {
    private static CommentCollector instance = null;

    @Deprecated
    public static synchronized CommentCollector getInstance() {
        if (instance == null) {
            instance = new CommentCollector();
        }
        return instance;
    }

    private CommentCollector() {
    }

    @Deprecated
    public void collect(WorkingCopy copy) throws IOException {
        GeneratorUtilities.importComments(copy, copy.getCompilationUnit(), copy.getCompilationUnit());
    }

    @Deprecated
    public void collect(TokenSequence<JavaTokenId> ts, CompilationInfo ci) {
        GeneratorUtilities.importComments(ci, ci.getCompilationUnit(), ci.getCompilationUnit());
    }
}

