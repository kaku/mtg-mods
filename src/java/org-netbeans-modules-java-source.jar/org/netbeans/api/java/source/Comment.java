/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.java.source;

public final class Comment {
    private Style style;
    private int pos;
    private int endPos;
    private int indent;
    private String text;

    public static Comment create(String s) {
        return new Comment(Style.BLOCK, -2, -2, -2, s);
    }

    public static Comment create(Style style, int pos, int endPos, int indent, String text) {
        return new Comment(style, pos, endPos, indent, text);
    }

    public static Comment create(Style style, String text) {
        return new Comment(style, -2, -2, -2, text);
    }

    private Comment(Style style, int pos, int endPos, int indent, String text) {
        this.style = style;
        this.pos = pos;
        this.endPos = endPos;
        this.indent = indent;
        this.text = text;
    }

    public Style style() {
        return this.style;
    }

    public int pos() {
        return this.pos;
    }

    public int endPos() {
        return this.endPos;
    }

    public int indent() {
        return this.indent;
    }

    public boolean isDocComment() {
        return this.style == Style.JAVADOC;
    }

    public String getText() {
        return this.text;
    }

    public boolean isNew() {
        return this.pos == -2;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(this.style.toString());
        sb.append(" pos=");
        sb.append(this.pos);
        sb.append(" endPos=");
        sb.append(this.endPos);
        sb.append(" indent=");
        sb.append(this.indent);
        sb.append(' ');
        sb.append(this.text);
        return sb.toString();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Comment)) {
            return false;
        }
        Comment c = (Comment)obj;
        return c.style == this.style && c.pos == this.pos && c.endPos == this.endPos && c.indent == this.indent && c.text.equals(this.text);
    }

    public int hashCode() {
        return this.style.hashCode() + this.pos + this.endPos + this.indent + this.text.hashCode();
    }

    public static enum Style {
        LINE,
        BLOCK,
        JAVADOC,
        WHITESPACE;
        

        private Style() {
        }
    }

}

