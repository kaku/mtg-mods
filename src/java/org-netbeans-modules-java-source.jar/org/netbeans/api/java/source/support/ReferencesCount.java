/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.modules.parsing.lucene.support.Convertor
 *  org.openide.util.Parameters
 */
package org.netbeans.api.java.source.support;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.modules.java.source.ElementHandleAccessor;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;
import org.netbeans.modules.java.source.usages.ClassIndexManager;
import org.netbeans.modules.parsing.lucene.support.Convertor;
import org.openide.util.Parameters;

public final class ReferencesCount {
    private static final Logger LOG = Logger.getLogger(ReferencesCount.class.getName());
    private final Object lck = new Object();
    private final Iterable<? extends URL> roots;
    private Map<String, Integer> typeFreqs;
    private Map<String, Integer> pkgFreqs;

    private ReferencesCount(@NonNull Iterable<? extends URL> roots) {
        this.roots = roots;
    }

    public int getTypeReferenceCount(@NonNull ElementHandle<? extends TypeElement> type) {
        Parameters.notNull((CharSequence)"binaryName", type);
        if (!type.getKind().isClass() && !type.getKind().isInterface() && type.getKind() != ElementKind.OTHER) {
            throw new IllegalArgumentException(type.toString());
        }
        try {
            this.init();
            Integer count = this.typeFreqs.get(SourceUtils.getJVMSignature(type)[0]);
            return count == null ? 0 : count;
        }
        catch (InterruptedException ie) {
            return 0;
        }
    }

    public int getPackageReferenceCount(@NonNull ElementHandle<? extends PackageElement> pkg) {
        Parameters.notNull((CharSequence)"pkgName", pkg);
        if (pkg.getKind() != ElementKind.PACKAGE) {
            throw new IllegalArgumentException(pkg.toString());
        }
        try {
            this.init();
            Integer count = this.pkgFreqs.get(SourceUtils.getJVMSignature(pkg)[0]);
            return count == null ? 0 : count;
        }
        catch (InterruptedException ie) {
            return 0;
        }
    }

    @NonNull
    public Iterable<? extends ElementHandle<? extends TypeElement>> getUsedTypes() {
        try {
            this.init();
            return new AsHandlesIterable(this.typeFreqs.keySet(), (Convertor)new Convertor<String, ElementHandle<TypeElement>>(){

                @NonNull
                public ElementHandle<TypeElement> convert(@NonNull String p) {
                    return ElementHandleAccessor.getInstance().create(ElementKind.OTHER, p);
                }
            });
        }
        catch (InterruptedException ie) {
            return Collections.emptySet();
        }
    }

    @NonNull
    public Iterable<? extends ElementHandle<? extends PackageElement>> getUsedPackages() {
        try {
            this.init();
            return new AsHandlesIterable(this.pkgFreqs.keySet(), (Convertor)new Convertor<String, ElementHandle<PackageElement>>(){

                @NonNull
                public ElementHandle<PackageElement> convert(@NonNull String p) {
                    return ElementHandleAccessor.getInstance().create(ElementKind.PACKAGE, p);
                }
            });
        }
        catch (InterruptedException ie) {
            return Collections.emptySet();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void init() throws InterruptedException {
        Object object = this.lck;
        synchronized (object) {
            if (this.typeFreqs == null) {
                long st = System.currentTimeMillis();
                ClassIndexManager cim = ClassIndexManager.getDefault();
                HashMap<String, Integer> typef = new HashMap<String, Integer>();
                HashMap<String, Integer> pkgf = new HashMap<String, Integer>();
                try {
                    for (URL root : this.roots) {
                        ClassIndexImpl ci = cim.getUsagesQuery(root, true);
                        if (ci != null) {
                            ci.getReferencesFrequences(typef, pkgf);
                            continue;
                        }
                        if (!LOG.isLoggable(Level.FINE)) continue;
                        LOG.log(Level.FINE, "No ClasIndexImpl for root: {0} scan: {1}", new Object[]{root, SourceUtils.isScanInProgress()});
                    }
                    this.typeFreqs = Collections.unmodifiableMap(typef);
                    this.pkgFreqs = Collections.unmodifiableMap(pkgf);
                }
                catch (IOException ioe) {
                    this.typeFreqs = Collections.emptyMap();
                    this.pkgFreqs = Collections.emptyMap();
                }
                long et = System.currentTimeMillis();
                LOG.log(Level.FINE, "Frequencies calculation time: {0}ms.", et - st);
            }
        }
        assert (this.typeFreqs != null);
        assert (this.pkgFreqs != null);
    }

    @NonNull
    public static ReferencesCount get(@NonNull ClasspathInfo cpInfo) {
        Parameters.notNull((CharSequence)"cpInfo", (Object)cpInfo);
        List scp = cpInfo.getClassPath(ClasspathInfo.PathKind.SOURCE).entries();
        ArrayList<URL> roots = new ArrayList<URL>(scp.size());
        for (ClassPath.Entry e : scp) {
            roots.add(e.getURL());
        }
        return new ReferencesCount(roots);
    }

    @NonNull
    public static ReferencesCount get(@NonNull URL root) {
        Parameters.notNull((CharSequence)"cpInfo", (Object)root);
        return new ReferencesCount(Collections.singleton(root));
    }

    private static class AsHandlesIterator<P, R>
    implements Iterator<R> {
        private final Iterator<P> from;
        private final Convertor<P, R> fnc;

        private AsHandlesIterator(@NonNull Iterator<P> from, @NonNull Convertor<P, R> fnc) {
            assert (from != null);
            assert (fnc != null);
            this.from = from;
            this.fnc = fnc;
        }

        @Override
        public boolean hasNext() {
            return this.from.hasNext();
        }

        @Override
        public R next() {
            return (R)this.fnc.convert(this.from.next());
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Read only Collection.");
        }
    }

    private static class AsHandlesIterable<P, R>
    implements Iterable<R> {
        private final Iterable<P> from;
        private final Convertor<P, R> fnc;

        private AsHandlesIterable(@NonNull Iterable<P> from, @NonNull Convertor<P, R> fnc) {
            assert (from != null);
            assert (fnc != null);
            this.from = from;
            this.fnc = fnc;
        }

        @Override
        public Iterator<R> iterator() {
            return new AsHandlesIterator(this.from.iterator(), this.fnc);
        }
    }

}

