/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.doctree.DocCommentTree
 *  com.sun.source.doctree.DocTree
 *  com.sun.source.doctree.DocTree$Kind
 *  com.sun.source.doctree.DocTreeVisitor
 *  com.sun.source.doctree.ReferenceTree
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.BreakTree
 *  com.sun.source.tree.CaseTree
 *  com.sun.source.tree.CatchTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ContinueTree
 *  com.sun.source.tree.EnhancedForLoopTree
 *  com.sun.source.tree.ExpressionStatementTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.ForLoopTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.LabeledStatementTree
 *  com.sun.source.tree.LambdaExpressionTree
 *  com.sun.source.tree.MemberReferenceTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodInvocationTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.NewClassTree
 *  com.sun.source.tree.Scope
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.ThrowTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.TreeVisitor
 *  com.sun.source.tree.TryTree
 *  com.sun.source.tree.TypeParameterTree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.DocSourcePositions
 *  com.sun.source.util.DocTreePath
 *  com.sun.source.util.DocTreePathScanner
 *  com.sun.source.util.DocTrees
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreePathScanner
 *  com.sun.source.util.TreeScanner
 *  com.sun.source.util.Trees
 *  com.sun.tools.javac.api.JavacScope
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.api.JavacTrees
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$TypeSymbol
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.comp.Enter
 *  com.sun.tools.javac.comp.Env
 *  com.sun.tools.javac.comp.Resolve
 *  com.sun.tools.javac.tree.DCTree
 *  com.sun.tools.javac.tree.DCTree$DCReference
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCBlock
 *  com.sun.tools.javac.tree.JCTree$JCBreak
 *  com.sun.tools.javac.tree.JCTree$JCContinue
 *  com.sun.tools.javac.tree.JCTree$JCExpression
 *  com.sun.tools.javac.tree.JCTree$JCMethodDecl
 *  com.sun.tools.javac.tree.JCTree$JCModifiers
 *  com.sun.tools.javac.tree.JCTree$JCStatement
 *  com.sun.tools.javac.tree.TreeMaker
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.List
 *  com.sun.tools.javac.util.Name
 *  com.sun.tools.javac.util.Name$Table
 *  com.sun.tools.javac.util.Names
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.lexer.JavadocTokenId
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.lib.nbjavac.services.NBResolve
 *  org.netbeans.lib.nbjavac.services.NBTreeMaker
 *  org.netbeans.lib.nbjavac.services.NBTreeMaker$IndexedClassDecl
 */
package org.netbeans.api.java.source;

import com.sun.source.doctree.DocCommentTree;
import com.sun.source.doctree.DocTree;
import com.sun.source.doctree.DocTreeVisitor;
import com.sun.source.doctree.ReferenceTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.BreakTree;
import com.sun.source.tree.CaseTree;
import com.sun.source.tree.CatchTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ContinueTree;
import com.sun.source.tree.EnhancedForLoopTree;
import com.sun.source.tree.ExpressionStatementTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.ForLoopTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.LabeledStatementTree;
import com.sun.source.tree.LambdaExpressionTree;
import com.sun.source.tree.MemberReferenceTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.Scope;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.ThrowTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TreeVisitor;
import com.sun.source.tree.TryTree;
import com.sun.source.tree.TypeParameterTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.DocSourcePositions;
import com.sun.source.util.DocTreePath;
import com.sun.source.util.DocTreePathScanner;
import com.sun.source.util.DocTrees;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreePathScanner;
import com.sun.source.util.TreeScanner;
import com.sun.source.util.Trees;
import com.sun.tools.javac.api.JavacScope;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.api.JavacTrees;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.comp.Enter;
import com.sun.tools.javac.comp.Env;
import com.sun.tools.javac.comp.Resolve;
import com.sun.tools.javac.tree.DCTree;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;
import java.util.AbstractSet;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.UnionType;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.lexer.JavadocTokenId;
import org.netbeans.api.java.source.Comment;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.GeneratorUtilities;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.lib.nbjavac.services.NBResolve;
import org.netbeans.lib.nbjavac.services.NBTreeMaker;
import org.netbeans.modules.java.source.builder.CommentHandlerService;
import org.netbeans.modules.java.source.builder.CommentSetImpl;
import org.netbeans.modules.java.source.parsing.CompilationInfoImpl;
import org.netbeans.modules.java.source.pretty.ImportAnalysis2;
import org.netbeans.modules.java.source.transform.ImmutableDocTreeTranslator;
import org.netbeans.modules.java.source.transform.ImmutableTreeTranslator;

public final class TreeUtilities {
    public static final Set<Tree.Kind> CLASS_TREE_KINDS = EnumSet.of(Tree.Kind.ANNOTATION_TYPE, Tree.Kind.CLASS, Tree.Kind.ENUM, Tree.Kind.INTERFACE);
    private final CompilationInfo info;
    private final CommentHandlerService handler;
    static Set<Character> EXOTIC_ESCAPE = new HashSet<Character>(Arrays.asList(Character.valueOf('!'), Character.valueOf('#'), Character.valueOf('$'), Character.valueOf('%'), Character.valueOf('&'), Character.valueOf('('), Character.valueOf(')'), Character.valueOf('*'), Character.valueOf('+'), Character.valueOf(','), Character.valueOf('-'), Character.valueOf(':'), Character.valueOf('='), Character.valueOf('?'), Character.valueOf('@'), Character.valueOf('^'), Character.valueOf('_'), Character.valueOf('`'), Character.valueOf('{'), Character.valueOf('|'), Character.valueOf('}')));
    private static final Map<Character, Character> ESCAPE_UNENCODE;
    private static final Map<Character, Character> ESCAPE_ENCODE;

    TreeUtilities(CompilationInfo info) {
        assert (info != null);
        this.info = info;
        this.handler = CommentHandlerService.instance(info.impl.getJavacTask().getContext());
    }

    @Deprecated
    public boolean isClass(ClassTree tree) {
        return (((JCTree.JCModifiers)tree.getModifiers()).flags & 25088) == 0;
    }

    @Deprecated
    public boolean isInterface(ClassTree tree) {
        long flags = ((JCTree.JCModifiers)tree.getModifiers()).flags;
        return (flags & 512) != 0 && (flags & 8192) == 0;
    }

    @Deprecated
    public boolean isEnum(ClassTree tree) {
        return (((JCTree.JCModifiers)tree.getModifiers()).flags & 16384) != 0;
    }

    public boolean isEnumConstant(VariableTree tree) {
        return (((JCTree.JCModifiers)tree.getModifiers()).flags & 16384) != 0;
    }

    @Deprecated
    public boolean isAnnotation(ClassTree tree) {
        return (((JCTree.JCModifiers)tree.getModifiers()).flags & 8192) != 0;
    }

    public boolean isSynthetic(TreePath path) throws NullPointerException {
        if (path == null) {
            throw new NullPointerException();
        }
        while (path != null) {
            if (this.isSynthetic(path.getCompilationUnit(), path.getLeaf())) {
                return true;
            }
            path = path.getParentPath();
        }
        return false;
    }

    boolean isSynthetic(CompilationUnitTree cut, Tree leaf) throws NullPointerException {
        ExpressionStatementTree est;
        MethodInvocationTree mit;
        IdentifierTree it;
        JCTree tree = (JCTree)leaf;
        if (tree.pos == -1) {
            return true;
        }
        if (leaf.getKind() == Tree.Kind.METHOD) {
            return (((JCTree.JCMethodDecl)leaf).mods.flags & 0x1000000000L) != 0;
        }
        if (leaf.getKind() == Tree.Kind.EXPRESSION_STATEMENT && (est = (ExpressionStatementTree)leaf).getExpression().getKind() == Tree.Kind.METHOD_INVOCATION && (mit = (MethodInvocationTree)est.getExpression()).getMethodSelect().getKind() == Tree.Kind.IDENTIFIER && "super".equals((it = (IdentifierTree)mit.getMethodSelect()).getName().toString())) {
            SourcePositions sp = this.info.getTrees().getSourcePositions();
            return sp.getEndPosition(cut, leaf) == -1;
        }
        return false;
    }

    public java.util.List<Comment> getComments(Tree tree, boolean preceding) {
        CommentSetImpl set = this.handler.getComments(tree);
        TreeUtilities.ensureCommentsMapped(this.info, tree, set);
        java.util.List<Comment> comments = preceding ? set.getPrecedingComments() : set.getTrailingComments();
        return Collections.unmodifiableList(comments);
    }

    static void ensureCommentsMapped(CompilationInfo info, @NullAllowed Tree tree, CommentSetImpl set) {
        if (!set.areCommentsMapped() && tree != null) {
            TreePath tp;
            boolean assertsEnabled = false;
            boolean automap = true;
            if (!$assertionsDisabled) {
                assertsEnabled = true;
                if (!true) {
                    throw new AssertionError();
                }
            }
            TreePath treePath = tp = info.getCompilationUnit() == tree ? new TreePath(info.getCompilationUnit()) : TreePath.getPath((CompilationUnitTree)info.getCompilationUnit(), (Tree)tree);
            if (tp == null) {
                if (!(!assertsEnabled || info.getTreeUtilities().isSynthetic(info.getCompilationUnit(), tree) || info instanceof WorkingCopy && ((WorkingCopy)info).validateIsReplacement(tree))) {
                    Logger.getLogger(TreeUtilities.class.getName()).log(assertsEnabled ? Level.WARNING : Level.FINE, "Comment automap requested for Tree not from the root compilation info. Please, make sure to call GeneratorUtilities.importComments before Treeutilities.getComments. Tree: {0}", (Object)tree);
                    Logger.getLogger(TreeUtilities.class.getName()).log(assertsEnabled ? Level.INFO : Level.FINE, "Caller", new Exception());
                }
                automap = false;
            }
            if (automap) {
                GeneratorUtilities.importComments(info, tree, info.getCompilationUnit());
            }
        }
    }

    public TreePath pathFor(int pos) {
        return this.pathFor(new TreePath(this.info.getCompilationUnit()), pos);
    }

    public TreePath pathFor(TreePath path, int pos) {
        return this.pathFor(path, pos, this.info.getTrees().getSourcePositions());
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public TreePath pathFor(TreePath path, int pos, SourcePositions sourcePositions) {
        if (this.info == null) throw new IllegalArgumentException();
        if (path == null) throw new IllegalArgumentException();
        if (sourcePositions == null) {
            throw new IllegalArgumentException();
        }
        try {
            class PathFinder
            extends TreePathScanner<Void, Void> {
                private int pos;
                private SourcePositions sourcePositions;

                PathFinder(int pos, SourcePositions sourcePositions) {
                    this.pos = pos;
                    this.sourcePositions = sourcePositions;
                }

                public Void scan(Tree tree, Void p) {
                    if (tree != null && this.sourcePositions.getStartPosition(this.getCurrentPath().getCompilationUnit(), tree) < (long)this.pos && this.sourcePositions.getEndPosition(this.getCurrentPath().getCompilationUnit(), tree) >= (long)this.pos) {
                        class Result
                        extends Error {
                            TreePath path;

                            Result(TreePath path) {
                                this.path = path;
                            }
                        }
                        if (tree.getKind() == Tree.Kind.ERRONEOUS) {
                            tree.accept((TreeVisitor)this, (Object)p);
                            throw new Result(this.getCurrentPath());
                        }
                        TreePathScanner.super.scan(tree, (Object)p);
                        throw new Result(new TreePath(this.getCurrentPath(), tree));
                    }
                    return null;
                }

                public Void visitVariable(VariableTree node, Void p) {
                    int[] span = TreeUtilities.this.findNameSpan(node);
                    if (span != null && span[0] <= this.pos && this.pos < span[1]) {
                        throw new Result(this.getCurrentPath());
                    }
                    return (Void)TreePathScanner.super.visitVariable(node, (Object)p);
                }

                public Void visitMethod(MethodTree node, Void p) {
                    int[] span = TreeUtilities.this.findNameSpan(node);
                    if (span != null && span[0] <= this.pos && this.pos < span[1]) {
                        throw new Result(this.getCurrentPath());
                    }
                    return (Void)TreePathScanner.super.visitMethod(node, (Object)p);
                }
            }
            new PathFinder(pos, sourcePositions).scan(path, (Object)null);
        }
        catch (1Result result) {
            path = result.path;
        }
        if (path.getLeaf() == path.getCompilationUnit()) {
            return path;
        }
        TokenSequence<JavaTokenId> tokenList = this.tokensFor(path.getLeaf(), sourcePositions);
        tokenList.moveEnd();
        if (!tokenList.movePrevious()) return path;
        if (tokenList.offset() >= pos) return path;
        switch ((JavaTokenId)tokenList.token().id()) {
            case GTGTGT: 
            case GTGT: 
            case GT: {
                if (path.getLeaf().getKind() == Tree.Kind.MEMBER_SELECT) return path;
                if (CLASS_TREE_KINDS.contains((Object)path.getLeaf().getKind())) return path;
                if (path.getLeaf().getKind() == Tree.Kind.MEMBER_REFERENCE) return path;
                if (path.getLeaf().getKind() == Tree.Kind.GREATER_THAN) return path;
            }
            case RPAREN: {
                if (path.getLeaf().getKind() == Tree.Kind.PARENTHESIZED) {
                    return path.getParentPath();
                }
                if (path.getLeaf().getKind() == Tree.Kind.ENHANCED_FOR_LOOP) return path;
                if (path.getLeaf().getKind() == Tree.Kind.FOR_LOOP) return path;
                if (path.getLeaf().getKind() == Tree.Kind.IF) return path;
                if (path.getLeaf().getKind() == Tree.Kind.WHILE_LOOP) return path;
                if (path.getLeaf().getKind() == Tree.Kind.DO_WHILE_LOOP) return path;
                if (path.getLeaf().getKind() == Tree.Kind.TYPE_CAST) return path;
                if (path.getLeaf().getKind() == Tree.Kind.LAMBDA_EXPRESSION) return path;
            }
            case SEMICOLON: {
                if (path.getLeaf().getKind() == Tree.Kind.EMPTY_STATEMENT) return path;
                if (path.getLeaf().getKind() == Tree.Kind.FOR_LOOP && (long)tokenList.offset() <= sourcePositions.getStartPosition(path.getCompilationUnit(), (Tree)((ForLoopTree)path.getLeaf()).getUpdate().get(0))) return path;
            }
            case RBRACE: {
                path = path.getParentPath();
                switch (path.getLeaf().getKind()) {
                    case CATCH: {
                        path = path.getParentPath();
                    }
                    case METHOD: 
                    case FOR_LOOP: 
                    case ENHANCED_FOR_LOOP: 
                    case SYNCHRONIZED: 
                    case WHILE_LOOP: 
                    case TRY: {
                        return path.getParentPath();
                    }
                    case IF: {
                        do {
                            if ((path = path.getParentPath()) == null) return path;
                        } while (path.getLeaf().getKind() == Tree.Kind.IF);
                    }
                }
            }
        }
        return path;
    }

    public DocTreePath pathFor(TreePath treepath, DocCommentTree doc, int pos) {
        return this.pathFor(new DocTreePath(treepath, doc), pos);
    }

    public DocTreePath pathFor(DocTreePath path, int pos) {
        return this.pathFor(path, pos, this.info.getDocTrees().getSourcePositions());
    }

    public DocTreePath pathFor(DocTreePath path, int pos, DocSourcePositions sourcePositions) {
        if (this.info == null || path == null || sourcePositions == null) {
            throw new IllegalArgumentException();
        }
        try {
            class PathFinder
            extends DocTreePathScanner<Void, TreePath> {
                private int pos;
                private DocSourcePositions sourcePositions;

                PathFinder(int pos, DocSourcePositions sourcePositions) {
                    this.pos = pos;
                    this.sourcePositions = sourcePositions;
                }

                public Void scan(DocTree tree, TreePath p) {
                    if (tree != null && this.sourcePositions.getStartPosition(p.getCompilationUnit(), this.getCurrentPath().getDocComment(), tree) < (long)this.pos && this.sourcePositions.getEndPosition(p.getCompilationUnit(), this.getCurrentPath().getDocComment(), tree) >= (long)this.pos) {
                        class Result
                        extends Error {
                            DocTreePath path;

                            Result(DocTreePath path) {
                                this.path = path;
                            }
                        }
                        if (tree.getKind() == DocTree.Kind.ERRONEOUS) {
                            tree.accept((DocTreeVisitor)this, (Object)p);
                            throw new Result(this.getCurrentPath());
                        }
                        DocTreePathScanner.super.scan(tree, (Object)p);
                        throw new Result(new DocTreePath(this.getCurrentPath(), tree));
                    }
                    return null;
                }
            }
            new PathFinder(pos, sourcePositions).scan(path, (Object)path.getTreePath());
        }
        catch (2Result result) {
            path = result.path;
        }
        if (path.getLeaf() == path.getDocComment()) {
            return path;
        }
        return path;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public TypeMirror parseType(String expr, TypeElement scope) {
        Enter enter = Enter.instance((Context)this.info.impl.getJavacTask().getContext());
        TreeMaker jcMaker = TreeMaker.instance((Context)this.info.impl.getJavacTask().getContext());
        int oldPos = jcMaker.pos;
        try {
            if (enter.getClassEnv((Symbol.TypeSymbol)scope) == null && this.info.getTrees().getTree(scope) == null) {
                TypeMirror typeMirror = null;
                return typeMirror;
            }
            Type type = this.info.impl.getJavacTask().parseType(expr, scope);
            return type;
        }
        finally {
            jcMaker.pos = oldPos;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    Tree parseType(String expr) {
        TreeMaker jcMaker = TreeMaker.instance((Context)this.info.impl.getJavacTask().getContext());
        int oldPos = jcMaker.pos;
        try {
            Tree tree = this.info.impl.getJavacTask().parseType(expr);
            return tree;
        }
        finally {
            jcMaker.pos = oldPos;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public StatementTree parseStatement(String stmt, SourcePositions[] sourcePositions) {
        TreeMaker jcMaker = TreeMaker.instance((Context)this.info.impl.getJavacTask().getContext());
        int oldPos = jcMaker.pos;
        try {
            JCTree.JCStatement jCStatement = this.info.impl.getJavacTask().parseStatement((CharSequence)stmt, sourcePositions);
            return jCStatement;
        }
        finally {
            jcMaker.pos = oldPos;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public ExpressionTree parseExpression(String expr, SourcePositions[] sourcePositions) {
        TreeMaker jcMaker = TreeMaker.instance((Context)this.info.impl.getJavacTask().getContext());
        int oldPos = jcMaker.pos;
        try {
            JCTree.JCExpression jCExpression = this.info.impl.getJavacTask().parseExpression((CharSequence)expr, sourcePositions);
            return jCExpression;
        }
        finally {
            jcMaker.pos = oldPos;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public ExpressionTree parseVariableInitializer(String init, SourcePositions[] sourcePositions) {
        TreeMaker jcMaker = TreeMaker.instance((Context)this.info.impl.getJavacTask().getContext());
        int oldPos = jcMaker.pos;
        try {
            JCTree.JCExpression jCExpression = this.info.impl.getJavacTask().parseVariableInitializer((CharSequence)init, sourcePositions);
            return jCExpression;
        }
        finally {
            jcMaker.pos = oldPos;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public BlockTree parseStaticBlock(String block, SourcePositions[] sourcePositions) {
        TreeMaker jcMaker = TreeMaker.instance((Context)this.info.impl.getJavacTask().getContext());
        int oldPos = jcMaker.pos;
        try {
            JCTree.JCBlock jCBlock = this.info.impl.getJavacTask().parseStaticBlock((CharSequence)block, sourcePositions);
            return jCBlock;
        }
        finally {
            jcMaker.pos = oldPos;
        }
    }

    public Scope scopeFor(int pos) {
        java.util.List<StatementTree> stmts = null;
        SourcePositions sourcePositions = this.info.getTrees().getSourcePositions();
        TreePath path = this.pathFor(pos);
        CompilationUnitTree root = path.getCompilationUnit();
        switch (path.getLeaf().getKind()) {
            case BLOCK: {
                stmts = ((BlockTree)path.getLeaf()).getStatements();
                break;
            }
            case FOR_LOOP: {
                stmts = ((ForLoopTree)path.getLeaf()).getInitializer();
                break;
            }
            case ENHANCED_FOR_LOOP: {
                stmts = Collections.singletonList(((EnhancedForLoopTree)path.getLeaf()).getStatement());
                break;
            }
            case CASE: {
                stmts = ((CaseTree)path.getLeaf()).getStatements();
                break;
            }
            case METHOD: {
                stmts = ((MethodTree)path.getLeaf()).getParameters();
            }
        }
        if (stmts != null) {
            StatementTree tree = null;
            for (StatementTree st : stmts) {
                if (sourcePositions.getStartPosition(root, (Tree)st) >= (long)pos) continue;
                tree = st;
            }
            if (tree != null) {
                path = new TreePath(path, (Tree)tree);
            }
        }
        Scope scope = this.info.getTrees().getScope(path);
        if (CLASS_TREE_KINDS.contains((Object)path.getLeaf().getKind())) {
            TokenSequence ts = this.info.getTokenHierarchy().tokenSequence(JavaTokenId.language());
            ts.move(pos);
            block12 : while (ts.movePrevious()) {
                switch ((JavaTokenId)ts.token().id()) {
                    case WHITESPACE: 
                    case LINE_COMMENT: 
                    case BLOCK_COMMENT: 
                    case JAVADOC_COMMENT: {
                        continue block12;
                    }
                    case EXTENDS: 
                    case IMPLEMENTS: {
                        ((JavacScope)scope).getEnv().baseClause = true;
                    }
                }
                return scope;
            }
        }
        return scope;
    }

    public Scope toScopeWithDisabledAccessibilityChecks(Scope scope) {
        return new NBScope((JavacScope)scope);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public TypeMirror attributeTree(Tree tree, Scope scope) {
        if (scope instanceof NBScope && ((NBScope)scope).areAccessibilityChecksDisabled()) {
            NBResolve.instance((Context)this.info.impl.getJavacTask().getContext()).disableAccessibilityChecks();
        }
        try {
            Type type = this.info.impl.getJavacTask().attributeTree((JCTree)tree, ((JavacScope)scope).getEnv());
            return type;
        }
        finally {
            NBResolve.instance((Context)this.info.impl.getJavacTask().getContext()).restoreAccessbilityChecks();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Scope attributeTreeTo(Tree tree, Scope scope, Tree to) {
        if (scope instanceof NBScope && ((NBScope)scope).areAccessibilityChecksDisabled()) {
            NBResolve.instance((Context)this.info.impl.getJavacTask().getContext()).disableAccessibilityChecks();
        }
        try {
            JavacScope javacScope = this.info.impl.getJavacTask().attributeTreeTo((JCTree)tree, ((JavacScope)scope).getEnv(), (JCTree)to);
            return javacScope;
        }
        finally {
            NBResolve.instance((Context)this.info.impl.getJavacTask().getContext()).restoreAccessbilityChecks();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public TypeMirror reattributeTree(Tree tree, Scope scope) {
        Env env = ((JavacScope)scope).getEnv();
        this.copyInnerClassIndexes((Tree)env.tree, tree);
        if (scope instanceof NBScope && ((NBScope)scope).areAccessibilityChecksDisabled()) {
            NBResolve.instance((Context)this.info.impl.getJavacTask().getContext()).disableAccessibilityChecks();
        }
        try {
            Type type = this.info.impl.getJavacTask().attributeTree((JCTree)tree, env);
            return type;
        }
        finally {
            NBResolve.instance((Context)this.info.impl.getJavacTask().getContext()).restoreAccessbilityChecks();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Scope reattributeTreeTo(Tree tree, Scope scope, Tree to) {
        Env env = ((JavacScope)scope).getEnv();
        this.copyInnerClassIndexes((Tree)env.tree, tree);
        if (scope instanceof NBScope && ((NBScope)scope).areAccessibilityChecksDisabled()) {
            NBResolve.instance((Context)this.info.impl.getJavacTask().getContext()).disableAccessibilityChecks();
        }
        try {
            JavacScope javacScope = this.info.impl.getJavacTask().attributeTreeTo((JCTree)tree, env, (JCTree)to);
            return javacScope;
        }
        finally {
            NBResolve.instance((Context)this.info.impl.getJavacTask().getContext()).restoreAccessbilityChecks();
        }
    }

    public TokenSequence<JavaTokenId> tokensFor(Tree tree) {
        return this.tokensFor(tree, this.info.getTrees().getSourcePositions());
    }

    public TokenSequence<JavaTokenId> tokensFor(Tree tree, SourcePositions sourcePositions) {
        int start = (int)sourcePositions.getStartPosition(this.info.getCompilationUnit(), tree);
        int end = (int)sourcePositions.getEndPosition(this.info.getCompilationUnit(), tree);
        return this.info.getTokenHierarchy().tokenSequence(JavaTokenId.language()).subSequence(start, end);
    }

    public boolean isAccessible(Scope scope, Element member, TypeMirror type) {
        return type instanceof DeclaredType ? this.info.getTrees().isAccessible(scope, member, (DeclaredType)type) : false;
    }

    public boolean isStaticContext(Scope scope) {
        Resolve.instance((Context)this.info.impl.getJavacTask().getContext());
        return Resolve.isStatic((Env)((JavacScope)scope).getEnv());
    }

    public Set<TypeMirror> getUncaughtExceptions(TreePath path) {
        UnrelatedTypeMirrorSet set = new UnrelatedTypeMirrorSet(this.info.getTypes());
        new UncaughtExceptionsVisitor(this.info).scan(path, (Object)set);
        return set;
    }

    public int[] findBodySpan(ClassTree clazz) {
        JCTree jcTree = (JCTree)clazz;
        int pos = jcTree.pos;
        if (pos < 0) {
            return null;
        }
        TokenSequence tokenSequence = this.info.getTokenHierarchy().tokenSequence(JavaTokenId.language());
        tokenSequence.move(pos);
        int startPos = -1;
        int endPos = (int)this.info.getTrees().getSourcePositions().getEndPosition(this.info.getCompilationUnit(), (Tree)clazz);
        while (tokenSequence.moveNext()) {
            if (tokenSequence.token().id() != JavaTokenId.LBRACE) continue;
            startPos = tokenSequence.offset();
            break;
        }
        if (startPos == -1 || endPos == -1) {
            return null;
        }
        return new int[]{startPos, endPos};
    }

    public int[] findNameSpan(ClassTree clazz) {
        return this.findNameSpan(clazz.getSimpleName().toString(), (Tree)clazz, JavaTokenId.CLASS, JavaTokenId.INTERFACE, JavaTokenId.ENUM, JavaTokenId.AT, JavaTokenId.WHITESPACE, JavaTokenId.BLOCK_COMMENT, JavaTokenId.LINE_COMMENT, JavaTokenId.JAVADOC_COMMENT);
    }

    public int[] findNameSpan(MethodTree method) {
        String name;
        if (this.isSynthetic(this.info.getCompilationUnit(), (Tree)method)) {
            return null;
        }
        JCTree.JCMethodDecl jcm = (JCTree.JCMethodDecl)method;
        if (jcm.name == jcm.name.table.names.init) {
            Element clazz;
            TreePath path = this.info.getTrees().getPath(this.info.getCompilationUnit(), (Tree)jcm);
            if (path == null) {
                return null;
            }
            Element em = this.info.getTrees().getElement(path);
            if (em == null || (clazz = em.getEnclosingElement()) == null || !clazz.getKind().isClass()) {
                return null;
            }
            name = clazz.getSimpleName().toString();
        } else {
            name = method.getName().toString();
        }
        return this.findNameSpan(name, (Tree)method, new JavaTokenId[0]);
    }

    public int[] findNameSpan(VariableTree var) {
        return this.findNameSpan(var.getName().toString(), (Tree)var, new JavaTokenId[0]);
    }

    public int[] findNameSpan(LabeledStatementTree lst) {
        return this.findNameSpan(lst.getLabel().toString(), (Tree)lst, new JavaTokenId[0]);
    }

    public int[] findNameSpan(TypeParameterTree tpt) {
        return this.findNameSpan(tpt.getName().toString(), (Tree)tpt, new JavaTokenId[0]);
    }

    public int[] findNameSpan(BreakTree brk) {
        return this.findNameSpan(brk.getLabel().toString(), (Tree)brk, new JavaTokenId[0]);
    }

    public int[] findNameSpan(ContinueTree cont) {
        return this.findNameSpan(cont.getLabel().toString(), (Tree)cont, new JavaTokenId[0]);
    }

    @CheckForNull
    public int[] findMethodParameterSpan(@NonNull MethodTree method) {
        JCTree jcTree = (JCTree)method;
        int pos = jcTree.pos;
        if (pos < 0) {
            return null;
        }
        TokenSequence tokenSequence = this.info.getTokenHierarchy().tokenSequence(JavaTokenId.language());
        tokenSequence.move(pos);
        int startPos = -1;
        int endPos = -1;
        while (tokenSequence.moveNext()) {
            if (tokenSequence.token().id() == JavaTokenId.LPAREN) {
                startPos = tokenSequence.offset();
            }
            if (tokenSequence.token().id() == JavaTokenId.RPAREN) {
                endPos = tokenSequence.offset();
                break;
            }
            if (tokenSequence.token().id() != JavaTokenId.LBRACE) continue;
            return null;
        }
        if (startPos == -1 || endPos == -1) {
            return null;
        }
        return new int[]{startPos, endPos};
    }

    public int[] findNameSpan(MemberSelectTree mst) {
        return this.findNameSpan(mst.getIdentifier().toString(), (Tree)mst, JavaTokenId.DOT, JavaTokenId.WHITESPACE, JavaTokenId.BLOCK_COMMENT, JavaTokenId.LINE_COMMENT, JavaTokenId.JAVADOC_COMMENT);
    }

    public int[] findNameSpan(MemberReferenceTree mst) {
        return this.findNameSpan(mst.getName().toString(), (Tree)mst, JavaTokenId.DOT, JavaTokenId.WHITESPACE, JavaTokenId.BLOCK_COMMENT, JavaTokenId.LINE_COMMENT, JavaTokenId.JAVADOC_COMMENT);
    }

    public int[] findNameSpan(DocCommentTree docTree, ReferenceTree ref) {
        boolean wasNext;
        Name name = ((DCTree.DCReference)ref).memberName;
        if (name == null || !SourceVersion.isIdentifier((CharSequence)name)) {
            return null;
        }
        int pos = (int)this.info.getDocTrees().getSourcePositions().getStartPosition(this.info.getCompilationUnit(), docTree, (DocTree)ref);
        if (pos < 0) {
            return null;
        }
        TokenSequence tokenSequence = this.info.getTokenHierarchy().tokenSequence(JavaTokenId.language());
        tokenSequence.move(pos);
        if (!tokenSequence.moveNext() || tokenSequence.token().id() != JavaTokenId.JAVADOC_COMMENT) {
            return null;
        }
        TokenSequence jdocTS = tokenSequence.embedded(JavadocTokenId.language());
        jdocTS.move(pos);
        while ((wasNext = jdocTS.moveNext()) && jdocTS.token().id() != JavadocTokenId.HASH) {
        }
        if (wasNext && jdocTS.moveNext() && jdocTS.token().id() == JavadocTokenId.IDENT && name.contentEquals(jdocTS.token().text())) {
            return new int[]{jdocTS.offset(), jdocTS.offset() + jdocTS.token().length()};
        }
        return null;
    }

    private /* varargs */ int[] findNameSpan(String name, Tree t, JavaTokenId ... allowedTokens) {
        boolean wasNext;
        if (!SourceVersion.isIdentifier(name)) {
            return null;
        }
        JCTree jcTree = (JCTree)t;
        int pos = jcTree.pos;
        if (pos < 0) {
            return null;
        }
        EnumSet<JavaTokenId> allowedTokensSet = EnumSet.noneOf(JavaTokenId.class);
        allowedTokensSet.addAll(Arrays.asList(allowedTokens));
        TokenSequence tokenSequence = this.info.getTokenHierarchy().tokenSequence(JavaTokenId.language());
        tokenSequence.move(pos);
        while ((wasNext = tokenSequence.moveNext()) && allowedTokensSet.contains((Object)tokenSequence.token().id())) {
        }
        if (wasNext && tokenSequence.token().id() == JavaTokenId.IDENTIFIER) {
            boolean nameMatches = name.contentEquals(tokenSequence.token().text());
            if (!nameMatches) {
                ExpressionTree expr = this.info.getTreeUtilities().parseExpression(tokenSequence.token().text().toString(), new SourcePositions[1]);
                boolean bl = nameMatches = expr.getKind() == Tree.Kind.IDENTIFIER && name.contentEquals(((IdentifierTree)expr).getName());
            }
            if (nameMatches) {
                return new int[]{tokenSequence.offset(), tokenSequence.offset() + tokenSequence.token().length()};
            }
        }
        tokenSequence.move(pos);
        if (tokenSequence.moveNext() && tokenSequence.token().id() == JavaTokenId.JAVADOC_COMMENT) {
            return new int[]{pos + 1, pos + name.length() + 1};
        }
        return null;
    }

    public StatementTree getBreakContinueTarget(TreePath breakOrContinue) throws IllegalArgumentException {
        if (this.info.getPhase().compareTo(JavaSource.Phase.RESOLVED) < 0) {
            throw new IllegalArgumentException("Not in correct Phase. Required: Phase.RESOLVED, got: Phase." + this.info.getPhase().toString());
        }
        Tree leaf = breakOrContinue.getLeaf();
        switch (leaf.getKind()) {
            case BREAK: {
                return (StatementTree)((JCTree.JCBreak)leaf).target;
            }
            case CONTINUE: {
                StatementTree target = (StatementTree)((JCTree.JCContinue)leaf).target;
                if (target == null) {
                    return null;
                }
                if (((JCTree.JCContinue)leaf).label == null) {
                    return target;
                }
                TreePath tp = breakOrContinue;
                while (tp.getLeaf() != target) {
                    tp = tp.getParentPath();
                }
                Tree parent = tp.getParentPath().getLeaf();
                if (parent.getKind() == Tree.Kind.LABELED_STATEMENT) {
                    return (StatementTree)parent;
                }
                return target;
            }
        }
        throw new IllegalArgumentException("Unsupported kind: " + (Object)leaf.getKind());
    }

    @NonNull
    public CharSequence decodeIdentifier(@NonNull CharSequence text) {
        return TreeUtilities.decodeIdentifierInternal(text);
    }

    @NonNull
    public CharSequence encodeIdentifier(@NonNull CharSequence ident) {
        return TreeUtilities.encodeIdentifierInternal(ident);
    }

    @NonNull
    static CharSequence decodeIdentifierInternal(@NonNull CharSequence text) {
        if (text.charAt(0) != '#') {
            return text;
        }
        int count = text.charAt(text.length() - 1) == '\"' ? text.length() - 1 : text.length();
        StringBuilder sb = new StringBuilder(text.length());
        for (int c = 2; c < count; ++c) {
            if (text.charAt(c) == '\\' && ++c < count) {
                if (EXOTIC_ESCAPE.contains(Character.valueOf(text.charAt(c)))) {
                    sb.append('\\');
                    sb.append(text.charAt(c));
                    continue;
                }
                Character remaped = ESCAPE_UNENCODE.get(Character.valueOf(text.charAt(c)));
                if (remaped != null) {
                    sb.append(remaped);
                    continue;
                }
                sb.append(text.charAt(c));
                continue;
            }
            sb.append(text.charAt(c));
        }
        return sb.toString();
    }

    @NonNull
    static CharSequence encodeIdentifierInternal(@NonNull CharSequence ident) {
        if (ident.length() == 0) {
            return ident;
        }
        StringBuilder sb = new StringBuilder(ident.length());
        boolean needsExotic = Character.isJavaIdentifierStart(ident.charAt(0));
        for (int i = 0; i < ident.length(); ++i) {
            char c = ident.charAt(i);
            if (Character.isJavaIdentifierPart(c)) {
                sb.append(c);
                continue;
            }
            needsExotic = true;
            Character target = ESCAPE_ENCODE.get(Character.valueOf(c));
            if (target != null) {
                sb.append('\\');
                sb.append(target);
                continue;
            }
            sb.append(c);
        }
        if (needsExotic) {
            sb.append("\"");
            sb.insert(0, "#\"");
            return sb.toString();
        }
        return ident;
    }

    @NonNull
    public Tree translate(@NonNull Tree original, @NonNull Map<? extends Tree, ? extends Tree> original2Translated) {
        return this.translate(original, original2Translated, (ImportAnalysis2)new NoImports(this.info), null);
    }

    @NonNull
    Tree translate(@NonNull Tree original, final @NonNull Map<? extends Tree, ? extends Tree> original2Translated, ImportAnalysis2 ia, Map<Tree, Object> tree2Tag) {
        ImmutableTreeTranslator itt = new ImmutableTreeTranslator(this.info instanceof WorkingCopy ? (WorkingCopy)this.info : null){
            @NonNull
            private Map<Tree, Tree> map;

            @Override
            public Tree translate(Tree tree) {
                Tree translated = this.map.remove((Object)tree);
                if (translated != null) {
                    return this.translate(translated);
                }
                return super.translate(tree);
            }
        };
        Context c = this.info.impl.getJavacTask().getContext();
        itt.attach(c, ia, tree2Tag);
        return itt.translate(original);
    }

    @NonNull
    public DocTree translate(@NonNull DocTree original, @NonNull Map<? extends DocTree, ? extends DocTree> original2Translated) {
        return this.translate(original, original2Translated, (ImportAnalysis2)new NoImports(this.info), null);
    }

    @NonNull
    DocTree translate(@NonNull DocTree original, final @NonNull Map<? extends DocTree, ? extends DocTree> original2Translated, ImportAnalysis2 ia, Map<Tree, Object> tree2Tag) {
        ImmutableDocTreeTranslator itt = new ImmutableDocTreeTranslator(this.info instanceof WorkingCopy ? (WorkingCopy)this.info : null){
            @NonNull
            private Map<DocTree, DocTree> map;

            @Override
            public DocTree translate(DocTree tree) {
                DocTree translated = this.map.remove((Object)tree);
                if (translated != null) {
                    return this.translate(translated);
                }
                return super.translate(tree);
            }
        };
        Context c = this.info.impl.getJavacTask().getContext();
        itt.attach(c, ia, tree2Tag);
        return itt.translate(original);
    }

    private void copyInnerClassIndexes(Tree from, Tree to) {
        final int[] fromIdx = new int[]{-3};
        TreeScanner scanner = new TreeScanner<Void, Void>(){

            public Void scan(Tree node, Void p) {
                return fromIdx[0] < -1 ? (Void)TreeScanner.super.scan(node, (Object)p) : null;
            }

            public Void visitClass(ClassTree node, Void p) {
                if (fromIdx[0] < -2) {
                    return (Void)TreeScanner.super.visitClass(node, (Object)p);
                }
                fromIdx[0] = ((NBTreeMaker.IndexedClassDecl)node).index;
                return null;
            }

            public Void visitMethod(MethodTree node, Void p) {
                return fromIdx[0] < -2 ? (Void)TreeScanner.super.visitMethod(node, (Object)p) : null;
            }

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            public Void visitBlock(BlockTree node, Void p) {
                int old = fromIdx[0];
                fromIdx[0] = -2;
                try {
                    Void void_ = (Void)TreeScanner.super.visitBlock(node, (Object)p);
                    return void_;
                }
                finally {
                    if (fromIdx[0] < -1) {
                        fromIdx[0] = old;
                    }
                }
            }
        };
        scanner.scan(from, (Object)null);
        if (fromIdx[0] < -1) {
            return;
        }
        scanner = new TreeScanner<Void, Void>(){

            public Void visitClass(ClassTree node, Void p) {
                int[] arrn = fromIdx;
                int n = arrn[0];
                arrn[0] = n + 1;
                ((NBTreeMaker.IndexedClassDecl)node).index = n;
                return null;
            }
        };
        scanner.scan(to, (Object)null);
    }

    public boolean isCompileTimeConstantExpression(TreePath expression) {
        TypeMirror attributeTree = this.info.getTrees().getTypeMirror(expression);
        Type attributeTreeImpl = (Type)attributeTree;
        return attributeTreeImpl != null && attributeTreeImpl.constValue() != null;
    }

    @CheckForNull
    public ExpressionTree getReferenceClass(@NonNull DocTreePath path) {
        TreePath tp = path.getTreePath();
        DCTree.DCReference ref = (DCTree.DCReference)path.getLeaf();
        ((JavacTrees)this.info.getTrees()).ensureDocReferenceAttributed(tp, ref);
        return (ExpressionTree)ref.qualifierExpression;
    }

    @CheckForNull
    public javax.lang.model.element.Name getReferenceName(@NonNull DocTreePath path) {
        return ((DCTree.DCReference)path.getLeaf()).memberName;
    }

    @CheckForNull
    public java.util.List<? extends Tree> getReferenceParameters(@NonNull DocTreePath path) {
        TreePath tp = path.getTreePath();
        DCTree.DCReference ref = (DCTree.DCReference)path.getLeaf();
        ((JavacTrees)this.info.getTrees()).ensureDocReferenceAttributed(tp, ref);
        return ref.paramTypes;
    }

    static {
        HashMap<Character, Character> unencode = new HashMap<Character, Character>();
        unencode.put(Character.valueOf('n'), Character.valueOf('\n'));
        unencode.put(Character.valueOf('t'), Character.valueOf('\t'));
        unencode.put(Character.valueOf('b'), Character.valueOf('\b'));
        unencode.put(Character.valueOf('r'), Character.valueOf('\r'));
        ESCAPE_UNENCODE = Collections.unmodifiableMap(unencode);
        HashMap<Character, Character> encode = new HashMap<Character, Character>();
        encode.put(Character.valueOf('\n'), Character.valueOf('n'));
        encode.put(Character.valueOf('\t'), Character.valueOf('t'));
        encode.put(Character.valueOf('\b'), Character.valueOf('b'));
        encode.put(Character.valueOf('\r'), Character.valueOf('r'));
        ESCAPE_ENCODE = Collections.unmodifiableMap(encode);
    }

    private static final class NBScope
    extends JavacScope {
        private NBScope(JavacScope scope) {
            super(scope.getEnv());
        }

        private boolean areAccessibilityChecksDisabled() {
            return true;
        }
    }

    private static class UnrelatedTypeMirrorSet
    extends AbstractSet<TypeMirror> {
        private Types types;
        private LinkedList<TypeMirror> list = new LinkedList();

        public UnrelatedTypeMirrorSet(Types types) {
            this.types = types;
        }

        @Override
        public boolean add(TypeMirror typeMirror) {
            ListIterator<TypeMirror> it = this.list.listIterator();
            while (it.hasNext()) {
                TypeMirror tm = it.next();
                if (this.types.isSubtype(typeMirror, tm)) {
                    return false;
                }
                if (!this.types.isSubtype(tm, typeMirror)) continue;
                it.remove();
            }
            return this.list.add(typeMirror);
        }

        @Override
        public Iterator<TypeMirror> iterator() {
            return this.list.iterator();
        }

        @Override
        public int size() {
            return this.list.size();
        }
    }

    private static class UncaughtExceptionsVisitor
    extends TreePathScanner<Void, Set<TypeMirror>> {
        private final CompilationInfo info;

        private UncaughtExceptionsVisitor(CompilationInfo info) {
            this.info = info;
        }

        public Void visitMethodInvocation(MethodInvocationTree node, Set<TypeMirror> p) {
            TreePathScanner.super.visitMethodInvocation(node, p);
            Element el = this.info.getTrees().getElement(this.getCurrentPath());
            if (el != null && el.getKind() == ElementKind.METHOD) {
                p.addAll(((ExecutableElement)el).getThrownTypes());
            }
            return null;
        }

        public Void visitNewClass(NewClassTree node, Set<TypeMirror> p) {
            TreePathScanner.super.visitNewClass(node, p);
            Element el = this.info.getTrees().getElement(this.getCurrentPath());
            if (el != null && el.getKind() == ElementKind.CONSTRUCTOR) {
                p.addAll(((ExecutableElement)el).getThrownTypes());
            }
            return null;
        }

        public Void visitThrow(ThrowTree node, Set<TypeMirror> p) {
            TreePathScanner.super.visitThrow(node, p);
            TypeMirror tm = this.info.getTrees().getTypeMirror(new TreePath(this.getCurrentPath(), (Tree)node.getExpression()));
            if (tm != null) {
                if (tm.getKind() == TypeKind.DECLARED) {
                    p.add(tm);
                } else if (tm.getKind() == TypeKind.UNION) {
                    p.addAll(((UnionType)tm).getAlternatives());
                }
            }
            return null;
        }

        public Void visitTry(TryTree node, Set<TypeMirror> p) {
            LinkedHashSet<? extends TypeMirror> s = new LinkedHashSet<TypeMirror>();
            Trees trees = this.info.getTrees();
            Types types = this.info.getTypes();
            Elements elements = this.info.getElements();
            for (Tree res : node.getResources()) {
                TypeMirror resType = trees.getTypeMirror(new TreePath(this.getCurrentPath(), res));
                if (resType == null || resType.getKind() != TypeKind.DECLARED) continue;
                for (ExecutableElement method : ElementFilter.methodsIn(elements.getAllMembers((TypeElement)((DeclaredType)resType).asElement()))) {
                    if (!"close".contentEquals(method.getSimpleName()) || !method.getParameters().isEmpty() || !method.getTypeParameters().isEmpty()) continue;
                    s.addAll(method.getThrownTypes());
                }
            }
            this.scan((Tree)node.getBlock(), s);
            LinkedHashSet<TypeMirror> c = new LinkedHashSet<TypeMirror>();
            for (CatchTree ct : node.getCatches()) {
                TypeMirror t = trees.getTypeMirror(new TreePath(this.getCurrentPath(), ct.getParameter().getType()));
                if (t == null) continue;
                if (t.getKind() == TypeKind.UNION) {
                    for (TypeMirror tm : ((UnionType)t).getAlternatives()) {
                        if (tm == null || tm.getKind() == TypeKind.ERROR) continue;
                        c.add(tm);
                    }
                    continue;
                }
                if (t.getKind() == TypeKind.ERROR) continue;
                c.add(t);
            }
            for (TypeMirror t : c) {
                Iterator it = s.iterator();
                while (it.hasNext()) {
                    if (!types.isSubtype((TypeMirror)it.next(), t)) continue;
                    it.remove();
                }
            }
            p.addAll(s);
            this.scan((Iterable)node.getCatches(), p);
            this.scan((Tree)node.getFinallyBlock(), p);
            return null;
        }

        public Void visitMethod(MethodTree node, Set<TypeMirror> p) {
            LinkedHashSet s = new LinkedHashSet();
            this.scan((Tree)node.getBody(), s);
            for (ExpressionTree et : node.getThrows()) {
                TypeMirror t = this.info.getTrees().getTypeMirror(new TreePath(this.getCurrentPath(), (Tree)et));
                if (t == null || t.getKind() == TypeKind.ERROR) continue;
                Iterator it = s.iterator();
                while (it.hasNext()) {
                    if (!this.info.getTypes().isSubtype((TypeMirror)it.next(), t)) continue;
                    it.remove();
                }
            }
            p.addAll(s);
            return null;
        }

        public Void visitClass(ClassTree node, Set<TypeMirror> p) {
            return null;
        }

        public Void visitLambdaExpression(LambdaExpressionTree node, Set<TypeMirror> p) {
            return null;
        }
    }

    private static final class NoImports
    extends ImportAnalysis2 {
        public NoImports(CompilationInfo info) {
            super(info);
        }

        @Override
        public void classEntered(ClassTree clazz) {
        }

        @Override
        public void enterVisibleThroughClasses(ClassTree clazz) {
        }

        @Override
        public void classLeft() {
        }

        @Override
        public ExpressionTree resolveImport(MemberSelectTree orig, Element element) {
            return orig;
        }

        @Override
        public void setCompilationUnit(CompilationUnitTree cut) {
        }

        @Override
        public void setImports(java.util.List<? extends ImportTree> importsToAdd) {
        }

        @Override
        public Set<? extends Element> getImports() {
            return Collections.emptySet();
        }

        @Override
        public void setPackage(ExpressionTree packageNameTree) {
        }
    }

}

