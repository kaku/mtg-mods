/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.java.source;

import java.util.EventObject;
import javax.lang.model.element.TypeElement;
import org.netbeans.api.java.source.ClassIndex;
import org.netbeans.api.java.source.ElementHandle;

public final class TypesEvent
extends EventObject {
    private final Iterable<? extends ElementHandle<TypeElement>> types;

    TypesEvent(ClassIndex source, Iterable<? extends ElementHandle<TypeElement>> types) {
        super(source);
        assert (types != null);
        this.types = types;
    }

    public Iterable<? extends ElementHandle<TypeElement>> getTypes() {
        return this.types;
    }

    @Override
    public String toString() {
        return String.format("TypesEvent [%s]", this.types.toString());
    }
}

