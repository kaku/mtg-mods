/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$CompletionFailure
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Log
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullUnknown
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.platform.JavaPlatform
 *  org.netbeans.api.java.platform.JavaPlatformManager
 *  org.netbeans.modules.parsing.api.Embedding
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.Task
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.SourceModificationEvent
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.PositionRef
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.Parameters
 */
package org.netbeans.api.java.source;

import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.Log;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.Document;
import javax.tools.JavaFileObject;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullUnknown;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.api.java.platform.JavaPlatformManager;
import org.netbeans.api.java.source.ClassIndex;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.api.java.source.PositionConverter;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.modules.java.source.JavaSourceAccessor;
import org.netbeans.modules.java.source.parsing.ClasspathInfoTask;
import org.netbeans.modules.java.source.parsing.CompilationInfoImpl;
import org.netbeans.modules.java.source.parsing.JavacParser;
import org.netbeans.modules.java.source.parsing.JavacParserFactory;
import org.netbeans.modules.java.source.parsing.JavacParserResult;
import org.netbeans.modules.java.source.parsing.MimeTask;
import org.netbeans.modules.java.source.parsing.NewComilerTask;
import org.netbeans.modules.java.source.save.ElementOverlay;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.PositionRef;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Parameters;

public final class JavaSource {
    private final Collection<Source> sources;
    private final Collection<FileObject> files;
    private final ClasspathInfo classpathInfo;
    private ClasspathInfo cachedCpInfo;
    private static final Logger LOGGER;
    private static Map<FileObject, Reference<JavaSource>> file2JavaSource;
    private static final String[] supportedMIMETypes;

    @NullUnknown
    public static /* varargs */ JavaSource create(@NonNull ClasspathInfo cpInfo, @NonNull FileObject ... files) throws IllegalArgumentException {
        if (files == null || cpInfo == null) {
            throw new IllegalArgumentException();
        }
        return JavaSource._create(cpInfo, Arrays.asList(files));
    }

    @NullUnknown
    public static JavaSource create(@NonNull ClasspathInfo cpInfo, @NonNull Collection<? extends FileObject> files) throws IllegalArgumentException {
        return JavaSource._create(cpInfo, files);
    }

    @NullUnknown
    private static JavaSource _create(ClasspathInfo cpInfo, @NonNull Collection<? extends FileObject> files) throws IllegalArgumentException {
        if (files == null) {
            throw new IllegalArgumentException();
        }
        try {
            return new JavaSource(cpInfo, files);
        }
        catch (DataObjectNotFoundException donf) {
            Logger.getLogger("global").warning("Ignoring non existent file: " + FileUtil.getFileDisplayName((FileObject)donf.getFileObject()));
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        return null;
    }

    @CheckForNull
    public static JavaSource forFileObject(@NonNull FileObject fileObject) throws IllegalArgumentException {
        JavaSource js;
        if (fileObject == null) {
            throw new IllegalArgumentException("fileObject == null");
        }
        if (!fileObject.isValid()) {
            LOGGER.log(Level.FINE, "FileObject ({0}) passed to JavaSource.forFileObject is invalid", fileObject.toURI().toString());
            return null;
        }
        String mimeType = null;
        try {
            if (fileObject.getFileSystem().isDefault() && fileObject.getAttribute("javax.script.ScriptEngine") != null && fileObject.getAttribute("template") == Boolean.TRUE) {
                LOGGER.log(Level.FINE, "FileObject ({0}) passed to JavaSource.forFileObject is a template", fileObject.toURI().toString());
                return null;
            }
            DataObject od = DataObject.find((FileObject)fileObject);
            EditorCookie ec = (EditorCookie)od.getLookup().lookup(EditorCookie.class);
            if (!(ec instanceof CloneableEditorSupport || "application/x-class-file".equals(mimeType = FileUtil.getMIMEType((FileObject)fileObject, (String[])supportedMIMETypes)) || "class".equals(fileObject.getExt()))) {
                LOGGER.log(Level.FINE, "DataObject ({1}) created for FileObject ({0}) passed to JavaSource.forFileObject does not provide CloneableEditorSupport and is not a classfile", new Object[]{fileObject.toURI().toString(), od.getClass().getName()});
                return null;
            }
        }
        catch (FileStateInvalidException ex) {
            LOGGER.log(Level.FINE, null, (Throwable)ex);
            return null;
        }
        catch (DataObjectNotFoundException ex) {
            LOGGER.log(Level.FINE, null, (Throwable)ex);
            return null;
        }
        Reference<JavaSource> ref = file2JavaSource.get((Object)fileObject);
        JavaSource javaSource = js = ref != null ? ref.get() : null;
        if (js == null) {
            String string = mimeType = mimeType == null ? FileUtil.getMIMEType((FileObject)fileObject, (String[])supportedMIMETypes) : mimeType;
            if ("application/x-class-file".equals(mimeType) || "class".equals(fileObject.getExt())) {
                ClassPath srcPath;
                ClassPath compilePath;
                ClassPath execPath;
                ClassPath bootPath = ClassPath.getClassPath((FileObject)fileObject, (String)"classpath/boot");
                if (bootPath == null) {
                    bootPath = JavaPlatformManager.getDefault().getDefaultPlatform().getBootstrapLibraries();
                }
                if ((compilePath = ClassPath.getClassPath((FileObject)fileObject, (String)"classpath/compile")) == null) {
                    compilePath = ClassPathSupport.createClassPath((URL[])new URL[0]);
                }
                if ((srcPath = ClassPath.getClassPath((FileObject)fileObject, (String)"classpath/source")) == null) {
                    srcPath = ClassPathSupport.createClassPath((URL[])new URL[0]);
                }
                if ((execPath = ClassPath.getClassPath((FileObject)fileObject, (String)"classpath/execute")) != null) {
                    bootPath = ClassPathSupport.createProxyClassPath((ClassPath[])new ClassPath[]{execPath, bootPath});
                }
                ClasspathInfo info = ClasspathInfo.create(bootPath, compilePath, srcPath);
                FileObject root = ClassPathSupport.createProxyClassPath((ClassPath[])new ClassPath[]{bootPath, compilePath, srcPath}).findOwnerRoot(fileObject);
                if (root == null) {
                    LOGGER.log(Level.FINE, "FileObject ({0}) passed to JavaSource.forFileObject of mimeType classfile does not have a corresponding root", fileObject.toURI().toString());
                    return null;
                }
                try {
                    js = new JavaSource(info, fileObject, root);
                }
                catch (IOException ioe) {
                    Exceptions.printStackTrace((Throwable)ioe);
                }
            } else {
                if (!"text/x-java".equals(mimeType) && !"java".equals(fileObject.getExt())) {
                    LOGGER.log(Level.FINE, "FileObject ({0}) passed to JavaSource.forFileObject is not a Java source file (mimetype: {1})", new Object[]{fileObject.toURI().toString(), mimeType});
                    return null;
                }
                js = JavaSource._create(null, Collections.singletonList(fileObject));
            }
            file2JavaSource.put(fileObject, new WeakReference<JavaSource>(js));
        }
        return js;
    }

    @CheckForNull
    public static JavaSource forDocument(@NonNull Document doc) throws IllegalArgumentException {
        JavaSource js;
        Object source;
        DataObject dObj;
        if (doc == null) {
            throw new IllegalArgumentException("doc == null");
        }
        Reference ref = (Reference)doc.getProperty(JavaSource.class);
        JavaSource javaSource = js = ref != null ? (JavaSource)ref.get() : null;
        if (js == null && (source = doc.getProperty("stream")) instanceof DataObject && (dObj = (DataObject)source) != null) {
            js = JavaSource.forFileObject(dObj.getPrimaryFile());
        }
        return js;
    }

    private JavaSource(ClasspathInfo cpInfo, @NonNull Collection<? extends FileObject> files) throws IOException {
        boolean multipleSources = files.size() > 1;
        ArrayList<Source> sources = new ArrayList<Source>(files.size());
        ArrayList<FileObject> fl = new ArrayList<FileObject>(files.size());
        for (FileObject file : files) {
            Logger.getLogger("TIMER").log(Level.FINE, "JavaSource", new Object[]{file, this});
            if (!file.isValid()) {
                if (multipleSources) {
                    LOGGER.warning("Ignoring non existent file: " + FileUtil.getFileDisplayName((FileObject)file));
                    continue;
                }
                DataObject.find((FileObject)file);
                continue;
            }
            fl.add(file);
            sources.add(Source.create((FileObject)file));
        }
        this.files = Collections.unmodifiableList(fl);
        this.sources = Collections.unmodifiableList(sources);
        this.classpathInfo = cpInfo;
    }

    private JavaSource(@NonNull ClasspathInfo info, @NonNull FileObject classFileObject, @NonNull FileObject root) throws IOException {
        assert (info != null);
        assert (classFileObject != null);
        assert (root != null);
        this.files = Collections.singletonList(classFileObject);
        this.sources = Collections.singletonList(Source.create((FileObject)classFileObject));
        this.classpathInfo = info;
    }

    public void runUserActionTask(@NonNull Task<CompilationController> task, boolean shared) throws IOException {
        this.runUserActionTaskImpl(task, shared);
    }

    private long runUserActionTaskImpl(Task<CompilationController> task, boolean shared) throws IOException {
        Parameters.notNull((CharSequence)"task", task);
        long currentId = -1;
        if (this.sources.isEmpty()) {
            try {
                ParserManager.parse((String)"text/x-java", (UserTask)new MimeTask(this, task, this.classpathInfo));
            }
            catch (ParseException pe) {
                Throwable rootCase = pe.getCause();
                if (rootCase instanceof Symbol.CompletionFailure) {
                    IOException ioe = new IOException();
                    ioe.initCause(rootCase);
                    throw ioe;
                }
                if (rootCase instanceof RuntimeException) {
                    throw (RuntimeException)rootCase;
                }
                IOException ioe = new IOException();
                ioe.initCause(rootCase);
                throw ioe;
            }
        }
        try {
            MultiTask _task = new MultiTask(this, task, this.classpathInfo);
            ParserManager.parse(this.sources, (UserTask)_task);
        }
        catch (ParseException pe) {
            Throwable rootCase = pe.getCause();
            if (rootCase instanceof Symbol.CompletionFailure) {
                IOException ioe = new IOException();
                ioe.initCause(rootCase);
                throw ioe;
            }
            if (rootCase instanceof RuntimeException) {
                throw (RuntimeException)rootCase;
            }
            IOException ioe = new IOException();
            ioe.initCause(rootCase);
            throw ioe;
        }
        return currentId;
    }

    long createTaggedController(long timestamp, Object[] controller) throws IOException {
        assert (controller.length == 1);
        assert (controller[0] == null || controller[0] instanceof CompilationController);
        try {
            CompilationController cc = (CompilationController)controller[0];
            NewComilerTask _task = new NewComilerTask(this.classpathInfo, cc, timestamp);
            ParserManager.parse(this.sources, (UserTask)_task);
            controller[0] = _task.getCompilationController();
            return _task.getTimeStamp();
        }
        catch (ParseException pe) {
            Throwable rootCase = pe.getCause();
            if (rootCase instanceof Symbol.CompletionFailure) {
                IOException ioe = new IOException();
                ioe.initCause(rootCase);
                throw ioe;
            }
            if (rootCase instanceof RuntimeException) {
                throw (RuntimeException)rootCase;
            }
            IOException ioe = new IOException();
            ioe.initCause(rootCase);
            throw ioe;
        }
    }

    @NonNull
    public Future<Void> runWhenScanFinished(@NonNull Task<CompilationController> task, boolean shared) throws IOException {
        Parameters.notNull((CharSequence)"task", task);
        if (this.sources.isEmpty()) {
            try {
                return ParserManager.parseWhenScanFinished((String)"text/x-java", (UserTask)new MimeTask(this, task, this.classpathInfo));
            }
            catch (ParseException pe) {
                Throwable rootCase = pe.getCause();
                if (rootCase instanceof Symbol.CompletionFailure) {
                    IOException ioe = new IOException();
                    ioe.initCause(rootCase);
                    throw ioe;
                }
                if (rootCase instanceof RuntimeException) {
                    throw (RuntimeException)rootCase;
                }
                IOException ioe = new IOException();
                ioe.initCause(rootCase);
                throw ioe;
            }
        }
        try {
            MultiTask _task = new MultiTask(this, task, this.classpathInfo);
            return ParserManager.parseWhenScanFinished(this.sources, (UserTask)_task);
        }
        catch (ParseException pe) {
            Throwable rootCase = pe.getCause();
            if (rootCase instanceof Symbol.CompletionFailure) {
                IOException ioe = new IOException();
                ioe.initCause(rootCase);
                throw ioe;
            }
            if (rootCase instanceof RuntimeException) {
                throw (RuntimeException)rootCase;
            }
            IOException ioe = new IOException();
            ioe.initCause(rootCase);
            throw ioe;
        }
    }

    @NonNull
    public ModificationResult runModificationTask(final @NonNull Task<WorkingCopy> task) throws IOException {
        if (task == null) {
            throw new IllegalArgumentException("Task cannot be null");
        }
        final ModificationResult result = new ModificationResult(this);
        final ElementOverlay overlay = ElementOverlay.getOrCreateOverlay();
        long start = System.currentTimeMillis();
        Task<CompilationController> inner = new Task<CompilationController>(){

            @Override
            public void run(CompilationController cc) throws Exception {
                WorkingCopy copy = new WorkingCopy(cc.impl, overlay);
                copy.setJavaSource(JavaSource.this);
                if (JavaSource.this.sources.isEmpty()) {
                    copy.toPhase(Phase.PARSED);
                }
                task.run(copy);
                JavacTaskImpl jt = copy.impl.getJavacTask();
                Log.instance((Context)jt.getContext()).nerrors = 0;
                List<ModificationResult.Difference> diffs = copy.getChanges(result.tag2Span);
                if (diffs != null && diffs.size() > 0) {
                    FileObject file = copy.getFileObject();
                    result.diffs.put(file != null ? file : FileUtil.createMemoryFileSystem().getRoot().createData("temp", "java"), diffs);
                }
            }
        };
        this.runUserActionTask(inner, true);
        if (this.sources.size() == 1) {
            Logger.getLogger("TIMER").log(Level.FINE, "Modification Task", new Object[]{this.sources.iterator().next().getFileObject(), System.currentTimeMillis() - start});
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @NonNull
    public ClasspathInfo getClasspathInfo() {
        JavaSource javaSource = this;
        synchronized (javaSource) {
            if (this.cachedCpInfo != null) {
                return this.cachedCpInfo;
            }
        }
        ClasspathInfo _tmp = this.classpathInfo;
        if (_tmp == null) {
            assert (!this.files.isEmpty());
            _tmp = ClasspathInfo.create(this.files.iterator().next());
        }
        JavaSource javaSource2 = this;
        synchronized (javaSource2) {
            if (this.cachedCpInfo == null) {
                this.cachedCpInfo = _tmp;
            }
            return this.cachedCpInfo;
        }
    }

    @NonNull
    public Collection<FileObject> getFileObjects() {
        return this.files;
    }

    static {
        JavaSourceAccessor.setINSTANCE(new JavaSourceAccessorImpl());
        LOGGER = Logger.getLogger(JavaSource.class.getName());
        file2JavaSource = new WeakHashMap<FileObject, Reference<JavaSource>>();
        supportedMIMETypes = new String[]{"application/x-class-file", "text/x-java"};
    }

    private static class JavaSourceAccessorImpl
    extends JavaSourceAccessor {
        private JavaSourceAccessorImpl() {
        }

        @Override
        public JavacTaskImpl getJavacTask(CompilationInfo compilationInfo) {
            assert (compilationInfo != null);
            return compilationInfo.impl.getJavacTask();
        }

        @Override
        public JavaSource create(ClasspathInfo cpInfo, PositionConverter binding, Collection<? extends FileObject> files) throws IllegalArgumentException {
            return JavaSource.create(cpInfo, files);
        }

        @Override
        public CompilationController createCompilationController(Source s) throws IOException, ParseException {
            Parameters.notNull((CharSequence)"s", (Object)s);
            JavacParserFactory factory = JavacParserFactory.getDefault();
            Snapshot snapshot = s.createSnapshot();
            JavacParser parser = factory.createPrivateParser(snapshot);
            if (parser == null) {
                return null;
            }
            UserTask dummy = new UserTask(){

                public void run(ResultIterator resultIterator) throws Exception {
                }
            };
            parser.parse(snapshot, (org.netbeans.modules.parsing.api.Task)dummy, null);
            return CompilationController.get(parser.getResult((org.netbeans.modules.parsing.api.Task)dummy));
        }

        @Override
        public long createTaggedCompilationController(JavaSource js, long currentTag, Object[] out) throws IOException {
            return js.createTaggedController(currentTag, out);
        }

        @Override
        public CompilationInfo createCompilationInfo(CompilationInfoImpl impl) {
            return new CompilationInfo(impl);
        }

        @Override
        public CompilationController createCompilationController(CompilationInfoImpl impl) {
            return new CompilationController(impl);
        }

        @Override
        public void invalidate(CompilationInfo info) {
            assert (info != null);
            info.invalidate();
        }

        @Override
        public Collection<Source> getSources(JavaSource js) {
            assert (js != null);
            return js.sources;
        }

        @Override
        public void setJavaSource(CompilationInfo info, JavaSource js) {
            assert (info != null);
            assert (js != null);
            info.setJavaSource(js);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void invalidateCachedClasspathInfo(FileObject file) {
            JavaSource js;
            assert (file != null);
            Reference ref = (Reference)file2JavaSource.get((Object)file);
            if (ref != null && (js = (JavaSource)ref.get()) != null) {
                JavaSource javaSource = js;
                synchronized (javaSource) {
                    js.cachedCpInfo = null;
                }
            }
        }

        @Override
        public CompilationInfoImpl getCompilationInfoImpl(CompilationInfo info) {
            assert (info != null);
            return info.impl;
        }

        @NonNull
        @Override
        public String generateReadableParameterName(@NonNull String typeName, @NonNull Set<String> used) {
            return SourceUtils.generateReadableParameterName(typeName, used);
        }

        @Override
        public ModificationResult.Difference createDifference(ModificationResult.Difference.Kind kind, PositionRef startPos, PositionRef endPos, String oldText, String newText, String description) {
            return new ModificationResult.Difference(kind, startPos, endPos, oldText, newText, description);
        }

        @Override
        public ModificationResult.Difference createNewFileDifference(JavaFileObject fileObject, String text) {
            return new ModificationResult.CreateChange(fileObject, text);
        }

        @Override
        public ModificationResult createModificationResult(Map<FileObject, List<ModificationResult.Difference>> diffs, Map<?, int[]> tag2Span) {
            ModificationResult result = new ModificationResult(null);
            result.diffs = diffs;
            result.tag2Span = tag2Span;
            return result;
        }

        @Override
        public ElementUtilities createElementUtilities(@NonNull JavacTaskImpl jt) {
            return new ElementUtilities(jt);
        }

        @Override
        public Map<FileObject, List<ModificationResult.Difference>> getDiffsFromModificationResult(ModificationResult mr) {
            return mr.diffs;
        }

        @Override
        public Map<?, int[]> getTagsFromModificationResult(ModificationResult mr) {
            return mr.tag2Span;
        }

        @Override
        public ClassIndex createClassIndex(ClassPath bootPath, ClassPath classPath, ClassPath sourcePath, boolean supportsChanges) {
            return new ClassIndex(bootPath, classPath, sourcePath, supportsChanges);
        }

    }

    private static class MultiTask
    extends ClasspathInfoTask {
        private final JavaSource js;
        private final Task<CompilationController> task;

        public MultiTask(JavaSource js, Task<CompilationController> task, ClasspathInfo cpInfo) {
            super(cpInfo);
            assert (js != null);
            assert (task != null);
            this.js = js;
            this.task = task;
        }

        public void run(ResultIterator resultIterator) throws Exception {
            Snapshot snapshot = resultIterator.getSnapshot();
            if ("text/x-java".equals(snapshot.getMimeType()) || "application/x-class-file".equals(snapshot.getMimeType())) {
                Parser.Result result = resultIterator.getParserResult();
                if (result == null) {
                    return;
                }
                CompilationController cc = CompilationController.get(result);
                assert (cc != null);
                cc.setJavaSource(this.js);
                this.task.run(cc);
                JavacTaskImpl jt = cc.impl.getJavacTask();
                Log.instance((Context)jt.getContext()).nerrors = 0;
            } else {
                Parser.Result result = this.findEmbeddedJava(resultIterator);
                if (result == null) {
                    return;
                }
                CompilationController cc = CompilationController.get(result);
                assert (cc != null);
                cc.setJavaSource(this.js);
                this.task.run(cc);
                JavacTaskImpl jt = cc.impl.getJavacTask();
                Log.instance((Context)jt.getContext()).nerrors = 0;
            }
        }

        public String toString() {
            return this.getClass().getName() + "[" + this.task.getClass().getName() + "]";
        }

        private Parser.Result findEmbeddedJava(ResultIterator theMess) throws ParseException {
            ArrayList<Embedding> todo = new ArrayList<Embedding>();
            for (Embedding embedding2 : theMess.getEmbeddings()) {
                if ("text/x-java".equals(embedding2.getMimeType())) {
                    return theMess.getResultIterator(embedding2).getParserResult();
                }
                todo.add(embedding2);
            }
            for (Embedding embedding2 : todo) {
                Parser.Result result = this.findEmbeddedJava(theMess.getResultIterator(embedding2));
                if (result == null) continue;
                return result;
            }
            return null;
        }
    }

    public static final class InsufficientMemoryException
    extends IOException {
        private FileObject fo;

        private InsufficientMemoryException(String message, FileObject fo) {
            super(message);
            this.fo = fo;
        }

        private InsufficientMemoryException(FileObject fo) {
            this(NbBundle.getMessage(JavaSource.class, (String)"MSG_UnsufficientMemoryException", (Object)FileUtil.getFileDisplayName((FileObject)fo)), fo);
        }

        public FileObject getFile() {
            return this.fo;
        }
    }

    public static enum Priority {
        MAX,
        HIGH,
        ABOVE_NORMAL,
        NORMAL,
        BELOW_NORMAL,
        LOW,
        MIN;
        

        private Priority() {
        }
    }

    public static enum Phase {
        MODIFIED,
        PARSED,
        ELEMENTS_RESOLVED,
        RESOLVED,
        UP_TO_DATE;
        

        private Phase() {
        }
    }

}

