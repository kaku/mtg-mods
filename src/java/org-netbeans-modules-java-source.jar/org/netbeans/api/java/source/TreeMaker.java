/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.doctree.AttributeTree
 *  com.sun.source.doctree.AttributeTree$ValueKind
 *  com.sun.source.doctree.AuthorTree
 *  com.sun.source.doctree.CommentTree
 *  com.sun.source.doctree.DeprecatedTree
 *  com.sun.source.doctree.DocCommentTree
 *  com.sun.source.doctree.DocRootTree
 *  com.sun.source.doctree.DocTree
 *  com.sun.source.doctree.EndElementTree
 *  com.sun.source.doctree.EntityTree
 *  com.sun.source.doctree.IdentifierTree
 *  com.sun.source.doctree.InheritDocTree
 *  com.sun.source.doctree.LinkTree
 *  com.sun.source.doctree.LiteralTree
 *  com.sun.source.doctree.ParamTree
 *  com.sun.source.doctree.ReferenceTree
 *  com.sun.source.doctree.ReturnTree
 *  com.sun.source.doctree.SeeTree
 *  com.sun.source.doctree.SerialDataTree
 *  com.sun.source.doctree.SerialFieldTree
 *  com.sun.source.doctree.SerialTree
 *  com.sun.source.doctree.SinceTree
 *  com.sun.source.doctree.StartElementTree
 *  com.sun.source.doctree.TextTree
 *  com.sun.source.doctree.ThrowsTree
 *  com.sun.source.doctree.UnknownBlockTagTree
 *  com.sun.source.doctree.UnknownInlineTagTree
 *  com.sun.source.doctree.ValueTree
 *  com.sun.source.doctree.VersionTree
 *  com.sun.source.tree.AnnotatedTypeTree
 *  com.sun.source.tree.AnnotationTree
 *  com.sun.source.tree.ArrayAccessTree
 *  com.sun.source.tree.ArrayTypeTree
 *  com.sun.source.tree.AssertTree
 *  com.sun.source.tree.AssignmentTree
 *  com.sun.source.tree.BinaryTree
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.BreakTree
 *  com.sun.source.tree.CaseTree
 *  com.sun.source.tree.CatchTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.CompoundAssignmentTree
 *  com.sun.source.tree.ConditionalExpressionTree
 *  com.sun.source.tree.ContinueTree
 *  com.sun.source.tree.DoWhileLoopTree
 *  com.sun.source.tree.EmptyStatementTree
 *  com.sun.source.tree.EnhancedForLoopTree
 *  com.sun.source.tree.ErroneousTree
 *  com.sun.source.tree.ExpressionStatementTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.ForLoopTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.IfTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.InstanceOfTree
 *  com.sun.source.tree.LabeledStatementTree
 *  com.sun.source.tree.LambdaExpressionTree
 *  com.sun.source.tree.LiteralTree
 *  com.sun.source.tree.MemberReferenceTree
 *  com.sun.source.tree.MemberReferenceTree$ReferenceMode
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodInvocationTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.NewArrayTree
 *  com.sun.source.tree.NewClassTree
 *  com.sun.source.tree.ParameterizedTypeTree
 *  com.sun.source.tree.ParenthesizedTree
 *  com.sun.source.tree.PrimitiveTypeTree
 *  com.sun.source.tree.ReturnTree
 *  com.sun.source.tree.Scope
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.SwitchTree
 *  com.sun.source.tree.SynchronizedTree
 *  com.sun.source.tree.ThrowTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.TryTree
 *  com.sun.source.tree.TypeCastTree
 *  com.sun.source.tree.TypeParameterTree
 *  com.sun.source.tree.UnaryTree
 *  com.sun.source.tree.UnionTypeTree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.tree.WhileLoopTree
 *  com.sun.source.tree.WildcardTree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreeScanner
 *  com.sun.source.util.Trees
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCModifiers
 *  com.sun.tools.javac.util.Context
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenSequence
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Parameters
 */
package org.netbeans.api.java.source;

import com.sun.source.doctree.AttributeTree;
import com.sun.source.doctree.AuthorTree;
import com.sun.source.doctree.CommentTree;
import com.sun.source.doctree.DeprecatedTree;
import com.sun.source.doctree.DocCommentTree;
import com.sun.source.doctree.DocRootTree;
import com.sun.source.doctree.DocTree;
import com.sun.source.doctree.EndElementTree;
import com.sun.source.doctree.EntityTree;
import com.sun.source.doctree.IdentifierTree;
import com.sun.source.doctree.InheritDocTree;
import com.sun.source.doctree.LinkTree;
import com.sun.source.doctree.ParamTree;
import com.sun.source.doctree.ReferenceTree;
import com.sun.source.doctree.ReturnTree;
import com.sun.source.doctree.SeeTree;
import com.sun.source.doctree.SerialDataTree;
import com.sun.source.doctree.SerialFieldTree;
import com.sun.source.doctree.SerialTree;
import com.sun.source.doctree.SinceTree;
import com.sun.source.doctree.StartElementTree;
import com.sun.source.doctree.TextTree;
import com.sun.source.doctree.ThrowsTree;
import com.sun.source.doctree.UnknownBlockTagTree;
import com.sun.source.doctree.UnknownInlineTagTree;
import com.sun.source.doctree.ValueTree;
import com.sun.source.doctree.VersionTree;
import com.sun.source.tree.AnnotatedTypeTree;
import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.ArrayAccessTree;
import com.sun.source.tree.ArrayTypeTree;
import com.sun.source.tree.AssertTree;
import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BinaryTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.BreakTree;
import com.sun.source.tree.CaseTree;
import com.sun.source.tree.CatchTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.CompoundAssignmentTree;
import com.sun.source.tree.ConditionalExpressionTree;
import com.sun.source.tree.ContinueTree;
import com.sun.source.tree.DoWhileLoopTree;
import com.sun.source.tree.EmptyStatementTree;
import com.sun.source.tree.EnhancedForLoopTree;
import com.sun.source.tree.ErroneousTree;
import com.sun.source.tree.ExpressionStatementTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.ForLoopTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.InstanceOfTree;
import com.sun.source.tree.LabeledStatementTree;
import com.sun.source.tree.LambdaExpressionTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MemberReferenceTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.NewArrayTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.ParameterizedTypeTree;
import com.sun.source.tree.ParenthesizedTree;
import com.sun.source.tree.PrimitiveTypeTree;
import com.sun.source.tree.Scope;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.SwitchTree;
import com.sun.source.tree.SynchronizedTree;
import com.sun.source.tree.ThrowTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TryTree;
import com.sun.source.tree.TypeCastTree;
import com.sun.source.tree.TypeParameterTree;
import com.sun.source.tree.UnaryTree;
import com.sun.source.tree.UnionTypeTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.tree.WhileLoopTree;
import com.sun.source.tree.WildcardTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreeScanner;
import com.sun.source.util.Trees;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.Context;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.tools.JavaFileObject;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.source.AssignComments;
import org.netbeans.api.java.source.Comment;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.GeneratorUtilities;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.java.source.builder.ASTService;
import org.netbeans.modules.java.source.builder.CommentHandlerService;
import org.netbeans.modules.java.source.builder.TreeFactory;
import org.netbeans.modules.java.source.parsing.CompilationInfoImpl;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.query.CommentHandler;
import org.netbeans.modules.java.source.query.CommentSet;
import org.openide.filesystems.FileObject;
import org.openide.util.Parameters;

public final class TreeMaker {
    private TreeFactory delegate;
    private CommentHandler handler;
    private final ASTService model;
    private WorkingCopy copy;

    TreeMaker(WorkingCopy copy, TreeFactory delegate) {
        this.delegate = delegate;
        this.copy = copy;
        this.handler = CommentHandlerService.instance(copy.impl.getJavacTask().getContext());
        this.model = ASTService.instance(copy.impl.getJavacTask().getContext());
    }

    public AnnotationTree Annotation(Tree type, List<? extends ExpressionTree> arguments) {
        return this.delegate.Annotation(type, arguments);
    }

    public AnnotationTree TypeAnnotation(Tree type, List<? extends ExpressionTree> arguments) {
        return this.delegate.TypeAnnotation(type, arguments);
    }

    AnnotatedTypeTree AnnotatedType(Tree underlyingType, List<? extends AnnotationTree> annotations) {
        return this.delegate.AnnotatedType(annotations, underlyingType);
    }

    public ArrayAccessTree ArrayAccess(ExpressionTree array, ExpressionTree index) {
        return this.delegate.ArrayAccess(array, index);
    }

    public ArrayTypeTree ArrayType(Tree type) {
        return this.delegate.ArrayType(type);
    }

    public AssertTree Assert(ExpressionTree condition, ExpressionTree detail) {
        return this.delegate.Assert(condition, detail);
    }

    public AssignmentTree Assignment(ExpressionTree variable, ExpressionTree expression) {
        return this.delegate.Assignment(variable, expression);
    }

    public BinaryTree Binary(Tree.Kind operator, ExpressionTree left, ExpressionTree right) {
        return this.delegate.Binary(operator, left, right);
    }

    public BlockTree Block(List<? extends StatementTree> statements, boolean isStatic) {
        return this.delegate.Block(statements, isStatic);
    }

    public BreakTree Break(CharSequence label) {
        return this.delegate.Break(label);
    }

    public CaseTree Case(ExpressionTree expression, List<? extends StatementTree> statements) {
        return this.delegate.Case(expression, statements);
    }

    public CatchTree Catch(VariableTree parameter, BlockTree block) {
        return this.delegate.Catch(parameter, block);
    }

    public ClassTree Class(ModifiersTree modifiers, CharSequence simpleName, List<? extends TypeParameterTree> typeParameters, Tree extendsClause, List<? extends Tree> implementsClauses, List<? extends Tree> memberDecls) {
        return this.delegate.Class(modifiers, simpleName, typeParameters, extendsClause, implementsClauses, memberDecls);
    }

    public ClassTree Interface(ModifiersTree modifiers, CharSequence simpleName, List<? extends TypeParameterTree> typeParameters, List<? extends Tree> extendsClauses, List<? extends Tree> memberDecls) {
        return this.delegate.Interface(modifiers, simpleName, typeParameters, extendsClauses, memberDecls);
    }

    public ClassTree AnnotationType(ModifiersTree modifiers, CharSequence simpleName, List<? extends Tree> memberDecls) {
        return this.delegate.AnnotationType(modifiers, simpleName, memberDecls);
    }

    public ClassTree Enum(ModifiersTree modifiers, CharSequence simpleName, List<? extends Tree> implementsClauses, List<? extends Tree> memberDecls) {
        return this.delegate.Enum(modifiers, simpleName, implementsClauses, memberDecls);
    }

    public CompilationUnitTree CompilationUnit(ExpressionTree packageName, List<? extends ImportTree> imports, List<? extends Tree> typeDeclarations, JavaFileObject sourceFile) {
        return this.delegate.CompilationUnit(Collections.emptyList(), packageName, imports, typeDeclarations, sourceFile);
    }

    public CompilationUnitTree CompilationUnit(List<? extends AnnotationTree> packageAnnotations, ExpressionTree packageName, List<? extends ImportTree> imports, List<? extends Tree> typeDeclarations, JavaFileObject sourceFile) {
        return this.delegate.CompilationUnit(packageAnnotations, packageName, imports, typeDeclarations, sourceFile);
    }

    public CompilationUnitTree CompilationUnit(FileObject sourceRoot, String path, List<? extends ImportTree> imports, List<? extends Tree> typeDeclarations) {
        String[] nameComponent = FileObjects.getFolderAndBaseName(path, '/');
        JavaFileObject sourceFile = FileObjects.templateFileObject(sourceRoot, nameComponent[0], nameComponent[1]);
        com.sun.source.tree.IdentifierTree pkg = nameComponent[0].length() == 0 ? null : this.Identifier(nameComponent[0].replace('/', '.'));
        return this.delegate.CompilationUnit(Collections.emptyList(), (ExpressionTree)pkg, imports, typeDeclarations, sourceFile);
    }

    public CompilationUnitTree CompilationUnit(List<? extends AnnotationTree> packageAnnotations, FileObject sourceRoot, String path, List<? extends ImportTree> imports, List<? extends Tree> typeDeclarations) {
        String[] nameComponent = FileObjects.getFolderAndBaseName(path, '/');
        JavaFileObject sourceFile = FileObjects.templateFileObject(sourceRoot, nameComponent[0], nameComponent[1]);
        com.sun.source.tree.IdentifierTree pkg = nameComponent[0].length() == 0 ? null : this.Identifier(nameComponent[0].replace('/', '.'));
        return this.delegate.CompilationUnit(packageAnnotations, (ExpressionTree)pkg, imports, typeDeclarations, sourceFile);
    }

    public CompoundAssignmentTree CompoundAssignment(Tree.Kind operator, ExpressionTree variable, ExpressionTree expression) {
        return this.delegate.CompoundAssignment(operator, variable, expression);
    }

    public ConditionalExpressionTree ConditionalExpression(ExpressionTree condition, ExpressionTree trueExpression, ExpressionTree falseExpression) {
        return this.delegate.ConditionalExpression(condition, trueExpression, falseExpression);
    }

    public MethodTree Constructor(ModifiersTree modifiers, List<? extends TypeParameterTree> typeParameters, List<? extends VariableTree> parameters, List<? extends ExpressionTree> throwsList, BlockTree body) {
        return this.delegate.Method(modifiers, "<init>", null, typeParameters, parameters, throwsList, body, null);
    }

    public MethodTree Constructor(ModifiersTree modifiers, List<? extends TypeParameterTree> typeParameters, List<? extends VariableTree> parameters, List<? extends ExpressionTree> throwsList, String bodyText) {
        return this.Method(modifiers, (CharSequence)"<init>", null, typeParameters, parameters, throwsList, bodyText, null);
    }

    public ContinueTree Continue(CharSequence label) {
        return this.delegate.Continue(label);
    }

    public UnionTypeTree UnionType(List<? extends Tree> typeComponents) {
        return this.delegate.UnionType(typeComponents);
    }

    public DoWhileLoopTree DoWhileLoop(ExpressionTree condition, StatementTree statement) {
        return this.delegate.DoWhileLoop(condition, statement);
    }

    public EmptyStatementTree EmptyStatement() {
        return this.delegate.EmptyStatement();
    }

    public EnhancedForLoopTree EnhancedForLoop(VariableTree variable, ExpressionTree expression, StatementTree statement) {
        return this.delegate.EnhancedForLoop(variable, expression, statement);
    }

    public ErroneousTree Erroneous(List<? extends Tree> errorTrees) {
        return this.delegate.Erroneous(errorTrees);
    }

    public ExpressionStatementTree ExpressionStatement(ExpressionTree expression) {
        return this.delegate.ExpressionStatement(expression);
    }

    public ForLoopTree ForLoop(List<? extends StatementTree> initializer, ExpressionTree condition, List<? extends ExpressionStatementTree> update, StatementTree statement) {
        return this.delegate.ForLoop(initializer, condition, update, statement);
    }

    public com.sun.source.tree.IdentifierTree Identifier(CharSequence name) {
        return this.delegate.Identifier(name);
    }

    public com.sun.source.tree.IdentifierTree Identifier(Element element) {
        return this.delegate.Identifier(element);
    }

    public IfTree If(ExpressionTree condition, StatementTree thenStatement, StatementTree elseStatement) {
        return this.delegate.If(condition, thenStatement, elseStatement);
    }

    public ImportTree Import(Tree qualid, boolean importStatic) {
        return this.delegate.Import(qualid, importStatic);
    }

    public InstanceOfTree InstanceOf(ExpressionTree expression, Tree type) {
        return this.delegate.InstanceOf(expression, type);
    }

    public LabeledStatementTree LabeledStatement(CharSequence label, StatementTree statement) {
        return this.delegate.LabeledStatement(label, statement);
    }

    public LambdaExpressionTree LambdaExpression(List<? extends VariableTree> parameters, Tree body) {
        return this.delegate.LambdaExpression(parameters, body);
    }

    public LiteralTree Literal(Object value) {
        return this.delegate.Literal(value);
    }

    public MemberReferenceTree MemberReference(MemberReferenceTree.ReferenceMode refMode, ExpressionTree expression, CharSequence name, List<? extends ExpressionTree> typeArguments) {
        return this.delegate.MemberReference(refMode, name, expression, typeArguments);
    }

    public MemberSelectTree MemberSelect(ExpressionTree expression, CharSequence identifier) {
        return this.delegate.MemberSelect(expression, identifier);
    }

    public MemberSelectTree MemberSelect(ExpressionTree expression, Element element) {
        return this.delegate.MemberSelect(expression, element);
    }

    public MethodInvocationTree MethodInvocation(List<? extends ExpressionTree> typeArguments, ExpressionTree method, List<? extends ExpressionTree> arguments) {
        return this.delegate.MethodInvocation(typeArguments, method, arguments);
    }

    public MethodTree Method(ModifiersTree modifiers, CharSequence name, Tree returnType, List<? extends TypeParameterTree> typeParameters, List<? extends VariableTree> parameters, List<? extends ExpressionTree> throwsList, BlockTree body, ExpressionTree defaultValue) {
        return this.Method(modifiers, name, returnType, typeParameters, parameters, throwsList, body, defaultValue, false);
    }

    public MethodTree Method(ModifiersTree modifiers, CharSequence name, Tree returnType, List<? extends TypeParameterTree> typeParameters, List<? extends VariableTree> parameters, List<? extends ExpressionTree> throwsList, BlockTree body, ExpressionTree defaultValue, boolean isVarArg) {
        return this.delegate.Method(modifiers, name, returnType, typeParameters, parameters, throwsList, body, defaultValue, isVarArg);
    }

    @Deprecated
    public MethodTree Method(ExecutableElement element, BlockTree body) {
        return this.delegate.Method(element, body);
    }

    public ModifiersTree Modifiers(Set<Modifier> flags, List<? extends AnnotationTree> annotations) {
        return this.delegate.Modifiers(flags, annotations);
    }

    public ModifiersTree Modifiers(long flags, List<? extends AnnotationTree> annotations) {
        return this.delegate.Modifiers(flags, annotations);
    }

    public ModifiersTree Modifiers(Set<Modifier> flags) {
        return this.delegate.Modifiers(flags);
    }

    public ModifiersTree Modifiers(ModifiersTree oldMods, List<? extends AnnotationTree> annotations) {
        return this.delegate.Modifiers(oldMods, annotations);
    }

    public NewArrayTree NewArray(Tree elemtype, List<? extends ExpressionTree> dimensions, List<? extends ExpressionTree> initializers) {
        return this.delegate.NewArray(elemtype, dimensions, initializers);
    }

    public NewClassTree NewClass(ExpressionTree enclosingExpression, List<? extends ExpressionTree> typeArguments, ExpressionTree identifier, List<? extends ExpressionTree> arguments, ClassTree classBody) {
        return this.delegate.NewClass(enclosingExpression, typeArguments, identifier, arguments, classBody);
    }

    public ParameterizedTypeTree ParameterizedType(Tree type, List<? extends Tree> typeArguments) {
        return this.delegate.ParameterizedType(type, typeArguments);
    }

    public ParenthesizedTree Parenthesized(ExpressionTree expression) {
        return this.delegate.Parenthesized(expression);
    }

    @NonNull
    public PrimitiveTypeTree PrimitiveType(@NonNull TypeKind typekind) {
        Parameters.notNull((CharSequence)"typekind", (Object)((Object)typekind));
        return this.delegate.PrimitiveType(typekind);
    }

    @NonNull
    public ExpressionTree QualIdent(@NonNull Element element) {
        Parameters.notNull((CharSequence)"element", (Object)element);
        return this.delegate.QualIdent(element);
    }

    @NonNull
    public ExpressionTree QualIdent(@NonNull String name) {
        Parameters.notNull((CharSequence)"name", (Object)name);
        return this.delegate.QualIdent(name);
    }

    public com.sun.source.tree.ReturnTree Return(ExpressionTree expression) {
        return this.delegate.Return(expression);
    }

    public SwitchTree Switch(ExpressionTree expression, List<? extends CaseTree> cases) {
        return this.delegate.Switch(expression, cases);
    }

    public SynchronizedTree Synchronized(ExpressionTree expression, BlockTree block) {
        return this.delegate.Synchronized(expression, block);
    }

    public ThrowTree Throw(ExpressionTree expression) {
        return this.delegate.Throw(expression);
    }

    public TryTree Try(BlockTree tryBlock, List<? extends CatchTree> catches, BlockTree finallyBlock) {
        return this.Try(Collections.emptyList(), tryBlock, catches, finallyBlock);
    }

    public TryTree Try(List<? extends Tree> resources, BlockTree tryBlock, List<? extends CatchTree> catches, BlockTree finallyBlock) {
        return this.delegate.Try(resources, tryBlock, catches, finallyBlock);
    }

    @NonNull
    public Tree Type(@NonNull TypeMirror type) {
        Parameters.notNull((CharSequence)"type", (Object)type);
        return this.delegate.Type(type);
    }

    @NonNull
    public Tree Type(@NonNull String type) {
        Parameters.notNull((CharSequence)"type", (Object)type);
        Tree typeTree = this.copy.getTreeUtilities().parseType(type);
        final HashMap translate = new HashMap();
        new TreeScanner<Void, Void>(){

            public Void visitMemberSelect(MemberSelectTree node, Void p) {
                translate.put(node, TreeMaker.this.QualIdent(node.toString()));
                return null;
            }

            public Void visitIdentifier(com.sun.source.tree.IdentifierTree node, Void p) {
                translate.put(node, TreeMaker.this.QualIdent(node.toString()));
                return null;
            }
        }.scan(typeTree, (Object)null);
        return this.copy.getTreeUtilities().translate(typeTree, translate);
    }

    public TypeCastTree TypeCast(Tree type, ExpressionTree expression) {
        return this.delegate.TypeCast(type, expression);
    }

    public TypeParameterTree TypeParameter(CharSequence name, List<? extends ExpressionTree> bounds) {
        return this.delegate.TypeParameter(name, bounds);
    }

    public UnaryTree Unary(Tree.Kind operator, ExpressionTree arg) {
        return this.delegate.Unary(operator, arg);
    }

    public VariableTree Variable(ModifiersTree modifiers, CharSequence name, Tree type, ExpressionTree initializer) {
        return this.delegate.Variable(modifiers, name, type, initializer);
    }

    public VariableTree Variable(VariableElement variable, ExpressionTree initializer) {
        return this.delegate.Variable(variable, initializer);
    }

    public WhileLoopTree WhileLoop(ExpressionTree condition, StatementTree statement) {
        return this.delegate.WhileLoop(condition, statement);
    }

    public WildcardTree Wildcard(Tree.Kind kind, Tree type) {
        return this.delegate.Wildcard(kind, type);
    }

    public AnnotationTree addAnnotationAttrValue(AnnotationTree annotation, ExpressionTree attrValue) {
        return this.delegate.addAnnotationAttrValue(annotation, attrValue);
    }

    public AnnotationTree insertAnnotationAttrValue(AnnotationTree annotation, int index, ExpressionTree attrValue) {
        return this.delegate.insertAnnotationAttrValue(annotation, index, attrValue);
    }

    public AnnotationTree removeAnnotationAttrValue(AnnotationTree annotation, ExpressionTree attrValue) {
        return this.delegate.removeAnnotationAttrValue(annotation, attrValue);
    }

    public AnnotationTree removeAnnotationAttrValue(AnnotationTree annotation, int index) {
        return this.delegate.removeAnnotationAttrValue(annotation, index);
    }

    public BlockTree addBlockStatement(BlockTree block, StatementTree statement) {
        return this.delegate.addBlockStatement(block, statement);
    }

    public BlockTree insertBlockStatement(BlockTree block, int index, StatementTree statement) {
        return this.delegate.insertBlockStatement(block, index, statement);
    }

    public BlockTree removeBlockStatement(BlockTree block, StatementTree statement) {
        return this.delegate.removeBlockStatement(block, statement);
    }

    public BlockTree removeBlockStatement(BlockTree block, int index) {
        return this.delegate.removeBlockStatement(block, index);
    }

    public CaseTree addCaseStatement(CaseTree kejs, StatementTree statement) {
        return this.delegate.addCaseStatement(kejs, statement);
    }

    public CaseTree insertCaseStatement(CaseTree kejs, int index, StatementTree statement) {
        return this.delegate.insertCaseStatement(kejs, index, statement);
    }

    public CaseTree removeCaseStatement(CaseTree kejs, StatementTree statement) {
        return this.delegate.removeCaseStatement(kejs, statement);
    }

    public CaseTree removeCaseStatement(CaseTree kejs, int index) {
        return this.delegate.removeCaseStatement(kejs, index);
    }

    public ClassTree addClassMember(ClassTree clazz, Tree member) {
        return this.delegate.addClassMember(clazz, member);
    }

    public ClassTree insertClassMember(ClassTree clazz, int index, Tree member) {
        return this.delegate.insertClassMember(clazz, index, member);
    }

    public ClassTree removeClassMember(ClassTree clazz, Tree member) {
        return this.delegate.removeClassMember(clazz, member);
    }

    public ClassTree removeClassMember(ClassTree clazz, int index) {
        return this.delegate.removeClassMember(clazz, index);
    }

    public ClassTree addClassTypeParameter(ClassTree clazz, TypeParameterTree typeParameter) {
        return this.delegate.addClassTypeParameter(clazz, typeParameter);
    }

    public ClassTree insertClassTypeParameter(ClassTree clazz, int index, TypeParameterTree typeParameter) {
        return this.delegate.insertClassTypeParameter(clazz, index, typeParameter);
    }

    public ClassTree removeClassTypeParameter(ClassTree clazz, TypeParameterTree typeParameter) {
        return this.delegate.removeClassTypeParameter(clazz, typeParameter);
    }

    public ClassTree removeClassTypeParameter(ClassTree clazz, int index) {
        return this.delegate.removeClassTypeParameter(clazz, index);
    }

    public ClassTree addClassImplementsClause(ClassTree clazz, Tree implementsClause) {
        return this.delegate.addClassImplementsClause(clazz, implementsClause);
    }

    public ClassTree insertClassImplementsClause(ClassTree clazz, int index, Tree implementsClause) {
        return this.delegate.insertClassImplementsClause(clazz, index, implementsClause);
    }

    public ClassTree removeClassImplementsClause(ClassTree clazz, Tree implementsClause) {
        return this.delegate.removeClassImplementsClause(clazz, this.asRemoved((T)implementsClause));
    }

    public ClassTree removeClassImplementsClause(ClassTree clazz, int index) {
        return this.delegate.removeClassImplementsClause(clazz, index);
    }

    public CompilationUnitTree addCompUnitTypeDecl(CompilationUnitTree compilationUnit, Tree typeDeclaration) {
        return this.delegate.addCompUnitTypeDecl(compilationUnit, typeDeclaration);
    }

    public CompilationUnitTree insertCompUnitTypeDecl(CompilationUnitTree compilationUnit, int index, Tree typeDeclaration) {
        return this.delegate.insertCompUnitTypeDecl(compilationUnit, index, typeDeclaration);
    }

    public CompilationUnitTree removeCompUnitTypeDecl(CompilationUnitTree compilationUnit, Tree typeDeclaration) {
        return this.delegate.removeCompUnitTypeDecl(compilationUnit, typeDeclaration);
    }

    public CompilationUnitTree removeCompUnitTypeDecl(CompilationUnitTree compilationUnit, int index) {
        return this.delegate.removeCompUnitTypeDecl(compilationUnit, index);
    }

    public CompilationUnitTree addCompUnitImport(CompilationUnitTree compilationUnit, ImportTree importt) {
        return this.delegate.addCompUnitImport(compilationUnit, importt);
    }

    public CompilationUnitTree insertCompUnitImport(CompilationUnitTree compilationUnit, int index, ImportTree importt) {
        return this.delegate.insertCompUnitImport(compilationUnit, index, importt);
    }

    public CompilationUnitTree removeCompUnitImport(CompilationUnitTree compilationUnit, ImportTree importt) {
        return this.delegate.removeCompUnitImport(compilationUnit, importt);
    }

    public CompilationUnitTree removeCompUnitImport(CompilationUnitTree compilationUnit, int index) {
        return this.delegate.removeCompUnitImport(compilationUnit, index);
    }

    public CompilationUnitTree addPackageAnnotation(CompilationUnitTree cut, AnnotationTree annotation) {
        return this.delegate.addPackageAnnotation(cut, annotation);
    }

    public CompilationUnitTree insertPackageAnnotation(CompilationUnitTree cut, int index, AnnotationTree annotation) {
        return this.delegate.insertPackageAnnotation(cut, index, annotation);
    }

    public CompilationUnitTree removePackageAnnotation(CompilationUnitTree cut, AnnotationTree annotation) {
        return this.delegate.removePackageAnnotation(cut, annotation);
    }

    public CompilationUnitTree removePackageAnnotation(CompilationUnitTree cut, int index) {
        return this.delegate.removePackageAnnotation(cut, index);
    }

    public ForLoopTree addForLoopInitializer(ForLoopTree forLoop, StatementTree initializer) {
        return this.delegate.addForLoopInitializer(forLoop, initializer);
    }

    public ForLoopTree insertForLoopInitializer(ForLoopTree forLoop, int index, StatementTree initializer) {
        return this.delegate.insertForLoopInitializer(forLoop, index, initializer);
    }

    public ForLoopTree removeForLoopInitializer(ForLoopTree forLoop, StatementTree initializer) {
        return this.delegate.removeForLoopInitializer(forLoop, initializer);
    }

    public ForLoopTree removeForLoopInitializer(ForLoopTree forLoop, int index) {
        return this.delegate.removeForLoopInitializer(forLoop, index);
    }

    public ForLoopTree addForLoopUpdate(ForLoopTree forLoop, ExpressionStatementTree update) {
        return this.delegate.addForLoopUpdate(forLoop, update);
    }

    public ForLoopTree insertForLoopUpdate(ForLoopTree forLoop, int index, ExpressionStatementTree update) {
        return this.delegate.insertForLoopUpdate(forLoop, index, update);
    }

    public ForLoopTree removeForLoopUpdate(ForLoopTree forLoop, ExpressionStatementTree update) {
        return this.delegate.removeForLoopUpdate(forLoop, update);
    }

    public ForLoopTree removeForLoopUpdate(ForLoopTree forLoop, int index) {
        return this.delegate.removeForLoopUpdate(forLoop, index);
    }

    public MethodInvocationTree addMethodInvocationArgument(MethodInvocationTree methodInvocation, ExpressionTree argument) {
        return this.delegate.addMethodInvocationArgument(methodInvocation, argument);
    }

    public MethodInvocationTree insertMethodInvocationArgument(MethodInvocationTree methodInvocation, int index, ExpressionTree argument) {
        return this.delegate.insertMethodInvocationArgument(methodInvocation, index, argument);
    }

    public MethodInvocationTree removeMethodInvocationArgument(MethodInvocationTree methodInvocation, ExpressionTree argument) {
        return this.delegate.removeMethodInvocationArgument(methodInvocation, argument);
    }

    public MethodInvocationTree removeMethodInvocationArgument(MethodInvocationTree methodInvocation, int index) {
        return this.delegate.removeMethodInvocationArgument(methodInvocation, index);
    }

    public MethodInvocationTree addMethodInvocationTypeArgument(MethodInvocationTree methodInvocation, ExpressionTree typeArgument) {
        return this.delegate.addMethodInvocationTypeArgument(methodInvocation, typeArgument);
    }

    public MethodInvocationTree insertMethodInvocationTypeArgument(MethodInvocationTree methodInvocation, int index, ExpressionTree typeArgument) {
        return this.delegate.insertMethodInvocationTypeArgument(methodInvocation, index, typeArgument);
    }

    public MethodInvocationTree removeMethodInvocationTypeArgument(MethodInvocationTree methodInvocation, ExpressionTree typeArgument) {
        return this.delegate.removeMethodInvocationTypeArgument(methodInvocation, typeArgument);
    }

    public MethodInvocationTree removeMethodInvocationTypeArgument(MethodInvocationTree methodInvocation, int index) {
        return this.delegate.removeMethodInvocationTypeArgument(methodInvocation, index);
    }

    public MethodTree addMethodParameter(MethodTree method, VariableTree parameter) {
        return this.delegate.addMethodParameter(method, parameter);
    }

    public MethodTree insertMethodParameter(MethodTree method, int index, VariableTree parameter) {
        return this.delegate.insertMethodParameter(method, index, parameter);
    }

    public MethodTree removeMethodParameter(MethodTree method, VariableTree parameter) {
        return this.delegate.removeMethodParameter(method, parameter);
    }

    public MethodTree removeMethodParameter(MethodTree method, int index) {
        return this.delegate.removeMethodParameter(method, index);
    }

    public MethodTree addMethodTypeParameter(MethodTree method, TypeParameterTree typeParameter) {
        return this.delegate.addMethodTypeParameter(method, typeParameter);
    }

    public MethodTree insertMethodTypeParameter(MethodTree method, int index, TypeParameterTree typeParameter) {
        return this.delegate.insertMethodTypeParameter(method, index, typeParameter);
    }

    public MethodTree removeMethodTypeParameter(MethodTree method, TypeParameterTree typeParameter) {
        return this.delegate.removeMethodTypeParameter(method, typeParameter);
    }

    public MethodTree removeMethodTypeParameter(MethodTree method, int index) {
        return this.delegate.removeMethodTypeParameter(method, index);
    }

    public MethodTree addMethodThrows(MethodTree method, ExpressionTree throwz) {
        return this.delegate.addMethodThrows(method, throwz);
    }

    public MethodTree insertMethodThrows(MethodTree method, int index, ExpressionTree throwz) {
        return this.delegate.insertMethodThrows(method, index, throwz);
    }

    public MethodTree removeMethodThrows(MethodTree method, ExpressionTree throwz) {
        return this.delegate.removeMethodThrows(method, throwz);
    }

    public MethodTree removeMethodThrows(MethodTree method, int index) {
        return this.delegate.removeMethodThrows(method, index);
    }

    public ModifiersTree addModifiersAnnotation(ModifiersTree modifiers, AnnotationTree annotation) {
        return this.delegate.addModifiersAnnotation(modifiers, annotation);
    }

    public ModifiersTree insertModifiersAnnotation(ModifiersTree modifiers, int index, AnnotationTree annotation) {
        return this.delegate.insertModifiersAnnotation(modifiers, index, annotation);
    }

    public ModifiersTree removeModifiersAnnotation(ModifiersTree modifiers, AnnotationTree annotation) {
        return this.delegate.removeModifiersAnnotation(modifiers, annotation);
    }

    public ModifiersTree removeModifiersAnnotation(ModifiersTree modifiers, int index) {
        return this.delegate.removeModifiersAnnotation(modifiers, index);
    }

    public ModifiersTree addModifiersModifier(ModifiersTree modifiers, Modifier modifier) {
        long c = ((JCTree.JCModifiers)modifiers).flags & -68719476737L;
        switch (modifier) {
            case ABSTRACT: {
                c |= 1024;
                break;
            }
            case FINAL: {
                c |= 16;
                break;
            }
            case NATIVE: {
                c |= 256;
                break;
            }
            case PRIVATE: {
                c |= 2;
                break;
            }
            case PROTECTED: {
                c |= 4;
                break;
            }
            case PUBLIC: {
                c |= 1;
                break;
            }
            case STATIC: {
                c |= 8;
                break;
            }
            case STRICTFP: {
                c |= 2048;
                break;
            }
            case SYNCHRONIZED: {
                c |= 32;
                break;
            }
            case TRANSIENT: {
                c |= 128;
                break;
            }
            case VOLATILE: {
                c |= 64;
                break;
            }
            case DEFAULT: {
                c |= 0x80000000000L;
                break;
            }
        }
        return this.Modifiers(c, modifiers.getAnnotations());
    }

    public ModifiersTree removeModifiersModifier(ModifiersTree modifiers, Modifier modifier) {
        long c = ((JCTree.JCModifiers)modifiers).flags & -68719476737L;
        switch (modifier) {
            case ABSTRACT: {
                c &= -1025;
                break;
            }
            case FINAL: {
                c &= -17;
                break;
            }
            case NATIVE: {
                c &= -257;
                break;
            }
            case PRIVATE: {
                c &= -3;
                break;
            }
            case PROTECTED: {
                c &= -5;
                break;
            }
            case PUBLIC: {
                c &= -2;
                break;
            }
            case STATIC: {
                c &= -9;
                break;
            }
            case STRICTFP: {
                c &= -2049;
                break;
            }
            case SYNCHRONIZED: {
                c &= -33;
                break;
            }
            case TRANSIENT: {
                c &= -129;
                break;
            }
            case VOLATILE: {
                c &= -65;
                break;
            }
            case DEFAULT: {
                c &= -8796093022209L;
                break;
            }
        }
        return this.Modifiers(c, modifiers.getAnnotations());
    }

    public NewArrayTree addNewArrayDimension(NewArrayTree newArray, ExpressionTree dimension) {
        return this.delegate.addNewArrayDimension(newArray, dimension);
    }

    public NewArrayTree insertNewArrayDimension(NewArrayTree newArray, int index, ExpressionTree dimension) {
        return this.delegate.insertNewArrayDimension(newArray, index, dimension);
    }

    public NewArrayTree removeNewArrayDimension(NewArrayTree newArray, ExpressionTree dimension) {
        return this.delegate.removeNewArrayDimension(newArray, dimension);
    }

    public NewArrayTree removeNewArrayDimension(NewArrayTree newArray, int index) {
        return this.delegate.removeNewArrayDimension(newArray, index);
    }

    public NewArrayTree addNewArrayInitializer(NewArrayTree newArray, ExpressionTree initializer) {
        return this.delegate.addNewArrayInitializer(newArray, initializer);
    }

    public NewArrayTree insertNewArrayInitializer(NewArrayTree newArray, int index, ExpressionTree initializer) {
        return this.delegate.insertNewArrayInitializer(newArray, index, initializer);
    }

    public NewArrayTree removeNewArrayInitializer(NewArrayTree newArray, ExpressionTree initializer) {
        return this.delegate.removeNewArrayInitializer(newArray, initializer);
    }

    public NewArrayTree removeNewArrayInitializer(NewArrayTree newArray, int index) {
        return this.delegate.removeNewArrayInitializer(newArray, index);
    }

    public NewClassTree addNewClassArgument(NewClassTree newClass, ExpressionTree argument) {
        return this.delegate.addNewClassArgument(newClass, argument);
    }

    public NewClassTree insertNewClassArgument(NewClassTree newClass, int index, ExpressionTree argument) {
        return this.delegate.insertNewClassArgument(newClass, index, argument);
    }

    public NewClassTree removeNewClassArgument(NewClassTree newClass, ExpressionTree argument) {
        return this.delegate.removeNewClassArgument(newClass, argument);
    }

    public NewClassTree removeNewClassArgument(NewClassTree newClass, int index) {
        return this.delegate.removeNewClassArgument(newClass, index);
    }

    public NewClassTree addNewClassTypeArgument(NewClassTree newClass, ExpressionTree typeArgument) {
        return this.delegate.addNewClassTypeArgument(newClass, typeArgument);
    }

    public NewClassTree insertNewClassTypeArgument(NewClassTree newClass, int index, ExpressionTree typeArgument) {
        return this.delegate.insertNewClassTypeArgument(newClass, index, typeArgument);
    }

    public NewClassTree removeNewClassTypeArgument(NewClassTree newClass, ExpressionTree typeArgument) {
        return this.delegate.removeNewClassTypeArgument(newClass, typeArgument);
    }

    public NewClassTree removeNewClassTypeArgument(NewClassTree newClass, int index) {
        return this.delegate.removeNewClassTypeArgument(newClass, index);
    }

    public ParameterizedTypeTree addParameterizedTypeTypeArgument(ParameterizedTypeTree parameterizedType, ExpressionTree argument) {
        return this.delegate.addParameterizedTypeTypeArgument(parameterizedType, argument);
    }

    public ParameterizedTypeTree insertParameterizedTypeTypeArgument(ParameterizedTypeTree parameterizedType, int index, ExpressionTree argument) {
        return this.delegate.insertParameterizedTypeTypeArgument(parameterizedType, index, argument);
    }

    public ParameterizedTypeTree removeParameterizedTypeTypeArgument(ParameterizedTypeTree parameterizedType, ExpressionTree argument) {
        return this.delegate.removeParameterizedTypeTypeArgument(parameterizedType, argument);
    }

    public ParameterizedTypeTree removeParameterizedTypeTypeArgument(ParameterizedTypeTree parameterizedType, int index) {
        return this.delegate.removeParameterizedTypeTypeArgument(parameterizedType, index);
    }

    public SwitchTree addSwitchCase(SwitchTree swic, CaseTree kejs) {
        return this.delegate.addSwitchCase(swic, kejs);
    }

    public SwitchTree insertSwitchCase(SwitchTree swic, int index, CaseTree kejs) {
        return this.delegate.insertSwitchCase(swic, index, kejs);
    }

    public SwitchTree removeSwitchCase(SwitchTree swic, CaseTree kejs) {
        return this.delegate.removeSwitchCase(swic, kejs);
    }

    public SwitchTree removeSwitchCase(SwitchTree swic, int index) {
        return this.delegate.removeSwitchCase(swic, index);
    }

    public TryTree addTryCatch(TryTree traj, CatchTree kec) {
        return this.delegate.addTryCatch(traj, kec);
    }

    public TryTree insertTryCatch(TryTree traj, int index, CatchTree kec) {
        return this.delegate.insertTryCatch(traj, index, kec);
    }

    public TryTree removeTryCatch(TryTree traj, CatchTree kec) {
        return this.delegate.removeTryCatch(traj, kec);
    }

    public TryTree removeTryCatch(TryTree traj, int index) {
        return this.delegate.removeTryCatch(traj, index);
    }

    public TypeParameterTree addTypeParameterBound(TypeParameterTree typeParameter, ExpressionTree bound) {
        return this.delegate.addTypeParameterBound(typeParameter, bound);
    }

    public TypeParameterTree insertTypeParameterBound(TypeParameterTree typeParameter, int index, ExpressionTree bound) {
        return this.delegate.insertTypeParameterBound(typeParameter, index, bound);
    }

    public TypeParameterTree removeTypeParameterBound(TypeParameterTree typeParameter, ExpressionTree bound) {
        return this.delegate.removeTypeParameterBound(typeParameter, bound);
    }

    public TypeParameterTree removeTypeParameterBound(TypeParameterTree typeParameter, int index) {
        return this.delegate.removeTypeParameterBound(typeParameter, index);
    }

    public <N extends Tree> N setLabel(N node, CharSequence aLabel) throws IllegalArgumentException {
        N result = this.asReplacementOf((T)this.setLabelImpl(node, aLabel), (Tree)node, true);
        GeneratorUtilities gu = GeneratorUtilities.get(this.copy);
        gu.copyComments((Tree)node, (Tree)result, true);
        gu.copyComments((Tree)node, (Tree)result, false);
        return result;
    }

    private <N extends Tree> N setLabelImpl(N node, CharSequence aLabel) throws IllegalArgumentException {
        Tree.Kind kind = node.getKind();
        switch (kind) {
            case BREAK: {
                BreakTree t = (BreakTree)node;
                BreakTree clone = this.Break(aLabel);
                return (N)clone;
            }
            case ANNOTATION_TYPE: 
            case CLASS: 
            case ENUM: 
            case INTERFACE: {
                ClassTree t = (ClassTree)node;
                List members = t.getMembers();
                ArrayList<Object> membersCopy = new ArrayList<Object>();
                for (Tree member : members) {
                    MethodTree m;
                    if (member.getKind() == Tree.Kind.METHOD && "<init>".contentEquals((m = (MethodTree)member).getName())) {
                        if (this.model.getPos((Tree)t) != this.model.getPos((Tree)m)) {
                            MethodTree a = this.setLabel((N)m, aLabel);
                            this.model.setPos((Tree)a, this.model.getPos((Tree)m));
                            membersCopy.add((Object)a);
                            continue;
                        }
                        membersCopy.add((Object)member);
                        continue;
                    }
                    membersCopy.add((Object)member);
                }
                ClassTree clone = this.Class(t.getModifiers(), aLabel, t.getTypeParameters(), t.getExtendsClause(), t.getImplementsClause(), membersCopy);
                return (N)clone;
            }
            case CONTINUE: {
                ContinueTree t = (ContinueTree)node;
                ContinueTree clone = this.Continue(aLabel);
                return (N)clone;
            }
            case IDENTIFIER: {
                com.sun.source.tree.IdentifierTree t = (com.sun.source.tree.IdentifierTree)node;
                com.sun.source.tree.IdentifierTree clone = this.Identifier(aLabel);
                return (N)clone;
            }
            case LABELED_STATEMENT: {
                LabeledStatementTree t = (LabeledStatementTree)node;
                LabeledStatementTree clone = this.LabeledStatement(aLabel, t.getStatement());
                return (N)clone;
            }
            case MEMBER_SELECT: {
                MemberSelectTree t = (MemberSelectTree)node;
                MemberSelectTree clone = this.MemberSelect(t.getExpression(), aLabel);
                return (N)clone;
            }
            case MEMBER_REFERENCE: {
                MemberReferenceTree t = (MemberReferenceTree)node;
                MemberReferenceTree clone = this.MemberReference(t.getMode(), t.getQualifierExpression(), aLabel, t.getTypeArguments());
                return (N)clone;
            }
            case METHOD: {
                MethodTree t = (MethodTree)node;
                MethodTree clone = this.Method(t.getModifiers(), aLabel, t.getReturnType(), t.getTypeParameters(), t.getParameters(), t.getThrows(), t.getBody(), (ExpressionTree)t.getDefaultValue());
                return (N)clone;
            }
            case TYPE_PARAMETER: {
                TypeParameterTree t = (TypeParameterTree)node;
                TypeParameterTree clone = this.TypeParameter(aLabel, t.getBounds());
                return (N)clone;
            }
            case VARIABLE: {
                VariableTree t = (VariableTree)node;
                VariableTree clone = this.Variable(t.getModifiers(), aLabel, t.getType(), t.getInitializer());
                this.model.setPos((Tree)clone, this.model.getPos((Tree)t));
                return (N)clone;
            }
        }
        throw new IllegalArgumentException("Invalid node's kind. Supported kinds are BREAK, CLASS, CONTINUE, IDENTIFIER, LABELED_STATEMENT, MEMBER_SELECT, METHOD, TYPE_PARAMETER, VARIABLE");
    }

    public ClassTree setExtends(ClassTree node, ExpressionTree extendz) {
        ClassTree result = this.Class(node.getModifiers(), node.getSimpleName(), node.getTypeParameters(), (Tree)extendz, node.getImplementsClause(), node.getMembers());
        return result;
    }

    public <N extends Tree> N setInitialValue(N node, ExpressionTree initializer) {
        switch (node.getKind()) {
            case VARIABLE: {
                VariableTree t = (VariableTree)node;
                VariableTree clone = this.Variable(t.getModifiers(), t.getName(), t.getType(), initializer);
                return (N)clone;
            }
            case METHOD: {
                MethodTree t = (MethodTree)node;
                MethodTree clone = this.Method(t.getModifiers(), (CharSequence)t.getName(), t.getReturnType(), t.getTypeParameters(), t.getParameters(), t.getThrows(), t.getBody(), initializer);
                return (N)clone;
            }
        }
        throw new IllegalArgumentException("Invalid kind " + (Object)node.getKind());
    }

    public void addComment(Tree tree, Comment comment, boolean preceding) throws IllegalStateException {
        this.insertComment(tree, comment, -1, preceding);
    }

    public void insertComment(Tree tree, Comment comment, int index, boolean preceding) throws IllegalStateException {
        if (this.handler == null) {
            throw new IllegalStateException("Cannot modify comments outside runModificationTask.");
        }
        CommentSet set = this.handler.getComments(tree);
        if (set == null) {
            if (index != 0 && index != -1) {
                throw new IllegalArgumentException("Index out of bounds: " + index);
            }
            this.handler.addComment(tree, comment);
            if (!preceding) {
                set = this.handler.getComments(tree);
                assert (set != null);
                set.addTrailingComment(comment);
                set.getPrecedingComments().remove(comment);
            }
        } else if (index == -1) {
            if (preceding) {
                set.addPrecedingComment(comment);
            } else {
                set.addTrailingComment(comment);
            }
        } else {
            List<Comment> comments = preceding ? set.getPrecedingComments() : set.getTrailingComments();
            if (comments.size() >= index) {
                comments.add(index, comment);
            } else {
                throw new IllegalArgumentException("Index out of bounds, index=" + index + ", length=" + comments.size());
            }
        }
    }

    public void removeComment(Tree tree, int index, boolean preceding) throws IllegalStateException {
        if (this.handler == null) {
            throw new IllegalStateException("Cannot modify comments outside runModificationTask.");
        }
        CommentSet set = this.handler.getComments(tree);
        if (set == null) {
            throw new IllegalArgumentException("Index out of bounds: " + index);
        }
        List<Comment> comments = preceding ? set.getPrecedingComments() : set.getTrailingComments();
        if (comments.size() <= index) {
            throw new IllegalArgumentException("Index out of bounds, index=" + index + ", length=" + comments.size());
        }
        comments.remove(index);
    }

    public <T extends Tree> T asNew(T tree) {
        if (this.handler == null) {
            throw new IllegalStateException("Cannot work with comments outside runModificationTask.");
        }
        if (tree == null) {
            throw new NullPointerException("tree");
        }
        this.copy.associateTree((Tree)tree, null, true);
        return tree;
    }

    public <T extends Tree> T asReplacementOf(T treeNew, Tree treeOld) {
        return this.asReplacementOf(treeNew, treeOld, false);
    }

    public <T extends Tree> T asReplacementOf(T treeNew, Tree treeOld, boolean defaultOnly) {
        if (this.handler == null) {
            throw new IllegalStateException("Cannot work with comments outside runModificationTask.");
        }
        if (treeOld == null) {
            throw new NullPointerException("treeOld");
        }
        if (treeNew != null) {
            this.copy.associateTree((Tree)treeNew, treeOld, !defaultOnly);
        }
        return treeNew;
    }

    public <T extends Tree> T asRemoved(T tree) {
        if (tree == null) {
            throw new NullPointerException("tree");
        }
        this.copy.associateTree((Tree)tree, null, true);
        return tree;
    }

    public BlockTree createMethodBody(MethodTree method, String bodyText) {
        SourcePositions[] positions = new SourcePositions[1];
        TreeUtilities treeUtils = this.copy.getTreeUtilities();
        StatementTree body = treeUtils.parseStatement(bodyText, positions);
        assert (Tree.Kind.BLOCK == body.getKind());
        Scope scope = this.copy.getTrees().getScope(TreePath.getPath((CompilationUnitTree)this.copy.getCompilationUnit(), (Tree)method));
        treeUtils.attributeTree((Tree)body, scope);
        this.mapComments((BlockTree)body, bodyText, this.copy, this.handler, positions[0]);
        new TreePosCleaner().scan((Tree)body, null);
        return (BlockTree)body;
    }

    public MethodTree Method(ModifiersTree modifiers, CharSequence name, Tree returnType, List<? extends TypeParameterTree> typeParameters, List<? extends VariableTree> parameters, List<? extends ExpressionTree> throwsList, String bodyText, ExpressionTree defaultValue) {
        StatementTree body;
        SourcePositions[] positions = new SourcePositions[1];
        StatementTree statementTree = body = bodyText != null ? this.copy.getTreeUtilities().parseStatement(bodyText, positions) : null;
        if (body != null) {
            assert (Tree.Kind.BLOCK == body.getKind());
            this.mapComments((BlockTree)body, bodyText, this.copy, this.handler, positions[0]);
            new TreePosCleaner().scan((Tree)body, null);
        }
        return this.delegate.Method(modifiers, name, returnType, typeParameters, parameters, throwsList, (BlockTree)body, defaultValue);
    }

    private void mapComments(BlockTree block, String inputText, WorkingCopy copy, CommentHandler comments, SourcePositions positions) {
        TokenSequence seq = TokenHierarchy.create((CharSequence)inputText, (Language)JavaTokenId.language()).tokenSequence(JavaTokenId.language());
        AssignComments ti = new AssignComments((CompilationInfo)copy, (Tree)block, seq, positions);
        ti.scan((Tree)block, null);
    }

    public AttributeTree Attribute(CharSequence name, AttributeTree.ValueKind vkind, List<? extends DocTree> value) {
        return this.delegate.Attribute(name, vkind, value);
    }

    public AuthorTree Author(List<? extends DocTree> name) {
        return this.delegate.Author(name);
    }

    public EntityTree Entity(CharSequence name) {
        return this.delegate.Entity(name);
    }

    public DeprecatedTree Deprecated(List<? extends DocTree> text) {
        return this.delegate.Deprecated(text);
    }

    public DocCommentTree DocComment(List<? extends DocTree> firstSentence, List<? extends DocTree> body, List<? extends DocTree> tags) {
        return this.delegate.DocComment(firstSentence, body, tags);
    }

    public ParamTree Param(boolean isTypeParameter, IdentifierTree name, List<? extends DocTree> description) {
        return this.delegate.Param(isTypeParameter, name, description);
    }

    public LinkTree Link(ReferenceTree ref, List<? extends DocTree> label) {
        return this.delegate.Link(ref, label);
    }

    public com.sun.source.doctree.LiteralTree DocLiteral(TextTree text) {
        return this.delegate.Literal(text);
    }

    public ReturnTree DocReturn(List<? extends DocTree> description) {
        return this.delegate.Return(description);
    }

    public ReferenceTree Reference(@NullAllowed ExpressionTree qualExpr, @NullAllowed CharSequence member, @NullAllowed List<? extends Tree> paramTypes) {
        return this.delegate.Reference(qualExpr, member, paramTypes);
    }

    public SeeTree See(List<? extends DocTree> reference) {
        return this.delegate.See(reference);
    }

    public SerialTree Serial(List<? extends DocTree> description) {
        return this.delegate.Serial(description);
    }

    public SerialDataTree SerialData(List<? extends DocTree> description) {
        return this.delegate.SerialData(description);
    }

    public SerialFieldTree SerialField(IdentifierTree name, ReferenceTree type, List<? extends DocTree> description) {
        return this.delegate.SerialField(name, type, description);
    }

    public SinceTree Since(List<? extends DocTree> text) {
        return this.delegate.Since(text);
    }

    public StartElementTree StartElement(CharSequence name, List<? extends DocTree> attrs, boolean selfClosing) {
        return this.delegate.StartElement(name, attrs, selfClosing);
    }

    public TextTree Text(String text) {
        return this.delegate.Text(text);
    }

    public ThrowsTree Throws(ReferenceTree name, List<? extends DocTree> description) {
        return this.delegate.Throws(name, description);
    }

    public UnknownBlockTagTree UnknownBlockTag(CharSequence name, List<? extends DocTree> content) {
        return this.delegate.UnknownBlockTag(name, content);
    }

    public UnknownInlineTagTree UnknownInlineTag(CharSequence name, List<? extends DocTree> content) {
        return this.delegate.UnknownInlineTag(name, content);
    }

    public VersionTree Version(List<? extends DocTree> text) {
        return this.delegate.Version(text);
    }

    public ValueTree Value(ReferenceTree ref) {
        return this.delegate.Value(ref);
    }

    public com.sun.source.doctree.LiteralTree Code(TextTree text) {
        return this.delegate.Code(text);
    }

    public CommentTree Comment(String text) {
        return this.delegate.Comment(text);
    }

    public DocRootTree DocRoot() {
        return this.delegate.DocRoot();
    }

    public EndElementTree EndElement(CharSequence name) {
        return this.delegate.EndElement(name);
    }

    public ThrowsTree Exception(ReferenceTree name, List<? extends DocTree> description) {
        return this.delegate.Exception(name, description);
    }

    public IdentifierTree DocIdentifier(CharSequence name) {
        return this.delegate.DocIdentifier(name);
    }

    public InheritDocTree InheritDoc() {
        return this.delegate.InheritDoc();
    }

    public LinkTree LinkPlain(ReferenceTree ref, List<? extends DocTree> label) {
        return this.delegate.LinkPlain(ref, label);
    }

    public LambdaExpressionTree addLambdaParameter(LambdaExpressionTree method, VariableTree parameter) {
        return this.delegate.addLambdaParameter(method, parameter);
    }

    public LambdaExpressionTree insertLambdaParameter(LambdaExpressionTree method, int index, VariableTree parameter) {
        return this.delegate.insertLambdaParameter(method, index, parameter);
    }

    public LambdaExpressionTree removeLambdaParameter(LambdaExpressionTree method, VariableTree parameter) {
        return this.delegate.removeLambdaParameter(method, parameter);
    }

    public LambdaExpressionTree removeLambdaParameter(LambdaExpressionTree method, int index) {
        return this.delegate.removeLambdaParameter(method, index);
    }

    public LambdaExpressionTree setLambdaBody(LambdaExpressionTree method, Tree newBody) {
        return this.delegate.setLambdaBody(method, newBody);
    }

    private static class TreePosCleaner
    extends TreeScanner<Void, Void> {
        private TreePosCleaner() {
        }

        public Void scan(Tree tree, Void p) {
            if (tree != null) {
                ((JCTree)tree).pos = -2;
            }
            return (Void)TreeScanner.super.scan(tree, (Object)p);
        }
    }

}

