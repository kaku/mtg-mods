/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullUnknown
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.util.Parameters
 */
package org.netbeans.api.java.source;

import java.io.IOException;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullUnknown;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.modules.java.source.parsing.CompilationInfoImpl;
import org.netbeans.modules.java.source.parsing.JavacParser;
import org.netbeans.modules.java.source.parsing.JavacParserResult;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.util.Parameters;

public class CompilationController
extends CompilationInfo {
    CompilationController(CompilationInfoImpl impl) {
        super(impl);
    }

    @NullUnknown
    public static CompilationController get(@NonNull Parser.Result result) {
        Parameters.notNull((CharSequence)"result", (Object)result);
        CompilationController info = null;
        if (result instanceof JavacParserResult) {
            JavacParserResult javacResult = (JavacParserResult)result;
            info = javacResult.get(CompilationController.class);
        }
        return info;
    }

    @NonNull
    public JavaSource.Phase toPhase(@NonNull JavaSource.Phase phase) throws IOException {
        return this.impl.toPhase(phase);
    }

    @Override
    protected void doInvalidate() {
        JavacParser parser = this.impl.getParser();
        if (parser != null) {
            parser.resultFinished(false);
        }
    }
}

