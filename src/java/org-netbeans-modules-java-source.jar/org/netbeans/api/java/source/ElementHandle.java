/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.code.Symbol$MethodSymbol
 *  com.sun.tools.javac.code.Symbol$PackageSymbol
 *  com.sun.tools.javac.code.Symbol$TypeSymbol
 *  com.sun.tools.javac.code.Symbol$VarSymbol
 *  com.sun.tools.javac.code.Symtab
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.jvm.Target
 *  com.sun.tools.javac.model.JavacElements
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Name
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 *  org.openide.util.WeakSet
 */
package org.netbeans.api.java.source;

import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Symtab;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.jvm.Target;
import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.Name;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.TypeMirrorHandle;
import org.netbeans.modules.java.source.ElementHandleAccessor;
import org.netbeans.modules.java.source.parsing.CompilationInfoImpl;
import org.netbeans.modules.java.source.usages.ClassFileUtil;
import org.openide.util.Parameters;
import org.openide.util.WeakSet;

public final class ElementHandle<T extends Element> {
    private static final Logger log = Logger.getLogger(ElementHandle.class.getName());
    private final ElementKind kind;
    private final String[] signatures;
    private static final WeakSet<ElementHandle<?>> NORMALIZATION_CACHE;

    private /* varargs */ ElementHandle(ElementKind kind, String ... signatures) {
        assert (kind != null);
        assert (signatures != null);
        this.kind = kind;
        this.signatures = signatures;
    }

    @CheckForNull
    public T resolve(@NonNull CompilationInfo compilationInfo) {
        Parameters.notNull((CharSequence)"compilationInfo", (Object)compilationInfo);
        T result = this.resolveImpl(compilationInfo.impl.getJavacTask());
        if (result == null) {
            if (log.isLoggable(Level.INFO)) {
                log.log(Level.INFO, "Cannot resolve: {0}", this.toString());
            }
        } else if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "Resolved element = {0}", result);
        }
        return result;
    }

    private T resolveImpl(JavacTaskImpl jt) {
        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "Resolving element kind: {0}", (Object)this.kind);
        }
        switch (this.kind) {
            case PACKAGE: {
                assert (this.signatures.length == 1);
                return (T)jt.getElements().getPackageElement((CharSequence)this.signatures[0]);
            }
            case CLASS: 
            case INTERFACE: 
            case ENUM: 
            case ANNOTATION_TYPE: {
                assert (this.signatures.length == 1);
                Element type = ElementHandle.getTypeElementByBinaryName(this.signatures[0], jt);
                if (type instanceof TypeElement) {
                    return (T)type;
                }
                log.log(Level.INFO, "Resolved type is null for kind = {0}", (Object)this.kind);
                break;
            }
            case OTHER: {
                assert (this.signatures.length == 1);
                return (T)ElementHandle.getTypeElementByBinaryName(this.signatures[0], jt);
            }
            case METHOD: 
            case CONSTRUCTOR: {
                assert (this.signatures.length == 3);
                Element type = ElementHandle.getTypeElementByBinaryName(this.signatures[0], jt);
                if (type instanceof TypeElement) {
                    List<? extends Element> members = type.getEnclosedElements();
                    for (Element member : members) {
                        if (this.kind != member.getKind()) continue;
                        String[] desc = ClassFileUtil.createExecutableDescriptor((ExecutableElement)member);
                        assert (desc.length == 3);
                        if (!this.signatures[1].equals(desc[1]) || !this.signatures[2].equals(desc[2])) continue;
                        return (T)member;
                    }
                    break;
                }
                if (type != null) {
                    return (T)new Symbol.MethodSymbol(0, jt.getElements().getName((CharSequence)this.signatures[1]), Symtab.instance((Context)jt.getContext()).unknownType, (Symbol)type);
                }
                log.log(Level.INFO, "Resolved type is null for kind = {0}", (Object)this.kind);
                break;
            }
            case INSTANCE_INIT: 
            case STATIC_INIT: {
                assert (this.signatures.length == 2);
                Element type = ElementHandle.getTypeElementByBinaryName(this.signatures[0], jt);
                if (type instanceof TypeElement) {
                    List<? extends Element> members = type.getEnclosedElements();
                    for (Element member : members) {
                        if (this.kind != member.getKind()) continue;
                        String[] desc = ClassFileUtil.createExecutableDescriptor((ExecutableElement)member);
                        assert (desc.length == 2);
                        if (!this.signatures[1].equals(desc[1])) continue;
                        return (T)member;
                    }
                    break;
                }
                log.log(Level.INFO, "Resolved type is null for kind = {0}", (Object)this.kind);
                break;
            }
            case FIELD: 
            case ENUM_CONSTANT: {
                assert (this.signatures.length == 3);
                Element type = ElementHandle.getTypeElementByBinaryName(this.signatures[0], jt);
                if (type instanceof TypeElement) {
                    List<? extends Element> members = type.getEnclosedElements();
                    for (Element member : members) {
                        if (this.kind != member.getKind()) continue;
                        String[] desc = ClassFileUtil.createFieldDescriptor((VariableElement)member);
                        assert (desc.length == 3);
                        if (!this.signatures[1].equals(desc[1]) || !this.signatures[2].equals(desc[2])) continue;
                        return (T)member;
                    }
                    break;
                }
                if (type != null) {
                    return (T)new Symbol.VarSymbol(0, jt.getElements().getName((CharSequence)this.signatures[1]), Symtab.instance((Context)jt.getContext()).unknownType, (Symbol)type);
                }
                log.log(Level.INFO, "Resolved type is null for kind = {0}", (Object)this.kind);
                break;
            }
            case TYPE_PARAMETER: {
                if (this.signatures.length == 2) {
                    Element type = ElementHandle.getTypeElementByBinaryName(this.signatures[0], jt);
                    if (type instanceof TypeElement) {
                        List<? extends TypeParameterElement> tpes = ((TypeElement)type).getTypeParameters();
                        for (TypeParameterElement tpe : tpes) {
                            if (!tpe.getSimpleName().contentEquals(this.signatures[1])) continue;
                            return (T)tpe;
                        }
                        break;
                    }
                    log.log(Level.INFO, "Resolved type is null for kind = {0} signatures.length = {1}", new Object[]{this.kind, this.signatures.length});
                    break;
                }
                if (this.signatures.length == 4) {
                    Element type = ElementHandle.getTypeElementByBinaryName(this.signatures[0], jt);
                    if (type instanceof TypeElement) {
                        List<? extends Element> members = type.getEnclosedElements();
                        for (Element member : members) {
                            if (member.getKind() != ElementKind.METHOD && member.getKind() != ElementKind.CONSTRUCTOR) continue;
                            String[] desc = ClassFileUtil.createExecutableDescriptor((ExecutableElement)member);
                            assert (desc.length == 3);
                            if (!this.signatures[1].equals(desc[1]) || !this.signatures[2].equals(desc[2])) continue;
                            assert (member instanceof ExecutableElement);
                            List<? extends TypeParameterElement> tpes = ((ExecutableElement)member).getTypeParameters();
                            for (TypeParameterElement tpe : tpes) {
                                if (!tpe.getSimpleName().contentEquals(this.signatures[3])) continue;
                                return (T)tpe;
                            }
                        }
                        break;
                    }
                    log.log(Level.INFO, "Resolved type is null for kind = {0} signatures.length = {1}", new Object[]{this.kind, this.signatures.length});
                    break;
                }
                throw new IllegalStateException();
            }
            default: {
                throw new IllegalStateException();
            }
        }
        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "All resolvings failed. Returning null.");
        }
        return null;
    }

    public boolean signatureEquals(@NonNull ElementHandle<? extends Element> handle) {
        if (!ElementHandle.isSameKind(this.kind, handle.kind) || this.signatures.length != handle.signatures.length) {
            return false;
        }
        for (int i = 0; i < this.signatures.length; ++i) {
            if (this.signatures[i].equals(handle.signatures[i])) continue;
            return false;
        }
        return true;
    }

    private static boolean isSameKind(ElementKind k1, ElementKind k2) {
        if (k1 == k2 || k1 == ElementKind.OTHER && (k2.isClass() || k2.isInterface()) || k2 == ElementKind.OTHER && (k1.isClass() || k1.isInterface())) {
            return true;
        }
        return false;
    }

    @NonNull
    public String getBinaryName() throws IllegalStateException {
        if (this.kind.isClass() && !ElementHandle.isArray(this.signatures[0]) || this.kind.isInterface() || this.kind == ElementKind.OTHER) {
            return this.signatures[0];
        }
        throw new IllegalStateException();
    }

    @NonNull
    public String getQualifiedName() throws IllegalStateException {
        if (this.kind.isClass() && !ElementHandle.isArray(this.signatures[0]) || this.kind.isInterface() || this.kind == ElementKind.OTHER) {
            return this.signatures[0].replace(Target.DEFAULT.syntheticNameChar(), '.');
        }
        throw new IllegalStateException();
    }

    public boolean signatureEquals(@NonNull T element) {
        ElementKind thisKind;
        ElementKind ek = element.getKind();
        if (ek != (thisKind = this.getKind()) && (thisKind != ElementKind.OTHER || !ek.isClass() && !ek.isInterface())) {
            return false;
        }
        ElementHandle<T> handle = ElementHandle.create(element);
        return this.signatureEquals((T)handle);
    }

    @NonNull
    public ElementKind getKind() {
        return this.kind;
    }

    @NonNull
    public static <T extends Element> ElementHandle<T> create(@NonNull T element) throws IllegalArgumentException {
        ElementHandle<T> eh = ElementHandle.createImpl(element);
        return (ElementHandle)NORMALIZATION_CACHE.putIfAbsent(eh);
    }

    @NonNull
    public static ElementHandle<PackageElement> createPackageElementHandle(@NonNull String packageName) {
        Parameters.notNull((CharSequence)"packageName", (Object)packageName);
        return new ElementHandle<PackageElement>(ElementKind.PACKAGE, packageName);
    }

    @NonNull
    public static ElementHandle<TypeElement> createTypeElementHandle(@NonNull ElementKind kind, @NonNull String binaryName) throws IllegalArgumentException {
        Parameters.notNull((CharSequence)"kind", (Object)((Object)kind));
        Parameters.notNull((CharSequence)"binaryName", (Object)binaryName);
        if (!kind.isClass() && !kind.isInterface()) {
            throw new IllegalArgumentException(kind.toString());
        }
        return new ElementHandle<TypeElement>(kind, binaryName);
    }

    @NonNull
    private static <T extends Element> ElementHandle<T> createImpl(@NonNull T element) throws IllegalArgumentException {
        String[] signatures;
        Parameters.notNull((CharSequence)"element", element);
        ElementKind kind = element.getKind();
        switch (kind) {
            case PACKAGE: {
                assert (element instanceof PackageElement);
                signatures = new String[]{((PackageElement)element).getQualifiedName().toString()};
                break;
            }
            case CLASS: 
            case INTERFACE: 
            case ENUM: 
            case ANNOTATION_TYPE: {
                assert (element instanceof TypeElement);
                signatures = new String[]{ClassFileUtil.encodeClassNameOrArray((TypeElement)element)};
                break;
            }
            case METHOD: 
            case CONSTRUCTOR: 
            case INSTANCE_INIT: 
            case STATIC_INIT: {
                assert (element instanceof ExecutableElement);
                signatures = ClassFileUtil.createExecutableDescriptor((ExecutableElement)element);
                break;
            }
            case FIELD: 
            case ENUM_CONSTANT: {
                assert (element instanceof VariableElement);
                signatures = ClassFileUtil.createFieldDescriptor((VariableElement)element);
                break;
            }
            case TYPE_PARAMETER: {
                assert (element instanceof TypeParameterElement);
                TypeParameterElement tpe = (TypeParameterElement)element;
                Element ge = tpe.getGenericElement();
                ElementKind gek = ge.getKind();
                if (gek.isClass() || gek.isInterface()) {
                    assert (ge instanceof TypeElement);
                    signatures = new String[]{ClassFileUtil.encodeClassNameOrArray((TypeElement)ge), tpe.getSimpleName().toString()};
                    break;
                }
                if (gek == ElementKind.METHOD || gek == ElementKind.CONSTRUCTOR) {
                    assert (ge instanceof ExecutableElement);
                    String[] _sigs = ClassFileUtil.createExecutableDescriptor((ExecutableElement)ge);
                    signatures = new String[_sigs.length + 1];
                    System.arraycopy(_sigs, 0, signatures, 0, _sigs.length);
                    signatures[_sigs.length] = tpe.getSimpleName().toString();
                    break;
                }
                throw new IllegalArgumentException(gek.toString());
            }
            default: {
                throw new IllegalArgumentException(kind.toString());
            }
        }
        return new ElementHandle<T>(kind, signatures);
    }

    @NonNull
    public static ElementHandle<? extends TypeElement> from(@NonNull TypeMirrorHandle<? extends DeclaredType> typeMirrorHandle) {
        Parameters.notNull((CharSequence)"typeMirrorHandle", typeMirrorHandle);
        if (typeMirrorHandle.getKind() != TypeKind.DECLARED) {
            throw new IllegalStateException("Incorrect kind: " + (Object)((Object)typeMirrorHandle.getKind()));
        }
        return typeMirrorHandle.getElementHandle();
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(this.getClass().getSimpleName());
        result.append('[');
        result.append("kind=").append(this.kind.toString());
        result.append("; sigs=");
        for (String sig : this.signatures) {
            result.append(sig);
            result.append(' ');
        }
        result.append(']');
        return result.toString();
    }

    public int hashCode() {
        int hashCode = 0;
        for (String sig : this.signatures) {
            hashCode ^= sig != null ? sig.hashCode() : 0;
        }
        return hashCode;
    }

    public boolean equals(Object other) {
        if (other instanceof ElementHandle) {
            return this.signatureEquals((T)((ElementHandle)other));
        }
        return false;
    }

    String[] getSignature() {
        return this.signatures;
    }

    private static Element getTypeElementByBinaryName(String signature, JavacTaskImpl jt) {
        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "Calling getTypeElementByBinaryName: signature = {0}", signature);
        }
        if (ElementHandle.isNone(signature)) {
            return Symtab.instance((Context)jt.getContext()).noSymbol;
        }
        if (ElementHandle.isArray(signature)) {
            return Symtab.instance((Context)jt.getContext()).arrayClass;
        }
        JavacElements elements = jt.getElements();
        return elements.getTypeElementByBinaryName((CharSequence)signature);
    }

    private static boolean isNone(String signature) {
        return signature.length() == 0;
    }

    private static boolean isArray(String signature) {
        return signature.length() == 1 && signature.charAt(0) == '[';
    }

    static {
        ElementHandleAccessor.setInstance(new ElementHandleAccessorImpl());
        NORMALIZATION_CACHE = new WeakSet();
    }

    private static class ElementHandleAccessorImpl
    extends ElementHandleAccessor {
        private ElementHandleAccessorImpl() {
        }

        @Override
        public /* varargs */ ElementHandle create(ElementKind kind, String ... descriptors) {
            assert (kind != null);
            assert (descriptors != null);
            switch (kind) {
                case PACKAGE: {
                    if (descriptors.length != 1) {
                        throw new IllegalArgumentException();
                    }
                    return new ElementHandle(kind, descriptors);
                }
                case CLASS: 
                case INTERFACE: 
                case ENUM: 
                case ANNOTATION_TYPE: 
                case OTHER: {
                    if (descriptors.length != 1) {
                        throw new IllegalArgumentException();
                    }
                    return new ElementHandle(kind, descriptors);
                }
                case METHOD: 
                case CONSTRUCTOR: {
                    if (descriptors.length != 3) {
                        throw new IllegalArgumentException();
                    }
                    return new ElementHandle(kind, descriptors);
                }
                case INSTANCE_INIT: 
                case STATIC_INIT: {
                    if (descriptors.length != 2) {
                        throw new IllegalArgumentException();
                    }
                    return new ElementHandle(kind, descriptors);
                }
                case FIELD: 
                case ENUM_CONSTANT: {
                    if (descriptors.length != 3) {
                        throw new IllegalArgumentException();
                    }
                    return new ElementHandle(kind, descriptors);
                }
            }
            throw new IllegalArgumentException();
        }

        @Override
        public <T extends Element> T resolve(ElementHandle<T> handle, JavacTaskImpl jti) {
            return (T)handle.resolveImpl(jti);
        }

        @NonNull
        @Override
        public String[] getJVMSignature(@NonNull ElementHandle<?> handle) {
            return Arrays.copyOf(handle.signatures, handle.signatures.length);
        }
    }

}

