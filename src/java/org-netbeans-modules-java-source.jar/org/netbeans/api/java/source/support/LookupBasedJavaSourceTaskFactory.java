/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.parsing.spi.TaskIndexingMode
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Parameters
 */
package org.netbeans.api.java.source.support;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.JavaSourceTaskFactory;
import org.netbeans.api.java.source.support.OpenedEditors;
import org.netbeans.modules.parsing.spi.TaskIndexingMode;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Parameters;

public abstract class LookupBasedJavaSourceTaskFactory
extends JavaSourceTaskFactory {
    private Lookup.Result<FileObject> fileObjectResult;
    private Lookup.Result<DataObject> dataObjectResult;
    private Lookup.Result<Node> nodeResult;
    private List<FileObject> currentFiles;
    private LookupListener listener;
    private String[] supportedMimeTypes;

    public LookupBasedJavaSourceTaskFactory(JavaSource.Phase phase, JavaSource.Priority priority) {
        this(phase, priority, (String[])null);
    }

    public /* varargs */ LookupBasedJavaSourceTaskFactory(JavaSource.Phase phase, JavaSource.Priority priority, String ... supportedMimeTypes) {
        super(phase, priority, TaskIndexingMode.DISALLOWED_DURING_SCAN);
        this.currentFiles = Collections.emptyList();
        this.listener = new LookupListenerImpl();
        this.supportedMimeTypes = supportedMimeTypes != null ? (String[])supportedMimeTypes.clone() : null;
    }

    public /* varargs */ LookupBasedJavaSourceTaskFactory(@NonNull JavaSource.Phase phase, @NonNull JavaSource.Priority priority, @NonNull TaskIndexingMode taskIndexingMode, @NonNull String ... supportedMimeTypes) {
        super(phase, priority, taskIndexingMode);
        Parameters.notNull((CharSequence)"supportedMimeTypes", (Object)supportedMimeTypes);
        this.currentFiles = Collections.emptyList();
        this.listener = new LookupListenerImpl();
        this.supportedMimeTypes = supportedMimeTypes.length > 0 ? (String[])supportedMimeTypes.clone() : null;
    }

    protected final synchronized void setLookup(Lookup lookup) {
        if (this.fileObjectResult != null) {
            this.fileObjectResult.removeLookupListener(this.listener);
        }
        if (this.dataObjectResult != null) {
            this.dataObjectResult.removeLookupListener(this.listener);
        }
        if (this.nodeResult != null) {
            this.nodeResult.removeLookupListener(this.listener);
        }
        this.fileObjectResult = lookup.lookupResult(FileObject.class);
        this.dataObjectResult = lookup.lookupResult(DataObject.class);
        this.nodeResult = lookup.lookupResult(Node.class);
        this.fileObjectResult.addLookupListener(this.listener);
        this.dataObjectResult.addLookupListener(this.listener);
        this.nodeResult.addLookupListener(this.listener);
        this.updateCurrentFiles();
        this.fileObjectsChanged();
    }

    private synchronized void updateCurrentFiles() {
        HashSet<FileObject> newCurrentFiles = new HashSet<FileObject>();
        newCurrentFiles.addAll(this.fileObjectResult.allInstances());
        for (DataObject d : this.dataObjectResult.allInstances()) {
            newCurrentFiles.add(d.getPrimaryFile());
        }
        for (Node n : this.nodeResult.allInstances()) {
            newCurrentFiles.addAll(n.getLookup().lookupAll(FileObject.class));
            for (DataObject d2 : n.getLookup().lookupAll(DataObject.class)) {
                newCurrentFiles.add(d2.getPrimaryFile());
            }
        }
        List<FileObject> newCurrentFilesFiltered = OpenedEditors.filterSupportedMIMETypes(new LinkedList<FileObject>(newCurrentFiles), this.supportedMimeTypes);
        if (!this.currentFiles.equals(newCurrentFilesFiltered)) {
            this.currentFiles = newCurrentFilesFiltered;
            this.lookupContentChanged();
        }
    }

    public synchronized List<FileObject> getFileObjects() {
        return this.currentFiles;
    }

    protected void lookupContentChanged() {
    }

    private class LookupListenerImpl
    implements LookupListener {
        private LookupListenerImpl() {
        }

        public void resultChanged(LookupEvent ev) {
            LookupBasedJavaSourceTaskFactory.this.updateCurrentFiles();
            LookupBasedJavaSourceTaskFactory.this.fileObjectsChanged();
        }
    }

}

