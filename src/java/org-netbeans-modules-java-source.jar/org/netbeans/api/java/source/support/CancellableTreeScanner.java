/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.Tree
 *  com.sun.source.util.TreeScanner
 */
package org.netbeans.api.java.source.support;

import com.sun.source.tree.Tree;
import com.sun.source.util.TreeScanner;
import java.util.concurrent.atomic.AtomicBoolean;

public class CancellableTreeScanner<R, P>
extends TreeScanner<R, P> {
    private final AtomicBoolean internalCanceled;
    private final AtomicBoolean canceled;

    public CancellableTreeScanner() {
        this(null);
    }

    public CancellableTreeScanner(AtomicBoolean canceled) {
        this.canceled = canceled;
        this.internalCanceled = new AtomicBoolean();
    }

    protected boolean isCanceled() {
        return this.internalCanceled.get() || this.canceled != null && this.canceled.get();
    }

    public void cancel() {
        this.internalCanceled.set(true);
    }

    public R scan(Tree tree, P p) {
        if (this.isCanceled()) {
            return null;
        }
        return (R)TreeScanner.super.scan(tree, p);
    }

    public R scan(Iterable<? extends Tree> trees, P p) {
        if (this.isCanceled()) {
            return null;
        }
        return (R)TreeScanner.super.scan(trees, p);
    }
}

