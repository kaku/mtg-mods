/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.annotations.common.NullUnknown
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.api.java.platform.JavaPlatform
 *  org.netbeans.api.java.platform.JavaPlatformManager
 *  org.netbeans.api.lexer.InputAttributes
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 *  org.netbeans.spi.java.classpath.ClassPathFactory
 *  org.netbeans.spi.java.classpath.ClassPathImplementation
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Parameters
 *  org.openide.util.WeakListeners
 */
package org.netbeans.api.java.source;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeListener;
import javax.swing.text.Document;
import javax.tools.JavaFileManager;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.annotations.common.NullUnknown;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.api.java.platform.JavaPlatformManager;
import org.netbeans.api.java.source.ClassIndex;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.source.classpath.AptSourcePath;
import org.netbeans.modules.java.source.classpath.CacheClassPath;
import org.netbeans.modules.java.source.classpath.SourcePath;
import org.netbeans.modules.java.source.indexing.TransactionContext;
import org.netbeans.modules.java.source.parsing.AptSourceFileManager;
import org.netbeans.modules.java.source.parsing.CachingArchiveProvider;
import org.netbeans.modules.java.source.parsing.CachingFileManager;
import org.netbeans.modules.java.source.parsing.FileManagerTransaction;
import org.netbeans.modules.java.source.parsing.InferableJavaFileObject;
import org.netbeans.modules.java.source.parsing.MemoryFileManager;
import org.netbeans.modules.java.source.parsing.OutputFileManager;
import org.netbeans.modules.java.source.parsing.ProcessorGenerated;
import org.netbeans.modules.java.source.parsing.ProxyFileManager;
import org.netbeans.modules.java.source.parsing.SiblingProvider;
import org.netbeans.modules.java.source.parsing.SiblingSource;
import org.netbeans.modules.java.source.parsing.SiblingSupport;
import org.netbeans.modules.java.source.parsing.SourceFileManager;
import org.netbeans.modules.java.source.usages.ClasspathInfoAccessor;
import org.netbeans.spi.java.classpath.ClassPathFactory;
import org.netbeans.spi.java.classpath.ClassPathImplementation;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.util.ChangeSupport;
import org.openide.util.Parameters;
import org.openide.util.WeakListeners;

public final class ClasspathInfo {
    private static final ClassPath EMPTY_PATH;
    private static final Logger log;
    private final CachingArchiveProvider archiveProvider;
    private final ClassPath srcClassPath;
    private final ClassPath bootClassPath;
    private final ClassPath compileClassPath;
    private final ClassPath cachedAptSrcClassPath;
    private final ClassPath cachedSrcClassPath;
    private final ClassPath cachedBootClassPath;
    private final ClassPath cachedCompileClassPath;
    private final ClassPath outputClassPath;
    private final ClassPathListener cpListener;
    private final boolean useModifiedFiles;
    private final boolean ignoreExcludes;
    private final JavaFileFilterImplementation filter;
    private final MemoryFileManager memoryFileManager;
    private final ChangeSupport listenerList;
    private final FileManagerTransaction fmTx;
    private final ProcessorGenerated pgTx;
    private ClassIndex usagesQuery;

    private ClasspathInfo(@NonNull CachingArchiveProvider archiveProvider, @NonNull ClassPath bootCp, @NonNull ClassPath compileCp, ClassPath srcCp, JavaFileFilterImplementation filter, boolean backgroundCompilation, boolean ignoreExcludes, boolean hasMemoryFileManager, boolean useModifiedFiles) {
        assert (archiveProvider != null);
        assert (bootCp != null);
        assert (compileCp != null);
        this.cpListener = new ClassPathListener();
        this.archiveProvider = archiveProvider;
        this.bootClassPath = bootCp;
        this.compileClassPath = compileCp;
        this.listenerList = new ChangeSupport((Object)this);
        this.cachedBootClassPath = CacheClassPath.forBootPath(this.bootClassPath, backgroundCompilation);
        this.cachedCompileClassPath = CacheClassPath.forClassPath(this.compileClassPath, backgroundCompilation);
        if (!backgroundCompilation) {
            this.cachedBootClassPath.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.cpListener, (Object)this.cachedBootClassPath));
            this.cachedCompileClassPath.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.cpListener, (Object)this.cachedCompileClassPath));
        }
        if (srcCp == null) {
            this.cachedSrcClassPath = this.srcClassPath = EMPTY_PATH;
            this.cachedAptSrcClassPath = null;
            this.outputClassPath = EMPTY_PATH;
        } else {
            this.srcClassPath = srcCp;
            ClassPathImplementation noApt = AptSourcePath.sources(srcCp);
            this.cachedSrcClassPath = ClassPathFactory.createClassPath((ClassPathImplementation)SourcePath.filtered(noApt, backgroundCompilation));
            this.cachedAptSrcClassPath = ClassPathFactory.createClassPath((ClassPathImplementation)SourcePath.filtered(AptSourcePath.aptCache(srcCp), backgroundCompilation));
            this.outputClassPath = CacheClassPath.forSourcePath(ClassPathFactory.createClassPath((ClassPathImplementation)noApt), backgroundCompilation);
            if (!backgroundCompilation) {
                this.cachedSrcClassPath.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.cpListener, (Object)this.cachedSrcClassPath));
            }
        }
        this.ignoreExcludes = ignoreExcludes;
        this.useModifiedFiles = useModifiedFiles;
        this.filter = filter;
        if (hasMemoryFileManager) {
            if (srcCp == null) {
                throw new IllegalStateException();
            }
            this.memoryFileManager = new MemoryFileManager();
        } else {
            this.memoryFileManager = null;
        }
        if (backgroundCompilation) {
            TransactionContext txCtx = TransactionContext.get();
            this.fmTx = txCtx.get(FileManagerTransaction.class);
            this.pgTx = txCtx.get(ProcessorGenerated.class);
        } else {
            this.fmTx = FileManagerTransaction.treeLoaderOnly();
            this.pgTx = ProcessorGenerated.nullWrite();
        }
        assert (this.fmTx != null);
        assert (this.pgTx != null);
    }

    public String toString() {
        return String.format("ClasspathInfo [boot: %s, compile: %s, src: %s, internal boot: %s, internal compile: %s, internal src: %s, internal out: %s]", new Object[]{this.bootClassPath, this.compileClassPath, this.srcClassPath, this.cachedBootClassPath, this.cachedCompileClassPath, this.cachedSrcClassPath, this.outputClassPath});
    }

    public int hashCode() {
        return Arrays.hashCode(ClasspathInfo.toURIs(this.srcClassPath));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ClasspathInfo)) {
            return false;
        }
        ClasspathInfo other = (ClasspathInfo)obj;
        return Arrays.equals(ClasspathInfo.toURIs(this.srcClassPath), ClasspathInfo.toURIs(other.srcClassPath)) && Arrays.equals(ClasspathInfo.toURIs(this.compileClassPath), ClasspathInfo.toURIs(other.compileClassPath)) && Arrays.equals(ClasspathInfo.toURIs(this.bootClassPath), ClasspathInfo.toURIs(other.bootClassPath));
    }

    @NullUnknown
    public static ClasspathInfo create(@NonNull File file) {
        if (file == null) {
            throw new IllegalArgumentException("Cannot pass null as parameter of ClasspathInfo.create(java.io.File)");
        }
        FileObject fo = FileUtil.toFileObject((File)file);
        if (fo == null) {
            return null;
        }
        return ClasspathInfo.create(fo);
    }

    @NullUnknown
    public static ClasspathInfo create(@NonNull Document doc) {
        Parameters.notNull((CharSequence)"doc", (Object)doc);
        Object source = doc.getProperty("stream");
        if (source instanceof DataObject) {
            DataObject dObj = (DataObject)source;
            return ClasspathInfo.create(dObj.getPrimaryFile());
        }
        String mimeType = (String)doc.getProperty("mimeType");
        if ("text/x-dialog-binding".equals(mimeType)) {
            LanguagePath path;
            Object obj;
            InputAttributes attributes = (InputAttributes)doc.getProperty(InputAttributes.class);
            Document d = (Document)attributes.getValue(path = LanguagePath.get((Language)((Language)MimeLookup.getLookup((String)mimeType).lookup(Language.class))), (Object)"dialogBinding.document");
            if (d != null && (obj = d.getProperty("stream")) instanceof DataObject) {
                DataObject dObj = (DataObject)obj;
                return ClasspathInfo.create(dObj.getPrimaryFile());
            }
            FileObject fileObject = (FileObject)attributes.getValue(path, (Object)"dialogBinding.fileObject");
            if (fileObject != null) {
                return ClasspathInfo.create(fileObject);
            }
        }
        return null;
    }

    private static ClasspathInfo create(@NonNull FileObject fo, JavaFileFilterImplementation filter, boolean backgroundCompilation, boolean ignoreExcludes, boolean hasMemoryFileManager, boolean useModifiedFiles) {
        ClassPath srcPath;
        ClassPath compilePath;
        ClassPath bootPath = ClassPath.getClassPath((FileObject)fo, (String)"classpath/boot");
        if (bootPath == null) {
            bootPath = JavaPlatformManager.getDefault().getDefaultPlatform().getBootstrapLibraries();
        }
        if ((compilePath = ClassPath.getClassPath((FileObject)fo, (String)"classpath/compile")) == null) {
            compilePath = EMPTY_PATH;
        }
        if ((srcPath = ClassPath.getClassPath((FileObject)fo, (String)"classpath/source")) == null) {
            srcPath = EMPTY_PATH;
        }
        return ClasspathInfo.create(bootPath, compilePath, srcPath, filter, backgroundCompilation, ignoreExcludes, hasMemoryFileManager, useModifiedFiles);
    }

    @NonNull
    public static ClasspathInfo create(@NonNull FileObject fo) {
        return ClasspathInfo.create(fo, null, false, false, false, true);
    }

    private static ClasspathInfo create(@NonNull ClassPath bootPath, @NonNull ClassPath classPath, ClassPath sourcePath, JavaFileFilterImplementation filter, boolean backgroundCompilation, boolean ignoreExcludes, boolean hasMemoryFileManager, boolean useModifiedFiles) {
        return new ClasspathInfo(CachingArchiveProvider.getDefault(), bootPath, classPath, sourcePath, filter, backgroundCompilation, ignoreExcludes, hasMemoryFileManager, useModifiedFiles);
    }

    @NonNull
    public static ClasspathInfo create(@NonNull ClassPath bootPath, @NonNull ClassPath classPath, @NullAllowed ClassPath sourcePath) {
        Parameters.notNull((CharSequence)"bootPath", (Object)bootPath);
        Parameters.notNull((CharSequence)"classPath", (Object)classPath);
        return ClasspathInfo.create(bootPath, classPath, sourcePath, null, false, false, false, true);
    }

    public void addChangeListener(@NonNull ChangeListener listener) {
        this.listenerList.addChangeListener(listener);
    }

    public synchronized void removeChangeListener(@NonNull ChangeListener listener) {
        this.listenerList.removeChangeListener(listener);
    }

    public ClassPath getClassPath(@NonNull PathKind pathKind) {
        switch (pathKind) {
            case BOOT: {
                return this.bootClassPath;
            }
            case COMPILE: {
                return this.compileClassPath;
            }
            case SOURCE: {
                return this.srcClassPath;
            }
        }
        assert (false);
        return null;
    }

    ClassPath getCachedClassPath(PathKind pathKind) {
        switch (pathKind) {
            case BOOT: {
                return this.cachedBootClassPath;
            }
            case COMPILE: {
                return this.cachedCompileClassPath;
            }
            case SOURCE: {
                return this.cachedSrcClassPath;
            }
            case OUTPUT: {
                return this.outputClassPath;
            }
        }
        assert (false);
        return null;
    }

    @NonNull
    public synchronized ClassIndex getClassIndex() {
        if (this.usagesQuery == null) {
            this.usagesQuery = new ClassIndex(this.bootClassPath, this.compileClassPath, this.cachedSrcClassPath);
        }
        return this.usagesQuery;
    }

    @NonNull
    private synchronized JavaFileManager createFileManager() {
        boolean hasSources = this.cachedSrcClassPath != EMPTY_PATH;
        SiblingSource siblings = SiblingSupport.create();
        ProxyFileManager fileManager = new ProxyFileManager(new CachingFileManager(this.archiveProvider, this.cachedBootClassPath, true, true), new CachingFileManager(this.archiveProvider, this.cachedCompileClassPath, false, true), hasSources ? (!this.useModifiedFiles ? new CachingFileManager(this.archiveProvider, this.cachedSrcClassPath, this.filter, false, this.ignoreExcludes) : new SourceFileManager(this.cachedSrcClassPath, this.ignoreExcludes)) : null, this.cachedAptSrcClassPath != null ? new AptSourceFileManager(this.cachedSrcClassPath, this.cachedAptSrcClassPath, siblings.getProvider(), this.fmTx) : null, hasSources ? new OutputFileManager(this.archiveProvider, this.outputClassPath, this.cachedSrcClassPath, this.cachedAptSrcClassPath, siblings.getProvider(), this.fmTx) : null, this.memoryFileManager, this.pgTx, siblings);
        return fileManager;
    }

    private void fireChangeListenerStateChanged() {
        this.listenerList.fireChange();
    }

    @CheckForNull
    private static URI[] toURIs(@NullAllowed ClassPath cp) {
        if (cp == null) {
            return null;
        }
        List entries = cp.entries();
        ArrayList<URI> roots = new ArrayList<URI>(entries.size());
        for (ClassPath.Entry entry : entries) {
            try {
                roots.add(entry.getURL().toURI());
            }
            catch (URISyntaxException ex) {
                log.log(Level.INFO, "Cannot convert {0} to URI.", entry.getURL());
            }
        }
        return roots.toArray(new URI[roots.size()]);
    }

    static {
        block2 : {
            EMPTY_PATH = ClassPathSupport.createClassPath((URL[])new URL[0]);
            log = Logger.getLogger(ClasspathInfo.class.getName());
            ClasspathInfoAccessor.setINSTANCE(new ClasspathInfoAccessorImpl());
            try {
                Class.forName(ClassIndex.class.getName(), true, CompilationInfo.class.getClassLoader());
            }
            catch (ClassNotFoundException ex) {
                if (!log.isLoggable(Level.SEVERE)) break block2;
                log.log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }

    private static class ClasspathInfoAccessorImpl
    extends ClasspathInfoAccessor {
        private ClasspathInfoAccessorImpl() {
        }

        @NonNull
        @Override
        public JavaFileManager createFileManager(@NonNull ClasspathInfo cpInfo) {
            return cpInfo.createFileManager();
        }

        @NonNull
        @Override
        public FileManagerTransaction getFileManagerTransaction(@NonNull ClasspathInfo cpInfo) {
            return cpInfo.fmTx;
        }

        @Override
        public ClassPath getCachedClassPath(ClasspathInfo cpInfo, PathKind kind) {
            return cpInfo.getCachedClassPath(kind);
        }

        @Override
        public ClasspathInfo create(ClassPath bootPath, ClassPath classPath, ClassPath sourcePath, JavaFileFilterImplementation filter, boolean backgroundCompilation, boolean ignoreExcludes, boolean hasMemoryFileManager, boolean useModifiedFiles) {
            return ClasspathInfo.create(bootPath, classPath, sourcePath, filter, backgroundCompilation, ignoreExcludes, hasMemoryFileManager, useModifiedFiles);
        }

        @Override
        public ClasspathInfo create(@NonNull FileObject fo, JavaFileFilterImplementation filter, boolean backgroundCompilation, boolean ignoreExcludes, boolean hasMemoryFileManager, boolean useModifiedFiles) {
            return ClasspathInfo.create(fo, filter, backgroundCompilation, ignoreExcludes, hasMemoryFileManager, useModifiedFiles);
        }

        @Override
        public boolean registerVirtualSource(ClasspathInfo cpInfo, InferableJavaFileObject jfo) throws UnsupportedOperationException {
            if (cpInfo.memoryFileManager == null) {
                throw new UnsupportedOperationException("The ClassPathInfo doesn't support memory JavacFileManager");
            }
            return cpInfo.memoryFileManager.register(jfo);
        }

        @Override
        public boolean unregisterVirtualSource(ClasspathInfo cpInfo, String fqn) throws UnsupportedOperationException {
            if (cpInfo.memoryFileManager == null) {
                throw new UnsupportedOperationException();
            }
            return cpInfo.memoryFileManager.unregister(fqn);
        }
    }

    private class ClassPathListener
    implements PropertyChangeListener {
        private ClassPathListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent event) {
            if ("roots".equals(event.getPropertyName())) {
                ClasspathInfo.this.fireChangeListenerStateChanged();
            }
        }
    }

    public static enum PathKind {
        BOOT,
        COMPILE,
        SOURCE,
        OUTPUT;
        

        private PathKind() {
        }
    }

}

