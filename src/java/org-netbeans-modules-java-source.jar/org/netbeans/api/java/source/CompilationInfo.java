/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.util.DocTrees
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.api.JavacTrees
 *  com.sun.tools.javac.code.Source
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.model.JavacElements
 *  com.sun.tools.javac.model.JavacTypes
 *  com.sun.tools.javac.util.Context
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.CheckReturnValue
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.annotations.common.NullUnknown
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.modules.java.preprocessorbridge.spi.WrapperFactory
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Pair
 *  org.openide.util.Parameters
 */
package org.netbeans.api.java.source;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.DocTrees;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.api.JavacTrees;
import com.sun.tools.javac.code.Source;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.model.JavacTypes;
import com.sun.tools.javac.util.Context;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.swing.text.Document;
import javax.tools.Diagnostic;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.CheckReturnValue;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.annotations.common.NullUnknown;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.PositionConverter;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.TypeUtilities;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.modules.java.preprocessorbridge.spi.WrapperFactory;
import org.netbeans.modules.java.source.parsing.CompilationInfoImpl;
import org.netbeans.modules.java.source.parsing.DocPositionRegion;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.JavacParser;
import org.netbeans.modules.java.source.parsing.JavacParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Pair;
import org.openide.util.Parameters;

public class CompilationInfo {
    private static final boolean VERIFY_CONFINEMENT = Boolean.getBoolean(CompilationInfo.class.getName() + ".vetifyConfinement");
    final CompilationInfoImpl impl;
    private boolean invalid;
    private Trees trees;
    private ElementUtilities elementUtilities;
    private TreeUtilities treeUtilities;
    private TypeUtilities typeUtilities;
    private JavaSource javaSource;

    CompilationInfo(CompilationInfoImpl impl) {
        assert (impl != null);
        this.impl = impl;
    }

    @NullUnknown
    public static CompilationInfo get(@NonNull Parser.Result result) {
        Parameters.notNull((CharSequence)"result", (Object)result);
        CompilationInfo info = null;
        if (result instanceof JavacParserResult) {
            JavacParserResult javacResult = (JavacParserResult)result;
            info = javacResult.get(CompilationInfo.class);
        }
        return info;
    }

    @NonNull
    public JavaSource.Phase getPhase() {
        this.checkConfinement();
        return this.impl.getPhase();
    }

    @CheckForNull
    @CheckReturnValue
    public TreePath getChangedTree() {
        this.checkConfinement();
        if (JavaSource.Phase.PARSED.compareTo(this.impl.getPhase()) > 0) {
            return null;
        }
        Pair<DocPositionRegion, MethodTree> changedTree = this.impl.getChangedTree();
        if (changedTree == null) {
            return null;
        }
        CompilationUnitTree cu = this.impl.getCompilationUnit();
        if (cu == null) {
            return null;
        }
        return TreePath.getPath((CompilationUnitTree)cu, (Tree)((Tree)changedTree.second()));
    }

    public CompilationUnitTree getCompilationUnit() {
        this.checkConfinement();
        return this.impl.getCompilationUnit();
    }

    @NonNull
    public String getText() {
        this.checkConfinement();
        return this.impl.getText();
    }

    @NonNull
    public Snapshot getSnapshot() {
        this.checkConfinement();
        return this.impl.getSnapshot();
    }

    @NonNull
    public TokenHierarchy<?> getTokenHierarchy() {
        this.checkConfinement();
        return this.impl.getTokenHierarchy();
    }

    @NonNull
    public List<Diagnostic> getDiagnostics() {
        this.checkConfinement();
        return this.impl.getDiagnostics();
    }

    @NullUnknown
    public List<? extends TypeElement> getTopLevelElements() throws IllegalStateException {
        this.checkConfinement();
        if (this.impl.getFileObject() == null) {
            throw new IllegalStateException();
        }
        ArrayList<Object> result = new ArrayList<Object>();
        if (this.impl.isClassFile()) {
            Elements elements = this.getElements();
            assert (elements != null);
            assert (this.impl.getRoot() != null);
            String name = FileObjects.convertFolder2Package(FileObjects.stripExtension(FileUtil.getRelativePath((FileObject)this.impl.getRoot(), (FileObject)this.impl.getFileObject())));
            Symbol.ClassSymbol e = ((JavacElements)elements).getTypeElementByBinaryName((CharSequence)name);
            if (e != null) {
                result.add((Object)e);
            }
        } else {
            CompilationUnitTree cu = this.getCompilationUnit();
            if (cu == null) {
                return null;
            }
            Trees ts = this.getTrees();
            assert (ts != null);
            List typeDecls = cu.getTypeDecls();
            TreePath cuPath = new TreePath(cu);
            for (Tree t : typeDecls) {
                TreePath p = new TreePath(cuPath, t);
                Element e = ts.getElement(p);
                if (e == null || !e.getKind().isClass() && !e.getKind().isInterface()) continue;
                result.add((TypeElement)e);
            }
        }
        return Collections.unmodifiableList(result);
    }

    @NonNull
    public synchronized Trees getTrees() {
        this.checkConfinement();
        if (this.trees == null) {
            WrapperFactory factory;
            this.trees = JavacTrees.instance((Context)this.impl.getJavacTask().getContext());
            Snapshot snapshot = this.impl.getSnapshot();
            Document doc = snapshot != null ? snapshot.getSource().getDocument(false) : null;
            WrapperFactory wrapperFactory = factory = doc != null ? (WrapperFactory)doc.getProperty(WrapperFactory.class) : null;
            if (factory != null) {
                this.trees = factory.wrapTrees(this.trees);
            }
        }
        return this.trees;
    }

    @NonNull
    public DocTrees getDocTrees() {
        return (DocTrees)this.getTrees();
    }

    @NonNull
    public Types getTypes() {
        this.checkConfinement();
        return this.impl.getJavacTask().getTypes();
    }

    @NonNull
    public Elements getElements() {
        this.checkConfinement();
        return this.impl.getJavacTask().getElements();
    }

    @NullUnknown
    public JavaSource getJavaSource() {
        this.checkConfinement();
        return this.javaSource;
    }

    void setJavaSource(JavaSource javaSource) {
        this.javaSource = javaSource;
    }

    @NonNull
    public ClasspathInfo getClasspathInfo() {
        this.checkConfinement();
        return this.impl.getClasspathInfo();
    }

    @NullUnknown
    public FileObject getFileObject() {
        this.checkConfinement();
        return this.impl.getFileObject();
    }

    @Deprecated
    public PositionConverter getPositionConverter() {
        this.checkConfinement();
        if (this.impl.getFileObject() == null) {
            throw new IllegalStateException();
        }
        return new PositionConverter(this.impl.getSnapshot());
    }

    @CheckForNull
    public Document getDocument() throws IOException {
        this.checkConfinement();
        return this.impl.getDocument();
    }

    @NonNull
    public synchronized TreeUtilities getTreeUtilities() {
        this.checkConfinement();
        if (this.treeUtilities == null) {
            this.treeUtilities = new TreeUtilities(this);
        }
        return this.treeUtilities;
    }

    @NonNull
    public synchronized ElementUtilities getElementUtilities() {
        this.checkConfinement();
        if (this.elementUtilities == null) {
            this.elementUtilities = new ElementUtilities(this);
        }
        return this.elementUtilities;
    }

    @NonNull
    public synchronized TypeUtilities getTypeUtilities() {
        this.checkConfinement();
        if (this.typeUtilities == null) {
            this.typeUtilities = new TypeUtilities(this);
        }
        return this.typeUtilities;
    }

    @NonNull
    public SourceVersion getSourceVersion() {
        this.checkConfinement();
        return Source.toSourceVersion((Source)Source.instance((Context)this.impl.getJavacTask().getContext()));
    }

    @CheckForNull
    public Object getCachedValue(@NonNull Object key) {
        Parameters.notNull((CharSequence)"key", (Object)key);
        return this.impl.getCachedValue(key);
    }

    public void putCachedValue(@NonNull Object key, @NullAllowed Object value, @NonNull CacheClearPolicy clearPolicy) {
        Parameters.notNull((CharSequence)"key", (Object)key);
        Parameters.notNull((CharSequence)"clearPolicy", (Object)((Object)clearPolicy));
        this.impl.putCachedValue(key, value, clearPolicy);
    }

    final void invalidate() {
        this.invalid = true;
        this.impl.taskFinished();
        this.doInvalidate();
    }

    protected void doInvalidate() {
        JavacParser parser = this.impl.getParser();
        if (parser != null) {
            parser.resultFinished(true);
        }
    }

    final void checkConfinement() throws IllegalStateException {
        if (VERIFY_CONFINEMENT && this.invalid) {
            throw new IllegalStateException(String.format("Access to the shared %s outside a guarded run method.", this.getClass().getSimpleName()));
        }
    }

    public static enum CacheClearPolicy {
        ON_TASK_END,
        ON_CHANGE,
        ON_SIGNATURE_CHANGE;
        

        private CacheClearPolicy() {
        }
    }

}

