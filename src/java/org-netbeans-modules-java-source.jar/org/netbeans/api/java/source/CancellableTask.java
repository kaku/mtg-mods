/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.java.source;

import org.netbeans.api.java.source.Task;

public interface CancellableTask<P>
extends Task<P> {
    public void cancel();
}

