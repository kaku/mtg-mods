/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.document.Document
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullUnknown
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.api.project.ProjectManager
 *  org.netbeans.modules.parsing.impl.Utilities
 *  org.netbeans.modules.parsing.impl.indexing.PathRegistry
 *  org.netbeans.modules.parsing.lucene.support.Convertor
 *  org.netbeans.modules.parsing.lucene.support.Index
 *  org.netbeans.modules.parsing.lucene.support.Index$IndexClosedException
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.Parameters
 *  org.openide.util.WeakListeners
 */
package org.netbeans.api.java.source;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import org.apache.lucene.document.Document;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullUnknown;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.source.ClassIndexListener;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.RootsEvent;
import org.netbeans.api.java.source.TypesEvent;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.usages.ClassIndexFactory;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;
import org.netbeans.modules.java.source.usages.ClassIndexImplEvent;
import org.netbeans.modules.java.source.usages.ClassIndexImplListener;
import org.netbeans.modules.java.source.usages.ClassIndexManager;
import org.netbeans.modules.java.source.usages.ClassIndexManagerEvent;
import org.netbeans.modules.java.source.usages.ClassIndexManagerListener;
import org.netbeans.modules.java.source.usages.DocumentUtil;
import org.netbeans.modules.parsing.impl.Utilities;
import org.netbeans.modules.parsing.impl.indexing.PathRegistry;
import org.netbeans.modules.parsing.lucene.support.Convertor;
import org.netbeans.modules.parsing.lucene.support.Index;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.Parameters;
import org.openide.util.WeakListeners;

public final class ClassIndex {
    private static final Logger LOGGER = Logger.getLogger(ClassIndex.class.getName());
    private final ClassPath bootPath;
    private final ClassPath classPath;
    private final ClassPath sourcePath;
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private final Set<URL> oldSources;
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private final Set<URL> oldBoot;
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private final Set<URL> oldCompile;
    private final Set<ClassIndexImpl> sourceIndeces;
    private final Set<ClassIndexImpl> depsIndeces;
    private final Collection<ClassIndexListener> listeners = new ConcurrentLinkedQueue<ClassIndexListener>();
    private final SPIListener spiListener;

    ClassIndex(ClassPath bootPath, ClassPath classPath, ClassPath sourcePath) {
        this(bootPath, classPath, sourcePath, true);
    }

    ClassIndex(@NonNull ClassPath bootPath, @NonNull ClassPath classPath, @NonNull ClassPath sourcePath, boolean supportsChanges) {
        this.spiListener = new SPIListener();
        assert (bootPath != null);
        assert (classPath != null);
        assert (sourcePath != null);
        this.bootPath = bootPath;
        this.classPath = classPath;
        this.sourcePath = sourcePath;
        this.oldBoot = new HashSet<URL>();
        this.oldCompile = new HashSet<URL>();
        this.oldSources = new HashSet<URL>();
        this.depsIndeces = new HashSet<ClassIndexImpl>();
        this.sourceIndeces = new HashSet<ClassIndexImpl>();
        if (supportsChanges) {
            this.bootPath.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.spiListener, (Object)this.bootPath));
            this.classPath.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.spiListener, (Object)this.classPath));
            this.sourcePath.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.spiListener, (Object)this.sourcePath));
        }
        this.reset(true, true);
    }

    public void addClassIndexListener(@NonNull ClassIndexListener listener) {
        assert (listener != null);
        this.listeners.add(listener);
    }

    public void removeClassIndexListener(@NonNull ClassIndexListener listener) {
        assert (listener != null);
        this.listeners.remove(listener);
    }

    @NullUnknown
    public Set<ElementHandle<TypeElement>> getElements(@NonNull ElementHandle<TypeElement> element, @NonNull Set<SearchKind> searchKind, @NonNull Set<? extends SearchScopeType> scope) {
        return this.searchImpl(element, searchKind, scope, new Convertor<ClassIndexImpl, Convertor<Document, ElementHandle<TypeElement>>>(){

            @NonNull
            public Convertor<Document, ElementHandle<TypeElement>> convert(@NonNull ClassIndexImpl p) {
                return DocumentUtil.elementHandleConvertor();
            }
        });
    }

    @NullUnknown
    public Set<ElementHandle<TypeElement>> getElementsForPackage(@NonNull ElementHandle<PackageElement> element, @NonNull Set<SearchKind> searchKind, @NonNull Set<? extends SearchScopeType> scope) {
        return this.searchImpl(element, searchKind, scope, new Convertor<ClassIndexImpl, Convertor<Document, ElementHandle<TypeElement>>>(){

            @NonNull
            public Convertor<Document, ElementHandle<TypeElement>> convert(@NonNull ClassIndexImpl p) {
                return DocumentUtil.elementHandleConvertor();
            }
        });
    }

    @NullUnknown
    public Set<FileObject> getResources(@NonNull ElementHandle<TypeElement> element, @NonNull Set<SearchKind> searchKind, @NonNull Set<? extends SearchScopeType> scope) {
        return this.searchImpl(element, searchKind, scope, new Convertor<ClassIndexImpl, Convertor<Document, FileObject>>(){

            @NonNull
            public Convertor<Document, FileObject> convert(@NonNull ClassIndexImpl p) {
                return DocumentUtil.fileObjectConvertor(p.getSourceRoots());
            }
        });
    }

    @NullUnknown
    public Set<FileObject> getResourcesForPackage(@NonNull ElementHandle<PackageElement> element, @NonNull Set<SearchKind> searchKind, @NonNull Set<? extends SearchScopeType> scope) {
        return this.searchImpl(element, searchKind, scope, new Convertor<ClassIndexImpl, Convertor<Document, FileObject>>(){

            @NonNull
            public Convertor<Document, FileObject> convert(@NonNull ClassIndexImpl p) {
                return DocumentUtil.fileObjectConvertor(p.getSourceRoots());
            }
        });
    }

    @NullUnknown
    private <T> Set<T> searchImpl(@NonNull ElementHandle<? extends Element> element, @NonNull Set<SearchKind> searchKind, @NonNull Set<? extends SearchScopeType> scope, @NonNull Convertor<? super ClassIndexImpl, Convertor<Document, T>> convertor) {
        Parameters.notNull((CharSequence)"element", element);
        Parameters.notNull((CharSequence)"element.signatue", (Object)element.getSignature()[0]);
        Parameters.notNull((CharSequence)"searchKind", searchKind);
        Parameters.notNull((CharSequence)"scope", scope);
        Parameters.notNull((CharSequence)"convertor", convertor);
        HashSet result = new HashSet();
        Set<ClassIndexImpl.UsageType> ut = ClassIndex.encodeSearchKind(element.getKind(), searchKind);
        if (!ut.isEmpty()) {
            try {
                Iterable<? extends ClassIndexImpl> queries = this.getQueries(scope);
                for (ClassIndexImpl query : queries) {
                    try {
                        query.search(element, ut, scope, (Convertor)convertor.convert((Object)query), result);
                    }
                    catch (Index.IndexClosedException e) {
                        ClassIndex.logClosedIndex(query);
                    }
                    catch (IOException e) {
                        Exceptions.printStackTrace((Throwable)e);
                    }
                }
            }
            catch (InterruptedException e) {
                return null;
            }
        }
        return Collections.unmodifiableSet(result);
    }

    @NullUnknown
    public Set<ElementHandle<TypeElement>> getDeclaredTypes(@NonNull String name, @NonNull NameKind kind, @NonNull Set<? extends SearchScopeType> scope) {
        assert (name != null);
        assert (kind != null);
        HashSet result = new HashSet();
        Iterable<? extends ClassIndexImpl> queries = this.getQueries(scope);
        Convertor<Document, ElementHandle<TypeElement>> thConvertor = DocumentUtil.elementHandleConvertor();
        try {
            for (ClassIndexImpl query : queries) {
                try {
                    query.getDeclaredTypes(name, kind, scope, thConvertor, result);
                }
                catch (Index.IndexClosedException e) {
                    ClassIndex.logClosedIndex(query);
                }
                catch (IOException e) {
                    Exceptions.printStackTrace((Throwable)e);
                }
            }
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.log(Level.FINE, "ClassIndex.getDeclaredTypes returned {0} elements\n", result.size());
            }
            return Collections.unmodifiableSet(result);
        }
        catch (InterruptedException e) {
            return null;
        }
    }

    @NullUnknown
    public Iterable<Symbols> getDeclaredSymbols(@NonNull String name, @NonNull NameKind kind, @NonNull Set<? extends SearchScopeType> scope) {
        Parameters.notNull((CharSequence)"name", (Object)name);
        Parameters.notNull((CharSequence)"kind", (Object)((Object)kind));
        HashMap result = new HashMap();
        Iterable<? extends ClassIndexImpl> queries = this.getQueries(scope);
        Convertor<Document, ElementHandle<TypeElement>> thConvertor = DocumentUtil.elementHandleConvertor();
        try {
            for (ClassIndexImpl query : queries) {
                try {
                    query.getDeclaredElements(name, kind, thConvertor, result);
                }
                catch (Index.IndexClosedException e) {
                    ClassIndex.logClosedIndex(query);
                }
                catch (IOException e) {
                    Exceptions.printStackTrace((Throwable)e);
                }
            }
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.log(Level.FINE, "ClassIndex.getDeclaredTypes returned {0} elements\n", result.size());
            }
            ArrayList<Symbols> finalResult = new ArrayList<Symbols>();
            for (Map.Entry e : result.entrySet()) {
                finalResult.add(new Symbols((ElementHandle)e.getKey(), (Set)e.getValue()));
            }
            return Collections.unmodifiableList(finalResult);
        }
        catch (InterruptedException e) {
            return null;
        }
    }

    @NullUnknown
    public Set<String> getPackageNames(@NonNull String prefix, boolean directOnly, @NonNull Set<? extends SearchScopeType> scope) {
        assert (prefix != null);
        HashSet<String> result = new HashSet<String>();
        Iterable<? extends ClassIndexImpl> queries = this.getQueries(scope);
        try {
            for (ClassIndexImpl query : queries) {
                try {
                    query.getPackageNames(prefix, directOnly, result);
                }
                catch (Index.IndexClosedException e) {
                    ClassIndex.logClosedIndex(query);
                }
                catch (IOException e) {
                    Exceptions.printStackTrace((Throwable)e);
                }
            }
            return Collections.unmodifiableSet(result);
        }
        catch (InterruptedException e) {
            return null;
        }
    }

    @NonNull
    public static /* varargs */ SearchScopeType createPackageSearchScope(final @NonNull SearchScopeType base, @NonNull String ... pkgs) {
        Parameters.notNull((CharSequence)"base", (Object)base);
        Parameters.notNull((CharSequence)"pkgs", (Object)pkgs);
        HashSet<String> pkgSet = new HashSet<String>(Arrays.asList(pkgs));
        Set<? extends String> basePkgs = base.getPackages();
        if (basePkgs != null) {
            pkgSet.addAll(basePkgs);
        }
        final Set<String> newPkgs = Collections.unmodifiableSet(pkgSet);
        return new SearchScopeType(){

            @Override
            public Set<? extends String> getPackages() {
                return newPkgs;
            }

            @Override
            public boolean isSources() {
                return base.isSources();
            }

            @Override
            public boolean isDependencies() {
                return base.isDependencies();
            }
        };
    }

    private static void logClosedIndex(ClassIndexImpl query) {
        assert (query != null);
        LOGGER.info("Ignoring closed index: " + query.toString());
    }

    private void reset(final boolean source, final boolean deps) {
        ProjectManager.mutex().readAccess(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                ClassIndex classIndex = ClassIndex.this;
                synchronized (classIndex) {
                    if (source) {
                        for (ClassIndexImpl impl : ClassIndex.this.sourceIndeces) {
                            impl.removeClassIndexImplListener(ClassIndex.this.spiListener);
                        }
                        ClassIndex.this.sourceIndeces.clear();
                        ClassIndex.this.oldSources.clear();
                        ClassIndex.this.createQueriesForRoots(ClassIndex.this.sourcePath, true, ClassIndex.this.sourceIndeces, ClassIndex.this.oldSources);
                    }
                    if (deps) {
                        for (ClassIndexImpl impl : ClassIndex.this.depsIndeces) {
                            impl.removeClassIndexImplListener(ClassIndex.this.spiListener);
                        }
                        ClassIndex.this.depsIndeces.clear();
                        ClassIndex.this.oldBoot.clear();
                        ClassIndex.this.oldCompile.clear();
                        ClassIndex.this.createQueriesForRoots(ClassIndex.this.bootPath, false, ClassIndex.this.depsIndeces, ClassIndex.this.oldBoot);
                        ClassIndex.this.createQueriesForRoots(ClassIndex.this.classPath, false, ClassIndex.this.depsIndeces, ClassIndex.this.oldCompile);
                    }
                }
            }
        });
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Iterable<? extends ClassIndexImpl> getQueries(Set<? extends SearchScopeType> scope) {
        HashSet<ClassIndexImpl> result = new HashSet<ClassIndexImpl>();
        ClassIndex classIndex = this;
        synchronized (classIndex) {
            for (SearchScopeType s : scope) {
                if (s.isSources()) {
                    result.addAll(this.sourceIndeces);
                }
                if (!s.isDependencies()) continue;
                result.addAll(this.depsIndeces);
            }
        }
        LOGGER.log(Level.FINE, "ClassIndex.queries[Scope={0}, sourcePath={1}, bootPath={2}, classPath={3}] => {4}\n", new Object[]{scope, this.sourcePath, this.bootPath, this.classPath, result});
        return result;
    }

    private void createQueriesForRoots(ClassPath cp, boolean sources, Set<? super ClassIndexImpl> queries, Set<? super URL> oldState) {
        PathRegistry preg = PathRegistry.getDefault();
        List entries = cp.entries();
        for (ClassPath.Entry entry : entries) {
            URL[] srcRoots;
            if (!sources) {
                srcRoots = preg.sourceForBinaryQuery(entry.getURL(), cp, true);
                if (srcRoots == null) {
                    srcRoots = new URL[]{entry.getURL()};
                }
            } else {
                srcRoots = new URL[]{entry.getURL()};
            }
            for (URL srcRoot : srcRoots) {
                oldState.add(srcRoot);
                ClassIndexImpl ci = ClassIndexManager.getDefault().getUsagesQuery(srcRoot, true);
                if (ci == null) continue;
                ci.addClassIndexImplListener(this.spiListener);
                queries.add(ci);
            }
        }
    }

    private static Set<ClassIndexImpl.UsageType> encodeSearchKind(ElementKind elementKind, Set<SearchKind> kind) {
        assert (kind != null);
        EnumSet<ClassIndexImpl.UsageType> result = EnumSet.noneOf(ClassIndexImpl.UsageType.class);
        block12 : for (SearchKind sk : kind) {
            switch (sk) {
                case METHOD_REFERENCES: {
                    result.add(ClassIndexImpl.UsageType.METHOD_REFERENCE);
                    continue block12;
                }
                case FIELD_REFERENCES: {
                    result.add(ClassIndexImpl.UsageType.FIELD_REFERENCE);
                    continue block12;
                }
                case TYPE_REFERENCES: {
                    result.add(ClassIndexImpl.UsageType.TYPE_REFERENCE);
                    continue block12;
                }
                case IMPLEMENTORS: {
                    switch (elementKind) {
                        case INTERFACE: 
                        case ANNOTATION_TYPE: {
                            result.add(ClassIndexImpl.UsageType.SUPER_INTERFACE);
                            continue block12;
                        }
                        case CLASS: {
                            result.add(ClassIndexImpl.UsageType.SUPER_CLASS);
                            continue block12;
                        }
                        case ENUM: {
                            continue block12;
                        }
                        case OTHER: 
                        case PACKAGE: {
                            result.add(ClassIndexImpl.UsageType.SUPER_INTERFACE);
                            result.add(ClassIndexImpl.UsageType.SUPER_CLASS);
                            continue block12;
                        }
                    }
                    throw new IllegalArgumentException();
                }
            }
            throw new IllegalArgumentException();
        }
        return result;
    }

    private void fireByWorker(Runnable action) {
        assert (action != null);
        if (Utilities.isTaskProcessorThread()) {
            action.run();
        } else {
            Utilities.scheduleSpecialTask((Runnable)action, (int)0);
        }
    }

    private static void assertParserEventThread() {
        assert (Utilities.isTaskProcessorThread());
    }

    static {
        ClassIndexImpl.FACTORY = new ClassIndexFactoryImpl();
    }

    private class SPIListener
    implements ClassIndexImplListener,
    ClassIndexManagerListener,
    PropertyChangeListener {
        private final AtomicBoolean attached;

        private SPIListener() {
            this.attached = new AtomicBoolean();
        }

        @Override
        public void typesAdded(final ClassIndexImplEvent event) {
            assert (event != null);
            Runnable action = new Runnable(){

                @Override
                public void run() {
                    ClassIndex.assertParserEventThread();
                    TypesEvent _event = new TypesEvent(ClassIndex.this, event.getTypes());
                    for (ClassIndexListener l : ClassIndex.this.listeners) {
                        l.typesAdded(_event);
                    }
                }
            };
            ClassIndex.this.fireByWorker(action);
        }

        @Override
        public void typesRemoved(final ClassIndexImplEvent event) {
            assert (event != null);
            Runnable action = new Runnable(){

                @Override
                public void run() {
                    ClassIndex.assertParserEventThread();
                    TypesEvent _event = new TypesEvent(ClassIndex.this, event.getTypes());
                    for (ClassIndexListener l : ClassIndex.this.listeners) {
                        l.typesRemoved(_event);
                    }
                }
            };
            ClassIndex.this.fireByWorker(action);
        }

        @Override
        public void typesChanged(final ClassIndexImplEvent event) {
            assert (event != null);
            Runnable action = new Runnable(){

                @Override
                public void run() {
                    ClassIndex.assertParserEventThread();
                    TypesEvent _event = new TypesEvent(ClassIndex.this, event.getTypes());
                    for (ClassIndexListener l : ClassIndex.this.listeners) {
                        l.typesChanged(_event);
                    }
                }
            };
            ClassIndex.this.fireByWorker(action);
        }

        @Override
        public void classIndexAdded(ClassIndexManagerEvent event) {
            assert (event != null);
            Set<? extends URL> roots = event.getRoots();
            assert (roots != null);
            final LinkedList ar = new LinkedList();
            boolean srcF = this.containsRoot(ClassIndex.this.sourcePath, roots, ar, false);
            boolean depF = this.containsRoot(ClassIndex.this.bootPath, roots, ar, true);
            if (srcF || (depF |= this.containsRoot(ClassIndex.this.classPath, roots, ar, true))) {
                ClassIndex.this.reset(srcF, depF);
                Runnable action = new Runnable(){

                    @Override
                    public void run() {
                        ClassIndex.assertParserEventThread();
                        RootsEvent e = new RootsEvent(ClassIndex.this, ar);
                        for (ClassIndexListener l : ClassIndex.this.listeners) {
                            l.rootsAdded(e);
                        }
                    }
                };
                ClassIndex.this.fireByWorker(action);
            }
        }

        @Override
        public void classIndexRemoved(ClassIndexManagerEvent event) {
        }

        private void attachClassIndexManagerListener() {
            if (!this.attached.getAndSet(true)) {
                ClassIndexManager manager = ClassIndexManager.getDefault();
                manager.addClassIndexManagerListener((ClassIndexManagerListener)WeakListeners.create(ClassIndexManagerListener.class, (EventListener)this, (Object)manager));
            }
        }

        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        private boolean containsRoot(ClassPath cp, Set<? extends URL> roots, List<? super URL> affectedRoots, boolean translate) {
            List entries = cp.entries();
            PathRegistry preg = PathRegistry.getDefault();
            boolean result = false;
            for (ClassPath.Entry entry : entries) {
                URL url = entry.getURL();
                URL[] srcRoots = null;
                if (translate) {
                    srcRoots = preg.sourceForBinaryQuery(entry.getURL(), cp, false);
                }
                if (srcRoots == null) {
                    if (!roots.contains(url)) continue;
                    affectedRoots.add(url);
                    result = true;
                    continue;
                }
                for (URL _url : srcRoots) {
                    if (!roots.contains(_url)) continue;
                    affectedRoots.add(url);
                    result = true;
                }
            }
            return result;
        }

        private boolean containsNewRoot(ClassPath cp, Set<? extends URL> roots, List<? super URL> newRoots, List<? super URL> removedRoots, Set<? super URL> attachListener, boolean translate) throws IOException {
            List entries = cp.entries();
            PathRegistry preg = PathRegistry.getDefault();
            boolean result = false;
            for (ClassPath.Entry entry : entries) {
                URL url = entry.getURL();
                URL[] srcRoots = null;
                if (translate) {
                    srcRoots = preg.sourceForBinaryQuery(entry.getURL(), cp, false);
                }
                if (srcRoots == null) {
                    if (roots.remove(url)) continue;
                    if (JavaIndex.isIndexed(url)) {
                        newRoots.add(url);
                        result = true;
                        continue;
                    }
                    attachListener.add(url);
                    continue;
                }
                for (URL _url : srcRoots) {
                    if (roots.remove(_url)) continue;
                    if (JavaIndex.isIndexed(_url)) {
                        newRoots.add(_url);
                        result = true;
                        continue;
                    }
                    attachListener.add(_url);
                }
            }
            List<? super URL> c = removedRoots;
            c.addAll(roots);
            return result |= !roots.isEmpty();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("entries".equals(evt.getPropertyName())) {
                LinkedList newRoots = new LinkedList();
                LinkedList removedRoots = new LinkedList();
                boolean dirtySource = false;
                boolean dirtyDeps = false;
                try {
                    ClassIndex classIndex;
                    HashSet copy;
                    Object source = evt.getSource();
                    HashSet unknownRoots = new HashSet();
                    if (source == ClassIndex.this.sourcePath) {
                        classIndex = ClassIndex.this;
                        synchronized (classIndex) {
                            copy = new HashSet(ClassIndex.this.oldSources);
                        }
                        dirtySource = this.containsNewRoot(ClassIndex.this.sourcePath, copy, newRoots, removedRoots, unknownRoots, false);
                    } else if (source == ClassIndex.this.classPath) {
                        classIndex = ClassIndex.this;
                        synchronized (classIndex) {
                            copy = new HashSet(ClassIndex.this.oldCompile);
                        }
                        dirtyDeps = this.containsNewRoot(ClassIndex.this.classPath, copy, newRoots, removedRoots, unknownRoots, true);
                    } else if (source == ClassIndex.this.bootPath) {
                        classIndex = ClassIndex.this;
                        synchronized (classIndex) {
                            copy = new HashSet(ClassIndex.this.oldBoot);
                        }
                        dirtyDeps = this.containsNewRoot(ClassIndex.this.bootPath, copy, newRoots, removedRoots, unknownRoots, true);
                    }
                    if (!unknownRoots.isEmpty()) {
                        this.attachClassIndexManagerListener();
                        ClassIndexManager mgr = ClassIndexManager.getDefault();
                        Iterator it = unknownRoots.iterator();
                        while (it.hasNext()) {
                            URL url = (URL)it.next();
                            if (JavaIndex.isIndexed(url)) continue;
                            it.remove();
                        }
                        if (!unknownRoots.isEmpty()) {
                            this.classIndexAdded(new ClassIndexManagerEvent(mgr, unknownRoots));
                        }
                    }
                    if (dirtySource || dirtyDeps) {
                        ClassIndex.this.reset(dirtySource, dirtyDeps);
                        final RootsEvent ae = newRoots.isEmpty() ? null : new RootsEvent(ClassIndex.this, newRoots);
                        final RootsEvent re = removedRoots.isEmpty() ? null : new RootsEvent(ClassIndex.this, removedRoots);
                        ClassIndex.this.fireByWorker(new Runnable(){

                            @Override
                            public void run() {
                                ClassIndex.assertParserEventThread();
                                if (ae != null) {
                                    for (ClassIndexListener l : ClassIndex.this.listeners) {
                                        l.rootsAdded(ae);
                                    }
                                }
                                if (re != null) {
                                    for (ClassIndexListener l : ClassIndex.this.listeners) {
                                        l.rootsRemoved(re);
                                    }
                                }
                            }
                        });
                    }
                }
                catch (IOException ioe) {
                    Exceptions.printStackTrace((Throwable)ioe);
                }
            }
        }

    }

    private static class ClassIndexFactoryImpl
    implements ClassIndexFactory {
        private ClassIndexFactoryImpl() {
        }

        @Override
        public ClassIndex create(ClassPath bootPath, ClassPath classPath, ClassPath sourcePath) {
            return new ClassIndex(bootPath, classPath, sourcePath);
        }
    }

    public static final class Symbols {
        private final ElementHandle<TypeElement> enclosingType;
        private final Set<String> symbols;

        Symbols(ElementHandle<TypeElement> enclosingType, Set<String> symbols) {
            this.enclosingType = enclosingType;
            this.symbols = symbols;
        }

        public ElementHandle<TypeElement> getEnclosingType() {
            return this.enclosingType;
        }

        public Set<String> getSymbols() {
            return this.symbols;
        }
    }

    public static interface SearchScopeType {
        @CheckForNull
        public Set<? extends String> getPackages();

        public boolean isSources();

        public boolean isDependencies();
    }

    public static enum SearchScope implements SearchScopeType
    {
        SOURCE{

            @Override
            public boolean isSources() {
                return true;
            }

            @Override
            public boolean isDependencies() {
                return false;
            }
        }
        ,
        DEPENDENCIES{

            @Override
            public boolean isSources() {
                return false;
            }

            @Override
            public boolean isDependencies() {
                return true;
            }
        };
        

        private SearchScope() {
        }

        @CheckForNull
        @Override
        public Set<? extends String> getPackages() {
            return null;
        }

    }

    public static enum SearchKind {
        IMPLEMENTORS,
        METHOD_REFERENCES,
        FIELD_REFERENCES,
        TYPE_REFERENCES;
        

        private SearchKind() {
        }
    }

    public static enum NameKind {
        SIMPLE_NAME,
        PREFIX,
        CASE_INSENSITIVE_PREFIX,
        CAMEL_CASE,
        REGEXP,
        CASE_INSENSITIVE_REGEXP,
        CAMEL_CASE_INSENSITIVE;
        

        private NameKind() {
        }
    }

}

