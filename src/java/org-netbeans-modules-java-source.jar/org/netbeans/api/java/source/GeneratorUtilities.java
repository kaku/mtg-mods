/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.AnnotationTree
 *  com.sun.source.tree.AssignmentTree
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ErroneousTree
 *  com.sun.source.tree.ExpressionStatementTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.LiteralTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodInvocationTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.NewArrayTree
 *  com.sun.source.tree.PrimitiveTypeTree
 *  com.sun.source.tree.ReturnTree
 *  com.sun.source.tree.Scope
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.TypeParameterTree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.tree.WildcardTree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreePathScanner
 *  com.sun.source.util.TreeScanner
 *  com.sun.source.util.Trees
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.code.Scope
 *  com.sun.tools.javac.code.Scope$Entry
 *  com.sun.tools.javac.code.Scope$StarImportScope
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.jvm.ClassReader
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCCompilationUnit
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.List
 *  com.sun.tools.javac.util.Name
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.api.queries.FileEncodingQuery
 *  org.netbeans.editor.GuardedDocument
 *  org.netbeans.editor.MarkBlockChain
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package org.netbeans.api.java.source;

import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ErroneousTree;
import com.sun.source.tree.ExpressionStatementTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.NewArrayTree;
import com.sun.source.tree.PrimitiveTypeTree;
import com.sun.source.tree.ReturnTree;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TypeParameterTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.tree.WildcardTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreePathScanner;
import com.sun.source.util.TreeScanner;
import com.sun.source.util.Trees;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.code.Scope;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.jvm.ClassReader;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.QualifiedNameable;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ExecutableType;
import javax.lang.model.type.NoType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.type.WildcardType;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.text.Document;
import javax.swing.text.StyledDocument;
import javax.tools.JavaFileObject;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.source.AssignComments;
import org.netbeans.api.java.source.CodeStyle;
import org.netbeans.api.java.source.CodeStyleUtils;
import org.netbeans.api.java.source.Comment;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.java.source.TranslateIdentifier;
import org.netbeans.api.java.source.TreeMaker;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.api.queries.FileEncodingQuery;
import org.netbeans.editor.GuardedDocument;
import org.netbeans.editor.MarkBlockChain;
import org.netbeans.modules.java.source.builder.CommentHandlerService;
import org.netbeans.modules.java.source.builder.CommentSetImpl;
import org.netbeans.modules.java.source.parsing.AbstractSourceFileObject;
import org.netbeans.modules.java.source.parsing.CompilationInfoImpl;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.query.CommentSet;
import org.netbeans.modules.java.source.save.DiffContext;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public final class GeneratorUtilities {
    private WorkingCopy copy;
    private static final String GENERATED_METHOD_BODY = "Templates/Classes/Code/GeneratedMethodBody";
    private static final String OVERRIDDEN_METHOD_BODY = "Templates/Classes/Code/OverriddenMethodBody";
    private static final String METHOD_RETURN_TYPE = "method_return_type";
    private static final String DEFAULT_RETURN_TYPE_VALUE = "default_return_value";
    private static final String SUPER_METHOD_CALL = "super_method_call";
    private static final String METHOD_NAME = "method_name";
    private static final String CLASS_NAME = "class_name";
    private static final String SIMPLE_CLASS_NAME = "simple_class_name";
    private static final String SCRIPT_ENGINE_ATTR = "javax.script.ScriptEngine";
    private static final String STRING_OUTPUT_MODE_ATTR = "com.sun.script.freemarker.stringOut";
    private static ScriptEngineManager manager;

    private GeneratorUtilities(WorkingCopy copy) {
        this.copy = copy;
    }

    public static GeneratorUtilities get(WorkingCopy copy) {
        return new GeneratorUtilities(copy);
    }

    public CompilationUnitTree createFromTemplate(FileObject sourceRoot, String path, ElementKind kind) throws IOException {
        String[] nameComponent = FileObjects.getFolderAndBaseName(path, '/');
        JavaFileObject sourceFile = FileObjects.templateFileObject(sourceRoot, nameComponent[0], nameComponent[1]);
        FileObject template = FileUtil.getConfigFile((String)this.copy.template(kind));
        FileObject targetFile = this.copy.doCreateFromTemplate(template, sourceFile);
        CompilationUnitTree templateCUT = (CompilationUnitTree)this.copy.impl.getJavacTask().parse(new JavaFileObject[]{FileObjects.sourceFileObject(targetFile, targetFile.getParent())}).iterator().next();
        CompilationUnitTree importComments = GeneratorUtilities.get(this.copy).importComments((T)templateCUT, templateCUT);
        CompilationUnitTree result = this.copy.getTreeMaker().CompilationUnit(importComments.getPackageAnnotations(), sourceRoot, path, importComments.getImports(), importComments.getTypeDecls());
        return result;
    }

    public ClassTree insertClassMember(ClassTree clazz, Tree member) {
        assert (clazz != null && member != null);
        Document doc = null;
        try {
            doc = this.copy.getDocument();
            if (doc == null) {
                DataObject data = DataObject.find((FileObject)this.copy.getFileObject());
                EditorCookie cookie = (EditorCookie)data.getCookie(EditorCookie.class);
                doc = cookie.openDocument();
            }
        }
        catch (IOException ioe) {
            // empty catch block
        }
        CodeStyle codeStyle = DiffContext.getCodeStyle(this.copy);
        ClassMemberComparator comparator = new ClassMemberComparator(codeStyle);
        SourcePositions sp = this.copy.getTrees().getSourcePositions();
        TreeUtilities utils = this.copy.getTreeUtilities();
        CompilationUnitTree compilationUnit = this.copy.getCompilationUnit();
        Tree lastMember = null;
        int idx = -1;
        int gsidx = -1;
        String[] gsnames = codeStyle.keepGettersAndSettersTogether() ? GeneratorUtilities.correspondingGSNames(member) : null;
        int i = 0;
        for (Tree tree : clazz.getMembers()) {
            if (!utils.isSynthetic(compilationUnit, tree)) {
                if (gsnames != null && gsidx < 0) {
                    for (String name : gsnames) {
                        if (!name.equals(GeneratorUtilities.name(tree))) continue;
                        if (GeneratorUtilities.isSetter(tree)) {
                            gsidx = codeStyle.sortMembersInGroupsAlphabetically() ? i : i + 1;
                            continue;
                        }
                        if (!GeneratorUtilities.isGetter(tree) && !GeneratorUtilities.isBooleanGetter(tree)) continue;
                        gsidx = i + 1;
                    }
                }
                if (idx < 0 && (codeStyle.getClassMemberInsertionPoint() == CodeStyle.InsertionPoint.FIRST_IN_CATEGORY && comparator.compare(member, tree) <= 0 || comparator.compare(member, tree) < 0)) {
                    if (doc == null || !(doc instanceof GuardedDocument)) {
                        idx = i;
                        continue;
                    }
                    int pos = (int)(lastMember != null ? sp.getEndPosition(compilationUnit, lastMember) : sp.getStartPosition(compilationUnit, (Tree)clazz));
                    pos = ((GuardedDocument)doc).getGuardedBlockChain().adjustToBlockEnd(pos);
                    long treePos = sp.getStartPosition(compilationUnit, tree);
                    if (treePos < 0 || (long)pos <= treePos) {
                        idx = i;
                    }
                }
            }
            ++i;
            lastMember = tree;
        }
        if (idx < 0) {
            idx = i;
        }
        return this.copy.getTreeMaker().insertClassMember(clazz, gsidx < 0 ? idx : gsidx, member);
    }

    public ClassTree insertClassMembers(ClassTree clazz, Iterable<? extends Tree> members) {
        assert (members != null);
        for (Tree member : members) {
            clazz = this.insertClassMember(clazz, member);
        }
        return clazz;
    }

    public java.util.List<? extends MethodTree> createAllAbstractMethodImplementations(TypeElement clazz) {
        return this.createAbstractMethodImplementations(clazz, this.copy.getElementUtilities().findUnimplementedMethods(clazz));
    }

    public java.util.List<? extends MethodTree> createAbstractMethodImplementations(TypeElement clazz, Iterable<? extends ExecutableElement> methods) {
        assert (methods != null);
        ArrayList<MethodTree> ret = new ArrayList<MethodTree>();
        for (ExecutableElement method : methods) {
            ret.add(this.createAbstractMethodImplementation(clazz, method));
        }
        this.tagFirst(ret);
        return ret;
    }

    public MethodTree createAbstractMethodImplementation(TypeElement clazz, ExecutableElement method) {
        assert (clazz != null && method != null);
        return this.createMethod(method, clazz, true);
    }

    public java.util.List<? extends MethodTree> createOverridingMethods(TypeElement clazz, Iterable<? extends ExecutableElement> methods) {
        assert (methods != null);
        ArrayList<MethodTree> ret = new ArrayList<MethodTree>();
        for (ExecutableElement method : methods) {
            ret.add(this.createOverridingMethod(clazz, method));
        }
        this.tagFirst(ret);
        return ret;
    }

    public MethodTree createOverridingMethod(TypeElement clazz, ExecutableElement method) {
        assert (clazz != null && method != null);
        return this.createMethod(method, clazz, false);
    }

    public MethodTree createMethod(DeclaredType asMemberOf, ExecutableElement method) {
        TreeMaker make = this.copy.getTreeMaker();
        CodeStyle cs = DiffContext.getCodeStyle(this.copy);
        Set<Modifier> mods = method.getModifiers();
        EnumSet<Modifier> flags = mods.isEmpty() ? EnumSet.noneOf(Modifier.class) : EnumSet.copyOf(mods);
        flags.remove((Object)Modifier.ABSTRACT);
        flags.remove((Object)Modifier.NATIVE);
        flags.remove((Object)Modifier.DEFAULT);
        ExecutableType et = (ExecutableType)method.asType();
        try {
            et = (ExecutableType)this.copy.getTypes().asMemberOf(asMemberOf, method);
        }
        catch (IllegalArgumentException iae) {
            // empty catch block
        }
        ArrayList<TypeParameterTree> typeParams = new ArrayList<TypeParameterTree>();
        for (TypeVariable typeVariable : et.getTypeVariables()) {
            ArrayList<ExpressionTree> bounds = new ArrayList<ExpressionTree>();
            TypeMirror bound = typeVariable.getUpperBound();
            if (bound.getKind() != TypeKind.NULL) {
                if (bound.getKind() == TypeKind.DECLARED) {
                    Symbol.ClassSymbol boundSymbol = (Symbol.ClassSymbol)((DeclaredType)bound).asElement();
                    if (boundSymbol.getSimpleName().length() == 0 && (boundSymbol.flags() & 0x1000000) != 0) {
                        bounds.add((ExpressionTree)make.Type((TypeMirror)boundSymbol.getSuperclass()));
                        for (Type iface : boundSymbol.getInterfaces()) {
                            bounds.add((ExpressionTree)make.Type((TypeMirror)iface));
                        }
                    } else if (!boundSymbol.getQualifiedName().contentEquals((CharSequence)"java.lang.Object")) {
                        bounds.add((ExpressionTree)make.Type(bound));
                    }
                } else {
                    bounds.add((ExpressionTree)make.Type(bound));
                }
            }
            typeParams.add(make.TypeParameter(typeVariable.asElement().getSimpleName(), bounds));
        }
        Tree returnType = make.Type(et.getReturnType());
        ArrayList<VariableTree> params = new ArrayList<VariableTree>();
        boolean isVarArgs = method.isVarArgs();
        Iterator<? extends VariableElement> formArgNames = method.getParameters().iterator();
        Iterator<? extends TypeMirror> formArgTypes = et.getParameterTypes().iterator();
        ModifiersTree parameterModifiers = make.Modifiers(EnumSet.noneOf(Modifier.class));
        while (formArgNames.hasNext() && formArgTypes.hasNext()) {
            VariableElement formArgName = formArgNames.next();
            TypeMirror formArgType = formArgTypes.next();
            if (isVarArgs && !formArgNames.hasNext()) {
                parameterModifiers = make.Modifiers(0x400000000L, Collections.emptyList());
            }
            String paramName = GeneratorUtilities.addParamPrefixSuffix(GeneratorUtilities.removeParamPrefixSuffix(formArgName, cs), cs);
            params.add(make.Variable(parameterModifiers, paramName, this.resolveWildcard(formArgType), null));
        }
        ArrayList<ExpressionTree> throwsList = new ArrayList<ExpressionTree>();
        for (TypeMirror tm : et.getThrownTypes()) {
            throwsList.add((ExpressionTree)make.Type(tm));
        }
        ModifiersTree mt = make.Modifiers(flags, Collections.emptyList());
        return make.Method(mt, (CharSequence)method.getSimpleName(), returnType, typeParams, params, throwsList, "{}", null);
    }

    public MethodTree createConstructor(TypeElement clazz, Iterable<? extends VariableElement> fields, ExecutableElement constructor) {
        return this.createConstructor(clazz, fields, constructor, false);
    }

    public MethodTree createDefaultConstructor(TypeElement clazz, Iterable<? extends VariableElement> fields, ExecutableElement constructor) {
        return this.createConstructor(clazz, fields, constructor, true);
    }

    private MethodTree createConstructor(TypeElement clazz, Iterable<? extends VariableElement> fields, ExecutableElement constructor, boolean isDefault) {
        assert (clazz != null && fields != null);
        TreeMaker make = this.copy.getTreeMaker();
        CodeStyle cs = DiffContext.getCodeStyle(this.copy);
        EnumSet<Modifier> mods = EnumSet.of(clazz.getKind() == ElementKind.ENUM ? Modifier.PRIVATE : Modifier.PUBLIC);
        ArrayList<VariableTree> parameters = new ArrayList<VariableTree>();
        LinkedList<ExpressionStatementTree> statements = new LinkedList<ExpressionStatementTree>();
        ModifiersTree parameterModifiers = make.Modifiers(EnumSet.noneOf(Modifier.class));
        LinkedList<ExpressionTree> throwsList = new LinkedList<ExpressionTree>();
        LinkedList<TypeParameterTree> typeParams = new LinkedList<TypeParameterTree>();
        for (VariableElement ve : fields) {
            TypeMirror type = this.copy.getTypes().asMemberOf((DeclaredType)clazz.asType(), ve);
            if (isDefault) {
                statements.add(make.ExpressionStatement((ExpressionTree)make.Assignment((ExpressionTree)make.MemberSelect((ExpressionTree)make.Identifier("this"), ve.getSimpleName()), (ExpressionTree)make.Literal(GeneratorUtilities.defaultValue(type)))));
                continue;
            }
            String paramName = GeneratorUtilities.addParamPrefixSuffix(GeneratorUtilities.removeFieldPrefixSuffix(ve, cs), cs);
            parameters.add(make.Variable(parameterModifiers, paramName, make.Type(type), null));
            statements.add(make.ExpressionStatement((ExpressionTree)make.Assignment((ExpressionTree)make.MemberSelect((ExpressionTree)make.Identifier("this"), ve.getSimpleName()), (ExpressionTree)make.Identifier(paramName))));
        }
        if (constructor != null) {
            ExecutableType constructorType;
            ExecutableType executableType = constructorType = clazz.getSuperclass().getKind() == TypeKind.DECLARED && ((DeclaredType)clazz.getSuperclass()).asElement() == constructor.getEnclosingElement() ? (ExecutableType)this.copy.getTypes().asMemberOf((DeclaredType)clazz.getSuperclass(), constructor) : null;
            if (!constructor.getParameters().isEmpty()) {
                Iterator<? extends TypeMirror> parameterTypes;
                ArrayList<Object> arguments = new ArrayList<Object>();
                Iterator<? extends VariableElement> parameterElements = constructor.getParameters().iterator();
                Iterator<? extends TypeMirror> iterator = parameterTypes = constructorType != null ? constructorType.getParameterTypes().iterator() : null;
                while (parameterElements.hasNext()) {
                    TypeMirror type;
                    VariableElement ve2 = parameterElements.next();
                    TypeMirror typeMirror = type = parameterTypes != null ? parameterTypes.next() : ve2.asType();
                    if (isDefault) {
                        arguments.add((Object)make.Literal(GeneratorUtilities.defaultValue(type)));
                        continue;
                    }
                    String paramName = GeneratorUtilities.addParamPrefixSuffix(GeneratorUtilities.removeParamPrefixSuffix(ve2, cs), cs);
                    parameters.add(make.Variable(parameterModifiers, paramName, make.Type(type), null));
                    arguments.add((Object)make.Identifier(paramName));
                }
                statements.addFirst(make.ExpressionStatement((ExpressionTree)make.MethodInvocation(Collections.emptyList(), (ExpressionTree)make.Identifier("super"), arguments)));
            }
            constructorType = constructorType != null ? constructorType : (ExecutableType)constructor.asType();
            for (TypeMirror th : constructorType.getThrownTypes()) {
                throwsList.add((ExpressionTree)make.Type(th));
            }
            for (TypeParameterElement typeParameterElement : constructor.getTypeParameters()) {
                LinkedList<ExpressionTree> boundsList = new LinkedList<ExpressionTree>();
                for (TypeMirror bound : typeParameterElement.getBounds()) {
                    boundsList.add((ExpressionTree)make.Type(bound));
                }
                typeParams.add(make.TypeParameter(typeParameterElement.getSimpleName(), boundsList));
            }
        }
        BlockTree body = make.Block(statements, false);
        return make.Method(make.Modifiers(mods), "<init>", null, typeParams, parameters, throwsList, body, null, constructor != null ? constructor.isVarArgs() : false);
    }

    public MethodTree createConstructor(ClassTree clazz, Iterable<? extends VariableTree> fields) {
        assert (clazz != null && fields != null);
        TreeMaker make = this.copy.getTreeMaker();
        CodeStyle cs = DiffContext.getCodeStyle(this.copy);
        EnumSet<Modifier> mods = EnumSet.of(this.copy.getTreeUtilities().isEnum(clazz) ? Modifier.PRIVATE : Modifier.PUBLIC);
        ArrayList<VariableTree> parameters = new ArrayList<VariableTree>();
        ArrayList<ExpressionStatementTree> statements = new ArrayList<ExpressionStatementTree>();
        ModifiersTree parameterModifiers = make.Modifiers(EnumSet.noneOf(Modifier.class));
        for (VariableTree vt : fields) {
            String paramName = GeneratorUtilities.addParamPrefixSuffix(GeneratorUtilities.removeFieldPrefixSuffix(vt, cs), cs);
            parameters.add(make.Variable(parameterModifiers, paramName, vt.getType(), null));
            statements.add(make.ExpressionStatement((ExpressionTree)make.Assignment((ExpressionTree)make.MemberSelect((ExpressionTree)make.Identifier("this"), vt.getName()), (ExpressionTree)make.Identifier(paramName))));
        }
        BlockTree body = make.Block(statements, false);
        return make.Method(make.Modifiers(mods), (CharSequence)"<init>", null, Collections.emptyList(), parameters, Collections.emptyList(), body, null);
    }

    public MethodTree createGetter(TypeElement clazz, VariableElement field) {
        TypeMirror type;
        assert (clazz != null && field != null);
        TreeMaker make = this.copy.getTreeMaker();
        CodeStyle cs = DiffContext.getCodeStyle(this.copy);
        EnumSet<Modifier> mods = EnumSet.of(Modifier.PUBLIC);
        boolean isStatic = field.getModifiers().contains((Object)Modifier.STATIC);
        if (isStatic) {
            mods.add(Modifier.STATIC);
        }
        boolean isBoolean = (type = this.copy.getTypes().asMemberOf((DeclaredType)clazz.asType(), field)).getKind() == TypeKind.BOOLEAN;
        String getterName = CodeStyleUtils.computeGetterName(field.getSimpleName(), isBoolean, isStatic, cs);
        BlockTree body = make.Block(Collections.singletonList(make.Return((ExpressionTree)make.Identifier(field.getSimpleName()))), false);
        return make.Method(make.Modifiers(mods), (CharSequence)getterName, make.Type(type), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), body, null);
    }

    public MethodTree createGetter(VariableTree field) {
        Tree type;
        assert (field != null);
        TreeMaker make = this.copy.getTreeMaker();
        CodeStyle cs = DiffContext.getCodeStyle(this.copy);
        EnumSet<Modifier> mods = EnumSet.of(Modifier.PUBLIC);
        boolean isStatic = field.getModifiers().getFlags().contains((Object)Modifier.STATIC);
        if (isStatic) {
            mods.add(Modifier.STATIC);
        }
        boolean isBoolean = (type = field.getType()).getKind() == Tree.Kind.PRIMITIVE_TYPE && ((PrimitiveTypeTree)type).getPrimitiveTypeKind() == TypeKind.BOOLEAN;
        String getterName = CodeStyleUtils.computeGetterName(field.getName(), isBoolean, isStatic, cs);
        BlockTree body = make.Block(Collections.singletonList(make.Return((ExpressionTree)make.Identifier(field.getName()))), false);
        return make.Method(make.Modifiers(mods), (CharSequence)getterName, type, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), body, null);
    }

    public MethodTree createSetter(TypeElement clazz, VariableElement field) {
        assert (clazz != null && field != null);
        TreeMaker make = this.copy.getTreeMaker();
        CodeStyle cs = DiffContext.getCodeStyle(this.copy);
        EnumSet<Modifier> mods = EnumSet.of(Modifier.PUBLIC);
        boolean isStatic = field.getModifiers().contains((Object)Modifier.STATIC);
        if (isStatic) {
            mods.add(Modifier.STATIC);
        }
        javax.lang.model.element.Name name = field.getSimpleName();
        assert (name.length() > 0);
        TypeMirror type = this.copy.getTypes().asMemberOf((DeclaredType)clazz.asType(), field);
        String setterName = CodeStyleUtils.computeSetterName(field.getSimpleName(), isStatic, cs);
        String paramName = GeneratorUtilities.addParamPrefixSuffix(GeneratorUtilities.removeFieldPrefixSuffix(field, cs), cs);
        java.util.List<VariableTree> params = Collections.singletonList(make.Variable(make.Modifiers(EnumSet.noneOf(Modifier.class)), paramName, make.Type(type), null));
        BlockTree body = make.Block(Collections.singletonList(make.ExpressionStatement((ExpressionTree)make.Assignment((ExpressionTree)make.MemberSelect((ExpressionTree)(isStatic ? make.Identifier(field.getEnclosingElement().getSimpleName()) : make.Identifier("this")), name), (ExpressionTree)make.Identifier(paramName)))), false);
        return make.Method(make.Modifiers(mods), (CharSequence)setterName, make.Type(this.copy.getTypes().getNoType(TypeKind.VOID)), Collections.emptyList(), params, Collections.emptyList(), body, null);
    }

    public MethodTree createSetter(ClassTree clazz, VariableTree field) {
        assert (clazz != null && field != null);
        TreeMaker make = this.copy.getTreeMaker();
        EnumSet<Modifier> mods = EnumSet.of(Modifier.PUBLIC);
        boolean isStatic = field.getModifiers().getFlags().contains((Object)Modifier.STATIC);
        if (isStatic) {
            mods.add(Modifier.STATIC);
        }
        javax.lang.model.element.Name name = field.getName();
        assert (name.length() > 0);
        CodeStyle cs = DiffContext.getCodeStyle(this.copy);
        String propName = GeneratorUtilities.removeFieldPrefixSuffix(field, cs);
        String setterName = CodeStyleUtils.computeSetterName(field.getName(), isStatic, cs);
        String paramName = GeneratorUtilities.addParamPrefixSuffix(propName, cs);
        java.util.List<VariableTree> params = Collections.singletonList(make.Variable(make.Modifiers(EnumSet.noneOf(Modifier.class)), paramName, field.getType(), null));
        BlockTree body = make.Block(Collections.singletonList(make.ExpressionStatement((ExpressionTree)make.Assignment((ExpressionTree)make.MemberSelect((ExpressionTree)(isStatic ? make.Identifier(clazz.getSimpleName()) : make.Identifier("this")), name), (ExpressionTree)make.Identifier(paramName)))), false);
        return make.Method(make.Modifiers(mods), (CharSequence)setterName, make.Type(this.copy.getTypes().getNoType(TypeKind.VOID)), Collections.emptyList(), params, Collections.emptyList(), body, null);
    }

    public CompilationUnitTree addImports(CompilationUnitTree cut, Set<? extends Element> toImport) {
        ExpressionTree packageName;
        assert (cut != null && toImport != null && toImport.size() > 0);
        ArrayList<Element> elementsToImport = new ArrayList<Element>(toImport.size());
        HashSet<String> staticImportNames = new HashSet<String>();
        for (Element e : toImport) {
            switch (e.getKind()) {
                case METHOD: 
                case ENUM_CONSTANT: 
                case FIELD: {
                    StringBuilder name = new StringBuilder(((TypeElement)e.getEnclosingElement()).getQualifiedName()).append('.').append(e.getSimpleName());
                    if (!staticImportNames.add(name.toString())) break;
                }
                default: {
                    elementsToImport.add(e);
                }
            }
        }
        Trees trees = this.copy.getTrees();
        Elements elements = this.copy.getElements();
        ElementUtilities elementUtilities = this.copy.getElementUtilities();
        CodeStyle cs = DiffContext.getCodeStyle(this.copy);
        int treshold = cs.useSingleClassImport() ? cs.countForUsingStarImport() : 1;
        int staticTreshold = cs.countForUsingStaticStarImport();
        LinkedHashMap<PackageElement, Integer> pkgCounts = new LinkedHashMap<PackageElement, Integer>();
        PackageElement pkg = elements.getPackageElement("java.lang");
        if (pkg != null) {
            pkgCounts.put(pkg, -2);
        }
        PackageElement packageElement = pkg = (packageName = cut.getPackageName()) != null ? (PackageElement)trees.getElement(TreePath.getPath((CompilationUnitTree)cut, (Tree)packageName)) : null;
        if (pkg == null && packageName != null) {
            pkg = elements.getPackageElement(elements.getName(packageName.toString()));
        }
        if (pkg == null) {
            pkg = elements.getPackageElement(elements.getName(""));
        }
        pkgCounts.put(pkg, -2);
        LinkedHashMap<TypeElement, Integer> typeCounts = new LinkedHashMap<TypeElement, Integer>();
        Scope.StarImportScope importScope = new Scope.StarImportScope((Symbol)pkg);
        if (((JCTree.JCCompilationUnit)cut).starImportScope != null) {
            importScope.importAll((Scope)((JCTree.JCCompilationUnit)cut).starImportScope);
        }
        for (Element e2 : elementsToImport) {
            Integer cnt;
            boolean isStatic = false;
            Element el = null;
            switch (e2.getKind()) {
                case PACKAGE: {
                    el = e2;
                    break;
                }
                case ANNOTATION_TYPE: 
                case CLASS: 
                case ENUM: 
                case INTERFACE: {
                    if (e2.getEnclosingElement().getKind() != ElementKind.PACKAGE) break;
                    el = e2.getEnclosingElement();
                    break;
                }
                case METHOD: 
                case ENUM_CONSTANT: 
                case FIELD: {
                    isStatic = true;
                    el = e2.getEnclosingElement();
                    break;
                }
                default: {
                    assert (false);
                    break;
                }
            }
            if (el == null) continue;
            Integer n = cnt = isStatic ? (Integer)typeCounts.get((TypeElement)el) : (Integer)pkgCounts.get((PackageElement)el);
            if (cnt == null) {
                cnt = 0;
            }
            if (cnt >= 0) {
                if (el == e2) {
                    cnt = -1;
                } else {
                    Integer n2 = cnt;
                    Integer n3 = cnt = Integer.valueOf(cnt + 1);
                    if (isStatic) {
                        if (cnt >= staticTreshold) {
                            cnt = -1;
                        }
                    } else if (cnt >= treshold || GeneratorUtilities.checkPackagesForStarImport(((PackageElement)el).getQualifiedName().toString(), cs)) {
                        cnt = -1;
                    }
                }
            }
            if (isStatic) {
                typeCounts.put((TypeElement)el, cnt);
                continue;
            }
            pkgCounts.put((PackageElement)el, cnt);
        }
        ArrayList<ImportTree> imports = new ArrayList<ImportTree>(cut.getImports());
        for (ImportTree imp : imports) {
            Element el;
            Integer n;
            Integer cnt;
            Element e3 = this.getImportedElement(cut, imp);
            if (elementsToImport.contains(e3)) continue;
            if (imp.isStatic()) {
                if (e3.getKind().isClass() || e3.getKind().isInterface()) {
                    el = e3;
                    while (el != null) {
                        TypeMirror tm;
                        cnt = (Integer)typeCounts.get((TypeElement)el);
                        if (cnt != null) {
                            typeCounts.put((TypeElement)el, -2);
                        }
                        el = (tm = ((TypeElement)el).getSuperclass()).getKind() == TypeKind.DECLARED ? ((DeclaredType)tm).asElement() : null;
                    }
                    continue;
                }
                el = elementUtilities.enclosingTypeElement(e3);
                if (el == null || (cnt = (Integer)typeCounts.get((TypeElement)el)) == null) continue;
                if (cnt >= 0) {
                    Integer tm = cnt;
                    n = cnt = Integer.valueOf(cnt + 1);
                    if (cnt >= staticTreshold) {
                        cnt = -1;
                    }
                }
                typeCounts.put((TypeElement)el, cnt);
                continue;
            }
            el = e3.getKind() == ElementKind.PACKAGE ? e3 : ((e3.getKind().isClass() || e3.getKind().isInterface()) && e3.getEnclosingElement().getKind() == ElementKind.PACKAGE ? e3.getEnclosingElement() : null);
            if (el == null || (cnt = (Integer)pkgCounts.get((PackageElement)el)) == null) continue;
            if (el == e3) {
                cnt = -2;
            } else if (cnt >= 0) {
                Integer tm = cnt;
                n = cnt = Integer.valueOf(cnt + 1);
                if (cnt >= treshold) {
                    cnt = -1;
                }
            }
            pkgCounts.put((PackageElement)el, cnt);
        }
        HashSet<Element> explicitNamedImports = new HashSet<Element>();
        block17 : for (Element element : elementsToImport) {
            if (!element.getKind().isClass() && !element.getKind().isInterface()) continue;
            Scope.Entry e4 = importScope.lookup((Name)element.getSimpleName());
            while (e4.scope != null) {
                if ((e4.sym.getKind().isClass() || e4.sym.getKind().isInterface()) && e4.sym != element) {
                    explicitNamedImports.add(element);
                    continue block17;
                }
                e4 = e4.next();
            }
        }
        Map<javax.lang.model.element.Name, TypeElement> usedTypes = null;
        for (Map.Entry entry : pkgCounts.entrySet()) {
            if ((Integer)entry.getValue() == -1) {
                for (Element element2 : ((PackageElement)entry.getKey()).getEnclosedElements()) {
                    if (!element2.getKind().isClass() && !element2.getKind().isInterface()) continue;
                    Scope.Entry starEntry = importScope.lookup((Name)element2.getSimpleName());
                    if (starEntry.scope == null) continue;
                    TypeElement te = null;
                    for (Element e5 : elementsToImport) {
                        if (!e5.getKind().isClass() && !e5.getKind().isInterface() || element2.getSimpleName() != e5.getSimpleName()) continue;
                        te = (TypeElement)e5;
                        break;
                    }
                    if (te != null) {
                        explicitNamedImports.add(te);
                        continue;
                    }
                    if (usedTypes == null) {
                        usedTypes = this.getUsedTypes(cut);
                    }
                    if ((te = usedTypes.get(element2.getSimpleName())) == null) continue;
                    elementsToImport.add(te);
                    explicitNamedImports.add(te);
                }
            }
            if ((Integer)entry.getValue() >= 0 || !(entry.getKey() instanceof Symbol)) continue;
            importScope.importAll(((Symbol)entry.getKey()).members());
        }
        ImportsComparator comparator = new ImportsComparator(cs);
        Collections.sort(elementsToImport, comparator);
        TreeMaker make = this.copy.getTreeMaker();
        int currentToImport = elementsToImport.size() - 1;
        int currentExisting = imports.size() - 1;
        while (currentToImport >= 0) {
            Integer cnt;
            boolean isStar;
            Element currentToImportElement = (Element)elementsToImport.get(currentToImport);
            boolean isStatic = false;
            Element el = null;
            switch (currentToImportElement.getKind()) {
                case PACKAGE: {
                    el = currentToImportElement;
                    break;
                }
                case ANNOTATION_TYPE: 
                case CLASS: 
                case ENUM: 
                case INTERFACE: {
                    if (currentToImportElement.getEnclosingElement().getKind() != ElementKind.PACKAGE) break;
                    el = currentToImportElement.getEnclosingElement();
                    break;
                }
                case METHOD: 
                case ENUM_CONSTANT: 
                case FIELD: {
                    isStatic = true;
                    el = currentToImportElement.getEnclosingElement();
                }
            }
            Integer n = el == null ? Integer.valueOf(0) : (cnt = isStatic ? (Integer)typeCounts.get((TypeElement)el) : (Integer)pkgCounts.get((PackageElement)el));
            if (explicitNamedImports.contains(currentToImportElement)) {
                cnt = 0;
            }
            if (cnt == -2) {
                --currentToImport;
                continue;
            }
            if (cnt == -1) {
                currentToImportElement = el;
                if (isStatic) {
                    typeCounts.put((TypeElement)el, -2);
                } else {
                    pkgCounts.put((PackageElement)el, -2);
                }
            }
            boolean bl = isStar = currentToImportElement.getKind() == ElementKind.PACKAGE || isStatic && (currentToImportElement.getKind().isClass() || currentToImportElement.getKind().isInterface());
            while (--currentExisting >= 0) {
                ImportTree imp2 = (ImportTree)imports.get((int)currentExisting);
                Element impElement = this.getImportedElement(cut, imp2);
                Element element2 = imp2.isStatic() ? (impElement.getKind().isClass() || impElement.getKind().isInterface() ? impElement : elementUtilities.enclosingTypeElement(impElement)) : (impElement.getKind() == ElementKind.PACKAGE ? impElement : (el = (impElement.getKind().isClass() || impElement.getKind().isInterface()) && impElement.getEnclosingElement().getKind() == ElementKind.PACKAGE ? impElement.getEnclosingElement() : null));
                if (isStatic == imp2.isStatic() && (currentToImportElement == impElement || isStar && currentToImportElement == el)) {
                    imports.remove((int)currentExisting);
                    continue;
                }
                if (comparator.compare(currentToImportElement, (Object)imp2) > 0) break;
            }
            ExpressionTree qualIdent = this.qualIdentFor(currentToImportElement);
            if (isStar) {
                qualIdent = make.MemberSelect(qualIdent, elements.getName("*"));
            }
            imports.add((int)(currentExisting + true), make.Import((Tree)qualIdent, isStatic));
            --currentToImport;
        }
        return make.CompilationUnit(cut.getPackageAnnotations(), cut.getPackageName(), imports, cut.getTypeDecls(), cut.getSourceFile());
    }

    public <T extends Tree> T importFQNs(T original) {
        return TranslateIdentifier.importFQNs(this.copy, original);
    }

    public <T extends Tree> T importComments(T original, CompilationUnitTree cut) {
        return GeneratorUtilities.importComments(this.copy, original, cut);
    }

    static <T extends Tree> T importComments(CompilationInfo info, T original, CompilationUnitTree cut) {
        try {
            CommentSetImpl comments = CommentHandlerService.instance(info.impl.getJavacTask().getContext()).getComments((Tree)original);
            if (comments.areCommentsMapped()) {
                return (T)original;
            }
            JCTree.JCCompilationUnit unit = (JCTree.JCCompilationUnit)cut;
            TokenHierarchy tokens = unit.getSourceFile() instanceof AbstractSourceFileObject ? ((AbstractSourceFileObject)unit.getSourceFile()).getTokenHierarchy() : TokenHierarchy.create((CharSequence)unit.getSourceFile().getCharContent(true), (Language)JavaTokenId.language());
            TokenSequence seq = tokens.tokenSequence(JavaTokenId.language());
            TreePath tp = TreePath.getPath((CompilationUnitTree)cut, original);
            T toMap = tp != null && original.getKind() != Tree.Kind.COMPILATION_UNIT ? tp.getParentPath().getLeaf() : original;
            AssignComments translator = new AssignComments(info, (Tree)original, (TokenSequence<JavaTokenId>)seq, (CompilationUnitTree)unit);
            translator.scan((Tree)toMap, null);
            return (T)original;
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
            return (T)original;
        }
    }

    public void copyComments(Tree source, Tree target, boolean preceding) {
        CommentHandlerService handler = CommentHandlerService.instance(this.copy.impl.getJavacTask().getContext());
        CommentSetImpl s = handler.getComments(source);
        TreeUtilities.ensureCommentsMapped(this.copy, source, s);
        CommentSetImpl t = handler.getComments(target);
        if (preceding) {
            t.addComments(CommentSet.RelativePosition.PRECEDING, this.copy.useComments(s.getComments(CommentSet.RelativePosition.PRECEDING)));
            t.addComments(CommentSet.RelativePosition.INNER, this.copy.useComments(s.getComments(CommentSet.RelativePosition.INNER)));
        } else {
            t.addComments(CommentSet.RelativePosition.INLINE, this.copy.useComments(s.getComments(CommentSet.RelativePosition.INLINE)));
            t.addComments(CommentSet.RelativePosition.TRAILING, this.copy.useComments(s.getComments(CommentSet.RelativePosition.TRAILING)));
        }
    }

    public /* varargs */ ModifiersTree appendToAnnotationValue(ModifiersTree modifiers, TypeElement annotation, String attributeName, ExpressionTree ... attributeValuesToAdd) {
        return (ModifiersTree)this.appendToAnnotationValue((Tree)modifiers, annotation, attributeName, attributeValuesToAdd);
    }

    public /* varargs */ CompilationUnitTree appendToAnnotationValue(CompilationUnitTree compilationUnit, TypeElement annotation, String attributeName, ExpressionTree ... attributeValuesToAdd) {
        return (CompilationUnitTree)this.appendToAnnotationValue((Tree)compilationUnit, annotation, attributeName, attributeValuesToAdd);
    }

    private /* varargs */ Tree appendToAnnotationValue(Tree modifiers, TypeElement annotation, String attributeName, ExpressionTree ... attributeValuesToAdd) {
        TreeMaker make = this.copy.getTreeMaker();
        java.util.List annotations = null;
        if (modifiers.getKind() == Tree.Kind.MODIFIERS) {
            annotations = ((ModifiersTree)modifiers).getAnnotations();
        } else if (modifiers.getKind() == Tree.Kind.COMPILATION_UNIT) {
            annotations = ((CompilationUnitTree)modifiers).getPackageAnnotations();
        } else {
            throw new IllegalStateException();
        }
        for (AnnotationTree at : annotations) {
            TreePath tp = new TreePath(new TreePath(this.copy.getCompilationUnit()), at.getAnnotationType());
            Element e = this.copy.getTrees().getElement(tp);
            if (!annotation.equals(e)) continue;
            java.util.List arguments = at.getArguments();
            for (ExpressionTree et : arguments) {
                ExpressionTree expression;
                if (et.getKind() == Tree.Kind.ASSIGNMENT) {
                    AssignmentTree assignment = (AssignmentTree)et;
                    if (!((IdentifierTree)assignment.getVariable()).getName().contentEquals(attributeName)) continue;
                    expression = assignment.getExpression();
                } else {
                    if (!"value".equals(attributeName)) continue;
                    expression = et;
                }
                java.util.List currentValues = expression.getKind() == Tree.Kind.NEW_ARRAY ? ((NewArrayTree)expression).getInitializers() : Collections.singletonList(expression);
                assert (currentValues != null);
                ArrayList<ExpressionTree> values = new ArrayList<ExpressionTree>(currentValues);
                values.addAll(Arrays.asList(attributeValuesToAdd));
                NewArrayTree newAssignment = make.NewArray(null, Collections.emptyList(), values);
                return this.copy.getTreeUtilities().translate(modifiers, Collections.singletonMap(expression, newAssignment));
            }
            AnnotationTree newAnnotation = make.addAnnotationAttrValue(at, (ExpressionTree)make.Assignment((ExpressionTree)make.Identifier(attributeName), (ExpressionTree)make.NewArray(null, Collections.emptyList(), Arrays.asList(attributeValuesToAdd))));
            return this.copy.getTreeUtilities().translate(modifiers, Collections.singletonMap(at, newAnnotation));
        }
        ExpressionTree attribute = attributeValuesToAdd.length > 1 ? make.NewArray(null, Collections.emptyList(), Arrays.asList(attributeValuesToAdd)) : attributeValuesToAdd[0];
        ExpressionTree attributeAssignmentTree = "value".equals(attributeName) ? attribute : make.Assignment((ExpressionTree)make.Identifier(attributeName), attribute);
        AnnotationTree newAnnotation = make.Annotation((Tree)make.QualIdent(annotation), Collections.singletonList(attributeAssignmentTree));
        if (modifiers.getKind() == Tree.Kind.MODIFIERS) {
            return make.addModifiersAnnotation((ModifiersTree)modifiers, newAnnotation);
        }
        if (modifiers.getKind() == Tree.Kind.COMPILATION_UNIT) {
            return make.addPackageAnnotation((CompilationUnitTree)modifiers, newAnnotation);
        }
        throw new IllegalStateException();
    }

    private MethodTree createMethod(ExecutableElement element, TypeElement clazz, boolean isImplement) {
        MethodTree method;
        TreeMaker make = this.copy.getTreeMaker();
        MethodTree prototype = this.createMethod((DeclaredType)clazz.asType(), element);
        ModifiersTree mt = prototype.getModifiers();
        if (GeneratorUtilities.supportsOverride(this.copy) && this.copy.getSourceVersion().compareTo(SourceVersion.RELEASE_5) >= 0) {
            boolean generate = true;
            if (this.copy.getSourceVersion().compareTo(SourceVersion.RELEASE_5) == 0) {
                boolean bl = generate = !element.getEnclosingElement().getKind().isInterface();
            }
            if (generate) {
                mt = make.addModifiersAnnotation(prototype.getModifiers(), make.Annotation((Tree)make.Identifier("Override"), Collections.emptyList()));
            }
        }
        String bodyTemplate = null;
        if (isImplement && clazz.getKind().isInterface()) {
            mt = make.addModifiersModifier(mt, Modifier.DEFAULT);
        }
        boolean isAbstract = element.getModifiers().contains((Object)Modifier.ABSTRACT);
        if (isImplement || clazz.getKind().isClass() && (!isAbstract || !clazz.getModifiers().contains((Object)Modifier.ABSTRACT))) {
            try {
                bodyTemplate = "{" + GeneratorUtilities.readFromTemplate(isAbstract ? "Templates/Classes/Code/GeneratedMethodBody" : "Templates/Classes/Code/OverriddenMethodBody", this.createBindings(clazz, element)) + "\n}";
            }
            catch (Exception e) {
                bodyTemplate = "{}";
            }
        } else if (clazz.getKind().isClass()) {
            mt = make.addModifiersModifier(mt, Modifier.ABSTRACT);
        }
        if ((method = make.Method(mt, (CharSequence)prototype.getName(), prototype.getReturnType(), prototype.getTypeParameters(), prototype.getParameters(), prototype.getThrows(), bodyTemplate, null)).getBody() != null) {
            if (GeneratorUtilities.containsErrors((Tree)method.getBody())) {
                this.copy.rewrite((Tree)method.getBody(), (Tree)make.Block(Collections.emptyList(), false));
            } else {
                Trees trees = this.copy.getTrees();
                TreePath path = trees.getPath((Element)clazz);
                if (path == null) {
                    path = new TreePath(this.copy.getCompilationUnit());
                }
                com.sun.source.tree.Scope s = trees.getScope(path);
                BlockTree body = method.getBody();
                this.copy.getTreeUtilities().attributeTree((Tree)body, s);
                body = this.importFQNs((T)body);
                this.copy.rewrite((Tree)method.getBody(), (Tree)body);
            }
        }
        return method;
    }

    private static Object defaultValue(TypeMirror type) {
        switch (type.getKind()) {
            case BOOLEAN: {
                return false;
            }
            case BYTE: 
            case CHAR: 
            case DOUBLE: 
            case FLOAT: 
            case INT: 
            case LONG: 
            case SHORT: {
                return 0;
            }
        }
        return null;
    }

    private static boolean supportsOverride(CompilationInfo info) {
        return info.getElements().getTypeElement("java.lang.Override") != null;
    }

    private Tree resolveWildcard(TypeMirror type) {
        Tree result;
        TreeMaker make = this.copy.getTreeMaker();
        if (type != null && type.getKind() == TypeKind.WILDCARD) {
            WildcardType wt = (WildcardType)type;
            TypeMirror bound = wt.getSuperBound();
            if (bound == null) {
                bound = wt.getExtendsBound();
            }
            if (bound == null) {
                return make.Type("java.lang.Object");
            }
            result = make.Type(bound);
        } else {
            result = make.Type(type);
        }
        final IdentityHashMap translate = new IdentityHashMap();
        new TreeScanner<Void, Void>(){

            public Void visitWildcard(WildcardTree node, Void p) {
                Tree bound = node.getBound();
                if (bound != null && (bound.getKind() == Tree.Kind.EXTENDS_WILDCARD || bound.getKind() == Tree.Kind.SUPER_WILDCARD)) {
                    translate.put(bound, ((WildcardTree)bound).getBound());
                }
                return (Void)TreeScanner.super.visitWildcard(node, (Object)p);
            }
        }.scan(result, (Object)null);
        return this.copy.getTreeUtilities().translate(result, translate);
    }

    private Element getImportedElement(CompilationUnitTree cut, ImportTree imp) {
        Element element;
        Trees trees = this.copy.getTrees();
        Tree qualIdent = imp.getQualifiedIdentifier();
        if (qualIdent.getKind() != Tree.Kind.MEMBER_SELECT) {
            Element element2 = trees.getElement(TreePath.getPath((CompilationUnitTree)cut, (Tree)qualIdent));
            if (element2 == null) {
                String fqn = qualIdent.toString();
                if (fqn.endsWith(".*")) {
                    fqn = fqn.substring(0, fqn.length() - 2);
                }
                element2 = this.getElementByFQN(fqn);
            }
            return element2;
        }
        javax.lang.model.element.Name name = ((MemberSelectTree)qualIdent).getIdentifier();
        if ("*".contentEquals(name)) {
            Element element3 = trees.getElement(TreePath.getPath((CompilationUnitTree)cut, (Tree)((MemberSelectTree)qualIdent).getExpression()));
            if (element3 == null) {
                element3 = this.getElementByFQN(((MemberSelectTree)qualIdent).getExpression().toString());
            }
            return element3;
        }
        if (imp.isStatic()) {
            Element parent = trees.getElement(TreePath.getPath((CompilationUnitTree)cut, (Tree)((MemberSelectTree)qualIdent).getExpression()));
            if (parent == null) {
                parent = this.getElementByFQN(((MemberSelectTree)qualIdent).getExpression().toString());
            }
            if (parent != null && (parent.getKind().isClass() || parent.getKind().isInterface())) {
                com.sun.source.tree.Scope s = trees.getScope(new TreePath(cut));
                for (Element e : parent.getEnclosedElements()) {
                    if (name != e.getSimpleName() || !e.getModifiers().contains((Object)Modifier.STATIC) || !trees.isAccessible(s, e, (DeclaredType)parent.asType())) continue;
                    return e;
                }
                return parent;
            }
        }
        if ((element = trees.getElement(TreePath.getPath((CompilationUnitTree)cut, (Tree)qualIdent))) == null) {
            element = this.getElementByFQN(qualIdent.toString());
        }
        return element;
    }

    private Element getElementByFQN(String fqn) {
        Elements elements = this.copy.getElements();
        QualifiedNameable element = elements.getTypeElement(fqn);
        if (element == null) {
            element = elements.getPackageElement(fqn);
        }
        if (element == null) {
            element = ClassReader.instance((Context)this.copy.impl.getJavacTask().getContext()).enterClass((Name)elements.getName(fqn));
        }
        return element;
    }

    private Map<javax.lang.model.element.Name, TypeElement> getUsedTypes(CompilationUnitTree cut) {
        final Trees trees = this.copy.getTrees();
        final HashMap<javax.lang.model.element.Name, TypeElement> map = new HashMap<javax.lang.model.element.Name, TypeElement>();
        new TreePathScanner<Void, Void>(){

            public Void visitIdentifier(IdentifierTree node, Void p) {
                Element element;
                if (!map.containsKey(node.getName()) && (element = trees.getElement(this.getCurrentPath())) != null && (element.getKind().isClass() || element.getKind().isInterface()) && element.asType().getKind() != TypeKind.ERROR) {
                    map.put(node.getName(), (TypeElement)element);
                }
                return (Void)TreePathScanner.super.visitIdentifier(node, (Object)p);
            }

            public Void visitCompilationUnit(CompilationUnitTree node, Void p) {
                this.scan((Iterable)node.getPackageAnnotations(), (Object)p);
                return (Void)this.scan((Iterable)node.getTypeDecls(), (Object)p);
            }
        }.scan((Tree)cut, (Object)null);
        return map;
    }

    private ExpressionTree qualIdentFor(Element e) {
        TreeMaker tm = this.copy.getTreeMaker();
        if (e.getKind() == ElementKind.PACKAGE) {
            String name = ((PackageElement)e).getQualifiedName().toString();
            if (e instanceof Symbol) {
                int lastDot = name.lastIndexOf(46);
                if (lastDot < 0) {
                    return tm.Identifier(e);
                }
                return tm.MemberSelect(this.qualIdentFor(name.substring(0, lastDot)), e);
            }
            return this.qualIdentFor(name);
        }
        Element ee = e.getEnclosingElement();
        if (e instanceof Symbol) {
            return ee.getSimpleName().length() > 0 ? tm.MemberSelect(this.qualIdentFor(ee), e) : tm.Identifier(e);
        }
        return ee.getSimpleName().length() > 0 ? tm.MemberSelect(this.qualIdentFor(ee), e.getSimpleName()) : tm.Identifier(e.getSimpleName());
    }

    private ExpressionTree qualIdentFor(String name) {
        Elements elements = this.copy.getElements();
        TreeMaker tm = this.copy.getTreeMaker();
        int lastDot = name.lastIndexOf(46);
        if (lastDot < 0) {
            return tm.Identifier(elements.getName(name));
        }
        return tm.MemberSelect(this.qualIdentFor(name.substring(0, lastDot)), elements.getName(name.substring(lastDot + 1)));
    }

    private Map<String, Object> createBindings(TypeElement clazz, ExecutableElement element) {
        Object value;
        CodeStyle cs = DiffContext.getCodeStyle(this.copy);
        HashMap<String, Object> bindings = new HashMap<String, Object>();
        bindings.put("class_name", clazz.getQualifiedName().toString());
        bindings.put("simple_class_name", clazz.getSimpleName().toString());
        bindings.put("method_name", element.getSimpleName().toString());
        bindings.put("method_return_type", element.getReturnType().toString());
        switch (element.getReturnType().getKind()) {
            case BOOLEAN: {
                value = "false";
                break;
            }
            case BYTE: 
            case CHAR: 
            case DOUBLE: 
            case FLOAT: 
            case INT: 
            case LONG: 
            case SHORT: {
                value = 0;
                break;
            }
            default: {
                value = "null";
            }
        }
        bindings.put("default_return_value", value);
        StringBuilder sb = new StringBuilder();
        if (element.isDefault() && element.getEnclosingElement().getKind().isInterface()) {
            Types types = this.copy.getTypes();
            TypeMirror enclType = element.getEnclosingElement().asType();
            if (!types.isSubtype(clazz.getSuperclass(), enclType)) {
                for (TypeMirror iface : clazz.getInterfaces()) {
                    if (!types.isSubtype(iface, enclType)) continue;
                    sb.append(((DeclaredType)iface).asElement().getSimpleName()).append('.');
                    break;
                }
            }
        }
        sb.append("super.").append(element.getSimpleName()).append('(');
        Iterator<? extends VariableElement> it = element.getParameters().iterator();
        while (it.hasNext()) {
            VariableElement ve = it.next();
            sb.append(GeneratorUtilities.addParamPrefixSuffix(GeneratorUtilities.removeParamPrefixSuffix(ve, cs), cs));
            if (!it.hasNext()) continue;
            sb.append(",");
        }
        sb.append(')');
        bindings.put("super_method_call", sb);
        return bindings;
    }

    private static String name(Tree tree) {
        switch (tree.getKind()) {
            case VARIABLE: {
                return ((VariableTree)tree).getName().toString();
            }
            case METHOD: {
                return ((MethodTree)tree).getName().toString();
            }
            case CLASS: {
                return ((ClassTree)tree).getSimpleName().toString();
            }
            case IDENTIFIER: {
                return ((IdentifierTree)tree).getName().toString();
            }
            case MEMBER_SELECT: {
                return GeneratorUtilities.name((Tree)((MemberSelectTree)tree).getExpression()) + '.' + ((MemberSelectTree)tree).getIdentifier();
            }
        }
        return "";
    }

    private static String[] correspondingGSNames(Tree member) {
        if (GeneratorUtilities.isSetter(member)) {
            String name = GeneratorUtilities.name(member);
            VariableTree param = (VariableTree)((MethodTree)member).getParameters().get(0);
            if (param.getType().getKind() == Tree.Kind.PRIMITIVE_TYPE && ((PrimitiveTypeTree)param.getType()).getPrimitiveTypeKind() == TypeKind.BOOLEAN) {
                return new String[]{"" + 'g' + name.substring(1), "is" + name.substring(3)};
            }
            return new String[]{"" + 'g' + name.substring(1)};
        }
        if (GeneratorUtilities.isGetter(member)) {
            return new String[]{"" + 's' + GeneratorUtilities.name(member).substring(1)};
        }
        if (GeneratorUtilities.isBooleanGetter(member)) {
            return new String[]{"set" + GeneratorUtilities.name(member).substring(2)};
        }
        return null;
    }

    private static boolean isSetter(Tree member) {
        return member.getKind() == Tree.Kind.METHOD && GeneratorUtilities.name(member).startsWith("set") && ((MethodTree)member).getParameters().size() == 1 && ((MethodTree)member).getReturnType().getKind() == Tree.Kind.PRIMITIVE_TYPE && ((PrimitiveTypeTree)((MethodTree)member).getReturnType()).getPrimitiveTypeKind() == TypeKind.VOID;
    }

    private static boolean isGetter(Tree member) {
        return member.getKind() == Tree.Kind.METHOD && GeneratorUtilities.name(member).startsWith("get") && ((MethodTree)member).getParameters().isEmpty() && (((MethodTree)member).getReturnType().getKind() != Tree.Kind.PRIMITIVE_TYPE || ((PrimitiveTypeTree)((MethodTree)member).getReturnType()).getPrimitiveTypeKind() != TypeKind.VOID);
    }

    private static boolean isBooleanGetter(Tree member) {
        return member.getKind() == Tree.Kind.METHOD && GeneratorUtilities.name(member).startsWith("is") && ((MethodTree)member).getParameters().isEmpty() && ((MethodTree)member).getReturnType().getKind() == Tree.Kind.PRIMITIVE_TYPE && ((PrimitiveTypeTree)((MethodTree)member).getReturnType()).getPrimitiveTypeKind() == TypeKind.BOOLEAN;
    }

    private static String removeFieldPrefixSuffix(VariableElement var, CodeStyle cs) {
        boolean isStatic;
        return CodeStyleUtils.removePrefixSuffix(var.getSimpleName(), (isStatic = var.getModifiers().contains((Object)Modifier.STATIC)) ? cs.getStaticFieldNamePrefix() : cs.getFieldNamePrefix(), isStatic ? cs.getStaticFieldNameSuffix() : cs.getFieldNameSuffix());
    }

    private static String removeFieldPrefixSuffix(VariableTree var, CodeStyle cs) {
        boolean isStatic;
        return CodeStyleUtils.removePrefixSuffix(var.getName(), (isStatic = var.getModifiers().getFlags().contains((Object)Modifier.STATIC)) ? cs.getStaticFieldNamePrefix() : cs.getFieldNamePrefix(), isStatic ? cs.getStaticFieldNameSuffix() : cs.getFieldNameSuffix());
    }

    private static String addParamPrefixSuffix(CharSequence name, CodeStyle cs) {
        return CodeStyleUtils.addPrefixSuffix(name, cs.getParameterNamePrefix(), cs.getParameterNameSuffix());
    }

    private static String removeParamPrefixSuffix(VariableElement var, CodeStyle cs) {
        return CodeStyleUtils.removePrefixSuffix(var.getSimpleName(), cs.getParameterNamePrefix(), cs.getParameterNameSuffix());
    }

    private void tagFirst(java.util.List<MethodTree> methods) {
        BlockTree body;
        if (methods.size() > 0 && (body = methods.get(0).getBody()) != null && !body.getStatements().isEmpty()) {
            this.copy.tag((Tree)body.getStatements().get(0), "methodBodyTag");
        }
    }

    static boolean checkPackagesForStarImport(String pkgName, CodeStyle cs) {
        for (String s : cs.getPackagesForStarImport()) {
            if (!(s.endsWith(".*") ? pkgName.startsWith(s = s.substring(0, s.length() - 2)) : pkgName.equals(s))) continue;
            return true;
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static String readFromTemplate(String pathToTemplate, Map<String, Object> values) throws IOException, ScriptException {
        FileObject template = FileUtil.getConfigFile((String)pathToTemplate);
        Charset sourceEnc = FileEncodingQuery.getEncoding((FileObject)template);
        ScriptEngine eng = GeneratorUtilities.engine(template);
        Bindings bind = eng.getContext().getBindings(100);
        bind.putAll(values);
        Reader is = null;
        try {
            eng.getContext().setAttribute(FileObject.class.getName(), (Object)template, 100);
            eng.getContext().setAttribute("javax.script.filename", template.getNameExt(), 100);
            eng.getContext().setAttribute("com.sun.script.freemarker.stringOut", true, 100);
            is = new InputStreamReader(template.getInputStream(), sourceEnc);
            String string = (String)eng.eval(is);
            return string;
        }
        finally {
            if (is != null) {
                is.close();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static ScriptEngine engine(FileObject fo) {
        Object obj = fo.getAttribute("javax.script.ScriptEngine");
        if (obj instanceof ScriptEngine) {
            return (ScriptEngine)obj;
        }
        if (obj instanceof String) {
            Class<GeneratorUtilities> class_ = GeneratorUtilities.class;
            synchronized (GeneratorUtilities.class) {
                if (manager == null) {
                    ClassLoader loader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
                    manager = new ScriptEngineManager(loader != null ? loader : Thread.currentThread().getContextClassLoader());
                }
                // ** MonitorExit[var2_2] (shouldn't be in output)
                return manager.getEngineByName((String)obj);
            }
        }
        return null;
    }

    private static boolean containsErrors(Tree tree) {
        Boolean b = new TreeScanner<Boolean, Boolean>(){

            public Boolean visitErroneous(ErroneousTree node, Boolean p) {
                return true;
            }

            public Boolean reduce(Boolean r1, Boolean r2) {
                if (r1 == null) {
                    r1 = false;
                }
                if (r2 == null) {
                    r2 = false;
                }
                return r1 != false || r2 != false;
            }

            public Boolean scan(Tree node, Boolean p) {
                return p != false ? p : (Boolean)TreeScanner.super.scan(node, (Object)p);
            }
        }.scan(tree, Boolean.valueOf(false));
        return b != null ? b : false;
    }

    private static class ImportsComparator
    implements Comparator<Object> {
        private final CodeStyle.ImportGroups groups;

        private ImportsComparator(CodeStyle cs) {
            this.groups = cs.getImportGroups();
        }

        @Override
        public int compare(Object o1, Object o2) {
            if (o1 == o2) {
                return 0;
            }
            boolean isStatic1 = false;
            StringBuilder sb1 = new StringBuilder();
            if (o1 instanceof ImportTree) {
                isStatic1 = ((ImportTree)o1).isStatic();
                sb1.append(((ImportTree)o1).getQualifiedIdentifier().toString());
            } else if (o1 instanceof Element) {
                Element e1 = (Element)o1;
                if (e1.getKind().isField() || e1.getKind() == ElementKind.METHOD) {
                    sb1.append('.').append(e1.getSimpleName());
                    e1 = e1.getEnclosingElement();
                    isStatic1 = true;
                }
                if (e1.getKind().isClass() || e1.getKind().isInterface()) {
                    sb1.insert(0, ((TypeElement)e1).getQualifiedName());
                } else if (e1.getKind() == ElementKind.PACKAGE) {
                    sb1.insert(0, ((PackageElement)e1).getQualifiedName());
                }
            }
            String s1 = sb1.toString();
            boolean isStatic2 = false;
            StringBuilder sb2 = new StringBuilder();
            if (o2 instanceof ImportTree) {
                isStatic2 = ((ImportTree)o2).isStatic();
                sb2.append(((ImportTree)o2).getQualifiedIdentifier().toString());
            } else if (o2 instanceof Element) {
                Element e2 = (Element)o2;
                if (e2.getKind().isField() || e2.getKind() == ElementKind.METHOD) {
                    sb2.append('.').append(e2.getSimpleName());
                    e2 = e2.getEnclosingElement();
                    isStatic2 = true;
                }
                if (e2.getKind().isClass() || e2.getKind().isInterface()) {
                    sb2.insert(0, ((TypeElement)e2).getQualifiedName());
                } else if (e2.getKind() == ElementKind.PACKAGE) {
                    sb2.insert(0, ((PackageElement)e2).getQualifiedName());
                }
            }
            String s2 = sb2.toString();
            int bal = this.groups.getGroupId(s1, isStatic1) - this.groups.getGroupId(s2, isStatic2);
            return bal == 0 ? s1.compareTo(s2) : bal;
        }
    }

    private static class ClassMemberComparator
    implements Comparator<Tree> {
        private final CodeStyle.MemberGroups groups;
        private final boolean sortMembersAlpha;
        private final boolean keepGASTogether;

        public ClassMemberComparator(CodeStyle cs) {
            this.groups = cs.getClassMemberGroups();
            this.sortMembersAlpha = cs.sortMembersInGroupsAlphabetically();
            this.keepGASTogether = cs.keepGettersAndSettersTogether();
        }

        @Override
        public int compare(Tree tree1, Tree tree2) {
            if (tree1 == tree2) {
                return 0;
            }
            int diff = this.groups.getGroupId(tree1) - this.groups.getGroupId(tree2);
            if (diff == 0 && this.sortMembersAlpha) {
                String name1 = GeneratorUtilities.name(tree1);
                String name2 = GeneratorUtilities.name(tree2);
                if (this.keepGASTogether) {
                    if (GeneratorUtilities.isSetter(tree1)) {
                        name1 = "g" + name1.substring(1) + "+1";
                    }
                    if (GeneratorUtilities.isSetter(tree2)) {
                        name2 = "g" + name2.substring(1) + "+1";
                    }
                }
                diff = name1.compareTo(name2);
            }
            return diff;
        }
    }

}

