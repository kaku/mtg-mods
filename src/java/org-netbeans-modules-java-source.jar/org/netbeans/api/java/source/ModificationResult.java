/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Log
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullUnknown
 *  org.netbeans.api.queries.FileEncodingQuery
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 *  org.netbeans.modules.parsing.api.Embedding
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.impl.Utilities
 *  org.netbeans.modules.parsing.impl.indexing.friendapi.IndexingController
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.NbDocument
 *  org.openide.text.PositionRef
 *  org.openide.util.NbBundle
 *  org.openide.util.Parameters
 *  org.openide.util.Utilities
 */
package org.netbeans.api.java.source;

import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.Log;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.StyledDocument;
import javax.tools.JavaFileObject;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullUnknown;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.api.queries.FileEncodingQuery;
import org.netbeans.editor.BaseDocument;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.source.JavaFileFilterQuery;
import org.netbeans.modules.java.source.JavaSourceAccessor;
import org.netbeans.modules.java.source.JavaSourceSupportAccessor;
import org.netbeans.modules.java.source.parsing.CompilationInfoImpl;
import org.netbeans.modules.java.source.parsing.SourceFileManager;
import org.netbeans.modules.java.source.save.ElementOverlay;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.impl.Utilities;
import org.netbeans.modules.parsing.impl.indexing.friendapi.IndexingController;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.text.NbDocument;
import org.openide.text.PositionRef;
import org.openide.util.NbBundle;
import org.openide.util.Parameters;

public final class ModificationResult {
    private static final Logger LOG = Logger.getLogger(ModificationResult.class.getName());
    private Collection<Source> sources;
    private boolean committed;
    Map<FileObject, List<Difference>> diffs = new HashMap<FileObject, List<Difference>>();
    Map<?, int[]> tag2Span = new IdentityHashMap();
    private final Throwable creator;
    static LinkedList<Throwable> lastCommitted = new LinkedList();

    ModificationResult(JavaSource js) {
        boolean keepStackTrace = false;
        if (!$assertionsDisabled) {
            keepStackTrace = true;
            if (!true) {
                throw new AssertionError();
            }
        }
        this.creator = keepStackTrace ? new Throwable() : null;
        this.sources = js != null ? JavaSourceAccessor.getINSTANCE().getSources(js) : null;
    }

    private ModificationResult(Collection<Source> sources) {
        boolean keepStackTrace = false;
        if (!$assertionsDisabled) {
            keepStackTrace = true;
            if (!true) {
                throw new AssertionError();
            }
        }
        this.creator = keepStackTrace ? new Throwable() : null;
        this.sources = sources;
    }

    @NonNull
    public static ModificationResult runModificationTask(@NonNull Collection<Source> sources, final @NonNull UserTask task) throws ParseException {
        final ModificationResult result = new ModificationResult(sources);
        final ElementOverlay overlay = ElementOverlay.getOrCreateOverlay();
        ParserManager.parse(sources, (UserTask)new UserTask(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            public void run(ResultIterator resultIterator) throws Exception {
                ResultIterator resultIterator2 = resultIterator = "text/x-java".equals(resultIterator.getSnapshot().getMimeType()) ? resultIterator : this.findEmbeddedJava(resultIterator);
                if (resultIterator != null) {
                    Parser.Result parserResult = resultIterator.getParserResult();
                    CompilationController cc = CompilationController.get(parserResult);
                    assert (cc != null);
                    WorkingCopy copy = new WorkingCopy(cc.impl, overlay);
                    assert (WorkingCopy.instance == null);
                    WorkingCopy.instance = new WeakReference<WorkingCopy>(copy);
                    try {
                        task.run(resultIterator);
                    }
                    finally {
                        WorkingCopy.instance = null;
                    }
                    JavacTaskImpl jt = copy.impl.getJavacTask();
                    Log.instance((Context)jt.getContext()).nerrors = 0;
                    List<Difference> diffs = copy.getChanges(result.tag2Span);
                    if (diffs != null && diffs.size() > 0) {
                        result.diffs.put(copy.getFileObject(), diffs);
                    }
                }
            }

            private ResultIterator findEmbeddedJava(ResultIterator theMess) throws ParseException {
                LinkedList<Embedding> todo = new LinkedList<Embedding>();
                for (Embedding embedding2 : theMess.getEmbeddings()) {
                    if ("text/x-java".equals(embedding2.getMimeType())) {
                        return theMess.getResultIterator(embedding2);
                    }
                    todo.add(embedding2);
                }
                for (Embedding embedding2 : todo) {
                    ResultIterator result2 = this.findEmbeddedJava(theMess.getResultIterator(embedding2));
                    if (result2 == null) continue;
                    return result2;
                }
                return null;
            }
        });
        return result;
    }

    @NonNull
    public Set<? extends FileObject> getModifiedFileObjects() {
        return this.diffs.keySet();
    }

    public List<? extends Difference> getDifferences(@NonNull FileObject fo) {
        return this.diffs.get((Object)fo);
    }

    @NonNull
    public Set<File> getNewFiles() {
        HashSet<File> newFiles = new HashSet<File>();
        for (List<Difference> ds : this.diffs.values()) {
            for (Difference d : ds) {
                if (d.getKind() != Difference.Kind.CREATE) continue;
                newFiles.add(org.openide.util.Utilities.toFile((URI)((CreateChange)d).getFileObject().toUri()));
            }
        }
        return newFiles;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void commit() throws IOException {
        if (this.committed) {
            throw new IllegalStateException("Calling commit on already committed Modificationesult.");
        }
        try {
            IndexingController.getDefault().enterProtectedMode();
            try {
                for (Map.Entry<FileObject, List<Difference>> me : this.diffs.entrySet()) {
                    ModificationResult.commit(me.getKey(), me.getValue(), null);
                }
            }
            finally {
                HashSet<FileObject> alreadyRefreshed;
                alreadyRefreshed = new HashSet<FileObject>();
                try {
                    if (this.sources != null) {
                        SourceFileManager.ModifiedFiles modifiedFiles = SourceFileManager.getModifiedFiles();
                        for (Source source : this.sources) {
                            Utilities.revalidate((Source)source);
                            FileObject srcFile = source.getFileObject();
                            alreadyRefreshed.add(srcFile);
                            modifiedFiles.fileModified(srcFile.toURI());
                        }
                    }
                }
                finally {
                    IndexingController.getDefault().exitProtectedMode(null);
                }
                for (FileObject currentlyVisibleInEditor : JavaSourceSupportAccessor.ACCESSOR.getVisibleEditorsFiles()) {
                    Source source;
                    if (alreadyRefreshed.contains((Object)currentlyVisibleInEditor) || (source = Source.create((FileObject)currentlyVisibleInEditor)) == null) continue;
                    Utilities.revalidate((Source)source);
                }
            }
            while (lastCommitted.size() > 10) {
                lastCommitted.removeLast();
            }
            lastCommitted.addFirst(this.creator);
        }
        finally {
            this.committed = true;
            this.sources = null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void commit(FileObject fo, final List<Difference> differences, final Writer out) throws IOException {
        StyledDocument doc;
        EditorCookie ec;
        DataObject dObj = DataObject.find((FileObject)fo);
        EditorCookie editorCookie = ec = dObj != null ? (EditorCookie)dObj.getCookie(EditorCookie.class) : null;
        if (ec != null && out == null && (doc = ec.getDocument()) != null) {
            final IOException[] exceptions = new IOException[1];
            NbDocument.runAtomic((StyledDocument)doc, (Runnable)new Runnable(){

                @Override
                public void run() {
                    try {
                        ModificationResult.commit2(doc, differences, out);
                    }
                    catch (IOException ex) {
                        exceptions[0] = ex;
                    }
                }
            });
            if (exceptions[0] != null) {
                LOG.log(Level.INFO, "ModificationResult commit failed with an exception: ", exceptions[0]);
                int s = lastCommitted.size();
                for (Throwable t : lastCommitted) {
                    LOG.log(Level.INFO, "Previous commit number " + s--, t);
                }
                throw exceptions[0];
            }
            return;
        }
        Reader in = null;
        Writer out2 = out;
        JavaFileFilterImplementation filter = JavaFileFilterQuery.getFilter(fo);
        try {
            boolean ownOutput;
            int n;
            Charset encoding = FileEncodingQuery.getEncoding((FileObject)fo);
            boolean[] hasReturnChar = new boolean[1];
            in = new BufferedReader(new InputStreamReader((InputStream)new ByteArrayInputStream(fo.asBytes()), encoding));
            if (filter != null) {
                in = filter.filterReader(in);
            }
            in = new FilteringReader(in, hasReturnChar);
            boolean bl = ownOutput = out != null;
            if (out2 == null) {
                out2 = new BufferedWriter(new OutputStreamWriter(fo.getOutputStream(), encoding));
                if (filter != null) {
                    out2 = filter.filterWriter(out2);
                }
                out2 = new FilteringWriter(out2, hasReturnChar);
            }
            int offset = 0;
            for (Difference diff : differences) {
                int n2;
                if (diff.isExcluded()) continue;
                if (Difference.Kind.CREATE == diff.getKind()) {
                    if (ownOutput) continue;
                    ModificationResult.createUnit(diff, null);
                    continue;
                }
                int pos = diff.getStartPosition().getOffset();
                int toread = pos - offset;
                char[] buff = new char[toread];
                int rc = 0;
                while ((n2 = in.read(buff, 0, toread - rc)) > 0 && rc < toread) {
                    out2.write(buff, 0, n2);
                    rc += n2;
                    offset += n2;
                }
                switch (diff.getKind()) {
                    int len;
                    case INSERT: {
                        out2.write(diff.getNewText());
                        break;
                    }
                    case REMOVE: {
                        len = diff.getEndPosition().getOffset() - diff.getStartPosition().getOffset();
                        in.skip(len);
                        offset += len;
                        break;
                    }
                    case CHANGE: {
                        len = diff.getEndPosition().getOffset() - diff.getStartPosition().getOffset();
                        in.skip(len);
                        offset += len;
                        out2.write(diff.getNewText());
                    }
                }
            }
            char[] buff = new char[1024];
            while ((n = in.read(buff)) > 0) {
                out2.write(buff, 0, n);
            }
        }
        finally {
            if (in != null) {
                in.close();
            }
            if (out2 != null) {
                out2.close();
            }
        }
    }

    private static void commit2(StyledDocument doc, List<Difference> differences, Writer out) throws IOException {
        for (Difference diff : differences) {
            if (diff.isExcluded()) continue;
            switch (diff.getKind()) {
                case INSERT: 
                case REMOVE: 
                case CHANGE: {
                    ModificationResult.processDocument(doc, diff);
                    break;
                }
                case CREATE: {
                    ModificationResult.createUnit(diff, out);
                }
            }
        }
    }

    private static void processDocument(final StyledDocument doc, final Difference diff) throws IOException {
        final BadLocationException[] blex = new BadLocationException[1];
        Runnable task = new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                DocumentListener l = null;
                try {
                    if (doc instanceof BaseDocument) {
                        l = new DocumentListener(){

                            @Override
                            public void insertUpdate(DocumentEvent e) {
                                DocumentUtilities.putEventProperty((DocumentEvent)e, (Object)"caretIgnore", (Object)Boolean.TRUE);
                            }

                            @Override
                            public void removeUpdate(DocumentEvent e) {
                                DocumentUtilities.putEventProperty((DocumentEvent)e, (Object)"caretIgnore", (Object)Boolean.TRUE);
                            }

                            @Override
                            public void changedUpdate(DocumentEvent e) {
                                DocumentUtilities.putEventProperty((DocumentEvent)e, (Object)"caretIgnore", (Object)Boolean.TRUE);
                            }
                        };
                        doc.addDocumentListener(l);
                    }
                    ModificationResult.processDocumentLocked(doc, diff);
                }
                catch (BadLocationException ex) {
                    blex[0] = ex;
                }
                finally {
                    if (l != null) {
                        doc.removeDocumentListener(l);
                    }
                }
            }

        };
        if (diff.isCommitToGuards()) {
            NbDocument.runAtomic((StyledDocument)doc, (Runnable)task);
        } else {
            try {
                NbDocument.runAtomicAsUser((StyledDocument)doc, (Runnable)task);
            }
            catch (BadLocationException ex) {
                blex[0] = ex;
            }
        }
        if (blex[0] != null) {
            IOException ioe = new IOException();
            ioe.initCause(blex[0]);
            throw ioe;
        }
    }

    private static void processDocumentLocked(Document doc, Difference diff) throws BadLocationException {
        switch (diff.getKind()) {
            case INSERT: {
                doc.insertString(diff.getStartPosition().getOffset(), diff.getNewText(), null);
                break;
            }
            case REMOVE: {
                doc.remove(diff.getStartPosition().getOffset(), diff.getEndPosition().getOffset() - diff.getStartPosition().getOffset());
                break;
            }
            case CHANGE: {
                int delta = diff.getNewText().length();
                int offs = diff.getStartPosition().getOffset();
                int removeLen = diff.getEndPosition().getOffset() - offs;
                doc.insertString(offs, diff.getNewText(), null);
                doc.remove(delta + offs, removeLen);
                break;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void createUnit(Difference diff, Writer out) {
        CreateChange change = (CreateChange)diff;
        Writer w = out;
        try {
            if (w == null) {
                change.getFileObject().openOutputStream();
                w = change.getFileObject().openWriter();
            }
            w.append(change.getNewText());
        }
        catch (IOException e) {
            Logger.getLogger(WorkingCopy.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        finally {
            if (w != null) {
                try {
                    w.close();
                }
                catch (IOException e) {
                    Logger.getLogger(WorkingCopy.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                }
            }
        }
    }

    @NonNull
    public String getResultingSource(@NonNull FileObject fileObject) throws IOException, IllegalArgumentException {
        Parameters.notNull((CharSequence)"fileObject", (Object)fileObject);
        if (!this.getModifiedFileObjects().contains((Object)fileObject)) {
            throw new IllegalArgumentException("File: " + FileUtil.getFileDisplayName((FileObject)fileObject) + " is not modified in this ModificationResult");
        }
        StringWriter writer = new StringWriter();
        ModificationResult.commit(fileObject, this.diffs.get((Object)fileObject), writer);
        return writer.toString();
    }

    @NullUnknown
    public int[] getSpan(@NonNull Object tag) {
        return this.tag2Span.get(tag);
    }

    private static final class FilteringWriter
    extends Writer {
        private final boolean[] hasReturnChar;
        private final Writer delegate;

        public FilteringWriter(Writer delegate, boolean[] hasReturnChar) {
            this.hasReturnChar = hasReturnChar;
            this.delegate = delegate;
        }

        @Override
        public void write(char[] cbuf, int off, int len) throws IOException {
            if (this.hasReturnChar[0]) {
                char[] buf = new char[len * 2];
                int j = 0;
                for (int i = off; i < off + len; ++i) {
                    if (cbuf[i] == '\n') {
                        buf[j++] = 13;
                        buf[j++] = 10;
                        continue;
                    }
                    buf[j++] = cbuf[i];
                }
                this.delegate.write(buf, 0, j);
            } else {
                this.delegate.write(cbuf, off, len);
            }
        }

        @Override
        public void flush() throws IOException {
            this.delegate.flush();
        }

        @Override
        public void close() throws IOException {
            this.delegate.close();
        }
    }

    private static final class FilteringReader
    extends Reader {
        private final Reader delegate;
        private final boolean[] hasReturnChar;
        private boolean beforeFirstLine = true;

        public FilteringReader(Reader delegate, boolean[] hasReturnChar) {
            this.delegate = delegate;
            this.hasReturnChar = hasReturnChar;
        }

        @Override
        public int read(char[] cbuf, int off, int len) throws IOException {
            int read;
            int j;
            do {
                if ((read = this.delegate.read(cbuf, off, len)) == -1) {
                    return -1;
                }
                j = 0;
                for (int i = off; i < off + read; ++i) {
                    if (cbuf[i] != '\r') {
                        cbuf[j++] = cbuf[i];
                        if (!this.beforeFirstLine || cbuf[i] != '\n') continue;
                        this.beforeFirstLine = false;
                        continue;
                    }
                    if (!this.beforeFirstLine) continue;
                    this.hasReturnChar[0] = true;
                    this.beforeFirstLine = false;
                }
            } while (j == 0 && read > 0);
            return j;
        }

        @Override
        public void close() throws IOException {
            this.delegate.close();
        }
    }

    static class CreateChange
    extends Difference {
        JavaFileObject fileObject;

        CreateChange(JavaFileObject fileObject, String text) {
            super(Difference.Kind.CREATE, null, null, null, text, NbBundle.getMessage(ModificationResult.class, (String)"TXT_CreateFile", (Object)fileObject.getName()));
            this.fileObject = fileObject;
        }

        public JavaFileObject getFileObject() {
            return this.fileObject;
        }

        @Override
        public String toString() {
            return (Object)((Object)this.kind) + "Create File: " + this.fileObject.getName() + "; contents = \"\n" + this.newText + "\"";
        }
    }

    public static class Difference {
        Kind kind;
        final PositionRef startPos;
        final PositionRef endPos;
        String oldText;
        String newText;
        final String description;
        private boolean excluded;
        private boolean ignoreGuards = false;

        Difference(Kind kind, PositionRef startPos, PositionRef endPos, String oldText, String newText, String description) {
            this.kind = kind;
            this.startPos = startPos;
            this.endPos = endPos;
            this.oldText = oldText;
            this.newText = newText;
            this.description = description;
            this.excluded = false;
        }

        Difference(Kind kind, PositionRef startPos, PositionRef endPos, String oldText, String newText) {
            this(kind, startPos, endPos, oldText, newText, null);
        }

        @NonNull
        public Kind getKind() {
            return this.kind;
        }

        @NonNull
        public PositionRef getStartPosition() {
            return this.startPos;
        }

        @NonNull
        public PositionRef getEndPosition() {
            return this.endPos;
        }

        @NonNull
        public String getOldText() {
            return this.oldText;
        }

        @NonNull
        public String getNewText() {
            return this.newText;
        }

        public boolean isExcluded() {
            return this.excluded;
        }

        public void exclude(boolean b) {
            this.excluded = b;
        }

        public boolean isCommitToGuards() {
            return this.ignoreGuards;
        }

        public void setCommitToGuards(boolean b) {
            this.ignoreGuards = b;
        }

        public String toString() {
            return (Object)((Object)this.kind) + "<" + this.startPos.getOffset() + ", " + this.endPos.getOffset() + ">: " + this.oldText + " -> " + this.newText;
        }

        public String getDescription() {
            return this.description;
        }

        public static enum Kind {
            INSERT,
            REMOVE,
            CHANGE,
            CREATE;
            

            private Kind() {
            }
        }

    }

}

