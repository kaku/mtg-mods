/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.api.java.source;

import org.netbeans.modules.parsing.api.Snapshot;

public final class PositionConverter {
    private final Snapshot snapshot;

    PositionConverter(Snapshot snapshot) {
        assert (snapshot != null);
        this.snapshot = snapshot;
    }

    PositionConverter() {
        this.snapshot = null;
    }

    public int getOriginalPosition(int javaSourcePosition) {
        return this.snapshot != null ? this.snapshot.getOriginalOffset(javaSourcePosition) : javaSourcePosition;
    }

    public int getJavaSourcePosition(int originalPosition) {
        return this.snapshot != null ? this.snapshot.getEmbeddedOffset(originalPosition) : originalPosition;
    }
}

