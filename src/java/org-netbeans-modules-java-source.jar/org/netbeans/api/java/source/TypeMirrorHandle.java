/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.code.Symbol$PackageSymbol
 *  com.sun.tools.javac.code.Symbol$TypeSymbol
 *  com.sun.tools.javac.code.Symtab
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.code.Type$ArrayType
 *  com.sun.tools.javac.code.Type$ClassType
 *  com.sun.tools.javac.code.Type$ErrorType
 *  com.sun.tools.javac.code.Type$JCNoType
 *  com.sun.tools.javac.code.Type$TypeVar
 *  com.sun.tools.javac.code.Type$UnionClassType
 *  com.sun.tools.javac.code.Type$Visitor
 *  com.sun.tools.javac.code.Type$WildcardType
 *  com.sun.tools.javac.code.TypeTag
 *  com.sun.tools.javac.code.Types
 *  com.sun.tools.javac.code.Types$DefaultTypeVisitor
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.List
 *  com.sun.tools.javac.util.ListBuffer
 *  com.sun.tools.javac.util.Name
 *  com.sun.tools.javac.util.Name$Table
 *  com.sun.tools.javac.util.Names
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.api.java.source;

import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Symtab;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.TypeTag;
import com.sun.tools.javac.code.Types;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.ListBuffer;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ErrorType;
import javax.lang.model.type.IntersectionType;
import javax.lang.model.type.NoType;
import javax.lang.model.type.NullType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.ReferenceType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.type.UnionType;
import javax.lang.model.type.WildcardType;
import javax.lang.model.util.Types;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.modules.java.source.parsing.CompilationInfoImpl;

public final class TypeMirrorHandle<T extends TypeMirror> {
    private final TypeKind kind;
    private final ElementHandle<? extends Element> element;
    private java.util.List<TypeMirrorHandle<? extends TypeMirror>> typeMirrors;

    private TypeMirrorHandle(TypeKind kind, ElementHandle<? extends Element> element, java.util.List<TypeMirrorHandle<? extends TypeMirror>> typeArguments) {
        this.kind = kind;
        this.element = element;
        this.typeMirrors = typeArguments;
    }

    @NonNull
    public static <T extends TypeMirror> TypeMirrorHandle<T> create(@NonNull T tm) {
        return TypeMirrorHandle.create(tm, new HashMap<TypeMirror, TypeMirrorHandle>());
    }

    private static <T extends TypeMirror> TypeMirrorHandle<T> create(T tm, Map<TypeMirror, TypeMirrorHandle> map) {
        TypeMirrorHandle handle = map.get(tm);
        if (handle != null) {
            return handle.typeMirrors == null ? handle : handle.typeMirrors.get(0);
        }
        map.put((TypeMirror)tm, new TypeMirrorHandle<T>(null, null, null));
        TypeKind kind = tm.getKind();
        ElementHandle<Element> element = null;
        java.util.List typeMirrors = null;
        switch (kind) {
            case BOOLEAN: 
            case BYTE: 
            case CHAR: 
            case DOUBLE: 
            case FLOAT: 
            case INT: 
            case LONG: 
            case NONE: 
            case NULL: 
            case SHORT: 
            case VOID: {
                break;
            }
            case DECLARED: {
                boolean genericOuter;
                java.util.List<? extends TypeMirror> targs;
                TypeMirror targ;
                TypeParameterElement tpe;
                boolean compoundType;
                DeclaredType dt = (DeclaredType)tm;
                boolean bl = compoundType = ((Type)tm).tsym != null && (((Type)tm).tsym.flags_field & 0x1000000) != 0;
                if (!compoundType) {
                    TypeElement te = (TypeElement)dt.asElement();
                    element = ElementHandle.create(te);
                    Element encl = te.getEnclosingElement();
                    boolean bl2 = genericOuter = (encl.getKind().isClass() || encl.getKind().isInterface()) && !((TypeElement)encl).getTypeParameters().isEmpty() && !te.getModifiers().contains((Object)Modifier.STATIC);
                    if (te.getTypeParameters().isEmpty() && !genericOuter) break;
                    targs = dt.getTypeArguments();
                } else {
                    genericOuter = false;
                    TypeElement fakeClass = (TypeElement)dt.asElement();
                    ArrayList<? extends TypeMirror> temp = new ArrayList<TypeMirror>(fakeClass.getInterfaces().size() + 1);
                    temp.add(fakeClass.getSuperclass());
                    temp.addAll(fakeClass.getInterfaces());
                    targs = temp;
                }
                if (!targs.isEmpty() && (targ = targs.get(0)).getKind() == TypeKind.TYPEVAR && (tpe = (TypeParameterElement)((TypeVariable)targ).asElement()).getGenericElement() == dt.asElement()) break;
                if (genericOuter) {
                    typeMirrors = new ArrayList(targs.size() + 1);
                    typeMirrors.add(TypeMirrorHandle.create(dt.getEnclosingType(), map));
                } else {
                    typeMirrors = new ArrayList(targs.size());
                }
                for (TypeMirror ta : targs) {
                    typeMirrors.add(TypeMirrorHandle.create(ta, map));
                }
                break;
            }
            case ARRAY: {
                typeMirrors = Collections.singletonList(TypeMirrorHandle.create(((ArrayType)tm).getComponentType(), map));
                break;
            }
            case TYPEVAR: {
                TypeVariable tv = (TypeVariable)tm;
                element = ElementHandle.create(tv.asElement());
                typeMirrors = new ArrayList();
                typeMirrors.add(tv.getLowerBound() != null ? TypeMirrorHandle.create(tv.getLowerBound(), map) : null);
                typeMirrors.add(tv.getUpperBound() != null ? TypeMirrorHandle.create(tv.getUpperBound(), map) : null);
                break;
            }
            case WILDCARD: {
                WildcardType wt = (WildcardType)tm;
                typeMirrors = new ArrayList();
                typeMirrors.add(wt.getExtendsBound() != null ? TypeMirrorHandle.create(wt.getExtendsBound(), map) : null);
                typeMirrors.add(wt.getSuperBound() != null ? TypeMirrorHandle.create(wt.getSuperBound(), map) : null);
                break;
            }
            case ERROR: {
                ErrorType et = (ErrorType)tm;
                element = ElementHandle.create(et.asElement());
                break;
            }
            case UNION: {
                typeMirrors = new ArrayList<TypeMirrorHandle<? extends TypeMirror>>();
                for (TypeMirror alternative : ((UnionType)tm).getAlternatives()) {
                    typeMirrors.add(TypeMirrorHandle.create(alternative, map));
                }
                break;
            }
            case INTERSECTION: {
                typeMirrors = new ArrayList();
                for (TypeMirror alternative : ((IntersectionType)tm).getBounds()) {
                    typeMirrors.add(TypeMirrorHandle.create(alternative, map));
                }
                break;
            }
            default: {
                throw new IllegalArgumentException("Currently unsupported TypeKind: " + (Object)((Object)tm.getKind()));
            }
        }
        handle = new TypeMirrorHandle<T>(kind, element, typeMirrors);
        map.get(tm).typeMirrors = Collections.singletonList(handle);
        return handle;
    }

    public T resolve(@NonNull CompilationInfo info) {
        return this.resolve(info, new HashMap<TypeMirrorHandle, PlaceholderType>());
    }

    private T resolve(CompilationInfo info, Map<TypeMirrorHandle, PlaceholderType> map) {
        if (this.kind == null) {
            TypeMirrorHandle<? extends TypeMirror> handle = this.typeMirrors.get(0);
            PlaceholderType pt = map.get(handle);
            if (pt == null) {
                pt = new PlaceholderType();
                map.put(handle, pt);
            }
            return (T)(pt.delegate != null ? pt.delegate : pt);
        }
        switch (this.kind) {
            case BOOLEAN: 
            case BYTE: 
            case CHAR: 
            case DOUBLE: 
            case FLOAT: 
            case INT: 
            case LONG: 
            case SHORT: {
                return (T)info.getTypes().getPrimitiveType(this.kind);
            }
            case NONE: 
            case VOID: {
                return (T)info.getTypes().getNoType(this.kind);
            }
            case NULL: {
                return (T)info.getTypes().getNullType();
            }
            case DECLARED: {
                com.sun.tools.javac.code.Types t = com.sun.tools.javac.code.Types.instance((Context)info.impl.getJavacTask().getContext());
                if (this.element == null) {
                    List resolvedBounds = List.nil();
                    for (TypeMirrorHandle<? extends TypeMirror> bound : this.typeMirrors) {
                        TypeMirror resolved = TypeMirrorHandle.super.resolve(info, map);
                        if (resolved == null) {
                            return null;
                        }
                        resolvedBounds = resolvedBounds.prepend((Object)((Type)resolved));
                    }
                    Type ct = t.makeCompoundType(resolvedBounds.reverse());
                    ct.getTypeArguments();
                    return (T)ct;
                }
                TypeElement te = (TypeElement)this.element.resolve(info);
                if (te == null) {
                    return null;
                }
                if (this.typeMirrors == null) {
                    return (T)te.asType();
                }
                Iterator<TypeMirrorHandle<? extends TypeMirror>> it = this.typeMirrors.iterator();
                Element encl = te.getEnclosingElement();
                boolean genericOuter = (encl.getKind().isClass() || encl.getKind().isInterface()) && !((TypeElement)encl).getTypeParameters().isEmpty() && !te.getModifiers().contains((Object)Modifier.STATIC);
                TypeMirror outer = null;
                if (genericOuter) {
                    TypeMirror typeMirror = outer = it.hasNext() ? TypeMirrorHandle.super.resolve(info, map) : null;
                    if (outer == null || outer.getKind() != TypeKind.DECLARED) {
                        return null;
                    }
                }
                ArrayList<TypeMirror> resolvedTypeArguments = new ArrayList<TypeMirror>();
                while (it.hasNext()) {
                    TypeMirror resolved = TypeMirrorHandle.super.resolve(info, map);
                    if (resolved == null) {
                        return null;
                    }
                    resolvedTypeArguments.add(resolved);
                }
                DeclaredType dt = outer != null ? info.getTypes().getDeclaredType((DeclaredType)outer, te, resolvedTypeArguments.toArray(new TypeMirror[resolvedTypeArguments.size()])) : info.getTypes().getDeclaredType(te, resolvedTypeArguments.toArray(new TypeMirror[resolvedTypeArguments.size()]));
                t.supertype((Type)dt);
                t.interfaces((Type)dt);
                PlaceholderType pt = map.get(this);
                if (pt != null) {
                    pt.delegate = (Type)dt;
                    new Visitor().visitClassType((Type.ClassType)dt, null);
                }
                return (T)dt;
            }
            case ARRAY: {
                TypeMirror resolved = TypeMirrorHandle.super.resolve(info, map);
                if (resolved == null) {
                    return null;
                }
                ArrayType at = info.getTypes().getArrayType(resolved);
                PlaceholderType pt = map.get(this);
                if (pt != null) {
                    pt.delegate = (Type)at;
                    new Visitor().visitArrayType((Type.ArrayType)at, null);
                }
                return (T)at;
            }
            case TYPEVAR: {
                Element e = this.element.resolve(info);
                if (!(e instanceof Symbol.TypeSymbol)) {
                    return null;
                }
                TypeMirrorHandle<? extends TypeMirror> lBound = this.typeMirrors.get(0);
                TypeMirror lowerBound = lBound != null ? TypeMirrorHandle.super.resolve(info, map) : null;
                TypeMirrorHandle<? extends TypeMirror> uBound = this.typeMirrors.get(1);
                TypeMirror upperBound = uBound != null ? TypeMirrorHandle.super.resolve(info, map) : null;
                Type.TypeVar tv = new Type.TypeVar((Symbol.TypeSymbol)e, (Type)upperBound, (Type)lowerBound);
                PlaceholderType pt = map.get(this);
                if (pt != null) {
                    pt.delegate = (Type)tv;
                    new Visitor().visitTypeVar(tv, null);
                }
                return (T)tv;
            }
            case WILDCARD: {
                TypeMirrorHandle<? extends TypeMirror> eBound = this.typeMirrors.get(0);
                TypeMirror extendsBound = eBound != null ? TypeMirrorHandle.super.resolve(info, map) : null;
                TypeMirrorHandle<? extends TypeMirror> sBound = this.typeMirrors.get(1);
                TypeMirror superBound = sBound != null ? TypeMirrorHandle.super.resolve(info, map) : null;
                WildcardType wt = info.getTypes().getWildcardType(extendsBound, superBound);
                PlaceholderType pt = map.get(this);
                if (pt != null) {
                    pt.delegate = (Type)wt;
                    new Visitor().visitWildcardType((Type.WildcardType)wt, null);
                }
                return (T)wt;
            }
            case ERROR: {
                Element e = this.element.resolve(info);
                if (e == null) {
                    String[] signatures = this.element.getSignature();
                    assert (signatures.length == 1);
                    Context context = info.impl.getJavacTask().getContext();
                    return (T)new Type.ErrorType(Names.instance((Context)context).table.fromString(signatures[0]), (Symbol)Symtab.instance((Context)context).rootPackage, (Type)Type.noType);
                }
                if (!(e instanceof Symbol.ClassSymbol)) {
                    return null;
                }
                return (T)new Type.ErrorType((Symbol.ClassSymbol)e, (Type)Type.noType);
            }
            case UNION: {
                List resolvedAlternatives = List.nil();
                for (TypeMirrorHandle<? extends TypeMirror> alternative : this.typeMirrors) {
                    TypeMirror resolvedAlternative = TypeMirrorHandle.super.resolve(info, map);
                    if (resolvedAlternative == null) {
                        return null;
                    }
                    resolvedAlternatives = resolvedAlternatives.prepend((Object)((Type)resolvedAlternative));
                }
                com.sun.tools.javac.code.Types t = com.sun.tools.javac.code.Types.instance((Context)info.impl.getJavacTask().getContext());
                Type lub = t.lub(resolvedAlternatives);
                if (!lub.hasTag(TypeTag.CLASS)) {
                    return null;
                }
                return (T)new Type.UnionClassType((Type.ClassType)lub, resolvedAlternatives);
            }
            case INTERSECTION: {
                ListBuffer resolvedBounds = new ListBuffer();
                for (TypeMirrorHandle<? extends TypeMirror> alternative : this.typeMirrors) {
                    TypeMirror resolvedAlternative = TypeMirrorHandle.super.resolve(info, map);
                    if (resolvedAlternative == null) {
                        return null;
                    }
                    resolvedBounds = resolvedBounds.append((Object)((Type)resolvedAlternative));
                }
                com.sun.tools.javac.code.Types t = com.sun.tools.javac.code.Types.instance((Context)info.impl.getJavacTask().getContext());
                return (T)t.makeCompoundType(resolvedBounds.toList());
            }
        }
        throw new IllegalStateException("Internal error: unknown TypeHandle kind: " + (Object)((Object)this.kind));
    }

    @NonNull
    public TypeKind getKind() {
        return this.kind;
    }

    ElementHandle<? extends Element> getElementHandle() {
        return this.element;
    }

    private static class Visitor
    extends Types.DefaultTypeVisitor<Void, Void> {
        private Visitor() {
        }

        public Void visitType(Type t, Void s) {
            return null;
        }

        public Void visitArrayType(Type.ArrayType t, Void s) {
            if (t.elemtype != null) {
                t.elemtype.accept((Type.Visitor)this, (Object)s);
            }
            return null;
        }

        public Void visitClassType(Type.ClassType t, Void s) {
            List l;
            if (t.supertype_field != null) {
                t.supertype_field.accept((Type.Visitor)this, (Object)s);
            }
            if (t.interfaces_field != null) {
                l = t.interfaces_field;
                while (l.nonEmpty()) {
                    ((Type)l.head).accept((Type.Visitor)this, (Object)s);
                    l = l.tail;
                }
            }
            if (t.typarams_field != null) {
                l = t.typarams_field;
                while (l.nonEmpty()) {
                    ((Type)l.head).accept((Type.Visitor)this, (Object)s);
                    l = l.tail;
                }
            }
            return null;
        }

        public Void visitTypeVar(Type.TypeVar t, Void s) {
            if (t.bound instanceof PlaceholderType) {
                t.bound = ((PlaceholderType)t.bound).delegate;
            } else if (t.bound != null) {
                t.bound.accept((Type.Visitor)this, (Object)s);
            }
            if (t.lower instanceof PlaceholderType) {
                t.lower = ((PlaceholderType)t.lower).delegate;
            } else if (t.lower != null) {
                t.lower.accept((Type.Visitor)this, (Object)s);
            }
            return null;
        }

        public Void visitWildcardType(Type.WildcardType t, Void s) {
            if (t.type instanceof PlaceholderType) {
                t.type = ((PlaceholderType)t.type).delegate;
            } else if (t.type != null) {
                t.type.accept((Type.Visitor)this, (Object)s);
            }
            if (t.bound != null) {
                t.bound.accept((Type.Visitor)this, (Object)s);
            }
            return null;
        }
    }

    private static class PlaceholderType
    extends Type
    implements ReferenceType {
        private Type delegate = null;

        public PlaceholderType() {
            super(null);
        }

        public TypeTag getTag() {
            return TypeTag.UNKNOWN;
        }
    }

}

