/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.java.source;

import java.net.URL;
import java.util.EventObject;
import org.netbeans.api.java.source.ClassIndex;

public final class RootsEvent
extends EventObject {
    private final Iterable<? extends URL> roots;

    RootsEvent(ClassIndex source, Iterable<? extends URL> roots) {
        super(source);
        assert (roots != null);
        this.roots = roots;
    }

    public Iterable<? extends URL> getRoots() {
        return this.roots;
    }

    @Override
    public String toString() {
        return String.format("RootsEvent [%s]", this.roots.toString());
    }
}

