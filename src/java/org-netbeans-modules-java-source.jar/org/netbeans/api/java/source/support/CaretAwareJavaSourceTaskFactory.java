/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.parsing.spi.TaskIndexingMode
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.api.java.source.support;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.JTextComponent;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.JavaSourceTaskFactory;
import org.netbeans.api.java.source.support.OpenedEditors;
import org.netbeans.modules.parsing.spi.TaskIndexingMode;
import org.openide.filesystems.FileObject;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;

public abstract class CaretAwareJavaSourceTaskFactory
extends JavaSourceTaskFactory {
    private static final int DEFAULT_RESCHEDULE_TIMEOUT = 300;
    private static final RequestProcessor WORKER = new RequestProcessor("CaretAwareJavaSourceTaskFactory worker");
    private int timeout;
    private String[] supportedMimeTypes;
    private Map<JTextComponent, ComponentListener> component2Listener = new WeakHashMap<JTextComponent, ComponentListener>();
    private static Map<FileObject, Integer> file2LastPosition = new WeakHashMap<FileObject, Integer>();

    public CaretAwareJavaSourceTaskFactory(JavaSource.Phase phase, JavaSource.Priority priority) {
        this(phase, priority, (String[])null);
    }

    public /* varargs */ CaretAwareJavaSourceTaskFactory(JavaSource.Phase phase, JavaSource.Priority priority, String ... supportedMimeTypes) {
        super(phase, priority, TaskIndexingMode.DISALLOWED_DURING_SCAN);
        OpenedEditors.getDefault().addChangeListener(new ChangeListenerImpl());
        this.timeout = 300;
        this.supportedMimeTypes = supportedMimeTypes != null ? (String[])supportedMimeTypes.clone() : null;
    }

    public /* varargs */ CaretAwareJavaSourceTaskFactory(@NonNull JavaSource.Phase phase, @NonNull JavaSource.Priority priority, @NonNull TaskIndexingMode taskIndexingMode, @NonNull String ... supportedMimeTypes) {
        super(phase, priority, taskIndexingMode);
        Parameters.notNull((CharSequence)"supportedMimeTypes", (Object)supportedMimeTypes);
        OpenedEditors.getDefault().addChangeListener(new ChangeListenerImpl());
        this.timeout = 300;
        this.supportedMimeTypes = supportedMimeTypes.length > 0 ? (String[])supportedMimeTypes.clone() : null;
    }

    public List<FileObject> getFileObjects() {
        List<FileObject> files = OpenedEditors.filterSupportedMIMETypes(OpenedEditors.getDefault().getVisibleEditorsFiles(), this.supportedMimeTypes);
        return files;
    }

    public static synchronized int getLastPosition(FileObject file) {
        if (file == null) {
            throw new NullPointerException("Cannot pass null file!");
        }
        Integer position = file2LastPosition.get((Object)file);
        if (position == null) {
            return 0;
        }
        return position;
    }

    static synchronized void setLastPosition(FileObject file, int position) {
        file2LastPosition.put(file, position);
    }

    private class ComponentListener
    implements CaretListener,
    Runnable {
        private final Reference<JTextComponent> component;
        private final RequestProcessor.Task rescheduleTask;

        public ComponentListener(JTextComponent component) {
            this.component = new WeakReference<JTextComponent>(component);
            this.rescheduleTask = WORKER.create((Runnable)this);
        }

        @Override
        public void run() {
            JTextComponent c = this.component.get();
            if (c == null) {
                return;
            }
            FileObject file = OpenedEditors.getFileObject(c);
            if (file != null) {
                CaretAwareJavaSourceTaskFactory.this.reschedule(file);
            }
        }

        @Override
        public void caretUpdate(CaretEvent e) {
            JTextComponent c = this.component.get();
            if (c == null) {
                return;
            }
            FileObject file = OpenedEditors.getFileObject(c);
            if (file != null) {
                CaretAwareJavaSourceTaskFactory.setLastPosition(file, c.getCaretPosition());
                this.rescheduleTask.schedule(CaretAwareJavaSourceTaskFactory.this.timeout);
            }
        }
    }

    private class ChangeListenerImpl
    implements ChangeListener {
        private ChangeListenerImpl() {
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            ArrayList<JTextComponent> added = new ArrayList<JTextComponent>(OpenedEditors.getDefault().getVisibleEditors());
            ArrayList removed = new ArrayList(CaretAwareJavaSourceTaskFactory.this.component2Listener.keySet());
            added.removeAll(CaretAwareJavaSourceTaskFactory.this.component2Listener.keySet());
            removed.removeAll(OpenedEditors.getDefault().getVisibleEditors());
            for (JTextComponent c2 : removed) {
                c2.removeCaretListener((CaretListener)CaretAwareJavaSourceTaskFactory.this.component2Listener.remove(c2));
            }
            for (JTextComponent c2 : added) {
                ComponentListener l = new ComponentListener(c2);
                c2.addCaretListener(l);
                CaretAwareJavaSourceTaskFactory.this.component2Listener.put(c2, l);
                CaretAwareJavaSourceTaskFactory.setLastPosition(OpenedEditors.getFileObject(c2), c2.getCaretPosition());
            }
            CaretAwareJavaSourceTaskFactory.this.fileObjectsChanged();
        }
    }

}

