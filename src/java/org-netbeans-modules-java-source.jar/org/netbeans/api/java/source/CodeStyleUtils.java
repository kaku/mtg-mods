/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 */
package org.netbeans.api.java.source;

import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.source.CodeStyle;
import org.netbeans.lib.editor.util.CharSequenceUtilities;

public final class CodeStyleUtils {
    private CodeStyleUtils() {
    }

    @NonNull
    public static String addPrefixSuffix(@NullAllowed CharSequence name, @NullAllowed String prefix, @NullAllowed String suffix) {
        StringBuilder sb = new StringBuilder();
        boolean capitalize = false;
        if (prefix != null && prefix.length() > 0) {
            if (Character.isAlphabetic(prefix.charAt(prefix.length() - 1))) {
                capitalize = true;
            }
            sb.insert(0, prefix);
        }
        if (name != null) {
            sb.append((CharSequence)(capitalize ? CodeStyleUtils.getCapitalizedName(name) : name));
        }
        if (suffix != null) {
            sb.append(suffix);
        }
        return sb.toString();
    }

    @NonNull
    public static String removePrefixSuffix(@NonNull CharSequence name, @NullAllowed String prefix, @NullAllowed String suffix) {
        StringBuilder sb = new StringBuilder(name);
        int start = 0;
        int end = name.length();
        boolean decapitalize = false;
        if (prefix != null && prefix.length() > 0 && CharSequenceUtilities.startsWith((CharSequence)name, (CharSequence)prefix)) {
            start = prefix.length();
            if (Character.isAlphabetic(prefix.charAt(prefix.length() - 1))) {
                decapitalize = true;
            }
        }
        if (suffix != null && CharSequenceUtilities.endsWith((CharSequence)name, (CharSequence)suffix)) {
            end -= suffix.length();
        }
        String result = sb.substring(start, end);
        if (decapitalize) {
            return CodeStyleUtils.getDecapitalizedName(result);
        }
        return result;
    }

    @NonNull
    public static String getCapitalizedName(CharSequence name) {
        StringBuilder sb = new StringBuilder(name);
        while (sb.length() > 1 && sb.charAt(0) == '_') {
            sb.deleteCharAt(0);
        }
        if (sb.length() > 1 && Character.isUpperCase(sb.charAt(1))) {
            return sb.toString();
        }
        if (sb.length() > 0) {
            sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
        }
        return sb.toString();
    }

    @NonNull
    public static String getDecapitalizedName(@NonNull CharSequence name) {
        if (name.length() > 1 && (Character.isUpperCase(name.charAt(1)) || Character.isLowerCase(name.charAt(0)))) {
            return name.toString();
        }
        StringBuilder sb = new StringBuilder(name);
        if (sb.length() > 0) {
            sb.setCharAt(0, Character.toLowerCase(sb.charAt(0)));
        }
        return sb.toString();
    }

    @NonNull
    public static String computeGetterName(CharSequence fieldName, boolean isBoolean, boolean isStatic, CodeStyle cs) {
        StringBuilder sb = new StringBuilder(CodeStyleUtils.getCapitalizedName(CodeStyleUtils.removeFieldPrefixSuffix(fieldName, isStatic, cs)));
        sb.insert(0, isBoolean ? "is" : "get");
        String getterName = sb.toString();
        return getterName;
    }

    @NonNull
    public static String computeSetterName(CharSequence fieldName, boolean isStatic, CodeStyle cs) {
        StringBuilder name = new StringBuilder(CodeStyleUtils.getCapitalizedName(CodeStyleUtils.removeFieldPrefixSuffix(fieldName, isStatic, cs)));
        name.insert(0, "set");
        return name.toString();
    }

    private static String removeFieldPrefixSuffix(CharSequence fieldName, boolean isStatic, CodeStyle cs) {
        return CodeStyleUtils.removePrefixSuffix(fieldName, isStatic ? cs.getStaticFieldNamePrefix() : cs.getFieldNamePrefix(), isStatic ? cs.getStaticFieldNameSuffix() : cs.getFieldNameSuffix());
    }
}

