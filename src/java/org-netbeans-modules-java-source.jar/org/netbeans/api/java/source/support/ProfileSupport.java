/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.queries.SourceLevelQuery
 *  org.netbeans.api.java.queries.SourceLevelQuery$Profile
 *  org.netbeans.modules.classfile.Annotation
 *  org.netbeans.modules.classfile.AnnotationComponent
 *  org.netbeans.modules.classfile.CPEntry
 *  org.netbeans.modules.classfile.ClassFile
 *  org.netbeans.modules.classfile.ClassName
 *  org.netbeans.modules.classfile.ElementValue
 *  org.netbeans.modules.classfile.PrimitiveElementValue
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.Union2
 *  org.openide.util.Utilities
 */
package org.netbeans.api.java.source.support;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executor;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.queries.SourceLevelQuery;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.modules.classfile.Annotation;
import org.netbeans.modules.classfile.AnnotationComponent;
import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.ClassFile;
import org.netbeans.modules.classfile.ClassName;
import org.netbeans.modules.classfile.ElementValue;
import org.netbeans.modules.classfile.PrimitiveElementValue;
import org.netbeans.modules.java.source.ElementHandleAccessor;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.parsing.Archive;
import org.netbeans.modules.java.source.parsing.CachingArchiveProvider;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;
import org.openide.util.Union2;
import org.openide.util.Utilities;

public class ProfileSupport {
    private static final String RES_MANIFEST = "META-INF/MANIFEST.MF";
    private static final String ATTR_PROFILE = "Profile";
    private static final String ANNOTATION_PROFILE = "jdk/Profile+Annotation";
    private static final String ANNOTATION_VALUE = "value";
    private static final Logger LOG = Logger.getLogger(ProfileSupport.class.getName());
    private static final RequestProcessor RP = new RequestProcessor(ProfileSupport.class);

    private ProfileSupport() {
    }

    public static void findProfileViolations(@NonNull SourceLevelQuery.Profile profileToCheck, @NonNull Iterable<URL> bootClassPath, @NonNull Iterable<URL> compileClassPath, @NonNull Iterable<URL> sourcePath, @NonNull Set<Validation> check, @NonNull ViolationCollectorFactory collectorFactory) {
        ProfileSupport.findProfileViolations(profileToCheck, bootClassPath, compileClassPath, sourcePath, check, collectorFactory, (Executor)RP);
    }

    @NonNull
    public static Collection<Violation> findProfileViolations(@NonNull SourceLevelQuery.Profile profileToCheck, @NonNull Iterable<URL> bootClassPath, @NonNull Iterable<URL> compileClassPath, @NonNull Iterable<URL> sourcePath, @NonNull Set<Validation> check) {
        DefaultProfileViolationCollector collector = new DefaultProfileViolationCollector();
        ProfileSupport.findProfileViolations(profileToCheck, bootClassPath, compileClassPath, sourcePath, check, collector, new CurrentThreadExecutor());
        return collector.getViolations();
    }

    public static void findProfileViolations(@NonNull SourceLevelQuery.Profile profileToCheck, @NonNull Iterable<URL> bootClassPath, @NonNull Iterable<URL> compileClassPath, @NonNull Iterable<URL> sourcePath, @NonNull Set<Validation> check, @NonNull ViolationCollectorFactory collectorFactory, @NonNull Executor executor) {
        Parameters.notNull((CharSequence)"profileToCheck", (Object)profileToCheck);
        Parameters.notNull((CharSequence)"compileClassPath", compileClassPath);
        Parameters.notNull((CharSequence)"sourcePath", sourcePath);
        Parameters.notNull((CharSequence)"check", check);
        Parameters.notNull((CharSequence)"collectorFactory", (Object)collectorFactory);
        Parameters.notNull((CharSequence)"executor", (Object)executor);
        Context ctx = new Context(profileToCheck, bootClassPath, collectorFactory, check);
        if (check.contains((Object)Validation.BINARIES_BY_MANIFEST) || check.contains((Object)Validation.BINARIES_BY_CLASS_FILES)) {
            for (URL compileRoot : compileClassPath) {
                executor.execute(Validator.forBinary(compileRoot, ctx));
            }
        }
        if (check.contains((Object)Validation.SOURCES)) {
            for (URL sourceRoot : sourcePath) {
                executor.execute(Validator.forSource(sourceRoot, ctx));
            }
        }
    }

    private static final class TypeCache {
        private final Object UNKNOWN = new Object();
        private final ConcurrentMap<String, Object> cache;
        private final Archive ctSym;

        private TypeCache(@NonNull Archive ctSym) {
            assert (ctSym != null);
            this.ctSym = ctSym;
            this.cache = new ConcurrentHashMap<String, Object>();
        }

        @NonNull
        static TypeCache newInstance(Iterable<? extends URL> bootClassPath) {
            Archive ctSym = null;
            CachingArchiveProvider ap = CachingArchiveProvider.getDefault();
            for (URL root : bootClassPath) {
                if (!ap.hasCtSym(root)) continue;
                ctSym = ap.getArchive(root, true);
                break;
            }
            if (ctSym == null) {
                throw new IllegalArgumentException(String.format("No profile info for boot classpath: %s", bootClassPath));
            }
            return new TypeCache(ctSym);
        }

        @CheckForNull
        SourceLevelQuery.Profile profileForType(@NonNull ClassName className) {
            String binName = className.getInternalName();
            Object res = this.cache.get(binName);
            if (res == null) {
                res = this.findProfile(binName);
                this.cache.put(binName, res);
            }
            return res == this.UNKNOWN ? null : (SourceLevelQuery.Profile)res;
        }

        @NonNull
        private Object findProfile(@NonNull String binaryName) {
            Object res;
            block15 : {
                res = this.UNKNOWN;
                StringBuilder sb = new StringBuilder(binaryName);
                sb.append('.');
                sb.append("class");
                try {
                    JavaFileObject jfo = this.ctSym.getFile(sb.toString());
                    if (jfo == null) break block15;
                    InputStream in = jfo.openInputStream();
                    Throwable throwable = null;
                    try {
                        ClassFile cf = new ClassFile(in);
                        Annotation a = cf.getAnnotation(ClassName.getClassName((String)"jdk/Profile+Annotation"));
                        if (a == null) {
                            res = SourceLevelQuery.Profile.COMPACT1;
                            break block15;
                        }
                        AnnotationComponent ac = a.getComponent("value");
                        res = TypeCache.profileFromAnnotationComponent(ac);
                    }
                    catch (Throwable x2) {
                        throwable = x2;
                        throw x2;
                    }
                    finally {
                        if (in != null) {
                            if (throwable != null) {
                                try {
                                    in.close();
                                }
                                catch (Throwable x2) {
                                    throwable.addSuppressed(x2);
                                }
                            } else {
                                in.close();
                            }
                        }
                    }
                }
                catch (IOException ioe) {
                    Exceptions.printStackTrace((Throwable)ioe);
                }
            }
            return res;
        }

        @NonNull
        private static SourceLevelQuery.Profile profileFromAnnotationComponent(@NullAllowed AnnotationComponent ac) {
            if (ac == null) {
                return SourceLevelQuery.Profile.COMPACT1;
            }
            try {
                ElementValue ev = ac.getValue();
                if (!(ev instanceof PrimitiveElementValue)) {
                    return SourceLevelQuery.Profile.COMPACT1;
                }
                CPEntry cpEntry = ((PrimitiveElementValue)ev).getValue();
                if (cpEntry.getTag() != 3) {
                    return SourceLevelQuery.Profile.COMPACT1;
                }
                int ordinal = (Integer)cpEntry.getValue();
                if (ordinal <= 0) {
                    return SourceLevelQuery.Profile.COMPACT1;
                }
                SourceLevelQuery.Profile[] values = SourceLevelQuery.Profile.values();
                if (ordinal >= values.length) {
                    return SourceLevelQuery.Profile.DEFAULT;
                }
                return values[ordinal - 1];
            }
            catch (NumberFormatException nfe) {
                return SourceLevelQuery.Profile.COMPACT1;
            }
        }
    }

    private static final class ArchiveCache {
        private static final int MAX_CACHE_SIZE = Integer.getInteger("ProfileSupport.ArchiveCache.size", 1024);
        private static volatile ArchiveCache instance;
        private final Map<Key, Union2<SourceLevelQuery.Profile, String>> cache;

        private ArchiveCache() {
            this.cache = Collections.synchronizedMap(new LinkedHashMap<Key, Union2<SourceLevelQuery.Profile, String>>(16, 0.75f, true){

                @Override
                protected boolean removeEldestEntry(Map.Entry<Key, Union2<SourceLevelQuery.Profile, String>> entry) {
                    return this.size() > MAX_CACHE_SIZE;
                }
            });
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         * Enabled force condition propagation
         * Lifted jumps to return sites
         */
        @NonNull
        static ArchiveCache getInstance() {
            ArchiveCache cache = instance;
            if (cache != null) return cache;
            Class<ArchiveCache> class_ = ArchiveCache.class;
            synchronized (ArchiveCache.class) {
                cache = instance;
                if (cache != null) return cache;
                {
                    instance = cache = new ArchiveCache();
                }
                // ** MonitorExit[var1_1] (shouldn't be in output)
                return cache;
            }
        }

        @CheckForNull
        Union2<SourceLevelQuery.Profile, String> getProfile(@NonNull Key key) {
            Union2<SourceLevelQuery.Profile, String> res = this.cache.get(key);
            if (LOG.isLoggable(Level.FINER)) {
                Object[] arrobject = new Object[2];
                arrobject[0] = key;
                arrobject[1] = res.hasFirst() ? (Serializable)res.first() : (Serializable)res.second();
                LOG.log(Level.FINER, "cache[{0}]->{1}", arrobject);
            }
            return res;
        }

        void putProfile(@NonNull Key key, @NonNull Union2<SourceLevelQuery.Profile, String> profile) {
            if (LOG.isLoggable(Level.FINER)) {
                Object[] arrobject = new Object[2];
                arrobject[0] = key;
                arrobject[1] = profile.hasFirst() ? (Serializable)profile.first() : (Serializable)profile.second();
                LOG.log(Level.FINER, "cache[{0}]<-{1}", arrobject);
            }
            this.cache.put(key, profile);
        }

        @CheckForNull
        Key createKey(@NonNull URL rootURL) {
            URL fileURL = FileUtil.getArchiveFile((URL)rootURL);
            if (fileURL == null) {
                return null;
            }
            FileObject fileFo = URLMapper.findFileObject((URL)fileURL);
            if (fileFo == null) {
                return null;
            }
            return new Key(fileFo.toURI(), fileFo.lastModified().getTime(), fileFo.getSize());
        }

        private static final class Key {
            private final URI root;
            private final long mtime;
            private final long size;

            Key(@NonNull URI root, long mtime, long size) {
                this.root = root;
                this.mtime = mtime;
                this.size = size;
            }

            public int hashCode() {
                int hash = 17;
                hash = 31 * hash + (this.root != null ? this.root.hashCode() : 0);
                hash = 31 * hash + (int)(this.mtime ^ this.mtime >>> 32);
                hash = 31 * hash + (int)(this.size ^ this.size >>> 32);
                return hash;
            }

            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof Key)) {
                    return false;
                }
                Key other = (Key)obj;
                return this.root.equals(other.root) && this.mtime == other.mtime && this.size == other.size;
            }

            public String toString() {
                return String.format("Key{root: %s, mtime: %d, size: %d}", this.root, this.mtime, this.size);
            }
        }

    }

    private static class DefaultProfileViolationCollector
    implements ViolationCollectorFactory,
    ViolationCollector {
        private final Queue<Violation> violations = new ArrayDeque<Violation>();

        private DefaultProfileViolationCollector() {
        }

        @Override
        public ViolationCollector create(@NonNull URL root) {
            return this;
        }

        @Override
        public boolean isCancelled() {
            return false;
        }

        @Override
        public void reportProfileViolation(@NonNull Violation violation) {
            this.violations.offer(violation);
        }

        @Override
        public void finished() {
        }

        Collection<Violation> getViolations() {
            return Collections.unmodifiableCollection(this.violations);
        }
    }

    private static class CurrentThreadExecutor
    implements Executor {
        private CurrentThreadExecutor() {
        }

        @Override
        public void execute(Runnable command) {
            command.run();
        }
    }

    private static abstract class Validator
    implements Runnable {
        protected final Context context;
        protected final URL root;

        Validator(@NonNull URL root, @NonNull Context context) {
            assert (root != null);
            assert (context != null);
            this.root = root;
            this.context = context;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public final void run() {
            ViolationCollector collector = this.context.newCollector(this.root);
            assert (collector != null);
            try {
                this.validate(collector);
            }
            finally {
                collector.finished();
            }
        }

        protected final void validateBinaryRoot(@NonNull URL root, @NonNull ViolationCollector collector) {
            FileObject rootFo = URLMapper.findFileObject((URL)root);
            if (rootFo == null) {
                return;
            }
            Enumeration children = rootFo.getChildren(true);
            while (children.hasMoreElements() && !this.context.isCancelled()) {
                FileObject fo = (FileObject)children.nextElement();
                if (!this.isImportant(fo)) continue;
                this.validateBinaryFile(fo, collector);
            }
        }

        @CheckForNull
        protected URL map(@NonNull FileObject fo) {
            return fo.toURL();
        }

        protected abstract void validate(@NonNull ViolationCollector var1);

        private boolean isImportant(@NonNull FileObject file) {
            return file.isData() && ("class".equals(file.getExt()) || "sig".equals(file.getExt()));
        }

        private void validateBinaryFile(@NonNull FileObject fo, @NonNull ViolationCollector collector) {
            SourceLevelQuery.Profile profileToCheck = this.context.getRequredProfile();
            TypeCache tc = this.context.getTypeCache();
            try {
                InputStream in = fo.getInputStream();
                Throwable throwable = null;
                try {
                    ClassFile cf = new ClassFile(in);
                    for (ClassName className : cf.getAllClassNames()) {
                        SourceLevelQuery.Profile p = tc.profileForType(className);
                        if (p == null || profileToCheck.compareTo((Enum)p) >= 0) continue;
                        collector.reportProfileViolation(new Violation(this.root, p, this.map(fo), ElementHandleAccessor.getInstance().create(ElementKind.CLASS, className.getInternalName().replace('/', '.'))));
                    }
                }
                catch (Throwable x2) {
                    throwable = x2;
                    throw x2;
                }
                finally {
                    if (in != null) {
                        if (throwable != null) {
                            try {
                                in.close();
                            }
                            catch (Throwable x2) {
                                throwable.addSuppressed(x2);
                            }
                        } else {
                            in.close();
                        }
                    }
                }
            }
            catch (IOException ioe) {
                LOG.log(Level.INFO, "Cannot validate file: {0}", FileUtil.getFileDisplayName((FileObject)fo));
            }
        }

        static Validator forSource(@NonNull URL root, @NonNull Context context) {
            return new SourceValidator(root, context);
        }

        static Validator forBinary(@NonNull URL root, @NonNull Context context) {
            return new BinaryValidator(root, context);
        }

        private static final class SourceValidator
        extends Validator {
            private final File cacheRoot;
            private final ClasspathInfo resolveCps;

            private SourceValidator(@NonNull URL root, @NonNull Context context) {
                File f;
                super(root, context);
                try {
                    f = JavaIndex.getClassFolder(root, true);
                }
                catch (IOException ioe) {
                    f = null;
                }
                this.cacheRoot = f;
                this.resolveCps = ClasspathInfo.create(ClassPath.EMPTY, ClassPath.EMPTY, ClassPathSupport.createClassPath((URL[])new URL[]{root}));
            }

            @Override
            protected void validate(@NonNull ViolationCollector collector) {
                if (this.context.isCancelled()) {
                    return;
                }
                try {
                    if (this.cacheRoot != null) {
                        this.validateBinaryRoot(Utilities.toURI((File)this.cacheRoot).toURL(), collector);
                    }
                }
                catch (MalformedURLException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }

            @CheckForNull
            @Override
            protected URL map(@NonNull FileObject fo) {
                String relative = FileObjects.convertFolder2Package(FileObjects.stripExtension(FileObjects.getRelativePath(this.cacheRoot, FileUtil.toFile((FileObject)fo))));
                FileObject sourceFile = SourceUtils.getFile(ElementHandleAccessor.getInstance().create(ElementKind.CLASS, relative), this.resolveCps);
                return sourceFile == null ? null : sourceFile.toURL();
            }
        }

        private static final class BinaryValidator
        extends Validator {
            private BinaryValidator(@NonNull URL root, @NonNull Context context) {
                super(root, context);
            }

            @Override
            protected void validate(@NonNull ViolationCollector collector) {
                if (this.context.isCancelled()) {
                    return;
                }
                SourceLevelQuery.Profile current = null;
                if (this.context.shouldValidate(Validation.BINARIES_BY_MANIFEST)) {
                    Union2<SourceLevelQuery.Profile, String> res = this.findProfileInManifest(this.root);
                    if (!res.hasFirst()) {
                        collector.reportProfileViolation(new Violation(this.root, null, null, null));
                        return;
                    }
                    current = (SourceLevelQuery.Profile)res.first();
                    if (current != SourceLevelQuery.Profile.DEFAULT && current.compareTo((Enum)this.context.getRequredProfile()) > 0) {
                        collector.reportProfileViolation(new Violation(this.root, (SourceLevelQuery.Profile)res.first(), null, null));
                        return;
                    }
                }
                if (this.context.shouldValidate(Validation.BINARIES_BY_CLASS_FILES) && (current == null || current == SourceLevelQuery.Profile.DEFAULT)) {
                    this.validateBinaryRoot(this.root, collector);
                }
            }

            @NonNull
            private Union2<SourceLevelQuery.Profile, String> findProfileInManifest(@NonNull URL root) {
                FileObject manifestFile;
                Union2 res;
                SourceLevelQuery.Profile profile;
                ArchiveCache ac = this.context.getArchiveCache();
                ArchiveCache.Key key = ac.createKey(root);
                if (key != null && (res = ac.getProfile(key)) != null) {
                    return res;
                }
                String profileName = null;
                FileObject rootFo = URLMapper.findFileObject((URL)root);
                if (rootFo != null && (manifestFile = rootFo.getFileObject("META-INF/MANIFEST.MF")) != null) {
                    try {
                        InputStream in = manifestFile.getInputStream();
                        Throwable throwable = null;
                        try {
                            Manifest manifest = new Manifest(in);
                            Attributes attrs = manifest.getMainAttributes();
                            profileName = attrs.getValue("Profile");
                        }
                        catch (Throwable x2) {
                            throwable = x2;
                            throw x2;
                        }
                        finally {
                            if (in != null) {
                                if (throwable != null) {
                                    try {
                                        in.close();
                                    }
                                    catch (Throwable x2) {
                                        throwable.addSuppressed(x2);
                                    }
                                } else {
                                    in.close();
                                }
                            }
                        }
                    }
                    catch (IOException ioe) {
                        LOG.log(Level.INFO, "Cannot read Profile attribute from: {0}", FileUtil.getFileDisplayName((FileObject)manifestFile));
                    }
                }
                Union2 union2 = res = (profile = SourceLevelQuery.Profile.forName((String)profileName)) != null ? Union2.createFirst((Object)profile) : Union2.createSecond((Object)profileName);
                if (key != null) {
                    ac.putProfile(key, res);
                }
                return res;
            }
        }

    }

    private static final class Context {
        private final ArchiveCache archiveCache;
        private final TypeCache typeCache;
        private final SourceLevelQuery.Profile profileToCheck;
        private final ViolationCollectorFactory factory;
        private final Set<Validation> validations;

        Context(@NonNull SourceLevelQuery.Profile profileToCheck, @NonNull Iterable<? extends URL> bootClassPath, @NonNull ViolationCollectorFactory factory, @NonNull Set<Validation> validations) {
            assert (profileToCheck != null);
            assert (bootClassPath != null);
            assert (factory != null);
            assert (validations != null);
            this.archiveCache = ArchiveCache.getInstance();
            this.typeCache = !bootClassPath.iterator().hasNext() && (validations.isEmpty() || validations.equals(EnumSet.of(Validation.BINARIES_BY_MANIFEST))) ? null : TypeCache.newInstance(bootClassPath);
            this.profileToCheck = profileToCheck;
            this.factory = factory;
            this.validations = EnumSet.copyOf(validations);
        }

        @NonNull
        ArchiveCache getArchiveCache() {
            return this.archiveCache;
        }

        @NonNull
        TypeCache getTypeCache() {
            if (this.typeCache == null) {
                throw new IllegalArgumentException("No type cache");
            }
            return this.typeCache;
        }

        @NonNull
        SourceLevelQuery.Profile getRequredProfile() {
            return this.profileToCheck;
        }

        @NonNull
        ViolationCollector newCollector(@NonNull URL root) {
            return this.factory.create(root);
        }

        boolean shouldValidate(@NonNull Validation validation) {
            return this.validations.contains((Object)validation);
        }

        boolean isCancelled() {
            return this.factory.isCancelled();
        }
    }

    public static interface ViolationCollectorFactory {
        @NonNull
        public ViolationCollector create(@NonNull URL var1);

        public boolean isCancelled();
    }

    public static interface ViolationCollector {
        public void reportProfileViolation(@NonNull Violation var1);

        public void finished();
    }

    public static class Violation {
        private final URL root;
        private final SourceLevelQuery.Profile profile;
        private final URL file;
        private final ElementHandle<TypeElement> type;

        private Violation(@NonNull URL root, @NullAllowed SourceLevelQuery.Profile profile, @NullAllowed URL file, @NullAllowed ElementHandle<TypeElement> type) {
            Parameters.notNull((CharSequence)"root", (Object)root);
            this.root = root;
            this.profile = profile;
            this.file = file;
            this.type = type;
        }

        @NonNull
        public URL getRoot() {
            return this.root;
        }

        @CheckForNull
        public SourceLevelQuery.Profile getRequiredProfile() {
            return this.profile;
        }

        @CheckForNull
        public URL getFile() {
            return this.file;
        }

        @CheckForNull
        public ElementHandle<TypeElement> getUsedType() {
            return this.type;
        }
    }

    public static enum Validation {
        SOURCES,
        BINARIES_BY_MANIFEST,
        BINARIES_BY_CLASS_FILES;
        

        private Validation() {
        }
    }

}

