/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.util.TreePath
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 */
package org.netbeans.api.java.source.matching;

import com.sun.source.util.TreePath;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import org.netbeans.api.annotations.common.NonNull;
import org.openide.util.Parameters;

public class Pattern {
    final Collection<? extends TreePath> pattern;
    final Map<String, TypeMirror> variable2Type;
    final Collection<? extends VariableElement> remappable;
    final boolean allowRemapToTrees;

    @NonNull
    public static Pattern createSimplePattern(@NonNull TreePath pattern) {
        return new Pattern(Arrays.asList(new TreePath[]{pattern}), null, null, false);
    }

    @NonNull
    public static Pattern createSimplePattern(@NonNull Iterable<? extends TreePath> pattern) {
        return new Pattern(Pattern.toCollection(pattern), null, null, false);
    }

    @NonNull
    public static Pattern createPatternWithFreeVariables(@NonNull TreePath pattern, @NonNull Map<String, TypeMirror> variable2Type) {
        return new Pattern(Collections.singletonList(pattern), variable2Type, null, false);
    }

    @NonNull
    public static Pattern createPatternWithFreeVariables(@NonNull Iterable<? extends TreePath> pattern, @NonNull Map<String, TypeMirror> variable2Type) {
        return new Pattern(Pattern.toCollection(pattern), variable2Type, null, false);
    }

    @NonNull
    public static Pattern createPatternWithRemappableVariables(@NonNull TreePath pattern, @NonNull Collection<? extends VariableElement> remappable, boolean allowRemapToTrees) {
        return new Pattern(Collections.singletonList(pattern), null, remappable, allowRemapToTrees);
    }

    @NonNull
    public static Pattern createPatternWithRemappableVariables(@NonNull Iterable<? extends TreePath> pattern, @NonNull Collection<? extends VariableElement> remappable, boolean allowRemapToTrees) {
        return new Pattern(Pattern.toCollection(pattern), null, remappable, allowRemapToTrees);
    }

    private static Collection<? extends TreePath> toCollection(Iterable<? extends TreePath> pattern) {
        ArrayList<TreePath> result = new ArrayList<TreePath>();
        for (TreePath tp : pattern) {
            Parameters.notNull((CharSequence)"pattern", (Object)tp);
            result.add(tp);
        }
        return result;
    }

    private Pattern(Collection<? extends TreePath> pattern, Map<String, TypeMirror> variable2Type, Collection<? extends VariableElement> remappable, boolean allowRemapToTrees) {
        this.pattern = pattern;
        this.variable2Type = variable2Type;
        this.remappable = remappable;
        this.allowRemapToTrees = allowRemapToTrees;
    }
}

