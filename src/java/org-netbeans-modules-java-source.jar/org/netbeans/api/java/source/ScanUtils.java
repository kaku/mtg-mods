/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.openide.util.Exceptions
 *  org.openide.util.Parameters
 */
package org.netbeans.api.java.source;

import java.io.IOException;
import java.util.Collections;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;
import javax.lang.model.element.Element;
import javax.swing.SwingUtilities;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.Task;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.openide.util.Exceptions;
import org.openide.util.Parameters;

public final class ScanUtils {
    private static final ThreadLocal<Boolean> retryGuard = new ThreadLocal();

    private ScanUtils() {
    }

    public static Future<Void> postUserActionTask(@NonNull JavaSource src, @NonNull Task<CompilationController> uat) {
        Parameters.notNull((CharSequence)"src", (Object)src);
        Parameters.notNull((CharSequence)"uat", uat);
        AtomicReference<Object> status = new AtomicReference<Object>(null);
        Future<Void> f = ScanUtils.postJavaTask(src, uat, status);
        Throwable t = status.get();
        if (t != null) {
            Exceptions.printStackTrace((Throwable)t);
        }
        return f;
    }

    public static Future<Void> postUserTask(@NonNull Source src, @NonNull UserTask task) {
        Parameters.notNull((CharSequence)"src", (Object)src);
        Parameters.notNull((CharSequence)"task", (Object)task);
        AtomicReference<Object> status = new AtomicReference<Object>(null);
        Future<Void> f = ScanUtils.postUserTask(src, task, status);
        Throwable t = status.get();
        if (t != null) {
            Exceptions.printStackTrace((Throwable)t);
        }
        return f;
    }

    public static void waitUserActionTask(@NonNull JavaSource src, @NonNull Task<CompilationController> uat) throws IOException {
        Parameters.notNull((CharSequence)"src", (Object)src);
        Parameters.notNull((CharSequence)"uat", uat);
        if (SwingUtilities.isEventDispatchThread()) {
            throw new IllegalStateException("Illegal to call within EDT");
        }
        AtomicReference<Object> status = new AtomicReference<Object>(null);
        Future<Void> f = ScanUtils.postJavaTask(src, uat, status);
        if (f.isDone()) {
            return;
        }
        try {
            f.get();
        }
        catch (InterruptedException ex) {
            IOException ioex = new IOException("Interrupted", ex);
            throw ioex;
        }
        catch (ExecutionException ex) {
            Throwable cause = ex.getCause();
            if (cause instanceof IOException) {
                throw (IOException)cause;
            }
            IOException ioex = new IOException("Failed", ex);
            throw ioex;
        }
        Throwable t = status.get();
        if (t != null) {
            if (t instanceof IOException) {
                throw (IOException)t;
            }
            IOException ioex = new IOException("Exception during processing", t);
            throw ioex;
        }
    }

    public static void waitUserTask(@NonNull Source src, @NonNull UserTask task) throws ParseException {
        Parameters.notNull((CharSequence)"src", (Object)src);
        Parameters.notNull((CharSequence)"task", (Object)task);
        if (SwingUtilities.isEventDispatchThread()) {
            throw new IllegalStateException("Illegal to call within EDT");
        }
        AtomicReference<Object> status = new AtomicReference<Object>(null);
        Future<Void> f = ScanUtils.postUserTask(src, task, status);
        if (f.isDone()) {
            Throwable t = status.get();
            if (t != null) {
                if (t instanceof ParseException) {
                    throw (ParseException)t;
                }
                throw new ParseException("User task failure", t);
            }
            return;
        }
        try {
            f.get();
        }
        catch (InterruptedException ex) {
            ParseException err = new ParseException("Interrupted", (Throwable)ex);
            throw err;
        }
        catch (ExecutionException ex) {
            Throwable cause = ex.getCause();
            if (cause instanceof ParseException) {
                throw (ParseException)cause;
            }
            ParseException ioex = new ParseException("User task failure", (Throwable)ex);
            throw ioex;
        }
        Throwable t = status.get();
        if (t != null) {
            if (t instanceof ParseException) {
                throw (ParseException)t;
            }
            ParseException err = new ParseException("User task failure", t);
            throw err;
        }
    }

    public static <T extends Element> T checkElement(@NonNull CompilationInfo info, @NullAllowed T e) {
        ScanUtils.checkRetryContext();
        if (e == null) {
            ScanUtils.signalIncompleteData(info, null);
            return e;
        }
        if (!info.getElementUtilities().isErroneous((Element)e)) {
            return e;
        }
        if (ScanUtils.shouldSignal()) {
            ScanUtils.signalIncompleteData(info, ElementHandle.create(e));
        }
        return e;
    }

    private static boolean shouldSignal() {
        return SourceUtils.isScanInProgress() && Boolean.TRUE.equals(retryGuard.get());
    }

    public static void signalIncompleteData(@NonNull CompilationInfo ci, @NullAllowed ElementHandle handle) {
        ScanUtils.checkRetryContext();
        if (ScanUtils.shouldSignal()) {
            throw new RetryWhenScanFinished(ci, handle);
        }
    }

    private static void checkRetryContext() throws IllegalStateException {
        Boolean b = retryGuard.get();
        if (b == null) {
            throw new IllegalStateException("The method may be only called within SourceUtils.waitUserActionTask");
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static Future<Void> postJavaTask(JavaSource javaSrc, Task<CompilationController> javaTask, AtomicReference<Throwable> status) {
        FinishedFuture handle;
        boolean retry = false;
        Boolean b = retryGuard.get();
        boolean mode = b == null || b != false;
        try {
            retryGuard.set(mode);
            javaSrc.runUserActionTask(javaTask, true);
        }
        catch (RuntimeException e) {
            status.set(e);
        }
        catch (IOException e) {
            status.set(e);
        }
        catch (RetryWhenScanFinished e) {
            retry = true;
        }
        finally {
            if (b == null) {
                retryGuard.remove();
            } else {
                retryGuard.set(b);
            }
        }
        if (!retry) {
            return new FinishedFuture();
        }
        TaskWrapper wrapper = new TaskWrapper(javaTask, status, mode);
        try {
            handle = javaSrc.runWhenScanFinished(wrapper, true);
        }
        catch (IOException ex) {
            status.set(ex);
            handle = new FinishedFuture();
        }
        return handle;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static Future<Void> postUserTask(Source src, UserTask task, AtomicReference<Throwable> status) {
        Future handle;
        boolean retry = false;
        Boolean b = retryGuard.get();
        boolean mode = b == null || b != false;
        try {
            retryGuard.set(mode);
            ParserManager.parse(Collections.singleton(src), (UserTask)task);
        }
        catch (ParseException ex) {
            status.set((Throwable)ex);
        }
        catch (RetryWhenScanFinished e) {
            retry = true;
        }
        finally {
            if (b == null) {
                retryGuard.remove();
            } else {
                retryGuard.set(b);
            }
        }
        if (!retry) {
            return new FinishedFuture();
        }
        TaskWrapper wrapper = new TaskWrapper(task, status, mode);
        try {
            handle = ParserManager.parseWhenScanFinished(Collections.singletonList(src), (UserTask)wrapper);
        }
        catch (ParseException ex) {
            status.set((Throwable)ex);
            handle = new FinishedFuture();
        }
        return handle;
    }

    static class RetryWhenScanFinished
    extends Error {
        private ElementHandle elHandle;
        private Source source;

        public RetryWhenScanFinished(CompilationInfo ci, ElementHandle elHandle) {
            if (ci != null && ci.getSnapshot() != null) {
                this.source = ci.getSnapshot().getSource();
            }
            this.elHandle = elHandle;
        }

        @Override
        public String toString() {
            return "RetryWhenScanFinished{elHandle=" + this.elHandle + ", source=" + (Object)this.source + '}';
        }
    }

    private static class TaskWrapper
    extends UserTask
    implements Task<CompilationController> {
        private Task<CompilationController> javaTask;
        private UserTask task;
        private AtomicReference<Throwable> status;
        private boolean mode;

        public TaskWrapper(UserTask task, AtomicReference<Throwable> status, boolean mode) {
            this.task = task;
            this.status = status;
            this.mode = mode;
        }

        public TaskWrapper(Task<CompilationController> javaTask, AtomicReference<Throwable> status, boolean mode) {
            this.javaTask = javaTask;
            this.status = status;
            this.mode = mode;
        }

        @Override
        public void run(ResultIterator resultIterator) throws Exception {
            Boolean b = (Boolean)retryGuard.get();
            try {
                retryGuard.set(this.mode);
                this.task.run(resultIterator);
            }
            catch (RetryWhenScanFinished ex) {
                this.status.set(ex);
            }
            catch (Exception ex) {
                this.status.set(ex);
                throw ex;
            }
            finally {
                if (b == null) {
                    retryGuard.remove();
                } else {
                    retryGuard.set(b);
                }
            }
        }

        @Override
        public void run(CompilationController parameter) throws Exception {
            Boolean b = (Boolean)retryGuard.get();
            try {
                retryGuard.set(this.mode);
                this.javaTask.run(parameter);
            }
            catch (RetryWhenScanFinished ex) {
                this.status.set(ex);
            }
            catch (Exception ex) {
                this.status.set(ex);
                throw ex;
            }
            finally {
                if (b == null) {
                    retryGuard.remove();
                } else {
                    retryGuard.set(b);
                }
            }
        }
    }

    private static final class FinishedFuture
    implements Future<Void> {
        private FinishedFuture() {
        }

        @Override
        public boolean cancel(boolean mayInterruptIfRunning) {
            return false;
        }

        @Override
        public Void get() throws InterruptedException, ExecutionException {
            return null;
        }

        @Override
        public Void get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            return null;
        }

        @Override
        public boolean isCancelled() {
            return false;
        }

        @Override
        public boolean isDone() {
            return true;
        }

        public String toString() {
            return "FinishedFuture";
        }
    }

}

