/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CaseTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreePathScanner
 *  com.sun.source.util.Trees
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.api.java.source;

import com.sun.source.tree.CaseTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreePathScanner;
import com.sun.source.util.Trees;
import com.sun.tools.javac.code.Symbol;
import java.util.HashMap;
import java.util.Map;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.tools.JavaFileObject;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.TreeMaker;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.WorkingCopy;

class TranslateIdentifier
extends TreePathScanner<Void, Void> {
    private final Map<Tree, Tree> translateMap = new HashMap<Tree, Tree>();
    @NonNull
    private final CompilationInfo info;
    @NonNull
    private final TreeMaker make;

    public static <T extends Tree> T importFQNs(WorkingCopy copy, T tree) {
        if (tree == null) {
            return null;
        }
        TranslateIdentifier ti = new TranslateIdentifier(copy);
        ti.scan(tree.getKind() == Tree.Kind.COMPILATION_UNIT ? new TreePath((CompilationUnitTree)tree) : new TreePath(new TreePath(copy.getCompilationUnit()), tree), (Object)null);
        return (T)copy.getTreeUtilities().translate((Tree)tree, (Map<? extends Tree, ? extends Tree>)ti.translateMap);
    }

    private TranslateIdentifier(@NonNull WorkingCopy copy) {
        this.info = copy;
        this.make = copy.getTreeMaker();
    }

    public Void visitIdentifier(IdentifierTree node, Void p) {
        TreePath path = this.getCurrentPath();
        Element element = this.info.getTrees().getElement(path);
        if (element != null && element.asType().getKind() != TypeKind.ERROR && (element.getKind().isClass() || element.getKind().isInterface() || element.getKind().isField() && ((Symbol)element).isStatic())) {
            Tree parent;
            Tree tree = parent = path.getParentPath() != null ? path.getParentPath().getLeaf() : null;
            if (parent != null && parent.getKind() == Tree.Kind.CASE && ((CaseTree)parent).getExpression() == node && element.getKind() == ElementKind.ENUM_CONSTANT || path.getCompilationUnit().getSourceFile() == ((Symbol)element).enclClass().sourcefile) {
                this.translateMap.put((Tree)node, (Tree)this.make.Identifier(element.getSimpleName()));
            } else {
                this.translateMap.put((Tree)node, (Tree)this.make.QualIdent(element));
            }
        }
        return null;
    }

    public Void visitMemberSelect(MemberSelectTree node, Void p) {
        TypeElement e = this.info.getElements().getTypeElement(node.toString());
        if (e != null) {
            this.translateMap.put((Tree)node, (Tree)this.make.QualIdent(e));
            return null;
        }
        return (Void)TreePathScanner.super.visitMemberSelect(node, (Object)p);
    }

    public Void visitMethod(MethodTree node, Void p) {
        if (this.info.getTreeUtilities().isSynthetic(this.getCurrentPath().getCompilationUnit(), (Tree)node)) {
            return null;
        }
        return (Void)TreePathScanner.super.visitMethod(node, (Object)p);
    }
}

