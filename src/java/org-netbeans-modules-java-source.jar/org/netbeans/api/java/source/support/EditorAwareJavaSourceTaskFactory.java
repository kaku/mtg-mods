/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.parsing.spi.TaskIndexingMode
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Parameters
 */
package org.netbeans.api.java.source.support;

import java.util.Collection;
import java.util.List;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.JavaSourceTaskFactory;
import org.netbeans.api.java.source.support.OpenedEditors;
import org.netbeans.modules.parsing.spi.TaskIndexingMode;
import org.openide.filesystems.FileObject;
import org.openide.util.Parameters;

public abstract class EditorAwareJavaSourceTaskFactory
extends JavaSourceTaskFactory {
    private String[] supportedMimeTypes;

    protected EditorAwareJavaSourceTaskFactory(JavaSource.Phase phase, JavaSource.Priority priority) {
        this(phase, priority, (String[])null);
    }

    protected /* varargs */ EditorAwareJavaSourceTaskFactory(JavaSource.Phase phase, JavaSource.Priority priority, String ... supportedMimeTypes) {
        super(phase, priority, TaskIndexingMode.DISALLOWED_DURING_SCAN);
        OpenedEditors.getDefault().addChangeListener(new ChangeListenerImpl());
        this.supportedMimeTypes = supportedMimeTypes != null ? (String[])supportedMimeTypes.clone() : null;
    }

    protected /* varargs */ EditorAwareJavaSourceTaskFactory(@NonNull JavaSource.Phase phase, @NonNull JavaSource.Priority priority, @NonNull TaskIndexingMode taskIndexingMode, @NonNull String ... supportedMimeTypes) {
        super(phase, priority, taskIndexingMode);
        Parameters.notNull((CharSequence)"supportedMimeTypes", (Object)supportedMimeTypes);
        OpenedEditors.getDefault().addChangeListener(new ChangeListenerImpl());
        this.supportedMimeTypes = supportedMimeTypes.length > 0 ? (String[])supportedMimeTypes.clone() : null;
    }

    public List<FileObject> getFileObjects() {
        List<FileObject> files = OpenedEditors.filterSupportedMIMETypes(OpenedEditors.getDefault().getVisibleEditorsFiles(), this.supportedMimeTypes);
        return files;
    }

    private class ChangeListenerImpl
    implements ChangeListener {
        private ChangeListenerImpl() {
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            EditorAwareJavaSourceTaskFactory.this.fileObjectsChanged();
        }
    }

}

