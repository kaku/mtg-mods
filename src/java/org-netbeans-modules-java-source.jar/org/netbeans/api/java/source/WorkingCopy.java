/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.doctree.DocCommentTree
 *  com.sun.source.doctree.DocTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.EnhancedForLoopTree
 *  com.sun.source.tree.ExpressionStatementTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.ForLoopTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.MethodInvocationTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.TreeVisitor
 *  com.sun.source.tree.TryTree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.DocTrees
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreePathScanner
 *  com.sun.source.util.TreeScanner
 *  com.sun.source.util.Trees
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCCompilationUnit
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.List
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.annotations.common.NullUnknown
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.Parameters
 *  org.openide.util.Utilities
 */
package org.netbeans.api.java.source;

import com.sun.source.doctree.DocCommentTree;
import com.sun.source.doctree.DocTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.EnhancedForLoopTree;
import com.sun.source.tree.ExpressionStatementTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.ForLoopTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TreeVisitor;
import com.sun.source.tree.TryTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.DocTrees;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreePathScanner;
import com.sun.source.util.TreeScanner;
import com.sun.source.util.Trees;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.lang.ref.Reference;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Name;
import javax.swing.text.BadLocationException;
import javax.tools.JavaFileObject;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.annotations.common.NullUnknown;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.source.Comment;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.GeneratorUtilities;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.api.java.source.PositionConverter;
import org.netbeans.api.java.source.TreeMaker;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.java.source.builder.CommentHandlerService;
import org.netbeans.modules.java.source.builder.CommentSetImpl;
import org.netbeans.modules.java.source.builder.TreeFactory;
import org.netbeans.modules.java.source.parsing.CompilationInfoImpl;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.JavacParserResult;
import org.netbeans.modules.java.source.pretty.ImportAnalysis2;
import org.netbeans.modules.java.source.query.CommentSet;
import org.netbeans.modules.java.source.save.CasualDiff;
import org.netbeans.modules.java.source.save.DiffContext;
import org.netbeans.modules.java.source.save.DiffUtilities;
import org.netbeans.modules.java.source.save.ElementOverlay;
import org.netbeans.modules.java.source.save.OverlayTemplateAttributesProvider;
import org.netbeans.modules.java.source.transform.ImmutableDocTreeTranslator;
import org.netbeans.modules.java.source.transform.ImmutableTreeTranslator;
import org.netbeans.modules.java.source.transform.TreeDuplicator;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.Parameters;
import org.openide.util.Utilities;

public class WorkingCopy
extends CompilationController {
    static Reference<WorkingCopy> instance;
    private Map<Tree, Tree> changes;
    private Map<Tree, Map<DocTree, DocTree>> docChanges;
    private Map<JavaFileObject, CompilationUnitTree> externalChanges;
    private java.util.List<CasualDiff.Diff> textualChanges;
    private Map<Integer, String> userInfo;
    private boolean afterCommit = false;
    private TreeMaker treeMaker;
    private Map<Tree, Object> tree2Tag;
    private final ElementOverlay overlay;
    private Map<Tree, Boolean> introducedTrees;
    private Map<Tree, Tree> rewriteHints = new HashMap<Tree, Tree>();
    static final Tree NOT_LINKED;
    private static boolean REWRITE_WHOLE_FILE;
    private Set<Comment> usedComments;

    WorkingCopy(CompilationInfoImpl impl, ElementOverlay overlay) {
        super(impl);
        this.overlay = overlay;
    }

    private synchronized void init() {
        if (this.changes != null) {
            return;
        }
        this.treeMaker = new TreeMaker(this, TreeFactory.instance(this.getContext()));
        this.changes = new IdentityHashMap<Tree, Tree>();
        this.docChanges = new IdentityHashMap<Tree, Map<DocTree, DocTree>>();
        this.tree2Tag = new IdentityHashMap<Tree, Object>();
        this.externalChanges = null;
        this.textualChanges = new ArrayList<CasualDiff.Diff>();
        this.userInfo = new HashMap<Integer, String>();
        this.introducedTrees = new IdentityHashMap<Tree, Boolean>();
        this.getContext().put(ElementOverlay.class, (Object)null);
        this.getContext().put(ElementOverlay.class, (Object)this.overlay);
    }

    private Context getContext() {
        return this.impl.getJavacTask().getContext();
    }

    @NullUnknown
    public static WorkingCopy get(@NonNull Parser.Result result) {
        WorkingCopy copy;
        JavacParserResult javacResult;
        CompilationController controller;
        Parameters.notNull((CharSequence)"result", (Object)result);
        WorkingCopy workingCopy = copy = instance != null ? instance.get() : null;
        if (copy != null && result instanceof JavacParserResult && (controller = (javacResult = (JavacParserResult)result).get(CompilationController.class)) != null && controller.impl == copy.impl) {
            return copy;
        }
        return null;
    }

    @NonNull
    @Override
    public JavaSource.Phase toPhase(@NonNull JavaSource.Phase phase) throws IOException {
        JavaSource.Phase result = super.toPhase(phase);
        if (result.compareTo(JavaSource.Phase.PARSED) >= 0) {
            this.init();
        }
        return result;
    }

    @NonNull
    public synchronized TreeMaker getTreeMaker() throws IllegalStateException {
        this.checkConfinement();
        if (this.treeMaker == null) {
            throw new IllegalStateException("Cannot call getTreeMaker before toPhase.");
        }
        return this.treeMaker;
    }

    Map<Tree, Tree> getChangeSet() {
        return this.changes;
    }

    public synchronized void rewrite(@NullAllowed Tree oldTree, @NonNull Tree newTree) {
        this.checkConfinement();
        if (this.changes == null) {
            throw new IllegalStateException("Cannot call rewrite before toPhase.");
        }
        if (oldTree == newTree) {
            return;
        }
        if (oldTree == null && Tree.Kind.COMPILATION_UNIT == newTree.getKind()) {
            this.createCompilationUnit((JCTree.JCCompilationUnit)newTree);
            return;
        }
        if (oldTree == null || newTree == null) {
            throw new IllegalArgumentException("Null values are not allowed.");
        }
        Tree t = this.rewriteHints.get((Object)oldTree);
        if (t == null) {
            this.associateTree(newTree, oldTree, false);
        }
        assert (new TreeCollector(this.introducedTrees, true).scan(newTree, null) != null);
        this.changes.put(oldTree, newTree);
    }

    boolean validateIsReplacement(Tree t) {
        boolean ok = true;
        if (!$assertionsDisabled) {
            ok = false;
            if (false) {
                throw new AssertionError();
            }
        }
        return ok || this.introducedTrees.containsKey((Object)t);
    }

    public synchronized void rewrite(@NonNull Tree tree, @NonNull DocTree oldTree, @NonNull DocTree newTree) {
        this.checkConfinement();
        if (this.docChanges == null) {
            throw new IllegalStateException("Cannot call rewrite before toPhase.");
        }
        if (oldTree == newTree) {
            return;
        }
        Map<DocTree, DocTree> changesMap = this.docChanges.get((Object)tree);
        if (changesMap == null) {
            changesMap = new IdentityHashMap<DocTree, DocTree>();
            this.docChanges.put(tree, changesMap);
        }
        changesMap.put(oldTree, newTree);
    }

    public synchronized void rewriteInComment(int start, int length, @NonNull String newText) throws IllegalArgumentException {
        int commentSuffix;
        int commentPrefix;
        this.checkConfinement();
        TokenSequence ts = this.getTokenHierarchy().tokenSequence(JavaTokenId.language());
        ts.move(start);
        if (!ts.moveNext()) {
            throw new IllegalArgumentException("Cannot rewriteInComment start=" + start + ", text length=" + this.getText().length());
        }
        if (ts.token().id() != JavaTokenId.LINE_COMMENT && ts.token().id() != JavaTokenId.BLOCK_COMMENT && ts.token().id() != JavaTokenId.JAVADOC_COMMENT) {
            throw new IllegalArgumentException("Cannot rewriteInComment: attempt to rewrite non-comment token: " + (Object)ts.token().id());
        }
        if (ts.offset() + ts.token().length() < start + length) {
            throw new IllegalArgumentException("Cannot rewriteInComment: attempt to rewrite text after comment token. Token end offset: " + (ts.offset() + ts.token().length()) + ", rewrite end offset: " + (start + length));
        }
        switch ((JavaTokenId)ts.token().id()) {
            case LINE_COMMENT: {
                commentPrefix = 2;
                commentSuffix = 0;
                break;
            }
            case BLOCK_COMMENT: {
                commentPrefix = 2;
                commentSuffix = 2;
                break;
            }
            case JAVADOC_COMMENT: {
                commentPrefix = 3;
                commentSuffix = 2;
                break;
            }
            default: {
                throw new IllegalStateException("Internal error");
            }
        }
        if (ts.offset() + commentPrefix > start) {
            throw new IllegalArgumentException("Cannot rewriteInComment: attempt to rewrite comment prefix");
        }
        if (ts.offset() + ts.token().length() - commentSuffix < start + length) {
            throw new IllegalArgumentException("Cannot rewriteInComment: attempt to rewrite comment suffix");
        }
        this.textualChanges.add(CasualDiff.Diff.delete(start, start + length));
        this.textualChanges.add(CasualDiff.Diff.insert(start + length, newText));
        this.userInfo.put(start, NbBundle.getMessage(CasualDiff.class, (String)"TXT_RenameInComment"));
    }

    public synchronized void tag(@NonNull Tree t, @NonNull Object tag) {
        this.tree2Tag.put(t, tag);
    }

    @NonNull
    public synchronized Tree resolveRewriteTarget(@NonNull Tree in) {
        IdentityHashMap<Tree, Tree> localChanges = new IdentityHashMap<Tree, Tree>(this.changes);
        while (localChanges.containsKey((Object)in)) {
            in = localChanges.remove((Object)in);
        }
        return in;
    }

    synchronized void associateTree(Tree nue, Tree original, boolean force) {
        if (this.rewriteHints == null) {
            this.rewriteHints = new HashMap<Tree, Tree>(7);
        }
        if (original == null) {
            Tree ex = this.rewriteHints.get((Object)nue);
            if (ex == null || force) {
                this.rewriteHints.put(nue, NOT_LINKED);
            }
        } else {
            if (original == nue) {
                return;
            }
            Tree ex = this.rewriteHints.get((Object)original);
            if (ex == null || force) {
                this.rewriteHints.put(original, nue);
            }
        }
    }

    private static String codeForCompilationUnit(CompilationUnitTree topLevel) throws IOException {
        return ((JCTree.JCCompilationUnit)topLevel).sourcefile.getCharContent(true).toString();
    }

    private void addSyntheticTrees(DiffContext diffContext, Tree node) {
        ExpressionTree select;
        ExpressionTree est;
        if (node == null) {
            return;
        }
        if (((JCTree)node).pos == -1) {
            diffContext.syntheticTrees.add(node);
            return;
        }
        if (node.getKind() == Tree.Kind.EXPRESSION_STATEMENT && (est = ((ExpressionStatementTree)node).getExpression()).getKind() == Tree.Kind.METHOD_INVOCATION && (select = ((MethodInvocationTree)est).getMethodSelect()).getKind() == Tree.Kind.IDENTIFIER && ((IdentifierTree)select).getName().contentEquals("super") && this.getTreeUtilities().isSynthetic((CompilationUnitTree)diffContext.origUnit, node)) {
            diffContext.syntheticTrees.add(node);
        }
    }

    private java.util.List<ModificationResult.Difference> processCurrentCompilationUnit(final DiffContext diffContext, Map<?, int[]> tag2Span) throws IOException, BadLocationException {
        Set<? extends Element> nueImports;
        final LinkedHashSet<TreePath> pathsToRewrite = new LinkedHashSet<TreePath>();
        final IdentityHashMap<TreePath, Map<Tree, Tree>> parent2Rewrites = new IdentityHashMap<TreePath, Map<Tree, Tree>>();
        final IdentityHashMap<Tree, DocCommentTree> tree2Doc = new IdentityHashMap<Tree, DocCommentTree>();
        boolean fillImports = true;
        HashMap<Integer, String> userInfo = new HashMap<Integer, String>();
        final HashSet<Tree> oldTrees = new HashSet<Tree>();
        final IdentityHashMap presentInResult = new IdentityHashMap();
        if (CasualDiff.OLD_TREES_VERBATIM) {
            new TreeScanner<Void, Void>(){
                private boolean synthetic;

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                public Void scan(Tree node, Void p) {
                    if (node == null) {
                        return null;
                    }
                    boolean oldSynthetic = this.synthetic;
                    try {
                        this.synthetic |= WorkingCopy.this.getTreeUtilities().isSynthetic((CompilationUnitTree)diffContext.origUnit, node);
                        if (!this.synthetic) {
                            oldTrees.add(node);
                        }
                        WorkingCopy.this.addSyntheticTrees(diffContext, node);
                        Void void_ = (Void)TreeScanner.super.scan(node, (Object)p);
                        return void_;
                    }
                    finally {
                        this.synthetic = oldSynthetic;
                    }
                }

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                public Void visitForLoop(ForLoopTree node, Void p) {
                    try {
                        Void void_ = (Void)TreeScanner.super.visitForLoop(node, (Object)p);
                        return void_;
                    }
                    finally {
                        oldTrees.removeAll(node.getInitializer());
                    }
                }

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                public Void visitEnhancedForLoop(EnhancedForLoopTree node, Void p) {
                    try {
                        Void void_ = (Void)TreeScanner.super.visitEnhancedForLoop(node, (Object)p);
                        return void_;
                    }
                    finally {
                        oldTrees.remove((Object)node.getVariable());
                    }
                }

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                public Void visitTry(TryTree node, Void p) {
                    try {
                        Void void_ = (Void)TreeScanner.super.visitTry(node, (Object)p);
                        return void_;
                    }
                    finally {
                        oldTrees.removeAll(node.getResources());
                    }
                }
            }.scan((Tree)diffContext.origUnit, null);
        } else {
            new TreeScanner<Void, Void>(){

                public Void scan(Tree node, Void p) {
                    WorkingCopy.this.addSyntheticTrees(diffContext, node);
                    WorkingCopy.this.addPresentInResult(presentInResult, node, true);
                    return (Void)TreeScanner.super.scan(node, (Object)p);
                }
            }.scan((Tree)diffContext.origUnit, null);
        }
        if (!REWRITE_WHOLE_FILE) {
            new TreePathScanner<Void, Void>(){
                private TreePath currentParent;
                private final Map<Tree, TreePath> tree2Path;
                private final ElementOverlay.FQNComputer fqn;
                private final Set<Tree> rewriteTarget;

                private TreePath getParentPath(TreePath tp, Tree t) {
                    Tree parent;
                    if (tp != null) {
                        while (tp.getLeaf().getKind() != Tree.Kind.COMPILATION_UNIT && WorkingCopy.this.getTreeUtilities().isSynthetic(tp)) {
                            tp = tp.getParentPath();
                        }
                        parent = tp.getLeaf();
                    } else {
                        parent = t;
                    }
                    TreePath c = this.tree2Path.get((Object)parent);
                    if (c == null) {
                        c = tp != null ? tp : new TreePath((CompilationUnitTree)t);
                        this.tree2Path.put(parent, c);
                    }
                    return c;
                }

                public Void scan(Tree tree, Void p) {
                    if (WorkingCopy.this.changes.containsKey((Object)tree) || WorkingCopy.this.docChanges.containsKey((Object)tree)) {
                        if (this.currentParent == null) {
                            this.currentParent = this.getParentPath(this.getCurrentPath(), tree);
                            if (this.currentParent.getParentPath() != null && this.currentParent.getParentPath().getLeaf().getKind() == Tree.Kind.COMPILATION_UNIT) {
                                this.currentParent = this.currentParent.getParentPath();
                            }
                            pathsToRewrite.add(this.currentParent);
                            if (!parent2Rewrites.containsKey((Object)this.currentParent)) {
                                parent2Rewrites.put(this.currentParent, new IdentityHashMap());
                            }
                        }
                        Tree rev = (Tree)WorkingCopy.this.changes.get((Object)tree);
                        Tree hint = WorkingCopy.this.resolveRewriteHint(tree);
                        if (rev != null) {
                            Map rewrites = (Map)parent2Rewrites.get((Object)this.currentParent);
                            WorkingCopy.this.changes.remove((Object)tree);
                            if (hint == null) {
                                WorkingCopy.this.addPresentInResult(presentInResult, rev, false);
                            }
                            rewrites.put(tree, rev);
                            this.scan(rev, p);
                        } else {
                            if (hint == null) {
                                WorkingCopy.this.addPresentInResult(presentInResult, rev, false);
                            }
                            WorkingCopy.this.addPresentInResult(presentInResult, tree, true);
                            TreePathScanner.super.scan(tree, (Object)p);
                        }
                    } else {
                        WorkingCopy.this.addPresentInResult(presentInResult, tree, true);
                        TreePathScanner.super.scan(tree, (Object)p);
                    }
                    if (this.currentParent != null && this.currentParent.getLeaf() == tree) {
                        this.currentParent = null;
                    }
                    return null;
                }

                public Void visitCompilationUnit(CompilationUnitTree node, Void p) {
                    this.fqn.setCompilationUnit(node);
                    return (Void)TreePathScanner.super.visitCompilationUnit(node, (Object)p);
                }

                public Void visitClass(ClassTree node, Void p) {
                    String parent = this.fqn.getFQN();
                    this.fqn.enterClass(node);
                    WorkingCopy.this.overlay.registerClass(parent, this.fqn.getFQN(), node, this.rewriteTarget.contains((Object)node));
                    TreePathScanner.super.visitClass(node, (Object)p);
                    this.fqn.leaveClass();
                    return null;
                }
            }.scan((Tree)diffContext.origUnit, null);
        } else {
            TreePath topLevel = new TreePath((CompilationUnitTree)diffContext.origUnit);
            pathsToRewrite.add(topLevel);
            parent2Rewrites.put(topLevel, this.changes);
            fillImports = false;
        }
        ArrayList<CasualDiff.Diff> diffs = new ArrayList<CasualDiff.Diff>();
        ImportAnalysis2 ia = new ImportAnalysis2(this);
        boolean importsFilled = false;
        for (final TreePath path : pathsToRewrite) {
            ArrayList<ClassTree> classes = new ArrayList<ClassTree>();
            if (path.getParentPath() != null) {
                for (Tree t : path.getParentPath()) {
                    if (t.getKind() == Tree.Kind.COMPILATION_UNIT && !importsFilled) {
                        CompilationUnitTree cutt = (CompilationUnitTree)t;
                        ia.setCompilationUnit(cutt);
                        ia.setPackage(cutt.getPackageName());
                        ia.setImports(cutt.getImports());
                        importsFilled = true;
                    }
                    if (!TreeUtilities.CLASS_TREE_KINDS.contains((Object)t.getKind())) continue;
                    classes.add((ClassTree)t);
                }
            } else if (path.getLeaf().getKind() == Tree.Kind.COMPILATION_UNIT && ((Map)parent2Rewrites.get((Object)path)).size() == 1) {
                CompilationUnitTree origCUT = (CompilationUnitTree)path.getLeaf();
                Tree nue = (Tree)((Map)parent2Rewrites.get((Object)path)).get((Object)origCUT);
                if (nue != null && nue.getKind() == Tree.Kind.COMPILATION_UNIT) {
                    CompilationUnitTree nueCUT = (CompilationUnitTree)nue;
                    if (Utilities.compareObjects((Object)origCUT.getPackageAnnotations(), (Object)nueCUT.getPackageAnnotations()) && Utilities.compareObjects((Object)origCUT.getPackageName(), (Object)nueCUT.getPackageName()) && Utilities.compareObjects((Object)origCUT.getTypeDecls(), (Object)nueCUT.getTypeDecls())) {
                        fillImports = false;
                        diffs.addAll(CasualDiff.diff(this.getContext(), diffContext, this.getTreeUtilities(), origCUT.getImports(), nueCUT.getImports(), userInfo, this.tree2Tag, tree2Doc, tag2Span, oldTrees));
                        continue;
                    }
                }
            }
            Collections.reverse(classes);
            for (ClassTree ct : classes) {
                ia.classEntered(ct);
                ia.enterVisibleThroughClasses(ct);
            }
            final Map rewrites = (Map)parent2Rewrites.get((Object)path);
            ImmutableDocTreeTranslator itt = new ImmutableDocTreeTranslator(this){
                @NonNull
                private Map<Tree, Tree> map;
                @NonNull
                private Map<DocTree, DocTree> docMap;
                private final TreeVisitor<Tree, Void> duplicator;

                @Override
                public Tree translate(Tree tree) {
                    DocCommentTree newDoc;
                    if (WorkingCopy.this.docChanges.containsKey((Object)tree)) {
                        Tree newTree = null;
                        if (!this.map.containsKey((Object)tree)) {
                            Tree importComments = GeneratorUtilities.get(WorkingCopy.this).importComments(tree, WorkingCopy.this.getCompilationUnit());
                            newTree = (Tree)importComments.accept(this.duplicator, (Object)null);
                            this.map.put(tree, newTree);
                        }
                        this.docMap = (Map)WorkingCopy.this.docChanges.remove((Object)tree);
                        newDoc = this.docMap.size() == 1 && this.docMap.containsKey(null) ? (DocCommentTree)this.translate((DocTree)((DocCommentTree)this.docMap.get(null))) : (DocCommentTree)this.translate((DocTree)((DocTrees)WorkingCopy.this.getTrees()).getDocCommentTree(new TreePath(path, tree)));
                        tree2Doc.put(tree, newDoc);
                        if (newTree != null && tree != newTree) {
                            tree2Doc.put(newTree, newDoc);
                        }
                    }
                    Tree translated = this.map.remove((Object)tree);
                    if (WorkingCopy.this.docChanges.containsKey((Object)translated)) {
                        this.docMap = (Map)WorkingCopy.this.docChanges.remove((Object)translated);
                        assert (this.docMap.size() == 1);
                        assert (this.docMap.containsKey(null));
                        newDoc = (DocCommentTree)this.translate((DocTree)((DocCommentTree)this.docMap.get(null)));
                        tree2Doc.put(translated, newDoc);
                    }
                    Tree t = translated != null ? this.translate(translated) : super.translate(tree);
                    if (tree2Doc != null && tree != t && tree2Doc.containsKey((Object)tree)) {
                        tree2Doc.put(t, tree2Doc.remove((Object)tree));
                    }
                    if (tree2Doc != null && translated != t && tree2Doc.containsKey((Object)translated)) {
                        tree2Doc.put(t, tree2Doc.remove((Object)translated));
                    }
                    return t;
                }

                @Override
                public DocTree translate(DocTree tree) {
                    DocTree translated;
                    if (this.docMap != null && (translated = this.docMap.remove((Object)tree)) != null) {
                        return this.translate(translated);
                    }
                    return super.translate(tree);
                }
            };
            Context c = this.impl.getJavacTask().getContext();
            itt.attach(c, ia, this.tree2Tag);
            Tree brandNew = itt.translate(path.getLeaf());
            new CommentReplicator(presentInResult.keySet()).scan((Tree)diffContext.origUnit, (Object)null);
            this.addCommentsToContext(diffContext);
            for (ClassTree ct2 : classes) {
                ia.classLeft();
            }
            if (brandNew.getKind() == Tree.Kind.COMPILATION_UNIT) {
                fillImports = false;
            }
            diffs.addAll(CasualDiff.diff(this.getContext(), diffContext, this.getTreeUtilities(), path, (JCTree)brandNew, userInfo, this.tree2Tag, tree2Doc, tag2Span, oldTrees));
        }
        if (fillImports && (nueImports = ia.getImports()) != null && !nueImports.isEmpty()) {
            CompilationUnitTree ncut = GeneratorUtilities.get(this).addImports((CompilationUnitTree)diffContext.origUnit, nueImports);
            diffs.addAll(CasualDiff.diff(this.getContext(), diffContext, this.getTreeUtilities(), diffContext.origUnit.getImports(), ncut.getImports(), userInfo, this.tree2Tag, tree2Doc, tag2Span, oldTrees));
        }
        diffs.addAll(this.textualChanges);
        userInfo.putAll(this.userInfo);
        try {
            return DiffUtilities.diff2ModificationResultDifference(diffContext.file, diffContext.positionConverter, userInfo, WorkingCopy.codeForCompilationUnit((CompilationUnitTree)diffContext.origUnit), diffs);
        }
        catch (IOException ex) {
            if (!diffContext.file.isValid()) {
                Logger.getLogger(WorkingCopy.class.getName()).log(Level.FINE, null, ex);
                return Collections.emptyList();
            }
            throw ex;
        }
    }

    private void addCommentsToContext(DiffContext context) {
        HashMap<Integer, Comment> m = new HashMap<Integer, Comment>(this.usedComments.size());
        for (Comment c : this.usedComments) {
            m.put(c.pos(), c);
        }
        context.usedComments = m;
    }

    private void addPresentInResult(Map<Tree, Boolean> present, Tree t, boolean mark) {
        present.put(t, mark);
        CommentSetImpl csi = CommentHandlerService.instance(this.impl.getJavacTask().getContext()).getComments(t);
        for (CommentSet.RelativePosition pos : CommentSet.RelativePosition.values()) {
            this.useComments(csi.getComments(pos));
        }
    }

    java.util.List<Comment> useComments(java.util.List<Comment> comments) {
        if (this.usedComments == null) {
            this.usedComments = new HashSet<Comment>();
        }
        this.usedComments.addAll(comments);
        return comments;
    }

    private Tree resolveRewriteHint(Tree orig) {
        Tree last;
        Tree target = null;
        Tree from = orig;
        do {
            last = target;
            target = from;
            if ((target = this.rewriteHints.get((Object)target)) == NOT_LINKED) {
                return target;
            }
            from = target;
        } while (target != null);
        return last;
    }

    private java.util.List<ModificationResult.Difference> processExternalCUs(Map<?, int[]> tag2Span, Set<Tree> syntheticTrees) {
        if (this.externalChanges == null) {
            return Collections.emptyList();
        }
        LinkedList<ModificationResult.Difference> result = new LinkedList<ModificationResult.Difference>();
        for (CompilationUnitTree t : this.externalChanges.values()) {
            try {
                FileObject targetFile = this.doCreateFromTemplate(t);
                CompilationUnitTree templateCUT = (CompilationUnitTree)this.impl.getJavacTask().parse(new JavaFileObject[]{FileObjects.sourceFileObject(targetFile, targetFile.getParent())}).iterator().next();
                CompilationUnitTree importComments = GeneratorUtilities.get(this).importComments(templateCUT, templateCUT);
                this.changes.put((Tree)importComments, (Tree)t);
                StringWriter target = new StringWriter();
                ModificationResult.commit(targetFile, this.processCurrentCompilationUnit(new DiffContext(this, templateCUT, WorkingCopy.codeForCompilationUnit(templateCUT), new PositionConverter(), targetFile, syntheticTrees, this.getFileObject() != null ? this.getCompilationUnit() : null, this.getFileObject() != null ? this.getText() : null), tag2Span), target);
                result.add(new ModificationResult.CreateChange(t.getSourceFile(), target.toString()));
                target.close();
            }
            catch (BadLocationException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return result;
    }

    String template(ElementKind kind) {
        if (kind == null) {
            return "Templates/Classes/Empty.java";
        }
        switch (kind) {
            case CLASS: {
                return "Templates/Classes/Class.java";
            }
            case INTERFACE: {
                return "Templates/Classes/Interface.java";
            }
            case ANNOTATION_TYPE: {
                return "Templates/Classes/AnnotationType.java";
            }
            case ENUM: {
                return "Templates/Classes/Enum.java";
            }
            case PACKAGE: {
                return "Templates/Classes/package-info.java";
            }
        }
        Logger.getLogger(WorkingCopy.class.getName()).log(Level.SEVERE, "Cannot resolve template for {0}", (Object)kind);
        return "Templates/Classes/Empty.java";
    }

    FileObject doCreateFromTemplate(CompilationUnitTree cut) throws IOException {
        ElementKind kind;
        if ("package-info.java".equals(cut.getSourceFile().getName())) {
            kind = ElementKind.PACKAGE;
        } else if (cut.getTypeDecls().isEmpty()) {
            kind = null;
        } else {
            switch (((Tree)cut.getTypeDecls().get(0)).getKind()) {
                case CLASS: {
                    kind = ElementKind.CLASS;
                    break;
                }
                case INTERFACE: {
                    kind = ElementKind.INTERFACE;
                    break;
                }
                case ANNOTATION_TYPE: {
                    kind = ElementKind.ANNOTATION_TYPE;
                    break;
                }
                case ENUM: {
                    kind = ElementKind.ENUM;
                    break;
                }
                default: {
                    Logger.getLogger(WorkingCopy.class.getName()).log(Level.SEVERE, "Cannot resolve template for {0}", (Object)((Tree)cut.getTypeDecls().get(0)).getKind());
                    kind = null;
                }
            }
        }
        FileObject template = FileUtil.getConfigFile((String)this.template(kind));
        return this.doCreateFromTemplate(template, cut.getSourceFile());
    }

    FileObject doCreateFromTemplate(FileObject template, JavaFileObject sourceFile) throws IOException {
        FileObject scratchFolder = FileUtil.createMemoryFileSystem().getRoot();
        if (template == null) {
            return scratchFolder.createData("out", "java");
        }
        DataObject templateDO = DataObject.find((FileObject)template);
        if (!templateDO.isTemplate()) {
            return scratchFolder.createData("out", "java");
        }
        File pack = Utilities.toFile((URI)sourceFile.toUri()).getParentFile();
        while (FileUtil.toFileObject((File)pack) == null) {
            pack = pack.getParentFile();
        }
        FileObject targetFolder = FileUtil.toFileObject((File)pack);
        DataFolder targetDataFolder = DataFolder.findFolder((FileObject)targetFolder);
        scratchFolder.setAttribute(OverlayTemplateAttributesProvider.ATTR_ORIG_FILE, (Object)targetDataFolder);
        String name = FileObjects.getName(sourceFile, true);
        DataObject newFile = templateDO.createFromTemplate(DataFolder.findFolder((FileObject)scratchFolder), name);
        return newFile.getPrimaryFile();
    }

    java.util.List<ModificationResult.Difference> getChanges(Map<?, int[]> tag2Span) throws IOException, BadLocationException {
        if (this.afterCommit) {
            throw new IllegalStateException("The commit method can be called only once on a WorkingCopy instance");
        }
        this.afterCommit = true;
        if (this.changes == null) {
            return null;
        }
        if (this.externalChanges != null) {
            for (CompilationUnitTree t : this.externalChanges.values()) {
                final ElementOverlay.FQNComputer fqn = new ElementOverlay.FQNComputer();
                fqn.setCompilationUnit(t);
                this.overlay.registerPackage(fqn.getFQN());
                new TreeScanner<Void, Void>(){

                    public Void visitClass(ClassTree node, Void p) {
                        String parent = fqn.getFQN();
                        fqn.enterClass(node);
                        WorkingCopy.this.overlay.registerClass(parent, fqn.getFQN(), node, true);
                        TreeScanner.super.visitClass(node, (Object)p);
                        fqn.leaveClass();
                        return null;
                    }
                }.scan((Tree)t, (Object)null);
            }
        }
        LinkedList<ModificationResult.Difference> result = new LinkedList<ModificationResult.Difference>();
        HashSet<Tree> syntheticTrees = new HashSet<Tree>();
        if (this.getFileObject() != null) {
            result.addAll(this.processCurrentCompilationUnit(new DiffContext(this, syntheticTrees), tag2Span));
        }
        result.addAll(this.processExternalCUs(tag2Span, syntheticTrees));
        this.overlay.clearElementsCache();
        return result;
    }

    private void createCompilationUnit(JCTree.JCCompilationUnit unitTree) {
        if (this.externalChanges == null) {
            this.externalChanges = new HashMap<JavaFileObject, CompilationUnitTree>();
        }
        this.externalChanges.put(unitTree.getSourceFile(), (CompilationUnitTree)unitTree);
    }

    static {
        NOT_LINKED = new Tree(){

            public Tree.Kind getKind() {
                return Tree.Kind.OTHER;
            }

            public <R, D> R accept(TreeVisitor<R, D> visitor, D data) {
                return (R)visitor.visitOther((Tree)this, data);
            }
        };
        REWRITE_WHOLE_FILE = Boolean.getBoolean(WorkingCopy.class.getName() + ".rewrite-whole-file");
    }

    private class CommentReplicator
    extends TreePathScanner<Object, Object> {
        private final Set<Tree> stillPresent;
        private boolean collectCommentsFromRemovedNodes;
        private Map<Tree, Tree> copyTo;
        private final CommentHandlerService commentHandler;
        private Tree parentToCopy;
        private Set<Comment> retained;
        private CommentSet.RelativePosition collectToPosition;

        CommentReplicator(Set<Tree> presentNodes) {
            this.copyTo = new IdentityHashMap<Tree, Tree>();
            this.retained = new HashSet<Comment>();
            this.stillPresent = presentNodes;
            this.commentHandler = CommentHandlerService.instance(WorkingCopy.this.impl.getJavacTask().getContext());
        }

        private Object scanAndReduce(Tree node, Object p, Object r) {
            return this.reduce(this.scan(node, p), r);
        }

        public Object scan(Iterable<? extends Tree> nodes, Object p) {
            Object r = null;
            if (nodes != null) {
                boolean first = true;
                Tree saveParent = this.parentToCopy;
                CommentSet.RelativePosition savePosition = this.collectToPosition;
                this.collectToPosition = CommentSet.RelativePosition.INNER;
                if (this.collectCommentsFromRemovedNodes) {
                    for (Tree node : nodes) {
                        Tree target = WorkingCopy.this.resolveRewriteHint(node);
                        if (target != null) {
                            if (target == WorkingCopy.NOT_LINKED) continue;
                            this.parentToCopy = target;
                            this.collectToPosition = CommentSet.RelativePosition.PRECEDING;
                            break;
                        }
                        if (!this.stillPresent.contains((Object)node)) continue;
                        this.parentToCopy = node;
                        this.collectToPosition = CommentSet.RelativePosition.PRECEDING;
                        break;
                    }
                }
                boolean reset = false;
                for (Tree node : nodes) {
                    if (this.collectCommentsFromRemovedNodes) {
                        Tree target = WorkingCopy.this.resolveRewriteHint(node);
                        if (target != null && target != WorkingCopy.NOT_LINKED) {
                            this.parentToCopy = target;
                            this.collectToPosition = CommentSet.RelativePosition.INNER;
                            reset = true;
                        } else if (this.stillPresent.contains((Object)node)) {
                            this.parentToCopy = node;
                            this.collectToPosition = CommentSet.RelativePosition.INNER;
                            reset = true;
                        }
                    }
                    r = first ? this.scan(node, p) : this.scanAndReduce(node, p, r);
                    first = false;
                    if (!reset) continue;
                    this.collectToPosition = CommentSet.RelativePosition.TRAILING;
                }
                this.parentToCopy = saveParent;
                this.collectToPosition = savePosition;
            }
            return r;
        }

        public Object scan(Tree l, Object p) {
            boolean collectChildren = false;
            Tree newParentCopy = null;
            boolean saveCollect = this.collectCommentsFromRemovedNodes;
            Tree target = WorkingCopy.this.resolveRewriteHint(l);
            if (target == WorkingCopy.NOT_LINKED) {
                collectChildren = false;
            } else if (target != null) {
                if (!this.commentHandler.getComments(target).hasComments() && !this.stillPresent.contains((Object)l)) {
                    this.commentHandler.copyComments(l, target, null, WorkingCopy.this.usedComments);
                    newParentCopy = target;
                    collectChildren = true;
                }
            } else if (!this.stillPresent.contains((Object)l)) {
                collectChildren = this.collectCommentsFromRemovedNodes;
                if (this.collectCommentsFromRemovedNodes && this.parentToCopy != null) {
                    this.commentHandler.copyComments(l, this.parentToCopy, this.collectToPosition, WorkingCopy.this.usedComments);
                }
            }
            if (this.stillPresent.contains((Object)l)) {
                newParentCopy = l;
            }
            Tree saveParent = this.parentToCopy;
            this.collectCommentsFromRemovedNodes = collectChildren;
            if (newParentCopy != null) {
                this.parentToCopy = newParentCopy;
            }
            Object v = TreePathScanner.super.scan(l, p);
            this.parentToCopy = saveParent;
            this.collectCommentsFromRemovedNodes = saveCollect;
            return v;
        }
    }

    class Translator
    extends ImmutableTreeTranslator {
        private Map<Tree, Tree> changeMap;

        public Translator() {
            super(WorkingCopy.this);
        }

        Tree translate(Tree tree, Map<Tree, Tree> changeMap) {
            this.changeMap = new HashMap<Tree, Tree>(changeMap);
            return this.translate(tree);
        }

        @Override
        public Tree translate(Tree tree) {
            assert (this.changeMap != null);
            if (tree == null) {
                return null;
            }
            Tree repl = this.changeMap.remove((Object)tree);
            Tree newRepl = repl != null ? this.translate(repl) : super.translate(tree);
            return newRepl;
        }
    }

    private static class TreeCollector
    extends TreeScanner {
        private final Map<Tree, Boolean> collectTo;
        private final boolean add;

        public TreeCollector(Map<Tree, Boolean> collectTo, boolean add) {
            this.collectTo = collectTo;
            this.add = add;
        }

        public Object scan(Tree node, Object p) {
            if (this.add) {
                this.collectTo.put(node, true);
            } else {
                this.collectTo.remove((Object)node);
            }
            super.scan(node, p);
            return true;
        }
    }

}

