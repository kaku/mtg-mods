/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreePathScanner
 *  com.sun.source.util.Trees
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.util.Context
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.LineCookie
 *  org.openide.cookies.OpenCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.text.Line
 *  org.openide.text.Line$Set
 *  org.openide.text.Line$ShowOpenType
 *  org.openide.text.Line$ShowVisibilityType
 *  org.openide.text.NbDocument
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.UserQuestionException
 */
package org.netbeans.api.java.source;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreePathScanner;
import com.sun.source.util.Trees;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.util.Context;
import java.io.IOException;
import java.util.Collection;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.swing.Icon;
import javax.swing.SwingUtilities;
import javax.swing.text.StyledDocument;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.modules.java.source.parsing.CompilationInfoImpl;
import org.netbeans.modules.java.source.pretty.VeryPretty;
import org.netbeans.modules.java.source.save.DiffContext;
import org.netbeans.modules.java.ui.Icons;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.LineCookie;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.text.Line;
import org.openide.text.NbDocument;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.UserQuestionException;

@Deprecated
public final class UiUtils {
    private static Logger log = Logger.getLogger(UiUtils.class.getName());

    private UiUtils() {
    }

    @Deprecated
    public static Icon getElementIcon(ElementKind elementKind, Collection<Modifier> modifiers) {
        return Icons.getElementIcon(elementKind, modifiers);
    }

    @Deprecated
    public static Icon getDeclarationIcon(Element element) {
        return UiUtils.getElementIcon(element.getKind(), element.getModifiers());
    }

    @Deprecated
    public static boolean open(ClasspathInfo cpInfo, Element el) {
        Object[] openInfo = UiUtils.getOpenInfo(cpInfo, el);
        if (openInfo != null) {
            assert (openInfo[0] instanceof FileObject);
            assert (openInfo[1] instanceof Integer);
            return UiUtils.doOpen((FileObject)openInfo[0], (Integer)openInfo[1]);
        }
        return false;
    }

    @Deprecated
    public static boolean open(FileObject toSearch, ElementHandle<? extends Element> toOpen) {
        if (toSearch == null || toOpen == null) {
            throw new IllegalArgumentException("null not supported");
        }
        Object[] openInfo = UiUtils.getOpenInfo(toSearch, toOpen);
        if (openInfo != null) {
            assert (openInfo[0] instanceof FileObject);
            assert (openInfo[1] instanceof Integer);
            return UiUtils.doOpen((FileObject)openInfo[0], (Integer)openInfo[1]);
        }
        return false;
    }

    private static String getMethodHeader(MethodTree tree, CompilationInfo info, String s) {
        Context context = info.impl.getJavacTask().getContext();
        VeryPretty veryPretty = new VeryPretty(new DiffContext(info));
        return veryPretty.getMethodHeader(tree, s);
    }

    private static String getClassHeader(ClassTree tree, CompilationInfo info, String s) {
        Context context = info.impl.getJavacTask().getContext();
        VeryPretty veryPretty = new VeryPretty(new DiffContext(info));
        return veryPretty.getClassHeader(tree, s);
    }

    private static String getVariableHeader(VariableTree tree, CompilationInfo info, String s) {
        Context context = info.impl.getJavacTask().getContext();
        VeryPretty veryPretty = new VeryPretty(new DiffContext(info));
        return veryPretty.getVariableHeader(tree, s);
    }

    @Deprecated
    public static String getHeader(TreePath treePath, CompilationInfo info, String formatString) {
        assert (info != null);
        assert (treePath != null);
        Element element = info.getTrees().getElement(treePath);
        if (element != null) {
            return UiUtils.getHeader(element, info, formatString);
        }
        return null;
    }

    @Deprecated
    public static String getHeader(Element element, CompilationInfo info, String formatString) {
        assert (element != null);
        assert (info != null);
        assert (formatString != null);
        Tree tree = info.getTrees().getTree(element);
        if (tree != null) {
            if (tree.getKind() == Tree.Kind.METHOD) {
                return UiUtils.getMethodHeader((MethodTree)tree, info, formatString);
            }
            if (TreeUtilities.CLASS_TREE_KINDS.contains((Object)tree.getKind())) {
                return UiUtils.getClassHeader((ClassTree)tree, info, formatString);
            }
            if (tree.getKind() == Tree.Kind.VARIABLE) {
                return UiUtils.getVariableHeader((VariableTree)tree, info, formatString);
            }
        }
        return formatString.replaceAll("%name%", element.getSimpleName().toString()).replaceAll("%[a-z]*%", "");
    }

    @Deprecated
    public static boolean open(FileObject fo, int offset) {
        return UiUtils.doOpen(fo, offset);
    }

    static Object[] getOpenInfo(ClasspathInfo cpInfo, Element el) {
        FileObject fo = SourceUtils.getFile(el, cpInfo);
        if (fo != null) {
            return UiUtils.getOpenInfo(fo, ElementHandle.create(el));
        }
        return null;
    }

    static Object[] getOpenInfo(FileObject fo, ElementHandle<? extends Element> handle) {
        assert (fo != null);
        try {
            int offset = UiUtils.getOffset(fo, handle);
            return new Object[]{fo, offset};
        }
        catch (IOException e) {
            if (log.isLoggable(Level.SEVERE)) {
                log.log(Level.SEVERE, e.getMessage(), e);
            }
            return null;
        }
    }

    @Deprecated
    public static int getDistance(String s, String t) {
        int n = s.length();
        int m = t.length();
        if (n == 0) {
            return m;
        }
        if (m == 0) {
            return n;
        }
        int[][] d = new int[n + 1][m + 1];
        int i = 0;
        while (i <= n) {
            d[i][0] = i++;
        }
        int j = 0;
        while (j <= m) {
            d[0][j] = j++;
        }
        for (i = 1; i <= n; ++i) {
            char s_i = s.charAt(i - 1);
            for (j = 1; j <= m; ++j) {
                char t_j = t.charAt(j - 1);
                int cost = s_i == t_j ? 0 : 1;
                d[i][j] = UiUtils.min(d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + cost);
            }
        }
        return d[n][m];
    }

    private static int min(int a, int b, int c) {
        int mi = a;
        if (b < mi) {
            mi = b;
        }
        if (c < mi) {
            mi = c;
        }
        return mi;
    }

    private static boolean doOpen(FileObject fo, int offset) {
        block9 : {
            try {
                OpenCookie oc;
                DataObject od = DataObject.find((FileObject)fo);
                EditorCookie ec = (EditorCookie)od.getLookup().lookup(EditorCookie.class);
                LineCookie lc = (LineCookie)od.getLookup().lookup(LineCookie.class);
                if (ec != null && lc != null && offset != -1) {
                    StyledDocument doc = null;
                    try {
                        doc = ec.openDocument();
                    }
                    catch (UserQuestionException uqe) {
                        Object value = DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Confirmation((Object)uqe.getLocalizedMessage(), NbBundle.getMessage(UiUtils.class, (String)"TXT_Question"), 0));
                        if (value != NotifyDescriptor.YES_OPTION) {
                            return false;
                        }
                        uqe.confirmed();
                        doc = ec.openDocument();
                    }
                    if (doc != null) {
                        Line l;
                        int line = NbDocument.findLineNumber((StyledDocument)doc, (int)offset);
                        int lineOffset = NbDocument.findLineOffset((StyledDocument)doc, (int)line);
                        int column = offset - lineOffset;
                        if (line != -1 && (l = lc.getLineSet().getCurrent(line)) != null) {
                            UiUtils.doShow(l, column);
                            return true;
                        }
                    }
                }
                if ((oc = (OpenCookie)od.getLookup().lookup(OpenCookie.class)) != null) {
                    UiUtils.doOpen(oc);
                    return true;
                }
            }
            catch (IOException e) {
                if (!log.isLoggable(Level.INFO)) break block9;
                log.log(Level.INFO, e.getMessage(), e);
            }
        }
        return false;
    }

    private static void doShow(final Line l, final int column) {
        if (SwingUtilities.isEventDispatchThread()) {
            l.show(Line.ShowOpenType.OPEN, Line.ShowVisibilityType.FOCUS, column);
        } else {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    l.show(Line.ShowOpenType.OPEN, Line.ShowVisibilityType.FOCUS, column);
                }
            });
        }
    }

    private static void doOpen(final OpenCookie oc) {
        if (SwingUtilities.isEventDispatchThread()) {
            oc.open();
        } else {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    oc.open();
                }
            });
        }
    }

    private static int getOffset(FileObject fo, final ElementHandle<? extends Element> handle) throws IOException {
        assert (handle != null);
        final int[] result = new int[]{-1};
        JavaSource js = JavaSource.forFileObject(fo);
        js.runUserActionTask(new Task<CompilationController>(){

            @Override
            public void run(CompilationController info) {
                block4 : {
                    try {
                        info.toPhase(JavaSource.Phase.RESOLVED);
                    }
                    catch (IOException ioe) {
                        if (!log.isLoggable(Level.SEVERE)) break block4;
                        log.log(Level.SEVERE, ioe.getMessage(), ioe);
                    }
                }
                Object el = handle.resolve(info);
                if (el == null) {
                    throw new IllegalArgumentException();
                }
                FindDeclarationVisitor v = new FindDeclarationVisitor((Element)el, info);
                CompilationUnitTree cu = info.getCompilationUnit();
                v.scan((Tree)cu, (Object)null);
                Tree elTree = v.declTree;
                if (elTree != null) {
                    result[0] = (int)info.getTrees().getSourcePositions().getStartPosition(cu, elTree);
                }
            }
        }, true);
        return result[0];
    }

    private static class FindDeclarationVisitor
    extends TreePathScanner<Void, Void> {
        private Element element;
        private Tree declTree;
        private CompilationInfo info;

        public FindDeclarationVisitor(Element element, CompilationInfo info) {
            this.element = element;
            this.info = info;
        }

        public Void visitClass(ClassTree tree, Void d) {
            this.handleDeclaration();
            TreePathScanner.super.visitClass(tree, (Object)d);
            return null;
        }

        public Void visitMethod(MethodTree tree, Void d) {
            this.handleDeclaration();
            TreePathScanner.super.visitMethod(tree, (Object)d);
            return null;
        }

        public Void visitVariable(VariableTree tree, Void d) {
            this.handleDeclaration();
            TreePathScanner.super.visitVariable(tree, (Object)d);
            return null;
        }

        public void handleDeclaration() {
            Element found = this.info.getTrees().getElement(this.getCurrentPath());
            if (this.element.equals(found)) {
                this.declTree = this.getCurrentPath().getLeaf();
            }
        }
    }

    @Deprecated
    public static final class PrintPart {
        public static final String ANNOTATIONS = "%annotations";
        public static final String NAME = "%name%";
        public static final String TYPE = "%type%";
        public static final String THROWS = "%throws%";
        public static final String IMPLEMENTS = "%implements%";
        public static final String EXTENDS = "%extends%";
        public static final String TYPEPARAMETERS = "%typeparameters%";
        public static final String FLAGS = "%flags%";
        public static final String PARAMETERS = "%parameters%";

        private PrintPart() {
        }
    }

}

