/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.loaders.MultiDataObject
 *  org.openide.loaders.MultiDataObject$Entry
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 */
package org.netbeans.api.java.loaders;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.java.JavaDataLoader;
import org.netbeans.modules.java.JavaNode;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.loaders.MultiDataObject;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

public final class JavaDataSupport {
    private JavaDataSupport() {
    }

    public static MultiDataObject.Entry createJavaFileEntry(MultiDataObject mdo, FileObject javafile) {
        return new JavaDataLoader.JavaFileEntry(mdo, javafile);
    }

    public static Node createJavaNode(FileObject javafile) {
        try {
            DataObject jdo = DataObject.find((FileObject)javafile);
            return new JavaNode(jdo, true);
        }
        catch (DataObjectNotFoundException ex) {
            Logger.getLogger(JavaDataSupport.class.getName()).log(Level.INFO, null, (Throwable)ex);
            return new AbstractNode(Children.LEAF);
        }
    }
}

