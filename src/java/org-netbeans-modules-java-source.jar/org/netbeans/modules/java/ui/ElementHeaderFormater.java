/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.VariableTree
 *  com.sun.tools.javac.util.Name
 */
package org.netbeans.modules.java.ui;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.VariableTree;
import com.sun.tools.javac.util.Name;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.modules.java.source.pretty.VeryPretty;
import org.netbeans.modules.java.source.save.DiffContext;

public class ElementHeaderFormater {
    private ElementHeaderFormater() {
    }

    public static String getMethodHeader(MethodTree tree, javax.lang.model.element.Name enclosingClassName, CompilationInfo info, String s) {
        VeryPretty veryPretty = new VeryPretty(new DiffContext(info));
        if (enclosingClassName != null) {
            veryPretty.enclClassName = (Name)enclosingClassName;
        }
        return veryPretty.getMethodHeader(tree, s);
    }

    public static String getClassHeader(ClassTree tree, CompilationInfo info, String s) {
        VeryPretty veryPretty = new VeryPretty(new DiffContext(info));
        return veryPretty.getClassHeader(tree, s);
    }

    public static String getVariableHeader(VariableTree tree, CompilationInfo info, String s) {
        VeryPretty veryPretty = new VeryPretty(new DiffContext(info));
        return veryPretty.getVariableHeader(tree, s);
    }
}

