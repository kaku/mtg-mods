/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer$Factory
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.LayoutStyle;
import javax.swing.border.Border;
import org.netbeans.modules.java.ui.FmtOptions;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class FmtBraces
extends JPanel {
    private JLabel bracesGenerationLabel;
    private JLabel bracesPlacementLabel;
    private JComboBox classDeclCombo;
    private JLabel classDeclLabel;
    private JComboBox doWhileBracesCombo;
    private JLabel doWhileBracesLabel;
    private JComboBox forBracesCombo;
    private JLabel forBracesLabel;
    private JComboBox ifBracesCombo;
    private JLabel ifBracesLabel;
    private JSeparator jSeparator1;
    private JSeparator jSeparator2;
    private JComboBox methodDeclCombo;
    private JLabel methodDeclLabel;
    private JComboBox otherCombo;
    private JLabel otherLabel;
    private JCheckBox specialElseIfCheckBox;
    private JComboBox whileBracesCombo;
    private JLabel whileBracesLabel;

    public FmtBraces() {
        this.initComponents();
        this.classDeclCombo.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "classDeclBracePlacement");
        this.methodDeclCombo.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "methodDeclBracePlacement");
        this.otherCombo.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "otherBracePlacement");
        this.specialElseIfCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "specialElseIf");
        this.ifBracesCombo.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "redundantIfBraces");
        this.forBracesCombo.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "redundantForBraces");
        this.whileBracesCombo.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "redundantWhileBraces");
        this.doWhileBracesCombo.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "redundantDoWhileBraces");
    }

    public static PreferencesCustomizer.Factory getController() {
        return new FmtOptions.CategorySupport.Factory("braces", FmtBraces.class, NbBundle.getMessage(FmtBraces.class, (String)"SAMPLE_Braces"), {"blankLinesBeforeClass", "0"});
    }

    private void initComponents() {
        this.bracesPlacementLabel = new JLabel();
        this.classDeclLabel = new JLabel();
        this.classDeclCombo = new JComboBox();
        this.methodDeclLabel = new JLabel();
        this.methodDeclCombo = new JComboBox();
        this.otherLabel = new JLabel();
        this.otherCombo = new JComboBox();
        this.specialElseIfCheckBox = new JCheckBox();
        this.bracesGenerationLabel = new JLabel();
        this.ifBracesLabel = new JLabel();
        this.ifBracesCombo = new JComboBox();
        this.forBracesLabel = new JLabel();
        this.forBracesCombo = new JComboBox();
        this.whileBracesLabel = new JLabel();
        this.whileBracesCombo = new JComboBox();
        this.doWhileBracesLabel = new JLabel();
        this.doWhileBracesCombo = new JComboBox();
        this.jSeparator1 = new JSeparator();
        this.jSeparator2 = new JSeparator();
        this.setName(NbBundle.getMessage(FmtBraces.class, (String)"LBL_Braces"));
        this.setOpaque(false);
        Mnemonics.setLocalizedText((JLabel)this.bracesPlacementLabel, (String)NbBundle.getMessage(FmtBraces.class, (String)"LBL_br_bracesPlacement"));
        this.classDeclLabel.setLabelFor(this.classDeclCombo);
        Mnemonics.setLocalizedText((JLabel)this.classDeclLabel, (String)NbBundle.getMessage(FmtBraces.class, (String)"LBL_bp_ClassDecl"));
        this.classDeclCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.methodDeclLabel.setLabelFor(this.methodDeclCombo);
        Mnemonics.setLocalizedText((JLabel)this.methodDeclLabel, (String)NbBundle.getMessage(FmtBraces.class, (String)"LBL_bp_MethodDecl"));
        this.methodDeclCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.methodDeclCombo.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FmtBraces.this.methodDeclComboActionPerformed(evt);
            }
        });
        this.otherLabel.setLabelFor(this.otherCombo);
        Mnemonics.setLocalizedText((JLabel)this.otherLabel, (String)NbBundle.getMessage(FmtBraces.class, (String)"LBL_bp_Other"));
        this.otherCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        Mnemonics.setLocalizedText((AbstractButton)this.specialElseIfCheckBox, (String)NbBundle.getMessage(FmtBraces.class, (String)"LBL_bp_SpecialElseIf"));
        this.specialElseIfCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.specialElseIfCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.specialElseIfCheckBox.setOpaque(false);
        Mnemonics.setLocalizedText((JLabel)this.bracesGenerationLabel, (String)NbBundle.getMessage(FmtBraces.class, (String)"LBL_br_bracesGeneration"));
        this.ifBracesLabel.setLabelFor(this.ifBracesCombo);
        Mnemonics.setLocalizedText((JLabel)this.ifBracesLabel, (String)NbBundle.getMessage(FmtBraces.class, (String)"LBL_bg_If"));
        this.ifBracesCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.forBracesLabel.setLabelFor(this.forBracesCombo);
        Mnemonics.setLocalizedText((JLabel)this.forBracesLabel, (String)NbBundle.getMessage(FmtBraces.class, (String)"LBL_bg_For"));
        this.forBracesCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.whileBracesLabel.setLabelFor(this.whileBracesCombo);
        Mnemonics.setLocalizedText((JLabel)this.whileBracesLabel, (String)NbBundle.getMessage(FmtBraces.class, (String)"LBL_bg_While"));
        this.whileBracesCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.doWhileBracesLabel.setLabelFor(this.doWhileBracesCombo);
        Mnemonics.setLocalizedText((JLabel)this.doWhileBracesLabel, (String)NbBundle.getMessage(FmtBraces.class, (String)"LBL_bg_DoWhile"));
        this.doWhileBracesCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.doWhileBracesLabel).addComponent(this.otherLabel).addComponent(this.methodDeclLabel).addComponent(this.whileBracesLabel)).addGap(12, 12, 12).addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.otherCombo, -2, -1, -2).addComponent(this.classDeclCombo, -2, -1, -2).addComponent(this.ifBracesCombo, GroupLayout.Alignment.TRAILING, -2, -1, -2).addComponent(this.forBracesCombo, GroupLayout.Alignment.TRAILING, -2, -1, -2).addComponent(this.whileBracesCombo, GroupLayout.Alignment.TRAILING, -2, -1, -2)).addComponent(this.doWhileBracesCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)).addComponent(this.methodDeclCombo, -2, -1, -2))).addGroup(layout.createSequentialGroup().addComponent(this.bracesPlacementLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jSeparator1, -1, 103, 32767)).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.classDeclLabel)).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.specialElseIfCheckBox)).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.ifBracesLabel)).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.forBracesLabel)).addGroup(layout.createSequentialGroup().addComponent(this.bracesGenerationLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jSeparator2, -1, 98, 32767))).addContainerGap()));
        layout.linkSize(0, this.classDeclCombo, this.doWhileBracesCombo, this.forBracesCombo, this.ifBracesCombo, this.methodDeclCombo, this.otherCombo, this.whileBracesCombo);
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.bracesPlacementLabel).addComponent(this.jSeparator1, -2, 10, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.classDeclLabel, -2, 18, -2).addComponent(this.classDeclCombo, -2, -1, -2)).addGap(6, 6, 6).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.methodDeclLabel).addComponent(this.methodDeclCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.otherLabel).addComponent(this.otherCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.specialElseIfCheckBox).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGap(20, 20, 20).addComponent(this.bracesGenerationLabel)).addGroup(layout.createSequentialGroup().addGap(28, 28, 28).addComponent(this.jSeparator2, -2, -1, -2))).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.ifBracesLabel).addComponent(this.ifBracesCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.forBracesLabel, -2, 16, -2).addComponent(this.forBracesCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.whileBracesLabel).addComponent(this.whileBracesCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.doWhileBracesLabel).addComponent(this.doWhileBracesCombo, -2, -1, -2)).addContainerGap(-1, 32767)));
    }

    private void methodDeclComboActionPerformed(ActionEvent evt) {
    }

}

