/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 */
package org.netbeans.modules.java.ui;

import java.io.PrintStream;
import java.lang.reflect.Method;
import java.util.BitSet;
import java.util.EventListener;
import javax.swing.ListModel;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

public final class LazyListModel
implements ListModel,
Runnable,
ListDataListener {
    private static int NOT_TESTED = -32769;
    private static int EMPTY_VALUE = -32770;
    private static final boolean skipExpensiveAsserts = Boolean.getBoolean("org.openide.explorer.view.LazyListModel.skipExpensiveAsserts");
    private boolean log;
    private ListModel listModel;
    private Filter filter;
    private Object defaultValue;
    private EventListenerList list = new EventListenerList();
    private int originalSize;
    private int size;
    private int[] external;
    private BitSet checked;
    private int refused;
    private BitSet tested;
    private boolean markDirty;
    static Boolean CREATE;

    private LazyListModel(ListModel m, Filter f, double expectedRadio, Object defaultValue) {
        this.listModel = m;
        this.filter = f;
        this.defaultValue = defaultValue;
        m.addListDataListener(this);
    }

    final Filter getFilter() {
        return this.filter;
    }

    final boolean isComputed(int index) {
        return this.tested != null && this.tested.get(index);
    }

    private void markDirty() {
        this.markDirty = true;
        this.getFilter().scheduleUpdate(this);
    }

    @Override
    public void run() {
        if (!this.markDirty) {
            return;
        }
        this.markDirty = false;
        if (this.log) {
            System.err.println("updateYourAssumeptions ();");
        }
        this.updateYourAssumeptions();
    }

    private void notifyRemoval(int from, int to) {
        ListDataEvent ev = new ListDataEvent(this, 2, from, to - 1);
        LazyListModel.removeInterval(this.external, from, to);
        int cnt = to - from;
        this.size -= cnt;
        this.regenerateCheckedBitSet();
        this.fireChange(ev);
    }

    private void regenerateCheckedBitSet() {
        this.checked = new BitSet(this.size);
        for (int i = 0; i < this.size; ++i) {
            if (this.external[i] < 0) continue;
            this.checked.set(i);
        }
    }

    private int getExternal(int index) {
        if (index == this.size) {
            return this.originalSize;
        }
        if (index < 0) {
            return -1;
        }
        return this.external[index];
    }

    final void updateYourAssumeptions() {
        if (this.external == null) {
            return;
        }
        int sizeBefore = this.size;
        boolean notifiedRemovals = false;
        int i = 0;
        while (i < this.size) {
            while (this.getExternal(i) >= 0 && i < this.size) {
                ++i;
            }
            if (i == this.size) break;
            if (this.getExternal(i) == NOT_TESTED) {
                int minusOneIndex = i - 1;
                while (i < this.size && this.getExternal(i) == NOT_TESTED) {
                    ++i;
                }
                int count = i - minusOneIndex - 1;
                int from = this.getExternal(minusOneIndex) + 1;
                int to = this.getExternal(i);
                assert (from >= 0);
                assert (to >= 0);
                assert (to >= from);
                int howMuch = count - (to - from);
                if (howMuch <= 0) continue;
                this.notifyRemoval(i - howMuch, i);
                i -= howMuch;
                continue;
            }
            int minusTwoIndex = i;
            while (i < this.size && this.getExternal(i) == EMPTY_VALUE) {
                ++i;
            }
            this.notifyRemoval(minusTwoIndex, i);
            i = minusTwoIndex;
        }
        assert (this.externalContraints());
    }

    private boolean externalContraints() {
        assert (this.external != null);
        assert (this.external.length >= this.size);
        if (!skipExpensiveAsserts) {
            for (int i = 1; i < this.size; ++i) {
                assert (this.external[i - 1] != NOT_TESTED || this.external[i] != EMPTY_VALUE);
                assert (this.external[i - 1] != EMPTY_VALUE || this.external[i] != NOT_TESTED);
                assert (this.external[i] < 0 || this.external[i] > this.external[i - 1]);
                assert (this.external[i] < 0 == !this.checked.get(i));
            }
        }
        return true;
    }

    private static void removeInterval(int[] array, int index0, int index1) {
        assert (index0 < index1);
        System.arraycopy(array, index1, array, index0, array.length - index1);
    }

    public static LazyListModel create(ListModel listModel, Filter f, double expectedRadio, Object defValue) {
        return LazyListModel.create(listModel, f, expectedRadio, defValue, false);
    }

    static LazyListModel create(ListModel listModel, Filter f, double expectedRadio, Object defValue, boolean log) {
        LazyListModel lazy = new LazyListModel(listModel, f, expectedRadio, defValue);
        lazy.log = log;
        return lazy;
    }

    public static LazyListModel create(Children.Keys ch, ListModel model) {
        class NodeF
        implements Filter {
            private Method m;

            NodeF() {
            }

            @Override
            public boolean accept(Object obj) {
                return obj instanceof Node;
            }

            @Override
            public void scheduleUpdate(Runnable run) {
                try {
                    if (this.m == null) {
                        this.m = Children.Keys.class.getDeclaredMethod("updateMyAssumptions", Runnable.class);
                        this.m.setAccessible(true);
                    }
                    this.m.invoke((Object)Keys.this, run);
                }
                catch (Exception ex) {
                    IllegalStateException i = new IllegalStateException(ex.getMessage());
                    i.initCause(ex);
                    throw i;
                }
            }
        }
        return LazyListModel.create(model, ch.new NodeF(), 1.0, null);
    }

    @Override
    public void addListDataListener(ListDataListener l) {
        this.list.add(ListDataListener.class, l);
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
        this.list.remove(ListDataListener.class, l);
    }

    private void fireChange(ListDataEvent ev) {
        if (this.list.getListenerCount() == 0) {
            return;
        }
        Object[] arr = this.list.getListenerList();
        block5 : for (int i = arr.length - 1; i >= 0; i -= 2) {
            ListDataListener l = (ListDataListener)arr[i];
            switch (ev.getType()) {
                case 0: {
                    l.contentsChanged(ev);
                    continue block5;
                }
                case 1: {
                    l.intervalAdded(ev);
                    continue block5;
                }
                case 2: {
                    l.intervalRemoved(ev);
                    continue block5;
                }
                default: {
                    throw new IllegalArgumentException("Unknown type: " + ev.getType());
                }
            }
        }
    }

    private boolean accepted(int indx, Object[] result) {
        Object v = this.listModel.getElementAt(indx);
        this.tested.set(indx);
        if (this.filter.accept(v)) {
            result[0] = v;
            return true;
        }
        this.markDirty();
        return false;
    }

    private void initialize() {
        if (this.tested == null) {
            this.originalSize = this.listModel.getSize();
            this.size = this.listModel.getSize();
            this.tested = new BitSet(this.size);
            this.external = new int[this.size];
            for (int i = 0; i < this.size; ++i) {
                this.external[i] = NOT_TESTED;
            }
            this.checked = new BitSet(this.size);
        }
        assert (this.externalContraints());
    }

    public Object getElementAt(int index) {
        int minIndex;
        int maxIndex;
        this.initialize();
        if (this.log) {
            System.err.println("model.getElementAt (" + index + ");");
        }
        if (this.external[index] >= 0) {
            return this.listModel.getElementAt(this.external[index]);
        }
        if (this.external[index] == EMPTY_VALUE) {
            return this.defaultValue;
        }
        if (CREATE != null && !CREATE.booleanValue()) {
            assert (Thread.holdsLock(CREATE));
            return this.defaultValue;
        }
        for (minIndex = index; minIndex >= 0 && this.getExternal(minIndex) < 0; --minIndex) {
        }
        if (this.checked.get(index)) {
            maxIndex = index;
        } else {
            maxIndex = this.checked.nextSetBit(index);
            if (maxIndex == -1 || maxIndex > this.size) {
                maxIndex = this.size;
            }
        }
        int myMinIndex = this.getExternal(minIndex) + 1;
        int myMaxIndex = this.getExternal(maxIndex);
        assert (myMaxIndex >= myMaxIndex);
        if (myMaxIndex != myMinIndex) {
            Object[] result;
            int myIndex = myMinIndex + (index - minIndex) - 1;
            if (myIndex >= myMaxIndex) {
                myIndex = myMaxIndex - 1;
            }
            if (this.accepted(myIndex, result = new Object[1])) {
                assert (this.external[index] == NOT_TESTED);
                this.external[index] = myIndex;
                this.checked.set(index);
                return result[0];
            }
            boolean checkBefore = true;
            boolean checkAfter = true;
            int i = 1;
            while (checkAfter || checkBefore) {
                if (checkBefore) {
                    boolean bl = checkBefore = index - i >= minIndex && myIndex - i >= myMinIndex && this.getExternal(index - i) == NOT_TESTED;
                    if (checkBefore && this.accepted(myIndex - i, result)) {
                        this.external[index] = myIndex - i;
                        this.checked.set(index);
                        return result[0];
                    }
                }
                if (checkAfter) {
                    boolean bl = checkAfter = index + i < maxIndex && myIndex + i < myMaxIndex && this.getExternal(index + i) == NOT_TESTED;
                    if (checkAfter && this.accepted(myIndex + i, result)) {
                        this.external[index] = myIndex + i;
                        this.checked.set(index);
                        return result[0];
                    }
                }
                ++i;
            }
        }
        this.markDirty();
        for (int i = minIndex + 1; i < maxIndex; ++i) {
            assert (this.external[i] == NOT_TESTED);
            this.external[i] = EMPTY_VALUE;
        }
        this.checked.clear(minIndex + 1, maxIndex);
        assert (this.external[index] == EMPTY_VALUE);
        return this.defaultValue;
    }

    @Override
    public int getSize() {
        this.initialize();
        return this.size;
    }

    private static BitSet insertAt(BitSet b, int at, int len, int size) {
        BitSet before = b.get(0, at);
        BitSet res = new BitSet(size);
        res.or(before);
        int max = b.length();
        while (at < max) {
            res.set(at + len, b.get(at));
            ++at;
        }
        return res;
    }

    private static BitSet removeAt(BitSet b, int at, int len, int newSize) {
        BitSet clone = (BitSet)b.clone();
        int max = b.length();
        while (at < max) {
            clone.set(at, b.get(at + len));
            ++at;
        }
        clone.set(newSize, b.size(), false);
        return clone;
    }

    @Override
    public void contentsChanged(ListDataEvent listDataEvent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void intervalAdded(ListDataEvent listDataEvent) {
        int i;
        if (this.external == null) {
            return;
        }
        this.updateYourAssumeptions();
        int first = listDataEvent.getIndex0();
        int end = listDataEvent.getIndex1() + 1;
        int len = end - first;
        int newOriginalSize = this.originalSize + len;
        int newSize = this.size + len;
        this.tested = LazyListModel.insertAt(this.tested, first, len, newOriginalSize);
        int insert = this.findExternalIndex(first);
        int[] newExternal = new int[newSize];
        System.arraycopy(this.external, 0, newExternal, 0, insert);
        for (i = 0; i < len; ++i) {
            newExternal[insert + i] = NOT_TESTED;
        }
        for (i = insert + len; i < newExternal.length; ++i) {
            int v = this.external[i - len];
            newExternal[i] = v < 0 ? v : v + len;
        }
        this.external = newExternal;
        this.size = newSize;
        this.originalSize = newOriginalSize;
        this.regenerateCheckedBitSet();
        this.fireChange(new ListDataEvent(this, 1, insert, insert + len - 1));
        assert (this.externalContraints());
    }

    private int findExternalIndex(int myIndex) {
        int outIndex = 0;
        for (int i = -1; i < this.size; ++i) {
            outIndex = this.getExternal(i) == NOT_TESTED ? ++outIndex : this.getExternal(i);
            if (outIndex < myIndex) continue;
            return i;
        }
        return this.size;
    }

    @Override
    public void intervalRemoved(ListDataEvent listDataEvent) {
        if (this.external == null) {
            return;
        }
        this.updateYourAssumeptions();
        int first = listDataEvent.getIndex0();
        int end = listDataEvent.getIndex1() + 1;
        int len = end - first;
        int newOriginalSize = this.originalSize - len;
        int f = this.findExternalIndex(first);
        int e = this.findExternalIndex(end);
        assert (f >= 0);
        assert (e >= f);
        int outLen = e - f;
        int[] newExternal = (int[])this.external.clone();
        for (int i = e; i < this.size; ++i) {
            int v = this.external[i];
            newExternal[i - outLen] = v < 0 ? v : v - len;
            this.checked.set(i - outLen, v >= 0);
        }
        this.external = newExternal;
        this.size -= outLen;
        this.originalSize = newOriginalSize;
        if (outLen != 0) {
            this.fireChange(new ListDataEvent(this, 2, f, e - 1));
        }
        assert (this.externalContraints());
    }

    public static interface Filter {
        public boolean accept(Object var1);

        public void scheduleUpdate(Runnable var1);
    }

}

