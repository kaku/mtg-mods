/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.PrimitiveTypeTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.TypeParameterTree
 *  com.sun.source.tree.VariableTree
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer$Factory
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.ui;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.PrimitiveTypeTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TypeParameterTree;
import com.sun.source.tree.VariableTree;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.prefs.Preferences;
import javax.lang.model.element.Modifier;
import javax.lang.model.type.TypeKind;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.border.Border;
import org.netbeans.api.java.source.CodeStyle;
import org.netbeans.api.java.source.CodeStyleUtils;
import org.netbeans.api.java.source.GeneratorUtilities;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.TreeMaker;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.modules.java.ui.FmtOptions;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class FmtNaming
extends JPanel
implements Runnable {
    private JLabel fieldLabel;
    private JTextField fieldPrefixField;
    private JTextField fieldSuffixField;
    private JPanel jPanel1;
    private JLabel localVarLabel;
    private JTextField localVarPrefixField;
    private JTextField localVarSuffixField;
    private JLabel namingConventionsLabel;
    private JLabel parameterLabel;
    private JTextField parameterPrefixField;
    private JTextField parameterSuffixField;
    private JCheckBox preferLongerNamesCheckBox;
    private JLabel prefixLabel;
    private JLabel staticFieldLabel;
    private JTextField staticFieldPrefixField;
    private JTextField staticFieldSuffixField;
    private JLabel suffixLabel;

    public FmtNaming() {
        this.initComponents();
        this.preferLongerNamesCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "preferLongerNames");
        this.preferLongerNamesCheckBox.setVisible(false);
        this.fieldPrefixField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "fieldNamePrefix");
        this.fieldSuffixField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "fieldNameSuffix");
        this.staticFieldPrefixField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "staticFieldNamePrefix");
        this.staticFieldSuffixField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "staticFieldNameSuffix");
        this.parameterPrefixField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "parameterNamePrefix");
        this.parameterSuffixField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "parameterNameSuffix");
        this.localVarPrefixField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "localVarNamePrefix");
        this.localVarSuffixField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "localVarNameSuffix");
    }

    public static PreferencesCustomizer.Factory getController() {
        return new PreferencesCustomizer.Factory(){

            public PreferencesCustomizer create(Preferences preferences) {
                NamingCategorySupport support = new NamingCategorySupport(preferences, new FmtNaming());
                ((Runnable)((Object)support.panel)).run();
                return support;
            }
        };
    }

    private void initComponents() {
        this.namingConventionsLabel = new JLabel();
        this.preferLongerNamesCheckBox = new JCheckBox();
        this.jPanel1 = new JPanel();
        this.prefixLabel = new JLabel();
        this.suffixLabel = new JLabel();
        this.fieldLabel = new JLabel();
        this.fieldPrefixField = new JTextField();
        this.fieldSuffixField = new JTextField();
        this.staticFieldLabel = new JLabel();
        this.staticFieldPrefixField = new JTextField();
        this.staticFieldSuffixField = new JTextField();
        this.parameterLabel = new JLabel();
        this.parameterPrefixField = new JTextField();
        this.parameterSuffixField = new JTextField();
        this.localVarLabel = new JLabel();
        this.localVarSuffixField = new JTextField();
        this.localVarPrefixField = new JTextField();
        this.setName(NbBundle.getMessage(FmtNaming.class, (String)"LBL_Naming"));
        this.setOpaque(false);
        Mnemonics.setLocalizedText((JLabel)this.namingConventionsLabel, (String)NbBundle.getMessage(FmtNaming.class, (String)"LBL_gen_Naming"));
        Mnemonics.setLocalizedText((AbstractButton)this.preferLongerNamesCheckBox, (String)NbBundle.getMessage(FmtNaming.class, (String)"LBL_gen_PreferLongerNames"));
        this.preferLongerNamesCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.preferLongerNamesCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.preferLongerNamesCheckBox.setOpaque(false);
        this.jPanel1.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this.prefixLabel, (String)NbBundle.getMessage(FmtNaming.class, (String)"LBL_gen_Prefix"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.insets = new Insets(0, 8, 4, 0);
        this.jPanel1.add((Component)this.prefixLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.suffixLabel, (String)NbBundle.getMessage(FmtNaming.class, (String)"LBL_gen_Suffix"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.insets = new Insets(0, 8, 4, 0);
        this.jPanel1.add((Component)this.suffixLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.fieldLabel, (String)NbBundle.getMessage(FmtNaming.class, (String)"LBL_gen_Field"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 17;
        this.jPanel1.add((Component)this.fieldLabel, gridBagConstraints);
        this.fieldPrefixField.setColumns(5);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.insets = new Insets(4, 8, 4, 0);
        this.jPanel1.add((Component)this.fieldPrefixField, gridBagConstraints);
        this.fieldSuffixField.setColumns(5);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.insets = new Insets(4, 8, 4, 0);
        this.jPanel1.add((Component)this.fieldSuffixField, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.staticFieldLabel, (String)NbBundle.getMessage(FmtNaming.class, (String)"LBL_gen_StaticField"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 17;
        this.jPanel1.add((Component)this.staticFieldLabel, gridBagConstraints);
        this.staticFieldPrefixField.setColumns(5);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.insets = new Insets(4, 8, 4, 0);
        this.jPanel1.add((Component)this.staticFieldPrefixField, gridBagConstraints);
        this.staticFieldSuffixField.setColumns(5);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.insets = new Insets(4, 8, 4, 0);
        this.jPanel1.add((Component)this.staticFieldSuffixField, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.parameterLabel, (String)NbBundle.getMessage(FmtNaming.class, (String)"LBL_gen_Parameter"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = 17;
        this.jPanel1.add((Component)this.parameterLabel, gridBagConstraints);
        this.parameterPrefixField.setColumns(5);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.insets = new Insets(4, 8, 4, 0);
        this.jPanel1.add((Component)this.parameterPrefixField, gridBagConstraints);
        this.parameterSuffixField.setColumns(5);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.insets = new Insets(4, 8, 4, 0);
        this.jPanel1.add((Component)this.parameterSuffixField, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.localVarLabel, (String)NbBundle.getMessage(FmtNaming.class, (String)"LBL_gen_LocalVariable"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(4, 0, 0, 0);
        this.jPanel1.add((Component)this.localVarLabel, gridBagConstraints);
        this.localVarSuffixField.setColumns(5);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.insets = new Insets(4, 8, 0, 0);
        this.jPanel1.add((Component)this.localVarSuffixField, gridBagConstraints);
        this.localVarPrefixField.setColumns(5);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.insets = new Insets(4, 8, 0, 0);
        this.jPanel1.add((Component)this.localVarPrefixField, gridBagConstraints);
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.namingConventionsLabel).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.preferLongerNamesCheckBox).addComponent(this.jPanel1, -2, 274, -2)))).addContainerGap(-1, 32767)));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.namingConventionsLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.preferLongerNamesCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jPanel1, -2, 144, -2).addContainerGap(-1, 32767)));
    }

    @Override
    public void run() {
    }

    private static final class NamingCategorySupport
    extends FmtOptions.DocumentCategorySupport {
        private NamingCategorySupport(Preferences preferences, JPanel panel) {
            super(preferences, "naming", panel, NbBundle.getMessage(FmtNaming.class, (String)"SAMPLE_Naming"), {"blankLinesBeforeFields", "1"});
        }

        @Override
        protected void doModification(ResultIterator resultIterator) throws Exception {
            CodeStyle codeStyle = FmtOptions.codeStyleProducer.create(this.previewPrefs);
            WorkingCopy copy = WorkingCopy.get(resultIterator.getParserResult());
            copy.toPhase(JavaSource.Phase.RESOLVED);
            TreeMaker tm = copy.getTreeMaker();
            GeneratorUtilities gu = GeneratorUtilities.get(copy);
            CompilationUnitTree cut = copy.getCompilationUnit();
            ClassTree ct = (ClassTree)cut.getTypeDecls().get(0);
            ArrayList<Object> members = new ArrayList<Object>();
            String name = CodeStyleUtils.addPrefixSuffix("name", codeStyle.getFieldNamePrefix(), codeStyle.getFieldNameSuffix());
            VariableTree field = tm.Variable(tm.Modifiers(Collections.singleton(Modifier.PRIVATE)), name, tm.Type("String"), null);
            members.add((Object)field);
            String cond = CodeStyleUtils.addPrefixSuffix("cond", codeStyle.getFieldNamePrefix(), codeStyle.getFieldNameSuffix());
            VariableTree booleanField = tm.Variable(tm.Modifiers(Collections.singleton(Modifier.PRIVATE)), cond, (Tree)tm.PrimitiveType(TypeKind.BOOLEAN), null);
            members.add((Object)booleanField);
            members.add((Object)gu.createConstructor(ct, Collections.singletonList(field)));
            members.add((Object)gu.createGetter(field));
            members.add((Object)gu.createSetter(ct, field));
            members.add((Object)gu.createGetter(booleanField));
            members.add((Object)gu.createSetter(ct, booleanField));
            ModifiersTree mods = tm.Modifiers(EnumSet.of(Modifier.PRIVATE, Modifier.STATIC));
            ClassTree nested = tm.Class(mods, "Nested", Collections.emptyList(), null, Collections.emptyList(), Collections.emptyList());
            members.add((Object)nested);
            IdentifierTree nestedId = tm.Identifier("Nested");
            String instance = CodeStyleUtils.addPrefixSuffix("instance", codeStyle.getStaticFieldNamePrefix(), codeStyle.getStaticFieldNameSuffix());
            VariableTree staticField = tm.Variable(mods, instance, (Tree)nestedId, null);
            members.add((Object)staticField);
            members.add((Object)gu.createGetter(staticField));
            members.add((Object)gu.createSetter(ct, staticField));
            ClassTree newCT = gu.insertClassMembers(ct, members);
            copy.rewrite((Tree)ct, (Tree)newCT);
        }
    }

}

