/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.editor.indent.api.Reformat
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer$Factory
 *  org.netbeans.modules.options.editor.spi.PreviewProvider
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.openide.cookies.SaveCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.java.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import org.netbeans.api.java.source.CodeStyle;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.editor.indent.api.Reformat;
import org.netbeans.modules.java.source.save.Reformatter;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.netbeans.modules.options.editor.spi.PreviewProvider;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

public class FmtOptions {
    public static final String expandTabToSpaces = "expand-tabs";
    public static final String tabSize = "tab-size";
    public static final String spacesPerTab = "spaces-per-tab";
    public static final String indentSize = "indent-shift-width";
    public static final String continuationIndentSize = "continuationIndentSize";
    public static final String labelIndent = "labelIndent";
    public static final String absoluteLabelIndent = "absoluteLabelIndent";
    public static final String indentTopLevelClassMembers = "indentTopLevelClassMembers";
    public static final String indentCasesFromSwitch = "indentCasesFromSwitch";
    public static final String rightMargin = "text-limit-width";
    public static final String addLeadingStarInComment = "addLeadingStarInComment";
    public static final String preferLongerNames = "preferLongerNames";
    public static final String fieldNamePrefix = "fieldNamePrefix";
    public static final String fieldNameSuffix = "fieldNameSuffix";
    public static final String staticFieldNamePrefix = "staticFieldNamePrefix";
    public static final String staticFieldNameSuffix = "staticFieldNameSuffix";
    public static final String parameterNamePrefix = "parameterNamePrefix";
    public static final String parameterNameSuffix = "parameterNameSuffix";
    public static final String localVarNamePrefix = "localVarNamePrefix";
    public static final String localVarNameSuffix = "localVarNameSuffix";
    public static final String qualifyFieldAccess = "qualifyFieldAccess";
    public static final String useIsForBooleanGetters = "useIsForBooleanGetters";
    public static final String addOverrideAnnotation = "addOverrideAnnotation";
    public static final String makeLocalVarsFinal = "makeLocalVarsFinal";
    public static final String makeParametersFinal = "makeParametersFinal";
    public static final String classMembersOrder = "classMembersOrder";
    public static final String sortMembersByVisibility = "sortMembersByVisibility";
    public static final String visibilityOrder = "visibilityOrder";
    public static final String keepGettersAndSettersTogether = "keepGettersAndSettersTogether";
    public static final String sortMembersInGroups = "sortMembersInGroups";
    public static final String classMemberInsertionPoint = "classMemberInsertionPoint";
    public static final String classDeclBracePlacement = "classDeclBracePlacement";
    public static final String methodDeclBracePlacement = "methodDeclBracePlacement";
    public static final String otherBracePlacement = "otherBracePlacement";
    public static final String specialElseIf = "specialElseIf";
    public static final String redundantIfBraces = "redundantIfBraces";
    public static final String redundantForBraces = "redundantForBraces";
    public static final String redundantWhileBraces = "redundantWhileBraces";
    public static final String redundantDoWhileBraces = "redundantDoWhileBraces";
    public static final String alignMultilineMethodParams = "alignMultilineMethodParams";
    public static final String alignMultilineLambdaParams = "alignMultilineLambdaParams";
    public static final String alignMultilineCallArgs = "alignMultilineCallArgs";
    public static final String alignMultilineAnnotationArgs = "alignMultilineAnnotationArgs";
    public static final String alignMultilineImplements = "alignMultilineImplements";
    public static final String alignMultilineThrows = "alignMultilineThrows";
    public static final String alignMultilineParenthesized = "alignMultilineParenthesized";
    public static final String alignMultilineBinaryOp = "alignMultilineBinaryOp";
    public static final String alignMultilineTernaryOp = "alignMultilineTernaryOp";
    public static final String alignMultilineAssignment = "alignMultilineAssignment";
    public static final String alignMultilineTryResources = "alignMultilineTryResources";
    public static final String alignMultilineDisjunctiveCatchTypes = "alignMultilineDisjunctiveCatchTypes";
    public static final String alignMultilineFor = "alignMultilineFor";
    public static final String alignMultilineArrayInit = "alignMultilineArrayInit";
    public static final String placeElseOnNewLine = "placeElseOnNewLine";
    public static final String placeWhileOnNewLine = "placeWhileOnNewLine";
    public static final String placeCatchOnNewLine = "placeCatchOnNewLine";
    public static final String placeFinallyOnNewLine = "placeFinallyOnNewLine";
    public static final String placeNewLineAfterModifiers = "placeNewLineAfterModifiers";
    public static final String wrapExtendsImplementsKeyword = "wrapExtendsImplementsKeyword";
    public static final String wrapExtendsImplementsList = "wrapExtendsImplementsList";
    public static final String wrapMethodParams = "wrapMethodParams";
    public static final String wrapLambdaParams = "wrapLambdaParams";
    public static final String wrapLambdaArrow = "wrapLambdaArrow";
    public static final String wrapAfterLambdaArrow = "wrapAfterLambdaArrow";
    public static final String wrapThrowsKeyword = "wrapThrowsKeyword";
    public static final String wrapThrowsList = "wrapThrowsList";
    public static final String wrapMethodCallArgs = "wrapMethodCallArgs";
    public static final String wrapAnnotationArgs = "wrapAnnotationArgs";
    public static final String wrapChainedMethodCalls = "wrapChainedMethodCalls";
    public static final String wrapAfterDotInChainedMethodCalls = "wrapAfterDotInChainedMethodCalls";
    public static final String wrapArrayInit = "wrapArrayInit";
    public static final String wrapTryResources = "wrapTryResources";
    public static final String wrapDisjunctiveCatchTypes = "wrapDisjunctiveCatchTypes";
    public static final String wrapFor = "wrapFor";
    public static final String wrapForStatement = "wrapForStatement";
    public static final String wrapIfStatement = "wrapIfStatement";
    public static final String wrapWhileStatement = "wrapWhileStatement";
    public static final String wrapDoWhileStatement = "wrapDoWhileStatement";
    public static final String wrapCaseStatements = "wrapCaseStatements";
    public static final String wrapAssert = "wrapAssert";
    public static final String wrapEnumConstants = "wrapEnumConstants";
    public static final String wrapAnnotations = "wrapAnnotations";
    public static final String wrapBinaryOps = "wrapBinaryOps";
    public static final String wrapAfterBinaryOps = "wrapAfterBinaryOps";
    public static final String wrapTernaryOps = "wrapTernaryOps";
    public static final String wrapAfterTernaryOps = "wrapAfterTernaryOps";
    public static final String wrapAssignOps = "wrapAssignOps";
    public static final String wrapAfterAssignOps = "wrapAfterAssignOps";
    public static final String blankLinesInDeclarations = "blankLinesInDeclarations";
    public static final String blankLinesInCode = "blankLinesInCode";
    public static final String blankLinesBeforePackage = "blankLinesBeforePackage";
    public static final String blankLinesAfterPackage = "blankLinesAfterPackage";
    public static final String blankLinesBeforeImports = "blankLinesBeforeImports";
    public static final String blankLinesAfterImports = "blankLinesAfterImports";
    public static final String blankLinesBeforeClass = "blankLinesBeforeClass";
    public static final String blankLinesAfterClass = "blankLinesAfterClass";
    public static final String blankLinesAfterClassHeader = "blankLinesAfterClassHeader";
    public static final String blankLinesAfterAnonymousClassHeader = "blankLinesAfterAnonymousClassHeader";
    public static final String blankLinesBeforeClassClosingBrace = "blankLinesBeforeClassClosingBrace";
    public static final String blankLinesBeforeAnonymousClosingBrace = "blankLinesBeforeAnonymousClassClosingBrace";
    public static final String blankLinesBeforeFields = "blankLinesBeforeFields";
    public static final String blankLinesAfterFields = "blankLinesAfterFields";
    public static final String blankLinesBeforeMethods = "blankLinesBeforeMethods";
    public static final String blankLinesAfterMethods = "blankLinesAfterMethods";
    public static final String spaceBeforeWhile = "spaceBeforeWhile";
    public static final String spaceBeforeElse = "spaceBeforeElse";
    public static final String spaceBeforeCatch = "spaceBeforeCatch";
    public static final String spaceBeforeFinally = "spaceBeforeFinally";
    public static final String spaceBeforeMethodDeclParen = "spaceBeforeMethodDeclParen";
    public static final String spaceBeforeMethodCallParen = "spaceBeforeMethodCallParen";
    public static final String spaceBeforeIfParen = "spaceBeforeIfParen";
    public static final String spaceBeforeForParen = "spaceBeforeForParen";
    public static final String spaceBeforeWhileParen = "spaceBeforeWhileParen";
    public static final String spaceBeforeTryParen = "spaceBeforeTryParen";
    public static final String spaceBeforeCatchParen = "spaceBeforeCatchParen";
    public static final String spaceBeforeSwitchParen = "spaceBeforeSwitchParen";
    public static final String spaceBeforeSynchronizedParen = "spaceBeforeSynchronizedParen";
    public static final String spaceBeforeAnnotationParen = "spaceBeforeAnnotationParen";
    public static final String spaceAroundUnaryOps = "spaceAroundUnaryOps";
    public static final String spaceAroundBinaryOps = "spaceAroundBinaryOps";
    public static final String spaceAroundTernaryOps = "spaceAroundTernaryOps";
    public static final String spaceAroundAssignOps = "spaceAroundAssignOps";
    public static final String spaceAroundAnnotationValueAssignOps = "spaceAroundAnnotationValueAssignOps";
    public static final String spaceAroundLambdaArrow = "spaceAroundLambdaArrow";
    public static final String spaceAroundMethodReferenceDoubleColon = "spaceAroundMethodReferenceDoubleColon";
    public static final String spaceBeforeClassDeclLeftBrace = "spaceBeforeClassDeclLeftBrace";
    public static final String spaceBeforeMethodDeclLeftBrace = "spaceBeforeMethodDeclLeftBrace";
    public static final String spaceBeforeIfLeftBrace = "spaceBeforeIfLeftBrace";
    public static final String spaceBeforeElseLeftBrace = "spaceBeforeElseLeftBrace";
    public static final String spaceBeforeWhileLeftBrace = "spaceBeforeWhileLeftBrace";
    public static final String spaceBeforeForLeftBrace = "spaceBeforeForLeftBrace";
    public static final String spaceBeforeDoLeftBrace = "spaceBeforeDoLeftBrace";
    public static final String spaceBeforeSwitchLeftBrace = "spaceBeforeSwitchLeftBrace";
    public static final String spaceBeforeTryLeftBrace = "spaceBeforeTryLeftBrace";
    public static final String spaceBeforeCatchLeftBrace = "spaceBeforeCatchLeftBrace";
    public static final String spaceBeforeFinallyLeftBrace = "spaceBeforeFinallyLeftBrace";
    public static final String spaceBeforeSynchronizedLeftBrace = "spaceBeforeSynchronizedLeftBrace";
    public static final String spaceBeforeStaticInitLeftBrace = "spaceBeforeStaticInitLeftBrace";
    public static final String spaceBeforeArrayInitLeftBrace = "spaceBeforeArrayInitLeftBrace";
    public static final String spaceWithinParens = "spaceWithinParens";
    public static final String spaceWithinMethodDeclParens = "spaceWithinMethodDeclParens";
    public static final String spaceWithinLambdaParens = "spaceWithinLambdaParens";
    public static final String spaceWithinMethodCallParens = "spaceWithinMethodCallParens";
    public static final String spaceWithinIfParens = "spaceWithinIfParens";
    public static final String spaceWithinForParens = "spaceWithinForParens";
    public static final String spaceWithinWhileParens = "spaceWithinWhileParens";
    public static final String spaceWithinSwitchParens = "spaceWithinSwitchParens";
    public static final String spaceWithinTryParens = "spaceWithinTryParens";
    public static final String spaceWithinCatchParens = "spaceWithinCatchParens";
    public static final String spaceWithinSynchronizedParens = "spaceWithinSynchronizedParens";
    public static final String spaceWithinTypeCastParens = "spaceWithinTypeCastParens";
    public static final String spaceWithinAnnotationParens = "spaceWithinAnnotationParens";
    public static final String spaceWithinBraces = "spaceWithinBraces";
    public static final String spaceWithinArrayInitBrackets = "spaceWithinArrayInitBrackets";
    public static final String spaceWithinArrayIndexBrackets = "spaceWithinArrayIndexBrackets";
    public static final String spaceBeforeComma = "spaceBeforeComma";
    public static final String spaceAfterComma = "spaceAfterComma";
    public static final String spaceBeforeSemi = "spaceBeforeSemi";
    public static final String spaceAfterSemi = "spaceAfterSemi";
    public static final String spaceBeforeColon = "spaceBeforeColon";
    public static final String spaceAfterColon = "spaceAfterColon";
    public static final String spaceAfterTypeCast = "spaceAfterTypeCast";
    public static final String useSingleClassImport = "useSingleClassImport";
    public static final String usePackageImport = "usePackageImport";
    public static final String useFQNs = "useFQNs";
    public static final String importInnerClasses = "importInnerClasses";
    public static final String preferStaticImports = "preferStaticImports";
    public static final String allowConvertToStarImport = "allowConvertToStarImport";
    public static final String countForUsingStarImport = "countForUsingStarImport";
    public static final String allowConvertToStaticStarImport = "allowConvertToStaticStarImport";
    public static final String countForUsingStaticStarImport = "countForUsingStaticStarImport";
    public static final String packagesForStarImport = "packagesForStarImport";
    public static final String separateStaticImports = "separateStaticImports";
    public static final String importGroupsOrder = "importGroupsOrder";
    public static final String separateImportGroups = "separateImportGroups";
    public static final String enableCommentFormatting = "enableCommentFormatting";
    public static final String enableBlockCommentFormatting = "enableBlockCommentFormatting";
    public static final String wrapCommentText = "wrapCommentText";
    public static final String wrapOneLineComment = "wrapOneLineComment";
    public static final String preserveNewLinesInComments = "preserveNewLinesInComments";
    public static final String blankLineAfterJavadocDescription = "blankLineAfterJavadocDescription";
    public static final String blankLineAfterJavadocParameterDescriptions = "blankLineAfterJavadocParameterDescriptions";
    public static final String blankLineAfterJavadocReturnTag = "blankLineAfterJavadocReturnTag";
    public static final String generateParagraphTagOnBlankLines = "generateParagraphTagOnBlankLines";
    public static final String alignJavadocParameterDescriptions = "alignJavadocParameterDescriptions";
    public static final String alignJavadocReturnDescription = "alignJavadocReturnDescription";
    public static final String alignJavadocExceptionDescriptions = "alignJavadocExceptionDescriptions";
    public static CodeStyleProducer codeStyleProducer;
    static final String CODE_STYLE_PROFILE = "CodeStyle";
    static final String DEFAULT_PROFILE = "default";
    static final String PROJECT_PROFILE = "project";
    static final String JAVA_MIME_TYPE = "text/x-java";
    static final String usedProfile = "usedProfile";
    private static final String JAVA = "text/x-java";
    private static final String TRUE = "true";
    private static final String FALSE = "false";
    private static final String WRAP_ALWAYS;
    private static final String WRAP_IF_LONG;
    private static final String WRAP_NEVER;
    private static final String BP_NEW_LINE;
    private static final String BP_NEW_LINE_HALF_INDENTED;
    private static final String BP_NEW_LINE_INDENTED;
    private static final String BP_SAME_LINE;
    private static final String BGS_ELIMINATE;
    private static final String BGS_LEAVE_ALONE;
    private static final String BGS_GENERATE;
    private static final String IP_CARET;
    private static final String IP_FIRST;
    private static final String IP_LAST;
    private static Map<String, String> defaults;
    private static final RequestProcessor REFORMAT_RP;

    private FmtOptions() {
    }

    public static int getDefaultAsInt(String key) {
        return Integer.parseInt(defaults.get(key));
    }

    public static boolean getDefaultAsBoolean(String key) {
        return Boolean.parseBoolean(defaults.get(key));
    }

    public static String getDefaultAsString(String key) {
        return defaults.get(key);
    }

    public static boolean isInteger(String optionID) {
        String value = defaults.get(optionID);
        try {
            Integer.parseInt(value);
            return true;
        }
        catch (NumberFormatException numberFormatException) {
            return false;
        }
    }

    private static void createDefaults() {
        String[][] defaultValues = new String[][]{{"expand-tabs", "true"}, {"tab-size", "4"}, {"spaces-per-tab", "4"}, {"indent-shift-width", "4"}, {"continuationIndentSize", "8"}, {"labelIndent", "0"}, {"absoluteLabelIndent", "false"}, {"indentTopLevelClassMembers", "true"}, {"indentCasesFromSwitch", "true"}, {"text-limit-width", "80"}, {"addLeadingStarInComment", "true"}, {"preferLongerNames", "true"}, {"fieldNamePrefix", ""}, {"fieldNameSuffix", ""}, {"staticFieldNamePrefix", ""}, {"staticFieldNameSuffix", ""}, {"parameterNamePrefix", ""}, {"parameterNameSuffix", ""}, {"localVarNamePrefix", ""}, {"localVarNameSuffix", ""}, {"qualifyFieldAccess", "false"}, {"useIsForBooleanGetters", "true"}, {"addOverrideAnnotation", "true"}, {"makeLocalVarsFinal", "false"}, {"makeParametersFinal", "false"}, {"classMembersOrder", "STATIC FIELD;STATIC_INIT;STATIC METHOD;FIELD;INSTANCE_INIT;CONSTRUCTOR;METHOD;STATIC CLASS;CLASS"}, {"sortMembersByVisibility", "false"}, {"visibilityOrder", "PUBLIC;PRIVATE;PROTECTED;DEFAULT"}, {"keepGettersAndSettersTogether", "false"}, {"sortMembersInGroups", "false"}, {"classMemberInsertionPoint", IP_CARET}, {"classDeclBracePlacement", BP_SAME_LINE}, {"methodDeclBracePlacement", BP_SAME_LINE}, {"otherBracePlacement", BP_SAME_LINE}, {"specialElseIf", "true"}, {"redundantIfBraces", BGS_GENERATE}, {"redundantForBraces", BGS_GENERATE}, {"redundantWhileBraces", BGS_GENERATE}, {"redundantDoWhileBraces", BGS_GENERATE}, {"alignMultilineMethodParams", "false"}, {"alignMultilineLambdaParams", "false"}, {"alignMultilineCallArgs", "false"}, {"alignMultilineAnnotationArgs", "false"}, {"alignMultilineImplements", "false"}, {"alignMultilineThrows", "false"}, {"alignMultilineParenthesized", "false"}, {"alignMultilineBinaryOp", "false"}, {"alignMultilineTernaryOp", "false"}, {"alignMultilineAssignment", "false"}, {"alignMultilineTryResources", "false"}, {"alignMultilineDisjunctiveCatchTypes", "false"}, {"alignMultilineFor", "false"}, {"alignMultilineArrayInit", "false"}, {"placeElseOnNewLine", "false"}, {"placeWhileOnNewLine", "false"}, {"placeCatchOnNewLine", "false"}, {"placeFinallyOnNewLine", "false"}, {"placeNewLineAfterModifiers", "false"}, {"wrapExtendsImplementsKeyword", WRAP_NEVER}, {"wrapExtendsImplementsList", WRAP_NEVER}, {"wrapMethodParams", WRAP_NEVER}, {"wrapLambdaParams", WRAP_NEVER}, {"wrapLambdaArrow", WRAP_NEVER}, {"wrapAfterLambdaArrow", "false"}, {"wrapThrowsKeyword", WRAP_NEVER}, {"wrapThrowsList", WRAP_NEVER}, {"wrapMethodCallArgs", WRAP_NEVER}, {"wrapAnnotationArgs", WRAP_NEVER}, {"wrapChainedMethodCalls", WRAP_NEVER}, {"wrapAfterDotInChainedMethodCalls", "true"}, {"wrapArrayInit", WRAP_NEVER}, {"wrapTryResources", WRAP_NEVER}, {"wrapDisjunctiveCatchTypes", WRAP_NEVER}, {"wrapFor", WRAP_NEVER}, {"wrapForStatement", WRAP_ALWAYS}, {"wrapIfStatement", WRAP_ALWAYS}, {"wrapWhileStatement", WRAP_ALWAYS}, {"wrapDoWhileStatement", WRAP_ALWAYS}, {"wrapCaseStatements", WRAP_ALWAYS}, {"wrapAssert", WRAP_NEVER}, {"wrapEnumConstants", WRAP_NEVER}, {"wrapAnnotations", WRAP_ALWAYS}, {"wrapBinaryOps", WRAP_NEVER}, {"wrapAfterBinaryOps", "false"}, {"wrapTernaryOps", WRAP_NEVER}, {"wrapAfterTernaryOps", "false"}, {"wrapAssignOps", WRAP_NEVER}, {"wrapAfterAssignOps", "false"}, {"blankLinesInDeclarations", "1"}, {"blankLinesInCode", "1"}, {"blankLinesBeforePackage", "0"}, {"blankLinesAfterPackage", "1"}, {"blankLinesBeforeImports", "1"}, {"blankLinesAfterImports", "1"}, {"blankLinesBeforeClass", "1"}, {"blankLinesAfterClass", "0"}, {"blankLinesAfterClassHeader", "1"}, {"blankLinesAfterAnonymousClassHeader", "0"}, {"blankLinesBeforeClassClosingBrace", "0"}, {"blankLinesBeforeAnonymousClassClosingBrace", "0"}, {"blankLinesBeforeFields", "0"}, {"blankLinesAfterFields", "0"}, {"blankLinesBeforeMethods", "1"}, {"blankLinesAfterMethods", "0"}, {"spaceBeforeWhile", "true"}, {"spaceBeforeElse", "true"}, {"spaceBeforeCatch", "true"}, {"spaceBeforeFinally", "true"}, {"spaceBeforeMethodDeclParen", "false"}, {"spaceBeforeMethodCallParen", "false"}, {"spaceBeforeIfParen", "true"}, {"spaceBeforeForParen", "true"}, {"spaceBeforeWhileParen", "true"}, {"spaceBeforeTryParen", "true"}, {"spaceBeforeCatchParen", "true"}, {"spaceBeforeSwitchParen", "true"}, {"spaceBeforeSynchronizedParen", "true"}, {"spaceBeforeAnnotationParen", "false"}, {"spaceAroundUnaryOps", "false"}, {"spaceAroundBinaryOps", "true"}, {"spaceAroundTernaryOps", "true"}, {"spaceAroundAssignOps", "true"}, {"spaceAroundAnnotationValueAssignOps", "true"}, {"spaceAroundLambdaArrow", "true"}, {"spaceAroundMethodReferenceDoubleColon", "false"}, {"spaceBeforeClassDeclLeftBrace", "true"}, {"spaceBeforeMethodDeclLeftBrace", "true"}, {"spaceBeforeIfLeftBrace", "true"}, {"spaceBeforeElseLeftBrace", "true"}, {"spaceBeforeWhileLeftBrace", "true"}, {"spaceBeforeForLeftBrace", "true"}, {"spaceBeforeDoLeftBrace", "true"}, {"spaceBeforeSwitchLeftBrace", "true"}, {"spaceBeforeTryLeftBrace", "true"}, {"spaceBeforeCatchLeftBrace", "true"}, {"spaceBeforeFinallyLeftBrace", "true"}, {"spaceBeforeSynchronizedLeftBrace", "true"}, {"spaceBeforeStaticInitLeftBrace", "true"}, {"spaceBeforeArrayInitLeftBrace", "false"}, {"spaceWithinParens", "false"}, {"spaceWithinMethodDeclParens", "false"}, {"spaceWithinLambdaParens", "false"}, {"spaceWithinMethodCallParens", "false"}, {"spaceWithinIfParens", "false"}, {"spaceWithinForParens", "false"}, {"spaceWithinWhileParens", "false"}, {"spaceWithinSwitchParens", "false"}, {"spaceWithinTryParens", "false"}, {"spaceWithinCatchParens", "false"}, {"spaceWithinSynchronizedParens", "false"}, {"spaceWithinTypeCastParens", "false"}, {"spaceWithinAnnotationParens", "false"}, {"spaceWithinBraces", "false"}, {"spaceWithinArrayInitBrackets", "false"}, {"spaceWithinArrayIndexBrackets", "false"}, {"spaceBeforeComma", "false"}, {"spaceAfterComma", "true"}, {"spaceBeforeSemi", "false"}, {"spaceAfterSemi", "true"}, {"spaceBeforeColon", "true"}, {"spaceAfterColon", "true"}, {"spaceAfterTypeCast", "true"}, {"useSingleClassImport", "true"}, {"usePackageImport", "false"}, {"useFQNs", "false"}, {"importInnerClasses", "false"}, {"preferStaticImports", "false"}, {"allowConvertToStarImport", "false"}, {"countForUsingStarImport", "5"}, {"allowConvertToStaticStarImport", "false"}, {"countForUsingStaticStarImport", "3"}, {"packagesForStarImport", ""}, {"separateStaticImports", "false"}, {"importGroupsOrder", ""}, {"separateImportGroups", "true"}, {"enableCommentFormatting", "true"}, {"enableBlockCommentFormatting", "false"}, {"wrapCommentText", "true"}, {"wrapOneLineComment", "true"}, {"preserveNewLinesInComments", "false"}, {"blankLineAfterJavadocDescription", "true"}, {"blankLineAfterJavadocParameterDescriptions", "false"}, {"blankLineAfterJavadocReturnTag", "false"}, {"generateParagraphTagOnBlankLines", "false"}, {"alignJavadocParameterDescriptions", "false"}, {"alignJavadocReturnDescription", "false"}, {"alignJavadocExceptionDescriptions", "false"}};
        defaults = new HashMap<String, String>();
        for (String[] strings : defaultValues) {
            defaults.put(strings[0], strings[1]);
        }
    }

    static {
        WRAP_ALWAYS = CodeStyle.WrapStyle.WRAP_ALWAYS.name();
        WRAP_IF_LONG = CodeStyle.WrapStyle.WRAP_IF_LONG.name();
        WRAP_NEVER = CodeStyle.WrapStyle.WRAP_NEVER.name();
        BP_NEW_LINE = CodeStyle.BracePlacement.NEW_LINE.name();
        BP_NEW_LINE_HALF_INDENTED = CodeStyle.BracePlacement.NEW_LINE_HALF_INDENTED.name();
        BP_NEW_LINE_INDENTED = CodeStyle.BracePlacement.NEW_LINE_INDENTED.name();
        BP_SAME_LINE = CodeStyle.BracePlacement.SAME_LINE.name();
        BGS_ELIMINATE = CodeStyle.BracesGenerationStyle.ELIMINATE.name();
        BGS_LEAVE_ALONE = CodeStyle.BracesGenerationStyle.LEAVE_ALONE.name();
        BGS_GENERATE = CodeStyle.BracesGenerationStyle.GENERATE.name();
        IP_CARET = CodeStyle.InsertionPoint.CARET_LOCATION.name();
        IP_FIRST = CodeStyle.InsertionPoint.FIRST_IN_CATEGORY.name();
        IP_LAST = CodeStyle.InsertionPoint.LAST_IN_CATEGORY.name();
        FmtOptions.createDefaults();
        REFORMAT_RP = new RequestProcessor("Java Format Previewer");
    }

    public static interface CodeStyleProducer {
        public CodeStyle create(Preferences var1);
    }

    public static final class ProxyPreferences
    extends AbstractPreferences {
        private final Preferences[] delegates;

        public /* varargs */ ProxyPreferences(Preferences ... delegates) {
            super(null, "");
            this.delegates = delegates;
        }

        @Override
        protected void putSpi(String key, String value) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected String getSpi(String key) {
            for (Preferences p : this.delegates) {
                String value = p.get(key, null);
                if (value == null) continue;
                return value;
            }
            return null;
        }

        @Override
        protected void removeSpi(String key) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected void removeNodeSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected String[] keysSpi() throws BackingStoreException {
            HashSet<String> keys = new HashSet<String>();
            for (Preferences p : this.delegates) {
                keys.addAll(Arrays.asList(p.keys()));
            }
            return keys.toArray(new String[keys.size()]);
        }

        @Override
        protected String[] childrenNamesSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected AbstractPreferences childSpi(String name) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected void syncSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected void flushSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    public static class PreviewPreferences
    extends AbstractPreferences {
        private Map<String, Object> map = new HashMap<String, Object>();

        public PreviewPreferences() {
            super(null, "");
        }

        @Override
        protected void putSpi(String key, String value) {
            this.map.put(key, value);
        }

        @Override
        protected String getSpi(String key) {
            return (String)this.map.get(key);
        }

        @Override
        protected void removeSpi(String key) {
            this.map.remove(key);
        }

        @Override
        protected void removeNodeSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected String[] keysSpi() throws BackingStoreException {
            String[] array = new String[this.map.keySet().size()];
            return this.map.keySet().toArray(array);
        }

        @Override
        protected String[] childrenNamesSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected AbstractPreferences childSpi(String name) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected void syncSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected void flushSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    public static class CategorySupport
    implements ActionListener,
    ChangeListener,
    ListDataListener,
    TableModelListener,
    DocumentListener,
    PreviewProvider,
    PreferencesCustomizer {
        public static final String OPTION_ID = "org.netbeans.modules.java.ui.FormatingOptions.ID";
        private static final int LOAD = 0;
        private static final int STORE = 1;
        private static final int ADD_LISTENERS = 2;
        private static final ComboItem[] bracePlacement = new ComboItem[]{new ComboItem(CodeStyle.BracePlacement.SAME_LINE.name(), "LBL_bp_SAME_LINE"), new ComboItem(CodeStyle.BracePlacement.NEW_LINE.name(), "LBL_bp_NEW_LINE"), new ComboItem(CodeStyle.BracePlacement.NEW_LINE_HALF_INDENTED.name(), "LBL_bp_NEW_LINE_HALF_INDENTED"), new ComboItem(CodeStyle.BracePlacement.NEW_LINE_INDENTED.name(), "LBL_bp_NEW_LINE_INDENTED")};
        private static final ComboItem[] bracesGeneration = new ComboItem[]{new ComboItem(CodeStyle.BracesGenerationStyle.GENERATE.name(), "LBL_bg_GENERATE"), new ComboItem(CodeStyle.BracesGenerationStyle.LEAVE_ALONE.name(), "LBL_bg_LEAVE_ALONE"), new ComboItem(CodeStyle.BracesGenerationStyle.ELIMINATE.name(), "LBL_bg_ELIMINATE")};
        private static final ComboItem[] wrap = new ComboItem[]{new ComboItem(CodeStyle.WrapStyle.WRAP_ALWAYS.name(), "LBL_wrp_WRAP_ALWAYS"), new ComboItem(CodeStyle.WrapStyle.WRAP_IF_LONG.name(), "LBL_wrp_WRAP_IF_LONG"), new ComboItem(CodeStyle.WrapStyle.WRAP_NEVER.name(), "LBL_wrp_WRAP_NEVER")};
        private static final ComboItem[] insertionPoint = new ComboItem[]{new ComboItem(CodeStyle.InsertionPoint.LAST_IN_CATEGORY.name(), "LBL_ip_LAST_IN_CATEGORY"), new ComboItem(CodeStyle.InsertionPoint.FIRST_IN_CATEGORY.name(), "LBL_ip_FIRST_IN_CATEGORY"), new ComboItem(CodeStyle.InsertionPoint.ORDERED_IN_CATEGORY.name(), "LBL_ip_ORDERED_IN_CATEGORY"), new ComboItem(CodeStyle.InsertionPoint.CARET_LOCATION.name(), "LBL_ip_CARET_LOCATION")};
        protected final String previewText;
        private final String id;
        protected final JPanel panel;
        private final List<JComponent> components = new LinkedList<JComponent>();
        private JEditorPane previewPane;
        protected final Preferences preferences;
        protected final Preferences previewPrefs;
        AtomicBoolean pendingRefresh = new AtomicBoolean(false);

        protected /* varargs */ CategorySupport(Preferences preferences, String id, JPanel panel, String previewText, String[] ... forcedOptions) {
            this.preferences = preferences;
            this.id = id;
            this.panel = panel;
            this.previewText = previewText != null ? previewText : NbBundle.getMessage(FmtOptions.class, (String)"SAMPLE_Default");
            this.scan(panel, this.components);
            PreviewPreferences forcedPrefs = new PreviewPreferences();
            for (String[] option : forcedOptions) {
                forcedPrefs.put(option[0], option[1]);
            }
            this.previewPrefs = new ProxyPreferences(forcedPrefs, preferences);
            this.loadFrom(preferences);
            this.addListeners();
        }

        protected void addListeners() {
            this.scan(2, null);
        }

        protected void loadFrom(Preferences preferences) {
            this.scan(0, preferences);
        }

        protected void storeTo(Preferences p) {
            this.scan(1, p);
        }

        protected void notifyChanged() {
            this.storeTo(this.preferences);
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    CategorySupport.this.refreshPreview();
                }
            });
        }

        protected void loadListData(JList list, String optionID, Preferences p) {
        }

        protected void storeListData(JList list, String optionID, Preferences node) {
        }

        protected void loadTableData(JTable table, String optionID, Preferences p) {
        }

        protected void storeTableData(JTable table, String optionID, Preferences node) {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.notifyChanged();
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            this.notifyChanged();
        }

        @Override
        public void contentsChanged(ListDataEvent e) {
        }

        @Override
        public void intervalAdded(ListDataEvent e) {
            this.notifyChanged();
        }

        @Override
        public void intervalRemoved(ListDataEvent e) {
        }

        @Override
        public void tableChanged(TableModelEvent e) {
            this.notifyChanged();
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            this.notifyChanged();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            this.notifyChanged();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            this.notifyChanged();
        }

        public JComponent getPreviewComponent() {
            if (this.previewPane == null) {
                this.previewPane = new JEditorPane();
                this.previewPane.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtOptions.class, (String)"AN_Preview"));
                this.previewPane.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtOptions.class, (String)"AD_Preview"));
                this.previewPane.putClientProperty("HighlightsLayerIncludes", "^org\\.netbeans\\.modules\\.editor\\.lib2\\.highlighting\\.SyntaxHighlighting$");
                this.previewPane.setEditorKit(CloneableEditorSupport.getEditorKit((String)"text/x-java"));
                this.previewPane.setEditable(false);
            }
            return this.previewPane;
        }

        public void refreshPreview() {
            if (this.pendingRefresh.getAndSet(true)) {
                return;
            }
            final JEditorPane jep = (JEditorPane)this.getPreviewComponent();
            jep.setIgnoreRepaint(true);
            REFORMAT_RP.post(new Runnable(){
                private String text;

                @Override
                public void run() {
                    if (SwingUtilities.isEventDispatchThread()) {
                        try {
                            int rm = CategorySupport.this.previewPrefs.getInt("text-limit-width", FmtOptions.getDefaultAsInt("text-limit-width"));
                            jep.putClientProperty("TextLimitLine", rm);
                            jep.getDocument().putProperty("text-line-wrap", "");
                            jep.getDocument().putProperty("tab-size", "");
                            jep.getDocument().putProperty("text-limit-width", "");
                        }
                        catch (NumberFormatException e) {
                            // empty catch block
                        }
                        jep.setIgnoreRepaint(true);
                        jep.setText(this.text);
                        jep.setIgnoreRepaint(false);
                        jep.scrollRectToVisible(new Rectangle(0, 0, 10, 10));
                        jep.repaint(100);
                        return;
                    }
                    try {
                        Class.forName(CodeStyle.class.getName(), true, CodeStyle.class.getClassLoader());
                    }
                    catch (ClassNotFoundException cnfe) {
                        // empty catch block
                    }
                    CategorySupport.this.pendingRefresh.getAndSet(false);
                    CodeStyle codeStyle = FmtOptions.codeStyleProducer.create(CategorySupport.this.previewPrefs);
                    this.text = Reformatter.reformat(CategorySupport.this.previewText, codeStyle);
                    SwingUtilities.invokeLater(this);
                }
            }, 100);
        }

        public JComponent getComponent() {
            return this.panel;
        }

        public String getDisplayName() {
            return this.panel.getName();
        }

        public String getId() {
            return this.id;
        }

        public HelpCtx getHelpCtx() {
            return null;
        }

        private void performOperation(int operation, JComponent jc, String optionID, Preferences p) {
            switch (operation) {
                case 0: {
                    this.loadData(jc, optionID, p);
                    break;
                }
                case 1: {
                    this.storeData(jc, optionID, p);
                    break;
                }
                case 2: {
                    this.addListener(jc);
                }
            }
        }

        private void scan(int what, Preferences p) {
            for (JComponent jc : this.components) {
                Object o = jc.getClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID");
                if (o instanceof String) {
                    this.performOperation(what, jc, (String)o, p);
                    continue;
                }
                if (!(o instanceof String[])) continue;
                for (String oid : (String[])o) {
                    this.performOperation(what, jc, oid, p);
                }
            }
        }

        private void scan(Container container, List<JComponent> components) {
            for (Component c : container.getComponents()) {
                JComponent jc;
                Object o;
                if (c instanceof JComponent && ((o = (jc = (JComponent)c).getClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID")) instanceof String || o instanceof String[])) {
                    components.add(jc);
                }
                if (!(c instanceof Container)) continue;
                this.scan((Container)c, components);
            }
        }

        private void loadData(JComponent jc, String optionID, Preferences node) {
            if (jc instanceof JTextField) {
                JTextField field = (JTextField)jc;
                field.setText(node.get(optionID, FmtOptions.getDefaultAsString(optionID)));
            } else if (jc instanceof JSpinner) {
                JSpinner js = (JSpinner)jc;
                js.setValue(node.getInt(optionID, FmtOptions.getDefaultAsInt(optionID)));
            } else if (jc instanceof JToggleButton) {
                JToggleButton toggle = (JToggleButton)jc;
                boolean df = FmtOptions.getDefaultAsBoolean(optionID);
                toggle.setSelected(node.getBoolean(optionID, df));
            } else if (jc instanceof JComboBox) {
                JComboBox cb = (JComboBox)jc;
                String value = node.get(optionID, FmtOptions.getDefaultAsString(optionID));
                ComboBoxModel model = this.createModel(value);
                cb.setModel(model);
                ComboItem item = CategorySupport.whichItem(value, model);
                cb.setSelectedItem(item);
            } else if (jc instanceof JList) {
                this.loadListData((JList)jc, optionID, node);
            } else if (jc instanceof JTable) {
                this.loadTableData((JTable)jc, optionID, node);
            }
        }

        private void storeData(JComponent jc, String optionID, Preferences node) {
            if (jc instanceof JTextField) {
                JTextField field = (JTextField)jc;
                String text = field.getText();
                if (FmtOptions.isInteger(optionID)) {
                    try {
                        int i = Integer.parseInt(text);
                    }
                    catch (NumberFormatException e) {
                        return;
                    }
                }
                if (!optionID.equals("tab-size") && !optionID.equals("spaces-per-tab") && !optionID.equals("indent-shift-width") && FmtOptions.getDefaultAsString(optionID).equals(text)) {
                    node.remove(optionID);
                } else {
                    node.put(optionID, text);
                }
            } else if (jc instanceof JSpinner) {
                JSpinner js = (JSpinner)jc;
                Object value = js.getValue();
                if (FmtOptions.getDefaultAsInt(optionID) == (Integer)value) {
                    node.remove(optionID);
                } else {
                    node.putInt(optionID, (Integer)value);
                }
            } else if (jc instanceof JToggleButton) {
                JToggleButton toggle = (JToggleButton)jc;
                if (!optionID.equals("expand-tabs") && FmtOptions.getDefaultAsBoolean(optionID) == toggle.isSelected()) {
                    node.remove(optionID);
                } else {
                    node.putBoolean(optionID, toggle.isSelected());
                }
            } else if (jc instanceof JComboBox) {
                JComboBox cb = (JComboBox)jc;
                String value = ((ComboItem)cb.getSelectedItem()).value;
                if (FmtOptions.getDefaultAsString(optionID).equals(value)) {
                    node.remove(optionID);
                } else {
                    node.put(optionID, value);
                }
            } else if (jc instanceof JList) {
                this.storeListData((JList)jc, optionID, node);
            } else if (jc instanceof JTable) {
                this.storeTableData((JTable)jc, optionID, node);
            }
        }

        private void addListener(JComponent jc) {
            if (jc instanceof JTextField) {
                JTextField field = (JTextField)jc;
                field.addActionListener(this);
                field.getDocument().addDocumentListener(this);
            } else if (jc instanceof JSpinner) {
                JSpinner spinner = (JSpinner)jc;
                spinner.addChangeListener(this);
            } else if (jc instanceof JToggleButton) {
                JToggleButton toggle = (JToggleButton)jc;
                toggle.addActionListener(this);
            } else if (jc instanceof JComboBox) {
                JComboBox cb = (JComboBox)jc;
                cb.addActionListener(this);
            } else if (jc instanceof JList) {
                JList jl = (JList)jc;
                jl.getModel().addListDataListener(this);
            } else if (jc instanceof JTable) {
                JTable jt = (JTable)jc;
                jt.getModel().addTableModelListener(this);
            }
        }

        private ComboBoxModel createModel(String value) {
            for (ComboItem comboItem222 : bracePlacement) {
                if (!value.equals(comboItem222.value)) continue;
                return new DefaultComboBoxModel<ComboItem>(bracePlacement);
            }
            for (ComboItem comboItem222 : bracesGeneration) {
                if (!value.equals(comboItem222.value)) continue;
                return new DefaultComboBoxModel<ComboItem>(bracesGeneration);
            }
            for (ComboItem comboItem222 : wrap) {
                if (!value.equals(comboItem222.value)) continue;
                return new DefaultComboBoxModel<ComboItem>(wrap);
            }
            for (ComboItem comboItem222 : insertionPoint) {
                if (!value.equals(comboItem222.value)) continue;
                return new DefaultComboBoxModel<ComboItem>(insertionPoint);
            }
            return null;
        }

        private static ComboItem whichItem(String value, ComboBoxModel model) {
            for (int i = 0; i < model.getSize(); ++i) {
                ComboItem item = (ComboItem)model.getElementAt(i);
                if (!value.equals(item.value)) continue;
                return item;
            }
            return null;
        }

        private static class ComboItem {
            String value;
            String displayName;

            public ComboItem(String value, String key) {
                this.value = value;
                this.displayName = NbBundle.getMessage(FmtOptions.class, (String)key);
            }

            public String toString() {
                return this.displayName;
            }
        }

        public static final class Factory
        implements PreferencesCustomizer.Factory {
            private final String id;
            private final Class<? extends JPanel> panelClass;
            private final String previewText;
            private final String[][] forcedOptions;

            public /* varargs */ Factory(String id, Class<? extends JPanel> panelClass, String previewText, String[] ... forcedOptions) {
                this.id = id;
                this.panelClass = panelClass;
                this.previewText = previewText;
                this.forcedOptions = forcedOptions;
            }

            public PreferencesCustomizer create(Preferences preferences) {
                try {
                    CategorySupport categorySupport = new CategorySupport(preferences, this.id, this.panelClass.newInstance(), this.previewText, this.forcedOptions);
                    if (categorySupport.panel instanceof Runnable) {
                        ((Runnable)((Object)categorySupport.panel)).run();
                    }
                    return categorySupport;
                }
                catch (Exception e) {
                    return null;
                }
            }
        }

    }

    static abstract class DocumentCategorySupport
    extends CategorySupport {
        private final Source[] sources = new Source[2];
        private int sourceIndex;

        public /* varargs */ DocumentCategorySupport(Preferences preferences, String id, JPanel panel, String previewText, String[] ... forcedOptions) {
            super(preferences, id, panel, previewText, forcedOptions);
        }

        private String getSourceName(int index) {
            if (index == 0) {
                return "org.netbeans.samples.ClassA";
            }
            return "org.netbeans.samples" + (index + 1) + ".ClassA";
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private Document reformatDocument(int index) {
            assert (REFORMAT_RP.isRequestProcessorThread());
            try {
                Class.forName(CodeStyle.class.getName(), true, CodeStyle.class.getClassLoader());
            }
            catch (ClassNotFoundException cnfe) {
                // empty catch block
            }
            CodeStyle codeStyle = FmtOptions.codeStyleProducer.create(this.previewPrefs);
            try {
                Document doc;
                Source s;
                block12 : {
                    if (this.sources[index] == null) {
                        FileObject fo = FileUtil.createMemoryFileSystem().getRoot().createData(this.getSourceName(index), "java");
                        this.sources[index] = Source.create((FileObject)fo);
                    }
                    if ((doc = (s = this.sources[index]).getDocument(true)).getLength() > 0) {
                        doc.remove(0, doc.getLength());
                    }
                    doc.insertString(0, this.previewText, null);
                    doc.putProperty(CodeStyle.class, codeStyle);
                    this.reformatSource(doc, s);
                    final Reformat reformat = Reformat.get((Document)doc);
                    reformat.lock();
                    try {
                        if (doc instanceof BaseDocument) {
                            ((BaseDocument)doc).runAtomicAsUser(new Runnable(){

                                @Override
                                public void run() {
                                    try {
                                        reformat.reformat(0, doc.getLength());
                                    }
                                    catch (BadLocationException ble) {
                                        // empty catch block
                                    }
                                }
                            });
                            break block12;
                        }
                        reformat.reformat(0, doc.getLength());
                    }
                    finally {
                        reformat.unlock();
                    }
                }
                DataObject dataObject = DataObject.find((FileObject)s.getFileObject());
                SaveCookie sc = (SaveCookie)dataObject.getLookup().lookup(SaveCookie.class);
                if (sc != null) {
                    sc.save();
                }
                return doc;
            }
            catch (Exception ex) {
                return null;
            }
        }

        protected void doModification(ResultIterator iterator) throws Exception {
        }

        protected void reformatSource(Document d, Source s) throws ParseException, IOException {
            ModificationResult result = ModificationResult.runModificationTask(Collections.singleton(s), new UserTask(){

                public void run(ResultIterator resultIterator) throws Exception {
                    DocumentCategorySupport.this.doModification(resultIterator);
                }
            });
            result.commit();
        }

        @Override
        public void refreshPreview() {
            if (this.pendingRefresh.getAndSet(true)) {
                return;
            }
            final JEditorPane jep = (JEditorPane)this.getPreviewComponent();
            int rm = this.previewPrefs.getInt("text-limit-width", FmtOptions.getDefaultAsInt("text-limit-width"));
            jep.putClientProperty("TextLimitLine", rm);
            jep.getDocument().putProperty("text-line-wrap", "");
            jep.getDocument().putProperty("tab-size", "");
            jep.getDocument().putProperty("text-limit-width", "");
            REFORMAT_RP.post(new Runnable(){
                private Document doc;

                @Override
                public void run() {
                    if (SwingUtilities.isEventDispatchThread()) {
                        jep.setIgnoreRepaint(true);
                        if (this.doc != null) {
                            jep.setDocument(this.doc);
                        }
                        jep.scrollRectToVisible(new Rectangle(0, 0, 10, 10));
                        jep.repaint(100);
                        jep.setIgnoreRepaint(false);
                    } else {
                        DocumentCategorySupport.this.pendingRefresh.getAndSet(false);
                        int index = DocumentCategorySupport.this.sourceIndex;
                        this.doc = DocumentCategorySupport.this.reformatDocument(index);
                        DocumentCategorySupport.this.sourceIndex = (DocumentCategorySupport.this.sourceIndex + 1) % DocumentCategorySupport.this.sources.length;
                        SwingUtilities.invokeLater(this);
                    }
                }
            }, 100);
        }

    }

}

