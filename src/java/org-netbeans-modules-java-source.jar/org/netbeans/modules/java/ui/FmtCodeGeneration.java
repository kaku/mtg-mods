/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.AssignmentTree
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionStatementTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.LiteralTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.NewClassTree
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.TypeParameterTree
 *  com.sun.source.tree.VariableTree
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer$Factory
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.ui;

import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionStatementTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TypeParameterTree;
import com.sun.source.tree.VariableTree;
import java.awt.Component;
import java.awt.Container;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.prefs.Preferences;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.swing.AbstractButton;
import javax.swing.AbstractListModel;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.LayoutStyle;
import javax.swing.ListModel;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.netbeans.api.java.source.GeneratorUtilities;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.TreeMaker;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.modules.java.ui.FmtOptions;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class FmtCodeGeneration
extends JPanel
implements Runnable,
ListSelectionListener {
    private ComboBoxModel ipModel;
    private JCheckBox addOverrideAnnortationCheckBox;
    private JButton downButton;
    private JComboBox insertionPointComboBox;
    private JLabel insertionPointLabel;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane2;
    private JSeparator jSeparator1;
    private JSeparator jSeparator3;
    private JCheckBox keepGASTogetherCheckBox;
    private JCheckBox localVarsFinalCheckBox;
    private JLabel memberOrderLabel;
    private JList membersOrderList;
    private JLabel otherLabel;
    private JCheckBox parametersFinalCheckBox;
    private JCheckBox qualifyFieldAccessCheckBox;
    private JCheckBox sortByVisibilityCheckBox;
    private JCheckBox sortMembersAlphaCheckBox;
    private JButton upButton;
    private JButton visDownButton;
    private JButton visUpButton;
    private JList visibilityOrderList;

    public FmtCodeGeneration() {
        this.initComponents();
        this.qualifyFieldAccessCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "qualifyFieldAccess");
        this.addOverrideAnnortationCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "addOverrideAnnotation");
        this.parametersFinalCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "makeParametersFinal");
        this.localVarsFinalCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "makeLocalVarsFinal");
        this.membersOrderList.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "classMembersOrder");
        this.sortByVisibilityCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "sortMembersByVisibility");
        this.visibilityOrderList.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "visibilityOrder");
        this.keepGASTogetherCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "keepGettersAndSettersTogether");
        this.sortMembersAlphaCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "sortMembersInGroups");
        this.insertionPointComboBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "classMemberInsertionPoint");
    }

    public static PreferencesCustomizer.Factory getController() {
        return new PreferencesCustomizer.Factory(){

            public PreferencesCustomizer create(Preferences preferences) {
                CodeGenCategorySupport support = new CodeGenCategorySupport(preferences, new FmtCodeGeneration());
                ((Runnable)((Object)support.panel)).run();
                return support;
            }
        };
    }

    @Override
    public void run() {
        this.membersOrderList.setSelectedIndex(0);
        this.membersOrderList.addListSelectionListener(this);
        this.enableMembersOrderButtons();
        this.visibilityOrderList.setSelectedIndex(0);
        this.visibilityOrderList.addListSelectionListener(this);
        this.enableVisibilityOrder();
        this.enableInsertionPoint();
        this.otherLabel.setVisible(false);
        this.qualifyFieldAccessCheckBox.setVisible(false);
        this.addOverrideAnnortationCheckBox.setVisible(false);
        this.parametersFinalCheckBox.setVisible(false);
        this.localVarsFinalCheckBox.setVisible(false);
        this.jSeparator3.setVisible(false);
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource() == this.membersOrderList) {
            this.enableMembersOrderButtons();
        } else {
            this.enableVisibilityOrder();
        }
    }

    private void initComponents() {
        this.otherLabel = new JLabel();
        this.qualifyFieldAccessCheckBox = new JCheckBox();
        this.addOverrideAnnortationCheckBox = new JCheckBox();
        this.parametersFinalCheckBox = new JCheckBox();
        this.localVarsFinalCheckBox = new JCheckBox();
        this.memberOrderLabel = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this.membersOrderList = new JList();
        this.upButton = new JButton();
        this.downButton = new JButton();
        this.jSeparator3 = new JSeparator();
        this.sortByVisibilityCheckBox = new JCheckBox();
        this.jScrollPane2 = new JScrollPane();
        this.visibilityOrderList = new JList();
        this.visUpButton = new JButton();
        this.visDownButton = new JButton();
        this.insertionPointLabel = new JLabel();
        this.keepGASTogetherCheckBox = new JCheckBox();
        this.sortMembersAlphaCheckBox = new JCheckBox();
        this.jSeparator1 = new JSeparator();
        this.insertionPointComboBox = new JComboBox();
        this.setName(NbBundle.getMessage(FmtCodeGeneration.class, (String)"LBL_CodeGeneration"));
        this.setOpaque(false);
        Mnemonics.setLocalizedText((JLabel)this.otherLabel, (String)NbBundle.getMessage(FmtCodeGeneration.class, (String)"LBL_gen_Other"));
        Mnemonics.setLocalizedText((AbstractButton)this.qualifyFieldAccessCheckBox, (String)NbBundle.getMessage(FmtCodeGeneration.class, (String)"LBL_gen_QualifyFieldAccess"));
        this.qualifyFieldAccessCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.qualifyFieldAccessCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.qualifyFieldAccessCheckBox.setOpaque(false);
        Mnemonics.setLocalizedText((AbstractButton)this.addOverrideAnnortationCheckBox, (String)NbBundle.getMessage(FmtCodeGeneration.class, (String)"LBL_gen_AddOverrideAnnotation"));
        this.addOverrideAnnortationCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.addOverrideAnnortationCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.addOverrideAnnortationCheckBox.setOpaque(false);
        Mnemonics.setLocalizedText((AbstractButton)this.parametersFinalCheckBox, (String)NbBundle.getMessage(FmtCodeGeneration.class, (String)"LBL_gen_ParametersFinal"));
        this.parametersFinalCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.parametersFinalCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.parametersFinalCheckBox.setOpaque(false);
        Mnemonics.setLocalizedText((AbstractButton)this.localVarsFinalCheckBox, (String)NbBundle.getMessage(FmtCodeGeneration.class, (String)"LBL_gen_LocalVariablesFinal"));
        this.localVarsFinalCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.localVarsFinalCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.localVarsFinalCheckBox.setOpaque(false);
        Mnemonics.setLocalizedText((JLabel)this.memberOrderLabel, (String)NbBundle.getMessage(FmtCodeGeneration.class, (String)"LBL_gen_MembersOreder"));
        this.membersOrderList.setModel(new AbstractListModel(){
            String[] strings;

            @Override
            public int getSize() {
                return this.strings.length;
            }

            @Override
            public Object getElementAt(int i) {
                return this.strings[i];
            }
        });
        this.membersOrderList.setSelectionMode(0);
        this.jScrollPane1.setViewportView(this.membersOrderList);
        Mnemonics.setLocalizedText((AbstractButton)this.upButton, (String)NbBundle.getMessage(FmtCodeGeneration.class, (String)"LBL_gen_MembersOrederUp"));
        this.upButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FmtCodeGeneration.this.upButtonActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.downButton, (String)NbBundle.getMessage(FmtCodeGeneration.class, (String)"LBL_gen_MembersOrederDown"));
        this.downButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FmtCodeGeneration.this.downButtonActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.sortByVisibilityCheckBox, (String)NbBundle.getMessage(FmtCodeGeneration.class, (String)"LBL_gen_SortByVisibility"));
        this.sortByVisibilityCheckBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FmtCodeGeneration.this.sortByVisibilityCheckBoxActionPerformed(evt);
            }
        });
        this.visibilityOrderList.setModel(new AbstractListModel(){
            String[] strings;

            @Override
            public int getSize() {
                return this.strings.length;
            }

            @Override
            public Object getElementAt(int i) {
                return this.strings[i];
            }
        });
        this.visibilityOrderList.setSelectionMode(0);
        this.jScrollPane2.setViewportView(this.visibilityOrderList);
        Mnemonics.setLocalizedText((AbstractButton)this.visUpButton, (String)NbBundle.getMessage(FmtCodeGeneration.class, (String)"LBL_gen_MembersOrederUp"));
        this.visUpButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FmtCodeGeneration.this.visUpButtonActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.visDownButton, (String)NbBundle.getMessage(FmtCodeGeneration.class, (String)"LBL_gen_MembersOrederDown"));
        this.visDownButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FmtCodeGeneration.this.visDownButtonActionPerformed(evt);
            }
        });
        this.insertionPointLabel.setLabelFor(this.insertionPointComboBox);
        Mnemonics.setLocalizedText((JLabel)this.insertionPointLabel, (String)NbBundle.getMessage(FmtCodeGeneration.class, (String)"LBL_gen_InsertionPoint"));
        Mnemonics.setLocalizedText((AbstractButton)this.keepGASTogetherCheckBox, (String)NbBundle.getMessage(FmtCodeGeneration.class, (String)"LBL_gen_KeepGASTogether"));
        Mnemonics.setLocalizedText((AbstractButton)this.sortMembersAlphaCheckBox, (String)NbBundle.getMessage(FmtCodeGeneration.class, (String)"LBL_gen_SortMembersAlpha"));
        this.sortMembersAlphaCheckBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FmtCodeGeneration.this.sortMembersAlphaCheckBoxActionPerformed(evt);
            }
        });
        this.insertionPointComboBox.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.otherLabel).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.qualifyFieldAccessCheckBox).addComponent(this.addOverrideAnnortationCheckBox).addComponent(this.parametersFinalCheckBox).addComponent(this.localVarsFinalCheckBox)))).addGap(0, 0, 32767)).addComponent(this.jSeparator3).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.memberOrderLabel).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane1, -2, 160, -2).addComponent(this.jScrollPane2, -2, 160, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.downButton, -2, 108, -2).addComponent(this.upButton, -2, 108, -2).addComponent(this.visDownButton, -2, 108, -2).addComponent(this.visUpButton, -2, 108, -2))).addComponent(this.sortByVisibilityCheckBox).addComponent(this.keepGASTogetherCheckBox).addComponent(this.sortMembersAlphaCheckBox).addGroup(layout.createSequentialGroup().addComponent(this.insertionPointLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.insertionPointComboBox, 0, -1, 32767))).addContainerGap(-1, 32767)).addComponent(this.jSeparator1));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.otherLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.qualifyFieldAccessCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.addOverrideAnnortationCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.parametersFinalCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.localVarsFinalCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.jSeparator3, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.memberOrderLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane1, -2, 156, -2).addGroup(layout.createSequentialGroup().addComponent(this.upButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.downButton))).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.sortByVisibilityCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane2, -2, 71, -2).addGroup(layout.createSequentialGroup().addComponent(this.visUpButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.visDownButton))).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.keepGASTogetherCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.sortMembersAlphaCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.jSeparator1, -2, 7, -2).addGap(3, 3, 3).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.insertionPointLabel).addComponent(this.insertionPointComboBox, -2, -1, -2)).addContainerGap(-1, 32767)));
    }

    private void upButtonActionPerformed(ActionEvent evt) {
        int idx = this.membersOrderList.getSelectedIndex();
        if (idx > 0) {
            Object val = this.membersOrderList.getModel().getElementAt(idx);
            ((DefaultListModel)this.membersOrderList.getModel()).removeElementAt(idx);
            ((DefaultListModel)this.membersOrderList.getModel()).insertElementAt(val, idx - 1);
            this.membersOrderList.setSelectedIndex(idx - 1);
        }
    }

    private void downButtonActionPerformed(ActionEvent evt) {
        int idx = this.membersOrderList.getSelectedIndex();
        if (idx >= 0 && idx < this.membersOrderList.getModel().getSize() - 1) {
            Object val = this.membersOrderList.getModel().getElementAt(idx);
            ((DefaultListModel)this.membersOrderList.getModel()).removeElementAt(idx);
            ((DefaultListModel)this.membersOrderList.getModel()).insertElementAt(val, idx + 1);
            this.membersOrderList.setSelectedIndex(idx + 1);
        }
    }

    private void visUpButtonActionPerformed(ActionEvent evt) {
        int idx = this.visibilityOrderList.getSelectedIndex();
        if (idx > 0) {
            Object val = this.visibilityOrderList.getModel().getElementAt(idx);
            ((DefaultListModel)this.visibilityOrderList.getModel()).removeElementAt(idx);
            ((DefaultListModel)this.visibilityOrderList.getModel()).insertElementAt(val, idx - 1);
            this.visibilityOrderList.setSelectedIndex(idx - 1);
        }
    }

    private void visDownButtonActionPerformed(ActionEvent evt) {
        int idx = this.visibilityOrderList.getSelectedIndex();
        if (idx >= 0 && idx < this.visibilityOrderList.getModel().getSize() - 1) {
            Object val = this.visibilityOrderList.getModel().getElementAt(idx);
            ((DefaultListModel)this.visibilityOrderList.getModel()).removeElementAt(idx);
            ((DefaultListModel)this.visibilityOrderList.getModel()).insertElementAt(val, idx + 1);
            this.visibilityOrderList.setSelectedIndex(idx + 1);
        }
    }

    private void sortByVisibilityCheckBoxActionPerformed(ActionEvent evt) {
        this.enableVisibilityOrder();
    }

    private void sortMembersAlphaCheckBoxActionPerformed(ActionEvent evt) {
        this.enableInsertionPoint();
    }

    private void enableMembersOrderButtons() {
        int idx = this.membersOrderList.getSelectedIndex();
        this.upButton.setEnabled(idx > 0);
        this.downButton.setEnabled(idx >= 0 && idx < this.membersOrderList.getModel().getSize() - 1);
    }

    private void enableVisibilityOrder() {
        int idx = this.visibilityOrderList.getSelectedIndex();
        boolean b = this.sortByVisibilityCheckBox.isSelected();
        this.visibilityOrderList.setEnabled(b);
        this.visUpButton.setEnabled(b && idx > 0);
        this.visDownButton.setEnabled(b && idx >= 0 && idx < this.visibilityOrderList.getModel().getSize() - 1);
    }

    private void enableInsertionPoint() {
        Object[] values;
        if (this.ipModel == null) {
            this.ipModel = this.insertionPointComboBox.getModel();
        }
        Object toSelect = this.insertionPointComboBox.getSelectedItem();
        if (this.sortMembersAlphaCheckBox.isSelected()) {
            if (toSelect != this.ipModel.getElementAt(3)) {
                toSelect = this.ipModel.getElementAt(2);
            }
            values = new Object[]{this.ipModel.getElementAt(2), this.ipModel.getElementAt(3)};
        } else {
            if (toSelect != this.ipModel.getElementAt(3)) {
                toSelect = this.ipModel.getElementAt(0);
            }
            values = new Object[]{this.ipModel.getElementAt(0), this.ipModel.getElementAt(1), this.ipModel.getElementAt(3)};
        }
        this.insertionPointComboBox.setModel(new DefaultComboBoxModel<Object>(values));
        this.insertionPointComboBox.setSelectedItem(toSelect);
    }

    private static final class CodeGenCategorySupport
    extends FmtOptions.DocumentCategorySupport {
        private CodeGenCategorySupport(Preferences preferences, JPanel panel) {
            super(preferences, "code-generation", panel, NbBundle.getMessage(FmtCodeGeneration.class, (String)"SAMPLE_CodeGen"), {"blankLinesBeforeFields", "1"});
        }

        @Override
        protected void loadListData(JList list, String optionID, Preferences node) {
            DefaultListModel<Object> model = new DefaultListModel<Object>();
            String value = node.get(optionID, FmtOptions.getDefaultAsString(optionID));
            for (String s : value.trim().split("\\s*[,;]\\s*")) {
                if ("classMembersOrder".equals(optionID)) {
                    Element e = new Element();
                    if (s.startsWith("STATIC ")) {
                        e.isStatic = true;
                        s = s.substring(7);
                    }
                    e.kind = ElementKind.valueOf(s);
                    model.addElement(e);
                    continue;
                }
                Visibility v = new Visibility();
                v.kind = s;
                model.addElement(v);
            }
            list.setModel(model);
        }

        @Override
        protected void storeListData(JList list, String optionID, Preferences node) {
            String value;
            StringBuilder sb = null;
            for (int i = 0; i < list.getModel().getSize(); ++i) {
                if (sb == null) {
                    sb = new StringBuilder();
                } else {
                    sb.append(';');
                }
                if ("classMembersOrder".equals(optionID)) {
                    Element e = (Element)list.getModel().getElementAt(i);
                    if (e.isStatic) {
                        sb.append("STATIC ");
                    }
                    sb.append(e.kind.name());
                    continue;
                }
                Visibility v = (Visibility)list.getModel().getElementAt(i);
                sb.append(v.kind);
            }
            String string = value = sb != null ? sb.toString() : "";
            if (FmtOptions.getDefaultAsString(optionID).equals(value)) {
                node.remove(optionID);
            } else {
                node.put(optionID, value);
            }
        }

        @Override
        protected void doModification(ResultIterator resultIterator) throws Exception {
            WorkingCopy copy = WorkingCopy.get(resultIterator.getParserResult());
            copy.toPhase(JavaSource.Phase.RESOLVED);
            TreeMaker tm = copy.getTreeMaker();
            GeneratorUtilities gu = GeneratorUtilities.get(copy);
            CompilationUnitTree cut = copy.getCompilationUnit();
            ClassTree ct = (ClassTree)cut.getTypeDecls().get(0);
            VariableTree field = (VariableTree)ct.getMembers().get(1);
            ArrayList<Object> members = new ArrayList<Object>();
            AssignmentTree stat = tm.Assignment((ExpressionTree)tm.Identifier("name"), (ExpressionTree)tm.Literal("Name"));
            BlockTree init = tm.Block(Collections.singletonList(tm.ExpressionStatement((ExpressionTree)stat)), false);
            members.add((Object)init);
            members.add((Object)gu.createConstructor(ct, Collections.emptyList()));
            members.add((Object)gu.createGetter(field));
            ModifiersTree mods = tm.Modifiers(EnumSet.of(Modifier.PRIVATE));
            ClassTree inner = tm.Class(mods, "Inner", Collections.emptyList(), null, Collections.emptyList(), Collections.emptyList());
            members.add((Object)inner);
            mods = tm.Modifiers(EnumSet.of(Modifier.PRIVATE, Modifier.STATIC));
            ClassTree nested = tm.Class(mods, "Nested", Collections.emptyList(), null, Collections.emptyList(), Collections.emptyList());
            members.add((Object)nested);
            IdentifierTree nestedId = tm.Identifier("Nested");
            VariableTree staticField = tm.Variable(mods, "instance", (Tree)nestedId, null);
            members.add((Object)staticField);
            NewClassTree nct = tm.NewClass(null, Collections.emptyList(), (ExpressionTree)nestedId, Collections.emptyList(), null);
            stat = tm.Assignment((ExpressionTree)tm.Identifier("instance"), (ExpressionTree)nct);
            BlockTree staticInit = tm.Block(Collections.singletonList(tm.ExpressionStatement((ExpressionTree)stat)), true);
            members.add((Object)staticInit);
            members.add((Object)gu.createGetter(staticField));
            ClassTree newCT = gu.insertClassMembers(ct, members);
            copy.rewrite((Tree)ct, (Tree)newCT);
        }

        private static class Visibility {
            private String kind;

            private Visibility() {
            }

            public String toString() {
                return NbBundle.getMessage(FmtCodeGeneration.class, (String)("VAL_gen_" + this.kind));
            }
        }

        private static class Element {
            private boolean isStatic;
            private ElementKind kind;

            private Element() {
            }

            public String toString() {
                return (this.isStatic ? new StringBuilder().append(NbBundle.getMessage(FmtCodeGeneration.class, (String)"VAL_gen_STATIC")).append(" ").toString() : "") + NbBundle.getMessage(FmtCodeGeneration.class, (String)new StringBuilder().append("VAL_gen_").append(this.kind.name()).toString());
            }
        }

    }

}

