/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer$Factory
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.border.Border;
import org.netbeans.modules.java.ui.FmtOptions;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class FmtTabsIndents
extends JPanel {
    private JCheckBox absoluteLabelIndentCheckBox;
    private JTextField continuationIndentSizeField;
    private JLabel continuationIndentSizeLabel;
    private JCheckBox indentCasesFromSwitchCheckBox;
    private JCheckBox indentTopLevelClassMembersCheckBox;
    private JTextField labelIndentField;
    private JLabel labelIndentLabel;

    public FmtTabsIndents() {
        this.initComponents();
        this.continuationIndentSizeField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "continuationIndentSize");
        this.labelIndentField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "labelIndent");
        this.absoluteLabelIndentCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "absoluteLabelIndent");
        this.indentTopLevelClassMembersCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "indentTopLevelClassMembers");
        this.indentCasesFromSwitchCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "indentCasesFromSwitch");
    }

    public static PreferencesCustomizer.Factory getController() {
        return new FmtOptions.CategorySupport.Factory("tabs-and-indents", FmtTabsIndents.class, NbBundle.getMessage(FmtTabsIndents.class, (String)"SAMPLE_TabsIndents"), {"blankLinesBeforeClass", "0"});
    }

    private void initComponents() {
        this.continuationIndentSizeLabel = new JLabel();
        this.continuationIndentSizeField = new JTextField();
        this.labelIndentLabel = new JLabel();
        this.labelIndentField = new JTextField();
        this.absoluteLabelIndentCheckBox = new JCheckBox();
        this.indentTopLevelClassMembersCheckBox = new JCheckBox();
        this.indentCasesFromSwitchCheckBox = new JCheckBox();
        this.setName(NbBundle.getMessage(FmtTabsIndents.class, (String)"LBL_TabsAndIndents"));
        this.setOpaque(false);
        this.continuationIndentSizeLabel.setLabelFor(this.continuationIndentSizeField);
        Mnemonics.setLocalizedText((JLabel)this.continuationIndentSizeLabel, (String)NbBundle.getMessage(FmtTabsIndents.class, (String)"LBL_ContinuationIndentSize"));
        this.labelIndentLabel.setLabelFor(this.labelIndentField);
        Mnemonics.setLocalizedText((JLabel)this.labelIndentLabel, (String)NbBundle.getMessage(FmtTabsIndents.class, (String)"LBL_LabelIndent"));
        Mnemonics.setLocalizedText((AbstractButton)this.absoluteLabelIndentCheckBox, (String)NbBundle.getMessage(FmtTabsIndents.class, (String)"LBL_AbsoluteLabelIndent"));
        this.absoluteLabelIndentCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        Mnemonics.setLocalizedText((AbstractButton)this.indentTopLevelClassMembersCheckBox, (String)NbBundle.getMessage(FmtTabsIndents.class, (String)"LBL_IndentTopLevelClassMemberts"));
        this.indentTopLevelClassMembersCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        Mnemonics.setLocalizedText((AbstractButton)this.indentCasesFromSwitchCheckBox, (String)NbBundle.getMessage(FmtTabsIndents.class, (String)"LBL_IndentCasesFromSwitch"));
        this.indentCasesFromSwitchCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.continuationIndentSizeLabel).addComponent(this.labelIndentLabel)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.continuationIndentSizeField, -2, 36, -2).addComponent(this.labelIndentField, -2, 36, -2))).addComponent(this.indentCasesFromSwitchCheckBox).addComponent(this.indentTopLevelClassMembersCheckBox).addComponent(this.absoluteLabelIndentCheckBox)).addContainerGap(-1, 32767)));
        layout.linkSize(0, this.continuationIndentSizeField, this.labelIndentField);
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.continuationIndentSizeLabel).addComponent(this.continuationIndentSizeField, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.labelIndentLabel).addComponent(this.labelIndentField, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.absoluteLabelIndentCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.indentTopLevelClassMembersCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.indentCasesFromSwitchCheckBox).addContainerGap(33, 32767)));
        this.continuationIndentSizeField.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtTabsIndents.class, (String)"FmtTabsIndents.continuationIndentSizeField.AccessibleContext.accessibleDescription"));
        this.labelIndentField.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtTabsIndents.class, (String)"FmtTabsIndents.labelIndentField.AccessibleContext.accessibleDescription"));
        this.absoluteLabelIndentCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtTabsIndents.class, (String)"FmtTabsIndents.absoluteLabelIndentCheckBox.AccessibleContext.accessibleDescription"));
        this.indentTopLevelClassMembersCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtTabsIndents.class, (String)"FmtTabsIndents.indentTopLevelClassMembersCheckBox.AccessibleContext.accessibleDescription"));
        this.indentCasesFromSwitchCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtTabsIndents.class, (String)"FmtTabsIndents.indentCasesFromSwitchCheckBox.AccessibleContext.accessibleDescription"));
    }
}

