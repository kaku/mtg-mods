/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer$Factory
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.LayoutStyle;
import org.netbeans.modules.java.ui.FmtOptions;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.openide.util.NbBundle;

public class FmtComments
extends JPanel
implements Runnable {
    private JCheckBox addLeadingStarCheckBox;
    private JCheckBox alignExceptionsCheckBox;
    private JCheckBox alignParamsCheckBox;
    private JCheckBox alignReturnCheckBox;
    private JCheckBox blankLineAfterDescCheckBox;
    private JCheckBox blankLineAfterParamsCheckBox;
    private JCheckBox blankLineAfterReturnCheckBox;
    private JCheckBox enableBlockCommentFormatCheckBox;
    private JCheckBox enableCommentFormatCheckBox;
    private JLabel generalLabel;
    private JCheckBox generatePCheckBox;
    private JSeparator jSeparator1;
    private JSeparator jSeparator2;
    private JLabel javadocLabel;
    private JCheckBox preserveNewLinesCheckBox;
    private JCheckBox wrapCommentTextCheckBox;
    private JCheckBox wrapOneLineCheckBox;

    public FmtComments() {
        this.initComponents();
        this.enableCommentFormatCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "enableCommentFormatting");
        this.enableBlockCommentFormatCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "enableBlockCommentFormatting");
        this.addLeadingStarCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "addLeadingStarInComment");
        this.wrapCommentTextCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "wrapCommentText");
        this.wrapOneLineCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "wrapOneLineComment");
        this.preserveNewLinesCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "preserveNewLinesInComments");
        this.blankLineAfterDescCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "blankLineAfterJavadocDescription");
        this.blankLineAfterParamsCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "blankLineAfterJavadocParameterDescriptions");
        this.blankLineAfterReturnCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "blankLineAfterJavadocReturnTag");
        this.generatePCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "generateParagraphTagOnBlankLines");
        this.alignParamsCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "alignJavadocParameterDescriptions");
        this.alignReturnCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "alignJavadocReturnDescription");
        this.alignExceptionsCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "alignJavadocExceptionDescriptions");
    }

    public static PreferencesCustomizer.Factory getController() {
        return new FmtOptions.CategorySupport.Factory("comments", FmtComments.class, NbBundle.getMessage(FmtComments.class, (String)"SAMPLE_Comments"), {"text-limit-width", "45"}, {"blankLinesBeforeClass", "0"});
    }

    @Override
    public void run() {
        this.enableControls(this.enableCommentFormatCheckBox.isSelected());
    }

    private void initComponents() {
        this.enableCommentFormatCheckBox = new JCheckBox();
        this.enableBlockCommentFormatCheckBox = new JCheckBox();
        this.generalLabel = new JLabel();
        this.jSeparator1 = new JSeparator();
        this.addLeadingStarCheckBox = new JCheckBox();
        this.wrapCommentTextCheckBox = new JCheckBox();
        this.wrapOneLineCheckBox = new JCheckBox();
        this.preserveNewLinesCheckBox = new JCheckBox();
        this.javadocLabel = new JLabel();
        this.jSeparator2 = new JSeparator();
        this.blankLineAfterDescCheckBox = new JCheckBox();
        this.blankLineAfterParamsCheckBox = new JCheckBox();
        this.blankLineAfterReturnCheckBox = new JCheckBox();
        this.generatePCheckBox = new JCheckBox();
        this.alignParamsCheckBox = new JCheckBox();
        this.alignReturnCheckBox = new JCheckBox();
        this.alignExceptionsCheckBox = new JCheckBox();
        this.setName(NbBundle.getMessage(FmtComments.class, (String)"LBL_Comments"));
        this.setOpaque(false);
        this.enableCommentFormatCheckBox.setText(NbBundle.getMessage(FmtComments.class, (String)"LBL_doc_enableCommentFormat"));
        this.enableCommentFormatCheckBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FmtComments.this.enableCommentFormatCheckBoxActionPerformed(evt);
            }
        });
        this.enableBlockCommentFormatCheckBox.setText(NbBundle.getMessage(FmtComments.class, (String)"LBL_doc_enableBlockCommentFormat"));
        this.generalLabel.setText(NbBundle.getMessage(FmtComments.class, (String)"LBL_doc_generalLabel"));
        this.addLeadingStarCheckBox.setText(NbBundle.getMessage(FmtComments.class, (String)"LBL_doc_addLeadingStar"));
        this.wrapCommentTextCheckBox.setText(NbBundle.getMessage(FmtComments.class, (String)"LBL_doc_wrapCommentText"));
        this.wrapOneLineCheckBox.setText(NbBundle.getMessage(FmtComments.class, (String)"LBL_doc_wrapOneLineCheckBox"));
        this.preserveNewLinesCheckBox.setText(NbBundle.getMessage(FmtComments.class, (String)"LBL_doc_preserveNewLinesCheckBox"));
        this.javadocLabel.setText(NbBundle.getMessage(FmtComments.class, (String)"LBL_doc_javadocLabel"));
        this.blankLineAfterDescCheckBox.setText(NbBundle.getMessage(FmtComments.class, (String)"LBL_doc_blankLineAfterDescCheckBox"));
        this.blankLineAfterParamsCheckBox.setText(NbBundle.getMessage(FmtComments.class, (String)"LBL_doc_blankLineAfterParamsCheckBox"));
        this.blankLineAfterReturnCheckBox.setText(NbBundle.getMessage(FmtComments.class, (String)"LBL_doc_blankLineAfterReturnCheckBox"));
        this.generatePCheckBox.setText(NbBundle.getMessage(FmtComments.class, (String)"LBL_doc_generatePCheckBox"));
        this.alignParamsCheckBox.setText(NbBundle.getMessage(FmtComments.class, (String)"LBL_doc_alignParamsCheckBox"));
        this.alignReturnCheckBox.setText(NbBundle.getMessage(FmtComments.class, (String)"LBL_doc_alignReturnCheckBox"));
        this.alignExceptionsCheckBox.setText(NbBundle.getMessage(FmtComments.class, (String)"LBL_doc_alignExceptionsCheckBox"));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.javadocLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jSeparator2)).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.enableCommentFormatCheckBox).addGroup(layout.createSequentialGroup().addGap(12, 12, 12).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.wrapCommentTextCheckBox).addComponent(this.addLeadingStarCheckBox).addComponent(this.wrapOneLineCheckBox).addComponent(this.preserveNewLinesCheckBox).addComponent(this.blankLineAfterParamsCheckBox).addComponent(this.blankLineAfterDescCheckBox).addComponent(this.blankLineAfterReturnCheckBox).addComponent(this.generatePCheckBox).addComponent(this.alignParamsCheckBox).addComponent(this.alignReturnCheckBox).addComponent(this.alignExceptionsCheckBox).addComponent(this.enableBlockCommentFormatCheckBox)))).addGap(0, 0, 32767)).addGroup(layout.createSequentialGroup().addComponent(this.generalLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jSeparator1))).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.enableCommentFormatCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.enableBlockCommentFormatCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jSeparator1, GroupLayout.Alignment.TRAILING, -2, 7, -2).addComponent(this.generalLabel, GroupLayout.Alignment.TRAILING)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.addLeadingStarCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.wrapCommentTextCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.wrapOneLineCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.preserveNewLinesCheckBox).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.javadocLabel)).addGroup(layout.createSequentialGroup().addGap(15, 15, 15).addComponent(this.jSeparator2, -2, -1, -2))).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.blankLineAfterDescCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.blankLineAfterParamsCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.blankLineAfterReturnCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.generatePCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.alignParamsCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.alignReturnCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.alignExceptionsCheckBox).addContainerGap(-1, 32767)));
    }

    private void enableCommentFormatCheckBoxActionPerformed(ActionEvent evt) {
        this.enableControls(this.enableCommentFormatCheckBox.isSelected());
    }

    private void enableControls(boolean b) {
        this.enableBlockCommentFormatCheckBox.setEnabled(b);
        this.addLeadingStarCheckBox.setEnabled(b);
        this.alignExceptionsCheckBox.setEnabled(b);
        this.alignParamsCheckBox.setEnabled(b);
        this.alignReturnCheckBox.setEnabled(b);
        this.blankLineAfterDescCheckBox.setEnabled(b);
        this.blankLineAfterParamsCheckBox.setEnabled(b);
        this.blankLineAfterReturnCheckBox.setEnabled(b);
        this.generatePCheckBox.setEnabled(b);
        this.preserveNewLinesCheckBox.setEnabled(b);
        this.wrapCommentTextCheckBox.setEnabled(b);
        this.wrapOneLineCheckBox.setEnabled(b);
    }

}

