/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer$Factory
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import org.netbeans.modules.java.ui.FmtOptions;
import org.netbeans.modules.java.ui.NumericKeyListener;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class FmtBlankLines
extends JPanel {
    private JTextField aClassField;
    private JTextField aClassFooterField;
    private JLabel aClassFooterLabel;
    private JTextField aClassHeaderField;
    private JLabel aClassHeaderLabel;
    private JLabel aClassLabel;
    private JTextField aFieldsField;
    private JLabel aFieldsLabel;
    private JLabel aImports;
    private JTextField aImportsField;
    private JTextField aMethodsField;
    private JLabel aMethodsLabel;
    private JTextField aPackageField;
    private JLabel aPackageLabel;
    private JTextField anAnonymousClassFooterField;
    private JLabel anAnonymousClassFooterLabel;
    private JTextField anAnonymousClassHeaderField;
    private JLabel anAnonymousClassHeaderLabel;
    private JTextField bClassField;
    private JLabel bClassLabel;
    private JTextField bFieldsField;
    private JLabel bFieldsLabel;
    private JTextField bImportsField;
    private JLabel bImportsLabel;
    private JTextField bMethodsField;
    private JLabel bMethodsLabel;
    private JTextField bPackageField;
    private JLabel bPackageLabel;
    private JTextField inCodeField;
    private JLabel inCodeLabel;
    private JTextField inDeclarationsField;
    private JLabel inDeclarationsLabel;
    private JSeparator jSeparator1;
    private JSeparator jSeparator2;
    private JLabel maxLabel;
    private JLabel minLabel;

    public FmtBlankLines() {
        this.initComponents();
        this.inDeclarationsField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "blankLinesInDeclarations");
        this.inCodeField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "blankLinesInCode");
        this.bPackageField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "blankLinesBeforePackage");
        this.aPackageField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "blankLinesAfterPackage");
        this.bImportsField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "blankLinesBeforeImports");
        this.aImportsField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "blankLinesAfterImports");
        this.bClassField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "blankLinesBeforeClass");
        this.aClassField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "blankLinesAfterClass");
        this.aClassHeaderField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "blankLinesAfterClassHeader");
        this.anAnonymousClassHeaderField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "blankLinesAfterAnonymousClassHeader");
        this.aClassFooterField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "blankLinesBeforeClassClosingBrace");
        this.anAnonymousClassFooterField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "blankLinesBeforeAnonymousClassClosingBrace");
        this.bFieldsField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "blankLinesBeforeFields");
        this.aFieldsField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "blankLinesAfterFields");
        this.bMethodsField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "blankLinesBeforeMethods");
        this.aMethodsField.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "blankLinesAfterMethods");
        this.inDeclarationsField.addKeyListener(new NumericKeyListener());
        this.inCodeField.addKeyListener(new NumericKeyListener());
        this.bPackageField.addKeyListener(new NumericKeyListener());
        this.aPackageField.addKeyListener(new NumericKeyListener());
        this.bImportsField.addKeyListener(new NumericKeyListener());
        this.aImportsField.addKeyListener(new NumericKeyListener());
        this.bClassField.addKeyListener(new NumericKeyListener());
        this.aClassField.addKeyListener(new NumericKeyListener());
        this.aClassHeaderField.addKeyListener(new NumericKeyListener());
        this.anAnonymousClassHeaderField.addKeyListener(new NumericKeyListener());
        this.aClassFooterField.addKeyListener(new NumericKeyListener());
        this.anAnonymousClassFooterField.addKeyListener(new NumericKeyListener());
        this.bFieldsField.addKeyListener(new NumericKeyListener());
        this.aFieldsField.addKeyListener(new NumericKeyListener());
        this.bMethodsField.addKeyListener(new NumericKeyListener());
        this.aMethodsField.addKeyListener(new NumericKeyListener());
    }

    public static PreferencesCustomizer.Factory getController() {
        return new FmtOptions.CategorySupport.Factory("blank-lines", FmtBlankLines.class, NbBundle.getMessage(FmtBlankLines.class, (String)"SAMPLE_BlankLines"), new String[0][]);
    }

    private void initComponents() {
        this.maxLabel = new JLabel();
        this.jSeparator1 = new JSeparator();
        this.inDeclarationsLabel = new JLabel();
        this.inDeclarationsField = new JTextField();
        this.inCodeLabel = new JLabel();
        this.inCodeField = new JTextField();
        this.minLabel = new JLabel();
        this.jSeparator2 = new JSeparator();
        this.bPackageLabel = new JLabel();
        this.bPackageField = new JTextField();
        this.aPackageLabel = new JLabel();
        this.aPackageField = new JTextField();
        this.bImportsLabel = new JLabel();
        this.bImportsField = new JTextField();
        this.aImports = new JLabel();
        this.aImportsField = new JTextField();
        this.bClassLabel = new JLabel();
        this.bClassField = new JTextField();
        this.aClassLabel = new JLabel();
        this.aClassField = new JTextField();
        this.aClassHeaderLabel = new JLabel();
        this.aClassHeaderField = new JTextField();
        this.anAnonymousClassHeaderLabel = new JLabel();
        this.anAnonymousClassHeaderField = new JTextField();
        this.aClassFooterLabel = new JLabel();
        this.aClassFooterField = new JTextField();
        this.anAnonymousClassFooterLabel = new JLabel();
        this.anAnonymousClassFooterField = new JTextField();
        this.bFieldsLabel = new JLabel();
        this.bFieldsField = new JTextField();
        this.aFieldsLabel = new JLabel();
        this.aFieldsField = new JTextField();
        this.bMethodsLabel = new JLabel();
        this.bMethodsField = new JTextField();
        this.aMethodsLabel = new JLabel();
        this.aMethodsField = new JTextField();
        this.setName(NbBundle.getMessage(FmtBlankLines.class, (String)"LBL_BlankLines"));
        this.setOpaque(false);
        Mnemonics.setLocalizedText((JLabel)this.maxLabel, (String)NbBundle.getMessage(FmtBlankLines.class, (String)"LBL_blMax"));
        this.inDeclarationsLabel.setLabelFor(this.inDeclarationsField);
        Mnemonics.setLocalizedText((JLabel)this.inDeclarationsLabel, (String)NbBundle.getMessage(FmtBlankLines.class, (String)"LBL_blInDeclarations"));
        this.inDeclarationsField.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FmtBlankLines.this.inDeclarationsFieldActionPerformed(evt);
            }
        });
        this.inCodeLabel.setLabelFor(this.inCodeField);
        Mnemonics.setLocalizedText((JLabel)this.inCodeLabel, (String)NbBundle.getMessage(FmtBlankLines.class, (String)"LBL_blInCode"));
        Mnemonics.setLocalizedText((JLabel)this.minLabel, (String)NbBundle.getMessage(FmtBlankLines.class, (String)"LBL_blMin"));
        this.bPackageLabel.setLabelFor(this.bPackageField);
        Mnemonics.setLocalizedText((JLabel)this.bPackageLabel, (String)NbBundle.getMessage(FmtBlankLines.class, (String)"LBL_blBeforePackage"));
        this.bPackageField.setColumns(2);
        this.aPackageLabel.setLabelFor(this.aPackageField);
        Mnemonics.setLocalizedText((JLabel)this.aPackageLabel, (String)NbBundle.getMessage(FmtBlankLines.class, (String)"LBL_blAfterPackage"));
        this.aPackageField.setColumns(5);
        this.bImportsLabel.setLabelFor(this.bImportsField);
        Mnemonics.setLocalizedText((JLabel)this.bImportsLabel, (String)NbBundle.getMessage(FmtBlankLines.class, (String)"LBL_blBeforeImports"));
        this.bImportsField.setColumns(5);
        this.aImports.setLabelFor(this.aImportsField);
        Mnemonics.setLocalizedText((JLabel)this.aImports, (String)NbBundle.getMessage(FmtBlankLines.class, (String)"LBL_blAfterImports"));
        this.aImportsField.setColumns(5);
        this.bClassLabel.setLabelFor(this.bClassField);
        Mnemonics.setLocalizedText((JLabel)this.bClassLabel, (String)NbBundle.getMessage(FmtBlankLines.class, (String)"LBL_blBeforeClass"));
        this.bClassField.setColumns(5);
        this.aClassLabel.setLabelFor(this.aClassField);
        Mnemonics.setLocalizedText((JLabel)this.aClassLabel, (String)NbBundle.getMessage(FmtBlankLines.class, (String)"LBL_blAfterClass"));
        this.aClassField.setColumns(5);
        this.aClassHeaderLabel.setLabelFor(this.aClassHeaderField);
        Mnemonics.setLocalizedText((JLabel)this.aClassHeaderLabel, (String)NbBundle.getMessage(FmtBlankLines.class, (String)"LBL_blAfterClassHeader"));
        this.aClassHeaderField.setColumns(5);
        this.anAnonymousClassHeaderLabel.setLabelFor(this.anAnonymousClassHeaderField);
        Mnemonics.setLocalizedText((JLabel)this.anAnonymousClassHeaderLabel, (String)NbBundle.getMessage(FmtBlankLines.class, (String)"LBL_blAfterAnonymousClassHeader"));
        this.aClassFooterLabel.setLabelFor(this.aClassFooterField);
        Mnemonics.setLocalizedText((JLabel)this.aClassFooterLabel, (String)NbBundle.getMessage(FmtBlankLines.class, (String)"LBL_blBeforeClassFooter"));
        this.aClassFooterField.setColumns(5);
        this.anAnonymousClassFooterLabel.setLabelFor(this.anAnonymousClassFooterField);
        Mnemonics.setLocalizedText((JLabel)this.anAnonymousClassFooterLabel, (String)NbBundle.getMessage(FmtBlankLines.class, (String)"LBL_blBeforeAnonymousClassFooter"));
        this.bFieldsLabel.setLabelFor(this.bFieldsField);
        Mnemonics.setLocalizedText((JLabel)this.bFieldsLabel, (String)NbBundle.getMessage(FmtBlankLines.class, (String)"LBL_blBeforeFields"));
        this.bFieldsField.setColumns(5);
        this.aFieldsLabel.setLabelFor(this.aFieldsField);
        Mnemonics.setLocalizedText((JLabel)this.aFieldsLabel, (String)NbBundle.getMessage(FmtBlankLines.class, (String)"LBL_blAfterFields"));
        this.aFieldsField.setColumns(5);
        this.bMethodsLabel.setLabelFor(this.bMethodsField);
        Mnemonics.setLocalizedText((JLabel)this.bMethodsLabel, (String)NbBundle.getMessage(FmtBlankLines.class, (String)"LBL_blBeforeMethods"));
        this.bMethodsField.setColumns(5);
        this.aMethodsLabel.setLabelFor(this.aMethodsField);
        Mnemonics.setLocalizedText((JLabel)this.aMethodsLabel, (String)NbBundle.getMessage(FmtBlankLines.class, (String)"LBL_blAfterMethods"));
        this.aMethodsField.setColumns(5);
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.bPackageLabel).addComponent(this.aPackageLabel).addComponent(this.bImportsLabel).addComponent(this.aImports).addComponent(this.bClassLabel).addComponent(this.aClassLabel).addComponent(this.aClassHeaderLabel).addComponent(this.anAnonymousClassHeaderLabel).addComponent(this.aClassFooterLabel).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.anAnonymousClassFooterLabel).addComponent(this.inCodeLabel).addComponent(this.inDeclarationsLabel)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.bFieldsField, -2, 26, -2).addComponent(this.anAnonymousClassFooterField, -2, 26, -2).addComponent(this.aFieldsField, -2, 26, -2).addComponent(this.bMethodsField, -2, 26, -2).addComponent(this.aMethodsField, -2, 26, -2).addComponent(this.aClassFooterField, -2, 26, -2).addComponent(this.anAnonymousClassHeaderField, -2, 26, -2).addComponent(this.aClassHeaderField, -2, 26, -2).addComponent(this.aClassField, -2, 26, -2).addComponent(this.bClassField, -2, 26, -2).addComponent(this.aImportsField, -2, 26, -2).addComponent(this.bImportsField, -2, 26, -2).addComponent(this.aPackageField, -2, 26, -2).addComponent(this.bPackageField, -2, -1, -2).addComponent(this.inCodeField, -2, 26, -2).addComponent(this.inDeclarationsField, -2, 26, -2))).addComponent(this.aMethodsLabel).addComponent(this.bMethodsLabel).addComponent(this.aFieldsLabel).addComponent(this.bFieldsLabel))).addGroup(layout.createSequentialGroup().addComponent(this.minLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jSeparator2)).addGroup(layout.createSequentialGroup().addComponent(this.maxLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jSeparator1, -2, 0, 32767))).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.maxLabel).addComponent(this.jSeparator1, -2, 10, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.inDeclarationsLabel).addComponent(this.inDeclarationsField, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.inCodeLabel).addComponent(this.inCodeField, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.minLabel).addGroup(layout.createSequentialGroup().addGap(6, 6, 6).addComponent(this.jSeparator2, -2, 9, -2))).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.bPackageLabel).addComponent(this.bPackageField, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.aPackageField, -2, -1, -2).addComponent(this.aPackageLabel)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.bImportsField, -2, -1, -2).addComponent(this.bImportsLabel)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.aImportsField, -2, -1, -2).addComponent(this.aImports)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.bClassField, -2, -1, -2).addComponent(this.bClassLabel)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.aClassField, -2, -1, -2).addComponent(this.aClassLabel)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.aClassHeaderLabel).addComponent(this.aClassHeaderField, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.anAnonymousClassHeaderLabel).addComponent(this.anAnonymousClassHeaderField, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.aClassFooterLabel).addComponent(this.aClassFooterField, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.anAnonymousClassFooterLabel).addComponent(this.anAnonymousClassFooterField, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.bFieldsLabel).addComponent(this.bFieldsField, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.aFieldsLabel).addComponent(this.aFieldsField, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.bMethodsLabel).addComponent(this.bMethodsField, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.aMethodsLabel).addComponent(this.aMethodsField, -2, -1, -2)).addContainerGap(-1, 32767)));
        layout.linkSize(1, this.aClassField, this.aClassHeaderField, this.aImportsField, this.aMethodsField, this.aPackageField, this.bClassField, this.bFieldsField, this.bImportsField, this.bMethodsField, this.bPackageField);
    }

    private void inDeclarationsFieldActionPerformed(ActionEvent evt) {
    }

}

