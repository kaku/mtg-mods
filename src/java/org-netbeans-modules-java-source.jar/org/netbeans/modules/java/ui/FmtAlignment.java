/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer$Factory
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.LayoutStyle;
import javax.swing.border.Border;
import org.netbeans.api.java.source.CodeStyle;
import org.netbeans.modules.java.ui.FmtOptions;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class FmtAlignment
extends JPanel {
    private JCheckBox amAnnotationArgsCheckBox;
    private JCheckBox amArrayInitCheckBox1;
    private JCheckBox amAssignCheckBox1;
    private JCheckBox amBinaryOpCheckBox1;
    private JCheckBox amCallArgsCheckBox;
    private JCheckBox amForCheckBox1;
    private JCheckBox amImplementsCheckBox1;
    private JCheckBox amLambdaParamsCheckBox;
    private JCheckBox amMethodParamsCheckBox;
    private JCheckBox amMultiCatchCheckBox;
    private JCheckBox amParenthesizedCheckBox1;
    private JCheckBox amTernaryOpCheckBox1;
    private JCheckBox amThrowsCheckBox1;
    private JCheckBox amTryResourcesCheckBox;
    private JSeparator jSeparator1;
    private JSeparator jSeparator2;
    private JLabel multilineAlignmentLabel;
    private JLabel newLinesLabel;
    private JCheckBox nlCatchCheckBox;
    private JCheckBox nlElseCheckBox;
    private JCheckBox nlFinallyCheckBox;
    private JCheckBox nlModifiersCheckBox;
    private JCheckBox nlWhileCheckBox;

    public FmtAlignment() {
        this.initComponents();
        this.nlElseCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "placeElseOnNewLine");
        this.nlWhileCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "placeWhileOnNewLine");
        this.nlCatchCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "placeCatchOnNewLine");
        this.nlFinallyCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "placeFinallyOnNewLine");
        this.nlModifiersCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "placeNewLineAfterModifiers");
        this.amMethodParamsCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "alignMultilineMethodParams");
        this.amLambdaParamsCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "alignMultilineLambdaParams");
        this.amCallArgsCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "alignMultilineCallArgs");
        this.amAnnotationArgsCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "alignMultilineAnnotationArgs");
        this.amTryResourcesCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "alignMultilineTryResources");
        this.amMultiCatchCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "alignMultilineDisjunctiveCatchTypes");
        this.amArrayInitCheckBox1.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "alignMultilineArrayInit");
        this.amAssignCheckBox1.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "alignMultilineAssignment");
        this.amBinaryOpCheckBox1.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "alignMultilineBinaryOp");
        this.amForCheckBox1.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "alignMultilineFor");
        this.amImplementsCheckBox1.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "alignMultilineImplements");
        this.amParenthesizedCheckBox1.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "alignMultilineParenthesized");
        this.amTernaryOpCheckBox1.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "alignMultilineTernaryOp");
        this.amThrowsCheckBox1.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "alignMultilineThrows");
    }

    public static PreferencesCustomizer.Factory getController() {
        return new FmtOptions.CategorySupport.Factory("alignment", FmtAlignment.class, NbBundle.getMessage(FmtAlignment.class, (String)"SAMPLE_Align"), {"wrapArrayInit", CodeStyle.WrapStyle.WRAP_ALWAYS.name()}, {"wrapEnumConstants", CodeStyle.WrapStyle.WRAP_ALWAYS.name()}, {"wrapExtendsImplementsList", CodeStyle.WrapStyle.WRAP_ALWAYS.name()}, {"wrapFor", CodeStyle.WrapStyle.WRAP_ALWAYS.name()}, {"wrapMethodCallArgs", CodeStyle.WrapStyle.WRAP_ALWAYS.name()}, {"wrapAnnotationArgs", CodeStyle.WrapStyle.WRAP_ALWAYS.name()}, {"wrapMethodParams", CodeStyle.WrapStyle.WRAP_ALWAYS.name()}, {"wrapLambdaParams", CodeStyle.WrapStyle.WRAP_ALWAYS.name()}, {"wrapTernaryOps", CodeStyle.WrapStyle.WRAP_ALWAYS.name()}, {"wrapThrowsList", CodeStyle.WrapStyle.WRAP_ALWAYS.name()}, {"wrapTryResources", CodeStyle.WrapStyle.WRAP_ALWAYS.name()}, {"wrapDisjunctiveCatchTypes", CodeStyle.WrapStyle.WRAP_ALWAYS.name()}, {"blankLinesBeforeClass", "0"});
    }

    private void initComponents() {
        this.newLinesLabel = new JLabel();
        this.nlElseCheckBox = new JCheckBox();
        this.nlWhileCheckBox = new JCheckBox();
        this.nlCatchCheckBox = new JCheckBox();
        this.nlFinallyCheckBox = new JCheckBox();
        this.nlModifiersCheckBox = new JCheckBox();
        this.multilineAlignmentLabel = new JLabel();
        this.amMethodParamsCheckBox = new JCheckBox();
        this.amLambdaParamsCheckBox = new JCheckBox();
        this.amCallArgsCheckBox = new JCheckBox();
        this.amAnnotationArgsCheckBox = new JCheckBox();
        this.amImplementsCheckBox1 = new JCheckBox();
        this.amThrowsCheckBox1 = new JCheckBox();
        this.amTryResourcesCheckBox = new JCheckBox();
        this.amMultiCatchCheckBox = new JCheckBox();
        this.amArrayInitCheckBox1 = new JCheckBox();
        this.amBinaryOpCheckBox1 = new JCheckBox();
        this.amTernaryOpCheckBox1 = new JCheckBox();
        this.amAssignCheckBox1 = new JCheckBox();
        this.amForCheckBox1 = new JCheckBox();
        this.amParenthesizedCheckBox1 = new JCheckBox();
        this.jSeparator1 = new JSeparator();
        this.jSeparator2 = new JSeparator();
        this.setName(NbBundle.getMessage(FmtAlignment.class, (String)"LBL_Alignment"));
        this.setOpaque(false);
        Mnemonics.setLocalizedText((JLabel)this.newLinesLabel, (String)NbBundle.getMessage(FmtAlignment.class, (String)"LBL_al_newLines"));
        Mnemonics.setLocalizedText((AbstractButton)this.nlElseCheckBox, (String)NbBundle.getMessage(FmtAlignment.class, (String)"LBL_nl_Else"));
        this.nlElseCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.nlElseCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.nlElseCheckBox.setOpaque(false);
        Mnemonics.setLocalizedText((AbstractButton)this.nlWhileCheckBox, (String)NbBundle.getMessage(FmtAlignment.class, (String)"LBL_nl_While"));
        this.nlWhileCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.nlWhileCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.nlWhileCheckBox.setOpaque(false);
        Mnemonics.setLocalizedText((AbstractButton)this.nlCatchCheckBox, (String)NbBundle.getMessage(FmtAlignment.class, (String)"LBL_nl_Catch"));
        this.nlCatchCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.nlCatchCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.nlCatchCheckBox.setOpaque(false);
        Mnemonics.setLocalizedText((AbstractButton)this.nlFinallyCheckBox, (String)NbBundle.getMessage(FmtAlignment.class, (String)"LBL_nl_Finally"));
        this.nlFinallyCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.nlFinallyCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.nlFinallyCheckBox.setOpaque(false);
        Mnemonics.setLocalizedText((AbstractButton)this.nlModifiersCheckBox, (String)NbBundle.getMessage(FmtAlignment.class, (String)"LBL_nl_Modifiers"));
        this.nlModifiersCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.nlModifiersCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.nlModifiersCheckBox.setOpaque(false);
        Mnemonics.setLocalizedText((JLabel)this.multilineAlignmentLabel, (String)NbBundle.getMessage(FmtAlignment.class, (String)"LBL_al_multilineAlignment"));
        Mnemonics.setLocalizedText((AbstractButton)this.amMethodParamsCheckBox, (String)NbBundle.getMessage(FmtAlignment.class, (String)"LBL_am_MethodParams"));
        this.amMethodParamsCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.amMethodParamsCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.amMethodParamsCheckBox.setOpaque(false);
        Mnemonics.setLocalizedText((AbstractButton)this.amLambdaParamsCheckBox, (String)NbBundle.getMessage(FmtAlignment.class, (String)"LBL_am_LambdaParams"));
        this.amLambdaParamsCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.amLambdaParamsCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.amLambdaParamsCheckBox.setOpaque(false);
        Mnemonics.setLocalizedText((AbstractButton)this.amCallArgsCheckBox, (String)NbBundle.getMessage(FmtAlignment.class, (String)"LBL_am_CallArgs"));
        this.amCallArgsCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.amCallArgsCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.amCallArgsCheckBox.setOpaque(false);
        Mnemonics.setLocalizedText((AbstractButton)this.amAnnotationArgsCheckBox, (String)NbBundle.getMessage(FmtAlignment.class, (String)"LBL_am_AnnotationArgs"));
        this.amAnnotationArgsCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.amAnnotationArgsCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.amAnnotationArgsCheckBox.setOpaque(false);
        Mnemonics.setLocalizedText((AbstractButton)this.amImplementsCheckBox1, (String)NbBundle.getMessage(FmtAlignment.class, (String)"LBL_an_Implements"));
        this.amImplementsCheckBox1.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.amImplementsCheckBox1.setMargin(new Insets(0, 0, 0, 0));
        this.amImplementsCheckBox1.setOpaque(false);
        Mnemonics.setLocalizedText((AbstractButton)this.amThrowsCheckBox1, (String)NbBundle.getMessage(FmtAlignment.class, (String)"LBL_am_Throws"));
        this.amThrowsCheckBox1.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.amThrowsCheckBox1.setMargin(new Insets(0, 0, 0, 0));
        this.amThrowsCheckBox1.setOpaque(false);
        Mnemonics.setLocalizedText((AbstractButton)this.amTryResourcesCheckBox, (String)NbBundle.getMessage(FmtAlignment.class, (String)"LBL_am_TryResources"));
        this.amTryResourcesCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.amTryResourcesCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.amTryResourcesCheckBox.setOpaque(false);
        Mnemonics.setLocalizedText((AbstractButton)this.amMultiCatchCheckBox, (String)NbBundle.getMessage(FmtAlignment.class, (String)"LBL_am_MultiCatch"));
        this.amMultiCatchCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.amMultiCatchCheckBox.setMargin(new Insets(0, 0, 0, 0));
        this.amMultiCatchCheckBox.setOpaque(false);
        Mnemonics.setLocalizedText((AbstractButton)this.amArrayInitCheckBox1, (String)NbBundle.getMessage(FmtAlignment.class, (String)"LBL_am_ArrayInit"));
        this.amArrayInitCheckBox1.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.amArrayInitCheckBox1.setMargin(new Insets(0, 0, 0, 0));
        this.amArrayInitCheckBox1.setOpaque(false);
        Mnemonics.setLocalizedText((AbstractButton)this.amBinaryOpCheckBox1, (String)NbBundle.getMessage(FmtAlignment.class, (String)"LBL_am_BinaryOp"));
        this.amBinaryOpCheckBox1.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.amBinaryOpCheckBox1.setMargin(new Insets(0, 0, 0, 0));
        this.amBinaryOpCheckBox1.setOpaque(false);
        Mnemonics.setLocalizedText((AbstractButton)this.amTernaryOpCheckBox1, (String)NbBundle.getMessage(FmtAlignment.class, (String)"LBL_am_TernaryOp"));
        this.amTernaryOpCheckBox1.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.amTernaryOpCheckBox1.setMargin(new Insets(0, 0, 0, 0));
        this.amTernaryOpCheckBox1.setOpaque(false);
        Mnemonics.setLocalizedText((AbstractButton)this.amAssignCheckBox1, (String)NbBundle.getMessage(FmtAlignment.class, (String)"LBL_am_Assign"));
        this.amAssignCheckBox1.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.amAssignCheckBox1.setMargin(new Insets(0, 0, 0, 0));
        this.amAssignCheckBox1.setOpaque(false);
        Mnemonics.setLocalizedText((AbstractButton)this.amForCheckBox1, (String)NbBundle.getMessage(FmtAlignment.class, (String)"LBL_am_For"));
        this.amForCheckBox1.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.amForCheckBox1.setMargin(new Insets(0, 0, 0, 0));
        this.amForCheckBox1.setOpaque(false);
        Mnemonics.setLocalizedText((AbstractButton)this.amParenthesizedCheckBox1, (String)NbBundle.getMessage(FmtAlignment.class, (String)"LBL_am_Paren"));
        this.amParenthesizedCheckBox1.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.amParenthesizedCheckBox1.setMargin(new Insets(0, 0, 0, 0));
        this.amParenthesizedCheckBox1.setOpaque(false);
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false).addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup().addComponent(this.newLinesLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jSeparator1)).addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup().addComponent(this.multilineAlignmentLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jSeparator2)).addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.amThrowsCheckBox1).addComponent(this.amAnnotationArgsCheckBox).addComponent(this.nlElseCheckBox).addComponent(this.nlWhileCheckBox).addComponent(this.nlCatchCheckBox).addComponent(this.amMethodParamsCheckBox).addComponent(this.amAssignCheckBox1).addComponent(this.amBinaryOpCheckBox1).addComponent(this.amMultiCatchCheckBox)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.amTryResourcesCheckBox).addComponent(this.amArrayInitCheckBox1).addComponent(this.amTernaryOpCheckBox1).addComponent(this.amCallArgsCheckBox).addComponent(this.nlModifiersCheckBox).addComponent(this.nlFinallyCheckBox).addComponent(this.amImplementsCheckBox1).addComponent(this.amForCheckBox1).addComponent(this.amLambdaParamsCheckBox)))).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.amParenthesizedCheckBox1)));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.newLinesLabel)).addGroup(layout.createSequentialGroup().addGap(17, 17, 17).addComponent(this.jSeparator1, -2, 10, -2))).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.nlElseCheckBox).addComponent(this.nlFinallyCheckBox)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.nlWhileCheckBox).addComponent(this.nlModifiersCheckBox)).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.nlCatchCheckBox).addGap(18, 18, 18).addComponent(this.multilineAlignmentLabel)).addGroup(layout.createSequentialGroup().addGap(44, 44, 44).addComponent(this.jSeparator2, -2, 10, -2))).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.amMethodParamsCheckBox).addComponent(this.amLambdaParamsCheckBox)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.amAnnotationArgsCheckBox).addComponent(this.amCallArgsCheckBox)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.amThrowsCheckBox1).addComponent(this.amImplementsCheckBox1)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.amMultiCatchCheckBox).addComponent(this.amTryResourcesCheckBox)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.amBinaryOpCheckBox1).addComponent(this.amArrayInitCheckBox1)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.amAssignCheckBox1).addComponent(this.amTernaryOpCheckBox1)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.amParenthesizedCheckBox1).addComponent(this.amForCheckBox1)).addContainerGap(14, 32767)));
    }
}

