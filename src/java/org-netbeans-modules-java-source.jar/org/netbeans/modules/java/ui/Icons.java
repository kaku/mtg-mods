/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 */
package org.netbeans.modules.java.ui;

import java.awt.Image;
import java.util.Collection;
import java.util.Collections;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.openide.util.ImageUtilities;

public final class Icons {
    private static final String ICON_BASE = "org/netbeans/modules/java/source/resources/icons/";
    private static final String GIF_EXTENSION = ".gif";
    private static final String PNG_EXTENSION = ".png";
    private static final String WAIT = "org/netbeans/modules/java/source/resources/icons/wait.png";

    private Icons() {
    }

    public static Icon getBusyIcon() {
        Image img = ImageUtilities.loadImage((String)"org/netbeans/modules/java/source/resources/icons/wait.png");
        if (img == null) {
            return null;
        }
        return new ImageIcon(img);
    }

    public static Icon getElementIcon(ElementKind elementKind, Collection<Modifier> modifiers) {
        if (modifiers == null) {
            modifiers = Collections.emptyList();
        }
        Image img = null;
        switch (elementKind) {
            case PACKAGE: {
                img = ImageUtilities.loadImage((String)"org/netbeans/modules/java/source/resources/icons/package.gif");
                break;
            }
            case ENUM: {
                img = ImageUtilities.loadImage((String)"org/netbeans/modules/java/source/resources/icons/enum.png");
                break;
            }
            case ANNOTATION_TYPE: {
                img = ImageUtilities.loadImage((String)"org/netbeans/modules/java/source/resources/icons/annotation.png");
                break;
            }
            case CLASS: {
                img = ImageUtilities.loadImage((String)"org/netbeans/modules/java/source/resources/icons/class.png");
                break;
            }
            case INTERFACE: {
                img = ImageUtilities.loadImage((String)"org/netbeans/modules/java/source/resources/icons/interface.png");
                break;
            }
            case FIELD: {
                img = ImageUtilities.loadImage((String)Icons.getIconName(elementKind, "org/netbeans/modules/java/source/resources/icons/field", ".png", modifiers));
                break;
            }
            case ENUM_CONSTANT: {
                img = ImageUtilities.loadImage((String)"org/netbeans/modules/java/source/resources/icons/constant.png");
                break;
            }
            case CONSTRUCTOR: {
                img = ImageUtilities.loadImage((String)Icons.getIconName(elementKind, "org/netbeans/modules/java/source/resources/icons/constructor", ".png", modifiers));
                break;
            }
            case INSTANCE_INIT: 
            case STATIC_INIT: {
                img = ImageUtilities.loadImage((String)Icons.getIconName(elementKind, "org/netbeans/modules/java/source/resources/icons/initializer", ".png", modifiers));
                break;
            }
            case METHOD: {
                img = ImageUtilities.loadImage((String)Icons.getIconName(elementKind, "org/netbeans/modules/java/source/resources/icons/method", ".png", modifiers));
                break;
            }
            default: {
                img = null;
            }
        }
        return img == null ? null : new ImageIcon(img);
    }

    private static String getIconName(ElementKind kind, String typeName, String extension, Collection<Modifier> modifiers) {
        StringBuffer fileName = new StringBuffer(typeName);
        if (modifiers.contains((Object)Modifier.STATIC)) {
            fileName.append("Static");
        }
        if (kind == ElementKind.STATIC_INIT || kind == ElementKind.INSTANCE_INIT) {
            return fileName.append(extension).toString();
        }
        if (modifiers.contains((Object)Modifier.PUBLIC)) {
            return fileName.append("Public").append(extension).toString();
        }
        if (modifiers.contains((Object)Modifier.PROTECTED)) {
            return fileName.append("Protected").append(extension).toString();
        }
        if (modifiers.contains((Object)Modifier.PRIVATE)) {
            return fileName.append("Private").append(extension).toString();
        }
        return fileName.append("Package").append(extension).toString();
    }

}

