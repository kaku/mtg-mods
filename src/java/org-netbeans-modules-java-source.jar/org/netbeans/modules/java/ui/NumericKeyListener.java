/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.ui;

import java.awt.Component;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class NumericKeyListener
implements KeyListener {
    @Override
    public void keyPressed(KeyEvent evt) {
    }

    @Override
    public void keyReleased(KeyEvent evt) {
    }

    @Override
    public void keyTyped(KeyEvent evt) {
        if (!Character.isDigit(evt.getKeyChar()) && !Character.isISOControl(evt.getKeyChar())) {
            evt.consume();
            Component c = evt.getComponent();
            if (c != null) {
                c.getToolkit().beep();
            }
        }
    }
}

