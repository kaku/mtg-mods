/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.AssignmentTree
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.CatchTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionStatementTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.LiteralTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodInvocationTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.NewClassTree
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.TryTree
 *  com.sun.source.tree.VariableTree
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer$Factory
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.ui;

import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.CatchTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionStatementTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TryTree;
import com.sun.source.tree.VariableTree;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.prefs.Preferences;
import javax.lang.model.element.Modifier;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.TreeMaker;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.modules.java.ui.FmtOptions;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class FmtImports
extends JPanel
implements Runnable,
ListSelectionListener {
    private Preferences preferences;
    private JButton addButton;
    private JButton addStarImportPackageButton;
    private ButtonGroup buttonGroup;
    private JRadioButton fqnRadioButton;
    private JCheckBox importInnerClassesCheckBox;
    private JLabel importLayoutLabel;
    private JTable importLayoutTable;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane2;
    private JSeparator jSeparator1;
    private JButton moveDownButton;
    private JButton moveUpButton;
    private JRadioButton packageImportsRadioButton;
    private JCheckBox preferStaticImportsCheckBox;
    private JButton removeButton;
    private JButton removeStarImportPackageButton;
    private JCheckBox separateGroupsCheckBox;
    private JCheckBox separateStaticImportsCheckBox;
    private JRadioButton singleClassImportsRadioButton;
    private JLabel starImportPackagesLabel;
    private JTable starImportPackagesTable;
    private JCheckBox starImportTresholdCheckBox;
    private JSpinner starImportTresholdSpinner;
    private JCheckBox starStaticImportTresholdCheckBox;
    private JSpinner startStaticImportTresholdSpinner;
    static final String allOtherImports = NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_allOtherImports");

    public FmtImports(Preferences preferences) {
        this.preferences = preferences;
        this.initComponents();
        this.buttonGroup.add(this.singleClassImportsRadioButton);
        this.buttonGroup.add(this.packageImportsRadioButton);
        this.buttonGroup.add(this.fqnRadioButton);
        this.singleClassImportsRadioButton.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "useSingleClassImport");
        this.importInnerClassesCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "importInnerClasses");
        this.preferStaticImportsCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "preferStaticImports");
        this.starImportTresholdCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "allowConvertToStarImport");
        this.starImportTresholdSpinner.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "countForUsingStarImport");
        this.starStaticImportTresholdCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "allowConvertToStaticStarImport");
        this.startStaticImportTresholdSpinner.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "countForUsingStaticStarImport");
        this.starImportPackagesTable.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "packagesForStarImport");
        this.packageImportsRadioButton.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "usePackageImport");
        this.fqnRadioButton.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "useFQNs");
        this.separateStaticImportsCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "separateStaticImports");
        this.importLayoutTable.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "importGroupsOrder");
        this.separateGroupsCheckBox.putClientProperty("org.netbeans.modules.java.ui.FormatingOptions.ID", "separateImportGroups");
    }

    public static PreferencesCustomizer.Factory getController() {
        return new PreferencesCustomizer.Factory(){

            public PreferencesCustomizer create(Preferences preferences) {
                ImportsCategorySupport support = new ImportsCategorySupport(preferences, new FmtImports(preferences));
                ((Runnable)((Object)support.panel)).run();
                return support;
            }
        };
    }

    @Override
    public void run() {
        this.starImportPackagesTable.getSelectionModel().addListSelectionListener(this);
        this.importLayoutTable.getSelectionModel().addListSelectionListener(this);
        this.enableControls(this.singleClassImportsRadioButton.isSelected());
        this.enableImportLayoutButtons();
        this.setStarImportPackagesTableColumnsWidth();
        this.setImportLayoutTableColumnsWidth();
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        this.enableStarImportPackageButton(this.singleClassImportsRadioButton.isSelected());
        this.enableImportLayoutButtons();
    }

    private void initComponents() {
        this.buttonGroup = new ButtonGroup();
        this.singleClassImportsRadioButton = new JRadioButton();
        this.importInnerClassesCheckBox = new JCheckBox();
        this.starImportTresholdCheckBox = new JCheckBox();
        this.starImportTresholdSpinner = new JSpinner();
        this.starStaticImportTresholdCheckBox = new JCheckBox();
        this.startStaticImportTresholdSpinner = new JSpinner();
        this.starImportPackagesLabel = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this.starImportPackagesTable = new JTable();
        this.addStarImportPackageButton = new JButton();
        this.removeStarImportPackageButton = new JButton();
        this.packageImportsRadioButton = new JRadioButton();
        this.fqnRadioButton = new JRadioButton();
        this.preferStaticImportsCheckBox = new JCheckBox();
        this.jSeparator1 = new JSeparator();
        this.separateStaticImportsCheckBox = new JCheckBox();
        this.importLayoutLabel = new JLabel();
        this.jScrollPane2 = new JScrollPane();
        this.importLayoutTable = new JTable();
        this.addButton = new JButton();
        this.moveUpButton = new JButton();
        this.moveDownButton = new JButton();
        this.removeButton = new JButton();
        this.separateGroupsCheckBox = new JCheckBox();
        this.setName(NbBundle.getMessage(FmtImports.class, (String)"LBL_Imports"));
        this.setOpaque(false);
        Mnemonics.setLocalizedText((AbstractButton)this.singleClassImportsRadioButton, (String)NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_useSingleClass"));
        this.singleClassImportsRadioButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FmtImports.this.singleClassImportsRadioButtonActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.importInnerClassesCheckBox, (String)NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_importInnerClasses"));
        Mnemonics.setLocalizedText((AbstractButton)this.starImportTresholdCheckBox, (String)NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_importTreshold"));
        this.starImportTresholdCheckBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FmtImports.this.starImportTresholdCheckBoxActionPerformed(evt);
            }
        });
        this.starImportTresholdSpinner.setModel(new SpinnerNumberModel((Number)0, Integer.valueOf(0), null, (Number)1));
        Mnemonics.setLocalizedText((AbstractButton)this.starStaticImportTresholdCheckBox, (String)NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_staticImportTreshold"));
        this.starStaticImportTresholdCheckBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FmtImports.this.starStaticImportTresholdCheckBoxActionPerformed(evt);
            }
        });
        this.startStaticImportTresholdSpinner.setModel(new SpinnerNumberModel((Number)0, Integer.valueOf(0), null, (Number)1));
        this.starImportPackagesLabel.setLabelFor(this.starImportPackagesTable);
        Mnemonics.setLocalizedText((JLabel)this.starImportPackagesLabel, (String)NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_starImportPackages"));
        this.starImportPackagesTable.setSelectionMode(0);
        this.starImportPackagesTable.getTableHeader().setResizingAllowed(false);
        this.starImportPackagesTable.getTableHeader().setReorderingAllowed(false);
        this.jScrollPane1.setViewportView(this.starImportPackagesTable);
        Mnemonics.setLocalizedText((AbstractButton)this.addStarImportPackageButton, (String)NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_add"));
        this.addStarImportPackageButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FmtImports.this.addStarImportPackageButtonActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.removeStarImportPackageButton, (String)NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_remove"));
        this.removeStarImportPackageButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FmtImports.this.removeStarImportPackageButtonActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.packageImportsRadioButton, (String)NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_usePackage"));
        this.packageImportsRadioButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FmtImports.this.packageImportsRadioButtonActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.fqnRadioButton, (String)NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_useFQN"));
        this.fqnRadioButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FmtImports.this.fqnRadioButtonActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.preferStaticImportsCheckBox, (String)NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_preferStaticImports"));
        Mnemonics.setLocalizedText((AbstractButton)this.separateStaticImportsCheckBox, (String)NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_separateStaticImports"));
        this.separateStaticImportsCheckBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FmtImports.this.separateStaticImportsCheckBoxActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((JLabel)this.importLayoutLabel, (String)NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_importLayout"));
        this.importLayoutTable.getTableHeader().setResizingAllowed(false);
        this.importLayoutTable.getTableHeader().setReorderingAllowed(false);
        this.jScrollPane2.setViewportView(this.importLayoutTable);
        Mnemonics.setLocalizedText((AbstractButton)this.addButton, (String)NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_add"));
        this.addButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FmtImports.this.addButtonActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.moveUpButton, (String)NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_moveUp"));
        this.moveUpButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FmtImports.this.moveUpButtonActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.moveDownButton, (String)NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_moveDown"));
        this.moveDownButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FmtImports.this.moveDownButtonActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.removeButton, (String)NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_remove"));
        this.removeButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FmtImports.this.removeButtonActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.separateGroupsCheckBox, (String)NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_separateGroups"));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGap(21, 21, 21).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane1, -2, 0, 32767).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.starImportTresholdCheckBox).addComponent(this.starImportPackagesLabel).addComponent(this.starStaticImportTresholdCheckBox)).addGap(0, 0, 32767))).addGap(6, 6, 6).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false).addComponent(this.addStarImportPackageButton, GroupLayout.Alignment.LEADING, -1, -1, 32767).addComponent(this.removeStarImportPackageButton, GroupLayout.Alignment.LEADING, -2, 108, -2)).addGroup(GroupLayout.Alignment.TRAILING, layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.starImportTresholdSpinner, GroupLayout.Alignment.TRAILING).addComponent(this.startStaticImportTresholdSpinner, GroupLayout.Alignment.TRAILING, -2, 38, -2)))).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane2, -2, 0, 32767).addComponent(this.separateStaticImportsCheckBox)).addGap(6, 6, 6).addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false).addComponent(this.addButton, GroupLayout.Alignment.LEADING, -1, -1, 32767).addComponent(this.moveUpButton, GroupLayout.Alignment.LEADING, -1, -1, 32767).addComponent(this.removeButton, GroupLayout.Alignment.LEADING, -1, -1, 32767).addComponent(this.moveDownButton, GroupLayout.Alignment.LEADING))).addComponent(this.jSeparator1).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.singleClassImportsRadioButton).addGroup(layout.createSequentialGroup().addGap(21, 21, 21).addComponent(this.importInnerClassesCheckBox)).addComponent(this.packageImportsRadioButton).addComponent(this.fqnRadioButton).addComponent(this.preferStaticImportsCheckBox).addComponent(this.importLayoutLabel).addComponent(this.separateGroupsCheckBox)).addGap(0, 0, 32767))).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.singleClassImportsRadioButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.importInnerClassesCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.starImportTresholdSpinner, -2, -1, -2).addComponent(this.starImportTresholdCheckBox)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.startStaticImportTresholdSpinner, -2, -1, -2).addComponent(this.starStaticImportTresholdCheckBox)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.starImportPackagesLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addGroup(layout.createSequentialGroup().addComponent(this.addStarImportPackageButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.removeStarImportPackageButton)).addComponent(this.jScrollPane1, GroupLayout.Alignment.TRAILING, -2, 56, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.packageImportsRadioButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.fqnRadioButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.preferStaticImportsCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.jSeparator1, -2, -1, -2).addGap(11, 11, 11).addComponent(this.importLayoutLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.separateStaticImportsCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addGroup(layout.createSequentialGroup().addComponent(this.addButton, -2, 25, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.moveUpButton, -2, 25, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.moveDownButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.removeButton)).addComponent(this.jScrollPane2, -2, 118, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.separateGroupsCheckBox).addContainerGap(-1, 32767)));
    }

    private void singleClassImportsRadioButtonActionPerformed(ActionEvent evt) {
        this.enableControls(this.singleClassImportsRadioButton.isSelected());
    }

    private void packageImportsRadioButtonActionPerformed(ActionEvent evt) {
        this.enableControls(this.singleClassImportsRadioButton.isSelected());
    }

    private void fqnRadioButtonActionPerformed(ActionEvent evt) {
        this.enableControls(this.singleClassImportsRadioButton.isSelected());
    }

    private void addStarImportPackageButtonActionPerformed(ActionEvent evt) {
        ((DefaultTableModel)this.starImportPackagesTable.getModel()).addRow(new Object[]{"", true});
        int rowIndex = this.starImportPackagesTable.getRowCount() - 1;
        this.starImportPackagesTable.getSelectionModel().setSelectionInterval(rowIndex, rowIndex);
        this.starImportPackagesTable.requestFocusInWindow();
        this.starImportPackagesTable.editCellAt(rowIndex, 0);
    }

    private void removeStarImportPackageButtonActionPerformed(ActionEvent evt) {
        int row = this.starImportPackagesTable.getSelectedRow();
        if (row >= 0) {
            TableCellEditor cellEditor = this.starImportPackagesTable.getCellEditor();
            if (cellEditor != null) {
                cellEditor.cancelCellEditing();
            }
            ((DefaultTableModel)this.starImportPackagesTable.getModel()).removeRow(row);
        }
    }

    private void addButtonActionPerformed(ActionEvent evt) {
        if (this.importLayoutTable.getColumnCount() == 1) {
            ((DefaultTableModel)this.importLayoutTable.getModel()).addRow(new Object[]{""});
        } else {
            ((DefaultTableModel)this.importLayoutTable.getModel()).addRow(new Object[]{false, ""});
        }
        int rowIndex = this.importLayoutTable.getRowCount() - 1;
        this.importLayoutTable.getSelectionModel().setSelectionInterval(rowIndex, rowIndex);
        this.importLayoutTable.requestFocusInWindow();
        this.importLayoutTable.editCellAt(rowIndex, this.importLayoutTable.getColumnCount() - 1);
    }

    private void moveUpButtonActionPerformed(ActionEvent evt) {
        int row = this.importLayoutTable.getSelectedRow();
        if (row > 0) {
            ((DefaultTableModel)this.importLayoutTable.getModel()).moveRow(row, row, row - 1);
            this.importLayoutTable.getSelectionModel().setSelectionInterval(row - 1, row - 1);
        }
    }

    private void moveDownButtonActionPerformed(ActionEvent evt) {
        int row = this.importLayoutTable.getSelectedRow();
        if (row >= 0 && row < this.importLayoutTable.getRowCount() - 1) {
            ((DefaultTableModel)this.importLayoutTable.getModel()).moveRow(row, row, row + 1);
            this.importLayoutTable.getSelectionModel().setSelectionInterval(row + 1, row + 1);
        }
    }

    private void removeButtonActionPerformed(ActionEvent evt) {
        int row = this.importLayoutTable.getSelectedRow();
        if (row >= 0) {
            TableCellEditor cellEditor = this.importLayoutTable.getCellEditor();
            if (cellEditor != null) {
                cellEditor.cancelCellEditing();
            }
            ((DefaultTableModel)this.importLayoutTable.getModel()).removeRow(row);
        }
    }

    private void separateStaticImportsCheckBoxActionPerformed(ActionEvent evt) {
        TableModel oldModel = this.importLayoutTable.getModel();
        DefaultTableModel newModel = (DefaultTableModel)FmtImports.createTableModel("importGroupsOrder", this.preferences);
        this.importLayoutTable.setModel(newModel);
        this.setImportLayoutTableColumnsWidth();
        for (TableModelListener l : ((DefaultTableModel)oldModel).getTableModelListeners()) {
            oldModel.removeTableModelListener(l);
            newModel.addTableModelListener(l);
            l.tableChanged(null);
        }
    }

    private void starImportTresholdCheckBoxActionPerformed(ActionEvent evt) {
        this.starImportTresholdSpinner.setEnabled(this.starImportTresholdCheckBox.isSelected());
    }

    private void starStaticImportTresholdCheckBoxActionPerformed(ActionEvent evt) {
        this.startStaticImportTresholdSpinner.setEnabled(this.starStaticImportTresholdCheckBox.isSelected());
    }

    private void enableControls(boolean b) {
        this.importInnerClassesCheckBox.setEnabled(b);
        this.starImportTresholdCheckBox.setEnabled(b);
        this.starImportTresholdSpinner.setEnabled(b && this.starImportTresholdCheckBox.isSelected());
        this.starStaticImportTresholdCheckBox.setEnabled(b);
        this.startStaticImportTresholdSpinner.setEnabled(b && this.starStaticImportTresholdCheckBox.isSelected());
        this.starImportPackagesLabel.setEnabled(b);
        this.starImportPackagesTable.setEnabled(b);
        this.addStarImportPackageButton.setEnabled(b);
        this.enableStarImportPackageButton(b);
    }

    private void enableStarImportPackageButton(boolean b) {
        this.removeStarImportPackageButton.setEnabled(b && this.starImportPackagesTable.getSelectedRow() >= 0);
    }

    private void enableImportLayoutButtons() {
        int row = this.importLayoutTable.getSelectedRow();
        this.moveUpButton.setEnabled(row > 0);
        this.moveDownButton.setEnabled(row >= 0 && row < this.importLayoutTable.getRowCount() - 1);
        this.removeButton.setEnabled(row >= 0 && allOtherImports != this.importLayoutTable.getValueAt(row, this.importLayoutTable.getColumnCount() - 1));
    }

    private void setStarImportPackagesTableColumnsWidth() {
        int tableWidth = this.starImportPackagesTable.getPreferredSize().width;
        TableColumn column = this.starImportPackagesTable.getColumnModel().getColumn(1);
        int colWidth = this.starImportPackagesTable.getTableHeader().getDefaultRenderer().getTableCellRendererComponent((JTable)this.starImportPackagesTable, (Object)column.getHeaderValue(), (boolean)false, (boolean)false, (int)0, (int)0).getPreferredSize().width;
        column.setPreferredWidth(colWidth);
        this.starImportPackagesTable.getColumnModel().getColumn(0).setPreferredWidth(tableWidth - colWidth);
    }

    private void setImportLayoutTableColumnsWidth() {
        if (this.importLayoutTable.getColumnCount() > 1) {
            int tableWidth = this.importLayoutTable.getPreferredSize().width;
            TableColumn column = this.importLayoutTable.getColumnModel().getColumn(0);
            int colWidth = this.importLayoutTable.getTableHeader().getDefaultRenderer().getTableCellRendererComponent((JTable)this.importLayoutTable, (Object)column.getHeaderValue(), (boolean)false, (boolean)false, (int)0, (int)0).getPreferredSize().width;
            column.setPreferredWidth(colWidth);
            this.importLayoutTable.getColumnModel().getColumn(1).setPreferredWidth(tableWidth - colWidth);
        }
    }

    private static TableModel createTableModel(String optionID, Preferences node) {
        DefaultTableModel model = null;
        if ("packagesForStarImport".equals(optionID)) {
            model = new DefaultTableModel(new Object[0][], new String[]{NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_Package"), NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_WithSub")}){

                @Override
                public Class<?> getColumnClass(int columnIndex) {
                    return columnIndex == 0 ? String.class : Boolean.class;
                }
            };
        } else {
            Object[] arrobject;
            boolean separate = node.getBoolean("separateStaticImports", FmtOptions.getDefaultAsBoolean("separateStaticImports"));
            if (separate) {
                String[] arrstring = new String[2];
                arrstring[0] = NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_Static");
                arrobject = arrstring;
                arrstring[1] = NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_Package");
            } else {
                Object[] arrobject2 = new String[1];
                arrobject = arrobject2;
                arrobject2[0] = NbBundle.getMessage(FmtImports.class, (String)"LBL_imp_Package");
            }
            Object[] colNames = arrobject;
            model = new DefaultTableModel(new Object[0][], colNames){

                @Override
                public Class<?> getColumnClass(int columnIndex) {
                    return columnIndex == this.getColumnCount() - 1 ? String.class : Boolean.class;
                }

                @Override
                public boolean isCellEditable(int row, int column) {
                    return FmtImports.allOtherImports != this.getValueAt(row, this.getColumnCount() - 1);
                }
            };
        }
        if (model != null) {
            boolean containsOtherImports = false;
            boolean containsStaticOtherImports = false;
            String value = node.get(optionID, FmtOptions.getDefaultAsString(optionID));
            for (String s : value.trim().split("\\s*[,;]\\s*")) {
                boolean isStatic = false;
                boolean isStar = false;
                if (s.startsWith("static ")) {
                    isStatic = true;
                    s = s.substring(7);
                }
                if (s.endsWith(".*")) {
                    isStar = true;
                    s = s.substring(0, s.length() - 2);
                }
                if (s.length() <= 0 || "importGroupsOrder".equals(optionID) && model.getColumnCount() <= 1 && isStatic) continue;
                if ("*".equals(s)) {
                    s = allOtherImports;
                    if (isStatic) {
                        containsStaticOtherImports = true;
                    } else {
                        containsOtherImports = true;
                    }
                }
                Object[] val = new Object[model.getColumnCount()];
                for (int i = 0; i < val.length; ++i) {
                    val[i] = Boolean.class.equals(model.getColumnClass(i)) ? Boolean.valueOf(i == 0 ? isStatic : isStar) : s;
                }
                model.addRow(val);
            }
            if ("importGroupsOrder".equals(optionID)) {
                if (model.getColumnCount() == 1) {
                    if (!containsOtherImports) {
                        model.addRow(new Object[]{allOtherImports});
                    }
                } else {
                    if (!containsOtherImports) {
                        model.addRow(new Object[]{false, allOtherImports});
                    }
                    if (!containsStaticOtherImports) {
                        model.addRow(new Object[]{true, allOtherImports});
                    }
                }
            }
        }
        return model;
    }

    private static final class ImportsCategorySupport
    extends FmtOptions.DocumentCategorySupport {
        private ImportsCategorySupport(Preferences preferences, JPanel panel) {
            super(preferences, "imports", panel, NbBundle.getMessage(FmtImports.class, (String)"SAMPLE_Imports"), new String[0][]);
        }

        @Override
        protected void loadTableData(JTable table, String optionID, Preferences p) {
            TableModel model = FmtImports.createTableModel(optionID, p);
            table.setModel(model);
        }

        @Override
        protected void storeTableData(JTable table, String optionID, Preferences node) {
            String value;
            StringBuilder sb = null;
            for (int i = 0; i < table.getRowCount(); ++i) {
                if (sb == null) {
                    sb = new StringBuilder();
                } else {
                    sb.append(';');
                }
                for (int j = 0; j < table.getColumnCount(); ++j) {
                    if (Boolean.class.equals(table.getColumnClass(j))) {
                        if (!((Boolean)table.getValueAt(i, j)).booleanValue()) continue;
                        sb.append(j == 0 ? "static " : ".*");
                        continue;
                    }
                    Object val = table.getValueAt(i, j);
                    sb.append((Object)(FmtImports.allOtherImports == val ? "*" : val));
                }
            }
            String string = value = sb != null ? sb.toString() : "";
            if (FmtOptions.getDefaultAsString(optionID).equals(value)) {
                node.remove(optionID);
            } else {
                node.put(optionID, value);
            }
        }

        @Override
        protected void doModification(ResultIterator resultIterator) throws Exception {
            WorkingCopy copy = WorkingCopy.get(resultIterator.getParserResult());
            copy.toPhase(JavaSource.Phase.RESOLVED);
            CompilationUnitTree cut = copy.getCompilationUnit();
            ClassTree ct = (ClassTree)cut.getTypeDecls().get(0);
            MethodTree mt = (MethodTree)ct.getMembers().get(1);
            TreeMaker treeMaker = copy.getTreeMaker();
            BlockTree bt = mt.getBody();
            VariableTree stmt = treeMaker.Variable(treeMaker.Modifiers(EnumSet.noneOf(Modifier.class)), "is", (Tree)treeMaker.QualIdent("java.io.InputStream"), (ExpressionTree)treeMaker.Literal(null));
            bt = treeMaker.addBlockStatement(bt, (StatementTree)stmt);
            BlockTree tryBlock = treeMaker.Block(Collections.emptyList(), false);
            NewClassTree et = treeMaker.NewClass(null, Collections.emptyList(), treeMaker.QualIdent("java.io.File"), Collections.singletonList(treeMaker.Literal("test.txt")), null);
            stmt = treeMaker.Variable(treeMaker.Modifiers(EnumSet.noneOf(Modifier.class)), "f", (Tree)treeMaker.QualIdent("java.io.File"), (ExpressionTree)et);
            tryBlock = treeMaker.addBlockStatement(tryBlock, (StatementTree)stmt);
            et = treeMaker.NewClass(null, Collections.emptyList(), treeMaker.QualIdent("java.io.FileInputStream"), Collections.singletonList(treeMaker.Identifier("f")), null);
            et = treeMaker.Assignment((ExpressionTree)treeMaker.Identifier("is"), (ExpressionTree)et);
            tryBlock = treeMaker.addBlockStatement(tryBlock, (StatementTree)treeMaker.ExpressionStatement((ExpressionTree)et));
            et = treeMaker.MemberSelect((ExpressionTree)treeMaker.Identifier("is"), "read");
            et = treeMaker.MethodInvocation(Collections.emptyList(), (ExpressionTree)et, Collections.emptyList());
            stmt = treeMaker.Try(treeMaker.Block(Collections.singletonList(treeMaker.ExpressionStatement((ExpressionTree)et)), false), Collections.emptyList(), null);
            et = treeMaker.MethodInvocation(Collections.emptyList(), (ExpressionTree)treeMaker.MemberSelect(treeMaker.QualIdent("java.util.logging.Logger"), "getLogger"), Collections.emptyList());
            et = treeMaker.addMethodInvocationArgument((MethodInvocationTree)et, (ExpressionTree)treeMaker.MethodInvocation(Collections.emptyList(), (ExpressionTree)treeMaker.MemberSelect((ExpressionTree)treeMaker.MemberSelect(treeMaker.QualIdent("org.netbeans.samples.ClassA"), "class"), "getName"), Collections.emptyList()));
            et = treeMaker.MethodInvocation(Collections.emptyList(), (ExpressionTree)treeMaker.MemberSelect((ExpressionTree)et, "log"), Collections.emptyList());
            et = treeMaker.addMethodInvocationArgument((MethodInvocationTree)et, (ExpressionTree)treeMaker.MemberSelect(treeMaker.QualIdent("java.util.logging.Logger"), "SEVERE"));
            et = treeMaker.addMethodInvocationArgument((MethodInvocationTree)et, (ExpressionTree)treeMaker.Literal(null));
            et = treeMaker.addMethodInvocationArgument((MethodInvocationTree)et, (ExpressionTree)treeMaker.Identifier("ex"));
            BlockTree catchBlock = treeMaker.Block(Collections.singletonList(treeMaker.ExpressionStatement((ExpressionTree)et)), false);
            stmt = treeMaker.addTryCatch((TryTree)stmt, treeMaker.Catch(treeMaker.Variable(treeMaker.Modifiers(EnumSet.noneOf(Modifier.class)), "ex", (Tree)treeMaker.QualIdent("java.io.IOException"), null), catchBlock));
            tryBlock = treeMaker.addBlockStatement(tryBlock, (StatementTree)stmt);
            et = treeMaker.MemberSelect((ExpressionTree)treeMaker.Identifier("is"), "close");
            et = treeMaker.MethodInvocation(Collections.emptyList(), (ExpressionTree)et, Collections.emptyList());
            stmt = treeMaker.Try(treeMaker.Block(Collections.singletonList(treeMaker.ExpressionStatement((ExpressionTree)et)), false), Collections.emptyList(), null);
            stmt = treeMaker.addTryCatch((TryTree)stmt, treeMaker.Catch(treeMaker.Variable(treeMaker.Modifiers(EnumSet.noneOf(Modifier.class)), "ex", (Tree)treeMaker.QualIdent("java.io.IOException"), null), catchBlock));
            stmt = treeMaker.Try(tryBlock, Collections.emptyList(), treeMaker.Block(Collections.singletonList(stmt), false));
            stmt = treeMaker.addTryCatch((TryTree)stmt, treeMaker.Catch(treeMaker.Variable(treeMaker.Modifiers(EnumSet.noneOf(Modifier.class)), "ex", (Tree)treeMaker.QualIdent("java.io.FileNotFoundException"), null), catchBlock));
            bt = treeMaker.addBlockStatement(bt, (StatementTree)stmt);
            copy.rewrite((Tree)mt.getBody(), (Tree)bt);
        }
    }

}

