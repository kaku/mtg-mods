/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlRenderer
 *  org.openide.awt.HtmlRenderer$Renderer
 */
package org.netbeans.modules.java.ui;

import java.awt.Component;
import java.util.Set;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTree;
import javax.swing.ListCellRenderer;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;
import org.netbeans.api.java.source.UiUtils;
import org.openide.awt.HtmlRenderer;

public final class Renderers {
    private Renderers() {
    }

    public static TreeCellRenderer declarationTreeRenderer() {
        return new DeclarationTreeRenderer();
    }

    public static ListCellRenderer declarationListRenderer() {
        return new DeclarationTreeRenderer();
    }

    private static class DeclarationTreeRenderer
    implements TreeCellRenderer,
    ListCellRenderer {
        HtmlRenderer.Renderer renderer = HtmlRenderer.createRenderer();

        private DeclarationTreeRenderer() {
        }

        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            String toolTip;
            Icon icon;
            String name;
            if (value instanceof DefaultMutableTreeNode) {
                value = ((DefaultMutableTreeNode)value).getUserObject();
            }
            if (value instanceof TypeElement) {
                TypeElement te = (TypeElement)value;
                name = DeclarationTreeRenderer.getDisplayName(te);
                toolTip = DeclarationTreeRenderer.getToolTipText(te);
                icon = UiUtils.getElementIcon(te.getKind(), te.getModifiers());
            } else {
                name = "??";
                name = value == null ? "NULL" : value.toString();
                toolTip = name;
                icon = null;
            }
            JLabel comp = (JLabel)this.renderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            comp.setText(name);
            comp.setToolTipText(toolTip);
            if (icon != null) {
                comp.setIcon(icon);
            }
            return comp;
        }

        @Override
        public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
            String toolTip;
            Icon icon;
            String name;
            if (value instanceof DefaultMutableTreeNode) {
                value = ((DefaultMutableTreeNode)value).getUserObject();
            }
            if (value instanceof TypeElement) {
                TypeElement te = (TypeElement)value;
                name = DeclarationTreeRenderer.getDisplayName(te);
                toolTip = DeclarationTreeRenderer.getToolTipText(te);
                icon = UiUtils.getElementIcon(te.getKind(), te.getModifiers());
            } else {
                name = "???";
                name = value == null ? "NULL" : value.toString();
                toolTip = name;
                icon = null;
            }
            JLabel comp = (JLabel)this.renderer.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
            comp.setText(name);
            comp.setToolTipText(toolTip);
            if (icon != null) {
                comp.setIcon(icon);
            }
            return comp;
        }

        private static String getDisplayName(TypeElement te) {
            boolean deprecated = false;
            String simpleName = te.getSimpleName().toString();
            String qualifiedName = te.getQualifiedName().toString();
            int lastIndex = qualifiedName.length() - simpleName.length();
            lastIndex = lastIndex == 0 ? lastIndex : lastIndex - 1;
            return "<html><b>" + (deprecated ? "<s>" : "") + simpleName + (deprecated ? "</s></b>" : "</b>") + "<font color=\"#707070\"> (" + qualifiedName.substring(0, lastIndex) + ")</font></html>";
        }

        private static String getToolTipText(TypeElement value) {
            return value.getQualifiedName().toString();
        }
    }

}

