/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.AnnotationTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  org.netbeans.core.spi.multiview.text.MultiViewEditorElement
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectExistsException
 *  org.openide.loaders.MultiDataObject
 *  org.openide.loaders.MultiDataObject$Entry
 *  org.openide.loaders.MultiFileLoader
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.java;

import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.Tree;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import javax.lang.model.element.Name;
import javax.tools.JavaFileObject;
import org.netbeans.api.java.loaders.JavaDataSupport;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.TreeMaker;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.core.spi.multiview.text.MultiViewEditorElement;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

public final class JavaDataObject
extends MultiDataObject {
    public JavaDataObject(FileObject pf, MultiFileLoader loader) throws DataObjectExistsException {
        super(pf, loader);
        this.registerEditor("text/x-java", true);
    }

    public Node createNodeDelegate() {
        return JavaDataSupport.createJavaNode(this.getPrimaryFile());
    }

    protected int associateLookup() {
        return 1;
    }

    protected DataObject handleCopyRename(DataFolder df, String name, String ext) throws IOException {
        FileObject fo = this.getPrimaryEntry().copyRename(df.getPrimaryFile(), name, ext);
        DataObject dob = DataObject.find((FileObject)fo);
        return dob;
    }

    public static MultiViewEditorElement createMultiViewEditorElement(Lookup context) {
        return new MultiViewEditorElement(context);
    }

    static void renameFO(FileObject fileToUpdate, final String packageName, final String newName, final String originalName) throws IOException {
        JavaSource javaSource = JavaSource.forFileObject(fileToUpdate);
        Task<WorkingCopy> task = new Task<WorkingCopy>(){

            @Override
            public void run(WorkingCopy workingCopy) throws IOException {
                workingCopy.toPhase(JavaSource.Phase.RESOLVED);
                TreeMaker make = workingCopy.getTreeMaker();
                CompilationUnitTree compilationUnitTree = workingCopy.getCompilationUnit();
                CompilationUnitTree cutCopy = make.CompilationUnit(compilationUnitTree.getPackageAnnotations(), (ExpressionTree)("".equals(packageName) ? null : make.Identifier(packageName)), compilationUnitTree.getImports(), compilationUnitTree.getTypeDecls(), compilationUnitTree.getSourceFile());
                workingCopy.rewrite((Tree)compilationUnitTree, (Tree)cutCopy);
                if (originalName != null && !originalName.equals(newName)) {
                    for (Tree typeDecl : compilationUnitTree.getTypeDecls()) {
                        ClassTree clazz;
                        if (!TreeUtilities.CLASS_TREE_KINDS.contains((Object)typeDecl.getKind()) || !originalName.contentEquals((clazz = (ClassTree)typeDecl).getSimpleName())) continue;
                        Tree copy = make.setLabel(typeDecl, newName);
                        workingCopy.rewrite(typeDecl, copy);
                    }
                }
            }
        };
        javaSource.runModificationTask(task).commit();
    }

}

