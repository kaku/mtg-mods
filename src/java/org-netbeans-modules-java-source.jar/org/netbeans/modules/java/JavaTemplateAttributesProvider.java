/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.queries.SourceLevelQuery
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.CreateFromTemplateAttributesProvider
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.modules.SpecificationVersion
 */
package org.netbeans.modules.java;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.queries.SourceLevelQuery;
import org.openide.filesystems.FileObject;
import org.openide.loaders.CreateFromTemplateAttributesProvider;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.modules.SpecificationVersion;

public final class JavaTemplateAttributesProvider
implements CreateFromTemplateAttributesProvider {
    private static final Logger LOG = Logger.getLogger(JavaTemplateAttributesProvider.class.getName());
    private static final SpecificationVersion VER15 = new SpecificationVersion("1.5");

    public Map<String, ?> attributesFor(DataObject template, DataFolder target, String name) {
        FileObject templateFO = template.getPrimaryFile();
        if (!"java".equals(templateFO.getExt()) || templateFO.isFolder()) {
            return null;
        }
        FileObject targetFO = target.getPrimaryFile();
        HashMap<String, Object> result = new HashMap<String, Object>();
        ClassPath cp = ClassPath.getClassPath((FileObject)targetFO, (String)"classpath/source");
        if (cp == null) {
            LOG.warning("No classpath was found for folder: " + (Object)target.getPrimaryFile());
        } else {
            result.put("package", cp.getResourceName(targetFO, '.', false));
        }
        String sourceLevel = SourceLevelQuery.getSourceLevel((FileObject)targetFO);
        if (sourceLevel != null) {
            result.put("javaSourceLevel", sourceLevel);
            if (this.isJava15orLater(sourceLevel)) {
                result.put("java15style", Boolean.TRUE);
            }
        }
        return result;
    }

    private boolean isJava15orLater(String sourceLevel) {
        SpecificationVersion ver = new SpecificationVersion(sourceLevel);
        return ver.compareTo((Object)VER15) >= 0;
    }
}

