/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.CreateFromTemplateAttributesProvider
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectExistsException
 *  org.openide.loaders.ExtensionList
 *  org.openide.loaders.FileEntry
 *  org.openide.loaders.MultiDataObject
 *  org.openide.loaders.MultiDataObject$Entry
 *  org.openide.loaders.MultiFileLoader
 *  org.openide.loaders.UniFileLoader
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.Format;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.loaders.JavaDataSupport;
import org.netbeans.modules.java.IndentFileEntry;
import org.netbeans.modules.java.JMapFormat;
import org.netbeans.modules.java.JavaDataObject;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.CreateFromTemplateAttributesProvider;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.ExtensionList;
import org.openide.loaders.FileEntry;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.loaders.UniFileLoader;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public final class JavaDataLoader
extends UniFileLoader {
    public static final String JAVA_MIME_TYPE = "text/x-java";
    public static final String JAVA_EXTENSION = "java";
    private static final String PACKAGE_INFO = "package-info";
    static final long serialVersionUID = -6286836352608877232L;

    public JavaDataLoader() {
        super("org.netbeans.modules.java.JavaDataObject");
    }

    protected void initialize() {
        super.initialize();
        ExtensionList extensions = new ExtensionList();
        extensions.addExtension("java");
        extensions.addMimeType("text/x-java");
        this.setExtensions(extensions);
    }

    protected String actionsContext() {
        return "Loaders/text/x-java/Actions/";
    }

    protected String defaultDisplayName() {
        return NbBundle.getMessage(JavaDataLoader.class, (String)"PROP_JavaLoader_Name");
    }

    protected MultiDataObject createMultiObject(FileObject primaryFile) throws DataObjectExistsException, IOException {
        if (this.getExtensions().isRegistered(primaryFile)) {
            return new JavaDataObject(primaryFile, (MultiFileLoader)this);
        }
        return null;
    }

    protected FileObject findPrimaryFile(FileObject fo) {
        if (fo.isFolder()) {
            return null;
        }
        return super.findPrimaryFile(fo);
    }

    protected MultiDataObject.Entry createPrimaryEntry(MultiDataObject obj, FileObject primaryFile) {
        if (this.getExtensions().isRegistered(primaryFile)) {
            return JavaDataSupport.createJavaFileEntry(obj, primaryFile);
        }
        return new FileEntry(obj, primaryFile);
    }

    static Map<String, String> createStringsMap() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("USER", System.getProperty("user.name"));
        Date d = new Date();
        map.put("DATE", DateFormat.getDateInstance(1).format(d));
        map.put("TIME", DateFormat.getTimeInstance(3).format(d));
        return map;
    }

    public static class JavaFileEntry
    extends IndentFileEntry {
        static final long serialVersionUID = 8244159045498569616L;

        public JavaFileEntry(MultiDataObject obj, FileObject file) {
            super(obj, file);
        }

        protected Format createFormat(FileObject target, String n, String e) {
            Map<String, String> map = JavaDataLoader.createStringsMap();
            this.modifyMap(map, target, n, e);
            JMapFormat format = new JMapFormat(map);
            format.setLeftBrace("__");
            format.setRightBrace("__");
            format.setCondDelimiter("$");
            format.setExactMatch(false);
            return format;
        }

        protected void modifyMap(Map<String, String> map, FileObject target, String n, String e) {
            ClassPath cp = ClassPath.getClassPath((FileObject)target, (String)"classpath/source");
            String resourcePath = "";
            if (cp != null) {
                resourcePath = cp.getResourceName(target);
                if (resourcePath == null) {
                    Logger.getLogger(JavaDataLoader.class.getName()).log(Level.WARNING, "{0} is not on its own source path", FileUtil.getFileDisplayName((FileObject)target));
                    resourcePath = "";
                }
            } else {
                Logger.getLogger(JavaDataLoader.class.getName()).warning("No classpath was found for folder: " + (Object)target);
            }
            map.put("NAME", n);
            map.put("PACKAGE", resourcePath.replace('/', '.'));
            map.put("PACKAGE_SLASHES", resourcePath);
            if (target.isRoot()) {
                map.put("PACKAGE_AND_NAME", n);
                map.put("PACKAGE_AND_NAME_SLASHES", n);
            } else {
                map.put("PACKAGE_AND_NAME", resourcePath.replace('/', '.') + '.' + n);
                map.put("PACKAGE_AND_NAME_SLASHES", resourcePath + '/' + n);
            }
            map.put("QUOTES", "\"");
            for (CreateFromTemplateAttributesProvider provider : Lookup.getDefault().lookupAll(CreateFromTemplateAttributesProvider.class)) {
                Object aName;
                Map attrs = provider.attributesFor((DataObject)this.getDataObject(), DataFolder.findFolder((FileObject)target), n);
                if (attrs == null || !((aName = attrs.get("user")) instanceof String)) continue;
                map.put("USER", (String)aName);
                break;
            }
        }

        @Override
        public FileObject createFromTemplate(FileObject f, String name) throws IOException {
            String pkgName;
            if (this.getFile().getAttribute("javax.script.ScriptEngine") == null) {
                Logger.getLogger(JavaDataLoader.class.getName()).log(Level.WARNING, "Please replace template {0} with the new scripting support. See http://bits.netbeans.org/7.1/javadoc/org-openide-loaders/apichanges.html#scripting", this.getFile().getPath());
            }
            if (name == null) {
                name = FileUtil.findFreeFileName((FileObject)f, (String)f.getName(), (String)"java");
            } else if (!"package-info".equals(name) && !Utilities.isJavaIdentifier((String)name)) {
                throw new IOException(NbBundle.getMessage(JavaDataObject.class, (String)"FMT_Not_Valid_FileName", (Object)name));
            }
            this.initializeIndentEngine();
            FileObject fo = super.createFromTemplate(f, name);
            ClassPath cp = ClassPath.getClassPath((FileObject)fo, (String)"classpath/source");
            if (cp != null) {
                pkgName = cp.getResourceName(f, '.', false);
                if (pkgName == null) {
                    Logger.getLogger(JavaDataLoader.class.getName()).log(Level.WARNING, "{0} is not on its own source path", FileUtil.getFileDisplayName((FileObject)fo));
                    pkgName = "";
                }
            } else {
                pkgName = "";
            }
            JavaDataObject.renameFO(fo, pkgName, name, this.getFile().getName());
            this.destroyDataObject(fo);
            return fo;
        }

        private void destroyDataObject(FileObject fo) throws IOException {
            DataObject dobj = DataObject.find((FileObject)fo);
            try {
                dobj.setValid(false);
            }
            catch (PropertyVetoException ex) {
                throw (IOException)new IOException().initCause(ex);
            }
        }
    }

}

