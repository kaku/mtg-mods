/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.doctree.AttributeTree
 *  com.sun.source.doctree.AttributeTree$ValueKind
 *  com.sun.source.doctree.AuthorTree
 *  com.sun.source.doctree.CommentTree
 *  com.sun.source.doctree.DeprecatedTree
 *  com.sun.source.doctree.DocCommentTree
 *  com.sun.source.doctree.DocRootTree
 *  com.sun.source.doctree.DocTree
 *  com.sun.source.doctree.EndElementTree
 *  com.sun.source.doctree.EntityTree
 *  com.sun.source.doctree.ErroneousTree
 *  com.sun.source.doctree.IdentifierTree
 *  com.sun.source.doctree.InheritDocTree
 *  com.sun.source.doctree.LinkTree
 *  com.sun.source.doctree.LiteralTree
 *  com.sun.source.doctree.ParamTree
 *  com.sun.source.doctree.ReferenceTree
 *  com.sun.source.doctree.ReturnTree
 *  com.sun.source.doctree.SeeTree
 *  com.sun.source.doctree.SerialDataTree
 *  com.sun.source.doctree.SerialFieldTree
 *  com.sun.source.doctree.SerialTree
 *  com.sun.source.doctree.SinceTree
 *  com.sun.source.doctree.StartElementTree
 *  com.sun.source.doctree.TextTree
 *  com.sun.source.doctree.ThrowsTree
 *  com.sun.source.doctree.UnknownBlockTagTree
 *  com.sun.source.doctree.UnknownInlineTagTree
 *  com.sun.source.doctree.ValueTree
 *  com.sun.source.doctree.VersionTree
 *  com.sun.source.tree.AnnotatedTypeTree
 *  com.sun.source.tree.AnnotationTree
 *  com.sun.source.tree.ArrayAccessTree
 *  com.sun.source.tree.ArrayTypeTree
 *  com.sun.source.tree.AssertTree
 *  com.sun.source.tree.AssignmentTree
 *  com.sun.source.tree.BinaryTree
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.BreakTree
 *  com.sun.source.tree.CaseTree
 *  com.sun.source.tree.CatchTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.CompoundAssignmentTree
 *  com.sun.source.tree.ConditionalExpressionTree
 *  com.sun.source.tree.ContinueTree
 *  com.sun.source.tree.DoWhileLoopTree
 *  com.sun.source.tree.EmptyStatementTree
 *  com.sun.source.tree.EnhancedForLoopTree
 *  com.sun.source.tree.ErroneousTree
 *  com.sun.source.tree.ExpressionStatementTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.ForLoopTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.IfTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.InstanceOfTree
 *  com.sun.source.tree.IntersectionTypeTree
 *  com.sun.source.tree.LabeledStatementTree
 *  com.sun.source.tree.LambdaExpressionTree
 *  com.sun.source.tree.LiteralTree
 *  com.sun.source.tree.MemberReferenceTree
 *  com.sun.source.tree.MemberReferenceTree$ReferenceMode
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodInvocationTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.NewArrayTree
 *  com.sun.source.tree.NewClassTree
 *  com.sun.source.tree.ParameterizedTypeTree
 *  com.sun.source.tree.ParenthesizedTree
 *  com.sun.source.tree.PrimitiveTypeTree
 *  com.sun.source.tree.ReturnTree
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.SwitchTree
 *  com.sun.source.tree.SynchronizedTree
 *  com.sun.source.tree.ThrowTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.TryTree
 *  com.sun.source.tree.TypeCastTree
 *  com.sun.source.tree.TypeParameterTree
 *  com.sun.source.tree.UnaryTree
 *  com.sun.source.tree.UnionTypeTree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.tree.WhileLoopTree
 *  com.sun.source.tree.WildcardTree
 *  com.sun.tools.javac.code.BoundKind
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$MethodSymbol
 *  com.sun.tools.javac.code.Symbol$TypeSymbol
 *  com.sun.tools.javac.code.Symbol$VarSymbol
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.code.Type$ErrorType
 *  com.sun.tools.javac.code.Type$WildcardType
 *  com.sun.tools.javac.code.TypeTag
 *  com.sun.tools.javac.jvm.ClassReader
 *  com.sun.tools.javac.model.JavacElements
 *  com.sun.tools.javac.model.JavacTypes
 *  com.sun.tools.javac.parser.Tokens
 *  com.sun.tools.javac.parser.Tokens$Comment
 *  com.sun.tools.javac.tree.DCTree
 *  com.sun.tools.javac.tree.DCTree$DCAttribute
 *  com.sun.tools.javac.tree.DCTree$DCAuthor
 *  com.sun.tools.javac.tree.DCTree$DCComment
 *  com.sun.tools.javac.tree.DCTree$DCDeprecated
 *  com.sun.tools.javac.tree.DCTree$DCDocComment
 *  com.sun.tools.javac.tree.DCTree$DCDocRoot
 *  com.sun.tools.javac.tree.DCTree$DCEndElement
 *  com.sun.tools.javac.tree.DCTree$DCEntity
 *  com.sun.tools.javac.tree.DCTree$DCIdentifier
 *  com.sun.tools.javac.tree.DCTree$DCInheritDoc
 *  com.sun.tools.javac.tree.DCTree$DCLink
 *  com.sun.tools.javac.tree.DCTree$DCLiteral
 *  com.sun.tools.javac.tree.DCTree$DCParam
 *  com.sun.tools.javac.tree.DCTree$DCReference
 *  com.sun.tools.javac.tree.DCTree$DCReturn
 *  com.sun.tools.javac.tree.DCTree$DCSee
 *  com.sun.tools.javac.tree.DCTree$DCSerial
 *  com.sun.tools.javac.tree.DCTree$DCSerialData
 *  com.sun.tools.javac.tree.DCTree$DCSerialField
 *  com.sun.tools.javac.tree.DCTree$DCSince
 *  com.sun.tools.javac.tree.DCTree$DCStartElement
 *  com.sun.tools.javac.tree.DCTree$DCText
 *  com.sun.tools.javac.tree.DCTree$DCThrows
 *  com.sun.tools.javac.tree.DCTree$DCUnknownBlockTag
 *  com.sun.tools.javac.tree.DCTree$DCUnknownInlineTag
 *  com.sun.tools.javac.tree.DCTree$DCValue
 *  com.sun.tools.javac.tree.DCTree$DCVersion
 *  com.sun.tools.javac.tree.DocTreeMaker
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCAnnotatedType
 *  com.sun.tools.javac.tree.JCTree$JCAnnotation
 *  com.sun.tools.javac.tree.JCTree$JCArrayAccess
 *  com.sun.tools.javac.tree.JCTree$JCArrayTypeTree
 *  com.sun.tools.javac.tree.JCTree$JCAssert
 *  com.sun.tools.javac.tree.JCTree$JCAssign
 *  com.sun.tools.javac.tree.JCTree$JCAssignOp
 *  com.sun.tools.javac.tree.JCTree$JCBinary
 *  com.sun.tools.javac.tree.JCTree$JCBlock
 *  com.sun.tools.javac.tree.JCTree$JCBreak
 *  com.sun.tools.javac.tree.JCTree$JCCase
 *  com.sun.tools.javac.tree.JCTree$JCCatch
 *  com.sun.tools.javac.tree.JCTree$JCClassDecl
 *  com.sun.tools.javac.tree.JCTree$JCCompilationUnit
 *  com.sun.tools.javac.tree.JCTree$JCConditional
 *  com.sun.tools.javac.tree.JCTree$JCContinue
 *  com.sun.tools.javac.tree.JCTree$JCDoWhileLoop
 *  com.sun.tools.javac.tree.JCTree$JCEnhancedForLoop
 *  com.sun.tools.javac.tree.JCTree$JCErroneous
 *  com.sun.tools.javac.tree.JCTree$JCExpression
 *  com.sun.tools.javac.tree.JCTree$JCExpressionStatement
 *  com.sun.tools.javac.tree.JCTree$JCFieldAccess
 *  com.sun.tools.javac.tree.JCTree$JCForLoop
 *  com.sun.tools.javac.tree.JCTree$JCIdent
 *  com.sun.tools.javac.tree.JCTree$JCIf
 *  com.sun.tools.javac.tree.JCTree$JCImport
 *  com.sun.tools.javac.tree.JCTree$JCInstanceOf
 *  com.sun.tools.javac.tree.JCTree$JCLabeledStatement
 *  com.sun.tools.javac.tree.JCTree$JCLambda
 *  com.sun.tools.javac.tree.JCTree$JCLambda$ParameterKind
 *  com.sun.tools.javac.tree.JCTree$JCLiteral
 *  com.sun.tools.javac.tree.JCTree$JCMemberReference
 *  com.sun.tools.javac.tree.JCTree$JCMethodDecl
 *  com.sun.tools.javac.tree.JCTree$JCMethodInvocation
 *  com.sun.tools.javac.tree.JCTree$JCModifiers
 *  com.sun.tools.javac.tree.JCTree$JCNewArray
 *  com.sun.tools.javac.tree.JCTree$JCNewClass
 *  com.sun.tools.javac.tree.JCTree$JCParens
 *  com.sun.tools.javac.tree.JCTree$JCPrimitiveTypeTree
 *  com.sun.tools.javac.tree.JCTree$JCReturn
 *  com.sun.tools.javac.tree.JCTree$JCSkip
 *  com.sun.tools.javac.tree.JCTree$JCStatement
 *  com.sun.tools.javac.tree.JCTree$JCSwitch
 *  com.sun.tools.javac.tree.JCTree$JCSynchronized
 *  com.sun.tools.javac.tree.JCTree$JCThrow
 *  com.sun.tools.javac.tree.JCTree$JCTry
 *  com.sun.tools.javac.tree.JCTree$JCTypeApply
 *  com.sun.tools.javac.tree.JCTree$JCTypeCast
 *  com.sun.tools.javac.tree.JCTree$JCTypeIntersection
 *  com.sun.tools.javac.tree.JCTree$JCTypeParameter
 *  com.sun.tools.javac.tree.JCTree$JCTypeUnion
 *  com.sun.tools.javac.tree.JCTree$JCUnary
 *  com.sun.tools.javac.tree.JCTree$JCVariableDecl
 *  com.sun.tools.javac.tree.JCTree$JCWhileLoop
 *  com.sun.tools.javac.tree.JCTree$JCWildcard
 *  com.sun.tools.javac.tree.JCTree$Tag
 *  com.sun.tools.javac.tree.JCTree$TypeBoundKind
 *  com.sun.tools.javac.tree.TreeMaker
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Key
 *  com.sun.tools.javac.util.DiagnosticSource
 *  com.sun.tools.javac.util.List
 *  com.sun.tools.javac.util.ListBuffer
 *  com.sun.tools.javac.util.Name
 *  com.sun.tools.javac.util.Names
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.java.source.builder;

import com.sun.source.doctree.AttributeTree;
import com.sun.source.doctree.AuthorTree;
import com.sun.source.doctree.CommentTree;
import com.sun.source.doctree.DeprecatedTree;
import com.sun.source.doctree.DocCommentTree;
import com.sun.source.doctree.DocRootTree;
import com.sun.source.doctree.DocTree;
import com.sun.source.doctree.EndElementTree;
import com.sun.source.doctree.EntityTree;
import com.sun.source.doctree.InheritDocTree;
import com.sun.source.doctree.LinkTree;
import com.sun.source.doctree.ParamTree;
import com.sun.source.doctree.ReferenceTree;
import com.sun.source.doctree.ReturnTree;
import com.sun.source.doctree.SeeTree;
import com.sun.source.doctree.SerialDataTree;
import com.sun.source.doctree.SerialFieldTree;
import com.sun.source.doctree.SerialTree;
import com.sun.source.doctree.SinceTree;
import com.sun.source.doctree.StartElementTree;
import com.sun.source.doctree.TextTree;
import com.sun.source.doctree.ThrowsTree;
import com.sun.source.doctree.UnknownBlockTagTree;
import com.sun.source.doctree.UnknownInlineTagTree;
import com.sun.source.doctree.ValueTree;
import com.sun.source.doctree.VersionTree;
import com.sun.source.tree.AnnotatedTypeTree;
import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.ArrayAccessTree;
import com.sun.source.tree.ArrayTypeTree;
import com.sun.source.tree.AssertTree;
import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BinaryTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.BreakTree;
import com.sun.source.tree.CaseTree;
import com.sun.source.tree.CatchTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.CompoundAssignmentTree;
import com.sun.source.tree.ConditionalExpressionTree;
import com.sun.source.tree.ContinueTree;
import com.sun.source.tree.DoWhileLoopTree;
import com.sun.source.tree.EmptyStatementTree;
import com.sun.source.tree.EnhancedForLoopTree;
import com.sun.source.tree.ErroneousTree;
import com.sun.source.tree.ExpressionStatementTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.ForLoopTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.InstanceOfTree;
import com.sun.source.tree.IntersectionTypeTree;
import com.sun.source.tree.LabeledStatementTree;
import com.sun.source.tree.LambdaExpressionTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MemberReferenceTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.NewArrayTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.ParameterizedTypeTree;
import com.sun.source.tree.ParenthesizedTree;
import com.sun.source.tree.PrimitiveTypeTree;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.SwitchTree;
import com.sun.source.tree.SynchronizedTree;
import com.sun.source.tree.ThrowTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TryTree;
import com.sun.source.tree.TypeCastTree;
import com.sun.source.tree.TypeParameterTree;
import com.sun.source.tree.UnaryTree;
import com.sun.source.tree.UnionTypeTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.tree.WhileLoopTree;
import com.sun.source.tree.WildcardTree;
import com.sun.tools.javac.code.BoundKind;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.TypeTag;
import com.sun.tools.javac.jvm.ClassReader;
import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.model.JavacTypes;
import com.sun.tools.javac.parser.Tokens;
import com.sun.tools.javac.tree.DCTree;
import com.sun.tools.javac.tree.DocTreeMaker;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.DiagnosticSource;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.ListBuffer;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.JavaFileObject;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.java.source.builder.ASTService;
import org.netbeans.modules.java.source.builder.CommentHandlerService;
import org.netbeans.modules.java.source.builder.QualIdentTree;

public class TreeFactory {
    Names names;
    ClassReader classReader;
    TreeMaker make;
    ASTService model;
    Elements elements;
    Types types;
    private final CommentHandlerService chs;
    private static final Context.Key<TreeFactory> contextKey = new Context.Key();
    private DocTreeMaker docMake;

    public static synchronized TreeFactory instance(Context context) {
        TreeFactory instance = (TreeFactory)context.get(contextKey);
        if (instance == null) {
            instance = new TreeFactory(context);
        }
        return instance;
    }

    protected TreeFactory(Context context) {
        context.put(contextKey, (Object)this);
        this.model = ASTService.instance(context);
        this.names = Names.instance((Context)context);
        this.classReader = ClassReader.instance((Context)context);
        this.make = TreeMaker.instance((Context)context);
        this.docMake = DocTreeMaker.instance((Context)context);
        this.elements = JavacElements.instance((Context)context);
        this.types = JavacTypes.instance((Context)context);
        this.chs = CommentHandlerService.instance(context);
        this.make.toplevel = null;
    }

    public AnnotationTree Annotation(Tree type, java.util.List<? extends ExpressionTree> arguments) {
        ListBuffer lb = new ListBuffer();
        for (ExpressionTree t : arguments) {
            lb.append((Object)((JCTree.JCExpression)t));
        }
        return this.make.at(-2).Annotation((JCTree)type, lb.toList());
    }

    public AnnotationTree TypeAnnotation(Tree type, java.util.List<? extends ExpressionTree> arguments) {
        ListBuffer lb = new ListBuffer();
        for (ExpressionTree t : arguments) {
            lb.append((Object)((JCTree.JCExpression)t));
        }
        return this.make.at(-2).TypeAnnotation((JCTree)type, lb.toList());
    }

    public AnnotatedTypeTree AnnotatedType(java.util.List<? extends AnnotationTree> annotations, Tree underlyingType) {
        ListBuffer lb = new ListBuffer();
        for (AnnotationTree t : annotations) {
            lb.append((Object)((JCTree.JCAnnotation)t));
        }
        return this.make.at(-2).AnnotatedType(lb.toList(), (JCTree.JCExpression)underlyingType);
    }

    public ArrayAccessTree ArrayAccess(ExpressionTree array, ExpressionTree index) {
        return this.make.at(-2).Indexed((JCTree.JCExpression)array, (JCTree.JCExpression)index);
    }

    public ArrayTypeTree ArrayType(Tree type) {
        return this.make.at(-2).TypeArray((JCTree.JCExpression)type);
    }

    public AssertTree Assert(ExpressionTree condition, ExpressionTree detail) {
        return this.make.at(-2).Assert((JCTree.JCExpression)condition, (JCTree.JCExpression)detail);
    }

    public AssignmentTree Assignment(ExpressionTree variable, ExpressionTree expression) {
        return this.make.at(-2).Assign((JCTree.JCExpression)variable, (JCTree.JCExpression)expression);
    }

    public BinaryTree Binary(Tree.Kind operator, ExpressionTree left, ExpressionTree right) {
        JCTree.Tag op;
        switch (operator) {
            case MULTIPLY: {
                op = JCTree.Tag.MUL;
                break;
            }
            case DIVIDE: {
                op = JCTree.Tag.DIV;
                break;
            }
            case REMAINDER: {
                op = JCTree.Tag.MOD;
                break;
            }
            case PLUS: {
                op = JCTree.Tag.PLUS;
                break;
            }
            case MINUS: {
                op = JCTree.Tag.MINUS;
                break;
            }
            case LEFT_SHIFT: {
                op = JCTree.Tag.SL;
                break;
            }
            case RIGHT_SHIFT: {
                op = JCTree.Tag.SR;
                break;
            }
            case UNSIGNED_RIGHT_SHIFT: {
                op = JCTree.Tag.USR;
                break;
            }
            case LESS_THAN: {
                op = JCTree.Tag.LT;
                break;
            }
            case GREATER_THAN: {
                op = JCTree.Tag.GT;
                break;
            }
            case LESS_THAN_EQUAL: {
                op = JCTree.Tag.LE;
                break;
            }
            case GREATER_THAN_EQUAL: {
                op = JCTree.Tag.GE;
                break;
            }
            case EQUAL_TO: {
                op = JCTree.Tag.EQ;
                break;
            }
            case NOT_EQUAL_TO: {
                op = JCTree.Tag.NE;
                break;
            }
            case AND: {
                op = JCTree.Tag.BITAND;
                break;
            }
            case XOR: {
                op = JCTree.Tag.BITXOR;
                break;
            }
            case OR: {
                op = JCTree.Tag.BITOR;
                break;
            }
            case CONDITIONAL_AND: {
                op = JCTree.Tag.AND;
                break;
            }
            case CONDITIONAL_OR: {
                op = JCTree.Tag.OR;
                break;
            }
            default: {
                throw new IllegalArgumentException("Illegal binary operator: " + (Object)operator);
            }
        }
        return this.make.at(-2).Binary(op, (JCTree.JCExpression)left, (JCTree.JCExpression)right);
    }

    public BlockTree Block(java.util.List<? extends StatementTree> statements, boolean isStatic) {
        ListBuffer lb = new ListBuffer();
        for (StatementTree t : statements) {
            lb.append((Object)((JCTree.JCStatement)t));
        }
        return this.make.at(-2).Block(isStatic ? 8 : 0, lb.toList());
    }

    public BreakTree Break(CharSequence label) {
        Name n = label != null ? this.names.fromString(label.toString()) : null;
        return this.make.at(-2).Break(n);
    }

    public CaseTree Case(ExpressionTree expression, java.util.List<? extends StatementTree> statements) {
        ListBuffer lb = new ListBuffer();
        for (StatementTree t : statements) {
            lb.append((Object)((JCTree.JCStatement)t));
        }
        return this.make.at(-2).Case((JCTree.JCExpression)expression, lb.toList());
    }

    public CatchTree Catch(VariableTree parameter, BlockTree block) {
        return this.make.at(-2).Catch((JCTree.JCVariableDecl)parameter, (JCTree.JCBlock)block);
    }

    public ClassTree Class(ModifiersTree modifiers, CharSequence simpleName, java.util.List<? extends TypeParameterTree> typeParameters, Tree extendsClause, java.util.List<? extends Tree> implementsClauses, java.util.List<? extends Tree> memberDecls) {
        ListBuffer typarams = new ListBuffer();
        for (TypeParameterTree t : typeParameters) {
            typarams.append((Object)((JCTree.JCTypeParameter)t));
        }
        ListBuffer impls = new ListBuffer();
        for (Tree t2 : implementsClauses) {
            impls.append((Object)((JCTree.JCExpression)t2));
        }
        ListBuffer defs = new ListBuffer();
        for (Tree t3 : memberDecls) {
            defs.append((Object)((JCTree)t3));
        }
        return this.make.at(-2).ClassDef((JCTree.JCModifiers)modifiers, this.names.fromString(simpleName.toString()), typarams.toList(), (JCTree.JCExpression)extendsClause, impls.toList(), defs.toList());
    }

    public ClassTree Interface(ModifiersTree modifiers, CharSequence simpleName, java.util.List<? extends TypeParameterTree> typeParameters, java.util.List<? extends Tree> extendsClauses, java.util.List<? extends Tree> memberDecls) {
        long flags = this.getBitFlags(modifiers.getFlags()) | 512;
        return this.Class(flags, (List)modifiers.getAnnotations(), simpleName, typeParameters, null, extendsClauses, memberDecls);
    }

    public ClassTree AnnotationType(ModifiersTree modifiers, CharSequence simpleName, java.util.List<? extends Tree> memberDecls) {
        long flags = this.getBitFlags(modifiers.getFlags()) | 8192;
        return this.Class(flags, (List)modifiers.getAnnotations(), simpleName, Collections.emptyList(), null, Collections.emptyList(), memberDecls);
    }

    public ClassTree Enum(ModifiersTree modifiers, CharSequence simpleName, java.util.List<? extends Tree> implementsClauses, java.util.List<? extends Tree> memberDecls) {
        long flags = this.getBitFlags(modifiers.getFlags()) | 16384;
        return this.Class(flags, (List)modifiers.getAnnotations(), simpleName, Collections.emptyList(), null, implementsClauses, memberDecls);
    }

    public CompilationUnitTree CompilationUnit(@NonNull java.util.List<? extends AnnotationTree> packageAnnotations, ExpressionTree packageDecl, java.util.List<? extends ImportTree> importDecls, java.util.List<? extends Tree> typeDecls, JavaFileObject sourceFile) {
        ListBuffer annotations = new ListBuffer();
        for (AnnotationTree at : packageAnnotations) {
            annotations.add((Object)((JCTree.JCAnnotation)at));
        }
        ListBuffer defs = new ListBuffer();
        if (importDecls != null) {
            for (Tree t : importDecls) {
                defs.append((Object)((JCTree)t));
            }
        }
        if (typeDecls != null) {
            for (Tree t : typeDecls) {
                defs.append((Object)((JCTree)t));
            }
        }
        JCTree.JCCompilationUnit unit = this.make.at(-2).TopLevel(annotations.toList(), (JCTree.JCExpression)packageDecl, defs.toList());
        unit.sourcefile = sourceFile;
        return unit;
    }

    public CompoundAssignmentTree CompoundAssignment(Tree.Kind operator, ExpressionTree variable, ExpressionTree expression) {
        JCTree.Tag op;
        switch (operator) {
            case MULTIPLY_ASSIGNMENT: {
                op = JCTree.Tag.MUL_ASG;
                break;
            }
            case DIVIDE_ASSIGNMENT: {
                op = JCTree.Tag.DIV_ASG;
                break;
            }
            case REMAINDER_ASSIGNMENT: {
                op = JCTree.Tag.MOD_ASG;
                break;
            }
            case PLUS_ASSIGNMENT: {
                op = JCTree.Tag.PLUS_ASG;
                break;
            }
            case MINUS_ASSIGNMENT: {
                op = JCTree.Tag.MINUS_ASG;
                break;
            }
            case LEFT_SHIFT_ASSIGNMENT: {
                op = JCTree.Tag.SL_ASG;
                break;
            }
            case RIGHT_SHIFT_ASSIGNMENT: {
                op = JCTree.Tag.SR_ASG;
                break;
            }
            case UNSIGNED_RIGHT_SHIFT_ASSIGNMENT: {
                op = JCTree.Tag.USR_ASG;
                break;
            }
            case AND_ASSIGNMENT: {
                op = JCTree.Tag.BITAND_ASG;
                break;
            }
            case XOR_ASSIGNMENT: {
                op = JCTree.Tag.BITXOR_ASG;
                break;
            }
            case OR_ASSIGNMENT: {
                op = JCTree.Tag.BITOR_ASG;
                break;
            }
            default: {
                throw new IllegalArgumentException("Illegal binary operator: " + (Object)operator);
            }
        }
        return this.make.at(-2).Assignop(op, (JCTree)((JCTree.JCExpression)variable), (JCTree)((JCTree.JCExpression)expression));
    }

    public ConditionalExpressionTree ConditionalExpression(ExpressionTree condition, ExpressionTree trueExpression, ExpressionTree falseExpression) {
        return this.make.at(-2).Conditional((JCTree.JCExpression)condition, (JCTree.JCExpression)trueExpression, (JCTree.JCExpression)falseExpression);
    }

    public ContinueTree Continue(CharSequence label) {
        Name n = label != null ? this.names.fromString(label.toString()) : null;
        return this.make.at(-2).Continue(n);
    }

    public UnionTypeTree UnionType(java.util.List<? extends Tree> typeComponents) {
        ListBuffer components = new ListBuffer();
        for (Tree t : typeComponents) {
            components.append((Object)((JCTree.JCExpression)t));
        }
        return this.make.at(-2).TypeUnion(components.toList());
    }

    public DoWhileLoopTree DoWhileLoop(ExpressionTree condition, StatementTree statement) {
        return this.make.at(-2).DoLoop((JCTree.JCStatement)statement, (JCTree.JCExpression)condition);
    }

    public EmptyStatementTree EmptyStatement() {
        return this.make.at(-2).Skip();
    }

    public EnhancedForLoopTree EnhancedForLoop(VariableTree variable, ExpressionTree expression, StatementTree statement) {
        return this.make.at(-2).ForeachLoop((JCTree.JCVariableDecl)variable, (JCTree.JCExpression)expression, (JCTree.JCStatement)statement);
    }

    public ErroneousTree Erroneous(java.util.List<? extends Tree> errorTrees) {
        ListBuffer errors = new ListBuffer();
        for (Tree t : errorTrees) {
            errors.append((Object)((JCTree)t));
        }
        return this.make.at(-2).Erroneous(errors.toList());
    }

    public ExpressionStatementTree ExpressionStatement(ExpressionTree expression) {
        return this.make.at(-2).Exec((JCTree.JCExpression)expression);
    }

    public ForLoopTree ForLoop(java.util.List<? extends StatementTree> initializer, ExpressionTree condition, java.util.List<? extends ExpressionStatementTree> update, StatementTree statement) {
        ListBuffer init = new ListBuffer();
        for (StatementTree t : initializer) {
            init.append((Object)((JCTree.JCStatement)t));
        }
        ListBuffer step = new ListBuffer();
        for (ExpressionStatementTree t2 : update) {
            step.append((Object)((JCTree.JCExpressionStatement)t2));
        }
        return this.make.at(-2).ForLoop(init.toList(), (JCTree.JCExpression)condition, step.toList(), (JCTree.JCStatement)statement);
    }

    public IdentifierTree Identifier(CharSequence name) {
        return this.make.at(-2).Ident(this.names.fromString(name.toString()));
    }

    public IdentifierTree Identifier(Element element) {
        return this.make.at(-2).Ident((Symbol)element);
    }

    public IfTree If(ExpressionTree condition, StatementTree thenStatement, StatementTree elseStatement) {
        return this.make.at(-2).If((JCTree.JCExpression)condition, (JCTree.JCStatement)thenStatement, (JCTree.JCStatement)elseStatement);
    }

    public ImportTree Import(Tree qualid, boolean importStatic) {
        return this.make.at(-2).Import((JCTree)qualid, importStatic);
    }

    public InstanceOfTree InstanceOf(ExpressionTree expression, Tree type) {
        return this.make.at(-2).TypeTest((JCTree.JCExpression)expression, (JCTree)type);
    }

    public IntersectionTypeTree IntersectionType(java.util.List<? extends Tree> bounds) {
        ListBuffer jcbounds = new ListBuffer();
        for (Tree t : bounds) {
            jcbounds.append((Object)((JCTree.JCExpression)t));
        }
        return this.make.at(-2).TypeIntersection(jcbounds.toList());
    }

    public LabeledStatementTree LabeledStatement(CharSequence label, StatementTree statement) {
        return this.make.at(-2).Labelled(this.names.fromString(label.toString()), (JCTree.JCStatement)statement);
    }

    public LambdaExpressionTree LambdaExpression(java.util.List<? extends VariableTree> parameters, Tree body) {
        ListBuffer params = new ListBuffer();
        for (Tree t : parameters) {
            params.append((Object)((JCTree.JCVariableDecl)t));
        }
        return this.make.at(-2).Lambda(params.toList(), (JCTree)body);
    }

    public LiteralTree Literal(Object value) {
        try {
            if (value instanceof Boolean) {
                return this.make.at(-2).Literal(TypeTag.BOOLEAN, (Object)(value == Boolean.FALSE ? 0 : 1));
            }
            if (value instanceof Character) {
                return this.make.at(-2).Literal(TypeTag.CHAR, (Object)Integer.valueOf(((Character)value).charValue()));
            }
            if (value instanceof Byte) {
                return this.make.at(-2).Literal(TypeTag.INT, (Object)((Byte)value).intValue());
            }
            if (value instanceof Short) {
                return this.make.at(-2).Literal(TypeTag.INT, (Object)((Short)value).intValue());
            }
            if (value == null) {
                return this.make.at(-2).Literal(TypeTag.BOT, value);
            }
            return this.make.at(-2).Literal(value);
        }
        catch (AssertionError e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    public MemberSelectTree MemberSelect(ExpressionTree expression, CharSequence identifier) {
        return this.make.at(-2).Select((JCTree.JCExpression)expression, this.names.fromString(identifier.toString()));
    }

    public MemberSelectTree MemberSelect(ExpressionTree expression, Element element) {
        return (MemberSelectTree)this.make.at(-2).Select((JCTree.JCExpression)expression, (Symbol)element);
    }

    public MethodInvocationTree MethodInvocation(java.util.List<? extends ExpressionTree> typeArguments, ExpressionTree method, java.util.List<? extends ExpressionTree> arguments) {
        ListBuffer typeargs = new ListBuffer();
        for (ExpressionTree t : typeArguments) {
            typeargs.append((Object)((JCTree.JCExpression)t));
        }
        ListBuffer args = new ListBuffer();
        for (ExpressionTree t2 : arguments) {
            args.append((Object)((JCTree.JCExpression)t2));
        }
        return this.make.at(-2).Apply(typeargs.toList(), (JCTree.JCExpression)method, args.toList());
    }

    public MethodTree Method(ModifiersTree modifiers, CharSequence name, Tree returnType, java.util.List<? extends TypeParameterTree> typeParameters, java.util.List<? extends VariableTree> parameters, java.util.List<? extends ExpressionTree> throwsList, BlockTree body, ExpressionTree defaultValue) {
        return this.Method(modifiers, name, returnType, typeParameters, parameters, throwsList, body, defaultValue, false);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public MethodTree Method(ModifiersTree modifiers, CharSequence name, Tree returnType, java.util.List<? extends TypeParameterTree> typeParameters, java.util.List<? extends VariableTree> parameters, java.util.List<? extends ExpressionTree> throwsList, BlockTree body, ExpressionTree defaultValue, boolean isVarArgs) {
        ListBuffer typarams = new ListBuffer();
        for (TypeParameterTree t : typeParameters) {
            typarams.append((Object)((JCTree.JCTypeParameter)t));
        }
        ListBuffer params = new ListBuffer();
        if (!parameters.isEmpty() && isVarArgs) {
            JCTree.JCVariableDecl variableDecl = (JCTree.JCVariableDecl)parameters.get(parameters.size() - 1);
            if (variableDecl.getKind() == Tree.Kind.ARRAY_TYPE) throw new IllegalArgumentException("Last parameter isn't array. Can't set varargs flag.");
            variableDecl.mods = this.make.at(-2).Modifiers(variableDecl.mods.flags | 0x400000000L);
        } else if (parameters.isEmpty() && isVarArgs) {
            throw new IllegalArgumentException("Can't set varargs flag on empty parameter list.");
        }
        for (VariableTree t2 : parameters) {
            params.append((Object)((JCTree.JCVariableDecl)t2));
        }
        ListBuffer throwz = new ListBuffer();
        for (ExpressionTree t3 : throwsList) {
            throwz.append((Object)((JCTree.JCExpression)t3));
        }
        return this.make.at(-2).MethodDef((JCTree.JCModifiers)modifiers, this.names.fromString(name.toString()), (JCTree.JCExpression)returnType, typarams.toList(), params.toList(), throwz.toList(), (JCTree.JCBlock)body, (JCTree.JCExpression)defaultValue);
    }

    public MethodTree Method(ExecutableElement element, BlockTree body) {
        return this.make.at(-2).MethodDef((Symbol.MethodSymbol)element, (JCTree.JCBlock)body);
    }

    public MemberReferenceTree MemberReference(MemberReferenceTree.ReferenceMode refMode, CharSequence name, ExpressionTree expression, java.util.List<? extends ExpressionTree> typeArguments) {
        ListBuffer targs;
        if (typeArguments != null) {
            targs = new ListBuffer();
            for (ExpressionTree t : typeArguments) {
                targs.append((Object)((JCTree.JCExpression)t));
            }
        } else {
            targs = null;
        }
        return this.make.at(-2).Reference(refMode, this.names.fromString(name.toString()), (JCTree.JCExpression)expression, targs != null ? targs.toList() : null);
    }

    public ModifiersTree Modifiers(Set<Modifier> flagset, java.util.List<? extends AnnotationTree> annotations) {
        return this.Modifiers(TreeFactory.modifiersToFlags(flagset), annotations);
    }

    public ModifiersTree Modifiers(long mods, java.util.List<? extends AnnotationTree> annotations) {
        ListBuffer anns = new ListBuffer();
        for (AnnotationTree t : annotations) {
            anns.append((Object)((JCTree.JCAnnotation)t));
        }
        return this.make.at(-2).Modifiers(mods, anns.toList());
    }

    public static long modifiersToFlags(Set<Modifier> flagset) {
        long flags = 0;
        block14 : for (Modifier mod : flagset) {
            switch (mod) {
                case PUBLIC: {
                    flags |= 1;
                    continue block14;
                }
                case PROTECTED: {
                    flags |= 4;
                    continue block14;
                }
                case PRIVATE: {
                    flags |= 2;
                    continue block14;
                }
                case ABSTRACT: {
                    flags |= 1024;
                    continue block14;
                }
                case STATIC: {
                    flags |= 8;
                    continue block14;
                }
                case FINAL: {
                    flags |= 16;
                    continue block14;
                }
                case TRANSIENT: {
                    flags |= 128;
                    continue block14;
                }
                case VOLATILE: {
                    flags |= 64;
                    continue block14;
                }
                case SYNCHRONIZED: {
                    flags |= 32;
                    continue block14;
                }
                case NATIVE: {
                    flags |= 256;
                    continue block14;
                }
                case STRICTFP: {
                    flags |= 2048;
                    continue block14;
                }
                case DEFAULT: {
                    flags |= 0x80000000000L;
                    continue block14;
                }
            }
            throw new AssertionError((Object)("Unknown Modifier: " + (Object)((Object)mod)));
        }
        return flags;
    }

    public ModifiersTree Modifiers(Set<Modifier> flagset) {
        return this.Modifiers(flagset, (java.util.List<? extends AnnotationTree>)List.nil());
    }

    public ModifiersTree Modifiers(ModifiersTree oldMods, java.util.List<? extends AnnotationTree> annotations) {
        ListBuffer anns = new ListBuffer();
        for (AnnotationTree t : annotations) {
            anns.append((Object)((JCTree.JCAnnotation)t));
        }
        return this.make.at(-2).Modifiers(((JCTree.JCModifiers)oldMods).flags, anns.toList());
    }

    public NewArrayTree NewArray(Tree elemtype, java.util.List<? extends ExpressionTree> dimensions, java.util.List<? extends ExpressionTree> initializers) {
        ListBuffer dims = new ListBuffer();
        for (ExpressionTree t : dimensions) {
            dims.append((Object)((JCTree.JCExpression)t));
        }
        ListBuffer elems = null;
        if (initializers != null) {
            elems = new ListBuffer();
            for (ExpressionTree t2 : initializers) {
                elems.append((Object)((JCTree.JCExpression)t2));
            }
        }
        return this.make.at(-2).NewArray((JCTree.JCExpression)elemtype, dims.toList(), elems != null ? elems.toList() : null);
    }

    public NewClassTree NewClass(ExpressionTree enclosingExpression, java.util.List<? extends ExpressionTree> typeArguments, ExpressionTree identifier, java.util.List<? extends ExpressionTree> arguments, ClassTree classBody) {
        ListBuffer typeargs = new ListBuffer();
        for (ExpressionTree t : typeArguments) {
            typeargs.append((Object)((JCTree.JCExpression)t));
        }
        ListBuffer args = new ListBuffer();
        for (ExpressionTree t2 : arguments) {
            args.append((Object)((JCTree.JCExpression)t2));
        }
        return this.make.at(-2).NewClass((JCTree.JCExpression)enclosingExpression, typeargs.toList(), (JCTree.JCExpression)identifier, args.toList(), (JCTree.JCClassDecl)classBody);
    }

    public ParameterizedTypeTree ParameterizedType(Tree type, java.util.List<? extends Tree> typeArguments) {
        ListBuffer typeargs = new ListBuffer();
        for (Tree t : typeArguments) {
            typeargs.append((Object)((JCTree.JCExpression)t));
        }
        return this.make.at(-2).TypeApply((JCTree.JCExpression)type, typeargs.toList());
    }

    public ParenthesizedTree Parenthesized(ExpressionTree expression) {
        return this.make.at(-2).Parens((JCTree.JCExpression)expression);
    }

    public PrimitiveTypeTree PrimitiveType(TypeKind typekind) {
        TypeTag typetag;
        switch (typekind) {
            case BOOLEAN: {
                typetag = TypeTag.BOOLEAN;
                break;
            }
            case BYTE: {
                typetag = TypeTag.BYTE;
                break;
            }
            case SHORT: {
                typetag = TypeTag.SHORT;
                break;
            }
            case INT: {
                typetag = TypeTag.INT;
                break;
            }
            case LONG: {
                typetag = TypeTag.LONG;
                break;
            }
            case CHAR: {
                typetag = TypeTag.CHAR;
                break;
            }
            case FLOAT: {
                typetag = TypeTag.FLOAT;
                break;
            }
            case DOUBLE: {
                typetag = TypeTag.DOUBLE;
                break;
            }
            case VOID: {
                typetag = TypeTag.VOID;
                break;
            }
            default: {
                throw new AssertionError((Object)("unknown primitive type " + (Object)((Object)typekind)));
            }
        }
        return this.make.at(-2).TypeIdent(typetag);
    }

    public ExpressionTree QualIdentImpl(Element element) {
        return this.make.at(-2).QualIdent((Symbol)element);
    }

    public ExpressionTree QualIdent(Element element) {
        Symbol s = (Symbol)element;
        if (s.owner != null && (s.owner.kind == 16 || s.owner.name.isEmpty())) {
            JCTree.JCIdent result = this.make.at(-2).Ident(s);
            result.setType(s.type);
            return result;
        }
        QualIdentTree result = new QualIdentTree(this.make.at(-2).QualIdent(s.owner), s.name, s);
        result.setPos(this.make.pos).setType(s.type);
        return result;
    }

    public ExpressionTree QualIdent(String name) {
        int lastDot = name.lastIndexOf(46);
        if (lastDot == -1) {
            return this.Identifier(name);
        }
        QualIdentTree result = new QualIdentTree(((JCTree.JCExpression)this.QualIdent(name.substring(0, lastDot))).setPos(-2), (Name)this.elements.getName(name.substring(lastDot + 1)), name);
        result.setPos(-2);
        return result;
    }

    public com.sun.source.tree.ReturnTree Return(ExpressionTree expression) {
        return this.make.at(-2).Return((JCTree.JCExpression)expression);
    }

    public SwitchTree Switch(ExpressionTree expression, java.util.List<? extends CaseTree> caseList) {
        ListBuffer cases = new ListBuffer();
        for (CaseTree t : caseList) {
            cases.append((Object)((JCTree.JCCase)t));
        }
        return this.make.at(-2).Switch((JCTree.JCExpression)expression, cases.toList());
    }

    public SynchronizedTree Synchronized(ExpressionTree expression, BlockTree block) {
        return this.make.at(-2).Synchronized((JCTree.JCExpression)expression, (JCTree.JCBlock)block);
    }

    public ThrowTree Throw(ExpressionTree expression) {
        return this.make.at(-2).Throw((JCTree.JCExpression)expression);
    }

    public TryTree Try(java.util.List<? extends Tree> resources, BlockTree tryBlock, java.util.List<? extends CatchTree> catchList, BlockTree finallyBlock) {
        ListBuffer res = new ListBuffer();
        for (Tree t : resources) {
            res.append((Object)((JCTree)t));
        }
        ListBuffer catches = new ListBuffer();
        for (CatchTree t2 : catchList) {
            catches.append((Object)((JCTree.JCCatch)t2));
        }
        return this.make.at(-2).Try(res.toList(), (JCTree.JCBlock)tryBlock, catches.toList(), (JCTree.JCBlock)finallyBlock);
    }

    public List<JCTree.JCExpression> Types(java.util.List<Type> ts) {
        ListBuffer types = new ListBuffer();
        for (Type t : ts) {
            types.append((Object)((JCTree.JCExpression)this.Type((TypeMirror)t)));
        }
        return types.toList();
    }

    public ExpressionTree Type(TypeMirror type) {
        JCTree.JCLiteral tp;
        Type t = (Type)type;
        switch (type.getKind()) {
            case WILDCARD: {
                Type.WildcardType a = (Type.WildcardType)type;
                tp = this.make.at(-2).Wildcard(this.make.at(-2).TypeBoundKind(a.kind), (JCTree)((JCTree.JCExpression)this.Type((TypeMirror)a.type)));
                break;
            }
            case DECLARED: {
                JCTree.JCExpression clazz = (JCTree.JCExpression)this.QualIdent((Element)t.tsym);
                tp = t.getTypeArguments().isEmpty() ? clazz : this.make.at(-2).TypeApply(clazz, this.Types((java.util.List<Type>)t.getTypeArguments()));
                break;
            }
            case ARRAY: {
                tp = this.make.at(-2).TypeArray((JCTree.JCExpression)this.Type(((ArrayType)type).getComponentType()));
                break;
            }
            case NULL: {
                tp = this.make.at(-2).Literal(TypeTag.BOT, (Object)null);
                break;
            }
            case ERROR: {
                tp = this.make.at(-2).Ident(((Type.ErrorType)type).tsym.name);
                break;
            }
            default: {
                return this.make.at(-2).Type((Type)type);
            }
        }
        return tp;
    }

    public TypeCastTree TypeCast(Tree type, ExpressionTree expression) {
        return this.make.at(-2).TypeCast((JCTree)type, (JCTree.JCExpression)expression);
    }

    public TypeParameterTree TypeParameter(CharSequence name, java.util.List<? extends ExpressionTree> boundsList) {
        ListBuffer bounds = new ListBuffer();
        for (Tree t : boundsList) {
            bounds.append((Object)((JCTree.JCExpression)t));
        }
        return this.make.at(-2).TypeParameter(this.names.fromString(name.toString()), bounds.toList());
    }

    public UnaryTree Unary(Tree.Kind operator, ExpressionTree arg) {
        JCTree.Tag op;
        switch (operator) {
            case POSTFIX_INCREMENT: {
                op = JCTree.Tag.POSTINC;
                break;
            }
            case POSTFIX_DECREMENT: {
                op = JCTree.Tag.POSTDEC;
                break;
            }
            case PREFIX_INCREMENT: {
                op = JCTree.Tag.PREINC;
                break;
            }
            case PREFIX_DECREMENT: {
                op = JCTree.Tag.PREDEC;
                break;
            }
            case UNARY_PLUS: {
                op = JCTree.Tag.POS;
                break;
            }
            case UNARY_MINUS: {
                op = JCTree.Tag.NEG;
                break;
            }
            case BITWISE_COMPLEMENT: {
                op = JCTree.Tag.COMPL;
                break;
            }
            case LOGICAL_COMPLEMENT: {
                op = JCTree.Tag.NOT;
                break;
            }
            default: {
                throw new IllegalArgumentException("Illegal unary operator: " + (Object)operator);
            }
        }
        return this.make.at(-2).Unary(op, (JCTree.JCExpression)arg);
    }

    public VariableTree Variable(ModifiersTree modifiers, CharSequence name, Tree type, ExpressionTree initializer) {
        return this.make.at(-2).VarDef((JCTree.JCModifiers)modifiers, this.names.fromString(name.toString()), (JCTree.JCExpression)type, (JCTree.JCExpression)initializer);
    }

    public VariableTree Variable(VariableElement variable, ExpressionTree initializer) {
        return this.make.at(-2).VarDef((Symbol.VarSymbol)variable, (JCTree.JCExpression)initializer);
    }

    public WhileLoopTree WhileLoop(ExpressionTree condition, StatementTree statement) {
        return this.make.at(-2).WhileLoop((JCTree.JCExpression)condition, (JCTree.JCStatement)statement);
    }

    public WildcardTree Wildcard(Tree.Kind kind, Tree type) {
        BoundKind boundKind;
        switch (kind) {
            case UNBOUNDED_WILDCARD: {
                boundKind = BoundKind.UNBOUND;
                break;
            }
            case EXTENDS_WILDCARD: {
                boundKind = BoundKind.EXTENDS;
                break;
            }
            case SUPER_WILDCARD: {
                boundKind = BoundKind.SUPER;
                break;
            }
            default: {
                throw new IllegalArgumentException("Unknown wildcard bound " + (Object)kind);
            }
        }
        JCTree.TypeBoundKind tbk = this.make.at(-2).TypeBoundKind(boundKind);
        return this.make.at(-2).Wildcard(tbk, (JCTree)((JCTree.JCExpression)type));
    }

    public AnnotationTree addAnnotationAttrValue(AnnotationTree annotation, ExpressionTree attrValue) {
        return this.modifyAnnotationAttrValue(annotation, -1, attrValue, Operation.ADD);
    }

    public AnnotationTree insertAnnotationAttrValue(AnnotationTree annotation, int index, ExpressionTree attrValue) {
        return this.modifyAnnotationAttrValue(annotation, index, attrValue, Operation.ADD);
    }

    public AnnotationTree removeAnnotationAttrValue(AnnotationTree annotation, ExpressionTree attrValue) {
        return this.modifyAnnotationAttrValue(annotation, -1, attrValue, Operation.REMOVE);
    }

    public AnnotationTree removeAnnotationAttrValue(AnnotationTree annotation, int index) {
        return this.modifyAnnotationAttrValue(annotation, index, null, Operation.REMOVE);
    }

    private AnnotationTree modifyAnnotationAttrValue(AnnotationTree annotation, int index, ExpressionTree attrValue, Operation op) {
        AnnotationTree copy = annotation.getKind() == Tree.Kind.ANNOTATION ? this.Annotation(annotation.getAnnotationType(), this.c(annotation.getArguments(), index, (E)attrValue, op)) : this.TypeAnnotation(annotation.getAnnotationType(), this.c(annotation.getArguments(), index, (E)attrValue, op));
        return copy;
    }

    public BlockTree addBlockStatement(BlockTree block, StatementTree statement) {
        return this.modifyBlockStatement(block, -1, statement, Operation.ADD);
    }

    public BlockTree insertBlockStatement(BlockTree block, int index, StatementTree statement) {
        return this.modifyBlockStatement(block, index, statement, Operation.ADD);
    }

    public BlockTree removeBlockStatement(BlockTree block, StatementTree statement) {
        return this.modifyBlockStatement(block, -1, statement, Operation.REMOVE);
    }

    public BlockTree removeBlockStatement(BlockTree block, int index) {
        return this.modifyBlockStatement(block, index, null, Operation.REMOVE);
    }

    private BlockTree modifyBlockStatement(BlockTree block, int index, StatementTree statement, Operation op) {
        BlockTree copy = this.Block(this.c(block.getStatements(), index, (E)statement, op), block.isStatic());
        return copy;
    }

    public CaseTree addCaseStatement(CaseTree kejs, StatementTree statement) {
        return this.modifyCaseStatement(kejs, -1, statement, Operation.ADD);
    }

    public CaseTree insertCaseStatement(CaseTree kejs, int index, StatementTree statement) {
        return this.modifyCaseStatement(kejs, index, statement, Operation.ADD);
    }

    public CaseTree removeCaseStatement(CaseTree kejs, StatementTree statement) {
        return this.modifyCaseStatement(kejs, -1, statement, Operation.REMOVE);
    }

    public CaseTree removeCaseStatement(CaseTree kejs, int index) {
        return this.modifyCaseStatement(kejs, index, null, Operation.REMOVE);
    }

    private CaseTree modifyCaseStatement(CaseTree kejs, int index, StatementTree statement, Operation op) {
        CaseTree copy = this.Case(kejs.getExpression(), this.c(kejs.getStatements(), index, (E)statement, op));
        return copy;
    }

    public ClassTree addClassMember(ClassTree clazz, Tree member) {
        return this.modifyClassMember(clazz, -1, member, Operation.ADD);
    }

    public ClassTree insertClassMember(ClassTree clazz, int index, Tree member) {
        return this.modifyClassMember(clazz, index, member, Operation.ADD);
    }

    public ClassTree removeClassMember(ClassTree clazz, Tree member) {
        return this.modifyClassMember(clazz, -1, member, Operation.REMOVE);
    }

    public ClassTree removeClassMember(ClassTree clazz, int index) {
        return this.modifyClassMember(clazz, index, null, Operation.REMOVE);
    }

    private ClassTree modifyClassMember(ClassTree clazz, int index, Tree member, Operation op) {
        ClassTree copy = this.Class(clazz.getModifiers(), clazz.getSimpleName(), clazz.getTypeParameters(), clazz.getExtendsClause(), clazz.getImplementsClause(), this.c(clazz.getMembers(), index, (E)member, op));
        return copy;
    }

    public ClassTree addClassTypeParameter(ClassTree clazz, TypeParameterTree typeParameter) {
        return this.modifyClassTypeParameter(clazz, -1, typeParameter, Operation.ADD);
    }

    public ClassTree insertClassTypeParameter(ClassTree clazz, int index, TypeParameterTree typeParameter) {
        return this.modifyClassTypeParameter(clazz, index, typeParameter, Operation.ADD);
    }

    public ClassTree removeClassTypeParameter(ClassTree clazz, TypeParameterTree typeParameter) {
        return this.modifyClassTypeParameter(clazz, -1, typeParameter, Operation.REMOVE);
    }

    public ClassTree removeClassTypeParameter(ClassTree clazz, int index) {
        return this.modifyClassTypeParameter(clazz, index, null, Operation.REMOVE);
    }

    private ClassTree modifyClassTypeParameter(ClassTree clazz, int index, TypeParameterTree typeParameter, Operation op) {
        ClassTree copy = this.Class(clazz.getModifiers(), clazz.getSimpleName(), this.c(clazz.getTypeParameters(), index, (E)typeParameter, op), clazz.getExtendsClause(), clazz.getImplementsClause(), clazz.getMembers());
        return copy;
    }

    public ClassTree addClassImplementsClause(ClassTree clazz, Tree implementsClause) {
        return this.modifyClassImplementsClause(clazz, -1, implementsClause, Operation.ADD);
    }

    public ClassTree insertClassImplementsClause(ClassTree clazz, int index, Tree implementsClause) {
        return this.modifyClassImplementsClause(clazz, index, implementsClause, Operation.ADD);
    }

    public ClassTree removeClassImplementsClause(ClassTree clazz, Tree implementsClause) {
        return this.modifyClassImplementsClause(clazz, -1, implementsClause, Operation.REMOVE);
    }

    public ClassTree removeClassImplementsClause(ClassTree clazz, int index) {
        return this.modifyClassImplementsClause(clazz, index, null, Operation.REMOVE);
    }

    private ClassTree modifyClassImplementsClause(ClassTree clazz, int index, Tree implementsClause, Operation op) {
        ClassTree copy = this.Class(clazz.getModifiers(), clazz.getSimpleName(), clazz.getTypeParameters(), clazz.getExtendsClause(), this.c(clazz.getImplementsClause(), index, (E)implementsClause, op), clazz.getMembers());
        return copy;
    }

    public CompilationUnitTree addCompUnitTypeDecl(CompilationUnitTree compilationUnit, Tree typeDeclaration) {
        return this.modifyCompUnitTypeDecl(compilationUnit, -1, typeDeclaration, Operation.ADD);
    }

    public CompilationUnitTree insertCompUnitTypeDecl(CompilationUnitTree compilationUnit, int index, Tree typeDeclaration) {
        return this.modifyCompUnitTypeDecl(compilationUnit, index, typeDeclaration, Operation.ADD);
    }

    public CompilationUnitTree removeCompUnitTypeDecl(CompilationUnitTree compilationUnit, Tree typeDeclaration) {
        return this.modifyCompUnitTypeDecl(compilationUnit, -1, typeDeclaration, Operation.REMOVE);
    }

    public CompilationUnitTree removeCompUnitTypeDecl(CompilationUnitTree compilationUnit, int index) {
        return this.modifyCompUnitTypeDecl(compilationUnit, index, null, Operation.REMOVE);
    }

    private CompilationUnitTree modifyCompUnitTypeDecl(CompilationUnitTree compilationUnit, int index, Tree typeDeclaration, Operation op) {
        CompilationUnitTree copy = this.CompilationUnit(compilationUnit.getPackageAnnotations(), compilationUnit.getPackageName(), compilationUnit.getImports(), this.c(compilationUnit.getTypeDecls(), index, (E)typeDeclaration, op), compilationUnit.getSourceFile());
        return copy;
    }

    public CompilationUnitTree addCompUnitImport(CompilationUnitTree compilationUnit, ImportTree importt) {
        return this.modifyCompUnitImport(compilationUnit, -1, importt, Operation.ADD);
    }

    public CompilationUnitTree insertCompUnitImport(CompilationUnitTree compilationUnit, int index, ImportTree importt) {
        return this.modifyCompUnitImport(compilationUnit, index, importt, Operation.ADD);
    }

    public CompilationUnitTree removeCompUnitImport(CompilationUnitTree compilationUnit, ImportTree importt) {
        return this.modifyCompUnitImport(compilationUnit, -1, importt, Operation.REMOVE);
    }

    public CompilationUnitTree removeCompUnitImport(CompilationUnitTree compilationUnit, int index) {
        return this.modifyCompUnitImport(compilationUnit, index, null, Operation.REMOVE);
    }

    private CompilationUnitTree modifyCompUnitImport(CompilationUnitTree compilationUnit, int index, ImportTree importt, Operation op) {
        CompilationUnitTree copy = this.CompilationUnit(compilationUnit.getPackageAnnotations(), compilationUnit.getPackageName(), this.c(compilationUnit.getImports(), index, (E)importt, op), compilationUnit.getTypeDecls(), compilationUnit.getSourceFile());
        return copy;
    }

    public CompilationUnitTree addPackageAnnotation(CompilationUnitTree cut, AnnotationTree annotation) {
        return this.modifyPackageAnnotation(cut, -1, annotation, Operation.ADD);
    }

    public CompilationUnitTree insertPackageAnnotation(CompilationUnitTree cut, int index, AnnotationTree annotation) {
        return this.modifyPackageAnnotation(cut, index, annotation, Operation.ADD);
    }

    public CompilationUnitTree removePackageAnnotation(CompilationUnitTree cut, AnnotationTree annotation) {
        return this.modifyPackageAnnotation(cut, -1, annotation, Operation.REMOVE);
    }

    public CompilationUnitTree removePackageAnnotation(CompilationUnitTree cut, int index) {
        return this.modifyPackageAnnotation(cut, index, null, Operation.REMOVE);
    }

    private CompilationUnitTree modifyPackageAnnotation(CompilationUnitTree cut, int index, AnnotationTree annotation, Operation op) {
        CompilationUnitTree copy = this.CompilationUnit(this.c(cut.getPackageAnnotations(), index, (E)annotation, op), cut.getPackageName(), cut.getImports(), cut.getTypeDecls(), cut.getSourceFile());
        return copy;
    }

    public ForLoopTree addForLoopInitializer(ForLoopTree forLoop, StatementTree statement) {
        return this.modifyForLoopInitializer(forLoop, -1, statement, Operation.ADD);
    }

    public ForLoopTree insertForLoopInitializer(ForLoopTree forLoop, int index, StatementTree statement) {
        return this.modifyForLoopInitializer(forLoop, index, statement, Operation.ADD);
    }

    public ForLoopTree removeForLoopInitializer(ForLoopTree forLoop, StatementTree statement) {
        return this.modifyForLoopInitializer(forLoop, -1, statement, Operation.REMOVE);
    }

    public ForLoopTree removeForLoopInitializer(ForLoopTree forLoop, int index) {
        return this.modifyForLoopInitializer(forLoop, index, null, Operation.REMOVE);
    }

    private ForLoopTree modifyForLoopInitializer(ForLoopTree forLoop, int index, StatementTree statement, Operation op) {
        ForLoopTree copy = this.ForLoop(this.c(forLoop.getInitializer(), index, (E)statement, op), forLoop.getCondition(), forLoop.getUpdate(), forLoop.getStatement());
        return copy;
    }

    public ForLoopTree addForLoopUpdate(ForLoopTree forLoop, ExpressionStatementTree update) {
        return this.modifyForLoopUpdate(forLoop, -1, update, Operation.ADD);
    }

    public ForLoopTree insertForLoopUpdate(ForLoopTree forLoop, int index, ExpressionStatementTree update) {
        return this.modifyForLoopUpdate(forLoop, index, update, Operation.ADD);
    }

    public ForLoopTree removeForLoopUpdate(ForLoopTree forLoop, ExpressionStatementTree update) {
        return this.modifyForLoopUpdate(forLoop, -1, update, Operation.REMOVE);
    }

    public ForLoopTree removeForLoopUpdate(ForLoopTree forLoop, int index) {
        return this.modifyForLoopUpdate(forLoop, index, null, Operation.REMOVE);
    }

    private ForLoopTree modifyForLoopUpdate(ForLoopTree forLoop, int index, ExpressionStatementTree update, Operation op) {
        ForLoopTree copy = this.ForLoop(forLoop.getInitializer(), forLoop.getCondition(), this.c(forLoop.getUpdate(), index, (E)update, op), forLoop.getStatement());
        return copy;
    }

    public MethodInvocationTree addMethodInvocationArgument(MethodInvocationTree methodInvocation, ExpressionTree argument) {
        return this.modifyMethodInvocationArgument(methodInvocation, -1, argument, Operation.ADD);
    }

    public MethodInvocationTree insertMethodInvocationArgument(MethodInvocationTree methodInvocation, int index, ExpressionTree argument) {
        return this.modifyMethodInvocationArgument(methodInvocation, index, argument, Operation.ADD);
    }

    public MethodInvocationTree removeMethodInvocationArgument(MethodInvocationTree methodInvocation, ExpressionTree argument) {
        return this.modifyMethodInvocationArgument(methodInvocation, -1, argument, Operation.REMOVE);
    }

    public MethodInvocationTree removeMethodInvocationArgument(MethodInvocationTree methodInvocation, int index) {
        return this.modifyMethodInvocationArgument(methodInvocation, index, null, Operation.REMOVE);
    }

    private MethodInvocationTree modifyMethodInvocationArgument(MethodInvocationTree methodInvocation, int index, ExpressionTree argument, Operation op) {
        MethodInvocationTree copy = this.MethodInvocation(methodInvocation.getTypeArguments(), methodInvocation.getMethodSelect(), this.c(methodInvocation.getArguments(), index, (E)argument, op));
        return copy;
    }

    public MethodInvocationTree addMethodInvocationTypeArgument(MethodInvocationTree methodInvocation, ExpressionTree typeArgument) {
        return this.modifyMethodInvocationTypeArgument(methodInvocation, -1, typeArgument, Operation.ADD);
    }

    public MethodInvocationTree insertMethodInvocationTypeArgument(MethodInvocationTree methodInvocation, int index, ExpressionTree typeArgument) {
        return this.modifyMethodInvocationTypeArgument(methodInvocation, index, typeArgument, Operation.ADD);
    }

    public MethodInvocationTree removeMethodInvocationTypeArgument(MethodInvocationTree methodInvocation, ExpressionTree typeArgument) {
        return this.modifyMethodInvocationTypeArgument(methodInvocation, -1, typeArgument, Operation.REMOVE);
    }

    public MethodInvocationTree removeMethodInvocationTypeArgument(MethodInvocationTree methodInvocation, int index) {
        return this.modifyMethodInvocationArgument(methodInvocation, index, null, Operation.REMOVE);
    }

    private MethodInvocationTree modifyMethodInvocationTypeArgument(MethodInvocationTree methodInvocation, int index, ExpressionTree typeArgument, Operation op) {
        MethodInvocationTree copy = this.MethodInvocation(this.c(methodInvocation.getTypeArguments(), index, (E)typeArgument, op), methodInvocation.getMethodSelect(), methodInvocation.getArguments());
        return copy;
    }

    public MethodTree addMethodTypeParameter(MethodTree method, TypeParameterTree typeParameter) {
        return this.modifyMethodTypeParameter(method, -1, typeParameter, Operation.ADD);
    }

    public MethodTree insertMethodTypeParameter(MethodTree method, int index, TypeParameterTree typeParameter) {
        return this.modifyMethodTypeParameter(method, index, typeParameter, Operation.ADD);
    }

    public MethodTree removeMethodTypeParameter(MethodTree method, TypeParameterTree typeParameter) {
        return this.modifyMethodTypeParameter(method, -1, typeParameter, Operation.REMOVE);
    }

    public MethodTree removeMethodTypeParameter(MethodTree method, int index) {
        return this.modifyMethodTypeParameter(method, index, null, Operation.REMOVE);
    }

    private MethodTree modifyMethodTypeParameter(MethodTree method, int index, TypeParameterTree typeParameter, Operation op) {
        MethodTree copy = this.Method(method.getModifiers(), method.getName(), method.getReturnType(), this.c(method.getTypeParameters(), index, (E)typeParameter, op), method.getParameters(), method.getThrows(), method.getBody(), (ExpressionTree)method.getDefaultValue());
        return copy;
    }

    public MethodTree addMethodParameter(MethodTree method, VariableTree parameter) {
        return this.modifyMethodParameter(method, -1, parameter, Operation.ADD);
    }

    public MethodTree insertMethodParameter(MethodTree method, int index, VariableTree parameter) {
        return this.modifyMethodParameter(method, index, parameter, Operation.ADD);
    }

    public MethodTree removeMethodParameter(MethodTree method, VariableTree parameter) {
        return this.modifyMethodParameter(method, -1, parameter, Operation.REMOVE);
    }

    public MethodTree removeMethodParameter(MethodTree method, int index) {
        return this.modifyMethodParameter(method, index, null, Operation.REMOVE);
    }

    private MethodTree modifyMethodParameter(MethodTree method, int index, VariableTree parameter, Operation op) {
        MethodTree copy = this.Method(method.getModifiers(), method.getName(), method.getReturnType(), method.getTypeParameters(), this.c(method.getParameters(), index, (E)parameter, op), method.getThrows(), method.getBody(), (ExpressionTree)method.getDefaultValue());
        return copy;
    }

    public MethodTree addMethodThrows(MethodTree method, ExpressionTree throwz) {
        return this.modifyMethodThrows(method, -1, throwz, Operation.ADD);
    }

    public MethodTree insertMethodThrows(MethodTree method, int index, ExpressionTree throwz) {
        return this.modifyMethodThrows(method, index, throwz, Operation.ADD);
    }

    public MethodTree removeMethodThrows(MethodTree method, ExpressionTree throwz) {
        return this.modifyMethodThrows(method, -1, throwz, Operation.REMOVE);
    }

    public MethodTree removeMethodThrows(MethodTree method, int index) {
        return this.modifyMethodThrows(method, index, null, Operation.REMOVE);
    }

    private MethodTree modifyMethodThrows(MethodTree method, int index, ExpressionTree throwz, Operation op) {
        MethodTree copy = this.Method(method.getModifiers(), method.getName(), method.getReturnType(), method.getTypeParameters(), method.getParameters(), this.c(method.getThrows(), index, (E)throwz, op), method.getBody(), (ExpressionTree)method.getDefaultValue());
        return copy;
    }

    public ModifiersTree addModifiersAnnotation(ModifiersTree modifiers, AnnotationTree annotation) {
        return this.modifyModifiersAnnotation(modifiers, -1, annotation, Operation.ADD);
    }

    public ModifiersTree insertModifiersAnnotation(ModifiersTree modifiers, int index, AnnotationTree annotation) {
        return this.modifyModifiersAnnotation(modifiers, index, annotation, Operation.ADD);
    }

    public ModifiersTree removeModifiersAnnotation(ModifiersTree modifiers, AnnotationTree annotation) {
        return this.modifyModifiersAnnotation(modifiers, -1, annotation, Operation.REMOVE);
    }

    public ModifiersTree removeModifiersAnnotation(ModifiersTree modifiers, int index) {
        return this.modifyModifiersAnnotation(modifiers, index, null, Operation.REMOVE);
    }

    private ModifiersTree modifyModifiersAnnotation(ModifiersTree modifiers, int index, AnnotationTree annotation, Operation op) {
        ModifiersTree copy = this.Modifiers(((JCTree.JCModifiers)modifiers).flags, this.c(modifiers.getAnnotations(), index, (E)annotation, op));
        return copy;
    }

    public NewArrayTree addNewArrayDimension(NewArrayTree newArray, ExpressionTree dimension) {
        return this.modifyNewArrayDimension(newArray, -1, dimension, Operation.ADD);
    }

    public NewArrayTree insertNewArrayDimension(NewArrayTree newArray, int index, ExpressionTree dimension) {
        return this.modifyNewArrayDimension(newArray, index, dimension, Operation.ADD);
    }

    public NewArrayTree removeNewArrayDimension(NewArrayTree newArray, ExpressionTree dimension) {
        return this.modifyNewArrayDimension(newArray, -1, dimension, Operation.REMOVE);
    }

    public NewArrayTree removeNewArrayDimension(NewArrayTree newArray, int index) {
        return this.modifyNewArrayDimension(newArray, index, null, Operation.REMOVE);
    }

    private NewArrayTree modifyNewArrayDimension(NewArrayTree newArray, int index, ExpressionTree dimension, Operation op) {
        NewArrayTree copy = this.NewArray(newArray.getType(), this.c(newArray.getDimensions(), index, (E)dimension, op), newArray.getInitializers());
        return copy;
    }

    public NewArrayTree addNewArrayInitializer(NewArrayTree newArray, ExpressionTree initializer) {
        return this.modifyNewArrayInitializer(newArray, -1, initializer, Operation.ADD);
    }

    public NewArrayTree insertNewArrayInitializer(NewArrayTree newArray, int index, ExpressionTree initializer) {
        return this.modifyNewArrayInitializer(newArray, index, initializer, Operation.ADD);
    }

    public NewArrayTree removeNewArrayInitializer(NewArrayTree newArray, ExpressionTree initializer) {
        return this.modifyNewArrayInitializer(newArray, -1, initializer, Operation.REMOVE);
    }

    public NewArrayTree removeNewArrayInitializer(NewArrayTree newArray, int index) {
        return this.modifyNewArrayInitializer(newArray, index, null, Operation.REMOVE);
    }

    private NewArrayTree modifyNewArrayInitializer(NewArrayTree newArray, int index, ExpressionTree initializer, Operation op) {
        NewArrayTree copy = this.NewArray(newArray.getType(), newArray.getDimensions(), this.c(newArray.getInitializers(), index, (E)initializer, op));
        return copy;
    }

    public NewClassTree addNewClassArgument(NewClassTree newClass, ExpressionTree argument) {
        return this.modifyNewClassArgument(newClass, -1, argument, Operation.ADD);
    }

    public NewClassTree insertNewClassArgument(NewClassTree newClass, int index, ExpressionTree argument) {
        return this.modifyNewClassArgument(newClass, index, argument, Operation.ADD);
    }

    public NewClassTree removeNewClassArgument(NewClassTree newClass, ExpressionTree argument) {
        return this.modifyNewClassArgument(newClass, -1, argument, Operation.REMOVE);
    }

    public NewClassTree removeNewClassArgument(NewClassTree newClass, int index) {
        return this.modifyNewClassArgument(newClass, index, null, Operation.REMOVE);
    }

    private NewClassTree modifyNewClassArgument(NewClassTree newClass, int index, ExpressionTree argument, Operation op) {
        NewClassTree copy = this.NewClass(newClass.getEnclosingExpression(), newClass.getTypeArguments(), newClass.getIdentifier(), this.c(newClass.getArguments(), index, (E)argument, op), newClass.getClassBody());
        return copy;
    }

    public NewClassTree addNewClassTypeArgument(NewClassTree newClass, ExpressionTree typeArgument) {
        return this.modifyNewClassTypeArgument(newClass, -1, typeArgument, Operation.ADD);
    }

    public NewClassTree insertNewClassTypeArgument(NewClassTree newClass, int index, ExpressionTree typeArgument) {
        return this.modifyNewClassTypeArgument(newClass, index, typeArgument, Operation.ADD);
    }

    public NewClassTree removeNewClassTypeArgument(NewClassTree newClass, ExpressionTree typeArgument) {
        return this.modifyNewClassTypeArgument(newClass, -1, typeArgument, Operation.REMOVE);
    }

    public NewClassTree removeNewClassTypeArgument(NewClassTree newClass, int index) {
        return this.modifyNewClassTypeArgument(newClass, index, null, Operation.REMOVE);
    }

    private NewClassTree modifyNewClassTypeArgument(NewClassTree newClass, int index, ExpressionTree typeArgument, Operation op) {
        NewClassTree copy = this.NewClass(newClass.getEnclosingExpression(), this.c(newClass.getTypeArguments(), index, (E)typeArgument, op), newClass.getIdentifier(), newClass.getArguments(), newClass.getClassBody());
        return copy;
    }

    public ParameterizedTypeTree addParameterizedTypeTypeArgument(ParameterizedTypeTree parameterizedType, ExpressionTree argument) {
        return this.modifyParameterizedTypeTypeArgument(parameterizedType, -1, argument, Operation.ADD);
    }

    public ParameterizedTypeTree insertParameterizedTypeTypeArgument(ParameterizedTypeTree parameterizedType, int index, ExpressionTree argument) {
        return this.modifyParameterizedTypeTypeArgument(parameterizedType, index, argument, Operation.ADD);
    }

    public ParameterizedTypeTree removeParameterizedTypeTypeArgument(ParameterizedTypeTree parameterizedType, ExpressionTree argument) {
        return this.modifyParameterizedTypeTypeArgument(parameterizedType, -1, argument, Operation.REMOVE);
    }

    public ParameterizedTypeTree removeParameterizedTypeTypeArgument(ParameterizedTypeTree parameterizedType, int index) {
        return this.modifyParameterizedTypeTypeArgument(parameterizedType, index, null, Operation.REMOVE);
    }

    private ParameterizedTypeTree modifyParameterizedTypeTypeArgument(ParameterizedTypeTree parameterizedType, int index, ExpressionTree argument, Operation op) {
        ParameterizedTypeTree copy = this.ParameterizedType(parameterizedType.getType(), this.c(parameterizedType.getTypeArguments(), index, (E)argument, op));
        return copy;
    }

    public SwitchTree addSwitchCase(SwitchTree swic, CaseTree kejs) {
        return this.modifySwitchCase(swic, -1, kejs, Operation.ADD);
    }

    public SwitchTree insertSwitchCase(SwitchTree swic, int index, CaseTree kejs) {
        return this.modifySwitchCase(swic, index, kejs, Operation.ADD);
    }

    public SwitchTree removeSwitchCase(SwitchTree swic, CaseTree kejs) {
        return this.modifySwitchCase(swic, -1, kejs, Operation.REMOVE);
    }

    public SwitchTree removeSwitchCase(SwitchTree swic, int index) {
        return this.modifySwitchCase(swic, index, null, Operation.REMOVE);
    }

    private SwitchTree modifySwitchCase(SwitchTree swic, int index, CaseTree kejs, Operation op) {
        SwitchTree copy = this.Switch(swic.getExpression(), this.c(swic.getCases(), index, (E)kejs, op));
        return copy;
    }

    public TryTree addTryCatch(TryTree traj, CatchTree kec) {
        return this.modifyTryCatch(traj, -1, kec, Operation.ADD);
    }

    public TryTree insertTryCatch(TryTree traj, int index, CatchTree kec) {
        return this.modifyTryCatch(traj, index, kec, Operation.ADD);
    }

    public TryTree removeTryCatch(TryTree traj, CatchTree kec) {
        return this.modifyTryCatch(traj, -1, kec, Operation.REMOVE);
    }

    public TryTree removeTryCatch(TryTree traj, int index) {
        return this.modifyTryCatch(traj, index, null, Operation.REMOVE);
    }

    private TryTree modifyTryCatch(TryTree traj, int index, CatchTree kec, Operation op) {
        TryTree copy = this.Try(traj.getResources(), traj.getBlock(), this.c(traj.getCatches(), index, (E)kec, op), traj.getFinallyBlock());
        return copy;
    }

    public TypeParameterTree addTypeParameterBound(TypeParameterTree typeParameter, ExpressionTree bound) {
        return this.modifyTypeParameterBound(typeParameter, -1, bound, Operation.ADD);
    }

    public TypeParameterTree insertTypeParameterBound(TypeParameterTree typeParameter, int index, ExpressionTree bound) {
        return this.modifyTypeParameterBound(typeParameter, index, bound, Operation.ADD);
    }

    public TypeParameterTree removeTypeParameterBound(TypeParameterTree typeParameter, ExpressionTree bound) {
        return this.modifyTypeParameterBound(typeParameter, -1, bound, Operation.REMOVE);
    }

    public TypeParameterTree removeTypeParameterBound(TypeParameterTree typeParameter, int index) {
        return this.modifyTypeParameterBound(typeParameter, index, null, Operation.REMOVE);
    }

    private TypeParameterTree modifyTypeParameterBound(TypeParameterTree typeParameter, int index, ExpressionTree bound, Operation op) {
        TypeParameterTree copy = this.TypeParameter(typeParameter.getName(), this.c(typeParameter.getBounds(), index, (E)bound, op));
        return copy;
    }

    public LambdaExpressionTree addLambdaParameter(LambdaExpressionTree method, VariableTree parameter) {
        return this.modifyLambdaParameter(method, -1, parameter, Operation.ADD);
    }

    public LambdaExpressionTree insertLambdaParameter(LambdaExpressionTree method, int index, VariableTree parameter) {
        return this.modifyLambdaParameter(method, index, parameter, Operation.ADD);
    }

    public LambdaExpressionTree removeLambdaParameter(LambdaExpressionTree method, VariableTree parameter) {
        return this.modifyLambdaParameter(method, -1, parameter, Operation.REMOVE);
    }

    public LambdaExpressionTree removeLambdaParameter(LambdaExpressionTree method, int index) {
        return this.modifyLambdaParameter(method, index, null, Operation.REMOVE);
    }

    private LambdaExpressionTree modifyLambdaParameter(LambdaExpressionTree method, int index, VariableTree parameter, Operation op) {
        LambdaExpressionTree copy = this.LambdaExpression(this.c(method.getParameters(), index, (E)parameter, op), method.getBody());
        ((JCTree.JCLambda)copy).paramKind = ((JCTree.JCLambda)method).paramKind;
        return copy;
    }

    public LambdaExpressionTree setLambdaBody(LambdaExpressionTree method, Tree newBody) {
        return this.LambdaExpression(method.getParameters(), newBody);
    }

    private <E extends Tree> java.util.List<E> c(java.util.List<? extends E> originalList, int index, E item, Operation operation) {
        ArrayList<E> copy = new ArrayList<E>(originalList);
        switch (operation) {
            case ADD: {
                if (index > -1) {
                    copy.add(index, item);
                    break;
                }
                copy.add(item);
                break;
            }
            case REMOVE: {
                if (index > -1) {
                    copy.remove(index);
                    break;
                }
                copy.remove(item);
            }
        }
        return copy;
    }

    private java.util.List<TypeMirror> typesFromTrees(java.util.List<? extends Tree> trees) {
        ArrayList<TypeMirror> types = new ArrayList<TypeMirror>();
        for (Tree t : trees) {
            types.add(this.model.getType(t));
        }
        return types;
    }

    private ClassTree Class(long modifiers, List<JCTree.JCAnnotation> annotations, CharSequence simpleName, java.util.List<? extends TypeParameterTree> typeParameters, Tree extendsClause, java.util.List<? extends Tree> implementsClauses, java.util.List<? extends Tree> memberDecls) {
        ListBuffer typarams = new ListBuffer();
        for (TypeParameterTree t : typeParameters) {
            typarams.append((Object)((JCTree.JCTypeParameter)t));
        }
        ListBuffer impls = new ListBuffer();
        for (Tree t2 : implementsClauses) {
            impls.append((Object)((JCTree.JCExpression)t2));
        }
        ListBuffer defs = new ListBuffer();
        for (Tree t3 : memberDecls) {
            defs.append((Object)((JCTree)t3));
        }
        return this.make.at(-2).ClassDef(this.make.at(-2).Modifiers(modifiers, annotations), this.names.fromString(simpleName.toString()), typarams.toList(), (JCTree.JCExpression)extendsClause, impls.toList(), defs.toList());
    }

    private long getBitFlags(Set<Modifier> modifiers) {
        int flags = 0;
        for (Modifier modifier : modifiers) {
            switch (modifier) {
                case PUBLIC: {
                    flags |= true;
                    break;
                }
                case PROTECTED: {
                    flags |= 4;
                    break;
                }
                case PRIVATE: {
                    flags |= 2;
                    break;
                }
                case ABSTRACT: {
                    flags |= 1024;
                    break;
                }
                case STATIC: {
                    flags |= 8;
                    break;
                }
                case FINAL: {
                    flags |= 16;
                    break;
                }
                case TRANSIENT: {
                    flags |= 128;
                    break;
                }
                case VOLATILE: {
                    flags |= 64;
                    break;
                }
                case SYNCHRONIZED: {
                    flags |= 32;
                    break;
                }
                case NATIVE: {
                    flags |= 256;
                    break;
                }
                case STRICTFP: {
                    flags |= 2048;
                    break;
                }
            }
        }
        return flags;
    }

    public AttributeTree Attribute(CharSequence name, AttributeTree.ValueKind vkind, java.util.List<? extends DocTree> value) {
        ListBuffer lb = null;
        if (value != null) {
            lb = new ListBuffer();
            for (DocTree t : value) {
                lb.append((Object)((DCTree)t));
            }
        }
        return this.docMake.at(-2).Attribute(this.names.fromString(name.toString()), vkind, lb != null ? lb.toList() : null);
    }

    public AuthorTree Author(java.util.List<? extends DocTree> name) {
        ListBuffer lb = new ListBuffer();
        for (DocTree t : name) {
            lb.append((Object)((DCTree)t));
        }
        return this.docMake.at(-2).Author(lb.toList());
    }

    public DeprecatedTree Deprecated(java.util.List<? extends DocTree> text) {
        ListBuffer txt = new ListBuffer();
        for (DocTree t : text) {
            txt.append((Object)((DCTree)t));
        }
        return this.docMake.at(-2).Deprecated(txt.toList());
    }

    public DocCommentTree DocComment(java.util.List<? extends DocTree> firstSentence, java.util.List<? extends DocTree> body, java.util.List<? extends DocTree> tags) {
        ListBuffer fs = new ListBuffer();
        for (DocTree t : firstSentence) {
            fs.append((Object)((DCTree)t));
        }
        ListBuffer bd = new ListBuffer();
        for (DocTree t2 : body) {
            bd.append((Object)((DCTree)t2));
        }
        ListBuffer tg = new ListBuffer();
        for (DocTree t3 : tags) {
            tg.append((Object)((DCTree)t3));
        }
        return this.docMake.at(-2).DocComment(null, fs.toList(), bd.toList(), tg.toList());
    }

    public /* varargs */ com.sun.source.doctree.ErroneousTree Erroneous(String text, DiagnosticSource diagSource, String code, Object ... args) {
        String msg = "Erroneous tree implemented: " + text + " " + code;
        throw new AssertionError((Object)msg);
    }

    public ParamTree Param(boolean isTypeParameter, com.sun.source.doctree.IdentifierTree name, java.util.List<? extends DocTree> description) {
        ListBuffer desc = new ListBuffer();
        for (DocTree t : description) {
            desc.append((Object)((DCTree)t));
        }
        return this.docMake.at(-2).Param(isTypeParameter, (DCTree.DCIdentifier)name, desc.toList());
    }

    public LinkTree Link(ReferenceTree ref, java.util.List<? extends DocTree> label) {
        ListBuffer lbl = new ListBuffer();
        for (DocTree t : label) {
            lbl.append((Object)((DCTree)t));
        }
        return this.docMake.at(-2).Link((DCTree.DCReference)ref, lbl.toList());
    }

    public com.sun.source.doctree.LiteralTree Literal(TextTree text) {
        return this.docMake.at(-2).Literal((DCTree.DCText)text);
    }

    public ReturnTree Return(java.util.List<? extends DocTree> description) {
        ListBuffer desc = new ListBuffer();
        for (DocTree t : description) {
            desc.append((Object)((DCTree)t));
        }
        return this.docMake.at(-2).Return(desc.toList());
    }

    public SeeTree See(java.util.List<? extends DocTree> reference) {
        ListBuffer ref = new ListBuffer();
        for (DocTree t : reference) {
            ref.append((Object)((DCTree)t));
        }
        return this.docMake.at(-2).See(ref.toList());
    }

    public SerialTree Serial(java.util.List<? extends DocTree> description) {
        ListBuffer desc = new ListBuffer();
        for (DocTree t : description) {
            desc.append((Object)((DCTree)t));
        }
        return this.docMake.at(-2).Serial(desc.toList());
    }

    public SerialDataTree SerialData(java.util.List<? extends DocTree> description) {
        ListBuffer desc = new ListBuffer();
        for (DocTree t : description) {
            desc.append((Object)((DCTree)t));
        }
        return this.docMake.at(-2).SerialData(desc.toList());
    }

    public SerialFieldTree SerialField(com.sun.source.doctree.IdentifierTree name, ReferenceTree type, java.util.List<? extends DocTree> description) {
        ListBuffer desc = new ListBuffer();
        for (DocTree t : description) {
            desc.append((Object)((DCTree)t));
        }
        return this.docMake.at(-2).SerialField((DCTree.DCIdentifier)name, (DCTree.DCReference)type, desc.toList());
    }

    public SinceTree Since(java.util.List<? extends DocTree> text) {
        ListBuffer txt = new ListBuffer();
        for (DocTree t : text) {
            txt.append((Object)((DCTree)t));
        }
        return this.docMake.at(-2).Since(txt.toList());
    }

    public StartElementTree StartElement(CharSequence name, java.util.List<? extends DocTree> attrs, boolean selfClosing) {
        ListBuffer atr = new ListBuffer();
        for (DocTree t : attrs) {
            atr.append((Object)((DCTree)t));
        }
        return this.docMake.at(-2).StartElement(this.names.fromString(name.toString()), atr.toList(), selfClosing);
    }

    public TextTree Text(String text) {
        return this.docMake.at(-2).Text(text);
    }

    public ThrowsTree Throws(ReferenceTree name, java.util.List<? extends DocTree> description) {
        ListBuffer desc = new ListBuffer();
        for (DocTree t : description) {
            desc.append((Object)((DCTree)t));
        }
        return this.docMake.at(-2).Throws((DCTree.DCReference)name, desc.toList());
    }

    public UnknownBlockTagTree UnknownBlockTag(CharSequence name, java.util.List<? extends DocTree> content) {
        ListBuffer cont = new ListBuffer();
        for (DocTree t : content) {
            cont.append((Object)((DCTree)t));
        }
        return this.docMake.at(-2).UnknownBlockTag(this.names.fromString(name.toString()), cont.toList());
    }

    public UnknownInlineTagTree UnknownInlineTag(CharSequence name, java.util.List<? extends DocTree> content) {
        ListBuffer cont = new ListBuffer();
        for (DocTree t : content) {
            cont.append((Object)((DCTree)t));
        }
        return this.docMake.at(-2).UnknownInlineTag(this.names.fromString(name.toString()), cont.toList());
    }

    public ValueTree Value(ReferenceTree ref) {
        return this.docMake.at(-2).Value((DCTree.DCReference)ref);
    }

    public VersionTree Version(java.util.List<? extends DocTree> text) {
        ListBuffer txt = new ListBuffer();
        for (DocTree t : text) {
            txt.append((Object)((DCTree)t));
        }
        return this.docMake.at(-2).Version(txt.toList());
    }

    public com.sun.source.doctree.LiteralTree Code(TextTree text) {
        return this.docMake.at(-2).Code((DCTree.DCText)text);
    }

    public CommentTree Comment(String text) {
        return this.docMake.at(-2).Comment(text);
    }

    public DocRootTree DocRoot() {
        return this.docMake.at(-2).DocRoot();
    }

    public EndElementTree EndElement(CharSequence name) {
        return this.docMake.at(-2).EndElement(this.names.fromString(name.toString()));
    }

    public EntityTree Entity(CharSequence name) {
        return this.docMake.at(-2).Entity(this.names.fromString(name.toString()));
    }

    public ThrowsTree Exception(ReferenceTree name, java.util.List<? extends DocTree> description) {
        ListBuffer desc = new ListBuffer();
        for (DocTree t : description) {
            desc.append((Object)((DCTree)t));
        }
        return this.docMake.at(-2).Exception((DCTree.DCReference)name, desc.toList());
    }

    public com.sun.source.doctree.IdentifierTree DocIdentifier(CharSequence name) {
        return this.docMake.at(-2).Identifier(this.names.fromString(name.toString()));
    }

    public InheritDocTree InheritDoc() {
        return this.docMake.at(-2).InheritDoc();
    }

    public LinkTree LinkPlain(ReferenceTree ref, java.util.List<? extends DocTree> label) {
        ListBuffer lbl = new ListBuffer();
        for (DocTree t : label) {
            lbl.append((Object)((DCTree)t));
        }
        return this.docMake.at(-2).LinkPlain((DCTree.DCReference)ref, lbl.toList());
    }

    public ReferenceTree Reference(ExpressionTree qualExpr, CharSequence member, java.util.List<? extends Tree> paramTypes) {
        List paramTypesList = null;
        if (paramTypes != null) {
            ListBuffer lbl = new ListBuffer();
            for (Tree t : paramTypes) {
                lbl.append((Object)((JCTree)t));
            }
            paramTypesList = lbl.toList();
        }
        return this.docMake.at(-2).Reference("", (JCTree)((JCTree.JCExpression)qualExpr), member != null ? this.names.fromString(member.toString()) : null, paramTypesList);
    }

    private static enum Operation {
        ADD,
        REMOVE;
        

        private Operation() {
        }
    }

}

