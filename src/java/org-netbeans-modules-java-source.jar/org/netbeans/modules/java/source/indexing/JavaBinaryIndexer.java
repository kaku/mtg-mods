/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.model.FilteredMemberList
 *  com.sun.tools.javac.model.JavacElements
 *  com.sun.tools.javac.util.Context
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 *  org.netbeans.modules.parsing.impl.indexing.friendapi.IndexingController
 *  org.netbeans.modules.parsing.spi.indexing.BinaryIndexer
 *  org.netbeans.modules.parsing.spi.indexing.BinaryIndexerFactory
 *  org.netbeans.modules.parsing.spi.indexing.Context
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.java.source.indexing;

import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.model.FilteredMemberList;
import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.util.Context;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.ElementFilter;
import javax.swing.JComponent;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticListener;
import javax.tools.JavaFileObject;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.source.JBrowseModule;
import org.netbeans.modules.java.source.TreeLoader;
import org.netbeans.modules.java.source.indexing.JavaCustomIndexer;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.indexing.TransactionContext;
import org.netbeans.modules.java.source.parsing.FileManagerTransaction;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.JavacParser;
import org.netbeans.modules.java.source.usages.BinaryAnalyser;
import org.netbeans.modules.java.source.usages.ClassIndexEventsTransaction;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;
import org.netbeans.modules.java.source.usages.ClassIndexManager;
import org.netbeans.modules.java.source.usages.ClasspathInfoAccessor;
import org.netbeans.modules.parsing.impl.indexing.friendapi.IndexingController;
import org.netbeans.modules.parsing.spi.indexing.BinaryIndexer;
import org.netbeans.modules.parsing.spi.indexing.BinaryIndexerFactory;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;

public class JavaBinaryIndexer
extends BinaryIndexer {
    static final Logger LOG = Logger.getLogger(JavaBinaryIndexer.class.getName());
    private static final int CLEAN_ALL_LIMIT = 1000;

    protected void index(org.netbeans.modules.parsing.spi.indexing.Context context) {
        LOG.log(Level.FINE, "index({0})", context.getRootURI());
        try {
            BinaryAnalyser ba;
            ClassIndexManager cim = ClassIndexManager.getDefault();
            ClassIndexImpl uq = cim.createUsagesQuery(context.getRootURI(), false);
            if (uq == null) {
                return;
            }
            if (context.isAllFilesIndexing() && (ba = uq.getBinaryAnalyser()) != null) {
                BinaryAnalyser.Changes changes = ba.analyse(context);
                if (changes.done) {
                    Map binDeps = IndexingController.getDefault().getBinaryRootDependencies();
                    Map srcDeps = IndexingController.getDefault().getRootDependencies();
                    Map peers = IndexingController.getDefault().getRootPeers();
                    ArrayList<ElementHandle<TypeElement>> changed = new ArrayList<ElementHandle<TypeElement>>(changes.changed.size() + changes.removed.size());
                    changed.addAll(changes.changed);
                    changed.addAll(changes.removed);
                    if (!(changes.changed.isEmpty() && changes.added.isEmpty() && changes.removed.isEmpty())) {
                        JavaBinaryIndexer.deleteSigFiles(context.getRootURI(), changed);
                        if (changes.preBuildArgs) {
                            JavaBinaryIndexer.preBuildArgs(context.getRootURI(), JComponent.class.getName());
                        }
                    }
                    Map<URL, Set<URL>> toRebuild = JavaCustomIndexer.findDependent(context.getRootURI(), srcDeps, binDeps, peers, changed, !changes.added.isEmpty(), false);
                    for (Map.Entry<URL, Set<URL>> entry : toRebuild.entrySet()) {
                        context.addSupplementaryFiles(entry.getKey(), (Collection)entry.getValue());
                    }
                }
            }
        }
        catch (IllegalArgumentException iae) {
            Exceptions.printStackTrace((Throwable)iae);
        }
        catch (IOException ioe) {
            Exceptions.printStackTrace((Throwable)ioe);
        }
    }

    private static void deleteSigFiles(URL root, List<? extends ElementHandle<TypeElement>> toRemove) throws IOException {
        File cacheFolder = JavaIndex.getClassFolder(root);
        if (cacheFolder.exists()) {
            if (toRemove.size() > 1000) {
                FileObjects.deleteRecursively(cacheFolder);
            } else {
                for (ElementHandle<TypeElement> eh : toRemove) {
                    StringBuilder sb = new StringBuilder(FileObjects.convertPackage2Folder(eh.getBinaryName(), File.separatorChar));
                    sb.append('.');
                    sb.append("sig");
                    File f = new File(cacheFolder, sb.toString());
                    f.delete();
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void preBuildArgs(@NonNull FileObject root, @NonNull FileObject file) throws IOException {
        String relativePath = FileObjects.convertFolder2Package(FileObjects.stripExtension(FileUtil.getRelativePath((FileObject)root, (FileObject)file)));
        TransactionContext txCtx = TransactionContext.beginTrans().register(FileManagerTransaction.class, FileManagerTransaction.writeThrough());
        try {
            JavaBinaryIndexer.preBuildArgs(root.getURL(), relativePath);
        }
        finally {
            txCtx.commit();
        }
    }

    private static void preBuildArgs(@NonNull URL archiveUrl, @NonNull String fqn) {
        class DevNullDiagnosticListener
        implements DiagnosticListener<JavaFileObject> {
            DevNullDiagnosticListener() {
            }

            @Override
            public void report(Diagnostic<? extends JavaFileObject> diagnostic) {
                if (JavaBinaryIndexer.LOG.isLoggable(Level.FINE)) {
                    JavaBinaryIndexer.LOG.log(Level.FINE, "Diagnostic reported during prebuilding args: {0}", diagnostic.toString());
                }
            }
        }
        ClasspathInfo cpInfo = ClasspathInfoAccessor.getINSTANCE().create(ClassPathSupport.createClassPath((URL[])new URL[]{archiveUrl}), ClassPathSupport.createClassPath((URL[])new URL[0]), ClassPathSupport.createClassPath((URL[])new URL[0]), null, true, true, false, false);
        JavacTaskImpl jt = JavacParser.createJavacTask(cpInfo, new DevNullDiagnosticListener(), null, null, null, null, null, null);
        TreeLoader.preRegister(jt.getContext(), cpInfo, true);
        try {
            jt.parse(new JavaFileObject[0]);
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        Symbol.ClassSymbol jc = jt.getElements().getTypeElementByBinaryName((CharSequence)fqn);
        if (jc != null) {
            List<ExecutableElement> methods = ElementFilter.methodsIn(jt.getElements().getAllMembers((TypeElement)jc));
            for (ExecutableElement method : methods) {
                List<? extends VariableElement> params = method.getParameters();
                if (params.isEmpty()) continue;
                params.get(0).getSimpleName();
            }
        }
    }

    public static class Factory
    extends BinaryIndexerFactory {
        public BinaryIndexer createIndexer() {
            return new JavaBinaryIndexer();
        }

        public String getIndexerName() {
            return "java";
        }

        public int getIndexVersion() {
            return 14;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void rootsRemoved(Iterable<? extends URL> removedRoots) {
            assert (removedRoots != null);
            TransactionContext txCtx = TransactionContext.beginTrans().register(ClassIndexEventsTransaction.class, ClassIndexEventsTransaction.create(false));
            try {
                ClassIndexManager cim = ClassIndexManager.getDefault();
                for (URL removedRoot : removedRoots) {
                    cim.removeRoot(removedRoot);
                }
            }
            catch (IOException e) {
                Exceptions.printStackTrace((Throwable)e);
            }
            finally {
                try {
                    if (JBrowseModule.isClosed()) {
                        txCtx.rollBack();
                    } else {
                        txCtx.commit();
                    }
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }

        public boolean scanStarted(org.netbeans.modules.parsing.spi.indexing.Context context) {
            try {
                TransactionContext.beginStandardTransaction(context.getRootURI(), false, context.isAllFilesIndexing(), context.checkForEditorModifications());
                ClassIndexImpl uq = ClassIndexManager.getDefault().createUsagesQuery(context.getRootURI(), false);
                if (uq == null) {
                    return true;
                }
                if (uq.getState() != ClassIndexImpl.State.NEW) {
                    return true;
                }
                return uq.isValid();
            }
            catch (IOException ioe) {
                JavaIndex.LOG.log(Level.WARNING, "Exception while checking cache validity for root: " + context.getRootURI(), ioe);
                return false;
            }
        }

        public void scanFinished(org.netbeans.modules.parsing.spi.indexing.Context context) {
            TransactionContext txCtx = TransactionContext.get();
            assert (txCtx != null);
            try {
                if (context.isCancelled()) {
                    txCtx.rollBack();
                } else {
                    txCtx.commit();
                }
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
    }

}

