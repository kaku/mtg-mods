/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.java.preprocessorbridge.spi.VirtualSourceProvider
 *  org.netbeans.modules.java.preprocessorbridge.spi.VirtualSourceProvider$Result
 *  org.netbeans.modules.parsing.spi.indexing.Indexable
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Pair
 *  org.openide.util.Parameters
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.java.source.usages;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.tools.JavaFileObject;
import org.netbeans.modules.java.preprocessorbridge.spi.VirtualSourceProvider;
import org.netbeans.modules.java.source.indexing.JavaCustomIndexer;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.ForwardingPrefetchableJavaFileObject;
import org.netbeans.modules.java.source.parsing.PrefetchableJavaFileObject;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Pair;
import org.openide.util.Parameters;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;

public final class VirtualSourceProviderQuery {
    private static final Lookup.Result<VirtualSourceProvider> result = Lookup.getDefault().lookupResult(VirtualSourceProvider.class);
    private static Map<String, VirtualSourceProvider> ext2prov;
    private static final LookupListener l;

    private VirtualSourceProviderQuery() {
    }

    public static boolean hasVirtualSource(File file) {
        Parameters.notNull((CharSequence)"file", (Object)file);
        String ext = FileObjects.getExtension(file.getName());
        return VirtualSourceProviderQuery.getExt2ProvMap().keySet().contains(ext);
    }

    public static boolean hasVirtualSource(FileObject file) {
        Parameters.notNull((CharSequence)"file", (Object)file);
        String ext = file.getExt();
        return VirtualSourceProviderQuery.getExt2ProvMap().keySet().contains(ext);
    }

    public static boolean hasVirtualSource(String extension) {
        Parameters.notNull((CharSequence)"extension", (Object)extension);
        return VirtualSourceProviderQuery.getExt2ProvMap().keySet().contains(extension);
    }

    public static boolean hasVirtualSource(Indexable indexable) {
        Parameters.notNull((CharSequence)"indexable", (Object)indexable);
        URL url = indexable.getURL();
        if (url == null) {
            return false;
        }
        String extension = FileObjects.getExtension(url.getFile());
        return VirtualSourceProviderQuery.hasVirtualSource(extension);
    }

    public static Collection<? extends JavaCustomIndexer.CompileTuple> translate(Iterable<? extends Indexable> indexables, File root) throws IOException {
        VirtualSourceProvider prov;
        Parameters.notNull((CharSequence)"files", indexables);
        Parameters.notNull((CharSequence)"root", (Object)root);
        HashMap<String, Pair> m = new HashMap<String, Pair>();
        Map<String, VirtualSourceProvider> e2p = VirtualSourceProviderQuery.getExt2ProvMap();
        HashMap<File, Indexable> file2indexables = new HashMap<File, Indexable>();
        for (Indexable indexable : indexables) {
            String ext = FileObjects.getExtension(indexable.getURL().getPath());
            prov = e2p.get(ext);
            if (prov == null) continue;
            Pair p = (Pair)m.get(ext);
            List l = null;
            if (p == null) {
                l = new LinkedList();
                m.put(ext, Pair.of((Object)prov, l));
            } else {
                l = (List)p.second();
            }
            try {
                File file = Utilities.toFile((URI)indexable.getURL().toURI());
                l.add(file);
                file2indexables.put(file, indexable);
                continue;
            }
            catch (URISyntaxException use) {
                IOException ioe = new IOException();
                ioe.initCause(use);
                throw ioe;
            }
        }
        R r = new R(root, file2indexables);
        for (Pair p : m.values()) {
            prov = (VirtualSourceProvider)p.first();
            List tf = (List)p.second();
            r.setProvider(prov);
            prov.translate((Iterable)tf, root, (VirtualSourceProvider.Result)r);
        }
        return r.getResult();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static Map<String, VirtualSourceProvider> getExt2ProvMap() {
        Class<VirtualSourceProviderQuery> class_ = VirtualSourceProviderQuery.class;
        synchronized (VirtualSourceProviderQuery.class) {
            if (ext2prov != null) {
                // ** MonitorExit[var0] (shouldn't be in output)
                return ext2prov;
            }
            // ** MonitorExit[var0] (shouldn't be in output)
            LinkedList allInstances = new LinkedList(result.allInstances());
            Class<VirtualSourceProviderQuery> class_2 = VirtualSourceProviderQuery.class;
            synchronized (VirtualSourceProviderQuery.class) {
                if (ext2prov == null) {
                    ext2prov = new HashMap<String, VirtualSourceProvider>();
                    for (VirtualSourceProvider vsp : allInstances) {
                        for (String ext : vsp.getSupportedExtensions()) {
                            ext2prov.put(ext, vsp);
                        }
                    }
                }
                // ** MonitorExit[var1_2] (shouldn't be in output)
                return ext2prov;
            }
        }
    }

    private static synchronized void reset() {
        ext2prov = null;
    }

    static {
        l = new LookupListener(){

            public void resultChanged(LookupEvent ev) {
                VirtualSourceProviderQuery.reset();
            }
        };
        result.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)l, result));
    }

    private static class R
    implements VirtualSourceProvider.Result {
        private final File root;
        private final Map<? extends File, Indexable> file2indexables;
        private final String rootURL;
        private VirtualSourceProvider currentProvider;
        final List<JavaCustomIndexer.CompileTuple> res = new LinkedList<JavaCustomIndexer.CompileTuple>();

        public R(File root, Map<? extends File, Indexable> file2indexables) throws IOException {
            assert (root != null);
            assert (file2indexables != null);
            this.root = root;
            String _rootURL = Utilities.toURI((File)root).toURL().toString();
            if (!_rootURL.endsWith("/")) {
                _rootURL = _rootURL + '/';
            }
            this.rootURL = _rootURL;
            this.file2indexables = file2indexables;
        }

        public List<JavaCustomIndexer.CompileTuple> getResult() {
            this.currentProvider = null;
            return this.res;
        }

        void setProvider(VirtualSourceProvider provider) {
            assert (provider != null);
            this.currentProvider = provider;
        }

        public void add(File source, String packageName, String relativeName, CharSequence content) {
            try {
                Indexable indexable = this.file2indexables.get(source);
                assert (indexable != null);
                String baseName = relativeName + '.' + FileObjects.getExtension(source.getName());
                String folder = FileObjects.convertPackage2Folder(packageName);
                if (folder.length() > 0) {
                    folder = folder + '/';
                }
                this.res.add(new JavaCustomIndexer.CompileTuple(new ForwardingPrefetchableJavaFileObject(FileObjects.memoryFileObject(packageName, baseName, new URI(this.rootURL + folder + baseName), System.currentTimeMillis(), content)){

                    @Override
                    public JavaFileObject.Kind getKind() {
                        return JavaFileObject.Kind.SOURCE;
                    }
                }, indexable, true, this.currentProvider.index()));
            }
            catch (URISyntaxException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }

    }

}

