/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.parsing.api.indexing.IndexingManager
 */
package org.netbeans.modules.java.source.usages;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.java.source.indexing.TransactionContext;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;
import org.netbeans.modules.parsing.api.indexing.IndexingManager;

public final class PersistentIndexTransaction
extends TransactionContext.Service {
    private static final Logger LOG = Logger.getLogger(PersistentIndexTransaction.class.getName());
    private final URL root;
    private ClassIndexImpl.Writer indexWriter;
    private boolean closedTx;
    private boolean brokenIndex;

    private PersistentIndexTransaction(@NonNull URL root) {
        this.root = root;
    }

    @NonNull
    public static PersistentIndexTransaction create(@NonNull URL root) {
        return new PersistentIndexTransaction(root);
    }

    @Override
    protected void commit() throws IOException {
        this.closeTx();
        if (this.indexWriter != null) {
            if (!this.brokenIndex) {
                try {
                    this.indexWriter.commit();
                }
                catch (Throwable t) {
                    if (t instanceof ThreadDeath) {
                        throw (ThreadDeath)t;
                    }
                    LOG.log(Level.WARNING, "Broken index for root: {0} reason: {1}, recovering.", new Object[]{this.root, t.getMessage()});
                    this.brokenIndex = true;
                }
            } else {
                this.rollBackImpl();
            }
            if (this.brokenIndex) {
                this.handleBrokenRoot();
            }
        }
    }

    @Override
    protected void rollBack() throws IOException {
        this.closeTx();
        if (this.indexWriter != null) {
            this.rollBackImpl();
            if (this.brokenIndex) {
                this.handleBrokenRoot();
            }
        }
    }

    public void setIndexWriter(@NonNull ClassIndexImpl.Writer writer) {
        assert (this.indexWriter == null);
        assert (writer != null);
        this.indexWriter = writer;
    }

    public void setBroken() {
        this.brokenIndex = true;
    }

    @CheckForNull
    public ClassIndexImpl.Writer getIndexWriter() {
        return this.indexWriter;
    }

    private void closeTx() {
        if (this.closedTx) {
            throw new IllegalStateException("Already commited or rolled back transaction.");
        }
        this.closedTx = true;
    }

    private void handleBrokenRoot() throws IOException {
        this.indexWriter.clear();
        IndexingManager.getDefault().refreshIndex(this.root, null, true, false);
    }

    private void rollBackImpl() {
        try {
            this.indexWriter.rollback();
        }
        catch (Throwable t) {
            if (t instanceof ThreadDeath) {
                throw (ThreadDeath)t;
            }
            LOG.log(Level.WARNING, "Broken index for root: {0} reason: {1}, recovering.", new Object[]{this.root, t.getMessage()});
            this.brokenIndex = true;
        }
    }
}

