/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.ParserFactory
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.java.source.parsing;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.modules.java.source.parsing.JavacParser;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserFactory;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.util.Lookup;

public class JavacParserFactory
extends ParserFactory {
    private static final Logger TIMER = Logger.getLogger("TIMER.JavacParser");

    public JavacParser createParser(Collection<Snapshot> snapshots) {
        assert (snapshots != null);
        if (snapshots.size() == 1) {
            FileObject fo = snapshots.iterator().next().getSource().getFileObject();
            try {
                if (fo == null) {
                    return null;
                }
                if (fo.getFileSystem().isDefault() && fo.getAttribute("javax.script.ScriptEngine") != null && fo.getAttribute("template") == Boolean.TRUE) {
                    return null;
                }
            }
            catch (FileStateInvalidException fsie) {
                // empty catch block
            }
        }
        JavacParser parser = new JavacParser(snapshots, false);
        if (TIMER.isLoggable(Level.FINE)) {
            LogRecord rec = new LogRecord(Level.FINE, "JavacParser");
            rec.setParameters(new Object[]{parser});
            TIMER.log(rec);
        }
        return parser;
    }

    public JavacParser createPrivateParser(Snapshot snapshot) {
        assert (snapshot != null);
        return snapshot.getSource().getFileObject() != null ? new JavacParser(Collections.singletonList(snapshot), true) : null;
    }

    public static JavacParserFactory getDefault() {
        Lookup lookup = MimeLookup.getLookup((String)"text/x-java");
        return (JavacParserFactory)((Object)lookup.lookup(ParserFactory.class));
    }
}

