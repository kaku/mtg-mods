/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 *  org.netbeans.modules.parsing.api.indexing.IndexingManager
 *  org.netbeans.spi.project.ProjectConfiguration
 *  org.netbeans.spi.project.ProjectConfigurationProvider
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.java.source.indexing;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.source.JavaFileFilterQuery;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.parsing.api.indexing.IndexingManager;
import org.netbeans.spi.project.ProjectConfiguration;
import org.netbeans.spi.project.ProjectConfigurationProvider;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

final class JavaFileFilterListener
implements ChangeListener {
    private static final String ATTR_FILTER_CFG = "filterCfg";
    private static JavaFileFilterListener instance;
    private final Map<URL, JavaFileFilterImplementation> listensOn = Collections.synchronizedMap(new HashMap());

    private JavaFileFilterListener() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    boolean startListeningOn(FileObject root) {
        boolean result;
        assert (root != null);
        result = true;
        try {
            URL rootURL = root.getURL();
            Map<URL, JavaFileFilterImplementation> map = this.listensOn;
            synchronized (map) {
                JavaFileFilterImplementation filter = this.listensOn.get(rootURL);
                if (filter == null && (filter = JavaFileFilterQuery.getFilter(root)) != null) {
                    filter.addChangeListener((ChangeListener)this);
                    this.listensOn.put(rootURL, filter);
                    result = this.verify(rootURL);
                }
            }
        }
        catch (IOException ioe) {
            Exceptions.printStackTrace((Throwable)ioe);
        }
        catch (URISyntaxException use) {
            Exceptions.printStackTrace((Throwable)use);
        }
        return result;
    }

    JavaFileFilterImplementation stopListeningOn(URL rootURL) {
        assert (rootURL != null);
        JavaFileFilterImplementation filter = this.listensOn.remove(rootURL);
        if (filter != null) {
            filter.removeChangeListener((ChangeListener)this);
        }
        return filter;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void stateChanged(ChangeEvent event) {
        Map.Entry[] entries;
        Map<URL, JavaFileFilterImplementation> map = this.listensOn;
        synchronized (map) {
            entries = this.listensOn.entrySet().toArray(new Map.Entry[this.listensOn.size()]);
        }
        Object source = event.getSource();
        for (Map.Entry entry : entries) {
            if (!((JavaFileFilterImplementation)entry.getValue()).equals(source)) continue;
            URL root = (URL)entry.getKey();
            try {
                this.verify(root);
            }
            catch (IOException ioe) {
                Exceptions.printStackTrace((Throwable)ioe);
            }
            catch (URISyntaxException use) {
                Exceptions.printStackTrace((Throwable)use);
            }
            finally {
                IndexingManager.getDefault().refreshIndex(root, null, true);
            }
        }
    }

    private boolean verify(URL root) throws IOException, URISyntaxException {
        ProjectConfiguration cfg;
        ProjectConfigurationProvider cp;
        boolean result = true;
        Project p = FileOwnerQuery.getOwner((URI)root.toURI());
        if (p != null && (cp = (ProjectConfigurationProvider)p.getLookup().lookup(ProjectConfigurationProvider.class)) != null && (cfg = cp.getActiveConfiguration()) != null) {
            String name = cfg.getDisplayName();
            result = !JavaIndex.ensureAttributeValue(root, "filterCfg", name);
        }
        return result;
    }

    static synchronized JavaFileFilterListener getDefault() {
        if (instance == null) {
            instance = new JavaFileFilterListener();
        }
        return instance;
    }
}

