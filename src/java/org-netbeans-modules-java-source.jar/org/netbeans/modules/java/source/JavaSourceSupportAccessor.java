/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.java.source;

import java.util.Collection;
import org.netbeans.modules.java.source.JavaSourceAccessor;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public abstract class JavaSourceSupportAccessor {
    public static JavaSourceSupportAccessor ACCESSOR;

    public abstract Collection<FileObject> getVisibleEditorsFiles();

    static {
        try {
            Class.forName("org.netbeans.api.java.source.support.OpenedEditors", true, JavaSourceAccessor.class.getClassLoader());
        }
        catch (ClassNotFoundException e) {
            Exceptions.printStackTrace((Throwable)e);
        }
    }
}

