/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.spi.indexing.Context
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package org.netbeans.modules.java.source.indexing;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public final class CheckSums {
    private static final String CHECK_SUMS_FILE = "checksums.properties";
    private static final String DEPRECATED = "DEPRECATED";
    private final Context context;
    private final Properties props = new Properties();
    private final MessageDigest md;

    public static CheckSums forContext(Context context) throws IOException, NoSuchAlgorithmException {
        return new CheckSums(context);
    }

    private CheckSums(Context context) throws IOException, NoSuchAlgorithmException {
        assert (context != null);
        this.context = context;
        this.md = MessageDigest.getInstance("MD5");
        this.load();
    }

    public boolean checkAndSet(URL file, Iterable<? extends TypeElement> topLevelElements, Elements elements) {
        String sum;
        String fileId = file.toExternalForm();
        String value = (String)this.props.setProperty(fileId, sum = CheckSums.computeCheckSum(this.md, topLevelElements, elements));
        return value == null || value.equals(sum);
    }

    public void remove(URL file) {
        String fileId = file.toExternalForm();
        this.props.remove(fileId);
    }

    public void store() throws IOException {
        File indexDir = FileUtil.toFile((FileObject)this.context.getIndexFolder());
        File f = new File(indexDir, "checksums.properties");
        FileOutputStream out = new FileOutputStream(f);
        Throwable throwable = null;
        try {
            this.props.store(out, "");
        }
        catch (Throwable x2) {
            throwable = x2;
            throw x2;
        }
        finally {
            if (out != null) {
                if (throwable != null) {
                    try {
                        out.close();
                    }
                    catch (Throwable x2) {
                        throwable.addSuppressed(x2);
                    }
                } else {
                    out.close();
                }
            }
        }
    }

    private void load() throws IOException {
        File indexDir = FileUtil.toFile((FileObject)this.context.getIndexFolder());
        File f = new File(indexDir, "checksums.properties");
        if (f.canRead()) {
            try {
                FileInputStream in = new FileInputStream(f);
                Throwable throwable = null;
                try {
                    this.props.load(in);
                }
                catch (Throwable x2) {
                    throwable = x2;
                    throw x2;
                }
                finally {
                    if (in != null) {
                        if (throwable != null) {
                            try {
                                in.close();
                            }
                            catch (Throwable x2) {
                                throwable.addSuppressed(x2);
                            }
                        } else {
                            in.close();
                        }
                    }
                }
            }
            catch (IllegalArgumentException iae) {
                this.props.clear();
            }
        }
    }

    static String computeCheckSum(MessageDigest md, Iterable<? extends TypeElement> topLevelElements, Elements elements) {
        LinkedList<TypeElement> toHandle = new LinkedList<TypeElement>();
        for (TypeElement te2 : topLevelElements) {
            toHandle.offer(te2);
        }
        ArrayList<String> sigs = new ArrayList<String>();
        while (!toHandle.isEmpty()) {
            TypeElement te = (TypeElement)toHandle.poll();
            if (te == null) continue;
            sigs.add(String.valueOf(te.asType()) + CheckSums.getExtendedModifiers(elements, te));
            for (Element e : te.getEnclosedElements()) {
                switch (e.getKind()) {
                    case CLASS: 
                    case INTERFACE: 
                    case ENUM: 
                    case ANNOTATION_TYPE: {
                        if (e.getModifiers().contains((Object)Modifier.PRIVATE)) break;
                        toHandle.offer((TypeElement)e);
                        break;
                    }
                    case CONSTRUCTOR: 
                    case METHOD: 
                    case FIELD: 
                    case ENUM_CONSTANT: {
                        if (e.getModifiers().contains((Object)Modifier.PRIVATE)) break;
                        StringBuilder sb = new StringBuilder();
                        sb.append(e.getSimpleName());
                        sb.append(String.valueOf(e.asType()));
                        sb.append(CheckSums.getExtendedModifiers(elements, e));
                        sigs.add(sb.toString());
                    }
                }
            }
        }
        Collections.sort(sigs);
        StringBuilder sb = new StringBuilder();
        for (String s : sigs) {
            sb.append(s);
        }
        byte[] bytes = md.digest(sb.toString().getBytes());
        return new String(bytes);
    }

    private static String getExtendedModifiers(Elements elements, Element el) {
        Object v;
        StringBuilder sb = new StringBuilder();
        for (Modifier m : el.getModifiers()) {
            sb.append(m.name());
        }
        if (elements.isDeprecated(el)) {
            sb.append("DEPRECATED");
        }
        if (el.getKind() == ElementKind.FIELD && (v = ((VariableElement)el).getConstantValue()) != null) {
            sb.append(v.getClass().getName());
            sb.append(String.valueOf(v));
        }
        return sb.toString();
    }

}

