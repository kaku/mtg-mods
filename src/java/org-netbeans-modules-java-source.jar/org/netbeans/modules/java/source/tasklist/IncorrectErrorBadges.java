/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.modules.parsing.api.indexing.IndexingManager
 *  org.netbeans.modules.parsing.impl.indexing.PathRegistry
 *  org.netbeans.modules.parsing.impl.indexing.friendapi.IndexingController
 *  org.netbeans.modules.parsing.spi.indexing.ErrorsCache
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.java.source.tasklist;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.tools.Diagnostic;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.source.CancellableTask;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.support.EditorAwareJavaSourceTaskFactory;
import org.netbeans.modules.parsing.api.indexing.IndexingManager;
import org.netbeans.modules.parsing.impl.indexing.PathRegistry;
import org.netbeans.modules.parsing.impl.indexing.friendapi.IndexingController;
import org.netbeans.modules.parsing.spi.indexing.ErrorsCache;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.util.RequestProcessor;

public class IncorrectErrorBadges
implements CancellableTask<CompilationInfo> {
    private static final boolean DISABLE = Boolean.getBoolean(IncorrectErrorBadges.class.getName() + ".disable");
    private static final Logger LOG = Logger.getLogger(IncorrectErrorBadges.class.getName());
    private int invocationCount;
    private long timestamp;
    private FactoryImpl factory;
    private static final RequestProcessor WORKER = new RequestProcessor(IncorrectErrorBadges.class.getName());

    private IncorrectErrorBadges(FactoryImpl factory) {
        this.factory = factory;
    }

    @Override
    public void cancel() {
    }

    @Override
    public void run(CompilationInfo info) {
        if (DISABLE) {
            LOG.fine("Disabled");
            return;
        }
        if (IndexingController.getDefault().isInProtectedMode()) {
            LOG.fine("RepositoryUpdater in protected mode");
            return;
        }
        LOG.log(Level.FINE, "invocationCount={0}, file={1}", new Object[]{this.invocationCount, info.getFileObject()});
        if (this.invocationCount++ > 1) {
            LOG.log(Level.FINE, "Too many invocations: {0}", this.invocationCount);
            return;
        }
        try {
            FileObject file;
            boolean hasErrorBadge;
            boolean containsError = false;
            for (Diagnostic d : info.getDiagnostics()) {
                if (d.getKind() != Diagnostic.Kind.ERROR) continue;
                LOG.log(Level.FINE, "File contains errors: {0}", (Object)info.getFileObject());
                containsError = true;
                break;
            }
            if (hasErrorBadge = ErrorsCache.isInError((FileObject)(file = info.getFileObject()), (boolean)false)) {
                LOG.log(Level.FINE, "Errorscache.isInError: {0}", (Object)info.getFileObject());
            }
            if (containsError == hasErrorBadge) {
                return;
            }
            DataObject d2 = DataObject.find((FileObject)file);
            if (d2.isModified()) {
                LOG.log(Level.FINE, "File is modified: {0}", (Object)info.getFileObject());
                return;
            }
            if (this.invocationCount == 1) {
                this.timestamp = file.lastModified().getTime();
                LOG.log(Level.FINE, "Capturing timestamp={0}, file={1}", new Object[]{this.timestamp, info.getFileObject()});
                WORKER.post(new Runnable(){

                    @Override
                    public void run() {
                        IncorrectErrorBadges.this.factory.rescheduleImpl(file);
                    }
                }, 2 * IndexingController.getDefault().getFileLocksDelay());
                return;
            }
            long lastModified = file.lastModified().getTime();
            if (this.timestamp != 0 && this.timestamp != lastModified) {
                LOG.log(Level.FINE, "File modified since last check: {0}, timestamp={1}, lastModified={2}, invocationCount={3}", new Object[]{info.getFileObject(), this.timestamp, lastModified, this.invocationCount});
                return;
            }
            LOG.log(Level.WARNING, "Incorrect error badges detected, file={0}.", FileUtil.getFileDisplayName((FileObject)file));
            ClassPath sourcePath = info.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.SOURCE);
            FileObject root = sourcePath.findOwnerRoot(file);
            if (root == null) {
                LOG.log(Level.WARNING, "The file is not on its own source classpath, ignoring.");
                return;
            }
            if (!PathRegistry.getDefault().isKnownRoot(root.getURL())) {
                LOG.log(Level.WARNING, "Not PathRegistry controlled root: " + (Object)root);
                return;
            }
            LOG.log(Level.WARNING, "Going to recompute root={0}, files in error={1}.", new Object[]{FileUtil.getFileDisplayName((FileObject)root), ErrorsCache.getAllFilesInError((URL)root.getURL())});
            IndexingManager.getDefault().refreshIndex(root.getURL(), null, true, true);
        }
        catch (IOException ex) {
            LOG.log(Level.FINE, null, ex);
        }
    }

    public static final class FactoryImpl
    extends EditorAwareJavaSourceTaskFactory {
        public FactoryImpl() {
            super(JavaSource.Phase.UP_TO_DATE, JavaSource.Priority.MIN);
        }

        @Override
        protected CancellableTask<CompilationInfo> createTask(FileObject file) {
            return new IncorrectErrorBadges(this);
        }

        void rescheduleImpl(FileObject file) {
            this.reschedule(file);
        }
    }

}

