/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.java.source;

import java.util.Collection;
import org.netbeans.api.java.source.JavaSourceTaskFactory;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.RequestProcessor;

public final class JavaSourceTaskFactoryManager {
    private static JavaSourceTaskFactoryManager INSTANCE;
    private Lookup.Result<JavaSourceTaskFactory> factories;
    public static Accessor ACCESSOR;

    public static synchronized void register() {
        INSTANCE = new JavaSourceTaskFactoryManager();
    }

    private JavaSourceTaskFactoryManager() {
        final RequestProcessor.Task updateTask = new RequestProcessor("JavaSourceTaskFactoryManager Worker", 1).create(new Runnable(){

            @Override
            public void run() {
                JavaSourceTaskFactoryManager.this.update();
            }
        });
        this.factories = Lookup.getDefault().lookupResult(JavaSourceTaskFactory.class);
        this.factories.addLookupListener(new LookupListener(){

            public void resultChanged(LookupEvent ev) {
                updateTask.schedule(0);
            }
        });
        this.update();
    }

    private void update() {
        for (JavaSourceTaskFactory f : this.factories.allInstances()) {
            ACCESSOR.fireChangeEvent(f);
        }
    }

    public static interface Accessor {
        public void fireChangeEvent(JavaSourceTaskFactory var1);
    }

}

