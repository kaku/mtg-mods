/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.util.ChangeSupport
 */
package org.netbeans.modules.java.source.parsing;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.openide.util.ChangeSupport;

class ClasspathInfoListener
implements ChangeListener {
    private final ChangeSupport changeSupport;
    private final Runnable callBack;

    ClasspathInfoListener(@NonNull ChangeSupport changedSupport, @NullAllowed Runnable callBack) {
        assert (changedSupport != null);
        this.changeSupport = changedSupport;
        this.callBack = callBack;
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if (this.callBack != null) {
            this.callBack.run();
        }
        this.changeSupport.fireChange();
    }
}

