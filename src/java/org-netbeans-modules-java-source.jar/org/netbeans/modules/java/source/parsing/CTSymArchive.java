/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 */
package org.netbeans.modules.java.source.parsing;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.java.source.parsing.CachingArchive;

public class CTSymArchive
extends CachingArchive {
    private static final Logger LOG = Logger.getLogger(CTSymArchive.class.getName());
    private final File ctSym;
    private final String pathToRootInCtSym;
    private ZipFile zipFile;
    private Set<String> pkgs;

    CTSymArchive(@NonNull File archive, @NullAllowed String pathToRootInArchive, @NonNull File ctSym, @NullAllowed String pathToRootInCtSym) {
        super(archive, pathToRootInArchive, true);
        this.ctSym = ctSym;
        this.pathToRootInCtSym = pathToRootInCtSym;
    }

    @Override
    protected void beforeInit() throws IOException {
        this.zipFile = new ZipFile(this.ctSym);
        this.pkgs = new HashSet<String>();
        Enumeration<? extends ZipEntry> entries = this.zipFile.entries();
        while (entries.hasMoreElements()) {
            int i;
            String dirname;
            ZipEntry entry = entries.nextElement();
            if (entry.isDirectory()) continue;
            String name = entry.getName();
            if (this.pathToRootInCtSym != null) {
                if (!name.startsWith(this.pathToRootInCtSym)) continue;
                i = name.lastIndexOf(47);
                dirname = i < this.pathToRootInCtSym.length() ? "" : name.substring(this.pathToRootInCtSym.length(), i);
            } else {
                i = name.lastIndexOf(47);
                dirname = i == -1 ? "" : name.substring(0, i);
            }
            this.pkgs.add(dirname);
        }
    }

    @Override
    protected short getFlags(@NonNull String dirname) throws IOException {
        boolean isPublic = this.pkgs.contains(dirname);
        LOG.log(Level.FINE, "Package: {0} is public: {1}", new Object[]{dirname, isPublic});
        return isPublic ? 0 : 1;
    }

    @Override
    protected void afterInit(boolean success) throws IOException {
        this.pkgs = null;
    }

    @Override
    protected ZipFile getArchive(short flags) {
        return flags == 0 ? this.zipFile : super.getArchive(flags);
    }

    @Override
    protected String getPathToRoot(short flags) {
        return flags == 0 ? this.pathToRootInCtSym : super.getPathToRoot(flags);
    }
}

