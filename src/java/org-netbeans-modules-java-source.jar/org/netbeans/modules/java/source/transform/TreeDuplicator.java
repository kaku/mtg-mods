/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.AnnotatedTypeTree
 *  com.sun.source.tree.AnnotationTree
 *  com.sun.source.tree.ArrayAccessTree
 *  com.sun.source.tree.ArrayTypeTree
 *  com.sun.source.tree.AssertTree
 *  com.sun.source.tree.AssignmentTree
 *  com.sun.source.tree.BinaryTree
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.BreakTree
 *  com.sun.source.tree.CaseTree
 *  com.sun.source.tree.CatchTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.CompoundAssignmentTree
 *  com.sun.source.tree.ConditionalExpressionTree
 *  com.sun.source.tree.ContinueTree
 *  com.sun.source.tree.DoWhileLoopTree
 *  com.sun.source.tree.EmptyStatementTree
 *  com.sun.source.tree.EnhancedForLoopTree
 *  com.sun.source.tree.ErroneousTree
 *  com.sun.source.tree.ExpressionStatementTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.ForLoopTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.IfTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.InstanceOfTree
 *  com.sun.source.tree.IntersectionTypeTree
 *  com.sun.source.tree.LabeledStatementTree
 *  com.sun.source.tree.LambdaExpressionTree
 *  com.sun.source.tree.LiteralTree
 *  com.sun.source.tree.MemberReferenceTree
 *  com.sun.source.tree.MemberReferenceTree$ReferenceMode
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodInvocationTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.NewArrayTree
 *  com.sun.source.tree.NewClassTree
 *  com.sun.source.tree.ParameterizedTypeTree
 *  com.sun.source.tree.ParenthesizedTree
 *  com.sun.source.tree.PrimitiveTypeTree
 *  com.sun.source.tree.ReturnTree
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.SwitchTree
 *  com.sun.source.tree.SynchronizedTree
 *  com.sun.source.tree.ThrowTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.TreeVisitor
 *  com.sun.source.tree.TryTree
 *  com.sun.source.tree.TypeCastTree
 *  com.sun.source.tree.TypeParameterTree
 *  com.sun.source.tree.UnaryTree
 *  com.sun.source.tree.UnionTypeTree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.tree.WhileLoopTree
 *  com.sun.source.tree.WildcardTree
 *  com.sun.tools.javac.util.Context
 */
package org.netbeans.modules.java.source.transform;

import com.sun.source.tree.AnnotatedTypeTree;
import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.ArrayAccessTree;
import com.sun.source.tree.ArrayTypeTree;
import com.sun.source.tree.AssertTree;
import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BinaryTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.BreakTree;
import com.sun.source.tree.CaseTree;
import com.sun.source.tree.CatchTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.CompoundAssignmentTree;
import com.sun.source.tree.ConditionalExpressionTree;
import com.sun.source.tree.ContinueTree;
import com.sun.source.tree.DoWhileLoopTree;
import com.sun.source.tree.EmptyStatementTree;
import com.sun.source.tree.EnhancedForLoopTree;
import com.sun.source.tree.ErroneousTree;
import com.sun.source.tree.ExpressionStatementTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.ForLoopTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.InstanceOfTree;
import com.sun.source.tree.IntersectionTypeTree;
import com.sun.source.tree.LabeledStatementTree;
import com.sun.source.tree.LambdaExpressionTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MemberReferenceTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.NewArrayTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.ParameterizedTypeTree;
import com.sun.source.tree.ParenthesizedTree;
import com.sun.source.tree.PrimitiveTypeTree;
import com.sun.source.tree.ReturnTree;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.SwitchTree;
import com.sun.source.tree.SynchronizedTree;
import com.sun.source.tree.ThrowTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TreeVisitor;
import com.sun.source.tree.TryTree;
import com.sun.source.tree.TypeCastTree;
import com.sun.source.tree.TypeParameterTree;
import com.sun.source.tree.UnaryTree;
import com.sun.source.tree.UnionTypeTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.tree.WhileLoopTree;
import com.sun.source.tree.WildcardTree;
import com.sun.tools.javac.util.Context;
import java.util.List;
import java.util.Set;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.tools.JavaFileObject;
import org.netbeans.modules.java.source.builder.ASTService;
import org.netbeans.modules.java.source.builder.CommentHandlerService;
import org.netbeans.modules.java.source.builder.TreeFactory;

public class TreeDuplicator
implements TreeVisitor<Tree, Void> {
    private final CommentHandlerService comments;
    private final ASTService model;
    private final TreeFactory make;

    public TreeDuplicator(Context context) {
        this.comments = CommentHandlerService.instance(context);
        this.model = ASTService.instance(context);
        this.make = TreeFactory.instance(context);
    }

    public Tree visitAnnotatedType(AnnotatedTypeTree tree, Void p) {
        AnnotatedTypeTree n = this.make.AnnotatedType(tree.getAnnotations(), (Tree)tree.getUnderlyingType());
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitAnnotation(AnnotationTree tree, Void p) {
        AnnotationTree n = tree.getKind() == Tree.Kind.ANNOTATION ? this.make.Annotation(tree.getAnnotationType(), tree.getArguments()) : this.make.TypeAnnotation(tree.getAnnotationType(), tree.getArguments());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitMethodInvocation(MethodInvocationTree tree, Void p) {
        MethodInvocationTree n = this.make.MethodInvocation(tree.getTypeArguments(), tree.getMethodSelect(), tree.getArguments());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitAssert(AssertTree tree, Void p) {
        AssertTree n = this.make.Assert(tree.getCondition(), tree.getDetail());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitAssignment(AssignmentTree tree, Void p) {
        AssignmentTree n = this.make.Assignment(tree.getVariable(), tree.getExpression());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitCompoundAssignment(CompoundAssignmentTree tree, Void p) {
        CompoundAssignmentTree n = this.make.CompoundAssignment(tree.getKind(), tree.getVariable(), tree.getExpression());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitBinary(BinaryTree tree, Void p) {
        BinaryTree n = this.make.Binary(tree.getKind(), tree.getLeftOperand(), tree.getRightOperand());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitBlock(BlockTree tree, Void p) {
        BlockTree n = this.make.Block(tree.getStatements(), tree.isStatic());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitBreak(BreakTree tree, Void p) {
        BreakTree n = this.make.Break(tree.getLabel());
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitCase(CaseTree tree, Void p) {
        CaseTree n = this.make.Case(tree.getExpression(), tree.getStatements());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitCatch(CatchTree tree, Void p) {
        CatchTree n = this.make.Catch(tree.getParameter(), tree.getBlock());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitClass(ClassTree tree, Void p) {
        ClassTree n = this.make.Class(tree.getModifiers(), tree.getSimpleName(), tree.getTypeParameters(), tree.getExtendsClause(), tree.getImplementsClause(), tree.getMembers());
        this.model.setElement((Tree)n, this.model.getElement((Tree)tree));
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitConditionalExpression(ConditionalExpressionTree tree, Void p) {
        ConditionalExpressionTree n = this.make.ConditionalExpression(tree.getCondition(), tree.getTrueExpression(), tree.getFalseExpression());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitContinue(ContinueTree tree, Void p) {
        ContinueTree n = this.make.Continue(tree.getLabel());
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitDoWhileLoop(DoWhileLoopTree tree, Void p) {
        DoWhileLoopTree n = this.make.DoWhileLoop(tree.getCondition(), tree.getStatement());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitErroneous(ErroneousTree tree, Void p) {
        ErroneousTree n = this.make.Erroneous(tree.getErrorTrees());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitExpressionStatement(ExpressionStatementTree tree, Void p) {
        ExpressionStatementTree n = this.make.ExpressionStatement(tree.getExpression());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitEnhancedForLoop(EnhancedForLoopTree tree, Void p) {
        EnhancedForLoopTree n = this.make.EnhancedForLoop(tree.getVariable(), tree.getExpression(), tree.getStatement());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitForLoop(ForLoopTree tree, Void p) {
        ForLoopTree n = this.make.ForLoop(tree.getInitializer(), tree.getCondition(), tree.getUpdate(), tree.getStatement());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitIdentifier(IdentifierTree tree, Void p) {
        IdentifierTree n = this.make.Identifier(tree.getName());
        this.model.setElement((Tree)n, this.model.getElement((Tree)tree));
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitIf(IfTree tree, Void p) {
        IfTree n = this.make.If(tree.getCondition(), tree.getThenStatement(), tree.getElseStatement());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitImport(ImportTree tree, Void p) {
        ImportTree n = this.make.Import(tree.getQualifiedIdentifier(), tree.isStatic());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitArrayAccess(ArrayAccessTree tree, Void p) {
        ArrayAccessTree n = this.make.ArrayAccess(tree.getExpression(), tree.getIndex());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitLabeledStatement(LabeledStatementTree tree, Void p) {
        LabeledStatementTree n = this.make.LabeledStatement(tree.getLabel(), tree.getStatement());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitLiteral(LiteralTree tree, Void p) {
        LiteralTree n = this.make.Literal(tree.getValue());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitMethod(MethodTree tree, Void p) {
        MethodTree n = this.make.Method(tree.getModifiers(), tree.getName().toString(), (Tree)((ExpressionTree)tree.getReturnType()), tree.getTypeParameters(), tree.getParameters(), tree.getThrows(), tree.getBody(), (ExpressionTree)tree.getDefaultValue());
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitModifiers(ModifiersTree tree, Void p) {
        ModifiersTree n = this.make.Modifiers(tree.getFlags(), tree.getAnnotations());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitNewArray(NewArrayTree tree, Void p) {
        NewArrayTree n = this.make.NewArray(tree.getType(), tree.getDimensions(), tree.getInitializers());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitNewClass(NewClassTree tree, Void p) {
        NewClassTree n = this.make.NewClass(tree.getEnclosingExpression(), tree.getTypeArguments(), tree.getIdentifier(), tree.getArguments(), tree.getClassBody());
        this.model.setElement((Tree)n, this.model.getElement((Tree)tree));
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitLambdaExpression(LambdaExpressionTree tree, Void p) {
        LambdaExpressionTree n = this.make.LambdaExpression(tree.getParameters(), tree.getBody());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitParenthesized(ParenthesizedTree tree, Void p) {
        ParenthesizedTree n = this.make.Parenthesized(tree.getExpression());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitReturn(ReturnTree tree, Void p) {
        ReturnTree n = this.make.Return(tree.getExpression());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitMemberSelect(MemberSelectTree tree, Void p) {
        MemberSelectTree n = this.make.MemberSelect(tree.getExpression(), tree.getIdentifier());
        this.model.setElement((Tree)n, this.model.getElement((Tree)tree));
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitMemberReference(MemberReferenceTree tree, Void p) {
        MemberReferenceTree n = this.make.MemberReference(tree.getMode(), tree.getName(), tree.getQualifierExpression(), tree.getTypeArguments());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitEmptyStatement(EmptyStatementTree tree, Void p) {
        EmptyStatementTree n = this.make.EmptyStatement();
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitSwitch(SwitchTree tree, Void p) {
        SwitchTree n = this.make.Switch(tree.getExpression(), tree.getCases());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitSynchronized(SynchronizedTree tree, Void p) {
        SynchronizedTree n = this.make.Synchronized(tree.getExpression(), tree.getBlock());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitThrow(ThrowTree tree, Void p) {
        ThrowTree n = this.make.Throw(tree.getExpression());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitCompilationUnit(CompilationUnitTree tree, Void p) {
        CompilationUnitTree n = this.make.CompilationUnit(tree.getPackageAnnotations(), tree.getPackageName(), tree.getImports(), tree.getTypeDecls(), tree.getSourceFile());
        this.model.setElement((Tree)n, this.model.getElement((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitTry(TryTree tree, Void p) {
        TryTree n = this.make.Try(tree.getResources(), tree.getBlock(), tree.getCatches(), tree.getFinallyBlock());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitParameterizedType(ParameterizedTypeTree tree, Void p) {
        ParameterizedTypeTree n = this.make.ParameterizedType(tree.getType(), tree.getTypeArguments());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitUnionType(UnionTypeTree tree, Void p) {
        UnionTypeTree n = this.make.UnionType(tree.getTypeAlternatives());
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitIntersectionType(IntersectionTypeTree tree, Void p) {
        IntersectionTypeTree n = this.make.IntersectionType(tree.getBounds());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitArrayType(ArrayTypeTree tree, Void p) {
        ArrayTypeTree n = this.make.ArrayType(tree.getType());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitTypeCast(TypeCastTree tree, Void p) {
        TypeCastTree n = this.make.TypeCast(tree.getType(), tree.getExpression());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitPrimitiveType(PrimitiveTypeTree tree, Void p) {
        PrimitiveTypeTree n = this.make.PrimitiveType(tree.getPrimitiveTypeKind());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitTypeParameter(TypeParameterTree tree, Void p) {
        TypeParameterTree n = this.make.TypeParameter(tree.getName(), tree.getBounds());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitInstanceOf(InstanceOfTree tree, Void p) {
        InstanceOfTree n = this.make.InstanceOf(tree.getExpression(), tree.getType());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitUnary(UnaryTree tree, Void p) {
        UnaryTree n = this.make.Unary(tree.getKind(), tree.getExpression());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitVariable(VariableTree tree, Void p) {
        VariableTree n = this.make.Variable(tree.getModifiers(), tree.getName().toString(), tree.getType(), tree.getInitializer());
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitWhileLoop(WhileLoopTree tree, Void p) {
        WhileLoopTree n = this.make.WhileLoop(tree.getCondition(), tree.getStatement());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitWildcard(WildcardTree tree, Void p) {
        WildcardTree n = this.make.Wildcard(tree.getKind(), tree.getBound());
        this.model.setType((Tree)n, this.model.getType((Tree)tree));
        this.comments.copyComments((Tree)tree, (Tree)n);
        this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
        return n;
    }

    public Tree visitOther(Tree tree, Void p) {
        return tree;
    }
}

