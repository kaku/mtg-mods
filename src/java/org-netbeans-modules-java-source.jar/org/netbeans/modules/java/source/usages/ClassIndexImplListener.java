/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.source.usages;

import java.util.EventListener;
import org.netbeans.modules.java.source.usages.ClassIndexImplEvent;

public interface ClassIndexImplListener
extends EventListener {
    public void typesAdded(ClassIndexImplEvent var1);

    public void typesRemoved(ClassIndexImplEvent var1);

    public void typesChanged(ClassIndexImplEvent var1);
}

