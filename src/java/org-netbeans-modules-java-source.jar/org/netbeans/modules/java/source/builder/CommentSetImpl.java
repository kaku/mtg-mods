/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.source.builder;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.java.source.Comment;
import org.netbeans.modules.java.source.query.CommentSet;

public final class CommentSetImpl
implements Cloneable,
CommentSet {
    private boolean commentsMapped;
    private final Map<CommentSet.RelativePosition, List<Comment>> commentsMap = new HashMap<CommentSet.RelativePosition, List<Comment>>();

    @Override
    public void addPrecedingComment(String s) {
        this.addPrecedingComment(Comment.create(s));
    }

    @Override
    public void addPrecedingComment(Comment c) {
        this.addComment(CommentSet.RelativePosition.PRECEDING, c);
    }

    @Override
    public void addPrecedingComments(List<Comment> comments) {
        for (Comment comment : comments) {
            this.addComment(CommentSet.RelativePosition.PRECEDING, comment);
        }
    }

    @Override
    public void addTrailingComment(String s) {
        this.addTrailingComment(Comment.create(s));
    }

    @Override
    public void addTrailingComment(Comment c) {
        this.addComment(CommentSet.RelativePosition.TRAILING, c);
    }

    @Override
    public void addTrailingComments(List<Comment> comments) {
        for (Comment comment : comments) {
            this.addComment(CommentSet.RelativePosition.TRAILING, comment);
        }
    }

    @Override
    public List<Comment> getPrecedingComments() {
        return this.getComments(CommentSet.RelativePosition.PRECEDING);
    }

    @Override
    public List<Comment> getTrailingComments() {
        return this.getComments(CommentSet.RelativePosition.TRAILING);
    }

    @Override
    public boolean hasComments() {
        return !this.commentsMap.isEmpty();
    }

    @Override
    public int pos() {
        return this.pos(CommentSet.RelativePosition.PRECEDING);
    }

    @Override
    public int pos(CommentSet.RelativePosition position) {
        List<Comment> list = this.getComments(position);
        return list.isEmpty() ? -2 : list.get(0).pos();
    }

    @Override
    public void addComment(CommentSet.RelativePosition positioning, Comment c) {
        this.addComment(positioning, c, false);
    }

    public void addComment(CommentSet.RelativePosition positioning, Comment c, boolean mergeExisting) {
        List comments;
        if (this.commentsMap.containsKey((Object)positioning)) {
            comments = this.commentsMap.get((Object)positioning);
        } else {
            comments = new LinkedList();
            this.commentsMap.put(positioning, comments);
        }
        if (c.isNew()) {
            comments.add(c);
        } else {
            int index = 0;
            int npos = c.pos();
            for (Comment o : comments) {
                if (o.isNew()) {
                    comments.add(c);
                    return;
                }
                int pos = o.pos();
                if (pos > npos) break;
                if (pos == npos && c == o) {
                    return;
                }
                ++index;
            }
            if (mergeExisting) {
                comments.add(index, c);
            } else {
                comments.add(c);
            }
        }
    }

    public void addComments(CommentSet.RelativePosition positioning, Iterable<? extends Comment> comments) {
        for (Comment c : comments) {
            this.addComment(positioning, c, true);
        }
    }

    @Override
    public List<Comment> getComments(CommentSet.RelativePosition positioning) {
        if (this.commentsMap.containsKey((Object)positioning)) {
            return this.commentsMap.get((Object)positioning);
        }
        return Collections.emptyList();
    }

    @Override
    public boolean hasChanges() {
        if (this.commentsMap.isEmpty()) {
            return false;
        }
        for (List<Comment> commentList : this.commentsMap.values()) {
            for (Comment comment : commentList) {
                if (!comment.isNew()) continue;
                return true;
            }
        }
        return false;
    }

    public Object clone() {
        try {
            return super.clone();
        }
        catch (CloneNotSupportedException e) {
            throw new InternalError("Unexpected " + e);
        }
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append('{');
        boolean first = true;
        for (Map.Entry<CommentSet.RelativePosition, List<Comment>> entry : this.commentsMap.entrySet()) {
            if (!first) {
                sb.append(", ");
                first = false;
            }
            sb.append("[").append((Object)entry.getKey()).append(" -> ");
            for (Comment comment : entry.getValue()) {
                sb.append(',').append(comment.getText());
            }
            sb.append("]");
        }
        sb.append('}');
        return sb.toString();
    }

    public boolean areCommentsMapped() {
        return this.commentsMapped;
    }

    public void commentsMapped() {
        this.commentsMapped = true;
    }

    public void clearComments(CommentSet.RelativePosition forPosition) {
        this.commentsMap.remove((Object)forPosition);
    }
}

