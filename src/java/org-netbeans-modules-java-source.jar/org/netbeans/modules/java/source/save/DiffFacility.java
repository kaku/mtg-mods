/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 */
package org.netbeans.modules.java.source.save;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.java.source.save.CasualDiff;
import org.netbeans.modules.java.source.save.ComputeDiff;
import org.netbeans.modules.java.source.save.Difference;

class DiffFacility {
    private final Collection<CasualDiff.Diff> gdiff;
    private int[] sections;
    private int lineStart;

    public DiffFacility(Collection<CasualDiff.Diff> diff) {
        this.gdiff = diff;
    }

    private static List<Line> getLines(String text) {
        char[] chars = text.toCharArray();
        ArrayList<Line> list = new ArrayList<Line>();
        int pointer = 0;
        for (int i = 0; i < chars.length; ++i) {
            if (chars[i] != '\n') continue;
            list.add(new Line(new String(chars, pointer, i - pointer + 1), pointer, i + 1));
            pointer = i + 1;
        }
        if (pointer < chars.length) {
            list.add(new Line(new String(chars, pointer, chars.length - pointer), pointer, chars.length));
        }
        return list;
    }

    public DiffFacility withSections(int[] sections, int lineStart) {
        this.sections = sections;
        this.lineStart = lineStart;
        return this;
    }

    private int[] computeLineSections(Line[] lines1, Line[] lines2, int offset) {
        int i1 = 0;
        int i2 = 0;
        int delta = Math.max(0, offset - this.lineStart);
        int[] res = new int[this.sections.length];
        for (int p = 0; p < this.sections.length; p += 2) {
            int orig = this.sections[p] - offset;
            int nue = this.sections[p + 1] - delta;
            while (i1 < lines1.length && lines1[i1].end <= orig) {
                ++i1;
            }
            while (i2 < lines2.length && lines2[i2].end <= nue) {
                ++i2;
            }
            if (i1 < lines1.length && i2 < lines2.length) {
                if (lines1[i1].start >= orig != lines2[i2].start >= nue) {
                    continue;
                }
            } else {
                return p == res.length ? res : Arrays.copyOf(res, p);
            }
            res[p] = i1;
            res[p + 1] = i2;
        }
        return res;
    }

    public List<CasualDiff.Diff> makeListMatch(String text1, String text2, int offset) {
        if (this.sections == null) {
            this.sections = new int[]{text1.length(), text2.length()};
        }
        List<Line> list1 = DiffFacility.getLines(text1);
        List<Line> list2 = DiffFacility.getLines(text2);
        Line[] lines1 = list1.toArray(new Line[list1.size()]);
        Line[] lines2 = list2.toArray(new Line[list2.size()]);
        List<Difference> diffs = new ComputeDiff<Line>(lines1, lines2, this.computeLineSections(lines1, lines2, offset)).diff();
        for (Difference diff : diffs) {
            StringBuilder builder;
            int type;
            String match2;
            int i;
            int delStart = diff.getDeletedStart();
            int delEnd = diff.getDeletedEnd();
            int addStart = diff.getAddedStart();
            int addEnd = diff.getAddedEnd();
            int n = delEnd != -1 && addEnd != -1 ? 99 : (type = delEnd == -1 ? 97 : 100);
            if (type == 97) {
                builder = new StringBuilder();
                for (i = addStart; i <= addEnd; ++i) {
                    builder.append(lines2[i].data);
                }
                this.gdiff.add(CasualDiff.Diff.insert(delEnd == -1 ? (delStart < lines1.length ? lines1[delStart].start + offset : (lines1.length != 0 ? lines1[lines1.length - 1].end + offset : offset)) : lines1[delEnd].end + offset, builder.toString()));
                continue;
            }
            if (type == 100) {
                this.gdiff.add(CasualDiff.Diff.delete(lines1[delStart].start + offset, lines1[delEnd].end + offset));
                continue;
            }
            if (addEnd - addStart > delEnd - delStart) {
                builder = new StringBuilder();
                for (i = delStart; i <= delEnd; ++i) {
                    builder.append(lines1[i].data);
                }
                String match1 = builder.toString();
                builder = new StringBuilder();
                for (int i2 = addStart; i2 <= addStart + delEnd - delStart; ++i2) {
                    builder.append(lines2[i2].data);
                }
                match2 = builder.toString();
                this.makeTokenListMatch(match1, match2, lines1[delStart].start + offset);
                builder = new StringBuilder();
                for (int i3 = addStart + delEnd - delStart + 1; i3 <= addEnd; ++i3) {
                    builder.append(lines2[i3].data);
                }
                String s = builder.toString();
                if ("".equals(s)) continue;
                this.gdiff.add(CasualDiff.Diff.insert(lines1[delEnd].end + offset, s));
                continue;
            }
            builder = new StringBuilder();
            for (i = delStart; i <= delEnd; ++i) {
                builder.append(lines1[i].data);
            }
            String match1 = builder.toString();
            builder = new StringBuilder();
            for (int i4 = addStart; i4 <= addEnd; ++i4) {
                builder.append(lines2[i4].data);
            }
            match2 = builder.toString();
            this.makeTokenListMatch(match1, match2, lines1[delStart].start + offset);
        }
        return null;
    }

    private void removeOrStripLastNewline(List<Line> list1) {
        int lastNewline;
        int idx = list1.size() - 1;
        Line last1 = list1.remove(idx);
        int firstNewline = last1.data.indexOf(10);
        if (firstNewline != (lastNewline = last1.data.lastIndexOf(10))) {
            String stripped = last1.data.substring(0, last1.data.lastIndexOf(10));
            list1.add(new Line(stripped, last1.start, last1.start + stripped.length()));
        }
    }

    private void removeSameTrailingLineComments(List<Line> list1, List<Line> list2) {
        String s1 = list1.get((int)(list1.size() - 1)).data;
        String s2 = list2.get((int)(list2.size() - 1)).data;
        assert (s1.startsWith("//"));
        assert (s2.startsWith("//"));
        if ((s1 = s1.substring(2).trim()).equals(s2 = s2.substring(2).trim())) {
            list1.remove(list1.size() - 1);
            list2.remove(list2.size() - 1);
        }
    }

    public List<CasualDiff.Diff> makeTokenListMatch(String text1, String text2, int currentPos) {
        TokenSequence seq1 = TokenHierarchy.create((CharSequence)text1, (Language)JavaTokenId.language()).tokenSequence(JavaTokenId.language());
        TokenSequence seq2 = TokenHierarchy.create((CharSequence)text2, (Language)JavaTokenId.language()).tokenSequence(JavaTokenId.language());
        ArrayList<Line> list1 = new ArrayList<Line>();
        ArrayList<Line> list2 = new ArrayList<Line>();
        JavaTokenId lastId1 = null;
        while (seq1.moveNext()) {
            String data = seq1.token().text().toString();
            lastId1 = (JavaTokenId)seq1.token().id();
            list1.add(new Line(data, seq1.offset(), seq1.offset() + data.length()));
        }
        JavaTokenId lastId2 = null;
        while (seq2.moveNext()) {
            String data = seq2.token().text().toString();
            lastId2 = (JavaTokenId)seq2.token().id();
            list2.add(new Line(data, seq2.offset(), seq2.offset() + data.length()));
        }
        if (lastId1 != null && lastId1 == lastId2) {
            if (lastId1 == JavaTokenId.WHITESPACE && ((Line)list1.get((int)(list1.size() - 1))).data.endsWith("\n") == list2.get((int)(list2.size() - 1)).data.endsWith("\n")) {
                this.removeOrStripLastNewline(list1);
                this.removeOrStripLastNewline(list2);
            } else if (lastId1 == JavaTokenId.LINE_COMMENT) {
                this.removeSameTrailingLineComments(list1, list2);
            }
        }
        Line[] lines1 = list1.toArray(new Line[list1.size()]);
        Line[] lines2 = list2.toArray(new Line[list2.size()]);
        List<Difference> diffs = new ComputeDiff<Line>(lines1, lines2, null).diff();
        for (Difference diff : diffs) {
            StringBuilder builder;
            int type;
            int i;
            int delStart = diff.getDeletedStart();
            int delEnd = diff.getDeletedEnd();
            int addStart = diff.getAddedStart();
            int addEnd = diff.getAddedEnd();
            int n = delEnd != -1 && addEnd != -1 ? 99 : (type = delEnd == -1 ? 97 : 100);
            if (type == 97) {
                builder = new StringBuilder();
                for (i = addStart; i <= addEnd; ++i) {
                    builder.append(lines2[i].data);
                }
                this.gdiff.add(CasualDiff.Diff.insert(currentPos + (delEnd == -1 ? (delStart < lines1.length ? lines1[delStart].start : (lines1.length > 0 ? lines1[lines1.length - 1].end : 0)) : lines1[delEnd].end), builder.toString()));
                continue;
            }
            if (type == 100) {
                this.gdiff.add(CasualDiff.Diff.delete(currentPos + lines1[delStart].start, currentPos + lines1[delEnd].end));
                continue;
            }
            builder = new StringBuilder();
            this.gdiff.add(CasualDiff.Diff.delete(currentPos + lines1[delStart].start, currentPos + lines1[delEnd].end));
            for (i = addStart; i <= addEnd; ++i) {
                builder.append(lines2[i].data);
            }
            this.gdiff.add(CasualDiff.Diff.insert(currentPos + lines1[delEnd].end, builder.toString()));
        }
        return null;
    }

    private static class Line {
        String data;
        int end;
        int start;

        Line(String data, int start, int end) {
            this.start = start;
            this.end = end;
            this.data = data;
        }

        public String toString() {
            return this.data.toString();
        }

        public boolean equals(Object o) {
            if (o instanceof Line) {
                return this.data.equals(((Line)o).data);
            }
            return false;
        }

        public int hashCode() {
            return this.data.hashCode();
        }
    }

}

