/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.index.Term
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.java.source.usages;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.lucene.index.Term;
import org.netbeans.api.annotations.common.NonNull;

final class TermCollector {
    private final Map<Integer, Set<Term>> doc2Terms = new HashMap<Integer, Set<Term>>();

    TermCollector() {
    }

    void add(int docId, @NonNull Term term) {
        Set<Term> slot = this.doc2Terms.get(docId);
        if (slot == null) {
            slot = new HashSet<Term>();
            this.doc2Terms.put(docId, slot);
        }
        slot.add(term);
    }

    Set<Term> get(int docId) {
        return this.doc2Terms.get(docId);
    }

    static interface TermCollecting {
        public void attach(TermCollector var1);
    }

}

