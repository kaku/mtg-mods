/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.java.source.parsing;

import java.net.URL;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.java.source.parsing.SiblingProvider;

public interface SiblingSource {
    public void push(@NonNull URL var1, boolean var2);

    @NonNull
    public URL pop();

    @NonNull
    public SiblingProvider getProvider();
}

