/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.impl.indexing.PathRegistry
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.netbeans.modules.parsing.spi.SchedulerTask
 *  org.netbeans.modules.parsing.spi.TaskFactory
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.java.source.classpath;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.JavaParserResultTask;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.usages.ClasspathInfoAccessor;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.impl.indexing.PathRegistry;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.SchedulerTask;
import org.netbeans.modules.parsing.spi.TaskFactory;
import org.openide.util.Exceptions;

public class SourcePathCheck
extends JavaParserResultTask {
    private final Factory factory;

    public SourcePathCheck(Factory factory) {
        super(JavaSource.Phase.PARSED);
        this.factory = factory;
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    public void run(Parser.Result result, SchedulerEvent event) {
        CompilationInfo info = CompilationInfo.get(result);
        ClasspathInfo cpInfo = info.getClasspathInfo();
        if (cpInfo != null) {
            ClassPath cachedSrc = ClasspathInfoAccessor.getINSTANCE().getCachedClassPath(cpInfo, ClasspathInfo.PathKind.SOURCE);
            ClassPath src = cpInfo.getClassPath(ClasspathInfo.PathKind.SOURCE);
            try {
                HashSet<URL> unknown = new HashSet<URL>();
                if (cachedSrc.entries().isEmpty() && !src.entries().isEmpty()) {
                    for (ClassPath.Entry entry : src.entries()) {
                        URL url = entry.getURL();
                        if (this.factory.firedFor.contains(url) || JavaIndex.hasSourceCache(url, false) || FileOwnerQuery.getOwner((URI)url.toURI()) == null) continue;
                        unknown.add(url);
                        this.factory.firedFor.add(url);
                    }
                }
                if (!unknown.isEmpty()) {
                    PathRegistry.getDefault().registerUnknownSourceRoots(src, unknown);
                }
            }
            catch (URISyntaxException e) {
                Exceptions.printStackTrace((Throwable)e);
            }
        }
    }

    public int getPriority() {
        return 1;
    }

    public Class<? extends Scheduler> getSchedulerClass() {
        return Scheduler.EDITOR_SENSITIVE_TASK_SCHEDULER;
    }

    public void cancel() {
    }

    public static final class Factory
    extends TaskFactory {
        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        private final Set<URL> firedFor = new HashSet<URL>();

        public Collection<? extends SchedulerTask> create(Snapshot snapshot) {
            return Collections.singleton(new SourcePathCheck(this));
        }
    }

}

