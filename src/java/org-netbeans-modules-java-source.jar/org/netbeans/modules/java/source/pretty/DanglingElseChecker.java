/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCBlock
 *  com.sun.tools.javac.tree.JCTree$JCDoWhileLoop
 *  com.sun.tools.javac.tree.JCTree$JCForLoop
 *  com.sun.tools.javac.tree.JCTree$JCIf
 *  com.sun.tools.javac.tree.JCTree$JCLabeledStatement
 *  com.sun.tools.javac.tree.JCTree$JCStatement
 *  com.sun.tools.javac.tree.JCTree$JCSynchronized
 *  com.sun.tools.javac.tree.JCTree$JCWhileLoop
 *  com.sun.tools.javac.tree.JCTree$Visitor
 *  com.sun.tools.javac.util.List
 */
package org.netbeans.modules.java.source.pretty;

import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.List;

public class DanglingElseChecker
extends JCTree.Visitor {
    boolean foundDanglingElse;

    public boolean hasDanglingElse(JCTree t) {
        if (t == null) {
            return false;
        }
        this.foundDanglingElse = false;
        t.accept((JCTree.Visitor)this);
        return this.foundDanglingElse;
    }

    public void visitTree(JCTree tree) {
    }

    public void visitIf(JCTree.JCIf tree) {
        if (tree.elsepart == null) {
            this.foundDanglingElse = true;
        } else {
            tree.elsepart.accept((JCTree.Visitor)this);
        }
    }

    public void visitWhileLoop(JCTree.JCWhileLoop tree) {
        tree.body.accept((JCTree.Visitor)this);
    }

    public void visitDoLoop(JCTree.JCDoWhileLoop tree) {
        tree.body.accept((JCTree.Visitor)this);
    }

    public void visitForLoop(JCTree.JCForLoop tree) {
        tree.body.accept((JCTree.Visitor)this);
    }

    public void visitSynchronized(JCTree.JCSynchronized tree) {
        tree.body.accept((JCTree.Visitor)this);
    }

    public void visitLabelled(JCTree.JCLabeledStatement tree) {
        tree.body.accept((JCTree.Visitor)this);
    }

    public void visitBlock(JCTree.JCBlock tree) {
        if (!tree.stats.isEmpty() && tree.stats.tail.isEmpty()) {
            ((JCTree.JCStatement)tree.stats.head).accept((JCTree.Visitor)this);
        }
    }
}

