/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.parsing.spi.indexing.SuspendStatus
 */
package org.netbeans.modules.java.source.indexing;

import java.io.Closeable;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.java.source.indexing.JavaCustomIndexer;
import org.netbeans.modules.java.source.indexing.JavaIndexerWorker;
import org.netbeans.modules.parsing.spi.indexing.SuspendStatus;

class SourcePrefetcher
implements Iterator<JavaCustomIndexer.CompileTuple>,
Closeable {
    private final Iterator<? extends JavaCustomIndexer.CompileTuple> iterator;
    private boolean active;

    private SourcePrefetcher(@NonNull Iterator<? extends JavaCustomIndexer.CompileTuple> iterator) {
        assert (iterator != null);
        this.iterator = iterator;
    }

    @Override
    public boolean hasNext() {
        return this.iterator.hasNext();
    }

    @CheckForNull
    @Override
    public JavaCustomIndexer.CompileTuple next() {
        if (this.active) {
            throw new IllegalStateException("Call remove to free resources");
        }
        JavaCustomIndexer.CompileTuple res = this.iterator.next();
        this.active = true;
        return res;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void remove() {
        if (!this.active) {
            throw new IllegalStateException("Call next before remove");
        }
        try {
            this.iterator.remove();
        }
        finally {
            this.active = false;
        }
    }

    @Override
    public void close() throws IOException {
        if (this.iterator instanceof Closeable) {
            ((Closeable)((Object)this.iterator)).close();
        }
    }

    static SourcePrefetcher create(@NonNull Collection<? extends JavaCustomIndexer.CompileTuple> files, @NonNull SuspendStatus suspendStatus) {
        return new SourcePrefetcher(JavaIndexerWorker.getCompileTupleIterator(files, suspendStatus));
    }
}

