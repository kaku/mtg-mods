/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.java.source.indexing;

import java.io.IOException;
import java.net.URL;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.indexing.TransactionContext;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;
import org.netbeans.modules.java.source.usages.ClassIndexManager;
import org.openide.util.Parameters;

class CacheAttributesTransaction
extends TransactionContext.Service {
    private final URL root;
    private final boolean srcRoot;
    private final boolean allFiles;
    private boolean closed;
    private boolean invalid;

    private CacheAttributesTransaction(@NonNull URL root, boolean srcRoot, boolean allFiles) {
        this.root = root;
        this.srcRoot = srcRoot;
        this.allFiles = allFiles;
    }

    static CacheAttributesTransaction create(@NonNull URL root, boolean srcRoot, boolean allFiles) {
        Parameters.notNull((CharSequence)"root", (Object)root);
        return new CacheAttributesTransaction(root, srcRoot, allFiles);
    }

    @Override
    protected void commit() throws IOException {
        this.closeTx();
        ClassIndexImpl uq = ClassIndexManager.getDefault().getUsagesQuery(this.root, false);
        if (uq == null) {
            return;
        }
        if (this.srcRoot) {
            if (uq.getState() == ClassIndexImpl.State.NEW && uq.getType() != ClassIndexImpl.Type.SOURCE) {
                JavaIndex.setAttribute(this.root, "source", Boolean.TRUE.toString());
            }
        } else if (this.allFiles) {
            JavaIndex.setAttribute(this.root, "source", Boolean.FALSE.toString());
        }
        if (!this.invalid) {
            uq.setState(ClassIndexImpl.State.INITIALIZED);
        }
    }

    @Override
    protected void rollBack() throws IOException {
        this.closeTx();
    }

    void setInvalid(boolean invalid) {
        this.invalid = invalid;
    }

    private void closeTx() {
        if (this.closed) {
            throw new IllegalStateException("Already commited or rolled back transaction.");
        }
        this.closed = true;
    }
}

