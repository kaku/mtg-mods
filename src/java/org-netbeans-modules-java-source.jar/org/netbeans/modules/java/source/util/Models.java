/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.source.util;

import java.util.List;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;
import org.netbeans.modules.java.source.util.Factory;

public final class Models {
    private Models() {
    }

    public static <T> ListModel fromList(List<? extends T> list) {
        return new ListListModel<T>(list);
    }

    public static <T, P> ListModel translating(ListModel model, Factory<T, P> factory) {
        return new TranslatingListModel<T, P>(model, factory);
    }

    private static class TranslatingListModel<T, P>
    implements ListModel {
        private Factory<T, P> factory;
        private ListModel listModel;

        public TranslatingListModel(ListModel model, Factory<T, P> factory) {
            this.listModel = model;
            this.factory = factory;
        }

        public T getElementAt(int index) {
            Object original = this.listModel.getElementAt(index);
            return this.factory.create(original);
        }

        @Override
        public int getSize() {
            return this.listModel.getSize();
        }

        @Override
        public void removeListDataListener(ListDataListener l) {
        }

        @Override
        public void addListDataListener(ListDataListener l) {
        }
    }

    private static class ListListModel<T>
    implements ListModel {
        private List<? extends T> list;

        public ListListModel(List<? extends T> list) {
            this.list = list;
        }

        public T getElementAt(int index) {
            return this.list.get(index);
        }

        @Override
        public int getSize() {
            return this.list.size();
        }

        @Override
        public void removeListDataListener(ListDataListener l) {
        }

        @Override
        public void addListDataListener(ListDataListener l) {
        }
    }

}

