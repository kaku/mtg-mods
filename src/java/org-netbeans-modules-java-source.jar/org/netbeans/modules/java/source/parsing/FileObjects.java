/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.api.ClientCodeWrapper
 *  com.sun.tools.javac.api.ClientCodeWrapper$Trusted
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.queries.FileEncodingQuery
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 *  org.netbeans.modules.parsing.spi.indexing.Indexable
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.Parameters
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.source.parsing;

import com.sun.tools.javac.api.ClientCodeWrapper;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.Buffer;
import java.nio.CharBuffer;
import java.nio.channels.CompletionHandler;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.NestingKind;
import javax.tools.FileObject;
import javax.tools.JavaFileObject;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.queries.FileEncodingQuery;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.source.JavaFileFilterQuery;
import org.netbeans.modules.java.source.parsing.AbstractSourceFileObject;
import org.netbeans.modules.java.source.parsing.FastJar;
import org.netbeans.modules.java.source.parsing.ForwardingInferableJavaFileObject;
import org.netbeans.modules.java.source.parsing.InferableJavaFileObject;
import org.netbeans.modules.java.source.parsing.PrefetchableJavaFileObject;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.Parameters;
import org.openide.util.Utilities;

public class FileObjects {
    public static final Comparator<String> SIMPLE_NAME_STRING_COMPARATOR = new Comparator<String>(){

        @Override
        public int compare(String o1, String o2) {
            return FileObjects.getSimpleName(o1).compareTo(FileObjects.getSimpleName(o2));
        }
    };
    public static final Comparator<JavaFileObject> SIMPLE_NAME_FILEOBJECT_COMPARATOR = new Comparator<JavaFileObject>(){

        @Override
        public int compare(JavaFileObject o1, JavaFileObject o2) {
            String n1 = FileObjects.getSimpleName(o1);
            String n2 = FileObjects.getSimpleName(o2);
            return n1.compareTo(n2);
        }
    };
    public static final String JAVA = "java";
    public static final String CLASS = "class";
    public static final String JAR = "jar";
    public static final String FILE = "file";
    public static final String ZIP = "zip";
    public static final String HTML = "html";
    public static final String SIG = "sig";
    public static final String RS = "rs";
    public static final String RX = "rx";
    public static final String RAPT = "rapt";
    public static final String RES = "res";
    public static final char NBFS_SEPARATOR_CHAR = '/';
    public static final String RESOURCES = "resouces.res";
    private static final String encodingName = new OutputStreamWriter(new ByteArrayOutputStream()).getEncoding();
    private static final Set<String> javaFlavorExt = new HashSet<String>();

    private FileObjects() {
    }

    public static InferableJavaFileObject zipFileObject(File zipFile, String folder, String baseName, long mtime) {
        assert (zipFile != null);
        return new ZipFileObject(zipFile, folder, baseName, mtime);
    }

    public static InferableJavaFileObject zipFileObject(File zipFile, String folder, String baseName, long mtime, long offset) {
        assert (zipFile != null);
        return new FastZipFileObject(zipFile, folder, baseName, mtime, offset);
    }

    public static InferableJavaFileObject zipFileObject(@NonNull ZipFile zipFile, @NonNull String folder, @NonNull String baseName, long mtime) {
        return FileObjects.zipFileObject(zipFile, null, folder, baseName, mtime);
    }

    public static InferableJavaFileObject zipFileObject(@NonNull ZipFile zipFile, @NullAllowed String pathToRootInArchive, @NonNull String folder, @NonNull String baseName, long mtime) {
        assert (zipFile != null);
        return new CachedZipFileObject(zipFile, pathToRootInArchive, folder, baseName, mtime);
    }

    @NonNull
    public static PrefetchableJavaFileObject fileFileObject(@NonNull File file, @NonNull File root, @NullAllowed JavaFileFilterImplementation filter, @NullAllowed Charset encoding) {
        assert (file != null);
        assert (root != null);
        String[] pkgNamePair = FileObjects.getFolderAndBaseName(FileObjects.getRelativePath(root, file), File.separatorChar);
        return new FileBase(file, FileObjects.convertFolder2Package(pkgNamePair[0], File.separatorChar), pkgNamePair[1], filter, encoding);
    }

    @NonNull
    public static PrefetchableJavaFileObject fileFileObject(@NonNull Indexable indexable, @NonNull File root, @NullAllowed JavaFileFilterImplementation filter, @NullAllowed Charset encoding) throws IOException {
        assert (indexable != null);
        assert (root != null);
        String[] pkgNamePair = FileObjects.getFolderAndBaseName(indexable.getRelativePath(), '/');
        try {
            File file = Utilities.toFile((URI)indexable.getURL().toURI());
            return new FileBase(file, FileObjects.convertFolder2Package(pkgNamePair[0]), pkgNamePair[1], filter, encoding);
        }
        catch (URISyntaxException use) {
            throw new IOException(use);
        }
    }

    @NonNull
    public static PrefetchableJavaFileObject asyncWriteFileObject(@NonNull File file, @NonNull File root, @NullAllowed JavaFileFilterImplementation filter, @NullAllowed Charset encoding, @NonNull Executor pool, @NonNull CompletionHandler<Void, Void> done) {
        String[] pkgNamePair = FileObjects.getFolderAndBaseName(FileObjects.getRelativePath(root, file), File.separatorChar);
        return new AsyncWriteFileObject(file, FileObjects.convertFolder2Package(pkgNamePair[0], File.separatorChar), pkgNamePair[1], filter, encoding, pool, done);
    }

    @NonNull
    public static JavaFileObject templateFileObject(@NonNull org.openide.filesystems.FileObject root, @NonNull String path, @NonNull String name) {
        assert (root != null);
        assert (path != null);
        JavaFileFilterImplementation filter = JavaFileFilterQuery.getFilter(root);
        Charset encoding = FileEncodingQuery.getEncoding((org.openide.filesystems.FileObject)root);
        File rootFile = FileUtil.toFile((org.openide.filesystems.FileObject)root);
        if (rootFile == null) {
            throw new IllegalArgumentException();
        }
        File file = FileUtil.normalizeFile((File)new File(new File(rootFile, path.replace('/', File.separatorChar)), name));
        return new NewFromTemplateFileObject(file, FileObjects.convertFolder2Package(path), name, filter, encoding);
    }

    public static AbstractSourceFileObject sourceFileObject(org.openide.filesystems.FileObject file, org.openide.filesystems.FileObject root) {
        try {
            return FileObjects.sourceFileObject(file, root, null, false);
        }
        catch (IOException ioe) {
            throw new IllegalArgumentException(ioe);
        }
    }

    public static AbstractSourceFileObject sourceFileObject(org.openide.filesystems.FileObject file, org.openide.filesystems.FileObject root, JavaFileFilterImplementation filter, boolean renderNow) throws IOException {
        assert (file != null);
        if (!file.isValid() || file.isVirtual()) {
            throw new InvalidFileException(file);
        }
        return AbstractSourceFileObject.getFactory().createJavaFileObject(new AbstractSourceFileObject.Handle(file, root), filter, null, renderNow);
    }

    public static AbstractSourceFileObject sourceFileObject(org.openide.filesystems.FileObject file, org.openide.filesystems.FileObject root, JavaFileFilterImplementation filter, CharSequence content) throws IOException {
        assert (file != null);
        if (!file.isValid() || file.isVirtual()) {
            throw new InvalidFileException(file);
        }
        return AbstractSourceFileObject.getFactory().createJavaFileObject(new AbstractSourceFileObject.Handle(file, root), filter, content, true);
    }

    public static AbstractSourceFileObject sourceFileObject(final URL path, org.openide.filesystems.FileObject root) throws IOException {
        AbstractSourceFileObject.Handle handle = new AbstractSourceFileObject.Handle(root){

            @Override
            public org.openide.filesystems.FileObject resolveFileObject(boolean write) {
                org.openide.filesystems.FileObject res = super.resolveFileObject(write);
                if (res == null) {
                    try {
                        this.file = write ? FileUtil.createData((org.openide.filesystems.FileObject)this.root, (String)this.getRelativePath()) : URLMapper.findFileObject((URL)path);
                        res = this.file;
                    }
                    catch (IOException e) {
                        // empty catch block
                    }
                }
                return res;
            }

            @Override
            public URL getURL() throws IOException {
                return path;
            }

            @Override
            public String getExt() {
                String ext = super.getExt();
                if (ext == null) {
                    ext = FileObjects.getExtension(path.getPath());
                }
                return ext;
            }

            @Override
            public String getName(boolean includeExtension) {
                String name = super.getName(includeExtension);
                if (name == null) {
                    name = FileObjects.getBaseName(path.getPath(), '/');
                    if (!includeExtension) {
                        name = FileObjects.stripExtension(name);
                    }
                }
                return name;
            }

            @Override
            public String getRelativePath() {
                String relativePath = super.getRelativePath();
                if (relativePath == null) {
                    try {
                        relativePath = FileObjects.getRelativePath(this.root.toURL(), path);
                    }
                    catch (URISyntaxException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
                return relativePath;
            }

            @Override
            public boolean equals(Object obj) {
                if (this.file != null) {
                    return super.equals(obj);
                }
                if (obj instanceof AbstractSourceFileObject.Handle) {
                    try {
                        return this.getURL().equals(((AbstractSourceFileObject.Handle)obj).getURL());
                    }
                    catch (IOException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
                return false;
            }

            @Override
            public int hashCode() {
                return path.hashCode();
            }
        };
        return AbstractSourceFileObject.getFactory().createJavaFileObject(handle, null, null, false);
    }

    public static PrefetchableJavaFileObject memoryFileObject(CharSequence pkg, CharSequence name, CharSequence content) {
        return FileObjects.memoryFileObject(pkg, name, null, System.currentTimeMillis(), content);
    }

    public static PrefetchableJavaFileObject memoryFileObject(CharSequence pkg, CharSequence name, URI uri, long lastModified, CharSequence content) {
        Parameters.notNull((CharSequence)"pkg", (Object)pkg);
        Parameters.notNull((CharSequence)"name", (Object)name);
        Parameters.notNull((CharSequence)"content", (Object)content);
        String pkgStr = pkg instanceof String ? (String)pkg : pkg.toString();
        String nameStr = name instanceof String ? (String)name : name.toString();
        int length = content.length();
        if (length != 0 && Character.isWhitespace(content.charAt(length - 1))) {
            return new MemoryFileObject(pkgStr, nameStr, uri, lastModified, CharBuffer.wrap(content));
        }
        return new MemoryFileObject(pkgStr, nameStr, uri, lastModified, (CharBuffer)CharBuffer.allocate(length + 1).append(content).append(' ').flip());
    }

    public static InferableJavaFileObject nullWriteFileObject(@NonNull InferableJavaFileObject delegate) {
        return delegate instanceof NullWriteFileObject ? delegate : new NullWriteFileObject(delegate);
    }

    public static String stripExtension(String fileName) {
        int dot = fileName.lastIndexOf(".");
        return dot == -1 ? fileName : fileName.substring(0, dot);
    }

    public static String getExtension(String fileName) {
        int dot = fileName.lastIndexOf(46);
        return dot == -1 || dot == fileName.length() - 1 ? "" : fileName.substring(dot + 1);
    }

    public static String getName(JavaFileObject fo, boolean noExt) {
        assert (fo != null);
        if (fo instanceof Base) {
            Base baseFileObject = (Base)fo;
            return noExt ? baseFileObject.getNameWithoutExtension() : baseFileObject.getName();
        }
        try {
            int index2;
            URL url = fo.toUri().toURL();
            String path = url.getPath();
            int index1 = path.lastIndexOf(47);
            int len = noExt ? ((index2 = path.lastIndexOf(46)) > index1 ? index2 : path.length()) : path.length();
            path = path.substring(index1 + 1, len);
            return path;
        }
        catch (MalformedURLException e) {
            return null;
        }
    }

    public static String getBaseName(String fileName) {
        return FileObjects.getBaseName(fileName, File.separatorChar);
    }

    public static String getBaseName(String fileName, char separator) {
        return FileObjects.getFolderAndBaseName(fileName, separator)[1];
    }

    public static String[] getFolderAndBaseName(String fileName, char separator) {
        int i = fileName.lastIndexOf(separator);
        if (i == -1) {
            return new String[]{"", fileName};
        }
        return new String[]{fileName.substring(0, i), fileName.substring(i + 1)};
    }

    @NonNull
    public static String getBinaryName(@NonNull File file, @NonNull File root) {
        assert (file != null && root != null);
        String fileName = FileObjects.getRelativePath(root, file);
        int index = fileName.lastIndexOf(46);
        if (index > 0) {
            fileName = fileName.substring(0, index);
        }
        return fileName.replace(File.separatorChar, '.');
    }

    @NonNull
    public static String convertPackage2Folder(@NonNull String packageName) {
        return FileObjects.convertPackage2Folder(packageName, '/');
    }

    @NonNull
    public static String convertPackage2Folder(@NonNull String packageName, char separatorChar) {
        return packageName.replace('.', separatorChar);
    }

    @NonNull
    public static String convertFolder2Package(@NonNull String folderName) {
        return FileObjects.convertFolder2Package(folderName, '/');
    }

    @NonNull
    public static String convertFolder2Package(@NonNull String folderName, char folderSeparator) {
        return folderName.replace(folderSeparator, '.');
    }

    public static boolean isParentOf(@NonNull URL folder, @NonNull URL file) {
        assert (folder != null && file != null);
        return file.toExternalForm().startsWith(folder.toExternalForm());
    }

    @NonNull
    public static String getRelativePath(@NonNull String packageName, @NonNull String relativeName) {
        if (packageName.isEmpty()) {
            return relativeName;
        }
        StringBuilder relativePath = new StringBuilder();
        relativePath.append(packageName.replace('.', '/'));
        relativePath.append('/');
        relativePath.append(relativeName);
        return relativePath.toString();
    }

    @NonNull
    public static String[] getParentRelativePathAndName(@NonNull String fqn) {
        String[] result = FileObjects.getPackageAndName(fqn);
        if (result != null) {
            result[0] = result[0].replace('.', '/');
        }
        return result;
    }

    @NonNull
    public static String[] getPackageAndName(@NonNull String fqn) {
        if (fqn.charAt(fqn.length() - 1) == '.') {
            return null;
        }
        int index = fqn.lastIndexOf(46);
        if (index < 0) {
            return new String[]{"", fqn};
        }
        return new String[]{fqn.substring(0, index), fqn.substring(index + 1)};
    }

    @NonNull
    public static JavaFileObject.Kind getKind(@NullAllowed String extension) {
        if (extension == null) {
            return JavaFileObject.Kind.OTHER;
        }
        String lcextension = extension.toLowerCase(Locale.ENGLISH);
        if ("java".equals(lcextension)) {
            return JavaFileObject.Kind.SOURCE;
        }
        if ("class".equals(lcextension) || "sig".equals(lcextension)) {
            return JavaFileObject.Kind.CLASS;
        }
        if ("html".equals(lcextension)) {
            return JavaFileObject.Kind.HTML;
        }
        if (javaFlavorExt.contains(lcextension)) {
            return JavaFileObject.Kind.SOURCE;
        }
        return JavaFileObject.Kind.OTHER;
    }

    public static void deleteRecursively(@NonNull File folder) {
        File[] children;
        assert (folder != null);
        if (folder.isDirectory() && (children = folder.listFiles()) != null) {
            for (File file : children) {
                FileObjects.deleteRecursively(file);
            }
        }
        folder.delete();
    }

    @NonNull
    public static String getRelativePath(@NonNull File root, @NonNull File fo) {
        int foIndex;
        String rootPath = root.getAbsolutePath();
        String foPath = fo.getAbsolutePath();
        assert (foPath.startsWith(rootPath));
        int index = rootPath.length();
        if (rootPath.charAt(index - 1) != File.separatorChar) {
            ++index;
        }
        if ((foIndex = foPath.length()) <= index) {
            return "";
        }
        return foPath.substring(index);
    }

    @NonNull
    public static String getRelativePath(@NonNull URL root, @NonNull URL fo) throws URISyntaxException {
        String path = FileObjects.getRelativePath(Utilities.toFile((URI)root.toURI()), Utilities.toFile((URI)fo.toURI()));
        return path.replace(File.separatorChar, '/');
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @NonNull
    public static byte[] asBytes(@NonNull File file) throws IOException {
        byte[] data;
        data = new byte[(int)file.length()];
        BufferedInputStream in = new BufferedInputStream(new FileInputStream(file));
        try {
            int read;
            int len = 0;
            while ((read = in.read(data, len, data.length - len)) > 0) {
                len += read;
            }
            if (len != data.length) {
                data = Arrays.copyOf(data, len);
            }
        }
        finally {
            in.close();
        }
        return data;
    }

    public static boolean isValidFileName(@NonNull FileObject fo) {
        String name = fo instanceof Base ? ((Base)fo).getPath() : fo.toUri().getPath();
        return FileObjects.isValidFileName(name);
    }

    public static boolean isValidFileName(@NonNull CharSequence fileName) {
        for (int i = 0; i < fileName.length(); ++i) {
            char c = fileName.charAt(i);
            switch (c) {
                case '<': 
                case '>': {
                    return false;
                }
            }
        }
        return true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static CharSequence getCharContent(InputStream ins, Charset encoding, JavaFileFilterImplementation filter, long expectedLength, boolean ignoreEncodingErrors) throws IOException {
        int red;
        char[] result;
        Reader in = encoding != null ? new InputStreamReader(ins, encoding) : new InputStreamReader(ins);
        if (filter != null) {
            in = filter.filterReader(in);
        }
        red = 0;
        try {
            int rv;
            int len = (int)expectedLength;
            if (len == 0) {
                ++len;
            }
            result = new char[len + 1];
            while ((rv = in.read(result, red, len - red)) >= 0) {
                if ((red += rv) != len) continue;
                char[] _tmp = new char[2 * len];
                System.arraycopy(result, 0, _tmp, 0, len);
                result = _tmp;
                len = result.length;
            }
        }
        finally {
            in.close();
        }
        result[red++] = 10;
        CharBuffer buffer = CharBuffer.wrap(result, 0, red);
        return buffer;
    }

    private static String getSimpleName(JavaFileObject fo) {
        String name = FileObjects.getName(fo, true);
        int i = name.lastIndexOf(36);
        if (i == -1) {
            return name;
        }
        return name.substring(i + 1);
    }

    private static String getSimpleName(String fileName) {
        String name = FileObjects.getBaseName(fileName);
        int i = name.lastIndexOf(36);
        if (i == -1) {
            return name;
        }
        return name.substring(i + 1);
    }

    static {
        javaFlavorExt.add("btrace");
    }

    public static class InvalidFileException
    extends IOException {
        public InvalidFileException() {
        }

        public InvalidFileException(org.openide.filesystems.FileObject fo) {
            super(NbBundle.getMessage(FileObjects.class, (String)"FMT_InvalidFile", (Object)FileUtil.getFileDisplayName((org.openide.filesystems.FileObject)fo)));
        }
    }

    private static final class AsyncOutputStream
    extends OutputStream {
        private static final int BUFSIZ = 4096;
        private final Callable<OutputStream> superOpenOututStream;
        private final Executor pool;
        private final CompletionHandler<Void, Void> done;
        private byte[] buffer;
        private int index;

        AsyncOutputStream(@NonNull Callable<OutputStream> superOpenOututStream, @NonNull Executor pool, @NonNull CompletionHandler<Void, Void> done) {
            this.superOpenOututStream = superOpenOututStream;
            this.pool = pool;
            this.done = done;
            if (done instanceof Runnable) {
                ((Runnable)((Object)done)).run();
            }
            this.buffer = new byte[4096];
        }

        @Override
        public void write(int b) throws IOException {
            this.ensureSize(1);
            this.buffer[this.index++] = (byte)b;
        }

        @Override
        public void write(byte[] b, int off, int len) throws IOException {
            this.ensureSize(len);
            System.arraycopy(b, off, this.buffer, this.index, len);
            this.index += len;
        }

        @Override
        public void close() throws IOException {
            this.pool.execute(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    Throwable ex = null;
                    try {
                        OutputStream out = (OutputStream)AsyncOutputStream.this.superOpenOututStream.call();
                        Throwable throwable = null;
                        try {
                            out.write(AsyncOutputStream.this.buffer, 0, AsyncOutputStream.this.index);
                        }
                        catch (Throwable x2) {
                            throwable = x2;
                            throw x2;
                        }
                        finally {
                            if (out != null) {
                                if (throwable != null) {
                                    try {
                                        out.close();
                                    }
                                    catch (Throwable x2) {
                                        throwable.addSuppressed(x2);
                                    }
                                } else {
                                    out.close();
                                }
                            }
                        }
                    }
                    catch (Throwable t) {
                        ex = t;
                        if (t instanceof ThreadDeath) {
                            throw (ThreadDeath)t;
                        }
                        if (!(t instanceof InterruptedException)) {
                            Exceptions.printStackTrace((Throwable)t);
                        }
                    }
                    finally {
                        if (ex == null) {
                            AsyncOutputStream.this.done.completed(null, null);
                        } else {
                            AsyncOutputStream.this.done.failed(ex, null);
                        }
                    }
                }
            });
        }

        private void ensureSize(int added) {
            int bufLen = this.buffer.length;
            int required = this.index + added;
            if (bufLen < required) {
                while (bufLen < required) {
                    bufLen <<= 1;
                }
                byte[] newBuffer = new byte[bufLen];
                System.arraycopy(this.buffer, 0, newBuffer, 0, this.buffer.length);
                this.buffer = newBuffer;
            }
        }

    }

    @ClientCodeWrapper.Trusted
    private static final class AsyncWriteFileObject
    extends FileBase {
        private final Executor pool;
        private final CompletionHandler<Void, Void> done;

        AsyncWriteFileObject(@NonNull File file, @NonNull String pkgName, @NonNull String name, @NullAllowed JavaFileFilterImplementation filter, @NullAllowed Charset encoding, @NonNull Executor pool, @NonNull CompletionHandler<Void, Void> done) {
            super(file, pkgName, name, filter, encoding);
            Parameters.notNull((CharSequence)"pool", (Object)pool);
            Parameters.notNull((CharSequence)"done", done);
            this.pool = pool;
            this.done = done;
        }

        @Override
        public OutputStream openOutputStream() throws IOException {
            return new AsyncOutputStream(new Callable<OutputStream>(){

                @Override
                public OutputStream call() throws Exception {
                    return AsyncWriteFileObject.this.openOutputStream();
                }
            }, this.pool, this.done);
        }

    }

    @ClientCodeWrapper.Trusted
    private static class NullWriteFileObject
    extends ForwardingInferableJavaFileObject {
        private NullWriteFileObject(@NonNull InferableJavaFileObject delegate) {
            super(delegate);
        }

        @Override
        public OutputStream openOutputStream() throws IOException {
            return new NullOutputStream();
        }

        @Override
        public Writer openWriter() throws IOException {
            return new OutputStreamWriter(this.openOutputStream());
        }

        private static class NullOutputStream
        extends OutputStream {
            private NullOutputStream() {
            }

            @Override
            public void write(int b) throws IOException {
            }
        }

    }

    @ClientCodeWrapper.Trusted
    private static class MemoryFileObject
    extends Base
    implements PrefetchableJavaFileObject {
        final long lastModified;
        final CharBuffer cb;
        final URI uri;
        final boolean isVirtual;

        public MemoryFileObject(String packageName, String fileName, URI uri, long lastModified, CharBuffer cb) {
            super(packageName, fileName);
            this.cb = cb;
            this.lastModified = lastModified;
            this.uri = uri;
            this.isVirtual = uri != null;
        }

        @Override
        public CharBuffer getCharContent(boolean ignoreEncodingErrors) throws IOException {
            return this.cb.duplicate();
        }

        @Override
        public boolean delete() {
            return false;
        }

        @Override
        public URI toUri() {
            if (this.uri != null) {
                return this.uri;
            }
            return URI.create(FileObjects.convertPackage2Folder(this.pkgName) + '/' + this.nameWithoutExt);
        }

        @Override
        public boolean isVirtual() {
            return this.isVirtual;
        }

        @Override
        public long getLastModified() {
            return this.lastModified;
        }

        @Override
        public InputStream openInputStream() throws IOException {
            return new ByteArrayInputStream(this.cb.toString().getBytes("UTF-8"));
        }

        @Override
        public OutputStream openOutputStream() throws IOException {
            throw new UnsupportedOperationException();
        }

        @Override
        public Reader openReader(boolean b) throws IOException {
            return new StringReader(this.cb.toString());
        }

        @Override
        public Writer openWriter() throws IOException {
            throw new UnsupportedOperationException();
        }

        @Override
        public int prefetch() throws IOException {
            return 0;
        }

        @Override
        public int dispose() {
            return 0;
        }
    }

    @ClientCodeWrapper.Trusted
    private static class CachedZipFileObject
    extends ZipFileBase {
        private final ZipFile zipFile;

        CachedZipFileObject(@NonNull ZipFile zipFile, @NullAllowed String pathToRootInArchive, @NonNull String folderName, @NonNull String baseName, long mtime) {
            super(pathToRootInArchive, folderName, baseName, mtime);
            assert (zipFile != null);
            this.zipFile = zipFile;
        }

        @Override
        public InputStream openInputStream() throws IOException {
            return new BufferedInputStream(this.zipFile.getInputStream(new ZipEntry(this.resName)));
        }

        @Override
        public URI getArchiveURI() {
            return Utilities.toURI((File)new File(this.zipFile.getName()));
        }

        @Override
        protected long getSize() throws IOException {
            ZipEntry ze = this.zipFile.getEntry(this.resName);
            return ze == null ? 0 : ze.getSize();
        }
    }

    @ClientCodeWrapper.Trusted
    private static class FastZipFileObject
    extends ZipFileObject {
        private long offset;

        FastZipFileObject(File archiveFile, String folderName, String baseName, long mtime, long offset) {
            super(archiveFile, folderName, baseName, mtime);
            this.offset = offset;
        }

        @Override
        public InputStream openInputStream() throws IOException {
            try {
                return new BufferedInputStream(FastJar.getInputStream(this.archiveFile, this.offset));
            }
            catch (FileNotFoundException fnf) {
                throw fnf;
            }
            catch (IOException e) {
                return super.openInputStream();
            }
            catch (IndexOutOfBoundsException e) {
                return super.openInputStream();
            }
        }

        @Override
        public long getSize() throws IOException {
            try {
                long size;
                ZipEntry e = FastJar.getZipEntry(this.archiveFile, this.offset);
                if (e != null && (size = e.getSize()) != -1) {
                    return size;
                }
            }
            catch (IOException e) {
                // empty catch block
            }
            return super.getSize();
        }
    }

    @ClientCodeWrapper.Trusted
    private static class ZipFileObject
    extends ZipFileBase {
        protected final File archiveFile;

        ZipFileObject(File archiveFile, String folderName, String baseName, long mtime) {
            super(null, folderName, baseName, mtime);
            assert (archiveFile != null);
            this.archiveFile = archiveFile;
        }

        @Override
        public InputStream openInputStream() throws IOException {
            class ZipInputStream
            extends InputStream {
                private ZipFile zipfile;
                private InputStream delegate;

                public ZipInputStream(ZipFile zf) throws IOException {
                    assert (zf != null);
                    this.zipfile = zf;
                    try {
                        this.delegate = zf.getInputStream(new ZipEntry(ZipFileObject.this.resName));
                        if (this.delegate == null) {
                            throw new IOException();
                        }
                    }
                    catch (IOException e) {
                        try {
                            this.zipfile.close();
                        }
                        catch (IOException e2) {
                            // empty catch block
                        }
                        throw e;
                    }
                }

                @Override
                public int read() throws IOException {
                    throw new UnsupportedOperationException("Not supported yet.");
                }

                @Override
                public int read(byte[] b, int off, int len) throws IOException {
                    return this.delegate.read(b, off, len);
                }

                @Override
                public int available() throws IOException {
                    return this.delegate.available();
                }

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void close() throws IOException {
                    try {
                        this.delegate.close();
                    }
                    finally {
                        this.zipfile.close();
                    }
                }
            }
            ZipFile zf = new ZipFile(this.archiveFile);
            return new BufferedInputStream(new ZipInputStream(zf));
        }

        @Override
        public URI getArchiveURI() {
            return Utilities.toURI((File)this.archiveFile);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        protected long getSize() throws IOException {
            ZipFile zf = new ZipFile(this.archiveFile);
            try {
                ZipEntry ze = zf.getEntry(this.resName);
                long l = ze == null ? 0 : ze.getSize();
                return l;
            }
            finally {
                zf.close();
            }
        }

    }

    static abstract class ZipFileBase
    extends Base {
        protected final long mtime;
        protected final String resName;

        public ZipFileBase(@NullAllowed String pathToRootInArchive, @NonNull String folderName, @NonNull String baseName, long mtime) {
            super(FileObjects.convertFolder2Package(folderName), baseName);
            assert (pathToRootInArchive == null || pathToRootInArchive.charAt(pathToRootInArchive.length() - 1) == '/');
            this.mtime = mtime;
            if (folderName.length() == 0) {
                if (pathToRootInArchive != null) {
                    StringBuilder rn = new StringBuilder(pathToRootInArchive.length() + baseName.length());
                    rn.append(pathToRootInArchive);
                    rn.append(baseName);
                    this.resName = rn.toString();
                } else {
                    this.resName = baseName;
                }
            } else {
                int pathToRootLen = pathToRootInArchive == null ? 0 : pathToRootInArchive.length();
                StringBuilder rn = new StringBuilder(pathToRootLen + folderName.length() + 1 + baseName.length());
                if (pathToRootInArchive != null) {
                    rn.append(pathToRootInArchive);
                }
                rn.append(folderName);
                rn.append('/');
                rn.append(baseName);
                this.resName = rn.toString();
            }
        }

        @Override
        public OutputStream openOutputStream() throws IOException {
            throw new UnsupportedOperationException();
        }

        @Override
        public Reader openReader(boolean b) throws IOException {
            if (this.getKind() == JavaFileObject.Kind.CLASS) {
                throw new UnsupportedOperationException();
            }
            return new InputStreamReader(this.openInputStream(), encodingName);
        }

        @Override
        public Writer openWriter() throws IOException {
            throw new UnsupportedOperationException();
        }

        @Override
        public long getLastModified() {
            return this.mtime;
        }

        @Override
        public boolean delete() {
            throw new UnsupportedOperationException();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public CharBuffer getCharContent(boolean ignoreEncodingErrors) throws IOException {
            Reader r = this.openReader(ignoreEncodingErrors);
            try {
                int rv;
                int red = 0;
                int len = (int)this.getSize();
                char[] result = new char[len + 1];
                while ((rv = r.read(result, red, len - red)) > 0 && (red += rv) < len) {
                }
                int j = 0;
                for (int i2 = 0; i2 < red; ++i2) {
                    if (result[i2] == '\r') {
                        if (i2 + 1 < red && result[i2 + 1] == '\n') continue;
                        result[j++] = 10;
                        continue;
                    }
                    result[j++] = result[i2];
                }
                result[j] = 10;
                CharBuffer i2 = CharBuffer.wrap(result, 0, j);
                return i2;
            }
            finally {
                r.close();
            }
        }

        @Override
        public final URI toUri() {
            URI zdirURI = this.getArchiveURI();
            try {
                return new URI("jar:" + zdirURI.toString() + "!/" + this.resName);
            }
            catch (URISyntaxException e) {
                StringBuilder sb = new StringBuilder();
                String[] elements = this.resName.split("/");
                try {
                    for (int i = 0; i < elements.length; ++i) {
                        String element = elements[i];
                        element = URLEncoder.encode(element, "UTF-8");
                        element = element.replace("+", "%20");
                        sb.append(element);
                        if (i >= elements.length - 1) continue;
                        sb.append('/');
                    }
                    return new URI("jar:" + zdirURI.toString() + "!/" + sb.toString());
                }
                catch (UnsupportedEncodingException e2) {
                    throw new IllegalStateException(e2);
                }
                catch (URISyntaxException e2) {
                    throw new IllegalStateException(e2);
                }
            }
        }

        public int hashCode() {
            return this.resName.hashCode();
        }

        public boolean equals(Object other) {
            if (!(other instanceof ZipFileBase)) {
                return false;
            }
            ZipFileBase o = (ZipFileBase)other;
            return this.getArchiveURI().equals(o.getArchiveURI()) && this.resName.equals(o.resName);
        }

        protected abstract URI getArchiveURI();

        protected abstract long getSize() throws IOException;
    }

    @ClientCodeWrapper.Trusted
    private static final class NewFromTemplateFileObject
    extends FileBase {
        public NewFromTemplateFileObject(File f, String packageName, String baseName, JavaFileFilterImplementation filter, Charset encoding) {
            super(f, packageName, baseName, filter, encoding);
        }

        @Override
        public InputStream openInputStream() throws IOException {
            if (this.f.exists()) {
                return super.openInputStream();
            }
            return new ByteArrayInputStream(new byte[0]);
        }

        @Override
        public Reader openReader(boolean b) throws IOException {
            if (this.f.exists()) {
                return super.openReader(b);
            }
            return new StringReader("");
        }

        @Override
        public OutputStream openOutputStream() throws IOException {
            if (!this.f.exists()) {
                this.create();
            }
            return super.openOutputStream();
        }

        @Override
        public Writer openWriter() throws IOException {
            if (!this.f.exists()) {
                this.create();
            }
            return super.openWriter();
        }

        @Override
        public CharSequence getCharContent(boolean ignoreEncodingErrors) throws IOException {
            if (this.f.exists()) {
                return super.getCharContent(ignoreEncodingErrors);
            }
            return "";
        }

        private void create() throws IOException {
            File parent = this.f.getParentFile();
            org.openide.filesystems.FileObject parentFo = FileUtil.createFolder((File)parent);
            assert (parentFo != null);
            DataFolder target = DataFolder.findFolder((org.openide.filesystems.FileObject)parentFo);
            org.openide.filesystems.FileObject template = FileUtil.getConfigFile((String)"Templates/Classes/Empty.java");
            if (template == null) {
                FileUtil.createData((org.openide.filesystems.FileObject)parentFo, (String)this.f.getName());
                return;
            }
            DataObject templateDobj = DataObject.find((org.openide.filesystems.FileObject)template);
            String simpleName = FileObjects.stripExtension(this.f.getName());
            DataObject newDobj = templateDobj.createFromTemplate(target, simpleName);
            assert (newDobj != null);
        }
    }

    @ClientCodeWrapper.Trusted
    public static class FileBase
    extends Base
    implements PrefetchableJavaFileObject {
        private static final boolean isWindows = Utilities.isWindows();
        protected final File f;
        protected final JavaFileFilterImplementation filter;
        protected final Charset encoding;
        URI uriCache;
        private volatile CharSequence data;

        protected FileBase(File file, String pkgName, String name, JavaFileFilterImplementation filter, Charset encoding) {
            super(pkgName, name);
            assert (file != null);
            this.f = file;
            this.filter = filter;
            this.encoding = encoding;
        }

        public File getFile() {
            return this.f;
        }

        @Override
        public InputStream openInputStream() throws IOException {
            return new BufferedInputStream(new FileInputStream(this.f));
        }

        @Override
        public Reader openReader(boolean b) throws IOException {
            return this.encoding == null ? new InputStreamReader(this.openInputStream()) : new InputStreamReader(this.openInputStream(), this.encoding);
        }

        @Override
        public OutputStream openOutputStream() throws IOException {
            File parent = this.f.getParentFile();
            if (!parent.exists()) {
                parent.mkdirs();
            }
            return new FileOutputStream(this.f);
        }

        @Override
        public Writer openWriter() throws IOException {
            if (this.encoding != null) {
                return new OutputStreamWriter(this.openOutputStream(), this.encoding);
            }
            return new OutputStreamWriter(this.openOutputStream());
        }

        @Override
        public boolean isNameCompatible(String simplename, JavaFileObject.Kind kind) {
            return isWindows ? this.kind == kind && this.nameWithoutExt.equalsIgnoreCase(simplename) : super.isNameCompatible(simplename, kind);
        }

        @Override
        public URI toUri() {
            if (this.uriCache == null) {
                this.uriCache = Utilities.toURI((File)this.f);
            }
            return this.uriCache;
        }

        @Override
        public long getLastModified() {
            return this.f.lastModified();
        }

        @Override
        public boolean delete() {
            return this.f.delete();
        }

        @Override
        public CharSequence getCharContent(boolean ignoreEncodingErrors) throws IOException {
            CharSequence res = this.data;
            if (res == null) {
                res = this.getCharContentImpl(ignoreEncodingErrors);
            }
            return res;
        }

        public boolean equals(Object other) {
            if (!(other instanceof FileBase)) {
                return false;
            }
            FileBase o = (FileBase)other;
            return this.f.equals(o.f);
        }

        public int hashCode() {
            return this.f.hashCode();
        }

        @Override
        public int prefetch() throws IOException {
            CharSequence chc;
            this.data = chc = this.getCharContentImpl(true);
            return chc.length();
        }

        @Override
        public int dispose() {
            CharSequence _data = this.data;
            if (_data != null) {
                this.data = null;
                return _data.length();
            }
            return 0;
        }

        private CharSequence getCharContentImpl(boolean ignoreEncodingErrors) throws IOException {
            return FileObjects.getCharContent(this.openInputStream(), this.encoding, this.filter, this.f.length(), ignoreEncodingErrors);
        }
    }

    public static abstract class Base
    implements InferableJavaFileObject {
        protected final JavaFileObject.Kind kind;
        protected final String pkgName;
        protected final String nameWithoutExt;
        protected final String ext;

        protected Base(String pkgName, String name) {
            assert (pkgName != null);
            assert (name != null);
            this.pkgName = pkgName;
            String[] res = Base.getNameExtPair(name);
            this.nameWithoutExt = res[0];
            this.ext = res[1];
            this.kind = FileObjects.getKind(this.ext);
        }

        @Override
        public JavaFileObject.Kind getKind() {
            return this.kind;
        }

        @Override
        public boolean isNameCompatible(String simplename, JavaFileObject.Kind k) {
            return this.kind == k && this.nameWithoutExt.equals(simplename);
        }

        @Override
        public NestingKind getNestingKind() {
            return null;
        }

        @Override
        public Modifier getAccessLevel() {
            return null;
        }

        public String toString() {
            return this.toUri().toString();
        }

        @NonNull
        public String getPackage() {
            return this.pkgName;
        }

        @NonNull
        public String getNameWithoutExtension() {
            return this.nameWithoutExt;
        }

        @NonNull
        @Override
        public String getName() {
            StringBuilder sb = new StringBuilder(this.nameWithoutExt);
            sb.append('.');
            sb.append(this.ext);
            return sb.toString();
        }

        @NonNull
        public String getExt() {
            return this.ext;
        }

        @NonNull
        public String getPath() {
            String res = FileObjects.convertPackage2Folder(this.inferBinaryName());
            if (!this.ext.isEmpty()) {
                StringBuilder sb = new StringBuilder(res);
                sb.append('.');
                sb.append(this.ext);
                res = sb.toString();
            }
            return res;
        }

        public boolean isVirtual() {
            return false;
        }

        @NonNull
        @Override
        public final String inferBinaryName() {
            StringBuilder sb = new StringBuilder();
            sb.append(this.pkgName);
            if (sb.length() > 0) {
                sb.append('.');
            }
            sb.append(this.nameWithoutExt);
            return sb.toString();
        }

        private static String[] getNameExtPair(String name) {
            String ext;
            String namenx;
            int index = name.lastIndexOf(46);
            if (index <= 0) {
                namenx = name;
                ext = "";
            } else {
                namenx = name.substring(0, index);
                ext = index == name.length() - 1 ? "" : name.substring(index + 1);
            }
            return new String[]{namenx, ext};
        }
    }

}

