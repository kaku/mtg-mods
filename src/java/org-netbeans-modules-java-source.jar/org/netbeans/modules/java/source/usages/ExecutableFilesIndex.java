/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.WeakSet
 */
package org.netbeans.modules.java.source.usages;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.openide.util.Exceptions;
import org.openide.util.WeakSet;

public class ExecutableFilesIndex {
    public static final ExecutableFilesIndex DEFAULT = new ExecutableFilesIndex();
    private URL lastLoadURL;
    private Set<String> mainSources;
    private Map<ChangeListener, String> listener2File = new WeakHashMap<ChangeListener, String>();
    private Map<String, Set<ChangeListener>> file2Listener = new WeakHashMap<String, Set<ChangeListener>>();

    public synchronized boolean isMainClass(URL root, URL source) {
        this.ensureLoad(root);
        return this.mainSources.contains(source.toExternalForm());
    }

    public synchronized Iterable<? extends URL> getMainClasses(URL root) {
        this.ensureLoad(root);
        ArrayList<URL> result = new ArrayList<URL>(this.mainSources.size());
        for (String surl : this.mainSources) {
            try {
                result.add(new URL(surl));
            }
            catch (MalformedURLException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return result;
    }

    public synchronized void setMainClass(URL root, URL source, boolean value) {
        this.ensureLoad(root);
        String ext = source.toExternalForm();
        boolean changed = value ? this.mainSources.add(ext) : this.mainSources.remove(ext);
        if (changed) {
            this.save(root);
            Set<ChangeListener> ls = this.file2Listener.get(ext);
            if (ls != null) {
                ChangeEvent e = null;
                for (ChangeListener l : ls) {
                    if (e == null) {
                        e = new ChangeEvent(source);
                    }
                    l.stateChanged(e);
                }
            }
        }
    }

    public synchronized void addChangeListener(URL source, ChangeListener l) {
        String ext = source.toExternalForm();
        this.listener2File.put(l, ext);
        WeakSet ls = this.file2Listener.get(ext);
        if (ls == null) {
            ls = new WeakSet();
            this.file2Listener.put(ext, (Set<ChangeListener>)ls);
        }
        ls.add((ChangeListener)l);
        this.file2Listener.put(ext, (Set<ChangeListener>)ls);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void ensureLoad(URL root) {
        if (this.lastLoadURL != null && this.lastLoadURL.equals(root)) {
            return;
        }
        try {
            this.mainSources = ExecutableFilesIndex.unwrap(JavaIndex.getAttribute(root, "executable-files", ""));
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
            this.mainSources = new HashSet<String>();
        }
        finally {
            this.lastLoadURL = root;
        }
    }

    private void save(URL root) {
        try {
            JavaIndex.setAttribute(root, "executable-files", ExecutableFilesIndex.wrap(this.mainSources));
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    static Set<String> unwrap(String value) {
        if (value.length() == 0) {
            return new HashSet<String>();
        }
        String[] executableFiles = value.split("::");
        HashSet<String> result = new HashSet<String>();
        for (String file : executableFiles) {
            file = file.replaceAll("\\\\d", ":");
            file = file.replaceAll("\\\\\\\\", "\\\\");
            result.add(file);
        }
        return result;
    }

    static String wrap(Set<String> values) {
        StringBuilder attribute = new StringBuilder();
        boolean first = true;
        Iterator<String> i$ = values.iterator();
        while (i$.hasNext()) {
            String s = i$.next();
            if (!first) {
                attribute.append("::");
            }
            s = s.replaceAll("\\\\", "\\\\\\\\");
            s = s.replaceAll(":", "\\\\d");
            attribute.append(s);
            first = false;
        }
        return attribute.toString();
    }
}

