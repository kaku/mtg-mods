/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.analysis.Analyzer
 *  org.apache.lucene.analysis.CharTokenizer
 *  org.apache.lucene.analysis.KeywordAnalyzer
 *  org.apache.lucene.analysis.PerFieldAnalyzerWrapper
 *  org.apache.lucene.analysis.TokenStream
 *  org.apache.lucene.analysis.WhitespaceAnalyzer
 *  org.apache.lucene.document.Document
 *  org.apache.lucene.document.Field
 *  org.apache.lucene.document.Field$Index
 *  org.apache.lucene.document.Field$Store
 *  org.apache.lucene.document.FieldSelector
 *  org.apache.lucene.document.Fieldable
 *  org.apache.lucene.index.Term
 *  org.apache.lucene.search.BooleanClause
 *  org.apache.lucene.search.BooleanClause$Occur
 *  org.apache.lucene.search.BooleanQuery
 *  org.apache.lucene.search.PrefixQuery
 *  org.apache.lucene.search.Query
 *  org.apache.lucene.search.TermQuery
 *  org.apache.lucene.search.WildcardQuery
 *  org.netbeans.modules.parsing.lucene.support.Convertor
 *  org.netbeans.modules.parsing.lucene.support.Queries
 *  org.netbeans.modules.parsing.lucene.support.Queries$QueryKind
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.Pair
 */
package org.netbeans.modules.java.source.usages;

import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.util.List;
import java.util.Set;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.CharTokenizer;
import org.apache.lucene.analysis.KeywordAnalyzer;
import org.apache.lucene.analysis.PerFieldAnalyzerWrapper;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldSelector;
import org.apache.lucene.document.Fieldable;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.PrefixQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.WildcardQuery;
import org.netbeans.api.java.source.ClassIndex;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.modules.java.source.ElementHandleAccessor;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;
import org.netbeans.modules.java.source.usages.ClassIndexManager;
import org.netbeans.modules.parsing.lucene.support.Convertor;
import org.netbeans.modules.parsing.lucene.support.Queries;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.Pair;

public class DocumentUtil {
    static final String FIELD_PACKAGE_NAME = "packageName";
    static final String FIELD_SIMPLE_NAME = "simpleName";
    static final String FIELD_CASE_INSENSITIVE_NAME = "ciName";
    static final String FIELD_IDENTS = "ids";
    static final String FIELD_FEATURE_IDENTS = "fids";
    static final String FIELD_CASE_INSENSITIVE_FEATURE_IDENTS = "cifids";
    private static final String FIELD_BINARY_NAME = "binaryName";
    private static final String FIELD_SOURCE = "source";
    private static final String FIELD_REFERENCES = "references";
    private static final char PKG_SEPARATOR = '.';
    private static final char NO = '-';
    private static final char YES = '+';
    private static final char WILDCARD_QUERY_WILDCARD = '?';
    private static final char REGEX_QUERY_WILDCARD = '.';
    private static final char EK_CLASS = 'C';
    private static final char EK_INTERFACE = 'I';
    private static final char EK_ENUM = 'E';
    private static final char EK_ANNOTATION = 'A';
    private static final int SIZE = ClassIndexImpl.UsageType.values().length;

    private DocumentUtil() {
    }

    public static Analyzer createAnalyzer() {
        PerFieldAnalyzerWrapper analyzer = new PerFieldAnalyzerWrapper((Analyzer)new KeywordAnalyzer());
        analyzer.addAnalyzer("ids", (Analyzer)new WhitespaceAnalyzer());
        analyzer.addAnalyzer("fids", (Analyzer)new WhitespaceAnalyzer());
        analyzer.addAnalyzer("cifids", (Analyzer)new LCWhitespaceAnalyzer());
        return analyzer;
    }

    public static /* varargs */ Convertor<Document, FileObject> fileObjectConvertor(FileObject ... roots) {
        assert (roots != null);
        return new FileObjectConvertor(roots);
    }

    public static Convertor<Document, ElementHandle<TypeElement>> elementHandleConvertor() {
        return new ElementHandleConvertor();
    }

    public static Convertor<Document, String> binaryNameConvertor() {
        return new BinaryNameConvertor();
    }

    static Convertor<Document, String> sourceNameConvertor() {
        return new SourceNameConvertor();
    }

    static Convertor<Pair<Pair<String, String>, Object[]>, Document> documentConvertor() {
        return new DocumentConvertor();
    }

    static Convertor<Pair<String, String>, Query> queryClassWithEncConvertor(boolean fileBased) {
        return new QueryClassesWithEncConvertor(fileBased);
    }

    static Convertor<Pair<String, String>, Query> queryClassConvertor() {
        return new QueryClassConvertor();
    }

    static String getBinaryName(Document doc) {
        return DocumentUtil.getBinaryName(doc, null);
    }

    static String getBinaryName(Document doc, ElementKind[] kind) {
        assert (doc != null);
        Field pkgField = doc.getField("packageName");
        Field snField = doc.getField("binaryName");
        if (snField == null) {
            return null;
        }
        String tmp = snField.stringValue();
        String snName = tmp.substring(0, tmp.length() - 1);
        if (snName.length() == 0) {
            return null;
        }
        if (kind != null) {
            assert (kind.length == 1);
            kind[0] = DocumentUtil.decodeKind(tmp.charAt(tmp.length() - 1));
        }
        if (pkgField == null) {
            return snName;
        }
        String pkg = pkgField.stringValue();
        if (pkg.length() == 0) {
            return snName;
        }
        return pkg + '.' + snName;
    }

    public static String getSimpleBinaryName(Document doc) {
        assert (doc != null);
        Field field = doc.getField("binaryName");
        if (field == null) {
            return null;
        }
        return field.stringValue();
    }

    static String getPackageName(Document doc) {
        assert (doc != null);
        Field field = doc.getField("packageName");
        return field == null ? null : field.stringValue();
    }

    static Query binaryNameQuery(String resourceName) {
        String sName;
        String pkgName;
        BooleanQuery query = new BooleanQuery();
        int index = resourceName.lastIndexOf(46);
        if (index < 0) {
            pkgName = "";
            sName = resourceName;
        } else {
            pkgName = resourceName.substring(0, index);
            sName = resourceName.substring(index + 1);
        }
        sName = sName + '?';
        query.add((Query)new TermQuery(new Term("packageName", pkgName)), BooleanClause.Occur.MUST);
        query.add((Query)new WildcardQuery(new Term("binaryName", sName)), BooleanClause.Occur.MUST);
        return query;
    }

    static Term referencesTerm(String resourceName, Set<? extends ClassIndexImpl.UsageType> usageType, boolean javaRegEx) {
        assert (resourceName != null);
        if (!resourceName.isEmpty()) {
            char wildcard;
            char[] arrc;
            char c = wildcard = javaRegEx ? '.' : '?';
            if (javaRegEx) {
                char[] arrc2 = new char[2];
                arrc2[0] = 92;
                arrc = arrc2;
                arrc2[1] = 43;
            } else {
                char[] arrc3 = new char[1];
                arrc = arrc3;
                arrc3[0] = 43;
            }
            char[] yes = arrc;
            if (usageType != null) {
                resourceName = DocumentUtil.encodeUsage(resourceName, usageType, wildcard, yes).toString();
            } else {
                StringBuilder sb = new StringBuilder(resourceName);
                for (int i = 0; i < SIZE; ++i) {
                    sb.append(wildcard);
                }
                resourceName = sb.toString();
            }
        }
        return new Term("references", resourceName);
    }

    private static Document createDocument(String binaryName, List<String> references, String featureIdents, String idents, String source) {
        String fileName;
        String pkgName;
        assert (binaryName != null);
        assert (references != null);
        int index = binaryName.lastIndexOf(46);
        if (index < 0) {
            fileName = binaryName;
            pkgName = "";
        } else {
            fileName = binaryName.substring(index + 1);
            pkgName = binaryName.substring(0, index);
        }
        assert (fileName.length() > 1);
        index = fileName.lastIndexOf(36);
        String simpleName = index < 0 ? fileName.substring(0, fileName.length() - 1) : fileName.substring(index + 1, fileName.length() - 1);
        String caseInsensitiveName = simpleName.toLowerCase();
        Document doc = new Document();
        Field field = new Field("binaryName", fileName, Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS);
        doc.add((Fieldable)field);
        field = new Field("packageName", pkgName, Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS);
        doc.add((Fieldable)field);
        field = new Field("simpleName", simpleName, Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS);
        doc.add((Fieldable)field);
        field = new Field("ciName", caseInsensitiveName, Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS);
        doc.add((Fieldable)field);
        for (String reference : references) {
            field = new Field("references", reference, Field.Store.NO, Field.Index.NOT_ANALYZED_NO_NORMS);
            doc.add((Fieldable)field);
        }
        if (featureIdents != null) {
            field = new Field("fids", featureIdents, Field.Store.NO, Field.Index.ANALYZED_NO_NORMS);
            doc.add((Fieldable)field);
            field = new Field("cifids", featureIdents, Field.Store.NO, Field.Index.ANALYZED_NO_NORMS);
            doc.add((Fieldable)field);
        }
        if (idents != null) {
            field = new Field("ids", idents, Field.Store.NO, Field.Index.ANALYZED_NO_NORMS);
            doc.add((Fieldable)field);
        }
        if (source != null) {
            field = new Field("source", source, Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS);
            doc.add((Fieldable)field);
        }
        return doc;
    }

    static String encodeUsage(String className, Set<ClassIndexImpl.UsageType> usageTypes) {
        return DocumentUtil.encodeUsage(className, usageTypes, '-', new char[]{'+'}).toString();
    }

    private static StringBuilder encodeUsage(String className, Set<? extends ClassIndexImpl.UsageType> usageTypes, char fill, char[] yes) {
        assert (className != null);
        assert (usageTypes != null);
        StringBuilder builder = new StringBuilder();
        builder.append(className);
        for (ClassIndexImpl.UsageType ut : ClassIndexImpl.UsageType.values()) {
            if (usageTypes.contains((Object)ut)) {
                builder.append(yes);
                continue;
            }
            builder.append(fill);
        }
        return builder;
    }

    static String decodeUsage(String rawUsage, Set<ClassIndexImpl.UsageType> usageTypes) {
        assert (rawUsage != null);
        assert (usageTypes != null);
        assert (usageTypes.isEmpty());
        int rawUsageLen = rawUsage.length();
        assert (rawUsageLen > SIZE);
        int index = rawUsageLen - SIZE;
        String className = rawUsage.substring(0, index);
        String map = rawUsage.substring(index);
        ClassIndexImpl.UsageType[] values = ClassIndexImpl.UsageType.values();
        for (int i = 0; i < values.length; ++i) {
            if (map.charAt(i) != '+') continue;
            usageTypes.add(values[i]);
        }
        return className;
    }

    static ElementKind decodeKind(char kind) {
        switch (kind) {
            case 'C': {
                return ElementKind.CLASS;
            }
            case 'I': {
                return ElementKind.INTERFACE;
            }
            case 'E': {
                return ElementKind.ENUM;
            }
            case 'A': {
                return ElementKind.ANNOTATION_TYPE;
            }
        }
        throw new IllegalArgumentException();
    }

    static char encodeKind(ElementKind kind) {
        switch (kind) {
            case CLASS: {
                return 'C';
            }
            case INTERFACE: {
                return 'I';
            }
            case ENUM: {
                return 'E';
            }
            case ANNOTATION_TYPE: {
                return 'A';
            }
        }
        throw new IllegalArgumentException();
    }

    static FieldSelector declaredTypesFieldSelector() {
        return Queries.createFieldSelector((String[])new String[]{"packageName", "binaryName"});
    }

    static FieldSelector sourceNameFieldSelector() {
        return Queries.createFieldSelector((String[])new String[]{"source"});
    }

    static Queries.QueryKind translateQueryKind(ClassIndex.NameKind kind) {
        switch (kind) {
            case SIMPLE_NAME: {
                return Queries.QueryKind.EXACT;
            }
            case PREFIX: {
                return Queries.QueryKind.PREFIX;
            }
            case CASE_INSENSITIVE_PREFIX: {
                return Queries.QueryKind.CASE_INSENSITIVE_PREFIX;
            }
            case CAMEL_CASE: {
                return Queries.QueryKind.CAMEL_CASE;
            }
            case CAMEL_CASE_INSENSITIVE: {
                return Queries.QueryKind.CASE_INSENSITIVE_CAMEL_CASE;
            }
            case REGEXP: {
                return Queries.QueryKind.REGEXP;
            }
            case CASE_INSENSITIVE_REGEXP: {
                return Queries.QueryKind.CASE_INSENSITIVE_REGEXP;
            }
        }
        throw new IllegalArgumentException();
    }

    private static class QueryClassConvertor
    implements Convertor<Pair<String, String>, Query> {
        private QueryClassConvertor() {
        }

        public Query convert(Pair<String, String> p) {
            return QueryClassConvertor.binaryNameSourceNamePairQuery(p);
        }

        private static Query binaryNameSourceNamePairQuery(Pair<String, String> binaryNameSourceNamePair) {
            assert (binaryNameSourceNamePair != null);
            String binaryName = (String)binaryNameSourceNamePair.first();
            String sourceName = (String)binaryNameSourceNamePair.second();
            Query query = DocumentUtil.binaryNameQuery(binaryName);
            if (sourceName != null) {
                assert (query instanceof BooleanQuery);
                BooleanQuery bq = (BooleanQuery)query;
                bq.add((Query)new TermQuery(new Term("source", sourceName)), BooleanClause.Occur.MUST);
            }
            return query;
        }
    }

    private static class QueryClassesWithEncConvertor
    implements Convertor<Pair<String, String>, Query> {
        private final boolean fileBased;

        private QueryClassesWithEncConvertor(boolean fileBased) {
            this.fileBased = fileBased;
        }

        public Query convert(Pair<String, String> p) {
            String resourceName = (String)p.first();
            String sourceName = (String)p.second();
            return this.fileBased ? QueryClassesWithEncConvertor.createClassesInFileQuery(resourceName, sourceName) : QueryClassesWithEncConvertor.createClassWithEnclosedQuery(resourceName, sourceName);
        }

        private static Query createClassWithEnclosedQuery(String resourceName, String sourceName) {
            BooleanQuery query = QueryClassesWithEncConvertor.createFQNQuery(resourceName);
            if (sourceName != null) {
                query.add((Query)new TermQuery(new Term("source", sourceName)), BooleanClause.Occur.MUST);
            }
            return query;
        }

        private static Query createClassesInFileQuery(String resourceName, String sourceName) {
            if (sourceName != null) {
                BooleanQuery result = new BooleanQuery();
                result.add((Query)QueryClassesWithEncConvertor.createFQNQuery(FileObjects.convertFolder2Package(FileObjects.stripExtension(sourceName))), BooleanClause.Occur.SHOULD);
                result.add((Query)new TermQuery(new Term("source", sourceName)), BooleanClause.Occur.SHOULD);
                return result;
            }
            BooleanQuery result = new BooleanQuery();
            result.add((Query)QueryClassesWithEncConvertor.createFQNQuery(resourceName), BooleanClause.Occur.SHOULD);
            result.add((Query)new TermQuery(new Term("source", FileObjects.convertPackage2Folder(resourceName) + '.' + "java")), BooleanClause.Occur.SHOULD);
            return result;
        }

        private static BooleanQuery createFQNQuery(String resourceName) {
            String sName;
            String pkgName;
            int index = resourceName.lastIndexOf(46);
            if (index < 0) {
                pkgName = "";
                sName = resourceName;
            } else {
                pkgName = resourceName.substring(0, index);
                sName = resourceName.substring(index + 1);
            }
            BooleanQuery snQuery = new BooleanQuery();
            snQuery.add((Query)new WildcardQuery(new Term("binaryName", sName + '?')), BooleanClause.Occur.SHOULD);
            snQuery.add((Query)new PrefixQuery(new Term("binaryName", sName + '$')), BooleanClause.Occur.SHOULD);
            if (pkgName.length() == 0) {
                return snQuery;
            }
            BooleanQuery fqnQuery = new BooleanQuery();
            fqnQuery.add((Query)new TermQuery(new Term("packageName", pkgName)), BooleanClause.Occur.MUST);
            fqnQuery.add((Query)snQuery, BooleanClause.Occur.MUST);
            return fqnQuery;
        }
    }

    private static class DocumentConvertor
    implements Convertor<Pair<Pair<String, String>, Object[]>, Document> {
        private DocumentConvertor() {
        }

        public Document convert(Pair<Pair<String, String>, Object[]> entry) {
            Pair pair = (Pair)entry.first();
            String cn = (String)pair.first();
            String srcName = (String)pair.second();
            Object[] data = (Object[])entry.second();
            List cr = (List)data[0];
            String fids = (String)data[1];
            String ids = (String)data[2];
            return DocumentUtil.createDocument(cn, cr, fids, ids, srcName);
        }
    }

    private static class SourceNameConvertor
    implements Convertor<Document, String> {
        private SourceNameConvertor() {
        }

        public String convert(Document doc) {
            Field field = doc.getField("source");
            return field == null ? null : field.stringValue();
        }
    }

    private static class BinaryNameConvertor
    implements Convertor<Document, String> {
        private BinaryNameConvertor() {
        }

        public String convert(Document doc) {
            return DocumentUtil.getBinaryName(doc, null);
        }
    }

    private static class ElementHandleConvertor
    implements Convertor<Document, ElementHandle<TypeElement>> {
        private final ElementKind[] kindHolder = new ElementKind[1];

        private ElementHandleConvertor() {
        }

        public ElementHandle<TypeElement> convert(Document doc) {
            String binaryName = DocumentUtil.getBinaryName(doc, this.kindHolder);
            return binaryName == null ? null : this.convert(this.kindHolder[0], binaryName);
        }

        private ElementHandle<TypeElement> convert(ElementKind kind, String value) {
            return ElementHandleAccessor.getInstance().create(kind, value);
        }
    }

    private static class FileObjectConvertor
    implements Convertor<Document, FileObject> {
        private FileObject[] roots;

        private /* varargs */ FileObjectConvertor(FileObject ... roots) {
            this.roots = roots;
        }

        public FileObject convert(Document doc) {
            String binaryName = DocumentUtil.getBinaryName(doc, null);
            return binaryName == null ? null : this.convert(binaryName);
        }

        private FileObject convert(String value) {
            for (FileObject root : this.roots) {
                FileObject result = FileObjectConvertor.resolveFile(root, value);
                if (result == null) continue;
                return result;
            }
            ClassIndexManager cim = ClassIndexManager.getDefault();
            for (FileObject root2 : this.roots) {
                try {
                    FileObject result;
                    String sourceName;
                    ClassIndexImpl impl = cim.getUsagesQuery(root2.getURL(), true);
                    if (impl == null || (sourceName = impl.getSourceName(value)) == null || (result = root2.getFileObject(sourceName)) == null) continue;
                    return result;
                }
                catch (IOException e) {
                    Exceptions.printStackTrace((Throwable)e);
                    continue;
                }
                catch (InterruptedException ie) {
                    // empty catch block
                }
            }
            return null;
        }

        private static FileObject resolveFile(FileObject root, String classBinaryName) {
            FileObject folder;
            String name;
            assert (classBinaryName != null);
            int index = (classBinaryName = classBinaryName.replace('.', '/')).lastIndexOf(47);
            if (index < 0) {
                folder = root;
                name = classBinaryName;
            } else {
                assert (index > 0);
                assert (index < classBinaryName.length() - 1);
                folder = root.getFileObject(classBinaryName.substring(0, index));
                name = classBinaryName.substring(index + 1);
            }
            if (folder == null) {
                return null;
            }
            index = name.indexOf(36);
            if (index > 0) {
                name = name.substring(0, index);
            }
            for (FileObject child : folder.getChildren()) {
                if (!"java".equalsIgnoreCase(child.getExt()) || !name.equals(child.getName())) continue;
                return child;
            }
            return null;
        }
    }

    static final class LCWhitespaceAnalyzer
    extends Analyzer {
        LCWhitespaceAnalyzer() {
        }

        public TokenStream tokenStream(String fieldName, Reader reader) {
            return new LCWhitespaceTokenizer(reader);
        }
    }

    private static class LCWhitespaceTokenizer
    extends CharTokenizer {
        LCWhitespaceTokenizer(Reader r) {
            super(r);
        }

        protected boolean isTokenChar(int c) {
            return !Character.isWhitespace(c);
        }

        protected char normalize(char c) {
            return Character.toLowerCase(c);
        }
    }

}

