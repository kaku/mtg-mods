/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.PositionRef
 */
package org.netbeans.modules.java.source.save;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.api.java.source.PositionConverter;
import org.netbeans.modules.java.source.JavaSourceAccessor;
import org.netbeans.modules.java.source.save.CasualDiff;
import org.netbeans.modules.java.source.save.DiffFacility;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.PositionRef;

public class DiffUtilities {
    private static final Logger LOG = Logger.getLogger(DiffUtilities.class.getName());

    public static List<ModificationResult.Difference> diff2ModificationResultDifference(FileObject fo, PositionConverter converter, Map<Integer, String> userInfo, String originalCode, String newCode) throws IOException, BadLocationException {
        return DiffUtilities.diff2ModificationResultDifference(fo, converter, userInfo, originalCode, DiffUtilities.diff(originalCode, newCode, 0));
    }

    public static List<CasualDiff.Diff> diff(String origContent, String newContent, int offset) {
        return DiffUtilities.diff(origContent, newContent, offset, null, offset);
    }

    public static List<CasualDiff.Diff> diff(String origContent, String newContent, int offset, int[] sections, int lineStart) {
        ArrayList<CasualDiff.Diff> diffs = new ArrayList<CasualDiff.Diff>();
        new DiffFacility(diffs).withSections(sections, lineStart).makeListMatch(origContent, newContent, offset);
        return diffs;
    }

    public static List<ModificationResult.Difference> diff2ModificationResultDifference(FileObject fo, PositionConverter converter, Map<Integer, String> userInfo, String content, List<CasualDiff.Diff> diffs) throws IOException, BadLocationException {
        Collections.sort(diffs, new Comparator<CasualDiff.Diff>(){

            @Override
            public int compare(CasualDiff.Diff o1, CasualDiff.Diff o2) {
                return o1.getPos() - o2.getPos();
            }
        });
        Rewriter out = new Rewriter(fo, converter, userInfo);
        char[] buf = content.toCharArray();
        block4 : for (CasualDiff.Diff d : diffs) {
            switch (d.type) {
                case INSERT: {
                    out.copyTo(d.getPos());
                    out.writeTo(d.getText());
                    continue block4;
                }
                case DELETE: {
                    out.copyTo(d.getPos());
                    out.skipThrough(buf, d.getEnd());
                    continue block4;
                }
            }
            throw new AssertionError((Object)("unknown CasualDiff type: " + (Object)((Object)d.type)));
        }
        return out.diffs;
    }

    private static class Rewriter {
        private int offset = 0;
        private CloneableEditorSupport ces;
        private PositionConverter converter;
        public List<ModificationResult.Difference> diffs = new LinkedList<ModificationResult.Difference>();
        private Map<Integer, String> userInfo;

        public Rewriter(FileObject fo, PositionConverter converter, Map<Integer, String> userInfo) throws IOException {
            this.converter = converter;
            this.userInfo = userInfo;
            if (fo != null) {
                DataObject dObj = DataObject.find((FileObject)fo);
                CloneableEditorSupport cloneableEditorSupport = this.ces = dObj != null ? (CloneableEditorSupport)dObj.getCookie(EditorCookie.class) : null;
            }
            if (this.ces == null) {
                throw new IOException("Could not find CloneableEditorSupport for " + FileUtil.getFileDisplayName((FileObject)fo));
            }
        }

        public void writeTo(String s) throws IOException, BadLocationException {
            ModificationResult.Difference diff;
            ModificationResult.Difference difference = diff = this.diffs.size() > 0 ? this.diffs.get(this.diffs.size() - 1) : null;
            if (diff != null && diff.getKind() == ModificationResult.Difference.Kind.REMOVE && diff.getEndPosition().getOffset() == this.offset) {
                this.diffs.remove(this.diffs.size() - 1);
                this.diffs.add(JavaSourceAccessor.getINSTANCE().createDifference(ModificationResult.Difference.Kind.CHANGE, diff.getStartPosition(), diff.getEndPosition(), diff.getOldText(), s, diff.getDescription()));
            } else {
                int off;
                int n = off = this.converter != null ? this.converter.getOriginalPosition(this.offset) : this.offset;
                if (off >= 0) {
                    StyledDocument d = this.ces.getDocument();
                    PositionRef endRef = this.ces.createPositionRef(off, Position.Bias.Backward);
                    if (d != null) {
                        int l = d.getLength();
                        if (endRef.getOffset() > l) {
                            LOG.log(Level.WARNING, "Invalid diff position: {0}, doc.length: {1}", new Object[]{off, l});
                        }
                    }
                    this.diffs.add(JavaSourceAccessor.getINSTANCE().createDifference(ModificationResult.Difference.Kind.INSERT, this.ces.createPositionRef(off, Position.Bias.Forward), endRef, null, s, this.userInfo.get(this.offset)));
                }
            }
        }

        public void skipThrough(char[] in, int pos) throws IOException, BadLocationException {
            ModificationResult.Difference diff;
            String origText = new String(in, this.offset, pos - this.offset);
            ModificationResult.Difference difference = diff = this.diffs.size() > 0 ? this.diffs.get(this.diffs.size() - 1) : null;
            if (diff != null && diff.getKind() == ModificationResult.Difference.Kind.INSERT && diff.getStartPosition().getOffset() == this.offset) {
                this.diffs.remove(this.diffs.size() - 1);
                this.diffs.add(JavaSourceAccessor.getINSTANCE().createDifference(ModificationResult.Difference.Kind.CHANGE, diff.getStartPosition(), diff.getEndPosition(), origText, diff.getNewText(), diff.getDescription()));
            } else {
                int off;
                int n = off = this.converter != null ? this.converter.getOriginalPosition(this.offset) : this.offset;
                if (off >= 0) {
                    StyledDocument d = this.ces.getDocument();
                    PositionRef endRef = this.ces.createPositionRef(off + origText.length(), Position.Bias.Backward);
                    if (d != null) {
                        int l = d.getLength();
                        if (endRef.getOffset() > l) {
                            LOG.log(Level.WARNING, "Invalid diff position: {0}, doc.length: {1}", new Object[]{off, l});
                        }
                    }
                    this.diffs.add(JavaSourceAccessor.getINSTANCE().createDifference(ModificationResult.Difference.Kind.REMOVE, this.ces.createPositionRef(off, Position.Bias.Forward), endRef, origText, null, this.userInfo.get(this.offset)));
                }
            }
            this.offset = pos;
        }

        public void copyTo(int pos) throws IOException {
            this.offset = pos;
        }
    }

}

