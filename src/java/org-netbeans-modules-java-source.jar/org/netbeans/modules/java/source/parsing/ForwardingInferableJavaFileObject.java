/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.java.source.parsing;

import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileObject;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.java.source.parsing.InferableJavaFileObject;

public class ForwardingInferableJavaFileObject
extends ForwardingJavaFileObject<InferableJavaFileObject>
implements InferableJavaFileObject {
    public ForwardingInferableJavaFileObject(@NonNull InferableJavaFileObject delegate) {
        super(delegate);
        assert (delegate != null);
    }

    @Override
    public String inferBinaryName() {
        return ((InferableJavaFileObject)this.fileObject).inferBinaryName();
    }
}

