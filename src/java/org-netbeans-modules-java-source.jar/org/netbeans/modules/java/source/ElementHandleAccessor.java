/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.java.source;

import com.sun.tools.javac.api.JavacTaskImpl;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.ElementHandle;

public abstract class ElementHandleAccessor {
    private static volatile ElementHandleAccessor INSTANCE;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static ElementHandleAccessor getInstance() {
        ElementHandleAccessor result = INSTANCE;
        if (result == null) {
            Class<ElementHandleAccessor> class_ = ElementHandleAccessor.class;
            synchronized (ElementHandleAccessor.class) {
                if (INSTANCE == null) {
                    Class<ElementHandle> c = ElementHandle.class;
                    try {
                        Class.forName(c.getName(), true, c.getClassLoader());
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    assert (INSTANCE != null);
                }
                // ** MonitorExit[var1_1] (shouldn't be in output)
                return INSTANCE;
            }
        }
        return result;
    }

    public static void setInstance(ElementHandleAccessor instance) {
        assert (instance != null);
        INSTANCE = instance;
    }

    protected ElementHandleAccessor() {
    }

    public /* varargs */ abstract ElementHandle create(ElementKind var1, String ... var2);

    public abstract <T extends Element> T resolve(ElementHandle<T> var1, JavacTaskImpl var2);

    @NonNull
    public abstract String[] getJVMSignature(@NonNull ElementHandle<?> var1);
}

