/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.TreeVisitor
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCVariableDecl
 *  com.sun.tools.javac.tree.JCTree$Tag
 *  com.sun.tools.javac.tree.JCTree$Visitor
 *  com.sun.tools.javac.tree.TreeInfo
 */
package org.netbeans.modules.java.source.transform;

import com.sun.source.tree.Tree;
import com.sun.source.tree.TreeVisitor;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeInfo;
import java.util.List;

public class FieldGroupTree
extends JCTree
implements Tree {
    private final List<JCTree.JCVariableDecl> vars;
    private final boolean enumeration;
    private final boolean moreElementsFollowEnum;

    public FieldGroupTree(List<JCTree.JCVariableDecl> vars) {
        this(vars, false, false);
    }

    public FieldGroupTree(List<JCTree.JCVariableDecl> vars, boolean moreElementsFollowEnum) {
        this(vars, true, moreElementsFollowEnum);
    }

    private FieldGroupTree(List<JCTree.JCVariableDecl> vars, boolean enumeration, boolean moreElementsFollowEnum) {
        this.vars = vars;
        this.pos = TreeInfo.getStartPos((JCTree)((JCTree)vars.get(0)));
        this.enumeration = enumeration;
        this.moreElementsFollowEnum = moreElementsFollowEnum;
    }

    public Tree.Kind getKind() {
        return Tree.Kind.OTHER;
    }

    public List<JCTree.JCVariableDecl> getVariables() {
        return this.vars;
    }

    public boolean isEnum() {
        return this.enumeration;
    }

    public boolean moreElementsFollowEnum() {
        return this.moreElementsFollowEnum;
    }

    public int endPos() {
        return TreeInfo.endPos((JCTree)((JCTree)this.vars.get(this.vars.size() - 1)));
    }

    public <R, D> R accept(TreeVisitor<R, D> arg0, D arg1) {
        Object ret = null;
        for (JCTree.JCVariableDecl v : this.vars) {
            ret = v.accept(arg0, arg1);
        }
        return (R)ret;
    }

    public void accept(JCTree.Visitor arg0) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean equals(Object arg0) {
        if (arg0 instanceof FieldGroupTree) {
            return this.vars.equals(((FieldGroupTree)((Object)arg0)).getVariables());
        }
        return false;
    }

    public int hashCode() {
        return this.vars.hashCode();
    }

    public JCTree.Tag getTag() {
        return JCTree.Tag.NO_TAG;
    }
}

