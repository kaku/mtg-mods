/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  org.netbeans.modules.parsing.spi.indexing.Context
 *  org.netbeans.modules.parsing.spi.indexing.Indexable
 */
package org.netbeans.modules.java.source.indexing;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.Tree;
import java.io.File;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.tools.FileObject;
import javax.tools.JavaFileObject;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.modules.java.source.indexing.JavaCustomIndexer;
import org.netbeans.modules.java.source.indexing.JavaParsingContext;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.PrefetchableJavaFileObject;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.netbeans.modules.parsing.spi.indexing.Indexable;

abstract class CompileWorker {
    CompileWorker() {
    }

    abstract ParsingOutput compile(ParsingOutput var1, Context var2, JavaParsingContext var3, Collection<? extends JavaCustomIndexer.CompileTuple> var4);

    protected void computeFQNs(Map<JavaFileObject, List<String>> file2FQNs, CompilationUnitTree cut, JavaCustomIndexer.CompileTuple tuple) {
        String pack = cut.getPackageName() != null ? cut.getPackageName().toString() + "." : "";
        String path = tuple.indexable.getRelativePath();
        int i = path.lastIndexOf(46);
        if (i >= 0) {
            path = path.substring(0, i);
        }
        path = FileObjects.convertFolder2Package(path);
        LinkedList<String> fqns = new LinkedList<String>();
        boolean hasClassesLivingElsewhere = false;
        for (Tree t : cut.getTypeDecls()) {
            if (!TreeUtilities.CLASS_TREE_KINDS.contains((Object)t.getKind())) continue;
            String fqn = pack + ((ClassTree)t).getSimpleName().toString();
            fqns.add(fqn);
            if (path.equals(fqn)) continue;
            hasClassesLivingElsewhere = true;
        }
        if (hasClassesLivingElsewhere) {
            file2FQNs.put(tuple.jfo, fqns);
        }
    }

    static class ParsingOutput {
        final boolean success;
        final boolean lowMemory;
        final Map<JavaFileObject, List<String>> file2FQNs;
        final Set<ElementHandle<TypeElement>> addedTypes;
        final Set<File> createdFiles;
        final Set<Indexable> finishedFiles;
        final Set<ElementHandle<TypeElement>> modifiedTypes;
        final Set<FileObject> aptGenerated;

        private ParsingOutput(boolean success, boolean lowMemory, Map<JavaFileObject, List<String>> file2FQNs, Set<ElementHandle<TypeElement>> addedTypes, Set<File> createdFiles, Set<Indexable> finishedFiles, Set<ElementHandle<TypeElement>> modifiedTypes, Set<FileObject> aptGenerated) {
            assert (success && !lowMemory || !success);
            this.success = success;
            this.lowMemory = lowMemory;
            this.file2FQNs = file2FQNs;
            this.addedTypes = addedTypes;
            this.createdFiles = createdFiles;
            this.finishedFiles = finishedFiles;
            this.modifiedTypes = modifiedTypes;
            this.aptGenerated = aptGenerated;
        }

        static ParsingOutput success(Map<JavaFileObject, List<String>> file2FQNs, Set<ElementHandle<TypeElement>> addedTypes, Set<File> createdFiles, Set<Indexable> finishedFiles, Set<ElementHandle<TypeElement>> modifiedTypes, Set<FileObject> aptGenerated) {
            return new ParsingOutput(true, false, file2FQNs, addedTypes, createdFiles, finishedFiles, modifiedTypes, aptGenerated);
        }

        static ParsingOutput failure(Map<JavaFileObject, List<String>> file2FQNs, Set<ElementHandle<TypeElement>> addedTypes, Set<File> createdFiles, Set<Indexable> finishedFiles, Set<ElementHandle<TypeElement>> modifiedTypes, Set<FileObject> aptGenerated) {
            return new ParsingOutput(false, false, file2FQNs, addedTypes, createdFiles, finishedFiles, modifiedTypes, aptGenerated);
        }

        static ParsingOutput lowMemory(Map<JavaFileObject, List<String>> file2FQNs, Set<ElementHandle<TypeElement>> addedTypes, Set<File> createdFiles, Set<Indexable> finishedFiles, Set<ElementHandle<TypeElement>> modifiedTypes, Set<FileObject> aptGenerated) {
            return new ParsingOutput(false, true, file2FQNs, addedTypes, createdFiles, finishedFiles, modifiedTypes, aptGenerated);
        }
    }

}

