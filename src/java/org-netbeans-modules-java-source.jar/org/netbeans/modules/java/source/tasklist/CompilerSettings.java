/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.java.source.tasklist;

import java.util.Collection;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.source.ClasspathInfo;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public abstract class CompilerSettings {
    public static final String ENABLE_LINT = "enable_lint";

    protected CompilerSettings() {
    }

    public static String getCommandLine(ClasspathInfo cpInfo) {
        ClassPath sourceCP = cpInfo.getClassPath(ClasspathInfo.PathKind.SOURCE);
        FileObject[] roots = sourceCP != null ? sourceCP.getRoots() : new FileObject[]{};
        FileObject file = roots.length > 0 ? roots[0] : null;
        for (CompilerSettings cs : Lookup.getDefault().lookupAll(CompilerSettings.class)) {
            String cl = cs.buildCommandLine(file);
            if (cl == null) continue;
            return cl;
        }
        return "";
    }

    protected abstract String buildCommandLine(@NullAllowed FileObject var1);
}

