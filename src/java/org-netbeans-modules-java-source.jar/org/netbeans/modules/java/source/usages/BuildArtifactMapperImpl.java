/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.queries.AnnotationProcessingQuery
 *  org.netbeans.api.java.queries.BinaryForSourceQuery
 *  org.netbeans.api.java.queries.BinaryForSourceQuery$Result
 *  org.netbeans.api.java.queries.SourceForBinaryQuery
 *  org.netbeans.api.queries.FileBuiltQuery
 *  org.netbeans.api.queries.FileBuiltQuery$Status
 *  org.netbeans.api.queries.VisibilityQuery
 *  org.netbeans.modules.parsing.api.indexing.IndexingManager
 *  org.netbeans.modules.parsing.spi.indexing.ErrorsCache
 *  org.netbeans.spi.queries.FileBuiltQueryImplementation
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.awt.Mnemonics
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.util.WeakSet
 */
package org.netbeans.modules.java.source.usages;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.queries.AnnotationProcessingQuery;
import org.netbeans.api.java.queries.BinaryForSourceQuery;
import org.netbeans.api.java.queries.SourceForBinaryQuery;
import org.netbeans.api.java.source.BuildArtifactMapper;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.queries.FileBuiltQuery;
import org.netbeans.api.queries.VisibilityQuery;
import org.netbeans.modules.java.source.indexing.COSSynchronizingIndexer;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.usages.ContainsErrorsWarning;
import org.netbeans.modules.java.source.usages.fcs.FileChangeSupport;
import org.netbeans.modules.java.source.usages.fcs.FileChangeSupportEvent;
import org.netbeans.modules.java.source.usages.fcs.FileChangeSupportListener;
import org.netbeans.modules.parsing.api.indexing.IndexingManager;
import org.netbeans.modules.parsing.spi.indexing.ErrorsCache;
import org.netbeans.spi.queries.FileBuiltQueryImplementation;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileUtil;
import org.openide.util.ChangeSupport;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.util.WeakSet;

public class BuildArtifactMapperImpl {
    private static final String ASK_BEFORE_RUN_WITH_ERRORS = "askBeforeRunWithErrors";
    private static final String DIRTY_ROOT = "dirty";
    private static final Logger LOG = Logger.getLogger(BuildArtifactMapperImpl.class.getName());
    private static final String TAG_FILE_NAME = ".netbeans_automatic_build";
    private static final String TAG_UPDATE_RESOURCES = ".netbeans_update_resources";
    private static final String SIG = ".sig";
    private static final Map<URL, Set<BuildArtifactMapper.ArtifactsUpdated>> source2Listener = new HashMap<URL, Set<BuildArtifactMapper.ArtifactsUpdated>>();
    private static final long MINIMAL_TIMESTAMP = 2000;
    private static final Set<Object> alreadyWarned = new WeakSet();
    private static final Pattern RELATIVE_SLASH_SEPARATED_PATH = Pattern.compile("[^:/\\\\.][^:/\\\\]*(/[^:/\\\\.][^:/\\\\]*)*");
    private static Map<File, Reference<FileChangeListenerImpl>> file2Listener = new WeakHashMap<File, Reference<FileChangeListenerImpl>>();
    private static Map<FileChangeListenerImpl, File> listener2File = new WeakHashMap<FileChangeListenerImpl, File>();

    public static synchronized void addArtifactsUpdatedListener(URL sourceRoot, BuildArtifactMapper.ArtifactsUpdated listener) {
        Set<BuildArtifactMapper.ArtifactsUpdated> listeners = source2Listener.get(sourceRoot);
        if (listeners == null) {
            listeners = new HashSet<BuildArtifactMapper.ArtifactsUpdated>();
            source2Listener.put(sourceRoot, listeners);
        }
        listeners.add(listener);
    }

    public static synchronized void removeArtifactsUpdatedListener(URL sourceRoot, BuildArtifactMapper.ArtifactsUpdated listener) {
        Set<BuildArtifactMapper.ArtifactsUpdated> listeners = source2Listener.get(sourceRoot);
        if (listeners == null) {
            return;
        }
        listeners.remove(listener);
        if (listeners.isEmpty()) {
            source2Listener.remove(sourceRoot);
        }
    }

    private static boolean protectAgainstErrors(File targetFolder, FileObject[][] sources, Object context) throws MalformedURLException {
        Preferences pref = NbPreferences.forModule(BuildArtifactMapperImpl.class).node(BuildArtifactMapperImpl.class.getSimpleName());
        if (!pref.getBoolean("askBeforeRunWithErrors", true)) {
            return true;
        }
        BuildArtifactMapperImpl.sources(targetFolder, sources);
        for (FileObject file : sources[0]) {
            if (!ErrorsCache.isInError((FileObject)file, (boolean)true) || alreadyWarned.contains(context)) continue;
            JButton btnRunAnyway = new JButton();
            Mnemonics.setLocalizedText((AbstractButton)btnRunAnyway, (String)NbBundle.getMessage(BuildArtifactMapperImpl.class, (String)"BTN_RunAnyway"));
            btnRunAnyway.getAccessibleContext().setAccessibleName(NbBundle.getMessage(BuildArtifactMapperImpl.class, (String)"ACSN_BTN_RunAnyway"));
            btnRunAnyway.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(BuildArtifactMapperImpl.class, (String)"ACSD_BTN_RunAnyway"));
            JButton btnCancel = new JButton();
            Mnemonics.setLocalizedText((AbstractButton)btnCancel, (String)NbBundle.getMessage(BuildArtifactMapperImpl.class, (String)"BTN_Cancel"));
            btnCancel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(BuildArtifactMapperImpl.class, (String)"ACSN_BTN_Cancel"));
            btnCancel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(BuildArtifactMapperImpl.class, (String)"ACSD_BTN_Cancel"));
            ContainsErrorsWarning panel = new ContainsErrorsWarning();
            DialogDescriptor dd = new DialogDescriptor((Object)panel, NbBundle.getMessage(BuildArtifactMapperImpl.class, (String)"TITLE_ContainsErrorsWarning"), true, new Object[]{btnRunAnyway, btnCancel}, (Object)btnRunAnyway, 0, null, null);
            dd.setMessageType(2);
            Object option = DialogDisplayer.getDefault().notify((NotifyDescriptor)dd);
            if (option == btnRunAnyway) {
                pref.putBoolean("askBeforeRunWithErrors", panel.getAskBeforeRunning());
                alreadyWarned.add(context);
                return true;
            }
            return false;
        }
        return true;
    }

    private static void sources(File targetFolder, FileObject[][] sources) throws MalformedURLException {
        if (sources[0] == null) {
            URL targetFolderURL = FileUtil.urlForArchiveOrDir((File)targetFolder);
            sources[0] = SourceForBinaryQuery.findSourceRoots((URL)targetFolderURL).getRoots();
        }
    }

    private static File getTarget(URL source) {
        BinaryForSourceQuery.Result binaryRoots = BinaryForSourceQuery.findBinaryRoots((URL)source);
        File result = null;
        for (URL u : binaryRoots.getRoots()) {
            assert (u != null);
            if (u == null) continue;
            File f = FileUtil.archiveOrDirForURL((URL)u);
            try {
                if (FileUtil.isArchiveFile((URL)Utilities.toURI((File)f).toURL())) continue;
                if (f != null && result != null) {
                    Logger.getLogger(BuildArtifactMapperImpl.class.getName()).log(Level.WARNING, "More than one binary directory for root: {0}", source.toExternalForm());
                    return null;
                }
                result = f;
                continue;
            }
            catch (MalformedURLException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return result;
    }

    public static Boolean ensureBuilt(URL sourceRoot, Object context, boolean copyResources, boolean keepResourceUpToDate) throws IOException {
        File targetFolder = BuildArtifactMapperImpl.getTarget(sourceRoot);
        if (targetFolder == null) {
            return null;
        }
        try {
            SourceUtils.waitScanFinished();
        }
        catch (InterruptedException e) {
            LOG.log(Level.FINE, null, e);
            return null;
        }
        if (JavaIndex.ensureAttributeValue(sourceRoot, "dirty", null)) {
            IndexingManager.getDefault().refreshIndexAndWait(sourceRoot, null);
        }
        if (JavaIndex.getAttribute(sourceRoot, "dirty", null) != null) {
            return false;
        }
        FileObject[][] sources = new FileObject[1][];
        if (!BuildArtifactMapperImpl.protectAgainstErrors(targetFolder, sources, context)) {
            return false;
        }
        File tagFile = new File(targetFolder, ".netbeans_automatic_build");
        File tagUpdateResourcesFile = new File(targetFolder, ".netbeans_update_resources");
        boolean forceResourceCopy = copyResources && keepResourceUpToDate && !tagUpdateResourcesFile.exists();
        boolean cosActive = tagFile.exists();
        if (cosActive && !forceResourceCopy) {
            return true;
        }
        if (!cosActive) {
            BuildArtifactMapperImpl.delete(targetFolder, false);
        }
        if (!targetFolder.exists() && !targetFolder.mkdirs()) {
            throw new IOException("Cannot create destination folder: " + targetFolder.getAbsolutePath());
        }
        BuildArtifactMapperImpl.sources(targetFolder, sources);
        for (int i = sources[0].length - 1; i >= 0; --i) {
            FileObject sr = sources[0][i];
            if (!cosActive) {
                URL srURL = sr.toURL();
                File index = JavaIndex.getClassFolder(srURL, true);
                if (index == null) {
                    if (srURL.equals(AnnotationProcessingQuery.getAnnotationProcessingOptions((FileObject)sr).sourceOutputDirectory())) continue;
                    return null;
                }
                BuildArtifactMapperImpl.copyRecursively(index, targetFolder);
            }
            if (!copyResources) continue;
            Set<String> javaMimeTypes = COSSynchronizingIndexer.gatherJavaMimeTypes();
            String[] javaMimeTypesArr = javaMimeTypes.toArray(new String[0]);
            BuildArtifactMapperImpl.copyRecursively(sr, targetFolder, javaMimeTypes, javaMimeTypesArr);
        }
        if (!cosActive) {
            new FileOutputStream(tagFile).close();
        }
        if (keepResourceUpToDate) {
            new FileOutputStream(tagUpdateResourcesFile).close();
        }
        return true;
    }

    public static Boolean clean(URL sourceRoot) throws IOException {
        File targetFolder = BuildArtifactMapperImpl.getTarget(sourceRoot);
        if (targetFolder == null) {
            return null;
        }
        File tagFile = new File(targetFolder, ".netbeans_automatic_build");
        if (!tagFile.exists()) {
            return null;
        }
        try {
            SourceUtils.waitScanFinished();
        }
        catch (InterruptedException e) {
            LOG.log(Level.FINE, null, e);
            return false;
        }
        BuildArtifactMapperImpl.delete(targetFolder, false);
        BuildArtifactMapperImpl.delete(tagFile, true);
        return null;
    }

    public static File getTargetFolder(URL sourceRoot) {
        File targetFolder = BuildArtifactMapperImpl.getTarget(sourceRoot);
        if (targetFolder == null) {
            return null;
        }
        if (!new File(targetFolder, ".netbeans_automatic_build").exists()) {
            return null;
        }
        return targetFolder;
    }

    public static boolean isUpdateResources(File targetFolder) {
        return targetFolder != null && new File(targetFolder, ".netbeans_update_resources").exists();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public static void classCacheUpdated(URL sourceRoot, File cacheRoot, Iterable<File> deleted, Iterable<File> updated, boolean resource) {
        String relPath;
        if (!deleted.iterator().hasNext() && !updated.iterator().hasNext()) {
            return;
        }
        File targetFolder = BuildArtifactMapperImpl.getTargetFolder(sourceRoot);
        if (targetFolder == null) {
            return;
        }
        if (resource && !BuildArtifactMapperImpl.isUpdateResources(targetFolder)) {
            return;
        }
        LinkedList<File> updatedFiles = new LinkedList<File>();
        for (File deletedFile : deleted) {
            relPath = BuildArtifactMapperImpl.relativizeFile(cacheRoot, deletedFile);
            if (relPath == null) {
                throw new IllegalArgumentException(String.format("Deleted file: %s is not under cache root: %s, (normalized file: %s).", deletedFile.getAbsolutePath(), cacheRoot.getAbsolutePath(), FileUtil.normalizeFile((File)deletedFile).getAbsolutePath()));
            }
            File toDelete = BuildArtifactMapperImpl.resolveFile(targetFolder, relPath);
            toDelete.delete();
            updatedFiles.add(toDelete);
        }
        for (File updatedFile2 : updated) {
            relPath = BuildArtifactMapperImpl.relativizeFile(cacheRoot, updatedFile2);
            if (relPath == null) {
                throw new IllegalArgumentException(String.format("Updated file: %s is not under cache root: %s, (normalized file: %s).", updatedFile2.getAbsolutePath(), cacheRoot.getAbsolutePath(), FileUtil.normalizeFile((File)updatedFile2).getAbsolutePath()));
            }
            File target = BuildArtifactMapperImpl.resolveFile(targetFolder, relPath);
            try {
                BuildArtifactMapperImpl.copyFile(updatedFile2, target);
                updatedFiles.add(target);
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        if (updatedFiles.size() <= 0) return;
        Class<BuildArtifactMapperImpl> updatedFile2 = BuildArtifactMapperImpl.class;
        synchronized (BuildArtifactMapperImpl.class) {
            Set<BuildArtifactMapper.ArtifactsUpdated> listeners = source2Listener.get(sourceRoot);
            if (listeners != null) {
                listeners = new HashSet<BuildArtifactMapper.ArtifactsUpdated>(listeners);
            }
            // ** MonitorExit[updatedFile] (shouldn't be in output)
            if (listeners == null) return;
            for (BuildArtifactMapper.ArtifactsUpdated listener : listeners) {
                listener.artifactsUpdated(updatedFiles);
            }
            return;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void copyFile(File updatedFile, File target) throws IOException {
        File parent = target.getParentFile();
        if (parent != null && !parent.exists() && !parent.mkdirs()) {
            throw new IOException("Cannot create folder: " + parent.getAbsolutePath());
        }
        FileInputStream ins = null;
        OutputStream out = null;
        try {
            ins = new FileInputStream(updatedFile);
            out = new FileOutputStream(target);
            FileUtil.copy((InputStream)ins, (OutputStream)out);
        }
        catch (FileNotFoundException fnf) {
            LOG.log(Level.INFO, "Cannot open file.", fnf);
        }
        finally {
            if (ins != null) {
                try {
                    ins.close();
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            if (out != null) {
                try {
                    out.close();
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void copyFile(FileObject updatedFile, File target) throws IOException {
        File parent = target.getParentFile();
        if (parent != null && !parent.exists() && !parent.mkdirs()) {
            throw new IOException("Cannot create folder: " + parent.getAbsolutePath());
        }
        InputStream ins = null;
        FileOutputStream out = null;
        try {
            ins = updatedFile.getInputStream();
            out = new FileOutputStream(target);
            FileUtil.copy((InputStream)ins, (OutputStream)out);
        }
        finally {
            if (ins != null) {
                try {
                    ins.close();
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            if (out != null) {
                try {
                    out.close();
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }
    }

    private static void copyRecursively(File source, File target) throws IOException {
        if (source.isDirectory()) {
            if (target.exists() && !target.isDirectory()) {
                throw new IOException("Cannot create folder: " + target.getAbsolutePath() + ", already exists as a file.");
            }
            File[] listed = source.listFiles();
            if (listed == null) {
                return;
            }
            for (File f : listed) {
                String name = f.getName();
                if (name.endsWith(".sig")) {
                    name = name.substring(0, name.length() - "sig".length()) + "class";
                }
                BuildArtifactMapperImpl.copyRecursively(f, new File(target, name));
            }
        } else {
            if (target.isDirectory()) {
                throw new IOException("Cannot create file: " + target.getAbsolutePath() + ", already exists as a folder.");
            }
            BuildArtifactMapperImpl.copyFile(source, target);
        }
    }

    private static void copyRecursively(FileObject source, File target, Set<String> javaMimeTypes, String[] javaMimeTypesArr) throws IOException {
        if (!VisibilityQuery.getDefault().isVisible(source)) {
            return;
        }
        if (source.isFolder()) {
            FileObject[] listed;
            if (!target.exists()) {
                if (!target.mkdirs()) {
                    throw new IOException("Cannot create folder: " + target.getAbsolutePath());
                }
            } else if (!target.isDirectory()) {
                throw new IOException("Cannot create folder: " + target.getAbsolutePath() + ", already exists as a file.");
            }
            if ((listed = source.getChildren()) == null) {
                return;
            }
            for (FileObject f : listed) {
                if (f.isData() && javaMimeTypes.contains(FileUtil.getMIMEType((FileObject)f, (String[])javaMimeTypesArr))) continue;
                BuildArtifactMapperImpl.copyRecursively(f, new File(target, f.getNameExt()), javaMimeTypes, javaMimeTypesArr);
            }
        } else {
            if (target.isDirectory()) {
                throw new IOException("Cannot create file: " + target.getAbsolutePath() + ", already exists as a folder.");
            }
            BuildArtifactMapperImpl.copyFile(source, target);
        }
    }

    private static void delete(File file, boolean cleanCompletely) throws IOException {
        if (file.isDirectory()) {
            File[] listed = file.listFiles();
            if (listed == null) {
                return;
            }
            for (File f : listed) {
                BuildArtifactMapperImpl.delete(f, cleanCompletely);
            }
            if (cleanCompletely) {
                file.delete();
            }
        } else if (cleanCompletely || file.getName().endsWith(".class")) {
            file.delete();
        }
    }

    public static File resolveFile(File basedir, String filename) throws IllegalArgumentException {
        File f;
        if (basedir == null) {
            throw new NullPointerException("null basedir passed to resolveFile");
        }
        if (filename == null) {
            throw new NullPointerException("null filename passed to resolveFile");
        }
        if (!basedir.isAbsolute()) {
            throw new IllegalArgumentException("nonabsolute basedir passed to resolveFile: " + basedir);
        }
        if (filename.endsWith(".sig")) {
            filename = filename.substring(0, filename.length() - "sig".length()) + "class";
        }
        if (RELATIVE_SLASH_SEPARATED_PATH.matcher(filename).matches()) {
            f = new File(basedir, filename.replace('/', File.separatorChar));
        } else {
            String machinePath = filename.replace('/', File.separatorChar).replace('\\', File.separatorChar);
            f = new File(machinePath);
            if (!f.isAbsolute()) {
                f = new File(basedir, machinePath);
            }
            assert (f.isAbsolute());
        }
        return f;
    }

    public static String relativizeFile(File basedir, File file) {
        if (basedir.isFile()) {
            throw new IllegalArgumentException("Cannot relative w.r.t. a data file " + basedir);
        }
        if (basedir.equals(file)) {
            return ".";
        }
        StringBuffer b = new StringBuffer();
        File base = basedir;
        String filepath = file.getAbsolutePath();
        while (!filepath.startsWith(BuildArtifactMapperImpl.slashify(base.getAbsolutePath()))) {
            if ((base = base.getParentFile()) == null) {
                return null;
            }
            if (base.equals(file)) {
                b.append("..");
                return b.toString();
            }
            b.append("../");
        }
        URI u = Utilities.toURI((File)base).relativize(Utilities.toURI((File)file));
        assert (!u.isAbsolute());
        b.append(u.getPath());
        if (b.charAt(b.length() - 1) == '/') {
            b.setLength(b.length() - 1);
        }
        return b.toString();
    }

    private static String slashify(String path) {
        if (path.endsWith(File.separator)) {
            return path;
        }
        return path + File.separatorChar;
    }

    private static final class FileChangeListenerImpl
    implements FileChangeSupportListener {
        private RequestProcessor NOTIFY = new RequestProcessor(FileChangeListenerImpl.class.getName());
        private Set<ChangeListener> notify = new WeakSet();

        private FileChangeListenerImpl() {
        }

        @Override
        public void fileCreated(FileChangeSupportEvent event) {
            this.notifyListeners();
        }

        @Override
        public void fileDeleted(FileChangeSupportEvent event) {
            this.notifyListeners();
        }

        @Override
        public void fileModified(FileChangeSupportEvent event) {
            this.notifyListeners();
        }

        private synchronized void addListener(ChangeListener l) {
            this.notify.add(l);
        }

        private synchronized void notifyListeners() {
            final HashSet<ChangeListener> toNotify = new HashSet<ChangeListener>(this.notify);
            this.NOTIFY.post(new Runnable(){

                @Override
                public void run() {
                    for (ChangeListener l : toNotify) {
                        l.stateChanged(null);
                    }
                }
            });
        }

    }

    private static final class FileBuiltQueryStatusImpl
    implements FileBuiltQuery.Status,
    ChangeListener {
        private final FileBuiltQuery.Status delegate;
        private final File tag;
        private final FileChangeListenerImpl fileListener;
        private final ChangeSupport cs;

        public FileBuiltQueryStatusImpl(FileBuiltQuery.Status delegate, File tag, FileChangeListenerImpl fileListener) {
            this.cs = new ChangeSupport((Object)this);
            this.delegate = delegate;
            this.tag = tag;
            this.fileListener = fileListener;
            delegate.addChangeListener((ChangeListener)this);
            fileListener.addListener(this);
        }

        public boolean isBuilt() {
            return this.delegate.isBuilt() || this.tag.canRead();
        }

        public void addChangeListener(ChangeListener l) {
            this.cs.addChangeListener(l);
        }

        public void removeChangeListener(ChangeListener l) {
            this.cs.removeChangeListener(l);
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            this.cs.fireChange();
        }
    }

    public static final class FileBuildQueryImpl
    implements FileBuiltQueryImplementation {
        private final ThreadLocal<Boolean> recursive = new ThreadLocal();
        private final Map<FileObject, Reference<FileBuiltQuery.Status>> file2Status = new WeakHashMap<FileObject, Reference<FileBuiltQuery.Status>>();

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public FileBuiltQuery.Status getStatus(FileObject file) {
            FileBuiltQuery.Status result;
            Reference<FileBuiltQuery.Status> statusRef;
            FileBuildQueryImpl fileBuildQueryImpl = this;
            synchronized (fileBuildQueryImpl) {
                statusRef = this.file2Status.get((Object)file);
            }
            FileBuiltQuery.Status status = result = statusRef != null ? statusRef.get() : null;
            if (result != null) {
                return result;
            }
            if (this.recursive.get() != null) {
                return null;
            }
            this.recursive.set(true);
            try {
                FileObject owner;
                FileBuiltQuery.Status delegate = FileBuiltQuery.getStatus((FileObject)file);
                if (delegate == null) {
                    FileBuiltQuery.Status status2 = null;
                    return status2;
                }
                ClassPath source = ClassPath.getClassPath((FileObject)file, (String)"classpath/source");
                FileObject fileObject = owner = source != null ? source.findOwnerRoot(file) : null;
                if (owner == null) {
                    FileBuiltQuery.Status status3 = delegate;
                    return status3;
                }
                File target = BuildArtifactMapperImpl.getTarget(owner.getURL());
                File tagFile = FileUtil.normalizeFile((File)new File(target, ".netbeans_automatic_build"));
                FileBuildQueryImpl fileBuildQueryImpl2 = this;
                synchronized (fileBuildQueryImpl2) {
                    try {
                        FileChangeListenerImpl l;
                        Reference<FileBuiltQuery.Status> prevRef;
                        Reference ref = (Reference)file2Listener.get(tagFile);
                        FileChangeListenerImpl fileChangeListenerImpl = l = ref != null ? (FileChangeListenerImpl)ref.get() : null;
                        if (l == null) {
                            l = new FileChangeListenerImpl();
                            file2Listener.put(tagFile, new WeakReference<FileChangeListenerImpl>(l));
                            listener2File.put(l, tagFile);
                            FileChangeSupport.DEFAULT.addListener(l, tagFile);
                        }
                        FileBuiltQuery.Status status4 = result = (prevRef = this.file2Status.get((Object)file)) != null ? prevRef.get() : null;
                        if (result == null) {
                            result = new FileBuiltQueryStatusImpl(delegate, tagFile, l);
                            this.file2Status.put(file, new WeakReference<FileBuiltQueryStatusImpl>((FileBuiltQueryStatusImpl)result));
                        }
                        FileBuiltQuery.Status status5 = result;
                        return status5;
                    }
                    catch (Throwable var14_18) {
                        try {
                            throw var14_18;
                        }
                        catch (FileStateInvalidException ex) {
                            Exceptions.printStackTrace((Throwable)ex);
                            source = null;
                            return source;
                        }
                    }
                }
            }
            finally {
                this.recursive.remove();
            }
        }
    }

}

