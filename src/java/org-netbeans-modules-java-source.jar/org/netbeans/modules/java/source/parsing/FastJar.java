/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.source.parsing;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.Date;
import java.util.LinkedList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public final class FastJar {
    private static final int GIVE_UP = 65536;

    private FastJar() {
    }

    public static InputStream getInputStream(File file, Entry e) throws IOException {
        return FastJar.getInputStream(file, e.offset);
    }

    static InputStream getInputStream(File file, long offset) throws IOException {
        RandomAccessFile f = new RandomAccessFile(file, "r");
        f.seek(offset);
        ZipInputStream in = new ZipInputStream(new RandomAccessFileInputStream(f));
        ZipEntry e = in.getNextEntry();
        if (e != null && e.getCrc() == 0 && e.getMethod() == 0) {
            long cp = f.getFilePointer();
            in.close();
            f = new RandomAccessFile(file, "r");
            f.seek(cp);
            return new RandomAccessFileInputStream(f, e.getSize());
        }
        return in;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static ZipEntry getZipEntry(File file, long offset) throws IOException {
        RandomAccessFile f = new RandomAccessFile(file, "r");
        try {
            ZipEntry zipEntry;
            f.seek(offset);
            ZipInputStream in = new ZipInputStream(new RandomAccessFileInputStream(f));
            try {
                zipEntry = in.getNextEntry();
            }
            catch (Throwable var6_5) {
                in.close();
                throw var6_5;
            }
            in.close();
            return zipEntry;
        }
        finally {
            f.close();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static Iterable<? extends Entry> list(File f) throws IOException {
        RandomAccessFile b = new RandomAccessFile(f, "r");
        try {
            long size = (int)b.length();
            b.seek(size - 22);
            byte[] data = new byte[22];
            int giveup = 0;
            do {
                if (b.read(data, 0, 22) != 22) {
                    throw new IOException();
                }
                b.seek(b.getFilePointer() - 23);
                if (++giveup <= 65536) continue;
                throw new IOException();
            } while (FastJar.getsig(data) != 101010256);
            long censize = FastJar.endsiz(data);
            long cenoff = FastJar.endoff(data);
            b.seek(cenoff);
            LinkedList<Entry> result = new LinkedList<Entry>();
            int cenread = 0;
            data = new byte[46];
            while ((long)cenread < censize) {
                if (b.read(data, 0, 46) != 46) {
                    throw new IOException("No central table");
                }
                if (FastJar.getsig(data) != 33639248) {
                    throw new IOException("No central table");
                }
                int cennam = FastJar.cennam(data);
                int cenext = FastJar.cenext(data);
                int cencom = FastJar.cencom(data);
                long lhoff = FastJar.cenoff(data);
                long centim = FastJar.centim(data);
                String name = FastJar.name(b, cennam);
                int seekby = cenext + cencom;
                int cendatalen = 46 + cennam + seekby;
                cenread += cendatalen;
                result.add(new Entry(name, lhoff, centim));
                FastJar.seekBy(b, seekby);
            }
            LinkedList<Entry> cennam = result;
            return cennam;
        }
        finally {
            b.close();
        }
    }

    private static final String name(RandomAccessFile b, int cennam) throws IOException {
        byte[] name = new byte[cennam];
        b.read(name, 0, cennam);
        return new String(name, "UTF-8");
    }

    private static final long getsig(byte[] b) throws IOException {
        return FastJar.get32(b, 0);
    }

    private static final long endsiz(byte[] b) throws IOException {
        return FastJar.get32(b, 12);
    }

    private static final long endoff(byte[] b) throws IOException {
        return FastJar.get32(b, 16);
    }

    private static final long cenlen(byte[] b) throws IOException {
        return FastJar.get32(b, 24);
    }

    private static final long censiz(byte[] b) throws IOException {
        return FastJar.get32(b, 20);
    }

    private static final long centim(byte[] b) throws IOException {
        return FastJar.get32(b, 12);
    }

    private static final int cennam(byte[] b) throws IOException {
        return FastJar.get16(b, 28);
    }

    private static final int cenext(byte[] b) throws IOException {
        return FastJar.get16(b, 30);
    }

    private static final int cencom(byte[] b) throws IOException {
        return FastJar.get16(b, 32);
    }

    private static final long cenoff(byte[] b) throws IOException {
        return FastJar.get32(b, 42);
    }

    private static final int lochow(byte[] b) throws IOException {
        return FastJar.get16(b, 8);
    }

    private static final int locname(byte[] b) throws IOException {
        return FastJar.get16(b, 26);
    }

    private static final int locext(byte[] b) throws IOException {
        return FastJar.get16(b, 28);
    }

    private static final long locsiz(byte[] b) throws IOException {
        return FastJar.get32(b, 18);
    }

    private static final void seekBy(RandomAccessFile b, int offset) throws IOException {
        b.seek(b.getFilePointer() + (long)offset);
    }

    private static final int get16(byte[] b, int off) throws IOException {
        byte b1 = b[off];
        byte b2 = b[off + 1];
        return b1 & 255 | (b2 & 255) << 8;
    }

    private static final long get32(byte[] b, int off) throws IOException {
        int s1 = FastJar.get16(b, off);
        int s2 = FastJar.get16(b, off + 2);
        return (long)s1 | (long)s2 << 16;
    }

    public static final class Entry {
        public final String name;
        final long offset;
        private final long dosTime;

        public Entry(String name, long offset, long time) {
            assert (name != null);
            this.name = name;
            this.offset = offset;
            this.dosTime = time;
        }

        public long getTime() {
            Date d = new Date((int)((this.dosTime >> 25 & 127) + 80), (int)((this.dosTime >> 21 & 15) - 1), (int)(this.dosTime >> 16 & 31), (int)(this.dosTime >> 11 & 31), (int)(this.dosTime >> 5 & 63), (int)(this.dosTime << 1 & 62));
            return d.getTime();
        }
    }

    private static class RandomAccessFileInputStream
    extends InputStream {
        private final RandomAccessFile b;
        private final long len;

        public RandomAccessFileInputStream(RandomAccessFile b) throws IOException {
            assert (b != null);
            this.b = b;
            this.len = b.length();
        }

        public RandomAccessFileInputStream(RandomAccessFile b, long len) throws IOException {
            assert (b != null);
            assert (len >= 0);
            this.b = b;
            this.len = b.getFilePointer() + len;
        }

        @Override
        public int read(byte[] data, int offset, int size) throws IOException {
            int rem = this.available();
            if (rem == 0) {
                return -1;
            }
            int rlen = size < rem ? size : rem;
            return this.b.read(data, offset, rlen);
        }

        @Override
        public int read() throws IOException {
            if (this.available() == 0) {
                return -1;
            }
            return this.b.readByte();
        }

        @Override
        public int available() throws IOException {
            return (int)(this.len - this.b.getFilePointer());
        }

        @Override
        public void close() throws IOException {
            this.b.close();
        }
    }

}

