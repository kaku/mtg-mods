/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.TypeParameterTree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.TaskEvent
 *  com.sun.source.util.TaskEvent$Kind
 *  com.sun.source.util.TaskListener
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.code.Scope
 *  com.sun.tools.javac.code.Scope$Entry
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.code.Symbol$MethodSymbol
 *  com.sun.tools.javac.code.Symbol$VarSymbol
 *  com.sun.tools.javac.code.Symtab
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.code.Types
 *  com.sun.tools.javac.model.LazyTreeLoader
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCBlock
 *  com.sun.tools.javac.tree.JCTree$JCClassDecl
 *  com.sun.tools.javac.tree.JCTree$JCExpression
 *  com.sun.tools.javac.tree.JCTree$JCMethodDecl
 *  com.sun.tools.javac.tree.JCTree$JCModifiers
 *  com.sun.tools.javac.tree.JCTree$JCVariableDecl
 *  com.sun.tools.javac.tree.JCTree$Tag
 *  com.sun.tools.javac.tree.TreeScanner
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Key
 *  com.sun.tools.javac.util.CouplingAbort
 *  com.sun.tools.javac.util.List
 *  com.sun.tools.javac.util.Log
 *  com.sun.tools.javac.util.Log$DiagnosticHandler
 *  com.sun.tools.javac.util.Log$DiscardDiagnosticHandler
 *  com.sun.tools.javac.util.Name
 *  com.sun.tools.javac.util.Name$Table
 *  com.sun.tools.javac.util.Names
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.java.source;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TypeParameterTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.TaskEvent;
import com.sun.source.util.TaskListener;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.code.Scope;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Symtab;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.Types;
import com.sun.tools.javac.model.LazyTreeLoader;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeScanner;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.CouplingAbort;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Log;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;
import java.awt.EventQueue;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.swing.text.ChangedCharSetException;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.modules.java.source.JavaSourceAccessor;
import org.netbeans.modules.java.source.JavadocHelper;
import org.netbeans.modules.java.source.indexing.JavaBinaryIndexer;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.parsing.FileManagerTransaction;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.OutputFileManager;
import org.netbeans.modules.java.source.usages.ClasspathInfoAccessor;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public class TreeLoader
extends LazyTreeLoader {
    private static final Pattern ctor_summary_name = Pattern.compile("constructor[_.]summary");
    private static final Pattern method_summary_name = Pattern.compile("method[_.]summary");
    private static final Pattern field_detail_name = Pattern.compile("field[_.]detail");
    private static final Pattern ctor_detail_name = Pattern.compile("constructor[_.]detail");
    private static final Pattern method_detail_name = Pattern.compile("method[_.]detail");
    private static final ThreadLocal<Boolean> isTreeLoading = new ThreadLocal();
    private static final Logger LOGGER = Logger.getLogger(TreeLoader.class.getName());
    private static final boolean ALWAYS_ALLOW_JDOC_ARG_NAMES = Boolean.getBoolean("java.source.args.from.http.jdoc");
    public static boolean DISABLE_CONFINEMENT_TEST = false;
    public static boolean DISABLE_ARTIFICAL_PARAMETER_NAMES = false;
    private Context context;
    private ClasspathInfo cpInfo;
    private Map<Symbol.ClassSymbol, StringBuilder> couplingErrors;
    private boolean partialReparse;
    private Thread owner;
    private int depth;
    private final boolean checkContention;
    private static final int MAX_DUMPS = Integer.getInteger("org.netbeans.modules.java.source.parsing.JavacParser.maxDumps", 255);

    public static void preRegister(Context context, ClasspathInfo cpInfo, boolean detached) {
        context.put(lazyTreeLoaderKey, (Object)new TreeLoader(context, cpInfo, detached));
    }

    public static TreeLoader instance(Context ctx) {
        LazyTreeLoader tl = LazyTreeLoader.instance((Context)ctx);
        return tl instanceof TreeLoader ? (TreeLoader)tl : null;
    }

    public static boolean isTreeLoading() {
        return isTreeLoading.get() == Boolean.TRUE;
    }

    private TreeLoader(Context context, ClasspathInfo cpInfo, boolean detached) {
        this.context = context;
        this.cpInfo = cpInfo;
        this.checkContention = detached;
    }

    /*
     * Exception decompiling
     */
    public boolean loadTreeFor(Symbol.ClassSymbol clazz, boolean persist) {
        // This method has failed to decompile.  When submitting a bug report, please provide this stack trace, and (if you hold appropriate legal rights) the relevant class file.
        // org.benf.cfr.reader.util.ConfusedCFRException: Tried to end blocks [0[TRYBLOCK]], but top level block is 5[CATCHBLOCK]
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.processEndingBlocks(Op04StructuredStatement.java:397)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.buildNestedBlocks(Op04StructuredStatement.java:449)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op03SimpleStatement.createInitialStructuredBlock(Op03SimpleStatement.java:2877)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisInner(CodeAnalyser.java:825)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisOrWrapFail(CodeAnalyser.java:217)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysis(CodeAnalyser.java:162)
        // org.benf.cfr.reader.entities.attributes.AttributeCode.analyse(AttributeCode.java:95)
        // org.benf.cfr.reader.entities.Method.analyse(Method.java:355)
        // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:768)
        // org.benf.cfr.reader.entities.ClassFile.analyseTop(ClassFile.java:700)
        // org.benf.cfr.reader.Main.doJar(Main.java:134)
        // org.benf.cfr.reader.Main.main(Main.java:189)
        throw new IllegalStateException("Decompilation failed");
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean loadParamNames(Symbol.ClassSymbol clazz) {
        boolean contended = !this.attachTo(Thread.currentThread());
        try {
            assert (DISABLE_CONFINEMENT_TEST || JavaSourceAccessor.getINSTANCE().isJavaCompilerLocked() || !contended);
            if (clazz != null) {
                JavadocHelper.TextStream page = JavadocHelper.getJavadoc((Element)clazz, ALWAYS_ALLOW_JDOC_ARG_NAMES, null);
                if (!(page == null || page.isRemote() && EventQueue.isDispatchThread() || !this.getParamNamesFromJavadocText(page, clazz))) {
                    boolean bl = true;
                    return bl;
                }
                if (!DISABLE_ARTIFICAL_PARAMETER_NAMES) {
                    this.fillArtificalParamNames(clazz);
                    boolean bl = true;
                    return bl;
                }
            }
            boolean page = false;
            return page;
        }
        finally {
            this.dettachFrom(Thread.currentThread());
        }
    }

    public void couplingError(Symbol.ClassSymbol clazz, Tree t) {
        if (this.partialReparse) {
            super.couplingError(clazz, t);
        }
        if (clazz != null && this.couplingErrors != null) {
            StringBuilder sb = this.couplingErrors.get((Object)clazz);
            if (sb != null) {
                sb.append(TreeLoader.getTreeInfo(t));
            } else {
                this.couplingErrors.put(clazz, TreeLoader.getTreeInfo(t));
            }
        } else {
            TreeLoader.dumpCouplingAbort(new CouplingAbort(clazz, t), null);
        }
    }

    public void updateContext(Context context) {
        this.context = context;
    }

    public final void startPartialReparse() {
        this.partialReparse = true;
    }

    public final void endPartialReparse() {
        this.partialReparse = false;
    }

    public static boolean pruneTree(JCTree tree, final Symtab syms, final HashMap<Symbol.ClassSymbol, JCTree.JCClassDecl> syms2trees) {
        final AtomicBoolean ret = new AtomicBoolean(true);
        new TreeScanner(){

            public void visitMethodDef(JCTree.JCMethodDecl tree) {
                super.visitMethodDef(tree);
                if (tree.sym == null || tree.type == null || tree.type == syms.unknownType) {
                    ret.set(false);
                }
                tree.body = null;
            }

            public void visitVarDef(JCTree.JCVariableDecl tree) {
                super.visitVarDef(tree);
                if (tree.sym == null || tree.type == null || tree.type == syms.unknownType) {
                    ret.set(false);
                }
                tree.init = null;
            }

            public void visitClassDef(JCTree.JCClassDecl tree) {
                this.scan((JCTree)tree.mods);
                this.scan(tree.typarams);
                this.scan((JCTree)tree.extending);
                this.scan(tree.implementing);
                if (tree.defs != null) {
                    List prev = null;
                    List l = tree.defs;
                    while (l.nonEmpty()) {
                        this.scan((JCTree)l.head);
                        if (((JCTree)l.head).getTag() == JCTree.Tag.BLOCK) {
                            if (prev != null) {
                                prev.tail = l.tail;
                            } else {
                                tree.defs = l.tail;
                            }
                        }
                        prev = l;
                        l = l.tail;
                    }
                }
                if (tree.sym == null || tree.type == null || tree.type == syms.unknownType) {
                    ret.set(false);
                } else if (syms2trees != null) {
                    syms2trees.put(tree.sym, tree);
                }
            }
        }.scan(tree);
        return ret.get();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void dumpSymFile(JavaFileManager jfm, JavacTaskImpl jti, Symbol.ClassSymbol clazz, HashMap<Symbol.ClassSymbol, JCTree.JCClassDecl> syms2trees) throws IOException {
        block10 : {
            isTreeLoading.set(Boolean.TRUE);
            try {
                String binaryName = null;
                String surl = null;
                if (clazz.classfile != null) {
                    binaryName = jfm.inferBinaryName(StandardLocation.PLATFORM_CLASS_PATH, clazz.classfile);
                    if (binaryName == null) {
                        binaryName = jfm.inferBinaryName(StandardLocation.CLASS_PATH, clazz.classfile);
                    }
                    surl = clazz.classfile.toUri().toURL().toExternalForm();
                } else if (clazz.sourcefile != null) {
                    binaryName = jfm.inferBinaryName(StandardLocation.SOURCE_PATH, clazz.sourcefile);
                    surl = clazz.sourcefile.toUri().toURL().toExternalForm();
                }
                if (binaryName == null || surl == null) {
                    return;
                }
                int index = surl.lastIndexOf(FileObjects.convertPackage2Folder(binaryName));
                if (index > 0) {
                    File classes = JavaIndex.getClassFolder(new URL(surl.substring(0, index)));
                    TreeLoader.dumpSymFile(jfm, jti, clazz, classes, syms2trees);
                    break block10;
                }
                LOGGER.log(Level.INFO, "Invalid binary name when writing sym file for class: {0}, source: {1}, binary name {2}", new Object[]{clazz.flatname, surl, binaryName});
            }
            finally {
                isTreeLoading.remove();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void dumpSymFile(@NonNull JavaFileManager jfm, final @NonNull JavacTaskImpl jti, @NonNull Symbol.ClassSymbol clazz, @NonNull File classFolder, final @NonNull HashMap<Symbol.ClassSymbol, JCTree.JCClassDecl> syms2trees) throws IOException {
        Log log = Log.instance((Context)jti.getContext());
        JavaFileObject prevLogTo = log.useSource(null);
        Log.DiscardDiagnosticHandler discardDiagnosticHandler = new Log.DiscardDiagnosticHandler(log);
        TaskListener listener = new TaskListener(){

            public void started(TaskEvent e) {
                JCTree.JCClassDecl tree;
                if (e != null && e.getKind() == TaskEvent.Kind.GENERATE && (tree = (JCTree.JCClassDecl)syms2trees.get((Object)((Symbol.ClassSymbol)e.getTypeElement()))) != null) {
                    TreeLoader.pruneTree((JCTree)tree, Symtab.instance((Context)jti.getContext()), null);
                }
            }

            public void finished(TaskEvent e) {
            }
        };
        try {
            jfm.handleOption("output-root", Collections.singletonList(classFolder.getPath()).iterator());
            jti.addTaskListener(listener);
            jti.generate(Collections.singletonList(clazz));
        }
        catch (OutputFileManager.InvalidSourcePath isp) {
            LOGGER.log(Level.INFO, "InvalidSourcePath reported when writing sym file for class: {0}", (Object)clazz.flatname);
        }
        finally {
            jti.removeTaskListener(listener);
            jfm.handleOption("output-root", Collections.singletonList("").iterator());
            log.popDiagnosticHandler((Log.DiagnosticHandler)discardDiagnosticHandler);
            log.useSource(prevLogTo);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void dumpCouplingAbort(CouplingAbort couplingAbort, String treeInfo) {
        if (treeInfo == null) {
            treeInfo = TreeLoader.getTreeInfo(couplingAbort.getTree()).toString();
        }
        String dumpDir = System.getProperty("netbeans.user") + "/var/log/";
        JavaFileObject classFile = couplingAbort.getClassFile();
        String cfURI = classFile != null ? classFile.toUri().toASCIIString() : "<unknown>";
        JavaFileObject sourceFile = couplingAbort.getSourceFile();
        String sfURI = sourceFile != null ? sourceFile.toUri().toASCIIString() : "<unknown>";
        String origName = classFile != null ? classFile.getName() : "unknown";
        File f = new File(dumpDir + origName + ".dump");
        boolean dumpSucceeded = false;
        for (int i = 1; i < MAX_DUMPS && f.exists(); ++i) {
            f = new File(dumpDir + origName + '_' + i + ".dump");
        }
        if (!f.exists()) {
            try {
                f.getParentFile().mkdirs();
                FileOutputStream os = new FileOutputStream(f);
                PrintWriter writer = new PrintWriter(new OutputStreamWriter((OutputStream)os, "UTF-8"));
                try {
                    writer.println("Coupling error:");
                    writer.println(String.format("class file %s", cfURI));
                    writer.println(String.format("source file %s", sfURI));
                    writer.println("----- Source file content: ----------------------------------------");
                    if (sourceFile != null) {
                        try {
                            writer.println(sourceFile.getCharContent(true));
                        }
                        catch (UnsupportedOperationException uoe) {
                            writer.println("<unknown>");
                        }
                    } else {
                        writer.println("<unknown>");
                    }
                    writer.print("----- Trees: -------------------------------------------------------");
                    writer.println(treeInfo);
                    writer.println("----- Stack trace: ---------------------------------------------");
                    couplingAbort.printStackTrace(writer);
                }
                finally {
                    writer.close();
                    dumpSucceeded = true;
                }
            }
            catch (IOException ioe) {
                LOGGER.log(Level.INFO, "Error when writing coupling error dump file!", ioe);
            }
        }
        LOGGER.log(Level.WARNING, "Coupling error:\nclass file: {0}\nsource file: {1}{2}\n", new Object[]{cfURI, sfURI, treeInfo});
        if (!dumpSucceeded) {
            LOGGER.log(Level.WARNING, "Dump could not be written. Either dump file could not be created or all dump files were already used. Please check that you have write permission to '" + dumpDir + "' and " + "clean all *.dump files in that directory.");
        }
    }

    private static StringBuilder getTreeInfo(Tree t) {
        StringBuilder info = new StringBuilder("\n");
        if (t != null) {
            switch (t.getKind()) {
                case ANNOTATION_TYPE: 
                case CLASS: 
                case ENUM: 
                case INTERFACE: {
                    info.append("CLASS: ").append(((ClassTree)t).getSimpleName().toString());
                    break;
                }
                case VARIABLE: {
                    info.append("VARIABLE: ").append(((VariableTree)t).getName().toString());
                    break;
                }
                case METHOD: {
                    info.append("METHOD: ").append(((MethodTree)t).getName().toString());
                    break;
                }
                case TYPE_PARAMETER: {
                    info.append("TYPE_PARAMETER: ").append(((TypeParameterTree)t).getName().toString());
                    break;
                }
                default: {
                    info.append("TREE: <unknown>");
                }
            }
        }
        return info;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean getParamNamesFromJavadocText(JavadocHelper.TextStream page, final Symbol.ClassSymbol clazz) {
        InputStream is = null;
        String charset = null;
        do {
            ParserDelegator parser;
            try {
                is = page.openStream();
                InputStreamReader reader = charset == null ? new InputStreamReader(is) : new InputStreamReader(is, charset);
                parser = new ParserDelegator();
                parser.parse(reader, new HTMLEditorKit.ParserCallback(){
                    private int state;
                    private String signature;
                    private StringBuilder sb;

                    @Override
                    public void handleStartTag(HTML.Tag t, MutableAttributeSet a, int pos) {
                        if (t == HTML.Tag.A) {
                            String attrHref;
                            int idx;
                            String attrName = (String)a.getAttribute(HTML.Attribute.NAME);
                            if (attrName != null && ctor_summary_name.matcher(attrName).matches()) {
                                this.state = 10;
                            } else if (attrName != null && method_summary_name.matcher(attrName).matches()) {
                                this.state = 20;
                            } else if (attrName != null && field_detail_name.matcher(attrName).matches()) {
                                this.state = 30;
                            } else if (attrName != null && ctor_detail_name.matcher(attrName).matches()) {
                                this.state = 30;
                            } else if (attrName != null && method_detail_name.matcher(attrName).matches()) {
                                this.state = 30;
                            } else if ((this.state == 12 || this.state == 22) && (attrHref = (String)a.getAttribute(HTML.Attribute.HREF)) != null && (idx = attrHref.indexOf(35)) >= 0) {
                                this.signature = attrHref.substring(idx + 1);
                                this.sb = new StringBuilder();
                            }
                        } else if (t == HTML.Tag.TABLE) {
                            if (this.state == 10 || this.state == 20) {
                                ++this.state;
                            }
                        } else if (t == HTML.Tag.CODE) {
                            if (this.state == 11 || this.state == 21) {
                                ++this.state;
                            }
                        } else if (t == HTML.Tag.DIV && a.containsAttribute(HTML.Attribute.CLASS, "block")) {
                            if (this.state == 11 && this.signature != null && this.sb != null) {
                                this.setParamNames(this.signature, this.sb.toString().trim(), true);
                                this.signature = null;
                                this.sb = null;
                            } else if (this.state == 21 && this.signature != null && this.sb != null) {
                                this.setParamNames(this.signature, this.sb.toString().trim(), false);
                                this.signature = null;
                                this.sb = null;
                            }
                        }
                    }

                    @Override
                    public void handleEndTag(HTML.Tag t, int pos) {
                        if (t == HTML.Tag.CODE) {
                            if (this.state == 12 || this.state == 22) {
                                --this.state;
                            }
                        } else if (t == HTML.Tag.TABLE && (this.state == 11 || this.state == 21)) {
                            --this.state;
                        }
                    }

                    @Override
                    public void handleText(char[] data, int pos) {
                        if (this.signature != null && this.sb != null && (this.state == 12 || this.state == 22)) {
                            this.sb.append(data);
                        }
                    }

                    @Override
                    public void handleSimpleTag(HTML.Tag t, MutableAttributeSet a, int pos) {
                        if (t == HTML.Tag.BR) {
                            if (this.state == 11 && this.signature != null && this.sb != null) {
                                this.setParamNames(this.signature, this.sb.toString().trim(), true);
                                this.signature = null;
                                this.sb = null;
                            } else if (this.state == 21 && this.signature != null && this.sb != null) {
                                this.setParamNames(this.signature, this.sb.toString().trim(), false);
                                this.signature = null;
                                this.sb = null;
                            }
                        }
                    }

                    private void setParamNames(String signature, String names, boolean isCtor) {
                        ArrayList<String> paramTypes = new ArrayList<String>();
                        int idx = -1;
                        for (int i = 0; i < signature.length(); ++i) {
                            switch (signature.charAt(i)) {
                                case '(': 
                                case ')': 
                                case ',': 
                                case '-': {
                                    if (idx > -1 && idx < i - 1) {
                                        String typeName = signature.substring(idx + 1, i).trim();
                                        if (typeName.endsWith("...")) {
                                            typeName = typeName.substring(0, typeName.length() - 3) + "[]";
                                        }
                                        paramTypes.add(typeName);
                                    }
                                    idx = i;
                                }
                            }
                        }
                        String methodName = null;
                        ArrayList<String> paramNames = new ArrayList<String>();
                        idx = -1;
                        block9 : for (int i2 = 0; i2 < names.length(); ++i2) {
                            switch (names.charAt(i2)) {
                                case '(': {
                                    methodName = names.substring(0, i2);
                                    continue block9;
                                }
                                case ')': 
                                case ',': {
                                    if (idx <= -1) continue block9;
                                    paramNames.add(names.substring(idx + 1, i2));
                                    idx = -1;
                                    continue block9;
                                }
                                case '\u00a0': {
                                    idx = i2;
                                }
                            }
                        }
                        assert (methodName != null);
                        assert (paramTypes.size() == paramNames.size());
                        if (paramNames.size() > 0) {
                            Scope.Entry e = clazz.members().lookup(isCtor ? clazz.name.table.names.init : clazz.name.table.fromString(methodName));
                            while (e.scope != null) {
                                if (e.sym.kind == 16 && e.sym.owner == clazz) {
                                    Symbol.MethodSymbol sym = (Symbol.MethodSymbol)e.sym;
                                    List params = sym.params;
                                    if (this.checkParamTypes(params, paramTypes)) {
                                        for (String name : paramNames) {
                                            ((Symbol.VarSymbol)params.head).setName(clazz.name.table.fromString(name));
                                            params = params.tail;
                                        }
                                    }
                                }
                                e = e.next();
                            }
                        }
                    }

                    private boolean checkParamTypes(List<Symbol.VarSymbol> params, ArrayList<String> paramTypes) {
                        Types types = Types.instance((Context)TreeLoader.this.context);
                        for (String typeName : paramTypes) {
                            if (params.isEmpty()) {
                                return false;
                            }
                            Type type = ((Symbol.VarSymbol)params.head).type;
                            if (type.isParameterized()) {
                                type = types.erasure(type);
                            }
                            if (!typeName.equals(type.toString())) {
                                return false;
                            }
                            params = params.tail;
                        }
                        return params.isEmpty();
                    }
                }, charset != null);
                boolean bl = true;
                return bl;
            }
            catch (ChangedCharSetException e) {
                if (charset == null) {
                    charset = JavadocHelper.getCharSet(e);
                    continue;
                }
                e.printStackTrace();
            }
            catch (IOException ioe) {
                ioe.printStackTrace();
            }
            finally {
                parser = null;
                if (is == null) continue;
                try {
                    is.close();
                    continue;
                }
                catch (IOException ioe) {
                    ioe.printStackTrace();
                }
                continue;
            }
            break;
        } while (true);
        return false;
    }

    private void fillArtificalParamNames(Symbol.ClassSymbol clazz) {
        for (Symbol s : clazz.getEnclosedElements()) {
            Symbol.MethodSymbol ms;
            if (!(s instanceof Symbol.MethodSymbol) || (ms = (Symbol.MethodSymbol)s).getParameters().isEmpty()) continue;
            HashSet<String> usedNames = new HashSet<String>();
            for (Symbol.VarSymbol vs : ms.getParameters()) {
                String name = JavaSourceAccessor.getINSTANCE().generateReadableParameterName(vs.asType().toString(), usedNames);
                vs.setName(clazz.name.table.fromString(name));
            }
        }
    }

    private synchronized boolean attachTo(Thread thread) {
        assert (thread != null);
        if (!this.checkContention) {
            return true;
        }
        if (this.owner == null) {
            assert (this.depth == 0);
            this.owner = thread;
            ++this.depth;
            return true;
        }
        if (this.owner == thread) {
            assert (this.depth > 0);
            ++this.depth;
            return true;
        }
        assert (this.depth > 0);
        return false;
    }

    private synchronized boolean dettachFrom(Thread thread) {
        assert (thread != null);
        if (!this.checkContention) {
            return true;
        }
        if (this.owner == thread) {
            assert (this.depth > 0);
            --this.depth;
            if (this.depth == 0) {
                this.owner = null;
            }
            return true;
        }
        return false;
    }

    private boolean canWrite(ClasspathInfo cpInfo) {
        FileManagerTransaction fmTx = ClasspathInfoAccessor.getINSTANCE().getFileManagerTransaction(cpInfo);
        assert (fmTx != null);
        return fmTx.canWrite();
    }

}

