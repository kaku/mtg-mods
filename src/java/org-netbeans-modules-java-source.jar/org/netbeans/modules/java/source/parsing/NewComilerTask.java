/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Embedding
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 */
package org.netbeans.modules.java.source.parsing;

import java.util.LinkedList;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.modules.java.source.parsing.ClasspathInfoTask;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;

public class NewComilerTask
extends ClasspathInfoTask {
    private CompilationController result;
    private long timestamp;

    public NewComilerTask(ClasspathInfo cpInfo, CompilationController last, long timestamp) {
        super(cpInfo);
        this.result = last;
        this.timestamp = timestamp;
    }

    public void run(ResultIterator resultIterator) throws Exception {
        Snapshot snapshot = resultIterator.getSnapshot();
        if ("text/x-java".equals(snapshot.getMimeType())) {
            resultIterator.getParserResult();
        } else {
            this.findEmbeddedJava(resultIterator);
        }
    }

    private Parser.Result findEmbeddedJava(ResultIterator theMess) throws ParseException {
        LinkedList<Embedding> todo = new LinkedList<Embedding>();
        for (Embedding embedding2 : theMess.getEmbeddings()) {
            if ("text/x-java".equals(embedding2.getMimeType())) {
                return theMess.getResultIterator(embedding2).getParserResult();
            }
            todo.add(embedding2);
        }
        for (Embedding embedding2 : todo) {
            Parser.Result result = this.findEmbeddedJava(theMess.getResultIterator(embedding2));
            if (result == null) continue;
            return result;
        }
        return null;
    }

    public void setCompilationController(CompilationController result, long timestamp) {
        assert (result != null);
        this.result = result;
        this.timestamp = timestamp;
    }

    public CompilationController getCompilationController() {
        return this.result;
    }

    public long getTimeStamp() {
        return this.timestamp;
    }
}

