/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.guards.GuardedSection
 *  org.netbeans.api.editor.guards.GuardedSectionManager
 *  org.netbeans.api.editor.guards.InteriorSection
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 */
package org.netbeans.modules.java.source.save;

import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Iterator;
import javax.swing.text.Document;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.netbeans.api.editor.guards.GuardedSection;
import org.netbeans.api.editor.guards.GuardedSectionManager;
import org.netbeans.api.editor.guards.InteriorSection;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.java.source.save.PositionEstimator;

public final class BlockSequences {
    private final TokenSequence seq;
    private final Document doc;
    private int[] boundOffsets;
    private int max;
    private int len;

    BlockSequences(TokenSequence seq, Document doc, int textLen) {
        this.doc = doc;
        this.seq = seq;
        this.len = textLen;
        this.initialize();
    }

    public Iterator<Integer> getBoundaries() {
        return this.boundOffsets == null ? Collections.emptyList().iterator() : new Iterator<Integer>(){
            int p;

            @Override
            public boolean hasNext() {
                return this.p < BlockSequences.this.max;
            }

            @Override
            public Integer next() {
                return BlockSequences.this.boundOffsets[this.p++];
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported");
            }
        };
    }

    public boolean isWritable(int anchor) {
        if (this.boundOffsets == null) {
            return true;
        }
        int index = this.findSectionIndex(anchor);
        int s = this.boundOffsets[index];
        if (anchor >= s) {
            int e = this.boundOffsets[index];
            return anchor >= e;
        }
        return true;
    }

    public int findNextWritablePos(int anchor) {
        if (this.boundOffsets == null) {
            return anchor;
        }
        int index = this.findSectionIndex(anchor);
        int s = this.boundOffsets[index];
        if (anchor < s) {
            return anchor;
        }
        if (anchor == s) {
            return anchor - 1;
        }
        int e = this.boundOffsets[index];
        if (anchor > e) {
            return anchor;
        }
        this.seq.move(anchor);
        while (this.seq.moveNext() && PositionEstimator.nonRelevant.contains((Object)this.seq.token().id())) {
        }
        if (this.seq.offset() < e) {
            return anchor;
        }
        return e;
    }

    public int findSectionEnd(int anchor) {
        if (this.boundOffsets == null) {
            return this.len;
        }
        int index = this.findSectionIndex(anchor);
        int s = this.boundOffsets[index];
        if (anchor < s) {
            return s;
        }
        int e = this.boundOffsets[index];
        if (anchor < e) {
            return e;
        }
        return this.len;
    }

    private int findSectionIndex(int fromOffset) {
        int hi = this.max - 2;
        int lo = 0;
        if (fromOffset < this.boundOffsets[lo]) {
            return lo;
        }
        if (fromOffset >= this.boundOffsets[hi + 1]) {
            return hi;
        }
        while (lo < hi) {
            int mid = (hi + lo) / 2 & -2;
            if (fromOffset < this.boundOffsets[mid]) {
                hi = mid - 2;
                if (fromOffset < this.boundOffsets[hi + 1]) continue;
                return hi;
            }
            if (fromOffset >= this.boundOffsets[mid + 1]) {
                lo = mid + 2;
                if (fromOffset >= this.boundOffsets[lo]) continue;
                return lo;
            }
            return mid;
        }
        return lo;
    }

    private void initialize() {
        if (!(this.doc instanceof StyledDocument)) {
            return;
        }
        GuardedSectionManager mgr = GuardedSectionManager.getInstance((StyledDocument)((StyledDocument)this.doc));
        if (mgr == null) {
            return;
        }
        this.len = this.doc.getLength();
        int[] arr = new int[10];
        int p = 0;
        for (GuardedSection s : mgr.getGuardedSections()) {
            if (s instanceof InteriorSection) {
                InteriorSection is = (InteriorSection)s;
                arr = BlockSequences.ensureSize(arr, p + 2);
                arr[p++] = is.getStartPosition().getOffset();
                arr[p++] = is.getBodyStartPosition().getOffset();
                arr[p++] = is.getBodyEndPosition().getOffset() + 1;
                arr[p++] = is.getEndPosition().getOffset() + 1;
                continue;
            }
            arr = BlockSequences.ensureSize(arr, p);
            arr[p++] = s.getStartPosition().getOffset();
            arr[p++] = s.getEndPosition().getOffset() + 1;
        }
        if (p == 0) {
            return;
        }
        this.max = p;
        this.boundOffsets = arr;
    }

    private static int[] ensureSize(int[] arr, int p) {
        if (arr.length > p + 1) {
            return arr;
        }
        return Arrays.copyOf(arr, p * 2);
    }

}

