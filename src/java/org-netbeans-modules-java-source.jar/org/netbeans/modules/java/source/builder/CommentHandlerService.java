/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.Tree
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Key
 */
package org.netbeans.modules.java.source.builder;

import com.sun.source.tree.Tree;
import com.sun.tools.javac.util.Context;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.java.source.Comment;
import org.netbeans.modules.java.source.builder.CommentSetImpl;
import org.netbeans.modules.java.source.query.CommentHandler;
import org.netbeans.modules.java.source.query.CommentSet;

public class CommentHandlerService
implements CommentHandler {
    private static final Context.Key<CommentHandlerService> commentHandlerKey = new Context.Key();
    private final Map<Tree, CommentSetImpl> map = new HashMap<Tree, CommentSetImpl>();

    public static CommentHandlerService instance(Context context) {
        CommentHandlerService instance = (CommentHandlerService)context.get(commentHandlerKey);
        if (instance == null) {
            instance = new CommentHandlerService(context);
            CommentHandlerService.setCommentHandler(context, instance);
        }
        return instance;
    }

    public static void setCommentHandler(Context context, CommentHandlerService instance) {
        assert (context.get(commentHandlerKey) == null);
        context.put(commentHandlerKey, (Object)instance);
    }

    private CommentHandlerService(Context context) {
    }

    Map<Tree, CommentSetImpl> getCommentMap() {
        HashMap<Tree, CommentSetImpl> m = new HashMap<Tree, CommentSetImpl>(this.map);
        Iterator<Map.Entry<Tree, CommentSetImpl>> it = m.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Tree, CommentSetImpl> e = it.next();
            if (e.getValue().hasComments()) continue;
            it.remove();
        }
        return m;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean hasComments(Tree tree) {
        Map<Tree, CommentSetImpl> map = this.map;
        synchronized (map) {
            return this.map.containsKey((Object)tree);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public CommentSetImpl getComments(Tree tree) {
        Map<Tree, CommentSetImpl> map = this.map;
        synchronized (map) {
            CommentSetImpl cs = this.map.get((Object)tree);
            if (cs == null) {
                cs = new CommentSetImpl();
                this.map.put(tree, cs);
            }
            return cs;
        }
    }

    @Override
    public void copyComments(Tree fromTree, Tree toTree) {
        this.copyComments(fromTree, toTree, null, null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void copyComments(Tree fromTree, Tree toTree, CommentSet.RelativePosition copyToPos, Collection<Comment> copied) {
        if (fromTree == toTree) {
            return;
        }
        Map<Tree, CommentSetImpl> map = this.map;
        synchronized (map) {
            CommentSetImpl from = this.map.get((Object)fromTree);
            if (from != null) {
                CommentSetImpl to = this.map.get((Object)toTree);
                if (to == null) {
                    to = new CommentSetImpl();
                    this.map.put(toTree, to);
                }
                for (CommentSet.RelativePosition pos : CommentSet.RelativePosition.values()) {
                    for (Comment c : from.getComments(pos)) {
                        if (copied != null && !copied.add(c)) continue;
                        to.addComment(copyToPos == null ? pos : copyToPos, c, true);
                    }
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void addComment(Tree tree, Comment c) {
        Map<Tree, CommentSetImpl> map = this.map;
        synchronized (map) {
            CommentSetImpl set = this.map.get((Object)tree);
            if (set == null) {
                set = new CommentSetImpl();
                this.map.put(tree, set);
            }
            set.addPrecedingComment(c);
        }
    }

    public String toString() {
        return "CommentHandlerService[map=" + this.map + ']';
    }
}

