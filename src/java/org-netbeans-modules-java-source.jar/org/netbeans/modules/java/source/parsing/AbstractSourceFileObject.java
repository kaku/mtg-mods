/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.lexer.InputAttributes
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.queries.FileEncodingQuery
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.JarFileSystem
 *  org.openide.util.Lookup
 *  org.openide.util.Parameters
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.source.parsing;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.net.URI;
import java.net.URL;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.NestingKind;
import javax.swing.text.Document;
import javax.tools.JavaFileObject;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.queries.FileEncodingQuery;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.source.parsing.DefaultSourceFileObjectProvider;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.PrefetchableJavaFileObject;
import org.netbeans.modules.java.source.parsing.SourceFileManager;
import org.netbeans.modules.java.source.parsing.SourceFileObjectProvider;
import org.netbeans.modules.parsing.api.Source;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.JarFileSystem;
import org.openide.util.Lookup;
import org.openide.util.Parameters;
import org.openide.util.Utilities;

public abstract class AbstractSourceFileObject
implements PrefetchableJavaFileObject {
    private static final Logger LOG = Logger.getLogger(AbstractSourceFileObject.class.getName());
    private static SourceFileObjectProvider factory;
    private final Handle handle;
    private final JavaFileFilterImplementation filter;
    private final JavaFileObject.Kind kind;
    private String text;
    private TokenHierarchy<?> tokens;
    private URI uri;

    protected AbstractSourceFileObject(@NonNull Handle handle, @NullAllowed JavaFileFilterImplementation filter) {
        Parameters.notNull((CharSequence)"handle", (Object)handle);
        this.handle = handle;
        this.filter = filter;
        String ext = this.handle.getExt();
        this.kind = filter == null ? FileObjects.getKind(ext) : JavaFileObject.Kind.SOURCE;
    }

    @NonNull
    @Override
    public final JavaFileObject.Kind getKind() {
        return this.kind;
    }

    @NonNull
    @Override
    public final String getName() {
        return this.handle.getName(true);
    }

    @CheckForNull
    @Override
    public final NestingKind getNestingKind() {
        return null;
    }

    @CheckForNull
    @Override
    public final Modifier getAccessLevel() {
        return null;
    }

    @Override
    public final boolean isNameCompatible(@NonNull String simplename, @NonNull JavaFileObject.Kind kind) {
        assert (simplename != null);
        return this.kind == kind && this.getNameWithoutExtension().equals(simplename);
    }

    @NonNull
    @Override
    public final CharBuffer getCharContent(boolean ignoreEncodingErrors) throws IOException {
        String _text = this.text;
        if (_text == null) {
            _text = this.getContent(false);
        }
        return CharBuffer.wrap(_text);
    }

    @NonNull
    @Override
    public final Writer openWriter() throws IOException {
        FileObject file = this.handle.resolveFileObject(true);
        if (file == null) {
            throw new IOException("Cannot create file: " + this.toString());
        }
        return new OutputStreamWriter(this.openOutputStream(), FileEncodingQuery.getEncoding((FileObject)file));
    }

    @NonNull
    @Override
    public final Reader openReader(boolean ignoreEncodingErrors) throws IOException {
        String _text = this.text;
        if (_text == null) {
            _text = this.getContent(false);
        }
        return new StringReader(_text);
    }

    @NonNull
    @Override
    public final OutputStream openOutputStream() throws IOException {
        FileObject file = this.handle.resolveFileObject(true);
        if (file == null) {
            throw new IOException("Cannot create file: " + this.toString());
        }
        OutputStream res = this.createOutputStream();
        if (res == null) {
            res = new LckStream(file);
        }
        return res;
    }

    @NonNull
    @Override
    public final InputStream openInputStream() throws IOException {
        String _text = this.text;
        if (_text == null) {
            _text = this.getContent(false);
        }
        return new ByteArrayInputStream(_text.getBytes());
    }

    @Override
    public long getLastModified() {
        if (this.isModifiedByWorkingCopy()) {
            return System.currentTimeMillis();
        }
        Long dirty = this.isDirty();
        if (dirty != null) {
            return dirty;
        }
        return this.getFileLastModified();
    }

    @Override
    public final URI toUri() {
        if (this.uri == null) {
            try {
                this.uri = URI.create(this.handle.getURL().toExternalForm());
            }
            catch (IOException e) {
                LOG.log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return this.uri;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public final boolean delete() {
        boolean bl;
        if (this.isDirty() != null) {
            return false;
        }
        FileObject file = this.handle.resolveFileObject(false);
        if (file == null) {
            return false;
        }
        FileLock lock = file.lock();
        try {
            file.delete(lock);
            bl = true;
        }
        catch (Throwable var4_5) {
            try {
                lock.releaseLock();
                throw var4_5;
            }
            catch (IOException e) {
                return false;
            }
        }
        lock.releaseLock();
        return bl;
    }

    @CheckForNull
    @Override
    public final String inferBinaryName() {
        if (this.handle.root == null) {
            return null;
        }
        String relativePath = this.handle.getRelativePath();
        assert (relativePath != null);
        int index = relativePath.lastIndexOf(46);
        assert (index > 0);
        String result = relativePath.substring(0, index).replace('/', '.');
        return result;
    }

    @Override
    public final int prefetch() throws IOException {
        return 0;
    }

    @Override
    public final int dispose() {
        return 0;
    }

    @NonNull
    public final String toString() {
        URI uri = this.toUri();
        try {
            File file = Utilities.toFile((URI)uri);
            return file.getAbsolutePath();
        }
        catch (IllegalArgumentException iae) {
            return uri.toString();
        }
    }

    public final boolean equals(@NullAllowed Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof AbstractSourceFileObject)) {
            return false;
        }
        AbstractSourceFileObject otherSource = (AbstractSourceFileObject)other;
        return this.handle.equals(otherSource.handle);
    }

    public final int hashCode() {
        return this.handle.hashCode();
    }

    @NonNull
    public final String getNameWithoutExtension() {
        return this.handle.getName(false);
    }

    @NonNull
    public final TokenHierarchy<?> getTokenHierarchy() throws IOException {
        if (this.tokens == null) {
            CharBuffer charBuffer = this.getCharContent(true);
            this.tokens = TokenHierarchy.create((CharSequence)charBuffer, (boolean)false, (Language)JavaTokenId.language(), (Set)null, (InputAttributes)null);
        }
        return this.tokens;
    }

    public final void update() throws IOException {
        if (this.kind != JavaFileObject.Kind.CLASS) {
            this.getContent(true);
        }
    }

    public final void update(CharSequence content) throws IOException {
        if (content == null) {
            this.update();
        } else {
            FileObject file;
            Source source;
            if (this.filter != null && (file = this.handle.resolveFileObject(false)) != null && (source = Source.create((FileObject)file)) != null && source.getDocument(false) == null) {
                content = this.filter.filterCharSequence(content);
            }
            this.text = AbstractSourceFileObject.toString(content);
        }
        this.tokens = null;
    }

    @NonNull
    protected final Handle getHandle() {
        return this.handle;
    }

    protected final void resetCaches() {
        this.text = null;
    }

    @NonNull
    protected final CharSequence filter(@NonNull CharSequence text) {
        return this.filter != null ? this.filter.filterCharSequence(text) : text;
    }

    protected abstract Long isDirty();

    @CheckForNull
    protected abstract OutputStream createOutputStream() throws IOException;

    @NonNull
    protected abstract CharSequence createContent() throws IOException;

    @NonNull
    private String getContent(boolean assign) throws IOException {
        CharSequence content = this.createContent();
        String result = AbstractSourceFileObject.toString(content);
        if (assign) {
            this.text = result;
        }
        return result;
    }

    @NonNull
    private static String toString(@NonNull CharSequence c) {
        if (c instanceof String) {
            return (String)c;
        }
        return c.toString();
    }

    private boolean isModifiedByWorkingCopy() {
        FileObject file = this.handle.resolveFileObject(false);
        if (file == null) {
            return false;
        }
        return SourceFileManager.getModifiedFiles().isModified(file.toURI());
    }

    private long getFileLastModified() {
        FileObject file = this.handle.resolveFileObject(false);
        try {
            if (file == null || file.getFileSystem() instanceof JarFileSystem) {
                return 0;
            }
        }
        catch (FileStateInvalidException e) {
            // empty catch block
        }
        return file.lastModified().getTime();
    }

    @NonNull
    public static synchronized SourceFileObjectProvider getFactory() {
        if (factory == null && (AbstractSourceFileObject.factory = (SourceFileObjectProvider)Lookup.getDefault().lookup(SourceFileObjectProvider.class)) == null) {
            factory = new DefaultSourceFileObjectProvider();
        }
        return factory;
    }

    private class LckStream
    extends OutputStream {
        private final OutputStream delegate;
        private final FileLock lock;

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public LckStream(FileObject fo) throws IOException {
            assert (fo != null);
            this.lock = fo.lock();
            try {
                this.delegate = fo.getOutputStream(this.lock);
            }
            finally {
                if (this.delegate == null) {
                    this.lock.releaseLock();
                }
            }
        }

        @Override
        public void write(byte[] b, int off, int len) throws IOException {
            this.delegate.write(b, off, len);
        }

        @Override
        public void write(byte[] b) throws IOException {
            this.delegate.write(b);
        }

        @Override
        public void write(int b) throws IOException {
            this.delegate.write(b);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void close() throws IOException {
            try {
                this.delegate.close();
            }
            finally {
                this.lock.releaseLock();
                AbstractSourceFileObject.this.resetCaches();
            }
        }
    }

    public static class Handle {
        protected final FileObject root;
        protected FileObject file;

        protected Handle(FileObject root) {
            this.root = root;
        }

        public Handle(FileObject file, FileObject root) {
            assert (file != null);
            this.file = file;
            this.root = root;
        }

        public FileObject resolveFileObject(boolean write) {
            return this.file;
        }

        public URL getURL() throws IOException {
            return this.file == null ? null : this.file.toURL();
        }

        public String getExt() {
            return this.file == null ? null : this.file.getExt();
        }

        public String getName(boolean includeExtension) {
            return this.file == null ? null : (includeExtension ? this.file.getNameExt() : this.file.getName());
        }

        public String getRelativePath() {
            if (this.file == null) {
                return null;
            }
            String result = FileUtil.getRelativePath((FileObject)this.root, (FileObject)this.file);
            assert (result != null);
            return result;
        }

        public int hashCode() {
            return this.file == null ? 0 : this.file.hashCode();
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (this.getClass() != obj.getClass()) {
                return false;
            }
            Handle other = (Handle)obj;
            return this.file == null ? other.file == null : this.file.equals((Object)other.file);
        }
    }

}

