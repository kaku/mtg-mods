/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.java.source;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public final class JavaFileFilterQuery {
    private static JavaFileFilterImplementation unitTestFilter;
    private static Reference<FileObject> key;
    private static Reference<JavaFileFilterImplementation> value;

    private JavaFileFilterQuery() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static JavaFileFilterImplementation getFilter(FileObject fo) {
        assert (fo != null);
        if (unitTestFilter != null) {
            return unitTestFilter;
        }
        Class<JavaFileFilterImplementation> class_ = JavaFileFilterImplementation.class;
        synchronized (JavaFileFilterImplementation.class) {
            JavaFileFilterImplementation impl;
            Object _value;
            FileObject _key = key == null ? null : key.get();
            Object object = _value = value == null ? null : value.get();
            if (_key != null && _key.equals((Object)fo) && _value != null) {
                // ** MonitorExit[var1_1] (shouldn't be in output)
                return _value;
            }
            // ** MonitorExit[var1_1] (shouldn't be in output)
            Project p = FileOwnerQuery.getOwner((FileObject)fo);
            if (p != null && (impl = (JavaFileFilterImplementation)p.getLookup().lookup(JavaFileFilterImplementation.class)) != null) {
                _value = JavaFileFilterImplementation.class;
                synchronized (JavaFileFilterImplementation.class) {
                    key = new WeakReference<FileObject>(fo);
                    value = new WeakReference<JavaFileFilterImplementation>(impl);
                    // ** MonitorExit[_value] (shouldn't be in output)
                    return impl;
                }
            }
            return null;
        }
    }

    static void setTestFileFilter(JavaFileFilterImplementation testFilter) {
        unitTestFilter = testFilter;
    }
}

