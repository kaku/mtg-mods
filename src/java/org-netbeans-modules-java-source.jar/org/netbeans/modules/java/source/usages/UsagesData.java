/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.parsing.lucene.support.Convertor
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.java.source.usages;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;
import org.netbeans.modules.java.source.usages.DocumentUtil;
import org.netbeans.modules.parsing.lucene.support.Convertor;
import org.openide.util.Parameters;

final class UsagesData<T> {
    private final Convertor<? super T, String> convertor;
    private final Map<T, Set<ClassIndexImpl.UsageType>> usages = new HashMap<T, Set<ClassIndexImpl.UsageType>>();
    private final Set<CharSequence> featuresIdents = new HashSet<CharSequence>();
    private final Set<CharSequence> idents = new HashSet<CharSequence>();

    UsagesData(@NonNull Convertor<? super T, String> convertor) {
        Parameters.notNull((CharSequence)"convertor", convertor);
        this.convertor = convertor;
    }

    void addFeatureIdent(@NonNull CharSequence ident) {
        this.featuresIdents.add(ident);
    }

    void addIdent(@NonNull CharSequence ident) {
        this.idents.add(ident);
    }

    void addUsage(@NonNull T className, @NonNull ClassIndexImpl.UsageType type) {
        Set<ClassIndexImpl.UsageType> usageType = this.usages.get(className);
        if (usageType == null) {
            usageType = EnumSet.of(type);
            this.usages.put(className, usageType);
        } else {
            usageType.add(type);
        }
    }

    /* varargs */ void addUsages(@NonNull T className, @NonNull ClassIndexImpl.UsageType ... types) {
        Set<ClassIndexImpl.UsageType> usageType = this.usages.get(className);
        if (usageType == null) {
            usageType = EnumSet.noneOf(ClassIndexImpl.UsageType.class);
            this.usages.put(className, usageType);
        }
        for (ClassIndexImpl.UsageType type : types) {
            usageType.add(type);
        }
    }

    boolean hasUsage(@NonNull T name) {
        return this.usages.containsKey(name);
    }

    String featureIdentsToString() {
        return this.toString(this.featuresIdents);
    }

    String identsToString() {
        return this.toString(this.idents);
    }

    List<String> usagesToStrings() {
        ArrayList<String> result = new ArrayList<String>();
        for (Map.Entry<T, Set<ClassIndexImpl.UsageType>> entry : this.usages.entrySet()) {
            result.add(DocumentUtil.encodeUsage((String)this.convertor.convert(entry.getKey()), entry.getValue()));
        }
        return result;
    }

    private String toString(@NonNull Set<? extends CharSequence> data) {
        StringBuilder sb = new StringBuilder();
        for (CharSequence id : data) {
            sb.append(id);
            sb.append(' ');
        }
        return sb.toString();
    }
}

