/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.impl.indexing.friendapi.IndexingController
 *  org.netbeans.modules.refactoring.api.AbstractRefactoring
 *  org.netbeans.modules.refactoring.api.Problem
 *  org.netbeans.modules.refactoring.api.ProgressEvent
 *  org.netbeans.modules.refactoring.api.ProgressListener
 *  org.netbeans.modules.refactoring.api.RefactoringSession
 *  org.netbeans.modules.refactoring.spi.RefactoringElementsBag
 *  org.netbeans.modules.refactoring.spi.RefactoringPlugin
 *  org.netbeans.modules.refactoring.spi.RefactoringPluginFactory
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.java.source.usages;

import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.modules.java.source.JavaSourceAccessor;
import org.netbeans.modules.java.source.JavaSourceSupportAccessor;
import org.netbeans.modules.parsing.impl.indexing.friendapi.IndexingController;
import org.netbeans.modules.refactoring.api.AbstractRefactoring;
import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.api.ProgressEvent;
import org.netbeans.modules.refactoring.api.ProgressListener;
import org.netbeans.modules.refactoring.api.RefactoringSession;
import org.netbeans.modules.refactoring.spi.RefactoringElementsBag;
import org.netbeans.modules.refactoring.spi.RefactoringPlugin;
import org.netbeans.modules.refactoring.spi.RefactoringPluginFactory;
import org.openide.filesystems.FileObject;

public class AllRefactoringsPluginFactory
implements RefactoringPluginFactory {
    private static final Logger LOGGER = Logger.getLogger(AllRefactoringsPluginFactory.class.getName());

    public RefactoringPlugin createInstance(AbstractRefactoring refactoring) {
        return new RefactoringPluginImpl();
    }

    private static final class RefactoringPluginImpl
    implements RefactoringPlugin {
        private RefactoringPluginImpl() {
        }

        public Problem preCheck() {
            return null;
        }

        public Problem checkParameters() {
            return null;
        }

        public Problem fastCheckParameters() {
            return null;
        }

        public void cancelRequest() {
        }

        public Problem prepare(RefactoringElementsBag refactoringElements) {
            refactoringElements.getSession().addProgressListener(new ProgressListener(){

                public void start(ProgressEvent event) {
                    LOGGER.log(Level.FINE, "Refactoring started, locking RepositoryUpdater");
                    IndexingController.getDefault().enterProtectedMode();
                }

                public void step(ProgressEvent event) {
                }

                public void stop(ProgressEvent event) {
                    LOGGER.log(Level.FINE, "Refactoring finished, unlocking RepositoryUpdater");
                    IndexingController.getDefault().exitProtectedMode(new Runnable(){

                        @Override
                        public void run() {
                            LOGGER.log(Level.FINE, "Refreshing editor panes:");
                            for (FileObject f : JavaSourceSupportAccessor.ACCESSOR.getVisibleEditorsFiles()) {
                                JavaSource source = JavaSource.forFileObject(f);
                                if (LOGGER.isLoggable(Level.FINE)) {
                                    LOGGER.log(Level.FINE, "Refreshing file={0}, JavaSource={1}", new Object[]{f, source});
                                }
                                if (source == null) continue;
                                JavaSourceAccessor.getINSTANCE().revalidate(source);
                            }
                            LOGGER.log(Level.FINE, "done.");
                        }
                    });
                }

            });
            return null;
        }

    }

}

