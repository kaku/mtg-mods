/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.doctree.DocCommentTree
 *  com.sun.source.doctree.DocTree
 *  com.sun.source.doctree.DocTree$Kind
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.DocSourcePositions
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreeScanner
 *  com.sun.source.util.Trees
 *  com.sun.tools.javac.api.JavacTrees
 *  com.sun.tools.javac.code.BoundKind
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.code.Symbol$MethodSymbol
 *  com.sun.tools.javac.code.Symbol$VarSymbol
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.code.TypeTag
 *  com.sun.tools.javac.parser.Tokens
 *  com.sun.tools.javac.parser.Tokens$Comment
 *  com.sun.tools.javac.tree.DCTree
 *  com.sun.tools.javac.tree.DCTree$DCAttribute
 *  com.sun.tools.javac.tree.DCTree$DCAuthor
 *  com.sun.tools.javac.tree.DCTree$DCComment
 *  com.sun.tools.javac.tree.DCTree$DCDeprecated
 *  com.sun.tools.javac.tree.DCTree$DCDocComment
 *  com.sun.tools.javac.tree.DCTree$DCDocRoot
 *  com.sun.tools.javac.tree.DCTree$DCEndElement
 *  com.sun.tools.javac.tree.DCTree$DCEntity
 *  com.sun.tools.javac.tree.DCTree$DCErroneous
 *  com.sun.tools.javac.tree.DCTree$DCIdentifier
 *  com.sun.tools.javac.tree.DCTree$DCInheritDoc
 *  com.sun.tools.javac.tree.DCTree$DCLink
 *  com.sun.tools.javac.tree.DCTree$DCLiteral
 *  com.sun.tools.javac.tree.DCTree$DCParam
 *  com.sun.tools.javac.tree.DCTree$DCReference
 *  com.sun.tools.javac.tree.DCTree$DCReturn
 *  com.sun.tools.javac.tree.DCTree$DCSee
 *  com.sun.tools.javac.tree.DCTree$DCSerial
 *  com.sun.tools.javac.tree.DCTree$DCSerialData
 *  com.sun.tools.javac.tree.DCTree$DCSerialField
 *  com.sun.tools.javac.tree.DCTree$DCSince
 *  com.sun.tools.javac.tree.DCTree$DCStartElement
 *  com.sun.tools.javac.tree.DCTree$DCText
 *  com.sun.tools.javac.tree.DCTree$DCThrows
 *  com.sun.tools.javac.tree.DCTree$DCUnknownBlockTag
 *  com.sun.tools.javac.tree.DCTree$DCUnknownInlineTag
 *  com.sun.tools.javac.tree.DCTree$DCValue
 *  com.sun.tools.javac.tree.DCTree$DCVersion
 *  com.sun.tools.javac.tree.DocCommentTable
 *  com.sun.tools.javac.tree.EndPosTable
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCAnnotatedType
 *  com.sun.tools.javac.tree.JCTree$JCAnnotation
 *  com.sun.tools.javac.tree.JCTree$JCArrayAccess
 *  com.sun.tools.javac.tree.JCTree$JCArrayTypeTree
 *  com.sun.tools.javac.tree.JCTree$JCAssert
 *  com.sun.tools.javac.tree.JCTree$JCAssign
 *  com.sun.tools.javac.tree.JCTree$JCAssignOp
 *  com.sun.tools.javac.tree.JCTree$JCBinary
 *  com.sun.tools.javac.tree.JCTree$JCBlock
 *  com.sun.tools.javac.tree.JCTree$JCBreak
 *  com.sun.tools.javac.tree.JCTree$JCCase
 *  com.sun.tools.javac.tree.JCTree$JCCatch
 *  com.sun.tools.javac.tree.JCTree$JCClassDecl
 *  com.sun.tools.javac.tree.JCTree$JCCompilationUnit
 *  com.sun.tools.javac.tree.JCTree$JCConditional
 *  com.sun.tools.javac.tree.JCTree$JCContinue
 *  com.sun.tools.javac.tree.JCTree$JCDoWhileLoop
 *  com.sun.tools.javac.tree.JCTree$JCEnhancedForLoop
 *  com.sun.tools.javac.tree.JCTree$JCErroneous
 *  com.sun.tools.javac.tree.JCTree$JCExpression
 *  com.sun.tools.javac.tree.JCTree$JCExpressionStatement
 *  com.sun.tools.javac.tree.JCTree$JCFieldAccess
 *  com.sun.tools.javac.tree.JCTree$JCForLoop
 *  com.sun.tools.javac.tree.JCTree$JCIdent
 *  com.sun.tools.javac.tree.JCTree$JCIf
 *  com.sun.tools.javac.tree.JCTree$JCImport
 *  com.sun.tools.javac.tree.JCTree$JCInstanceOf
 *  com.sun.tools.javac.tree.JCTree$JCLabeledStatement
 *  com.sun.tools.javac.tree.JCTree$JCLambda
 *  com.sun.tools.javac.tree.JCTree$JCLambda$ParameterKind
 *  com.sun.tools.javac.tree.JCTree$JCLiteral
 *  com.sun.tools.javac.tree.JCTree$JCMemberReference
 *  com.sun.tools.javac.tree.JCTree$JCMethodDecl
 *  com.sun.tools.javac.tree.JCTree$JCMethodInvocation
 *  com.sun.tools.javac.tree.JCTree$JCModifiers
 *  com.sun.tools.javac.tree.JCTree$JCNewArray
 *  com.sun.tools.javac.tree.JCTree$JCNewClass
 *  com.sun.tools.javac.tree.JCTree$JCParens
 *  com.sun.tools.javac.tree.JCTree$JCPrimitiveTypeTree
 *  com.sun.tools.javac.tree.JCTree$JCReturn
 *  com.sun.tools.javac.tree.JCTree$JCStatement
 *  com.sun.tools.javac.tree.JCTree$JCSwitch
 *  com.sun.tools.javac.tree.JCTree$JCSynchronized
 *  com.sun.tools.javac.tree.JCTree$JCThrow
 *  com.sun.tools.javac.tree.JCTree$JCTry
 *  com.sun.tools.javac.tree.JCTree$JCTypeApply
 *  com.sun.tools.javac.tree.JCTree$JCTypeCast
 *  com.sun.tools.javac.tree.JCTree$JCTypeParameter
 *  com.sun.tools.javac.tree.JCTree$JCTypeUnion
 *  com.sun.tools.javac.tree.JCTree$JCUnary
 *  com.sun.tools.javac.tree.JCTree$JCVariableDecl
 *  com.sun.tools.javac.tree.JCTree$JCWhileLoop
 *  com.sun.tools.javac.tree.JCTree$JCWildcard
 *  com.sun.tools.javac.tree.JCTree$LetExpr
 *  com.sun.tools.javac.tree.JCTree$Tag
 *  com.sun.tools.javac.tree.JCTree$TypeBoundKind
 *  com.sun.tools.javac.tree.Pretty
 *  com.sun.tools.javac.tree.TreeInfo
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.List
 *  com.sun.tools.javac.util.Name
 *  com.sun.tools.javac.util.Names
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.editor.indent.api.Indent
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.NbCollections
 */
package org.netbeans.modules.java.source.save;

import com.sun.source.doctree.DocCommentTree;
import com.sun.source.doctree.DocTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.DocSourcePositions;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreeScanner;
import com.sun.source.util.Trees;
import com.sun.tools.javac.api.JavacTrees;
import com.sun.tools.javac.code.BoundKind;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.TypeTag;
import com.sun.tools.javac.parser.Tokens;
import com.sun.tools.javac.tree.DCTree;
import com.sun.tools.javac.tree.DocCommentTable;
import com.sun.tools.javac.tree.EndPosTable;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.Pretty;
import com.sun.tools.javac.tree.TreeInfo;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;
import java.io.Writer;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Position;
import javax.tools.JavaFileObject;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.source.CodeStyle;
import org.netbeans.api.java.source.Comment;
import org.netbeans.api.java.source.PositionConverter;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.editor.indent.api.Indent;
import org.netbeans.modules.java.source.builder.CommentHandlerService;
import org.netbeans.modules.java.source.pretty.CharBuffer;
import org.netbeans.modules.java.source.pretty.VeryPretty;
import org.netbeans.modules.java.source.query.CommentHandler;
import org.netbeans.modules.java.source.query.CommentSet;
import org.netbeans.modules.java.source.save.BlockSequences;
import org.netbeans.modules.java.source.save.DiffContext;
import org.netbeans.modules.java.source.save.DiffUtilities;
import org.netbeans.modules.java.source.save.EstimatorFactory;
import org.netbeans.modules.java.source.save.ListMatcher;
import org.netbeans.modules.java.source.save.Measure;
import org.netbeans.modules.java.source.save.PositionEstimator;
import org.netbeans.modules.java.source.transform.FieldGroupTree;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.NbCollections;

public class CasualDiff {
    public static boolean OLD_TREES_VERBATIM = Boolean.parseBoolean(System.getProperty(WorkingCopy.class.getName() + ".keep-old-trees", "true"));
    protected final Collection<Diff> diffs = new LinkedHashSet<Diff>();
    protected CommentHandler comments;
    protected JCTree.JCCompilationUnit oldTopLevel;
    protected TreeUtilities treeUtilities;
    protected final DiffContext diffContext;
    private TokenSequence<JavaTokenId> tokenSequence;
    private String origText;
    private VeryPretty printer;
    private final Context context;
    private final Names names;
    private static final Logger LOG = Logger.getLogger(CasualDiff.class.getName());
    private Map<Integer, String> diffInfo = new HashMap<Integer, String>();
    private final Map<Tree, ?> tree2Tag;
    private final Map<Object, int[]> tag2Span;
    private final Set<Tree> oldTrees;
    private final Map<Tree, DocCommentTree> tree2Doc;
    private boolean parameterPrint = false;
    private boolean enumConstantPrint = false;
    private Map<Integer, Integer> blockSequenceMap = new LinkedHashMap<Integer, Integer>();
    private Iterator<Integer> boundaries;
    private int nextBlockBoundary = -1;
    private Name origClassName = null;
    private Name newClassName = null;
    boolean anonClass = false;
    private static final EnumSet<JavaTokenId> LAMBDA_PARAM_END_TOKENS = EnumSet.of(JavaTokenId.RPAREN, JavaTokenId.ARROW);
    private boolean suppressParameterTypes;
    private static final EnumSet<Tree.Kind> compAssign = EnumSet.of(Tree.Kind.MULTIPLY_ASSIGNMENT, new Tree.Kind[]{Tree.Kind.DIVIDE_ASSIGNMENT, Tree.Kind.REMAINDER_ASSIGNMENT, Tree.Kind.PLUS_ASSIGNMENT, Tree.Kind.MINUS_ASSIGNMENT, Tree.Kind.LEFT_SHIFT_ASSIGNMENT, Tree.Kind.RIGHT_SHIFT_ASSIGNMENT, Tree.Kind.UNSIGNED_RIGHT_SHIFT_ASSIGNMENT, Tree.Kind.AND_ASSIGNMENT, Tree.Kind.XOR_ASSIGNMENT, Tree.Kind.OR_ASSIGNMENT});
    private static final EnumSet<Tree.Kind> binaries = EnumSet.of(Tree.Kind.MULTIPLY, new Tree.Kind[]{Tree.Kind.DIVIDE, Tree.Kind.REMAINDER, Tree.Kind.PLUS, Tree.Kind.MINUS, Tree.Kind.LEFT_SHIFT, Tree.Kind.RIGHT_SHIFT, Tree.Kind.UNSIGNED_RIGHT_SHIFT, Tree.Kind.LESS_THAN, Tree.Kind.GREATER_THAN, Tree.Kind.LESS_THAN_EQUAL, Tree.Kind.GREATER_THAN_EQUAL, Tree.Kind.EQUAL_TO, Tree.Kind.NOT_EQUAL_TO, Tree.Kind.AND, Tree.Kind.XOR, Tree.Kind.OR, Tree.Kind.CONDITIONAL_AND, Tree.Kind.CONDITIONAL_OR});
    private static final EnumSet<Tree.Kind> unaries = EnumSet.of(Tree.Kind.POSTFIX_INCREMENT, new Tree.Kind[]{Tree.Kind.POSTFIX_DECREMENT, Tree.Kind.PREFIX_INCREMENT, Tree.Kind.PREFIX_DECREMENT, Tree.Kind.UNARY_PLUS, Tree.Kind.UNARY_MINUS, Tree.Kind.BITWISE_COMPLEMENT, Tree.Kind.LOGICAL_COMPLEMENT});
    public static boolean noInvalidCopyTos = false;

    protected CasualDiff(Context context, DiffContext diffContext, TreeUtilities treeUtilities, Map<Tree, ?> tree2Tag, Map<Tree, DocCommentTree> tree2Doc, Map<?, int[]> tag2Span, Set<Tree> oldTrees) {
        this.comments = CommentHandlerService.instance(context);
        this.treeUtilities = treeUtilities;
        this.diffContext = diffContext;
        this.tokenSequence = diffContext.tokenSequence;
        this.origText = diffContext.origText;
        this.context = context;
        this.names = Names.instance((Context)context);
        this.tree2Tag = tree2Tag;
        this.tree2Doc = tree2Doc;
        this.tag2Span = tag2Span;
        this.printer = new VeryPretty(diffContext, diffContext.style, tree2Tag, tree2Doc, tag2Span, this.origText);
        this.printer.oldTrees = oldTrees;
        this.oldTrees = oldTrees;
    }

    private Collection<Diff> getDiffs() {
        return this.diffs;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static Collection<Diff> diff(Context context, DiffContext diffContext, TreeUtilities treeUtilities, TreePath oldTreePath, JCTree newTree, Map<Integer, String> userInfo, Map<Tree, ?> tree2Tag, Map<Tree, DocCommentTree> tree2Doc, Map<?, int[]> tag2Span, Set<Tree> oldTrees) {
        int lineStart;
        final CasualDiff td = new CasualDiff(context, diffContext, treeUtilities, tree2Tag, tree2Doc, tag2Span, oldTrees);
        JCTree oldTree = (JCTree)oldTreePath.getLeaf();
        td.oldTopLevel = (JCTree.JCCompilationUnit)(oldTree.getKind() == Tree.Kind.COMPILATION_UNIT ? oldTree : diffContext.origUnit);
        for (Tree t : oldTreePath) {
            int indent;
            java.util.List<JCTree> embeddedElements;
            if (t == oldTree) continue;
            if (TreeUtilities.CLASS_TREE_KINDS.contains((Object)t.getKind())) {
                embeddedElements = ((ClassTree)t).getMembers();
            } else {
                if (t.getKind() != Tree.Kind.BLOCK) continue;
                embeddedElements = ((BlockTree)t).getStatements();
            }
            embeddedElements = td.filterHidden(NbCollections.checkedListByCopy((java.util.List)embeddedElements, JCTree.class, (boolean)false));
            if (embeddedElements.isEmpty()) {
                indent = CasualDiff.getOldIndent(diffContext, t);
                if (indent < 0) {
                    td.printer.indent();
                    continue;
                }
                td.printer.setIndent(indent);
                td.printer.indent();
                break;
            }
            indent = CasualDiff.getOldIndent(diffContext, (Tree)embeddedElements.get(0));
            if (indent < 0) {
                td.printer.indent();
                continue;
            }
            td.printer.setIndent(indent);
            break;
        }
        if (TreeUtilities.CLASS_TREE_KINDS.contains((Object)oldTree.getKind()) && oldTreePath.getParentPath().getLeaf().getKind() == Tree.Kind.NEW_CLASS) {
            td.anonClass = true;
        }
        int[] bounds = td.getCommentCorrectedBounds(oldTree);
        boolean isCUT = oldTree.getKind() == Tree.Kind.COMPILATION_UNIT;
        int start = isCUT ? 0 : bounds[0];
        String origText = td.origText;
        int end = isCUT ? origText.length() : bounds[1];
        for (lineStart = start; lineStart > 0 && origText.charAt(lineStart - 1) != '\n'; --lineStart) {
        }
        td.printer.setInitialOffset(lineStart);
        JCTree current = oldTree;
        for (Tree t2 : oldTreePath) {
            if (t2.getKind() == Tree.Kind.METHOD) {
                MethodTree mt = (MethodTree)t2;
                for (Tree p : mt.getParameters()) {
                    if (p != current) continue;
                    td.parameterPrint = true;
                }
                break;
            }
            if (t2.getKind() == Tree.Kind.VARIABLE) {
                JCTree.JCVariableDecl vt = (JCTree.JCVariableDecl)t2;
                if ((vt.mods.flags & 16384) != 0 && vt.init == current) {
                    td.enumConstantPrint = true;
                }
            }
            current = t2;
        }
        if (oldTree.getKind() == Tree.Kind.MODIFIERS || oldTree.getKind() == Tree.Kind.METHOD || !td.parameterPrint && oldTree.getKind() == Tree.Kind.VARIABLE) {
            td.tokenSequence.move(start);
            if (td.tokenSequence.movePrevious() && td.tokenSequence.token().id() == JavaTokenId.WHITESPACE) {
                String text = td.tokenSequence.token().text().toString();
                int index = text.lastIndexOf(10);
                start = td.tokenSequence.offset();
                if (index > -1) {
                    start += index + 1;
                }
            }
        }
        td.copyTo(lineStart, start, td.printer);
        td.diffTree(oldTree, newTree, (JCTree)(oldTreePath.getParentPath() != null ? oldTreePath.getParentPath().getLeaf() : null), new int[]{start, bounds[1]});
        String resultSrc = td.printer.toString().substring(start - lineStart);
        if (!td.printer.reindentRegions.isEmpty()) {
            ArrayList<SectKey> keys = new ArrayList<SectKey>(td.blockSequenceMap.size());
            for (Map.Entry<Integer, Integer> e : td.blockSequenceMap.entrySet()) {
                int x = e.getValue();
                SectKey k = new SectKey(x);
                keys.add(k);
                td.tag2Span.put(k, new int[]{x, x});
            }
            try {
                String toParse = origText.substring(0, start) + resultSrc + origText.substring(end);
                BaseDocument doc = new BaseDocument(false, "text/x-java");
                doc.insertString(0, toParse, null);
                doc.putProperty(Language.class, (Object)JavaTokenId.language());
                doc.putProperty((Object)"stream", (Object)diffContext.file);
                Position startPos = doc.createPosition(start);
                Position endPos = doc.createPosition(start + resultSrc.length());
                IdentityHashMap<Object, Position[]> spans = new IdentityHashMap<Object, Position[]>(td.tag2Span.size());
                for (Map.Entry<Object, int[]> e2 : td.tag2Span.entrySet()) {
                    spans.put(e2.getKey(), new Position[]{doc.createPosition(e2.getValue()[0]), doc.createPosition(e2.getValue()[1])});
                }
                final Indent i = Indent.get((Document)doc);
                i.lock();
                try {
                    doc.runAtomic(new Runnable(){

                        @Override
                        public void run() {
                            for (int[] region : CasualDiff.access$000((CasualDiff)td).reindentRegions) {
                                try {
                                    i.reindent(region[0], region[1]);
                                }
                                catch (BadLocationException ex) {
                                    Exceptions.printStackTrace((Throwable)ex);
                                }
                            }
                        }
                    });
                }
                finally {
                    i.unlock();
                }
                resultSrc = doc.getText(startPos.getOffset(), endPos.getOffset() - startPos.getOffset());
                for (Map.Entry e3 : spans.entrySet()) {
                    int[] span = td.tag2Span.get(e3.getKey());
                    span[0] = ((Position[])e3.getValue())[0].getOffset();
                    span[1] = ((Position[])e3.getValue())[1].getOffset();
                }
                Iterator it = keys.iterator();
                for (Map.Entry<Integer, Integer> e4 : td.blockSequenceMap.entrySet()) {
                    SectKey k = (SectKey)it.next();
                    int[] span = td.tag2Span.get(k);
                    e4.setValue(span[0]);
                }
            }
            catch (BadLocationException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        String originalText = isCUT ? origText : origText.substring(start, end);
        userInfo.putAll(td.diffInfo);
        return td.checkDiffs(DiffUtilities.diff(originalText, resultSrc, start, td.readSections(originalText.length(), resultSrc.length(), lineStart, start), lineStart));
    }

    private int[] readSections(int l1, int l2, int printerStart, int diffStart) {
        Map<Integer, Integer> seqMap = this.blockSequenceMap;
        if (seqMap.isEmpty()) {
            int delta = diffStart - printerStart;
            return new int[]{l1 + delta, l2 + delta};
        }
        int[] res = new int[seqMap.size() * 2];
        int p = 0;
        for (Map.Entry<Integer, Integer> en : seqMap.entrySet()) {
            int point = en.getKey();
            if (point < printerStart || point > diffStart + l2) continue;
            res[p++] = en.getKey();
            res[p++] = en.getValue();
        }
        return res;
    }

    private java.util.List<Diff> checkDiffs(java.util.List<Diff> theDiffs) {
        if (theDiffs != null) {
            for (Diff d : theDiffs) {
                if (this.diffContext.positionConverter.getOriginalPosition(d.getPos()) <= this.diffContext.textLength && this.diffContext.positionConverter.getOriginalPosition(d.getEnd()) <= this.diffContext.textLength) continue;
                LOG.warning("Invalid diff: " + d);
            }
        }
        return theDiffs;
    }

    public static Collection<Diff> diff(Context context, DiffContext diffContext, TreeUtilities treeUtilities, java.util.List<? extends ImportTree> original, java.util.List<? extends ImportTree> nue, Map<Integer, String> userInfo, Map<Tree, ?> tree2Tag, Map<Tree, DocCommentTree> tree2Doc, Map<?, int[]> tag2Span, Set<Tree> oldTrees) {
        CasualDiff td = new CasualDiff(context, diffContext, treeUtilities, tree2Tag, tree2Doc, tag2Span, oldTrees);
        td.oldTopLevel = diffContext.origUnit;
        int start = td.oldTopLevel.getPackageName() != null ? td.endPos((JCTree)td.oldTopLevel.getPackageName()) : 0;
        LinkedList<JCTree.JCImport> originalJC = new LinkedList<JCTree.JCImport>();
        LinkedList<JCTree.JCImport> nueJC = new LinkedList<JCTree.JCImport>();
        for (ImportTree i2 : original) {
            originalJC.add((JCTree.JCImport)i2);
        }
        for (ImportTree i : nue) {
            nueJC.add((JCTree.JCImport)i);
        }
        PositionEstimator est = EstimatorFactory.imports(originalJC, nueJC, td.diffContext);
        int end = td.diffList(originalJC, nueJC, start, est, Measure.DEFAULT, td.printer);
        String resultSrc = td.printer.toString();
        if (start > end) {
            LOG.log(Level.INFO, "Illegal values: start = " + start + "; end = " + end + "." + "Please attach your messages.log to issue #200152 (https://netbeans.org/bugzilla/show_bug.cgi?id=200152)");
            LOG.log(Level.INFO, "-----\n" + td.diffContext.origText + "-----\n");
            LOG.log(Level.INFO, "Orig imports: " + original);
            LOG.log(Level.INFO, "New imports: " + nue);
        }
        String originalText = td.diffContext.origText.substring(start, end);
        userInfo.putAll(td.diffInfo);
        return td.checkDiffs(DiffUtilities.diff(originalText, resultSrc, start));
    }

    public int endPos(JCTree t) {
        if (t instanceof FieldGroupTree) {
            FieldGroupTree fgt = (FieldGroupTree)t;
            VariableTree vt = (VariableTree)fgt.getVariables().get(fgt.getVariables().size() - 1);
            return TreeInfo.getEndPos((JCTree)((JCTree)vt), (EndPosTable)this.oldTopLevel.endPositions);
        }
        return TreeInfo.getEndPos((JCTree)t, (EndPosTable)this.oldTopLevel.endPositions);
    }

    private int endPos(List<? extends JCTree> trees) {
        int result = -1;
        if (trees.nonEmpty()) {
            result = this.endPos((JCTree)trees.head);
            List l = trees.tail;
            while (l.nonEmpty()) {
                result = this.endPos((JCTree)l.head);
                l = l.tail;
            }
        }
        return result;
    }

    private int endPos(java.util.List<? extends JCTree> trees) {
        if (trees.isEmpty()) {
            return -1;
        }
        return this.endPos(trees.get(trees.size() - 1));
    }

    protected void diffTopLevel(JCTree.JCCompilationUnit oldT, JCTree.JCCompilationUnit newT, int[] elementBounds) {
        int start;
        int packageKeywordStart = start = elementBounds[0];
        if (oldT.pid != null) {
            this.tokenSequence.move(oldT.pid.getStartPosition());
            PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.BACKWARD);
            packageKeywordStart = this.tokenSequence.offset();
        }
        int localPointer = oldT.packageAnnotations.isEmpty() && !newT.packageAnnotations.isEmpty() ? packageKeywordStart : start;
        this.oldTopLevel = oldT;
        localPointer = this.diffAnnotationsLists(oldT.packageAnnotations, newT.packageAnnotations, localPointer, start);
        localPointer = this.diffPackageStatement(oldT, newT, packageKeywordStart, localPointer);
        PositionEstimator est = EstimatorFactory.imports(oldT.getImports(), newT.getImports(), this.diffContext);
        localPointer = this.diffList((java.util.List<? extends JCTree>)oldT.getImports(), (java.util.List<? extends JCTree>)newT.getImports(), localPointer, est, Measure.DEFAULT, this.printer);
        est = EstimatorFactory.toplevel(oldT.getTypeDecls(), newT.getTypeDecls(), this.diffContext);
        localPointer = this.diffList((java.util.List<? extends JCTree>)oldT.getTypeDecls(), (java.util.List<? extends JCTree>)newT.getTypeDecls(), localPointer, est, Measure.REAL_MEMBER, this.printer);
        if (localPointer < 0 || localPointer > this.origText.length()) {
            LOG.warning("Invalid localPointer (" + localPointer + "), see defect #226498 and report the log to the issue");
            LOG.warning("OldT:" + (Object)oldT);
            LOG.warning("NewT:" + (Object)newT);
            LOG.warning("CodeStyle: " + CasualDiff.printCodeStyle(this.diffContext.style));
            LOG.warning("origText(" + this.origText.length() + "): " + this.origText);
        }
        this.printer.print(this.origText.substring(localPointer));
    }

    private static int getOldIndent(DiffContext diffContext, Tree t) {
        int offset = (int)diffContext.trees.getSourcePositions().getStartPosition((CompilationUnitTree)diffContext.origUnit, t);
        if (offset < 0) {
            return -1;
        }
        while (offset > 0 && diffContext.origText.charAt(offset - 1) != '\n') {
            --offset;
        }
        int indent = 0;
        while (offset < diffContext.origText.length()) {
            char c;
            if ((c = diffContext.origText.charAt(offset++)) == '\t') {
                indent += diffContext.style.getTabSize();
                continue;
            }
            if (c == '\n' || !Character.isWhitespace(c)) break;
            ++indent;
        }
        return indent;
    }

    private boolean needStar(int localPointer) {
        if (localPointer <= 0) {
            return false;
        }
        while (localPointer > 0) {
            char c;
            if ((c = this.diffContext.origText.charAt(--localPointer)) == '\n') {
                return false;
            }
            if (Character.isWhitespace(c)) continue;
            if (localPointer > 3 && this.diffContext.origText.substring(localPointer - 2, localPointer + 1).equals("/**")) {
                return true;
            }
            return false;
        }
        return false;
    }

    private int adjustToPreviousNewLine(int oldPos, int localPointer) {
        char c;
        int offset = oldPos;
        if (offset < 0) {
            return localPointer;
        }
        while (offset > localPointer && (c = this.diffContext.origText.charAt(--offset)) != '\n') {
            if (c == '*' || Character.isWhitespace(c)) continue;
            return oldPos;
        }
        return offset;
    }

    private ChangeKind getChangeKind(Tree oldT, Tree newT) {
        if (oldT == newT) {
            return ChangeKind.NOCHANGE;
        }
        if (oldT != null && newT != null) {
            return ChangeKind.MODIFY;
        }
        if (oldT != null) {
            return ChangeKind.DELETE;
        }
        return ChangeKind.INSERT;
    }

    private int diffPackageStatement(JCTree.JCCompilationUnit oldT, JCTree.JCCompilationUnit newT, int packageKeywordStart, int localPointer) {
        ChangeKind change = this.getChangeKind((Tree)oldT.pid, (Tree)newT.pid);
        switch (change) {
            case NOCHANGE: {
                break;
            }
            case INSERT: {
                this.printer.printPackage(newT.pid);
                break;
            }
            case DELETE: {
                this.copyTo(localPointer, packageKeywordStart);
                this.tokenSequence.move(this.endPos((JCTree)oldT.pid));
                PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.FORWARD);
                localPointer = this.tokenSequence.offset() + 1;
                break;
            }
            case MODIFY: {
                this.copyTo(localPointer, CasualDiff.getOldPos((JCTree)oldT.pid));
                localPointer = this.endPos((JCTree)oldT.pid);
                this.printer.print((JCTree)newT.pid);
                this.diffInfo.put(CasualDiff.getOldPos((JCTree)oldT.pid), NbBundle.getMessage(CasualDiff.class, (String)"TXT_UpdatePackageStatement"));
            }
        }
        return localPointer;
    }

    protected int diffImport(JCTree.JCImport oldT, JCTree.JCImport newT, int[] bounds) {
        int localPointer = bounds[0];
        int[] qualBounds = this.getBounds(oldT.getQualifiedIdentifier());
        if (oldT.staticImport == newT.staticImport) {
            this.copyTo(localPointer, qualBounds[0]);
        } else if (oldT.staticImport) {
            PositionEstimator.moveFwdToToken(this.tokenSequence, localPointer, JavaTokenId.STATIC);
            this.copyTo(localPointer, this.tokenSequence.offset());
        } else {
            this.copyTo(localPointer, qualBounds[0]);
            this.printer.print("static ");
        }
        localPointer = this.diffTree(oldT.getQualifiedIdentifier(), newT.getQualifiedIdentifier(), qualBounds);
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffClassDef(JCTree.JCClassDecl oldT, JCTree.JCClassDecl newT, int[] bounds) {
        int localPointer = bounds[0];
        Name origOuterClassName = this.origClassName;
        Name newOuterClassName = this.newClassName;
        int insertHint = localPointer;
        java.util.List<JCTree> filteredOldTDefs = this.filterHidden((java.util.List<? extends JCTree>)oldT.defs);
        java.util.List<JCTree> filteredNewTDefs = this.filterHidden((java.util.List<? extends JCTree>)newT.defs);
        if (!this.anonClass) {
            boolean parens;
            PositionEstimator estimator;
            JavaTokenId[] arrjavaTokenId;
            this.tokenSequence.move(oldT.pos);
            this.tokenSequence.moveNext();
            this.tokenSequence.moveNext();
            int afterKindHint = this.tokenSequence.offset();
            PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.FORWARD);
            insertHint = this.tokenSequence.offset();
            localPointer = this.diffModifiers(oldT.mods, newT.mods, (JCTree)oldT, localPointer);
            if (this.kindChanged(oldT.mods.flags, newT.mods.flags)) {
                int pos = oldT.pos;
                if ((oldT.mods.flags & 8192) != 0) {
                    this.tokenSequence.move(pos);
                    this.tokenSequence.moveNext();
                    PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.BACKWARD);
                    pos = this.tokenSequence.offset();
                }
                if ((newT.mods.flags & 8192) != 0) {
                    this.copyTo(localPointer, pos);
                    this.printer.print("@interface");
                } else if ((newT.mods.flags & 16384) != 0) {
                    this.copyTo(localPointer, pos);
                    this.printer.print("enum");
                } else if ((newT.mods.flags & 512) != 0) {
                    this.copyTo(localPointer, pos);
                    this.printer.print("interface");
                } else {
                    this.copyTo(localPointer, pos);
                    this.printer.print("class");
                }
                localPointer = afterKindHint;
            }
            if (this.nameChanged(oldT.name, newT.name)) {
                this.copyTo(localPointer, insertHint);
                this.printer.print(newT.name);
                this.diffInfo.put(insertHint, NbBundle.getMessage(CasualDiff.class, (String)"TXT_ChangeClassName"));
                localPointer = insertHint += oldT.name.length();
            } else {
                int n = localPointer;
                localPointer = insertHint += oldT.name.length();
                this.copyTo(n, localPointer);
            }
            this.origClassName = oldT.name;
            this.newClassName = newT.name;
            if (oldT.typarams.nonEmpty() && newT.typarams.nonEmpty()) {
                int n = localPointer;
                localPointer = ((JCTree.JCTypeParameter)oldT.typarams.head).pos;
                this.copyTo(n, localPointer);
            }
            boolean bl = parens = oldT.typarams.isEmpty() && newT.typarams.nonEmpty();
            if (parens) {
                JavaTokenId[] arrjavaTokenId2 = new JavaTokenId[2];
                arrjavaTokenId2[0] = JavaTokenId.LT;
                arrjavaTokenId = arrjavaTokenId2;
                arrjavaTokenId2[1] = JavaTokenId.GT;
            } else {
                arrjavaTokenId = null;
            }
            localPointer = this.diffParameterList((java.util.List<? extends JCTree>)oldT.typarams, (java.util.List<? extends JCTree>)newT.typarams, arrjavaTokenId, localPointer, Measure.ARGUMENT);
            if (oldT.typarams.nonEmpty()) {
                insertHint = this.endPos((JCTree)oldT.typarams.last());
                this.tokenSequence.move(insertHint);
                PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.FORWARD);
                insertHint = this.tokenSequence.offset() + this.tokenSequence.token().length();
            }
            switch (this.getChangeKind((Tree)oldT.extending, (Tree)newT.extending)) {
                case NOCHANGE: {
                    insertHint = oldT.extending != null ? this.endPos((JCTree)oldT.extending) : insertHint;
                    int n = localPointer;
                    localPointer = insertHint;
                    this.copyTo(n, localPointer);
                    break;
                }
                case MODIFY: {
                    this.copyTo(localPointer, CasualDiff.getOldPos((JCTree)oldT.extending));
                    localPointer = this.diffTree((JCTree)oldT.extending, (JCTree)newT.extending, this.getBounds((JCTree)oldT.extending));
                    break;
                }
                case INSERT: {
                    this.copyTo(localPointer, insertHint);
                    this.printer.print(" extends ");
                    this.printer.print((JCTree)newT.extending);
                    localPointer = insertHint;
                    break;
                }
                case DELETE: {
                    this.copyTo(localPointer, insertHint);
                    localPointer = this.endPos((JCTree)oldT.extending);
                }
            }
            if (oldT.implementing.isEmpty()) {
                if (oldT.extending != null) {
                    insertHint = this.endPos((JCTree)oldT.extending);
                }
            } else {
                insertHint = ((JCTree.JCExpression)oldT.implementing.iterator().next()).getStartPosition();
            }
            long flags = oldT.sym != null ? oldT.sym.flags() : oldT.mods.flags;
            PositionEstimator positionEstimator = estimator = (flags & 512) == 0 ? EstimatorFactory.implementz(oldT.getImplementsClause(), newT.getImplementsClause(), this.diffContext) : EstimatorFactory.extendz(oldT.getImplementsClause(), newT.getImplementsClause(), this.diffContext);
            if (!newT.implementing.isEmpty()) {
                this.copyTo(localPointer, insertHint);
            }
            localPointer = this.diffList2((java.util.List<? extends JCTree>)oldT.implementing, (java.util.List<? extends JCTree>)newT.implementing, insertHint, estimator);
            insertHint = this.endPos((JCTree)oldT) - 1;
            insertHint = filteredOldTDefs.isEmpty() ? this.endPos((JCTree)oldT) - 1 : filteredOldTDefs.get(0).getStartPosition() - 1;
            this.tokenSequence.move(insertHint);
            this.tokenSequence.moveNext();
            insertHint = PositionEstimator.moveBackToToken(this.tokenSequence, insertHint, JavaTokenId.LBRACE) + 1;
        } else {
            insertHint = PositionEstimator.moveFwdToToken(this.tokenSequence, CasualDiff.getOldPos((JCTree)oldT), JavaTokenId.LBRACE);
            this.tokenSequence.moveNext();
            insertHint = this.tokenSequence.offset();
        }
        int old = this.printer.indent();
        Name origName = this.printer.enclClassName;
        this.printer.enclClassName = newT.getSimpleName();
        PositionEstimator est = EstimatorFactory.members(filteredOldTDefs, filteredNewTDefs, this.diffContext);
        if (localPointer < insertHint) {
            this.copyTo(localPointer, insertHint);
        }
        if ((newT.mods.flags & 16384) != 0 && !filteredNewTDefs.isEmpty()) {
            boolean xxx;
            boolean constMissing = filteredOldTDefs.isEmpty();
            boolean firstDiff = false;
            boolean oldEnum = constMissing;
            boolean bl = xxx = (newT.mods.flags & 16384) != 0 && filteredOldTDefs.isEmpty() && !filteredNewTDefs.isEmpty() && !this.isEnum((Tree)filteredNewTDefs.get(0)) && !newT.getSimpleName().isEmpty();
            if (constMissing) {
                firstDiff = !this.isEnum((Tree)filteredNewTDefs.get(0));
            } else {
                oldEnum = this.isEnum((Tree)filteredOldTDefs.get(0));
                boolean bl2 = firstDiff = oldEnum != this.isEnum((Tree)filteredNewTDefs.get(0));
            }
            if (firstDiff && !newT.getSimpleName().isEmpty()) {
                if (oldEnum) {
                    this.printer.blankline();
                    this.printer.toLeftMargin();
                    this.printer.print(";");
                    this.printer.newline();
                } else {
                    insertHint = this.removeExtraEnumSemicolon(insertHint);
                }
            }
        }
        localPointer = this.diffList(filteredOldTDefs, filteredNewTDefs, insertHint, est, Measure.REAL_MEMBER, this.printer);
        this.printer.enclClassName = origName;
        this.origClassName = origOuterClassName;
        this.newClassName = newOuterClassName;
        this.printer.undent(old);
        if (localPointer != -1 && localPointer < this.origText.length()) {
            if (this.origText.charAt(localPointer) == '}') {
                this.printer.toLeftMargin();
            }
            this.copyTo(localPointer, bounds[1]);
        }
        return bounds[1];
    }

    private int removeExtraEnumSemicolon(int insertHint) {
        int startWS = -1;
        int rewind = this.tokenSequence.offset();
        this.tokenSequence.move(insertHint);
        this.tokenSequence.moveNext();
        boolean semi = false;
        block5 : do {
            Token t = this.tokenSequence.token();
            switch ((JavaTokenId)t.id()) {
                case WHITESPACE: {
                    if (semi) {
                        int nl = t.text().toString().lastIndexOf(10);
                        if (nl == -1) {
                            startWS = this.tokenSequence.offset();
                            break;
                        }
                        startWS = this.tokenSequence.offset() + nl + 1;
                        break;
                    }
                    if (startWS != -1) continue block5;
                    startWS = t.text().toString().indexOf(10);
                    if (startWS == -1) {
                        startWS = this.tokenSequence.offset();
                        break;
                    }
                    startWS += this.tokenSequence.offset() + 1;
                    break;
                }
                case SEMICOLON: {
                    if (startWS >= 0) {
                        this.copyTo(insertHint, startWS);
                        insertHint = this.tokenSequence.offset() + t.length();
                    }
                    startWS = -1;
                    semi = true;
                    break;
                }
                case LINE_COMMENT: 
                case BLOCK_COMMENT: 
                case JAVADOC_COMMENT: {
                    if (semi) break block5;
                    startWS = -1;
                    break;
                }
                default: {
                    break block5;
                }
            }
        } while (this.tokenSequence.moveNext());
        if (semi && startWS > -1) {
            insertHint = startWS;
        }
        this.tokenSequence.move(rewind);
        this.tokenSequence.moveNext();
        return insertHint;
    }

    private boolean isEnum(Tree tree) {
        if (tree instanceof FieldGroupTree) {
            return ((FieldGroupTree)tree).isEnum();
        }
        if (tree instanceof VariableTree) {
            return (((JCTree.JCVariableDecl)tree).getModifiers().flags & 16384) != 0;
        }
        if (tree instanceof ClassTree) {
            return (((JCTree.JCClassDecl)tree).getModifiers().flags & 16384) != 0;
        }
        return false;
    }

    private boolean hasModifiers(JCTree.JCModifiers mods) {
        return mods != null && (!mods.getFlags().isEmpty() || !mods.getAnnotations().isEmpty());
    }

    protected int diffMethodDef(JCTree.JCMethodDecl oldT, JCTree.JCMethodDecl newT, int[] bounds) {
        int pos;
        int localPointer = bounds[0];
        if (!this.matchModifiers(oldT.mods, newT.mods)) {
            if (this.hasModifiers(newT.mods)) {
                localPointer = this.diffModifiers(oldT.mods, newT.mods, (JCTree)oldT, localPointer);
            } else {
                int oldPos = CasualDiff.getOldPos((JCTree)oldT.mods);
                this.copyTo(localPointer, oldPos);
                int n = localPointer = oldT.restype != null ? CasualDiff.getOldPos((JCTree)oldT.restype) : oldT.pos;
            }
        }
        int n = oldT.typarams.isEmpty() ? (oldT.restype != null ? CasualDiff.getOldPos((JCTree)oldT.restype) : oldT.pos) : (pos = CasualDiff.getOldPos((JCTree)oldT.typarams.head));
        if (!this.listsMatch((java.util.List<? extends JCTree>)oldT.typarams, (java.util.List<? extends JCTree>)newT.typarams)) {
            boolean parens;
            JavaTokenId[] arrjavaTokenId;
            if (newT.typarams.nonEmpty()) {
                this.copyTo(localPointer, pos);
            } else if (this.hasModifiers(oldT.mods)) {
                this.copyTo(localPointer, this.endPos((JCTree)oldT.mods));
            }
            boolean bl = parens = oldT.typarams.isEmpty() || newT.typarams.isEmpty();
            if (parens) {
                JavaTokenId[] arrjavaTokenId2 = new JavaTokenId[2];
                arrjavaTokenId2[0] = JavaTokenId.LT;
                arrjavaTokenId = arrjavaTokenId2;
                arrjavaTokenId2[1] = JavaTokenId.GT;
            } else {
                arrjavaTokenId = null;
            }
            localPointer = this.diffParameterList((java.util.List<? extends JCTree>)oldT.typarams, (java.util.List<? extends JCTree>)newT.typarams, arrjavaTokenId, pos, Measure.ARGUMENT);
            if (parens && oldT.typarams.isEmpty()) {
                this.printer.print(" ");
            }
        }
        if (oldT.restype != null) {
            int[] restypeBounds = this.getBounds((JCTree)oldT.restype);
            this.copyTo(localPointer, restypeBounds[0]);
            localPointer = this.diffTree((JCTree)oldT.restype, (JCTree)newT.restype, restypeBounds);
            if (restypeBounds[1] > localPointer) {
                int n2 = localPointer;
                localPointer = restypeBounds[1];
                this.copyTo(n2, localPointer);
            }
        } else if (oldT.restype == null && newT.restype != null) {
            int n3 = localPointer;
            localPointer = oldT.pos;
            this.copyTo(n3, localPointer);
            this.printer.print((JCTree)newT.restype);
            this.printer.print(" ");
        }
        int posHint = oldT.typarams.isEmpty() ? (oldT.restype != null ? oldT.restype.getStartPosition() : oldT.getStartPosition()) : ((JCTree.JCTypeParameter)oldT.typarams.iterator().next()).getStartPosition();
        if (!(oldT.name == this.names.init && this.origClassName == null || newT.name == this.names.init && this.newClassName == null)) {
            int origLength;
            int n4 = origLength = oldT.name == this.names.init && this.origClassName != null ? this.origClassName.length() : oldT.name.length();
            if (this.nameChanged(oldT.name, newT.name)) {
                this.copyTo(localPointer, oldT.pos);
                if (newT.name == this.names.init && this.newClassName != null) {
                    this.printer.print(this.newClassName);
                    localPointer = oldT.pos + origLength;
                } else {
                    this.printer.print(newT.name);
                    this.diffInfo.put(oldT.pos, NbBundle.getMessage(CasualDiff.class, (String)"TXT_RenameMethod", (Object)oldT.name));
                    localPointer = oldT.pos + origLength;
                }
            } else {
                int n5 = localPointer;
                localPointer = oldT.pos + origLength;
                this.copyTo(n5, localPointer);
            }
        }
        if (oldT.params.isEmpty()) {
            int startOffset = oldT.pos;
            PositionEstimator.moveFwdToToken(this.tokenSequence, startOffset, JavaTokenId.RPAREN);
            posHint = this.tokenSequence.offset();
        } else {
            posHint = ((JCTree.JCVariableDecl)oldT.params.iterator().next()).getStartPosition();
        }
        if (!this.listsMatch((java.util.List<? extends JCTree>)oldT.params, (java.util.List<? extends JCTree>)newT.params)) {
            this.copyTo(localPointer, posHint);
            int old = this.printer.setPrec(0);
            this.parameterPrint = true;
            Name oldEnclClassName = this.printer.enclClassName;
            this.printer.enclClassName = null;
            localPointer = this.diffParameterList((java.util.List<? extends JCTree>)oldT.params, (java.util.List<? extends JCTree>)newT.params, null, posHint, Measure.MEMBER);
            this.printer.enclClassName = oldEnclClassName;
            this.parameterPrint = false;
            this.printer.setPrec(old);
        }
        PositionEstimator.moveFwdToToken(this.tokenSequence, oldT.params.isEmpty() ? posHint : this.endPos((JCTree)oldT.params.last()), JavaTokenId.RPAREN);
        this.tokenSequence.moveNext();
        posHint = this.tokenSequence.offset();
        if (localPointer < posHint) {
            int n6 = localPointer;
            localPointer = posHint;
            this.copyTo(n6, localPointer);
        }
        if (oldT.thrown.isEmpty()) {
            if (oldT.body != null) {
                this.tokenSequence.move(oldT.body.pos);
                PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.BACKWARD);
                this.tokenSequence.moveNext();
                posHint = this.tokenSequence.offset();
            } else if (oldT.defaultValue != null) {
                this.tokenSequence.move(CasualDiff.getOldPos((JCTree)oldT.defaultValue));
                while (this.tokenSequence.movePrevious() && this.tokenSequence.token().id() != JavaTokenId.DEFAULT) {
                }
                PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.BACKWARD);
                this.tokenSequence.moveNext();
                posHint = this.tokenSequence.offset();
            } else {
                posHint = this.endPos((JCTree)oldT) - 1;
            }
        } else {
            posHint = ((JCTree.JCExpression)oldT.thrown.iterator().next()).getStartPosition();
        }
        if (!newT.thrown.isEmpty()) {
            int n7 = localPointer;
            localPointer = posHint;
            this.copyTo(n7, localPointer);
        }
        PositionEstimator est = EstimatorFactory.throwz(oldT.getThrows(), newT.getThrows(), this.diffContext);
        localPointer = this.diffList2((java.util.List<? extends JCTree>)oldT.thrown, (java.util.List<? extends JCTree>)newT.thrown, posHint, est);
        if (oldT.defaultValue != newT.defaultValue) {
            if (oldT.defaultValue == null) {
                this.printer.print(" default ");
                this.printer.print((JCTree)newT.defaultValue);
            } else if (newT.defaultValue == null) {
                localPointer = this.endPos((JCTree)oldT.defaultValue);
            } else {
                int[] restypeBounds = this.getBounds((JCTree)oldT.defaultValue);
                this.copyTo(localPointer, restypeBounds[0]);
                int n8 = localPointer = this.diffTree((JCTree)oldT.defaultValue, (JCTree)newT.defaultValue, restypeBounds);
                localPointer = restypeBounds[1];
                this.copyTo(n8, localPointer);
            }
        }
        if (newT.body == null && oldT.body != null) {
            localPointer = this.endPos((JCTree)oldT.body);
            this.printer.print(";");
        } else if (oldT.body != null && newT.body != null) {
            int[] bodyBounds = this.getBounds((JCTree)oldT.body);
            this.copyTo(localPointer, bodyBounds[0]);
            localPointer = this.diffTree((JCTree)oldT.body, (JCTree)newT.body, bodyBounds);
        } else if (oldT.body == null && newT.body != null) {
            this.tokenSequence.move(localPointer);
            PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.FORWARD);
            if (this.tokenSequence.token().id() == JavaTokenId.SEMICOLON) {
                localPointer = this.tokenSequence.offset() + this.tokenSequence.token().length();
            }
            if (this.diffContext.style.spaceBeforeMethodDeclLeftBrace()) {
                this.printer.print(" ");
            }
            this.printer.print((JCTree)newT.body);
        }
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffVarDef(JCTree.JCVariableDecl oldT, JCTree.JCVariableDecl newT, int pos) {
        int localPointer = oldT.pos;
        this.copyTo(pos, localPointer);
        if (this.nameChanged(oldT.name, newT.name)) {
            this.copyTo(localPointer, oldT.pos);
            this.printer.print(newT.name);
            this.diffInfo.put(oldT.pos, NbBundle.getMessage(CasualDiff.class, (String)"TXT_RenameVariable", (Object)oldT.name));
            localPointer = oldT.pos + oldT.name.length();
        }
        if (newT.init != null && oldT.init != null) {
            int n = localPointer;
            localPointer = CasualDiff.getOldPos((JCTree)oldT.init);
            this.copyTo(n, localPointer);
            localPointer = this.diffTree((JCTree)oldT.init, (JCTree)newT.init, new int[]{localPointer, this.endPos((JCTree)oldT.init)});
        } else {
            if (oldT.init != null && newT.init == null) {
                pos = CasualDiff.getOldPos((JCTree)oldT.init);
                this.tokenSequence.move(pos);
                PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.BACKWARD);
                PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.BACKWARD);
                this.tokenSequence.moveNext();
                int to = this.tokenSequence.offset();
                this.copyTo(localPointer, to);
                localPointer = this.endPos((JCTree)oldT.init);
            }
            if (oldT.init == null && newT.init != null) {
                int end = this.endPos((JCTree)oldT);
                this.tokenSequence.move(end);
                PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.BACKWARD);
                int n = localPointer;
                localPointer = this.tokenSequence.offset();
                this.copyTo(n, localPointer);
                this.printer.printVarInit(newT);
            }
        }
        int n = localPointer;
        localPointer = this.endPos((JCTree)oldT);
        this.copyTo(n, localPointer);
        return localPointer;
    }

    private int diffVarDef(JCTree.JCVariableDecl oldT, JCTree.JCVariableDecl newT, int[] bounds) {
        int localPointer = bounds[0];
        if ((oldT.mods.flags & 16384) != 0) {
            if (this.nameChanged(oldT.name, newT.name)) {
                this.copyTo(localPointer, oldT.pos);
                this.printer.print(newT.name);
                this.diffInfo.put(oldT.pos, NbBundle.getMessage(CasualDiff.class, (String)"TXT_RenameEnumConstant", (Object)oldT.name));
                localPointer = oldT.pos + oldT.name.length();
            }
            JCTree.JCNewClass oldInit = (JCTree.JCNewClass)oldT.init;
            JCTree.JCNewClass newInit = (JCTree.JCNewClass)newT.init;
            if (oldInit.args.nonEmpty() && newInit.args.nonEmpty()) {
                int n = localPointer;
                localPointer = CasualDiff.getOldPos((JCTree)oldInit.args.head);
                this.copyTo(n, localPointer);
                localPointer = this.diffParameterList((java.util.List<? extends JCTree>)oldInit.args, (java.util.List<? extends JCTree>)newInit.args, null, localPointer, Measure.ARGUMENT);
            }
            if (oldInit.def != null && newInit.def != null) {
                this.anonClass = true;
                int[] defBounds = new int[]{localPointer, this.endPos((JCTree)oldInit.def)};
                localPointer = this.diffTree((JCTree)oldInit.def, (JCTree)newInit.def, defBounds);
                this.anonClass = false;
            }
            this.copyTo(localPointer, bounds[1]);
            return bounds[1];
        }
        if (!this.matchModifiers(oldT.mods, newT.mods)) {
            if (this.hasModifiers(newT.mods)) {
                localPointer = this.diffModifiers(oldT.mods, newT.mods, (JCTree)oldT, localPointer);
            } else if (this.hasModifiers(oldT.mods)) {
                int oldPos = CasualDiff.getOldPos((JCTree)oldT.mods);
                this.copyTo(localPointer, oldPos);
                localPointer = CasualDiff.getOldPos((JCTree)oldT.vartype);
            }
        }
        boolean cLikeArray = false;
        boolean cLikeArrayChange = false;
        int addDimensions = 0;
        if (this.diffContext.syntheticTrees.contains((Object)oldT.vartype)) {
            if (!this.diffContext.syntheticTrees.contains((Object)newT.vartype)) {
                int n = localPointer;
                localPointer = oldT.pos;
                this.copyTo(n, localPointer);
                this.printer.suppressVariableType = this.suppressParameterTypes;
                int l = this.printer.out.length();
                this.printer.print((JCTree)newT.vartype);
                this.printer.suppressVariableType = false;
                if (l < this.printer.out.length()) {
                    this.printer.print(" ");
                }
            }
        } else if (this.suppressParameterTypes) {
            int[] vartypeBounds = this.getBounds((JCTree)oldT.vartype);
            localPointer = vartypeBounds[1];
        } else {
            if (newT.vartype == null) {
                throw new UnsupportedOperationException();
            }
            int[] vartypeBounds = this.getBounds((JCTree)oldT.vartype);
            addDimensions = this.dimension((JCTree)newT.vartype, -1);
            cLikeArray = vartypeBounds[1] > oldT.pos;
            cLikeArrayChange = cLikeArray && this.dimension((JCTree)oldT.vartype, oldT.pos) > addDimensions;
            this.copyTo(localPointer, vartypeBounds[0]);
            localPointer = this.diffTree((JCTree)oldT.vartype, (JCTree)newT.vartype, vartypeBounds);
        }
        if (this.nameChanged(oldT.name, newT.name)) {
            boolean isOldError;
            boolean bl = isOldError = oldT.name == Names.instance((Context)this.context).error;
            if (!isOldError) {
                this.copyTo(localPointer, oldT.pos);
            } else {
                this.printer.print(" ");
            }
            if (cLikeArray) {
                this.printer.eatChars(1);
                for (int i = 0; i < addDimensions; ++i) {
                    this.printer.print("[]");
                }
                this.printer.print(" ");
            }
            this.printer.print(newT.name);
            this.diffInfo.put(oldT.pos, NbBundle.getMessage(CasualDiff.class, (String)"TXT_RenameVariable", (Object)oldT.name));
            if (!isOldError) {
                if (cLikeArray) {
                    int[] clab = this.getBounds((JCTree)oldT.vartype);
                    localPointer = clab[1];
                } else {
                    localPointer = oldT.pos + oldT.name.length();
                }
            }
        } else if (cLikeArrayChange) {
            for (int i = 0; i < addDimensions; ++i) {
                this.printer.print("[]");
            }
            this.printer.print(" ");
            this.printer.print(newT.name);
        }
        if (newT.init != null && oldT.init != null) {
            int n = localPointer;
            localPointer = this.getCommentCorrectedOldPos((JCTree)oldT.init);
            this.copyTo(n, localPointer);
            localPointer = this.diffTree((JCTree)oldT.init, (JCTree)newT.init, new int[]{localPointer, this.endPos((JCTree)oldT.init)});
        } else {
            if (oldT.init != null && newT.init == null) {
                int pos = CasualDiff.getOldPos((JCTree)oldT.init);
                this.tokenSequence.move(pos);
                PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.BACKWARD);
                PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.BACKWARD);
                this.tokenSequence.moveNext();
                int to = this.tokenSequence.offset();
                this.copyTo(localPointer, to);
                localPointer = this.endPos((JCTree)oldT.init);
            }
            if (oldT.init == null && newT.init != null) {
                int end = this.endPos((JCTree)oldT);
                this.tokenSequence.move(end);
                this.tokenSequence.moveNext();
                if (!JavaTokenId.COMMA.equals((Object)this.tokenSequence.token().id()) && !JavaTokenId.SEMICOLON.equals((Object)this.tokenSequence.token().id())) {
                    this.tokenSequence.movePrevious();
                }
                PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.BACKWARD);
                this.tokenSequence.moveNext();
                int n = localPointer;
                localPointer = this.tokenSequence.offset();
                this.copyTo(n, localPointer);
                this.printer.printVarInit(newT);
            }
        }
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffBlock(JCTree.JCBlock oldT, JCTree.JCBlock newT, int[] blockBounds) {
        int localPointer = blockBounds[0];
        if (oldT.flags != newT.flags) {
            int sp = CasualDiff.getOldPos((JCTree)oldT);
            int n = localPointer;
            localPointer = sp;
            this.copyTo(n, localPointer);
            if ((oldT.flags & 8) == 0 && (newT.flags & 8) != 0) {
                this.printer.print("static");
                if (this.diffContext.style.spaceBeforeStaticInitLeftBrace()) {
                    this.printer.print(" ");
                }
            } else if ((oldT.flags & 8) != 0 && (newT.flags & 8) == 0) {
                this.tokenSequence.move(sp);
                if (this.tokenSequence.moveNext() && this.tokenSequence.token().id() == JavaTokenId.STATIC) {
                    localPointer = this.tokenSequence.offset() + this.tokenSequence.token().length();
                    if (this.tokenSequence.moveNext() && this.tokenSequence.token().id() == JavaTokenId.WHITESPACE) {
                        localPointer = this.tokenSequence.offset() + this.tokenSequence.token().length();
                    }
                }
            }
        } else {
            int n = localPointer;
            localPointer = oldT.pos + 1;
            this.copyTo(n, localPointer);
        }
        PositionEstimator est = EstimatorFactory.statements(this.filterHidden((java.util.List<? extends JCTree>)oldT.stats), this.filterHidden((java.util.List<? extends JCTree>)newT.stats), this.diffContext);
        int old = this.printer.indent();
        localPointer = this.diffInnerComments((JCTree)oldT, (JCTree)newT, localPointer);
        Name oldEnclosing = this.printer.enclClassName;
        this.printer.enclClassName = null;
        java.util.List<JCTree> oldstats = this.filterHidden((java.util.List<? extends JCTree>)oldT.stats);
        localPointer = this.diffList(oldstats, this.filterHidden((java.util.List<? extends JCTree>)newT.stats), localPointer, est, Measure.MEMBER, this.printer);
        this.printer.enclClassName = oldEnclosing;
        if (localPointer < this.endPos((JCTree)oldT)) {
            int n = localPointer;
            localPointer = this.endPos((JCTree)oldT);
            this.copyTo(n, localPointer);
        }
        this.printer.undent(old);
        return localPointer;
    }

    private int adjustLocalPointer(int localPointer, CommentSet cs, CommentSet.RelativePosition position) {
        if (cs == null) {
            return localPointer;
        }
        java.util.List<Comment> cl = cs.getComments(position);
        if (!cl.isEmpty()) {
            for (Comment comment : cl) {
                localPointer = Math.max(comment.endPos(), localPointer);
            }
        }
        return localPointer;
    }

    private boolean isComment(JavaTokenId tid) {
        switch (tid) {
            case LINE_COMMENT: 
            case BLOCK_COMMENT: 
            case JAVADOC_COMMENT: {
                return true;
            }
        }
        return false;
    }

    private int dimension(JCTree t, int afterPos) {
        int[] bounds;
        if (t.getKind() != Tree.Kind.ARRAY_TYPE) {
            return 0;
        }
        int add = afterPos >= 0 ? (afterPos < (bounds = this.getBounds(t))[1] ? 1 : 0) : 1;
        return add + this.dimension(((JCTree.JCArrayTypeTree)t).getType(), afterPos);
    }

    protected int diffDoLoop(JCTree.JCDoWhileLoop oldT, JCTree.JCDoWhileLoop newT, int[] bounds) {
        int localPointer = bounds[0];
        int[] bodyBounds = new int[]{localPointer, this.endPos((JCTree)oldT.body)};
        int oldIndent = newT.body.hasTag(JCTree.Tag.BLOCK) ? -1 : this.printer.indent();
        localPointer = this.diffTree((JCTree)oldT.body, (JCTree)newT.body, bodyBounds, oldT.getKind());
        if (!newT.body.hasTag(JCTree.Tag.BLOCK)) {
            this.printer.undent(oldIndent);
        }
        int[] condBounds = this.getBounds((JCTree)oldT.cond);
        if (oldT.body.getKind() != Tree.Kind.BLOCK && newT.body.getKind() == Tree.Kind.BLOCK) {
            PositionEstimator.moveBackToToken(this.tokenSequence, condBounds[0], JavaTokenId.WHILE);
            localPointer = this.tokenSequence.offset();
        } else {
            this.copyTo(localPointer, condBounds[0]);
            localPointer = this.diffTree((JCTree)oldT.cond, (JCTree)newT.cond, condBounds);
        }
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffWhileLoop(JCTree.JCWhileLoop oldT, JCTree.JCWhileLoop newT, int[] bounds) {
        int localPointer = bounds[0];
        int[] condPos = this.getBounds((JCTree)oldT.cond);
        this.copyTo(localPointer, condPos[0]);
        localPointer = this.diffTree((JCTree)oldT.cond, (JCTree)newT.cond, condPos);
        int[] bodyPos = new int[]{localPointer, this.endPos((JCTree)oldT.body)};
        int oldIndent = newT.body.hasTag(JCTree.Tag.BLOCK) ? -1 : this.printer.indent();
        localPointer = this.diffTree((JCTree)oldT.body, (JCTree)newT.body, bodyPos, oldT.getKind());
        if (!newT.body.hasTag(JCTree.Tag.BLOCK)) {
            this.printer.undent(oldIndent);
        }
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffForLoop(JCTree.JCForLoop oldT, JCTree.JCForLoop newT, int[] bounds) {
        int localPointer;
        if (oldT.init.nonEmpty()) {
            localPointer = CasualDiff.getOldPos((JCTree)oldT.init.head);
        } else {
            PositionEstimator.moveFwdToToken(this.tokenSequence, bounds[0], JavaTokenId.SEMICOLON);
            localPointer = this.tokenSequence.offset();
        }
        this.copyTo(bounds[0], localPointer);
        if (!this.listsMatch((java.util.List<? extends JCTree>)oldT.init, (java.util.List<? extends JCTree>)newT.init)) {
            boolean newVariable;
            boolean oldVariable = CasualDiff.containsVariable(oldT.init);
            if (oldVariable ^ (newVariable = CasualDiff.containsVariable(newT.init))) {
                int oldPrec = this.printer.setPrec(0);
                localPointer = this.diffParameterList((java.util.List<? extends JCTree>)oldT.init, (java.util.List<? extends JCTree>)newT.init, null, localPointer, Measure.ARGUMENT);
                this.printer.setPrec(oldPrec);
            } else if (oldVariable) {
                java.util.List oldInit = NbCollections.checkedListByCopy((java.util.List)oldT.init, JCTree.JCVariableDecl.class, (boolean)false);
                FieldGroupTree old = new FieldGroupTree(oldInit);
                java.util.List newInit = NbCollections.checkedListByCopy((java.util.List)newT.init, JCTree.JCVariableDecl.class, (boolean)false);
                FieldGroupTree nue = new FieldGroupTree(newInit);
                int[] initBounds = this.getBounds((JCTree)oldT.init.head);
                JCTree last = (JCTree)oldT.init.get(oldT.init.size() - 1);
                long endPos = this.diffContext.trees.getSourcePositions().getEndPosition((CompilationUnitTree)this.oldTopLevel, (Tree)last);
                initBounds[1] = (int)endPos;
                localPointer = this.diffTree(old, nue, initBounds);
            } else {
                localPointer = this.diffParameterList((java.util.List<? extends JCTree>)oldT.init, (java.util.List<? extends JCTree>)newT.init, null, localPointer, Measure.ARGUMENT);
            }
        }
        if (oldT.cond != null) {
            int n = localPointer;
            localPointer = CasualDiff.getOldPos((JCTree)oldT.cond);
            this.copyTo(n, localPointer);
            localPointer = this.diffTree((JCTree)oldT.cond, (JCTree)newT.cond, this.getBounds((JCTree)oldT.cond));
        } else {
            PositionEstimator.moveFwdToToken(this.tokenSequence, localPointer, JavaTokenId.SEMICOLON);
            int n = localPointer;
            localPointer = this.tokenSequence.offset();
            this.copyTo(n, localPointer);
        }
        if (oldT.step.nonEmpty()) {
            int n = localPointer;
            localPointer = CasualDiff.getOldPos((JCTree)oldT.step.head);
            this.copyTo(n, localPointer);
        } else {
            PositionEstimator.moveFwdToToken(this.tokenSequence, localPointer, JavaTokenId.SEMICOLON);
            this.tokenSequence.moveNext();
            int n = localPointer;
            localPointer = this.tokenSequence.offset();
            this.copyTo(n, localPointer);
        }
        localPointer = this.diffParameterList((java.util.List<? extends JCTree>)oldT.step, (java.util.List<? extends JCTree>)newT.step, null, localPointer, Measure.ARGUMENT);
        int[] bodyBounds = new int[]{localPointer, this.endPos((JCTree)oldT.body)};
        int oldIndent = newT.body.hasTag(JCTree.Tag.BLOCK) ? -1 : this.printer.indent();
        localPointer = this.diffTree((JCTree)oldT.body, (JCTree)newT.body, bodyBounds, oldT.getKind());
        if (!newT.body.hasTag(JCTree.Tag.BLOCK)) {
            this.printer.undent(oldIndent);
        }
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    private static boolean containsVariable(java.util.List<JCTree.JCStatement> statements) {
        for (JCTree.JCStatement s : statements) {
            if (s.getKind() != Tree.Kind.VARIABLE) continue;
            return true;
        }
        return false;
    }

    protected int diffForeachLoop(JCTree.JCEnhancedForLoop oldT, JCTree.JCEnhancedForLoop newT, int[] bounds) {
        int localPointer = bounds[0];
        int[] varBounds = this.getBounds((JCTree)oldT.var);
        this.copyTo(localPointer, varBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.var, (JCTree)newT.var, varBounds);
        int[] exprBounds = this.getBounds((JCTree)oldT.expr);
        this.copyTo(localPointer, exprBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.expr, (JCTree)newT.expr, exprBounds);
        int[] bodyBounds = new int[]{localPointer, this.endPos((JCTree)oldT.body)};
        int oldIndent = newT.body.hasTag(JCTree.Tag.BLOCK) ? -1 : this.printer.indent();
        localPointer = this.diffTree((JCTree)oldT.body, (JCTree)newT.body, bodyBounds, oldT.getKind());
        if (!newT.body.hasTag(JCTree.Tag.BLOCK)) {
            this.printer.undent(oldIndent);
        }
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffLabelled(JCTree.JCLabeledStatement oldT, JCTree.JCLabeledStatement newT, int[] bounds) {
        int localPointer = bounds[0];
        if (this.nameChanged(oldT.label, newT.label)) {
            int n = localPointer;
            localPointer = CasualDiff.getOldPos((JCTree)oldT);
            this.copyTo(n, localPointer);
            this.printer.print(newT.label);
            localPointer += oldT.label.length();
        }
        int[] bodyBounds = this.getBounds((JCTree)oldT.body);
        this.copyTo(localPointer, bodyBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.body, (JCTree)newT.body, bodyBounds);
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffSwitch(JCTree.JCSwitch oldT, JCTree.JCSwitch newT, int[] bounds) {
        int localPointer = bounds[0];
        int[] selectorBounds = this.getBounds((JCTree)oldT.selector);
        this.copyTo(localPointer, selectorBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.selector, (JCTree)newT.selector, selectorBounds);
        this.tokenSequence.move(selectorBounds[1]);
        while (this.tokenSequence.moveNext() && JavaTokenId.LBRACE != this.tokenSequence.token().id()) {
        }
        this.tokenSequence.moveNext();
        int n = localPointer;
        localPointer = this.tokenSequence.offset();
        this.copyTo(n, localPointer);
        PositionEstimator est = EstimatorFactory.cases(oldT.getCases(), newT.getCases(), this.diffContext);
        localPointer = this.diffList((java.util.List<? extends JCTree>)oldT.cases, (java.util.List<? extends JCTree>)newT.cases, localPointer, est, Measure.MEMBER, this.printer);
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffCase(JCTree.JCCase oldT, JCTree.JCCase newT, int[] bounds) {
        int localPointer = bounds[0];
        if (oldT.pat != null) {
            int[] patBounds = this.getBounds((JCTree)oldT.pat);
            this.copyTo(localPointer, patBounds[0]);
            localPointer = this.diffTree((JCTree)oldT.pat, (JCTree)newT.pat, patBounds);
            this.tokenSequence.move(patBounds[1]);
            while (this.tokenSequence.moveNext() && JavaTokenId.COLON != this.tokenSequence.token().id()) {
            }
            this.tokenSequence.moveNext();
            int n = localPointer;
            localPointer = this.tokenSequence.offset();
            this.copyTo(n, localPointer);
        }
        if (oldT.pat == null && newT.pat != null) {
            this.printer.print((JCTree)newT);
            this.printer.newline();
            return bounds[1];
        }
        PositionEstimator est = EstimatorFactory.statements(oldT.getStatements(), newT.getStatements(), this.diffContext);
        localPointer = this.diffList((java.util.List<? extends JCTree>)oldT.stats, (java.util.List<? extends JCTree>)newT.stats, localPointer, est, Measure.MEMBER, this.printer);
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffSynchronized(JCTree.JCSynchronized oldT, JCTree.JCSynchronized newT, int[] bounds) {
        int localPointer = bounds[0];
        int[] lockBounds = this.getBounds((JCTree)oldT.lock);
        this.copyTo(localPointer, lockBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.lock, (JCTree)newT.lock, lockBounds);
        int[] bodyBounds = this.getBounds((JCTree)oldT.body);
        this.copyTo(localPointer, bodyBounds[0]);
        int oldIndent = newT.body.hasTag(JCTree.Tag.BLOCK) ? -1 : this.printer.indent();
        localPointer = this.diffTree((JCTree)oldT.body, (JCTree)newT.body, bodyBounds);
        if (!newT.body.hasTag(JCTree.Tag.BLOCK)) {
            this.printer.undent(oldIndent);
        }
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffTry(JCTree.JCTry oldT, JCTree.JCTry newT, int[] bounds) {
        int localPointer = bounds[0];
        int[] bodyPos = this.getBounds((JCTree)oldT.body);
        if (!this.listsMatch((java.util.List<? extends JCTree>)oldT.resources, (java.util.List<? extends JCTree>)newT.resources)) {
            if (oldT.resources.nonEmpty() && newT.resources.isEmpty()) {
                this.tokenSequence.move(CasualDiff.getOldPos((JCTree)oldT.resources.head));
                PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.BACKWARD);
                assert (this.tokenSequence.token().id() == JavaTokenId.LPAREN);
                this.copyTo(localPointer, this.tokenSequence.offset());
                localPointer = bodyPos[0];
            } else {
                int pos;
                JavaTokenId[] arrjavaTokenId;
                pos = oldT.resources.isEmpty() ? (pos = bodyPos[0]) : CasualDiff.getOldPos((JCTree)oldT.resources.head);
                this.copyTo(localPointer, pos);
                boolean parens = oldT.resources.isEmpty() || newT.resources.isEmpty();
                int oldPrec = this.printer.setPrec(0);
                if (newT.resources.nonEmpty()) {
                    List l = newT.resources;
                    Tree t = (Tree)l.head;
                    while (t != null) {
                        this.printer.oldTrees.remove((Object)t);
                        l = l.tail;
                        t = (Tree)l.head;
                    }
                }
                if (parens) {
                    JavaTokenId[] arrjavaTokenId2 = new JavaTokenId[2];
                    arrjavaTokenId2[0] = JavaTokenId.LPAREN;
                    arrjavaTokenId = arrjavaTokenId2;
                    arrjavaTokenId2[1] = JavaTokenId.RPAREN;
                } else {
                    arrjavaTokenId = null;
                }
                localPointer = this.diffParameterList((java.util.List<? extends JCTree>)oldT.resources, (java.util.List<? extends JCTree>)newT.resources, arrjavaTokenId, pos, Measure.ARGUMENT, this.diffContext.style.spaceBeforeSemi(), this.diffContext.style.spaceAfterSemi(), false, ";");
                this.printer.setPrec(oldPrec);
                if (parens && oldT.resources.isEmpty()) {
                    this.printer.print(" ");
                }
            }
        }
        this.copyTo(localPointer, bodyPos[0]);
        int n = localPointer = this.diffTree((JCTree)oldT.body, (JCTree)newT.body, bodyPos);
        localPointer = bodyPos[1];
        this.copyTo(n, localPointer);
        PositionEstimator est = EstimatorFactory.catches(oldT.getCatches(), newT.getCatches(), oldT.finalizer != null, this.diffContext);
        localPointer = this.diffList((java.util.List<? extends JCTree>)oldT.catchers, (java.util.List<? extends JCTree>)newT.catchers, localPointer, est, Measure.DEFAULT, this.printer);
        if (oldT.finalizer != null) {
            int[] finalBounds = this.getBounds((JCTree)oldT.finalizer);
            if (newT.finalizer != null) {
                this.copyTo(localPointer, finalBounds[0]);
                localPointer = this.diffTree((JCTree)oldT.finalizer, (JCTree)newT.finalizer, finalBounds);
            } else {
                int endetHier = oldT.catchers.isEmpty() ? Math.max(this.endPos((JCTree)oldT.body), localPointer) : this.endPos(oldT.catchers);
                this.copyTo(localPointer, endetHier);
                localPointer = finalBounds[1];
            }
            this.copyTo(localPointer, bounds[1]);
        } else if (newT.finalizer != null) {
            int catchEnd = oldT.catchers.isEmpty() ? bounds[1] : this.endPos((JCTree)oldT.catchers.reverse().head);
            int n2 = localPointer;
            localPointer = catchEnd;
            this.copyTo(n2, localPointer);
            this.printer.printFinallyBlock(newT.finalizer);
            this.copyTo(localPointer, bounds[1]);
        } else {
            this.copyTo(localPointer, bounds[1]);
        }
        return bounds[1];
    }

    protected int diffCatch(JCTree.JCCatch oldT, JCTree.JCCatch newT, int[] bounds) {
        int localPointer = bounds[0];
        int[] paramBounds = this.getBounds((JCTree)oldT.param);
        this.copyTo(localPointer, paramBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.param, (JCTree)newT.param, paramBounds);
        int[] bodyBounds = this.getBounds((JCTree)oldT.body);
        this.copyTo(localPointer, bodyBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.body, (JCTree)newT.body, bodyBounds);
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffConditional(JCTree.JCConditional oldT, JCTree.JCConditional newT, int[] bounds) {
        int localPointer = bounds[0];
        int[] condBounds = this.getBounds((JCTree)oldT.cond);
        this.copyTo(localPointer, condBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.cond, (JCTree)newT.cond, condBounds);
        int[] trueBounds = this.getBounds((JCTree)oldT.truepart);
        this.copyTo(localPointer, trueBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.truepart, (JCTree)newT.truepart, trueBounds);
        int[] falseBounds = this.getBounds((JCTree)oldT.falsepart);
        this.copyTo(localPointer, falseBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.falsepart, (JCTree)newT.falsepart, falseBounds);
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffIf(JCTree.JCIf oldT, JCTree.JCIf newT, int[] bounds) {
        int localPointer = bounds[0];
        int start = this.printer.toString().length();
        int[] condBounds = this.getCommentCorrectedBounds((JCTree)oldT.cond);
        this.copyTo(localPointer, condBounds[0]);
        int n = localPointer = this.diffTree((JCTree)oldT.cond, (JCTree)newT.cond, null, condBounds);
        localPointer = condBounds[1];
        this.copyTo(n, localPointer);
        int[] partBounds = new int[]{localPointer, this.endPos((JCTree)oldT.thenpart)};
        this.printer.conditionStartHack = start;
        localPointer = this.diffTree((JCTree)oldT.thenpart, (JCTree)newT.thenpart, partBounds, oldT.getKind(), newT.elsepart == null);
        this.printer.conditionStartHack = -1;
        if (oldT.elsepart == null && newT.elsepart != null) {
            int n2 = localPointer;
            localPointer = partBounds[1];
            this.copyTo(n2, localPointer);
            this.printer.printElse(newT, newT.thenpart.getKind() == Tree.Kind.BLOCK);
        } else {
            if (oldT.elsepart != null && newT.elsepart == null) {
                this.copyTo(localPointer, partBounds[1]);
                this.copyTo(this.getBounds((JCTree)oldT.elsepart)[1], bounds[1]);
                return bounds[1];
            }
            if (oldT.elsepart != null) {
                if (oldT.thenpart.getKind() != newT.thenpart.getKind() && newT.thenpart.getKind() == Tree.Kind.BLOCK) {
                    this.tokenSequence.move(localPointer);
                    PositionEstimator.moveToDifferentThan(this.tokenSequence, PositionEstimator.Direction.FORWARD, EnumSet.of(JavaTokenId.WHITESPACE));
                    if (localPointer != this.tokenSequence.offset() && this.diffContext.style.spaceBeforeElse()) {
                        this.printer.print(" ");
                    }
                    localPointer = this.tokenSequence.offset();
                }
                partBounds = new int[]{localPointer, this.endPos((JCTree)oldT.elsepart)};
                localPointer = this.diffTree((JCTree)oldT.elsepart, (JCTree)newT.elsepart, partBounds, oldT.getKind());
                this.tokenSequence.move(localPointer);
                if (this.tokenSequence.movePrevious() && this.tokenSequence.token().id() == JavaTokenId.LINE_COMMENT) {
                    this.printer.newline();
                }
            }
        }
        if (localPointer < bounds[1]) {
            int n3 = localPointer;
            localPointer = bounds[1];
            this.copyTo(n3, localPointer);
        }
        return localPointer;
    }

    protected int diffExec(JCTree.JCExpressionStatement oldT, JCTree.JCExpressionStatement newT, int[] bounds) {
        int localPointer = bounds[0];
        int[] exprBounds = this.getBounds((JCTree)oldT.expr);
        this.copyTo(localPointer, exprBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.expr, (JCTree)newT.expr, exprBounds);
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffBreak(JCTree.JCBreak oldT, JCTree.JCBreak newT, int[] bounds) {
        Name oldTLabel = oldT.label;
        Name newTlabel = newT.label;
        return this.printBreakContinueTree(bounds, oldTLabel, newTlabel, (JCTree.JCStatement)oldT);
    }

    protected int diffContinue(JCTree.JCContinue oldT, JCTree.JCContinue newT, int[] bounds) {
        Name oldTLabel = oldT.label;
        Name newTlabel = newT.label;
        return this.printBreakContinueTree(bounds, oldTLabel, newTlabel, (JCTree.JCStatement)oldT);
    }

    protected int diffReturn(JCTree.JCReturn oldT, JCTree.JCReturn newT, int[] bounds) {
        int localPointer = bounds[0];
        if (oldT.expr != newT.expr) {
            if (oldT.expr == null) {
                this.tokenSequence.move(this.endPos((JCTree)oldT));
                this.tokenSequence.movePrevious();
                int n = localPointer;
                localPointer = this.tokenSequence.offset();
                this.copyTo(n, localPointer);
                if (this.tokenSequence.token().id() == JavaTokenId.SEMICOLON) {
                    this.tokenSequence.movePrevious();
                }
                if (this.tokenSequence.token().id() != JavaTokenId.WHITESPACE) {
                    this.printer.print(" ");
                }
                this.printer.print((JCTree)newT.expr);
            } else if (newT.expr == null) {
                int n = localPointer;
                localPointer = CasualDiff.getOldPos((JCTree)oldT) + "return".length();
                this.copyTo(n, localPointer);
                localPointer = this.endPos((JCTree)oldT.expr);
            } else {
                int[] exprBounds = this.getBounds((JCTree)oldT.expr);
                this.copyTo(bounds[0], exprBounds[0]);
                localPointer = this.diffTree((JCTree)oldT.expr, (JCTree)newT.expr, exprBounds);
            }
        }
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffThrow(JCTree.JCThrow oldT, JCTree.JCThrow newT, int[] bounds) {
        int localPointer = bounds[0];
        int[] exprBounds = this.getBounds((JCTree)oldT.expr);
        this.copyTo(localPointer, exprBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.expr, (JCTree)newT.expr, exprBounds);
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffAssert(JCTree.JCAssert oldT, JCTree.JCAssert newT, int[] bounds) {
        int localPointer = bounds[0];
        int[] condBounds = this.getBounds((JCTree)oldT.cond);
        this.copyTo(localPointer, condBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.cond, (JCTree)newT.cond, condBounds);
        if (oldT.detail != newT.detail) {
            if (oldT.detail == null) {
                this.copyTo(localPointer, condBounds[1]);
                localPointer = condBounds[1];
                this.printer.print(" : ");
                this.printer.print((JCTree)newT.detail);
            } else {
                int[] detailBounds = this.getBounds((JCTree)oldT.detail);
                if (newT.detail == null) {
                    this.copyTo(localPointer, condBounds[1]);
                    localPointer = detailBounds[1];
                } else {
                    this.copyTo(localPointer, detailBounds[0]);
                    localPointer = this.diffTree((JCTree)oldT.detail, (JCTree)newT.detail, detailBounds);
                }
            }
        }
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffApply(JCTree.JCMethodInvocation oldT, JCTree.JCMethodInvocation newT, int[] bounds) {
        int localPointer = bounds[0];
        int[] methBounds = this.getBounds((JCTree)oldT.meth);
        if (Tree.Kind.MEMBER_SELECT == oldT.meth.getKind() && oldT.meth.getKind() == newT.meth.getKind()) {
            localPointer = this.diffSelect((JCTree.JCFieldAccess)oldT.meth, (JCTree.JCFieldAccess)newT.meth, methBounds, oldT.typeargs, newT.typeargs);
        } else if (oldT.typeargs.isEmpty() && newT.typeargs.isEmpty()) {
            localPointer = this.diffTree((JCTree)oldT.meth, (JCTree)newT.meth, methBounds);
        } else {
            this.copyTo(localPointer, methBounds[0]);
            this.printer.printMethodSelect(newT);
            localPointer = methBounds[1];
        }
        if (!this.listsMatch((java.util.List<? extends JCTree>)oldT.args, (java.util.List<? extends JCTree>)newT.args)) {
            if (oldT.args.nonEmpty()) {
                int startArg1 = this.getCommentCorrectedOldPos((JCTree)oldT.args.head);
                this.tokenSequence.move(startArg1);
                PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.BACKWARD);
                this.tokenSequence.moveNext();
                int n = localPointer;
                localPointer = this.tokenSequence.offset();
                this.copyTo(n, localPointer);
            } else {
                int n = localPointer;
                localPointer = methBounds[1];
                this.copyTo(n, localPointer);
                this.tokenSequence.move(localPointer);
                PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.FORWARD);
                this.tokenSequence.moveNext();
                int n2 = localPointer;
                localPointer = this.tokenSequence.offset();
                this.copyTo(n2, localPointer);
            }
            localPointer = this.diffParameterList((java.util.List<? extends JCTree>)oldT.args, (java.util.List<? extends JCTree>)newT.args, null, localPointer, Measure.ARGUMENT);
        }
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffNewClass(JCTree.JCNewClass oldT, JCTree.JCNewClass newT, int[] bounds) {
        java.util.List<JCTree> oldTFilteredArgs;
        int localPointer = bounds[0];
        if (oldT.encl != null) {
            int[] enclBounds = this.getBounds((JCTree)oldT.encl);
            if (newT.encl == null) {
                PositionEstimator.moveFwdToToken(this.tokenSequence, enclBounds[1], JavaTokenId.DOT);
                this.tokenSequence.moveNext();
                localPointer = this.tokenSequence.offset();
            } else {
                localPointer = this.diffTree((JCTree)oldT.encl, (JCTree)newT.encl, enclBounds);
            }
        }
        this.diffParameterList((java.util.List<? extends JCTree>)oldT.typeargs, (java.util.List<? extends JCTree>)newT.typeargs, null, localPointer, Measure.ARGUMENT);
        if (!this.enumConstantPrint) {
            int[] clazzBounds = this.getBounds((JCTree)oldT.clazz);
            this.copyTo(localPointer, clazzBounds[0]);
            localPointer = this.diffTree((JCTree)oldT.clazz, (JCTree)newT.clazz, clazzBounds);
        }
        if (!(oldTFilteredArgs = this.filterHidden((java.util.List<? extends JCTree>)oldT.args)).isEmpty()) {
            int n = localPointer;
            localPointer = CasualDiff.getOldPos(oldTFilteredArgs.get(0));
            this.copyTo(n, localPointer);
        } else if (!this.enumConstantPrint) {
            PositionEstimator.moveFwdToToken(this.tokenSequence, oldT.pos, JavaTokenId.LPAREN);
            this.tokenSequence.moveNext();
            int n = localPointer;
            localPointer = this.tokenSequence.offset();
            this.copyTo(n, localPointer);
        }
        localPointer = this.diffParameterList((java.util.List<? extends JCTree>)oldTFilteredArgs, (java.util.List<? extends JCTree>)newT.args, null, localPointer, Measure.ARGUMENT);
        if (oldT.def != newT.def) {
            if (oldT.def != null && newT.def != null) {
                this.copyTo(localPointer, CasualDiff.getOldPos((JCTree)oldT.def));
                this.anonClass = true;
                localPointer = this.diffTree((JCTree)oldT.def, (JCTree)newT.def, this.getBounds((JCTree)oldT.def));
                this.anonClass = false;
            } else if (newT.def == null) {
                if (this.endPos(oldTFilteredArgs) > localPointer) {
                    this.copyTo(localPointer, this.endPos(oldTFilteredArgs));
                }
                this.printer.print(")");
                localPointer = this.endPos((JCTree)oldT.def);
            } else {
                int n = localPointer;
                localPointer = this.endPos((JCTree)oldT);
                this.copyTo(n, localPointer);
                this.printer.printNewClassBody(newT);
            }
        }
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffNewArray(JCTree.JCNewArray oldT, JCTree.JCNewArray newT, int[] bounds) {
        int localPointer = bounds[0];
        if (newT.elemtype != null) {
            if (oldT.elemtype != null) {
                int[] elemtypeBounds = this.getBounds((JCTree)oldT.elemtype);
                this.copyTo(localPointer, elemtypeBounds[0]);
                localPointer = this.diffTree((JCTree)oldT.elemtype, (JCTree)newT.elemtype, elemtypeBounds);
            }
            if (!this.listsMatch((java.util.List<? extends JCTree>)oldT.dims, (java.util.List<? extends JCTree>)newT.dims) && !newT.dims.isEmpty()) {
                List l1 = oldT.dims;
                List l2 = newT.dims;
                while (l1.nonEmpty()) {
                    int[] span = this.getBounds((JCTree)l1.head);
                    this.copyTo(localPointer, span[0]);
                    localPointer = this.diffTree((JCTree)l1.head, (JCTree)l2.head, span);
                    l1 = l1.tail;
                    l2 = l2.tail;
                }
            }
        } else if (oldT.elemtype != null) {
            this.copyTo(localPointer, CasualDiff.getOldPos((JCTree)oldT));
            if (oldT.elems != null) {
                localPointer = oldT.dims != null && !oldT.dims.isEmpty() ? this.endPos(oldT.dims) : this.endPos((JCTree)oldT.elemtype);
                PositionEstimator.moveFwdToToken(this.tokenSequence, localPointer, JavaTokenId.LBRACE);
                localPointer = this.tokenSequence.offset();
            } else {
                localPointer = this.endPos((JCTree)oldT);
            }
        }
        if (oldT.elems != null) {
            if (oldT.elems.head != null) {
                this.copyTo(localPointer, CasualDiff.getOldPos((JCTree)oldT.elems.head));
                localPointer = this.diffParameterList((java.util.List<? extends JCTree>)oldT.elems, (java.util.List<? extends JCTree>)newT.elems, null, CasualDiff.getOldPos((JCTree)oldT.elems.head), Measure.ARGUMENT);
            } else if (newT.elems != null && !newT.elems.isEmpty()) {
                PositionEstimator.moveFwdToToken(this.tokenSequence, localPointer, JavaTokenId.LBRACE);
                this.tokenSequence.moveNext();
                int n = localPointer;
                localPointer = this.tokenSequence.offset();
                this.copyTo(n, localPointer);
                localPointer = this.diffParameterList((java.util.List<? extends JCTree>)oldT.elems, (java.util.List<? extends JCTree>)newT.elems, null, localPointer, Measure.ARGUMENT);
            }
        } else if (newT.elems != null && !newT.elems.isEmpty()) {
            if (newT.elemtype != null) {
                this.printer.print("[]");
            }
            this.printer.print("{");
            localPointer = this.diffParameterList(Collections.emptyList(), (java.util.List<? extends JCTree>)newT.elems, null, localPointer, Measure.ARGUMENT);
            this.printer.print("}");
            PositionEstimator.moveFwdToToken(this.tokenSequence, localPointer, JavaTokenId.SEMICOLON);
            this.tokenSequence.moveNext();
            localPointer = bounds[1];
        }
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffParens(JCTree.JCParens oldT, JCTree.JCParens newT, int[] bounds) {
        int localPointer = bounds[0];
        this.copyTo(localPointer, this.getCommentCorrectedOldPos((JCTree)oldT.expr));
        localPointer = this.diffTree((JCTree)oldT.expr, (JCTree)newT.expr, this.getBounds((JCTree)oldT.expr));
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffAssign(JCTree.JCAssign oldT, JCTree.JCAssign newT, JCTree parent, int[] bounds) {
        int localPointer = bounds[0];
        int[] lhsBounds = this.getBounds((JCTree)oldT.lhs);
        if (lhsBounds[0] < 0) {
            lhsBounds[0] = CasualDiff.getOldPos((JCTree)oldT.rhs);
            lhsBounds[1] = -1;
        }
        this.copyTo(localPointer, lhsBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.lhs, (JCTree)newT.lhs, lhsBounds);
        int[] rhsBounds = this.getCommentCorrectedBounds((JCTree)oldT.rhs);
        if (oldT.lhs.getKind() == Tree.Kind.IDENTIFIER && newT.lhs.getKind() == Tree.Kind.IDENTIFIER && !((JCTree.JCIdent)oldT.lhs).name.equals((Object)((JCTree.JCIdent)newT.lhs).name)) {
            this.tokenSequence.move(rhsBounds[0]);
            PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.BACKWARD);
            if (this.tokenSequence.token().id() != JavaTokenId.EQ) {
                boolean spaceAroundAssignOps;
                boolean bl = spaceAroundAssignOps = parent.getKind() == Tree.Kind.ANNOTATION || parent.getKind() == Tree.Kind.TYPE_ANNOTATION ? this.diffContext.style.spaceAroundAnnotationValueAssignOps() : this.diffContext.style.spaceAroundAssignOps();
                if (spaceAroundAssignOps) {
                    this.printer.print(" = ");
                } else {
                    this.printer.print("=");
                }
                localPointer = lhsBounds[0];
            }
        }
        this.copyTo(localPointer, rhsBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.rhs, (JCTree)newT.rhs, rhsBounds);
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffAssignop(JCTree.JCAssignOp oldT, JCTree.JCAssignOp newT, int[] bounds) {
        int localPointer = bounds[0];
        int[] lhsBounds = this.getBounds((JCTree)oldT.lhs);
        this.copyTo(localPointer, lhsBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.lhs, (JCTree)newT.lhs, lhsBounds);
        if (oldT.getTag() != newT.getTag()) {
            this.copyTo(localPointer, oldT.pos);
            this.printer.print(this.getAssignementOperator((Tree)newT));
            localPointer = oldT.pos + this.getAssignementOperator((Tree)oldT).length();
        }
        int[] rhsBounds = this.getBounds((JCTree)oldT.rhs);
        this.copyTo(localPointer, rhsBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.rhs, (JCTree)newT.rhs, rhsBounds);
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    String getAssignementOperator(Tree t) {
        switch (t.getKind()) {
            case MULTIPLY_ASSIGNMENT: {
                return "*=";
            }
            case DIVIDE_ASSIGNMENT: {
                return "/=";
            }
            case REMAINDER_ASSIGNMENT: {
                return "%=";
            }
            case PLUS_ASSIGNMENT: {
                return "+=";
            }
            case MINUS_ASSIGNMENT: {
                return "-=";
            }
            case LEFT_SHIFT_ASSIGNMENT: {
                return "<<=";
            }
            case RIGHT_SHIFT_ASSIGNMENT: {
                return ">>=";
            }
            case AND_ASSIGNMENT: {
                return "&=";
            }
            case XOR_ASSIGNMENT: {
                return "^=";
            }
            case OR_ASSIGNMENT: {
                return "|=";
            }
            case UNSIGNED_RIGHT_SHIFT_ASSIGNMENT: {
                return ">>>=";
            }
        }
        throw new IllegalArgumentException("Illegal kind " + (Object)t.getKind());
    }

    protected int diffUnary(JCTree.JCUnary oldT, JCTree.JCUnary newT, int[] bounds) {
        boolean newOpOnLeft;
        int[] argBounds = this.getBounds((JCTree)oldT.arg);
        boolean bl = newOpOnLeft = newT.getKind() != Tree.Kind.POSTFIX_DECREMENT && newT.getKind() != Tree.Kind.POSTFIX_INCREMENT;
        if (newOpOnLeft) {
            if (oldT.getTag() != newT.getTag()) {
                this.printer.print(this.operatorName(newT.getTag()));
            } else {
                this.copyTo(bounds[0], argBounds[0]);
            }
        }
        int localPointer = this.diffTree((JCTree)oldT.arg, (JCTree)newT.arg, argBounds);
        localPointer = this.copyUpTo(localPointer, argBounds[1]);
        if (!newOpOnLeft) {
            if (oldT.getTag() != newT.getTag()) {
                this.printer.print(this.operatorName(newT.getTag()));
            } else {
                this.copyUpTo(localPointer, bounds[1]);
            }
        }
        return bounds[1];
    }

    protected int diffBinary(JCTree.JCBinary oldT, JCTree.JCBinary newT, int[] bounds) {
        int localPointer = bounds[0];
        int[] lhsBounds = this.getBounds((JCTree)oldT.lhs);
        this.copyTo(localPointer, lhsBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.lhs, (JCTree)newT.lhs, lhsBounds);
        if (oldT.getTag() != newT.getTag()) {
            this.copyTo(localPointer, oldT.pos);
            this.printer.print(this.operatorName(newT.getTag()));
            localPointer = oldT.pos + this.operatorName(oldT.getTag()).toString().length();
        }
        int[] rhsBounds = this.getCommentCorrectedBounds((JCTree)oldT.rhs);
        rhsBounds[0] = this.copyUpTo(localPointer, rhsBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.rhs, (JCTree)newT.rhs, rhsBounds);
        return this.copyUpTo(localPointer, bounds[1]);
    }

    private String operatorName(JCTree.Tag tag) {
        return new Pretty(null, false).operatorName(tag);
    }

    protected int diffTypeCast(JCTree.JCTypeCast oldT, JCTree.JCTypeCast newT, int[] bounds) {
        int localPointer = bounds[0];
        int[] clazzBounds = this.getBounds(oldT.clazz);
        this.copyTo(localPointer, clazzBounds[0]);
        localPointer = this.diffTree(oldT.clazz, newT.clazz, clazzBounds);
        int[] exprBounds = this.getBounds((JCTree)oldT.expr);
        exprBounds[0] = this.copyUpTo(localPointer, exprBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.expr, (JCTree)newT.expr, exprBounds);
        localPointer = this.copyUpTo(localPointer, bounds[1]);
        return localPointer;
    }

    protected int diffTypeTest(JCTree.JCInstanceOf oldT, JCTree.JCInstanceOf newT, int[] bounds) {
        int localPointer = bounds[0];
        int[] exprBounds = this.getBounds((JCTree)oldT.expr);
        this.copyTo(localPointer, exprBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.expr, (JCTree)newT.expr, exprBounds);
        int[] clazzBounds = this.getBounds(oldT.clazz);
        clazzBounds[0] = this.copyUpTo(localPointer, clazzBounds[0]);
        localPointer = this.diffTree(oldT.clazz, newT.clazz, clazzBounds);
        localPointer = this.copyUpTo(localPointer, bounds[1]);
        return localPointer;
    }

    protected int diffIndexed(JCTree.JCArrayAccess oldT, JCTree.JCArrayAccess newT, int[] bounds) {
        int localPointer = bounds[0];
        int[] indexedBounds = this.getBounds((JCTree)oldT.indexed);
        this.copyTo(localPointer, indexedBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.indexed, (JCTree)newT.indexed, indexedBounds);
        int[] indexBounds = this.getBounds((JCTree)oldT.index);
        indexBounds[0] = this.copyUpTo(localPointer, indexBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.index, (JCTree)newT.index, indexBounds);
        localPointer = this.copyUpTo(localPointer, bounds[1]);
        return localPointer;
    }

    protected int diffSelect(JCTree.JCFieldAccess oldT, JCTree.JCFieldAccess newT, int[] bounds, List<JCTree.JCExpression> oldTypePar, List<JCTree.JCExpression> newTypePar) {
        int localPointer = bounds[0];
        int[] selectedBounds = this.getBounds((JCTree)oldT.selected);
        this.copyTo(localPointer, selectedBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.selected, (JCTree)newT.selected, selectedBounds);
        if (oldTypePar != null && newTypePar != null) {
            boolean parens;
            int insertHint;
            JavaTokenId[] arrjavaTokenId;
            if (oldTypePar.nonEmpty() && newTypePar.nonEmpty()) {
                insertHint = ((JCTree.JCExpression)oldTypePar.head).pos;
            } else {
                this.tokenSequence.move(selectedBounds[1]);
                PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.FORWARD);
                this.tokenSequence.moveNext();
                insertHint = this.tokenSequence.offset();
            }
            int n = localPointer;
            localPointer = insertHint;
            this.copyTo(n, localPointer);
            boolean bl = parens = oldTypePar.isEmpty() && newTypePar.nonEmpty();
            if (parens) {
                JavaTokenId[] arrjavaTokenId2 = new JavaTokenId[2];
                arrjavaTokenId2[0] = JavaTokenId.LT;
                arrjavaTokenId = arrjavaTokenId2;
                arrjavaTokenId2[1] = JavaTokenId.GT;
            } else {
                arrjavaTokenId = null;
            }
            localPointer = this.diffParameterList((java.util.List<? extends JCTree>)oldTypePar, (java.util.List<? extends JCTree>)newTypePar, arrjavaTokenId, localPointer, Measure.ARGUMENT);
            if (oldTypePar.nonEmpty()) {
                this.tokenSequence.move(this.endPos((JCTree)oldTypePar.last()));
                PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.FORWARD);
                PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.FORWARD);
                int end = this.tokenSequence.offset();
                if (newTypePar.nonEmpty()) {
                    this.copyTo(localPointer, end);
                }
                localPointer = end;
            }
        } else {
            this.tokenSequence.move(selectedBounds[1]);
            if (oldT.name != Names.instance((Context)this.context).error) {
                PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.FORWARD);
                PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.FORWARD);
                int n = localPointer;
                localPointer = this.tokenSequence.offset();
                this.copyTo(n, localPointer);
            }
        }
        if (this.nameChanged(oldT.name, newT.name)) {
            int[] nameSpan = this.treeUtilities.findNameSpan((MemberSelectTree)oldT);
            this.printer.print(newT.name);
            this.diffInfo.put(localPointer, NbBundle.getMessage(CasualDiff.class, (String)"TXT_UpdateReferenceTo", (Object)oldT.name));
            localPointer = nameSpan != null ? nameSpan[1] : (localPointer += oldT.name.length());
        }
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffMemberReference(JCTree.JCMemberReference oldT, JCTree.JCMemberReference newT, int[] bounds) {
        List newTypePar;
        int localPointer = bounds[0];
        int[] exprBounds = this.getBounds((JCTree)oldT.expr);
        this.copyTo(localPointer, exprBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.expr, (JCTree)newT.expr, exprBounds);
        this.tokenSequence.move(exprBounds[1]);
        PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.FORWARD);
        if (this.tokenSequence.token() != null && this.tokenSequence.token().id() == JavaTokenId.COLONCOLON) {
            PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.FORWARD);
            int n = localPointer;
            localPointer = this.tokenSequence.offset();
            this.copyTo(n, localPointer);
        }
        List oldTypePar = oldT.typeargs != null ? oldT.typeargs : List.nil();
        List list = newTypePar = newT.typeargs != null ? newT.typeargs : List.nil();
        if (!this.listsMatch((java.util.List<? extends JCTree>)oldTypePar, (java.util.List<? extends JCTree>)newTypePar)) {
            JavaTokenId[] arrjavaTokenId;
            boolean parens;
            int insertHint = oldTypePar.nonEmpty() && newTypePar.nonEmpty() ? ((JCTree.JCExpression)oldTypePar.head).pos : localPointer;
            int n = localPointer;
            localPointer = insertHint;
            this.copyTo(n, localPointer);
            boolean bl = parens = oldTypePar.isEmpty() && newTypePar.nonEmpty();
            if (parens) {
                JavaTokenId[] arrjavaTokenId2 = new JavaTokenId[2];
                arrjavaTokenId2[0] = JavaTokenId.LT;
                arrjavaTokenId = arrjavaTokenId2;
                arrjavaTokenId2[1] = JavaTokenId.GT;
            } else {
                arrjavaTokenId = null;
            }
            localPointer = this.diffParameterList((java.util.List<? extends JCTree>)oldTypePar, (java.util.List<? extends JCTree>)newTypePar, arrjavaTokenId, localPointer, Measure.ARGUMENT);
            if (oldTypePar.nonEmpty()) {
                this.tokenSequence.move(this.endPos((JCTree)oldTypePar.last()));
                PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.FORWARD);
                PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.FORWARD);
                int end = this.tokenSequence.offset();
                if (newTypePar.nonEmpty()) {
                    this.copyTo(localPointer, end);
                }
                localPointer = end;
            }
        }
        if (this.nameChanged(oldT.name, newT.name)) {
            this.printer.print(newT.name);
            this.diffInfo.put(localPointer, NbBundle.getMessage(CasualDiff.class, (String)"TXT_UpdateReferenceTo", (Object)oldT.name));
            localPointer += oldT.name.length();
        }
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffSelect(JCTree.JCFieldAccess oldT, JCTree.JCFieldAccess newT, int[] bounds) {
        return this.diffSelect(oldT, newT, bounds, null, null);
    }

    protected int diffIdent(JCTree.JCIdent oldT, JCTree.JCIdent newT, int[] bounds) {
        if (this.nameChanged(oldT.name, newT.name)) {
            this.copyTo(bounds[0], oldT.pos);
            this.printer.print(newT.name);
            this.diffInfo.put(oldT.pos, NbBundle.getMessage(CasualDiff.class, (String)"TXT_UpdateReferenceTo", (Object)oldT.name));
        } else {
            this.copyTo(bounds[0], bounds[1]);
        }
        return bounds[1];
    }

    protected int diffLiteral(JCTree.JCLiteral oldT, JCTree.JCLiteral newT, int[] bounds) {
        if (oldT.typetag != newT.typetag || oldT.value != null && !oldT.value.equals(newT.value)) {
            int localPointer = bounds[0];
            int[] literalBounds = this.getBounds((JCTree)oldT);
            this.copyTo(localPointer, literalBounds[0]);
            this.printer.print((JCTree)newT);
            this.copyTo(literalBounds[1], bounds[1]);
        } else {
            this.copyTo(bounds[0], bounds[1]);
        }
        return bounds[1];
    }

    protected int diffTypeIdent(JCTree.JCPrimitiveTypeTree oldT, JCTree.JCPrimitiveTypeTree newT, int[] bounds) {
        if (oldT.typetag != newT.typetag) {
            this.printer.print((JCTree)newT);
        } else {
            this.copyTo(bounds[0], bounds[1]);
        }
        return bounds[1];
    }

    protected int diffTypeArray(JCTree.JCArrayTypeTree oldT, JCTree.JCArrayTypeTree newT, int[] bounds) {
        int localPointer = bounds[0];
        int[] elemtypeBounds = this.getBounds((JCTree)oldT.elemtype);
        localPointer = this.diffTree((JCTree)oldT.elemtype, (JCTree)newT.elemtype, elemtypeBounds);
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffTypeApply(JCTree.JCTypeApply oldT, JCTree.JCTypeApply newT, int[] bounds) {
        int localPointer = bounds[0];
        int[] clazzBounds = this.getBounds((JCTree)oldT.clazz);
        this.copyTo(localPointer, clazzBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.clazz, (JCTree)newT.clazz, clazzBounds);
        if (!this.listsMatch((java.util.List<? extends JCTree>)oldT.arguments, (java.util.List<? extends JCTree>)newT.arguments)) {
            JavaTokenId[] arrjavaTokenId;
            int pos = oldT.arguments.nonEmpty() ? CasualDiff.getOldPos((JCTree)oldT.arguments.head) : this.endPos((JCTree)oldT.clazz);
            this.copyTo(localPointer, pos);
            boolean printBrace = false;
            if (printBrace) {
                JavaTokenId[] arrjavaTokenId2 = new JavaTokenId[2];
                arrjavaTokenId2[0] = JavaTokenId.LT;
                arrjavaTokenId = arrjavaTokenId2;
                arrjavaTokenId2[1] = JavaTokenId.GT;
            } else {
                arrjavaTokenId = null;
            }
            localPointer = this.diffParameterList((java.util.List<? extends JCTree>)oldT.arguments, (java.util.List<? extends JCTree>)newT.arguments, arrjavaTokenId, pos, Measure.ARGUMENT);
        }
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffAnnotatedType(JCTree.JCAnnotatedType oldT, JCTree.JCAnnotatedType newT, int[] bounds) {
        int localPointer = bounds[0];
        if (!this.listsMatch((java.util.List<? extends JCTree>)oldT.annotations, (java.util.List<? extends JCTree>)newT.annotations)) {
            int pos = oldT.annotations.nonEmpty() ? CasualDiff.getOldPos((JCTree)oldT.annotations.head) : bounds[0];
            this.copyTo(localPointer, pos);
            localPointer = this.diffParameterList((java.util.List<? extends JCTree>)oldT.annotations, (java.util.List<? extends JCTree>)newT.annotations, null, null, pos, Measure.ARGUMENT, true, false, false, "");
        }
        int[] underlyingBounds = this.getBounds((JCTree)oldT.underlyingType);
        this.copyTo(localPointer, underlyingBounds[0]);
        localPointer = this.diffTree((JCTree)oldT.underlyingType, (JCTree)newT.underlyingType, underlyingBounds);
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffTypeParameter(JCTree.JCTypeParameter oldT, JCTree.JCTypeParameter newT, int[] bounds) {
        int localPointer = bounds[0];
        this.copyTo(localPointer, CasualDiff.getOldPos((JCTree)oldT));
        if (this.nameChanged(oldT.name, newT.name)) {
            this.printer.print(newT.name);
            localPointer += oldT.name.length();
        }
        if (!this.listsMatch((java.util.List<? extends JCTree>)oldT.bounds, (java.util.List<? extends JCTree>)newT.bounds)) {
            int pos;
            PositionEstimator est = EstimatorFactory.implementz(oldT.getBounds(), newT.getBounds(), this.diffContext);
            int n = pos = oldT.bounds.nonEmpty() ? CasualDiff.getOldPos((JCTree)oldT.bounds.head) : -1;
            if (pos > -1) {
                this.copyTo(localPointer, pos);
                localPointer = this.diffList2((java.util.List<? extends JCTree>)oldT.bounds, (java.util.List<? extends JCTree>)newT.bounds, pos, est);
            }
        }
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffWildcard(JCTree.JCWildcard oldT, JCTree.JCWildcard newT, int[] bounds) {
        JCTree newBound;
        int localPointer = bounds[0];
        if (oldT.kind != newT.kind) {
            this.copyTo(localPointer, oldT.pos);
            this.printer.print(newT.kind.toString());
            localPointer = oldT.pos + oldT.kind.toString().length();
        }
        JCTree oldBound = oldT.kind.kind != BoundKind.UNBOUND ? oldT.inner : null;
        JCTree jCTree = newBound = newT.kind.kind != BoundKind.UNBOUND ? newT.inner : null;
        if (oldBound == newBound && oldBound == null) {
            return localPointer;
        }
        int[] innerBounds = this.getBounds(oldBound);
        this.copyTo(localPointer, innerBounds[0]);
        localPointer = this.diffTree(oldBound, newBound, innerBounds);
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffTypeBoundKind(JCTree.TypeBoundKind oldT, JCTree.TypeBoundKind newT, int[] bounds) {
        int localPointer = bounds[0];
        if (oldT.kind != newT.kind) {
            this.copyTo(localPointer, oldT.pos);
            this.printer.print(newT.kind.toString());
            localPointer = oldT.pos + oldT.kind.toString().length();
        }
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffAnnotation(JCTree.JCAnnotation oldT, JCTree.JCAnnotation newT, int[] bounds) {
        int localPointer = bounds[0];
        int[] annotationBounds = this.getBounds(oldT.annotationType);
        this.copyTo(localPointer, annotationBounds[0]);
        localPointer = this.diffTree(oldT.annotationType, newT.annotationType, annotationBounds);
        JavaTokenId[] parens = null;
        if (oldT.args.nonEmpty()) {
            int n = localPointer;
            localPointer = CasualDiff.getOldPos((JCTree)oldT.args.head);
            this.copyTo(n, localPointer);
        } else {
            int endPos = this.endPos((JCTree)oldT);
            this.tokenSequence.move(endPos);
            this.tokenSequence.movePrevious();
            if (JavaTokenId.RPAREN != this.tokenSequence.token().id()) {
                parens = new JavaTokenId[]{JavaTokenId.LPAREN, JavaTokenId.RPAREN};
            } else {
                --endPos;
            }
            int n = localPointer;
            localPointer = endPos;
            this.copyTo(n, localPointer);
        }
        localPointer = this.diffParameterList((java.util.List<? extends JCTree>)oldT.args, (java.util.List<? extends JCTree>)newT.args, (JCTree)oldT, parens, localPointer, Measure.ARGUMENT);
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffModifiers(JCTree.JCModifiers oldT, JCTree.JCModifiers newT, JCTree parent, int localPointer) {
        if (oldT == newT) {
            return localPointer;
        }
        int startPos = oldT.pos != -1 ? CasualDiff.getOldPos((JCTree)oldT) : CasualDiff.getOldPos(parent);
        int firstAnnotationPos = !oldT.getAnnotations().isEmpty() ? CasualDiff.getOldPos((JCTree)oldT.getAnnotations().head) : -1;
        int endOffset = this.endPos((JCTree)oldT);
        if (startPos < firstAnnotationPos && oldT.flags != newT.flags) {
            this.copyTo(localPointer, startPos);
            this.printer.printFlags(newT.flags & -513, oldT.getFlags().isEmpty());
            this.tokenSequence.move(firstAnnotationPos);
            PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.BACKWARD);
            this.tokenSequence.moveNext();
            localPointer = this.tokenSequence.offset();
        }
        localPointer = this.diffAnnotationsLists(oldT.getAnnotations(), newT.getAnnotations(), startPos, localPointer);
        if ((oldT.flags & 8192) != 0) {
            this.tokenSequence.move(endOffset);
            this.tokenSequence.movePrevious();
            PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.BACKWARD);
            this.tokenSequence.moveNext();
            endOffset = this.tokenSequence.offset();
        }
        if (oldT.flags != newT.flags && startPos >= firstAnnotationPos) {
            if (localPointer == startPos) {
                if ((newT.flags & -513) != 0) {
                    this.printer.printFlags(newT.flags & -513, oldT.getFlags().isEmpty());
                    localPointer = endOffset > 0 ? endOffset : localPointer;
                } else if (endOffset > 0) {
                    this.tokenSequence.move(endOffset);
                    while (this.tokenSequence.moveNext() && JavaTokenId.WHITESPACE == this.tokenSequence.token().id()) {
                    }
                    localPointer = this.tokenSequence.offset();
                }
            } else {
                this.tokenSequence.move(localPointer);
                PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.FORWARD);
                int n = localPointer;
                localPointer = this.tokenSequence.offset();
                this.copyTo(n, localPointer);
                localPointer = this.tokenSequence.offset();
                if (!oldT.getFlags().isEmpty()) {
                    localPointer = endOffset;
                }
                this.printer.printFlags(newT.flags, oldT.getFlags().isEmpty());
            }
        } else if (endOffset > localPointer) {
            if (localPointer == startPos) {
                this.printer.toLeftMargin();
            }
            int n = localPointer;
            localPointer = endOffset;
            this.copyTo(n, localPointer);
        }
        return localPointer;
    }

    private int diffAnnotationsLists(List<JCTree.JCAnnotation> oldAnnotations, List<JCTree.JCAnnotation> newAnnotations, int startPos, int localPointer) {
        int annotationsEnd;
        int n = annotationsEnd = oldAnnotations.nonEmpty() ? this.endPos(oldAnnotations) : localPointer;
        if (this.listsMatch((java.util.List<? extends JCTree>)oldAnnotations, (java.util.List<? extends JCTree>)newAnnotations)) {
            int n2 = localPointer;
            localPointer = annotationsEnd != localPointer ? annotationsEnd : startPos;
            this.copyTo(n2, localPointer);
        } else {
            this.tokenSequence.move(startPos);
            if (this.tokenSequence.movePrevious() && JavaTokenId.WHITESPACE == this.tokenSequence.token().id()) {
                String text = this.tokenSequence.token().text().toString();
                int index = text.lastIndexOf(10);
                startPos = this.tokenSequence.offset();
                if (index > -1) {
                    startPos += index + 1;
                }
                if (startPos < localPointer) {
                    startPos = localPointer;
                }
            }
            this.copyTo(localPointer, startPos);
            PositionEstimator est = EstimatorFactory.annotations(oldAnnotations, newAnnotations, this.diffContext, this.parameterPrint);
            localPointer = this.diffList((java.util.List<? extends JCTree>)oldAnnotations, (java.util.List<? extends JCTree>)newAnnotations, startPos, est, Measure.ARGUMENT, this.printer);
        }
        return localPointer;
    }

    protected void diffLetExpr(JCTree.LetExpr oldT, JCTree.LetExpr newT) {
    }

    protected void diffErroneous(JCTree.JCErroneous oldT, JCTree.JCErroneous newT, int[] bounds) {
        JCTree oldTident = (JCTree)oldT.getErrorTrees().get(0);
        JCTree newTident = (JCTree)newT.getErrorTrees().get(0);
        if (oldTident.getKind() == Tree.Kind.IDENTIFIER && newTident.getKind() == Tree.Kind.IDENTIFIER) {
            this.diffIdent((JCTree.JCIdent)oldTident, (JCTree.JCIdent)newTident, bounds);
        }
    }

    protected int diffLambda(JCTree.JCLambda oldT, JCTree.JCLambda newT, int[] bounds) {
        int posHint;
        JavaTokenId id;
        int localPointer = bounds[0];
        if (oldT.params.isEmpty()) {
            int startOffset = oldT.pos;
            PositionEstimator.moveFwdToToken(this.tokenSequence, startOffset, JavaTokenId.RPAREN);
            posHint = this.tokenSequence.offset();
        } else {
            posHint = ((JCTree.JCVariableDecl)oldT.params.iterator().next()).getStartPosition();
        }
        if (!this.listsMatch((java.util.List<? extends JCTree>)oldT.params, (java.util.List<? extends JCTree>)newT.params)) {
            JavaTokenId id2;
            this.copyTo(localPointer, posHint);
            int old = this.printer.setPrec(0);
            this.parameterPrint = true;
            Name oldEnclClassName = this.printer.enclClassName;
            this.printer.enclClassName = null;
            this.suppressParameterTypes = newT.paramKind == JCTree.JCLambda.ParameterKind.IMPLICIT;
            JavaTokenId[] parens = null;
            if (newT.params.size() > 1 && (id2 = PositionEstimator.moveFwdToOneOfTokens(this.tokenSequence, oldT.params.isEmpty() ? posHint : this.endPos((JCTree)oldT.params.last()), LAMBDA_PARAM_END_TOKENS)) != JavaTokenId.RPAREN) {
                parens = new JavaTokenId[]{JavaTokenId.LPAREN, JavaTokenId.RPAREN};
            }
            localPointer = this.diffParameterList((java.util.List<? extends JCTree>)oldT.params, (java.util.List<? extends JCTree>)newT.params, parens, posHint, Measure.MEMBER);
            this.suppressParameterTypes = false;
            this.printer.enclClassName = oldEnclClassName;
            this.parameterPrint = false;
            this.printer.setPrec(old);
        }
        if ((id = PositionEstimator.moveFwdToOneOfTokens(this.tokenSequence, oldT.params.isEmpty() ? posHint : this.endPos((JCTree)oldT.params.last()), LAMBDA_PARAM_END_TOKENS)) == JavaTokenId.RPAREN) {
            this.tokenSequence.moveNext();
        }
        if (localPointer < (posHint = this.tokenSequence.offset())) {
            int n = localPointer;
            localPointer = posHint;
            this.copyTo(n, localPointer);
        }
        if (oldT.body != null && newT.body != null) {
            int[] bodyBounds = this.getBounds(oldT.body);
            this.copyTo(localPointer, bodyBounds[0]);
            localPointer = this.diffTree(oldT.body, newT.body, bodyBounds);
        }
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    protected int diffFieldGroup(FieldGroupTree oldT, FieldGroupTree newT, int[] bounds) {
        if (!this.listsMatch(oldT.getVariables(), newT.getVariables())) {
            int localpointer = this.getCommentCorrectedOldPos((JCTree)oldT.getVariables().get(0));
            if (bounds[0] < localpointer) {
                this.copyTo(bounds[0], localpointer);
            } else {
                localpointer = bounds[0];
            }
            if (oldT.isEnum()) {
                int pos = this.diffParameterList(oldT.getVariables(), newT.getVariables(), null, localpointer, Measure.ARGUMENT, this.diffContext.style.spaceBeforeComma(), this.diffContext.style.spaceAfterComma(), true, ",");
                this.copyTo(pos, bounds[1]);
                return bounds[1];
            }
            int pos = this.diffVarGroup(oldT.getVariables(), newT.getVariables(), null, localpointer, Measure.GROUP_VAR_MEASURE);
            this.copyTo(pos, bounds[1]);
            return bounds[1];
        }
        this.tokenSequence.move(oldT.endPos());
        PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.FORWARD);
        this.tokenSequence.moveNext();
        return this.tokenSequence.offset();
    }

    protected boolean listContains(java.util.List<? extends JCTree> list, JCTree tree) {
        for (JCTree t : list) {
            if (!this.treesMatch(t, tree)) continue;
            return true;
        }
        return false;
    }

    protected boolean treesMatch(JCTree t1, JCTree t2) {
        return this.treesMatch(t1, t2, true);
    }

    public boolean treesMatch(JCTree t1, JCTree t2, boolean deepMatch) {
        if (t1 == t2) {
            return true;
        }
        if (t1 == null || t2 == null) {
            return false;
        }
        if (t1.getTag() != t2.getTag()) {
            return false;
        }
        if (!deepMatch) {
            return true;
        }
        switch (t1.getTag()) {
            case TOPLEVEL: {
                return ((JCTree.JCCompilationUnit)t1).sourcefile.equals(((JCTree.JCCompilationUnit)t2).sourcefile);
            }
            case IMPORT: {
                return this.matchImport((JCTree.JCImport)t1, (JCTree.JCImport)t2);
            }
            case CLASSDEF: {
                return ((JCTree.JCClassDecl)t1).sym == ((JCTree.JCClassDecl)t2).sym;
            }
            case METHODDEF: {
                return ((JCTree.JCMethodDecl)t1).sym == ((JCTree.JCMethodDecl)t2).sym;
            }
            case VARDEF: {
                return ((JCTree.JCVariableDecl)t1).sym == ((JCTree.JCVariableDecl)t2).sym;
            }
            case SKIP: {
                return true;
            }
            case BLOCK: {
                return this.matchBlock((JCTree.JCBlock)t1, (JCTree.JCBlock)t2);
            }
            case DOLOOP: {
                return this.matchDoLoop((JCTree.JCDoWhileLoop)t1, (JCTree.JCDoWhileLoop)t2);
            }
            case WHILELOOP: {
                return this.matchWhileLoop((JCTree.JCWhileLoop)t1, (JCTree.JCWhileLoop)t2);
            }
            case FORLOOP: {
                return this.matchForLoop((JCTree.JCForLoop)t1, (JCTree.JCForLoop)t2);
            }
            case FOREACHLOOP: {
                return this.matchForeachLoop((JCTree.JCEnhancedForLoop)t1, (JCTree.JCEnhancedForLoop)t2);
            }
            case LABELLED: {
                return this.matchLabelled((JCTree.JCLabeledStatement)t1, (JCTree.JCLabeledStatement)t2);
            }
            case SWITCH: {
                return this.matchSwitch((JCTree.JCSwitch)t1, (JCTree.JCSwitch)t2);
            }
            case CASE: {
                return this.matchCase((JCTree.JCCase)t1, (JCTree.JCCase)t2);
            }
            case SYNCHRONIZED: {
                return this.matchSynchronized((JCTree.JCSynchronized)t1, (JCTree.JCSynchronized)t2);
            }
            case TRY: {
                return this.matchTry((JCTree.JCTry)t1, (JCTree.JCTry)t2);
            }
            case CATCH: {
                return this.matchCatch((JCTree.JCCatch)t1, (JCTree.JCCatch)t2);
            }
            case CONDEXPR: {
                return this.matchConditional((JCTree.JCConditional)t1, (JCTree.JCConditional)t2);
            }
            case IF: {
                return this.matchIf((JCTree.JCIf)t1, (JCTree.JCIf)t2);
            }
            case EXEC: {
                return this.treesMatch((JCTree)((JCTree.JCExpressionStatement)t1).expr, (JCTree)((JCTree.JCExpressionStatement)t2).expr);
            }
            case BREAK: {
                return this.matchBreak((JCTree.JCBreak)t1, (JCTree.JCBreak)t2);
            }
            case CONTINUE: {
                return this.matchContinue((JCTree.JCContinue)t1, (JCTree.JCContinue)t2);
            }
            case RETURN: {
                return this.treesMatch((JCTree)((JCTree.JCReturn)t1).expr, (JCTree)((JCTree.JCReturn)t2).expr);
            }
            case THROW: {
                return this.treesMatch((JCTree)((JCTree.JCThrow)t1).expr, (JCTree)((JCTree.JCThrow)t2).expr);
            }
            case ASSERT: {
                return this.matchAssert((JCTree.JCAssert)t1, (JCTree.JCAssert)t2);
            }
            case APPLY: {
                return this.matchApply((JCTree.JCMethodInvocation)t1, (JCTree.JCMethodInvocation)t2);
            }
            case NEWCLASS: {
                if (((JCTree.JCNewClass)t2).def != null) {
                    ((JCTree.JCNewClass)t2).def.sym = null;
                }
                return this.matchNewClass((JCTree.JCNewClass)t1, (JCTree.JCNewClass)t2);
            }
            case NEWARRAY: {
                return this.matchNewArray((JCTree.JCNewArray)t1, (JCTree.JCNewArray)t2);
            }
            case PARENS: {
                return this.treesMatch((JCTree)((JCTree.JCParens)t1).expr, (JCTree)((JCTree.JCParens)t2).expr);
            }
            case ASSIGN: {
                return this.matchAssign((JCTree.JCAssign)t1, (JCTree.JCAssign)t2);
            }
            case TYPECAST: {
                return this.matchTypeCast((JCTree.JCTypeCast)t1, (JCTree.JCTypeCast)t2);
            }
            case TYPETEST: {
                return this.matchTypeTest((JCTree.JCInstanceOf)t1, (JCTree.JCInstanceOf)t2);
            }
            case INDEXED: {
                return this.matchIndexed((JCTree.JCArrayAccess)t1, (JCTree.JCArrayAccess)t2);
            }
            case SELECT: {
                return this.matchSelect((JCTree.JCFieldAccess)t1, (JCTree.JCFieldAccess)t2);
            }
            case REFERENCE: {
                return this.matchReference((JCTree.JCMemberReference)t1, (JCTree.JCMemberReference)t2);
            }
            case IDENT: {
                return ((JCTree.JCIdent)t1).getName().contentEquals((CharSequence)((JCTree.JCIdent)t2).getName());
            }
            case LITERAL: {
                return this.matchLiteral((JCTree.JCLiteral)t1, (JCTree.JCLiteral)t2);
            }
            case TYPEIDENT: {
                return ((JCTree.JCPrimitiveTypeTree)t1).typetag == ((JCTree.JCPrimitiveTypeTree)t2).typetag;
            }
            case TYPEARRAY: {
                return this.treesMatch((JCTree)((JCTree.JCArrayTypeTree)t1).elemtype, (JCTree)((JCTree.JCArrayTypeTree)t2).elemtype);
            }
            case TYPEAPPLY: {
                return this.matchTypeApply((JCTree.JCTypeApply)t1, (JCTree.JCTypeApply)t2);
            }
            case TYPEPARAMETER: {
                return this.matchTypeParameter((JCTree.JCTypeParameter)t1, (JCTree.JCTypeParameter)t2);
            }
            case WILDCARD: {
                return this.matchWildcard((JCTree.JCWildcard)t1, (JCTree.JCWildcard)t2);
            }
            case TYPEBOUNDKIND: {
                return ((JCTree.TypeBoundKind)t1).kind == ((JCTree.TypeBoundKind)t2).kind;
            }
            case ANNOTATION: 
            case TYPE_ANNOTATION: {
                return this.matchAnnotation((JCTree.JCAnnotation)t1, (JCTree.JCAnnotation)t2);
            }
            case LETEXPR: {
                return this.matchLetExpr((JCTree.LetExpr)t1, (JCTree.LetExpr)t2);
            }
            case POS: 
            case NEG: 
            case NOT: 
            case COMPL: 
            case PREINC: 
            case PREDEC: 
            case POSTINC: 
            case POSTDEC: 
            case NULLCHK: {
                return this.matchUnary((JCTree.JCUnary)t1, (JCTree.JCUnary)t2);
            }
            case OR: 
            case AND: 
            case BITOR: 
            case BITXOR: 
            case BITAND: 
            case EQ: 
            case NE: 
            case LT: 
            case GT: 
            case LE: 
            case GE: 
            case SL: 
            case SR: 
            case USR: 
            case PLUS: 
            case MINUS: 
            case MUL: 
            case DIV: 
            case MOD: {
                return this.matchBinary((JCTree.JCBinary)t1, (JCTree.JCBinary)t2);
            }
            case BITOR_ASG: 
            case BITXOR_ASG: 
            case BITAND_ASG: 
            case SL_ASG: 
            case SR_ASG: 
            case USR_ASG: 
            case PLUS_ASG: 
            case MINUS_ASG: 
            case MUL_ASG: 
            case DIV_ASG: 
            case MOD_ASG: {
                return this.matchAssignop((JCTree.JCAssignOp)t1, (JCTree.JCAssignOp)t2);
            }
            case ANNOTATED_TYPE: {
                return this.matchAnnotatedType((JCTree.JCAnnotatedType)t1, (JCTree.JCAnnotatedType)t2);
            }
            case LAMBDA: {
                return this.matchLambda((JCTree.JCLambda)t1, (JCTree.JCLambda)t2);
            }
            case ERRONEOUS: {
                SourcePositions sps = this.diffContext.trees.getSourcePositions();
                int a1 = (int)sps.getStartPosition((CompilationUnitTree)this.diffContext.origUnit, (Tree)t1);
                int a2 = (int)sps.getEndPosition((CompilationUnitTree)this.diffContext.origUnit, (Tree)t1);
                int b1 = (int)sps.getStartPosition((CompilationUnitTree)this.diffContext.origUnit, (Tree)t2);
                int b2 = (int)sps.getEndPosition((CompilationUnitTree)this.diffContext.origUnit, (Tree)t2);
                if (a1 == b1 && a2 == b2) {
                    return true;
                }
                if (a1 == -2 || a2 == -2 || b1 == -2 || b2 == -2) {
                    return false;
                }
                if (a1 == -1 || a2 == -1 || b1 == -1 || b2 == -1) {
                    return false;
                }
                String sa = this.diffContext.origText.substring(a1, a2);
                String sb = this.diffContext.origText.substring(b1, b2);
                return sa.equals(sb);
            }
        }
        String msg = t1.getKind().toString() + " " + t1.getClass().getName();
        throw new AssertionError((Object)msg);
    }

    private boolean kindChanged(long oldFlags, long newFlags) {
        return (oldFlags & 25088) != (newFlags & 25088);
    }

    protected boolean nameChanged(Name oldName, Name newName) {
        byte[] arr2;
        if (oldName == newName) {
            return false;
        }
        if (oldName == null || newName == null) {
            return true;
        }
        byte[] arr1 = oldName.toUtf();
        int len = arr1.length;
        if (len != (arr2 = newName.toUtf()).length) {
            return true;
        }
        for (int i = 0; i < len; ++i) {
            if (arr1[i] == arr2[i]) continue;
            return true;
        }
        return false;
    }

    protected int diffList2(java.util.List<? extends JCTree> oldList, java.util.List<? extends JCTree> newList, int initialPos, PositionEstimator estimator) {
        if (oldList == newList) {
            return initialPos;
        }
        assert (oldList != null && newList != null);
        int lastOldPos = initialPos;
        ListMatcher<? extends JCTree> matcher = ListMatcher.instance(oldList, newList);
        if (!matcher.match()) {
            return initialPos;
        }
        Iterator<? extends JCTree> oldIter = oldList.iterator();
        ListMatcher.ResultItem<? extends JCTree>[] result = matcher.getTransformedResult();
        ListMatcher.Separator s = matcher.separatorInstance();
        s.compute();
        int[][] matrix = estimator.getMatrix();
        int testPos = initialPos;
        int i = 0;
        int newIndex = 0;
        boolean firstNewItem = true;
        block6 : for (int j = 0; j < result.length; ++j) {
            ListMatcher.ResultItem<? extends JCTree> item = result[j];
            switch (item.operation) {
                JCTree oldT;
                case MODIFY: {
                    this.tokenSequence.moveIndex(matrix[i][4]);
                    if (this.tokenSequence.moveNext()) {
                        testPos = this.tokenSequence.offset();
                        if (JavaTokenId.COMMA == this.tokenSequence.token().id()) {
                            testPos += JavaTokenId.COMMA.fixedText().length();
                        }
                    }
                    oldT = oldIter.next();
                    ++i;
                    if (!firstNewItem) {
                        this.copyTo(lastOldPos, CasualDiff.getOldPos(oldT));
                    }
                    if (this.treesMatch(oldT, (JCTree)item.element, false)) {
                        lastOldPos = this.diffTree(oldT, (JCTree)item.element, this.getBounds(oldT));
                    } else {
                        this.printer.print((JCTree)item.element);
                        lastOldPos = Math.max(testPos, this.endPos(oldT));
                    }
                    firstNewItem = false;
                    ++newIndex;
                    continue block6;
                }
                case INSERT: {
                    String tail;
                    String prec = s.head(j) ? estimator.head() : (s.prev(j) ? estimator.sep() : null);
                    String string = tail = s.next(j) ? estimator.sep() : null;
                    if (estimator.getIndentString() != null && !estimator.getIndentString().equals(" ")) {
                        prec = prec + estimator.getIndentString();
                    }
                    this.copyTo(lastOldPos, testPos);
                    this.printer.print(prec);
                    this.printer.print((JCTree)item.element);
                    this.printer.print(tail);
                    firstNewItem = false;
                    ++newIndex;
                    continue block6;
                }
                case DELETE: {
                    int delta = 0;
                    if (i == 0 && matrix[i + 1][2] != -1 && matrix[i + 1][2] == matrix[i + 1][3]) {
                        ++delta;
                    }
                    int startOffset = this.toOff(s.head(j) || s.prev(j) ? matrix[i][1] : matrix[i][2 + delta]);
                    int endOffset = this.toOff(s.tail(j) || s.next(j) ? matrix[i + 1][2] : matrix[i][4]);
                    assert (startOffset != -1 && endOffset != -1);
                    this.tokenSequence.moveIndex(matrix[i][4]);
                    if (this.tokenSequence.moveNext()) {
                        testPos = this.tokenSequence.offset();
                        if (JavaTokenId.COMMA == this.tokenSequence.token().id()) {
                            PositionEstimator.moveToDifferentThan(this.tokenSequence, PositionEstimator.Direction.FORWARD, EnumSet.of(JavaTokenId.WHITESPACE));
                            testPos = this.tokenSequence.offset();
                        }
                    }
                    lastOldPos = i == 0 && !newList.isEmpty() ? endOffset : Math.max(testPos, this.endPos((JCTree)item.element));
                    oldT = oldIter.next();
                    ++i;
                    continue block6;
                }
                case NOCHANGE: {
                    this.tokenSequence.moveIndex(matrix[i][4]);
                    if (this.tokenSequence.moveNext()) {
                        testPos = this.tokenSequence.offset();
                        if (JavaTokenId.COMMA == this.tokenSequence.token().id()) {
                            PositionEstimator.moveToDifferentThan(this.tokenSequence, PositionEstimator.Direction.FORWARD, EnumSet.of(JavaTokenId.WHITESPACE));
                            testPos = this.tokenSequence.offset();
                        }
                    }
                    oldT = oldIter.next();
                    ++i;
                    int copyTo = ++newIndex < newList.size() ? Math.max(testPos, this.endPos(oldT)) : this.endPos(oldT);
                    int n = lastOldPos;
                    lastOldPos = copyTo;
                    this.copyTo(n, lastOldPos);
                    firstNewItem = false;
                    break;
                }
            }
        }
        return lastOldPos;
    }

    private int printBreakContinueTree(int[] bounds, Name oldTLabel, Name newTlabel, JCTree.JCStatement oldT) {
        String stmt;
        int localPointer = bounds[0];
        String string = stmt = oldT.getKind() == Tree.Kind.BREAK ? "break" : "continue";
        if (this.nameChanged(oldTLabel, newTlabel)) {
            int labelPos = -1;
            int n = localPointer;
            localPointer = CasualDiff.getOldPos((JCTree)oldT);
            this.copyTo(n, localPointer);
            this.printer.print(stmt);
            localPointer += stmt.length();
            int commentStart = -1;
            int commentEnd = -1;
            if (oldTLabel != null && oldTLabel.length() > 0) {
                this.tokenSequence.move(localPointer);
                while (this.tokenSequence.moveNext()) {
                    Token tukac = this.tokenSequence.token();
                    if (this.isComment((JavaTokenId)tukac.id())) {
                        if (commentStart == -1) {
                            commentStart = this.tokenSequence.offset();
                        }
                        commentEnd = this.tokenSequence.offset() + tukac.length();
                        continue;
                    }
                    if (tukac.id() == JavaTokenId.WHITESPACE) continue;
                    break;
                }
                if (commentStart != -1) {
                    localPointer = this.copyUpTo(localPointer, commentEnd);
                }
                labelPos = this.tokenSequence.offset();
            }
            if (newTlabel != null && newTlabel.length() > 0) {
                if (oldTLabel != null) {
                    localPointer = this.copyUpTo(localPointer, labelPos);
                } else {
                    this.printer.print(" ");
                }
                this.printer.print(newTlabel);
            }
            if (oldTLabel != null) {
                localPointer = labelPos + oldTLabel.length();
            }
        }
        this.copyTo(localPointer, bounds[1]);
        return bounds[1];
    }

    private int toOff(int tokenIndex) {
        if (tokenIndex == -1) {
            return -1;
        }
        this.tokenSequence.moveIndex(tokenIndex);
        this.tokenSequence.moveNext();
        return this.tokenSequence.offset();
    }

    private int diffParameterList(java.util.List<? extends JCTree> oldList, java.util.List<? extends JCTree> newList, JavaTokenId[] makeAround, int pos, Comparator<JCTree> measure) {
        return this.diffParameterList(oldList, newList, null, makeAround, pos, measure);
    }

    private int diffParameterList(java.util.List<? extends JCTree> oldList, java.util.List<? extends JCTree> newList, JCTree parent, JavaTokenId[] makeAround, int pos, Comparator<JCTree> measure) {
        return this.diffParameterList(oldList, newList, parent, makeAround, pos, measure, this.diffContext.style.spaceBeforeComma(), this.diffContext.style.spaceAfterComma(), false, ",");
    }

    private int diffParameterList(java.util.List<? extends JCTree> oldList, java.util.List<? extends JCTree> newList, JavaTokenId[] makeAround, int pos, Comparator<JCTree> measure, boolean spaceBefore, boolean spaceAfter, boolean isEnum, String separator) {
        return this.diffParameterList(oldList, newList, null, makeAround, pos, measure, spaceBefore, spaceAfter, isEnum, separator);
    }

    private int diffParameterList(java.util.List<? extends JCTree> oldList, java.util.List<? extends JCTree> newList, JCTree parent, JavaTokenId[] makeAround, int pos, Comparator<JCTree> measure, boolean spaceBefore, boolean spaceAfter, boolean isEnum, String separator) {
        boolean printParens;
        assert (oldList != null && newList != null);
        if (oldList == newList || oldList.equals(newList)) {
            return pos;
        }
        boolean bl = printParens = makeAround != null && makeAround.length != 0;
        if (newList.isEmpty()) {
            int endPos = this.endPos(oldList);
            if (printParens) {
                this.tokenSequence.move(endPos);
                PositionEstimator.moveFwdToToken(this.tokenSequence, endPos, makeAround[1]);
                this.tokenSequence.moveNext();
                endPos = this.tokenSequence.offset();
                if (!PositionEstimator.nonRelevant.contains((Object)this.tokenSequence.token())) {
                    this.printer.print(" ");
                }
            }
            return endPos;
        }
        ListMatcher<JCTree> matcher = ListMatcher.instance(oldList, newList, measure);
        if (!matcher.match()) {
            return pos;
        }
        ListMatcher.ResultItem[] result = matcher.getResult();
        if (printParens) {
            this.printer.print(makeAround[0].fixedText());
        }
        int oldIndex = 0;
        boolean wasLeadingDelete = false;
        boolean wasComma = false;
        for (int j = 0; j < result.length; ++j) {
            ListMatcher.ResultItem<JCTree> item = result[j];
            switch (item.operation) {
                int[] bounds;
                int start;
                case MODIFY: {
                    JCTree tree = oldList.get(oldIndex++);
                    bounds = this.getCommentCorrectedBounds(tree);
                    this.tokenSequence.move(bounds[0]);
                    start = -1;
                    if (oldIndex != 1 && !separator.isEmpty()) {
                        if (wasLeadingDelete) {
                            start = Math.max(PositionEstimator.offsetToSrcWiteOnLine(this.tokenSequence, PositionEstimator.Direction.BACKWARD), pos);
                        } else {
                            PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.BACKWARD);
                        }
                    }
                    if (start == -1) {
                        this.tokenSequence.moveNext();
                        start = Math.max(this.tokenSequence.offset(), pos);
                    }
                    if (start < bounds[0]) {
                        this.copyTo(start, bounds[0], this.printer);
                    } else {
                        bounds[0] = Math.max(start, bounds[0]);
                    }
                    this.diffTree(tree, (JCTree)item.element, parent, bounds);
                    this.tokenSequence.move(bounds[1]);
                    PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.FORWARD);
                    if (!this.commaNeeded(result, item) && isEnum && this.tokenSequence.token().id() == JavaTokenId.RBRACKET) {
                        this.printer.print(";");
                    }
                    pos = Math.max(this.tokenSequence.offset(), bounds[1]);
                    this.copyTo(bounds[1], pos, this.printer);
                    wasLeadingDelete = false;
                    break;
                }
                case INSERT: {
                    if (wasComma && spaceAfter) {
                        this.printer.print(" ");
                    }
                    this.printer.suppressVariableType = this.suppressParameterTypes;
                    this.printer.print((JCTree)item.element);
                    this.printer.suppressVariableType = false;
                    wasLeadingDelete = false;
                    break;
                }
                case DELETE: {
                    wasLeadingDelete |= oldIndex++ == 0;
                    int endPos = this.getBounds((JCTree)item.element)[1];
                    this.tokenSequence.move(endPos);
                    PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.FORWARD);
                    if (this.tokenSequence.token().id() != JavaTokenId.COMMA || this.tokenSequence.moveNext()) {
                        // empty if block
                    }
                    pos = Math.max(this.tokenSequence.offset(), endPos);
                    break;
                }
                case NOCHANGE: {
                    if (oldIndex++ == 0 && wasComma && spaceAfter) {
                        this.printer.print(" ");
                    }
                    bounds = this.getCommentCorrectedBounds((JCTree)item.element);
                    this.tokenSequence.move(bounds[0]);
                    start = -1;
                    if (oldIndex != 1 && !separator.isEmpty()) {
                        if (wasLeadingDelete) {
                            start = PositionEstimator.offsetToSrcWiteOnLine(this.tokenSequence, PositionEstimator.Direction.BACKWARD);
                        } else {
                            PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.BACKWARD);
                        }
                    }
                    if (start == -1) {
                        this.tokenSequence.moveNext();
                        start = this.tokenSequence.offset();
                    }
                    this.tokenSequence.move(bounds[1]);
                    PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.FORWARD);
                    int end = isEnum ? (this.tokenSequence.token().id() == JavaTokenId.SEMICOLON || this.tokenSequence.token().id() == JavaTokenId.COMMA ? this.tokenSequence.offset() : bounds[1]) : (oldIndex < oldList.size() ? this.tokenSequence.offset() : bounds[1]);
                    pos = end;
                    this.copyTo(start, pos, this.printer);
                    wasLeadingDelete = false;
                    break;
                }
            }
            if (this.commaNeeded(result, item)) {
                if ((item.operation == ListMatcher.Operation.INSERT || oldIndex == oldList.size() && j + 1 < result.length && result[j + 1].operation == ListMatcher.Operation.INSERT) && spaceBefore) {
                    this.printer.print(" ");
                }
                this.printer.print(separator);
                wasComma = true;
                continue;
            }
            if (item.operation == ListMatcher.Operation.DELETE) continue;
            wasComma = false;
        }
        if (printParens) {
            this.printer.print(makeAround[1].fixedText());
        }
        if (oldList.isEmpty()) {
            return pos;
        }
        int endPos2 = this.endPos(oldList);
        this.tokenSequence.move(endPos2);
        PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.FORWARD);
        if (isEnum && (this.tokenSequence.token().id() == JavaTokenId.SEMICOLON || this.tokenSequence.token().id() == JavaTokenId.COMMA)) {
            return this.tokenSequence.offset();
        }
        return pos;
    }

    private int diffVarGroup(java.util.List<? extends JCTree> oldList, java.util.List<? extends JCTree> newList, JavaTokenId[] makeAround, int pos, Comparator<JCTree> measure) {
        boolean printParens;
        assert (oldList != null && newList != null);
        if (oldList == newList || oldList.equals(newList)) {
            return pos;
        }
        boolean bl = printParens = makeAround != null && makeAround.length != 0;
        if (newList.isEmpty()) {
            int endPos = this.endPos(oldList);
            if (printParens) {
                this.tokenSequence.move(endPos);
                PositionEstimator.moveFwdToToken(this.tokenSequence, endPos, makeAround[1]);
                this.tokenSequence.moveNext();
                endPos = this.tokenSequence.offset();
                if (!PositionEstimator.nonRelevant.contains((Object)this.tokenSequence.token())) {
                    this.printer.print(" ");
                }
            }
            return endPos;
        }
        ListMatcher<JCTree> matcher = ListMatcher.instance(oldList, newList, measure);
        if (!matcher.match()) {
            return pos;
        }
        ListMatcher.ResultItem[] result = matcher.getResult();
        if (printParens && oldList.isEmpty()) {
            this.printer.print(makeAround[0].fixedText());
        }
        int oldIndex = 0;
        boolean skipWhitespaces = false;
        for (int j = 0; j < result.length; ++j) {
            ListMatcher.ResultItem<JCTree> item = result[j];
            switch (item.operation) {
                int[] bounds;
                case MODIFY: {
                    int start;
                    JCTree tree = oldList.get(oldIndex++);
                    int[] bounds2 = this.getBounds(tree);
                    if (oldIndex != 1) {
                        bounds2[0] = tree.pos;
                    }
                    if (j == 0) {
                        int n = pos;
                        pos = bounds2[0];
                        this.copyTo(n, pos);
                    }
                    this.tokenSequence.move(bounds2[1]);
                    this.tokenSequence.movePrevious();
                    if (this.tokenSequence.token().id() == JavaTokenId.COMMA || this.tokenSequence.token().id() == JavaTokenId.SEMICOLON) {
                        bounds2[1] = this.tokenSequence.offset();
                    }
                    this.tokenSequence.move(bounds2[0]);
                    if (oldIndex != 1 && !skipWhitespaces) {
                        PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.BACKWARD);
                    }
                    this.tokenSequence.moveNext();
                    int n = start = this.tokenSequence.offset();
                    start = bounds2[0];
                    this.copyTo(n, start, this.printer);
                    if (j > 0) {
                        CommentSet old = this.comments.getComments((Tree)tree);
                        CommentSet cs = this.comments.getComments((Tree)item.element);
                        java.util.List<Comment> oldPrecedingComments = old.getComments(CommentSet.RelativePosition.PRECEDING);
                        java.util.List<Comment> newPrecedingComments = cs.getComments(CommentSet.RelativePosition.PRECEDING);
                        int indentReset = -1;
                        if (oldPrecedingComments.isEmpty() && !newPrecedingComments.isEmpty()) {
                            if (this.printer.out.isWhitespaceLine()) {
                                indentReset = this.printer.getIndent();
                                this.printer.setIndent(this.printer.out.getCol());
                            } else {
                                this.printer.newline();
                                this.printer.toLeftMargin();
                            }
                        }
                        start = this.diffPrecedingComments(tree, (JCTree)item.element, bounds2[0], start, false);
                        if (indentReset != -1) {
                            this.printer.setIndent(indentReset);
                        }
                    }
                    int localPointer = oldIndex != 1 ? this.diffVarDef((JCTree.JCVariableDecl)tree, (JCTree.JCVariableDecl)item.element, bounds2[0]) : this.diffVarDef((JCTree.JCVariableDecl)tree, (JCTree.JCVariableDecl)item.element, bounds2);
                    pos = bounds2[1];
                    this.copyTo(localPointer, pos, this.printer);
                    skipWhitespaces = false;
                    break;
                }
                case INSERT: {
                    JCTree.JCVariableDecl decl = (JCTree.JCVariableDecl)item.element;
                    if (j == 0) {
                        JCTree tree = oldList.get(oldIndex);
                        int[] bounds3 = this.getBounds(tree);
                        int n = pos;
                        pos = bounds3[0];
                        this.copyTo(n, pos);
                    }
                    if (oldIndex == 0) {
                        int oldPrec = this.printer.setPrec(0);
                        this.printer.visitVarDef(decl);
                        this.printer.setPrec(oldPrec);
                    } else {
                        if (this.diffContext.style.spaceAfterComma()) {
                            this.printer.print(" ");
                        }
                        this.printer.print(decl.name);
                        this.printer.printVarInit(decl);
                    }
                    skipWhitespaces = false;
                    break;
                }
                case NOCHANGE: {
                    ++oldIndex;
                    bounds = this.getBounds((JCTree)item.element);
                    if (j != 0) {
                        bounds[0] = ((JCTree)item.element).pos;
                    } else {
                        int n = pos;
                        pos = bounds[0];
                        this.copyTo(n, pos);
                    }
                    this.tokenSequence.move(bounds[0]);
                    if (j != 0 && !skipWhitespaces) {
                        PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.BACKWARD);
                    }
                    this.tokenSequence.moveNext();
                    int start = this.tokenSequence.offset();
                    int end = bounds[1];
                    this.tokenSequence.move(end);
                    this.tokenSequence.movePrevious();
                    if (this.tokenSequence.token().id() == JavaTokenId.COMMA || this.tokenSequence.token().id() == JavaTokenId.SEMICOLON) {
                        end = this.tokenSequence.offset();
                    }
                    pos = end;
                    this.copyTo(start, pos, this.printer);
                    skipWhitespaces = false;
                    break;
                }
                case DELETE: {
                    skipWhitespaces = false;
                    if (j == 0) {
                        JCTree.JCVariableDecl oldEl = (JCTree.JCVariableDecl)oldList.get(0);
                        JCTree.JCVariableDecl newEl = (JCTree.JCVariableDecl)newList.get(0);
                        int[] bounds4 = this.getBounds((JCTree)oldEl.getModifiers());
                        this.copyTo(pos, bounds4[0]);
                        pos = this.diffTree((JCTree)oldEl.getModifiers(), (JCTree)newEl.getModifiers(), bounds4);
                        bounds4 = this.getBounds(oldEl.getType());
                        int n = pos;
                        pos = bounds4[0];
                        this.copyTo(n, pos);
                        pos = this.diffTree(oldEl.getType(), newEl.getType(), bounds4);
                        this.copyTo(pos, ((JCTree)item.element).pos);
                        skipWhitespaces = true;
                    }
                    bounds = this.getBounds((JCTree)item.element);
                    this.tokenSequence.move(bounds[1]);
                    this.tokenSequence.movePrevious();
                    if (this.tokenSequence.token().id() == JavaTokenId.COMMA || this.tokenSequence.token().id() == JavaTokenId.SEMICOLON) {
                        bounds[1] = this.tokenSequence.offset();
                    }
                    pos = bounds[1];
                    break;
                }
            }
            if (!this.commaNeeded(result, item)) continue;
            this.printer.print(",");
        }
        if (printParens && oldList.isEmpty()) {
            this.printer.print(makeAround[1].fixedText());
        }
        return pos;
    }

    protected int diffUnionType(JCTree.JCTypeUnion oldT, JCTree.JCTypeUnion newT, int[] bounds) {
        int localPointer = bounds[0];
        return this.diffParameterList((java.util.List<? extends JCTree>)oldT.alternatives, (java.util.List<? extends JCTree>)newT.alternatives, null, localPointer, Measure.MEMBER, this.diffContext.style.spaceAroundBinaryOps(), this.diffContext.style.spaceAroundBinaryOps(), false, "|");
    }

    private boolean commaNeeded(ListMatcher.ResultItem[] arr, ListMatcher.ResultItem item) {
        if (item.operation == ListMatcher.Operation.DELETE) {
            return false;
        }
        boolean result = false;
        for (int i = 0; i < arr.length; ++i) {
            if (item == arr[i]) {
                result = true;
                continue;
            }
            if (!result || arr[i].operation == ListMatcher.Operation.DELETE) continue;
            return true;
        }
        return false;
    }

    private java.util.List<JCTree> filterHidden(java.util.List<? extends JCTree> list) {
        return CasualDiff.filterHidden(this.diffContext, list);
    }

    public static java.util.List<JCTree> filterHidden(DiffContext diffContext, java.util.List<? extends JCTree> list) {
        LinkedList<JCTree> result = new LinkedList<JCTree>();
        java.util.List fieldGroup = new ArrayList<JCTree.JCVariableDecl>();
        ArrayList<JCTree.JCVariableDecl> enumConstants = new ArrayList<JCTree.JCVariableDecl>();
        for (JCTree tree : list) {
            if (tree.pos == -1 || diffContext.syntheticTrees.contains((Object)tree)) continue;
            if (Tree.Kind.VARIABLE == tree.getKind()) {
                JCTree.JCVariableDecl var = (JCTree.JCVariableDecl)tree;
                if ((var.mods.flags & 16384) != 0) {
                    enumConstants.add(var);
                    continue;
                }
                if (!fieldGroup.isEmpty()) {
                    int oldPos = CasualDiff.getOldPos((JCTree)fieldGroup.get(0));
                    if (oldPos != -1 && oldPos != -2 && oldPos == CasualDiff.getOldPos((JCTree)var) && ((JCTree.JCVariableDecl)fieldGroup.get(0)).getModifiers() == var.getModifiers()) {
                        fieldGroup.add(var);
                        continue;
                    }
                    if (fieldGroup.size() > 1) {
                        result.add(new FieldGroupTree(fieldGroup));
                    } else {
                        result.add((JCTree)fieldGroup.get(0));
                    }
                    fieldGroup = new ArrayList();
                    fieldGroup.add(var);
                    continue;
                }
                fieldGroup.add((JCTree.JCVariableDecl)var);
                continue;
            }
            if (!fieldGroup.isEmpty()) {
                if (fieldGroup.size() > 1) {
                    result.add(new FieldGroupTree(fieldGroup));
                } else {
                    result.add((JCTree)fieldGroup.get(0));
                }
                fieldGroup = new ArrayList();
            }
            if (Tree.Kind.METHOD == tree.getKind()) {
                if (tree.pos == -1 || (((JCTree.JCMethodDecl)tree).mods.flags & 0x1000000000L) != 0) {
                    continue;
                }
            } else if (Tree.Kind.BLOCK == tree.getKind()) {
                JCTree.JCBlock block = (JCTree.JCBlock)tree;
                if (block.stats.isEmpty() && block.pos == -1 && block.flags == 0) continue;
            }
            result.add(tree);
        }
        if (!fieldGroup.isEmpty()) {
            if (fieldGroup.size() > 1) {
                result.add(new FieldGroupTree(fieldGroup));
            } else {
                result.add((JCTree)fieldGroup.get(0));
            }
        }
        if (!enumConstants.isEmpty()) {
            result.addFirst(new FieldGroupTree(enumConstants, !result.isEmpty()));
        }
        return result;
    }

    private int diffList(java.util.List<? extends JCTree> oldList, java.util.List<? extends JCTree> newList, int localPointer, PositionEstimator estimator, Comparator<JCTree> measure, VeryPretty printer) {
        if (oldList == newList || oldList.equals(newList)) {
            return localPointer;
        }
        assert (oldList != null && newList != null);
        ListMatcher<JCTree> matcher = ListMatcher.instance(oldList, newList, measure);
        if (!matcher.match()) {
            return localPointer;
        }
        LinkedList<JCTree> deletedItems = new LinkedList<JCTree>();
        JCTree lastdel = null;
        ListMatcher.ResultItem<JCTree>[] result = matcher.getResult();
        if (oldList.isEmpty() && !newList.isEmpty()) {
            StringBuilder aHead = new StringBuilder();
            StringBuilder aTail = new StringBuilder();
            int pos = estimator.prepare(localPointer, aHead, aTail);
            this.copyTo(localPointer, pos, printer);
            if (newList.get(0).getKind() == Tree.Kind.IMPORT) {
                printer.printImportsBlock(newList, true);
            } else {
                printer.print(aHead.toString());
                for (JCTree item : newList) {
                    if (LineInsertionType.BEFORE == estimator.lineInsertType()) {
                        printer.newline();
                    }
                    printer.print(item);
                    if (LineInsertionType.AFTER != estimator.lineInsertType()) continue;
                    printer.newline();
                }
                printer.print(aTail.toString());
            }
            return pos;
        }
        if (newList.isEmpty() && !oldList.isEmpty()) {
            int[] removalBounds = estimator.sectionRemovalBounds(null);
            this.copyTo(localPointer, removalBounds[0]);
            return removalBounds[1];
        }
        CodeStyle.ImportGroups importGroups = newList.get(0).getKind() == Tree.Kind.IMPORT && this.diffContext.style.separateImportGroups() ? this.diffContext.style.getImportGroups() : null;
        int lastGroup = -1;
        int i = 0;
        int insertPos = Math.min(this.getCommentCorrectedOldPos(oldList.get(i)), estimator.getInsertPos(0));
        int insertSaveLocalPointer = localPointer;
        if (insertPos < localPointer) {
            insertPos = -1;
        }
        block7 : for (int j = 0; j < result.length; ++j) {
            ListMatcher.ResultItem<JCTree> item = result[j];
            int group = -1;
            if (importGroups != null) {
                Name name = printer.fullName(((JCTree.JCImport)item.element).qualid);
                group = importGroups != null && name != null ? importGroups.getGroupId(name.toString(), ((JCTree.JCImport)item.element).staticImport) : -1;
            }
            switch (item.operation) {
                case MODIFY: {
                    lastGroup = group;
                    int[] bounds = estimator.getPositions(i);
                    bounds[0] = Math.min(bounds.length > 4 ? bounds[4] : bounds[0], this.getCommentCorrectedOldPos(oldList.get(i)));
                    this.copyTo(localPointer, bounds[0], printer);
                    localPointer = this.diffTree(oldList.get(i), (JCTree)item.element, bounds);
                    lastdel = null;
                    ++i;
                    insertPos = -1;
                    continue block7;
                }
                case INSERT: {
                    int pos;
                    boolean insetBlankLine = lastGroup >= 0 && lastGroup != group;
                    JCTree ld = null;
                    boolean match = false;
                    if (lastdel != null) {
                        boolean wasInFieldGroup = false;
                        if (lastdel instanceof FieldGroupTree) {
                            FieldGroupTree fieldGroupTree = (FieldGroupTree)lastdel;
                            for (JCTree.JCVariableDecl var : fieldGroupTree.getVariables()) {
                                if (!this.treesMatch((JCTree)item.element, (JCTree)var, false)) continue;
                                ld = lastdel;
                                wasInFieldGroup = true;
                                this.oldTrees.remove(item.element);
                                break;
                            }
                        }
                        match = wasInFieldGroup;
                        Iterator it = deletedItems.iterator();
                        while (!match && it.hasNext()) {
                            ld = (JCTree)it.next();
                            match = this.treesMatch((JCTree)item.element, ld, false);
                            if (!match) continue;
                            it.remove();
                        }
                    }
                    if (insertPos > -1 && i > 0) {
                        if (match) {
                            insertPos = Math.min(insertPos, i < oldList.size() ? estimator.getPositions(i)[0] : estimator.getPositions(i - 1)[2]);
                        }
                        if (insertPos > insertSaveLocalPointer) {
                            this.copyTo(insertSaveLocalPointer, insertPos);
                        }
                        localPointer = Math.max(localPointer, insertPos);
                    }
                    insertPos = -1;
                    lastGroup = group;
                    int n = importGroups != null ? (i == 0 || insetBlankLine && i < oldList.size() ? estimator.getPositions(i)[0] : estimator.getPositions(i - 1)[2]) : (pos = estimator.getInsertPos(i));
                    if (pos > localPointer) {
                        int n2 = localPointer;
                        localPointer = pos;
                        this.copyTo(n2, localPointer);
                    }
                    if (insetBlankLine) {
                        printer.blankline();
                    }
                    if (match) {
                        VeryPretty oldPrinter = this.printer;
                        int old = oldPrinter.indent();
                        this.printer = new VeryPretty(this.diffContext, this.diffContext.style, this.tree2Tag, this.tree2Doc, this.tag2Span, this.origText, oldPrinter.toString().length() + oldPrinter.getInitialOffset());
                        this.printer.reset(old, oldPrinter.out.getCol());
                        this.printer.oldTrees = this.oldTrees;
                        int index = oldList.indexOf((Object)ld);
                        int[] poss = estimator.getPositions(index);
                        int diffTo = this.diffTree(ld, (JCTree)item.element, poss);
                        this.copyTo(diffTo, poss[1]);
                        localPointer = Math.max(localPointer, poss[1]);
                        printer.print(this.printer.toString());
                        printer.reindentRegions.addAll(this.printer.reindentRegions);
                        this.printer = oldPrinter;
                        this.printer.undent(old);
                        if (!deletedItems.isEmpty()) continue block7;
                        lastdel = null;
                        continue block7;
                    }
                    if (LineInsertionType.BEFORE == estimator.lineInsertType()) {
                        printer.newline();
                    }
                    printer.print((JCTree)item.element);
                    if (LineInsertionType.AFTER != estimator.lineInsertType()) continue block7;
                    printer.newline();
                    continue block7;
                }
                case DELETE: {
                    int[] pos = estimator.getPositions(i);
                    if (localPointer < pos[0] && lastdel == null) {
                        this.copyTo(localPointer, pos[0], printer);
                        if (insertPos > -1) {
                            assert (localPointer == insertSaveLocalPointer);
                            insertSaveLocalPointer = pos[0];
                        }
                    }
                    if (lastdel == null) {
                        deletedItems.clear();
                    }
                    lastdel = oldList.get(i);
                    deletedItems.add(lastdel);
                    CommentSet ch = this.comments.getComments((Tree)oldList.get(i));
                    localPointer = Math.max(pos[1], Math.max(CasualDiff.commentEnd(ch, CommentSet.RelativePosition.INLINE), CasualDiff.commentEnd(ch, CommentSet.RelativePosition.TRAILING)));
                    ++i;
                    continue block7;
                }
                case NOCHANGE: {
                    boolean insetBlankLine = lastGroup >= 0 && lastGroup != group;
                    insertPos = -1;
                    lastGroup = group;
                    int[] pos = estimator.getPositions(i);
                    if (pos[0] > localPointer) {
                        this.copyTo(localPointer, pos[0], printer);
                    }
                    if (insetBlankLine) {
                        printer.blankline();
                    }
                    if (pos[0] >= localPointer) {
                        localPointer = pos[0];
                        if (pos.length > 3 && pos[3] != -1 && j + 1 < result.length) {
                            int n = localPointer;
                            localPointer = pos[3];
                            this.copyTo(n, localPointer, printer);
                            printer.print(estimator.append(i));
                        }
                    }
                    int n = localPointer;
                    localPointer = pos[1];
                    this.copyTo(n, localPointer, printer);
                    lastdel = null;
                    ++i;
                    break;
                }
            }
        }
        return localPointer;
    }

    private CommentSet getCommentsForTree(Tree t, boolean preceding) {
        if (t instanceof FieldGroupTree) {
            FieldGroupTree fgt = (FieldGroupTree)t;
            java.util.List<JCTree.JCVariableDecl> vars = fgt.getVariables();
            t = preceding ? vars.get(0) : vars.get(vars.size() - 1);
        }
        return this.comments.getComments(t);
    }

    protected int diffInnerComments(JCTree oldT, JCTree newT, int localPointer) {
        java.util.List<Comment> newPrecedingComments;
        CommentSet cs = this.getCommentsForTree((Tree)newT, true);
        CommentSet old = this.getCommentsForTree((Tree)oldT, true);
        java.util.List<Comment> oldPrecedingComments = old.getComments(CommentSet.RelativePosition.INNER);
        if (this.sameComments(oldPrecedingComments, newPrecedingComments = cs.getComments(CommentSet.RelativePosition.INNER))) {
            if (oldPrecedingComments.isEmpty()) {
                return localPointer;
            }
            int newP = oldPrecedingComments.get(oldPrecedingComments.size() - 1).endPos();
            this.copyTo(localPointer, newP);
            return newP;
        }
        return this.diffCommentLists(-1, oldPrecedingComments, newPrecedingComments, null, null, true, false, true, false, localPointer);
    }

    protected int diffPrecedingComments(JCTree oldT, JCTree newT, int oldTreeStartPos, int localPointer, boolean doNotDelete) {
        CommentSet cs = this.getCommentsForTree((Tree)newT, true);
        CommentSet old = this.getCommentsForTree((Tree)oldT, true);
        java.util.List<Comment> oldPrecedingComments = old.getComments(CommentSet.RelativePosition.PRECEDING);
        java.util.List<Comment> newPrecedingComments = cs.getComments(CommentSet.RelativePosition.PRECEDING);
        DocCommentTree newD = this.tree2Doc.get((Object)newT);
        if (this.sameComments(oldPrecedingComments, newPrecedingComments) && newD == null) {
            if (oldPrecedingComments.isEmpty()) {
                return localPointer;
            }
            int newP = oldPrecedingComments.get(oldPrecedingComments.size() - 1).endPos();
            if (newP > localPointer && newP < oldTreeStartPos) {
                this.copyTo(localPointer, newP);
                return newP;
            }
            return localPointer;
        }
        DCTree.DCDocComment oldD = this.oldTopLevel.docComments.getCommentTree(oldT);
        return this.diffCommentLists(oldTreeStartPos, oldPrecedingComments, newPrecedingComments, (DocCommentTree)oldD, newD, false, true, false, doNotDelete, localPointer);
    }

    protected int diffTrailingComments(JCTree oldT, JCTree newT, int localPointer, int elementEndWithComments) {
        CommentSet cs = this.getCommentsForTree((Tree)newT, false);
        CommentSet old = this.getCommentsForTree((Tree)oldT, false);
        java.util.List<Comment> oldInlineComments = old.getComments(CommentSet.RelativePosition.INLINE);
        java.util.List<Comment> newInlineComments = cs.getComments(CommentSet.RelativePosition.INLINE);
        java.util.List<Comment> oldTrailingComments = old.getComments(CommentSet.RelativePosition.TRAILING);
        java.util.List<Comment> newTrailingComments = cs.getComments(CommentSet.RelativePosition.TRAILING);
        if (this.sameComments(oldInlineComments, newInlineComments) && this.sameComments(oldTrailingComments, newTrailingComments)) {
            if (oldInlineComments.isEmpty() && oldTrailingComments.isEmpty()) {
                return localPointer;
            }
            int n = localPointer;
            localPointer = elementEndWithComments;
            this.copyTo(n, localPointer);
            return localPointer;
        }
        if (!this.sameComments(oldInlineComments, newInlineComments)) {
            while (this.printer.out.isWhitespaceLine()) {
                this.printer.eatChars(1);
            }
        }
        localPointer = this.diffCommentLists(CasualDiff.getOldPos(oldT), oldInlineComments, newInlineComments, null, null, false, false, false, false, localPointer);
        boolean containedEmbeddedNewLine = false;
        boolean containsEmbeddedNewLine = false;
        for (Comment oldComment : oldInlineComments) {
            if (oldComment.style() != Comment.Style.LINE) continue;
            containedEmbeddedNewLine = true;
        }
        for (Comment nueComment : newInlineComments) {
            if (nueComment.style() != Comment.Style.LINE) continue;
            containsEmbeddedNewLine = true;
        }
        if (containedEmbeddedNewLine && !containsEmbeddedNewLine) {
            this.printer.print("\n");
        }
        return this.diffCommentLists(CasualDiff.getOldPos(oldT), oldTrailingComments, newTrailingComments, null, null, true, false, false, false, localPointer);
    }

    private boolean sameComments(java.util.List<Comment> oldList, java.util.List<Comment> newList) {
        Iterator<Comment> oldIter = oldList.iterator();
        Iterator<Comment> newIter = newList.iterator();
        Comment oldC = this.safeNext(oldIter);
        Comment newC = this.safeNext(newIter);
        while (oldC != null && newC != null) {
            if (!this.commentsMatch(oldC, newC)) {
                return false;
            }
            oldC = this.safeNext(oldIter);
            newC = this.safeNext(newIter);
        }
        return !(oldC == null ^ newC == null);
    }

    private int diffCommentLists(int oldTreeStartPos, java.util.List<Comment> oldList, java.util.List<Comment> newList, DocCommentTree oldDoc, DocCommentTree newDoc, boolean trailing, boolean preceding, boolean inner, boolean doNotDeleteIfMissing, int localPointer) {
        int cStart;
        Comment javadoc = null;
        for (Comment comment : oldList) {
            if (comment.style() != Comment.Style.JAVADOC) continue;
            javadoc = comment;
        }
        Iterator<Comment> oldIter = oldList.iterator();
        Iterator<Comment> newIter = newList.iterator();
        Comment oldC = this.safeNext(oldIter);
        Comment newC = this.safeNext(newIter);
        boolean first = true;
        boolean firstNewCommentPrinted = false;
        while (oldC != null && newC != null) {
            cStart = this.commentStartCorrect(oldC);
            if (first && trailing && localPointer < cStart) {
                this.copyTo(localPointer, cStart);
            }
            first = false;
            int nextTarget = Math.max(localPointer, oldC.endPos());
            if (this.commentsMatch(oldC, newC)) {
                if (preceding && oldC == javadoc && oldDoc != null) {
                    localPointer = this.diffDocTree((DCTree.DCDocComment)oldDoc, (DCTree)oldDoc, (DCTree)newDoc, new int[]{localPointer, oldC.endPos()});
                }
                if (nextTarget > localPointer) {
                    this.copyTo(localPointer, nextTarget);
                }
                oldC = this.safeNext(oldIter);
                newC = this.safeNext(newIter);
                firstNewCommentPrinted = true;
            } else if (!this.listContains(newList, oldC)) {
                if (!this.listContains(oldList, newC)) {
                    int n = localPointer;
                    localPointer = oldC.pos();
                    this.copyTo(n, localPointer);
                    this.printer.printComment(newC, !trailing, false, true);
                    oldC = this.safeNext(oldIter);
                    newC = this.safeNext(newIter);
                } else {
                    oldC = this.safeNext(oldIter);
                }
            } else {
                if (!firstNewCommentPrinted && preceding) {
                    int n = localPointer;
                    localPointer = oldTreeStartPos;
                    this.copyTo(n, localPointer);
                }
                this.printer.print(newC.getText());
                newC = this.safeNext(newIter);
                firstNewCommentPrinted = true;
            }
            localPointer = nextTarget;
        }
        while (oldC != null) {
            cStart = this.commentStartCorrect(oldC);
            if (first && trailing && localPointer < cStart) {
                this.copyTo(localPointer, cStart);
            }
            if (oldC.style() == Comment.Style.WHITESPACE) {
                localPointer = cStart;
            } else if (first && doNotDeleteIfMissing) {
                localPointer = cStart;
            } else {
                first = false;
                localPointer = Math.max(localPointer, oldC.endPos());
            }
            oldC = this.safeNext(oldIter);
        }
        while (newC != null) {
            if (Comment.Style.WHITESPACE != newC.style()) {
                if (!firstNewCommentPrinted && preceding) {
                    int n = localPointer;
                    localPointer = oldTreeStartPos;
                    this.copyTo(n, localPointer);
                }
                this.printer.printComment(newC, !trailing, false, !preceding && !trailing);
                firstNewCommentPrinted = true;
            }
            newC = this.safeNext(newIter);
        }
        if (preceding && javadoc == null && newDoc != null) {
            if (!firstNewCommentPrinted && preceding) {
                int n = localPointer;
                localPointer = oldTreeStartPos;
                this.copyTo(n, localPointer);
            }
            this.printer.print((DCTree)newDoc);
        }
        return localPointer;
    }

    private int diffDocTree(DCTree.DCDocComment doc, DCTree oldT, DCTree newT, int[] elementBounds) {
        if (oldT == null && newT != null) {
            throw new IllegalArgumentException("Null is not allowed in parameters.");
        }
        if (oldT == newT) {
            return elementBounds[0];
        }
        if (newT == null) {
            this.tokenSequence.move(elementBounds[1]);
            if (!this.tokenSequence.moveNext()) {
                return elementBounds[1];
            }
            while (this.tokenSequence.token().id() == JavaTokenId.WHITESPACE && this.tokenSequence.moveNext()) {
            }
            return this.tokenSequence.offset();
        }
        int localpointer = elementBounds[0];
        if (oldT.getKind() != newT.getKind()) {
            int[] oldBounds = this.getBounds(oldT, doc);
            if (oldBounds[0] > elementBounds[0]) {
                this.copyTo(elementBounds[0], oldBounds[0]);
            }
            this.printer.print(newT);
            return oldBounds[1];
        }
        switch (oldT.getKind()) {
            case ATTRIBUTE: {
                localpointer = this.diffAttribute(doc, (DCTree.DCAttribute)oldT, (DCTree.DCAttribute)newT, elementBounds);
                break;
            }
            case DOC_COMMENT: {
                localpointer = this.diffDocComment(doc, (DCTree.DCDocComment)oldT, (DCTree.DCDocComment)newT, elementBounds);
                break;
            }
            case PARAM: {
                localpointer = this.diffParam(doc, (DCTree.DCParam)oldT, (DCTree.DCParam)newT, elementBounds);
                break;
            }
            case RETURN: {
                localpointer = this.diffReturn(doc, (DCTree.DCReturn)oldT, (DCTree.DCReturn)newT, elementBounds);
                break;
            }
            case IDENTIFIER: {
                localpointer = this.diffIdentifier(doc, (DCTree.DCIdentifier)oldT, (DCTree.DCIdentifier)newT, elementBounds);
                break;
            }
            case SEE: {
                localpointer = this.diffSee(doc, (DCTree.DCSee)oldT, (DCTree.DCSee)newT, elementBounds);
                break;
            }
            case LINK_PLAIN: 
            case LINK: {
                localpointer = this.diffLink(doc, (DCTree.DCLink)oldT, (DCTree.DCLink)newT, elementBounds);
                break;
            }
            case TEXT: {
                localpointer = this.diffText(doc, (DCTree.DCText)oldT, (DCTree.DCText)newT, elementBounds);
                break;
            }
            case AUTHOR: {
                localpointer = this.diffAuthor(doc, (DCTree.DCAuthor)oldT, (DCTree.DCAuthor)newT, elementBounds);
                break;
            }
            case COMMENT: {
                localpointer = this.diffComment(doc, (DCTree.DCComment)oldT, (DCTree.DCComment)newT, elementBounds);
                break;
            }
            case DEPRECATED: {
                localpointer = this.diffDeprecated(doc, (DCTree.DCDeprecated)oldT, (DCTree.DCDeprecated)newT, elementBounds);
                break;
            }
            case DOC_ROOT: {
                localpointer = this.diffDocRoot(doc, (DCTree.DCDocRoot)oldT, (DCTree.DCDocRoot)newT, elementBounds);
                break;
            }
            case ENTITY: {
                localpointer = this.diffEntity(doc, (DCTree.DCEntity)oldT, (DCTree.DCEntity)newT, elementBounds);
                break;
            }
            case ERRONEOUS: {
                localpointer = this.diffErroneous(doc, (DCTree.DCErroneous)oldT, (DCTree.DCErroneous)newT, elementBounds);
                break;
            }
            case EXCEPTION: 
            case THROWS: {
                localpointer = this.diffThrows(doc, (DCTree.DCThrows)oldT, (DCTree.DCThrows)newT, elementBounds);
                break;
            }
            case INHERIT_DOC: {
                localpointer = this.diffInheritDoc(doc, (DCTree.DCInheritDoc)oldT, (DCTree.DCInheritDoc)newT, elementBounds);
                break;
            }
            case CODE: 
            case LITERAL: {
                localpointer = this.diffLiteral(doc, (DCTree.DCLiteral)oldT, (DCTree.DCLiteral)newT, elementBounds);
                break;
            }
            case REFERENCE: {
                localpointer = this.diffReference(doc, (DCTree.DCReference)oldT, (DCTree.DCReference)newT, elementBounds);
                break;
            }
            case SERIAL: {
                localpointer = this.diffSerial(doc, (DCTree.DCSerial)oldT, (DCTree.DCSerial)newT, elementBounds);
                break;
            }
            case SERIAL_DATA: {
                localpointer = this.diffSerialData(doc, (DCTree.DCSerialData)oldT, (DCTree.DCSerialData)newT, elementBounds);
                break;
            }
            case SERIAL_FIELD: {
                localpointer = this.diffSerialField(doc, (DCTree.DCSerialField)oldT, (DCTree.DCSerialField)newT, elementBounds);
                break;
            }
            case SINCE: {
                localpointer = this.diffSince(doc, (DCTree.DCSince)oldT, (DCTree.DCSince)newT, elementBounds);
                break;
            }
            case START_ELEMENT: {
                localpointer = this.diffStartElement(doc, (DCTree.DCStartElement)oldT, (DCTree.DCStartElement)newT, elementBounds);
                break;
            }
            case END_ELEMENT: {
                localpointer = this.diffEndElement(doc, (DCTree.DCEndElement)oldT, (DCTree.DCEndElement)newT, elementBounds);
                break;
            }
            case UNKNOWN_BLOCK_TAG: {
                localpointer = this.diffUnknownBlockTag(doc, (DCTree.DCUnknownBlockTag)oldT, (DCTree.DCUnknownBlockTag)newT, elementBounds);
                break;
            }
            case UNKNOWN_INLINE_TAG: {
                localpointer = this.diffUnknownInlineTag(doc, (DCTree.DCUnknownInlineTag)oldT, (DCTree.DCUnknownInlineTag)newT, elementBounds);
                break;
            }
            case VALUE: {
                localpointer = this.diffValue(doc, (DCTree.DCValue)oldT, (DCTree.DCValue)newT, elementBounds);
                break;
            }
            case VERSION: {
                localpointer = this.diffVersion(doc, (DCTree.DCVersion)oldT, (DCTree.DCVersion)newT, elementBounds);
                break;
            }
            default: {
                if (oldT.getKind() == DocTree.Kind.OTHER) break;
                String msg = "Diff not implemented: " + oldT.getKind().toString() + " " + oldT.getClass().getName();
                throw new AssertionError((Object)msg);
            }
        }
        return localpointer;
    }

    private int diffAttribute(DCTree.DCDocComment doc, DCTree.DCAttribute oldT, DCTree.DCAttribute newT, int[] elementBounds) {
        return elementBounds[1];
    }

    private int diffDocComment(DCTree.DCDocComment doc, DCTree.DCDocComment oldT, DCTree.DCDocComment newT, int[] elementBounds) {
        int commentEnd;
        this.tokenSequence.move(elementBounds[0]);
        if (!this.tokenSequence.moveNext()) {
            return elementBounds[1];
        }
        while (this.tokenSequence.token().id() == JavaTokenId.WHITESPACE && this.tokenSequence.moveNext()) {
        }
        int localpointer = this.tokenSequence.offset() + 3;
        this.copyTo(elementBounds[0], localpointer);
        if (oldT.firstSentence.isEmpty() && newT.firstSentence.nonEmpty()) {
            this.printer.newline();
            this.printer.toLeftMargin();
            this.printer.print(" * ");
        }
        localpointer = this.diffList(doc, (java.util.List<? extends DCTree>)oldT.firstSentence, (java.util.List<? extends DCTree>)newT.firstSentence, localpointer, Measure.TAGS);
        localpointer = this.diffList(doc, (java.util.List<? extends DCTree>)oldT.body, (java.util.List<? extends DCTree>)newT.body, localpointer, Measure.TAGS);
        if (oldT.tags.isEmpty() && localpointer < (commentEnd = CasualDiff.commentEnd(doc))) {
            int n = localpointer;
            localpointer = commentEnd;
            this.copyTo(n, localpointer);
        }
        if ((localpointer = this.diffList(doc, (java.util.List<? extends DCTree>)oldT.tags, (java.util.List<? extends DCTree>)newT.tags, localpointer, Measure.TAGS)) < elementBounds[1]) {
            this.copyTo(localpointer, elementBounds[1]);
        }
        return elementBounds[1];
    }

    private int diffParam(DCTree.DCDocComment doc, DCTree.DCParam oldT, DCTree.DCParam newT, int[] elementBounds) {
        int localpointer;
        if (oldT.isTypeParameter != newT.isTypeParameter) {
            if (oldT.isTypeParameter) {
                localpointer = this.getOldPos((DCTree)oldT.name, doc);
                this.copyTo(elementBounds[0], localpointer - 1);
            } else {
                localpointer = this.getOldPos((DCTree)oldT.name, doc);
                this.copyTo(elementBounds[0], localpointer);
                this.printer.print("<");
            }
        } else {
            localpointer = this.getOldPos((DCTree)oldT.name, doc);
            this.copyTo(elementBounds[0], localpointer);
        }
        int nameEnd = this.endPos((DCTree)oldT.name, doc);
        localpointer = this.diffDocTree(doc, (DCTree)oldT.name, (DCTree)newT.name, new int[]{localpointer, nameEnd});
        if (localpointer < nameEnd) {
            int n = localpointer;
            localpointer = nameEnd;
            this.copyTo(n, localpointer);
        }
        if (oldT.isTypeParameter) {
            ++localpointer;
        }
        if (newT.isTypeParameter) {
            this.printer.print(">");
        }
        if ((localpointer = this.diffList(doc, (java.util.List<? extends DCTree>)oldT.description, (java.util.List<? extends DCTree>)newT.description, localpointer, Measure.TAGS)) < elementBounds[1]) {
            this.copyTo(localpointer, elementBounds[1]);
        }
        return elementBounds[1];
    }

    private int diffReturn(DCTree.DCDocComment doc, DCTree.DCReturn oldT, DCTree.DCReturn newT, int[] elementBounds) {
        int localpointer = oldT.description.isEmpty() ? elementBounds[1] : this.getOldPos((DCTree)oldT.description.head, doc);
        this.copyTo(elementBounds[0], localpointer);
        localpointer = this.diffList(doc, (java.util.List<? extends DCTree>)oldT.description, (java.util.List<? extends DCTree>)newT.description, localpointer, Measure.TAGS);
        if (localpointer < elementBounds[1]) {
            this.copyTo(localpointer, elementBounds[1]);
        }
        return elementBounds[1];
    }

    private int diffIdentifier(DCTree.DCDocComment doc, DCTree.DCIdentifier oldT, DCTree.DCIdentifier newT, int[] elementBounds) {
        if (oldT.name.equals((Object)newT.name)) {
            this.copyTo(elementBounds[0], elementBounds[1]);
        } else {
            this.printer.print(newT.name);
        }
        return elementBounds[1];
    }

    private int diffLink(DCTree.DCDocComment doc, DCTree.DCLink oldT, DCTree.DCLink newT, int[] elementBounds) {
        int localpointer = this.getOldPos((DCTree)oldT.ref, doc);
        this.copyTo(elementBounds[0], localpointer);
        localpointer = this.diffDocTree(doc, (DCTree)oldT.ref, (DCTree)newT.ref, new int[]{localpointer, this.endPos((DCTree)oldT.ref, doc)});
        localpointer = this.diffList(doc, (java.util.List<? extends DCTree>)oldT.label, (java.util.List<? extends DCTree>)newT.label, localpointer, Measure.TAGS);
        if (localpointer < elementBounds[1]) {
            this.copyTo(localpointer, elementBounds[1]);
        }
        return elementBounds[1];
    }

    private int diffSee(DCTree.DCDocComment doc, DCTree.DCSee oldT, DCTree.DCSee newT, int[] elementBounds) {
        int localpointer = this.getOldPos((DCTree)oldT.reference.head, doc);
        this.copyTo(elementBounds[0], localpointer);
        localpointer = this.diffList(doc, (java.util.List<? extends DCTree>)oldT.reference, (java.util.List<? extends DCTree>)newT.reference, localpointer, Measure.DOCTREE);
        if (localpointer < elementBounds[1]) {
            this.copyTo(localpointer, elementBounds[1]);
        }
        return elementBounds[1];
    }

    private int diffText(DCTree.DCDocComment doc, DCTree.DCText oldT, DCTree.DCText newT, int[] elementBounds) {
        if (oldT.text.equals(newT.text)) {
            this.copyTo(elementBounds[0], elementBounds[1]);
        } else {
            this.printer.print(newT.text);
        }
        return elementBounds[1];
    }

    private int diffAuthor(DCTree.DCDocComment doc, DCTree.DCAuthor oldT, DCTree.DCAuthor newT, int[] elementBounds) {
        int localpointer = oldT.name.isEmpty() ? elementBounds[1] : this.getOldPos((DCTree)oldT.name.head, doc);
        this.copyTo(elementBounds[0], localpointer);
        localpointer = this.diffList(doc, (java.util.List<? extends DCTree>)oldT.name, (java.util.List<? extends DCTree>)newT.name, localpointer, Measure.DOCTREE);
        if (localpointer < elementBounds[1]) {
            this.copyTo(localpointer, elementBounds[1]);
        }
        return elementBounds[1];
    }

    private int diffComment(DCTree.DCDocComment doc, DCTree.DCComment oldT, DCTree.DCComment newT, int[] elementBounds) {
        if (oldT.body.equals(newT.body)) {
            this.copyTo(elementBounds[0], elementBounds[1]);
        } else {
            this.printer.print(newT.body);
        }
        return elementBounds[1];
    }

    private int diffDeprecated(DCTree.DCDocComment doc, DCTree.DCDeprecated oldT, DCTree.DCDeprecated newT, int[] elementBounds) {
        int localpointer = oldT.body.isEmpty() ? elementBounds[1] : this.getOldPos((DCTree)oldT.body.head, doc);
        this.copyTo(elementBounds[0], localpointer);
        localpointer = this.diffList(doc, (java.util.List<? extends DCTree>)oldT.body, (java.util.List<? extends DCTree>)newT.body, localpointer, Measure.DOCTREE);
        if (localpointer < elementBounds[1]) {
            this.copyTo(localpointer, elementBounds[1]);
        }
        return elementBounds[1];
    }

    private int diffDocRoot(DCTree.DCDocComment doc, DCTree.DCDocRoot oldT, DCTree.DCDocRoot newT, int[] elementBounds) {
        this.copyTo(elementBounds[0], elementBounds[1]);
        return elementBounds[1];
    }

    private int diffEntity(DCTree.DCDocComment doc, DCTree.DCEntity oldT, DCTree.DCEntity newT, int[] elementBounds) {
        if (oldT.name.equals((Object)newT.name)) {
            this.copyTo(elementBounds[0], elementBounds[1]);
        } else {
            this.printer.print((DCTree)newT);
        }
        return elementBounds[1];
    }

    private int diffErroneous(DCTree.DCDocComment doc, DCTree.DCErroneous oldT, DCTree.DCErroneous newT, int[] elementBounds) {
        if (oldT.body.equals(newT.body)) {
            this.copyTo(elementBounds[0], elementBounds[1]);
        } else {
            this.printer.print(newT.body);
        }
        return elementBounds[1];
    }

    private int diffThrows(DCTree.DCDocComment doc, DCTree.DCThrows oldT, DCTree.DCThrows newT, int[] elementBounds) {
        int localpointer = this.getOldPos((DCTree)oldT.name, doc);
        this.copyTo(elementBounds[0], localpointer);
        int endPos = this.endPos((DCTree)oldT.name, doc);
        localpointer = this.diffDocTree(doc, (DCTree)oldT.name, (DCTree)newT.name, new int[]{localpointer, endPos});
        if (localpointer < endPos) {
            int n = localpointer;
            localpointer = endPos;
            this.copyTo(n, localpointer);
        }
        if ((localpointer = this.diffList(doc, (java.util.List<? extends DCTree>)oldT.description, (java.util.List<? extends DCTree>)newT.description, localpointer, Measure.TAGS)) < elementBounds[1]) {
            this.copyTo(localpointer, elementBounds[1]);
        }
        return elementBounds[1];
    }

    private int diffInheritDoc(DCTree.DCDocComment doc, DCTree.DCInheritDoc oldT, DCTree.DCInheritDoc newT, int[] elementBounds) {
        this.copyTo(elementBounds[0], elementBounds[1]);
        return elementBounds[1];
    }

    private int diffLiteral(DCTree.DCDocComment doc, DCTree.DCLiteral oldT, DCTree.DCLiteral newT, int[] elementBounds) {
        int localpointer = this.getOldPos((DCTree)oldT.body, doc);
        this.copyTo(elementBounds[0], localpointer);
        localpointer = this.diffDocTree(doc, (DCTree)oldT.body, (DCTree)newT.body, new int[]{localpointer, this.endPos((DCTree)oldT.body, doc)});
        if (localpointer < elementBounds[1]) {
            this.copyTo(localpointer, elementBounds[1]);
        }
        return elementBounds[1];
    }

    private int diffReference(DCTree.DCDocComment doc, DCTree.DCReference oldT, DCTree.DCReference newT, int[] elementBounds) {
        this.printer.print((DCTree)newT);
        return elementBounds[1];
    }

    private int diffSerial(DCTree.DCDocComment doc, DCTree.DCSerial oldT, DCTree.DCSerial newT, int[] elementBounds) {
        int localpointer = this.getOldPos((DCTree)oldT.description.head, doc);
        this.copyTo(elementBounds[0], localpointer);
        localpointer = this.diffList(doc, (java.util.List<? extends DCTree>)oldT.description, (java.util.List<? extends DCTree>)newT.description, localpointer, Measure.TAGS);
        if (localpointer < elementBounds[1]) {
            this.copyTo(localpointer, elementBounds[1]);
        }
        return elementBounds[1];
    }

    private int diffSerialData(DCTree.DCDocComment doc, DCTree.DCSerialData oldT, DCTree.DCSerialData newT, int[] elementBounds) {
        int localpointer = this.getOldPos((DCTree)oldT.description.head, doc);
        this.copyTo(elementBounds[0], localpointer);
        localpointer = this.diffList(doc, (java.util.List<? extends DCTree>)oldT.description, (java.util.List<? extends DCTree>)newT.description, localpointer, Measure.TAGS);
        if (localpointer < elementBounds[1]) {
            this.copyTo(localpointer, elementBounds[1]);
        }
        return elementBounds[1];
    }

    private int diffSerialField(DCTree.DCDocComment doc, DCTree.DCSerialField oldT, DCTree.DCSerialField newT, int[] elementBounds) {
        int localpointer = this.getOldPos((DCTree)oldT.name, doc);
        this.copyTo(elementBounds[0], localpointer);
        localpointer = this.diffDocTree(doc, (DCTree)oldT.name, (DCTree)newT.name, new int[]{localpointer, this.endPos((DCTree)oldT.name, doc)});
        localpointer = this.diffDocTree(doc, (DCTree)oldT.type, (DCTree)newT.type, new int[]{localpointer, this.endPos((DCTree)oldT.type, doc)});
        localpointer = this.diffList(doc, (java.util.List<? extends DCTree>)oldT.description, (java.util.List<? extends DCTree>)newT.description, localpointer, Measure.TAGS);
        if (localpointer < elementBounds[1]) {
            this.copyTo(localpointer, elementBounds[1]);
        }
        return elementBounds[1];
    }

    private int diffSince(DCTree.DCDocComment doc, DCTree.DCSince oldT, DCTree.DCSince newT, int[] elementBounds) {
        int localpointer = oldT.body.isEmpty() ? elementBounds[1] : this.getOldPos((DCTree)oldT.body.head, doc);
        this.copyTo(elementBounds[0], localpointer);
        localpointer = this.diffList(doc, (java.util.List<? extends DCTree>)oldT.body, (java.util.List<? extends DCTree>)newT.body, localpointer, Measure.DOCTREE);
        if (localpointer < elementBounds[1]) {
            this.copyTo(localpointer, elementBounds[1]);
        }
        return elementBounds[1];
    }

    private int diffStartElement(DCTree.DCDocComment doc, DCTree.DCStartElement oldT, DCTree.DCStartElement newT, int[] elementBounds) {
        int localpointer;
        int n = localpointer = oldT.attrs.isEmpty() ? elementBounds[1] - 1 : this.getOldPos((DCTree)oldT.attrs.head, doc);
        if (oldT.name.equals((Object)newT.name)) {
            this.copyTo(elementBounds[0], localpointer);
        } else {
            this.printer.print("<");
            this.printer.print(newT.name);
        }
        localpointer = this.diffList(doc, (java.util.List<? extends DCTree>)oldT.attrs, (java.util.List<? extends DCTree>)newT.attrs, localpointer, Measure.DOCTREE);
        if (localpointer < elementBounds[1]) {
            this.copyTo(localpointer, elementBounds[1]);
        }
        return elementBounds[1];
    }

    private int diffEndElement(DCTree.DCDocComment doc, DCTree.DCEndElement oldT, DCTree.DCEndElement newT, int[] elementBounds) {
        if (oldT.name.equals((Object)newT.name)) {
            this.copyTo(elementBounds[0], elementBounds[1]);
        } else {
            this.printer.print((DCTree)newT);
        }
        return elementBounds[1];
    }

    private int diffUnknownBlockTag(DCTree.DCDocComment doc, DCTree.DCUnknownBlockTag oldT, DCTree.DCUnknownBlockTag newT, int[] elementBounds) {
        int localpointer;
        int n = localpointer = oldT.content.isEmpty() ? elementBounds[1] : this.getOldPos((DCTree)oldT.content.head, doc);
        if (oldT.name.equals((Object)newT.name)) {
            this.copyTo(elementBounds[0], localpointer);
        } else {
            this.printer.print("@");
            this.printer.print(newT.name);
            this.printer.out.needSpace();
        }
        localpointer = this.diffList(doc, (java.util.List<? extends DCTree>)oldT.content, (java.util.List<? extends DCTree>)newT.content, localpointer, Measure.DOCTREE);
        if (localpointer < elementBounds[1]) {
            this.copyTo(localpointer, elementBounds[1]);
        }
        return elementBounds[1];
    }

    private int diffUnknownInlineTag(DCTree.DCDocComment doc, DCTree.DCUnknownInlineTag oldT, DCTree.DCUnknownInlineTag newT, int[] elementBounds) {
        int localpointer;
        int n = localpointer = oldT.content.isEmpty() ? elementBounds[1] : this.getOldPos((DCTree)oldT.content.head, doc);
        if (oldT.name.equals((Object)newT.name)) {
            this.copyTo(elementBounds[0], localpointer);
        } else {
            this.printer.print("{@");
            this.printer.print(newT.name);
            this.printer.out.needSpace();
        }
        localpointer = this.diffList(doc, (java.util.List<? extends DCTree>)oldT.content, (java.util.List<? extends DCTree>)newT.content, localpointer, Measure.DOCTREE);
        if (localpointer < elementBounds[1]) {
            this.copyTo(localpointer, elementBounds[1]);
        }
        return elementBounds[1];
    }

    private int diffValue(DCTree.DCDocComment doc, DCTree.DCValue oldT, DCTree.DCValue newT, int[] elementBounds) {
        int localpointer = this.getOldPos((DCTree)oldT.ref, doc);
        this.copyTo(elementBounds[0], localpointer);
        localpointer = this.diffDocTree(doc, (DCTree)oldT.ref, (DCTree)newT.ref, new int[]{localpointer, this.endPos((DCTree)oldT.ref, doc)});
        if (localpointer < elementBounds[1]) {
            this.copyTo(localpointer, elementBounds[1]);
        }
        return elementBounds[1];
    }

    private int diffVersion(DCTree.DCDocComment doc, DCTree.DCVersion oldT, DCTree.DCVersion newT, int[] elementBounds) {
        int localpointer = oldT.body.head != null ? this.getOldPos((DCTree)oldT.body.head, doc) : elementBounds[1];
        this.copyTo(elementBounds[0], localpointer);
        localpointer = this.diffList(doc, (java.util.List<? extends DCTree>)oldT.body, (java.util.List<? extends DCTree>)newT.body, localpointer, Measure.DOCTREE);
        if (localpointer < elementBounds[1]) {
            this.copyTo(localpointer, elementBounds[1]);
        }
        return elementBounds[1];
    }

    private int diffList(DCTree.DCDocComment doc, java.util.List<? extends DCTree> oldList, java.util.List<? extends DCTree> newList, int localPointer, Comparator<DCTree> measure) {
        assert (oldList != null && newList != null);
        if (oldList == newList || oldList.equals(newList)) {
            return localPointer;
        }
        ListMatcher<DCTree> matcher = ListMatcher.instance(oldList, newList, measure);
        if (!matcher.match()) {
            return localPointer;
        }
        DCTree lastdel = null;
        ListMatcher.ResultItem<DCTree>[] result = matcher.getResult();
        if (oldList.isEmpty() && !newList.isEmpty()) {
            StringBuilder aHead = new StringBuilder();
            StringBuilder aTail = new StringBuilder();
            this.printer.out.needSpace();
            for (DCTree item : newList) {
                this.printer.print(item);
            }
            return localPointer;
        }
        if (newList.isEmpty() && !oldList.isEmpty()) {
            int oldPos = this.adjustToPreviousNewLine(this.getOldPos(oldList.get(0), doc), localPointer);
            this.copyTo(localPointer, oldPos);
            return this.endPos(oldList, doc);
        }
        int insertPos = this.adjustToPreviousNewLine(this.getOldPos(oldList.get(0), doc), localPointer);
        if (insertPos > localPointer) {
            int n = localPointer;
            localPointer = insertPos;
            this.copyTo(n, localPointer);
        }
        int i = 0;
        block7 : for (int j = 0; j < result.length; ++j) {
            ListMatcher.ResultItem<DCTree> item = result[j];
            switch (item.operation) {
                int[] pos;
                DCTree oldT;
                case MODIFY: {
                    oldT = oldList.get(i);
                    pos = this.getBounds(oldT, doc);
                    this.copyTo(localPointer, pos[0]);
                    localPointer = this.diffDocTree(doc, oldT, (DCTree)item.element, pos);
                    ++i;
                    continue block7;
                }
                case INSERT: {
                    int oldPos = ((DCTree)item.element).pos;
                    boolean found = false;
                    if (oldPos > 0) {
                        for (DCTree oldT2 : oldList) {
                            int oldNodePos = oldT2.pos;
                            if (oldPos != oldNodePos) continue;
                            found = true;
                            VeryPretty oldPrinter = this.printer;
                            int old = oldPrinter.indent();
                            this.printer = new VeryPretty(this.diffContext, this.diffContext.style, this.tree2Tag, this.tree2Doc, this.tag2Span, this.origText, oldPrinter.toString().length() + oldPrinter.getInitialOffset());
                            this.printer.reset(old, oldPrinter.out.getCol());
                            this.printer.oldTrees = this.oldTrees;
                            int[] poss = this.getBounds(oldT2, doc);
                            int end = this.diffDocTree(doc, oldT2, (DCTree)item.element, poss);
                            this.copyTo(end, poss[1]);
                            this.printer.print(this.printer.toString());
                            this.printer.reindentRegions.addAll(this.printer.reindentRegions);
                            this.printer = oldPrinter;
                            this.printer.undent(old);
                            break;
                        }
                    }
                    if (found) continue block7;
                    this.printer.print((DCTree)item.element);
                    continue block7;
                }
                case DELETE: {
                    lastdel = oldT = oldList.get(i);
                    pos = this.getBounds(oldT, doc);
                    localPointer = pos[1];
                    ++i;
                    continue block7;
                }
                case NOCHANGE: {
                    oldT = oldList.get(i);
                    pos = this.getBounds(oldT, doc);
                    if (this.needStar(pos[0])) {
                        this.printer.print(" * ");
                    }
                    int n = localPointer;
                    localPointer = pos[1];
                    this.copyTo(n, localPointer, this.printer);
                    lastdel = null;
                    ++i;
                    break;
                }
            }
        }
        return localPointer;
    }

    private Comment safeNext(Iterator<Comment> iter) {
        return iter.hasNext() ? iter.next() : null;
    }

    private boolean commentsMatch(Comment oldC, Comment newC) {
        if (oldC == null && newC == null) {
            return true;
        }
        if (oldC == null || newC == null) {
            return false;
        }
        return oldC.equals(newC);
    }

    private boolean listContains(java.util.List<Comment> list, Comment comment) {
        for (Comment c : list) {
            if (!c.equals(comment)) continue;
            return true;
        }
        return false;
    }

    private int commentStartCorrect(Comment c) {
        this.tokenSequence.move(c.pos());
        boolean wasPrevious = false;
        while (this.tokenSequence.movePrevious()) {
            if (this.tokenSequence.token().id() != JavaTokenId.WHITESPACE) {
                return this.tokenSequence.offset() + this.tokenSequence.token().length();
            }
            int lastNewLine = this.tokenSequence.token().text().toString().lastIndexOf(10);
            if (lastNewLine != -1) {
                return this.tokenSequence.offset() + lastNewLine + 1;
            }
            wasPrevious = true;
        }
        if (wasPrevious) {
            return this.tokenSequence.offset();
        }
        return c.pos();
    }

    public static int commentStart(DiffContext diffContext, CommentSet comments, CommentSet.RelativePosition pos, int limit) {
        java.util.List<Comment> list = comments.getComments(pos);
        if (list.isEmpty()) {
            return Integer.MAX_VALUE;
        }
        diffContext.tokenSequence.move(limit);
        PositionEstimator.moveToSrcRelevant(diffContext.tokenSequence, PositionEstimator.Direction.BACKWARD);
        limit = diffContext.tokenSequence.offset() + diffContext.tokenSequence.token().length();
        int start = Integer.MAX_VALUE;
        for (Comment c : list) {
            if (c.pos() < limit) continue;
            start = Math.min(start, c.pos());
        }
        return start;
    }

    public static int commentEnd(CommentSet comments, CommentSet.RelativePosition pos) {
        java.util.List<Comment> list = comments.getComments(pos);
        if (list.isEmpty()) {
            return -1;
        }
        return list.get(list.size() - 1).endPos();
    }

    private static int commentEnd(DCTree.DCDocComment doc) {
        int length = doc.comment.getText().length();
        return doc.comment.getSourcePos(length - 1);
    }

    private static int getOldPos(JCTree oldT) {
        return TreeInfo.getStartPos((JCTree)oldT);
    }

    private int getOldPos(DCTree oldT, DCTree.DCDocComment doc) {
        return (int)oldT.getSourcePosition(doc);
    }

    public int endPos(DCTree oldT, DCTree.DCDocComment doc) {
        DocSourcePositions sp = JavacTrees.instance((Context)this.context).getSourcePositions();
        return (int)sp.getEndPosition(null, (DocCommentTree)doc, (DocTree)oldT);
    }

    private int endPos(java.util.List<? extends DCTree> trees, DCTree.DCDocComment doc) {
        if (trees.isEmpty()) {
            return -1;
        }
        return this.endPos(trees.get(trees.size() - 1), doc);
    }

    protected int diffTree(JCTree oldT, JCTree newT, int[] elementBounds) {
        return this.diffTree(oldT, newT, null, elementBounds);
    }

    protected int diffTree(JCTree oldT, JCTree newT, JCTree parent, int[] elementBounds) {
        int result;
        Object t = this.tree2Tag.get((Object)newT);
        if (t != null) {
            int start = this.printer.toString().length();
            result = this.diffTreeImpl(oldT, newT, parent, elementBounds);
            int end = this.printer.toString().length();
            this.tag2Span.put(t, new int[]{start + this.printer.getInitialOffset(), end + this.printer.getInitialOffset()});
        } else {
            result = this.diffTreeImpl(oldT, newT, parent, elementBounds);
        }
        return result;
    }

    private int getPosAfterCommentEnd(Tree t, int minPos) {
        CommentSet cs = this.getCommentsForTree(t, false);
        java.util.List<Comment> cmm = cs.getComments(CommentSet.RelativePosition.TRAILING);
        if (cmm.isEmpty()) {
            cmm = cs.getComments(CommentSet.RelativePosition.INLINE);
        }
        if (cmm.isEmpty()) {
            return minPos;
        }
        Comment c = cmm.get(cmm.size() - 1);
        int pos = c.endPos();
        assert (pos >= 0);
        if ((c.style() == Comment.Style.LINE || c.style() == Comment.Style.WHITESPACE) && pos > 0 && this.diffContext.origText.charAt(pos - 1) == '\n') {
            --pos;
        }
        return Math.max(minPos, pos);
    }

    private int getPosAfterCommentStart(Tree t, int minPos) {
        CommentSet cs = this.getCommentsForTree(t, true);
        java.util.List<Comment> cmm = cs.getComments(CommentSet.RelativePosition.PRECEDING);
        if (cmm.isEmpty()) {
            cmm = cs.getComments(CommentSet.RelativePosition.INNER);
        }
        if (cmm.isEmpty()) {
            return minPos;
        }
        Comment c = cmm.get(cmm.size() - 1);
        int pos = c.endPos();
        assert (pos >= 0);
        return Math.max(minPos, pos);
    }

    private int getPosAfterTreeComments(JCTree t, int end) {
        class Scn
        extends TreeScanner<Void, Void> {
            int max;

            Scn() {
                this.max = -1;
            }

            public Void scan(Tree node, Void p) {
                this.max = Math.max(CasualDiff.this.getPosAfterCommentEnd((Tree)((JCTree)node), -1), this.max);
                return (Void)TreeScanner.super.scan(node, (Object)p);
            }
        }
        Scn scn = new Scn();
        scn.scan((Tree)t, null);
        return Math.max(scn.max, end);
    }

    protected int diffTreeImpl(JCTree oldT, JCTree newT, JCTree parent, int[] elementBounds) {
        boolean handleImplicitLambda;
        if (oldT == null && newT != null) {
            throw new IllegalArgumentException("Null is not allowed in parameters.");
        }
        if (oldT == newT) {
            return elementBounds[0];
        }
        if (newT == null) {
            this.tokenSequence.move(elementBounds[1]);
            if (!this.tokenSequence.moveNext()) {
                return elementBounds[1];
            }
            while (this.tokenSequence.token().id() == JavaTokenId.WHITESPACE && this.tokenSequence.moveNext()) {
            }
            return this.tokenSequence.offset();
        }
        if (this.printer.handlePossibleOldTrees(Collections.singletonList(newT), true)) {
            return this.getCommentCorrectedEndPos(oldT);
        }
        boolean bl = handleImplicitLambda = parent != null && parent.hasTag(JCTree.Tag.LAMBDA) && ((JCTree.JCLambda)parent).params.size() == 1 && ((JCTree.JCLambda)parent).params.get(0) == oldT && ((JCTree.JCLambda)parent).paramKind == JCTree.JCLambda.ParameterKind.IMPLICIT && newT.hasTag(JCTree.Tag.VARDEF) && ((JCTree.JCVariableDecl)newT).getType() != null;
        if (handleImplicitLambda) {
            this.tokenSequence.move(CasualDiff.getOldPos(parent));
            if (this.tokenSequence.moveNext() && this.tokenSequence.token().id() == JavaTokenId.LPAREN) {
                handleImplicitLambda = false;
            } else {
                this.printer.print("(");
            }
        }
        if (!(oldT.getTag() == newT.getTag() || compAssign.contains((Object)oldT.getKind()) && compAssign.contains((Object)newT.getKind()) || binaries.contains((Object)oldT.getKind()) && binaries.contains((Object)newT.getKind()) || unaries.contains((Object)oldT.getKind()) && unaries.contains((Object)newT.getKind()))) {
            int[] oldBounds = this.getBounds(oldT);
            elementBounds[0] = this.getPosAfterCommentStart((Tree)oldT, elementBounds[0]);
            if (oldBounds[0] > elementBounds[0]) {
                this.copyTo(elementBounds[0], oldBounds[0]);
            }
            this.printer.print(newT);
            return this.getPosAfterTreeComments(oldT, oldBounds[1]);
        }
        int predComments = this.diffPrecedingComments(oldT, newT, CasualDiff.getOldPos(oldT), elementBounds[0], oldT.getTag() == JCTree.Tag.TOPLEVEL && this.diffContext.forceInitialComment);
        int retVal = -1;
        elementBounds[0] = Math.abs(predComments);
        int elementEnd = elementBounds[1];
        int commentsStart = Math.min(CasualDiff.commentStart(this.diffContext, this.comments.getComments((Tree)oldT), CommentSet.RelativePosition.INLINE, this.endPos(oldT)), CasualDiff.commentStart(this.diffContext, this.comments.getComments((Tree)oldT), CommentSet.RelativePosition.TRAILING, this.endPos(oldT)));
        if (commentsStart < elementBounds[1]) {
            int lastIndex;
            this.tokenSequence.move(commentsStart);
            elementBounds = Arrays.copyOf(elementBounds, elementBounds.length);
            elementBounds[1] = this.tokenSequence.movePrevious() && this.tokenSequence.token().id() == JavaTokenId.WHITESPACE && (lastIndex = this.tokenSequence.token().text().toString().lastIndexOf(10)) > -1 ? this.tokenSequence.offset() + lastIndex + 1 : commentsStart;
        }
        switch (oldT.getTag()) {
            case TOPLEVEL: {
                this.diffTopLevel((JCTree.JCCompilationUnit)oldT, (JCTree.JCCompilationUnit)newT, elementBounds);
                break;
            }
            case IMPORT: {
                retVal = this.diffImport((JCTree.JCImport)oldT, (JCTree.JCImport)newT, elementBounds);
                break;
            }
            case CLASSDEF: {
                retVal = this.diffClassDef((JCTree.JCClassDecl)oldT, (JCTree.JCClassDecl)newT, elementBounds);
                break;
            }
            case METHODDEF: {
                retVal = this.diffMethodDef((JCTree.JCMethodDecl)oldT, (JCTree.JCMethodDecl)newT, elementBounds);
                break;
            }
            case VARDEF: {
                retVal = this.diffVarDef((JCTree.JCVariableDecl)oldT, (JCTree.JCVariableDecl)newT, elementBounds);
                break;
            }
            case SKIP: {
                this.copyTo(elementBounds[0], elementBounds[1]);
                retVal = elementBounds[1];
                break;
            }
            case BLOCK: {
                retVal = this.diffBlock((JCTree.JCBlock)oldT, (JCTree.JCBlock)newT, elementBounds);
                break;
            }
            case DOLOOP: {
                retVal = this.diffDoLoop((JCTree.JCDoWhileLoop)oldT, (JCTree.JCDoWhileLoop)newT, elementBounds);
                break;
            }
            case WHILELOOP: {
                retVal = this.diffWhileLoop((JCTree.JCWhileLoop)oldT, (JCTree.JCWhileLoop)newT, elementBounds);
                break;
            }
            case FORLOOP: {
                retVal = this.diffForLoop((JCTree.JCForLoop)oldT, (JCTree.JCForLoop)newT, elementBounds);
                break;
            }
            case FOREACHLOOP: {
                retVal = this.diffForeachLoop((JCTree.JCEnhancedForLoop)oldT, (JCTree.JCEnhancedForLoop)newT, elementBounds);
                break;
            }
            case LABELLED: {
                retVal = this.diffLabelled((JCTree.JCLabeledStatement)oldT, (JCTree.JCLabeledStatement)newT, elementBounds);
                break;
            }
            case SWITCH: {
                retVal = this.diffSwitch((JCTree.JCSwitch)oldT, (JCTree.JCSwitch)newT, elementBounds);
                break;
            }
            case CASE: {
                retVal = this.diffCase((JCTree.JCCase)oldT, (JCTree.JCCase)newT, elementBounds);
                break;
            }
            case SYNCHRONIZED: {
                retVal = this.diffSynchronized((JCTree.JCSynchronized)oldT, (JCTree.JCSynchronized)newT, elementBounds);
                break;
            }
            case TRY: {
                retVal = this.diffTry((JCTree.JCTry)oldT, (JCTree.JCTry)newT, elementBounds);
                break;
            }
            case CATCH: {
                retVal = this.diffCatch((JCTree.JCCatch)oldT, (JCTree.JCCatch)newT, elementBounds);
                break;
            }
            case CONDEXPR: {
                retVal = this.diffConditional((JCTree.JCConditional)oldT, (JCTree.JCConditional)newT, elementBounds);
                break;
            }
            case IF: {
                retVal = this.diffIf((JCTree.JCIf)oldT, (JCTree.JCIf)newT, elementBounds);
                break;
            }
            case EXEC: {
                retVal = this.diffExec((JCTree.JCExpressionStatement)oldT, (JCTree.JCExpressionStatement)newT, elementBounds);
                break;
            }
            case BREAK: {
                retVal = this.diffBreak((JCTree.JCBreak)oldT, (JCTree.JCBreak)newT, elementBounds);
                break;
            }
            case CONTINUE: {
                retVal = this.diffContinue((JCTree.JCContinue)oldT, (JCTree.JCContinue)newT, elementBounds);
                break;
            }
            case RETURN: {
                retVal = this.diffReturn((JCTree.JCReturn)oldT, (JCTree.JCReturn)newT, elementBounds);
                break;
            }
            case THROW: {
                retVal = this.diffThrow((JCTree.JCThrow)oldT, (JCTree.JCThrow)newT, elementBounds);
                break;
            }
            case ASSERT: {
                retVal = this.diffAssert((JCTree.JCAssert)oldT, (JCTree.JCAssert)newT, elementBounds);
                break;
            }
            case APPLY: {
                retVal = this.diffApply((JCTree.JCMethodInvocation)oldT, (JCTree.JCMethodInvocation)newT, elementBounds);
                break;
            }
            case NEWCLASS: {
                retVal = this.diffNewClass((JCTree.JCNewClass)oldT, (JCTree.JCNewClass)newT, elementBounds);
                break;
            }
            case NEWARRAY: {
                retVal = this.diffNewArray((JCTree.JCNewArray)oldT, (JCTree.JCNewArray)newT, elementBounds);
                break;
            }
            case PARENS: {
                retVal = this.diffParens((JCTree.JCParens)oldT, (JCTree.JCParens)newT, elementBounds);
                break;
            }
            case ASSIGN: {
                retVal = this.diffAssign((JCTree.JCAssign)oldT, (JCTree.JCAssign)newT, parent, elementBounds);
                break;
            }
            case TYPECAST: {
                retVal = this.diffTypeCast((JCTree.JCTypeCast)oldT, (JCTree.JCTypeCast)newT, elementBounds);
                break;
            }
            case TYPETEST: {
                retVal = this.diffTypeTest((JCTree.JCInstanceOf)oldT, (JCTree.JCInstanceOf)newT, elementBounds);
                break;
            }
            case INDEXED: {
                retVal = this.diffIndexed((JCTree.JCArrayAccess)oldT, (JCTree.JCArrayAccess)newT, elementBounds);
                break;
            }
            case SELECT: {
                retVal = this.diffSelect((JCTree.JCFieldAccess)oldT, (JCTree.JCFieldAccess)newT, elementBounds);
                break;
            }
            case IDENT: {
                retVal = this.diffIdent((JCTree.JCIdent)oldT, (JCTree.JCIdent)newT, elementBounds);
                break;
            }
            case LITERAL: {
                retVal = this.diffLiteral((JCTree.JCLiteral)oldT, (JCTree.JCLiteral)newT, elementBounds);
                break;
            }
            case TYPEIDENT: {
                retVal = this.diffTypeIdent((JCTree.JCPrimitiveTypeTree)oldT, (JCTree.JCPrimitiveTypeTree)newT, elementBounds);
                break;
            }
            case TYPEARRAY: {
                retVal = this.diffTypeArray((JCTree.JCArrayTypeTree)oldT, (JCTree.JCArrayTypeTree)newT, elementBounds);
                break;
            }
            case TYPEAPPLY: {
                retVal = this.diffTypeApply((JCTree.JCTypeApply)oldT, (JCTree.JCTypeApply)newT, elementBounds);
                break;
            }
            case TYPEPARAMETER: {
                retVal = this.diffTypeParameter((JCTree.JCTypeParameter)oldT, (JCTree.JCTypeParameter)newT, elementBounds);
                break;
            }
            case WILDCARD: {
                retVal = this.diffWildcard((JCTree.JCWildcard)oldT, (JCTree.JCWildcard)newT, elementBounds);
                break;
            }
            case TYPEBOUNDKIND: {
                retVal = this.diffTypeBoundKind((JCTree.TypeBoundKind)oldT, (JCTree.TypeBoundKind)newT, elementBounds);
                break;
            }
            case ANNOTATION: 
            case TYPE_ANNOTATION: {
                retVal = this.diffAnnotation((JCTree.JCAnnotation)oldT, (JCTree.JCAnnotation)newT, elementBounds);
                break;
            }
            case LETEXPR: {
                this.diffLetExpr((JCTree.LetExpr)oldT, (JCTree.LetExpr)newT);
                break;
            }
            case POS: 
            case NEG: 
            case NOT: 
            case COMPL: 
            case PREINC: 
            case PREDEC: 
            case POSTINC: 
            case POSTDEC: 
            case NULLCHK: {
                retVal = this.diffUnary((JCTree.JCUnary)oldT, (JCTree.JCUnary)newT, elementBounds);
                break;
            }
            case OR: 
            case AND: 
            case BITOR: 
            case BITXOR: 
            case BITAND: 
            case EQ: 
            case NE: 
            case LT: 
            case GT: 
            case LE: 
            case GE: 
            case SL: 
            case SR: 
            case USR: 
            case PLUS: 
            case MINUS: 
            case MUL: 
            case DIV: 
            case MOD: {
                retVal = this.diffBinary((JCTree.JCBinary)oldT, (JCTree.JCBinary)newT, elementBounds);
                break;
            }
            case BITOR_ASG: 
            case BITXOR_ASG: 
            case BITAND_ASG: 
            case SL_ASG: 
            case SR_ASG: 
            case USR_ASG: 
            case PLUS_ASG: 
            case MINUS_ASG: 
            case MUL_ASG: 
            case DIV_ASG: 
            case MOD_ASG: {
                retVal = this.diffAssignop((JCTree.JCAssignOp)oldT, (JCTree.JCAssignOp)newT, elementBounds);
                break;
            }
            case ERRONEOUS: {
                this.diffErroneous((JCTree.JCErroneous)oldT, (JCTree.JCErroneous)newT, elementBounds);
                break;
            }
            case MODIFIERS: {
                retVal = this.diffModifiers((JCTree.JCModifiers)oldT, (JCTree.JCModifiers)newT, parent, elementBounds[0]);
                this.copyTo(retVal, elementBounds[1]);
                break;
            }
            case TYPEUNION: {
                retVal = this.diffUnionType((JCTree.JCTypeUnion)oldT, (JCTree.JCTypeUnion)newT, elementBounds);
                break;
            }
            case LAMBDA: {
                retVal = this.diffLambda((JCTree.JCLambda)oldT, (JCTree.JCLambda)newT, elementBounds);
                break;
            }
            case REFERENCE: {
                retVal = this.diffMemberReference((JCTree.JCMemberReference)oldT, (JCTree.JCMemberReference)newT, elementBounds);
                break;
            }
            case ANNOTATED_TYPE: {
                retVal = this.diffAnnotatedType((JCTree.JCAnnotatedType)oldT, (JCTree.JCAnnotatedType)newT, elementBounds);
                break;
            }
            default: {
                if (oldT.getKind() == Tree.Kind.OTHER) {
                    if (!(oldT instanceof FieldGroupTree)) break;
                    return this.diffFieldGroup((FieldGroupTree)oldT, (FieldGroupTree)newT, elementBounds);
                }
                String msg = "Diff not implemented: " + oldT.getKind().toString() + " " + oldT.getClass().getName();
                throw new AssertionError((Object)msg);
            }
        }
        if (handleImplicitLambda) {
            this.printer.print(")");
        }
        int endComment = this.getCommentCorrectedEndPos(oldT);
        return this.diffTrailingComments(oldT, newT, retVal, endComment);
    }

    protected boolean listsMatch(java.util.List<? extends JCTree> oldList, java.util.List<? extends JCTree> newList) {
        if (oldList == newList) {
            return true;
        }
        int n = oldList.size();
        if (newList.size() != n) {
            return false;
        }
        for (int i = 0; i < n; ++i) {
            if (this.treesMatch(oldList.get(i), newList.get(i))) continue;
            return false;
        }
        return true;
    }

    private boolean matchImport(JCTree.JCImport t1, JCTree.JCImport t2) {
        return t1.staticImport == t2.staticImport && this.treesMatch(t1.qualid, t2.qualid);
    }

    private boolean matchBlock(JCTree.JCBlock t1, JCTree.JCBlock t2) {
        return t1.flags == t2.flags && this.listsMatch((java.util.List<? extends JCTree>)t1.stats, (java.util.List<? extends JCTree>)t2.stats);
    }

    private boolean matchDoLoop(JCTree.JCDoWhileLoop t1, JCTree.JCDoWhileLoop t2) {
        return this.treesMatch((JCTree)t1.cond, (JCTree)t2.cond) && this.treesMatch((JCTree)t1.body, (JCTree)t2.body);
    }

    private boolean matchWhileLoop(JCTree.JCWhileLoop t1, JCTree.JCWhileLoop t2) {
        return this.treesMatch((JCTree)t1.cond, (JCTree)t2.cond) && this.treesMatch((JCTree)t1.body, (JCTree)t2.body);
    }

    private boolean matchForLoop(JCTree.JCForLoop t1, JCTree.JCForLoop t2) {
        return this.listsMatch((java.util.List<? extends JCTree>)t1.init, (java.util.List<? extends JCTree>)t2.init) && this.treesMatch((JCTree)t1.cond, (JCTree)t2.cond) && this.listsMatch((java.util.List<? extends JCTree>)t1.step, (java.util.List<? extends JCTree>)t2.step) && this.treesMatch((JCTree)t1.body, (JCTree)t2.body);
    }

    private boolean matchForeachLoop(JCTree.JCEnhancedForLoop t1, JCTree.JCEnhancedForLoop t2) {
        return this.treesMatch((JCTree)t1.var, (JCTree)t2.var) && this.treesMatch((JCTree)t1.expr, (JCTree)t2.expr) && this.treesMatch((JCTree)t1.body, (JCTree)t2.body);
    }

    private boolean matchLabelled(JCTree.JCLabeledStatement t1, JCTree.JCLabeledStatement t2) {
        return t1.label == t2.label && this.treesMatch((JCTree)t1.body, (JCTree)t2.body);
    }

    private boolean matchSwitch(JCTree.JCSwitch t1, JCTree.JCSwitch t2) {
        return this.treesMatch((JCTree)t1.selector, (JCTree)t2.selector) && this.listsMatch((java.util.List<? extends JCTree>)t1.cases, (java.util.List<? extends JCTree>)t2.cases);
    }

    private boolean matchCase(JCTree.JCCase t1, JCTree.JCCase t2) {
        return this.treesMatch((JCTree)t1.pat, (JCTree)t2.pat) && this.listsMatch((java.util.List<? extends JCTree>)t1.stats, (java.util.List<? extends JCTree>)t2.stats);
    }

    private boolean matchSynchronized(JCTree.JCSynchronized t1, JCTree.JCSynchronized t2) {
        return this.treesMatch((JCTree)t1.lock, (JCTree)t2.lock) && this.treesMatch((JCTree)t1.body, (JCTree)t2.body);
    }

    private boolean matchTry(JCTree.JCTry t1, JCTree.JCTry t2) {
        return this.treesMatch((JCTree)t1.finalizer, (JCTree)t2.finalizer) && this.listsMatch((java.util.List<? extends JCTree>)t1.catchers, (java.util.List<? extends JCTree>)t2.catchers) && this.treesMatch((JCTree)t1.body, (JCTree)t2.body);
    }

    private boolean matchCatch(JCTree.JCCatch t1, JCTree.JCCatch t2) {
        return this.treesMatch((JCTree)t1.param, (JCTree)t2.param) && this.treesMatch((JCTree)t1.body, (JCTree)t2.body);
    }

    private boolean matchConditional(JCTree.JCConditional t1, JCTree.JCConditional t2) {
        return this.treesMatch((JCTree)t1.cond, (JCTree)t2.cond) && this.treesMatch((JCTree)t1.truepart, (JCTree)t2.truepart) && this.treesMatch((JCTree)t1.falsepart, (JCTree)t2.falsepart);
    }

    private boolean matchIf(JCTree.JCIf t1, JCTree.JCIf t2) {
        return this.treesMatch((JCTree)t1.cond, (JCTree)t2.cond) && this.treesMatch((JCTree)t1.thenpart, (JCTree)t2.thenpart) && this.treesMatch((JCTree)t1.elsepart, (JCTree)t2.elsepart);
    }

    private boolean matchBreak(JCTree.JCBreak t1, JCTree.JCBreak t2) {
        return t1.label == t2.label && this.treesMatch(t1.target, t2.target);
    }

    private boolean matchContinue(JCTree.JCContinue t1, JCTree.JCContinue t2) {
        return t1.label == t2.label && this.treesMatch(t1.target, t2.target);
    }

    private boolean matchAssert(JCTree.JCAssert t1, JCTree.JCAssert t2) {
        return this.treesMatch((JCTree)t1.cond, (JCTree)t2.cond) && this.treesMatch((JCTree)t1.detail, (JCTree)t2.detail);
    }

    private boolean matchApply(JCTree.JCMethodInvocation t1, JCTree.JCMethodInvocation t2) {
        return t1.varargsElement == t2.varargsElement && this.listsMatch((java.util.List<? extends JCTree>)t1.typeargs, (java.util.List<? extends JCTree>)t2.typeargs) && this.treesMatch((JCTree)t1.meth, (JCTree)t2.meth) && this.listsMatch((java.util.List<? extends JCTree>)t1.args, (java.util.List<? extends JCTree>)t2.args);
    }

    private boolean matchNewClass(JCTree.JCNewClass t1, JCTree.JCNewClass t2) {
        return t1.constructor == t2.constructor && this.treesMatch((JCTree)t1.getIdentifier(), (JCTree)t2.getIdentifier()) && this.listsMatch((java.util.List<? extends JCTree>)t1.typeargs, (java.util.List<? extends JCTree>)t2.typeargs) && this.listsMatch((java.util.List<? extends JCTree>)t1.args, (java.util.List<? extends JCTree>)t2.args) && t1.varargsElement == t2.varargsElement && this.treesMatch((JCTree)t1.def, (JCTree)t2.def);
    }

    private boolean matchNewArray(JCTree.JCNewArray t1, JCTree.JCNewArray t2) {
        return this.treesMatch((JCTree)t1.elemtype, (JCTree)t2.elemtype) && this.listsMatch((java.util.List<? extends JCTree>)t1.dims, (java.util.List<? extends JCTree>)t2.dims) && this.listsMatch((java.util.List<? extends JCTree>)t1.elems, (java.util.List<? extends JCTree>)t2.elems);
    }

    private boolean matchAssign(JCTree.JCAssign t1, JCTree.JCAssign t2) {
        return this.treesMatch((JCTree)t1.lhs, (JCTree)t2.lhs) && this.treesMatch((JCTree)t1.rhs, (JCTree)t2.rhs);
    }

    private boolean matchAssignop(JCTree.JCAssignOp t1, JCTree.JCAssignOp t2) {
        return t1.operator == t2.operator && this.treesMatch((JCTree)t1.lhs, (JCTree)t2.lhs) && this.treesMatch((JCTree)t1.rhs, (JCTree)t2.rhs);
    }

    private boolean matchUnary(JCTree.JCUnary t1, JCTree.JCUnary t2) {
        return t1.operator == t2.operator && this.treesMatch((JCTree)t1.arg, (JCTree)t2.arg);
    }

    private boolean matchBinary(JCTree.JCBinary t1, JCTree.JCBinary t2) {
        return t1.operator == t2.operator && this.treesMatch((JCTree)t1.lhs, (JCTree)t2.lhs) && this.treesMatch((JCTree)t1.rhs, (JCTree)t2.rhs);
    }

    private boolean matchTypeCast(JCTree.JCTypeCast t1, JCTree.JCTypeCast t2) {
        return this.treesMatch(t1.clazz, t2.clazz) && this.treesMatch((JCTree)t1.expr, (JCTree)t2.expr);
    }

    private boolean matchTypeTest(JCTree.JCInstanceOf t1, JCTree.JCInstanceOf t2) {
        return this.treesMatch(t1.clazz, t2.clazz) && this.treesMatch((JCTree)t1.expr, (JCTree)t2.expr);
    }

    private boolean matchIndexed(JCTree.JCArrayAccess t1, JCTree.JCArrayAccess t2) {
        return this.treesMatch((JCTree)t1.indexed, (JCTree)t2.indexed) && this.treesMatch((JCTree)t1.index, (JCTree)t2.index);
    }

    private boolean matchSelect(JCTree.JCFieldAccess t1, JCTree.JCFieldAccess t2) {
        return this.treesMatch((JCTree)t1.selected, (JCTree)t2.selected) && t1.name == t2.name;
    }

    private boolean matchReference(JCTree.JCMemberReference t1, JCTree.JCMemberReference t2) {
        return this.treesMatch((JCTree)t1.expr, (JCTree)t2.expr) && t1.name == t2.name;
    }

    private boolean matchLiteral(JCTree.JCLiteral t1, JCTree.JCLiteral t2) {
        return t1.typetag == t2.typetag && (t1.value == t2.value || t1.value != null && t1.value.equals(t2.value));
    }

    private boolean matchTypeApply(JCTree.JCTypeApply t1, JCTree.JCTypeApply t2) {
        return this.treesMatch((JCTree)t1.clazz, (JCTree)t2.clazz) && this.listsMatch((java.util.List<? extends JCTree>)t1.arguments, (java.util.List<? extends JCTree>)t2.arguments);
    }

    private boolean matchAnnotatedType(JCTree.JCAnnotatedType t1, JCTree.JCAnnotatedType t2) {
        return this.treesMatch((JCTree)t1.underlyingType, (JCTree)t2.underlyingType) && this.listsMatch((java.util.List<? extends JCTree>)t1.annotations, (java.util.List<? extends JCTree>)t2.annotations);
    }

    private boolean matchTypeParameter(JCTree.JCTypeParameter t1, JCTree.JCTypeParameter t2) {
        return t1.name == t2.name && this.listsMatch((java.util.List<? extends JCTree>)t1.bounds, (java.util.List<? extends JCTree>)t2.bounds);
    }

    private boolean matchWildcard(JCTree.JCWildcard t1, JCTree.JCWildcard t2) {
        return t1.kind == t2.kind && this.treesMatch(t1.inner, t2.inner);
    }

    private boolean matchAnnotation(JCTree.JCAnnotation t1, JCTree.JCAnnotation t2) {
        return this.treesMatch(t1.annotationType, t2.annotationType) && this.listsMatch((java.util.List<? extends JCTree>)t1.args, (java.util.List<? extends JCTree>)t2.args);
    }

    private boolean matchModifiers(JCTree.JCModifiers t1, JCTree.JCModifiers t2) {
        return t1.flags == t2.flags && this.listsMatch((java.util.List<? extends JCTree>)t1.annotations, (java.util.List<? extends JCTree>)t2.annotations);
    }

    private boolean matchLetExpr(JCTree.LetExpr t1, JCTree.LetExpr t2) {
        return this.listsMatch((java.util.List<? extends JCTree>)t1.defs, (java.util.List<? extends JCTree>)t2.defs) && this.treesMatch(t1.expr, t2.expr);
    }

    private boolean matchLambda(JCTree.JCLambda t1, JCTree.JCLambda t2) {
        return this.listsMatch((java.util.List<? extends JCTree>)t1.params, (java.util.List<? extends JCTree>)t2.params) && this.treesMatch(t1.body, t2.body);
    }

    private boolean isCommaSeparated(JCTree.JCVariableDecl oldT) {
        if (CasualDiff.getOldPos((JCTree)oldT) <= 0 || oldT.pos <= 0) {
            return false;
        }
        this.tokenSequence.move(oldT.pos);
        PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.BACKWARD);
        if (this.tokenSequence.token() == null) {
            return false;
        }
        if (JavaTokenId.COMMA == this.tokenSequence.token().id()) {
            return true;
        }
        if (oldT.getInitializer() != null && (oldT.mods.flags & 16384) == 0) {
            this.tokenSequence.move(this.endPos((JCTree)oldT.getInitializer()));
        } else {
            this.tokenSequence.move(oldT.pos);
            this.tokenSequence.moveNext();
        }
        PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.FORWARD);
        if (this.tokenSequence.token() == null) {
            return false;
        }
        if (JavaTokenId.COMMA == this.tokenSequence.token().id()) {
            return true;
        }
        return false;
    }

    private int getCommentCorrectedOldPos(JCTree tree) {
        CommentSet ch = this.comments.getComments((Tree)tree);
        return Math.min(CasualDiff.getOldPos(tree), CasualDiff.commentStart(this.diffContext, ch, CommentSet.RelativePosition.PRECEDING, CasualDiff.getOldPos(tree)));
    }

    private int getCommentCorrectedEndPos(JCTree tree) {
        final int[] res = new int[]{this.endPos(tree)};
        new TreeScanner<Void, Void>(){

            public Void scan(Tree node, Void p) {
                if (node != null) {
                    CommentSet ch = CasualDiff.this.comments.getComments(node);
                    res[0] = Math.max(res[0], Math.max(CasualDiff.commentEnd(ch, CommentSet.RelativePosition.INLINE), CasualDiff.commentEnd(ch, CommentSet.RelativePosition.TRAILING)));
                }
                return (Void)TreeScanner.super.scan(node, (Object)p);
            }
        }.scan((Tree)tree, null);
        return res[0];
    }

    private int[] getCommentCorrectedBounds(JCTree tree) {
        return new int[]{this.getCommentCorrectedOldPos(tree), this.getCommentCorrectedEndPos(tree)};
    }

    private int[] getBounds(JCTree tree) {
        return new int[]{CasualDiff.getOldPos(tree), this.endPos(tree)};
    }

    private int[] getBounds(DCTree tree, DCTree.DCDocComment doc) {
        return new int[]{this.getOldPos(tree, doc), this.endPos(tree, doc)};
    }

    private int copyUpTo(int from, int to) {
        if (from < to) {
            this.copyTo(from, to);
            return to;
        }
        return from;
    }

    private void copyTo(int from, int to) {
        this.copyTo(from, to, this.printer);
    }

    public void copyTo(int from, int to, VeryPretty loc) {
        if (from == to) {
            return;
        }
        if (from > to || from < 0 || to < 0) {
            LOG.log(Level.INFO, "-----\n" + this.origText + "-----\n");
            LOG.log(Level.INFO, "Illegal values: from = " + from + "; to = " + to + "." + "Please, attach your messages.log to new issue!");
            if (noInvalidCopyTos) {
                throw new IllegalStateException("Illegal values: from = " + from + "; to = " + to + ".");
            }
            if (to >= 0) {
                this.printer.eatChars(from - to);
            }
            return;
        }
        if (to > this.origText.length()) {
            LOG.severe("-----\n" + this.origText + "-----\n");
            throw new IllegalArgumentException("Copying to " + to + " is greater then its size (" + this.origText.length() + ").");
        }
        if (this.boundaries == null) {
            this.boundaries = this.diffContext.blockSequences.getBoundaries();
        }
        if (this.nextBlockBoundary == -1 && this.boundaries.hasNext()) {
            this.nextBlockBoundary = this.boundaries.next();
        }
        while (this.nextBlockBoundary != -1 && this.nextBlockBoundary < from) {
            if (this.boundaries.hasNext()) {
                this.nextBlockBoundary = this.boundaries.next();
                continue;
            }
            this.nextBlockBoundary = -1;
            break;
        }
        while (from <= this.nextBlockBoundary && to >= this.nextBlockBoundary) {
            int off = this.nextBlockBoundary - from;
            int mapped = loc.out.length() + (from < this.nextBlockBoundary ? off : 0);
            Integer prev = this.blockSequenceMap.put(this.nextBlockBoundary, mapped);
            if (prev != null) {
                this.blockSequenceMap.put(this.nextBlockBoundary, prev);
            }
            this.nextBlockBoundary = this.boundaries.hasNext() ? this.boundaries.next() : -1;
        }
        loc.print(this.origText.substring(from, to));
    }

    private int diffTree(JCTree oldT, JCTree newT, int[] elementBounds, Tree.Kind parentKind) {
        return this.diffTree(oldT, newT, elementBounds, parentKind, true);
    }

    private int diffTree(JCTree oldT, JCTree newT, int[] elementBounds, Tree.Kind parentKind, boolean retainNewline) {
        if (oldT.getKind() != newT.getKind() && newT.getKind() == Tree.Kind.BLOCK) {
            this.tokenSequence.move(CasualDiff.getOldPos(oldT));
            PositionEstimator.moveToSrcRelevant(this.tokenSequence, PositionEstimator.Direction.BACKWARD);
            this.tokenSequence.moveNext();
            if (elementBounds[0] < this.tokenSequence.offset()) {
                this.copyTo(elementBounds[0], this.tokenSequence.offset());
            }
            this.printer.printBlock(oldT, newT, parentKind);
            int localpointer = this.getCommentCorrectedEndPos(oldT);
            if (retainNewline && localpointer > 0 && this.origText.charAt(localpointer - 1) == '\n') {
                this.printer.newline();
            }
            return localpointer;
        }
        elementBounds[0] = this.getCommentCorrectedBounds(oldT)[0];
        this.copyTo(elementBounds[0], elementBounds[0]);
        return this.diffTree(oldT, newT, elementBounds);
    }

    private static String printCodeStyle(CodeStyle style) {
        if (style == null) {
            return "<none>";
        }
        StringBuilder sb = new StringBuilder();
        try {
            Method[] arr;
            for (Method m : arr = style.getClass().getMethods()) {
                if (!Modifier.isPublic(m.getModifiers()) || m.getParameterTypes().length != 0) continue;
                String s = m.getName();
                if (s.startsWith("get")) {
                    s = s.substring(3);
                } else if (s.startsWith("is")) {
                    s = s.substring(2);
                }
                java.util.List<Object> val = m.invoke(style, new Object[0]);
                if (val instanceof Object[]) {
                    val = Arrays.asList((Object[])val);
                }
                sb.append(s).append(":").append(val).append("\n");
            }
        }
        catch (Exception ex) {
            // empty catch block
        }
        return sb.toString();
    }

    static /* synthetic */ VeryPretty access$000(CasualDiff x0) {
        return x0.printer;
    }

    public static class Diff {
        public DiffTypes type;
        int pos;
        int endOffset;
        protected JCTree oldTree;
        protected JCTree newTree;
        protected Comment oldComment;
        protected Comment newComment;
        private String text;
        boolean trailing;

        public static Diff insert(int pos, String text) {
            return new Diff(DiffTypes.INSERT, pos, -1, text);
        }

        public static Diff delete(int startOffset, int endOffset) {
            return new Diff(DiffTypes.DELETE, startOffset, endOffset, null);
        }

        Diff(DiffTypes type, int pos, int endOffset, String text) {
            this.type = type;
            this.pos = pos;
            this.endOffset = endOffset;
            this.text = text;
        }

        Diff(DiffTypes type, int pos, JCTree oldTree, JCTree newTree, Comment oldComment, Comment newComment, boolean trailing) {
            this(type, pos, -1, null);
            assert (pos >= 0);
            this.oldTree = oldTree;
            this.newTree = newTree;
            this.oldComment = oldComment;
            this.newComment = newComment;
            this.trailing = trailing;
        }

        public JCTree getOld() {
            return this.oldTree;
        }

        public JCTree getNew() {
            return this.newTree;
        }

        public int getPos() {
            return this.pos;
        }

        public int getEnd() {
            return this.endOffset;
        }

        public String getText() {
            return this.text;
        }

        public Comment getOldComment() {
            return this.oldComment;
        }

        public Comment getNewComment() {
            return this.newComment;
        }

        public boolean isTrailingComment() {
            return this.trailing;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof Diff)) {
                return false;
            }
            Diff d2 = (Diff)obj;
            return this.type != d2.type && this.pos != d2.pos && this.oldTree != d2.oldTree && this.newTree != d2.newTree && this.oldComment != d2.oldComment && this.newComment != d2.newComment && this.trailing != d2.trailing;
        }

        public int hashCode() {
            return this.type.hashCode() + this.pos + (this.oldTree != null ? this.oldTree.hashCode() : 0) + (this.newTree != null ? this.newTree.hashCode() : 0) + (this.oldComment != null ? this.oldComment.hashCode() : 0) + (this.newComment != null ? this.newComment.hashCode() : 0) + Boolean.valueOf(this.trailing).hashCode();
        }

        public String toString() {
            StringBuffer sb = new StringBuffer();
            sb.append("tree (");
            sb.append(this.type.toString());
            sb.append(") pos=");
            sb.append(this.pos);
            sb.append(", end=").append(this.endOffset);
            if (this.trailing) {
                sb.append(" trailing comment");
            }
            sb.append("\n");
            if (this.type == DiffTypes.DELETE || this.type == DiffTypes.INSERT || this.type == DiffTypes.MODIFY) {
                this.addDiffString(sb, (Object)this.oldTree, (Object)this.newTree);
            } else {
                this.addDiffString(sb, this.oldComment, this.newComment);
            }
            return sb.toString();
        }

        private void addDiffString(StringBuffer sb, Object o1, Object o2) {
            if (o1 != null) {
                sb.append("< ");
                sb.append(o1.toString());
                sb.append(o2 != null ? "\n---\n> " : "\n");
            } else {
                sb.append("> ");
            }
            if (o2 != null) {
                sb.append(o2.toString());
                sb.append('\n');
            }
        }
    }

    public static enum LineInsertionType {
        BEFORE,
        AFTER,
        NONE;
        

        private LineInsertionType() {
        }
    }

    public static enum DiffTypes {
        MODIFY("modify"),
        INSERT("insert"),
        DELETE("delete");
        
        public final String name;

        private DiffTypes(String name) {
            this.name = name;
        }
    }

    private static enum ChangeKind {
        INSERT,
        DELETE,
        MODIFY,
        NOCHANGE;
        

        private ChangeKind() {
        }
    }

    private static class SectKey {
        private int off;

        SectKey(int off) {
            this.off = off;
        }
    }

}

