/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.source.save;

class Difference {
    public static final int NONE = -1;
    private int delStart = -1;
    private int delEnd = -1;
    private int addStart = -1;
    private int addEnd = -1;

    public Difference(int delStart, int delEnd, int addStart, int addEnd) {
        this.delStart = delStart;
        this.delEnd = delEnd;
        this.addStart = addStart;
        this.addEnd = addEnd;
    }

    public int getDeletedStart() {
        return this.delStart;
    }

    public int getDeletedEnd() {
        return this.delEnd;
    }

    public int getAddedStart() {
        return this.addStart;
    }

    public int getAddedEnd() {
        return this.addEnd;
    }

    public void setDeleted(int line) {
        this.delStart = Math.min(line, this.delStart);
        this.delEnd = Math.max(line, this.delEnd);
    }

    public void setAdded(int line) {
        this.addStart = Math.min(line, this.addStart);
        this.addEnd = Math.max(line, this.addEnd);
    }

    public boolean equals(Object obj) {
        if (obj instanceof Difference) {
            Difference other = (Difference)obj;
            return this.delStart == other.delStart && this.delEnd == other.delEnd && this.addStart == other.addStart && this.addEnd == other.addEnd;
        }
        return false;
    }

    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("del: [" + this.delStart + ", " + this.delEnd + "]");
        buf.append(" ");
        buf.append("add: [" + this.addStart + ", " + this.addEnd + "]");
        return buf.toString();
    }
}

