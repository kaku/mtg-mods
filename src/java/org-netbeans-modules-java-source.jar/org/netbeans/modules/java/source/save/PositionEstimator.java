/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.Trees
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCCompilationUnit
 *  com.sun.tools.javac.tree.JCTree$JCModifiers
 *  com.sun.tools.javac.tree.JCTree$JCVariableDecl
 *  org.netbeans.api.editor.guards.GuardedSectionManager
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.openide.util.Pair
 */
package org.netbeans.modules.java.source.save;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.Trees;
import com.sun.tools.javac.tree.JCTree;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.text.Document;
import javax.swing.text.StyledDocument;
import org.netbeans.api.editor.guards.GuardedSectionManager;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.source.Comment;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.java.source.save.BlockSequences;
import org.netbeans.modules.java.source.save.CasualDiff;
import org.netbeans.modules.java.source.save.DiffContext;
import org.netbeans.modules.java.source.transform.FieldGroupTree;
import org.openide.util.Pair;

public abstract class PositionEstimator {
    public static final int NOPOS = -2;
    final List<? extends Tree> oldL;
    final List<? extends Tree> newL;
    final DiffContext diffContext;
    boolean initialized;
    final TokenSequence<JavaTokenId> seq;
    GuardedSectionManager guards;
    int[][] matrix;
    public static final EnumSet<JavaTokenId> nonRelevant = EnumSet.of(JavaTokenId.LINE_COMMENT, JavaTokenId.BLOCK_COMMENT, JavaTokenId.JAVADOC_COMMENT, JavaTokenId.WHITESPACE);

    PositionEstimator(List<? extends Tree> oldL, List<? extends Tree> newL, DiffContext diffContext) {
        this.oldL = oldL;
        this.newL = newL;
        this.diffContext = diffContext;
        this.seq = diffContext != null ? diffContext.tokenSequence : null;
        this.guards = diffContext.doc != null ? GuardedSectionManager.getInstance((StyledDocument)((StyledDocument)diffContext.doc)) : null;
        this.initialized = false;
    }

    protected abstract void initialize();

    public abstract int getInsertPos(int var1);

    public abstract int[] getPositions(int var1);

    abstract int prepare(int var1, StringBuilder var2, StringBuilder var3);

    public abstract int[] sectionRemovalBounds(StringBuilder var1);

    public CasualDiff.LineInsertionType lineInsertType() {
        return CasualDiff.LineInsertionType.NONE;
    }

    public abstract String head();

    public abstract String sep();

    public abstract String getIndentString();

    public String append(int index) {
        return "";
    }

    public int[][] getMatrix() {
        if (!this.initialized) {
            this.initialize();
        }
        return this.matrix;
    }

    int moveBelowGuarded(int pos) {
        if (this.diffContext != null) {
            return this.diffContext.blockSequences.findNextWritablePos(pos);
        }
        return pos;
    }

    public static JavaTokenId moveToSrcRelevant(TokenSequence<JavaTokenId> seq, Direction dir) {
        return PositionEstimator.moveToDifferentThan(seq, dir, nonRelevant);
    }

    public static int offsetToSrcWiteOnLine(TokenSequence<JavaTokenId> seq, Direction dir) {
        boolean notBound = false;
        seq.moveNext();
        int savePos = seq.offset();
        block0 : switch (dir) {
            case BACKWARD: {
                while (notBound = seq.movePrevious()) {
                    JavaTokenId tid = (JavaTokenId)seq.token().id();
                    if (tid == JavaTokenId.WHITESPACE) {
                        int nl = seq.token().text().toString().indexOf(10);
                        if (nl <= -1) continue;
                        return seq.offset() + nl + 1;
                    }
                    if (!nonRelevant.contains((Object)tid)) break block0;
                    savePos = seq.offset();
                }
                break;
            }
            case FORWARD: {
                while (notBound = seq.moveNext()) {
                    JavaTokenId tid = (JavaTokenId)seq.token().id();
                    if (tid == JavaTokenId.WHITESPACE) {
                        int nl = seq.token().text().toString().indexOf(10);
                        if (nl <= -1) continue;
                        return seq.offset() + nl;
                    }
                    if (!nonRelevant.contains((Object)tid)) break block0;
                    savePos = seq.offset() + seq.token().length();
                }
                break;
            }
        }
        if (!notBound) {
            return -1;
        }
        return savePos;
    }

    public static JavaTokenId moveToDifferentThan(TokenSequence<JavaTokenId> seq, Direction dir, EnumSet<JavaTokenId> set) {
        boolean notBound = false;
        switch (dir) {
            case BACKWARD: {
                while ((notBound = seq.movePrevious()) && set.contains((Object)seq.token().id())) {
                }
                break;
            }
            case FORWARD: {
                while ((notBound = seq.moveNext()) && set.contains((Object)seq.token().id())) {
                }
                break;
            }
        }
        return notBound ? (JavaTokenId)seq.token().id() : null;
    }

    private static int goAfterFirstNewLine(TokenSequence<JavaTokenId> seq) {
        if (seq.token() == null) {
            seq.movePrevious();
        }
        int base = seq.offset();
        seq.movePrevious();
        while (seq.moveNext() && nonRelevant.contains((Object)seq.token().id())) {
            switch ((JavaTokenId)seq.token().id()) {
                case LINE_COMMENT: {
                    seq.moveNext();
                    return seq.offset();
                }
                case WHITESPACE: {
                    char[] c = seq.token().text().toString().toCharArray();
                    int index = 0;
                    while (index < c.length) {
                        if (c[index++] != '\n') continue;
                        return base + index;
                    }
                    break;
                }
            }
        }
        return base;
    }

    private static int goAfterLastNewLine(TokenSequence<JavaTokenId> seq) {
        int base = seq.offset();
        seq.movePrevious();
        while (seq.moveNext() && nonRelevant.contains((Object)seq.token().id())) {
        }
        while (seq.movePrevious() && nonRelevant.contains((Object)seq.token().id())) {
            switch ((JavaTokenId)seq.token().id()) {
                case LINE_COMMENT: {
                    seq.moveNext();
                    return seq.offset();
                }
                case WHITESPACE: {
                    char[] c = seq.token().text().toString().toCharArray();
                    int i = c.length;
                    while (i > 0) {
                        if (c[--i] != '\n') continue;
                        return seq.offset() + i + 1;
                    }
                    break;
                }
            }
        }
        if ((seq.index() == 0 || seq.moveNext()) && nonRelevant.contains((Object)seq.token().id())) {
            return seq.offset();
        }
        return base;
    }

    public static final boolean isSeparator(JavaTokenId id) {
        return "separator".equals(id.primaryCategory());
    }

    static int moveFwdToToken(TokenSequence<JavaTokenId> tokenSequence, int pos, JavaTokenId id) {
        tokenSequence.move(pos);
        tokenSequence.moveNext();
        while (!id.equals((Object)tokenSequence.token().id())) {
            if (tokenSequence.moveNext()) continue;
            return -1;
        }
        return tokenSequence.offset();
    }

    static JavaTokenId moveFwdToOneOfTokens(TokenSequence<JavaTokenId> tokenSequence, int pos, EnumSet<JavaTokenId> ids) {
        tokenSequence.move(pos);
        tokenSequence.moveNext();
        while (!ids.contains((Object)tokenSequence.token().id())) {
            if (tokenSequence.moveNext()) continue;
            return null;
        }
        return (JavaTokenId)tokenSequence.token().id();
    }

    static int moveBackToToken(TokenSequence<JavaTokenId> tokenSequence, int pos, JavaTokenId id) {
        tokenSequence.move(pos);
        tokenSequence.moveNext();
        while (!id.equals((Object)tokenSequence.token().id())) {
            if (tokenSequence.movePrevious()) continue;
            return -1;
        }
        return tokenSequence.offset();
    }

    public static enum Direction {
        FORWARD,
        BACKWARD;
        

        private Direction() {
        }
    }

    static class TopLevelEstimator
    extends PositionEstimator {
        private List<int[]> data;

        public TopLevelEstimator(List<? extends Tree> oldL, List<? extends Tree> newL, DiffContext diffContext) {
            super(oldL, newL, diffContext);
        }

        @Override
        public void initialize() {
            int size = this.oldL.size();
            this.data = new ArrayList<int[]>(size);
            SourcePositions positions = this.diffContext.trees.getSourcePositions();
            JCTree.JCCompilationUnit compilationUnit = this.diffContext.origUnit;
            for (Tree item : this.oldL) {
                Token token;
                int indexOf;
                int treeStart = (int)positions.getStartPosition((CompilationUnitTree)compilationUnit, item);
                int treeEnd = (int)positions.getEndPosition((CompilationUnitTree)compilationUnit, item);
                this.seq.move(treeStart);
                this.seq.moveNext();
                if (null != TopLevelEstimator.moveToSrcRelevant(this.seq, Direction.BACKWARD)) {
                    this.seq.moveNext();
                }
                int previousEnd = this.seq.offset();
                while (nonRelevant.contains((Object)(token = this.seq.token()).id())) {
                    int localResult = -1;
                    switch ((JavaTokenId)token.id()) {
                        case WHITESPACE: {
                            int indexOf2 = token.text().toString().indexOf(10);
                            if (indexOf2 <= -1) break;
                            localResult = this.seq.offset() + indexOf2 + 1;
                            break;
                        }
                        case LINE_COMMENT: {
                            previousEnd = this.seq.offset() + token.text().length();
                            break;
                        }
                        case JAVADOC_COMMENT: {
                            previousEnd = this.seq.offset();
                        }
                    }
                    if (localResult > 0) {
                        previousEnd = localResult;
                        break;
                    }
                    if (this.seq.moveNext()) continue;
                    break;
                }
                int wideStart = previousEnd;
                this.seq.move(treeStart);
                this.seq.moveNext();
                this.seq.movePrevious();
                while (nonRelevant.contains((Object)(token = this.seq.token()).id())) {
                    int localResult = -1;
                    switch ((JavaTokenId)token.id()) {
                        case WHITESPACE: {
                            indexOf = token.text().toString().lastIndexOf(10);
                            if (indexOf <= -1) break;
                            localResult = this.seq.offset() + indexOf + 1;
                            break;
                        }
                        case LINE_COMMENT: {
                            localResult = this.seq.offset() + token.text().length();
                            break;
                        }
                        case BLOCK_COMMENT: 
                        case JAVADOC_COMMENT: {
                            wideStart = this.seq.offset();
                        }
                    }
                    if (wideStart > previousEnd) break;
                    if (localResult > 0) {
                        wideStart = localResult;
                    }
                    if (this.seq.movePrevious()) continue;
                    break;
                }
                this.seq.move(treeEnd);
                int wideEnd = treeEnd;
                while (this.seq.moveNext() && nonRelevant.contains((Object)(token = this.seq.token()).id())) {
                    if (JavaTokenId.WHITESPACE == token.id()) {
                        indexOf = token.text().toString().indexOf(10);
                        wideEnd = indexOf > -1 ? this.seq.offset() + indexOf + 1 : this.seq.offset();
                    } else {
                        if (JavaTokenId.LINE_COMMENT == token.id()) {
                            wideEnd = this.seq.offset() + token.text().length();
                            break;
                        }
                        if (JavaTokenId.JAVADOC_COMMENT == token.id()) break;
                    }
                    if (wideEnd <= treeEnd) continue;
                }
                if (wideEnd < treeEnd) {
                    wideEnd = treeEnd;
                }
                this.data.add(new int[]{wideStart, wideEnd, previousEnd});
            }
            this.initialized = true;
        }

        @Override
        public int getInsertPos(int index) {
            if (!this.initialized) {
                this.initialize();
            }
            if (this.data.isEmpty()) {
                return -1;
            }
            return index == this.data.size() ? this.data.get(index - 1)[2] : this.data.get(index)[0];
        }

        @Override
        public int[] sectionRemovalBounds(StringBuilder replacement) {
            int indexOf;
            Token token;
            if (!this.initialized) {
                this.initialize();
            }
            assert (!this.oldL.isEmpty() && this.newL.isEmpty());
            SourcePositions positions = this.diffContext.trees.getSourcePositions();
            JCTree.JCCompilationUnit compilationUnit = this.diffContext.origUnit;
            int sectionStart = (int)positions.getStartPosition((CompilationUnitTree)compilationUnit, (Tree)this.oldL.get(0));
            int sectionEnd = (int)positions.getEndPosition((CompilationUnitTree)compilationUnit, (Tree)this.oldL.get(this.oldL.size() - 1));
            this.seq.move(sectionStart);
            this.seq.moveNext();
            while (this.seq.movePrevious() && nonRelevant.contains((Object)(token = this.seq.token()).id())) {
                if (JavaTokenId.LINE_COMMENT == token.id()) {
                    this.seq.moveNext();
                    sectionStart = this.seq.offset();
                    break;
                }
                if (JavaTokenId.BLOCK_COMMENT == token.id() || JavaTokenId.JAVADOC_COMMENT == token.id()) break;
                if (JavaTokenId.WHITESPACE != token.id()) continue;
                indexOf = token.text().toString().indexOf(10);
                if (indexOf > -1) {
                    sectionStart = this.seq.offset() + indexOf + 1;
                    continue;
                }
                sectionStart = this.seq.offset();
            }
            this.seq.move(sectionEnd);
            this.seq.movePrevious();
            while (this.seq.moveNext() && nonRelevant.contains((Object)(token = this.seq.token()).id())) {
                if (JavaTokenId.LINE_COMMENT == token.id()) {
                    sectionEnd = this.seq.offset();
                    if (!this.seq.moveNext()) break;
                    sectionEnd = this.seq.offset();
                    break;
                }
                if (JavaTokenId.BLOCK_COMMENT == token.id() || JavaTokenId.JAVADOC_COMMENT == token.id()) break;
                if (JavaTokenId.WHITESPACE != token.id()) continue;
                indexOf = token.text().toString().lastIndexOf(10);
                if (indexOf > -1) {
                    sectionEnd = this.seq.offset() + indexOf + 1;
                    continue;
                }
                sectionEnd += this.seq.offset() + token.text().length();
            }
            return new int[]{sectionStart, sectionEnd};
        }

        @Override
        public String head() {
            return "";
        }

        @Override
        public String sep() {
            return "";
        }

        @Override
        public String getIndentString() {
            return "";
        }

        @Override
        public int[] getPositions(int index) {
            if (!this.initialized) {
                this.initialize();
            }
            return (int[])this.data.get(index).clone();
        }

        @Override
        public CasualDiff.LineInsertionType lineInsertType() {
            return CasualDiff.LineInsertionType.AFTER;
        }

        @Override
        public int prepare(int startPos, StringBuilder aHead, StringBuilder aTail) {
            this.seq.moveEnd();
            if (this.seq.movePrevious()) {
                if (JavaTokenId.WHITESPACE == this.seq.token().id()) {
                    int firstNewLineIndex = -1;
                    String tokenText = this.seq.token().text().toString();
                    firstNewLineIndex = tokenText.indexOf(10);
                    if (firstNewLineIndex > -1) {
                        if (tokenText.lastIndexOf(10) == firstNewLineIndex) {
                            aHead.append('\n');
                        }
                    } else {
                        aHead.append("\n\n");
                    }
                } else if (JavaTokenId.LINE_COMMENT != this.seq.token().id()) {
                    aHead.append("\n\n");
                }
                return this.seq.offset() + this.seq.token().text().length();
            }
            return startPos;
        }

        public String toString() {
            if (!this.initialized) {
                this.initialize();
            }
            String result = "";
            for (int i = 0; i < this.data.size(); ++i) {
                int[] pos = this.data.get(i);
                String s = this.diffContext.origText.substring(pos[0], pos[1]);
                result = result + "[" + s + "]";
            }
            return result;
        }
    }

    static class CatchesEstimator
    extends PositionEstimator {
        private final boolean hasFinally;
        private List<int[]> data;

        public CatchesEstimator(List<? extends Tree> oldL, List<? extends Tree> newL, boolean hasFinally, DiffContext diffContext) {
            super(oldL, newL, diffContext);
            this.hasFinally = hasFinally;
        }

        @Override
        public void initialize() {
            int size = this.oldL.size();
            this.data = new ArrayList<int[]>(size);
            SourcePositions positions = this.diffContext.trees.getSourcePositions();
            JCTree.JCCompilationUnit compilationUnit = this.diffContext.origUnit;
            for (Tree item : this.oldL) {
                Token token;
                int treeStart = (int)positions.getStartPosition((CompilationUnitTree)compilationUnit, item);
                int treeEnd = (int)positions.getEndPosition((CompilationUnitTree)compilationUnit, item);
                this.seq.move(treeStart);
                this.seq.moveNext();
                if (null != CatchesEstimator.moveToSrcRelevant(this.seq, Direction.BACKWARD)) {
                    this.seq.moveNext();
                }
                int previousEnd = this.seq.offset();
                while (nonRelevant.contains((Object)(token = this.seq.token()).id())) {
                    int localResult = -1;
                    switch ((JavaTokenId)token.id()) {
                        case WHITESPACE: {
                            int indexOf = token.text().toString().indexOf(10);
                            if (indexOf <= -1) break;
                            localResult = this.seq.offset() + indexOf;
                            break;
                        }
                        case LINE_COMMENT: {
                            previousEnd = this.seq.offset() + token.text().length();
                        }
                    }
                    if (localResult > 0) {
                        previousEnd = localResult;
                        break;
                    }
                    if (this.seq.moveNext()) continue;
                    break;
                }
                this.data.add(new int[]{previousEnd, treeEnd, previousEnd});
            }
            this.initialized = true;
        }

        @Override
        public int getInsertPos(int index) {
            if (!this.initialized) {
                this.initialize();
            }
            if (this.data.isEmpty()) {
                return -1;
            }
            return index == this.data.size() ? this.data.get(index - 1)[2] : this.data.get(index)[0];
        }

        @Override
        public int[] sectionRemovalBounds(StringBuilder replacement) {
            Token token;
            if (!this.initialized) {
                this.initialize();
            }
            assert (!this.oldL.isEmpty() && this.newL.isEmpty());
            SourcePositions positions = this.diffContext.trees.getSourcePositions();
            JCTree.JCCompilationUnit compilationUnit = this.diffContext.origUnit;
            int sectionStart = (int)positions.getStartPosition((CompilationUnitTree)compilationUnit, (Tree)this.oldL.get(0));
            int sectionEnd = (int)positions.getEndPosition((CompilationUnitTree)compilationUnit, (Tree)this.oldL.get(this.oldL.size() - 1));
            this.seq.move(sectionStart);
            this.seq.moveNext();
            while (this.seq.movePrevious() && nonRelevant.contains((Object)(token = this.seq.token()).id())) {
                if (JavaTokenId.LINE_COMMENT == token.id()) {
                    this.seq.moveNext();
                    sectionStart = this.seq.offset();
                    break;
                }
                if (JavaTokenId.BLOCK_COMMENT == token.id() || JavaTokenId.JAVADOC_COMMENT == token.id()) break;
                if (JavaTokenId.WHITESPACE != token.id()) continue;
                int indexOf = token.text().toString().indexOf(10);
                if (indexOf > -1) {
                    sectionStart = this.seq.offset() + indexOf + 1;
                    continue;
                }
                sectionStart = this.seq.offset();
            }
            this.seq.move(sectionEnd);
            int wideEnd = sectionEnd;
            token = null;
            this.seq.movePrevious();
            while (this.seq.moveNext() && nonRelevant.contains((Object)(token = this.seq.token()).id())) {
                if (JavaTokenId.LINE_COMMENT == token.id()) {
                    wideEnd = this.seq.offset();
                    if (!this.seq.moveNext()) break;
                    wideEnd = this.seq.offset();
                    break;
                }
                if (JavaTokenId.BLOCK_COMMENT == token.id() || JavaTokenId.JAVADOC_COMMENT == token.id()) break;
                if (JavaTokenId.WHITESPACE != token.id()) continue;
                int indexOf = token.text().toString().lastIndexOf(10);
                if (indexOf > -1) {
                    wideEnd = this.seq.offset() + indexOf + 1;
                    continue;
                }
                wideEnd = this.seq.offset() + token.text().length();
            }
            int[] arrn = new int[2];
            arrn[0] = sectionStart;
            arrn[1] = token != null && token.id() != JavaTokenId.FINALLY ? wideEnd : sectionEnd;
            return arrn;
        }

        @Override
        public String head() {
            return "";
        }

        @Override
        public String sep() {
            return "";
        }

        @Override
        public String getIndentString() {
            return "";
        }

        @Override
        public int[] getPositions(int index) {
            if (!this.initialized) {
                this.initialize();
            }
            return (int[])this.data.get(index).clone();
        }

        @Override
        public int prepare(int startPos, StringBuilder aHead, StringBuilder aTail) {
            if (!this.hasFinally) {
                return startPos;
            }
            this.seq.move(startPos);
            this.seq.moveNext();
            CatchesEstimator.moveToSrcRelevant(this.seq, Direction.BACKWARD);
            while (this.seq.moveNext() && nonRelevant.contains((Object)this.seq.token().id())) {
                if (JavaTokenId.WHITESPACE == this.seq.token().id()) {
                    int newlineInToken = this.seq.token().text().toString().indexOf(10);
                    if (newlineInToken <= -1) continue;
                    return this.seq.offset() + newlineInToken + 1;
                }
                if (JavaTokenId.LINE_COMMENT != this.seq.token().id()) continue;
                return this.seq.offset() + this.seq.token().text().length();
            }
            return startPos;
        }

        public String toString() {
            if (!this.initialized) {
                this.initialize();
            }
            String result = "";
            for (int i = 0; i < this.data.size(); ++i) {
                int[] pos = this.data.get(i);
                String s = this.diffContext.origText.substring(pos[0], pos[1]);
                result = result + "[" + s + "]";
            }
            return result;
        }
    }

    static class MembersEstimator
    extends PositionEstimator {
        private List<int[]> data;
        private List<String> append;
        private int minimalLeftPosition;
        private final boolean skipTrailingSemicolons;

        public MembersEstimator(List<? extends Tree> oldL, List<? extends Tree> newL, DiffContext diffContext, boolean skipTrailingSemicolons) {
            super(oldL, newL, diffContext);
            this.minimalLeftPosition = -1;
            this.skipTrailingSemicolons = skipTrailingSemicolons;
        }

        public MembersEstimator(List<? extends Tree> oldL, List<? extends Tree> newL, int minimalLeftPosition, DiffContext diffContext, boolean skipTrailingSemicolons) {
            super(oldL, newL, diffContext);
            this.minimalLeftPosition = minimalLeftPosition;
            this.skipTrailingSemicolons = skipTrailingSemicolons;
        }

        @Override
        public void initialize() {
            int size = this.oldL.size();
            this.data = new ArrayList<int[]>(size);
            this.append = new ArrayList<String>(size);
            SourcePositions positions = this.diffContext.trees.getSourcePositions();
            JCTree.JCCompilationUnit compilationUnit = this.diffContext.origUnit;
            boolean first = true;
            for (Tree item : this.oldL) {
                boolean nlBefore;
                Token token;
                Pair comment;
                int treeStart = (int)positions.getStartPosition((CompilationUnitTree)compilationUnit, item);
                int treeEnd = (int)positions.getEndPosition((CompilationUnitTree)compilationUnit, item);
                if (item instanceof FieldGroupTree) {
                    FieldGroupTree fgt = (FieldGroupTree)item;
                    List<JCTree.JCVariableDecl> vars = fgt.getVariables();
                    treeEnd = (int)positions.getEndPosition((CompilationUnitTree)compilationUnit, (Tree)vars.get(vars.size() - 1));
                } else {
                    this.seq.move(treeEnd);
                    if (this.seq.movePrevious() && nonRelevant.contains((Object)this.seq.token().id())) {
                        MembersEstimator.moveToSrcRelevant(this.seq, Direction.BACKWARD);
                        this.seq.moveNext();
                        treeEnd = this.seq.offset();
                    }
                }
                String itemAppend = "";
                int appendInsertPos = -1;
                if (this.isEnum(item)) {
                    this.seq.move(treeEnd);
                    MembersEstimator.moveToSrcRelevant(this.seq, Direction.FORWARD);
                    if (JavaTokenId.COMMA == this.seq.token().id()) {
                        treeEnd = this.seq.offset() + this.seq.token().length();
                        MembersEstimator.moveToSrcRelevant(this.seq, Direction.FORWARD);
                    }
                    if (JavaTokenId.SEMICOLON == this.seq.token().id()) {
                        this.seq.moveNext();
                    } else {
                        itemAppend = ";";
                        appendInsertPos = treeEnd;
                    }
                    treeEnd = this.seq.offset();
                }
                this.seq.move(treeStart);
                this.seq.moveNext();
                if (null != MembersEstimator.moveToSrcRelevant(this.seq, Direction.BACKWARD)) {
                    this.seq.moveNext();
                }
                int previousEnd = this.seq.offset();
                int wsOnlyStart = -1;
                int localResult = -1;
                int insertPos = -1;
                boolean bl = nlBefore = item.getKind() != Tree.Kind.EMPTY_STATEMENT;
                while (nonRelevant.contains((Object)(token = this.seq.token()).id())) {
                    switch ((JavaTokenId)token.id()) {
                        case WHITESPACE: {
                            int indexOf = token.text().toString().lastIndexOf(10);
                            if (indexOf > -1) {
                                insertPos = this.seq.offset() + token.text().toString().indexOf(10) + 1;
                                localResult = this.seq.offset() + indexOf + 1;
                                nlBefore = true;
                                break;
                            }
                            if (first || previousEnd == 0) {
                                wsOnlyStart = previousEnd;
                                break;
                            }
                            if (token.length() <= 1) break;
                            wsOnlyStart = this.seq.offset() + token.length() - 1;
                            break;
                        }
                        case LINE_COMMENT: 
                        case BLOCK_COMMENT: 
                        case JAVADOC_COMMENT: {
                            localResult = this.seq.offset();
                        }
                    }
                    if (localResult > 0) {
                        previousEnd = localResult;
                        break;
                    }
                    if (this.seq.moveNext()) continue;
                }
                if (localResult == -1) {
                    if (wsOnlyStart >= 0) {
                        previousEnd = wsOnlyStart;
                    }
                    localResult = this.seq.offset();
                }
                first = false;
                if (this.minimalLeftPosition != -1 && this.minimalLeftPosition > previousEnd) {
                    previousEnd = this.minimalLeftPosition;
                    insertPos = localResult = this.minimalLeftPosition;
                }
                if (insertPos == -1) {
                    insertPos = localResult;
                }
                this.seq.move(treeEnd);
                int wideEnd = treeEnd;
                LinkedList<Pair> commentEndPos = new LinkedList<Pair>();
                int maxLines = 0;
                int newlines = 0;
                boolean cont = true;
                block12 : while (cont && this.seq.moveNext()) {
                    Token t = this.seq.token();
                    switch ((JavaTokenId)t.id()) {
                        case WHITESPACE: {
                            if (newlines == 0) {
                                int indexOf = t.text().toString().indexOf(10);
                                if (indexOf > -1) {
                                    if (commentEndPos.isEmpty()) {
                                        wideEnd = this.seq.offset() + indexOf + (nlBefore ? 1 : 0);
                                    } else {
                                        commentEndPos.add(Pair.of((Object)((Pair)commentEndPos.getLast()).first(), (Object)(this.seq.offset() + indexOf + 1)));
                                    }
                                } else {
                                    wideEnd = t.length() > 1 ? this.seq.offset() + t.length() - 1 : this.seq.offset() + t.length();
                                }
                            }
                            newlines += this.numberOfNL(t);
                            continue block12;
                        }
                        case LINE_COMMENT: 
                        case BLOCK_COMMENT: {
                            if (this.seq.offset() > this.minimalLeftPosition) {
                                commentEndPos.add(Pair.of((Object)newlines, (Object)(this.seq.offset() + t.text().length())));
                            }
                            maxLines = Math.max(maxLines, newlines);
                            if (t.id() == JavaTokenId.LINE_COMMENT) {
                                newlines = 1;
                                continue block12;
                            }
                            newlines = 0;
                            continue block12;
                        }
                        case SEMICOLON: {
                            if (this.skipTrailingSemicolons) {
                                wideEnd = this.seq.offset() + t.text().length();
                                continue block12;
                            }
                            cont = false;
                            continue block12;
                        }
                        case RBRACE: {
                            maxLines = Integer.MAX_VALUE;
                        }
                    }
                    cont = false;
                }
                maxLines = Math.max(maxLines, newlines);
                Iterator i$ = commentEndPos.iterator();
                while (i$.hasNext() && ((Integer)(comment = (Pair)i$.next()).first() < maxLines || (Integer)comment.first() == 0)) {
                    wideEnd = (Integer)comment.second();
                }
                if (wideEnd < treeEnd) {
                    wideEnd = treeEnd;
                }
                if (this.minimalLeftPosition < wideEnd) {
                    this.minimalLeftPosition = wideEnd;
                }
                this.data.add(new int[]{previousEnd, wideEnd, previousEnd, appendInsertPos, insertPos});
                this.append.add(itemAppend);
            }
            this.initialized = true;
        }

        private boolean isEnum(Tree tree) {
            if (tree instanceof FieldGroupTree) {
                return ((FieldGroupTree)tree).isEnum();
            }
            if (tree instanceof VariableTree) {
                return (((JCTree.JCVariableDecl)tree).getModifiers().flags & 16384) != 0;
            }
            return false;
        }

        private int numberOfNL(Token<JavaTokenId> t) {
            int count = 0;
            CharSequence charSequence = t.text();
            for (int i = 0; i < charSequence.length(); ++i) {
                char a = charSequence.charAt(i);
                if ('\n' != a) continue;
                ++count;
            }
            return count;
        }

        @Override
        public int getInsertPos(int index) {
            if (!this.initialized) {
                this.initialize();
            }
            if (this.data.isEmpty()) {
                return -1;
            }
            int pos = index == this.data.size() ? this.data.get(index - 1)[1] : this.data.get(index)[4];
            return this.moveBelowGuarded(pos);
        }

        @Override
        public int[] sectionRemovalBounds(StringBuilder replacement) {
            Token token;
            if (!this.initialized) {
                this.initialize();
            }
            assert (!this.oldL.isEmpty() && this.newL.isEmpty());
            SourcePositions positions = this.diffContext.trees.getSourcePositions();
            JCTree.JCCompilationUnit compilationUnit = this.diffContext.origUnit;
            int sectionStart = (int)positions.getStartPosition((CompilationUnitTree)compilationUnit, (Tree)this.oldL.get(0));
            int sectionEnd = (int)positions.getEndPosition((CompilationUnitTree)compilationUnit, (Tree)this.oldL.get(this.oldL.size() - 1));
            this.seq.move(sectionStart);
            this.seq.moveNext();
            while (this.seq.movePrevious() && nonRelevant.contains((Object)(token = this.seq.token()).id())) {
                if (JavaTokenId.LINE_COMMENT == token.id() || JavaTokenId.BLOCK_COMMENT == token.id() || JavaTokenId.JAVADOC_COMMENT == token.id() || JavaTokenId.WHITESPACE != token.id()) continue;
                int indexOf = token.text().toString().indexOf(10);
                if (indexOf > -1) {
                    sectionStart = this.seq.offset() + indexOf + 1;
                    continue;
                }
                sectionStart = this.seq.offset();
            }
            this.seq.move(sectionEnd);
            this.seq.movePrevious();
            boolean moreWhitespaces = false;
            int lastNewline = -1;
            while (this.seq.moveNext() && nonRelevant.contains((Object)(token = this.seq.token()).id())) {
                boolean mapped;
                if (JavaTokenId.LINE_COMMENT == token.id()) {
                    sectionEnd = this.seq.offset();
                    boolean bl = mapped = this.diffContext.usedComments.get(sectionEnd) != null;
                    if (!mapped) {
                        if (moreWhitespaces || !this.seq.moveNext()) break;
                        sectionEnd = this.seq.offset();
                        break;
                    }
                    lastNewline = sectionEnd = this.seq.offset() + this.seq.token().length();
                    continue;
                }
                if (JavaTokenId.BLOCK_COMMENT == token.id() || JavaTokenId.JAVADOC_COMMENT == token.id()) {
                    boolean bl = mapped = this.diffContext.usedComments.get(sectionEnd) != null;
                    if (!mapped) break;
                    lastNewline = -1;
                    continue;
                }
                if (JavaTokenId.WHITESPACE != token.id()) continue;
                int indexOf = token.text().toString().lastIndexOf(10);
                int after = this.seq.offset() + token.text().length();
                if (indexOf > -1) {
                    sectionEnd = this.seq.offset() + indexOf + 1;
                    moreWhitespaces |= token.text().toString().indexOf(10) != indexOf;
                    lastNewline = sectionEnd;
                    continue;
                }
                if (lastNewline != -1) continue;
                sectionEnd = after;
            }
            return new int[]{sectionStart, sectionEnd};
        }

        @Override
        public String head() {
            return "";
        }

        @Override
        public String sep() {
            return "";
        }

        @Override
        public String getIndentString() {
            return "";
        }

        @Override
        public String append(int index) {
            return this.append.get(index);
        }

        @Override
        public int[] getPositions(int index) {
            if (!this.initialized) {
                this.initialize();
            }
            return (int[])this.data.get(index).clone();
        }

        @Override
        public CasualDiff.LineInsertionType lineInsertType() {
            return CasualDiff.LineInsertionType.AFTER;
        }

        @Override
        public int prepare(int startPos, StringBuilder aHead, StringBuilder aTail) {
            this.seq.move(startPos);
            this.seq.moveNext();
            MembersEstimator.moveToSrcRelevant(this.seq, Direction.BACKWARD);
            while (this.seq.moveNext() && nonRelevant.contains((Object)this.seq.token().id())) {
                if (JavaTokenId.WHITESPACE == this.seq.token().id()) {
                    int newlineInToken = this.seq.token().text().toString().indexOf(10);
                    if (newlineInToken <= -1) continue;
                    return this.seq.offset() + newlineInToken + 1;
                }
                if (JavaTokenId.LINE_COMMENT != this.seq.token().id()) continue;
                return this.seq.offset() + this.seq.token().text().length();
            }
            return startPos;
        }

        public String toString() {
            boolean inited = this.initialized;
            int spos = this.seq.offset();
            if (!inited) {
                this.initialize();
            }
            String result = "";
            for (int i = 0; i < this.data.size(); ++i) {
                int[] pos = this.data.get(i);
                String s = this.diffContext.origText.substring(pos[0], pos[1]);
                result = result + "[" + s + "]";
            }
            this.seq.move(spos);
            this.seq.moveNext();
            this.initialized = inited;
            return result;
        }
    }

    private static abstract class BaseEstimator
    extends PositionEstimator {
        JavaTokenId precToken;
        private ArrayList<String> separatorList;

        private BaseEstimator(JavaTokenId precToken, List<? extends Tree> oldL, List<? extends Tree> newL, DiffContext diffContext) {
            super(oldL, newL, diffContext);
            this.precToken = precToken;
        }

        @Override
        public String head() {
            return " " + this.precToken.fixedText() + " ";
        }

        @Override
        public String sep() {
            return ", ";
        }

        @Override
        public void initialize() {
            this.separatorList = new ArrayList(this.oldL.size());
            boolean first = true;
            int size = this.oldL.size();
            this.matrix = new int[size + 1][5];
            this.matrix[size] = new int[]{-1, -1, -1, -1, -1};
            int i = 0;
            SourcePositions positions = this.diffContext.trees.getSourcePositions();
            JCTree.JCCompilationUnit compilationUnit = this.diffContext.origUnit;
            for (Tree item : this.oldL) {
                String separatedText = "";
                int treeStart = (int)positions.getStartPosition((CompilationUnitTree)compilationUnit, item);
                int treeEnd = (int)positions.getEndPosition((CompilationUnitTree)compilationUnit, item);
                this.seq.move(treeStart);
                int startIndex = this.seq.index();
                int beforer = -1;
                if (first) {
                    while (this.seq.movePrevious() && this.seq.token().id() != this.precToken) {
                    }
                    int throwsIndex = this.seq.index();
                    beforer = throwsIndex + 1;
                    BaseEstimator.moveToSrcRelevant(this.seq, Direction.BACKWARD);
                    this.seq.moveNext();
                    int beg = this.seq.index();
                    this.seq.move(treeEnd);
                    this.matrix[i++] = new int[]{beg, throwsIndex, beforer, startIndex, this.seq.index()};
                    first = false;
                } else {
                    int afterPrevious = this.matrix[i - 1][4];
                    while (this.seq.movePrevious() && this.seq.token().id() != JavaTokenId.COMMA) {
                        if (this.seq.token().id() == JavaTokenId.WHITESPACE) {
                            separatedText = this.seq.token().text() + separatedText;
                            continue;
                        }
                        if (this.seq.token().id() != JavaTokenId.LINE_COMMENT) continue;
                        separatedText = "" + '\n' + separatedText;
                    }
                    this.separatorList.add(separatedText);
                    int separator = this.seq.index();
                    int afterSeparator = separator + 1;
                    if (afterPrevious == separator) {
                        afterPrevious = -1;
                    }
                    this.seq.move(treeEnd);
                    this.matrix[i++] = new int[]{afterPrevious, separator, afterSeparator, startIndex, this.seq.index()};
                }
                if (i == size) {
                    BaseEstimator.moveToSrcRelevant(this.seq, Direction.FORWARD);
                    this.matrix[i][2] = this.seq.index();
                }
                this.seq.move(treeEnd);
            }
            this.initialized = true;
        }

        @Override
        public String getIndentString() {
            if (!this.initialized) {
                this.initialize();
            }
            HashMap<String, Integer> map = new HashMap<String, Integer>();
            for (String item : this.separatorList) {
                Integer count;
                String s = item;
                if (s.lastIndexOf("\n") > -1) {
                    s = s.substring(item.lastIndexOf("\n"));
                }
                if ((count = (Integer)map.get(s)) != null) {
                    Integer n = count;
                    Integer n2 = count = Integer.valueOf(count + 1);
                    map.put(s, n);
                    continue;
                }
                map.put(s, 1);
            }
            int max = -1;
            String s = null;
            for (String item2 : map.keySet()) {
                if ((Integer)map.get(item2) <= max) continue;
                s = item2;
                max = (Integer)map.get(item2);
            }
            return s;
        }

        @Override
        public int getInsertPos(int index) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public int[] getPositions(int index) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public int prepare(int startPos, StringBuilder aHead, StringBuilder aTail) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public int[] sectionRemovalBounds(StringBuilder replacement) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    static class AnnotationsEstimator
    extends PositionEstimator {
        public AnnotationsEstimator(List<? extends Tree> oldL, List<? extends Tree> newL, DiffContext diffContext) {
            super(oldL, newL, diffContext);
        }

        @Override
        public void initialize() {
            int size = this.oldL.size();
            this.matrix = new int[size + 1][5];
            this.matrix[size] = new int[]{-1, -1, -1, -1, -1};
            SourcePositions positions = this.diffContext.trees.getSourcePositions();
            JCTree.JCCompilationUnit compilationUnit = this.diffContext.origUnit;
            int i = 0;
            for (Tree item : this.oldL) {
                int treeStart = (int)positions.getStartPosition((CompilationUnitTree)compilationUnit, item);
                int treeEnd = (int)positions.getEndPosition((CompilationUnitTree)compilationUnit, item);
                if (treeEnd < 0) continue;
                this.seq.move(treeStart);
                int startIndex = this.seq.index();
                AnnotationsEstimator.moveToSrcRelevant(this.seq, Direction.BACKWARD);
                this.seq.moveNext();
                int veryBeg = this.seq.index();
                this.seq.move(treeEnd);
                this.matrix[i++] = new int[]{veryBeg, veryBeg, veryBeg, startIndex, this.seq.index()};
                if (i != size) continue;
                this.seq.move(treeEnd);
                this.matrix[i][2] = this.seq.index();
            }
            this.initialized = true;
        }

        @Override
        public int getInsertPos(int index) {
            int tokenIndex;
            if (!this.initialized) {
                this.initialize();
            }
            if ((tokenIndex = this.matrix[index][2]) == -1) {
                return -1;
            }
            this.seq.moveIndex(tokenIndex);
            this.seq.moveNext();
            int off = PositionEstimator.goAfterLastNewLine(this.seq);
            return off;
        }

        @Override
        public String head() {
            return "";
        }

        @Override
        public String sep() {
            return "";
        }

        @Override
        public String getIndentString() {
            return "";
        }

        @Override
        public int[] getPositions(int index) {
            if (!this.initialized) {
                this.initialize();
            }
            int begin = this.getInsertPos(index);
            if (this.matrix[index][4] != -1) {
                this.seq.moveIndex(this.matrix[index][4]);
                this.seq.moveNext();
            }
            int end = PositionEstimator.goAfterFirstNewLine(this.seq);
            return new int[]{begin, end};
        }

        @Override
        public CasualDiff.LineInsertionType lineInsertType() {
            return CasualDiff.LineInsertionType.AFTER;
        }

        @Override
        public int prepare(int startPos, StringBuilder aHead, StringBuilder aTail) {
            return startPos;
        }

        @Override
        public int[] sectionRemovalBounds(StringBuilder replacement) {
            int indexOf;
            Token token;
            if (!this.initialized) {
                this.initialize();
            }
            assert (!this.oldL.isEmpty() && this.newL.isEmpty());
            SourcePositions positions = this.diffContext.trees.getSourcePositions();
            JCTree.JCCompilationUnit compilationUnit = this.diffContext.origUnit;
            int sectionStart = (int)positions.getStartPosition((CompilationUnitTree)compilationUnit, (Tree)this.oldL.get(0));
            int sectionEnd = (int)positions.getEndPosition((CompilationUnitTree)compilationUnit, (Tree)this.oldL.get(this.oldL.size() - 1));
            this.seq.move(sectionStart);
            this.seq.moveNext();
            int fullLineSectionStart = -1;
            while (this.seq.movePrevious() && nonRelevant.contains((Object)(token = this.seq.token()).id())) {
                if (JavaTokenId.LINE_COMMENT == token.id()) {
                    this.seq.moveNext();
                    sectionStart = this.seq.offset();
                    break;
                }
                if (JavaTokenId.BLOCK_COMMENT == token.id() || JavaTokenId.JAVADOC_COMMENT == token.id()) break;
                if (JavaTokenId.WHITESPACE != token.id()) continue;
                indexOf = token.text().toString().lastIndexOf(10);
                if (indexOf > -1) {
                    fullLineSectionStart = this.seq.offset() + indexOf + 1;
                    continue;
                }
                fullLineSectionStart = this.seq.offset();
            }
            this.seq.move(sectionEnd);
            this.seq.movePrevious();
            while (this.seq.moveNext() && nonRelevant.contains((Object)(token = this.seq.token()).id())) {
                if (JavaTokenId.LINE_COMMENT == token.id()) {
                    sectionEnd = this.seq.offset();
                    if (!this.seq.moveNext()) break;
                    sectionEnd = this.seq.offset();
                    break;
                }
                if (JavaTokenId.BLOCK_COMMENT == token.id() || JavaTokenId.JAVADOC_COMMENT == token.id()) break;
                if (JavaTokenId.WHITESPACE != token.id()) continue;
                indexOf = token.text().toString().lastIndexOf(10);
                if (indexOf > -1) {
                    sectionEnd = this.seq.offset() + indexOf + 1;
                    if (fullLineSectionStart <= -1) continue;
                    sectionStart = fullLineSectionStart;
                    continue;
                }
                sectionEnd = this.seq.offset() + token.text().length();
            }
            return new int[]{sectionStart, sectionEnd};
        }
    }

    static class CasesEstimator
    extends PositionEstimator {
        private List<int[]> data;

        public CasesEstimator(List<? extends Tree> oldL, List<? extends Tree> newL, DiffContext diffContext) {
            super(oldL, newL, diffContext);
        }

        @Override
        public void initialize() {
            int size = this.oldL.size();
            this.data = new ArrayList<int[]>(size);
            SourcePositions positions = this.diffContext.trees.getSourcePositions();
            JCTree.JCCompilationUnit compilationUnit = this.diffContext.origUnit;
            for (Tree item : this.oldL) {
                Token token;
                int indexOf;
                int treeStart = (int)positions.getStartPosition((CompilationUnitTree)compilationUnit, item);
                int treeEnd = (int)positions.getEndPosition((CompilationUnitTree)compilationUnit, item);
                this.seq.move(treeStart);
                this.seq.moveNext();
                if (null != CasesEstimator.moveToSrcRelevant(this.seq, Direction.BACKWARD)) {
                    this.seq.moveNext();
                }
                int previousEnd = this.seq.offset();
                while (nonRelevant.contains((Object)(token = this.seq.token()).id())) {
                    int localResult = -1;
                    switch ((JavaTokenId)token.id()) {
                        case WHITESPACE: {
                            indexOf = token.text().toString().indexOf(10);
                            if (indexOf <= -1) break;
                            localResult = this.seq.offset() + indexOf + 1;
                            break;
                        }
                        case LINE_COMMENT: {
                            previousEnd = this.seq.offset() + token.text().length();
                        }
                    }
                    if (localResult >= 0) {
                        previousEnd = localResult;
                        break;
                    }
                    if (this.seq.moveNext()) continue;
                    break;
                }
                this.seq.move(treeEnd);
                int wideEnd = treeEnd;
                while (this.seq.moveNext() && nonRelevant.contains((Object)(token = this.seq.token()).id())) {
                    if (JavaTokenId.WHITESPACE == token.id()) {
                        indexOf = token.text().toString().indexOf(10);
                        wideEnd = indexOf > -1 ? this.seq.offset() + indexOf + 1 : this.seq.offset();
                    } else {
                        if (JavaTokenId.LINE_COMMENT == token.id()) {
                            wideEnd = this.seq.offset() + token.text().length();
                            break;
                        }
                        if (JavaTokenId.JAVADOC_COMMENT == token.id()) break;
                    }
                    if (wideEnd <= treeEnd) continue;
                }
                if (wideEnd < treeEnd) {
                    wideEnd = treeEnd;
                }
                this.data.add(new int[]{previousEnd, wideEnd, previousEnd});
            }
            this.initialized = true;
        }

        @Override
        public int getInsertPos(int index) {
            if (!this.initialized) {
                this.initialize();
            }
            if (this.data.isEmpty()) {
                return -1;
            }
            return index == this.data.size() ? this.data.get(index - 1)[2] : this.data.get(index)[0];
        }

        @Override
        public int[] sectionRemovalBounds(StringBuilder replacement) {
            int indexOf;
            Token token;
            if (!this.initialized) {
                this.initialize();
            }
            assert (!this.oldL.isEmpty() && this.newL.isEmpty());
            SourcePositions positions = this.diffContext.trees.getSourcePositions();
            JCTree.JCCompilationUnit compilationUnit = this.diffContext.origUnit;
            int sectionStart = (int)positions.getStartPosition((CompilationUnitTree)compilationUnit, (Tree)this.oldL.get(0));
            int sectionEnd = (int)positions.getEndPosition((CompilationUnitTree)compilationUnit, (Tree)this.oldL.get(this.oldL.size() - 1));
            this.seq.move(sectionStart);
            this.seq.moveNext();
            while (this.seq.movePrevious() && nonRelevant.contains((Object)(token = this.seq.token()).id())) {
                if (JavaTokenId.LINE_COMMENT == token.id()) {
                    this.seq.moveNext();
                    sectionStart = this.seq.offset();
                    break;
                }
                if (JavaTokenId.BLOCK_COMMENT == token.id() || JavaTokenId.JAVADOC_COMMENT == token.id()) break;
                if (JavaTokenId.WHITESPACE != token.id()) continue;
                indexOf = token.text().toString().indexOf(10);
                if (indexOf > -1) {
                    sectionStart = this.seq.offset() + indexOf + 1;
                    continue;
                }
                sectionStart = this.seq.offset();
            }
            this.seq.move(sectionEnd);
            this.seq.movePrevious();
            while (this.seq.moveNext() && nonRelevant.contains((Object)(token = this.seq.token()).id())) {
                if (JavaTokenId.LINE_COMMENT == token.id()) {
                    sectionEnd = this.seq.offset();
                    if (!this.seq.moveNext()) break;
                    sectionEnd = this.seq.offset();
                    break;
                }
                if (JavaTokenId.BLOCK_COMMENT == token.id() || JavaTokenId.JAVADOC_COMMENT == token.id()) break;
                if (JavaTokenId.WHITESPACE != token.id()) continue;
                indexOf = token.text().toString().lastIndexOf(10);
                if (indexOf > -1) {
                    sectionEnd = this.seq.offset() + indexOf + 1;
                    continue;
                }
                sectionEnd += this.seq.offset() + token.text().length();
            }
            return new int[]{sectionStart, sectionEnd};
        }

        @Override
        public String head() {
            return "";
        }

        @Override
        public String sep() {
            return "";
        }

        @Override
        public String getIndentString() {
            return "";
        }

        @Override
        public int[] getPositions(int index) {
            if (!this.initialized) {
                this.initialize();
            }
            return (int[])this.data.get(index).clone();
        }

        @Override
        public int prepare(int startPos, StringBuilder aHead, StringBuilder aTail) {
            this.seq.move(startPos);
            this.seq.moveNext();
            CasesEstimator.moveToSrcRelevant(this.seq, Direction.BACKWARD);
            while (this.seq.moveNext() && nonRelevant.contains((Object)this.seq.token().id())) {
                if (JavaTokenId.WHITESPACE == this.seq.token().id()) {
                    int newlineInToken = this.seq.token().text().toString().indexOf(10);
                    if (newlineInToken <= -1) continue;
                    return this.seq.offset() + newlineInToken + 1;
                }
                if (JavaTokenId.LINE_COMMENT != this.seq.token().id()) continue;
                return this.seq.offset() + this.seq.token().text().length();
            }
            return startPos;
        }

        public String toString() {
            if (!this.initialized) {
                this.initialize();
            }
            String result = "";
            for (int i = 0; i < this.data.size(); ++i) {
                int[] pos = this.data.get(i);
                String s = this.diffContext.origText.substring(pos[0], pos[1]);
                result = result + "[" + s + "]";
            }
            return result;
        }

        @Override
        public CasualDiff.LineInsertionType lineInsertType() {
            return CasualDiff.LineInsertionType.AFTER;
        }
    }

    static class ImportsEstimator
    extends PositionEstimator {
        List<int[]> data;

        public ImportsEstimator(List<? extends ImportTree> oldL, List<? extends ImportTree> newL, DiffContext diffContext) {
            super(oldL, newL, diffContext);
        }

        @Override
        public void initialize() {
            int size = this.oldL.size();
            this.data = new ArrayList<int[]>(size);
            SourcePositions positions = this.diffContext.trees.getSourcePositions();
            JCTree.JCCompilationUnit compilationUnit = this.diffContext.origUnit;
            for (Tree item : this.oldL) {
                Token token;
                int indexOf;
                int treeStart = (int)positions.getStartPosition((CompilationUnitTree)compilationUnit, item);
                int treeEnd = (int)positions.getEndPosition((CompilationUnitTree)compilationUnit, item);
                this.seq.move(treeStart);
                this.seq.moveNext();
                int wideStart = PositionEstimator.goAfterLastNewLine(this.seq);
                this.seq.move(treeStart);
                this.seq.moveNext();
                if (null != ImportsEstimator.moveToSrcRelevant(this.seq, Direction.BACKWARD)) {
                    this.seq.moveNext();
                }
                int previousEnd = this.seq.offset();
                while (nonRelevant.contains((Object)(token = this.seq.token()).id())) {
                    int localResult = -1;
                    switch ((JavaTokenId)token.id()) {
                        case WHITESPACE: {
                            indexOf = token.text().toString().indexOf(10);
                            if (indexOf <= 0) break;
                            localResult = this.seq.offset() + indexOf + 1;
                            break;
                        }
                        case LINE_COMMENT: {
                            previousEnd = this.seq.offset() + token.text().length();
                        }
                    }
                    if (localResult > 0) {
                        previousEnd = localResult;
                        break;
                    }
                    if (this.seq.moveNext()) continue;
                    break;
                }
                this.seq.move(treeEnd);
                int wideEnd = treeEnd;
                while (this.seq.moveNext() && nonRelevant.contains((Object)(token = this.seq.token()).id())) {
                    if (JavaTokenId.WHITESPACE == token.id()) {
                        indexOf = token.text().toString().indexOf(10);
                        if (indexOf > -1) {
                            wideEnd = this.seq.offset() + indexOf + 1;
                            break;
                        }
                        wideEnd = this.seq.offset();
                        continue;
                    }
                    if (JavaTokenId.LINE_COMMENT == token.id()) {
                        wideEnd = this.seq.offset() + token.text().length();
                        break;
                    }
                    if (JavaTokenId.JAVADOC_COMMENT != token.id()) continue;
                }
                if (wideEnd < treeEnd) {
                    wideEnd = treeEnd;
                }
                this.data.add(new int[]{wideStart, wideEnd, previousEnd});
            }
            this.initialized = true;
        }

        @Override
        public int getInsertPos(int index) {
            if (!this.initialized) {
                this.initialize();
            }
            if (this.data.isEmpty()) {
                return -1;
            }
            return index == this.data.size() ? this.data.get(index - 1)[2] : this.data.get(index)[0];
        }

        @Override
        public int prepare(int startPos, StringBuilder aHead, StringBuilder aTail) {
            if (!this.initialized) {
                this.initialize();
            }
            JCTree.JCCompilationUnit cut = this.diffContext.origUnit;
            int resultPos = 0;
            if (cut.getTypeDecls().isEmpty()) {
                return this.diffContext.origText.length();
            }
            Tree t = (Tree)cut.getTypeDecls().get(0);
            SourcePositions positions = this.diffContext.trees.getSourcePositions();
            int typeDeclStart = (int)positions.getStartPosition((CompilationUnitTree)cut, t);
            this.seq.move(typeDeclStart);
            if (null == ImportsEstimator.moveToSrcRelevant(this.seq, Direction.BACKWARD)) {
                return 0;
            }
            resultPos = this.seq.offset() + this.seq.token().length();
            int counter = 0;
            while (this.seq.moveNext() && nonRelevant.contains((Object)this.seq.token().id()) && counter < 3) {
                if (JavaTokenId.WHITESPACE == this.seq.token().id()) {
                    String white = this.seq.token().text().toString();
                    int index = 0;
                    int pos = 0;
                    while ((pos = white.indexOf(10, pos)) > -1) {
                        ++pos;
                        if (++counter >= 3) continue;
                        index = pos;
                    }
                    resultPos += index;
                    continue;
                }
                if (JavaTokenId.LINE_COMMENT == this.seq.token().id()) {
                    ++counter;
                    resultPos += this.seq.token().text().toString().length();
                    continue;
                }
                if (JavaTokenId.BLOCK_COMMENT != this.seq.token().id() && JavaTokenId.JAVADOC_COMMENT != this.seq.token().id()) continue;
            }
            return resultPos;
        }

        @Override
        public int[] getPositions(int index) {
            if (!this.initialized) {
                this.initialize();
            }
            return (int[])this.data.get(index).clone();
        }

        @Override
        public CasualDiff.LineInsertionType lineInsertType() {
            return CasualDiff.LineInsertionType.AFTER;
        }

        @Override
        public String head() {
            throw new UnsupportedOperationException("Not applicable for imports!");
        }

        @Override
        public String sep() {
            throw new UnsupportedOperationException("Not applicable for imports!");
        }

        @Override
        public String getIndentString() {
            throw new UnsupportedOperationException("Not applicable for imports!");
        }

        public String toString() {
            String result = "";
            for (int i = 0; i < this.data.size(); ++i) {
                int[] pos = this.data.get(i);
                String s = this.diffContext.origText.substring(pos[0], pos[1]);
                result = result + "\"" + s + "\"\n";
            }
            return result;
        }

        @Override
        public int[] sectionRemovalBounds(StringBuilder replacement) {
            int indexOf;
            Token token;
            assert (!this.oldL.isEmpty() && this.newL.isEmpty());
            SourcePositions positions = this.diffContext.trees.getSourcePositions();
            JCTree.JCCompilationUnit compilationUnit = this.diffContext.origUnit;
            int sectionStart = (int)positions.getStartPosition((CompilationUnitTree)compilationUnit, (Tree)this.oldL.get(0));
            int sectionEnd = (int)positions.getEndPosition((CompilationUnitTree)compilationUnit, (Tree)this.oldL.get(this.oldL.size() - 1));
            this.seq.move(sectionStart);
            this.seq.moveNext();
            while (this.seq.movePrevious() && nonRelevant.contains((Object)(token = this.seq.token()).id())) {
                if (JavaTokenId.LINE_COMMENT == token.id()) {
                    this.seq.moveNext();
                    sectionStart = this.seq.offset();
                    break;
                }
                if (JavaTokenId.BLOCK_COMMENT == token.id() || JavaTokenId.JAVADOC_COMMENT == token.id()) break;
                if (JavaTokenId.WHITESPACE != token.id()) continue;
                indexOf = token.text().toString().indexOf(10);
                if (indexOf > -1) {
                    sectionStart = this.seq.offset() + indexOf + 1;
                    continue;
                }
                sectionStart = this.seq.offset();
            }
            this.seq.move(sectionEnd);
            this.seq.movePrevious();
            while (this.seq.moveNext() && nonRelevant.contains((Object)(token = this.seq.token()).id())) {
                if (JavaTokenId.LINE_COMMENT == token.id()) {
                    sectionEnd = this.seq.offset();
                    if (!this.seq.moveNext()) break;
                    sectionEnd = this.seq.offset();
                    break;
                }
                if (JavaTokenId.BLOCK_COMMENT == token.id() || JavaTokenId.JAVADOC_COMMENT == token.id()) break;
                if (JavaTokenId.WHITESPACE != token.id()) continue;
                indexOf = token.text().toString().indexOf(10);
                if (indexOf > -1) {
                    sectionEnd = this.seq.offset() + indexOf + 1;
                    continue;
                }
                sectionEnd = this.seq.offset() + token.text().length();
            }
            return new int[]{sectionStart, sectionEnd};
        }
    }

    static class ThrowsEstimator
    extends BaseEstimator {
        ThrowsEstimator(List<? extends ExpressionTree> oldL, List<? extends ExpressionTree> newL, DiffContext diffContext) {
            super(JavaTokenId.THROWS, oldL, newL, diffContext);
        }
    }

    static class ExtendsEstimator
    extends BaseEstimator {
        ExtendsEstimator(List<? extends Tree> oldL, List<? extends Tree> newL, DiffContext diffContext) {
            super(JavaTokenId.EXTENDS, oldL, newL, diffContext);
        }
    }

    static class ImplementsEstimator
    extends BaseEstimator {
        ImplementsEstimator(List<? extends Tree> oldL, List<? extends Tree> newL, DiffContext diffContext) {
            super(JavaTokenId.IMPLEMENTS, oldL, newL, diffContext);
        }
    }

}

