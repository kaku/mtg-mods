/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.java.source.parsing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import javax.tools.JavaFileObject;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.source.parsing.Archive;
import org.netbeans.modules.java.source.util.Iterators;
import org.openide.util.Parameters;

abstract class ProxyArchive
implements Archive {
    private final Archive[] delegates;

    private ProxyArchive(@NonNull Archive[] delegates) {
        Parameters.notNull((CharSequence)"delegates", (Object)delegates);
        this.delegates = delegates;
    }

    @Override
    public abstract Iterable<JavaFileObject> getFiles(String var1, ClassPath.Entry var2, Set<JavaFileObject.Kind> var3, JavaFileFilterImplementation var4) throws IOException;

    @Override
    public JavaFileObject getFile(String name) throws IOException {
        for (Archive delegate : this.delegates) {
            JavaFileObject jfo = delegate.getFile(name);
            if (jfo == null) continue;
            return jfo;
        }
        return null;
    }

    @Override
    public void clear() {
        for (Archive delegate : this.delegates) {
            delegate.clear();
        }
    }

    @Override
    public JavaFileObject create(String relativeName, JavaFileFilterImplementation filter) throws UnsupportedOperationException {
        for (Archive delegate : this.delegates) {
            try {
                return delegate.create(relativeName, filter);
            }
            catch (UnsupportedOperationException e) {
                continue;
            }
        }
        throw new UnsupportedOperationException("Create operation s not supported by delegates");
    }

    @NonNull
    static /* varargs */ ProxyArchive createComposite(@NonNull Archive ... delegates) {
        return new Composite(delegates);
    }

    @NonNull
    static /* varargs */ ProxyArchive createAdditionalPackages(@NonNull Archive ... delegates) {
        return new AddPkgs(delegates);
    }

    @NonNull
    private static Archive[] getDelegates(@NonNull ProxyArchive pa) {
        return pa.delegates;
    }

    private static boolean isEmpty(@NonNull Iterable<? extends JavaFileObject> it) {
        if (it instanceof Collection) {
            return ((Collection)it).isEmpty();
        }
        return !it.iterator().hasNext();
    }

    private static final class AddPkgs
    extends ProxyArchive {
        AddPkgs(@NonNull Archive[] delegates) {
            super(delegates);
        }

        @NonNull
        @Override
        public Iterable<JavaFileObject> getFiles(@NonNull String folderName, @NullAllowed ClassPath.Entry entry, @NonNull Set<JavaFileObject.Kind> kinds, @NullAllowed JavaFileFilterImplementation filter) throws IOException {
            for (Archive delegate : ProxyArchive.getDelegates(this)) {
                Iterable<JavaFileObject> it = delegate.getFiles(folderName, entry, kinds, filter);
                if (ProxyArchive.isEmpty(it)) continue;
                return it;
            }
            return Collections.emptyList();
        }
    }

    private static final class Composite
    extends ProxyArchive {
        Composite(@NonNull Archive[] delegates) {
            super(delegates);
        }

        @NonNull
        @Override
        public Iterable<JavaFileObject> getFiles(@NonNull String folderName, @NullAllowed ClassPath.Entry entry, @NonNull Set<JavaFileObject.Kind> kinds, @NullAllowed JavaFileFilterImplementation filter) throws IOException {
            ArrayList<Iterable<JavaFileObject>> collector = new ArrayList<Iterable<JavaFileObject>>();
            for (Archive delegate : ProxyArchive.getDelegates(this)) {
                Iterable<JavaFileObject> it = delegate.getFiles(folderName, entry, kinds, filter);
                if (ProxyArchive.isEmpty(it)) continue;
                collector.add(it);
            }
            return Iterators.chained(collector);
        }
    }

}

