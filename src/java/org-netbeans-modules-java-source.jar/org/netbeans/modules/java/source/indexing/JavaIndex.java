/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.parsing.impl.indexing.CacheFolder
 *  org.netbeans.modules.parsing.impl.indexing.SPIAccessor
 *  org.netbeans.modules.parsing.spi.indexing.Context
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 *  org.openide.util.Pair
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.source.indexing;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.net.URL;
import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.Properties;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.java.source.parsing.CachingArchiveProvider;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;
import org.netbeans.modules.java.source.usages.ClassIndexManager;
import org.netbeans.modules.parsing.impl.indexing.CacheFolder;
import org.netbeans.modules.parsing.impl.indexing.SPIAccessor;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import org.openide.util.Pair;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;

public final class JavaIndex {
    public static final String NAME = "java";
    public static final int VERSION = 14;
    static final Logger LOG = Logger.getLogger(JavaIndex.class.getName());
    private static final String CLASSES = "classes";
    private static final String APT_SOURCES = "sources";
    private static final String ATTR_FILE_NAME = "attributes.properties";
    private static final int SLIDING_WINDOW = 1000;
    private static final RequestProcessor RP = new RequestProcessor(JavaIndex.class);
    private static final RequestProcessor.Task SAVER = RP.create((Runnable)new Saver());
    private static final Object cacheLock = new Object();
    private static URL cacheRoot;
    private static Reference<Properties> cacheValue;
    private static final Queue<Pair<URL, Properties>> savePending;

    public static File getIndex(Context c) {
        return FileUtil.toFile((FileObject)c.getIndexFolder());
    }

    public static File getIndex(URL url) throws IOException {
        url = CachingArchiveProvider.getDefault().mapCtSymToJar(url);
        FileObject indexBaseFolder = CacheFolder.getDataFolder((URL)url);
        String path = SPIAccessor.getInstance().getIndexerPath("java", 14);
        FileObject indexFolder = FileUtil.createFolder((FileObject)indexBaseFolder, (String)path);
        return FileUtil.toFile((FileObject)indexFolder);
    }

    public static File getClassFolder(Context c) {
        return JavaIndex.getClassFolder(c, false);
    }

    public static File getClassFolder(Context c, boolean onlyIfExists) {
        return JavaIndex.processCandidate(new File(JavaIndex.getIndex(c), "classes"), onlyIfExists, true);
    }

    public static File getClassFolder(File root) throws IOException {
        return JavaIndex.getClassFolder(Utilities.toURI((File)root).toURL());
    }

    public static File getClassFolder(URL url) throws IOException {
        return JavaIndex.getClassFolder(url, false, true);
    }

    public static File getClassFolder(URL url, boolean onlyIfExists) throws IOException {
        return JavaIndex.getClassFolder(url, onlyIfExists, true);
    }

    public static File getClassFolder(URL url, boolean onlyIfExists, boolean create) throws IOException {
        return JavaIndex.processCandidate(new File(JavaIndex.getIndex(url), "classes"), onlyIfExists, create);
    }

    public static File getAptFolder(URL sourceRoot, boolean create) throws IOException {
        File aptSources = new File(JavaIndex.getIndex(sourceRoot), "sources");
        if (create) {
            aptSources.mkdirs();
        }
        return aptSources;
    }

    public static URL getSourceRootForClassFolder(URL binaryRoot) {
        FileObject folder = URLMapper.findFileObject((URL)binaryRoot);
        if (folder == null || !"classes".equals(folder.getName())) {
            return null;
        }
        if ((folder = folder.getParent()) == null || !String.valueOf(14).equals(folder.getName())) {
            return null;
        }
        if ((folder = folder.getParent()) == null || !"java".equals(folder.getName())) {
            return null;
        }
        if ((folder = folder.getParent()) == null) {
            return null;
        }
        return CacheFolder.getSourceRootForDataFolder((FileObject)folder);
    }

    public static boolean ensureAttributeValue(URL root, String attributeName, String attributeValue) throws IOException {
        return JavaIndex.ensureAttributeValue(root, attributeName, attributeValue, false);
    }

    public static boolean ensureAttributeValue(URL root, String attributeName, String attributeValue, boolean checkOnly) throws IOException {
        Properties p = JavaIndex.loadProperties(root);
        String current = p.getProperty(attributeName);
        if (current == null) {
            if (attributeValue != null) {
                if (!checkOnly) {
                    p.setProperty(attributeName, attributeValue);
                    JavaIndex.storeProperties(root, p, false);
                }
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "ensureAttributeValue attr: {0} current: {1} new: {2} checkOnly: {3}", new Object[]{attributeName, current, attributeValue, checkOnly});
                }
                return true;
            }
            return false;
        }
        if (current.equals(attributeValue)) {
            return false;
        }
        if (!checkOnly) {
            if (attributeValue != null) {
                p.setProperty(attributeName, attributeValue);
            } else {
                p.remove(attributeName);
            }
            JavaIndex.storeProperties(root, p, false);
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "ensureAttributeValue attr: {0} current: {1} new: {2} checkOnly: {3}", new Object[]{attributeName, current, attributeValue, checkOnly});
        }
        return true;
    }

    public static void setAttribute(URL root, String attributeName, String attributeValue) throws IOException {
        Properties p = JavaIndex.loadProperties(root);
        if (attributeValue != null) {
            p.setProperty(attributeName, attributeValue);
        } else {
            p.remove(attributeName);
        }
        JavaIndex.storeProperties(root, p, true);
    }

    public static String getAttribute(URL root, String attributeName, String defaultValue) throws IOException {
        Properties p = JavaIndex.loadProperties(root);
        return p.getProperty(attributeName, defaultValue);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static Properties loadProperties(URL root) throws IOException {
        Object object = cacheLock;
        synchronized (object) {
            Properties result;
            for (Pair<URL, Properties> active : savePending) {
                if (!((URL)active.first()).equals(root)) continue;
                return (Properties)active.second();
            }
            if (cacheRoot != null && cacheRoot.equals(root)) {
                Properties properties = result = cacheValue == null ? null : cacheValue.get();
                if (result != null) {
                    return result;
                }
            }
            File f = JavaIndex.getAttributeFile(root);
            result = new Properties();
            if (!f.exists()) {
                return result;
            }
            BufferedInputStream in = new BufferedInputStream(new FileInputStream(f));
            try {
                result.load(in);
            }
            catch (IllegalArgumentException iae) {
                LOG.log(Level.WARNING, "Broken attribute file: {0}", f.getAbsolutePath());
                result = new Properties();
            }
            finally {
                in.close();
            }
            cacheRoot = root;
            cacheValue = new SoftReference<Properties>(result);
            return result;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void storeProperties(URL root, Properties p, boolean barrier) throws IOException {
        Object object = cacheLock;
        synchronized (object) {
            if (barrier) {
                Iterator<Pair<URL, Properties>> it = savePending.iterator();
                while (it.hasNext()) {
                    Pair<URL, Properties> pending = it.next();
                    if (!((URL)pending.first()).equals(root)) continue;
                    it.remove();
                    break;
                }
                JavaIndex.storeImpl(root, p);
            } else {
                boolean alreadyStoring = false;
                for (Pair<URL, Properties> pending : savePending) {
                    if (!((URL)pending.first()).equals(root)) continue;
                    alreadyStoring = true;
                    break;
                }
                if (!alreadyStoring) {
                    savePending.offer((Pair)Pair.of((Object)root, (Object)p));
                }
                SAVER.schedule(1000);
            }
        }
    }

    private static void storeImpl(@NonNull URL root, @NonNull Properties p) throws IOException {
        if (!Thread.holdsLock(cacheLock)) {
            throw new IllegalStateException("Requires cacheLock");
        }
        File f = JavaIndex.getAttributeFile(root);
        BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(f));
        Throwable throwable = null;
        try {
            p.store(out, "");
        }
        catch (Throwable x2) {
            throwable = x2;
            throw x2;
        }
        finally {
            if (out != null) {
                if (throwable != null) {
                    try {
                        out.close();
                    }
                    catch (Throwable x2) {
                        throwable.addSuppressed(x2);
                    }
                } else {
                    out.close();
                }
            }
        }
        if (cacheRoot == null || cacheRoot.equals(root)) {
            cacheRoot = root;
            cacheValue = new SoftReference<Properties>(p);
        }
    }

    private static File getAttributeFile(URL root) throws IOException {
        return new File(JavaIndex.getIndex(root), "attributes.properties");
    }

    private static File processCandidate(File result, boolean onlyIfExists, boolean create) {
        if (onlyIfExists) {
            if (!result.exists()) {
                return null;
            }
            return result;
        }
        if (create) {
            result.mkdirs();
        }
        return result;
    }

    public static boolean hasSourceCache(@NonNull URL root, boolean testForInitialized) {
        assert (root != null);
        ClassIndexImpl uq = ClassIndexManager.getDefault().getUsagesQuery(root, !testForInitialized);
        return uq != null && (!testForInitialized || uq.getState() == ClassIndexImpl.State.INITIALIZED) && uq.getType() == ClassIndexImpl.Type.SOURCE;
    }

    public static boolean hasBinaryCache(@NonNull URL root, boolean testForInitialized) {
        assert (root != null);
        ClassIndexImpl uq = ClassIndexManager.getDefault().getUsagesQuery(root, !testForInitialized);
        return uq != null && (!testForInitialized || uq.getState() == ClassIndexImpl.State.INITIALIZED) && uq.getType() == ClassIndexImpl.Type.BINARY;
    }

    public static boolean isIndexed(@NonNull URL root) {
        return ClassIndexManager.getDefault().getUsagesQuery(root, false) != null;
    }

    private JavaIndex() {
    }

    static {
        savePending = new ArrayDeque<Pair<URL, Properties>>();
    }

    private static final class Saver
    implements Runnable {
        private Saver() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            Object object = cacheLock;
            synchronized (object) {
                Pair car = (Pair)savePending.peek();
                if (car != null) {
                    try {
                        JavaIndex.storeImpl((URL)car.first(), (Properties)car.second());
                    }
                    catch (IOException ioe) {
                        Exceptions.printStackTrace((Throwable)ioe);
                    }
                    finally {
                        savePending.remove();
                        if (!savePending.isEmpty()) {
                            SAVER.setPriority(1000);
                        }
                    }
                }
            }
        }
    }

}

