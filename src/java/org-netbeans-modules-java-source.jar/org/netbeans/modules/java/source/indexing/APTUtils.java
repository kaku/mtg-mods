/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.util.Context
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.api.java.queries.AnnotationProcessingQuery
 *  org.netbeans.api.java.queries.AnnotationProcessingQuery$Result
 *  org.netbeans.api.java.queries.AnnotationProcessingQuery$Trigger
 *  org.netbeans.api.java.queries.SourceLevelQuery
 *  org.netbeans.api.java.queries.SourceLevelQuery$Profile
 *  org.netbeans.api.java.queries.SourceLevelQuery$Result
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ui.OpenProjects
 *  org.netbeans.modules.parsing.api.indexing.IndexingManager
 *  org.netbeans.modules.parsing.impl.indexing.PathRegistry
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.java.source.indexing;

import com.sun.tools.javac.util.Context;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.processing.Processor;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.queries.AnnotationProcessingQuery;
import org.netbeans.api.java.queries.SourceLevelQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ui.OpenProjects;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.parsing.CachingArchiveClassLoader;
import org.netbeans.modules.parsing.api.indexing.IndexingManager;
import org.netbeans.modules.parsing.impl.indexing.PathRegistry;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.lookup.Lookups;

public class APTUtils
implements ChangeListener,
PropertyChangeListener {
    private static final Logger LOG = Logger.getLogger(APTUtils.class.getName());
    private static final String PROCESSOR_PATH = "processorPath";
    private static final String BOOT_PATH = "bootPath";
    private static final String COMPILE_PATH = "compilePath";
    private static final String APT_ENABLED = "aptEnabled";
    private static final String ANNOTATION_PROCESSORS = "annotationProcessors";
    private static final String SOURCE_LEVEL_ROOT = "sourceLevel";
    private static final String JRE_PROFILE = "jreProfile";
    private static final Map<URL, APTUtils> knownSourceRootsMap = new HashMap<URL, APTUtils>();
    private static final Map<FileObject, Reference<APTUtils>> auxiliarySourceRootsMap = new WeakHashMap<FileObject, Reference<APTUtils>>();
    private static final Lookup HARDCODED_PROCESSORS = Lookups.forPath((String)"Editors/text/x-java/AnnotationProcessors");
    private static final boolean DISABLE_CLASSLOADER_CACHE = Boolean.getBoolean("java.source.aptutils.disable.classloader.cache");
    private static final int SLIDING_WINDOW = 1000;
    private static final RequestProcessor RP = new RequestProcessor(APTUtils.class);
    private final FileObject root;
    private volatile ClassPath bootPath;
    private volatile ClassPath compilePath;
    private final AtomicReference<ClassPath> processorPath;
    private final AnnotationProcessingQuery.Result aptOptions;
    private final SourceLevelQuery.Result sourceLevel;
    private final RequestProcessor.Task slidingRefresh;
    private volatile ClassLoaderRef classLoaderCache;
    private static final Iterable<? extends String> javacPackages = Arrays.asList("com.sun.javadoc.", "com.sun.source.", "javax.annotation.processing.", "javax.lang.model.", "javax.tools.", "com.sun.tools.javac.", "com.sun.tools.javadoc.", "com.sun.tools.classfile.");

    private APTUtils(final @NonNull FileObject root) {
        this.root = root;
        this.bootPath = ClassPath.getClassPath((FileObject)root, (String)"classpath/boot");
        this.compilePath = ClassPath.getClassPath((FileObject)root, (String)"classpath/compile");
        this.processorPath = new AtomicReference<ClassPath>(ClassPath.getClassPath((FileObject)root, (String)"classpath/processor"));
        this.aptOptions = AnnotationProcessingQuery.getAnnotationProcessingOptions((FileObject)root);
        this.sourceLevel = SourceLevelQuery.getSourceLevel2((FileObject)root);
        this.slidingRefresh = RP.create(new Runnable(){

            @Override
            public void run() {
                IndexingManager.getDefault().refreshIndex(root.toURL(), Collections.emptyList(), false);
            }
        });
    }

    @NonNull
    public static APTUtils get(FileObject root) {
        APTUtils utils;
        if (root == null) {
            return null;
        }
        URL rootUrl = root.toURL();
        if (knownSourceRootsMap.containsKey(rootUrl)) {
            APTUtils utils2 = knownSourceRootsMap.get(rootUrl);
            if (utils2 == null) {
                utils2 = APTUtils.create(root);
                knownSourceRootsMap.put(rootUrl, utils2);
            }
            return utils2;
        }
        Reference<APTUtils> utilsRef = auxiliarySourceRootsMap.get((Object)root);
        APTUtils aPTUtils = utils = utilsRef != null ? utilsRef.get() : null;
        if (utils == null) {
            utils = APTUtils.create(root);
            auxiliarySourceRootsMap.put(root, new WeakReference<APTUtils>(utils));
        }
        return utils;
    }

    @CheckForNull
    public static APTUtils getIfExist(@NullAllowed FileObject root) {
        if (root == null) {
            return null;
        }
        URL rootUrl = root.toURL();
        APTUtils res = knownSourceRootsMap.get(rootUrl);
        if (res != null) {
            return res;
        }
        Reference<APTUtils> utilsRef = auxiliarySourceRootsMap.get((Object)root);
        res = utilsRef != null ? utilsRef.get() : null;
        return res;
    }

    public static void sourceRootRegistered(FileObject root, URL rootURL) {
        if (root == null || knownSourceRootsMap.containsKey(rootURL) || PathRegistry.getDefault().getUnknownRoots().contains(rootURL)) {
            return;
        }
        Reference<APTUtils> utilsRef = auxiliarySourceRootsMap.remove((Object)root);
        APTUtils utils = utilsRef != null ? utilsRef.get() : null;
        knownSourceRootsMap.put(rootURL, utils);
    }

    public static void sourceRootUnregistered(Iterable<? extends URL> roots) {
        for (URL root : roots) {
            knownSourceRootsMap.remove(root);
        }
        for (URL unknown : PathRegistry.getDefault().getUnknownRoots()) {
            knownSourceRootsMap.remove(unknown);
        }
        Project[] projects = OpenProjects.getDefault().getOpenProjects();
        if (projects.length == 0 && !knownSourceRootsMap.isEmpty()) {
            LOG.log(Level.WARNING, "Non removed known roots: {0}", knownSourceRootsMap.keySet());
            knownSourceRootsMap.clear();
        }
    }

    @NonNull
    private static APTUtils create(FileObject root) {
        APTUtils utils = new APTUtils(root);
        utils.listen();
        return utils;
    }

    public boolean aptEnabledOnScan() {
        return this.aptOptions.annotationProcessingEnabled().contains((Object)AnnotationProcessingQuery.Trigger.ON_SCAN);
    }

    public boolean aptEnabledInEditor() {
        return this.aptOptions.annotationProcessingEnabled().contains((Object)AnnotationProcessingQuery.Trigger.IN_EDITOR);
    }

    @CheckForNull
    public URL sourceOutputDirectory() {
        return this.aptOptions.sourceOutputDirectory();
    }

    public Collection<? extends Processor> resolveProcessors(boolean onScan) {
        ClassLoader cl;
        ClassPath pp = this.validatePaths();
        ClassLoaderRef cache = this.classLoaderCache;
        if (cache == null || (cl = cache.get(this.root)) == null) {
            if (pp == null) {
                pp = ClassPath.EMPTY;
            }
            cl = CachingArchiveClassLoader.forClassPath(pp, new BypassOpenIDEUtilClassLoader(Context.class.getClassLoader()));
            this.classLoaderCache = !DISABLE_CLASSLOADER_CACHE ? new ClassLoaderRef(cl, this.root) : null;
        }
        Collection<Processor> result = this.lookupProcessors(cl, onScan);
        return result;
    }

    public Map<? extends String, ? extends String> processorOptions() {
        return this.aptOptions.processorOptions();
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if (this.verifyAttributes(this.root, true)) {
            this.slidingRefresh.schedule(1000);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("roots".equals(evt.getPropertyName())) {
            this.classLoaderCache = null;
        }
        if (this.verifyAttributes(this.root, true)) {
            this.slidingRefresh.schedule(1000);
        }
    }

    private void listen() {
        APTUtils.listenOnProcessorPath(this.processorPath.get(), this);
        this.aptOptions.addChangeListener(WeakListeners.change((ChangeListener)this, (Object)this.aptOptions));
        if (this.sourceLevel.supportsChanges()) {
            this.sourceLevel.addChangeListener(WeakListeners.change((ChangeListener)this, (Object)this.sourceLevel));
        }
    }

    private static void listenOnProcessorPath(@NullAllowed ClassPath cp, @NonNull APTUtils target) {
        if (cp != null) {
            cp.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)target, (Object)cp));
            cp.getRoots();
        }
    }

    @CheckForNull
    private ClassPath validatePaths() {
        ClassPath pp = this.processorPath.get();
        if (pp != null) {
            return pp;
        }
        pp = ClassPath.getClassPath((FileObject)this.root, (String)"classpath/processor");
        if (pp != null && this.processorPath.compareAndSet((ClassPath)null, pp)) {
            this.bootPath = ClassPath.getClassPath((FileObject)this.root, (String)"classpath/boot");
            this.compilePath = ClassPath.getClassPath((FileObject)this.root, (String)"classpath/compile");
            APTUtils.listenOnProcessorPath(pp, this);
            this.classLoaderCache = null;
        }
        return pp;
    }

    private Collection<Processor> lookupProcessors(ClassLoader cl, boolean onScan) {
        Iterable<? extends String> processorNames = this.aptOptions.annotationProcessorsToRun();
        if (processorNames == null) {
            processorNames = this.getProcessorNames(cl);
        }
        LinkedList<Processor> result = new LinkedList<Processor>();
        for (String name : processorNames) {
            try {
                Class clazz = Class.forName(name, true, cl);
                Object instance = clazz.newInstance();
                if (!(instance instanceof Processor)) continue;
                result.add((Processor)instance);
                continue;
            }
            catch (ThreadDeath td) {
                throw td;
            }
            catch (Throwable t) {
                LOG.log(Level.FINE, null, t);
                continue;
            }
        }
        if (!onScan) {
            result.addAll(HARDCODED_PROCESSORS.lookupAll(Processor.class));
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Iterable<? extends String> getProcessorNames(ClassLoader cl) {
        LinkedList<String> result = new LinkedList<String>();
        try {
            Enumeration<URL> resources = cl.getResources("META-INF/services/" + Processor.class.getName());
            while (resources.hasMoreElements()) {
                BufferedReader ins = null;
                try {
                    String line;
                    ins = new BufferedReader(new InputStreamReader(resources.nextElement().openStream(), "UTF-8"));
                    while ((line = ins.readLine()) != null) {
                        int hash = line.indexOf(35);
                        String string = line = hash != -1 ? line.substring(0, hash) : line;
                        if ((line = line.trim()).length() <= 0) continue;
                        result.add(line);
                    }
                    continue;
                }
                catch (IOException ex) {
                    LOG.log(Level.FINE, null, ex);
                    continue;
                }
                finally {
                    if (ins == null) continue;
                    ins.close();
                    continue;
                }
            }
        }
        catch (IOException ex) {
            LOG.log(Level.FINE, null, ex);
        }
        return result;
    }

    boolean verifyAttributes(FileObject fo, boolean checkOnly) {
        if (fo == null) {
            return false;
        }
        boolean vote = false;
        try {
            boolean apEnabledOnScan;
            ClassPath pp = this.validatePaths();
            URL url = fo.toURL();
            if (JavaIndex.ensureAttributeValue(url, "sourceLevel", this.sourceLevel.getSourceLevel(), checkOnly)) {
                JavaIndex.LOG.fine("forcing reindex due to source level change");
                vote = true;
                if (checkOnly) {
                    return vote;
                }
            }
            if (JavaIndex.ensureAttributeValue(url, "jreProfile", this.sourceLevel.getProfile().getName(), checkOnly)) {
                JavaIndex.LOG.fine("forcing reindex due to jre profile change");
                vote = true;
                if (checkOnly) {
                    return vote;
                }
            }
            if (JavaIndex.ensureAttributeValue(url, "bootPath", APTUtils.pathToString(this.bootPath), checkOnly)) {
                JavaIndex.LOG.fine("forcing reindex due to boot path change");
                vote = true;
                if (checkOnly) {
                    return vote;
                }
            }
            if (JavaIndex.ensureAttributeValue(url, "compilePath", APTUtils.pathToString(this.compilePath), checkOnly)) {
                JavaIndex.LOG.fine("forcing reindex due to compile path change");
                vote = true;
                if (checkOnly) {
                    return vote;
                }
            }
            if (JavaIndex.ensureAttributeValue(url, "aptEnabled", (apEnabledOnScan = this.aptOptions.annotationProcessingEnabled().contains((Object)AnnotationProcessingQuery.Trigger.ON_SCAN)) ? Boolean.TRUE.toString() : null, checkOnly)) {
                JavaIndex.LOG.fine("forcing reindex due to change in annotation processing options");
                vote = true;
                if (checkOnly) {
                    return vote;
                }
            }
            if (!apEnabledOnScan) {
                return vote;
            }
            if (JavaIndex.ensureAttributeValue(url, "processorPath", APTUtils.pathToString(pp), checkOnly)) {
                JavaIndex.LOG.fine("forcing reindex due to processor path change");
                vote = true;
                if (checkOnly) {
                    return vote;
                }
            }
            if (JavaIndex.ensureAttributeValue(url, "annotationProcessors", this.encodeToStirng(this.aptOptions.annotationProcessorsToRun()), checkOnly)) {
                JavaIndex.LOG.fine("forcing reindex due to change in annotation processors");
                vote = true;
                if (checkOnly) {
                    return vote;
                }
            }
        }
        catch (IOException ioe) {
            Exceptions.printStackTrace((Throwable)ioe);
        }
        return vote;
    }

    @NonNull
    private static String pathToString(@NullAllowed ClassPath cp) {
        StringBuilder b = new StringBuilder();
        if (cp == null) {
            cp = ClassPath.EMPTY;
        }
        for (ClassPath.Entry cpe : cp.entries()) {
            FileObject fo = cpe.getRoot();
            if (fo == null) continue;
            URL u = fo.toURL();
            File f = FileUtil.archiveOrDirForURL((URL)u);
            if (f != null) {
                if (b.length() > 0) {
                    b.append(File.pathSeparatorChar);
                }
                b.append(f.getAbsolutePath());
                continue;
            }
            if (b.length() > 0) {
                b.append(File.pathSeparatorChar);
            }
            b.append(u);
        }
        return b.toString();
    }

    private String encodeToStirng(Iterable<? extends String> strings) {
        if (strings == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        Iterator<? extends String> it = strings.iterator();
        while (it.hasNext()) {
            sb.append((Object)it.next());
            if (!it.hasNext()) continue;
            sb.append(',');
        }
        return sb.length() > 0 ? sb.toString() : null;
    }

    private static final class ClassLoaderRef
    extends SoftReference<ClassLoader>
    implements Runnable {
        private final long timeStamp;
        private final String rootPath;

        public ClassLoaderRef(ClassLoader cl, FileObject root) {
            super(cl, Utilities.activeReferenceQueue());
            this.timeStamp = ClassLoaderRef.getTimeStamp(root);
            this.rootPath = FileUtil.getFileDisplayName((FileObject)root);
            LOG.log(Level.FINER, "ClassLoader for root {0} created.", new Object[]{this.rootPath});
        }

        public ClassLoader get(FileObject root) {
            long curTimeStamp = ClassLoaderRef.getTimeStamp(root);
            return curTimeStamp == this.timeStamp ? (ClassLoader)this.get() : null;
        }

        @Override
        public void run() {
            LOG.log(Level.FINER, "ClassLoader for root {0} freed.", new Object[]{this.rootPath});
        }

        private static long getTimeStamp(FileObject root) {
            FileObject archiveFile = FileUtil.getArchiveFile((FileObject)root);
            return archiveFile != null ? root.lastModified().getTime() : -1;
        }
    }

    private static final class BypassOpenIDEUtilClassLoader
    extends ClassLoader {
        private final ClassLoader contextCL;

        public BypassOpenIDEUtilClassLoader(ClassLoader contextCL) {
            super(BypassOpenIDEUtilClassLoader.getSystemClassLoader().getParent());
            this.contextCL = contextCL;
        }

        @Override
        protected synchronized Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
            char f;
            char c = f = name.length() > 4 ? name.charAt(4) : '\u0000';
            if (f == 'x' || f == 's') {
                for (String pack : javacPackages) {
                    if (!name.startsWith(pack)) continue;
                    return this.contextCL.loadClass(name);
                }
            }
            return super.loadClass(name, resolve);
        }
    }

}

