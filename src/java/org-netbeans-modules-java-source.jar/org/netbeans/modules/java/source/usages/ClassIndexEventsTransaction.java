/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.java.source.usages;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.lang.model.element.TypeElement;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.indexing.TransactionContext;
import org.netbeans.modules.java.source.usages.BuildArtifactMapperImpl;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;
import org.netbeans.modules.java.source.usages.ClassIndexManager;
import org.openide.util.Parameters;

public final class ClassIndexEventsTransaction
extends TransactionContext.Service {
    private final boolean source;
    private Set<URL> removedRoots;
    private Collection<ElementHandle<TypeElement>> addedTypes;
    private Collection<ElementHandle<TypeElement>> removedTypes;
    private Collection<ElementHandle<TypeElement>> changedTypes;
    private Collection<File> addedFiles;
    private Collection<File> removedFiles;
    private URL addedRoot;
    private URL changesInRoot;
    private boolean closed;

    private ClassIndexEventsTransaction(boolean src) {
        this.source = src;
        this.removedRoots = new HashSet<URL>();
        this.addedTypes = new HashSet<ElementHandle<TypeElement>>();
        this.removedTypes = new HashSet<ElementHandle<TypeElement>>();
        this.changedTypes = new HashSet<ElementHandle<TypeElement>>();
        this.addedFiles = new ArrayDeque<File>();
        this.removedFiles = new ArrayDeque<File>();
    }

    public void rootAdded(@NonNull URL root) {
        this.checkClosedTx();
        assert (root != null);
        assert (this.addedRoot == null);
        assert (this.changesInRoot == null || this.changesInRoot.equals(root));
        this.addedRoot = root;
    }

    public void rootRemoved(@NonNull URL root) {
        this.checkClosedTx();
        assert (root != null);
        this.removedRoots.add(root);
    }

    public void addedTypes(@NonNull URL root, @NonNull Collection<? extends ElementHandle<TypeElement>> added) {
        this.checkClosedTx();
        assert (root != null);
        assert (added != null);
        assert (this.changesInRoot == null || this.changesInRoot.equals(root));
        assert (this.addedRoot == null || this.addedRoot.equals(root));
        this.addedTypes.addAll(added);
        this.changesInRoot = root;
    }

    public void removedTypes(@NonNull URL root, @NonNull Collection<? extends ElementHandle<TypeElement>> removed) {
        this.checkClosedTx();
        assert (root != null);
        assert (removed != null);
        assert (this.changesInRoot == null || this.changesInRoot.equals(root));
        assert (this.addedRoot == null || this.addedRoot.equals(root));
        this.removedTypes.addAll(removed);
        this.changesInRoot = root;
    }

    public void changedTypes(@NonNull URL root, @NonNull Collection<? extends ElementHandle<TypeElement>> changed) {
        this.checkClosedTx();
        assert (root != null);
        assert (changed != null);
        assert (this.changesInRoot == null || this.changesInRoot.equals(root));
        assert (this.addedRoot == null || this.addedRoot.equals(root));
        this.changedTypes.addAll(changed);
        this.changesInRoot = root;
    }

    public void addedCacheFiles(@NonNull URL root, @NonNull Collection<? extends File> files) throws IllegalStateException {
        this.checkClosedTx();
        Parameters.notNull((CharSequence)"root", (Object)root);
        Parameters.notNull((CharSequence)"files", files);
        if (!this.source) {
            throw new IllegalStateException("The addedCacheFiles can be called only for source root.");
        }
        assert (this.changesInRoot == null || this.changesInRoot.equals(root));
        assert (this.addedRoot == null || this.addedRoot.equals(root));
        this.addedFiles.addAll(files);
        this.changesInRoot = root;
    }

    public void removedCacheFiles(@NonNull URL root, @NonNull Collection<? extends File> files) throws IllegalStateException {
        this.checkClosedTx();
        Parameters.notNull((CharSequence)"root", (Object)root);
        Parameters.notNull((CharSequence)"files", files);
        if (!this.source) {
            throw new IllegalStateException("The removedCacheFiles can be called only for source root.");
        }
        assert (this.changesInRoot == null || this.changesInRoot.equals(root));
        assert (this.addedRoot == null || this.addedRoot.equals(root));
        this.removedFiles.addAll(files);
        this.changesInRoot = root;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void commit() throws IOException {
        this.closeTx();
        try {
            ClassIndexImpl ci;
            block8 : {
                try {
                    if (this.addedFiles.isEmpty() && this.removedFiles.isEmpty()) break block8;
                    assert (this.changesInRoot != null);
                    BuildArtifactMapperImpl.classCacheUpdated(this.changesInRoot, JavaIndex.getClassFolder(this.changesInRoot), Collections.unmodifiableCollection(this.removedFiles), Collections.unmodifiableCollection(this.addedFiles), false);
                }
                catch (Throwable var3_3) {
                    ClassIndexImpl ci2;
                    ClassIndexManager ciManager = ClassIndexManager.getDefault();
                    ciManager.fire(this.addedRoot == null ? Collections.emptySet() : Collections.singleton(this.addedRoot), Collections.unmodifiableSet(this.removedRoots));
                    ClassIndexImpl classIndexImpl = ci2 = this.changesInRoot == null ? null : ciManager.getUsagesQuery(this.changesInRoot, false);
                    if (ci2 != null) {
                        ci2.typesEvent(Collections.unmodifiableCollection(this.addedTypes), Collections.unmodifiableCollection(this.removedTypes), Collections.unmodifiableCollection(this.changedTypes));
                    }
                    throw var3_3;
                }
            }
            ClassIndexManager ciManager = ClassIndexManager.getDefault();
            ciManager.fire(this.addedRoot == null ? Collections.emptySet() : Collections.singleton(this.addedRoot), Collections.unmodifiableSet(this.removedRoots));
            ClassIndexImpl classIndexImpl = ci = this.changesInRoot == null ? null : ciManager.getUsagesQuery(this.changesInRoot, false);
            if (ci != null) {
                ci.typesEvent(Collections.unmodifiableCollection(this.addedTypes), Collections.unmodifiableCollection(this.removedTypes), Collections.unmodifiableCollection(this.changedTypes));
            }
        }
        finally {
            this.clear();
        }
    }

    @Override
    protected void rollBack() throws IOException {
        this.closeTx();
        this.clear();
    }

    private void clear() {
        this.addedRoot = null;
        this.changesInRoot = null;
        this.removedRoots = null;
        this.addedTypes = null;
        this.removedTypes = null;
        this.changedTypes = null;
        this.addedFiles = null;
        this.removedFiles = null;
    }

    private void checkClosedTx() {
        if (this.closed) {
            throw new IllegalStateException("Already commited or rolled back transaction.");
        }
    }

    private void closeTx() {
        this.checkClosedTx();
        this.closed = true;
    }

    @NonNull
    public static ClassIndexEventsTransaction create(boolean source) {
        return new ClassIndexEventsTransaction(source);
    }
}

