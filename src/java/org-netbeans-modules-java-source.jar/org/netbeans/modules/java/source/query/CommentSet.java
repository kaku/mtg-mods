/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.source.query;

import java.util.List;
import org.netbeans.api.java.source.Comment;

public interface CommentSet {
    @Deprecated
    public void addPrecedingComment(Comment var1);

    @Deprecated
    public void addPrecedingComment(String var1);

    @Deprecated
    public void addPrecedingComments(List<Comment> var1);

    @Deprecated
    public void addTrailingComment(Comment var1);

    @Deprecated
    public void addTrailingComment(String var1);

    @Deprecated
    public void addTrailingComments(List<Comment> var1);

    @Deprecated
    public List<Comment> getPrecedingComments();

    @Deprecated
    public List<Comment> getTrailingComments();

    public boolean hasChanges();

    public boolean hasComments();

    public int pos();

    public int pos(RelativePosition var1);

    public void addComment(RelativePosition var1, Comment var2);

    public List<Comment> getComments(RelativePosition var1);

    public static enum RelativePosition {
        PRECEDING,
        INLINE,
        INNER,
        TRAILING;
        

        private RelativePosition() {
        }
    }

}

