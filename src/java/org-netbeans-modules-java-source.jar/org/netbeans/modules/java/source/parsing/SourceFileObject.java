/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.api.ClientCodeWrapper
 *  com.sun.tools.javac.api.ClientCodeWrapper$Trusted
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.queries.FileEncodingQuery
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObject$Registry
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.NbDocument
 */
package org.netbeans.modules.java.source.parsing;

import com.sun.tools.javac.api.ClientCodeWrapper;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.StyledDocument;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.queries.FileEncodingQuery;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.source.parsing.AbstractSourceFileObject;
import org.netbeans.modules.java.source.parsing.DocumentProvider;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.text.NbDocument;

@ClientCodeWrapper.Trusted
public class SourceFileObject
extends AbstractSourceFileObject
implements DocumentProvider {
    private static final Logger LOG = Logger.getLogger(SourceFileObject.class.getName());
    private final boolean hasFilter;

    @CheckForNull
    public static SourceFileObject create(@NonNull FileObject file, @NonNull FileObject root) {
        try {
            return new SourceFileObject(new AbstractSourceFileObject.Handle(file, root), null, null, false);
        }
        catch (IOException ioe) {
            if (LOG.isLoggable(Level.SEVERE)) {
                LOG.log(Level.SEVERE, ioe.getMessage(), ioe);
            }
            return null;
        }
    }

    public SourceFileObject(@NonNull AbstractSourceFileObject.Handle handle, @NullAllowed JavaFileFilterImplementation filter, @NullAllowed CharSequence content, boolean renderNow) throws IOException {
        super(handle, filter);
        boolean bl = this.hasFilter = filter != null;
        if (content != null || renderNow) {
            this.update(content);
        }
    }

    @Override
    protected final Long isDirty() {
        FileObject file = this.getHandle().resolveFileObject(false);
        if (file == null) {
            return null;
        }
        DataObject.Registry regs = DataObject.getRegistry();
        Set modified = regs.getModifiedSet();
        for (DataObject dobj : modified) {
            StyledDocument doc;
            EditorCookie ec;
            if (!file.equals((Object)dobj.getPrimaryFile()) || (ec = (EditorCookie)dobj.getCookie(EditorCookie.class)) == null || (doc = ec.getDocument()) == null) continue;
            return DocumentUtilities.getDocumentTimestamp((Document)doc);
        }
        return null;
    }

    @CheckForNull
    @Override
    protected final OutputStream createOutputStream() throws IOException {
        FileObject file = this.getHandle().resolveFileObject(true);
        if (file == null) {
            throw new IOException("Cannot create file: " + this.toString());
        }
        StyledDocument doc = this.getDocument();
        if (doc != null) {
            return new DocumentStream(doc);
        }
        return null;
    }

    @NonNull
    @Override
    protected final CharSequence createContent() throws IOException {
        FileObject file = this.getHandle().resolveFileObject(false);
        if (file == null) {
            throw new FileNotFoundException("Cannot open file: " + this.toString());
        }
        Source source = Source.create((FileObject)file);
        if (source == null) {
            throw new IOException("No source for: " + FileUtil.getFileDisplayName((FileObject)file));
        }
        CharSequence content = source.createSnapshot().getText();
        if (this.hasFilter && source.getDocument(false) == null) {
            content = this.filter(content);
        }
        return content;
    }

    @Override
    public StyledDocument getDocument() {
        FileObject file = this.getHandle().resolveFileObject(false);
        if (file == null) {
            return null;
        }
        Source src = Source.create((FileObject)file);
        if (src == null) {
            return null;
        }
        Document doc = src.getDocument(false);
        return doc instanceof StyledDocument ? (StyledDocument)doc : null;
    }

    @Override
    public void runAtomic(Runnable r) {
        assert (r != null);
        StyledDocument doc = this.getDocument();
        if (doc == null) {
            throw new IllegalStateException();
        }
        NbDocument.runAtomic((StyledDocument)doc, (Runnable)r);
    }

    private class DocumentStream
    extends OutputStream {
        private static final int BUF_SIZ = 2048;
        private final StyledDocument doc;
        private byte[] data;
        private int pos;

        public DocumentStream(StyledDocument doc) {
            assert (doc != null);
            this.doc = doc;
            this.data = new byte[2048];
            this.pos = 0;
        }

        @Override
        public synchronized void write(byte[] b, int off, int len) throws IOException {
            this.ensureSize(len);
            System.arraycopy(b, off, this.data, this.pos, len);
            this.pos += len;
        }

        @Override
        public synchronized void write(byte[] b) throws IOException {
            this.ensureSize(b.length);
            System.arraycopy(b, 0, this.data, this.pos, b.length);
            this.pos += b.length;
        }

        @Override
        public synchronized void write(int b) throws IOException {
            this.ensureSize(1);
            this.data[this.pos++] = (byte)(b & 255);
        }

        private void ensureSize(int delta) {
            int requiredLength = this.pos + delta;
            if (this.data.length < requiredLength) {
                int newSize;
                for (newSize = this.data.length + 2048; newSize < requiredLength; newSize += 2048) {
                }
                byte[] newData = new byte[newSize];
                System.arraycopy(this.data, 0, newData, 0, this.pos);
                this.data = newData;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public synchronized void close() throws IOException {
            try {
                NbDocument.runAtomic((StyledDocument)this.doc, (Runnable)new Runnable(){

                    @Override
                    public void run() {
                        block2 : {
                            try {
                                DocumentStream.this.doc.remove(0, DocumentStream.this.doc.getLength());
                                DocumentStream.this.doc.insertString(0, new String(DocumentStream.this.data, 0, DocumentStream.this.pos, FileEncodingQuery.getEncoding((FileObject)SourceFileObject.this.getHandle().resolveFileObject(false))), null);
                            }
                            catch (BadLocationException e) {
                                if (!LOG.isLoggable(Level.SEVERE)) break block2;
                                LOG.log(Level.SEVERE, e.getMessage(), e);
                            }
                        }
                    }
                });
            }
            finally {
                SourceFileObject.this.resetCaches();
            }
        }

    }

}

