/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.api.JavacTrees
 *  com.sun.tools.javac.model.JavacElements
 *  com.sun.tools.javac.model.JavacTypes
 *  com.sun.tools.javac.util.Context
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.platform.JavaPlatform
 *  org.netbeans.api.java.platform.JavaPlatformManager
 *  org.netbeans.api.java.queries.SourceLevelQuery
 *  org.netbeans.api.java.queries.SourceLevelQuery$Profile
 *  org.netbeans.api.java.queries.SourceLevelQuery$Result
 *  org.netbeans.api.queries.FileEncodingQuery
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaIndexerPlugin
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaIndexerPlugin$Factory
 *  org.netbeans.modules.parsing.spi.indexing.Context
 *  org.netbeans.modules.parsing.spi.indexing.Indexable
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 *  org.openide.util.Pair
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.java.source.indexing;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.api.JavacTrees;
import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.model.JavacTypes;
import com.sun.tools.javac.util.Context;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import javax.lang.model.element.TypeElement;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.api.java.platform.JavaPlatformManager;
import org.netbeans.api.java.queries.SourceLevelQuery;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.queries.FileEncodingQuery;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaIndexerPlugin;
import org.netbeans.modules.java.source.JavaFileFilterQuery;
import org.netbeans.modules.java.source.JavaSourceAccessor;
import org.netbeans.modules.java.source.indexing.CheckSums;
import org.netbeans.modules.java.source.indexing.FQN2Files;
import org.netbeans.modules.java.source.indexing.JavaCustomIndexer;
import org.netbeans.modules.java.source.indexing.TransactionContext;
import org.netbeans.modules.java.source.parsing.InferableJavaFileObject;
import org.netbeans.modules.java.source.parsing.PrefetchableJavaFileObject;
import org.netbeans.modules.java.source.parsing.ProcessorGenerated;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;
import org.netbeans.modules.java.source.usages.ClassIndexManager;
import org.netbeans.modules.java.source.usages.ClasspathInfoAccessor;
import org.netbeans.modules.java.source.usages.SourceAnalyzerFactory;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;
import org.openide.util.Pair;
import org.openide.util.lookup.Lookups;

final class JavaParsingContext {
    private final org.netbeans.modules.parsing.spi.indexing.Context ctx;
    private final boolean rootNotNeeded;
    private final ClasspathInfo cpInfo;
    private final ClassIndexImpl uq;
    private SourceLevelQuery.Result sourceLevel;
    private boolean sourceLevelInitialized;
    private JavaFileFilterImplementation filter;
    private boolean filterInitialized;
    private Charset encoding;
    private boolean encodingInitialized;
    private SourceAnalyzerFactory.StorableAnalyzer sa;
    private CheckSums checkSums;
    private FQN2Files fqn2Files;
    private Iterable<? extends JavaIndexerPlugin> pluginsCache;
    private ProcessorGenerated processorGenerated;

    JavaParsingContext(org.netbeans.modules.parsing.spi.indexing.Context context, boolean allowNonExistentRoot) throws IOException {
        this.ctx = context;
        this.rootNotNeeded = allowNonExistentRoot && context.getRoot() == null;
        this.uq = ClassIndexManager.getDefault().createUsagesQuery(context.getRootURI(), true);
        if (!this.rootNotNeeded) {
            ClassPath srcPath;
            ClassPath compilePath;
            ClassPath bootPath = ClassPath.getClassPath((FileObject)this.ctx.getRoot(), (String)"classpath/boot");
            if (bootPath == null) {
                bootPath = JavaPlatformManager.getDefault().getDefaultPlatform().getBootstrapLibraries();
            }
            if ((compilePath = ClassPath.getClassPath((FileObject)this.ctx.getRoot(), (String)"classpath/compile")) == null) {
                compilePath = ClassPath.EMPTY;
            }
            if ((srcPath = ClassPath.getClassPath((FileObject)this.ctx.getRoot(), (String)"classpath/source")) == null) {
                srcPath = ClassPath.EMPTY;
            }
            this.cpInfo = ClasspathInfoAccessor.getINSTANCE().create(bootPath, compilePath, srcPath, null, true, context.isSourceForBinaryRootIndexing(), false, context.checkForEditorModifications());
        } else {
            this.cpInfo = null;
        }
    }

    public JavaParsingContext(org.netbeans.modules.parsing.spi.indexing.Context context, ClassPath bootPath, ClassPath compilePath, ClassPath sourcePath, Collection<? extends JavaCustomIndexer.CompileTuple> virtualSources) throws IOException {
        this.ctx = context;
        this.rootNotNeeded = false;
        this.uq = ClassIndexManager.getDefault().createUsagesQuery(context.getRootURI(), true);
        this.cpInfo = ClasspathInfoAccessor.getINSTANCE().create(bootPath, compilePath, sourcePath, this.filter, true, context.isSourceForBinaryRootIndexing(), !virtualSources.isEmpty(), context.checkForEditorModifications());
        JavaParsingContext.registerVirtualSources(this.cpInfo, virtualSources);
    }

    @CheckForNull
    ClasspathInfo getClasspathInfo() {
        return this.cpInfo;
    }

    @CheckForNull
    ClassIndexImpl getClassIndexImpl() throws IOException {
        return this.uq;
    }

    @CheckForNull
    String getSourceLevel() {
        SourceLevelQuery.Result sl = this.initSourceLevel();
        return sl == null ? null : sl.getSourceLevel();
    }

    @CheckForNull
    SourceLevelQuery.Profile getProfile() {
        SourceLevelQuery.Result sl = this.initSourceLevel();
        return sl == null ? null : sl.getProfile();
    }

    @CheckForNull
    JavaFileFilterImplementation getJavaFileFilter() {
        if (!this.filterInitialized) {
            this.filter = this.rootNotNeeded ? null : JavaFileFilterQuery.getFilter(this.ctx.getRoot());
            this.filterInitialized = true;
        }
        return this.filter;
    }

    @CheckForNull
    Charset getEncoding() {
        if (!this.encodingInitialized) {
            this.encoding = this.rootNotNeeded ? null : FileEncodingQuery.getEncoding((FileObject)this.ctx.getRoot());
            this.encodingInitialized = true;
        }
        return this.encoding;
    }

    @CheckForNull
    SourceAnalyzerFactory.StorableAnalyzer getSourceAnalyzer() throws IOException {
        if (this.sa == null) {
            this.sa = this.uq == null ? null : this.uq.getSourceAnalyser();
        }
        return this.sa;
    }

    @NonNull
    CheckSums getCheckSums() throws IOException {
        if (this.checkSums == null) {
            try {
                this.checkSums = CheckSums.forContext(this.ctx);
            }
            catch (NoSuchAlgorithmException e) {
                throw new IOException(e);
            }
        }
        return this.checkSums;
    }

    @NonNull
    FQN2Files getFQNs() throws IOException {
        if (this.fqn2Files == null) {
            this.fqn2Files = FQN2Files.forRoot(this.ctx.getRootURI());
        }
        return this.fqn2Files;
    }

    @NonNull
    ProcessorGenerated getProcessorGeneratedFiles() {
        if (this.processorGenerated == null) {
            this.processorGenerated = TransactionContext.get().get(ProcessorGenerated.class);
        }
        return this.processorGenerated;
    }

    void analyze(@NonNull Iterable<? extends CompilationUnitTree> trees, @NonNull JavacTaskImpl jt, @NonNull JavaCustomIndexer.CompileTuple active, @NonNull Set<? super ElementHandle<TypeElement>> newTypes, @NonNull boolean[] mainMethod) throws IOException {
        SourceAnalyzerFactory.StorableAnalyzer analyzer = this.getSourceAnalyzer();
        assert (analyzer != null);
        analyzer.analyse(trees, jt, active, newTypes, mainMethod);
        Lookup pluginServices = this.getPluginServices(jt);
        for (CompilationUnitTree cu : trees) {
            for (JavaIndexerPlugin plugin : this.getPlugins()) {
                plugin.process(cu, active.indexable, pluginServices);
            }
        }
    }

    void delete(@NonNull Indexable indexable, @NonNull List<Pair<String, String>> toDelete) throws IOException {
        for (Pair<String, String> pair : toDelete) {
            SourceAnalyzerFactory.StorableAnalyzer analyzer = this.getSourceAnalyzer();
            assert (analyzer != null);
            analyzer.delete(pair);
        }
        for (JavaIndexerPlugin plugin : this.getPlugins()) {
            plugin.delete(indexable);
        }
    }

    void store() throws IOException {
        if (this.checkSums != null) {
            this.checkSums.store();
        }
        if (this.fqn2Files != null) {
            this.fqn2Files.store();
        }
        if (this.sa != null) {
            try {
                this.sa.store();
            }
            catch (IOException ioe) {
                throw new BrokenIndexException(ioe);
            }
        }
    }

    void finish() {
        for (JavaIndexerPlugin plugin : this.getPlugins()) {
            plugin.finish();
        }
    }

    @NonNull
    private Iterable<? extends JavaIndexerPlugin> getPlugins() {
        if (this.pluginsCache == null) {
            this.pluginsCache = JavaParsingContext.createPlugins(this.ctx.getRootURI(), this.ctx.getIndexFolder());
        }
        return this.pluginsCache;
    }

    private static Iterable<? extends JavaIndexerPlugin> createPlugins(@NonNull URL root, @NonNull FileObject cacheFolder) {
        ArrayList<JavaIndexerPlugin> plugins = new ArrayList<JavaIndexerPlugin>();
        for (JavaIndexerPlugin.Factory factory : MimeLookup.getLookup((MimePath)MimePath.parse((String)"text/x-java")).lookupAll(JavaIndexerPlugin.Factory.class)) {
            JavaIndexerPlugin plugin = factory.create(root, cacheFolder);
            if (plugin == null) continue;
            plugins.add(plugin);
        }
        return plugins;
    }

    @NonNull
    private Lookup getPluginServices(JavacTaskImpl jt) {
        return Lookups.fixed((Object[])new Object[]{jt.getElements(), jt.getTypes(), JavacTrees.instance((Context)jt.getContext()), JavaSourceAccessor.getINSTANCE().createElementUtilities(jt)});
    }

    private SourceLevelQuery.Result initSourceLevel() {
        if (!this.sourceLevelInitialized) {
            this.sourceLevel = this.rootNotNeeded ? null : SourceLevelQuery.getSourceLevel2((FileObject)this.ctx.getRoot());
            this.sourceLevelInitialized = true;
        }
        return this.sourceLevel;
    }

    private static void registerVirtualSources(ClasspathInfo cpInfo, Collection<? extends JavaCustomIndexer.CompileTuple> virtualSources) {
        for (JavaCustomIndexer.CompileTuple compileTuple : virtualSources) {
            ClasspathInfoAccessor.getINSTANCE().registerVirtualSource(cpInfo, compileTuple.jfo);
        }
    }

    static class BrokenIndexException
    extends IOException {
        private BrokenIndexException(IOException cause) {
            super(cause);
        }
    }

}

