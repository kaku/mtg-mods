/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.Pair
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.java.source.usages;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.java.source.classpath.AptCacheForSourceQuery;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.indexing.TransactionContext;
import org.netbeans.modules.java.source.usages.ClassIndexEventsTransaction;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;
import org.netbeans.modules.java.source.usages.ClassIndexManagerEvent;
import org.netbeans.modules.java.source.usages.ClassIndexManagerListener;
import org.netbeans.modules.java.source.usages.PersistentClassIndex;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Pair;
import org.openide.util.Parameters;

public final class ClassIndexManager {
    public static final String PROP_DIRTY_ROOT = "dirty";
    public static final String PROP_SOURCE_ROOT = "source";
    private static final Logger LOG = Logger.getLogger(ClassIndexManager.class.getName());
    private static ClassIndexManager instance;
    private final Map<URL, ClassIndexImpl> instances = new HashMap<URL, ClassIndexImpl>();
    private final Map<URL, ClassIndexImpl> transientInstances = new HashMap<URL, ClassIndexImpl>();
    private final InternalLock internalLock;
    private final Map<ClassIndexManagerListener, Void> listeners;
    private boolean invalid;

    private ClassIndexManager() {
        this.internalLock = new InternalLock();
        this.listeners = Collections.synchronizedMap(new IdentityHashMap());
    }

    public void addClassIndexManagerListener(ClassIndexManagerListener listener) {
        assert (listener != null);
        this.listeners.put(listener, null);
    }

    public void removeClassIndexManagerListener(ClassIndexManagerListener listener) {
        assert (listener != null);
        this.listeners.remove(listener);
    }

    @CheckForNull
    public ClassIndexImpl getUsagesQuery(final @NonNull URL root, final boolean beforeCreateAllowed) {
        final ClassIndexImpl[] index = new ClassIndexImpl[]{null};
        FileUtil.runAtomicAction((Runnable)new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                InternalLock internalLock = ClassIndexManager.this.internalLock;
                synchronized (internalLock) {
                    assert (root != null);
                    if (ClassIndexManager.this.invalid) {
                        return;
                    }
                    Pair pair = ClassIndexManager.this.getClassIndex(root, beforeCreateAllowed, false);
                    index[0] = (ClassIndexImpl)pair.first();
                    if (index[0] != null) {
                        return;
                    }
                    URL translatedRoot = AptCacheForSourceQuery.getSourceFolder(root);
                    if (translatedRoot != null) {
                        pair = ClassIndexManager.this.getClassIndex(translatedRoot, beforeCreateAllowed, false);
                        index[0] = (ClassIndexImpl)pair.first();
                        if (index[0] != null) {
                            return;
                        }
                    } else {
                        translatedRoot = root;
                    }
                    if (beforeCreateAllowed) {
                        try {
                            String typeAttr = JavaIndex.getAttribute(translatedRoot, "source", null);
                            String dirtyAttr = JavaIndex.getAttribute(translatedRoot, "dirty", null);
                            if (!Boolean.TRUE.toString().equals(dirtyAttr)) {
                                if (Boolean.TRUE.toString().equals(typeAttr)) {
                                    index[0] = PersistentClassIndex.create(root, JavaIndex.getIndex(root), ClassIndexImpl.Type.SOURCE, ClassIndexImpl.Type.SOURCE);
                                    ClassIndexManager.this.transientInstances.put(root, index[0]);
                                } else if (Boolean.FALSE.toString().equals(typeAttr)) {
                                    index[0] = PersistentClassIndex.create(root, JavaIndex.getIndex(root), ClassIndexImpl.Type.BINARY, ClassIndexImpl.Type.BINARY);
                                    ClassIndexManager.this.transientInstances.put(root, index[0]);
                                }
                            } else {
                                LOG.log(Level.FINE, "Index for root: {0} is broken.", root);
                            }
                        }
                        catch (IOException ioe) {
                        }
                        catch (IllegalStateException ise) {
                            // empty catch block
                        }
                    }
                }
            }
        });
        return index[0];
    }

    @CheckForNull
    public ClassIndexImpl createUsagesQuery(@NonNull URL root, boolean source) throws IOException {
        TransactionContext txc = TransactionContext.get();
        if (txc == null) {
            throw new IllegalStateException("Not in transaction");
        }
        return this.createUsagesQuery(root, source, txc.get(ClassIndexEventsTransaction.class));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @CheckForNull
    public ClassIndexImpl createUsagesQuery(@NonNull URL root, boolean source, @NonNull ClassIndexEventsTransaction cietx) throws IOException {
        Parameters.notNull((CharSequence)"root", (Object)root);
        Parameters.notNull((CharSequence)"cietx", (Object)cietx);
        InternalLock internalLock = this.internalLock;
        synchronized (internalLock) {
            if (this.invalid) {
                return null;
            }
            Pair<ClassIndexImpl, Boolean> pair = this.getClassIndex(root, true, true);
            ClassIndexImpl qi = (ClassIndexImpl)pair.first();
            if (qi == null && (qi = this.getUsagesQuery(root, true)) == null) {
                qi = PersistentClassIndex.create(root, JavaIndex.getIndex(root), ClassIndexImpl.Type.EMPTY, source ? ClassIndexImpl.Type.SOURCE : ClassIndexImpl.Type.BINARY);
                this.instances.put(root, qi);
                this.markAddedRoot(cietx, root);
            }
            if (source && qi.getType() == ClassIndexImpl.Type.BINARY) {
                qi.close();
                qi = PersistentClassIndex.create(root, JavaIndex.getIndex(root), ClassIndexImpl.Type.SOURCE, ClassIndexImpl.Type.SOURCE);
                this.instances.put(root, qi);
                this.transientInstances.remove(root);
                this.markAddedRoot(cietx, root);
            } else if (((Boolean)pair.second()).booleanValue()) {
                this.markAddedRoot(cietx, root);
            }
            return qi;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeRoot(URL root) throws IOException {
        InternalLock internalLock = this.internalLock;
        synchronized (internalLock) {
            ClassIndexImpl ci = this.instances.remove(root);
            if (ci == null) {
                ci = this.transientInstances.remove(root);
            } else assert (!this.transientInstances.containsKey(root));
            if (ci != null) {
                ci.close();
                this.markRemovedRoot(root);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void close() {
        InternalLock internalLock = this.internalLock;
        synchronized (internalLock) {
            this.invalid = true;
            for (ClassIndexImpl ci : this.instances.values()) {
                try {
                    ci.close();
                }
                catch (IOException ioe) {
                    Exceptions.printStackTrace((Throwable)ioe);
                }
            }
        }
    }

    void fire(@NonNull Set<? extends URL> added, @NonNull Set<? extends URL> removed) {
        ClassIndexManagerEvent addEvent = added.isEmpty() ? null : new ClassIndexManagerEvent(this, added);
        ClassIndexManagerEvent rmEvent = removed.isEmpty() ? null : new ClassIndexManagerEvent(this, removed);
        this.fire(addEvent, rmEvent);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fire(@NullAllowed ClassIndexManagerEvent addEvent, @NullAllowed ClassIndexManagerEvent rmEvent) {
        if (!this.listeners.isEmpty()) {
            ClassIndexManagerListener[] _listeners;
            Map<ClassIndexManagerListener, Void> map = this.listeners;
            synchronized (map) {
                _listeners = this.listeners.keySet().toArray(new ClassIndexManagerListener[this.listeners.size()]);
            }
            for (ClassIndexManagerListener listener : _listeners) {
                if (addEvent != null) {
                    listener.classIndexAdded(addEvent);
                }
                if (rmEvent == null) continue;
                listener.classIndexRemoved(rmEvent);
            }
        }
    }

    @NonNull
    private Pair<ClassIndexImpl, Boolean> getClassIndex(URL root, boolean allowTransient, boolean promote) {
        ClassIndexImpl index = this.instances.get(root);
        boolean promoted = false;
        if (index == null && allowTransient) {
            if (promote) {
                index = this.transientInstances.remove(root);
                if (index != null) {
                    this.instances.put(root, index);
                    promoted = true;
                }
            } else {
                index = this.transientInstances.get(root);
            }
        }
        return Pair.of((Object)index, (Object)promoted);
    }

    private void markAddedRoot(@NonNull ClassIndexEventsTransaction cietx, @NonNull URL root) {
        cietx.rootAdded(root);
    }

    private void markRemovedRoot(@NonNull URL root) {
        TransactionContext txCtx = TransactionContext.get();
        txCtx.get(ClassIndexEventsTransaction.class).rootRemoved(root);
    }

    public static synchronized ClassIndexManager getDefault() {
        if (instance == null) {
            instance = new ClassIndexManager();
        }
        return instance;
    }

    private class InternalLock {
        private InternalLock() {
        }
    }

}

