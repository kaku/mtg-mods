/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.UserTask
 */
package org.netbeans.modules.java.source.parsing;

import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.modules.java.source.parsing.ClasspathInfoProvider;
import org.netbeans.modules.parsing.api.UserTask;

public abstract class ClasspathInfoTask
extends UserTask
implements ClasspathInfoProvider {
    private final ClasspathInfo cpInfo;

    protected ClasspathInfoTask(ClasspathInfo cpInfo) {
        this.cpInfo = cpInfo;
    }

    @Override
    public ClasspathInfo getClasspathInfo() {
        return this.cpInfo;
    }
}

