/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.code.Symbol$TypeSymbol
 *  com.sun.tools.javac.code.Symtab
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.code.TypeTag
 *  com.sun.tools.javac.code.Types
 *  com.sun.tools.javac.comp.AttrContext
 *  com.sun.tools.javac.comp.Enter
 *  com.sun.tools.javac.comp.Env
 *  com.sun.tools.javac.model.JavacElements
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCClassDecl
 *  com.sun.tools.javac.tree.JCTree$JCCompilationUnit
 *  com.sun.tools.javac.tree.TreeScanner
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.CouplingAbort
 *  com.sun.tools.javac.util.FatalError
 *  com.sun.tools.javac.util.JCDiagnostic
 *  com.sun.tools.javac.util.Log
 *  com.sun.tools.javac.util.MissingPlatformError
 *  com.sun.tools.javac.util.Name
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.queries.SourceLevelQuery
 *  org.netbeans.api.java.queries.SourceLevelQuery$Profile
 *  org.netbeans.lib.nbjavac.services.CancelAbort
 *  org.netbeans.lib.nbjavac.services.CancelService
 *  org.netbeans.modules.parsing.lucene.support.LowMemoryWatcher
 *  org.netbeans.modules.parsing.spi.indexing.Context
 *  org.netbeans.modules.parsing.spi.indexing.Indexable
 *  org.netbeans.modules.parsing.spi.indexing.SuspendStatus
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package org.netbeans.modules.java.source.indexing;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Symtab;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.TypeTag;
import com.sun.tools.javac.code.Types;
import com.sun.tools.javac.comp.AttrContext;
import com.sun.tools.javac.comp.Enter;
import com.sun.tools.javac.comp.Env;
import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeScanner;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.CouplingAbort;
import com.sun.tools.javac.util.FatalError;
import com.sun.tools.javac.util.JCDiagnostic;
import com.sun.tools.javac.util.Log;
import com.sun.tools.javac.util.MissingPlatformError;
import com.sun.tools.javac.util.Name;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.tools.FileObject;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.queries.SourceLevelQuery;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.lib.nbjavac.services.CancelAbort;
import org.netbeans.lib.nbjavac.services.CancelService;
import org.netbeans.modules.java.source.TreeLoader;
import org.netbeans.modules.java.source.indexing.APTUtils;
import org.netbeans.modules.java.source.indexing.CheckSums;
import org.netbeans.modules.java.source.indexing.CompileWorker;
import org.netbeans.modules.java.source.indexing.DiagnosticListenerImpl;
import org.netbeans.modules.java.source.indexing.FQN2Files;
import org.netbeans.modules.java.source.indexing.JavaCustomIndexer;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.indexing.JavaParsingContext;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.JavacParser;
import org.netbeans.modules.java.source.parsing.OutputFileManager;
import org.netbeans.modules.java.source.parsing.PrefetchableJavaFileObject;
import org.netbeans.modules.java.source.usages.ClassNamesForFileOraculumImpl;
import org.netbeans.modules.java.source.usages.ExecutableFilesIndex;
import org.netbeans.modules.parsing.lucene.support.LowMemoryWatcher;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.netbeans.modules.parsing.spi.indexing.SuspendStatus;
import org.openide.filesystems.FileUtil;

final class MultiPassCompileWorker
extends CompileWorker {
    private static final int MEMORY_LOW = 1;
    private static final int ERR = 2;
    private boolean checkForMemLow = true;

    MultiPassCompileWorker() {
    }

    @Override
    CompileWorker.ParsingOutput compile(CompileWorker.ParsingOutput previous, final org.netbeans.modules.parsing.spi.indexing.Context context, JavaParsingContext javaContext, Collection<? extends JavaCustomIndexer.CompileTuple> files) {
        LinkedList<JavaCustomIndexer.CompileTuple> toProcess = new LinkedList<JavaCustomIndexer.CompileTuple>();
        HashMap<PrefetchableJavaFileObject, JavaCustomIndexer.CompileTuple> jfo2tuples = new HashMap<PrefetchableJavaFileObject, JavaCustomIndexer.CompileTuple>();
        for (JavaCustomIndexer.CompileTuple i : files) {
            if (previous.finishedFiles.contains((Object)i.indexable)) continue;
            toProcess.add(i);
            jfo2tuples.put(i.jfo, i);
        }
        if (toProcess.isEmpty()) {
            return CompileWorker.ParsingOutput.success(previous.file2FQNs, previous.addedTypes, previous.createdFiles, previous.finishedFiles, previous.modifiedTypes, previous.aptGenerated);
        }
        ClassNamesForFileOraculumImpl cnffOraculum = new ClassNamesForFileOraculumImpl(previous.file2FQNs);
        final LowMemoryWatcher mem = LowMemoryWatcher.getInstance();
        DiagnosticListenerImpl diagnosticListener = new DiagnosticListenerImpl();
        LinkedList<JavaCustomIndexer.CompileTuple> bigFiles = new LinkedList<JavaCustomIndexer.CompileTuple>();
        JavacTaskImpl jt = null;
        JavaCustomIndexer.CompileTuple active = null;
        boolean aptEnabled = false;
        int state = 0;
        boolean isBigFile = false;
        while (!toProcess.isEmpty() || !bigFiles.isEmpty() || active != null) {
            ClassPath bootPath;
            String message;
            ClassPath classPath;
            ClassPath sourcePath;
            if (context.isCancelled()) {
                return null;
            }
            try {
                context.getSuspendStatus().parkWhileSuspended();
            }
            catch (InterruptedException ex) {
                // empty catch block
            }
            try {
                try {
                    if (mem.isLowMemory()) {
                        this.dumpSymFiles(jt, previous.createdFiles, context);
                        mem.isLowMemory();
                        jt = null;
                        diagnosticListener.cleanDiagnostics();
                        if ((state & 1) != 0) break;
                        state |= 1;
                        mem.free();
                        continue;
                    }
                    if (active == null) {
                        if (!toProcess.isEmpty()) {
                            active = (JavaCustomIndexer.CompileTuple)toProcess.removeFirst();
                            if (active == null || previous.finishedFiles.contains((Object)active.indexable)) continue;
                            isBigFile = false;
                        } else {
                            active = (JavaCustomIndexer.CompileTuple)bigFiles.removeFirst();
                            isBigFile = true;
                        }
                    }
                    if (jt == null) {
                        jt = JavacParser.createJavacTask(javaContext.getClasspathInfo(), diagnosticListener, javaContext.getSourceLevel(), javaContext.getProfile(), cnffOraculum, javaContext.getFQNs(), new CancelService(){

                            public boolean isCanceled() {
                                return context.isCancelled() || MultiPassCompileWorker.this.checkForMemLow && mem.isLowMemory();
                            }
                        }, active.aptGenerated ? null : APTUtils.get(context.getRoot()));
                        Iterable processors = jt.getProcessors();
                        boolean bl = aptEnabled = processors != null && processors.iterator().hasNext();
                        if (JavaIndex.LOG.isLoggable(Level.FINER)) {
                            JavaIndex.LOG.finer("Created new JavacTask for: " + FileUtil.getFileDisplayName((org.openide.filesystems.FileObject)context.getRoot()) + " " + javaContext.getClasspathInfo().toString());
                        }
                    }
                    Iterable trees = jt.parse(new JavaFileObject[]{active.jfo});
                    if (mem.isLowMemory()) {
                        this.dumpSymFiles(jt, previous.createdFiles, context);
                        mem.isLowMemory();
                        jt = null;
                        diagnosticListener.cleanDiagnostics();
                        trees = null;
                        if ((state & 1) != 0) {
                            if (isBigFile) break;
                            bigFiles.add(active);
                            active = null;
                            state &= -2;
                        } else {
                            state |= 1;
                        }
                        mem.free();
                        continue;
                    }
                    Iterable types = jt.enterTrees(trees);
                    if (jfo2tuples.remove(active.jfo) != null) {
                        class ScanNested
                        extends TreeScanner {
                            Set<JavaCustomIndexer.CompileTuple> dependencies;
                            final /* synthetic */ Types val$ts;
                            final /* synthetic */ HashMap val$jfo2tuples;
                            final /* synthetic */ CompileWorker.ParsingOutput val$previous;
                            final /* synthetic */ Indexable val$activeIndexable;

                            ScanNested() {
                                this.val$ts = var2_2;
                                this.val$jfo2tuples = var3_3;
                                this.val$previous = var4_4;
                                this.val$activeIndexable = var5_5;
                                this.dependencies = new LinkedHashSet<JavaCustomIndexer.CompileTuple>();
                            }

                            public void visitClassDef(JCTree.JCClassDecl node) {
                                if (node.sym != null) {
                                    Type st = this.val$ts.supertype(node.sym.type);
                                    boolean envForSuperTypeFound = false;
                                    while (!envForSuperTypeFound && st != null && st.hasTag(TypeTag.CLASS)) {
                                        Symbol.ClassSymbol c = st.tsym.outermostClass();
                                        JavaCustomIndexer.CompileTuple u = (JavaCustomIndexer.CompileTuple)this.val$jfo2tuples.get(c.sourcefile);
                                        if (u != null && !this.val$previous.finishedFiles.contains((Object)u.indexable) && !u.indexable.equals((Object)this.val$activeIndexable)) {
                                            this.dependencies.add(u);
                                            envForSuperTypeFound = true;
                                        }
                                        st = this.val$ts.supertype(st);
                                    }
                                }
                                super.visitClassDef(node);
                            }
                        }
                        Types ts = Types.instance((Context)jt.getContext());
                        Indexable activeIndexable = active.indexable;
                        ScanNested scanner = new ScanNested(this, ts, jfo2tuples, previous, activeIndexable);
                        for (CompilationUnitTree cut : trees) {
                            scanner.scan((JCTree)((JCTree.JCCompilationUnit)cut));
                        }
                        if (!scanner.dependencies.isEmpty()) {
                            toProcess.addFirst(active);
                            for (JavaCustomIndexer.CompileTuple tuple : scanner.dependencies) {
                                toProcess.addFirst(tuple);
                            }
                            active = null;
                            continue;
                        }
                    }
                    if (mem.isLowMemory()) {
                        this.dumpSymFiles(jt, previous.createdFiles, context);
                        mem.isLowMemory();
                        jt = null;
                        diagnosticListener.cleanDiagnostics();
                        trees = null;
                        types = null;
                        if ((state & 1) != 0) {
                            if (isBigFile) break;
                            bigFiles.add(active);
                            active = null;
                            state &= -2;
                        } else {
                            state |= 1;
                        }
                        mem.free();
                        continue;
                    }
                    jt.analyze(types);
                    if (aptEnabled) {
                        JavaCustomIndexer.addAptGenerated(context, javaContext, active, previous.aptGenerated);
                    }
                    if (mem.isLowMemory()) {
                        this.dumpSymFiles(jt, previous.createdFiles, context);
                        mem.isLowMemory();
                        jt = null;
                        diagnosticListener.cleanDiagnostics();
                        trees = null;
                        types = null;
                        if ((state & 1) != 0) {
                            if (isBigFile) break;
                            bigFiles.add(active);
                            active = null;
                            state &= -2;
                        } else {
                            state |= 1;
                        }
                        mem.free();
                        continue;
                    }
                    javaContext.getFQNs().set(types, active.indexable.getURL());
                    boolean[] main = new boolean[1];
                    if (javaContext.getCheckSums().checkAndSet(active.indexable.getURL(), types, (Elements)jt.getElements()) || context.isSupplementaryFilesIndexing()) {
                        javaContext.analyze(trees, jt, active, previous.addedTypes, main);
                    } else {
                        HashSet aTypes = new HashSet();
                        javaContext.analyze(trees, jt, active, aTypes, main);
                        previous.addedTypes.addAll(aTypes);
                        previous.modifiedTypes.addAll(aTypes);
                    }
                    ExecutableFilesIndex.DEFAULT.setMainClass(context.getRoot().getURL(), active.indexable.getURL(), main[0]);
                    JavaCustomIndexer.setErrors(context, active, diagnosticListener);
                    Iterable generatedFiles = jt.generate(types);
                    if (!active.virtual) {
                        for (JavaFileObject generated : generatedFiles) {
                            if (!(generated instanceof FileObjects.FileBase)) continue;
                            previous.createdFiles.add(((FileObjects.FileBase)generated).getFile());
                        }
                    }
                    Log.instance((Context)jt.getContext()).nerrors = 0;
                    previous.finishedFiles.add(active.indexable);
                    active = null;
                    state = 0;
                }
                catch (CancelAbort ca) {
                    if (mem.isLowMemory()) {
                        this.dumpSymFiles(jt, previous.createdFiles, context);
                        mem.isLowMemory();
                        jt = null;
                        diagnosticListener.cleanDiagnostics();
                        if ((state & 1) != 0) {
                            if (isBigFile) break;
                            bigFiles.add(active);
                            active = null;
                            state &= -2;
                        } else {
                            state |= 1;
                        }
                        mem.free();
                        continue;
                    }
                    if (!JavaIndex.LOG.isLoggable(Level.FINEST)) continue;
                    JavaIndex.LOG.log(Level.FINEST, "OnePassCompileWorker was canceled in root: " + FileUtil.getFileDisplayName((org.openide.filesystems.FileObject)context.getRoot()), (Throwable)ca);
                }
                continue;
            }
            catch (CouplingAbort ca) {
                TreeLoader.dumpCouplingAbort(ca, null);
                jt = null;
                diagnosticListener.cleanDiagnostics();
                if ((state & 2) != 0) {
                    if (active != null) {
                        previous.finishedFiles.add(active.indexable);
                    }
                    active = null;
                    state = 0;
                    continue;
                }
                state |= 2;
                continue;
            }
            catch (OutputFileManager.InvalidSourcePath isp) {
                if (JavaIndex.LOG.isLoggable(Level.FINEST)) {
                    bootPath = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.BOOT);
                    classPath = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.COMPILE);
                    sourcePath = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.SOURCE);
                    Object[] arrobject = new Object[5];
                    arrobject[0] = active.jfo.toUri().toString();
                    arrobject[1] = FileUtil.getFileDisplayName((org.openide.filesystems.FileObject)context.getRoot());
                    arrobject[2] = bootPath == null ? null : bootPath.toString();
                    arrobject[3] = classPath == null ? null : classPath.toString();
                    arrobject[4] = sourcePath == null ? null : sourcePath.toString();
                    message = String.format("MultiPassCompileWorker caused an exception\nFile: %s\nRoot: %s\nBootpath: %s\nClasspath: %s\nSourcepath: %s", arrobject);
                    JavaIndex.LOG.log(Level.FINEST, message, isp);
                }
                return CompileWorker.ParsingOutput.failure(previous.file2FQNs, previous.addedTypes, previous.createdFiles, previous.finishedFiles, previous.modifiedTypes, previous.aptGenerated);
            }
            catch (MissingPlatformError mpe) {
                if (JavaIndex.LOG.isLoggable(Level.FINEST)) {
                    bootPath = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.BOOT);
                    classPath = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.COMPILE);
                    sourcePath = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.SOURCE);
                    Object[] arrobject = new Object[5];
                    arrobject[0] = active.jfo.toUri().toString();
                    arrobject[1] = FileUtil.getFileDisplayName((org.openide.filesystems.FileObject)context.getRoot());
                    arrobject[2] = bootPath == null ? null : bootPath.toString();
                    arrobject[3] = classPath == null ? null : classPath.toString();
                    arrobject[4] = sourcePath == null ? null : sourcePath.toString();
                    message = String.format("MultiPassCompileWorker caused an exception\nFile: %s\nRoot: %s\nBootpath: %s\nClasspath: %s\nSourcepath: %s", arrobject);
                    JavaIndex.LOG.log(Level.FINEST, message, (Throwable)mpe);
                }
                JavaCustomIndexer.brokenPlatform(context, files, mpe.getDiagnostic());
                return CompileWorker.ParsingOutput.failure(previous.file2FQNs, previous.addedTypes, previous.createdFiles, previous.finishedFiles, previous.modifiedTypes, previous.aptGenerated);
            }
            catch (Throwable t) {
                Level level;
                if (t instanceof ThreadDeath) {
                    throw (ThreadDeath)t;
                }
                Level level2 = level = t instanceof FatalError ? Level.FINEST : Level.WARNING;
                if (JavaIndex.LOG.isLoggable(level)) {
                    ClassPath bootPath2 = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.BOOT);
                    ClassPath classPath2 = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.COMPILE);
                    ClassPath sourcePath2 = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.SOURCE);
                    Object[] arrobject = new Object[5];
                    arrobject[0] = active == null ? null : active.jfo.toUri().toString();
                    arrobject[1] = FileUtil.getFileDisplayName((org.openide.filesystems.FileObject)context.getRoot());
                    arrobject[2] = bootPath2 == null ? null : bootPath2.toString();
                    arrobject[3] = classPath2 == null ? null : classPath2.toString();
                    arrobject[4] = sourcePath2 == null ? null : sourcePath2.toString();
                    String message2 = String.format("MultiPassCompileWorker caused an exception\nFile: %s\nRoot: %s\nBootpath: %s\nClasspath: %s\nSourcepath: %s", arrobject);
                    JavaIndex.LOG.log(level, message2, t);
                }
                jt = null;
                diagnosticListener.cleanDiagnostics();
                if ((state & 2) != 0) {
                    if (active != null) {
                        previous.finishedFiles.add(active.indexable);
                    }
                    active = null;
                    state = 0;
                    continue;
                }
                state |= 2;
                continue;
            }
        }
        return !(state & true) ? CompileWorker.ParsingOutput.success(previous.file2FQNs, previous.addedTypes, previous.createdFiles, previous.finishedFiles, previous.modifiedTypes, previous.aptGenerated) : CompileWorker.ParsingOutput.lowMemory(previous.file2FQNs, previous.addedTypes, previous.createdFiles, previous.finishedFiles, previous.modifiedTypes, previous.aptGenerated);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void dumpSymFiles(JavacTaskImpl jti, Set<File> alreadyCreated, org.netbeans.modules.parsing.spi.indexing.Context ctx) throws IOException {
        if (jti != null) {
            JavaFileManager jfm = (JavaFileManager)jti.getContext().get(JavaFileManager.class);
            this.checkForMemLow = false;
            try {
                Types types = Types.instance((Context)jti.getContext());
                Enter enter = Enter.instance((Context)jti.getContext());
                Symtab syms = Symtab.instance((Context)jti.getContext());
                HashMap<Symbol.ClassSymbol, JCTree.JCClassDecl> syms2trees = new HashMap<Symbol.ClassSymbol, JCTree.JCClassDecl>();
                HashSet<Env> processedEnvs = new HashSet<Env>();
                File classes = JavaIndex.getClassFolder(ctx);
                for (Env env : jti.getTodo()) {
                    class ScanNested
                    extends TreeScanner {
                        private Env<AttrContext> env;
                        private Set<Env<AttrContext>> checked;
                        private List<Env<AttrContext>> dependencies;
                        final /* synthetic */ Types val$types;
                        final /* synthetic */ Enter val$enter;
                        final /* synthetic */ Symtab val$syms;
                        final /* synthetic */ HashMap val$syms2trees;

                        public ScanNested() {
                            this.val$types = var3_3;
                            this.val$enter = var4_4;
                            this.val$syms = var5_5;
                            this.val$syms2trees = env2;
                            this.checked = new HashSet<Env<AttrContext>>();
                            this.dependencies = new LinkedList<Env<AttrContext>>();
                            this.env = env;
                        }

                        public void visitClassDef(JCTree.JCClassDecl node) {
                            if (node.sym != null) {
                                Type st = this.val$types.supertype(node.sym.type);
                                boolean envForSuperTypeFound = false;
                                while (!envForSuperTypeFound && st != null && st.hasTag(TypeTag.CLASS)) {
                                    Symbol.ClassSymbol c = st.tsym.outermostClass();
                                    Env stEnv = this.val$enter.getEnv((Symbol.TypeSymbol)c);
                                    if (stEnv != null && this.env != stEnv) {
                                        if (this.checked.add((Env)stEnv)) {
                                            this.scan(stEnv.tree);
                                            if (TreeLoader.pruneTree(stEnv.tree, this.val$syms, this.val$syms2trees)) {
                                                this.dependencies.add((Env)stEnv);
                                            }
                                        }
                                        envForSuperTypeFound = true;
                                    }
                                    st = this.val$types.supertype(st);
                                }
                            }
                            super.visitClassDef(node);
                        }
                    }
                    if (!processedEnvs.add(env)) continue;
                    ScanNested scanner = new ScanNested(this, env, types, enter, syms, syms2trees);
                    scanner.scan(env.tree);
                    for (Env dep : scanner.dependencies) {
                        if (!processedEnvs.add(dep)) continue;
                        this.dumpSymFile(jfm, jti, dep.enclClass.sym, alreadyCreated, classes, syms2trees);
                    }
                    if (!TreeLoader.pruneTree(env.tree, syms, syms2trees)) continue;
                    this.dumpSymFile(jfm, jti, env.enclClass.sym, alreadyCreated, classes, syms2trees);
                }
            }
            finally {
                this.checkForMemLow = true;
            }
        }
    }

    private void dumpSymFile(@NonNull JavaFileManager jfm, @NonNull JavacTaskImpl jti, @NullAllowed Symbol.ClassSymbol cs, @NonNull Set<File> alreadyCreated, @NonNull File classes, @NonNull HashMap<Symbol.ClassSymbol, JCTree.JCClassDecl> syms2trees) throws IOException {
        if (cs == null) {
            return;
        }
        JavaFileObject file = jfm.getJavaFileForOutput(StandardLocation.CLASS_OUTPUT, cs.flatname.toString(), JavaFileObject.Kind.CLASS, cs.sourcefile);
        if (file instanceof FileObjects.FileBase && !alreadyCreated.contains(((FileObjects.FileBase)file).getFile())) {
            TreeLoader.dumpSymFile(jfm, jti, cs, classes, syms2trees);
        }
    }

}

