/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.api.DuplicateClassChecker
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.java.source.indexing;

import com.sun.tools.javac.api.DuplicateClassChecker;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.openide.util.Exceptions;

public final class FQN2Files
implements DuplicateClassChecker {
    private static final Logger LOG = Logger.getLogger(FQN2Files.class.getName());
    private static final String FQN2FILES_FILE = "fqn2files.properties";
    private final File propFile;
    private final Properties props = new Properties();

    public static FQN2Files forRoot(URL root) throws IOException {
        return new FQN2Files(root);
    }

    private FQN2Files(URL root) throws IOException {
        this.propFile = new File(JavaIndex.getIndex(root), "fqn2files.properties");
        this.propFile.createNewFile();
        this.load();
    }

    public void set(Iterable<? extends TypeElement> topLevelElements, URL file) {
        for (TypeElement element : topLevelElements) {
            String fqn = element.getQualifiedName().toString();
            String value = this.props.getProperty(fqn);
            if (value != null) continue;
            this.props.setProperty(fqn, file.toExternalForm());
        }
    }

    public boolean remove(String fqn, URL file) {
        String value = this.props.getProperty(fqn);
        if (value != null && value.equals(file.toExternalForm())) {
            this.props.remove(fqn);
            return true;
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void store() throws IOException {
        BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(this.propFile));
        try {
            this.props.store(out, "");
        }
        finally {
            out.close();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void load() throws IOException {
        BufferedInputStream in = new BufferedInputStream(new FileInputStream(this.propFile));
        try {
            this.props.load(in);
        }
        catch (IllegalArgumentException iae) {
            this.props.clear();
            LOG.log(Level.WARNING, "Broken {0}, ignoring.", this.propFile.getAbsolutePath());
        }
        finally {
            in.close();
        }
    }

    public boolean check(Name fqn, JavaFileObject jfo) {
        String value = this.props.getProperty(fqn.toString());
        try {
            return value != null && !value.equals(jfo.toUri().toURL().toExternalForm());
        }
        catch (MalformedURLException ex) {
            Exceptions.printStackTrace((Throwable)ex);
            return false;
        }
    }
}

