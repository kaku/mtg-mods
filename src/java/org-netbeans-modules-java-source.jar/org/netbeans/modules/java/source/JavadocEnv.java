/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.javadoc.ClassDoc
 *  com.sun.source.tree.Tree
 *  com.sun.source.util.TreePath
 *  com.sun.tools.javac.code.Scope
 *  com.sun.tools.javac.code.Scope$Entry
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.code.Symbol$MethodSymbol
 *  com.sun.tools.javac.code.Symbol$PackageSymbol
 *  com.sun.tools.javac.code.Symbol$TypeSymbol
 *  com.sun.tools.javac.code.Symbol$VarSymbol
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.code.TypeTag
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCClassDecl
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Factory
 *  com.sun.tools.javac.util.Context$Key
 *  com.sun.tools.javac.util.Name
 *  com.sun.tools.javac.util.Names
 *  com.sun.tools.javadoc.AnnotationTypeDocImpl
 *  com.sun.tools.javadoc.AnnotationTypeElementDocImpl
 *  com.sun.tools.javadoc.ClassDocImpl
 *  com.sun.tools.javadoc.ConstructorDocImpl
 *  com.sun.tools.javadoc.DocEnv
 *  com.sun.tools.javadoc.ExecutableMemberDocImpl
 *  com.sun.tools.javadoc.FieldDocImpl
 *  com.sun.tools.javadoc.MethodDocImpl
 *  com.sun.tools.javadoc.ModifierFilter
 *  com.sun.tools.javadoc.PackageDocImpl
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.java.source;

import com.sun.javadoc.ClassDoc;
import com.sun.source.tree.Tree;
import com.sun.source.util.TreePath;
import com.sun.tools.javac.code.Scope;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.TypeTag;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;
import com.sun.tools.javadoc.AnnotationTypeDocImpl;
import com.sun.tools.javadoc.AnnotationTypeElementDocImpl;
import com.sun.tools.javadoc.ClassDocImpl;
import com.sun.tools.javadoc.ConstructorDocImpl;
import com.sun.tools.javadoc.DocEnv;
import com.sun.tools.javadoc.ExecutableMemberDocImpl;
import com.sun.tools.javadoc.FieldDocImpl;
import com.sun.tools.javadoc.MethodDocImpl;
import com.sun.tools.javadoc.ModifierFilter;
import com.sun.tools.javadoc.PackageDocImpl;
import java.io.IOException;
import java.util.Map;
import java.util.StringTokenizer;
import javax.lang.model.element.Element;
import javax.lang.model.util.Elements;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.Task;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public class JavadocEnv
extends DocEnv {
    private ClasspathInfo cpInfo;
    private Context ctx;

    public static void preRegister(Context context, final ClasspathInfo cpInfo) {
        context.put(docEnvKey, (Context.Factory)new Context.Factory<DocEnv>(){

            public DocEnv make(Context c) {
                return new JavadocEnv(c, cpInfo);
            }
        });
    }

    private JavadocEnv(Context context, ClasspathInfo cpInfo) {
        super(context);
        this.ctx = context;
        this.cpInfo = cpInfo;
        this.showAccess = new ModifierFilter(-9223372036854775801L);
        this.legacyDoclet = false;
    }

    public ClassDocImpl getClassDoc(Symbol.ClassSymbol clazz) {
        if (clazz.type.hasTag(TypeTag.UNKNOWN)) {
            return null;
        }
        Object result = (ClassDocImpl)this.classMap.get((Object)clazz);
        if (result != null) {
            return result;
        }
        result = JavadocEnv.isAnnotationType((Symbol.ClassSymbol)clazz) ? new JavadocAnnotation(this, (DocEnv)this, clazz) : new JavadocClass(this, (DocEnv)this, clazz);
        this.classMap.put(clazz, result);
        return result;
    }

    protected void makeClassDoc(Symbol.ClassSymbol clazz, TreePath treePath) {
        if (clazz.type.hasTag(TypeTag.UNKNOWN)) {
            return;
        }
        Object result = (ClassDocImpl)this.classMap.get((Object)clazz);
        if (result != null) {
            if (treePath != null) {
                result.setTreePath(treePath);
            }
            return;
        }
        result = JavadocEnv.isAnnotationType((JCTree.JCClassDecl)((JCTree.JCClassDecl)treePath.getLeaf())) ? new JavadocAnnotation(this, this, clazz, treePath) : new JavadocClass(this, this, clazz, treePath);
        this.classMap.put(clazz, result);
    }

    public FieldDocImpl getFieldDoc(Symbol.VarSymbol var) {
        FieldDocImpl result = (FieldDocImpl)this.fieldMap.get((Object)var);
        if (result != null) {
            return result;
        }
        result = new JavadocField(this, (DocEnv)this, var);
        this.fieldMap.put(var, result);
        return result;
    }

    protected void makeFieldDoc(Symbol.VarSymbol var, TreePath treePath) {
        FieldDocImpl result = (FieldDocImpl)this.fieldMap.get((Object)var);
        if (result != null) {
            if (treePath != null) {
                result.setTreePath(treePath);
            }
        } else {
            result = new JavadocField(this, this, var, treePath);
            this.fieldMap.put(var, result);
        }
    }

    public MethodDocImpl getMethodDoc(Symbol.MethodSymbol meth) {
        ExecutableMemberDocImpl docImpl = (ExecutableMemberDocImpl)this.methodMap.get((Object)meth);
        if (docImpl != null && !docImpl.isMethod()) {
            return null;
        }
        MethodDocImpl result = (MethodDocImpl)docImpl;
        if (result != null) {
            return result;
        }
        result = new JavadocMethod(this, (DocEnv)this, meth);
        this.methodMap.put(meth, result);
        return result;
    }

    protected void makeMethodDoc(Symbol.MethodSymbol meth, TreePath treePath) {
        MethodDocImpl result = (MethodDocImpl)this.methodMap.get((Object)meth);
        if (result != null) {
            if (treePath != null) {
                result.setTreePath(treePath);
            }
        } else {
            result = new JavadocMethod(this, this, meth, treePath);
            this.methodMap.put(meth, result);
        }
    }

    public ConstructorDocImpl getConstructorDoc(Symbol.MethodSymbol meth) {
        ExecutableMemberDocImpl docImpl = (ExecutableMemberDocImpl)this.methodMap.get((Object)meth);
        if (docImpl != null && !docImpl.isConstructor()) {
            return null;
        }
        ConstructorDocImpl result = (ConstructorDocImpl)docImpl;
        if (result != null) {
            return result;
        }
        result = new JavadocConstructor(this, (DocEnv)this, meth);
        this.methodMap.put(meth, result);
        return result;
    }

    protected void makeConstructorDoc(Symbol.MethodSymbol meth, TreePath treePath) {
        ConstructorDocImpl result = (ConstructorDocImpl)this.methodMap.get((Object)meth);
        if (result != null) {
            if (treePath != null) {
                result.setTreePath(treePath);
            }
        } else {
            result = new JavadocConstructor(this, this, meth, treePath);
            this.methodMap.put(meth, result);
        }
    }

    public AnnotationTypeElementDocImpl getAnnotationTypeElementDoc(Symbol.MethodSymbol meth) {
        ExecutableMemberDocImpl docImpl = (ExecutableMemberDocImpl)this.methodMap.get((Object)meth);
        if (docImpl != null && !docImpl.isAnnotationTypeElement()) {
            return null;
        }
        AnnotationTypeElementDocImpl result = (AnnotationTypeElementDocImpl)docImpl;
        if (result != null) {
            return result;
        }
        result = new JavadocAnnotationTypeElement(this, (DocEnv)this, meth);
        this.methodMap.put(meth, result);
        return result;
    }

    protected void makeAnnotationTypeElementDoc(Symbol.MethodSymbol meth, TreePath treePath) {
        AnnotationTypeElementDocImpl result = (AnnotationTypeElementDocImpl)this.methodMap.get((Object)meth);
        if (result != null) {
            if (treePath != null) {
                result.setTreePath(treePath);
            }
        } else {
            result = new JavadocAnnotationTypeElement(this, this, meth, treePath);
            this.methodMap.put(meth, result);
        }
    }

    public PackageDocImpl getPackageDoc(Symbol.PackageSymbol pack) {
        PackageDocImpl result = (PackageDocImpl)this.packageMap.get((Object)pack);
        if (result != null) {
            return result;
        }
        result = new JavaDocPackage(this, pack, this.ctx);
        this.packageMap.put(pack, result);
        return result;
    }

    public ClassDocImpl lookupClass(String name) {
        ClassDocImpl cls = super.lookupClass(name);
        if (cls == null && name != null && !name.isEmpty()) {
            cls = this.loadClass(name);
        }
        return cls;
    }

    private String getRawCommentFor(Element element) {
        try {
            JavaSource js;
            FileObject fo = SourceUtils.getFile(element, this.cpInfo);
            if (fo != null && (js = JavaSource.forFileObject(fo)) != null) {
                final String[] ret = new String[1];
                final ElementHandle<Element> handle = ElementHandle.create(element);
                js.runUserActionTask(new Task<CompilationController>(){

                    @Override
                    public void run(CompilationController controller) throws Exception {
                        controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                        Object e = handle.resolve(controller);
                        if (e != null) {
                            ret[0] = controller.getElements().getDocComment((Element)e);
                        }
                    }
                }, true);
                return ret[0] != null ? ret[0] : "";
            }
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        return "";
    }

    private class JavaDocPackage
    extends PackageDocImpl
    implements ElementHolder {
        private JavaDocPackage(DocEnv env, Symbol.PackageSymbol sym, Context ctx) {
            super(env, sym);
        }

        public ClassDoc findClass(String className) {
            Names nameTable = Names.instance((Context)JavadocEnv.this.ctx);
            StringTokenizer st = new StringTokenizer(className, ".");
            Symbol.PackageSymbol s = this.sym;
            block0 : while (s != null && st.hasMoreTokens()) {
                Name clsName = nameTable.fromString(st.nextToken());
                Scope.Entry e = s.members().lookup(clsName);
                s = null;
                while (e.scope != null) {
                    if (e.sym.kind == 2 && (e.sym.flags_field & 4096) == 0) {
                        s = (Symbol.TypeSymbol)e.sym;
                        continue block0;
                    }
                    e = e.next();
                }
            }
            return s instanceof Symbol.ClassSymbol ? this.env.getClassDoc((Symbol.ClassSymbol)s) : null;
        }

        @Override
        public Element getElement() {
            return this.sym;
        }
    }

    private class JavadocAnnotationTypeElement
    extends AnnotationTypeElementDocImpl
    implements ElementHolder {
        final /* synthetic */ JavadocEnv this$0;

        private JavadocAnnotationTypeElement(JavadocEnv javadocEnv, DocEnv env, Symbol.MethodSymbol sym) {
            this.this$0 = javadocEnv;
            super(env, sym);
        }

        private JavadocAnnotationTypeElement(JavadocEnv javadocEnv, DocEnv env, Symbol.MethodSymbol sym, TreePath treePath) {
            this.this$0 = javadocEnv;
            super(env, sym, treePath);
        }

        protected String documentation() {
            if (this.documentation == null) {
                this.setRawCommentText(this.this$0.getRawCommentFor(this.getElement()));
            }
            return this.documentation;
        }

        @Override
        public Element getElement() {
            return this.sym;
        }
    }

    private class JavadocConstructor
    extends ConstructorDocImpl
    implements ElementHolder {
        final /* synthetic */ JavadocEnv this$0;

        private JavadocConstructor(JavadocEnv javadocEnv, DocEnv env, Symbol.MethodSymbol sym) {
            this.this$0 = javadocEnv;
            super(env, sym);
        }

        private JavadocConstructor(JavadocEnv javadocEnv, DocEnv env, Symbol.MethodSymbol sym, TreePath treePath) {
            this.this$0 = javadocEnv;
            super(env, sym, treePath);
        }

        protected String documentation() {
            if (this.documentation == null) {
                this.setRawCommentText(this.this$0.getRawCommentFor(this.getElement()));
            }
            return this.documentation;
        }

        @Override
        public Element getElement() {
            return this.sym;
        }
    }

    private class JavadocMethod
    extends MethodDocImpl
    implements ElementHolder {
        final /* synthetic */ JavadocEnv this$0;

        private JavadocMethod(JavadocEnv javadocEnv, DocEnv env, Symbol.MethodSymbol sym) {
            this.this$0 = javadocEnv;
            super(env, sym);
        }

        private JavadocMethod(JavadocEnv javadocEnv, DocEnv env, Symbol.MethodSymbol sym, TreePath treePath) {
            this.this$0 = javadocEnv;
            super(env, sym, treePath);
        }

        protected String documentation() {
            if (this.documentation == null) {
                this.setRawCommentText(this.this$0.getRawCommentFor(this.getElement()));
            }
            return this.documentation;
        }

        @Override
        public Element getElement() {
            return this.sym;
        }
    }

    private class JavadocField
    extends FieldDocImpl
    implements ElementHolder {
        final /* synthetic */ JavadocEnv this$0;

        private JavadocField(JavadocEnv javadocEnv, DocEnv env, Symbol.VarSymbol sym) {
            this.this$0 = javadocEnv;
            super(env, sym);
        }

        private JavadocField(JavadocEnv javadocEnv, DocEnv env, Symbol.VarSymbol sym, TreePath treePath) {
            this.this$0 = javadocEnv;
            super(env, sym, treePath);
        }

        protected String documentation() {
            if (this.documentation == null) {
                this.setRawCommentText(this.this$0.getRawCommentFor(this.getElement()));
            }
            return this.documentation;
        }

        @Override
        public Element getElement() {
            return this.sym;
        }
    }

    private class JavadocAnnotation
    extends AnnotationTypeDocImpl
    implements ElementHolder {
        final /* synthetic */ JavadocEnv this$0;

        private JavadocAnnotation(JavadocEnv javadocEnv, DocEnv env, Symbol.ClassSymbol sym) {
            this.this$0 = javadocEnv;
            super(env, sym);
        }

        private JavadocAnnotation(JavadocEnv javadocEnv, DocEnv env, Symbol.ClassSymbol sym, TreePath treePath) {
            this.this$0 = javadocEnv;
            super(env, sym, treePath);
        }

        protected String documentation() {
            if (this.documentation == null) {
                this.setRawCommentText(this.this$0.getRawCommentFor(this.getElement()));
            }
            return this.documentation;
        }

        @Override
        public Element getElement() {
            return this.tsym;
        }
    }

    private class JavadocClass
    extends ClassDocImpl
    implements ElementHolder {
        final /* synthetic */ JavadocEnv this$0;

        private JavadocClass(JavadocEnv javadocEnv, DocEnv env, Symbol.ClassSymbol sym) {
            this.this$0 = javadocEnv;
            super(env, sym);
        }

        private JavadocClass(JavadocEnv javadocEnv, DocEnv env, Symbol.ClassSymbol sym, TreePath treePath) {
            this.this$0 = javadocEnv;
            super(env, sym, treePath);
        }

        protected String documentation() {
            if (this.documentation == null) {
                this.setRawCommentText(this.this$0.getRawCommentFor(this.getElement()));
            }
            return this.documentation;
        }

        @Override
        public Element getElement() {
            return this.tsym;
        }
    }

    public static interface ElementHolder {
        public Element getElement();
    }

}

