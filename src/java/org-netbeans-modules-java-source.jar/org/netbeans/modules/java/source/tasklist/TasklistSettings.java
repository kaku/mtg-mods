/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.indexing.IndexingManager
 *  org.openide.util.NbPreferences
 */
package org.netbeans.modules.java.source.tasklist;

import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import org.netbeans.modules.parsing.api.indexing.IndexingManager;
import org.openide.util.NbPreferences;

public class TasklistSettings {
    private static final String KEY_DEPENDENCY_TRACKING = "dependency-tracking";
    private static final String DEFAULT_DEPENDENCY_TRACKING = DependencyTracking.ENABLED.name();

    private TasklistSettings() {
    }

    public static boolean isBadgesEnabled() {
        return TasklistSettings.getDependencyTracking() != DependencyTracking.DISABLED;
    }

    public static DependencyTracking getDependencyTracking() {
        String s = TasklistSettings.getPreferencesNode().get("dependency-tracking", DEFAULT_DEPENDENCY_TRACKING);
        try {
            return DependencyTracking.valueOf(s);
        }
        catch (IllegalArgumentException e) {
            return DependencyTracking.valueOf(DEFAULT_DEPENDENCY_TRACKING);
        }
    }

    private static Preferences getPreferencesNode() {
        return NbPreferences.forModule(TasklistSettings.class).node("tasklist");
    }

    static {
        TasklistSettings.getPreferencesNode().addPreferenceChangeListener(new PreferenceChangeListener(){
            private DependencyTracking curr = TasklistSettings.getDependencyTracking();

            @Override
            public void preferenceChange(PreferenceChangeEvent evt) {
                DependencyTracking dt;
                if ("dependency-tracking".equals(evt.getKey()) && this.curr != (dt = TasklistSettings.getDependencyTracking())) {
                    if (dt.ordinal() > this.curr.ordinal()) {
                        IndexingManager.getDefault().refreshAllIndices("java");
                    }
                    this.curr = dt;
                }
            }
        });
    }

    public static enum DependencyTracking {
        DISABLED,
        ENABLED_WITHIN_ROOT,
        ENABLED_WITHIN_PROJECT,
        ENABLED;
        

        private DependencyTracking() {
        }
    }

}

