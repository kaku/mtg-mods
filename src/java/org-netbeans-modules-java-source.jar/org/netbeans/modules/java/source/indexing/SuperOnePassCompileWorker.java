/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.Tree
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.code.Symbol$TypeSymbol
 *  com.sun.tools.javac.comp.Enter
 *  com.sun.tools.javac.comp.Env
 *  com.sun.tools.javac.model.JavacElements
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCClassDecl
 *  com.sun.tools.javac.tree.JCTree$JCCompilationUnit
 *  com.sun.tools.javac.tree.JCTree$Tag
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.CouplingAbort
 *  com.sun.tools.javac.util.FatalError
 *  com.sun.tools.javac.util.JCDiagnostic
 *  com.sun.tools.javac.util.Log
 *  com.sun.tools.javac.util.MissingPlatformError
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.queries.SourceLevelQuery
 *  org.netbeans.api.java.queries.SourceLevelQuery$Profile
 *  org.netbeans.lib.nbjavac.services.CancelAbort
 *  org.netbeans.lib.nbjavac.services.CancelService
 *  org.netbeans.modules.parsing.lucene.support.LowMemoryWatcher
 *  org.netbeans.modules.parsing.spi.indexing.Context
 *  org.netbeans.modules.parsing.spi.indexing.Indexable
 *  org.netbeans.modules.parsing.spi.indexing.SuspendStatus
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.java.source.indexing;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.Tree;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.comp.Enter;
import com.sun.tools.javac.comp.Env;
import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.CouplingAbort;
import com.sun.tools.javac.util.FatalError;
import com.sun.tools.javac.util.JCDiagnostic;
import com.sun.tools.javac.util.Log;
import com.sun.tools.javac.util.MissingPlatformError;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.tools.FileObject;
import javax.tools.JavaFileObject;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.queries.SourceLevelQuery;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.lib.nbjavac.services.CancelAbort;
import org.netbeans.lib.nbjavac.services.CancelService;
import org.netbeans.modules.java.source.TreeLoader;
import org.netbeans.modules.java.source.indexing.APTUtils;
import org.netbeans.modules.java.source.indexing.CheckSums;
import org.netbeans.modules.java.source.indexing.CompileWorker;
import org.netbeans.modules.java.source.indexing.DiagnosticListenerImpl;
import org.netbeans.modules.java.source.indexing.FQN2Files;
import org.netbeans.modules.java.source.indexing.JavaCustomIndexer;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.indexing.JavaParsingContext;
import org.netbeans.modules.java.source.indexing.SourcePrefetcher;
import org.netbeans.modules.java.source.parsing.FileManagerTransaction;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.JavacParser;
import org.netbeans.modules.java.source.parsing.OutputFileManager;
import org.netbeans.modules.java.source.parsing.PrefetchableJavaFileObject;
import org.netbeans.modules.java.source.usages.ClassNamesForFileOraculumImpl;
import org.netbeans.modules.java.source.usages.ExecutableFilesIndex;
import org.netbeans.modules.parsing.lucene.support.LowMemoryWatcher;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.netbeans.modules.parsing.spi.indexing.SuspendStatus;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;

final class SuperOnePassCompileWorker
extends CompileWorker {
    SuperOnePassCompileWorker() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    CompileWorker.ParsingOutput compile(CompileWorker.ParsingOutput previous, final org.netbeans.modules.parsing.spi.indexing.Context context, JavaParsingContext javaContext, Collection<? extends JavaCustomIndexer.CompileTuple> files) {
        HashSet<FileObject> aptGenerated;
        HashSet<File> createdFiles;
        HashSet<ElementHandle<TypeElement>> modifiedTypes;
        HashSet<Indexable> finished;
        HashSet<ElementHandle<TypeElement>> addedTypes;
        HashMap<JavaFileObject, List<String>> file2FQNs;
        block56 : {
            DiagnosticListenerImpl dc;
            ClassPath bootPath;
            boolean nop;
            LowMemoryWatcher mem;
            IdentityHashMap<CompilationUnitTree, JavaCustomIndexer.CompileTuple> units;
            ClassPath classPath;
            ClassPath sourcePath;
            JavacTaskImpl jt;
            LinkedList<CompilationUnitTree> trees;
            block55 : {
                file2FQNs = previous != null ? previous.file2FQNs : new HashMap<JavaFileObject, List<String>>();
                addedTypes = previous != null ? previous.addedTypes : new HashSet<ElementHandle<TypeElement>>();
                createdFiles = previous != null ? previous.createdFiles : new HashSet<File>();
                finished = previous != null ? previous.finishedFiles : new HashSet<Indexable>();
                modifiedTypes = previous != null ? previous.modifiedTypes : new HashSet<ElementHandle<TypeElement>>();
                aptGenerated = previous != null ? previous.aptGenerated : new HashSet<FileObject>();
                ClassNamesForFileOraculumImpl cnffOraculum = new ClassNamesForFileOraculumImpl(file2FQNs);
                mem = LowMemoryWatcher.getInstance();
                dc = new DiagnosticListenerImpl();
                trees = new LinkedList<CompilationUnitTree>();
                units = new IdentityHashMap<CompilationUnitTree, JavaCustomIndexer.CompileTuple>();
                jt = null;
                nop = true;
                SuspendStatus suspendStatus = context.getSuspendStatus();
                SourcePrefetcher sourcePrefetcher = SourcePrefetcher.create(files, suspendStatus);
                do {
                    if (sourcePrefetcher.hasNext()) {
                        JavaCustomIndexer.CompileTuple tuple = sourcePrefetcher.next();
                        try {
                            if (tuple == null) continue;
                            nop = false;
                            if (context.isCancelled()) {
                                CompileWorker.ParsingOutput parsingOutput = null;
                                return parsingOutput;
                            }
                            try {
                                if (mem.isLowMemory()) {
                                    jt = null;
                                    units = null;
                                    dc.cleanDiagnostics();
                                    mem.free();
                                }
                                if (jt == null) {
                                    jt = JavacParser.createJavacTask(javaContext.getClasspathInfo(), dc, javaContext.getSourceLevel(), javaContext.getProfile(), cnffOraculum, javaContext.getFQNs(), new CancelService(){

                                        public boolean isCanceled() {
                                            return context.isCancelled() || mem.isLowMemory();
                                        }
                                    }, tuple.aptGenerated ? null : APTUtils.get(context.getRoot()));
                                }
                                for (CompilationUnitTree cut : jt.parse(new JavaFileObject[]{tuple.jfo})) {
                                    trees.add(cut);
                                    if (units != null) {
                                        units.put(cut, tuple);
                                    }
                                    this.computeFQNs(file2FQNs, cut, tuple);
                                }
                                Log.instance((Context)jt.getContext()).nerrors = 0;
                            }
                            catch (CancelAbort ca) {
                                if (!context.isCancelled() || !JavaIndex.LOG.isLoggable(Level.FINEST)) continue;
                                JavaIndex.LOG.log(Level.FINEST, "SuperOnePassCompileWorker was canceled in root: " + FileUtil.getFileDisplayName((org.openide.filesystems.FileObject)context.getRoot()), (Throwable)ca);
                            }
                            catch (Throwable t) {
                                if (JavaIndex.LOG.isLoggable(Level.WARNING)) {
                                    ClassPath bootPath2 = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.BOOT);
                                    ClassPath classPath2 = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.COMPILE);
                                    ClassPath sourcePath2 = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.SOURCE);
                                    Object[] arrobject = new Object[5];
                                    arrobject[0] = tuple.indexable.getURL().toString();
                                    arrobject[1] = FileUtil.getFileDisplayName((org.openide.filesystems.FileObject)context.getRoot());
                                    arrobject[2] = bootPath2 == null ? null : bootPath2.toString();
                                    arrobject[3] = classPath2 == null ? null : classPath2.toString();
                                    arrobject[4] = sourcePath2 == null ? null : sourcePath2.toString();
                                    String message = String.format("SuperOnePassCompileWorker caused an exception\nFile: %s\nRoot: %s\nBootpath: %s\nClasspath: %s\nSourcepath: %s", arrobject);
                                    JavaIndex.LOG.log(Level.WARNING, message, t);
                                }
                                if (t instanceof ThreadDeath) {
                                    throw (ThreadDeath)t;
                                }
                                jt = null;
                                units = null;
                                dc.cleanDiagnostics();
                                mem.free();
                            }
                            continue;
                        }
                        finally {
                            sourcePrefetcher.remove();
                            continue;
                        }
                    }
                    break block55;
                    break;
                } while (true);
                finally {
                    try {
                        sourcePrefetcher.close();
                    }
                    catch (IOException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
            }
            if (nop) {
                return CompileWorker.ParsingOutput.success(file2FQNs, addedTypes, createdFiles, finished, modifiedTypes, aptGenerated);
            }
            if (jt == null || units == null || JavaCustomIndexer.NO_ONE_PASS_COMPILE_WORKER) {
                return CompileWorker.ParsingOutput.failure(file2FQNs, addedTypes, createdFiles, finished, modifiedTypes, aptGenerated);
            }
            if (context.isCancelled()) {
                return null;
            }
            if (mem.isLowMemory()) {
                units = null;
                mem.free();
                return CompileWorker.ParsingOutput.lowMemory(file2FQNs, addedTypes, createdFiles, finished, modifiedTypes, aptGenerated);
            }
            Iterable processors = jt.getProcessors();
            boolean aptEnabled = processors != null && processors.iterator().hasNext();
            try {
                final Iterable types = jt.enter(trees);
                if (context.isCancelled()) {
                    return null;
                }
                if (mem.isLowMemory()) {
                    units = null;
                    mem.free();
                    return CompileWorker.ParsingOutput.lowMemory(file2FQNs, addedTypes, createdFiles, finished, modifiedTypes, aptGenerated);
                }
                final IdentityHashMap clazz2Tuple = new IdentityHashMap();
                Enter enter = Enter.instance((Context)jt.getContext());
                for (TypeElement type : types) {
                    Env typeEnv = enter.getEnv((Symbol.TypeSymbol)type);
                    if (typeEnv == null) {
                        JavaIndex.LOG.log(Level.FINE, "No Env for: {0}", type.getQualifiedName());
                        continue;
                    }
                    clazz2Tuple.put(type, units.get((Object)typeEnv.toplevel));
                }
                jt.analyze(types);
                if (context.isCancelled()) {
                    return null;
                }
                if (mem.isLowMemory()) {
                    units = null;
                    mem.free();
                    return CompileWorker.ParsingOutput.lowMemory(file2FQNs, addedTypes, createdFiles, finished, modifiedTypes, aptGenerated);
                }
                for (Map.Entry unit : units.entrySet()) {
                    JavaCustomIndexer.CompileTuple active = (JavaCustomIndexer.CompileTuple)unit.getValue();
                    if (aptEnabled) {
                        JavaCustomIndexer.addAptGenerated(context, javaContext, active, aptGenerated);
                    }
                    ArrayList<Symbol.ClassSymbol> activeTypes = new ArrayList<Symbol.ClassSymbol>();
                    for (Tree tree : ((CompilationUnitTree)unit.getKey()).getTypeDecls()) {
                        Symbol.ClassSymbol sym;
                        if (!(tree instanceof JCTree) || ((JCTree)tree).getTag() != JCTree.Tag.CLASSDEF || (sym = ((JCTree.JCClassDecl)tree).sym) == null) continue;
                        activeTypes.add(sym);
                    }
                    javaContext.getFQNs().set(activeTypes, active.indexable.getURL());
                    boolean[] main = new boolean[1];
                    if (javaContext.getCheckSums().checkAndSet(active.indexable.getURL(), activeTypes, (Elements)jt.getElements()) || context.isSupplementaryFilesIndexing()) {
                        javaContext.analyze(Collections.singleton(unit.getKey()), jt, active, addedTypes, main);
                    } else {
                        HashSet aTypes = new HashSet();
                        javaContext.analyze(Collections.singleton(unit.getKey()), jt, active, aTypes, main);
                        addedTypes.addAll(aTypes);
                        modifiedTypes.addAll(aTypes);
                    }
                    ExecutableFilesIndex.DEFAULT.setMainClass(context.getRoot().getURL(), active.indexable.getURL(), main[0]);
                    JavaCustomIndexer.setErrors(context, active, dc);
                }
                if (context.isCancelled()) {
                    return null;
                }
                if (mem.isLowMemory()) {
                    units = null;
                    mem.free();
                    return CompileWorker.ParsingOutput.lowMemory(file2FQNs, addedTypes, createdFiles, finished, modifiedTypes, aptGenerated);
                }
                final JavacTaskImpl jtFin = jt;
                Future<Void> done = FileManagerTransaction.runConcurrent(new FileSystem.AtomicAction(){

                    public void run() throws IOException {
                        for (TypeElement type : types) {
                            Iterable generatedFiles = jtFin.generate(Collections.singletonList(type));
                            JavaCustomIndexer.CompileTuple unit = (JavaCustomIndexer.CompileTuple)clazz2Tuple.get(type);
                            if (unit != null && unit.virtual) continue;
                            for (JavaFileObject generated : generatedFiles) {
                                if (!(generated instanceof FileObjects.FileBase)) continue;
                                createdFiles.add(((FileObjects.FileBase)generated).getFile());
                            }
                        }
                    }
                });
                for (Map.Entry unit2 : units.entrySet()) {
                    finished.add(((JavaCustomIndexer.CompileTuple)unit2.getValue()).indexable);
                }
                done.get();
                return CompileWorker.ParsingOutput.success(file2FQNs, addedTypes, createdFiles, finished, modifiedTypes, aptGenerated);
            }
            catch (CouplingAbort ca) {
                TreeLoader.dumpCouplingAbort(ca, null);
            }
            catch (OutputFileManager.InvalidSourcePath isp) {
                if (JavaIndex.LOG.isLoggable(Level.FINEST)) {
                    bootPath = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.BOOT);
                    classPath = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.COMPILE);
                    sourcePath = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.SOURCE);
                    Object[] arrobject = new Object[4];
                    arrobject[0] = FileUtil.getFileDisplayName((org.openide.filesystems.FileObject)context.getRoot());
                    arrobject[1] = bootPath == null ? null : bootPath.toString();
                    arrobject[2] = classPath == null ? null : classPath.toString();
                    arrobject[3] = sourcePath == null ? null : sourcePath.toString();
                    String message = String.format("SuperOnePassCompileWorker caused an exception\nRoot: %s\nBootpath: %s\nClasspath: %s\nSourcepath: %s", arrobject);
                    JavaIndex.LOG.log(Level.FINEST, message, isp);
                }
            }
            catch (MissingPlatformError mpe) {
                if (JavaIndex.LOG.isLoggable(Level.FINEST)) {
                    bootPath = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.BOOT);
                    classPath = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.COMPILE);
                    sourcePath = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.SOURCE);
                    Object[] arrobject = new Object[4];
                    arrobject[0] = FileUtil.getFileDisplayName((org.openide.filesystems.FileObject)context.getRoot());
                    arrobject[1] = bootPath == null ? null : bootPath.toString();
                    arrobject[2] = classPath == null ? null : classPath.toString();
                    arrobject[3] = sourcePath == null ? null : sourcePath.toString();
                    String message = String.format("SuperOnePassCompileWorker caused an exception\nRoot: %s\nBootpath: %s\nClasspath: %s\nSourcepath: %s", arrobject);
                    JavaIndex.LOG.log(Level.FINEST, message, (Throwable)mpe);
                }
                JavaCustomIndexer.brokenPlatform(context, files, mpe.getDiagnostic());
            }
            catch (CancelAbort ca) {
                if (mem.isLowMemory()) {
                    units = null;
                    mem.free();
                    return CompileWorker.ParsingOutput.lowMemory(file2FQNs, addedTypes, createdFiles, finished, modifiedTypes, aptGenerated);
                }
                if (JavaIndex.LOG.isLoggable(Level.FINEST)) {
                    JavaIndex.LOG.log(Level.FINEST, "SuperOnePassCompileWorker was canceled in root: " + FileUtil.getFileDisplayName((org.openide.filesystems.FileObject)context.getRoot()), (Throwable)ca);
                }
            }
            catch (Throwable t) {
                Level level;
                if (t instanceof ThreadDeath) {
                    throw (ThreadDeath)t;
                }
                Level level2 = level = t instanceof FatalError ? Level.FINEST : Level.WARNING;
                if (!JavaIndex.LOG.isLoggable(level)) break block56;
                ClassPath bootPath3 = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.BOOT);
                ClassPath classPath3 = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.COMPILE);
                ClassPath sourcePath3 = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.SOURCE);
                Object[] arrobject = new Object[4];
                arrobject[0] = FileUtil.getFileDisplayName((org.openide.filesystems.FileObject)context.getRoot());
                arrobject[1] = bootPath3 == null ? null : bootPath3.toString();
                arrobject[2] = classPath3 == null ? null : classPath3.toString();
                arrobject[3] = sourcePath3 == null ? null : sourcePath3.toString();
                String message = String.format("SuperOnePassCompileWorker caused an exception\nRoot: %s\nBootpath: %s\nClasspath: %s\nSourcepath: %s", arrobject);
                JavaIndex.LOG.log(level, message, t);
            }
        }
        return CompileWorker.ParsingOutput.failure(file2FQNs, addedTypes, createdFiles, finished, modifiedTypes, aptGenerated);
    }

}

