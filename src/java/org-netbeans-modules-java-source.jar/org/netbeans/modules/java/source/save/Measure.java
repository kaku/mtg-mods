/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.doctree.DocTree
 *  com.sun.source.doctree.DocTree$Kind
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.tools.javac.tree.DCTree
 *  com.sun.tools.javac.tree.JCTree
 */
package org.netbeans.modules.java.source.save;

import com.sun.source.doctree.DocTree;
import com.sun.source.tree.Tree;
import com.sun.tools.javac.tree.DCTree;
import com.sun.tools.javac.tree.JCTree;
import java.util.Comparator;

public class Measure {
    static final Comparator<JCTree> DEFAULT = new Comparator<JCTree>(){

        @Override
        public int compare(JCTree first, JCTree second) {
            assert (first != null && second != null);
            if (first == second || first.equals((Object)second)) {
                return 0;
            }
            return 1000;
        }
    };
    static final Comparator<DCTree> DOCTREE = new Comparator<DCTree>(){

        @Override
        public int compare(DCTree first, DCTree second) {
            assert (first != null && second != null);
            if (first == second || first.equals((Object)second)) {
                return 0;
            }
            return 1000;
        }
    };
    static final Comparator<DCTree> TAGS = new Comparator<DCTree>(){

        @Override
        public int compare(DCTree t1, DCTree t2) {
            int distance = Measure.DOCTREE.compare(t1, t2);
            if (distance == 1000 && t1.getKind() == t2.getKind()) {
                return t1.pos == t2.pos ? 250 : 750;
            }
            return distance;
        }
    };
    static final Comparator<JCTree> REAL_MEMBER = new Comparator<JCTree>(){

        @Override
        public int compare(JCTree t1, JCTree t2) {
            int distance = Measure.DEFAULT.compare(t1, t2);
            if (distance == 1000 && t1.getKind() == t2.getKind()) {
                return t1.pos == t2.pos ? 250 : 750;
            }
            return distance;
        }
    };
    static final Comparator<JCTree> MEMBER = new Comparator<JCTree>(){

        @Override
        public int compare(JCTree t1, JCTree t2) {
            int distance = Measure.DEFAULT.compare(t1, t2);
            if (distance == 1000 && t1.getKind() == t2.getKind() && t1.pos == t2.pos) {
                return 250;
            }
            return distance;
        }
    };
    static final Comparator<JCTree> ARGUMENT = new Comparator<JCTree>(){

        @Override
        public int compare(JCTree t1, JCTree t2) {
            int distance = Measure.DEFAULT.compare(t1, t2);
            if (distance == 1000 && t1.getKind() == t2.getKind()) {
                return t1.pos == t2.pos ? 250 : 750;
            }
            return distance;
        }
    };
    static final Comparator<JCTree> GROUP_VAR_MEASURE = ARGUMENT;
    public static final int INFINITE_DISTANCE = 1000;
    public static final int OBJECTS_MATCH = 0;
    public static final int ALMOST_THE_SAME = 250;
    public static final int THE_SAME_KIND = 750;

    private Measure() {
    }

}

