/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.code.Symbol$MethodSymbol
 *  com.sun.tools.javac.code.Symbol$PackageSymbol
 *  com.sun.tools.javac.code.Symbol$TypeSymbol
 *  com.sun.tools.javac.code.Symtab
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.code.Type$ArrayType
 *  com.sun.tools.javac.code.Type$ClassType
 *  com.sun.tools.javac.code.Type$TypeVar
 *  com.sun.tools.javac.code.TypeTag
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCAnnotation
 *  com.sun.tools.javac.tree.JCTree$JCArrayAccess
 *  com.sun.tools.javac.tree.JCTree$JCArrayTypeTree
 *  com.sun.tools.javac.tree.JCTree$JCAssign
 *  com.sun.tools.javac.tree.JCTree$JCAssignOp
 *  com.sun.tools.javac.tree.JCTree$JCBinary
 *  com.sun.tools.javac.tree.JCTree$JCClassDecl
 *  com.sun.tools.javac.tree.JCTree$JCConditional
 *  com.sun.tools.javac.tree.JCTree$JCExpression
 *  com.sun.tools.javac.tree.JCTree$JCFieldAccess
 *  com.sun.tools.javac.tree.JCTree$JCIdent
 *  com.sun.tools.javac.tree.JCTree$JCInstanceOf
 *  com.sun.tools.javac.tree.JCTree$JCLiteral
 *  com.sun.tools.javac.tree.JCTree$JCMethodInvocation
 *  com.sun.tools.javac.tree.JCTree$JCModifiers
 *  com.sun.tools.javac.tree.JCTree$JCNewArray
 *  com.sun.tools.javac.tree.JCTree$JCNewClass
 *  com.sun.tools.javac.tree.JCTree$JCParens
 *  com.sun.tools.javac.tree.JCTree$JCPrimitiveTypeTree
 *  com.sun.tools.javac.tree.JCTree$JCTypeApply
 *  com.sun.tools.javac.tree.JCTree$JCTypeCast
 *  com.sun.tools.javac.tree.JCTree$JCUnary
 *  com.sun.tools.javac.tree.JCTree$JCVariableDecl
 *  com.sun.tools.javac.tree.JCTree$Tag
 *  com.sun.tools.javac.tree.JCTree$Visitor
 *  com.sun.tools.javac.tree.TreeInfo
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.List
 *  com.sun.tools.javac.util.Name
 */
package org.netbeans.modules.java.source.pretty;

import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Symtab;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.TypeTag;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeInfo;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;
import java.io.PrintStream;
import org.netbeans.modules.java.source.pretty.VeryPretty;

public class WidthEstimator
extends JCTree.Visitor {
    private int width;
    private int prec;
    private int maxwidth;
    private final Symtab symbols;
    private final TreeInfo treeinfo;

    public WidthEstimator(Context context) {
        this.symbols = Symtab.instance((Context)context);
        this.treeinfo = TreeInfo.instance((Context)context);
    }

    public int estimateWidth(JCTree t, int maxwidth) {
        this.width = 0;
        this.maxwidth = maxwidth;
        t.accept((JCTree.Visitor)this);
        return this.width;
    }

    public int estimateWidth(JCTree t) {
        return this.estimateWidth(t, 100);
    }

    public int estimateWidth(List<? extends JCTree> t, int maxwidth) {
        this.width = 0;
        this.maxwidth = maxwidth;
        while (t.nonEmpty() && this.width < this.maxwidth) {
            ((JCTree)t.head).accept((JCTree.Visitor)this);
            t = t.tail;
        }
        return this.width;
    }

    private void open(int contextPrec, int ownPrec) {
        if (ownPrec < contextPrec) {
            this.width += 2;
        }
    }

    private void width(Name n) {
        this.width += n.getByteLength();
    }

    private void width(String n) {
        this.width += n.length();
    }

    private void width(JCTree n) {
        if (this.width < this.maxwidth) {
            n.accept((JCTree.Visitor)this);
        }
    }

    private void width(JCTree n, Type t) {
        if (t == null) {
            this.width(n);
        } else {
            this.width(t);
        }
    }

    private void width(Type ty) {
        List typarams;
        while (ty instanceof Type.ArrayType) {
            ty = ((Type.ArrayType)ty).elemtype;
            this.width += 2;
        }
        this.widthQ((Symbol)ty.tsym);
        if (ty instanceof Type.ClassType && (typarams = ((Type.ClassType)ty).typarams_field) != null && typarams.nonEmpty()) {
            ++this.width;
            while (typarams.nonEmpty()) {
                ++this.width;
                this.width((Type)typarams.head);
                typarams = typarams.tail;
            }
        }
    }

    public void widthQ(Symbol t) {
        if (t.owner != null && t.owner != this.symbols.rootPackage && t.owner != this.symbols.unnamedPackage && !(t.type instanceof Type.TypeVar) && !(t.owner instanceof Symbol.MethodSymbol)) {
            ++this.width;
            this.widthQ(t.owner);
        }
        this.width(t.name);
    }

    private void width(List<? extends JCTree> n, int pad) {
        int nadd = 0;
        while (!n.isEmpty() && this.width < this.maxwidth) {
            this.width((JCTree)n.head);
            n = n.tail;
            ++nadd;
        }
        if (nadd > 1) {
            this.width += pad * nadd;
        }
    }

    private void width(List<? extends JCTree> n) {
        this.width(n, 2);
    }

    private void width(JCTree tree, int prec) {
        if (tree != null) {
            int prevPrec = this.prec;
            this.prec = prec;
            tree.accept((JCTree.Visitor)this);
            this.prec = prevPrec;
        }
    }

    public void visitTree(JCTree tree) {
        System.err.println("Need width calc for " + (Object)tree);
        this.width = this.maxwidth;
    }

    public void visitParens(JCTree.JCParens tree) {
        this.width += 2;
        this.width((JCTree)tree.expr);
    }

    public void visitApply(JCTree.JCMethodInvocation tree) {
        this.width += 2;
        this.width((JCTree)tree.meth, 15);
        this.width(tree.args);
    }

    public void visitNewClass(JCTree.JCNewClass tree) {
        if (tree.encl != null) {
            this.width((JCTree)tree.encl);
            ++this.width;
        }
        this.width += 4;
        if (tree.encl == null) {
            this.width((JCTree)tree.clazz, tree.clazz.type);
        } else if (tree.clazz.type != null) {
            this.width(tree.clazz.type.tsym.name);
        } else {
            this.width((JCTree)tree.clazz);
        }
        this.width += 2;
        this.width(tree.args, 2);
        if (tree.def != null) {
            this.width += 4;
            this.width(tree.def.defs, 2);
        }
    }

    public void visitNewArray(JCTree.JCNewArray tree) {
        if (tree.elemtype != null) {
            this.width += 4;
            JCTree.JCExpression elemtype = tree.elemtype;
            while (elemtype.getTag() == JCTree.Tag.TYPEARRAY) {
                this.width += 2;
                elemtype = ((JCTree.JCArrayTypeTree)elemtype).elemtype;
            }
            this.width((JCTree)elemtype);
            List l = tree.dims;
            while (l.nonEmpty()) {
                this.width += 2;
                this.width((JCTree)l.head);
                l = l.tail;
            }
        }
        if (tree.elems != null) {
            this.width += 4;
            this.width(tree.elems);
        }
    }

    private void widthAnnotations(List<JCTree.JCAnnotation> anns) {
        int nadd = 0;
        while (!anns.isEmpty() && this.width < this.maxwidth) {
            ++this.width;
            this.width((JCTree)anns.head);
            anns = anns.tail;
            ++nadd;
        }
        if (nadd > 1) {
            this.width += nadd;
        }
    }

    private void widthFlags(long flags) {
        if ((flags & 4096) != 0) {
            this.width += 14;
        }
        this.width += VeryPretty.flagNames(flags).length();
        if ((flags & 4095) != 0) {
            ++this.width;
        }
    }

    public void visitVarDef(JCTree.JCVariableDecl tree) {
        this.widthAnnotations(tree.mods.annotations);
        if ((tree.mods.flags & 16384) == 0) {
            this.widthFlags(tree.mods.flags);
            if (tree.vartype != null) {
                this.width((JCTree)tree.vartype, tree.type);
            }
            ++this.width;
        }
        this.width(tree.name);
        if (tree.init != null && (tree.mods.flags & 16384) == 0) {
            this.width += 3;
            this.width((JCTree)tree.init);
        }
    }

    public void visitConditional(JCTree.JCConditional tree) {
        this.open(this.prec, 3);
        this.width += 6;
        this.width((JCTree)tree.cond, 2);
        this.width((JCTree)tree.truepart, 3);
        this.width((JCTree)tree.falsepart, 3);
    }

    public void visitAssignop(JCTree.JCAssignOp tree) {
        this.open(this.prec, 2);
        this.width += 3;
        this.width(this.treeinfo.operatorName(tree.getTag()));
        this.width((JCTree)tree.lhs, 3);
        this.width((JCTree)tree.rhs, 2);
    }

    public void visitAssign(JCTree.JCAssign tree) {
        this.open(this.prec, 1);
        this.width += 3;
        this.width((JCTree)tree.lhs, 2);
        this.width((JCTree)tree.rhs, 1);
    }

    public void visitUnary(JCTree.JCUnary tree) {
        int ownprec = TreeInfo.opPrec((JCTree.Tag)tree.getTag());
        Name opname = this.treeinfo.operatorName(tree.getTag());
        this.open(this.prec, ownprec);
        this.width(opname);
        this.width((JCTree)tree.arg, ownprec);
    }

    public void visitBinary(JCTree.JCBinary tree) {
        int ownprec = TreeInfo.opPrec((JCTree.Tag)tree.getTag());
        Name opname = this.treeinfo.operatorName(tree.getTag());
        this.open(this.prec, ownprec);
        this.width(opname);
        this.width += 2;
        this.width((JCTree)tree.lhs, ownprec);
        this.width((JCTree)tree.rhs, ownprec + 1);
    }

    public void visitTypeCast(JCTree.JCTypeCast tree) {
        this.width += 2;
        this.open(this.prec, 14);
        this.width(tree.clazz, tree.clazz.type);
        this.width((JCTree)tree.expr, 14);
    }

    public void visitTypeTest(JCTree.JCInstanceOf tree) {
        this.open(this.prec, 10);
        this.width += 12;
        this.width((JCTree)tree.expr, 10);
        this.width(tree.clazz, tree.clazz.type);
    }

    public void visitIndexed(JCTree.JCArrayAccess tree) {
        this.width += 2;
        this.width((JCTree)tree.indexed, 15);
        this.width((JCTree)tree.index);
    }

    public void visitSelect(JCTree.JCFieldAccess tree) {
        if (tree.sym instanceof Symbol.ClassSymbol && tree.type != null) {
            this.width(tree.type);
        } else {
            ++this.width;
            this.width((JCTree)tree.selected, 15);
            this.width(tree.name);
        }
    }

    public void visitIdent(JCTree.JCIdent tree) {
        if (tree.sym instanceof Symbol.ClassSymbol) {
            this.width(tree.type);
        } else {
            this.width(tree.name);
        }
    }

    public void visitLiteral(JCTree.JCLiteral tree) {
        switch (tree.typetag) {
            case LONG: 
            case FLOAT: {
                ++this.width;
                this.width(tree.value.toString());
                break;
            }
            case CHAR: {
                this.width += 3;
                break;
            }
            case CLASS: {
                this.width += 2;
                this.width(tree.value.toString());
                break;
            }
            case BOOLEAN: {
                this.width(((Number)tree.value).intValue() == 1 ? "true" : "false");
                break;
            }
            case BOT: {
                this.width("null");
                break;
            }
            default: {
                this.width(tree.value.toString());
            }
        }
    }

    public void visitTypeIdent(JCTree.JCPrimitiveTypeTree tree) {
        this.width(tree.typetag.name());
    }

    public void visitTypeArray(JCTree.JCArrayTypeTree tree) {
        this.width((JCTree)tree.elemtype);
        this.width += 2;
    }

    public void visitTypeApply(JCTree.JCTypeApply that) {
        this.width((JCTree)that.clazz);
        this.width(that.arguments);
        this.width += 2 * that.arguments.size();
    }

}

