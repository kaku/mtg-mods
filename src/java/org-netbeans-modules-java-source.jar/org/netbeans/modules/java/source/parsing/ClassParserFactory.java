/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.ParserFactory
 */
package org.netbeans.modules.java.source.parsing;

import java.util.Collection;
import org.netbeans.modules.java.source.parsing.ClassParser;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserFactory;

public class ClassParserFactory
extends ParserFactory {
    public Parser createParser(Collection<Snapshot> snapshots) {
        assert (snapshots != null);
        assert (!snapshots.isEmpty());
        return new ClassParser();
    }
}

