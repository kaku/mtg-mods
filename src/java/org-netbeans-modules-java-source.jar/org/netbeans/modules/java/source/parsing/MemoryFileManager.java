/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.java.source.parsing;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import javax.tools.FileObject;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.InferableJavaFileObject;
import org.openide.util.Parameters;

public class MemoryFileManager
implements JavaFileManager {
    private final Map<String, List<Integer>> packages = new HashMap<String, List<Integer>>();
    private final Map<Integer, InferableJavaFileObject> content = new HashMap<Integer, InferableJavaFileObject>();
    private final AtomicInteger currentId = new AtomicInteger();

    @Override
    public ClassLoader getClassLoader(JavaFileManager.Location location) {
        throw new UnsupportedOperationException();
    }

    public List<JavaFileObject> list(JavaFileManager.Location location, String packageName, Set<JavaFileObject.Kind> kinds, boolean recurse) throws IOException {
        List<Integer> pkglst;
        if (recurse) {
            throw new UnsupportedEncodingException();
        }
        LinkedList<JavaFileObject> result = new LinkedList<JavaFileObject>();
        if (location == StandardLocation.SOURCE_PATH && (pkglst = this.packages.get(packageName)) != null) {
            for (Integer foid : pkglst) {
                InferableJavaFileObject jfo = this.content.get(foid);
                assert (jfo != null);
                if (!kinds.contains((Object)jfo.getKind())) continue;
                result.add(jfo);
            }
        }
        return result;
    }

    @Override
    public String inferBinaryName(JavaFileManager.Location location, JavaFileObject file) {
        if (location == StandardLocation.SOURCE_PATH && file instanceof InferableJavaFileObject) {
            return ((InferableJavaFileObject)file).inferBinaryName();
        }
        return null;
    }

    @Override
    public boolean isSameFile(FileObject a, FileObject b) {
        return a == null ? b == null : (b == null ? false : a.toUri().equals(b.toUri()));
    }

    @Override
    public boolean handleOption(String current, Iterator<String> remaining) {
        return false;
    }

    @Override
    public boolean hasLocation(JavaFileManager.Location location) {
        return location == StandardLocation.SOURCE_PATH;
    }

    @Override
    public JavaFileObject getJavaFileForInput(JavaFileManager.Location location, String className, JavaFileObject.Kind kind) throws IOException {
        String[] namePair;
        List<Integer> pkglst;
        if (location == StandardLocation.SOURCE_PATH && (namePair = FileObjects.getPackageAndName(className)) != null && (pkglst = this.packages.get(namePair[0])) != null) {
            for (Integer id : pkglst) {
                InferableJavaFileObject jfo = this.content.get(id);
                assert (jfo != null);
                if (!className.equals(jfo.inferBinaryName())) continue;
                return jfo;
            }
        }
        return null;
    }

    @Override
    public JavaFileObject getJavaFileForOutput(JavaFileManager.Location location, String className, JavaFileObject.Kind kind, FileObject sibling) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public FileObject getFileForInput(JavaFileManager.Location location, String packageName, String relativeName) throws IOException {
        List<Integer> pkglst;
        if (location == StandardLocation.SOURCE_PATH && (pkglst = this.packages.get(packageName)) != null) {
            for (Integer id : pkglst) {
                InferableJavaFileObject jfo = this.content.get(id);
                assert (jfo != null);
                if (!relativeName.equals(jfo.getName())) continue;
                return jfo;
            }
        }
        return null;
    }

    @Override
    public FileObject getFileForOutput(JavaFileManager.Location location, String packageName, String relativeName, FileObject sibling) throws IOException {
        throw new UnsupportedOperationException("");
    }

    @Override
    public void flush() throws IOException {
    }

    @Override
    public void close() throws IOException {
        this.packages.clear();
        this.content.clear();
    }

    @Override
    public int isSupportedOption(String option) {
        return -1;
    }

    public boolean register(InferableJavaFileObject jfo) {
        Parameters.notNull((CharSequence)"jfo", (Object)jfo);
        String inferedName = jfo.inferBinaryName();
        String[] pkgName = FileObjects.getPackageAndName(inferedName);
        List<Integer> ids = this.packages.get(pkgName[0]);
        if (ids == null) {
            ids = new LinkedList<Integer>();
            this.packages.put(pkgName[0], ids);
        }
        for (Integer id : ids) {
            InferableJavaFileObject rfo = this.content.get(id);
            assert (rfo != null);
            if (!inferedName.equals(rfo.inferBinaryName())) continue;
            this.content.put(id, jfo);
            return true;
        }
        Integer id2 = this.currentId.getAndIncrement();
        this.content.put(id2, jfo);
        ids.add(id2);
        return false;
    }

    public boolean unregister(String fqn) {
        Parameters.notNull((CharSequence)"fqn", (Object)fqn);
        String[] pkgName = FileObjects.getPackageAndName(fqn);
        List<Integer> ids = this.packages.get(pkgName[0]);
        Iterator<Integer> it = ids.iterator();
        while (it.hasNext()) {
            Integer id = it.next();
            InferableJavaFileObject jfo = this.content.get(id);
            assert (jfo != null);
            if (!fqn.equals(jfo.inferBinaryName())) continue;
            it.remove();
            this.content.remove(id);
            return true;
        }
        return false;
    }
}

