/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.java.source.indexing;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.java.source.indexing.CacheAttributesTransaction;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.parsing.FileManagerTransaction;
import org.netbeans.modules.java.source.parsing.ProcessorGenerated;
import org.netbeans.modules.java.source.parsing.SourceFileManager;
import org.netbeans.modules.java.source.usages.ClassIndexEventsTransaction;
import org.netbeans.modules.java.source.usages.PersistentIndexTransaction;
import org.openide.util.Parameters;

public final class TransactionContext {
    private static final ThreadLocal<TransactionContext> ctx = new ThreadLocal();
    private final Map<Class<? extends Service>, Service> services = new LinkedHashMap<Class<? extends Service>, Service>();

    private TransactionContext() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final void commit() throws IOException, IllegalStateException {
        if (ctx.get() != this) {
            throw new IllegalStateException();
        }
        try {
            Throwable cause = null;
            for (Service s : this.services.values()) {
                try {
                    s.commit();
                }
                catch (Throwable t) {
                    if (t instanceof ThreadDeath) {
                        throw (ThreadDeath)t;
                    }
                    if (cause != null) continue;
                    cause = t;
                }
            }
            if (cause != null) {
                throw new IOException(cause);
            }
        }
        finally {
            this.services.clear();
            ctx.remove();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final void rollBack() throws IOException, IllegalStateException {
        if (ctx.get() != this) {
            throw new IllegalStateException();
        }
        try {
            Throwable cause = null;
            for (Service s : this.services.values()) {
                try {
                    s.rollBack();
                }
                catch (Throwable t) {
                    if (t instanceof ThreadDeath) {
                        throw (ThreadDeath)t;
                    }
                    if (cause != null) continue;
                    cause = t;
                }
            }
            if (cause != null) {
                throw new IOException(cause);
            }
        }
        finally {
            this.services.clear();
            ctx.remove();
        }
    }

    public <T extends Service> TransactionContext register(@NonNull Class<T> type, @NonNull T service) throws IllegalStateException {
        Parameters.notNull((CharSequence)"type", type);
        Parameters.notNull((CharSequence)"service", service);
        if (this.services.containsKey(type)) {
            throw new IllegalStateException("Service already registered.");
        }
        this.services.put((Class<? extends Service>)type, (Service)type.cast(service));
        return this;
    }

    @CheckForNull
    public <T extends Service> T get(@NonNull Class<T> type) {
        return (T)((Service)type.cast(this.services.get(type)));
    }

    @NonNull
    public static TransactionContext beginTrans() throws IllegalStateException {
        if (ctx.get() != null) {
            throw new IllegalStateException();
        }
        TransactionContext res = new TransactionContext();
        ctx.set(res);
        return res;
    }

    @NonNull
    public static TransactionContext get() throws IllegalStateException {
        TransactionContext res = ctx.get();
        if (res == null) {
            throw new IllegalStateException();
        }
        return res;
    }

    @NonNull
    public static TransactionContext beginStandardTransaction(@NonNull URL root, boolean srcIndex, boolean allFilesIndexing, boolean checkForEditorModifications) throws IllegalStateException {
        boolean hasCache = srcIndex ? JavaIndex.hasSourceCache(root, false) : JavaIndex.hasBinaryCache(root, false);
        TransactionContext txCtx = TransactionContext.beginTrans().register(FileManagerTransaction.class, hasCache ? FileManagerTransaction.writeBack(root) : FileManagerTransaction.writeThrough()).register(ProcessorGenerated.class, ProcessorGenerated.create(root)).register(PersistentIndexTransaction.class, PersistentIndexTransaction.create(root)).register(CacheAttributesTransaction.class, CacheAttributesTransaction.create(root, srcIndex, allFilesIndexing)).register(ClassIndexEventsTransaction.class, ClassIndexEventsTransaction.create(srcIndex)).register(SourceFileManager.ModifiedFilesTransaction.class, SourceFileManager.newModifiedFilesTransaction(srcIndex, checkForEditorModifications));
        return txCtx;
    }

    public static abstract class Service {
        protected abstract void commit() throws IOException;

        protected abstract void rollBack() throws IOException;
    }

}

