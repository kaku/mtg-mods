/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaSourceUtilImpl
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.java.source;

import java.io.IOException;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.modules.java.source.JavaSourceAccessor;
import org.openide.filesystems.FileObject;

public class JavaSourceUtilImpl
extends org.netbeans.modules.java.preprocessorbridge.spi.JavaSourceUtilImpl {
    protected long createTaggedCompilationController(FileObject file, long currenTag, Object[] out) throws IOException {
        assert (file != null);
        JavaSource js = JavaSource.forFileObject(file);
        return JavaSourceAccessor.getINSTANCE().createTaggedCompilationController(js, currenTag, out);
    }
}

