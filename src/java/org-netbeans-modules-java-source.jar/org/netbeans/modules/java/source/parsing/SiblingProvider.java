/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.source.parsing;

import java.net.URL;

public interface SiblingProvider {
    public URL getSibling();

    public boolean isInSourceRoot();

    public boolean hasSibling();
}

