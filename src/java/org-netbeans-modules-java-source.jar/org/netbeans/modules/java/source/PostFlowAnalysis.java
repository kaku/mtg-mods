/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.code.Scope
 *  com.sun.tools.javac.code.Scope$Entry
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.code.Symbol$MethodSymbol
 *  com.sun.tools.javac.code.Symbol$TypeSymbol
 *  com.sun.tools.javac.code.Symtab
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.code.Types
 *  com.sun.tools.javac.comp.Enter
 *  com.sun.tools.javac.comp.Env
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCClassDecl
 *  com.sun.tools.javac.tree.JCTree$JCCompilationUnit
 *  com.sun.tools.javac.tree.JCTree$JCExpression
 *  com.sun.tools.javac.tree.JCTree$JCFieldAccess
 *  com.sun.tools.javac.tree.JCTree$JCMethodDecl
 *  com.sun.tools.javac.tree.JCTree$JCMethodInvocation
 *  com.sun.tools.javac.tree.JCTree$JCNewClass
 *  com.sun.tools.javac.tree.JCTree$Tag
 *  com.sun.tools.javac.tree.TreeInfo
 *  com.sun.tools.javac.tree.TreeScanner
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.JCDiagnostic
 *  com.sun.tools.javac.util.JCDiagnostic$DiagnosticPosition
 *  com.sun.tools.javac.util.List
 *  com.sun.tools.javac.util.Log
 *  com.sun.tools.javac.util.Name
 *  com.sun.tools.javac.util.Names
 *  com.sun.tools.javac.util.Pair
 */
package org.netbeans.modules.java.source;

import com.sun.tools.javac.code.Scope;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Symtab;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.Types;
import com.sun.tools.javac.comp.Enter;
import com.sun.tools.javac.comp.Env;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeInfo;
import com.sun.tools.javac.tree.TreeScanner;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.JCDiagnostic;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Log;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;
import com.sun.tools.javac.util.Pair;
import javax.lang.model.element.Element;
import javax.tools.JavaFileObject;

public class PostFlowAnalysis
extends TreeScanner {
    private Log log;
    private Types types;
    private Enter enter;
    private Names names;
    private Symtab syms;
    private List<Pair<Symbol.TypeSymbol, Symbol>> outerThisStack;
    private Symbol.TypeSymbol currentClass;
    private boolean checkThis = true;

    private PostFlowAnalysis(Context ctx) {
        this.log = Log.instance((Context)ctx);
        this.types = Types.instance((Context)ctx);
        this.enter = Enter.instance((Context)ctx);
        this.names = Names.instance((Context)ctx);
        this.syms = Symtab.instance((Context)ctx);
        this.outerThisStack = List.nil();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void analyze(Iterable<? extends Element> elems, Context ctx) {
        assert (elems != null);
        PostFlowAnalysis postFlowAnalysis = new PostFlowAnalysis(ctx);
        for (Element e : elems) {
            Env env;
            if (!(e instanceof Symbol.TypeSymbol) || (env = postFlowAnalysis.enter.getClassEnv((Symbol.TypeSymbol)e)) == null) continue;
            JavaFileObject prev = postFlowAnalysis.log.useSource(env.enclClass.sym.sourcefile != null ? env.enclClass.sym.sourcefile : env.toplevel.sourcefile);
            try {
                postFlowAnalysis.scan((JCTree)env.toplevel);
                continue;
            }
            finally {
                postFlowAnalysis.log.useSource(prev);
                continue;
            }
        }
    }

    private void analyze(Element e) {
        Env env;
        if (e instanceof Symbol.TypeSymbol && (env = this.enter.getClassEnv((Symbol.TypeSymbol)e)) != null) {
            this.scan((JCTree)env.toplevel);
        }
    }

    public void scan(JCTree tree) {
        if (tree != null && tree.type != null && tree.type.constValue() != null) {
            this.checkStringConstant(tree.pos(), tree.type.constValue());
        }
        super.scan(tree);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void visitClassDef(JCTree.JCClassDecl tree) {
        Symbol.TypeSymbol currentClassPrev = this.currentClass;
        this.currentClass = tree.sym;
        List<Pair<Symbol.TypeSymbol, Symbol>> prevOuterThisStack = this.outerThisStack;
        try {
            if (this.currentClass != null) {
                if (this.currentClass.hasOuterInstance()) {
                    this.outerThisDef((Symbol)this.currentClass);
                }
                super.visitClassDef(tree);
            }
        }
        finally {
            this.outerThisStack = prevOuterThisStack;
            this.currentClass = currentClassPrev;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void visitMethodDef(JCTree.JCMethodDecl tree) {
        if (tree.name == this.names.init && (this.currentClass.isInner() || (this.currentClass.owner.kind & 20) != 0)) {
            List<Pair<Symbol.TypeSymbol, Symbol>> prevOuterThisStack = this.outerThisStack;
            try {
                if (this.currentClass.hasOuterInstance()) {
                    this.outerThisDef((Symbol)tree.sym);
                }
                super.visitMethodDef(tree);
            }
            finally {
                this.outerThisStack = prevOuterThisStack;
            }
        }
        super.visitMethodDef(tree);
        if (tree.sym == null || tree.sym.owner == null || tree.type == null || tree.type == this.syms.unknownType) {
            return;
        }
        Type type = this.types.erasure(tree.type);
        Scope.Entry e = tree.sym.owner.members().lookup(tree.name);
        while (e.sym != null) {
            if (e.sym != tree.sym && this.types.isSameType(this.types.erasure(e.sym.type), type) && !e.sym.type.isErroneous() && !type.isErroneous()) {
                this.log.error(tree.pos(), "name.clash.same.erasure", new Object[]{tree.sym, e.sym});
                return;
            }
            e = e.next();
        }
    }

    public void visitNewClass(JCTree.JCNewClass tree) {
        Symbol c;
        super.visitNewClass(tree);
        Symbol symbol = c = tree.constructor != null ? tree.constructor.owner : null;
        if (c != null && c.hasOuterInstance() && tree.encl == null && (c.owner.kind & 20) != 0) {
            this.checkThis(tree.pos(), c.type.getEnclosingType().tsym);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void visitApply(JCTree.JCMethodInvocation tree) {
        boolean prevCheckThis = this.checkThis;
        try {
            Symbol c;
            Symbol meth = TreeInfo.symbol((JCTree)tree.meth);
            Name methName = TreeInfo.name((JCTree)tree.meth);
            if (meth != null && meth.name == this.names.init && (c = meth.owner).hasOuterInstance()) {
                this.checkThis = false;
                if (tree.meth.getTag() != JCTree.Tag.SELECT && ((c.owner.kind & 20) != 0 || methName == this.names._this)) {
                    this.checkThis(tree.meth.pos(), c.type.getEnclosingType().tsym);
                }
            }
            super.visitApply(tree);
        }
        finally {
            this.checkThis = prevCheckThis;
        }
    }

    public void visitSelect(JCTree.JCFieldAccess tree) {
        super.visitSelect(tree);
        if (tree.selected.type != null && (tree.name == this.names._this || tree.name == this.names._super && !this.types.isDirectSuperInterface(tree.selected.type.tsym, this.currentClass))) {
            this.checkThis(tree.pos(), tree.selected.type.tsym);
        }
    }

    private void checkThis(JCDiagnostic.DiagnosticPosition pos, Symbol.TypeSymbol c) {
        if (this.checkThis && this.currentClass != c) {
            List ots = this.outerThisStack;
            if (ots.isEmpty()) {
                this.log.error(pos, "no.encl.instance.of.type.in.scope", new Object[]{c});
                return;
            }
            Pair ot = (Pair)ots.head;
            Symbol.TypeSymbol otc = (Symbol.TypeSymbol)ot.fst;
            while (otc != c) {
                do {
                    if ((ots = ots.tail).isEmpty()) {
                        this.log.error(pos, "no.encl.instance.of.type.in.scope", new Object[]{c});
                        return;
                    }
                    ot = (Pair)ots.head;
                } while (ot.snd != otc);
                if (otc.owner.kind != 1 && !otc.hasOuterInstance()) {
                    this.log.error(pos, "cant.ref.before.ctor.called", new Object[]{c});
                    return;
                }
                otc = (Symbol.TypeSymbol)ot.fst;
            }
        }
    }

    private void outerThisDef(Symbol owner) {
        Type target = this.types.erasure(owner.enclClass().type.getEnclosingType());
        Pair outerThis = Pair.of((Object)target.tsym, (Object)owner);
        this.outerThisStack = this.outerThisStack.prepend((Object)outerThis);
    }

    private void checkStringConstant(JCDiagnostic.DiagnosticPosition pos, Object constValue) {
        if (constValue instanceof String && ((String)constValue).length() >= 65535) {
            this.log.error(pos, "limit.string", new Object[0]);
        }
    }
}

