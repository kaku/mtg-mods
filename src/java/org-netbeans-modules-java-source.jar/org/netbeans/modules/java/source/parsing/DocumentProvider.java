/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.source.parsing;

import javax.swing.text.StyledDocument;

public interface DocumentProvider {
    public StyledDocument getDocument();

    public void runAtomic(Runnable var1);
}

