/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ErroneousTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.MemberReferenceTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.NewClassTree
 *  com.sun.source.tree.ParameterizedTypeTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.TreeScanner
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.code.Symbol$MethodSymbol
 *  com.sun.tools.javac.code.Symbol$TypeSymbol
 *  com.sun.tools.javac.code.Symbol$VarSymbol
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCClassDecl
 *  com.sun.tools.javac.tree.JCTree$JCCompilationUnit
 *  com.sun.tools.javac.tree.JCTree$JCFieldAccess
 *  com.sun.tools.javac.tree.JCTree$JCIdent
 *  com.sun.tools.javac.tree.JCTree$JCMemberReference
 *  com.sun.tools.javac.tree.JCTree$JCMethodDecl
 *  com.sun.tools.javac.tree.JCTree$JCNewClass
 *  com.sun.tools.javac.tree.JCTree$JCVariableDecl
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Name
 *  com.sun.tools.javac.util.Names
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.modules.parsing.lucene.support.Convertor
 *  org.netbeans.modules.parsing.spi.indexing.Indexable
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 *  org.openide.util.Pair
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.java.source.usages;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ErroneousTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.MemberReferenceTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.ParameterizedTypeTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.TreeScanner;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.tools.FileObject;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.modules.java.source.ElementHandleAccessor;
import org.netbeans.modules.java.source.indexing.JavaCustomIndexer;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.OutputFileManager;
import org.netbeans.modules.java.source.parsing.PrefetchableJavaFileObject;
import org.netbeans.modules.java.source.usages.ClassFileUtil;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;
import org.netbeans.modules.java.source.usages.DocumentUtil;
import org.netbeans.modules.java.source.usages.UsagesData;
import org.netbeans.modules.parsing.lucene.support.Convertor;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import org.openide.util.Pair;
import org.openide.util.Parameters;

public final class SourceAnalyzerFactory {
    private static final boolean fullIndex = Boolean.getBoolean("org.netbeans.modules.java.source.usages.SourceAnalyser.fullIndex");
    private static final Logger LOG = Logger.getLogger(SourceAnalyzerFactory.class.getName());

    private SourceAnalyzerFactory() {
    }

    public static StorableAnalyzer createStorableAnalyzer(@NonNull ClassIndexImpl.Writer writer) {
        return new StorableAnalyzer(writer);
    }

    public static SimpleAnalyzer createSimpleAnalyzer() {
        return new SimpleAnalyzer();
    }

    private static class UsagesVisitor
    extends TreeScanner<Void, Map<Pair<String, String>, UsagesData<String>>> {
        private static final Convertor<String, String> CONVERTOR = new Convertor<String, String>(){

            public String convert(String p) {
                return p;
            }
        };
        private final Stack<Pair<String, String>> activeClass;
        private final Name errorName;
        private final Name pkgImportName;
        private final CompilationUnitTree cu;
        private final URL siblingUrl;
        private final String sourceName;
        private final boolean signatureFiles;
        private final Set<? super Pair<String, String>> topLevels;
        private final Set<? super ElementHandle<TypeElement>> newTypes;
        private final Set<Symbol> imports;
        private final Set<Symbol> staticImports;
        private final Set<Symbol> unusedPkgImports;
        private final Set<Pair<Symbol, ClassIndexImpl.UsageType>> packageAnnotations;
        private final Set<CharSequence> importIdents;
        private final Set<CharSequence> packageAnnotationIdents;
        private final boolean virtual;
        private boolean isStaticImport;
        private boolean isPkgImport;
        private State state;
        private Element enclosingElement = null;
        private Set<String> rsList;
        private boolean crossedTopLevel;
        private boolean mainMethod;

        public UsagesVisitor(JavacTaskImpl jt, CompilationUnitTree cu, JavaFileManager manager, JavaFileObject sibling, Set<? super ElementHandle<TypeElement>> newTypes, JavaCustomIndexer.CompileTuple tuple) throws MalformedURLException, IllegalArgumentException {
            assert (jt != null);
            assert (cu != null);
            assert (manager != null);
            assert (sibling != null);
            this.activeClass = new Stack();
            this.imports = new HashSet<Symbol>();
            this.staticImports = new HashSet<Symbol>();
            this.unusedPkgImports = new HashSet<Symbol>();
            this.importIdents = new HashSet<CharSequence>();
            this.packageAnnotationIdents = new HashSet<CharSequence>();
            this.packageAnnotations = new HashSet<Pair<Symbol, ClassIndexImpl.UsageType>>();
            Names names = Names.instance((Context)jt.getContext());
            this.errorName = names.error;
            this.pkgImportName = names.asterisk;
            this.state = State.OTHER;
            this.cu = cu;
            this.signatureFiles = true;
            this.virtual = tuple.virtual;
            this.siblingUrl = this.virtual ? tuple.indexable.getURL() : sibling.toUri().toURL();
            this.sourceName = this.inferBinaryName(manager, sibling);
            this.topLevels = null;
            this.newTypes = newTypes;
        }

        protected UsagesVisitor(JavacTaskImpl jt, CompilationUnitTree cu, JavaFileManager manager, JavaFileObject sibling, Set<? super Pair<String, String>> topLevels) throws MalformedURLException, IllegalArgumentException {
            assert (jt != null);
            assert (cu != null);
            assert (manager != null);
            assert (sibling != null);
            this.activeClass = new Stack();
            this.imports = new HashSet<Symbol>();
            this.staticImports = new HashSet<Symbol>();
            this.unusedPkgImports = new HashSet<Symbol>();
            this.importIdents = new HashSet<CharSequence>();
            this.packageAnnotationIdents = new HashSet<CharSequence>();
            this.packageAnnotations = new HashSet<Pair<Symbol, ClassIndexImpl.UsageType>>();
            Names names = Names.instance((Context)jt.getContext());
            this.errorName = names.error;
            this.pkgImportName = names.asterisk;
            this.state = State.OTHER;
            this.cu = cu;
            this.signatureFiles = false;
            this.siblingUrl = sibling.toUri().toURL();
            this.sourceName = this.inferBinaryName(manager, sibling);
            this.topLevels = topLevels;
            this.newTypes = null;
            this.virtual = false;
        }

        @CheckForNull
        public Void scan(@NonNull Tree node, @NonNull Map<Pair<String, String>, UsagesData<String>> p) {
            if (node == null) {
                return null;
            }
            TreeScanner.super.scan(node, p);
            return null;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @CheckForNull
        public Void visitCompilationUnit(@NonNull CompilationUnitTree node, @NonNull Map<Pair<String, String>, UsagesData<String>> p) {
            State oldState = this.state;
            try {
                this.state = State.PACKAGE_ANN;
                this.scan((Iterable)node.getPackageAnnotations(), p);
                this.scan((Tree)node.getPackageName(), p);
                this.state = State.IMPORT;
                this.scan((Iterable)node.getImports(), p);
            }
            finally {
                this.state = oldState;
            }
            this.scan((Iterable)node.getTypeDecls(), p);
            String className = null;
            if (!(this.imports.isEmpty() && this.staticImports.isEmpty() && this.unusedPkgImports.isEmpty())) {
                Pair name;
                className = UsagesVisitor.getResourceName(node);
                if (className != null) {
                    String classNameType = className + DocumentUtil.encodeKind(ElementKind.CLASS);
                    name = Pair.of((Object)classNameType, (Object)null);
                } else {
                    name = null;
                }
                this.addAndClearImports(name, p);
                this.addAndClearUnusedPkgImports(name, p);
            }
            if (!this.packageAnnotations.isEmpty()) {
                if (className == null) {
                    className = UsagesVisitor.getResourceName(node);
                }
                if (className != null) {
                    String classNameType = className + DocumentUtil.encodeKind(ElementKind.CLASS);
                    Pair name = Pair.of((Object)classNameType, (Object)null);
                    for (Pair<Symbol, ClassIndexImpl.UsageType> usage : this.packageAnnotations) {
                        this.addUsage((Symbol)usage.first(), name, p, (ClassIndexImpl.UsageType)((Object)usage.second()));
                    }
                    for (CharSequence ident : this.packageAnnotationIdents) {
                        this.addIdent(name, ident, p, false);
                    }
                }
                this.packageAnnotations.clear();
                this.packageAnnotationIdents.clear();
            }
            return null;
        }

        @CheckForNull
        public Void visitMemberSelect(@NonNull MemberSelectTree node, @NonNull Map<Pair<String, String>, UsagesData<String>> p) {
            this.handleVisitIdentSelect(((JCTree.JCFieldAccess)node).sym, node.getIdentifier(), p);
            State oldState = this.state;
            this.state = this.state == State.IMPORT || this.state == State.PACKAGE_ANN ? this.state : State.OTHER;
            Void ret = (Void)TreeScanner.super.visitMemberSelect(node, p);
            this.state = oldState;
            return ret;
        }

        @CheckForNull
        public Void visitIdentifier(@NonNull IdentifierTree node, @NonNull Map<Pair<String, String>, UsagesData<String>> p) {
            this.handleVisitIdentSelect(((JCTree.JCIdent)node).sym, node.getName(), p);
            return (Void)TreeScanner.super.visitIdentifier(node, p);
        }

        @CheckForNull
        public Void visitImport(@NonNull ImportTree node, @NonNull Map<Pair<String, String>, UsagesData<String>> p) {
            this.isStaticImport = node.isStatic();
            Tree qit = node.getQualifiedIdentifier();
            this.isPkgImport = qit.getKind() == Tree.Kind.MEMBER_SELECT && this.pkgImportName == ((MemberSelectTree)qit).getIdentifier();
            Void ret = (Void)TreeScanner.super.visitImport(node, p);
            this.isPkgImport = false;
            this.isStaticImport = false;
            return ret;
        }

        public Void visitMemberReference(MemberReferenceTree node, Map<Pair<String, String>, UsagesData<String>> p) {
            Symbol sym = ((JCTree.JCMemberReference)node).sym;
            this.handleVisitIdentSelect(sym, node.getName(), p);
            return (Void)TreeScanner.super.visitMemberReference(node, p);
        }

        private void handleVisitIdentSelect(@NullAllowed Symbol sym, @NonNull CharSequence name, @NonNull Map<Pair<String, String>, UsagesData<String>> p) {
            if (!this.activeClass.empty()) {
                this.addIdent(this.activeClass.peek(), name, p, false);
                if (sym != null) {
                    Symbol owner;
                    if (sym.kind == 63 && ((owner = sym.getEnclosingElement()).getKind().isClass() || owner.getKind().isInterface())) {
                        this.addUsage(owner, this.activeClass.peek(), p, ClassIndexImpl.UsageType.TYPE_REFERENCE);
                    }
                    if (sym.getKind().isClass() || sym.getKind().isInterface()) {
                        switch (this.state) {
                            case EXTENDS: {
                                this.addUsage(sym, this.activeClass.peek(), p, ClassIndexImpl.UsageType.SUPER_CLASS);
                                break;
                            }
                            case IMPLEMENTS: {
                                this.addUsage(sym, this.activeClass.peek(), p, ClassIndexImpl.UsageType.SUPER_INTERFACE);
                                break;
                            }
                            case OTHER: 
                            case GT: {
                                this.addUsage(sym, this.activeClass.peek(), p, ClassIndexImpl.UsageType.TYPE_REFERENCE);
                            }
                        }
                    } else if (sym.getKind().isField()) {
                        owner = sym.getEnclosingElement();
                        if (owner.getKind().isClass() || owner.getKind().isInterface()) {
                            this.addUsage(owner, this.activeClass.peek(), p, ClassIndexImpl.UsageType.FIELD_REFERENCE);
                        }
                        this.recordTypeUsage((TypeMirror)sym.asType(), p);
                    } else if (sym.getKind() == ElementKind.CONSTRUCTOR || sym.getKind() == ElementKind.METHOD) {
                        owner = sym.getEnclosingElement();
                        if (owner.getKind().isClass() || owner.getKind().isInterface()) {
                            this.addUsage(owner, this.activeClass.peek(), p, ClassIndexImpl.UsageType.METHOD_REFERENCE);
                        }
                        this.recordTypeUsage((TypeMirror)((Symbol.MethodSymbol)sym).getReturnType(), p);
                    }
                }
            } else if (this.state == State.IMPORT) {
                this.importIdents.add(name);
                if (sym != null && (sym.getKind().isClass() || sym.getKind().isInterface())) {
                    if (this.isStaticImport) {
                        this.staticImports.add(sym);
                    } else {
                        this.imports.add(sym);
                    }
                } else if (this.isPkgImport && sym != null && sym.getKind() == ElementKind.PACKAGE) {
                    this.unusedPkgImports.add(sym);
                    this.isPkgImport = false;
                }
            } else if (this.state == State.PACKAGE_ANN) {
                this.packageAnnotationIdents.add(name);
                if (sym != null) {
                    Symbol owner;
                    if (sym.kind == 63 && ((owner = sym.getEnclosingElement()).getKind().isClass() || owner.getKind().isInterface())) {
                        this.packageAnnotations.add((Pair)Pair.of((Object)owner, (Object)((Object)ClassIndexImpl.UsageType.TYPE_REFERENCE)));
                    }
                    if (sym.getKind().isClass() || sym.getKind().isInterface()) {
                        this.packageAnnotations.add((Pair)Pair.of((Object)sym, (Object)((Object)ClassIndexImpl.UsageType.TYPE_REFERENCE)));
                    } else if (sym.getKind().isField()) {
                        owner = sym.getEnclosingElement();
                        if (owner.getKind().isClass() || owner.getKind().isInterface()) {
                            this.packageAnnotations.add((Pair)Pair.of((Object)owner, (Object)((Object)ClassIndexImpl.UsageType.FIELD_REFERENCE)));
                        }
                    } else if ((sym.getKind() == ElementKind.CONSTRUCTOR || sym.getKind() == ElementKind.METHOD) && ((owner = sym.getEnclosingElement()).getKind().isClass() || owner.getKind().isInterface())) {
                        this.packageAnnotations.add((Pair)Pair.of((Object)owner, (Object)((Object)ClassIndexImpl.UsageType.METHOD_REFERENCE)));
                    }
                }
            }
        }

        @CheckForNull
        public Void visitParameterizedType(@NonNull ParameterizedTypeTree node, @NonNull Map<Pair<String, String>, UsagesData<String>> p) {
            this.scan(node.getType(), p);
            State currState = this.state;
            this.state = State.GT;
            this.scan((Iterable)node.getTypeArguments(), p);
            this.state = currState;
            return null;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @CheckForNull
        public Void visitClass(@NonNull ClassTree node, @NonNull Map<Pair<String, String>, UsagesData<String>> p) {
            Symbol.ClassSymbol sym = ((JCTree.JCClassDecl)node).sym;
            boolean errorInDecl = false;
            boolean errorIgnorSubtree = true;
            boolean topLevel = false;
            String className = null;
            Pair name = null;
            int nameFrom = -1;
            String simpleName = null;
            if (sym != null) {
                errorInDecl = this.hasErrorName((Symbol)sym);
                if (errorInDecl) {
                    if (!this.activeClass.isEmpty()) {
                        name = this.activeClass.get(0);
                        nameFrom = 0;
                    } else {
                        topLevel = true;
                        className = UsagesVisitor.getResourceName(this.cu);
                        if (className != null && !className.isEmpty()) {
                            String classNameType = className + DocumentUtil.encodeKind(ElementKind.CLASS);
                            name = Pair.of((Object)classNameType, (Object)null);
                            simpleName = className.substring(className.lastIndexOf(46) + 1);
                            nameFrom = 1;
                        } else {
                            LOG.log(Level.WARNING, "Cannot resolve {0} (class name: {1}), ignoring whole subtree.", new Object[]{sym, className});
                        }
                    }
                } else {
                    StringBuilder classNameBuilder = new StringBuilder();
                    ClassFileUtil.encodeClassName((TypeElement)sym, classNameBuilder, '.');
                    className = classNameBuilder.toString();
                    if (!className.isEmpty()) {
                        ElementKind kind = sym.getKind();
                        classNameBuilder.append(DocumentUtil.encodeKind(kind));
                        String classNameType = classNameBuilder.toString();
                        String resourceName = null;
                        topLevel = this.activeClass.isEmpty();
                        if (topLevel) {
                            if (this.virtual || !className.equals(this.sourceName)) {
                                if (this.signatureFiles && this.rsList == null) {
                                    this.rsList = new HashSet<String>();
                                    if (this.crossedTopLevel) {
                                        this.rsList.add(this.sourceName);
                                    }
                                }
                                StringBuilder rnBuilder = new StringBuilder(FileObjects.convertPackage2Folder(this.sourceName));
                                rnBuilder.append('.');
                                rnBuilder.append(FileObjects.getExtension(this.siblingUrl.getPath()));
                                resourceName = rnBuilder.toString();
                            } else {
                                this.crossedTopLevel = true;
                            }
                        } else {
                            resourceName = (String)this.activeClass.peek().second();
                        }
                        name = Pair.of((Object)classNameType, (Object)resourceName);
                        nameFrom = 2;
                        simpleName = sym.getSimpleName().toString();
                    } else {
                        LOG.log(Level.WARNING, "Invalid symbol {0} (source: {1}), ignoring whole subtree.", new Object[]{sym, this.siblingUrl});
                    }
                }
            }
            if (name != null) {
                this.activeClass.push(name);
                errorIgnorSubtree = false;
                if (className != null) {
                    if (topLevel) {
                        if (this.topLevels != null) {
                            this.topLevels.add((Pair<String, String>)Pair.of((Object)className, (Object)name.second()));
                        }
                        try {
                            this.addAndClearImports(name, p);
                        }
                        catch (IllegalArgumentException iae) {
                            String msg;
                            switch (nameFrom) {
                                case 0: {
                                    msg = MessageFormat.format("Name from enclosing class: {0}", this.activeClass);
                                    break;
                                }
                                case 1: {
                                    Object[] arrobject = new Object[1];
                                    arrobject[0] = this.cu instanceof JCTree.JCCompilationUnit ? (((JCTree.JCCompilationUnit)this.cu).sourcefile != null ? ((JCTree.JCCompilationUnit)this.cu).sourcefile.toUri() : null) : null;
                                    msg = MessageFormat.format("Name from compilation unit name: {0}", arrobject);
                                    break;
                                }
                                case 2: {
                                    msg = MessageFormat.format("Name from symbol: {0}", new Object[]{sym});
                                    break;
                                }
                                default: {
                                    msg = MessageFormat.format("Unknown state: {0}", nameFrom);
                                }
                            }
                            throw (IllegalArgumentException)Exceptions.attachMessage((Throwable)iae, (String)msg);
                        }
                    }
                    this.addUsage(className, name, p, ClassIndexImpl.UsageType.TYPE_REFERENCE);
                    this.addIdent(name, simpleName, p, true);
                    if (this.newTypes != null) {
                        this.newTypes.add((ElementHandle)ElementHandleAccessor.getInstance().create(ElementKind.OTHER, className));
                    }
                }
            }
            if (!errorIgnorSubtree) {
                Element old = this.enclosingElement;
                try {
                    this.enclosingElement = sym;
                    this.scan((Tree)node.getModifiers(), p);
                    this.scan((Iterable)node.getTypeParameters(), p);
                    this.state = errorInDecl ? State.OTHER : State.EXTENDS;
                    this.scan(node.getExtendsClause(), p);
                    this.state = errorInDecl ? State.OTHER : State.IMPLEMENTS;
                    this.scan((Iterable)node.getImplementsClause(), p);
                    this.state = State.OTHER;
                    this.scan((Iterable)node.getMembers(), p);
                    this.activeClass.pop();
                }
                finally {
                    this.enclosingElement = old;
                }
            }
            if (!errorInDecl && this.rsList != null) {
                this.rsList.add(className);
            }
            if (topLevel) {
                this.addAndClearUnusedPkgImports(name, p);
            }
            return null;
        }

        @CheckForNull
        public Void visitNewClass(@NonNull NewClassTree node, @NonNull Map<Pair<String, String>, UsagesData<String>> p) {
            Symbol owner;
            Symbol sym = ((JCTree.JCNewClass)node).constructor;
            if (sym != null && (owner = sym.getEnclosingElement()) != null && owner.getKind().isClass()) {
                this.addUsage(owner, this.activeClass.peek(), p, ClassIndexImpl.UsageType.METHOD_REFERENCE);
            }
            return (Void)TreeScanner.super.visitNewClass(node, p);
        }

        @CheckForNull
        public Void visitErroneous(@NonNull ErroneousTree tree, @NonNull Map<Pair<String, String>, UsagesData<String>> p) {
            List trees = tree.getErrorTrees();
            for (Tree t : trees) {
                this.scan(t, p);
            }
            return null;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @CheckForNull
        public Void visitMethod(@NonNull MethodTree node, @NonNull Map<Pair<String, String>, UsagesData<String>> p) {
            Element old = this.enclosingElement;
            try {
                this.enclosingElement = ((JCTree.JCMethodDecl)node).sym;
                if (this.enclosingElement != null && this.enclosingElement.getKind() == ElementKind.METHOD) {
                    this.mainMethod |= SourceUtils.isMainMethod((ExecutableElement)this.enclosingElement);
                    this.addIdent(this.activeClass.peek(), node.getName(), p, true);
                }
                Void void_ = (Void)TreeScanner.super.visitMethod(node, p);
                return void_;
            }
            finally {
                this.enclosingElement = old;
            }
        }

        @CheckForNull
        public Void visitVariable(@NonNull VariableTree node, @NonNull Map<Pair<String, String>, UsagesData<String>> p) {
            Symbol.VarSymbol s = ((JCTree.JCVariableDecl)node).sym;
            if (s != null && s.owner != null && (s.owner.getKind().isClass() || s.owner.getKind().isInterface())) {
                this.addIdent(this.activeClass.peek(), node.getName(), p, true);
            }
            return (Void)TreeScanner.super.visitVariable(node, p);
        }

        private void addAndClearImports(@NullAllowed Pair<String, String> nameOfCU, @NonNull Map<Pair<String, String>, UsagesData<String>> data) {
            if (nameOfCU != null) {
                for (Symbol s22 : this.imports) {
                    this.addUsage(s22, nameOfCU, data, ClassIndexImpl.UsageType.TYPE_REFERENCE);
                }
                for (Symbol s : this.staticImports) {
                    this.addUsages(s, nameOfCU, data, ClassIndexImpl.UsageType.TYPE_REFERENCE, ClassIndexImpl.UsageType.METHOD_REFERENCE, ClassIndexImpl.UsageType.FIELD_REFERENCE);
                }
                for (CharSequence s2 : this.importIdents) {
                    this.addIdent(nameOfCU, s2, data, false);
                }
            }
            this.imports.clear();
            this.staticImports.clear();
            this.importIdents.clear();
        }

        private void addAndClearUnusedPkgImports(@NullAllowed Pair<String, String> nameOfCU, @NonNull Map<Pair<String, String>, UsagesData<String>> data) {
            if (nameOfCU != null) {
                for (Symbol s : this.unusedPkgImports) {
                    StringBuilder sb = new StringBuilder();
                    sb.append((CharSequence)s.getQualifiedName());
                    sb.append(".package-info");
                    this.addUsage(sb.toString(), nameOfCU, data, ClassIndexImpl.UsageType.TYPE_REFERENCE);
                }
            }
            this.unusedPkgImports.clear();
        }

        @NonNull
        private String inferBinaryName(@NonNull JavaFileManager jfm, @NonNull JavaFileObject jfo) throws IllegalArgumentException {
            String result = jfm.inferBinaryName(StandardLocation.SOURCE_PATH, jfo);
            if (result != null) {
                return result;
            }
            org.openide.filesystems.FileObject fo = null;
            ClassPath scp = null;
            try {
                fo = URLMapper.findFileObject((URL)jfo.toUri().toURL());
                if (fo != null && (scp = ClassPath.getClassPath((org.openide.filesystems.FileObject)fo, (String)"classpath/source")) != null && (result = scp.getResourceName(fo, '.', false)) != null) {
                    return result;
                }
            }
            catch (MalformedURLException e) {
                // empty catch block
            }
            Object[] arrobject = new Object[4];
            arrobject[0] = jfo.toUri().toString();
            arrobject[1] = jfo.getClass().getName();
            arrobject[2] = fo == null ? "<null>" : FileUtil.getFileDisplayName((org.openide.filesystems.FileObject)fo);
            arrobject[3] = scp == null ? "<null>" : scp.toString();
            throw new IllegalArgumentException(String.format("File: %s Type: %s FileObject: %s Sourcepath: %s", arrobject));
        }

        private void addUsage(@NullAllowed Symbol sym, @NonNull Pair<String, String> owner, @NonNull Map<Pair<String, String>, UsagesData<String>> map, @NonNull ClassIndexImpl.UsageType type) {
            assert (map != null);
            assert (type != null);
            if (sym != null) {
                String className = UsagesVisitor.encodeClassName(sym);
                this.addUsage(className, owner, map, type);
                Symbol encElm = sym.getEnclosingElement();
                if (encElm.getKind() == ElementKind.PACKAGE) {
                    this.unusedPkgImports.remove((Object)encElm);
                }
            }
        }

        private void addUsage(@NullAllowed String className, @NonNull Pair<String, String> owner, @NonNull Map<Pair<String, String>, UsagesData<String>> map, @NonNull ClassIndexImpl.UsageType type) {
            if (className != null) {
                UsagesData<String> data = this.getData(owner, map);
                data.addUsage(className, type);
            }
        }

        private /* varargs */ void addUsages(@NullAllowed Symbol sym, @NonNull Pair<String, String> owner, @NonNull Map<Pair<String, String>, UsagesData<String>> map, @NonNull ClassIndexImpl.UsageType ... types) {
            assert (map != null);
            assert (types != null);
            if (sym != null) {
                String className = UsagesVisitor.encodeClassName(sym);
                this.addUsages(className, owner, map, types);
                Symbol encElm = sym.getEnclosingElement();
                if (encElm.getKind() == ElementKind.PACKAGE) {
                    this.unusedPkgImports.remove((Object)encElm);
                }
            }
        }

        private /* varargs */ void addUsages(@NullAllowed String className, @NonNull Pair<String, String> owner, @NonNull Map<Pair<String, String>, UsagesData<String>> map, @NonNull ClassIndexImpl.UsageType ... types) {
            if (className != null) {
                UsagesData<String> data = this.getData(owner, map);
                data.addUsages(className, types);
            }
        }

        private void addIdent(@NonNull Pair<String, String> owner, @NonNull CharSequence ident, @NonNull Map<Pair<String, String>, UsagesData<String>> map, boolean feature) {
            assert (owner != null);
            assert (ident != null);
            assert (map != null);
            if (feature || fullIndex) {
                UsagesData<String> data = this.getData(owner, map);
                if (fullIndex) {
                    data.addIdent(ident);
                }
                if (feature) {
                    data.addFeatureIdent(ident);
                }
            }
        }

        @NonNull
        private UsagesData<String> getData(@NonNull Pair<String, String> owner, @NonNull Map<Pair<String, String>, UsagesData<String>> map) {
            UsagesData<String> data = map.get(owner);
            if (data == null) {
                if (((String)owner.first()).charAt(((String)owner.first()).length() - 2) == '.') {
                    throw new IllegalArgumentException((String)owner.first());
                }
                data = new UsagesData<String>(CONVERTOR);
                map.put(owner, data);
            }
            return data;
        }

        private boolean hasErrorName(@NullAllowed Symbol cs) {
            while (cs != null) {
                if (cs.name == this.errorName) {
                    return true;
                }
                cs = cs.getEnclosingElement();
            }
            return false;
        }

        @CheckForNull
        private static String encodeClassName(@NonNull Symbol sym) {
            assert (sym instanceof Symbol.ClassSymbol);
            TypeElement toEncode = null;
            Type type = ((Symbol.ClassSymbol)sym).asType();
            if (sym.getEnclosingElement().getKind() == ElementKind.TYPE_PARAMETER) {
                TypeMirror ctype;
                if (type.getKind() == TypeKind.ARRAY && (ctype = ((ArrayType)type).getComponentType()).getKind() == TypeKind.DECLARED) {
                    toEncode = (TypeElement)((DeclaredType)ctype).asElement();
                }
            } else {
                toEncode = (TypeElement)sym;
            }
            return toEncode == null ? null : ClassFileUtil.encodeClassName(toEncode);
        }

        @CheckForNull
        private static String getResourceName(@NullAllowed CompilationUnitTree cu) {
            JavaFileObject jfo;
            URI uri;
            if (cu instanceof JCTree.JCCompilationUnit && (jfo = ((JCTree.JCCompilationUnit)cu).sourcefile) != null && (uri = jfo.toUri()) != null && uri.isAbsolute()) {
                try {
                    ClassPath cp;
                    org.openide.filesystems.FileObject fo = URLMapper.findFileObject((URL)uri.toURL());
                    if (fo != null && (cp = ClassPath.getClassPath((org.openide.filesystems.FileObject)fo, (String)"classpath/source")) != null) {
                        return cp.getResourceName(fo, '.', false);
                    }
                }
                catch (MalformedURLException e) {
                    Exceptions.printStackTrace((Throwable)e);
                }
            }
            return null;
        }

        private void recordTypeUsage(TypeMirror type, Map<Pair<String, String>, UsagesData<String>> p) {
            LinkedList<? extends TypeMirror> types = new LinkedList<TypeMirror>();
            types.add(type);
            while (!types.isEmpty()) {
                TypeMirror currentType = (TypeMirror)types.remove(0);
                if (currentType == null) continue;
                switch (currentType.getKind()) {
                    case DECLARED: {
                        Symbol.TypeSymbol typeSym = ((Type)currentType).tsym;
                        if (typeSym != null && (typeSym.getKind().isClass() || typeSym.getKind().isInterface())) {
                            this.addUsage((Symbol)typeSym, this.activeClass.peek(), p, ClassIndexImpl.UsageType.TYPE_REFERENCE);
                        }
                        types.addAll(((DeclaredType)currentType).getTypeArguments());
                        break;
                    }
                    case ARRAY: {
                        types.add(((ArrayType)currentType).getComponentType());
                    }
                }
            }
        }

        static enum State {
            EXTENDS,
            IMPLEMENTS,
            GT,
            OTHER,
            IMPORT,
            PACKAGE_ANN;
            

            private State() {
            }
        }

    }

    private static class BaseAnalyzer {
        protected final List<Pair<Pair<String, String>, Object[]>> references = new ArrayList<Pair<Pair<String, String>, Object[]>>();
        protected final Set<Pair<String, String>> toDelete = new HashSet<Pair<String, String>>();

        private BaseAnalyzer() {
        }

        protected final void addClassReferences(Pair<String, String> name, UsagesData<String> data) {
            assert (name != null);
            assert (data != null);
            Object[] result = new Object[]{data.usagesToStrings(), data.featureIdentsToString(), data.identsToString()};
            this.references.add((Pair)Pair.of(name, (Object)result));
        }
    }

    public static final class SimpleAnalyzer
    extends BaseAnalyzer {
        private boolean used;

        @CheckForNull
        public List<Pair<Pair<String, String>, Object[]>> analyseUnit(@NonNull CompilationUnitTree cu, @NonNull JavacTaskImpl jt) throws IOException {
            if (this.used) {
                throw new IllegalStateException("Trying to reuse SimpleAnalyzer");
            }
            this.used = true;
            try {
                HashMap<Pair<String, String>, UsagesData<String>> usages = new HashMap<Pair<String, String>, UsagesData<String>>();
                HashSet topLevels = new HashSet();
                JavaFileManager jfm = (JavaFileManager)jt.getContext().get(JavaFileManager.class);
                UsagesVisitor uv = new UsagesVisitor(jt, cu, jfm, cu.getSourceFile(), topLevels);
                uv.scan((Tree)cu, usages);
                for (Map.Entry<Pair<String, String>, UsagesData<String>> oe : usages.entrySet()) {
                    Pair<String, String> key = oe.getKey();
                    UsagesData<String> data = oe.getValue();
                    this.addClassReferences(key, data);
                }
                return this.references;
            }
            catch (IllegalArgumentException iae) {
                Exceptions.printStackTrace((Throwable)iae);
                return null;
            }
            catch (OutputFileManager.InvalidSourcePath e) {
                return null;
            }
        }
    }

    public static final class StorableAnalyzer
    extends BaseAnalyzer {
        private final ClassIndexImpl.Writer writer;

        private StorableAnalyzer(@NonNull ClassIndexImpl.Writer writer) {
            Parameters.notNull((CharSequence)"writer", (Object)writer);
            this.writer = writer;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void analyse(Iterable<? extends CompilationUnitTree> data, JavacTaskImpl jt, JavaCustomIndexer.CompileTuple tuple, Set<? super ElementHandle<TypeElement>> newTypes, boolean[] mainMethod) throws IOException {
            JavaFileManager manager = (JavaFileManager)jt.getContext().get(JavaFileManager.class);
            HashMap<Pair<String, String>, UsagesData<String>> usages = new HashMap<Pair<String, String>, UsagesData<String>>();
            for (CompilationUnitTree cu : data) {
                try {
                    UsagesVisitor uv;
                    FileObject fo;
                    uv = new UsagesVisitor(jt, cu, manager, tuple.jfo, newTypes, tuple);
                    uv.scan((Tree)cu, usages);
                    boolean[] arrbl = mainMethod;
                    arrbl[0] = arrbl[0] | uv.mainMethod;
                    if (uv.rsList == null || uv.rsList.size() <= 0) continue;
                    String ext = tuple.virtual ? FileObjects.getExtension(tuple.indexable.getURL().getPath()) + '.' + "rx" : "rs";
                    String relativePath = tuple.indexable.getRelativePath();
                    fo = manager.getFileForOutput(StandardLocation.CLASS_OUTPUT, "", FileObjects.stripExtension(relativePath) + '.' + ext, tuple.jfo);
                    assert (fo != null);
                    try {
                        BufferedReader in = new BufferedReader(new InputStreamReader(fo.openInputStream(), "UTF-8"));
                        try {
                            String line;
                            while ((line = in.readLine()) != null) {
                                uv.rsList.add(line);
                            }
                        }
                        finally {
                            in.close();
                        }
                    }
                    catch (FileNotFoundException e) {
                        // empty catch block
                    }
                    PrintWriter rsOut = new PrintWriter(new OutputStreamWriter(fo.openOutputStream(), "UTF-8"));
                    try {
                        for (String sig : uv.rsList) {
                            rsOut.println(sig);
                        }
                        continue;
                    }
                    finally {
                        rsOut.close();
                    }
                }
                catch (IllegalArgumentException iae) {
                    Exceptions.printStackTrace((Throwable)iae);
                }
            }
            if (tuple.index) {
                for (Map.Entry oe : usages.entrySet()) {
                    Pair key = (Pair)oe.getKey();
                    UsagesData value = (UsagesData)oe.getValue();
                    this.addClassReferences(key, value);
                }
            }
        }

        public void delete(Pair<String, String> name) throws IOException {
            this.toDelete.add(name);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void store() throws IOException {
            if (this.references.size() > 0 || this.toDelete.size() > 0) {
                try {
                    this.writer.deleteAndFlush(this.references, this.toDelete);
                }
                finally {
                    this.references.clear();
                    this.toDelete.clear();
                }
            }
        }
    }

}

