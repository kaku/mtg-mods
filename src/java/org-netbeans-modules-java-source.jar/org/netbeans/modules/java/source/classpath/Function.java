/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.source.classpath;

interface Function<P, R> {
    public R apply(P var1);
}

