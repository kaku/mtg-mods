/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCExpression
 *  com.sun.tools.javac.tree.JCTree$JCFieldAccess
 *  com.sun.tools.javac.util.Name
 */
package org.netbeans.modules.java.source.builder;

import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.Name;

public class QualIdentTree
extends JCTree.JCFieldAccess {
    private final String fqn;

    public QualIdentTree(JCTree.JCExpression selected, Name name, Symbol sym) {
        super(selected, name, sym);
        this.fqn = null;
    }

    public QualIdentTree(JCTree.JCExpression selected, Name name, String fqn) {
        super(selected, name, null);
        this.fqn = fqn;
    }

    public String getFQN() {
        return this.fqn;
    }
}

