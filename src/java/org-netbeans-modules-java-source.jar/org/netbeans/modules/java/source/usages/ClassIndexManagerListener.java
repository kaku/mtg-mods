/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.source.usages;

import java.util.EventListener;
import org.netbeans.modules.java.source.usages.ClassIndexManagerEvent;

public interface ClassIndexManagerListener
extends EventListener {
    public void classIndexAdded(ClassIndexManagerEvent var1);

    public void classIndexRemoved(ClassIndexManagerEvent var1);
}

