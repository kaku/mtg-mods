/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.spi.java.classpath.ClassPathImplementation
 *  org.netbeans.spi.java.classpath.FilteringPathResourceImplementation
 *  org.netbeans.spi.java.classpath.PathResourceImplementation
 *  org.openide.util.Pair
 *  org.openide.util.Parameters
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.java.source.classpath;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.net.URL;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;
import java.util.Set;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.java.source.classpath.Function;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.usages.ClassIndexManager;
import org.netbeans.modules.java.source.usages.ClassIndexManagerEvent;
import org.netbeans.modules.java.source.usages.ClassIndexManagerListener;
import org.netbeans.spi.java.classpath.ClassPathImplementation;
import org.netbeans.spi.java.classpath.FilteringPathResourceImplementation;
import org.netbeans.spi.java.classpath.PathResourceImplementation;
import org.openide.util.Pair;
import org.openide.util.Parameters;
import org.openide.util.WeakListeners;

public class SourcePath
implements ClassPathImplementation,
PropertyChangeListener {
    private static final boolean NO_SOURCE_FILTER = Boolean.getBoolean("SourcePath.no.source.filter");
    private final PropertyChangeSupport listeners;
    private final ClassPathImplementation delegate;
    private final boolean forcePreferSources;
    private final Function<Pair<Boolean, List<? extends PathResourceImplementation>>, List<PathResourceImplementation>> f;
    private long eventId;
    private List<PathResourceImplementation> resources;

    private SourcePath(@NonNull ClassPathImplementation delegate, boolean forcePreferSources, @NonNull Function<Pair<Boolean, List<? extends PathResourceImplementation>>, List<PathResourceImplementation>> f) {
        this.listeners = new PropertyChangeSupport(this);
        assert (delegate != null);
        assert (f != null);
        this.delegate = delegate;
        this.forcePreferSources = forcePreferSources;
        this.f = f;
        delegate.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)delegate));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public List<? extends PathResourceImplementation> getResources() {
        long currentEventId;
        SourcePath sourcePath = this;
        synchronized (sourcePath) {
            if (this.resources != null) {
                return this.resources;
            }
            currentEventId = this.eventId;
        }
        List<PathResourceImplementation> res = this.f.apply((Pair)Pair.of((Object)this.forcePreferSources, (Object)this.delegate.getResources()));
        SourcePath sourcePath2 = this;
        synchronized (sourcePath2) {
            if (currentEventId == this.eventId) {
                if (this.resources == null) {
                    this.resources = res;
                } else {
                    res = this.resources;
                }
            }
        }
        assert (res != null);
        return res;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        Parameters.notNull((CharSequence)"listener", (Object)listener);
        this.listeners.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        Parameters.notNull((CharSequence)"listener", (Object)listener);
        this.listeners.removePropertyChangeListener(listener);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void propertyChange(PropertyChangeEvent event) {
        SourcePath sourcePath = this;
        synchronized (sourcePath) {
            this.resources = null;
            ++this.eventId;
        }
        this.listeners.firePropertyChange("resources", null, null);
    }

    public static ClassPathImplementation filtered(@NonNull ClassPathImplementation cpImpl, boolean bkgComp) {
        assert (cpImpl != null);
        return new SourcePath(cpImpl, bkgComp, NO_SOURCE_FILTER ? new AllRoots() : new FilterNonOpened());
    }

    private static class FR
    implements FilteringPathResourceImplementation,
    PropertyChangeListener,
    ClassIndexManagerListener {
        private final PathResourceImplementation pr;
        private final boolean forcePreferSources;
        private final PropertyChangeSupport support;
        private URL[] cache;
        private long eventId;

        public FR(@NonNull PathResourceImplementation pr, boolean forcePreferSources) {
            assert (pr != null);
            this.pr = pr;
            this.forcePreferSources = forcePreferSources;
            this.support = new PropertyChangeSupport(this);
            this.pr.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)pr));
            ClassIndexManager manager = ClassIndexManager.getDefault();
            manager.addClassIndexManagerListener((ClassIndexManagerListener)WeakListeners.create(ClassIndexManagerListener.class, (EventListener)this, (Object)manager));
        }

        public boolean includes(URL root, String resource) {
            URL[] roots = this.getRoots();
            boolean contains = false;
            for (URL r : roots) {
                if (!r.equals(root)) continue;
                contains = true;
            }
            return contains && (!(this.pr instanceof FilteringPathResourceImplementation) || ((FilteringPathResourceImplementation)this.pr).includes(root, resource));
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public URL[] getRoots() {
            long currentEventId;
            FR fR = this;
            synchronized (fR) {
                if (this.cache != null) {
                    return this.cache;
                }
                currentEventId = this.eventId;
            }
            URL[] origRoots = this.pr.getRoots();
            ArrayList<URL> rootsList = new ArrayList<URL>(origRoots.length);
            for (URL url : origRoots) {
                if (!this.forcePreferSources && !JavaIndex.hasSourceCache(url, true)) continue;
                rootsList.add(url);
            }
            URL[] res = rootsList.toArray(new URL[rootsList.size()]);
            FR len$ = this;
            synchronized (len$) {
                if (currentEventId == this.eventId) {
                    if (this.cache == null) {
                        this.cache = res;
                    } else {
                        res = this.cache;
                    }
                }
            }
            assert (res != null);
            return res;
        }

        public ClassPathImplementation getContent() {
            return null;
        }

        public void addPropertyChangeListener(PropertyChangeListener listener) {
            this.support.addPropertyChangeListener(listener);
        }

        public void removePropertyChangeListener(PropertyChangeListener listener) {
            this.support.removePropertyChangeListener(listener);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("roots".equals(evt.getPropertyName())) {
                FR fR = this;
                synchronized (fR) {
                    this.cache = null;
                    ++this.eventId;
                }
                this.support.firePropertyChange("roots", null, null);
            } else if ("includes".equals(evt.getPropertyName())) {
                FR fR = this;
                synchronized (fR) {
                    this.cache = null;
                    ++this.eventId;
                }
                this.support.firePropertyChange("includes", null, null);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void classIndexAdded(ClassIndexManagerEvent event) {
            URL url;
            if (this.forcePreferSources) {
                return;
            }
            Set<? extends URL> newRoots = event.getRoots();
            boolean changed = false;
            Object arr$ = this.pr.getRoots();
            int len$ = arr$.length;
            for (int i$ = 0; i$ < len$ && !(changed = newRoots.contains(url = arr$[i$])); ++i$) {
            }
            if (changed) {
                arr$ = this;
                synchronized (arr$) {
                    this.cache = null;
                    ++this.eventId;
                }
                this.support.firePropertyChange("roots", null, null);
            }
        }

        @Override
        public void classIndexRemoved(ClassIndexManagerEvent event) {
        }
    }

    private static class AllRoots
    implements Function<Pair<Boolean, List<? extends PathResourceImplementation>>, List<PathResourceImplementation>> {
        private AllRoots() {
        }

        @Override
        public List<PathResourceImplementation> apply(Pair<Boolean, List<? extends PathResourceImplementation>> resources) {
            ArrayList<PathResourceImplementation> res = new ArrayList<PathResourceImplementation>(((List)resources.second()).size());
            for (PathResourceImplementation pr : (List)resources.second()) {
                res.add((PathResourceImplementation)new FR(pr, true));
            }
            return res;
        }
    }

    private static class FilterNonOpened
    implements Function<Pair<Boolean, List<? extends PathResourceImplementation>>, List<PathResourceImplementation>> {
        private FilterNonOpened() {
        }

        @Override
        public List<PathResourceImplementation> apply(@NonNull Pair<Boolean, List<? extends PathResourceImplementation>> resources) {
            ArrayList<PathResourceImplementation> res = new ArrayList<PathResourceImplementation>(((List)resources.second()).size());
            for (PathResourceImplementation pr : (List)resources.second()) {
                res.add((PathResourceImplementation)new FR(pr, (Boolean)resources.first()));
            }
            return res;
        }
    }

}

