/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.Tree
 */
package org.netbeans.modules.java.source.query;

import com.sun.source.tree.Tree;
import org.netbeans.api.java.source.Comment;
import org.netbeans.modules.java.source.query.CommentSet;

public interface CommentHandler {
    public boolean hasComments(Tree var1);

    public CommentSet getComments(Tree var1);

    public void copyComments(Tree var1, Tree var2);

    public void addComment(Tree var1, Comment var2);
}

