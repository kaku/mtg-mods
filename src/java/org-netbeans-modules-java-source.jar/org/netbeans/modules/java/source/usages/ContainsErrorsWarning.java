/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.source.usages;

import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class ContainsErrorsWarning
extends JPanel {
    private JCheckBox doNotAskAgain;
    private JLabel jLabel1;

    public ContainsErrorsWarning() {
        this.initComponents();
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.doNotAskAgain = new JCheckBox();
        Mnemonics.setLocalizedText((JLabel)this.jLabel1, (String)NbBundle.getBundle(ContainsErrorsWarning.class).getString("ContainsErrorsWarning.jLabel1.text"));
        Mnemonics.setLocalizedText((AbstractButton)this.doNotAskAgain, (String)NbBundle.getBundle(ContainsErrorsWarning.class).getString("ContainsErrorsWarning.doNotAskAgain.text"));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel1).addComponent(this.doNotAskAgain)).addContainerGap(-1, 32767)));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap(20, 32767).addComponent(this.jLabel1).addGap(18, 18, 18).addComponent(this.doNotAskAgain)));
        this.doNotAskAgain.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ContainsErrorsWarning.class, (String)"ACSN_CB_ContainsErrorsWarning.doNotAskAgain.text"));
        this.doNotAskAgain.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ContainsErrorsWarning.class, (String)"ACSD_CB_ContainsErrorsWarning.doNotAskAgain.text"));
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ContainsErrorsWarning.class, (String)"ACSN_ContainsErrorsWarning"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ContainsErrorsWarning.class, (String)"ACSD_ContainsErrorsWarning"));
    }

    public boolean getAskBeforeRunning() {
        return !this.doNotAskAgain.isSelected();
    }
}

