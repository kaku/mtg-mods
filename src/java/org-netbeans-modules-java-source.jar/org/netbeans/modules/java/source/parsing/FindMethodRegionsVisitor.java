/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.util.SimpleTreeVisitor
 *  com.sun.source.util.SourcePositions
 *  org.openide.util.Pair
 */
package org.netbeans.modules.java.source.parsing;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.SimpleTreeVisitor;
import com.sun.source.util.SourcePositions;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.modules.java.source.parsing.DocPositionRegion;
import org.openide.util.Pair;

class FindMethodRegionsVisitor
extends SimpleTreeVisitor<Void, Void> {
    private final Document doc;
    private final SourcePositions pos;
    private final AtomicBoolean canceled;
    private CompilationUnitTree cu;
    private final List<Pair<DocPositionRegion, MethodTree>> posRegions = new LinkedList<Pair<DocPositionRegion, MethodTree>>();

    public FindMethodRegionsVisitor(Document doc, SourcePositions pos, AtomicBoolean canceled) {
        assert (doc != null);
        assert (pos != null);
        assert (canceled != null);
        this.doc = doc;
        this.pos = pos;
        this.canceled = canceled;
    }

    public List<Pair<DocPositionRegion, MethodTree>> getResult() {
        if (this.canceled.get()) {
            this.posRegions.clear();
        }
        return this.posRegions;
    }

    public Void visitCompilationUnit(CompilationUnitTree node, Void p) {
        this.cu = node;
        for (Tree t : node.getTypeDecls()) {
            this.visit(t, (Object)p);
        }
        return null;
    }

    public Void visitClass(ClassTree node, Void p) {
        for (Tree t : node.getMembers()) {
            this.visit(t, (Object)p);
        }
        return null;
    }

    public Void visitMethod(MethodTree node, Void p) {
        assert (this.cu != null);
        if (!this.canceled.get()) {
            int startPos = (int)this.pos.getStartPosition(this.cu, (Tree)node.getBody());
            int endPos = (int)this.pos.getEndPosition(this.cu, (Tree)node.getBody());
            if (startPos >= 0) {
                try {
                    this.posRegions.add((Pair)Pair.of((Object)((Object)new DocPositionRegion(this.doc, startPos, endPos)), (Object)node));
                }
                catch (BadLocationException e) {
                    this.posRegions.clear();
                }
            }
        }
        return null;
    }
}

