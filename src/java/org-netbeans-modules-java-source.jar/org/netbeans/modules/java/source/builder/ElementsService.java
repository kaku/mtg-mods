/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.code.Scope
 *  com.sun.tools.javac.code.Scope$Entry
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.code.Symbol$MethodSymbol
 *  com.sun.tools.javac.code.Symbol$TypeSymbol
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.code.Type$MethodType
 *  com.sun.tools.javac.code.TypeTag
 *  com.sun.tools.javac.code.Types
 *  com.sun.tools.javac.model.JavacTypes
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Key
 *  com.sun.tools.javac.util.List
 *  com.sun.tools.javac.util.Name
 *  com.sun.tools.javac.util.Names
 */
package org.netbeans.modules.java.source.builder;

import com.sun.tools.javac.code.Scope;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.TypeTag;
import com.sun.tools.javac.model.JavacTypes;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.ExecutableType;
import javax.lang.model.util.Types;

public class ElementsService {
    private com.sun.tools.javac.code.Types jctypes;
    private Names names;
    private Types types;
    private static final Context.Key<ElementsService> KEY = new Context.Key();

    public static ElementsService instance(Context context) {
        ElementsService instance = (ElementsService)context.get(KEY);
        if (instance == null) {
            instance = new ElementsService(context);
        }
        return instance;
    }

    protected ElementsService(Context context) {
        context.put(KEY, (Object)this);
        this.jctypes = com.sun.tools.javac.code.Types.instance((Context)context);
        this.names = Names.instance((Context)context);
        this.types = JavacTypes.instance((Context)context);
    }

    public TypeElement outermostTypeElement(Element element) {
        Element e = element;
        Element prev = null;
        while (e.getKind() != ElementKind.PACKAGE) {
            prev = e;
            e = e.getEnclosingElement();
        }
        return prev instanceof TypeElement ? (TypeElement)prev : null;
    }

    public PackageElement packageElement(Element element) {
        Element e = element;
        while (e.getKind() != ElementKind.PACKAGE) {
            e = e.getEnclosingElement();
        }
        return (PackageElement)e;
    }

    public boolean overridesMethod(ExecutableElement element) {
        Symbol.MethodSymbol m = (Symbol.MethodSymbol)element;
        if ((m.flags() & 8) == 0) {
            Symbol.ClassSymbol owner = (Symbol.ClassSymbol)m.owner;
            Type sup = this.jctypes.supertype(m.owner.type);
            while (sup.hasTag(TypeTag.CLASS)) {
                Scope.Entry e = sup.tsym.members().lookup(m.name);
                while (e.scope != null) {
                    if (m.overrides(e.sym, (Symbol.TypeSymbol)owner, this.jctypes, true)) {
                        return true;
                    }
                    e = e.next();
                }
                sup = this.jctypes.supertype(sup);
            }
        }
        return false;
    }

    public boolean implementsMethod(ExecutableElement element) {
        Symbol.MethodSymbol m = (Symbol.MethodSymbol)element;
        Symbol.TypeSymbol owner = (Symbol.TypeSymbol)m.owner;
        for (Type type : this.jctypes.interfaces(m.owner.type)) {
            Scope.Entry e = type.tsym.members().lookup(m.name);
            while (e.scope != null) {
                if (m.overrides(e.sym, owner, this.jctypes, true)) {
                    return true;
                }
                e = e.next();
            }
        }
        return false;
    }

    public boolean alreadyDefinedIn(CharSequence name, ExecutableType method, TypeElement enclClass) {
        Type.MethodType meth = ((Type)method).asMethodType();
        Symbol.ClassSymbol clazz = (Symbol.ClassSymbol)enclClass;
        Scope scope = clazz.members();
        Name n = this.names.fromString(name.toString());
        Scope.Entry e = scope.lookup(n);
        while (e.scope == scope) {
            if (e.sym.type instanceof ExecutableType && this.types.isSubsignature((ExecutableType)meth, (ExecutableType)e.sym.type)) {
                return true;
            }
            e = e.next();
        }
        return false;
    }

    public boolean isMemberOf(Element e, TypeElement type) {
        return ((Symbol)e).isMemberOf((Symbol.TypeSymbol)type, this.jctypes);
    }

    public boolean isDeprecated(Element element) {
        Symbol sym = (Symbol)element;
        if ((sym.flags() & 131072) != 0 && (sym.owner.flags() & 131072) == 0) {
            return true;
        }
        Symbol.ClassSymbol owner = sym.enclClass();
        Type sup = this.jctypes.supertype(owner.type);
        while (sup.hasTag(TypeTag.CLASS)) {
            Scope.Entry e = sup.tsym.members().lookup(sym.name);
            while (e.scope != null) {
                if (sym.overrides(e.sym, (Symbol.TypeSymbol)owner, this.jctypes, true) && (e.sym.flags() & 131072) != 0) {
                    return true;
                }
                e = e.next();
            }
            sup = this.jctypes.supertype(sup);
        }
        return false;
    }

    public boolean isLocal(Element element) {
        return ((Symbol)element).isLocal();
    }

    public CharSequence getFullName(Element element) {
        Symbol sym = (Symbol)element;
        return element instanceof Symbol.ClassSymbol ? ((Symbol.ClassSymbol)element).fullname : Symbol.TypeSymbol.formFullName((Name)sym.name, (Symbol)sym.owner);
    }

    public Element getImplementationOf(ExecutableElement method, TypeElement origin) {
        return ((Symbol.MethodSymbol)method).implementation((Symbol.TypeSymbol)origin, this.jctypes, true);
    }

    public boolean isSynthetic(Element e) {
        return (((Symbol)e).flags() & 4096) != 0 || (((Symbol)e).flags() & 0x1000000000L) != 0;
    }

    public ExecutableElement getOverriddenMethod(ExecutableElement method) {
        Symbol.MethodSymbol m = (Symbol.MethodSymbol)method;
        Symbol.ClassSymbol origin = (Symbol.ClassSymbol)m.owner;
        Symbol.MethodSymbol bridgeCandidate = null;
        Type t = this.jctypes.supertype(origin.type);
        while (t.hasTag(TypeTag.CLASS)) {
            Symbol.TypeSymbol c = t.tsym;
            Scope.Entry e = c.members().lookup(m.name);
            while (e.scope != null) {
                if (m.overrides(e.sym, (Symbol.TypeSymbol)origin, this.jctypes, false)) {
                    if ((e.sym.flags() & 0x80000000L) > 0) {
                        if (bridgeCandidate == null) {
                            bridgeCandidate = (Symbol.MethodSymbol)e.sym;
                        }
                    } else {
                        return (Symbol.MethodSymbol)e.sym;
                    }
                }
                e = e.next();
            }
            t = this.jctypes.supertype(t);
        }
        return bridgeCandidate;
    }
}

