/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.util.Convert
 *  com.sun.tools.javac.util.Name
 *  org.netbeans.modules.classfile.CPClassInfo
 *  org.netbeans.modules.classfile.CPEntry
 *  org.netbeans.modules.classfile.CPFieldInfo
 *  org.netbeans.modules.classfile.CPMethodInfo
 *  org.netbeans.modules.classfile.ClassFile
 *  org.netbeans.modules.classfile.ClassName
 *  org.netbeans.modules.classfile.Code
 *  org.netbeans.modules.classfile.ConstantPool
 *  org.netbeans.modules.classfile.Method
 */
package org.netbeans.modules.java.source.usages;

import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.util.Convert;
import com.sun.tools.javac.util.Name;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.IntersectionType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import org.netbeans.modules.classfile.CPClassInfo;
import org.netbeans.modules.classfile.CPEntry;
import org.netbeans.modules.classfile.CPFieldInfo;
import org.netbeans.modules.classfile.CPMethodInfo;
import org.netbeans.modules.classfile.ClassFile;
import org.netbeans.modules.classfile.ClassName;
import org.netbeans.modules.classfile.Code;
import org.netbeans.modules.classfile.ConstantPool;
import org.netbeans.modules.classfile.Method;
import org.netbeans.modules.java.source.usages.BytecodeDecoder;

public class ClassFileUtil {
    private static final Logger log = Logger.getLogger(ClassFileUtil.class.getName());
    private static final Set<ElementKind> TYPE_DECLS = EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE, ElementKind.ENUM, ElementKind.ANNOTATION_TYPE);

    private ClassFileUtil() {
    }

    public static boolean accessesFiledOrMethod(String[] fieldInfo, String[] methodInfo, Code c, ConstantPool cp) {
        BytecodeDecoder bd = new BytecodeDecoder(c.getByteCodes());
        block4 : for (byte[] iw : bd) {
            switch (iw[0] & 255) {
                String className;
                String signature;
                int cpIndex;
                case 178: 
                case 179: 
                case 180: 
                case 181: {
                    if (fieldInfo == null) break;
                    cpIndex = BytecodeDecoder.toInt(iw[1], iw[2]);
                    CPFieldInfo cpFieldInfo = (CPFieldInfo)cp.get(cpIndex);
                    className = cpFieldInfo.getClassName().getInternalName();
                    String fieldName = cpFieldInfo.getFieldName();
                    signature = cpFieldInfo.getDescriptor();
                    if (!fieldInfo[0].equals(className) || fieldInfo[1] != null && (!fieldInfo[1].equals(fieldName) || !fieldInfo[2].equals(signature))) continue block4;
                    return true;
                }
                case 182: 
                case 183: 
                case 184: 
                case 185: {
                    if (methodInfo == null) break;
                    cpIndex = BytecodeDecoder.toInt(iw[1], iw[2]);
                    CPMethodInfo cpMethodInfo = (CPMethodInfo)cp.get(cpIndex);
                    className = cpMethodInfo.getClassName().getInternalName();
                    String methodName = cpMethodInfo.getMethodName();
                    signature = cpMethodInfo.getDescriptor();
                    if (!methodInfo[0].equals(className) || methodInfo[1] != null && (!methodInfo[1].equals(methodName) || !methodInfo[2].equals(signature))) break;
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean accessesFiledOrMethod(String[] fieldInfo, String[] methodInfo, Method m) {
        Code c = m.getCode();
        if (c != null) {
            ConstantPool cp = m.getClassFile().getConstantPool();
            return ClassFileUtil.accessesFiledOrMethod(fieldInfo, methodInfo, c, cp);
        }
        return false;
    }

    public static <T extends Method> Collection<T> accessesFiled(String[] fieldInfo, Collection<T> methods) {
        LinkedList<Method> result = new LinkedList<Method>();
        for (Method m : methods) {
            if (!ClassFileUtil.accessesFiledOrMethod(fieldInfo, null, m)) continue;
            result.add(m);
        }
        return result;
    }

    public static <T extends Method> Collection<T> callsMethod(String[] methodInfo, Collection<T> methods) {
        LinkedList<Method> result = new LinkedList<Method>();
        for (Method m : methods) {
            if (!ClassFileUtil.accessesFiledOrMethod(null, methodInfo, m)) continue;
            result.add(m);
        }
        return result;
    }

    public static String[] createFieldDescriptor(VariableElement ve) {
        assert (ve != null);
        String[] result = new String[3];
        Element enclosingElement = ve.getEnclosingElement();
        if (enclosingElement != null && enclosingElement.asType().getKind() == TypeKind.NONE) {
            result[0] = "";
        } else {
            assert (enclosingElement instanceof TypeElement);
            result[0] = ClassFileUtil.encodeClassNameOrArray((TypeElement)enclosingElement);
        }
        result[1] = ve.getSimpleName().toString();
        StringBuilder sb = new StringBuilder();
        ClassFileUtil.encodeType(ve.asType(), sb);
        result[2] = sb.toString();
        return result;
    }

    public static String[] createExecutableDescriptor(ExecutableElement ee) {
        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "Calling createExecutableDescriptor: ExecutableElement = {0}", ee);
        }
        assert (ee != null && ee.asType() != null);
        ElementKind kind = ee.getKind();
        Object[] result = kind == ElementKind.STATIC_INIT || kind == ElementKind.INSTANCE_INIT ? new String[2] : new String[3];
        Element enclosingType = ee.getEnclosingElement();
        if (enclosingType != null && enclosingType.asType().getKind() == TypeKind.NONE) {
            result[0] = "";
        } else {
            assert (enclosingType instanceof TypeElement);
            result[0] = ClassFileUtil.encodeClassNameOrArray((TypeElement)enclosingType);
        }
        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "Result of encodeClassNameOrArray = {0}", result[0]);
        }
        if (kind == ElementKind.METHOD || kind == ElementKind.CONSTRUCTOR) {
            StringBuilder retType = new StringBuilder();
            if (kind == ElementKind.METHOD) {
                result[1] = ee.getSimpleName().toString();
                if (ee.asType().getKind() == TypeKind.EXECUTABLE) {
                    ClassFileUtil.encodeType(ee.getReturnType(), retType);
                }
            } else {
                result[1] = "<init>";
                retType.append('V');
            }
            StringBuilder sb = new StringBuilder();
            sb.append('(');
            for (VariableElement pd : ee.getParameters()) {
                ClassFileUtil.encodeType(pd.asType(), sb);
            }
            sb.append(')');
            sb.append(retType);
            result[2] = sb.toString();
        } else if (kind == ElementKind.INSTANCE_INIT) {
            result[1] = "<init>";
        } else if (kind == ElementKind.STATIC_INIT) {
            result[1] = "<cinit>";
        } else {
            throw new IllegalArgumentException();
        }
        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "Result of createExecutableDescriptor = {0}", Arrays.toString(result));
        }
        return result;
    }

    public static String encodeClassNameOrArray(TypeElement td) {
        assert (td != null);
        javax.lang.model.element.Name qname = td.getQualifiedName();
        TypeMirror enclosingType = td.getEnclosingElement().asType();
        if (qname != null && enclosingType != null && enclosingType.getKind() == TypeKind.NONE && "Array".equals(qname.toString())) {
            return "[";
        }
        return ClassFileUtil.encodeClassName(td);
    }

    public static String encodeClassName(TypeElement td) {
        assert (td != null);
        StringBuilder sb = new StringBuilder();
        ClassFileUtil.encodeClassName(td, sb, '.');
        return sb.toString();
    }

    private static void encodeType(TypeMirror type, StringBuilder sb) {
        switch (type.getKind()) {
            case VOID: {
                sb.append('V');
                break;
            }
            case BOOLEAN: {
                sb.append('Z');
                break;
            }
            case BYTE: {
                sb.append('B');
                break;
            }
            case SHORT: {
                sb.append('S');
                break;
            }
            case INT: {
                sb.append('I');
                break;
            }
            case LONG: {
                sb.append('J');
                break;
            }
            case CHAR: {
                sb.append('C');
                break;
            }
            case FLOAT: {
                sb.append('F');
                break;
            }
            case DOUBLE: {
                sb.append('D');
                break;
            }
            case ARRAY: {
                sb.append('[');
                assert (type instanceof ArrayType);
                ClassFileUtil.encodeType(((ArrayType)type).getComponentType(), sb);
                break;
            }
            case DECLARED: {
                sb.append('L');
                TypeElement te = (TypeElement)((DeclaredType)type).asElement();
                ClassFileUtil.encodeClassName(te, sb, '/');
                sb.append(';');
                break;
            }
            case TYPEVAR: {
                assert (type instanceof TypeVariable);
                TypeVariable tr = (TypeVariable)type;
                TypeMirror upperBound = tr.getUpperBound();
                if (upperBound.getKind() == TypeKind.NULL) {
                    sb.append("Ljava/lang/Object;");
                    break;
                }
                ClassFileUtil.encodeType(upperBound, sb);
                break;
            }
            case ERROR: {
                TypeElement te = (TypeElement)((DeclaredType)type).asElement();
                if (te != null) {
                    sb.append('L');
                    ClassFileUtil.encodeClassName(te, sb, '/');
                    sb.append(';');
                    break;
                }
            }
            case INTERSECTION: {
                ClassFileUtil.encodeType(((IntersectionType)type).getBounds().get(0), sb);
                break;
            }
            default: {
                throw new IllegalArgumentException(String.format("Unsupported type: %s, kind: %s", new Object[]{type, type.getKind()}));
            }
        }
    }

    public static void encodeClassName(TypeElement te, StringBuilder sb, char separator) {
        Name name = ((Symbol.ClassSymbol)te).flatname;
        assert (name != null);
        int nameLength = name.getByteLength();
        char[] nameChars = new char[nameLength];
        int charLength = Convert.utf2chars((byte[])name.getByteArray(), (int)name.getByteOffset(), (char[])nameChars, (int)0, (int)nameLength);
        if (separator != '.') {
            for (int i = 0; i < charLength; ++i) {
                if (nameChars[i] != '.') continue;
                nameChars[i] = separator;
            }
        }
        sb.append(nameChars, 0, charLength);
    }

    public static ClassName getType(String jvmTypeId) {
        if (jvmTypeId.length() < 2) {
            return null;
        }
        if (jvmTypeId.charAt(0) == 'L') {
            return ClassName.getClassName((String)jvmTypeId);
        }
        if (jvmTypeId.charAt(0) == '[') {
            return ClassFileUtil.getType(jvmTypeId.substring(1));
        }
        return null;
    }

    public static ClassName getType(CPClassInfo ci) {
        String type = ci.getName();
        if (type.charAt(0) == '[') {
            return ClassFileUtil.getType(type);
        }
        return ci.getClassName();
    }

    public static ClassName[] getTypesFromMethodTypeSignature(String jvmTypeId) {
        HashSet<ClassName> result = new HashSet<ClassName>();
        ClassFileUtil.methodTypeSignature(jvmTypeId, new int[]{0}, result);
        return result.toArray((T[])new ClassName[result.size()]);
    }

    public static ClassName[] getTypesFromFiledTypeSignature(String jvmTypeId) {
        HashSet<ClassName> result = new HashSet<ClassName>();
        ClassFileUtil.typeSignatureType(jvmTypeId, new int[]{0}, result, false);
        return result.toArray((T[])new ClassName[result.size()]);
    }

    public static ClassName[] getTypesFromClassTypeSignature(String jvmTypeId) {
        HashSet<ClassName> result = new HashSet<ClassName>();
        ClassFileUtil.classTypeSignature(jvmTypeId, new int[]{0}, result);
        return result.toArray((T[])new ClassName[result.size()]);
    }

    private static char getChar(String buffer, int pos) {
        if (pos >= buffer.length()) {
            throw new IllegalStateException();
        }
        return buffer.charAt(pos);
    }

    private static void classTypeSignature(String jvmTypeId, int[] pos, Set<ClassName> s) {
        char c = ClassFileUtil.getChar(jvmTypeId, pos[0]);
        if (c == '<') {
            ClassFileUtil.formalTypeParameters(jvmTypeId, pos, s);
            c = ClassFileUtil.getChar(jvmTypeId, pos[0]);
        }
        ClassFileUtil.typeSignatureType(jvmTypeId, pos, s, false);
        while (pos[0] < jvmTypeId.length()) {
            ClassFileUtil.typeSignatureType(jvmTypeId, pos, s, false);
        }
    }

    private static void methodTypeSignature(String jvmTypeId, int[] pos, Set<ClassName> s) {
        char c = ClassFileUtil.getChar(jvmTypeId, pos[0]);
        if (c == '<') {
            ClassFileUtil.formalTypeParameters(jvmTypeId, pos, s);
            c = ClassFileUtil.getChar(jvmTypeId, pos[0]);
        }
        if (c != '(') {
            throw new IllegalStateException(jvmTypeId);
        }
        int[] arrn = pos;
        arrn[0] = arrn[0] + 1;
        c = ClassFileUtil.getChar(jvmTypeId, pos[0]);
        while (c != ')') {
            ClassFileUtil.typeSignatureType(jvmTypeId, pos, s, false);
            c = ClassFileUtil.getChar(jvmTypeId, pos[0]);
        }
        int[] arrn2 = pos;
        arrn2[0] = arrn2[0] + 1;
        ClassFileUtil.typeSignatureType(jvmTypeId, pos, s, false);
    }

    private static void formalTypeParam(String jvmTypeId, int[] pos, Set<ClassName> s) {
        char c;
        int n;
        do {
            int[] arrn = pos;
            n = arrn[0];
            arrn[0] = n + 1;
        } while ((c = ClassFileUtil.getChar(jvmTypeId, n)) != ':');
        c = ClassFileUtil.getChar(jvmTypeId, pos[0]);
        if (c != ':') {
            ClassFileUtil.typeSignatureType(jvmTypeId, pos, s, true);
            c = ClassFileUtil.getChar(jvmTypeId, pos[0]);
        }
        while (c == ':') {
            int[] arrn = pos;
            arrn[0] = arrn[0] + 1;
            ClassFileUtil.typeSignatureType(jvmTypeId, pos, s, true);
            c = ClassFileUtil.getChar(jvmTypeId, pos[0]);
        }
    }

    private static void formalTypeParameters(String jvmTypeId, int[] pos, Set<ClassName> s) {
        int[] arrn = pos;
        int n = arrn[0];
        arrn[0] = n + 1;
        char c = ClassFileUtil.getChar(jvmTypeId, n);
        if (c != '<') {
            throw new IllegalArgumentException(jvmTypeId);
        }
        c = ClassFileUtil.getChar(jvmTypeId, pos[0]);
        while (c != '>') {
            ClassFileUtil.formalTypeParam(jvmTypeId, pos, s);
            c = ClassFileUtil.getChar(jvmTypeId, pos[0]);
        }
        int[] arrn2 = pos;
        arrn2[0] = arrn2[0] + 1;
    }

    private static void typeArgument(String jvmTypeId, int[] pos, Set<ClassName> s) {
        char c = ClassFileUtil.getChar(jvmTypeId, pos[0]);
        if (c == '*') {
            int[] arrn = pos;
            arrn[0] = arrn[0] + 1;
            return;
        }
        if (c == '+' || c == '-') {
            int[] arrn = pos;
            arrn[0] = arrn[0] + 1;
            ClassFileUtil.typeSignatureType(jvmTypeId, pos, s, true);
        } else {
            ClassFileUtil.typeSignatureType(jvmTypeId, pos, s, true);
        }
    }

    private static void typeArgumentsList(String jvmTypeId, int[] pos, Set<ClassName> s) {
        int[] arrn = pos;
        int n = arrn[0];
        arrn[0] = n + 1;
        char c = ClassFileUtil.getChar(jvmTypeId, n);
        if (c != '<') {
            throw new IllegalStateException(jvmTypeId);
        }
        c = ClassFileUtil.getChar(jvmTypeId, pos[0]);
        while (c != '>') {
            ClassFileUtil.typeArgument(jvmTypeId, pos, s);
            c = ClassFileUtil.getChar(jvmTypeId, pos[0]);
        }
        int[] arrn2 = pos;
        arrn2[0] = arrn2[0] + 1;
    }

    private static void typeSignatureType(String jvmTypeId, int[] pos, Set<ClassName> s, boolean add) {
        int[] arrn = pos;
        int n = arrn[0];
        arrn[0] = n + 1;
        char c = ClassFileUtil.getChar(jvmTypeId, n);
        switch (c) {
            case 'B': 
            case 'C': 
            case 'D': 
            case 'F': 
            case 'I': 
            case 'J': 
            case 'S': 
            case 'V': 
            case 'Z': {
                break;
            }
            case 'L': {
                StringBuilder builder = new StringBuilder();
                do {
                    builder.append(c);
                    c = ClassFileUtil.getChar(jvmTypeId, pos[0]);
                    if (c == '<') {
                        ClassFileUtil.typeArgumentsList(jvmTypeId, pos, s);
                        int[] arrn2 = pos;
                        int n2 = arrn2[0];
                        arrn2[0] = n2 + 1;
                        c = ClassFileUtil.getChar(jvmTypeId, n2);
                        continue;
                    }
                    int[] arrn3 = pos;
                    arrn3[0] = arrn3[0] + 1;
                } while (c != ';');
                builder.append(c);
                if (!add) break;
                s.add(ClassName.getClassName((String)builder.toString()));
                break;
            }
            case 'T': {
                int n3;
                do {
                    int[] arrn4 = pos;
                    n3 = arrn4[0];
                    arrn4[0] = n3 + 1;
                } while ((c = ClassFileUtil.getChar(jvmTypeId, n3)) != ';');
                break;
            }
            case '[': {
                ClassFileUtil.typeSignatureType(jvmTypeId, pos, s, add);
            }
        }
    }

}

