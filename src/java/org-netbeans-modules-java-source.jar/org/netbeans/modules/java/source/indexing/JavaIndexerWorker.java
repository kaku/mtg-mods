/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.modules.parsing.spi.indexing.Indexable
 *  org.netbeans.modules.parsing.spi.indexing.SuspendStatus
 *  org.openide.util.Exceptions
 *  org.openide.util.RequestProcessor
 */
package org.netbeans.modules.java.source.indexing;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.java.source.indexing.JavaCustomIndexer;
import org.netbeans.modules.java.source.parsing.PrefetchableJavaFileObject;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.netbeans.modules.parsing.spi.indexing.SuspendStatus;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;

public final class JavaIndexerWorker {
    private static final Logger LOG = Logger.getLogger(JavaIndexerWorker.class.getName());
    private static final int DEFAULT_PROC_COUNT = 2;
    private static final int DEFAULT_BUFFER_SIZE = 1048576;
    private static final int MIN_PROC = 4;
    private static final int MIN_FILES = 10;
    private static final boolean PREFETCH_DISABLED = Boolean.getBoolean("SourcePrefetcher.disabled");
    private static final int PROC_COUNT = Integer.getInteger("SourcePrefetcher.proc.count", 2);
    static int BUFFER_SIZE = Integer.getInteger("SourcePrefetcher.buffer.size", 1048576);
    static Boolean TEST_DO_PREFETCH;
    private static final RequestProcessor RP;

    private JavaIndexerWorker() {
        throw new IllegalStateException("No instance allowed");
    }

    public static boolean supportsConcurrent() {
        int procCount = Runtime.getRuntime().availableProcessors();
        LOG.log(Level.FINER, "Proc Count: {0}, Prefetch disabled: {1}", new Object[]{procCount, PREFETCH_DISABLED});
        return procCount >= 4 && !PREFETCH_DISABLED;
    }

    @NonNull
    public static Executor getExecutor() {
        return RP;
    }

    @CheckForNull
    static /* varargs */ <T> T reduce(@NullAllowed T initialValue, @NonNull BinaryOperator<T> f, @NonNull Callable<T> ... actions) throws ExecutionException, InterruptedException {
        T result = initialValue;
        if (JavaIndexerWorker.supportsConcurrent()) {
            LOG.log(Level.FINE, "Using concurrent reduce, {0} workers", PROC_COUNT);
            List coResults = RP.invokeAll(Arrays.asList(actions));
            for (Future coResult : coResults) {
                result = f.apply(result, coResult.get());
            }
        } else {
            LOG.log(Level.FINE, "Using sequential reduce");
            for (Callable<T> action : actions) {
                try {
                    result = f.apply(result, action.call());
                    continue;
                }
                catch (Exception ex) {
                    throw new ExecutionException(ex);
                }
            }
        }
        return result;
    }

    @NonNull
    static Iterator<? extends JavaCustomIndexer.CompileTuple> getCompileTupleIterator(@NonNull Collection<? extends JavaCustomIndexer.CompileTuple> files, @NonNull SuspendStatus suspendStatus) {
        boolean doPrefetch;
        int probSize = files.size();
        LOG.log(Level.FINER, "File count: {0}", probSize);
        boolean supportsPar = JavaIndexerWorker.supportsConcurrent() && probSize > 10;
        boolean bl = doPrefetch = TEST_DO_PREFETCH != null ? TEST_DO_PREFETCH : supportsPar;
        if (doPrefetch && suspendStatus.isSuspendSupported()) {
            LOG.log(Level.FINE, "Using concurrent iterator, {0} workers", PROC_COUNT);
            return new ConcurrentIterator(files, suspendStatus);
        }
        LOG.fine("Using sequential iterator");
        return new NopRemoveItDecorator(files.iterator(), suspendStatus);
    }

    @NonNull
    private static <T> CompletionService<T> newCompletionService() {
        return new ExecutorCompletionService((Executor)RP);
    }

    static /* synthetic */ CompletionService access$300() {
        return JavaIndexerWorker.newCompletionService();
    }

    static {
        RP = new RequestProcessor(JavaIndexerWorker.class.getName(), PROC_COUNT, false, false);
    }

    private static final class ConcurrentIterator
    extends SuspendableIterator
    implements Closeable {
        private static final JavaCustomIndexer.CompileTuple DUMMY = new JavaCustomIndexer.CompileTuple(null, null, true, false);
        private final CompletionService<JavaCustomIndexer.CompileTuple> cs = JavaIndexerWorker.access$300();
        private final Semaphore sem = new Semaphore(JavaIndexerWorker.BUFFER_SIZE);
        private final Deque<JavaCustomIndexer.CompileTuple> virtuals = new ArrayDeque<JavaCustomIndexer.CompileTuple>();
        private int count;
        private JavaCustomIndexer.CompileTuple active;
        private volatile boolean closed;

        private ConcurrentIterator(@NonNull Iterable<? extends JavaCustomIndexer.CompileTuple> files, @NonNull SuspendStatus suspendStatus) {
            super(suspendStatus);
            for (final JavaCustomIndexer.CompileTuple ct : files) {
                if (ct.virtual) {
                    this.virtuals.offer(ct);
                } else {
                    this.cs.submit(new Callable<JavaCustomIndexer.CompileTuple>(){

                        @NonNull
                        @Override
                        public JavaCustomIndexer.CompileTuple call() throws Exception {
                            ConcurrentIterator.this.safePark();
                            if (ConcurrentIterator.this.closed) {
                                LOG.finest("Skipping prefetch due to close.");
                                return ct;
                            }
                            int len = Math.min(JavaIndexerWorker.BUFFER_SIZE, ct.jfo.prefetch());
                            if (LOG.isLoggable(Level.FINEST) && ConcurrentIterator.this.sem.availablePermits() - len < 0) {
                                LOG.finest("Buffer full");
                            }
                            ConcurrentIterator.this.sem.acquire(len);
                            return ct;
                        }
                    });
                }
                ++this.count;
            }
        }

        @Override
        public boolean hasNext() {
            this.ensureNotClosed();
            return this.count > 0;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public JavaCustomIndexer.CompileTuple next() {
            block10 : {
                this.ensureNotClosed();
                if (this.active != null) {
                    throw new IllegalStateException("Call remove to free resources");
                }
                if (!this.hasNext()) {
                    throw new IllegalStateException("No more tuples.");
                }
                this.safePark();
                try {
                    this.active = this.virtuals.isEmpty() ? this.cs.take().get() : this.virtuals.removeFirst();
                }
                catch (InterruptedException ex) {
                    this.active = DUMMY;
                    Exceptions.printStackTrace((Throwable)ex);
                }
                catch (ExecutionException ex) {
                    this.active = DUMMY;
                    Throwable rootCause = ex.getCause();
                    if (rootCause instanceof IOException) {
                        LOG.log(Level.INFO, rootCause.getLocalizedMessage());
                        break block10;
                    }
                    Exceptions.printStackTrace((Throwable)ex);
                }
                finally {
                    --this.count;
                }
            }
            return this.active == DUMMY ? null : this.active;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void remove() {
            this.ensureNotClosed();
            if (this.active == null) {
                throw new IllegalStateException("Call next before remove");
            }
            try {
                if (!this.active.virtual) {
                    int len = Math.min(JavaIndexerWorker.BUFFER_SIZE, this.active.jfo.dispose());
                    this.sem.release(len);
                }
            }
            finally {
                this.active = null;
            }
        }

        @Override
        public void close() throws IOException {
            this.ensureNotClosed();
            this.closed = true;
            this.sem.release(PROC_COUNT * JavaIndexerWorker.BUFFER_SIZE);
        }

        private void ensureNotClosed() {
            if (this.closed) {
                throw new IllegalStateException("Already closed SourcePrefetcher instance.");
            }
        }

    }

    private static final class NopRemoveItDecorator
    extends SuspendableIterator {
        private final Iterator<? extends JavaCustomIndexer.CompileTuple> delegate;

        private NopRemoveItDecorator(@NonNull Iterator<? extends JavaCustomIndexer.CompileTuple> delegate, @NonNull SuspendStatus suspendStatus) {
            super(suspendStatus);
            this.delegate = delegate;
        }

        @Override
        public boolean hasNext() {
            return this.delegate.hasNext();
        }

        @Override
        public JavaCustomIndexer.CompileTuple next() {
            return this.delegate.next();
        }

        @Override
        public void remove() {
        }
    }

    private static abstract class SuspendableIterator
    implements Iterator<JavaCustomIndexer.CompileTuple> {
        private final SuspendStatus suspendStatus;

        protected SuspendableIterator(@NonNull SuspendStatus suspendStatus) {
            assert (suspendStatus != null);
            this.suspendStatus = suspendStatus;
        }

        protected final void safePark() {
            try {
                this.suspendStatus.parkWhileSuspended();
            }
            catch (InterruptedException ex) {
                // empty catch block
            }
        }
    }

    static enum Bool implements BinaryOperator<Boolean>
    {
        AND{

            @NonNull
            @Override
            public Boolean apply(@NonNull Boolean left, @NonNull Boolean right) {
                return left == Boolean.TRUE ? right : Boolean.FALSE;
            }
        }
        ,
        OR{

            @NonNull
            @Override
            public Boolean apply(@NonNull Boolean left, @NonNull Boolean right) {
                return left == Boolean.FALSE ? right : Boolean.TRUE;
            }
        };
        

        private Bool() {
        }

    }

    static interface BinaryOperator<T> {
        public T apply(T var1, T var2);
    }

}

