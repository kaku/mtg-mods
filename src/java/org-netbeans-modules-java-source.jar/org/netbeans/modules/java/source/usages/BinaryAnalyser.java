/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.classfile.Annotation
 *  org.netbeans.modules.classfile.AnnotationComponent
 *  org.netbeans.modules.classfile.ArrayElementValue
 *  org.netbeans.modules.classfile.CPClassInfo
 *  org.netbeans.modules.classfile.CPFieldInfo
 *  org.netbeans.modules.classfile.CPInterfaceMethodInfo
 *  org.netbeans.modules.classfile.CPMethodInfo
 *  org.netbeans.modules.classfile.ClassElementValue
 *  org.netbeans.modules.classfile.ClassFile
 *  org.netbeans.modules.classfile.ClassName
 *  org.netbeans.modules.classfile.Code
 *  org.netbeans.modules.classfile.ConstantPool
 *  org.netbeans.modules.classfile.ElementValue
 *  org.netbeans.modules.classfile.EnumElementValue
 *  org.netbeans.modules.classfile.InvalidClassFormatException
 *  org.netbeans.modules.classfile.LocalVariableTableEntry
 *  org.netbeans.modules.classfile.LocalVariableTypeTableEntry
 *  org.netbeans.modules.classfile.Method
 *  org.netbeans.modules.classfile.NestedElementValue
 *  org.netbeans.modules.classfile.Parameter
 *  org.netbeans.modules.classfile.Variable
 *  org.netbeans.modules.parsing.impl.indexing.CancelRequest
 *  org.netbeans.modules.parsing.impl.indexing.IndexFactoryImpl
 *  org.netbeans.modules.parsing.impl.indexing.LogContext
 *  org.netbeans.modules.parsing.impl.indexing.SPIAccessor
 *  org.netbeans.modules.parsing.impl.indexing.SuspendSupport
 *  org.netbeans.modules.parsing.lucene.support.Convertor
 *  org.netbeans.modules.parsing.lucene.support.LowMemoryWatcher
 *  org.netbeans.modules.parsing.spi.indexing.Context
 *  org.netbeans.modules.parsing.spi.indexing.SuspendStatus
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 *  org.openide.util.Pair
 *  org.openide.util.Parameters
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.source.usages;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.swing.JComponent;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.modules.classfile.Annotation;
import org.netbeans.modules.classfile.AnnotationComponent;
import org.netbeans.modules.classfile.ArrayElementValue;
import org.netbeans.modules.classfile.CPClassInfo;
import org.netbeans.modules.classfile.CPFieldInfo;
import org.netbeans.modules.classfile.CPInterfaceMethodInfo;
import org.netbeans.modules.classfile.CPMethodInfo;
import org.netbeans.modules.classfile.ClassElementValue;
import org.netbeans.modules.classfile.ClassFile;
import org.netbeans.modules.classfile.ClassName;
import org.netbeans.modules.classfile.Code;
import org.netbeans.modules.classfile.ConstantPool;
import org.netbeans.modules.classfile.ElementValue;
import org.netbeans.modules.classfile.EnumElementValue;
import org.netbeans.modules.classfile.InvalidClassFormatException;
import org.netbeans.modules.classfile.LocalVariableTableEntry;
import org.netbeans.modules.classfile.LocalVariableTypeTableEntry;
import org.netbeans.modules.classfile.Method;
import org.netbeans.modules.classfile.NestedElementValue;
import org.netbeans.modules.classfile.Parameter;
import org.netbeans.modules.classfile.Variable;
import org.netbeans.modules.java.source.ElementHandleAccessor;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.usages.ClassFileUtil;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;
import org.netbeans.modules.java.source.usages.DocumentUtil;
import org.netbeans.modules.java.source.usages.LongHashMap;
import org.netbeans.modules.java.source.usages.UsagesData;
import org.netbeans.modules.parsing.impl.indexing.CancelRequest;
import org.netbeans.modules.parsing.impl.indexing.IndexFactoryImpl;
import org.netbeans.modules.parsing.impl.indexing.LogContext;
import org.netbeans.modules.parsing.impl.indexing.SPIAccessor;
import org.netbeans.modules.parsing.impl.indexing.SuspendSupport;
import org.netbeans.modules.parsing.lucene.support.Convertor;
import org.netbeans.modules.parsing.lucene.support.LowMemoryWatcher;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.netbeans.modules.parsing.spi.indexing.SuspendStatus;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import org.openide.util.Pair;
import org.openide.util.Parameters;
import org.openide.util.Utilities;

public class BinaryAnalyser {
    private static final String INIT = "<init>";
    private static final String CLINIT = "<clinit>";
    private static final String OUTHER_THIS_PREFIX = "this$";
    private static final String ACCESS_METHOD_PREFIX = "access$";
    private static final String ASSERTIONS_DISABLED = "$assertionsDisabled";
    private static final String ROOT = "/";
    private static final String TIME_STAMPS = "timestamps.properties";
    private static final String CRC = "crc.properties";
    private static final Logger LOGGER = Logger.getLogger(BinaryAnalyser.class.getName());
    private static final String JCOMPONENT = JComponent.class.getName();
    static final String OBJECT = Object.class.getName();
    private static boolean FULL_INDEX = Boolean.getBoolean("org.netbeans.modules.java.source.usages.BinaryAnalyser.fullIndex");
    private final ClassIndexImpl.Writer writer;
    private final File cacheRoot;
    private final List<Pair<Pair<String, String>, Object[]>> refs = new ArrayList<Pair<Pair<String, String>, Object[]>>();
    private final Set<Pair<String, String>> toDelete = new HashSet<Pair<String, String>>();
    private final LowMemoryWatcher lmListener;
    private Pair<LongHashMap<String>, Set<String>> timeStamps;

    BinaryAnalyser(@NonNull ClassIndexImpl.Writer writer, @NonNull File cacheRoot) {
        Parameters.notNull((CharSequence)"writer", (Object)writer);
        Parameters.notNull((CharSequence)"cacheRoot", (Object)cacheRoot);
        this.writer = writer;
        this.cacheRoot = cacheRoot;
        this.lmListener = LowMemoryWatcher.getInstance();
    }

    @NonNull
    public final Changes analyse(@NonNull Context ctx) throws IOException, IllegalArgumentException {
        Parameters.notNull((CharSequence)"ctx", (Object)ctx);
        RootProcessor p = this.createProcessor(ctx);
        if (p.execute()) {
            if (!p.hasChanges() && this.timeStampsEmpty()) {
                assert (this.refs.isEmpty());
                assert (this.toDelete.isEmpty());
                return UP_TO_DATE;
            }
            List<Pair<ElementHandle<TypeElement>, Long>> newState = p.result();
            List<Pair<ElementHandle<TypeElement>, Long>> oldState = this.loadCRCs(this.cacheRoot);
            boolean preBuildArgs = p.preBuildArgs();
            this.store();
            this.storeCRCs(this.cacheRoot, newState);
            this.storeTimeStamps();
            return BinaryAnalyser.diff(oldState, newState, preBuildArgs);
        }
        return FAILURE;
    }

    @Deprecated
    public final Changes analyse(@NonNull URL url) throws IOException, IllegalArgumentException {
        return this.analyse(SPIAccessor.getInstance().createContext(FileUtil.createMemoryFileSystem().getRoot(), url, "java", 14, null, false, false, false, SuspendSupport.NOP, null, null));
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    @NonNull
    private RootProcessor createProcessor(@NonNull Context ctx) throws IOException {
        URL root = ctx.getRootURI();
        String mainP = root.getProtocol();
        if ("jar".equals(mainP)) {
            URL innerURL = FileUtil.getArchiveFile((URL)root);
            if (!"file".equals(innerURL.getProtocol())) {
                FileObject rootFo = URLMapper.findFileObject((URL)root);
                if (rootFo == null) return new DeletedRootProcessor(ctx);
                if (this.isUpToDate("/", rootFo.lastModified().getTime())) return RootProcessor.UP_TO_DATE;
                return new NBFSProcessor(rootFo, ctx);
            }
            File archive = Utilities.toFile((URI)URI.create(innerURL.toExternalForm()));
            if (!archive.canRead()) return new DeletedRootProcessor(ctx);
            if (this.isUpToDate("/", archive.lastModified())) return RootProcessor.UP_TO_DATE;
            try {
                return new ArchiveProcessor(archive, ctx);
            }
            catch (ZipException e) {
                LOGGER.log(Level.WARNING, "Broken zip file: {0}", archive.getAbsolutePath());
                return RootProcessor.UP_TO_DATE;
            }
        }
        if (!"file".equals(mainP)) {
            FileObject rootFo = URLMapper.findFileObject((URL)root);
            if (rootFo == null) return new DeletedRootProcessor(ctx);
            return new NBFSProcessor(rootFo, ctx);
        }
        File rootFile = Utilities.toFile((URI)URI.create(root.toExternalForm()));
        if (rootFile.isDirectory()) {
            return new FolderProcessor(rootFile, ctx);
        }
        if (rootFile.exists()) return RootProcessor.UP_TO_DATE;
        return new DeletedRootProcessor(ctx);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private List<Pair<ElementHandle<TypeElement>, Long>> loadCRCs(File indexFolder) throws IOException {
        LinkedList<Pair<ElementHandle<TypeElement>, Long>> result;
        block8 : {
            result = new LinkedList<Pair<ElementHandle<TypeElement>, Long>>();
            File file = new File(indexFolder, "crc.properties");
            if (file.canRead()) {
                BufferedReader in = new BufferedReader(new InputStreamReader((InputStream)new FileInputStream(file), "UTF-8"));
                block5 : do {
                    String line;
                    while ((line = in.readLine()) != null) {
                        String[] parts = line.split("=");
                        if (parts.length != 2) continue;
                        try {
                            ElementHandle handle = ElementHandleAccessor.getInstance().create(ElementKind.OTHER, parts[0]);
                            Long crc = Long.parseLong(parts[1]);
                            result.add((Pair)Pair.of((Object)handle, (Object)crc));
                            continue block5;
                        }
                        catch (NumberFormatException e) {
                            continue;
                        }
                    }
                    break block8;
                    {
                        continue block5;
                        continue block5;
                        break;
                    }
                    break;
                } while (true);
                finally {
                    in.close();
                }
            }
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void storeCRCs(File indexFolder, List<Pair<ElementHandle<TypeElement>, Long>> state) throws IOException {
        File file = new File(indexFolder, "crc.properties");
        if (state.isEmpty()) {
            file.delete();
        } else {
            PrintWriter out = new PrintWriter(new OutputStreamWriter((OutputStream)new FileOutputStream(file), "UTF-8"));
            try {
                for (Pair<ElementHandle<TypeElement>, Long> pair : state) {
                    StringBuilder sb = new StringBuilder(((ElementHandle)pair.first()).getBinaryName());
                    sb.append('=');
                    sb.append((Long)pair.second());
                    out.println(sb.toString());
                }
            }
            finally {
                out.close();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @NonNull
    private Pair<LongHashMap<String>, Set<String>> getTimeStamps() throws IOException {
        if (this.timeStamps == null) {
            LongHashMap<String> map;
            block9 : {
                map = new LongHashMap<String>();
                File f = new File(this.cacheRoot, "timestamps.properties");
                if (f.exists()) {
                    BufferedReader in = new BufferedReader(new InputStreamReader((InputStream)new FileInputStream(f), "UTF-8"));
                    block5 : do {
                        String line;
                        while (null != (line = in.readLine())) {
                            int idx = line.indexOf(61);
                            if (idx == -1) continue;
                            try {
                                long ts = Long.parseLong(line.substring(idx + 1));
                                map.put(line.substring(0, idx), ts);
                                continue block5;
                            }
                            catch (NumberFormatException nfe) {
                                LOGGER.log(Level.FINE, "Invalid timestamp: line={0}, timestamps={1}, exception={2}", new Object[]{line, f.getPath(), nfe});
                                continue;
                            }
                        }
                        break block9;
                        {
                            continue block5;
                            continue block5;
                            break;
                        }
                        break;
                    } while (true);
                    finally {
                        in.close();
                    }
                }
            }
            this.timeStamps = Pair.of(map, new HashSet(map.keySet()));
        }
        return this.timeStamps;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void storeTimeStamps() throws IOException {
        File f = new File(this.cacheRoot, "timestamps.properties");
        if (this.timeStamps == null) {
            f.delete();
        } else {
            ((LongHashMap)this.timeStamps.first()).keySet().removeAll((Collection)this.timeStamps.second());
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter((OutputStream)new FileOutputStream(f), "UTF-8"));
            try {
                for (LongHashMap.Entry entry : ((LongHashMap)this.timeStamps.first()).entrySet()) {
                    out.write((String)entry.getKey());
                    out.write(61);
                    out.write(Long.toString(entry.getValue()));
                    out.newLine();
                }
                out.flush();
            }
            finally {
                this.timeStamps = null;
                out.close();
            }
        }
    }

    private boolean timeStampsEmpty() {
        return this.timeStamps == null || ((Set)this.timeStamps.second()).isEmpty();
    }

    private boolean isUpToDate(String resourceName, long timeStamp) throws IOException {
        Pair<LongHashMap<String>, Set<String>> ts = this.getTimeStamps();
        long oldTime = ((LongHashMap)ts.first()).put(resourceName, timeStamp);
        ((Set)ts.second()).remove(resourceName);
        return oldTime == timeStamp;
    }

    static Changes diff(List<Pair<ElementHandle<TypeElement>, Long>> oldState, List<Pair<ElementHandle<TypeElement>, Long>> newState, boolean preBuildArgs) {
        LinkedList<Object> changed = new LinkedList<Object>();
        LinkedList<Object> removed = new LinkedList<Object>();
        LinkedList<Object> added = new LinkedList<Object>();
        Iterator<Pair<ElementHandle<TypeElement>, Long>> oldIt = oldState.iterator();
        Iterator<Pair<ElementHandle<TypeElement>, Long>> newIt = newState.iterator();
        Pair<ElementHandle<TypeElement>, Long> oldE = null;
        Pair<ElementHandle<TypeElement>, Long> newE = null;
        while (oldIt.hasNext() && newIt.hasNext()) {
            int ni;
            if (oldE == null) {
                oldE = oldIt.next();
            }
            if (newE == null) {
                newE = newIt.next();
            }
            if ((ni = ((ElementHandle)oldE.first()).getBinaryName().compareTo(((ElementHandle)newE.first()).getBinaryName())) == 0) {
                if ((Long)oldE.second() == 0 || ((Long)oldE.second()).longValue() != ((Long)newE.second()).longValue()) {
                    changed.add(oldE.first());
                }
                newE = null;
                oldE = null;
                continue;
            }
            if (ni < 0) {
                removed.add(oldE.first());
                oldE = null;
                continue;
            }
            if (ni <= 0) continue;
            added.add(newE.first());
            newE = null;
        }
        if (oldE != null) {
            removed.add(oldE.first());
        }
        while (oldIt.hasNext()) {
            removed.add(oldIt.next().first());
        }
        if (newE != null) {
            added.add(newE.first());
        }
        while (newIt.hasNext()) {
            added.add(newIt.next().first());
        }
        return new Changes(true, added, removed, changed, preBuildArgs);
    }

    private void releaseData() {
        this.refs.clear();
        this.toDelete.clear();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void flush() throws IOException {
        try {
            if (this.refs.size() > 0 || this.toDelete.size() > 0) {
                this.writer.deleteAndFlush(this.refs, this.toDelete);
            }
        }
        finally {
            this.releaseData();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void store() throws IOException {
        try {
            this.writer.deleteAndStore(this.refs, this.toDelete);
        }
        finally {
            this.releaseData();
        }
    }

    private void delete(String className) throws IOException {
        assert (className != null);
        this.toDelete.add((Pair)Pair.of((Object)className, (Object)null));
    }

    private void analyse(InputStream inputStream) throws IOException {
        ClassFile classFile = new ClassFile(inputStream);
        ClassFileProcessor cfp = FULL_INDEX ? new FullIndexProcessor(classFile) : new ClassSignatureProcessor(classFile);
        this.delete(cfp.getClassName());
        UsagesData usages = cfp.analyse();
        String classNameType = cfp.getClassName() + DocumentUtil.encodeKind(this.getElementKind(classFile));
        Pair pair = Pair.of((Object)classNameType, (Object)null);
        this.addReferences(pair, usages);
    }

    private void addReferences(@NonNull Pair<String, String> name, @NonNull UsagesData<ClassName> usages) {
        assert (name != null);
        assert (usages != null);
        Object[] cr = new Object[]{usages.usagesToStrings(), usages.featureIdentsToString(), usages.identsToString()};
        this.refs.add((Pair)Pair.of(name, (Object)cr));
    }

    private ElementKind getElementKind(@NonNull ClassFile cf) {
        if (cf.isEnum()) {
            return ElementKind.ENUM;
        }
        if (cf.isAnnotation()) {
            return ElementKind.ANNOTATION_TYPE;
        }
        if ((cf.getAccess() & 512) == 512) {
            return ElementKind.INTERFACE;
        }
        return ElementKind.CLASS;
    }

    private final class DeletedRootProcessor
    extends RootProcessor {
        DeletedRootProcessor(Context ctx) throws IOException {
            super(ctx);
            Pair ts = BinaryAnalyser.this.getTimeStamps();
            if (!((LongHashMap)ts.first()).isEmpty()) {
                this.markChanged();
            }
        }

        @NonNull
        @Override
        protected boolean executeImpl() throws IOException {
            if (this.hasChanges()) {
                BinaryAnalyser.this.writer.clear();
            }
            return true;
        }
    }

    private final class NBFSProcessor
    extends RootProcessor {
        private final Enumeration<? extends FileObject> todo;
        private final FileObject root;

        NBFSProcessor(@NonNull FileObject root, Context ctx) throws IOException {
            super(ctx);
            assert (root != null);
            BinaryAnalyser.this.writer.clear();
            this.root = root;
            this.todo = root.getData(true);
            this.markChanged();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @NonNull
        @Override
        protected boolean executeImpl() throws IOException {
            while (this.todo.hasMoreElements()) {
                FileObject fo = this.todo.nextElement();
                if (this.accepts(fo.getName())) {
                    String rp = FileObjects.stripExtension(FileUtil.getRelativePath((FileObject)this.root, (FileObject)fo));
                    this.report(ElementHandleAccessor.getInstance().create(ElementKind.OTHER, FileObjects.convertFolder2Package(rp)), 0);
                    BufferedInputStream in = new BufferedInputStream(fo.getInputStream());
                    try {
                        BinaryAnalyser.this.analyse(in);
                    }
                    catch (InvalidClassFormatException icf) {
                        LOGGER.log(Level.WARNING, "Invalid class file format: {0}", FileUtil.getFileDisplayName((FileObject)fo));
                    }
                    finally {
                        in.close();
                    }
                    if (BinaryAnalyser.this.lmListener.isLowMemory()) {
                        BinaryAnalyser.this.flush();
                    }
                }
                if (!this.isCancelled()) continue;
                return false;
            }
            return true;
        }
    }

    private final class FolderProcessor
    extends RootProcessor {
        private final LinkedList<File> todo;
        private final String rootPath;

        public FolderProcessor(@NonNull File root, Context ctx) throws IOException {
            super(ctx);
            assert (root != null);
            String path = root.getAbsolutePath();
            if (path.charAt(path.length() - 1) != File.separatorChar) {
                path = path + File.separatorChar;
            }
            this.todo = new LinkedList();
            this.rootPath = path;
            File[] children = root.listFiles();
            if (children != null) {
                Collections.addAll(this.todo, children);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @NonNull
        @Override
        protected boolean executeImpl() throws IOException {
            while (!this.todo.isEmpty()) {
                File file = this.todo.removeFirst();
                if (file.isDirectory()) {
                    File[] c = file.listFiles();
                    if (c != null) {
                        Collections.addAll(this.todo, c);
                    }
                } else if (this.accepts(file.getName())) {
                    int slashIndex;
                    String filePath = file.getAbsolutePath();
                    long fileMTime = file.lastModified();
                    int dotIndex = filePath.lastIndexOf(46);
                    int endPos = dotIndex > (slashIndex = filePath.lastIndexOf(File.separatorChar)) ? dotIndex : filePath.length();
                    String relativePath = FileObjects.convertFolder2Package(filePath.substring(this.rootPath.length(), endPos), File.separatorChar);
                    this.report(ElementHandleAccessor.getInstance().create(ElementKind.OTHER, relativePath), fileMTime);
                    if (!BinaryAnalyser.this.isUpToDate(relativePath, fileMTime)) {
                        this.markChanged();
                        BinaryAnalyser.this.toDelete.add(Pair.of((Object)relativePath, (Object)null));
                        try {
                            BufferedInputStream in = new BufferedInputStream(new FileInputStream(file));
                            try {
                                BinaryAnalyser.this.analyse(in);
                            }
                            catch (InvalidClassFormatException icf) {
                                LOGGER.log(Level.WARNING, "Invalid class file format: {0}", file.getAbsolutePath());
                            }
                            finally {
                                in.close();
                            }
                        }
                        catch (IOException ex) {
                            LOGGER.log(Level.WARNING, "Cannot read file: {0}", file.getAbsolutePath());
                            LOGGER.log(Level.FINE, null, ex);
                        }
                        if (BinaryAnalyser.this.lmListener.isLowMemory()) {
                            BinaryAnalyser.this.flush();
                        }
                    }
                }
                if (!this.isCancelled()) continue;
                return false;
            }
            for (String deleted : (Set)BinaryAnalyser.this.getTimeStamps().second()) {
                BinaryAnalyser.this.delete(deleted);
                this.markChanged();
            }
            return true;
        }
    }

    private final class ArchiveProcessor
    extends RootProcessor {
        private final ZipFile zipFile;
        private final Enumeration<? extends ZipEntry> entries;

        ArchiveProcessor(@NonNull File file, Context ctx) throws IOException {
            super(ctx);
            assert (file != null);
            BinaryAnalyser.this.writer.clear();
            this.zipFile = new ZipFile(file);
            this.entries = this.zipFile.entries();
            this.markChanged();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         * Enabled aggressive block sorting
         * Enabled unnecessary exception pruning
         * Enabled aggressive exception aggregation
         */
        @NonNull
        @Override
        protected boolean executeImpl() throws IOException {
            try {
                while (this.entries.hasMoreElements()) {
                    ZipEntry ze;
                    try {
                        ze = this.entries.nextElement();
                    }
                    catch (InternalError err) {
                        LOGGER.log(Level.INFO, "Broken zip file: " + this.zipFile.getName(), err);
                        boolean bl = true;
                        this.zipFile.close();
                        return bl;
                    }
                    if (!ze.isDirectory() && this.accepts(ze.getName())) {
                        this.report(ElementHandleAccessor.getInstance().create(ElementKind.OTHER, FileObjects.convertFolder2Package(FileObjects.stripExtension(ze.getName()))), ze.getCrc());
                        BufferedInputStream in = new BufferedInputStream(this.zipFile.getInputStream(ze));
                        try {
                            BinaryAnalyser.this.analyse(in);
                        }
                        catch (RuntimeException | InvalidClassFormatException icf) {
                            LOGGER.log(Level.WARNING, "Invalid class file format: {0}!/{1}", new Object[]{Utilities.toURI((File)new File(this.zipFile.getName())), ze.getName()});
                            LOGGER.log(Level.INFO, "Class File Exception Details", (Throwable)icf);
                        }
                        catch (IOException x) {
                            Exceptions.attachMessage((Throwable)x, (String)("While scanning: " + ze.getName()));
                            throw x;
                        }
                        finally {
                            in.close();
                        }
                        if (BinaryAnalyser.this.lmListener.isLowMemory()) {
                            BinaryAnalyser.this.flush();
                        }
                    }
                    if (!this.isCancelled()) continue;
                    boolean in = false;
                    return in;
                }
                boolean ze = true;
                return ze;
            }
            finally {
                this.zipFile.close();
            }
        }
    }

    private static abstract class RootProcessor {
        private static final Comparator<Pair<ElementHandle<TypeElement>, Long>> COMPARATOR = new Comparator<Pair<ElementHandle<TypeElement>, Long>>(){

            @Override
            public int compare(Pair<ElementHandle<TypeElement>, Long> o1, Pair<ElementHandle<TypeElement>, Long> o2) {
                return ((ElementHandle)o1.first()).getBinaryName().compareTo(((ElementHandle)o2.first()).getBinaryName());
            }
        };
        static final RootProcessor UP_TO_DATE = new RootProcessor(){

            @NonNull
            @Override
            protected boolean executeImpl() throws IOException {
                return true;
            }
        };
        private final List<Pair<ElementHandle<TypeElement>, Long>> result = new ArrayList<Pair<ElementHandle<TypeElement>, Long>>();
        private final Context ctx;
        private boolean changed;
        private byte preBuildArgsState;

        RootProcessor(@NonNull Context ctx) {
            assert (ctx != null);
            this.ctx = ctx;
        }

        private RootProcessor() {
            this.ctx = null;
        }

        @NonNull
        protected final boolean execute() throws IOException {
            boolean res = this.executeImpl();
            if (res) {
                Collections.sort(this.result, COMPARATOR);
            }
            return res;
        }

        protected final boolean hasChanges() {
            return this.changed;
        }

        protected final boolean preBuildArgs() {
            return this.preBuildArgsState == 3;
        }

        @NonNull
        protected final List<Pair<ElementHandle<TypeElement>, Long>> result() {
            return this.result;
        }

        protected final void report(ElementHandle<TypeElement> te, long crc) {
            this.result.add((Pair)Pair.of(te, (Object)crc));
            String binName = te.getBinaryName();
            if (BinaryAnalyser.OBJECT.equals(binName)) {
                this.preBuildArgsState = (byte)(this.preBuildArgsState | 1);
            } else if (JCOMPONENT.equals(binName)) {
                this.preBuildArgsState = (byte)(this.preBuildArgsState | 2);
            }
        }

        protected final void markChanged() {
            this.changed = true;
        }

        protected final boolean isCancelled() {
            return this.ctx.isCancelled();
        }

        protected final boolean accepts(String name) {
            int index = name.lastIndexOf(46);
            if (index == -1 || index + 1 == name.length()) {
                return false;
            }
            return "class".equalsIgnoreCase(name.substring(index + 1));
        }

        @NonNull
        protected abstract boolean executeImpl() throws IOException;

    }

    private static final class FullIndexProcessor
    extends ClassSignatureProcessor {
        FullIndexProcessor(@NonNull ClassFile classFile) {
            super(classFile);
        }

        @Override
        void visit(@NonNull ClassFile cf) {
            ClassName name;
            this.handleAnnotations(cf.getAnnotations(), false);
            ConstantPool constantPool = cf.getConstantPool();
            for (CPFieldInfo field : constantPool.getAllConstants(CPFieldInfo.class)) {
                name = ClassFileUtil.getType(constantPool.getClass(field.getClassID()));
                if (name == null) continue;
                this.addUsage(name, ClassIndexImpl.UsageType.FIELD_REFERENCE);
            }
            for (CPMethodInfo method2 : constantPool.getAllConstants(CPMethodInfo.class)) {
                name = ClassFileUtil.getType(constantPool.getClass(method2.getClassID()));
                if (name == null) continue;
                this.addUsage(name, ClassIndexImpl.UsageType.METHOD_REFERENCE);
            }
            for (CPMethodInfo method2 : constantPool.getAllConstants(CPInterfaceMethodInfo.class)) {
                name = ClassFileUtil.getType(constantPool.getClass(method2.getClassID()));
                if (name == null) continue;
                this.addUsage(name, ClassIndexImpl.UsageType.METHOD_REFERENCE);
            }
            super.visit(cf);
            for (CPClassInfo ci : constantPool.getAllConstants(CPClassInfo.class)) {
                ClassName ciName = ClassFileUtil.getType(ci);
                if (ciName == null || this.hasUsage(ciName)) continue;
                this.addUsage(ciName, ClassIndexImpl.UsageType.TYPE_REFERENCE);
            }
        }

        @Override
        void visit(@NonNull Method m) {
            Code code;
            CPClassInfo[] classInfos;
            this.handleAnnotations(m.getAnnotations(), false);
            String jvmTypeId = m.getReturnType();
            ClassName type = ClassFileUtil.getType(jvmTypeId);
            if (type != null) {
                this.addUsage(type, ClassIndexImpl.UsageType.TYPE_REFERENCE);
            }
            List params = m.getParameters();
            for (Parameter param : params) {
                jvmTypeId = param.getDescriptor();
                type = ClassFileUtil.getType(jvmTypeId);
                if (type == null) continue;
                this.addUsage(type, ClassIndexImpl.UsageType.TYPE_REFERENCE);
            }
            for (CPClassInfo classInfo : classInfos = m.getExceptionClasses()) {
                type = classInfo.getClassName();
                if (type == null) continue;
                this.addUsage(type, ClassIndexImpl.UsageType.TYPE_REFERENCE);
            }
            jvmTypeId = m.getTypeSignature();
            if (jvmTypeId != null) {
                try {
                    ClassName[] typeSigNames;
                    for (ClassName typeSigName : typeSigNames = ClassFileUtil.getTypesFromMethodTypeSignature(jvmTypeId)) {
                        this.addUsage(typeSigName, ClassIndexImpl.UsageType.TYPE_REFERENCE);
                    }
                }
                catch (IllegalStateException is) {
                    LOGGER.log(Level.WARNING, "Invalid method signature: {0}::{1} signature is:{2}", new Object[]{this.getClassName(), m.getName(), jvmTypeId});
                }
            }
            if ((code = m.getCode()) != null) {
                LocalVariableTypeTableEntry[] varTypes;
                LocalVariableTableEntry[] vars;
                for (LocalVariableTableEntry var : vars = code.getLocalVariableTable()) {
                    type = ClassFileUtil.getType(var.getDescription());
                    if (type == null) continue;
                    this.addUsage(type, ClassIndexImpl.UsageType.TYPE_REFERENCE);
                }
                for (LocalVariableTypeTableEntry varType : varTypes = m.getCode().getLocalVariableTypeTable()) {
                    try {
                        ClassName[] typeSigNames;
                        for (ClassName typeSigName : typeSigNames = ClassFileUtil.getTypesFromFiledTypeSignature(varType.getSignature())) {
                            this.addUsage(typeSigName, ClassIndexImpl.UsageType.TYPE_REFERENCE);
                        }
                        continue;
                    }
                    catch (IllegalStateException is) {
                        LOGGER.log(Level.WARNING, "Invalid local variable signature: {0}::{1}", new Object[]{this.getClassName(), m.getName()});
                    }
                }
            }
            super.visit(m);
        }

        @Override
        void visit(Variable v) {
            this.handleAnnotations(v.getAnnotations(), false);
            String jvmTypeId = v.getDescriptor();
            ClassName type = ClassFileUtil.getType(jvmTypeId);
            if (type != null) {
                this.addUsage(type, ClassIndexImpl.UsageType.TYPE_REFERENCE);
            }
            if ((jvmTypeId = v.getTypeSignature()) != null) {
                try {
                    ClassName[] typeSigNames;
                    for (ClassName typeSigName : typeSigNames = ClassFileUtil.getTypesFromFiledTypeSignature(jvmTypeId)) {
                        this.addUsage(typeSigName, ClassIndexImpl.UsageType.TYPE_REFERENCE);
                    }
                }
                catch (IllegalStateException is) {
                    LOGGER.log(Level.WARNING, "Invalid field signature: {0}::{1} signature is: {2}", new Object[]{this.getClassName(), v.getName(), jvmTypeId});
                }
            }
            super.visit(v);
        }
    }

    private static class ClassSignatureProcessor
    extends ClassFileProcessor {
        ClassSignatureProcessor(@NonNull ClassFile classFile) {
            super(classFile);
        }

        @Override
        void visit(@NonNull ClassFile cf) {
            ClassName scName;
            String signature = cf.getTypeSignature();
            if (signature != null) {
                try {
                    for (ClassName typeSigName : ClassFileUtil.getTypesFromClassTypeSignature(signature)) {
                        this.addUsage(typeSigName, ClassIndexImpl.UsageType.TYPE_REFERENCE);
                    }
                }
                catch (RuntimeException re) {
                    StackTraceElement[] elements;
                    StringBuilder message = new StringBuilder("BinaryAnalyser: Cannot read type: " + signature + " cause: " + re.getLocalizedMessage() + '\n');
                    for (StackTraceElement e : elements = re.getStackTrace()) {
                        message.append(e.toString());
                        message.append('\n');
                    }
                    LOGGER.warning(message.toString());
                }
            }
            if ((scName = cf.getSuperClass()) != null) {
                this.addUsage(scName, ClassIndexImpl.UsageType.SUPER_CLASS);
            }
            Collection interfaces = cf.getInterfaces();
            for (ClassName ifaceName : interfaces) {
                this.addUsage(ifaceName, ClassIndexImpl.UsageType.SUPER_INTERFACE);
            }
            this.handleAnnotations(cf.getAnnotations(), true);
            super.visit(cf);
        }

        @Override
        void visit(@NonNull Method m) {
            String name = m.getName();
            if (!(m.isSynthetic() || ClassSignatureProcessor.isInit(name) || ClassSignatureProcessor.isAccessorMethod(name))) {
                this.addIdent(name);
            }
            super.visit(m);
        }

        @Override
        void visit(@NonNull Variable v) {
            String name = v.getName();
            if (!(v.isSynthetic() || ClassSignatureProcessor.isOutherThis(name) || ClassSignatureProcessor.isDisableAssertions(name))) {
                this.addIdent(name);
            }
            super.visit(v);
        }

        private static boolean isInit(@NonNull String name) {
            return "<init>".equals(name) || "<clinit>".equals(name);
        }

        private static boolean isOutherThis(@NonNull String name) {
            return name.startsWith("this$");
        }

        private static boolean isAccessorMethod(@NonNull String name) {
            return name.startsWith("access$");
        }

        private static boolean isDisableAssertions(@NonNull String name) {
            return "$assertionsDisabled".equals(name);
        }
    }

    private static class ClassFileProcessor {
        private static final Convertor<ClassName, String> CONVERTOR = new Convertor<ClassName, String>(){

            public String convert(ClassName p) {
                return p.getInternalName().replace('/', '.');
            }
        };
        private final ClassFile classFile;
        private final String className;
        private final UsagesData<ClassName> usages = new UsagesData<ClassName>(CONVERTOR);

        ClassFileProcessor(@NonNull ClassFile classFile) {
            this.classFile = classFile;
            this.className = (String)CONVERTOR.convert((Object)classFile.getName());
        }

        final String getClassName() {
            return this.className;
        }

        final UsagesData analyse() {
            this.visit(this.classFile);
            return this.usages;
        }

        void visit(@NonNull ClassFile cf) {
            for (Method method : cf.getMethods()) {
                this.visit(method);
            }
            for (Variable var : cf.getVariables()) {
                this.visit(var);
            }
        }

        void visit(@NonNull Method m) {
        }

        void visit(@NonNull Variable v) {
        }

        final void addIdent(@NonNull CharSequence ident) {
            assert (ident != null);
            this.usages.addFeatureIdent(ident);
        }

        final void addUsage(@NonNull ClassName name, @NonNull ClassIndexImpl.UsageType usage) {
            if (BinaryAnalyser.OBJECT.equals(name.getExternalName())) {
                return;
            }
            this.usages.addUsage(name, usage);
        }

        final boolean hasUsage(@NonNull ClassName name) {
            return this.usages.hasUsage(name);
        }

        final void handleAnnotations(@NonNull Iterable<? extends Annotation> annotations, boolean onlyTopLevel) {
            for (Annotation a : annotations) {
                this.addUsage(a.getType(), ClassIndexImpl.UsageType.TYPE_REFERENCE);
                if (onlyTopLevel) continue;
                LinkedList<ElementValue> toProcess = new LinkedList<ElementValue>();
                for (AnnotationComponent ac : a.getComponents()) {
                    toProcess.add(ac.getValue());
                }
                while (!toProcess.isEmpty()) {
                    ClassName className;
                    String type;
                    ElementValue ev = (ElementValue)toProcess.remove(0);
                    if (ev instanceof ArrayElementValue) {
                        toProcess.addAll(Arrays.asList(((ArrayElementValue)ev).getValues()));
                    }
                    if (ev instanceof NestedElementValue) {
                        Annotation nested = ((NestedElementValue)ev).getNestedValue();
                        this.addUsage(nested.getType(), ClassIndexImpl.UsageType.TYPE_REFERENCE);
                        for (AnnotationComponent ac2 : nested.getComponents()) {
                            toProcess.add(ac2.getValue());
                        }
                    }
                    if (ev instanceof ClassElementValue) {
                        this.addUsage(((ClassElementValue)ev).getClassName(), ClassIndexImpl.UsageType.TYPE_REFERENCE);
                    }
                    if (!(ev instanceof EnumElementValue) || (className = ClassFileUtil.getType(type = ((EnumElementValue)ev).getEnumType())) == null) continue;
                    this.addUsage(className, ClassIndexImpl.UsageType.TYPE_REFERENCE);
                }
            }
        }

    }

    public static final class Changes {
        private static final Changes UP_TO_DATE = new Changes(true, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), false);
        private static final Changes FAILURE = new Changes(false, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), false);
        public final List<ElementHandle<TypeElement>> added;
        public final List<ElementHandle<TypeElement>> removed;
        public final List<ElementHandle<TypeElement>> changed;
        public final boolean preBuildArgs;
        public final boolean done;

        private Changes(boolean done, List<ElementHandle<TypeElement>> added, List<ElementHandle<TypeElement>> removed, List<ElementHandle<TypeElement>> changed, boolean preBuildArgs) {
            this.done = done;
            this.added = added;
            this.removed = removed;
            this.changed = changed;
            this.preBuildArgs = preBuildArgs;
        }
    }

}

