/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.source.usages.fcs;

import java.io.File;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import org.netbeans.modules.java.source.usages.fcs.FileChangeSupportEvent;
import org.netbeans.modules.java.source.usages.fcs.FileChangeSupportListener;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileUtil;
import org.openide.util.Utilities;

public final class FileChangeSupport {
    public static final FileChangeSupport DEFAULT = new FileChangeSupport();
    private final Map<FileChangeSupportListener, Map<File, Holder>> holders = new WeakHashMap<FileChangeSupportListener, Map<File, Holder>>();

    private FileChangeSupport() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addListener(FileChangeSupportListener listener, File path) {
        assert (path.equals(FileUtil.normalizeFile((File)path)));
        Map<FileChangeSupportListener, Map<File, Holder>> map = this.holders;
        synchronized (map) {
            Map<File, Holder> f2H = this.holders.get(listener);
            if (f2H == null) {
                f2H = new HashMap<File, Holder>();
                this.holders.put(listener, f2H);
            }
            if (f2H.containsKey(path)) {
                throw new IllegalArgumentException("Already listening to " + path);
            }
            f2H.put(path, new Holder(listener, path));
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeListener(FileChangeSupportListener listener, File path) {
        assert (path.equals(FileUtil.normalizeFile((File)path)));
        Map<FileChangeSupportListener, Map<File, Holder>> map = this.holders;
        synchronized (map) {
            Map<File, Holder> f2H = this.holders.get(listener);
            if (f2H == null) {
                throw new IllegalArgumentException("Was not listening to " + path);
            }
            if (!f2H.containsKey(path)) {
                throw new IllegalArgumentException(listener + " was not listening to " + path + "; only to " + f2H.keySet());
            }
            f2H.remove(path);
        }
    }

    public void purge() {
        for (FileChangeSupportListener o : this.holders.keySet()) {
        }
    }

    private static final class Holder
    extends WeakReference<FileChangeSupportListener>
    implements FileChangeListener,
    Runnable {
        private final File path;
        private FileObject current;
        private File currentF;

        public Holder(FileChangeSupportListener listener, File path) {
            super(listener, Utilities.activeReferenceQueue());
            assert (path != null);
            this.path = path;
            this.locateCurrent();
        }

        private void locateCurrent() {
            FileObject oldCurrent;
            block6 : {
                oldCurrent = this.current;
                this.currentF = this.path;
                do {
                    try {
                        this.current = FileUtil.toFileObject((File)this.currentF);
                    }
                    catch (IllegalArgumentException x) {
                        this.currentF = FileUtil.normalizeFile((File)this.currentF);
                        this.current = FileUtil.toFileObject((File)this.currentF);
                    }
                    if (this.current != null) break block6;
                    this.currentF = this.currentF.getParentFile();
                } while (this.currentF != null);
                return;
            }
            assert (this.current != null);
            if (this.current != oldCurrent) {
                if (oldCurrent != null) {
                    oldCurrent.removeFileChangeListener((FileChangeListener)this);
                }
                this.current.addFileChangeListener((FileChangeListener)this);
                this.current.getChildren();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void someChange(FileObject modified) {
            FileObject nueCurrent;
            FileChangeSupportListener listener;
            File nueCurrentF;
            File oldCurrentF;
            Holder holder = this;
            synchronized (holder) {
                if (this.current == null) {
                    return;
                }
                listener = (FileChangeSupportListener)this.get();
                if (listener == null) {
                    return;
                }
                FileObject oldCurrent = this.current;
                oldCurrentF = this.currentF;
                this.locateCurrent();
                nueCurrent = this.current;
                nueCurrentF = this.currentF;
            }
            if (modified != null && modified == nueCurrent) {
                FileChangeSupportEvent event = new FileChangeSupportEvent(FileChangeSupport.DEFAULT, 2, this.path);
                listener.fileModified(event);
            } else {
                boolean oldWasCorrect = this.path.equals(oldCurrentF);
                boolean nueIsCorrect = this.path.equals(nueCurrentF);
                if (oldWasCorrect && !nueIsCorrect) {
                    FileChangeSupportEvent event = new FileChangeSupportEvent(FileChangeSupport.DEFAULT, 1, this.path);
                    listener.fileDeleted(event);
                } else if (nueIsCorrect && !oldWasCorrect) {
                    FileChangeSupportEvent event = new FileChangeSupportEvent(FileChangeSupport.DEFAULT, 0, this.path);
                    listener.fileCreated(event);
                }
            }
        }

        public void fileChanged(FileEvent fe) {
            this.someChange(fe.getFile());
        }

        public void fileDeleted(FileEvent fe) {
            this.someChange(null);
        }

        public void fileDataCreated(FileEvent fe) {
            this.someChange(null);
        }

        public void fileFolderCreated(FileEvent fe) {
            this.someChange(null);
        }

        public void fileRenamed(FileRenameEvent fe) {
            this.someChange(null);
        }

        public void fileAttributeChanged(FileAttributeEvent fe) {
        }

        @Override
        public synchronized void run() {
            if (this.current != null) {
                this.current.removeFileChangeListener((FileChangeListener)this);
                this.current = null;
            }
        }
    }

}

