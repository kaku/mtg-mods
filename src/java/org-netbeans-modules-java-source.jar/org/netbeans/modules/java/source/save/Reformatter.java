/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.AnnotatedTypeTree
 *  com.sun.source.tree.AnnotationTree
 *  com.sun.source.tree.ArrayAccessTree
 *  com.sun.source.tree.ArrayTypeTree
 *  com.sun.source.tree.AssertTree
 *  com.sun.source.tree.AssignmentTree
 *  com.sun.source.tree.BinaryTree
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.BreakTree
 *  com.sun.source.tree.CaseTree
 *  com.sun.source.tree.CatchTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.CompoundAssignmentTree
 *  com.sun.source.tree.ConditionalExpressionTree
 *  com.sun.source.tree.ContinueTree
 *  com.sun.source.tree.DoWhileLoopTree
 *  com.sun.source.tree.EmptyStatementTree
 *  com.sun.source.tree.EnhancedForLoopTree
 *  com.sun.source.tree.ErroneousTree
 *  com.sun.source.tree.ExpressionStatementTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.ForLoopTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.IfTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.InstanceOfTree
 *  com.sun.source.tree.IntersectionTypeTree
 *  com.sun.source.tree.LabeledStatementTree
 *  com.sun.source.tree.LambdaExpressionTree
 *  com.sun.source.tree.LambdaExpressionTree$BodyKind
 *  com.sun.source.tree.LiteralTree
 *  com.sun.source.tree.MemberReferenceTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodInvocationTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.NewArrayTree
 *  com.sun.source.tree.NewClassTree
 *  com.sun.source.tree.ParameterizedTypeTree
 *  com.sun.source.tree.ParenthesizedTree
 *  com.sun.source.tree.PrimitiveTypeTree
 *  com.sun.source.tree.ReturnTree
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.SwitchTree
 *  com.sun.source.tree.SynchronizedTree
 *  com.sun.source.tree.ThrowTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.TryTree
 *  com.sun.source.tree.TypeCastTree
 *  com.sun.source.tree.TypeParameterTree
 *  com.sun.source.tree.UnaryTree
 *  com.sun.source.tree.UnionTypeTree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.tree.WhileLoopTree
 *  com.sun.source.tree.WildcardTree
 *  com.sun.source.util.DocSourcePositions
 *  com.sun.source.util.SimpleTreeVisitor
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreePathScanner
 *  com.sun.source.util.Trees
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.api.JavacTrees
 *  com.sun.tools.javac.main.JavaCompiler
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCBlock
 *  com.sun.tools.javac.tree.JCTree$JCMethodDecl
 *  com.sun.tools.javac.tree.JCTree$JCModifiers
 *  com.sun.tools.javac.tree.JCTree$JCStatement
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.List
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.lexer.JavadocTokenId
 *  org.netbeans.api.java.platform.JavaPlatform
 *  org.netbeans.api.java.platform.JavaPlatformManager
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.GuardedDocument
 *  org.netbeans.modules.editor.indent.spi.Context
 *  org.netbeans.modules.editor.indent.spi.Context$Region
 *  org.netbeans.modules.editor.indent.spi.ExtraLock
 *  org.netbeans.modules.editor.indent.spi.ReformatTask
 *  org.netbeans.modules.editor.indent.spi.ReformatTask$Factory
 *  org.netbeans.modules.parsing.api.Embedding
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.impl.Utilities
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.openide.util.Pair
 */
package org.netbeans.modules.java.source.save;

import com.sun.source.tree.AnnotatedTypeTree;
import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.ArrayAccessTree;
import com.sun.source.tree.ArrayTypeTree;
import com.sun.source.tree.AssertTree;
import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BinaryTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.BreakTree;
import com.sun.source.tree.CaseTree;
import com.sun.source.tree.CatchTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.CompoundAssignmentTree;
import com.sun.source.tree.ConditionalExpressionTree;
import com.sun.source.tree.ContinueTree;
import com.sun.source.tree.DoWhileLoopTree;
import com.sun.source.tree.EmptyStatementTree;
import com.sun.source.tree.EnhancedForLoopTree;
import com.sun.source.tree.ErroneousTree;
import com.sun.source.tree.ExpressionStatementTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.ForLoopTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.InstanceOfTree;
import com.sun.source.tree.IntersectionTypeTree;
import com.sun.source.tree.LabeledStatementTree;
import com.sun.source.tree.LambdaExpressionTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MemberReferenceTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.NewArrayTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.ParameterizedTypeTree;
import com.sun.source.tree.ParenthesizedTree;
import com.sun.source.tree.PrimitiveTypeTree;
import com.sun.source.tree.ReturnTree;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.SwitchTree;
import com.sun.source.tree.SynchronizedTree;
import com.sun.source.tree.ThrowTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TryTree;
import com.sun.source.tree.TypeCastTree;
import com.sun.source.tree.TypeParameterTree;
import com.sun.source.tree.UnaryTree;
import com.sun.source.tree.UnionTypeTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.tree.WhileLoopTree;
import com.sun.source.tree.WildcardTree;
import com.sun.source.util.DocSourcePositions;
import com.sun.source.util.SimpleTreeVisitor;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreePathScanner;
import com.sun.source.util.Trees;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.api.JavacTrees;
import com.sun.tools.javac.main.JavaCompiler;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import javax.lang.model.element.Name;
import javax.lang.model.type.TypeKind;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.tools.JavaFileObject;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.lexer.JavadocTokenId;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.api.java.platform.JavaPlatformManager;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CodeStyle;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.TreeUtilities;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.GuardedDocument;
import org.netbeans.modules.editor.indent.spi.Context;
import org.netbeans.modules.editor.indent.spi.ExtraLock;
import org.netbeans.modules.editor.indent.spi.ReformatTask;
import org.netbeans.modules.java.source.JavaSourceAccessor;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.JavacParser;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.impl.Utilities;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.openide.util.Pair;

public class Reformatter
implements ReformatTask {
    private static final Object CT_HANDLER_DOC_PROPERTY = "code-template-insert-handler";
    private Source source;
    private org.netbeans.modules.editor.indent.spi.Context context;
    private CompilationController controller;
    private Embedding currentEmbedding;
    private Document doc;

    public Reformatter(Source source, org.netbeans.modules.editor.indent.spi.Context context) {
        this.source = source;
        this.context = context;
        this.doc = context.document();
    }

    public void reformat() throws BadLocationException {
        CodeStyle cs = (CodeStyle)this.doc.getProperty(CodeStyle.class);
        if (cs == null) {
            cs = CodeStyle.getDefault(this.doc);
        }
        java.util.List indentRegions = this.context.indentRegions();
        Collections.reverse(indentRegions);
        for (Context.Region region : indentRegions) {
            if (!this.initRegionData(region)) continue;
            this.reformatImpl(region, cs);
        }
    }

    public static String reformat(String text, CodeStyle style) {
        return Reformatter.reformat(text, style, style.getRightMargin());
    }

    public static String reformat(String text, CodeStyle style, int rightMargin) {
        StringBuilder sb = new StringBuilder(text);
        try {
            ClassPath empty = ClassPathSupport.createClassPath((URL[])new URL[0]);
            ClasspathInfo cpInfo = ClasspathInfo.create(JavaPlatformManager.getDefault().getDefaultPlatform().getBootstrapLibraries(), empty, empty);
            JavacTaskImpl javacTask = JavacParser.createJavacTask(cpInfo, null, null, null, null, null, null, null);
            Context ctx = javacTask.getContext();
            JavaCompiler.instance((Context)ctx).genEndPos = true;
            CompilationUnitTree tree = (CompilationUnitTree)javacTask.parse(new JavaFileObject[]{FileObjects.memoryFileObject("", "", text)}).iterator().next();
            DocSourcePositions sp = JavacTrees.instance((Context)ctx).getSourcePositions();
            TokenSequence tokens = TokenHierarchy.create((CharSequence)text, (Language)JavaTokenId.language()).tokenSequence(JavaTokenId.language());
            for (Diff diff : Pretty.reformat(text, tokens, new TreePath(tree), (SourcePositions)sp, style, rightMargin)) {
                int start = diff.getStartOffset();
                int end = diff.getEndOffset();
                sb.delete(start, end);
                String t = diff.getText();
                if (t == null || t.length() <= 0) continue;
                sb.insert(start, t);
            }
        }
        catch (IOException ioe) {
            // empty catch block
        }
        return sb.toString();
    }

    private boolean initRegionData(final Context.Region region) {
        if (this.controller == null || this.currentEmbedding != null && (!this.currentEmbedding.containsOriginalOffset(region.getStartOffset()) || !this.currentEmbedding.containsOriginalOffset(region.getEndOffset()))) {
            try {
                if ("text/x-java".equals(this.context.mimePath())) {
                    this.controller = JavaSourceAccessor.getINSTANCE().createCompilationController(this.source);
                } else {
                    ParserManager.parse(Collections.singletonList(this.source), (UserTask)new UserTask(){

                        public void run(ResultIterator resultIterator) throws Exception {
                            Parser.Result result = this.findEmbeddedJava(resultIterator);
                            if (result != null) {
                                Reformatter.this.controller = CompilationController.get(result);
                            }
                        }

                        private Parser.Result findEmbeddedJava(ResultIterator theMess) throws ParseException {
                            LinkedList<Embedding> todo = new LinkedList<Embedding>();
                            for (Embedding embedding2 : theMess.getEmbeddings()) {
                                if ("text/x-java".equals(embedding2.getMimeType()) && embedding2.containsOriginalOffset(region.getStartOffset()) && embedding2.containsOriginalOffset(region.getEndOffset())) {
                                    return theMess.getResultIterator(Reformatter.this.currentEmbedding = embedding2).getParserResult();
                                }
                                todo.add(embedding2);
                            }
                            for (Embedding embedding2 : todo) {
                                Parser.Result result = this.findEmbeddedJava(theMess.getResultIterator(embedding2));
                                if (result == null) continue;
                                return result;
                            }
                            return null;
                        }
                    });
                }
                if (this.controller == null) {
                    return false;
                }
                this.controller.toPhase(JavaSource.Phase.PARSED);
            }
            catch (Exception ex) {
                return false;
            }
        }
        return true;
    }

    private void reformatImpl(Context.Region region, CodeStyle cs) throws BadLocationException {
        boolean templateEdit = this.doc.getProperty(CT_HANDLER_DOC_PROPERTY) != null;
        int startOffset = region.getStartOffset();
        int endOffset = region.getEndOffset();
        startOffset = this.controller.getSnapshot().getEmbeddedOffset(startOffset);
        if (startOffset < 0) {
            return;
        }
        endOffset = this.controller.getSnapshot().getEmbeddedOffset(endOffset);
        if (endOffset < 0) {
            return;
        }
        int embeddingOffset = -1;
        int firstLineIndent = -1;
        if (!"text/x-java".equals(this.context.mimePath())) {
            firstLineIndent = this.context.lineIndent(this.context.lineStartOffset(region.getStartOffset()));
            TokenSequence ts = this.controller.getTokenHierarchy().tokenSequence(JavaTokenId.language());
            if (ts != null) {
                String t;
                ts.move(startOffset);
                if (ts.moveNext()) {
                    if (ts.token().id() == JavaTokenId.WHITESPACE) {
                        t = ts.token().text().toString();
                        if (ts.offset() < startOffset) {
                            t = t.substring(startOffset - ts.offset());
                        }
                        if (t.indexOf(10) < 0) {
                            embeddingOffset = ts.offset() + ts.token().length();
                        }
                    } else {
                        embeddingOffset = startOffset;
                    }
                }
                ts.move(endOffset);
                if (ts.moveNext() && ts.token().id() == JavaTokenId.WHITESPACE) {
                    int i;
                    t = ts.token().text().toString();
                    if (ts.offset() + t.length() > endOffset) {
                        t = t.substring(0, endOffset - ts.offset());
                    }
                    if ((i = t.lastIndexOf(10)) >= 0) {
                        endOffset -= t.length() - i;
                    }
                }
            }
        }
        if (startOffset >= endOffset) {
            return;
        }
        TreePath path = this.getCommonPath(startOffset, endOffset);
        if (path == null) {
            return;
        }
        for (Diff diff : Pretty.reformat(this.controller, path, cs, startOffset, endOffset, templateEdit, firstLineIndent)) {
            TokenSequence ts;
            String t;
            int idx;
            int idx2;
            int idx1;
            int start = diff.getStartOffset();
            int end = diff.getEndOffset();
            String text = diff.getText();
            if (startOffset > end || endOffset < start || endOffset == start && endOffset < this.doc.getLength() && (text == null || !text.trim().equals("}")) || embeddingOffset >= start || this.doc instanceof GuardedDocument && ((GuardedDocument)this.doc).isPosGuarded(start)) continue;
            if (startOffset >= start) {
                if (text != null && text.length() > 0) {
                    ts = this.controller.getTokenHierarchy().tokenSequence(JavaTokenId.language());
                    if (ts == null) continue;
                    if (ts.move(startOffset) == 0) {
                        if (!ts.movePrevious() && !ts.moveNext()) {
                            continue;
                        }
                    } else if (!ts.moveNext() && !ts.movePrevious()) continue;
                    if (ts.token().id() == JavaTokenId.WHITESPACE) {
                        t = ts.token().text().toString();
                        t = t.substring(0, startOffset - ts.offset());
                        if (ts.movePrevious() && ts.token().id() == JavaTokenId.LINE_COMMENT) {
                            t = "\n" + t;
                        }
                        if (templateEdit) {
                            idx = t.lastIndexOf(10);
                            if (idx >= 0) {
                                t = t.substring(idx + 1);
                                idx = text.lastIndexOf(10);
                                if (idx >= 0) {
                                    text = text.substring(idx + 1);
                                }
                                text = text.trim().length() > 0 ? null : (text.length() > t.length() ? text.substring(t.length()) : null);
                            } else {
                                text = null;
                            }
                        } else {
                            idx1 = 0;
                            idx2 = 0;
                            int lastIdx1 = 0;
                            int lastIdx2 = 0;
                            while ((idx1 = t.indexOf(10, lastIdx1)) >= 0 && (idx2 = text.indexOf(10, lastIdx2)) >= 0) {
                                lastIdx1 = idx1 + 1;
                                lastIdx2 = idx2 + 1;
                            }
                            idx2 = text.lastIndexOf(10);
                            if (idx2 >= 0 && idx2 >= lastIdx2) {
                                if (lastIdx1 == 0) {
                                    t = null;
                                } else {
                                    text = text.substring(idx2 + 1);
                                    t = t.substring(lastIdx1);
                                }
                            } else {
                                idx1 = t.lastIndexOf(10);
                                if (idx1 >= 0 && idx1 >= lastIdx1) {
                                    t = t.substring(idx1 + 1);
                                    text = text.substring(lastIdx2);
                                } else {
                                    t = t.substring(lastIdx1);
                                    text = text.substring(lastIdx2);
                                }
                            }
                            if (text != null && t != null) {
                                text = text.length() > t.length() ? text.substring(t.length()) : null;
                            }
                        }
                    } else if (startOffset > 0) continue;
                }
                start = startOffset;
            }
            if (endOffset < end) {
                if (text != null && text.length() > 0 && !templateEdit && (ts = this.controller.getTokenHierarchy().tokenSequence(JavaTokenId.language())) != null) {
                    ts.move(endOffset);
                    t = null;
                    if (ts.moveNext()) {
                        switch ((JavaTokenId)ts.token().id()) {
                            case WHITESPACE: {
                                t = ts.token().text().toString();
                                t = t.substring(endOffset - ts.offset());
                                break;
                            }
                            case JAVADOC_COMMENT: 
                            case BLOCK_COMMENT: {
                                t = ts.token().text().toString();
                                for (idx = endOffset - ts.offset(); idx < t.length() && t.charAt(idx) <= ' '; ++idx) {
                                }
                                t = t.substring(endOffset - ts.offset(), idx);
                            }
                        }
                    }
                    if (t != null) {
                        while ((idx1 = t.lastIndexOf(10)) >= 0 && (idx2 = text.lastIndexOf(10)) >= 0) {
                            t = t.substring(0, idx1);
                            text = text.substring(0, idx2);
                        }
                        text = text.length() > t.length() ? text.substring(0, text.length() - t.length()) : null;
                    }
                }
                end = endOffset;
            }
            start = this.controller.getSnapshot().getOriginalOffset(start);
            end = this.controller.getSnapshot().getOriginalOffset(end);
            if (start == -1 || end == -1 || start < region.getStartOffset() || end > region.getEndOffset()) continue;
            if (end - start > 0) {
                this.doc.remove(start, end - start);
            }
            if (text == null || text.length() <= 0) continue;
            this.doc.insertString(start, text, null);
        }
    }

    public ExtraLock reformatLock() {
        return "text/x-java".equals(this.source.getMimeType()) ? null : new ExtraLock(){

            public void lock() {
                Utilities.acquireParserLock();
            }

            public void unlock() {
                Utilities.releaseParserLock();
            }
        };
    }

    private TreePath getCommonPath(int startOffset, int endOffset) {
        TreeUtilities tu = this.controller.getTreeUtilities();
        TreePath startPath = tu.pathFor(startOffset);
        List reverseStartPath = List.nil();
        for (Tree t : startPath) {
            reverseStartPath = reverseStartPath.prepend((Object)t);
        }
        TreePath endPath = tu.pathFor(endOffset);
        List reverseEndPath = List.nil();
        for (Tree t2 : endPath) {
            reverseEndPath = reverseEndPath.prepend((Object)t2);
        }
        TreePath path = null;
        TreePath statementPath = null;
        while (reverseStartPath.head != null && reverseStartPath.head == reverseEndPath.head) {
            TreePath treePath = path = reverseStartPath.head instanceof CompilationUnitTree ? new TreePath((CompilationUnitTree)reverseStartPath.head) : new TreePath(path, (Tree)reverseStartPath.head);
            if (reverseStartPath.head instanceof StatementTree) {
                statementPath = path;
            }
            reverseStartPath = reverseStartPath.tail;
            reverseEndPath = reverseEndPath.tail;
        }
        return statementPath != null ? statementPath : path;
    }

    private static class Diff {
        private int start;
        private int end;
        private String text;

        private Diff(int start, int end, String text) {
            this.start = start;
            this.end = end;
            this.text = text;
        }

        public int getStartOffset() {
            return this.start;
        }

        public int getEndOffset() {
            return this.end;
        }

        public String getText() {
            return this.text;
        }

        public String toString() {
            return "Diff<" + this.start + "," + this.end + ">:" + this.text;
        }

        static /* synthetic */ int access$412(Diff x0, int x1) {
            return x0.end += x1;
        }
    }

    private static class Pretty
    extends TreePathScanner<Boolean, Void> {
        private static final String OPERATOR = "operator";
        private static final String EMPTY = "";
        private static final String SPACE = " ";
        private static final String NEWLINE = "\n";
        private static final String LEADING_STAR = "*";
        private static final String P_TAG = "<p>";
        private static final String END_P_TAG = "<p/>";
        private static final String CODE_TAG = "<code>";
        private static final String CODE_END_TAG = "</code>";
        private static final String PRE_TAG = "<pre>";
        private static final String PRE_END_TAG = "</pre>";
        private static final String JDOC_CODE_TAG = "@code";
        private static final String JDOC_DOCROOT_TAG = "@docRoot";
        private static final String JDOC_EXCEPTION_TAG = "@exception";
        private static final String JDOC_INHERITDOC_TAG = "@inheritDoc";
        private static final String JDOC_LINK_TAG = "@link";
        private static final String JDOC_LINKPLAIN_TAG = "@linkplain";
        private static final String JDOC_LITERAL_TAG = "@literal";
        private static final String JDOC_PARAM_TAG = "@param";
        private static final String JDOC_RETURN_TAG = "@return";
        private static final String JDOC_THROWS_TAG = "@throws";
        private static final String JDOC_VALUE_TAG = "@value";
        private static final String ERROR = "<error>";
        private final String fText;
        private final SourcePositions sp;
        private final CodeStyle cs;
        private final int rightMargin;
        private final int tabSize;
        private final int indentSize;
        private final int continuationIndentSize;
        private final boolean expandTabToSpaces;
        private TokenSequence<JavaTokenId> tokens;
        private int indent;
        private boolean continuationIndent;
        private int col;
        private int endPos;
        private int maxPreservedBlankLines;
        private int lastBlankLines;
        private int lastBlankLinesTokenIndex;
        private Diff lastBlankLinesDiff;
        private int lastNewLineOffset;
        private boolean afterAnnotation;
        private boolean wrapAnnotation;
        private WrapAbort checkWrap;
        private boolean fieldGroup;
        private boolean templateEdit;
        private LinkedList<Diff> diffs = new LinkedList();
        private DanglingElseChecker danglingElseChecker = new DanglingElseChecker();
        private CompilationUnitTree root;
        private int startOffset;
        private int endOffset;
        private int tpLevel;
        private boolean eof = false;
        private boolean bof = false;
        private boolean insideAnnotation = false;
        private int lastIndent = 0;
        private boolean isLastIndentContinuation = false;

        private Pretty(CompilationInfo info, TreePath path, CodeStyle cs, int startOffset, int endOffset, boolean templateEdit) {
            this(info.getText(), info.getTokenHierarchy().tokenSequence(JavaTokenId.language()), path, info.getTrees().getSourcePositions(), cs, startOffset, endOffset, cs.getRightMargin());
            this.templateEdit = templateEdit;
        }

        private Pretty(String text, TokenSequence<JavaTokenId> tokens, TreePath path, SourcePositions sp, CodeStyle cs, int startOffset, int endOffset, int rightMargin) {
            this.fText = text;
            this.sp = sp;
            this.cs = cs;
            this.rightMargin = rightMargin > 0 ? rightMargin : Integer.MAX_VALUE;
            this.tabSize = cs.getTabSize();
            this.indentSize = cs.getIndentSize();
            this.continuationIndentSize = cs.getContinuationIndentSize();
            this.expandTabToSpaces = cs.expandTabToSpaces();
            this.maxPreservedBlankLines = this.insideBlock(path) ? cs.getMaximumBlankLinesInCode() : cs.getMaximumBlankLinesInDeclarations();
            this.lastBlankLines = -1;
            this.lastBlankLinesTokenIndex = -1;
            this.lastBlankLinesDiff = null;
            this.lastNewLineOffset = -1;
            this.afterAnnotation = false;
            this.wrapAnnotation = false;
            this.fieldGroup = false;
            Tree tree = path.getLeaf();
            this.lastIndent = tokens != null ? this.getIndentLevel(tokens, path) : 0;
            this.col = this.indent = this.lastIndent;
            this.tokens = tokens;
            if (tree.getKind() == Tree.Kind.COMPILATION_UNIT) {
                tokens.moveEnd();
                tokens.movePrevious();
            } else {
                tokens.move((int)sp.getEndPosition(path.getCompilationUnit(), tree));
                if (!tokens.moveNext()) {
                    tokens.movePrevious();
                }
            }
            this.endPos = tokens.offset();
            if (tree.getKind() == Tree.Kind.COMPILATION_UNIT) {
                tokens.moveStart();
                this.bof = true;
            } else {
                tokens.move((int)sp.getStartPosition(path.getCompilationUnit(), tree));
            }
            tokens.moveNext();
            this.root = path.getCompilationUnit();
            this.startOffset = startOffset;
            this.endOffset = endOffset;
            this.tpLevel = 0;
        }

        public static LinkedList<Diff> reformat(CompilationInfo info, TreePath path, CodeStyle cs, int startOffset, int endOffset, boolean templateEdit, int firstLineIndent) {
            Pretty pretty = new Pretty(info, path, cs, startOffset, endOffset, templateEdit);
            if (pretty.indent >= 0) {
                if (firstLineIndent >= 0) {
                    pretty.indent = pretty.lastIndent = firstLineIndent;
                }
                pretty.scan(path, (Object)null);
            }
            if (path.getLeaf().getKind() == Tree.Kind.COMPILATION_UNIT) {
                int cnt;
                CompilationUnitTree cut = (CompilationUnitTree)path.getLeaf();
                java.util.List typeDecls = cut.getTypeDecls();
                int size = typeDecls.size();
                int n = cnt = size > 0 && TreeUtilities.CLASS_TREE_KINDS.contains((Object)((Tree)typeDecls.get(size - 1)).getKind()) ? cs.getBlankLinesAfterClass() : 1;
                if (cnt < 1) {
                    cnt = 1;
                }
                String s = pretty.getNewlines(cnt);
                pretty.tokens.moveEnd();
                pretty.tokens.movePrevious();
                if (pretty.tokens.token().id() != JavaTokenId.WHITESPACE) {
                    if (!pretty.tokens.token().text().toString().endsWith(s)) {
                        String text = info.getText();
                        pretty.diffs.addFirst(new Diff(text.length(), text.length(), s));
                    }
                } else if (!s.contentEquals(pretty.tokens.token().text())) {
                    pretty.diffs.addFirst(new Diff(pretty.tokens.offset(), pretty.tokens.offset() + pretty.tokens.token().length(), s));
                }
            }
            return pretty.diffs;
        }

        public static LinkedList<Diff> reformat(String text, TokenSequence<JavaTokenId> tokens, TreePath path, SourcePositions sp, CodeStyle cs, int rightMargin) {
            int cnt;
            Pretty pretty = new Pretty(text, tokens, path, sp, cs, 0, text.length(), rightMargin);
            pretty.scan(path, (Object)null);
            CompilationUnitTree cut = (CompilationUnitTree)path.getLeaf();
            java.util.List typeDecls = cut.getTypeDecls();
            int size = typeDecls.size();
            int n = cnt = size > 0 && TreeUtilities.CLASS_TREE_KINDS.contains((Object)((Tree)typeDecls.get(size - 1)).getKind()) ? cs.getBlankLinesAfterClass() : 1;
            if (cnt < 1) {
                cnt = 1;
            }
            String s = pretty.getNewlines(cnt);
            tokens.moveEnd();
            tokens.movePrevious();
            if (tokens.token().id() != JavaTokenId.WHITESPACE) {
                pretty.diffs.addFirst(new Diff(text.length(), text.length(), s));
            } else if (!s.contentEquals(tokens.token().text())) {
                pretty.diffs.addFirst(new Diff(tokens.offset(), tokens.offset() + tokens.token().length(), s));
            }
            return pretty.diffs;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean scan(Tree tree, Void p) {
            int lastEndPos = this.endPos;
            if (tree != null && tree.getKind() != Tree.Kind.COMPILATION_UNIT) {
                this.endPos = tree instanceof FakeBlock ? Integer.MAX_VALUE : (int)this.sp.getEndPosition(this.getCurrentPath().getCompilationUnit(), tree);
                if (tree.getKind() != Tree.Kind.ERRONEOUS && tree.getKind() != Tree.Kind.BLOCK && (tree.getKind() != Tree.Kind.CLASS || this.getCurrentPath().getLeaf().getKind() != Tree.Kind.NEW_CLASS) && tree.getKind() != Tree.Kind.NEW_ARRAY) {
                    int startPos = (int)this.sp.getStartPosition(this.getCurrentPath().getCompilationUnit(), tree);
                    if (startPos >= 0 && startPos > this.tokens.offset()) {
                        this.tokens.move(startPos);
                        if (!this.tokens.moveNext()) {
                            this.tokens.movePrevious();
                        }
                    }
                    if (startPos >= this.endPos) {
                        this.endPos = -1;
                    }
                }
            }
            try {
                if (this.endPos < 0) {
                    Boolean startPos = false;
                    return startPos;
                }
                Boolean ret = this.tokens.offset() <= this.endPos ? (Boolean)TreePathScanner.super.scan(tree, (Object)p) : null;
                Boolean bl = ret != null ? ret : true;
                return bl;
            }
            finally {
                this.endPos = lastEndPos;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitCompilationUnit(CompilationUnitTree node, Void p) {
            java.util.List imports;
            ExpressionTree pkg = node.getPackageName();
            if (pkg != null) {
                this.blankLines(this.cs.getBlankLinesBeforePackage());
                if (!node.getPackageAnnotations().isEmpty()) {
                    this.wrapList(this.cs.wrapAnnotations(), false, false, JavaTokenId.COMMA, node.getPackageAnnotations());
                    this.newline();
                }
                this.accept(JavaTokenId.PACKAGE, new JavaTokenId[0]);
                boolean old = this.continuationIndent;
                try {
                    this.continuationIndent = true;
                    this.space();
                    this.scan((Tree)pkg, p);
                    this.accept(JavaTokenId.SEMICOLON, new JavaTokenId[0]);
                }
                finally {
                    this.continuationIndent = old;
                }
                this.blankLines(this.cs.getBlankLinesAfterPackage());
            }
            if ((imports = node.getImports()) != null && !imports.isEmpty()) {
                this.blankLines(this.cs.getBlankLinesBeforeImports());
                for (ImportTree imp : imports) {
                    this.newline();
                    this.scan((Tree)imp, p);
                }
                this.blankLines(this.cs.getBlankLinesAfterImports());
            }
            boolean semiRead = false;
            for (Tree typeDecl : node.getTypeDecls()) {
                Diff d;
                if (semiRead && typeDecl.getKind() == Tree.Kind.EMPTY_STATEMENT) continue;
                if (TreeUtilities.CLASS_TREE_KINDS.contains((Object)typeDecl.getKind())) {
                    this.blankLines(this.cs.getBlankLinesBeforeClass());
                }
                this.scan(typeDecl, p);
                int index = this.tokens.index();
                int c = this.col;
                Diff diff = d = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                if (this.accept(JavaTokenId.SEMICOLON, new JavaTokenId[0]) == JavaTokenId.SEMICOLON) {
                    semiRead = true;
                } else {
                    this.rollback(index, c, d);
                    semiRead = false;
                }
                if (!TreeUtilities.CLASS_TREE_KINDS.contains((Object)typeDecl.getKind())) continue;
                this.blankLines(this.cs.getBlankLinesAfterClass());
            }
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitImport(ImportTree node, Void p) {
            this.accept(JavaTokenId.IMPORT, new JavaTokenId[0]);
            boolean old = this.continuationIndent;
            try {
                this.continuationIndent = true;
                this.space();
                if (node.isStatic()) {
                    this.accept(JavaTokenId.STATIC, new JavaTokenId[0]);
                    this.space();
                }
                this.scan(node.getQualifiedIdentifier(), p);
                this.accept(JavaTokenId.SEMICOLON, new JavaTokenId[0]);
            }
            finally {
                this.continuationIndent = old;
            }
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitClass(ClassTree node, Void p) {
            Diff diff;
            Tree parent = this.getCurrentPath().getParentPath().getLeaf();
            if (!(parent.getKind() == Tree.Kind.NEW_CLASS || parent.getKind() == Tree.Kind.VARIABLE && this.isEnumerator((VariableTree)parent))) {
                boolean old = this.continuationIndent;
                try {
                    java.util.List impls;
                    Tree ext;
                    java.util.List tparams;
                    ModifiersTree mods = node.getModifiers();
                    if (mods != null) {
                        if (this.scan((Tree)mods, p).booleanValue()) {
                            this.continuationIndent = true;
                            if (this.cs.placeNewLineAfterModifiers()) {
                                this.newline();
                            } else {
                                this.space();
                            }
                        } else if (this.afterAnnotation) {
                            this.newline();
                        }
                        this.afterAnnotation = false;
                    }
                    JavaTokenId id = this.accept(JavaTokenId.CLASS, JavaTokenId.INTERFACE, JavaTokenId.ENUM, JavaTokenId.AT);
                    this.continuationIndent = true;
                    if (id == JavaTokenId.AT) {
                        this.accept(JavaTokenId.INTERFACE, new JavaTokenId[0]);
                    }
                    this.space();
                    if (!"<error>".contentEquals(node.getSimpleName())) {
                        this.accept(JavaTokenId.IDENTIFIER, new JavaTokenId[0]);
                    }
                    if ((tparams = node.getTypeParameters()) != null && !tparams.isEmpty()) {
                        JavaTokenId accepted;
                        if (JavaTokenId.LT == this.accept(JavaTokenId.LT, new JavaTokenId[0])) {
                            ++this.tpLevel;
                        }
                        Iterator it = tparams.iterator();
                        while (it.hasNext()) {
                            TypeParameterTree tparam = (TypeParameterTree)it.next();
                            this.scan((Tree)tparam, p);
                            if (!it.hasNext()) continue;
                            this.spaces(this.cs.spaceBeforeComma() ? 1 : 0);
                            this.accept(JavaTokenId.COMMA, new JavaTokenId[0]);
                            this.spaces(this.cs.spaceAfterComma() ? 1 : 0);
                        }
                        if (this.tpLevel > 0 && (accepted = this.accept(JavaTokenId.GT, JavaTokenId.GTGT, JavaTokenId.GTGTGT)) != null) {
                            switch (accepted) {
                                case GTGTGT: {
                                    this.tpLevel -= 3;
                                    break;
                                }
                                case GTGT: {
                                    this.tpLevel -= 2;
                                    break;
                                }
                                case GT: {
                                    --this.tpLevel;
                                }
                            }
                        }
                    }
                    if ((ext = node.getExtendsClause()) != null) {
                        this.wrapToken(this.cs.wrapExtendsImplementsKeyword(), 1, JavaTokenId.EXTENDS, new JavaTokenId[0]);
                        this.spaces(1, true);
                        this.scan(ext, p);
                    }
                    if ((impls = node.getImplementsClause()) != null && !impls.isEmpty()) {
                        this.wrapToken(this.cs.wrapExtendsImplementsKeyword(), 1, id == JavaTokenId.INTERFACE ? JavaTokenId.EXTENDS : JavaTokenId.IMPLEMENTS, new JavaTokenId[0]);
                        this.wrapList(this.cs.wrapExtendsImplementsList(), this.cs.alignMultilineImplements(), true, JavaTokenId.COMMA, impls);
                    }
                }
                finally {
                    this.continuationIndent = old;
                }
            }
            CodeStyle.BracePlacement bracePlacement = this.cs.getClassDeclBracePlacement();
            boolean spaceBeforeLeftBrace = this.cs.spaceBeforeClassDeclLeftBrace();
            int old = this.lastIndent;
            int halfIndent = this.lastIndent;
            switch (bracePlacement) {
                case SAME_LINE: {
                    this.spaces(spaceBeforeLeftBrace ? 1 : 0, this.tokens.offset() < this.startOffset);
                    this.accept(JavaTokenId.LBRACE, new JavaTokenId[0]);
                    this.indent = this.lastIndent + this.indentSize;
                    break;
                }
                case NEW_LINE: {
                    this.newline();
                    this.accept(JavaTokenId.LBRACE, new JavaTokenId[0]);
                    this.indent = this.lastIndent + this.indentSize;
                    break;
                }
                case NEW_LINE_HALF_INDENTED: {
                    int oldLast = this.lastIndent;
                    halfIndent = this.indent = this.lastIndent + (this.indentSize >> 1);
                    this.newline();
                    this.accept(JavaTokenId.LBRACE, new JavaTokenId[0]);
                    this.indent = oldLast + this.indentSize;
                    break;
                }
                case NEW_LINE_INDENTED: {
                    halfIndent = this.indent = this.lastIndent + this.indentSize;
                    this.newline();
                    this.accept(JavaTokenId.LBRACE, new JavaTokenId[0]);
                }
            }
            boolean emptyClass = true;
            for (Tree member : node.getMembers()) {
                if (this.isSynthetic(this.getCurrentPath().getCompilationUnit(), member)) continue;
                emptyClass = false;
                break;
            }
            if (emptyClass) {
                this.newline();
            } else {
                if (!this.cs.indentTopLevelClassMembers()) {
                    this.indent = old;
                }
                this.blankLines(node.getSimpleName().length() == 0 ? this.cs.getBlankLinesAfterAnonymousClassHeader() : this.cs.getBlankLinesAfterClassHeader());
                JavaTokenId id = null;
                boolean first = true;
                boolean semiRead = false;
                block22 : for (Tree member2 : node.getMembers()) {
                    if (this.isSynthetic(this.getCurrentPath().getCompilationUnit(), member2)) continue;
                    switch (member2.getKind()) {
                        int c;
                        Diff d;
                        int index;
                        case VARIABLE: {
                            if (this.isEnumerator((VariableTree)member2)) {
                                this.wrapTree(this.cs.wrapEnumConstants(), -1, id == JavaTokenId.COMMA ? 1 : 0, member2);
                                index = this.tokens.index();
                                c = this.col;
                                d = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                                id = this.accept(JavaTokenId.COMMA, JavaTokenId.SEMICOLON);
                                if (id == JavaTokenId.COMMA) {
                                    index = this.tokens.index();
                                    c = this.col;
                                    Diff diff2 = d = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                                    if (this.accept(JavaTokenId.SEMICOLON, new JavaTokenId[0]) != null) break;
                                    this.rollback(index, c, d);
                                    break;
                                }
                                if (id == JavaTokenId.SEMICOLON) {
                                    this.blankLines(this.cs.getBlankLinesAfterFields());
                                    break;
                                }
                                this.rollback(index, c, d);
                                this.blankLines(this.cs.getBlankLinesAfterFields());
                                break;
                            }
                            boolean b = this.tokens.moveNext();
                            if (!b) break;
                            this.tokens.movePrevious();
                            if (!this.fieldGroup && !first) {
                                this.blankLines(this.cs.getBlankLinesBeforeFields());
                            }
                            this.scan(member2, p);
                            if (this.fieldGroup) break;
                            this.blankLines(this.cs.getBlankLinesAfterFields());
                            break;
                        }
                        case METHOD: {
                            if (!first) {
                                this.blankLines(this.cs.getBlankLinesBeforeMethods());
                            }
                            this.scan(member2, p);
                            this.blankLines(this.cs.getBlankLinesAfterMethods());
                            break;
                        }
                        case BLOCK: {
                            if (semiRead && !((BlockTree)member2).isStatic() && ((BlockTree)member2).getStatements().isEmpty()) {
                                semiRead = false;
                                continue block22;
                            }
                            if (!first) {
                                this.blankLines(this.cs.getBlankLinesBeforeMethods());
                                index = this.tokens.index();
                                c = this.col;
                                Diff diff3 = d = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                                if (this.accept(JavaTokenId.SEMICOLON, new JavaTokenId[0]) == JavaTokenId.SEMICOLON) continue block22;
                                this.rollback(index, c, d);
                            }
                            this.scan(member2, p);
                            this.blankLines(this.cs.getBlankLinesAfterMethods());
                            break;
                        }
                        case ANNOTATION_TYPE: 
                        case CLASS: 
                        case ENUM: 
                        case INTERFACE: {
                            if (!first) {
                                this.blankLines(this.cs.getBlankLinesBeforeClass());
                            }
                            this.scan(member2, p);
                            index = this.tokens.index();
                            c = this.col;
                            Diff diff4 = d = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                            if (this.accept(JavaTokenId.SEMICOLON, new JavaTokenId[0]) == JavaTokenId.SEMICOLON) {
                                semiRead = true;
                            } else {
                                this.rollback(index, c, d);
                                semiRead = false;
                            }
                            this.blankLines(this.cs.getBlankLinesAfterClass());
                        }
                    }
                    first = false;
                }
                if (this.lastBlankLinesTokenIndex < 0) {
                    this.newline();
                }
                this.blankLines(node.getSimpleName().length() == 0 ? this.cs.getBlankLinesBeforeAnonymousClassClosingBrace() : this.cs.getBlankLinesBeforeClassClosingBrace());
            }
            this.indent = halfIndent;
            Diff diff5 = diff = this.diffs.isEmpty() ? null : this.diffs.getFirst();
            if (diff != null && diff.end == this.tokens.offset()) {
                String spaces;
                if (diff.text != null) {
                    int idx = diff.text.lastIndexOf(10);
                    if (idx < 0) {
                        diff.text = this.getIndent();
                    } else {
                        diff.text = diff.text.substring(0, idx + 1) + this.getIndent();
                    }
                }
                String string = spaces = diff.text != null ? diff.text : this.getIndent();
                if (spaces.equals(this.fText.substring(diff.start, diff.end))) {
                    this.diffs.removeFirst();
                }
            } else if (this.tokens.movePrevious()) {
                if (this.tokens.token().id() == JavaTokenId.WHITESPACE) {
                    String ind;
                    String text = this.tokens.token().text().toString();
                    int idx = text.lastIndexOf(10);
                    if (idx >= 0) {
                        text = text.substring(idx + 1);
                        ind = this.getIndent();
                        if (!ind.equals(text)) {
                            this.addDiff(new Diff(this.tokens.offset() + idx + 1, this.tokens.offset() + this.tokens.token().length(), ind));
                        }
                    } else if (this.tokens.movePrevious()) {
                        if (this.tokens.token().id() == JavaTokenId.LINE_COMMENT) {
                            this.tokens.moveNext();
                            ind = this.getIndent();
                            if (!ind.equals(text)) {
                                this.addDiff(new Diff(this.tokens.offset(), this.tokens.offset() + this.tokens.token().length(), ind));
                            }
                        } else {
                            this.tokens.moveNext();
                        }
                    }
                }
                this.tokens.moveNext();
            }
            this.col = this.indent();
            this.accept(JavaTokenId.RBRACE, new JavaTokenId[0]);
            this.indent = this.lastIndent = old;
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitVariable(VariableTree node, Void p) {
            block42 : {
                boolean old = this.continuationIndent;
                try {
                    ExpressionTree init;
                    Tree parent = this.getCurrentPath().getParentPath().getLeaf();
                    boolean insideForOrCatch = EnumSet.of(Tree.Kind.FOR_LOOP, Tree.Kind.CATCH).contains((Object)parent.getKind());
                    ModifiersTree mods = node.getModifiers();
                    if (mods != null && !this.fieldGroup && this.sp.getStartPosition(this.root, (Tree)mods) < this.sp.getEndPosition(this.root, (Tree)mods)) {
                        if (this.scan((Tree)mods, p).booleanValue()) {
                            if (!insideForOrCatch) {
                                this.continuationIndent = true;
                                if (this.cs.placeNewLineAfterModifiers()) {
                                    this.newline();
                                } else {
                                    this.space();
                                }
                            } else {
                                this.space();
                            }
                        } else if (this.afterAnnotation) {
                            if (TreeUtilities.CLASS_TREE_KINDS.contains((Object)parent.getKind()) || parent.getKind() == Tree.Kind.BLOCK) {
                                switch (this.cs.wrapAnnotations()) {
                                    case WRAP_ALWAYS: {
                                        this.newline();
                                        break;
                                    }
                                    case WRAP_IF_LONG: {
                                        if (this.col >= this.rightMargin) {
                                            this.newline();
                                            break;
                                        }
                                        this.spaces(1, true);
                                        break;
                                    }
                                    case WRAP_NEVER: {
                                        this.spaces(1, true);
                                    }
                                }
                            } else {
                                this.space();
                            }
                        }
                        this.afterAnnotation = false;
                    }
                    if (this.isEnumerator(node)) {
                        this.continuationIndent = true;
                        this.accept(JavaTokenId.IDENTIFIER, new JavaTokenId[0]);
                        ExpressionTree init2 = node.getInitializer();
                        if (init2 != null && init2.getKind() == Tree.Kind.NEW_CLASS) {
                            java.util.List args;
                            NewClassTree nct = (NewClassTree)init2;
                            int index = this.tokens.index();
                            int c = this.col;
                            Diff d = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                            this.spaces(this.cs.spaceBeforeMethodCallParen() ? 1 : 0);
                            JavaTokenId id = this.accept(JavaTokenId.LPAREN, new JavaTokenId[0]);
                            if (id != JavaTokenId.LPAREN) {
                                this.rollback(index, c, d);
                            }
                            if ((args = nct.getArguments()) != null && !args.isEmpty()) {
                                int oldIndent = this.indent;
                                boolean continuation = this.isLastIndentContinuation;
                                if (continuation) {
                                    this.indent = this.indent();
                                }
                                try {
                                    this.spaces(this.cs.spaceWithinMethodCallParens() ? 1 : 0, true);
                                    this.wrapList(this.cs.wrapMethodCallArgs(), this.cs.alignMultilineCallArgs(), false, JavaTokenId.COMMA, args);
                                }
                                finally {
                                    this.indent = oldIndent;
                                    this.continuationIndent = continuation;
                                }
                                this.spaces(this.cs.spaceWithinMethodCallParens() ? 1 : 0, true);
                            }
                            if (id == JavaTokenId.LPAREN) {
                                this.accept(JavaTokenId.RPAREN, new JavaTokenId[0]);
                            }
                            this.continuationIndent = true;
                            ClassTree body = nct.getClassBody();
                            if (body != null) {
                                this.scan((Tree)body, p);
                            }
                        }
                        break block42;
                    }
                    if (!insideForOrCatch) {
                        this.continuationIndent = true;
                    }
                    if (node.getType() == null || this.scan(node.getType(), p).booleanValue()) {
                        if (node.getType() != null) {
                            this.spaces(1, this.fieldGroup);
                        }
                        if (!"<error>".contentEquals(node.getName())) {
                            this.accept(JavaTokenId.IDENTIFIER, new JavaTokenId[0]);
                        }
                    }
                    if ((init = node.getInitializer()) != null) {
                        int alignIndent = -1;
                        if (this.cs.alignMultilineAssignment()) {
                            alignIndent = this.col;
                            if (!"<error>".contentEquals(node.getName())) {
                                alignIndent -= node.getName().length();
                            }
                        }
                        if (this.cs.wrapAfterAssignOps()) {
                            boolean containedNewLine = this.spaces(this.cs.spaceAroundAssignOps() ? 1 : 0, false);
                            if (this.accept(JavaTokenId.EQ, new JavaTokenId[0]) == JavaTokenId.EQ && containedNewLine) {
                                this.newline();
                            }
                            if (init.getKind() == Tree.Kind.NEW_ARRAY && ((NewArrayTree)init).getType() == null) {
                                if (this.cs.getOtherBracePlacement() == CodeStyle.BracePlacement.SAME_LINE) {
                                    this.spaces(this.cs.spaceAroundAssignOps() ? 1 : 0);
                                }
                                this.scan((Tree)init, p);
                            } else {
                                this.wrapTree(this.cs.wrapAssignOps(), alignIndent, this.cs.spaceAroundAssignOps() ? 1 : 0, (Tree)init);
                            }
                        } else {
                            this.wrapOperatorAndTree(this.cs.wrapAssignOps(), alignIndent, this.cs.spaceAroundAssignOps() ? 1 : 0, (Tree)init);
                        }
                    }
                    this.fieldGroup = this.accept(JavaTokenId.SEMICOLON, JavaTokenId.COMMA) == JavaTokenId.COMMA;
                }
                finally {
                    this.continuationIndent = old;
                }
            }
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitMethod(MethodTree node, Void p) {
            BlockTree body;
            boolean old = this.continuationIndent;
            try {
                java.util.List tparams;
                Tree retType;
                Tree init;
                ModifiersTree mods = node.getModifiers();
                if (mods != null) {
                    if (this.scan((Tree)mods, p).booleanValue()) {
                        this.continuationIndent = true;
                        if (this.cs.placeNewLineAfterModifiers()) {
                            this.newline();
                        } else {
                            this.spaces(1, true);
                        }
                    } else {
                        this.newline();
                    }
                    this.afterAnnotation = false;
                }
                if ((tparams = node.getTypeParameters()) != null && !tparams.isEmpty()) {
                    JavaTokenId accepted;
                    if (JavaTokenId.LT == this.accept(JavaTokenId.LT, new JavaTokenId[0])) {
                        ++this.tpLevel;
                    }
                    this.continuationIndent = true;
                    Iterator it = tparams.iterator();
                    while (it.hasNext()) {
                        TypeParameterTree tparam = (TypeParameterTree)it.next();
                        this.scan((Tree)tparam, p);
                        if (!it.hasNext()) continue;
                        this.spaces(this.cs.spaceBeforeComma() ? 1 : 0);
                        this.accept(JavaTokenId.COMMA, new JavaTokenId[0]);
                        this.spaces(this.cs.spaceAfterComma() ? 1 : 0);
                    }
                    if (this.tpLevel > 0 && (accepted = this.accept(JavaTokenId.GT, JavaTokenId.GTGT, JavaTokenId.GTGTGT)) != null) {
                        switch (accepted) {
                            case GTGTGT: {
                                this.tpLevel -= 3;
                                break;
                            }
                            case GTGT: {
                                this.tpLevel -= 2;
                                break;
                            }
                            case GT: {
                                --this.tpLevel;
                            }
                        }
                    }
                    this.spaces(1, true);
                }
                if ((retType = node.getReturnType()) != null) {
                    this.scan(retType, p);
                    this.continuationIndent = true;
                    this.spaces(1, true);
                }
                if (!"<error>".contentEquals(node.getName())) {
                    this.accept(JavaTokenId.IDENTIFIER, new JavaTokenId[0]);
                }
                this.continuationIndent = true;
                this.spaces(this.cs.spaceBeforeMethodDeclParen() ? 1 : 0);
                this.accept(JavaTokenId.LPAREN, new JavaTokenId[0]);
                java.util.List params = node.getParameters();
                if (params != null && !params.isEmpty()) {
                    int oldIndent = this.indent;
                    boolean continuation = this.isLastIndentContinuation;
                    if (continuation) {
                        this.indent = this.indent();
                    }
                    try {
                        this.spaces(this.cs.spaceWithinMethodDeclParens() ? 1 : 0, true);
                        this.wrapList(this.cs.wrapMethodParams(), this.cs.alignMultilineMethodParams(), false, JavaTokenId.COMMA, params);
                    }
                    finally {
                        this.indent = oldIndent;
                        this.continuationIndent = continuation;
                    }
                    this.spaces(this.cs.spaceWithinMethodDeclParens() ? 1 : 0, true);
                }
                this.accept(JavaTokenId.RPAREN, new JavaTokenId[0]);
                this.continuationIndent = true;
                java.util.List threxs = node.getThrows();
                if (threxs != null && !threxs.isEmpty()) {
                    this.wrapToken(this.cs.wrapThrowsKeyword(), 1, JavaTokenId.THROWS, new JavaTokenId[0]);
                    this.wrapList(this.cs.wrapThrowsList(), this.cs.alignMultilineThrows(), true, JavaTokenId.COMMA, threxs);
                }
                if ((init = node.getDefaultValue()) != null) {
                    this.spaces(1, true);
                    this.accept(JavaTokenId.DEFAULT, new JavaTokenId[0]);
                    this.space();
                    this.scan(init, p);
                }
            }
            finally {
                this.continuationIndent = old;
            }
            if ((body = node.getBody()) != null) {
                this.scan((Tree)body, p);
            } else {
                this.accept(JavaTokenId.SEMICOLON, new JavaTokenId[0]);
            }
            return true;
        }

        public Boolean visitModifiers(ModifiersTree node, Void p) {
            boolean isStandalone;
            boolean ret = true;
            JavaTokenId id = null;
            this.afterAnnotation = false;
            Iterator annotations = node.getAnnotations().iterator();
            TreePath path = this.getCurrentPath().getParentPath();
            Tree parent = path.getLeaf();
            path = path.getParentPath();
            Tree grandParent = path.getLeaf();
            boolean bl = isStandalone = parent.getKind() != Tree.Kind.VARIABLE || TreeUtilities.CLASS_TREE_KINDS.contains((Object)grandParent.getKind()) || grandParent.getKind() == Tree.Kind.BLOCK;
            while (this.tokens.offset() < this.endPos) {
                if (this.afterAnnotation) {
                    if (!isStandalone) {
                        this.spaces(1, true);
                    } else {
                        switch (this.cs.wrapAnnotations()) {
                            case WRAP_ALWAYS: {
                                this.newline();
                                break;
                            }
                            case WRAP_IF_LONG: {
                                if (this.col >= this.rightMargin) {
                                    this.newline();
                                    break;
                                }
                                this.spaces(1, true);
                                break;
                            }
                            case WRAP_NEVER: {
                                this.spaces(1, true);
                            }
                        }
                    }
                } else if (id != null) {
                    this.space();
                }
                int index = this.tokens.index();
                int c = this.col;
                Diff d = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                int lbl = this.lastBlankLines;
                int lblti = this.lastBlankLinesTokenIndex;
                Diff lbld = this.lastBlankLinesDiff;
                id = this.accept(JavaTokenId.PRIVATE, JavaTokenId.PROTECTED, JavaTokenId.PUBLIC, JavaTokenId.STATIC, JavaTokenId.DEFAULT, JavaTokenId.TRANSIENT, JavaTokenId.FINAL, JavaTokenId.ABSTRACT, JavaTokenId.NATIVE, JavaTokenId.VOLATILE, JavaTokenId.SYNCHRONIZED, JavaTokenId.STRICTFP, JavaTokenId.AT);
                if (id == null) break;
                if (id == JavaTokenId.AT) {
                    if (annotations.hasNext()) {
                        this.rollback(index, c, d);
                        this.lastBlankLines = lbl;
                        this.lastBlankLinesTokenIndex = lblti;
                        this.lastBlankLinesDiff = lbld;
                        boolean bl2 = this.wrapAnnotation = this.cs.wrapAnnotations() == CodeStyle.WrapStyle.WRAP_ALWAYS;
                        if (!isStandalone || !this.afterAnnotation) {
                            this.scan((Tree)annotations.next(), p);
                        } else {
                            this.wrapTree(this.cs.wrapAnnotations(), -1, 0, (Tree)annotations.next());
                        }
                        this.wrapAnnotation = false;
                        this.afterAnnotation = true;
                        ret = false;
                        continue;
                    }
                    this.afterAnnotation = false;
                    ret = false;
                    continue;
                }
                this.afterAnnotation = false;
                ret = true;
            }
            return ret;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitAnnotation(AnnotationTree node, Void p) {
            this.accept(JavaTokenId.AT, new JavaTokenId[0]);
            boolean old = this.continuationIndent;
            try {
                this.continuationIndent = true;
                this.spaces(0, true);
                this.scan(node.getAnnotationType(), p);
                java.util.List args = node.getArguments();
                this.spaces(this.cs.spaceBeforeAnnotationParen() ? 1 : 0);
                this.accept(JavaTokenId.LPAREN, new JavaTokenId[0]);
                if (args != null && !args.isEmpty()) {
                    boolean oldInsideAnnotation = this.insideAnnotation;
                    this.insideAnnotation = true;
                    int oldIndent = this.indent;
                    boolean continuation = this.isLastIndentContinuation;
                    if (continuation) {
                        this.indent = this.indent();
                    }
                    try {
                        this.spaces(0, true);
                        this.wrapList(this.cs.wrapAnnotationArgs(), this.cs.alignMultilineAnnotationArgs(), this.cs.spaceWithinAnnotationParens(), JavaTokenId.COMMA, args);
                    }
                    finally {
                        this.indent = oldIndent;
                        this.continuationIndent = continuation;
                        this.insideAnnotation = oldInsideAnnotation;
                    }
                    this.spaces(this.cs.spaceWithinAnnotationParens() ? 1 : 0, true);
                }
                this.accept(JavaTokenId.RPAREN, new JavaTokenId[0]);
            }
            finally {
                this.continuationIndent = old;
            }
            return true;
        }

        public Boolean visitAnnotatedType(AnnotatedTypeTree node, Void p) {
            java.util.List annotations = node.getAnnotations();
            if (annotations != null && !annotations.isEmpty()) {
                switch (node.getUnderlyingType().getKind()) {
                    case MEMBER_SELECT: {
                        MemberSelectTree mst = (MemberSelectTree)node.getUnderlyingType();
                        this.scan((Tree)mst.getExpression(), p);
                        this.accept(JavaTokenId.DOT, new JavaTokenId[0]);
                        this.spaces(0);
                        Iterator it = annotations.iterator();
                        while (it.hasNext()) {
                            this.scan((Tree)it.next(), p);
                            if (!it.hasNext()) continue;
                            this.spaces(1, true);
                        }
                        this.space();
                        if ("<error>".contentEquals(mst.getIdentifier())) {
                            while (this.tokens.offset() < this.endPos) {
                                int len = this.tokens.token().length();
                                if (this.tokens.token().id() == JavaTokenId.WHITESPACE && this.tokens.offset() + len >= this.endPos) break;
                                this.col += len;
                                if (this.tokens.moveNext()) continue;
                            }
                            this.lastBlankLines = -1;
                            this.lastBlankLinesTokenIndex = -1;
                            this.lastBlankLinesDiff = null;
                        } else {
                            this.accept(JavaTokenId.IDENTIFIER, JavaTokenId.STAR, JavaTokenId.THIS, JavaTokenId.SUPER, JavaTokenId.CLASS);
                        }
                        return true;
                    }
                    case ARRAY_TYPE: {
                        ArrayTypeTree att = (ArrayTypeTree)node.getUnderlyingType();
                        boolean ret = this.scan(att.getType(), p);
                        this.space();
                        Iterator it = annotations.iterator();
                        while (it.hasNext()) {
                            this.scan((Tree)it.next(), p);
                            if (!it.hasNext()) continue;
                            this.spaces(1, true);
                        }
                        this.space();
                        JavaTokenId id = this.accept(JavaTokenId.LBRACKET, JavaTokenId.ELLIPSIS);
                        if (id == JavaTokenId.ELLIPSIS) {
                            return ret;
                        }
                        this.accept(JavaTokenId.RBRACKET, new JavaTokenId[0]);
                        return ret;
                    }
                }
                Iterator it = annotations.iterator();
                while (it.hasNext()) {
                    this.scan((Tree)it.next(), p);
                    if (!it.hasNext()) continue;
                    this.spaces(1, true);
                }
                this.space();
            }
            this.scan((Tree)node.getUnderlyingType(), p);
            return true;
        }

        public Boolean visitTypeParameter(TypeParameterTree node, Void p) {
            java.util.List bounds;
            if (!"<error>".contentEquals(node.getName())) {
                this.accept(JavaTokenId.IDENTIFIER, new JavaTokenId[0]);
            }
            if ((bounds = node.getBounds()) != null && !bounds.isEmpty()) {
                this.space();
                this.accept(JavaTokenId.EXTENDS, new JavaTokenId[0]);
                this.space();
                Iterator it = bounds.iterator();
                while (it.hasNext()) {
                    Tree bound = (Tree)it.next();
                    this.scan(bound, p);
                    if (!it.hasNext()) continue;
                    this.space();
                    this.accept(JavaTokenId.AMP, new JavaTokenId[0]);
                    this.space();
                }
            }
            return true;
        }

        public Boolean visitParameterizedType(ParameterizedTypeTree node, Void p) {
            boolean ltRead;
            JavaTokenId accepted;
            block10 : {
                Diff d;
                this.scan(node.getType(), p);
                int index = this.tokens.index();
                int c = this.col;
                Diff diff = d = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                if (JavaTokenId.LT == this.accept(JavaTokenId.LT, new JavaTokenId[0])) {
                    ++this.tpLevel;
                    ltRead = true;
                } else {
                    this.rollback(index, c, d);
                    ltRead = false;
                }
                java.util.List targs = node.getTypeArguments();
                if (targs != null && !targs.isEmpty()) {
                    Iterator it = targs.iterator();
                    Tree targ = it.hasNext() ? (Tree)it.next() : null;
                    do {
                        this.scan(targ, p);
                        Tree tree = targ = it.hasNext() ? (Tree)it.next() : null;
                        if (targ == null) break block10;
                        if (targ.getKind() == Tree.Kind.ERRONEOUS && ((ErroneousTree)targ).getErrorTrees().isEmpty() && !it.hasNext()) break;
                        this.spaces(this.cs.spaceBeforeComma() ? 1 : 0);
                        this.accept(JavaTokenId.COMMA, new JavaTokenId[0]);
                        this.spaces(this.cs.spaceAfterComma() ? 1 : 0);
                    } while (true);
                    this.scan(targ, p);
                    return true;
                }
            }
            if (ltRead && this.tpLevel > 0 && (accepted = this.accept(JavaTokenId.GT, JavaTokenId.GTGT, JavaTokenId.GTGTGT)) != null) {
                switch (accepted) {
                    case GTGTGT: {
                        this.tpLevel -= 3;
                        break;
                    }
                    case GTGT: {
                        this.tpLevel -= 2;
                        break;
                    }
                    case GT: {
                        --this.tpLevel;
                    }
                }
            }
            return true;
        }

        public Boolean visitWildcard(WildcardTree node, Void p) {
            this.accept(JavaTokenId.QUESTION, new JavaTokenId[0]);
            Tree bound = node.getBound();
            if (bound != null) {
                this.space();
                this.accept(JavaTokenId.EXTENDS, JavaTokenId.SUPER);
                this.space();
                this.scan(bound, p);
            }
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitBlock(BlockTree node, Void p) {
            CodeStyle.BracePlacement bracePlacement;
            if (node.isStatic()) {
                this.accept(JavaTokenId.STATIC, new JavaTokenId[0]);
            }
            boolean spaceBeforeLeftBrace = false;
            switch (this.getCurrentPath().getParentPath().getLeaf().getKind()) {
                case ANNOTATION_TYPE: 
                case CLASS: 
                case ENUM: 
                case INTERFACE: {
                    bracePlacement = this.cs.getOtherBracePlacement();
                    if (!node.isStatic()) break;
                    spaceBeforeLeftBrace = this.cs.spaceBeforeStaticInitLeftBrace();
                    break;
                }
                case METHOD: {
                    bracePlacement = this.cs.getMethodDeclBracePlacement();
                    spaceBeforeLeftBrace = this.cs.spaceBeforeMethodDeclLeftBrace();
                    break;
                }
                case LAMBDA_EXPRESSION: {
                    bracePlacement = this.cs.getOtherBracePlacement();
                    spaceBeforeLeftBrace = this.cs.spaceAroundLambdaArrow();
                    break;
                }
                case TRY: {
                    bracePlacement = this.cs.getOtherBracePlacement();
                    if (((TryTree)this.getCurrentPath().getParentPath().getLeaf()).getBlock() == node) {
                        spaceBeforeLeftBrace = this.cs.spaceBeforeTryLeftBrace();
                        break;
                    }
                    spaceBeforeLeftBrace = this.cs.spaceBeforeFinallyLeftBrace();
                    break;
                }
                case CATCH: {
                    bracePlacement = this.cs.getOtherBracePlacement();
                    spaceBeforeLeftBrace = this.cs.spaceBeforeCatchLeftBrace();
                    break;
                }
                case WHILE_LOOP: {
                    bracePlacement = this.cs.getOtherBracePlacement();
                    spaceBeforeLeftBrace = this.cs.spaceBeforeWhileLeftBrace();
                    break;
                }
                case FOR_LOOP: 
                case ENHANCED_FOR_LOOP: {
                    bracePlacement = this.cs.getOtherBracePlacement();
                    spaceBeforeLeftBrace = this.cs.spaceBeforeForLeftBrace();
                    break;
                }
                case DO_WHILE_LOOP: {
                    bracePlacement = this.cs.getOtherBracePlacement();
                    spaceBeforeLeftBrace = this.cs.spaceBeforeDoLeftBrace();
                    break;
                }
                case IF: {
                    bracePlacement = this.cs.getOtherBracePlacement();
                    if (((IfTree)this.getCurrentPath().getParentPath().getLeaf()).getThenStatement() == node) {
                        spaceBeforeLeftBrace = this.cs.spaceBeforeIfLeftBrace();
                        break;
                    }
                    spaceBeforeLeftBrace = this.cs.spaceBeforeElseLeftBrace();
                    break;
                }
                case SYNCHRONIZED: {
                    bracePlacement = this.cs.getOtherBracePlacement();
                    spaceBeforeLeftBrace = this.cs.spaceBeforeSynchronizedLeftBrace();
                    break;
                }
                case CASE: {
                    bracePlacement = this.cs.getOtherBracePlacement();
                    spaceBeforeLeftBrace = true;
                    break;
                }
                default: {
                    bracePlacement = this.cs.getOtherBracePlacement();
                }
            }
            int old = this.lastIndent;
            int halfIndent = this.lastIndent;
            switch (bracePlacement) {
                case SAME_LINE: {
                    this.spaces(spaceBeforeLeftBrace ? 1 : 0, this.tokens.offset() < this.startOffset);
                    if (node instanceof FakeBlock) {
                        this.appendToDiff("{");
                        this.lastBlankLines = -1;
                        this.lastBlankLinesTokenIndex = -1;
                        this.lastBlankLinesDiff = null;
                    } else {
                        this.accept(JavaTokenId.LBRACE, new JavaTokenId[0]);
                    }
                    this.indent = this.lastIndent + this.indentSize;
                    break;
                }
                case NEW_LINE: {
                    this.newline();
                    if (node instanceof FakeBlock) {
                        this.indent = this.lastIndent + this.indentSize;
                        this.appendToDiff("{");
                        this.lastBlankLines = -1;
                        this.lastBlankLinesTokenIndex = -1;
                        this.lastBlankLinesDiff = null;
                        break;
                    }
                    this.accept(JavaTokenId.LBRACE, new JavaTokenId[0]);
                    this.indent = this.lastIndent + this.indentSize;
                    break;
                }
                case NEW_LINE_HALF_INDENTED: {
                    int oldLast = this.lastIndent;
                    halfIndent = this.indent = this.lastIndent + (this.indentSize >> 1);
                    this.newline();
                    if (node instanceof FakeBlock) {
                        this.indent = oldLast + this.indentSize;
                        this.appendToDiff("{");
                        this.lastBlankLines = -1;
                        this.lastBlankLinesTokenIndex = -1;
                        this.lastBlankLinesDiff = null;
                        break;
                    }
                    this.accept(JavaTokenId.LBRACE, new JavaTokenId[0]);
                    this.indent = oldLast + this.indentSize;
                    break;
                }
                case NEW_LINE_INDENTED: {
                    halfIndent = this.indent = this.lastIndent + this.indentSize;
                    this.newline();
                    if (node instanceof FakeBlock) {
                        this.appendToDiff("{");
                        this.lastBlankLines = -1;
                        this.lastBlankLinesTokenIndex = -1;
                        this.lastBlankLinesDiff = null;
                        break;
                    }
                    this.accept(JavaTokenId.LBRACE, new JavaTokenId[0]);
                }
            }
            boolean isEmpty = true;
            int lastMaxPreservedBlankLines = this.maxPreservedBlankLines;
            this.maxPreservedBlankLines = this.cs.getMaximumBlankLinesInCode();
            for (StatementTree stat : node.getStatements()) {
                if (this.isSynthetic(this.getCurrentPath().getCompilationUnit(), (Tree)stat)) continue;
                isEmpty = false;
                if (stat.getKind() == Tree.Kind.LABELED_STATEMENT && this.cs.absoluteLabelIndent()) {
                    int o = this.indent;
                    int oLDiff = this.lastIndent - this.indent;
                    boolean oCI = this.continuationIndent;
                    try {
                        this.indent = 0;
                        this.continuationIndent = false;
                        if (node instanceof FakeBlock) {
                            this.appendToDiff(this.getNewlines(1) + this.getIndent());
                            this.col = this.indent();
                        } else {
                            this.newline();
                        }
                        oLDiff = this.lastIndent - this.indent;
                    }
                    finally {
                        this.indent = o;
                        this.lastIndent = oLDiff + this.indent;
                        this.continuationIndent = oCI;
                    }
                }
                if (node instanceof FakeBlock) {
                    this.appendToDiff(this.getNewlines(1) + this.getIndent());
                    this.col = this.indent();
                } else if (stat.getKind() == Tree.Kind.EMPTY_STATEMENT || stat.getKind() == Tree.Kind.EXPRESSION_STATEMENT && ((ExpressionStatementTree)stat).getExpression().getKind() == Tree.Kind.ERRONEOUS) {
                    this.spaces(0, true);
                } else if (!this.fieldGroup || stat.getKind() != Tree.Kind.VARIABLE) {
                    this.newline();
                }
                this.scan((Tree)stat, p);
            }
            if (isEmpty) {
                this.newline();
            }
            if (node instanceof FakeBlock) {
                this.indent = halfIndent;
                int i = this.tokens.index();
                boolean loop = true;
                block28 : while (loop) {
                    switch ((JavaTokenId)this.tokens.token().id()) {
                        case WHITESPACE: {
                            if (this.tokens.token().text().toString().indexOf(10) < 0) {
                                this.tokens.moveNext();
                                continue block28;
                            }
                            loop = false;
                            this.appendToDiff("\n");
                            this.col = 0;
                            continue block28;
                        }
                        case LINE_COMMENT: {
                            loop = false;
                        }
                        case BLOCK_COMMENT: {
                            this.tokens.moveNext();
                            continue block28;
                        }
                    }
                    if (this.tokens.index() != i) {
                        this.tokens.moveIndex(i);
                        this.tokens.moveNext();
                    }
                    loop = false;
                    this.appendToDiff("\n");
                    this.col = 0;
                }
                this.appendToDiff(this.getIndent() + "}");
                this.col = this.indent() + 1;
                this.lastBlankLines = -1;
                this.lastBlankLinesTokenIndex = -1;
                this.lastBlankLinesDiff = null;
            } else {
                Diff diff;
                this.newline();
                this.indent = halfIndent;
                Diff diff2 = diff = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                if (diff != null && diff.end == this.tokens.offset()) {
                    String spaces;
                    if (diff.text != null) {
                        int idx = diff.text.lastIndexOf(10);
                        if (idx < 0) {
                            diff.text = this.getIndent();
                        } else {
                            diff.text = diff.text.substring(0, idx + 1) + this.getIndent();
                        }
                    }
                    String string = spaces = diff.text != null ? diff.text : this.getIndent();
                    if (spaces.equals(this.fText.substring(diff.start, diff.end))) {
                        this.diffs.removeFirst();
                    }
                } else if (this.tokens.movePrevious()) {
                    if (this.tokens.token().id() == JavaTokenId.WHITESPACE) {
                        String text = this.tokens.token().text().toString();
                        int idx = text.lastIndexOf(10);
                        if (idx >= 0) {
                            text = text.substring(idx + 1);
                            String ind = this.getIndent();
                            if (!ind.equals(text)) {
                                this.addDiff(new Diff(this.tokens.offset() + idx + 1, this.tokens.offset() + this.tokens.token().length(), ind));
                            }
                        } else if (this.tokens.movePrevious()) {
                            if (this.tokens.token().id() == JavaTokenId.LINE_COMMENT) {
                                this.tokens.moveNext();
                                String ind = this.getIndent();
                                if (!ind.equals(text)) {
                                    this.addDiff(new Diff(this.tokens.offset(), this.tokens.offset() + this.tokens.token().length(), ind));
                                }
                            } else {
                                this.tokens.moveNext();
                            }
                        }
                    }
                    this.tokens.moveNext();
                }
                this.col = this.indent();
                this.accept(JavaTokenId.RBRACE, new JavaTokenId[0]);
            }
            this.maxPreservedBlankLines = lastMaxPreservedBlankLines;
            this.indent = this.lastIndent = old;
            return true;
        }

        public Boolean visitMemberSelect(MemberSelectTree node, Void p) {
            this.scan((Tree)node.getExpression(), p);
            if ("<error>".contentEquals(node.getIdentifier())) {
                while (this.tokens.offset() < this.endPos) {
                    int len = this.tokens.token().length();
                    if (this.tokens.token().id() == JavaTokenId.WHITESPACE && this.tokens.offset() + len >= this.endPos) break;
                    this.col += len;
                    if (this.tokens.moveNext()) continue;
                }
                this.lastBlankLines = -1;
                this.lastBlankLinesTokenIndex = -1;
                this.lastBlankLinesDiff = null;
            } else {
                this.accept(JavaTokenId.DOT, new JavaTokenId[0]);
                this.accept(JavaTokenId.IDENTIFIER, JavaTokenId.STAR, JavaTokenId.THIS, JavaTokenId.SUPER, JavaTokenId.CLASS);
            }
            return true;
        }

        public Boolean visitMemberReference(MemberReferenceTree node, Void p) {
            boolean ltRead;
            JavaTokenId accepted;
            block13 : {
                Diff d;
                this.scan((Tree)node.getQualifierExpression(), p);
                this.spaces(this.cs.spaceAroundMethodReferenceDoubleColon() ? 1 : 0);
                this.accept(JavaTokenId.COLONCOLON, new JavaTokenId[0]);
                this.spaces(this.cs.spaceAroundMethodReferenceDoubleColon() ? 1 : 0);
                int index = this.tokens.index();
                int c = this.col;
                Diff diff = d = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                if (JavaTokenId.LT == this.accept(JavaTokenId.LT, new JavaTokenId[0])) {
                    ++this.tpLevel;
                    ltRead = true;
                } else {
                    this.rollback(index, c, d);
                    ltRead = false;
                }
                java.util.List targs = node.getTypeArguments();
                if (targs != null && !targs.isEmpty()) {
                    Iterator it = targs.iterator();
                    Tree targ = it.hasNext() ? (Tree)it.next() : null;
                    do {
                        this.scan(targ, p);
                        Tree tree = targ = it.hasNext() ? (Tree)it.next() : null;
                        if (targ == null) break block13;
                        if (targ.getKind() == Tree.Kind.ERRONEOUS && ((ErroneousTree)targ).getErrorTrees().isEmpty() && !it.hasNext()) break;
                        this.spaces(this.cs.spaceBeforeComma() ? 1 : 0);
                        this.accept(JavaTokenId.COMMA, new JavaTokenId[0]);
                        this.spaces(this.cs.spaceAfterComma() ? 1 : 0);
                    } while (true);
                    this.scan(targ, p);
                    return true;
                }
            }
            if (ltRead && this.tpLevel > 0 && (accepted = this.accept(JavaTokenId.GT, JavaTokenId.GTGT, JavaTokenId.GTGTGT)) != null) {
                switch (accepted) {
                    case GTGTGT: {
                        this.tpLevel -= 3;
                        break;
                    }
                    case GTGT: {
                        this.tpLevel -= 2;
                        break;
                    }
                    case GT: {
                        --this.tpLevel;
                    }
                }
            }
            if ("<error>".contentEquals(node.getName())) {
                while (this.tokens.offset() < this.endPos) {
                    int len = this.tokens.token().length();
                    if (this.tokens.token().id() == JavaTokenId.WHITESPACE && this.tokens.offset() + len >= this.endPos) break;
                    this.col += len;
                    if (this.tokens.moveNext()) continue;
                }
                this.lastBlankLines = -1;
                this.lastBlankLinesTokenIndex = -1;
                this.lastBlankLinesDiff = null;
            } else {
                this.accept(JavaTokenId.IDENTIFIER, JavaTokenId.NEW);
            }
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitLambdaExpression(LambdaExpressionTree node, Void p) {
            JavaTokenId accepted;
            java.util.List params = node.getParameters();
            JavaTokenId javaTokenId = accepted = params != null && params.size() == 1 ? this.accept(JavaTokenId.LPAREN, JavaTokenId.IDENTIFIER) : this.accept(JavaTokenId.LPAREN, new JavaTokenId[0]);
            if (accepted == JavaTokenId.LPAREN) {
                boolean old = this.continuationIndent;
                try {
                    if (params != null && !params.isEmpty()) {
                        int oldIndent = this.indent;
                        boolean continuation = this.isLastIndentContinuation;
                        if (continuation) {
                            this.indent = this.indent();
                        }
                        try {
                            this.spaces(this.cs.spaceWithinLambdaParens() ? 1 : 0, true);
                            this.wrapList(this.cs.wrapLambdaParams(), this.cs.alignMultilineLambdaParams(), false, JavaTokenId.COMMA, params);
                        }
                        finally {
                            this.indent = oldIndent;
                            this.continuationIndent = continuation;
                        }
                        this.spaces(this.cs.spaceWithinLambdaParens() ? 1 : 0);
                    }
                    this.accept(JavaTokenId.RPAREN, new JavaTokenId[0]);
                }
                finally {
                    this.continuationIndent = old;
                }
            }
            if (this.cs.wrapAfterLambdaArrow()) {
                boolean containedNewLine = this.spaces(this.cs.spaceAroundLambdaArrow() ? 1 : 0, false);
                if (this.accept(JavaTokenId.ARROW, new JavaTokenId[0]) != null) {
                    this.col += 2;
                    this.lastBlankLines = -1;
                    this.lastBlankLinesTokenIndex = -1;
                    if (containedNewLine) {
                        this.newline();
                    }
                }
                if (node.getBodyKind() == LambdaExpressionTree.BodyKind.STATEMENT) {
                    boolean old = this.continuationIndent;
                    this.continuationIndent = this.isLastIndentContinuation;
                    try {
                        this.scan(node.getBody(), p);
                    }
                    finally {
                        this.continuationIndent = old;
                    }
                }
                this.wrapTree(this.cs.wrapLambdaArrow(), -1, this.cs.spaceAroundLambdaArrow() ? 1 : 0, node.getBody());
            } else {
                this.wrapOperatorAndTree(this.cs.wrapLambdaArrow(), -1, this.cs.spaceAroundLambdaArrow() ? 1 : 0, node.getBody());
            }
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitMethodInvocation(MethodInvocationTree node, Void p) {
            ExpressionTree ms = node.getMethodSelect();
            if (ms.getKind() == Tree.Kind.MEMBER_SELECT) {
                ExpressionTree exp = ((MemberSelectTree)ms).getExpression();
                this.scan((Tree)exp, p);
                CodeStyle.WrapStyle wrapStyle = this.cs.wrapChainedMethodCalls();
                if (wrapStyle == CodeStyle.WrapStyle.WRAP_ALWAYS && exp.getKind() != Tree.Kind.METHOD_INVOCATION) {
                    wrapStyle = CodeStyle.WrapStyle.WRAP_IF_LONG;
                }
                switch (wrapStyle) {
                    case WRAP_ALWAYS: {
                        if (this.cs.wrapAfterDotInChainedMethodCalls()) {
                            this.accept(JavaTokenId.DOT, new JavaTokenId[0]);
                            this.newline();
                        } else {
                            this.newline();
                            this.accept(JavaTokenId.DOT, new JavaTokenId[0]);
                        }
                        this.scanMethodCall(node);
                        break;
                    }
                    case WRAP_IF_LONG: {
                        int index = this.tokens.index();
                        int c = this.col;
                        Diff d = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                        int o = this.tokens.offset();
                        WrapAbort oldCheckWrap = this.checkWrap;
                        this.checkWrap = new WrapAbort(o);
                        try {
                            this.spaces(0, true);
                            this.accept(JavaTokenId.DOT, new JavaTokenId[0]);
                            this.spaces(0, true);
                            this.scanMethodCall(node);
                        }
                        catch (WrapAbort wa) {}
                        finally {
                            this.checkWrap = oldCheckWrap;
                        }
                        if (this.col <= this.rightMargin || o < this.lastNewLineOffset) break;
                        this.rollback(index, c, d);
                        if (this.cs.wrapAfterDotInChainedMethodCalls()) {
                            this.accept(JavaTokenId.DOT, new JavaTokenId[0]);
                            this.newline();
                        } else {
                            this.newline();
                            this.accept(JavaTokenId.DOT, new JavaTokenId[0]);
                        }
                        this.scanMethodCall(node);
                        break;
                    }
                    case WRAP_NEVER: {
                        this.spaces(0, true);
                        this.accept(JavaTokenId.DOT, new JavaTokenId[0]);
                        this.spaces(0, true);
                        this.scanMethodCall(node);
                    }
                }
            } else {
                this.scanMethodCall(node);
            }
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitNewClass(NewClassTree node, Void p) {
            ExpressionTree encl = node.getEnclosingExpression();
            if (encl != null) {
                this.scan((Tree)encl, p);
                this.accept(JavaTokenId.DOT, new JavaTokenId[0]);
            }
            this.accept(JavaTokenId.NEW, new JavaTokenId[0]);
            this.space();
            java.util.List targs = node.getTypeArguments();
            if (targs != null && !targs.isEmpty()) {
                JavaTokenId accepted;
                if (JavaTokenId.LT == this.accept(JavaTokenId.LT, new JavaTokenId[0])) {
                    ++this.tpLevel;
                }
                Iterator it = targs.iterator();
                while (it.hasNext()) {
                    Tree targ = (Tree)it.next();
                    this.scan(targ, p);
                    if (!it.hasNext()) continue;
                    this.spaces(this.cs.spaceBeforeComma() ? 1 : 0);
                    this.accept(JavaTokenId.COMMA, new JavaTokenId[0]);
                    this.spaces(this.cs.spaceAfterComma() ? 1 : 0);
                }
                if (this.tpLevel > 0 && (accepted = this.accept(JavaTokenId.GT, JavaTokenId.GTGT, JavaTokenId.GTGTGT)) != null) {
                    switch (accepted) {
                        case GTGTGT: {
                            this.tpLevel -= 3;
                            break;
                        }
                        case GTGT: {
                            this.tpLevel -= 2;
                            break;
                        }
                        case GT: {
                            --this.tpLevel;
                        }
                    }
                }
            }
            this.scan((Tree)node.getIdentifier(), p);
            this.spaces(this.cs.spaceBeforeMethodCallParen() ? 1 : 0);
            this.accept(JavaTokenId.LPAREN, new JavaTokenId[0]);
            boolean old = this.continuationIndent;
            try {
                java.util.List args = node.getArguments();
                if (args != null && !args.isEmpty()) {
                    int oldIndent = this.indent;
                    boolean continuation = this.isLastIndentContinuation;
                    if (continuation) {
                        this.indent = this.indent();
                    }
                    try {
                        this.spaces(this.cs.spaceWithinMethodCallParens() ? 1 : 0, true);
                        this.wrapList(this.cs.wrapMethodCallArgs(), this.cs.alignMultilineCallArgs(), false, JavaTokenId.COMMA, args);
                    }
                    finally {
                        this.indent = oldIndent;
                        this.continuationIndent = continuation;
                    }
                    this.spaces(this.cs.spaceWithinMethodCallParens() ? 1 : 0, true);
                }
                this.accept(JavaTokenId.RPAREN, new JavaTokenId[0]);
                this.continuationIndent = old;
                ClassTree body = node.getClassBody();
                if (body != null) {
                    this.continuationIndent = this.isLastIndentContinuation;
                    this.scan((Tree)body, p);
                }
            }
            finally {
                this.continuationIndent = old;
            }
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitAssert(AssertTree node, Void p) {
            this.accept(JavaTokenId.ASSERT, new JavaTokenId[0]);
            boolean old = this.continuationIndent;
            try {
                this.continuationIndent = true;
                this.space();
                this.scan((Tree)node.getCondition(), p);
                ExpressionTree detail = node.getDetail();
                if (detail != null) {
                    this.spaces(this.cs.spaceBeforeColon() ? 1 : 0);
                    this.accept(JavaTokenId.COLON, new JavaTokenId[0]);
                    this.wrapTree(this.cs.wrapAssert(), -1, this.cs.spaceAfterColon() ? 1 : 0, (Tree)detail);
                }
                this.accept(JavaTokenId.SEMICOLON, new JavaTokenId[0]);
            }
            finally {
                this.continuationIndent = old;
            }
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitReturn(ReturnTree node, Void p) {
            this.accept(JavaTokenId.RETURN, new JavaTokenId[0]);
            boolean old = this.continuationIndent;
            try {
                this.continuationIndent = true;
                ExpressionTree exp = node.getExpression();
                if (exp != null) {
                    this.space();
                    this.scan((Tree)exp, p);
                }
                this.accept(JavaTokenId.SEMICOLON, new JavaTokenId[0]);
            }
            finally {
                this.continuationIndent = old;
            }
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitThrow(ThrowTree node, Void p) {
            this.accept(JavaTokenId.THROW, new JavaTokenId[0]);
            boolean old = this.continuationIndent;
            try {
                this.continuationIndent = true;
                ExpressionTree exp = node.getExpression();
                if (exp != null) {
                    this.space();
                    this.scan((Tree)exp, p);
                }
                this.accept(JavaTokenId.SEMICOLON, new JavaTokenId[0]);
            }
            finally {
                this.continuationIndent = old;
            }
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitTry(TryTree node, Void p) {
            this.accept(JavaTokenId.TRY, new JavaTokenId[0]);
            java.util.List res = node.getResources();
            if (res != null && !res.isEmpty()) {
                boolean old = this.continuationIndent;
                try {
                    Diff d;
                    this.continuationIndent = true;
                    this.spaces(this.cs.spaceBeforeTryParen() ? 1 : 0);
                    this.accept(JavaTokenId.LPAREN, new JavaTokenId[0]);
                    this.spaces(this.cs.spaceWithinTryParens() ? 1 : 0, true);
                    this.wrapList(this.cs.wrapTryResources(), this.cs.alignMultilineTryResources(), false, JavaTokenId.SEMICOLON, res);
                    int index = this.tokens.index();
                    int c = this.col;
                    Diff diff = d = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                    if (this.accept(JavaTokenId.SEMICOLON, new JavaTokenId[0]) == null) {
                        this.rollback(index, c, d);
                    }
                    this.spaces(this.cs.spaceWithinTryParens() ? 1 : 0);
                    this.accept(JavaTokenId.RPAREN, new JavaTokenId[0]);
                }
                finally {
                    this.continuationIndent = old;
                }
            }
            this.scan((Tree)node.getBlock(), p);
            for (CatchTree catchTree : node.getCatches()) {
                if (this.cs.placeCatchOnNewLine()) {
                    this.newline();
                } else {
                    this.spaces(this.cs.spaceBeforeCatch() ? 1 : 0);
                }
                this.scan((Tree)catchTree, p);
            }
            BlockTree finallyBlockTree = node.getFinallyBlock();
            if (finallyBlockTree != null) {
                if (this.cs.placeFinallyOnNewLine()) {
                    this.newline();
                } else {
                    this.spaces(this.cs.spaceBeforeFinally() ? 1 : 0);
                }
                this.accept(JavaTokenId.FINALLY, new JavaTokenId[0]);
                this.scan((Tree)finallyBlockTree, p);
            }
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitCatch(CatchTree node, Void p) {
            this.accept(JavaTokenId.CATCH, new JavaTokenId[0]);
            boolean old = this.continuationIndent;
            try {
                this.continuationIndent = true;
                this.spaces(this.cs.spaceBeforeCatchParen() ? 1 : 0);
                this.accept(JavaTokenId.LPAREN, new JavaTokenId[0]);
                this.spaces(this.cs.spaceWithinCatchParens() ? 1 : 0);
                this.scan((Tree)node.getParameter(), p);
                this.spaces(this.cs.spaceWithinCatchParens() ? 1 : 0);
                this.accept(JavaTokenId.RPAREN, new JavaTokenId[0]);
            }
            finally {
                this.continuationIndent = old;
            }
            this.scan((Tree)node.getBlock(), p);
            return true;
        }

        public Boolean visitUnionType(UnionTypeTree node, Void p) {
            java.util.List alts = node.getTypeAlternatives();
            if (alts != null && !alts.isEmpty()) {
                this.wrapList(this.cs.wrapDisjunctiveCatchTypes(), this.cs.alignMultilineDisjunctiveCatchTypes(), false, JavaTokenId.BAR, alts);
            }
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitIf(IfTree node, Void p) {
            this.accept(JavaTokenId.IF, new JavaTokenId[0]);
            boolean old = this.continuationIndent;
            try {
                this.continuationIndent = true;
                this.spaces(this.cs.spaceBeforeIfParen() ? 1 : 0);
                this.scan((Tree)node.getCondition(), p);
            }
            finally {
                this.continuationIndent = old;
            }
            StatementTree elseStat = node.getElseStatement();
            CodeStyle.BracesGenerationStyle redundantIfBraces = this.cs.redundantIfBraces();
            if (elseStat != null && redundantIfBraces == CodeStyle.BracesGenerationStyle.ELIMINATE && this.danglingElseChecker.hasDanglingElse((Tree)node.getThenStatement()) || redundantIfBraces == CodeStyle.BracesGenerationStyle.GENERATE && ((long)this.startOffset > this.sp.getStartPosition(this.root, (Tree)node) || (long)this.endOffset < this.sp.getEndPosition(this.root, (Tree)node))) {
                redundantIfBraces = CodeStyle.BracesGenerationStyle.LEAVE_ALONE;
            }
            this.lastIndent = this.indent;
            boolean prevblock = this.wrapStatement(this.cs.wrapIfStatement(), redundantIfBraces, this.cs.spaceBeforeIfLeftBrace() ? 1 : 0, node.getThenStatement());
            if (elseStat != null) {
                if (this.cs.placeElseOnNewLine() || !prevblock) {
                    this.newline();
                } else {
                    this.spaces(this.cs.spaceBeforeElse() ? 1 : 0, this.tokens.offset() < this.startOffset);
                }
                this.accept(JavaTokenId.ELSE, new JavaTokenId[0]);
                if (elseStat.getKind() == Tree.Kind.IF && this.cs.specialElseIf()) {
                    this.space();
                    this.scan((Tree)elseStat, p);
                } else {
                    redundantIfBraces = this.cs.redundantIfBraces();
                    if (redundantIfBraces == CodeStyle.BracesGenerationStyle.GENERATE && ((long)this.startOffset > this.sp.getStartPosition(this.root, (Tree)node) || (long)this.endOffset < this.sp.getEndPosition(this.root, (Tree)node))) {
                        redundantIfBraces = CodeStyle.BracesGenerationStyle.LEAVE_ALONE;
                    }
                    this.wrapStatement(this.cs.wrapIfStatement(), redundantIfBraces, this.cs.spaceBeforeElseLeftBrace() ? 1 : 0, elseStat);
                }
            }
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitDoWhileLoop(DoWhileLoopTree node, Void p) {
            this.accept(JavaTokenId.DO, new JavaTokenId[0]);
            this.lastIndent = this.indent;
            boolean old = this.continuationIndent;
            try {
                boolean isBlock;
                Iterator stats;
                CodeStyle.BracesGenerationStyle redundantDoWhileBraces = this.cs.redundantDoWhileBraces();
                if (redundantDoWhileBraces == CodeStyle.BracesGenerationStyle.GENERATE && ((long)this.startOffset > this.sp.getStartPosition(this.root, (Tree)node) || (long)this.endOffset < this.sp.getEndPosition(this.root, (Tree)node))) {
                    redundantDoWhileBraces = CodeStyle.BracesGenerationStyle.LEAVE_ALONE;
                }
                boolean bl = isBlock = node.getStatement().getKind() == Tree.Kind.BLOCK || redundantDoWhileBraces == CodeStyle.BracesGenerationStyle.GENERATE;
                if (isBlock && redundantDoWhileBraces == CodeStyle.BracesGenerationStyle.ELIMINATE && (stats = ((BlockTree)node.getStatement()).getStatements().iterator()).hasNext()) {
                    StatementTree stat = (StatementTree)stats.next();
                    if (!stats.hasNext() && stat.getKind() != Tree.Kind.VARIABLE) {
                        isBlock = false;
                    }
                }
                isBlock = this.wrapStatement(this.cs.wrapDoWhileStatement(), redundantDoWhileBraces, !isBlock || this.cs.spaceBeforeDoLeftBrace() ? 1 : 0, node.getStatement());
                if (this.cs.placeWhileOnNewLine() || !isBlock) {
                    this.newline();
                } else {
                    this.spaces(this.cs.spaceBeforeWhile() ? 1 : 0);
                }
                this.accept(JavaTokenId.WHILE, new JavaTokenId[0]);
                this.continuationIndent = true;
                this.spaces(this.cs.spaceBeforeWhileParen() ? 1 : 0);
                this.scan((Tree)node.getCondition(), p);
                this.accept(JavaTokenId.SEMICOLON, new JavaTokenId[0]);
            }
            finally {
                this.continuationIndent = old;
            }
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitWhileLoop(WhileLoopTree node, Void p) {
            this.accept(JavaTokenId.WHILE, new JavaTokenId[0]);
            boolean old = this.continuationIndent;
            try {
                this.continuationIndent = true;
                this.spaces(this.cs.spaceBeforeWhileParen() ? 1 : 0);
                this.scan((Tree)node.getCondition(), p);
            }
            finally {
                this.continuationIndent = old;
            }
            this.lastIndent = this.indent;
            CodeStyle.BracesGenerationStyle redundantWhileBraces = this.cs.redundantWhileBraces();
            if (redundantWhileBraces == CodeStyle.BracesGenerationStyle.GENERATE && ((long)this.startOffset > this.sp.getStartPosition(this.root, (Tree)node) || (long)this.endOffset < this.sp.getEndPosition(this.root, (Tree)node))) {
                redundantWhileBraces = CodeStyle.BracesGenerationStyle.LEAVE_ALONE;
            }
            this.wrapStatement(this.cs.wrapWhileStatement(), redundantWhileBraces, this.cs.spaceBeforeWhileLeftBrace() ? 1 : 0, node.getStatement());
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitForLoop(ForLoopTree node, Void p) {
            this.accept(JavaTokenId.FOR, new JavaTokenId[0]);
            boolean old = this.continuationIndent;
            try {
                this.continuationIndent = true;
                this.spaces(this.cs.spaceBeforeForParen() ? 1 : 0);
                this.accept(JavaTokenId.LPAREN, new JavaTokenId[0]);
                this.spaces(this.cs.spaceWithinForParens() ? 1 : 0);
                java.util.List inits = node.getInitializer();
                int alignIndent = -1;
                if (inits != null && !inits.isEmpty()) {
                    if (this.cs.alignMultilineFor()) {
                        alignIndent = this.col;
                    }
                    Iterator it = inits.iterator();
                    while (it.hasNext()) {
                        this.scan((Tree)it.next(), p);
                        if (!it.hasNext() || this.fieldGroup) continue;
                        this.spaces(this.cs.spaceBeforeComma() ? 1 : 0);
                        this.accept(JavaTokenId.COMMA, new JavaTokenId[0]);
                        this.spaces(this.cs.spaceAfterComma() ? 1 : 0);
                    }
                    this.spaces(this.cs.spaceBeforeSemi() ? 1 : 0);
                }
                this.accept(JavaTokenId.SEMICOLON, new JavaTokenId[0]);
                ExpressionTree cond = node.getCondition();
                if (cond != null) {
                    this.wrapTree(this.cs.wrapFor(), alignIndent, this.cs.spaceAfterSemi() ? 1 : 0, (Tree)cond);
                    this.spaces(this.cs.spaceBeforeSemi() ? 1 : 0);
                }
                this.accept(JavaTokenId.SEMICOLON, new JavaTokenId[0]);
                java.util.List updates = node.getUpdate();
                if (updates != null && !updates.isEmpty()) {
                    boolean first = true;
                    Iterator it = updates.iterator();
                    while (it.hasNext()) {
                        ExpressionStatementTree update = (ExpressionStatementTree)it.next();
                        if (first) {
                            this.wrapTree(this.cs.wrapFor(), alignIndent, this.cs.spaceAfterSemi() ? 1 : 0, (Tree)update);
                        } else {
                            this.scan((Tree)update, p);
                        }
                        first = false;
                        if (!it.hasNext()) continue;
                        this.spaces(this.cs.spaceBeforeComma() ? 1 : 0);
                        this.accept(JavaTokenId.COMMA, new JavaTokenId[0]);
                        this.spaces(this.cs.spaceAfterComma() ? 1 : 0);
                    }
                }
                this.spaces(this.cs.spaceWithinForParens() ? 1 : 0);
                this.accept(JavaTokenId.RPAREN, new JavaTokenId[0]);
            }
            finally {
                this.continuationIndent = old;
            }
            this.lastIndent = this.indent;
            CodeStyle.BracesGenerationStyle redundantForBraces = this.cs.redundantForBraces();
            if (redundantForBraces == CodeStyle.BracesGenerationStyle.GENERATE && ((long)this.startOffset > this.sp.getStartPosition(this.root, (Tree)node) || (long)this.endOffset < this.sp.getEndPosition(this.root, (Tree)node))) {
                redundantForBraces = CodeStyle.BracesGenerationStyle.LEAVE_ALONE;
            }
            this.wrapStatement(this.cs.wrapForStatement(), redundantForBraces, this.cs.spaceBeforeForLeftBrace() ? 1 : 0, node.getStatement());
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitEnhancedForLoop(EnhancedForLoopTree node, Void p) {
            this.accept(JavaTokenId.FOR, new JavaTokenId[0]);
            boolean old = this.continuationIndent;
            try {
                this.continuationIndent = true;
                this.spaces(this.cs.spaceBeforeForParen() ? 1 : 0);
                this.accept(JavaTokenId.LPAREN, new JavaTokenId[0]);
                this.spaces(this.cs.spaceWithinForParens() ? 1 : 0);
                int alignIndent = this.cs.alignMultilineFor() ? this.col : -1;
                this.scan((Tree)node.getVariable(), p);
                this.wrapOperatorAndTree(this.cs.wrapFor(), alignIndent, this.cs.spaceBeforeColon() ? 1 : 0, this.cs.spaceAfterColon() ? 1 : 0, (Tree)node.getExpression());
                this.spaces(this.cs.spaceWithinForParens() ? 1 : 0);
                this.accept(JavaTokenId.RPAREN, new JavaTokenId[0]);
            }
            finally {
                this.continuationIndent = old;
            }
            this.lastIndent = this.indent;
            CodeStyle.BracesGenerationStyle redundantForBraces = this.cs.redundantForBraces();
            if (redundantForBraces == CodeStyle.BracesGenerationStyle.GENERATE && ((long)this.startOffset > this.sp.getStartPosition(this.root, (Tree)node) || (long)this.endOffset < this.sp.getEndPosition(this.root, (Tree)node))) {
                redundantForBraces = CodeStyle.BracesGenerationStyle.LEAVE_ALONE;
            }
            this.wrapStatement(this.cs.wrapForStatement(), redundantForBraces, this.cs.spaceBeforeForLeftBrace() ? 1 : 0, node.getStatement());
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitSynchronized(SynchronizedTree node, Void p) {
            this.accept(JavaTokenId.SYNCHRONIZED, new JavaTokenId[0]);
            boolean old = this.continuationIndent;
            try {
                this.continuationIndent = true;
                this.spaces(this.cs.spaceBeforeSynchronizedParen() ? 1 : 0);
                this.scan((Tree)node.getExpression(), p);
            }
            finally {
                this.continuationIndent = old;
            }
            this.lastIndent = this.indent;
            this.scan((Tree)node.getBlock(), p);
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitSwitch(SwitchTree node, Void p) {
            Diff diff;
            this.accept(JavaTokenId.SWITCH, new JavaTokenId[0]);
            boolean oldContinuationIndent = this.continuationIndent;
            try {
                this.continuationIndent = true;
                this.spaces(this.cs.spaceBeforeSwitchParen() ? 1 : 0);
                this.scan((Tree)node.getExpression(), p);
            }
            finally {
                this.continuationIndent = oldContinuationIndent;
            }
            CodeStyle.BracePlacement bracePlacement = this.cs.getOtherBracePlacement();
            boolean spaceBeforeLeftBrace = this.cs.spaceBeforeSwitchLeftBrace();
            boolean indentCases = this.cs.indentCasesFromSwitch();
            int old = this.lastIndent;
            int halfIndent = this.lastIndent;
            switch (bracePlacement) {
                case SAME_LINE: {
                    this.spaces(spaceBeforeLeftBrace ? 1 : 0, this.tokens.offset() < this.startOffset);
                    this.accept(JavaTokenId.LBRACE, new JavaTokenId[0]);
                    if (!indentCases) break;
                    this.indent = this.lastIndent + this.indentSize;
                    break;
                }
                case NEW_LINE: {
                    this.newline();
                    this.accept(JavaTokenId.LBRACE, new JavaTokenId[0]);
                    if (!indentCases) break;
                    this.indent = this.lastIndent + this.indentSize;
                    break;
                }
                case NEW_LINE_HALF_INDENTED: {
                    int oldLast = this.lastIndent;
                    halfIndent = this.indent = this.lastIndent + (this.indentSize >> 1);
                    this.newline();
                    this.accept(JavaTokenId.LBRACE, new JavaTokenId[0]);
                    if (indentCases) {
                        this.indent = oldLast + this.indentSize;
                        break;
                    }
                    this.indent = old;
                    break;
                }
                case NEW_LINE_INDENTED: {
                    halfIndent = this.indent = this.lastIndent + this.indentSize;
                    this.newline();
                    this.accept(JavaTokenId.LBRACE, new JavaTokenId[0]);
                    if (indentCases) break;
                    this.indent = old;
                }
            }
            for (CaseTree caseTree : node.getCases()) {
                this.newline();
                this.scan((Tree)caseTree, p);
            }
            this.newline();
            this.indent = halfIndent;
            Diff diff2 = diff = this.diffs.isEmpty() ? null : this.diffs.getFirst();
            if (diff != null && diff.end == this.tokens.offset()) {
                String spaces;
                if (diff.text != null) {
                    int idx = diff.text.lastIndexOf(10);
                    if (idx < 0) {
                        diff.text = this.getIndent();
                    } else {
                        diff.text = diff.text.substring(0, idx + 1) + this.getIndent();
                    }
                }
                String string = spaces = diff.text != null ? diff.text : this.getIndent();
                if (spaces.equals(this.fText.substring(diff.start, diff.end))) {
                    this.diffs.removeFirst();
                }
            } else if (this.tokens.movePrevious()) {
                int idx;
                String text;
                if (this.tokens.token().id() == JavaTokenId.WHITESPACE && (idx = (text = this.tokens.token().text().toString()).lastIndexOf(10)) >= 0) {
                    text = text.substring(idx + 1);
                    String ind = this.getIndent();
                    if (!ind.equals(text)) {
                        this.addDiff(new Diff(this.tokens.offset() + idx + 1, this.tokens.offset() + this.tokens.token().length(), ind));
                    }
                }
                this.tokens.moveNext();
            }
            this.accept(JavaTokenId.RBRACE, new JavaTokenId[0]);
            this.indent = this.lastIndent = old;
            return true;
        }

        public Boolean visitCase(CaseTree node, Void p) {
            ExpressionTree exp = node.getExpression();
            if (exp != null) {
                this.accept(JavaTokenId.CASE, new JavaTokenId[0]);
                this.space();
                this.scan((Tree)exp, p);
            } else {
                this.accept(JavaTokenId.DEFAULT, new JavaTokenId[0]);
            }
            this.accept(JavaTokenId.COLON, new JavaTokenId[0]);
            int old = this.indent;
            this.indent = this.lastIndent + this.indentSize;
            boolean first = true;
            for (StatementTree stat : node.getStatements()) {
                if (first) {
                    if (stat.getKind() == Tree.Kind.BLOCK) {
                        this.indent = this.lastIndent;
                    }
                    this.wrapStatement(this.cs.wrapCaseStatements(), CodeStyle.BracesGenerationStyle.LEAVE_ALONE, 1, stat);
                } else {
                    this.newline();
                    this.scan((Tree)stat, p);
                }
                first = false;
            }
            this.indent = old;
            return true;
        }

        public Boolean visitBreak(BreakTree node, Void p) {
            this.accept(JavaTokenId.BREAK, new JavaTokenId[0]);
            Name label = node.getLabel();
            if (label != null) {
                this.space();
                this.accept(JavaTokenId.IDENTIFIER, new JavaTokenId[0]);
            }
            this.accept(JavaTokenId.SEMICOLON, new JavaTokenId[0]);
            return true;
        }

        public Boolean visitContinue(ContinueTree node, Void p) {
            this.accept(JavaTokenId.CONTINUE, new JavaTokenId[0]);
            Name label = node.getLabel();
            if (label != null) {
                this.space();
                this.accept(JavaTokenId.IDENTIFIER, new JavaTokenId[0]);
            }
            this.accept(JavaTokenId.SEMICOLON, new JavaTokenId[0]);
            return true;
        }

        public Boolean visitAssignment(AssignmentTree node, Void p) {
            int alignIndent = this.cs.alignMultilineAssignment() ? this.col : -1;
            boolean b = this.scan((Tree)node.getVariable(), p);
            if (b || this.getCurrentPath().getParentPath().getLeaf().getKind() != Tree.Kind.ANNOTATION) {
                boolean spaceAroundAssignOps;
                boolean bl = spaceAroundAssignOps = this.insideAnnotation ? this.cs.spaceAroundAnnotationValueAssignOps() : this.cs.spaceAroundAssignOps();
                if (this.cs.wrapAfterAssignOps()) {
                    ExpressionTree expr;
                    boolean containedNewLine = this.spaces(spaceAroundAssignOps ? 1 : 0, false);
                    if (this.accept(JavaTokenId.EQ, new JavaTokenId[0]) == JavaTokenId.EQ && containedNewLine) {
                        this.newline();
                    }
                    if ((expr = node.getExpression()).getKind() == Tree.Kind.NEW_ARRAY && ((NewArrayTree)expr).getType() == null) {
                        if (this.cs.getOtherBracePlacement() == CodeStyle.BracePlacement.SAME_LINE) {
                            this.spaces(spaceAroundAssignOps ? 1 : 0);
                        }
                        this.scan((Tree)expr, p);
                    } else if (this.wrapAnnotation && expr.getKind() == Tree.Kind.ANNOTATION) {
                        this.wrapTree(CodeStyle.WrapStyle.WRAP_ALWAYS, alignIndent, spaceAroundAssignOps ? 1 : 0, (Tree)expr);
                    } else {
                        this.wrapTree(this.cs.wrapAssignOps(), alignIndent, spaceAroundAssignOps ? 1 : 0, (Tree)expr);
                    }
                } else {
                    this.wrapOperatorAndTree(this.cs.wrapAssignOps(), alignIndent, this.cs.spaceAroundAssignOps() ? 1 : 0, (Tree)node.getExpression());
                }
            } else {
                this.scan((Tree)node.getExpression(), p);
            }
            return true;
        }

        public Boolean visitCompoundAssignment(CompoundAssignmentTree node, Void p) {
            int alignIndent = this.cs.alignMultilineAssignment() ? this.col : -1;
            this.scan((Tree)node.getVariable(), p);
            if (this.cs.wrapAfterAssignOps()) {
                boolean containedNewLine = this.spaces(this.cs.spaceAroundAssignOps() ? 1 : 0, false);
                if ("operator".equals(((JavaTokenId)this.tokens.token().id()).primaryCategory())) {
                    this.col += this.tokens.token().length();
                    this.lastBlankLines = -1;
                    this.lastBlankLinesTokenIndex = -1;
                    this.lastBlankLinesDiff = null;
                    this.tokens.moveNext();
                    if (containedNewLine) {
                        this.newline();
                    }
                }
                this.wrapTree(this.cs.wrapAssignOps(), alignIndent, this.cs.spaceAroundAssignOps() ? 1 : 0, (Tree)node.getExpression());
            } else {
                this.wrapOperatorAndTree(this.cs.wrapAssignOps(), alignIndent, this.cs.spaceAroundAssignOps() ? 1 : 0, (Tree)node.getExpression());
            }
            return true;
        }

        public Boolean visitPrimitiveType(PrimitiveTypeTree node, Void p) {
            switch (node.getPrimitiveTypeKind()) {
                case BOOLEAN: {
                    this.accept(JavaTokenId.BOOLEAN, new JavaTokenId[0]);
                    break;
                }
                case BYTE: {
                    this.accept(JavaTokenId.BYTE, new JavaTokenId[0]);
                    break;
                }
                case CHAR: {
                    this.accept(JavaTokenId.CHAR, new JavaTokenId[0]);
                    break;
                }
                case DOUBLE: {
                    this.accept(JavaTokenId.DOUBLE, new JavaTokenId[0]);
                    break;
                }
                case FLOAT: {
                    this.accept(JavaTokenId.FLOAT, new JavaTokenId[0]);
                    break;
                }
                case INT: {
                    this.accept(JavaTokenId.INT, new JavaTokenId[0]);
                    break;
                }
                case LONG: {
                    this.accept(JavaTokenId.LONG, new JavaTokenId[0]);
                    break;
                }
                case SHORT: {
                    this.accept(JavaTokenId.SHORT, new JavaTokenId[0]);
                    break;
                }
                case VOID: {
                    this.accept(JavaTokenId.VOID, new JavaTokenId[0]);
                }
            }
            return true;
        }

        public Boolean visitArrayType(ArrayTypeTree node, Void p) {
            boolean ret = this.scan(node.getType(), p);
            int index = this.tokens.index();
            int c = this.col;
            Diff d = this.diffs.isEmpty() ? null : this.diffs.getFirst();
            JavaTokenId id = this.accept(JavaTokenId.LBRACKET, JavaTokenId.ELLIPSIS, JavaTokenId.IDENTIFIER);
            if (id == JavaTokenId.ELLIPSIS) {
                return ret;
            }
            if (id != JavaTokenId.IDENTIFIER) {
                this.accept(JavaTokenId.RBRACKET, new JavaTokenId[0]);
                return ret;
            }
            this.rollback(index, c, d);
            this.spaces(1, this.fieldGroup);
            this.accept(JavaTokenId.IDENTIFIER, new JavaTokenId[0]);
            this.accept(JavaTokenId.LBRACKET, new JavaTokenId[0]);
            this.accept(JavaTokenId.RBRACKET, new JavaTokenId[0]);
            return false;
        }

        public Boolean visitArrayAccess(ArrayAccessTree node, Void p) {
            this.scan((Tree)node.getExpression(), p);
            this.accept(JavaTokenId.LBRACKET, new JavaTokenId[0]);
            this.spaces(this.cs.spaceWithinArrayIndexBrackets() ? 1 : 0);
            this.scan((Tree)node.getIndex(), p);
            this.spaces(this.cs.spaceWithinArrayIndexBrackets() ? 1 : 0);
            this.accept(JavaTokenId.RBRACKET, new JavaTokenId[0]);
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitNewArray(NewArrayTree node, Void p) {
            Tree type = node.getType();
            java.util.List inits = node.getInitializers();
            if (type != null) {
                int n;
                this.accept(JavaTokenId.NEW, new JavaTokenId[0]);
                this.space();
                int n2 = n = inits != null ? 1 : 0;
                while (type.getKind() == Tree.Kind.ARRAY_TYPE) {
                    ++n;
                    type = ((ArrayTypeTree)type).getType();
                }
                this.scan(type, p);
                for (ExpressionTree dim : node.getDimensions()) {
                    this.accept(JavaTokenId.LBRACKET, new JavaTokenId[0]);
                    this.spaces(this.cs.spaceWithinArrayInitBrackets() ? 1 : 0);
                    this.scan((Tree)dim, p);
                    this.spaces(this.cs.spaceWithinArrayInitBrackets() ? 1 : 0);
                    this.accept(JavaTokenId.RBRACKET, new JavaTokenId[0]);
                }
                while (--n >= 0) {
                    this.accept(JavaTokenId.LBRACKET, new JavaTokenId[0]);
                    this.accept(JavaTokenId.RBRACKET, new JavaTokenId[0]);
                }
            }
            if (inits != null) {
                CodeStyle.BracePlacement bracePlacement = this.cs.getOtherBracePlacement();
                boolean spaceBeforeLeftBrace = this.cs.spaceBeforeArrayInitLeftBrace();
                boolean oldContinuationIndent = this.continuationIndent;
                try {
                    boolean afterNewline;
                    this.continuationIndent = this.isLastIndentContinuation;
                    int old = this.lastIndent;
                    int halfIndent = this.lastIndent;
                    switch (bracePlacement) {
                        case SAME_LINE: {
                            if (type != null) {
                                this.spaces(spaceBeforeLeftBrace ? 1 : 0, this.tokens.offset() < this.startOffset);
                            }
                            this.accept(JavaTokenId.LBRACE, new JavaTokenId[0]);
                            this.indent = this.lastIndent + this.indentSize;
                            break;
                        }
                        case NEW_LINE: {
                            this.newline();
                            this.accept(JavaTokenId.LBRACE, new JavaTokenId[0]);
                            this.indent = this.lastIndent + this.indentSize;
                            break;
                        }
                        case NEW_LINE_HALF_INDENTED: {
                            int oldLast = this.lastIndent;
                            halfIndent = this.indent = this.lastIndent + (this.indentSize >> 1);
                            this.newline();
                            this.accept(JavaTokenId.LBRACE, new JavaTokenId[0]);
                            this.indent = oldLast + this.indentSize;
                            break;
                        }
                        case NEW_LINE_INDENTED: {
                            halfIndent = this.indent = this.lastIndent + this.indentSize;
                            this.newline();
                            this.accept(JavaTokenId.LBRACE, new JavaTokenId[0]);
                        }
                    }
                    boolean bl = afterNewline = bracePlacement != CodeStyle.BracePlacement.SAME_LINE;
                    if (!inits.isEmpty()) {
                        Diff d;
                        if (afterNewline) {
                            this.newline();
                        } else {
                            this.spaces(this.cs.spaceWithinBraces() ? 1 : 0, true);
                        }
                        CodeStyle.WrapStyle ws = this.insideAnnotation && ((ExpressionTree)inits.get(0)).getKind() == Tree.Kind.ANNOTATION ? this.cs.wrapAnnotations() : this.cs.wrapArrayInit();
                        this.wrapList(ws, this.cs.alignMultilineArrayInit(), false, JavaTokenId.COMMA, inits);
                        if (this.tokens.token().text().toString().indexOf(10) >= 0) {
                            afterNewline = true;
                        }
                        int index = this.tokens.index();
                        int c = this.col;
                        Diff diff = d = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                        if (this.accept(JavaTokenId.COMMA, new JavaTokenId[0]) == null) {
                            this.rollback(index, c, d);
                        }
                        this.indent = this.lastIndent - this.indentSize;
                        if (afterNewline) {
                            this.newline();
                        } else {
                            this.spaces(this.cs.spaceWithinBraces() ? 1 : 0);
                        }
                    } else if (afterNewline) {
                        this.newline();
                    }
                    this.indent = halfIndent;
                    if (afterNewline) {
                        Diff diff;
                        Diff diff2 = diff = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                        if (diff != null && diff.end == this.tokens.offset()) {
                            String spaces;
                            if (diff.text != null) {
                                int idx = diff.text.lastIndexOf(10);
                                if (idx < 0) {
                                    diff.text = this.getIndent();
                                } else {
                                    diff.text = diff.text.substring(0, idx + 1) + this.getIndent();
                                }
                            }
                            String string = spaces = diff.text != null ? diff.text : this.getIndent();
                            if (spaces.equals(this.fText.substring(diff.start, diff.end))) {
                                this.diffs.removeFirst();
                            }
                        } else if (this.tokens.movePrevious()) {
                            String text;
                            int idx;
                            if (this.tokens.token().id() == JavaTokenId.WHITESPACE && (idx = (text = this.tokens.token().text().toString()).lastIndexOf(10)) >= 0) {
                                text = text.substring(idx + 1);
                                String ind = this.getIndent();
                                if (!ind.equals(text)) {
                                    this.addDiff(new Diff(this.tokens.offset() + idx + 1, this.tokens.offset() + this.tokens.token().length(), ind));
                                }
                            }
                            this.tokens.moveNext();
                        }
                    }
                    this.accept(JavaTokenId.RBRACE, new JavaTokenId[0]);
                    this.indent = this.lastIndent = old;
                }
                finally {
                    this.continuationIndent = oldContinuationIndent;
                }
            }
            return true;
        }

        public Boolean visitIdentifier(IdentifierTree node, Void p) {
            this.accept(JavaTokenId.IDENTIFIER, JavaTokenId.THIS, JavaTokenId.SUPER);
            return true;
        }

        public Boolean visitUnary(UnaryTree node, Void p) {
            JavaTokenId id = (JavaTokenId)this.tokens.token().id();
            if ("operator".equals(id.primaryCategory())) {
                this.spaces(this.cs.spaceAroundUnaryOps() ? 1 : 0);
                this.col += this.tokens.token().length();
                this.lastBlankLines = -1;
                this.lastBlankLinesTokenIndex = -1;
                this.lastBlankLinesDiff = null;
                this.tokens.moveNext();
                int index = this.tokens.index();
                int c = this.col;
                Diff d = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                this.spaces(this.cs.spaceAroundUnaryOps() ? 1 : 0);
                if (this.tokens.token().id() == id) {
                    this.rollback(index, c, d);
                    this.space();
                }
                this.scan((Tree)node.getExpression(), p);
            } else {
                this.scan((Tree)node.getExpression(), p);
                this.spaces(this.cs.spaceAroundUnaryOps() ? 1 : 0);
                this.col += this.tokens.token().length();
                this.lastBlankLines = -1;
                this.lastBlankLinesTokenIndex = -1;
                this.lastBlankLinesDiff = null;
                this.tokens.moveNext();
                this.spaces(this.cs.spaceAroundUnaryOps() ? 1 : 0);
            }
            return true;
        }

        public Boolean visitBinary(BinaryTree node, Void p) {
            int alignIndent = this.cs.alignMultilineBinaryOp() ? this.col : -1;
            this.scan((Tree)node.getLeftOperand(), p);
            if (this.cs.wrapAfterBinaryOps()) {
                boolean containedNewLine = this.spaces(this.cs.spaceAroundBinaryOps() ? 1 : 0, false);
                if ("operator".equals(((JavaTokenId)this.tokens.token().id()).primaryCategory())) {
                    this.col += this.tokens.token().length();
                    this.lastBlankLines = -1;
                    this.lastBlankLinesTokenIndex = -1;
                    this.tokens.moveNext();
                    if (containedNewLine) {
                        this.newline();
                    }
                }
                this.wrapTree(this.cs.wrapBinaryOps(), alignIndent, this.cs.spaceAroundBinaryOps() ? 1 : 0, (Tree)node.getRightOperand());
            } else {
                this.wrapOperatorAndTree(this.cs.wrapBinaryOps(), alignIndent, this.cs.spaceAroundBinaryOps() ? 1 : 0, (Tree)node.getRightOperand());
            }
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitConditionalExpression(ConditionalExpressionTree node, Void p) {
            block7 : {
                int alignIndent = this.cs.alignMultilineTernaryOp() ? this.col : -1;
                this.scan((Tree)node.getCondition(), p);
                boolean old = this.continuationIndent;
                int oldIndent = this.indent;
                try {
                    if (this.isLastIndentContinuation) {
                        this.indent = this.indent();
                    }
                    if (this.cs.wrapAfterTernaryOps()) {
                        boolean containedNewLine = this.spaces(this.cs.spaceAroundTernaryOps() ? 1 : 0, false);
                        this.accept(JavaTokenId.QUESTION, new JavaTokenId[0]);
                        if (containedNewLine) {
                            this.newline();
                        }
                        this.wrapTree(this.cs.wrapTernaryOps(), alignIndent, this.cs.spaceAroundTernaryOps() ? 1 : 0, (Tree)node.getTrueExpression());
                        containedNewLine = this.spaces(this.cs.spaceAroundTernaryOps() ? 1 : 0, false);
                        this.accept(JavaTokenId.COLON, new JavaTokenId[0]);
                        if (containedNewLine) {
                            this.newline();
                        }
                        this.wrapTree(this.cs.wrapTernaryOps(), alignIndent, this.cs.spaceAroundTernaryOps() ? 1 : 0, (Tree)node.getFalseExpression());
                        break block7;
                    }
                    this.wrapOperatorAndTree(this.cs.wrapTernaryOps(), alignIndent, this.cs.spaceAroundTernaryOps() ? 1 : 0, (Tree)node.getTrueExpression());
                    this.wrapOperatorAndTree(this.cs.wrapTernaryOps(), alignIndent, this.cs.spaceAroundTernaryOps() ? 1 : 0, (Tree)node.getFalseExpression());
                }
                finally {
                    this.indent = oldIndent;
                    this.continuationIndent = old;
                }
            }
            return true;
        }

        public Boolean visitEmptyStatement(EmptyStatementTree node, Void p) {
            this.accept(JavaTokenId.SEMICOLON, new JavaTokenId[0]);
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitExpressionStatement(ExpressionStatementTree node, Void p) {
            boolean old = this.continuationIndent;
            try {
                this.continuationIndent = true;
                this.scan((Tree)node.getExpression(), p);
                this.accept(JavaTokenId.SEMICOLON, new JavaTokenId[0]);
            }
            finally {
                this.continuationIndent = old;
            }
            return true;
        }

        public Boolean visitInstanceOf(InstanceOfTree node, Void p) {
            this.scan((Tree)node.getExpression(), p);
            this.space();
            this.accept(JavaTokenId.INSTANCEOF, new JavaTokenId[0]);
            this.space();
            this.scan(node.getType(), p);
            return true;
        }

        public Boolean visitLabeledStatement(LabeledStatementTree node, Void p) {
            int cnt;
            if (!"<error>".contentEquals(node.getLabel())) {
                this.accept(JavaTokenId.IDENTIFIER, new JavaTokenId[0]);
            }
            this.accept(JavaTokenId.COLON, new JavaTokenId[0]);
            int old = this.indent;
            if (!this.cs.absoluteLabelIndent()) {
                this.indent += this.cs.getLabelIndent();
            }
            if ((cnt = this.indent() - this.col) < 0) {
                this.newline();
            } else {
                this.spaces(cnt, true);
            }
            this.scan((Tree)node.getStatement(), p);
            this.indent = old;
            return true;
        }

        public Boolean visitTypeCast(TypeCastTree node, Void p) {
            this.accept(JavaTokenId.LPAREN, new JavaTokenId[0]);
            boolean spaceWithinParens = this.cs.spaceWithinTypeCastParens();
            this.spaces(spaceWithinParens ? 1 : 0);
            this.scan(node.getType(), p);
            this.spaces(spaceWithinParens ? 1 : 0);
            this.accept(JavaTokenId.RPAREN, new JavaTokenId[0]);
            this.spaces(this.cs.spaceAfterTypeCast() ? 1 : 0);
            this.scan((Tree)node.getExpression(), p);
            return true;
        }

        public Boolean visitIntersectionType(IntersectionTypeTree node, Void p) {
            Iterator it = node.getBounds().iterator();
            while (it.hasNext()) {
                Tree bound = (Tree)it.next();
                this.scan(bound, p);
                if (!it.hasNext()) continue;
                this.space();
                this.accept(JavaTokenId.AMP, new JavaTokenId[0]);
                this.space();
            }
            return true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Boolean visitParenthesized(ParenthesizedTree node, Void p) {
            this.accept(JavaTokenId.LPAREN, new JavaTokenId[0]);
            int old = this.indent;
            boolean oldContinuationIndent = this.continuationIndent;
            try {
                boolean spaceWithinParens;
                switch (this.getCurrentPath().getParentPath().getLeaf().getKind()) {
                    case IF: {
                        spaceWithinParens = this.cs.spaceWithinIfParens();
                        break;
                    }
                    case FOR_LOOP: {
                        spaceWithinParens = this.cs.spaceWithinForParens();
                        break;
                    }
                    case WHILE_LOOP: 
                    case DO_WHILE_LOOP: {
                        spaceWithinParens = this.cs.spaceWithinWhileParens();
                        break;
                    }
                    case SWITCH: {
                        spaceWithinParens = this.cs.spaceWithinSwitchParens();
                        break;
                    }
                    case SYNCHRONIZED: {
                        spaceWithinParens = this.cs.spaceWithinSynchronizedParens();
                        break;
                    }
                    default: {
                        spaceWithinParens = this.cs.spaceWithinParens();
                        if (!this.cs.alignMultilineParenthesized()) break;
                        this.indent = this.col;
                        this.continuationIndent = false;
                    }
                }
                this.spaces(spaceWithinParens ? 1 : 0);
                this.scan((Tree)node.getExpression(), p);
                this.spaces(spaceWithinParens ? 1 : 0);
            }
            finally {
                this.indent = old;
                this.continuationIndent = oldContinuationIndent;
            }
            this.accept(JavaTokenId.RPAREN, new JavaTokenId[0]);
            return true;
        }

        public Boolean visitLiteral(LiteralTree node, Void p) {
            do {
                this.col += this.tokens.token().length();
            } while (this.tokens.moveNext() && this.tokens.offset() < this.endPos);
            this.lastBlankLines = -1;
            this.lastBlankLinesTokenIndex = -1;
            this.lastBlankLinesDiff = null;
            return true;
        }

        public Boolean visitErroneous(ErroneousTree node, Void p) {
            for (Tree tree : node.getErrorTrees()) {
                int pos = (int)this.sp.getStartPosition(this.getCurrentPath().getCompilationUnit(), tree);
                while (this.tokens.offset() < pos) {
                    this.col += this.tokens.token().length();
                    if (this.tokens.moveNext()) continue;
                }
                this.lastBlankLines = -1;
                this.lastBlankLinesTokenIndex = -1;
                this.lastBlankLinesDiff = null;
                this.scan(tree, p);
            }
            while (this.tokens.offset() < this.endPos) {
                int len = this.tokens.token().length();
                if (this.tokens.token().id() == JavaTokenId.WHITESPACE && this.tokens.offset() + len >= this.endPos) break;
                this.col += len;
                if (this.tokens.moveNext()) continue;
            }
            this.lastBlankLines = -1;
            this.lastBlankLinesTokenIndex = -1;
            this.lastBlankLinesDiff = null;
            return true;
        }

        public Boolean visitOther(Tree node, Void p) {
            do {
                this.col += this.tokens.token().length();
            } while (this.tokens.moveNext() && this.tokens.offset() < this.endPos);
            this.lastBlankLines = -1;
            this.lastBlankLinesTokenIndex = -1;
            this.lastBlankLinesDiff = null;
            return true;
        }

        private /* varargs */ JavaTokenId accept(JavaTokenId first, JavaTokenId ... rest) {
            if (this.checkWrap != null && this.col > this.rightMargin && this.checkWrap.pos >= this.lastNewLineOffset) {
                throw this.checkWrap;
            }
            if (this.tokens.offset() >= this.endPos || this.eof) {
                return null;
            }
            this.lastBlankLines = -1;
            this.lastBlankLinesTokenIndex = -1;
            this.lastBlankLinesDiff = null;
            EnumSet<JavaTokenId[]> tokenIds = EnumSet.of(first, rest);
            Token lastWSToken = null;
            int after = 0;
            do {
                String spaces;
                if (this.tokens.offset() >= this.endPos || this.eof) {
                    if (lastWSToken != null) {
                        this.lastBlankLines = 0;
                        this.lastBlankLinesTokenIndex = this.tokens.index() - 1;
                        this.lastBlankLinesDiff = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                    }
                    return null;
                }
                JavaTokenId id = (JavaTokenId)this.tokens.token().id();
                if (tokenIds.contains((Object)id)) {
                    String string = after == 1 ? this.getIndent() : (spaces = after == 2 ? this.getNewlines(1) + this.getIndent() : null);
                    if (lastWSToken != null) {
                        if (spaces == null || !spaces.contentEquals(lastWSToken.text())) {
                            this.addDiff(new Diff(this.tokens.offset() - lastWSToken.length(), this.tokens.offset(), spaces));
                        }
                    } else if (spaces != null && spaces.length() > 0) {
                        this.addDiff(new Diff(this.tokens.offset(), this.tokens.offset(), spaces));
                    }
                    if (after > 0) {
                        this.col = this.indent();
                    }
                    this.col += this.tokens.token().length();
                    this.bof = false;
                    return this.tokens.moveNext() ? id : null;
                }
                switch (id) {
                    String tokenText;
                    int idx;
                    case WHITESPACE: {
                        lastWSToken = this.tokens.token();
                        break;
                    }
                    case LINE_COMMENT: {
                        if (lastWSToken != null) {
                            String string = after == 1 ? this.getIndent() : (spaces = after == 2 ? this.getNewlines(1) + this.getIndent() : " ");
                            if (!spaces.contentEquals(lastWSToken.text())) {
                                this.addDiff(new Diff(this.tokens.offset() - lastWSToken.length(), this.tokens.offset(), spaces));
                            }
                            lastWSToken = null;
                        } else {
                            String string = after == 1 ? this.getIndent() : (spaces = after == 2 ? this.getNewlines(1) + this.getIndent() : null);
                            if (spaces != null && spaces.length() > 0) {
                                this.addDiff(new Diff(this.tokens.offset(), this.tokens.offset(), spaces));
                            }
                        }
                        this.col = 0;
                        after = 1;
                        break;
                    }
                    case JAVADOC_COMMENT: {
                        if (lastWSToken != null) {
                            String string = after == 1 ? this.getIndent() : (spaces = after == 2 ? this.getNewlines(1) + this.getIndent() : " ");
                            if (!spaces.contentEquals(lastWSToken.text())) {
                                this.addDiff(new Diff(this.tokens.offset() - lastWSToken.length(), this.tokens.offset(), spaces));
                            }
                            lastWSToken = null;
                            this.col = after > 0 ? this.indent() : ++this.col;
                        } else {
                            String string = after == 1 ? this.getIndent() : (spaces = after == 2 ? this.getNewlines(1) + this.getIndent() : null);
                            if (spaces != null && spaces.length() > 0) {
                                this.addDiff(new Diff(this.tokens.offset(), this.tokens.offset(), spaces));
                            }
                            if (after > 0) {
                                this.col = this.indent();
                            }
                        }
                        tokenText = this.tokens.token().text().toString();
                        idx = tokenText.lastIndexOf(10);
                        if (idx >= 0) {
                            tokenText = tokenText.substring(idx + 1);
                        }
                        this.col += this.getCol(tokenText);
                        this.reformatComment();
                        after = 2;
                        break;
                    }
                    case BLOCK_COMMENT: {
                        String spaces2;
                        if (lastWSToken != null) {
                            String string = after == 1 ? this.getIndent() : (spaces2 = after == 2 ? this.getNewlines(1) + this.getIndent() : " ");
                            if (!spaces2.contentEquals(lastWSToken.text())) {
                                this.addDiff(new Diff(this.tokens.offset() - lastWSToken.length(), this.tokens.offset(), spaces2));
                            }
                            lastWSToken = null;
                            this.col = after > 0 ? this.indent() : ++this.col;
                        } else {
                            String string = after == 1 ? this.getIndent() : (spaces2 = after == 2 ? this.getNewlines(1) + this.getIndent() : null);
                            if (spaces2 != null && spaces2.length() > 0) {
                                this.addDiff(new Diff(this.tokens.offset(), this.tokens.offset(), spaces2));
                            }
                            if (after > 0) {
                                this.col = this.indent();
                            }
                        }
                        tokenText = this.tokens.token().text().toString();
                        idx = tokenText.lastIndexOf(10);
                        if (idx >= 0) {
                            tokenText = tokenText.substring(idx + 1);
                        }
                        this.col += this.getCol(tokenText);
                        this.reformatComment();
                        after = 0;
                        break;
                    }
                    default: {
                        if (lastWSToken != null) {
                            this.lastBlankLines = -1;
                            this.lastBlankLinesTokenIndex = this.tokens.index() - 1;
                            this.lastBlankLinesDiff = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                        }
                        this.bof = false;
                        return null;
                    }
                }
            } while (this.tokens.moveNext());
            this.eof = true;
            return null;
        }

        private void space() {
            this.spaces(1);
        }

        private void spaces(int count) {
            this.spaces(count, false);
        }

        private boolean spaces(int count, boolean preserveNewline) {
            if (this.checkWrap != null && this.col > this.rightMargin && this.checkWrap.pos >= this.lastNewLineOffset) {
                throw this.checkWrap;
            }
            Token lastWSToken = null;
            boolean containedNewLine = false;
            int after = 0;
            do {
                if (this.tokens.offset() >= this.endPos) {
                    if (lastWSToken != null) {
                        this.tokens.movePrevious();
                    }
                    return containedNewLine;
                }
                switch ((JavaTokenId)this.tokens.token().id()) {
                    String tokenText;
                    String text;
                    String text2;
                    int idx;
                    String spaces;
                    case WHITESPACE: {
                        lastWSToken = this.tokens.token();
                        break;
                    }
                    case LINE_COMMENT: {
                        if (lastWSToken != null) {
                            spaces = after == 1 ? this.getIndent() : (after == 2 ? this.getNewlines(1) + this.getIndent() : " ");
                            text = lastWSToken.text().toString();
                            idx = text.lastIndexOf(10);
                            if (idx >= 0) {
                                containedNewLine = true;
                                if (preserveNewline) {
                                    spaces = this.getNewlines(1) + this.getIndent();
                                    this.lastBlankLines = 1;
                                    this.lastBlankLinesTokenIndex = this.tokens.index();
                                    this.lastBlankLinesDiff = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                                    this.lastNewLineOffset = this.tokens.offset();
                                }
                            }
                            if (!spaces.contentEquals(lastWSToken.text())) {
                                this.addDiff(new Diff(this.tokens.offset() - lastWSToken.length(), this.tokens.offset(), spaces));
                            }
                            lastWSToken = null;
                        } else {
                            String string = after == 1 ? this.getIndent() : (spaces = after == 2 ? this.getNewlines(1) + this.getIndent() : null);
                            if (spaces != null && spaces.length() > 0) {
                                this.addDiff(new Diff(this.tokens.offset(), this.tokens.offset(), spaces));
                            }
                        }
                        this.col = 0;
                        after = 1;
                        break;
                    }
                    case JAVADOC_COMMENT: {
                        if (lastWSToken != null) {
                            spaces = after == 1 ? this.getIndent() : (after == 2 ? this.getNewlines(1) + this.getIndent() : " ");
                            text = lastWSToken.text().toString();
                            idx = text.lastIndexOf(10);
                            if (idx >= 0) {
                                containedNewLine = true;
                                if (preserveNewline) {
                                    spaces = this.getNewlines(1) + this.getIndent();
                                    after = 3;
                                    this.lastBlankLines = 1;
                                    this.lastBlankLinesTokenIndex = this.tokens.index();
                                    this.lastBlankLinesDiff = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                                    this.lastNewLineOffset = this.tokens.offset();
                                }
                            }
                            if (!spaces.contentEquals(lastWSToken.text())) {
                                this.addDiff(new Diff(this.tokens.offset() - lastWSToken.length(), this.tokens.offset(), spaces));
                            }
                            lastWSToken = null;
                            this.col = after > 0 ? this.indent() : ++this.col;
                        } else {
                            String string = after == 1 ? this.getIndent() : (spaces = after == 2 ? this.getNewlines(1) + this.getIndent() : null);
                            if (spaces != null && spaces.length() > 0) {
                                this.addDiff(new Diff(this.tokens.offset(), this.tokens.offset(), spaces));
                            }
                            if (after > 0) {
                                this.col = this.indent();
                            }
                        }
                        tokenText = this.tokens.token().text().toString();
                        int idx2 = tokenText.lastIndexOf(10);
                        if (idx2 >= 0) {
                            tokenText = tokenText.substring(idx2 + 1);
                        }
                        this.col += this.getCol(tokenText);
                        this.reformatComment();
                        after = 2;
                        break;
                    }
                    case BLOCK_COMMENT: {
                        String spaces2;
                        if (lastWSToken != null) {
                            spaces2 = after == 1 ? this.getIndent() : (after == 2 ? this.getNewlines(1) + this.getIndent() : " ");
                            text2 = lastWSToken.text().toString();
                            int idx2 = text2.lastIndexOf(10);
                            if (idx2 >= 0) {
                                containedNewLine = true;
                                if (preserveNewline) {
                                    spaces2 = this.getNewlines(1) + this.getIndent();
                                    after = 3;
                                    this.lastBlankLines = 1;
                                    this.lastBlankLinesTokenIndex = this.tokens.index();
                                    this.lastBlankLinesDiff = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                                    this.lastNewLineOffset = this.tokens.offset();
                                }
                            }
                            if (!spaces2.contentEquals(lastWSToken.text())) {
                                this.addDiff(new Diff(this.tokens.offset() - lastWSToken.length(), this.tokens.offset(), spaces2));
                            }
                            lastWSToken = null;
                            this.col = after > 0 ? this.indent() : ++this.col;
                        } else {
                            String string = after == 1 ? this.getIndent() : (spaces2 = after == 2 ? this.getNewlines(1) + this.getIndent() : null);
                            if (spaces2 != null && spaces2.length() > 0) {
                                this.addDiff(new Diff(this.tokens.offset(), this.tokens.offset(), spaces2));
                            }
                            if (after > 0) {
                                this.col = this.indent();
                            }
                        }
                        tokenText = this.tokens.token().text().toString();
                        int idx3 = tokenText.lastIndexOf(10);
                        if (idx3 >= 0) {
                            tokenText = tokenText.substring(idx3 + 1);
                        }
                        this.col += this.getCol(tokenText);
                        this.reformatComment();
                        after = 0;
                        break;
                    }
                    default: {
                        String spaces3;
                        String string = after == 1 ? this.getIndent() : (spaces3 = after == 2 ? this.getNewlines(1) + this.getIndent() : this.getSpaces(count));
                        if (lastWSToken != null) {
                            text2 = lastWSToken.text().toString();
                            int idx3 = text2.lastIndexOf(10);
                            if (idx3 >= 0) {
                                containedNewLine = true;
                                if (preserveNewline) {
                                    spaces3 = this.getNewlines(1) + this.getIndent();
                                    after = 3;
                                    this.lastBlankLines = 1;
                                    this.lastBlankLinesTokenIndex = this.tokens.index();
                                    this.lastBlankLinesDiff = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                                    this.lastNewLineOffset = this.tokens.offset();
                                }
                            }
                            if (!spaces3.contentEquals(lastWSToken.text())) {
                                this.addDiff(new Diff(this.tokens.offset() - lastWSToken.length(), this.tokens.offset(), spaces3));
                            }
                        } else if (spaces3.length() > 0) {
                            this.addDiff(new Diff(this.tokens.offset(), this.tokens.offset(), spaces3));
                        }
                        this.col = after > 0 ? this.indent() : (this.col += count);
                        return containedNewLine;
                    }
                }
            } while (this.tokens.moveNext());
            return containedNewLine;
        }

        private void newline() {
            this.blankLines(0);
        }

        private void blankLines(int count) {
            int maxCount;
            if (this.checkWrap != null && this.col > this.rightMargin && this.checkWrap.pos >= this.lastNewLineOffset) {
                throw this.checkWrap;
            }
            int n = maxCount = this.bof ? 0 : this.maxPreservedBlankLines;
            if (maxCount < count) {
                count = maxCount;
            }
            if (!this.bof && this.templateEdit && maxCount < 1) {
                maxCount = 1;
            }
            if (this.lastBlankLinesTokenIndex < 0) {
                this.lastBlankLines = count;
                this.lastBlankLinesTokenIndex = this.tokens.index();
                this.lastBlankLinesDiff = this.diffs.isEmpty() ? null : this.diffs.getFirst();
            } else if (this.lastBlankLines < count) {
                this.lastBlankLines = count;
                this.rollback(this.lastBlankLinesTokenIndex, this.lastBlankLinesTokenIndex, this.lastBlankLinesDiff);
            } else {
                Diff diff;
                Diff diff2 = diff = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                if (diff != null && diff.end == this.tokens.offset()) {
                    String spaces;
                    if (diff.text != null) {
                        int idx = diff.text.lastIndexOf(10);
                        if (idx < 0) {
                            diff.text = this.getIndent();
                        } else {
                            diff.text = diff.text.substring(0, idx + 1) + this.getIndent();
                        }
                    }
                    String string = spaces = diff.text != null ? diff.text : this.getIndent();
                    if (spaces.equals(this.fText.substring(diff.start, diff.end))) {
                        this.diffs.removeFirst();
                    }
                } else if (this.tokens.movePrevious()) {
                    if (this.tokens.token().id() == JavaTokenId.WHITESPACE) {
                        String text = this.tokens.token().text().toString();
                        int idx = text.lastIndexOf(10);
                        if (idx >= 0) {
                            text = text.substring(idx + 1);
                            String ind = this.getIndent();
                            if (!ind.equals(text)) {
                                this.addDiff(new Diff(this.tokens.offset() + idx + 1, this.tokens.offset() + this.tokens.token().length(), ind));
                            }
                        } else if (this.tokens.movePrevious()) {
                            if (this.tokens.token().id() == JavaTokenId.LINE_COMMENT) {
                                this.tokens.moveNext();
                                String ind = this.getIndent();
                                if (!ind.equals(text)) {
                                    this.addDiff(new Diff(this.tokens.offset(), this.tokens.offset() + this.tokens.token().length(), ind));
                                }
                            } else {
                                this.tokens.moveNext();
                            }
                        }
                    }
                    this.tokens.moveNext();
                }
                this.col = this.indent();
                return;
            }
            this.lastNewLineOffset = this.tokens.offset();
            this.checkWrap = null;
            Token lastToken = null;
            int after = 0;
            Diff pendingDiff = null;
            String pendingText = null;
            int beforeCnt = 0;
            do {
                if (this.tokens.offset() >= this.endPos) {
                    return;
                }
                switch ((JavaTokenId)this.tokens.token().id()) {
                    String text;
                    String indent;
                    String ind;
                    int offset;
                    int lastIdx;
                    int idx;
                    case WHITESPACE: {
                        lastToken = this.tokens.token();
                        break;
                    }
                    case BLOCK_COMMENT: {
                        if (this.tokens.index() > 1 && after != 1) {
                            if (maxCount < Integer.MAX_VALUE) {
                                ++maxCount;
                            }
                            ++count;
                        }
                        if (lastToken != null) {
                            offset = this.tokens.offset() - lastToken.length();
                            text = lastToken.text().toString();
                            idx = 0;
                            lastIdx = 0;
                            while (maxCount > 0 && (idx = text.indexOf(10, lastIdx)) >= 0) {
                                if (idx > lastIdx) {
                                    this.addDiff(new Diff(offset + lastIdx, offset + idx, null));
                                }
                                lastIdx = idx + 1;
                                --maxCount;
                                --count;
                            }
                            idx = text.lastIndexOf(10);
                            if (idx >= 0) {
                                if (idx > lastIdx) {
                                    this.addDiff(new Diff(offset + lastIdx, offset + idx + 1, null));
                                }
                                lastIdx = idx + 1;
                            }
                            if (lastIdx > 0) {
                                ind = this.getIndent();
                                if (!ind.contentEquals(text.substring(lastIdx))) {
                                    this.addDiff(new Diff(offset + lastIdx, this.tokens.offset(), ind));
                                }
                                this.col = this.indent();
                            }
                            lastToken = null;
                        }
                        this.reformatComment();
                        after = 3;
                        break;
                    }
                    case JAVADOC_COMMENT: {
                        if (this.tokens.index() > 1 && after != 1) {
                            if (maxCount < Integer.MAX_VALUE) {
                                ++maxCount;
                            }
                            ++count;
                        }
                        if (lastToken != null) {
                            offset = this.tokens.offset() - lastToken.length();
                            text = lastToken.text().toString();
                            idx = 0;
                            lastIdx = 0;
                            while (maxCount > 0 && (idx = text.indexOf(10, lastIdx)) >= 0) {
                                if (idx > lastIdx) {
                                    this.addDiff(new Diff(offset + lastIdx, offset + idx, null));
                                }
                                lastIdx = idx + 1;
                                --maxCount;
                                --count;
                                --beforeCnt;
                            }
                            idx = text.lastIndexOf(10);
                            if (idx >= 0) {
                                after = 0;
                                if (idx >= lastIdx) {
                                    this.addDiff(new Diff(offset + lastIdx, offset + idx + 1, null));
                                }
                                lastIdx = idx + 1;
                            }
                            if (lastIdx == 0 && count < 0 && after != 1) {
                                count = 0;
                            }
                            if (pendingDiff != null) {
                                pendingDiff.text = beforeCnt < 0 ? this.getIndent() : this.getNewlines(count) + this.getIndent();
                                if (!pendingDiff.text.contentEquals(pendingText)) {
                                    this.addDiff(pendingDiff);
                                    pendingDiff = null;
                                }
                            }
                            String string = after == 3 ? " " : (ind = pendingDiff == null || beforeCnt < 0 ? this.getNewlines(count) + this.getIndent() : this.getIndent());
                            if (!ind.contentEquals(text.substring(lastIdx))) {
                                this.addDiff(new Diff(offset + lastIdx, this.tokens.offset(), ind));
                            }
                            lastToken = null;
                            this.col = after == 3 ? this.col + 1 : this.indent();
                        } else {
                            String text2 = this.getNewlines(count) + this.getIndent();
                            if (text2.length() > 0) {
                                this.addDiff(new Diff(this.tokens.offset(), this.tokens.offset(), text2));
                            }
                            this.col = this.indent();
                        }
                        this.reformatComment();
                        count = 0;
                        after = 2;
                        break;
                    }
                    case LINE_COMMENT: {
                        if (lastToken != null) {
                            offset = this.tokens.offset() - lastToken.length();
                            text = lastToken.text().toString();
                            if (this.tokens.index() > 1 && after != 1 && text.indexOf(10) >= 0) {
                                if (maxCount < Integer.MAX_VALUE) {
                                    ++maxCount;
                                }
                                ++count;
                            }
                            idx = 0;
                            lastIdx = 0;
                            while (maxCount > 0 && (idx = text.indexOf(10, lastIdx)) >= 0) {
                                if (idx > lastIdx) {
                                    this.addDiff(new Diff(offset + lastIdx, offset + idx, null));
                                }
                                lastIdx = idx + 1;
                                --maxCount;
                                --count;
                                if (pendingText != null) continue;
                                ++beforeCnt;
                            }
                            idx = text.lastIndexOf(10);
                            if (idx >= 0) {
                                if (idx >= lastIdx) {
                                    this.addDiff(new Diff(offset + lastIdx, offset + idx + 1, null));
                                }
                                lastIdx = idx + 1;
                            }
                            if (lastIdx == 0 && after == 1) {
                                indent = this.getIndent();
                                if (!indent.contentEquals(text)) {
                                    this.addDiff(new Diff(offset, this.tokens.offset(), indent));
                                }
                            } else if (lastIdx > 0 && lastIdx < lastToken.length()) {
                                pendingText = text.substring(lastIdx);
                                pendingDiff = new Diff(offset + lastIdx, this.tokens.offset(), null);
                            }
                            lastToken = null;
                        }
                        after = 1;
                        break;
                    }
                    default: {
                        if (this.tokens.index() > 1 && after != 1) {
                            if (maxCount < Integer.MAX_VALUE) {
                                ++maxCount;
                            }
                            ++count;
                        }
                        if (lastToken != null) {
                            offset = this.tokens.offset() - lastToken.length();
                            text = lastToken.text().toString();
                            idx = 0;
                            lastIdx = 0;
                            while (maxCount > 0 && (idx = text.indexOf(10, lastIdx)) >= 0) {
                                if (idx > 0) {
                                    if (this.templateEdit && idx >= lastIdx) {
                                        this.addDiff(new Diff(offset + lastIdx, offset + idx, this.getIndent()));
                                    } else if (idx > lastIdx) {
                                        this.addDiff(new Diff(offset + lastIdx, offset + idx, null));
                                    }
                                }
                                lastIdx = idx + 1;
                                --maxCount;
                                --count;
                                --beforeCnt;
                            }
                            idx = text.lastIndexOf(10);
                            if (idx >= 0) {
                                after = 0;
                                if (idx >= lastIdx) {
                                    this.addDiff(new Diff(offset + lastIdx, offset + idx + 1, null));
                                }
                                lastIdx = idx + 1;
                            }
                            if (lastIdx == 0 && count < 0 && after != 1) {
                                count = 0;
                            }
                            if (pendingDiff != null) {
                                pendingDiff.text = beforeCnt < 0 ? this.getIndent() : this.getNewlines(count) + this.getIndent();
                                if (!pendingDiff.text.contentEquals(pendingText)) {
                                    this.addDiff(pendingDiff);
                                    pendingDiff = null;
                                }
                            }
                            String string = after == 3 ? " " : (indent = pendingDiff == null || beforeCnt < 0 ? this.getNewlines(count) + this.getIndent() : this.getIndent());
                            if (!indent.contentEquals(text.substring(lastIdx))) {
                                this.addDiff(new Diff(offset + lastIdx, this.tokens.offset(), indent));
                            }
                        } else {
                            String text3 = this.getNewlines(count) + this.getIndent();
                            if (text3.length() > 0) {
                                this.addDiff(new Diff(this.tokens.offset(), this.tokens.offset(), text3));
                            }
                        }
                        this.col = this.indent();
                        return;
                    }
                }
            } while (this.tokens.moveNext());
            this.eof = true;
        }

        private void rollback(int index, int col, Diff diff) {
            this.tokens.moveIndex(index);
            this.tokens.moveNext();
            if (diff == null) {
                this.diffs.clear();
            } else {
                while (!this.diffs.isEmpty() && this.diffs.getFirst() != diff) {
                    this.diffs.removeFirst();
                }
            }
            this.col = col;
            if (index < this.lastBlankLinesTokenIndex) {
                this.lastBlankLinesTokenIndex = -1;
            }
        }

        private void appendToDiff(String s) {
            Diff d;
            int offset = this.tokens.offset();
            Diff diff = d = this.diffs.isEmpty() ? null : this.diffs.getFirst();
            if (d != null && d.getEndOffset() == offset) {
                d.text = d.text + s;
            } else {
                this.addDiff(new Diff(offset, offset, s));
            }
        }

        private void addDiff(Diff diff) {
            Diff d;
            Diff diff2 = d = this.diffs.isEmpty() ? null : this.diffs.getFirst();
            if (d == null || d.getStartOffset() <= diff.getStartOffset()) {
                this.diffs.addFirst(diff);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private /* varargs */ int wrapToken(CodeStyle.WrapStyle wrapStyle, int spacesCnt, JavaTokenId first, JavaTokenId ... rest) {
            int ret = -1;
            switch (wrapStyle) {
                case WRAP_ALWAYS: {
                    this.newline();
                    ret = this.col;
                    this.accept(first, rest);
                    break;
                }
                case WRAP_IF_LONG: {
                    int index = this.tokens.index();
                    int c = this.col;
                    Diff d = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                    int o = this.tokens.offset();
                    WrapAbort oldCheckWrap = this.checkWrap;
                    this.checkWrap = new WrapAbort(o);
                    try {
                        this.spaces(spacesCnt, true);
                        ret = this.col;
                        this.accept(first, rest);
                    }
                    catch (WrapAbort wa) {}
                    finally {
                        this.checkWrap = oldCheckWrap;
                    }
                    if (this.col <= this.rightMargin || o < this.lastNewLineOffset) break;
                    this.rollback(index, c, d);
                    this.newline();
                    ret = this.col;
                    this.accept(first, rest);
                    break;
                }
                case WRAP_NEVER: {
                    this.spaces(spacesCnt, true);
                    ret = this.col;
                    this.accept(first, rest);
                }
            }
            return ret;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private int wrapTree(CodeStyle.WrapStyle wrapStyle, int alignIndent, int spacesCnt, Tree tree) {
            int ret;
            ret = -1;
            int oldLast = this.lastIndent;
            try {
                switch (wrapStyle) {
                    case WRAP_ALWAYS: {
                        int old = this.indent;
                        try {
                            if (alignIndent >= 0) {
                                this.indent = this.continuationIndent ? alignIndent - this.continuationIndentSize : alignIndent;
                            }
                            this.newline();
                            ret = this.col;
                            this.scan(tree, null);
                        }
                        finally {
                            this.indent = old;
                        }
                    }
                    case WRAP_IF_LONG: {
                        int index = this.tokens.index();
                        int c = this.col;
                        Diff d = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                        int old = this.indent;
                        WrapAbort oldCheckWrap = this.checkWrap;
                        int o = this.tokens.offset();
                        this.checkWrap = new WrapAbort(this.tokens.offset());
                        try {
                            try {
                                if (alignIndent >= 0) {
                                    this.indent = this.continuationIndent ? alignIndent - this.continuationIndentSize : alignIndent;
                                }
                                this.spaces(spacesCnt, true);
                                ret = this.col;
                                this.scan(tree, null);
                            }
                            finally {
                                this.indent = old;
                            }
                        }
                        catch (WrapAbort wa) {}
                        finally {
                            this.checkWrap = oldCheckWrap;
                        }
                        if (this.col > this.rightMargin && o >= this.lastNewLineOffset) {
                            this.rollback(index, c, d);
                            try {
                                this.indent = alignIndent >= 0 ? (this.continuationIndent ? alignIndent - this.continuationIndentSize : alignIndent) : old;
                                this.newline();
                                ret = this.col;
                                this.scan(tree, null);
                            }
                            finally {
                                this.indent = old;
                            }
                        }
                        break;
                    }
                    case WRAP_NEVER: {
                        int old = this.indent;
                        try {
                            if (alignIndent >= 0) {
                                this.indent = this.continuationIndent ? alignIndent - this.continuationIndentSize : alignIndent;
                            }
                            this.spaces(spacesCnt, true);
                            ret = this.col;
                            this.scan(tree, null);
                        }
                        finally {
                            this.indent = old;
                        }
                    }
                }
            }
            finally {
                this.lastIndent = oldLast;
            }
            return ret;
        }

        private int wrapOperatorAndTree(CodeStyle.WrapStyle wrapStyle, int alignIndent, int spacesCnt, Tree tree) {
            return this.wrapOperatorAndTree(wrapStyle, alignIndent, spacesCnt, spacesCnt, tree);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private int wrapOperatorAndTree(CodeStyle.WrapStyle wrapStyle, int alignIndent, int spacesCntBeforeOp, int spacesCntAfterOp, Tree tree) {
            int ret = -1;
            switch (wrapStyle) {
                case WRAP_ALWAYS: {
                    int old = this.indent;
                    int oldLast = this.lastIndent;
                    boolean oldContinuationIndent = this.continuationIndent;
                    try {
                        if (alignIndent >= 0) {
                            this.indent = this.continuationIndent ? alignIndent - this.continuationIndentSize : alignIndent;
                        }
                        this.newline();
                    }
                    finally {
                        this.indent = old;
                        this.lastIndent = oldLast;
                    }
                    ret = this.col;
                    if ("operator".equals(((JavaTokenId)this.tokens.token().id()).primaryCategory())) {
                        this.col += this.tokens.token().length();
                        this.lastBlankLines = -1;
                        this.lastBlankLinesTokenIndex = -1;
                        this.tokens.moveNext();
                    }
                    try {
                        if (tree.getKind() != Tree.Kind.BLOCK && (tree.getKind() != Tree.Kind.NEW_ARRAY || ((NewArrayTree)tree).getType() != null || this.cs.getOtherBracePlacement() == CodeStyle.BracePlacement.SAME_LINE)) {
                            this.spaces(spacesCntAfterOp);
                        } else {
                            this.continuationIndent = false;
                        }
                        this.scan(tree, null);
                    }
                    finally {
                        this.continuationIndent = oldContinuationIndent;
                    }
                }
                case WRAP_IF_LONG: {
                    int index = this.tokens.index();
                    int c = this.col;
                    Diff d = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                    int old = this.indent;
                    int oldLast = this.lastIndent;
                    boolean oldContinuationIndent = this.continuationIndent;
                    int o = this.tokens.offset();
                    WrapAbort oldCheckWrap = this.checkWrap;
                    this.checkWrap = new WrapAbort(o);
                    try {
                        try {
                            if (alignIndent >= 0) {
                                this.indent = this.continuationIndent ? alignIndent - this.continuationIndentSize : alignIndent;
                                this.indent = alignIndent;
                            }
                            this.spaces(spacesCntBeforeOp, true);
                        }
                        finally {
                            this.indent = old;
                            this.lastIndent = oldLast;
                        }
                        ret = this.col;
                        if ("operator".equals(((JavaTokenId)this.tokens.token().id()).primaryCategory())) {
                            this.col += this.tokens.token().length();
                            this.lastBlankLines = -1;
                            this.lastBlankLinesTokenIndex = -1;
                            this.tokens.moveNext();
                        }
                        try {
                            if (tree.getKind() != Tree.Kind.BLOCK && (tree.getKind() != Tree.Kind.NEW_ARRAY || ((NewArrayTree)tree).getType() != null || this.cs.getOtherBracePlacement() == CodeStyle.BracePlacement.SAME_LINE)) {
                                this.spaces(spacesCntAfterOp);
                            } else {
                                this.continuationIndent = false;
                            }
                            this.scan(tree, null);
                        }
                        finally {
                            this.continuationIndent = oldContinuationIndent;
                        }
                    }
                    catch (WrapAbort wa) {}
                    finally {
                        this.checkWrap = oldCheckWrap;
                    }
                    if (this.col <= this.rightMargin || o < this.lastNewLineOffset) break;
                    this.rollback(index, c, d);
                    try {
                        if (alignIndent >= 0) {
                            this.indent = this.continuationIndent ? alignIndent - this.continuationIndentSize : alignIndent;
                        } else {
                            this.indent = old;
                            this.lastIndent = oldLast;
                            this.continuationIndent = oldContinuationIndent;
                        }
                        this.newline();
                    }
                    finally {
                        this.indent = old;
                        this.lastIndent = oldLast;
                        this.continuationIndent = oldContinuationIndent;
                    }
                    ret = this.col;
                    if ("operator".equals(((JavaTokenId)this.tokens.token().id()).primaryCategory())) {
                        this.col += this.tokens.token().length();
                        this.lastBlankLines = -1;
                        this.lastBlankLinesTokenIndex = -1;
                        this.tokens.moveNext();
                    }
                    try {
                        if (tree.getKind() != Tree.Kind.BLOCK && (tree.getKind() != Tree.Kind.NEW_ARRAY || ((NewArrayTree)tree).getType() != null || this.cs.getOtherBracePlacement() == CodeStyle.BracePlacement.SAME_LINE)) {
                            this.spaces(spacesCntAfterOp);
                        } else {
                            this.continuationIndent = false;
                        }
                        this.scan(tree, null);
                    }
                    finally {
                        this.continuationIndent = oldContinuationIndent;
                    }
                }
                case WRAP_NEVER: {
                    int index = this.tokens.index();
                    int c = this.col;
                    Diff d = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                    int old = this.indent;
                    int oldLast = this.lastIndent;
                    boolean oldContinuationIndent = this.continuationIndent;
                    try {
                        if (alignIndent >= 0) {
                            this.indent = this.continuationIndent ? alignIndent - this.continuationIndentSize : alignIndent;
                        }
                        this.spaces(spacesCntBeforeOp, true);
                    }
                    finally {
                        this.indent = old;
                        this.lastIndent = oldLast;
                    }
                    ret = this.col;
                    if ("operator".equals(((JavaTokenId)this.tokens.token().id()).primaryCategory())) {
                        this.col += this.tokens.token().length();
                        this.lastBlankLines = -1;
                        this.lastBlankLinesTokenIndex = -1;
                        this.tokens.moveNext();
                    }
                    try {
                        if (tree.getKind() != Tree.Kind.BLOCK && (tree.getKind() != Tree.Kind.NEW_ARRAY || ((NewArrayTree)tree).getType() != null || this.cs.getOtherBracePlacement() == CodeStyle.BracePlacement.SAME_LINE)) {
                            if (this.spaces(spacesCntAfterOp, false)) {
                                this.rollback(index, c, d);
                                old = this.indent;
                                oldLast = this.lastIndent;
                                try {
                                    if (alignIndent >= 0) {
                                        this.indent = this.continuationIndent ? alignIndent - this.continuationIndentSize : alignIndent;
                                    }
                                    this.newline();
                                }
                                finally {
                                    this.indent = old;
                                    this.lastIndent = oldLast;
                                }
                                ret = this.col;
                                if ("operator".equals(((JavaTokenId)this.tokens.token().id()).primaryCategory())) {
                                    this.col += this.tokens.token().length();
                                    this.lastBlankLines = -1;
                                    this.lastBlankLinesTokenIndex = -1;
                                    this.tokens.moveNext();
                                }
                                this.spaces(spacesCntAfterOp);
                            }
                        } else {
                            this.continuationIndent = this.isLastIndentContinuation;
                        }
                        this.scan(tree, null);
                    }
                    finally {
                        this.continuationIndent = oldContinuationIndent;
                    }
                }
            }
            return ret;
        }

        private boolean wrapStatement(CodeStyle.WrapStyle wrapStyle, CodeStyle.BracesGenerationStyle bracesGenerationStyle, int spacesCnt, StatementTree tree) {
            if (tree.getKind() == Tree.Kind.EMPTY_STATEMENT) {
                this.scan((Tree)tree, null);
                return true;
            }
            if (tree.getKind() == Tree.Kind.BLOCK) {
                Iterator stats;
                if (bracesGenerationStyle == CodeStyle.BracesGenerationStyle.ELIMINATE && (stats = ((BlockTree)tree).getStatements().iterator()).hasNext()) {
                    StatementTree stat = (StatementTree)stats.next();
                    if (!stats.hasNext() && stat.getKind() != Tree.Kind.VARIABLE) {
                        Diff d;
                        this.accept(JavaTokenId.LBRACE, new JavaTokenId[0]);
                        int start = this.tokens.offset() - 1;
                        while (!this.diffs.isEmpty() && (d = this.diffs.getFirst()) != null && d.getStartOffset() >= start) {
                            this.diffs.removeFirst();
                        }
                        this.addDiff(new Diff(start, this.tokens.offset(), null));
                        int old = this.indent;
                        this.indent = this.lastIndent + this.indentSize;
                        this.wrapTree(wrapStyle, -1, spacesCnt, (Tree)stat);
                        this.indent = old;
                        this.accept(JavaTokenId.RBRACE, new JavaTokenId[0]);
                        this.tokens.moveIndex(this.tokens.index() - 2);
                        this.tokens.moveNext();
                        if (this.tokens.token().id() == JavaTokenId.WHITESPACE) {
                            start = this.tokens.offset();
                            if (this.tokens.movePrevious()) {
                                if (this.tokens.token().id() == JavaTokenId.LINE_COMMENT) {
                                    --start;
                                }
                                this.tokens.moveNext();
                            }
                            this.tokens.moveNext();
                        } else {
                            this.tokens.moveNext();
                            start = this.tokens.offset();
                        }
                        this.tokens.moveNext();
                        while (!this.diffs.isEmpty() && (d = this.diffs.getFirst()) != null && d.getStartOffset() >= start) {
                            this.diffs.removeFirst();
                        }
                        this.addDiff(new Diff(start, this.tokens.offset(), null));
                        return false;
                    }
                }
                this.scan((Tree)tree, null);
                return true;
            }
            if (bracesGenerationStyle == CodeStyle.BracesGenerationStyle.GENERATE) {
                this.scan((Tree)new FakeBlock(tree), null);
                return true;
            }
            int old = this.indent;
            this.indent = this.lastIndent + this.indentSize;
            this.wrapTree(wrapStyle, -1, spacesCnt, (Tree)tree);
            this.indent = old;
            return false;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void wrapList(CodeStyle.WrapStyle wrapStyle, boolean align, boolean prependSpace, JavaTokenId separator, java.util.List<? extends Tree> trees) {
            boolean spaceAfterSeparator;
            boolean spaceBeforeSeparator;
            boolean first = true;
            int alignIndent = -1;
            switch (separator) {
                case COMMA: {
                    spaceBeforeSeparator = this.cs.spaceBeforeComma();
                    spaceAfterSeparator = this.cs.spaceAfterComma();
                    break;
                }
                case SEMICOLON: {
                    spaceBeforeSeparator = this.cs.spaceBeforeSemi();
                    spaceAfterSeparator = this.cs.spaceAfterSemi();
                    break;
                }
                default: {
                    spaceBeforeSeparator = spaceAfterSeparator = this.cs.spaceAroundBinaryOps();
                }
            }
            Iterator<? extends Tree> it = trees.iterator();
            while (it.hasNext()) {
                Tree impl = it.next();
                if (this.wrapAnnotation && impl.getKind() == Tree.Kind.ANNOTATION) {
                    this.wrapTree(CodeStyle.WrapStyle.WRAP_ALWAYS, alignIndent, spaceAfterSeparator ? 1 : 0, impl);
                } else if (impl.getKind() == Tree.Kind.ERRONEOUS) {
                    this.scan(impl, null);
                } else if (first) {
                    int index = this.tokens.index();
                    int c = this.col;
                    Diff d = this.diffs.isEmpty() ? null : this.diffs.getFirst();
                    this.spaces(prependSpace ? 1 : 0, true);
                    if (align) {
                        alignIndent = this.col;
                    }
                    if (wrapStyle == CodeStyle.WrapStyle.WRAP_NEVER || c <= this.indent()) {
                        this.scan(impl, null);
                    } else {
                        int o = this.tokens.offset();
                        WrapAbort oldCheckWrap = this.checkWrap;
                        this.checkWrap = new WrapAbort(o);
                        try {
                            this.scan(impl, null);
                        }
                        catch (WrapAbort wa) {}
                        finally {
                            this.checkWrap = oldCheckWrap;
                        }
                        if (this.col > this.rightMargin && o >= this.lastNewLineOffset) {
                            this.rollback(index, c, d);
                            this.newline();
                            if (align) {
                                alignIndent = this.col;
                            }
                            this.scan(impl, null);
                        }
                    }
                } else {
                    this.wrapTree(wrapStyle, alignIndent, spaceAfterSeparator ? 1 : 0, impl);
                }
                first = false;
                if (!it.hasNext()) continue;
                this.spaces(spaceBeforeSeparator ? 1 : 0);
                this.accept(separator, new JavaTokenId[0]);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void scanMethodCall(MethodInvocationTree node) {
            java.util.List targs = node.getTypeArguments();
            if (targs != null && !targs.isEmpty()) {
                JavaTokenId accepted;
                if (JavaTokenId.LT == this.accept(JavaTokenId.LT, new JavaTokenId[0])) {
                    ++this.tpLevel;
                }
                Iterator it = targs.iterator();
                while (it.hasNext()) {
                    Tree targ = (Tree)it.next();
                    this.scan(targ, null);
                    if (!it.hasNext()) continue;
                    this.spaces(this.cs.spaceBeforeComma() ? 1 : 0);
                    this.accept(JavaTokenId.COMMA, new JavaTokenId[0]);
                    this.spaces(this.cs.spaceAfterComma() ? 1 : 0);
                }
                if (this.tpLevel > 0 && (accepted = this.accept(JavaTokenId.GT, JavaTokenId.GTGT, JavaTokenId.GTGTGT)) != null) {
                    switch (accepted) {
                        case GTGTGT: {
                            this.tpLevel -= 3;
                            break;
                        }
                        case GTGT: {
                            this.tpLevel -= 2;
                            break;
                        }
                        case GT: {
                            --this.tpLevel;
                        }
                    }
                }
            }
            this.accept(JavaTokenId.IDENTIFIER, JavaTokenId.THIS, JavaTokenId.SUPER);
            this.spaces(this.cs.spaceBeforeMethodCallParen() ? 1 : 0);
            this.accept(JavaTokenId.LPAREN, new JavaTokenId[0]);
            boolean old = this.continuationIndent;
            try {
                java.util.List args = node.getArguments();
                if (args != null && !args.isEmpty()) {
                    int oldIndent = this.indent;
                    boolean continuation = this.isLastIndentContinuation;
                    if (continuation) {
                        this.indent = this.indent();
                    }
                    try {
                        this.spaces(this.cs.spaceWithinMethodCallParens() ? 1 : 0, true);
                        this.wrapList(this.cs.wrapMethodCallArgs(), this.cs.alignMultilineCallArgs(), false, JavaTokenId.COMMA, args);
                    }
                    finally {
                        this.indent = oldIndent;
                        this.continuationIndent = continuation;
                    }
                    this.spaces(this.cs.spaceWithinMethodCallParens() ? 1 : 0, true);
                }
                this.accept(JavaTokenId.RPAREN, new JavaTokenId[0]);
            }
            finally {
                this.continuationIndent = old;
            }
        }

        private void reformatComment() {
            int actionType;
            String sub;
            int checkOffset;
            int i;
            Iterator it;
            char c;
            String lineStartString;
            if (this.tokens.token().id() != JavaTokenId.BLOCK_COMMENT && this.tokens.token().id() != JavaTokenId.JAVADOC_COMMENT) {
                return;
            }
            TokenSequence javadocTokens = null;
            TokenSequence embedded = this.tokens.embedded();
            if (embedded != null) {
                if (JavadocTokenId.language().equals((Object)embedded.language())) {
                    javadocTokens = embedded;
                } else {
                    return;
                }
            }
            String text = this.tokens.token().text().toString();
            int offset = this.tokens.offset();
            LinkedList<Pair<Integer, Integer>> marks = new LinkedList<Pair<Integer, Integer>>();
            int maxParamNameLength = 0;
            int maxExcNameLength = 0;
            if (javadocTokens != null) {
                int state = 0;
                int currWSOffset = -1;
                int lastWSOffset = -1;
                int identStart = -1;
                int lastNLOffset = -1;
                int lastAddedNLOffset = -1;
                boolean afterText = false;
                boolean insideTag = false;
                int nestedParenCnt = 0;
                StringBuilder cseq = null;
                Pair toAdd = null;
                Pair nlAdd = null;
                while (javadocTokens.moveNext()) {
                    switch ((JavadocTokenId)javadocTokens.token().id()) {
                        String tokenText;
                        case TAG: {
                            int newState;
                            toAdd = null;
                            nlAdd = null;
                            tokenText = javadocTokens.token().text().toString();
                            if ("@param".equalsIgnoreCase(tokenText)) {
                                newState = 1;
                            } else if ("@return".equalsIgnoreCase(tokenText)) {
                                newState = 3;
                            } else if ("@throws".equalsIgnoreCase(tokenText) || "@exception".equalsIgnoreCase(tokenText)) {
                                newState = 4;
                            } else {
                                if ("@link".equalsIgnoreCase(tokenText) || "@linkplain".equalsIgnoreCase(tokenText) || "@code".equalsIgnoreCase(tokenText) || "@docRoot".equalsIgnoreCase(tokenText) || "@inheritDoc".equalsIgnoreCase(tokenText) || "@value".equalsIgnoreCase(tokenText) || "@literal".equalsIgnoreCase(tokenText)) {
                                    insideTag = true;
                                    this.addMark(Pair.of((Object)(currWSOffset >= 0 ? currWSOffset : javadocTokens.offset() - offset), (Object)5), marks, state);
                                    currWSOffset = -1;
                                    lastWSOffset = -1;
                                    break;
                                }
                                if (insideTag) break;
                                newState = 7;
                            }
                            if (currWSOffset >= 0 && afterText) {
                                this.addMark(Pair.of((Object)currWSOffset, (Object)(state == 0 && this.cs.blankLineAfterJavadocDescription() || state == 2 && newState != 1 && this.cs.blankLineAfterJavadocParameterDescriptions() || state == 3 && this.cs.blankLineAfterJavadocReturnTag() ? 0 : 1)), marks, state);
                            }
                            if ((state = newState) == 3 && this.cs.alignJavadocReturnDescription()) {
                                toAdd = Pair.of((Object)(javadocTokens.offset() + javadocTokens.token().length() - offset), (Object)3);
                            }
                            currWSOffset = -1;
                            lastWSOffset = -1;
                            break;
                        }
                        case IDENT: {
                            if (toAdd != null) {
                                this.addMark(toAdd, marks, state);
                                toAdd = null;
                            }
                            nlAdd = null;
                            if (identStart < 0 && (state == 1 || state == 4)) {
                                identStart = javadocTokens.offset() - offset;
                            }
                            currWSOffset = -1;
                            lastWSOffset = -1;
                            afterText = true;
                            break;
                        }
                        case HTML_TAG: {
                            if (toAdd != null) {
                                this.addMark(toAdd, marks, state);
                            }
                            nlAdd = null;
                            tokenText = javadocTokens.token().text().toString();
                            if (tokenText.endsWith(">")) {
                                if ("<p>".equalsIgnoreCase(tokenText) || "<p/>".equalsIgnoreCase(tokenText)) {
                                    if (currWSOffset >= 0 && currWSOffset > lastAddedNLOffset && (toAdd == null || (Integer)toAdd.first() < currWSOffset)) {
                                        this.addMark(Pair.of((Object)currWSOffset, (Object)1), marks, state);
                                    }
                                    lastAddedNLOffset = javadocTokens.offset() + javadocTokens.token().length() - offset;
                                    this.addMark(Pair.of((Object)lastAddedNLOffset, (Object)1), marks, state);
                                    afterText = false;
                                } else if ("<pre>".equalsIgnoreCase(tokenText)) {
                                    if (currWSOffset >= 0 && state == 0 && (toAdd == null || (Integer)toAdd.first() < currWSOffset)) {
                                        this.addMark(Pair.of((Object)currWSOffset, (Object)1), marks, state);
                                    }
                                    this.addMark(Pair.of((Object)(javadocTokens.offset() - offset), (Object)5), marks, state);
                                    state = 6;
                                } else if ("<code>".equalsIgnoreCase(tokenText)) {
                                    this.addMark(Pair.of((Object)(javadocTokens.offset() - offset), (Object)5), marks, state);
                                } else if ("</pre>".equalsIgnoreCase(tokenText)) {
                                    state = 0;
                                    this.addMark(Pair.of((Object)(currWSOffset >= 0 ? currWSOffset : javadocTokens.offset() - offset), (Object)6), marks, state);
                                } else if ("</code>".equalsIgnoreCase(tokenText)) {
                                    this.addMark(Pair.of((Object)(currWSOffset >= 0 ? currWSOffset : javadocTokens.offset() - offset), (Object)6), marks, state);
                                } else {
                                    if (currWSOffset >= 0 && lastNLOffset >= currWSOffset && lastAddedNLOffset < currWSOffset && (toAdd == null || (Integer)toAdd.first() < currWSOffset)) {
                                        this.addMark(Pair.of((Object)currWSOffset, (Object)1), marks, state);
                                    }
                                    this.addMark(Pair.of((Object)(javadocTokens.offset() - offset), (Object)5), marks, state);
                                    this.addMark(Pair.of((Object)(javadocTokens.offset() + javadocTokens.token().length() - offset - 1), (Object)6), marks, state);
                                    nlAdd = Pair.of((Object)(javadocTokens.offset() + javadocTokens.token().length() - offset), (Object)1);
                                }
                            } else {
                                cseq = new StringBuilder(tokenText);
                            }
                            toAdd = null;
                            currWSOffset = -1;
                            lastWSOffset = -1;
                            break;
                        }
                        case OTHER_TEXT: {
                            currWSOffset = -1;
                            lastWSOffset = -1;
                            if (cseq == null) {
                                cseq = new StringBuilder();
                            }
                            cseq.append(javadocTokens.token().text());
                            int nlNum = 1;
                            int insideTagEndOffset = -1;
                            boolean addNow = false;
                            boolean nlFollows = false;
                            for (i = cseq.length(); i >= 0; --i) {
                                if (i == 0) {
                                    if (lastWSOffset < 0) {
                                        lastWSOffset = javadocTokens.offset() - offset;
                                    }
                                    if (currWSOffset >= 0 || nlNum < 0) continue;
                                    currWSOffset = javadocTokens.offset() - offset;
                                    continue;
                                }
                                c = cseq.charAt(i - 1);
                                if (Character.isWhitespace(c)) {
                                    if (c == '\n') {
                                        --nlNum;
                                        nlFollows = true;
                                        int off = javadocTokens.offset() + i - offset;
                                        if (off > lastNLOffset) {
                                            lastNLOffset = off;
                                        }
                                    }
                                    if (lastWSOffset >= 0 || currWSOffset < 0) continue;
                                    lastWSOffset = -2;
                                    continue;
                                }
                                nlFollows = false;
                                if (c == '*') continue;
                                if (toAdd != null) {
                                    this.addMark(toAdd, marks, state);
                                    toAdd = null;
                                } else {
                                    addNow = true;
                                }
                                if (insideTag) {
                                    if (c == '{') {
                                        ++nestedParenCnt;
                                    } else if (c == '}') {
                                        if (nestedParenCnt > 0) {
                                            --nestedParenCnt;
                                        } else {
                                            insideTagEndOffset = javadocTokens.offset() + i - offset - 1;
                                            insideTag = false;
                                        }
                                    }
                                }
                                if (lastWSOffset == -2) {
                                    lastWSOffset = javadocTokens.offset() + i - offset;
                                }
                                if (currWSOffset < 0 && nlNum >= 0) {
                                    currWSOffset = javadocTokens.offset() + i - offset;
                                }
                                afterText = true;
                            }
                            if (nlFollows && nlAdd != null) {
                                toAdd = nlAdd;
                            }
                            nlAdd = null;
                            if (identStart >= 0) {
                                int len = javadocTokens.offset() - offset - identStart;
                                for (int i2 = 0; i2 <= cseq.length(); ++i2) {
                                    if (i2 != cseq.length() && !Character.isWhitespace(cseq.charAt(i2))) continue;
                                    len += i2;
                                    break;
                                }
                                if (state == 1) {
                                    if (len > maxParamNameLength) {
                                        maxParamNameLength = len;
                                    }
                                    if (this.cs.alignJavadocParameterDescriptions()) {
                                        toAdd = Pair.of((Object)(identStart + len), (Object)2);
                                    }
                                    state = 2;
                                } else if (state == 4) {
                                    if (len > maxExcNameLength) {
                                        maxExcNameLength = len;
                                    }
                                    if (this.cs.alignJavadocExceptionDescriptions()) {
                                        toAdd = Pair.of((Object)(identStart + len), (Object)4);
                                    }
                                    state = 5;
                                }
                                if (addNow && toAdd != null) {
                                    this.addMark(toAdd, marks, state);
                                    toAdd = null;
                                }
                                identStart = -1;
                            }
                            if (insideTagEndOffset >= 0) {
                                this.addMark(Pair.of((Object)insideTagEndOffset, (Object)6), marks, state);
                            }
                            cseq = null;
                            break;
                        }
                        default: {
                            if (toAdd != null) {
                                this.addMark(toAdd, marks, state);
                                toAdd = null;
                            }
                            nlAdd = null;
                        }
                    }
                }
            }
            if ((it = marks.iterator()).hasNext()) {
                Pair next = (Pair)it.next();
                checkOffset = (Integer)next.first();
                actionType = (Integer)next.second();
            } else {
                checkOffset = Integer.MAX_VALUE;
                actionType = -1;
            }
            String indentString = this.getIndent();
            String string = lineStartString = this.cs.addLeadingStarInComment() ? indentString + " " + "*" + " " : indentString + " ";
            String blankLineString = javadocTokens != null && this.cs.generateParagraphTagOnBlankLines() ? (this.cs.addLeadingStarInComment() ? indentString + " " + "*" + " " + "<p>" : indentString + " " + "<p>") : (this.cs.addLeadingStarInComment() ? indentString + " " + "*" : "");
            int currNWSPos = -1;
            int lastNWSPos = -1;
            int currWSPos = -1;
            int lastWSPos = -1;
            int lastNewLinePos = -1;
            Diff pendingDiff = null;
            int start = javadocTokens != null ? 3 : 2;
            this.col += start;
            boolean preserveNewLines = true;
            boolean firstLine = true;
            boolean enableCommentFormatting = javadocTokens != null ? this.cs.enableJavadocFormatting() : !this.bof && this.cs.enableBlockCommentFormatting();
            boolean noFormat = false;
            int align = -1;
            for (i = start; i < text.length(); ++i) {
                String s;
                String sub2;
                String s2;
                c = text.charAt(i);
                if (Character.isWhitespace(c)) {
                    if (enableCommentFormatting) {
                        if (currNWSPos >= 0) {
                            lastNWSPos = currNWSPos;
                            currNWSPos = -1;
                        }
                        if (currWSPos < 0) {
                            currWSPos = i;
                            if (noFormat) {
                                ++this.col;
                            } else {
                                if (this.col > this.rightMargin && this.cs.wrapCommentText() && lastWSPos >= 0) {
                                    int num;
                                    int endOff = pendingDiff != null ? pendingDiff.getEndOffset() - offset : lastWSPos + 1;
                                    s = pendingDiff != null && pendingDiff.text != null && pendingDiff.text.charAt(0) == '\n' ? pendingDiff.text : "\n" + lineStartString;
                                    this.col = this.getCol(lineStartString) + i - endOff;
                                    if (align > 0 && (num = align - this.getCol(lineStartString)) > 0) {
                                        s = s + this.getSpaces(num);
                                        this.col += num;
                                    }
                                    ++this.col;
                                    if (endOff > lastWSPos && !s.equals(text.substring(lastWSPos, endOff))) {
                                        this.addDiff(new Diff(offset + lastWSPos, offset + endOff, s));
                                    }
                                } else if (pendingDiff != null) {
                                    String sub3 = text.substring(pendingDiff.start - offset, pendingDiff.end - offset);
                                    if (!sub3.equals(pendingDiff.text)) {
                                        this.addDiff(pendingDiff);
                                    }
                                    ++this.col;
                                } else {
                                    ++this.col;
                                }
                                pendingDiff = null;
                            }
                        }
                    }
                    if (c == '\n') {
                        if (lastNewLinePos >= 0) {
                            String subs;
                            if (enableCommentFormatting && !blankLineString.equals(subs = text.substring(lastNewLinePos + 1, i))) {
                                this.addDiff(new Diff(offset + lastNewLinePos + 1, offset + i, blankLineString));
                            }
                            preserveNewLines = true;
                            lastNewLinePos = i;
                            align = -1;
                        } else {
                            lastNewLinePos = currWSPos >= 0 ? currWSPos : i;
                        }
                        firstLine = false;
                    }
                    if (i < checkOffset || actionType != 5) continue;
                    noFormat = true;
                    align = -1;
                    if (it.hasNext()) {
                        Pair next = (Pair)it.next();
                        checkOffset = (Integer)next.first();
                        actionType = (Integer)next.second();
                        continue;
                    }
                    checkOffset = Integer.MAX_VALUE;
                    actionType = -1;
                    continue;
                }
                if (pendingDiff != null && (sub2 = text.substring(pendingDiff.start - offset, pendingDiff.end - offset)).equals(pendingDiff.text)) {
                    pendingDiff = null;
                }
                if (enableCommentFormatting) {
                    if (currNWSPos < 0) {
                        currNWSPos = i;
                    }
                    if (i >= checkOffset) {
                        noFormat = false;
                        switch (actionType) {
                            case 0: {
                                pendingDiff = new Diff(currWSPos >= 0 ? offset + currWSPos : offset + i, offset + i, "\n" + blankLineString + "\n");
                                lastNewLinePos = i - 1;
                                preserveNewLines = true;
                                align = -1;
                                break;
                            }
                            case 1: {
                                pendingDiff = new Diff(currWSPos >= 0 ? offset + currWSPos : offset + i, offset + i, "\n");
                                lastNewLinePos = i - 1;
                                preserveNewLines = true;
                                align = -1;
                                break;
                            }
                            case 2: {
                                this.col += maxParamNameLength + lastNWSPos - currWSPos;
                                align = this.col;
                                currWSPos = -1;
                                if (lastNewLinePos >= 0) break;
                                int num = maxParamNameLength + lastNWSPos + 1 - i;
                                if (num > 0) {
                                    this.addDiff(new Diff(offset + i, offset + i, this.getSpaces(num)));
                                    break;
                                }
                                if (num >= 0) break;
                                this.addDiff(new Diff(offset + i + num, offset + i, null));
                                break;
                            }
                            case 3: {
                                align = this.col;
                                break;
                            }
                            case 4: {
                                this.col += maxExcNameLength + lastNWSPos - currWSPos;
                                align = this.col;
                                currWSPos = -1;
                                if (lastNewLinePos >= 0) break;
                                int num = maxExcNameLength + lastNWSPos + 1 - i;
                                if (num > 0) {
                                    this.addDiff(new Diff(offset + i, offset + i, this.getSpaces(num)));
                                    break;
                                }
                                if (num >= 0) break;
                                this.addDiff(new Diff(offset + i + num, offset + i, null));
                                break;
                            }
                            case 5: {
                                noFormat = true;
                                if (currWSPos <= 0) break;
                                lastWSPos = currWSPos;
                                break;
                            }
                            case 6: {
                                preserveNewLines = true;
                            }
                        }
                        if (it.hasNext()) {
                            Pair next = (Pair)it.next();
                            checkOffset = (Integer)next.first();
                            actionType = (Integer)next.second();
                        } else {
                            checkOffset = Integer.MAX_VALUE;
                            actionType = -1;
                        }
                    }
                }
                if (lastNewLinePos >= 0) {
                    if (!preserveNewLines && !noFormat && i < text.length() - 2 && enableCommentFormatting && !this.cs.preserveNewLinesInComments() && this.cs.wrapCommentText()) {
                        lastWSPos = lastNewLinePos;
                        if (pendingDiff != null) {
                            pendingDiff.text = pendingDiff.text + " ";
                        } else {
                            pendingDiff = new Diff(offset + lastNewLinePos, offset + i, " ");
                        }
                        lastNewLinePos = -1;
                        if (c == '*') {
                            while (++i < text.length()) {
                                ++this.col;
                                c = text.charAt(i);
                                if (c == '\n') {
                                    pendingDiff.text = "\n" + blankLineString;
                                    preserveNewLines = true;
                                    lastNewLinePos = i;
                                    align = -1;
                                    break;
                                }
                                if (Character.isWhitespace(c)) continue;
                            }
                            if (pendingDiff != null) {
                                int diff = offset + i - pendingDiff.end;
                                Diff.access$412(pendingDiff, diff);
                                this.col -= diff;
                            }
                        }
                    } else {
                        String sub4;
                        String subs;
                        if (pendingDiff != null) {
                            pendingDiff.text = pendingDiff.text + (indentString + " ");
                            subs = text.substring(pendingDiff.start - offset, i);
                            if (pendingDiff.text.equals(subs)) {
                                lastNewLinePos = pendingDiff.start - offset;
                                pendingDiff = null;
                            }
                        } else {
                            String subs2;
                            s2 = "\n" + indentString + " ";
                            if (!s2.equals(subs2 = text.substring(lastNewLinePos, i))) {
                                pendingDiff = new Diff(offset + lastNewLinePos, offset + i, s2);
                            }
                        }
                        currWSPos = -1;
                        lastWSPos = -1;
                        this.col = this.getCol(indentString + " ");
                        if (enableCommentFormatting) {
                            if (c == '*') {
                                ++this.col;
                                while (++i < text.length()) {
                                    c = text.charAt(i);
                                    if (c == '\n') {
                                        if (!this.cs.addLeadingStarInComment()) {
                                            subs = text.substring(lastNewLinePos + 1, i);
                                            if (blankLineString.equals(subs)) {
                                                pendingDiff = null;
                                            } else if (pendingDiff != null) {
                                                pendingDiff.end = offset + i;
                                                pendingDiff.text = blankLineString;
                                            } else {
                                                pendingDiff = new Diff(offset + lastNewLinePos + 1, offset + i, blankLineString);
                                            }
                                        } else if (currWSPos >= 0) {
                                            if (pendingDiff != null && !(sub4 = text.substring(pendingDiff.start - offset, pendingDiff.end - offset)).equals(pendingDiff.text)) {
                                                this.addDiff(pendingDiff);
                                            }
                                            pendingDiff = new Diff(offset + currWSPos, offset + i, javadocTokens != null && lastNWSPos >= 0 && this.cs.generateParagraphTagOnBlankLines() ? " <p>" : "");
                                        }
                                        currWSPos = -1;
                                        lastNewLinePos = i;
                                        align = -1;
                                    } else {
                                        if (Character.isWhitespace(c)) {
                                            if (currWSPos >= 0) continue;
                                            currWSPos = i;
                                            ++this.col;
                                            continue;
                                        }
                                        if (c == '*' || c == '/') {
                                            ++this.col;
                                            lastNewLinePos = -1;
                                        } else {
                                            if (!this.cs.addLeadingStarInComment()) {
                                                if (noFormat) {
                                                    if (pendingDiff != null) {
                                                        pendingDiff.end = currWSPos >= 0 ? offset + currWSPos + 1 : pendingDiff.end + 1;
                                                    } else {
                                                        pendingDiff = new Diff(offset + lastNewLinePos + 1, currWSPos >= 0 ? offset + currWSPos + 1 : offset + i, indentString + " ");
                                                    }
                                                } else {
                                                    if (pendingDiff != null) {
                                                        pendingDiff.end = offset + i;
                                                    } else {
                                                        pendingDiff = new Diff(offset + lastNewLinePos + 1, offset + i, indentString + " ");
                                                    }
                                                    this.col = this.getCol(indentString + " ");
                                                }
                                            } else {
                                                if (currWSPos < 0) {
                                                    currWSPos = i;
                                                    ++this.col;
                                                }
                                                subs = text.substring(currWSPos, i);
                                                s = this.getSpaces(align < 0 ? 1 : align - this.getCol(lineStartString) + 1);
                                                if (!noFormat && !s.equals(subs)) {
                                                    String sub5;
                                                    if (pendingDiff != null && !(sub5 = text.substring(pendingDiff.start - offset, pendingDiff.end - offset)).equals(pendingDiff.text)) {
                                                        this.addDiff(pendingDiff);
                                                    }
                                                    pendingDiff = new Diff(offset + currWSPos, offset + i, s);
                                                }
                                            }
                                            lastNewLinePos = -1;
                                            currWSPos = -1;
                                        }
                                    }
                                    break;
                                }
                            } else {
                                if (this.cs.addLeadingStarInComment()) {
                                    int num = Math.max(align - this.col - 1, 1);
                                    s = this.getSpaces(num);
                                    if (pendingDiff != null) {
                                        pendingDiff.text = pendingDiff.text + ("*" + s);
                                    } else {
                                        pendingDiff = new Diff(offset + i, offset + i, "*" + s);
                                    }
                                    this.col += num + 1;
                                } else if (align > this.col) {
                                    int num = align - this.col;
                                    s = this.getSpaces(num);
                                    if (pendingDiff != null) {
                                        pendingDiff.text = pendingDiff.text + s;
                                    } else {
                                        pendingDiff = new Diff(offset + i, offset + i, s);
                                    }
                                    this.col += num;
                                }
                                lastNewLinePos = -1;
                            }
                        } else {
                            lastNewLinePos = -1;
                        }
                        if (pendingDiff != null) {
                            sub4 = text.substring(pendingDiff.start - offset, pendingDiff.end - offset);
                            if (!sub4.equals(pendingDiff.text)) {
                                this.addDiff(pendingDiff);
                            }
                            pendingDiff = null;
                        }
                    }
                } else if (enableCommentFormatting) {
                    if (firstLine) {
                        s2 = this.cs.wrapOneLineComments() ? "\n" + lineStartString : " ";
                        String string2 = sub = currWSPos >= 0 ? text.substring(currWSPos, i) : null;
                        if (!s2.equals(sub)) {
                            this.addDiff(new Diff(currWSPos >= 0 ? offset + currWSPos : offset + i, offset + i, s2));
                        }
                        if (this.cs.wrapOneLineComments()) {
                            this.col = this.getCol(lineStartString);
                        }
                        firstLine = false;
                    } else if (currWSPos >= 0) {
                        if (!noFormat) {
                            lastWSPos = currWSPos;
                            if (currWSPos < i - 1) {
                                pendingDiff = new Diff(offset + currWSPos + 1, offset + i, null);
                            }
                        }
                    } else if (c != '*') {
                        preserveNewLines = false;
                    }
                } else if (c != '*') {
                    preserveNewLines = false;
                }
                currWSPos = -1;
                ++this.col;
            }
            if (enableCommentFormatting) {
                for (i = text.length() - 3; i >= 0 && (c = text.charAt(i)) != '\n'; --i) {
                    if (Character.isWhitespace(c)) continue;
                    String s = this.cs.wrapOneLineComments() ? "\n" + indentString + " " : " ";
                    if (s.equals(sub = text.substring(i + 1, text.length() - 2))) break;
                    this.addDiff(new Diff(offset + i + 1, offset + text.length() - 2, s));
                    break;
                }
            }
        }

        private void addMark(Pair<Integer, Integer> mark, java.util.List<Pair<Integer, Integer>> marks, int state) {
            if (state != 6) {
                marks.add(mark);
            }
        }

        private int indent() {
            return this.continuationIndent ? this.indent + this.continuationIndentSize : this.indent;
        }

        private String getSpaces(int count) {
            if (count <= 0) {
                return "";
            }
            if (count == 1) {
                return " ";
            }
            StringBuilder sb = new StringBuilder();
            while (count-- > 0) {
                sb.append(' ');
            }
            return sb.toString();
        }

        private String getNewlines(int count) {
            if (count <= 0) {
                return "";
            }
            if (count == 1) {
                return "\n";
            }
            StringBuilder sb = new StringBuilder();
            while (count-- > 0) {
                sb.append('\n');
            }
            return sb.toString();
        }

        private String getIndent() {
            StringBuilder sb = new StringBuilder();
            int col = 0;
            if (!this.expandTabToSpaces) {
                while (col + this.tabSize <= this.indent()) {
                    sb.append('\t');
                    col += this.tabSize;
                }
            }
            while (col < this.indent()) {
                sb.append(" ");
                ++col;
            }
            this.lastIndent = this.indent;
            this.isLastIndentContinuation = this.continuationIndent;
            return sb.toString();
        }

        private int getIndentLevel(TokenSequence<JavaTokenId> tokens, TreePath path) {
            if (path.getLeaf().getKind() == Tree.Kind.COMPILATION_UNIT) {
                return 0;
            }
            Tree lastTree = null;
            int indent = -1;
            while (path != null) {
                int offset = (int)this.sp.getStartPosition(path.getCompilationUnit(), path.getLeaf());
                if (offset < 0) {
                    return indent;
                }
                tokens.move(offset);
                String text = null;
                while (tokens.movePrevious()) {
                    Token token = tokens.token();
                    if (token.id() == JavaTokenId.WHITESPACE) {
                        text = token.text().toString();
                        int idx = text.lastIndexOf(10);
                        if (idx < 0) continue;
                        text = text.substring(idx + 1);
                        indent = this.getCol(text);
                        break;
                    }
                    if (token.id() == JavaTokenId.LINE_COMMENT) {
                        indent = text != null ? this.getCol(text) : 0;
                        break;
                    }
                    if (token.id() != JavaTokenId.BLOCK_COMMENT && token.id() != JavaTokenId.JAVADOC_COMMENT) break;
                    text = null;
                }
                if (indent >= 0) break;
                lastTree = path.getLeaf();
                path = path.getParentPath();
            }
            if (lastTree != null && path != null) {
                block0 : switch (path.getLeaf().getKind()) {
                    case ANNOTATION_TYPE: 
                    case CLASS: 
                    case ENUM: 
                    case INTERFACE: {
                        for (Tree tree : ((ClassTree)path.getLeaf()).getMembers()) {
                            if (tree != lastTree) continue;
                            indent += this.tabSize;
                            break block0;
                        }
                        break;
                    }
                    case BLOCK: {
                        for (Tree tree : ((BlockTree)path.getLeaf()).getStatements()) {
                            if (tree != lastTree) continue;
                            indent += this.tabSize;
                            break block0;
                        }
                        break;
                    }
                }
            }
            return indent;
        }

        private int getCol(String text) {
            int col = 0;
            for (int i = 0; i < text.length(); ++i) {
                char c = text.charAt(i);
                if (c == '\n') {
                    col = 0;
                    continue;
                }
                if (c == '\t') {
                    col += this.tabSize;
                    col -= col % this.tabSize;
                    continue;
                }
                ++col;
            }
            return col;
        }

        private boolean insideBlock(TreePath path) {
            while (path != null) {
                if (Tree.Kind.BLOCK == path.getLeaf().getKind()) {
                    return true;
                }
                path = path.getParentPath();
            }
            return false;
        }

        private boolean isEnumerator(VariableTree tree) {
            return (((JCTree.JCModifiers)tree.getModifiers()).flags & 16384) != 0;
        }

        private boolean isSynthetic(CompilationUnitTree cut, Tree leaf) {
            ExpressionStatementTree est;
            MethodInvocationTree mit;
            IdentifierTree it;
            JCTree tree = (JCTree)leaf;
            if (tree.pos == -1) {
                return true;
            }
            if (leaf.getKind() == Tree.Kind.METHOD) {
                return (((JCTree.JCMethodDecl)leaf).mods.flags & 0x1000000000L) != 0;
            }
            if (leaf.getKind() == Tree.Kind.EXPRESSION_STATEMENT && (est = (ExpressionStatementTree)leaf).getExpression().getKind() == Tree.Kind.METHOD_INVOCATION && (mit = (MethodInvocationTree)est.getExpression()).getMethodSelect().getKind() == Tree.Kind.IDENTIFIER && "super".equals((it = (IdentifierTree)mit.getMethodSelect()).getName().toString())) {
                return this.sp.getEndPosition(cut, leaf) == -1;
            }
            return false;
        }

        private static class DanglingElseChecker
        extends SimpleTreeVisitor<Void, Void> {
            private boolean foundDanglingElse;

            private DanglingElseChecker() {
            }

            public boolean hasDanglingElse(Tree t) {
                if (t == null) {
                    return false;
                }
                this.foundDanglingElse = false;
                this.visit(t, (Object)null);
                return this.foundDanglingElse;
            }

            public Void visitBlock(BlockTree node, Void p) {
                StatementTree stat;
                Iterator it = node.getStatements().iterator();
                StatementTree statementTree = stat = it.hasNext() ? (StatementTree)it.next() : null;
                if (stat != null && !it.hasNext()) {
                    this.visit((Tree)stat, (Object)p);
                }
                return null;
            }

            public Void visitDoWhileLoop(DoWhileLoopTree node, Void p) {
                return (Void)this.visit((Tree)node.getStatement(), (Object)p);
            }

            public Void visitEnhancedForLoop(EnhancedForLoopTree node, Void p) {
                return (Void)this.visit((Tree)node.getStatement(), (Object)p);
            }

            public Void visitForLoop(ForLoopTree node, Void p) {
                return (Void)this.visit((Tree)node.getStatement(), (Object)p);
            }

            public Void visitIf(IfTree node, Void p) {
                if (node.getElseStatement() == null) {
                    this.foundDanglingElse = true;
                } else {
                    this.visit((Tree)node.getElseStatement(), (Object)p);
                }
                return null;
            }

            public Void visitLabeledStatement(LabeledStatementTree node, Void p) {
                return (Void)this.visit((Tree)node.getStatement(), (Object)p);
            }

            public Void visitSynchronized(SynchronizedTree node, Void p) {
                return (Void)this.visit((Tree)node.getBlock(), (Object)p);
            }

            public Void visitWhileLoop(WhileLoopTree node, Void p) {
                return (Void)this.visit((Tree)node.getStatement(), (Object)p);
            }
        }

        private static class FakeBlock
        extends JCTree.JCBlock {
            private StatementTree stat;

            private FakeBlock(StatementTree stat) {
                super(0, List.of((Object)((JCTree.JCStatement)stat)));
                this.stat = stat;
            }
        }

        private static class WrapAbort
        extends Error {
            private int pos;

            public WrapAbort(int pos) {
                this.pos = pos;
            }

            @Override
            public synchronized Throwable fillInStackTrace() {
                return null;
            }
        }

    }

    public static class Factory
    implements ReformatTask.Factory {
        public ReformatTask createTask(org.netbeans.modules.editor.indent.spi.Context context) {
            Source source = Source.create((Document)context.document());
            return source != null ? new Reformatter(source, context) : null;
        }
    }

}

