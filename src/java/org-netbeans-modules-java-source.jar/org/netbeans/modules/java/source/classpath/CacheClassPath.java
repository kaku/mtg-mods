/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.modules.parsing.impl.indexing.PathRegistry
 *  org.netbeans.spi.java.classpath.ClassPathFactory
 *  org.netbeans.spi.java.classpath.ClassPathImplementation
 *  org.netbeans.spi.java.classpath.PathResourceImplementation
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.java.source.classpath;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.usages.ClassIndexManager;
import org.netbeans.modules.java.source.usages.ClassIndexManagerEvent;
import org.netbeans.modules.java.source.usages.ClassIndexManagerListener;
import org.netbeans.modules.parsing.impl.indexing.PathRegistry;
import org.netbeans.spi.java.classpath.ClassPathFactory;
import org.netbeans.spi.java.classpath.ClassPathImplementation;
import org.netbeans.spi.java.classpath.PathResourceImplementation;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import org.openide.util.WeakListeners;

public class CacheClassPath
implements ClassPathImplementation,
PropertyChangeListener,
ClassIndexManagerListener {
    public static final boolean KEEP_JARS = Boolean.getBoolean("CacheClassPath.keepJars");
    private static final Logger LOG = Logger.getLogger(CacheClassPath.class.getName());
    private final ClassPath cp;
    private final boolean translate;
    private final boolean scan;
    private final PropertyChangeSupport listeners;
    private List<PathResourceImplementation> cache;
    private Set<URL> expectedSourceRoots;
    private long eventId;

    private CacheClassPath(ClassPath cp, boolean translate, boolean scan) {
        this.listeners = new PropertyChangeSupport(this);
        this.cp = cp;
        this.translate = translate;
        this.scan = scan;
        if (!scan) {
            this.cp.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)cp));
            ClassIndexManager cim = ClassIndexManager.getDefault();
            cim.addClassIndexManagerListener((ClassIndexManagerListener)WeakListeners.create(ClassIndexManagerListener.class, (EventListener)this, (Object)cim));
        }
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.listeners.removePropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.listeners.addPropertyChangeListener(listener);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void propertyChange(PropertyChangeEvent event) {
        if ("entries".equals(event.getPropertyName())) {
            CacheClassPath cacheClassPath = this;
            synchronized (cacheClassPath) {
                this.cache = null;
                ++this.eventId;
            }
            this.listeners.firePropertyChange("resources", null, null);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void classIndexAdded(ClassIndexManagerEvent event) {
        Set<? extends URL> added = event.getRoots();
        boolean fire = false;
        CacheClassPath cacheClassPath = this;
        synchronized (cacheClassPath) {
            if (this.expectedSourceRoots != null) {
                for (URL ar : added) {
                    if (!this.expectedSourceRoots.contains(ar)) continue;
                    this.cache = null;
                    ++this.eventId;
                    fire = true;
                    break;
                }
            }
        }
        if (fire) {
            this.listeners.firePropertyChange("resources", null, null);
        }
    }

    @Override
    public void classIndexRemoved(@NonNull ClassIndexManagerEvent event) {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public List<? extends PathResourceImplementation> getResources() {
        List res2;
        long currentEventId;
        CacheClassPath entry2;
        CacheClassPath cacheClassPath = this;
        synchronized (cacheClassPath) {
            if (this.cache != null) {
                return this.cache;
            }
            currentEventId = this.eventId;
        }
        List entries = this.cp.entries();
        LinkedHashSet<PathResourceImplementation> _cache = new LinkedHashSet<PathResourceImplementation>();
        PathRegistry preg = PathRegistry.getDefault();
        HashSet<URL> unInitializedSourceRoots = new HashSet<URL>();
        for (CacheClassPath entry2 : entries) {
            URL foo;
            FileObject fo;
            URL url = entry2.getURL();
            URL[] sourceUrls = this.translate ? preg.sourceForBinaryQuery(url, this.cp, true) : new URL[]{url};
            if (sourceUrls != null) {
                for (URL sourceUrl : sourceUrls) {
                    if (this.scan || JavaIndex.hasSourceCache(sourceUrl, false)) {
                        try {
                            File cacheFolder = JavaIndex.getClassFolder(sourceUrl);
                            URL cacheUrl = FileUtil.urlForArchiveOrDir((File)cacheFolder);
                            _cache.add(ClassPathSupport.createResource((URL)cacheUrl));
                        }
                        catch (IOException ioe) {
                            if (!LOG.isLoggable(Level.SEVERE)) continue;
                            LOG.log(Level.SEVERE, ioe.getMessage(), ioe);
                        }
                        continue;
                    }
                    unInitializedSourceRoots.add(sourceUrl);
                }
                if (!KEEP_JARS || !this.translate) continue;
                _cache.add(ClassPathSupport.createResource((URL)url));
                continue;
            }
            if ("jar".equals(url.getProtocol())) {
                FileObject fo2;
                URL foo2 = FileUtil.getArchiveFile((URL)url);
                if (!"file".equals(foo2.getProtocol()) && (fo2 = URLMapper.findFileObject((URL)foo2)) != null && "file".equals((foo2 = URLMapper.findURL((FileObject)fo2, (int)1)).getProtocol())) {
                    url = FileUtil.getArchiveRoot((URL)foo2);
                }
            } else if (!"file".equals(url.getProtocol()) && (fo = URLMapper.findFileObject((URL)url)) != null && (foo = URLMapper.findURL((FileObject)fo, (int)1)) != null && "file".equals(foo.getProtocol())) {
                url = foo;
            }
            _cache.add(new CachingPathResourceImpl(url, this.scan));
            _cache.add(ClassPathSupport.createResource((URL)url));
        }
        entry2 = this;
        synchronized (entry2) {
            List res2;
            if (currentEventId == this.eventId) {
                this.cache = new ArrayList<PathResourceImplementation>(_cache);
                this.expectedSourceRoots = unInitializedSourceRoots.isEmpty() ? null : Collections.unmodifiableSet(unInitializedSourceRoots);
                res2 = this.cache;
            } else {
                res2 = new ArrayList(_cache);
            }
        }
        assert (res2 != null);
        return res2;
    }

    public static ClassPath forClassPath(ClassPath cp, boolean ru) {
        assert (cp != null);
        return ClassPathFactory.createClassPath((ClassPathImplementation)new CacheClassPath(cp, true, ru));
    }

    public static ClassPath forBootPath(ClassPath cp, boolean ru) {
        assert (cp != null);
        return ClassPathFactory.createClassPath((ClassPathImplementation)new CacheClassPath(cp, true, ru));
    }

    public static ClassPath forSourcePath(ClassPath sourcePath, boolean ru) {
        assert (sourcePath != null);
        return ClassPathFactory.createClassPath((ClassPathImplementation)new CacheClassPath(sourcePath, false, ru));
    }

    private static final class CachingPathResourceImpl
    implements PathResourceImplementation {
        private static final URL[] EMPTY = new URL[0];
        private final URL originalRoot;
        private final boolean scan;
        private URL[] cacheRoot;

        public CachingPathResourceImpl(@NonNull URL originalRoot, boolean scan) {
            this.originalRoot = originalRoot;
            this.scan = scan;
        }

        public synchronized URL[] getRoots() {
            URL[] result = this.cacheRoot;
            if (result == null) {
                result = EMPTY;
                try {
                    File sigs = JavaIndex.getClassFolder(this.originalRoot, false, this.scan);
                    URL orl = FileUtil.urlForArchiveOrDir((File)sigs);
                    if (orl != null) {
                        result = new URL[]{orl};
                    } else {
                        LOG.log(Level.WARNING, "Invalid cache root: {0} exists: {1} dir: {2} retry: {3}", new Object[]{sigs.getAbsolutePath(), sigs.exists(), sigs.isDirectory(), FileUtil.urlForArchiveOrDir((File)sigs)});
                    }
                }
                catch (IOException ioe) {
                    Exceptions.printStackTrace((Throwable)ioe);
                }
                this.cacheRoot = result;
            }
            assert (result != null);
            return result;
        }

        public ClassPathImplementation getContent() {
            return null;
        }

        public void addPropertyChangeListener(PropertyChangeListener listener) {
        }

        public void removePropertyChangeListener(PropertyChangeListener listener) {
        }
    }

}

