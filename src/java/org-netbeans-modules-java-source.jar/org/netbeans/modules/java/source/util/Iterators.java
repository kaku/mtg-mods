/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.java.source.util;

import java.util.Collections;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.openide.util.Parameters;

public final class Iterators {
    private static final String NULL_AS_PARAMETER_MESSAGE = "Iterator(s) passed in as parameter must NOT be null.";

    private Iterators() {
    }

    public static <T> Iterable<T> chained(Iterable<? extends Iterable<T>> iterables) {
        return new ChainedIterable(iterables);
    }

    public static <T> Iterable<T> filter(Iterable<T> it, Comparable<? super T> c) {
        Parameters.notNull((CharSequence)"it", it);
        Parameters.notNull((CharSequence)"c", c);
        return new FilterIterable<T>(it, c);
    }

    private static class ChainedIterator<E>
    implements Iterator<E> {
        protected final Iterator<? extends Iterable<E>> iteratorChain;
        protected Iterator<E> currentIterator = null;

        public ChainedIterator(Iterable<? extends Iterable<E>> iterators) {
            this.iteratorChain = iterators.iterator();
        }

        protected void updateCurrentIterator() {
            if (this.currentIterator == null) {
                this.currentIterator = !this.iteratorChain.hasNext() ? Collections.emptyList().iterator() : this.iteratorChain.next().iterator();
            }
            while (!this.currentIterator.hasNext() && this.iteratorChain.hasNext()) {
                this.currentIterator = this.iteratorChain.next().iterator();
            }
        }

        @Override
        public boolean hasNext() {
            this.updateCurrentIterator();
            return this.currentIterator.hasNext();
        }

        @Override
        public E next() {
            this.updateCurrentIterator();
            return this.currentIterator.next();
        }

        @Override
        public void remove() throws UnsupportedOperationException {
            throw new UnsupportedOperationException();
        }
    }

    private static class ChainedIterable<T>
    implements Iterable<T> {
        final Iterable<? extends Iterable<T>> iterables;

        ChainedIterable(Iterable<? extends Iterable<T>> iterables) {
            assert (iterables != null);
            this.iterables = iterables;
        }

        @Override
        public Iterator<T> iterator() {
            return new ChainedIterator(this.iterables);
        }
    }

    private static class FilterIterator<T>
    implements Iterator<T> {
        private final Iterator<T> it;
        private final Comparable<? super T> c;
        private T nextValue;

        public FilterIterator(Iterator<T> it, Comparable<? super T> c) {
            this.it = it;
            this.c = c;
        }

        @Override
        public boolean hasNext() {
            if (this.nextValue != null) {
                return true;
            }
            while (this.it.hasNext()) {
                T val = this.it.next();
                if (this.c.compareTo(val) == 0) continue;
                this.nextValue = val;
                return true;
            }
            return false;
        }

        @Override
        public T next() {
            if (!this.hasNext()) {
                throw new NoSuchElementException();
            }
            T ret = this.nextValue;
            this.nextValue = null;
            return ret;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported operation.");
        }
    }

    private static class FilterIterable<T>
    implements Iterable<T> {
        private final Iterable<T> it;
        private final Comparable<? super T> c;

        public FilterIterable(Iterable<T> it, Comparable<? super T> c) {
            this.it = it;
            this.c = c;
        }

        @Override
        public Iterator<T> iterator() {
            return new FilterIterator<T>(this.it.iterator(), this.c);
        }
    }

}

