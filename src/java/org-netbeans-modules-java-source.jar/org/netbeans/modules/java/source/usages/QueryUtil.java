/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.index.Term
 *  org.apache.lucene.search.BooleanClause
 *  org.apache.lucene.search.BooleanClause$Occur
 *  org.apache.lucene.search.BooleanQuery
 *  org.apache.lucene.search.Query
 *  org.apache.lucene.search.TermQuery
 *  org.apache.lucene.search.WildcardQuery
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.modules.parsing.lucene.support.Queries
 *  org.netbeans.modules.parsing.lucene.support.Queries$QueryKind
 *  org.netbeans.modules.parsing.lucene.support.StoppableConvertor
 *  org.netbeans.modules.parsing.lucene.support.StoppableConvertor$Stop
 *  org.openide.util.Pair
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.java.source.usages;

import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Pattern;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.WildcardQuery;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.source.ClassIndex;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;
import org.netbeans.modules.java.source.usages.DocumentUtil;
import org.netbeans.modules.parsing.lucene.support.Queries;
import org.netbeans.modules.parsing.lucene.support.StoppableConvertor;
import org.openide.util.Pair;
import org.openide.util.Parameters;

class QueryUtil {
    QueryUtil() {
    }

    static Query createUsagesQuery(@NonNull String resourceName, @NonNull Set<? extends ClassIndexImpl.UsageType> mask, @NonNull BooleanClause.Occur operator) {
        Parameters.notNull((CharSequence)"resourceName", (Object)resourceName);
        Parameters.notNull((CharSequence)"mask", mask);
        Parameters.notNull((CharSequence)"operator", (Object)operator);
        if (operator == BooleanClause.Occur.SHOULD) {
            BooleanQuery query = new BooleanQuery();
            for (ClassIndexImpl.UsageType ut : mask) {
                WildcardQuery subQuery = new WildcardQuery(DocumentUtil.referencesTerm(resourceName, EnumSet.of(ut), false));
                query.add((Query)subQuery, operator);
            }
            return query;
        }
        if (operator == BooleanClause.Occur.MUST) {
            return new WildcardQuery(DocumentUtil.referencesTerm(resourceName, mask, false));
        }
        throw new IllegalArgumentException();
    }

    @NonNull
    static Query createPackageUsagesQuery(@NonNull String packageName, @NonNull Set<? extends ClassIndexImpl.UsageType> mask, @NonNull BooleanClause.Occur operator) {
        Parameters.notNull((CharSequence)"packageName", (Object)packageName);
        Parameters.notNull((CharSequence)"mask", mask);
        String pattern = Pattern.quote(packageName) + "\\.[^\\.]+";
        if (operator == BooleanClause.Occur.SHOULD) {
            BooleanQuery query = new BooleanQuery();
            for (ClassIndexImpl.UsageType ut : mask) {
                Term t = DocumentUtil.referencesTerm(pattern, EnumSet.of(ut), true);
                query.add(Queries.createQuery((String)t.field(), (String)t.field(), (String)t.text(), (Queries.QueryKind)Queries.QueryKind.REGEXP), operator);
            }
            return query;
        }
        if (operator == BooleanClause.Occur.MUST) {
            Term t = DocumentUtil.referencesTerm(pattern, mask, true);
            return Queries.createQuery((String)t.field(), (String)t.field(), (String)t.text(), (Queries.QueryKind)Queries.QueryKind.REGEXP);
        }
        throw new IllegalArgumentException();
    }

    @CheckForNull
    static Query scopeFilter(@NonNull Query q, @NonNull Set<? extends ClassIndex.SearchScopeType> scope) {
        assert (q != null);
        assert (scope != null);
        HashSet<? extends String> pkgs = null;
        for (ClassIndex.SearchScopeType s : scope) {
            Set<? extends String> sp = s.getPackages();
            if (sp == null) continue;
            if (pkgs == null) {
                pkgs = new HashSet<String>();
            }
            pkgs.addAll(sp);
        }
        if (pkgs == null) {
            return q;
        }
        switch (pkgs.size()) {
            case 0: {
                return null;
            }
            case 1: {
                BooleanQuery qFiltered = new BooleanQuery();
                qFiltered.add((Query)new TermQuery(new Term("packageName", (String)pkgs.iterator().next())), BooleanClause.Occur.MUST);
                qFiltered.add(q, BooleanClause.Occur.MUST);
                return qFiltered;
            }
        }
        BooleanQuery qPkgs = new BooleanQuery();
        for (String pkg : pkgs) {
            qPkgs.add((Query)new TermQuery(new Term("packageName", pkg)), BooleanClause.Occur.SHOULD);
        }
        BooleanQuery qFiltered = new BooleanQuery();
        qFiltered.add(q, BooleanClause.Occur.MUST);
        qFiltered.add((Query)qPkgs, BooleanClause.Occur.MUST);
        return qFiltered;
    }

    static Pair<StoppableConvertor<Term, String>, Term> createPackageFilter(@NullAllowed String prefix, boolean directOnly) {
        Term startTerm = new Term("packageName", prefix);
        PackageFilter filter = new PackageFilter(startTerm, directOnly);
        return Pair.of((Object)filter, (Object)startTerm);
    }

    private static class PackageFilter
    implements StoppableConvertor<Term, String> {
        private static final StoppableConvertor.Stop STOP = new StoppableConvertor.Stop();
        private final boolean directOnly;
        private final boolean all;
        private final String fieldName;
        private final String value;

        PackageFilter(@NonNull Term startTerm, boolean directOnly) {
            this.fieldName = startTerm.field();
            this.value = startTerm.text();
            this.directOnly = directOnly;
            this.all = this.value.length() == 0;
        }

        public String convert(Term currentTerm) throws StoppableConvertor.Stop {
            if (this.fieldName != currentTerm.field()) {
                throw STOP;
            }
            String currentText = currentTerm.text();
            if (this.all || currentText.startsWith(this.value)) {
                int index;
                if (this.directOnly && (index = currentText.indexOf(46, this.value.length())) > 0) {
                    currentText = currentText.substring(0, index);
                }
                return currentText;
            }
            return null;
        }
    }

}

