/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.api.java.queries.AnnotationProcessingQuery
 *  org.netbeans.spi.java.classpath.ClassPathImplementation
 *  org.netbeans.spi.java.classpath.FilteringPathResourceImplementation
 *  org.netbeans.spi.java.classpath.PathResourceImplementation
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.openide.filesystems.FileObject
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.java.source.classpath;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.queries.AnnotationProcessingQuery;
import org.netbeans.modules.java.source.classpath.AptCacheForSourceQuery;
import org.netbeans.modules.java.source.classpath.Function;
import org.netbeans.modules.java.source.indexing.APTUtils;
import org.netbeans.spi.java.classpath.ClassPathImplementation;
import org.netbeans.spi.java.classpath.FilteringPathResourceImplementation;
import org.netbeans.spi.java.classpath.PathResourceImplementation;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.openide.filesystems.FileObject;
import org.openide.util.WeakListeners;

public final class AptSourcePath
implements ClassPathImplementation,
PropertyChangeListener {
    private final ClassPath delegate;
    private final Function<List<ClassPath.Entry>, List<PathResourceImplementation>> fnc;
    private final PropertyChangeSupport pcSupport;
    private List<PathResourceImplementation> resources;
    private long eventId;

    private AptSourcePath(@NonNull ClassPath delegate, @NonNull Function<List<ClassPath.Entry>, List<PathResourceImplementation>> fnc) {
        assert (delegate != null);
        this.delegate = delegate;
        this.fnc = fnc;
        this.pcSupport = new PropertyChangeSupport(this);
        delegate.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)delegate));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public List<? extends PathResourceImplementation> getResources() {
        long currentEventId;
        AptSourcePath aptSourcePath = this;
        synchronized (aptSourcePath) {
            if (this.resources != null) {
                return this.resources;
            }
            currentEventId = this.eventId;
        }
        List<PathResourceImplementation> res = this.fnc.apply(this.delegate.entries());
        AptSourcePath aptSourcePath2 = this;
        synchronized (aptSourcePath2) {
            if (currentEventId == this.eventId) {
                if (this.resources == null) {
                    this.resources = res;
                } else {
                    res = this.resources;
                }
            }
        }
        assert (res != null);
        return res;
    }

    public void addPropertyChangeListener(@NonNull PropertyChangeListener listener) {
        assert (listener != null);
        this.pcSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(@NonNull PropertyChangeListener listener) {
        assert (listener != null);
        this.pcSupport.removePropertyChangeListener(listener);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        AptSourcePath aptSourcePath = this;
        synchronized (aptSourcePath) {
            this.resources = null;
            ++this.eventId;
        }
        this.pcSupport.firePropertyChange("resources", null, null);
    }

    public static ClassPathImplementation sources(@NonNull ClassPath cp) {
        assert (cp != null);
        return new AptSourcePath(cp, new MapToSources());
    }

    public static ClassPathImplementation aptCache(@NonNull ClassPath cp) {
        assert (cp != null);
        return new AptSourcePath(cp, new MapToAptCache());
    }

    public static ClassPathImplementation aptOputput(@NonNull ClassPath cp) {
        assert (cp != null);
        return new AptSourcePath(cp, new MapToAptGenerated());
    }

    @NonNull
    static Set<? extends URL> getAptBuildGeneratedFolders(@NonNull List<ClassPath.Entry> entries) {
        HashSet<URL> roots = new HashSet<URL>();
        for (ClassPath.Entry entry2 : entries) {
            roots.add(entry2.getURL());
        }
        for (ClassPath.Entry entry2 : entries) {
            APTUtils aptUtils;
            URL aptRoot;
            FileObject fo = entry2.getRoot();
            if (fo == null || !roots.contains(aptRoot = (aptUtils = APTUtils.getIfExist(fo)) != null ? aptUtils.sourceOutputDirectory() : AnnotationProcessingQuery.getAnnotationProcessingOptions((FileObject)fo).sourceOutputDirectory())) continue;
            return Collections.singleton(aptRoot);
        }
        return Collections.emptySet();
    }

    private static class FR
    implements FilteringPathResourceImplementation,
    PropertyChangeListener {
        private final ClassPath classPath;
        private final ClassPath.Entry entry;
        private final PropertyChangeSupport support;
        private final URL[] cache;

        public FR(ClassPath.Entry entry) {
            assert (entry != null);
            this.support = new PropertyChangeSupport(this);
            this.entry = entry;
            this.classPath = entry.getDefiningClassPath();
            this.classPath.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)this.classPath));
            this.cache = new URL[]{entry.getURL()};
        }

        public boolean includes(URL root, String resource) {
            assert (this.cache[0].equals(root));
            return this.entry.includes(resource);
        }

        public URL[] getRoots() {
            return this.cache;
        }

        public ClassPathImplementation getContent() {
            return null;
        }

        public void addPropertyChangeListener(PropertyChangeListener listener) {
            this.support.addPropertyChangeListener(listener);
        }

        public void removePropertyChangeListener(PropertyChangeListener listener) {
            this.support.removePropertyChangeListener(listener);
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("includes".equals(evt.getPropertyName())) {
                this.support.firePropertyChange("includes", null, null);
            }
        }
    }

    private static class MapToAptGenerated
    implements Function<List<ClassPath.Entry>, List<PathResourceImplementation>> {
        private MapToAptGenerated() {
        }

        @Override
        public List<PathResourceImplementation> apply(List<ClassPath.Entry> entries) {
            Set<? extends URL> aptGenerated = AptSourcePath.getAptBuildGeneratedFolders(entries);
            ArrayList<PathResourceImplementation> resources = new ArrayList<PathResourceImplementation>(aptGenerated.size());
            for (URL agr : aptGenerated) {
                resources.add(ClassPathSupport.createResource((URL)agr));
            }
            return resources;
        }
    }

    private static class MapToAptCache
    implements Function<List<ClassPath.Entry>, List<PathResourceImplementation>> {
        private MapToAptCache() {
        }

        @Override
        public List<PathResourceImplementation> apply(List<ClassPath.Entry> entries) {
            ArrayList<PathResourceImplementation> res = new ArrayList<PathResourceImplementation>();
            for (ClassPath.Entry entry : entries) {
                URL aptRoot = AptCacheForSourceQuery.getAptFolder(entry.getURL());
                if (aptRoot == null) continue;
                res.add(ClassPathSupport.createResource((URL)aptRoot));
            }
            return res;
        }
    }

    private static class MapToSources
    implements Function<List<ClassPath.Entry>, List<PathResourceImplementation>> {
        private MapToSources() {
        }

        @Override
        public List<PathResourceImplementation> apply(List<ClassPath.Entry> entries) {
            ArrayList<PathResourceImplementation> res = new ArrayList<PathResourceImplementation>();
            Set<? extends URL> aptBuildGenerated = AptSourcePath.getAptBuildGeneratedFolders(entries);
            for (ClassPath.Entry entry : entries) {
                if (aptBuildGenerated.contains(entry.getURL())) continue;
                res.add((PathResourceImplementation)new FR(entry));
            }
            return res;
        }
    }

}

