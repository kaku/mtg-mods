/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.AnnotatedTypeTree
 *  com.sun.source.tree.AnnotationTree
 *  com.sun.source.tree.ArrayAccessTree
 *  com.sun.source.tree.ArrayTypeTree
 *  com.sun.source.tree.AssertTree
 *  com.sun.source.tree.AssignmentTree
 *  com.sun.source.tree.BinaryTree
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.BreakTree
 *  com.sun.source.tree.CaseTree
 *  com.sun.source.tree.CatchTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.CompoundAssignmentTree
 *  com.sun.source.tree.ConditionalExpressionTree
 *  com.sun.source.tree.ContinueTree
 *  com.sun.source.tree.DoWhileLoopTree
 *  com.sun.source.tree.EmptyStatementTree
 *  com.sun.source.tree.EnhancedForLoopTree
 *  com.sun.source.tree.ErroneousTree
 *  com.sun.source.tree.ExpressionStatementTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.ForLoopTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.IfTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.InstanceOfTree
 *  com.sun.source.tree.IntersectionTypeTree
 *  com.sun.source.tree.LabeledStatementTree
 *  com.sun.source.tree.LambdaExpressionTree
 *  com.sun.source.tree.LiteralTree
 *  com.sun.source.tree.MemberReferenceTree
 *  com.sun.source.tree.MemberReferenceTree$ReferenceMode
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodInvocationTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.NewArrayTree
 *  com.sun.source.tree.NewClassTree
 *  com.sun.source.tree.ParameterizedTypeTree
 *  com.sun.source.tree.ParenthesizedTree
 *  com.sun.source.tree.PrimitiveTypeTree
 *  com.sun.source.tree.ReturnTree
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.SwitchTree
 *  com.sun.source.tree.SynchronizedTree
 *  com.sun.source.tree.ThrowTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.TreeVisitor
 *  com.sun.source.tree.TryTree
 *  com.sun.source.tree.TypeCastTree
 *  com.sun.source.tree.TypeParameterTree
 *  com.sun.source.tree.UnaryTree
 *  com.sun.source.tree.UnionTypeTree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.tree.WhileLoopTree
 *  com.sun.source.tree.WildcardTree
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.model.JavacElements
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCModifiers
 *  com.sun.tools.javac.util.Context
 */
package org.netbeans.modules.java.source.transform;

import com.sun.source.tree.AnnotatedTypeTree;
import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.ArrayAccessTree;
import com.sun.source.tree.ArrayTypeTree;
import com.sun.source.tree.AssertTree;
import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BinaryTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.BreakTree;
import com.sun.source.tree.CaseTree;
import com.sun.source.tree.CatchTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.CompoundAssignmentTree;
import com.sun.source.tree.ConditionalExpressionTree;
import com.sun.source.tree.ContinueTree;
import com.sun.source.tree.DoWhileLoopTree;
import com.sun.source.tree.EmptyStatementTree;
import com.sun.source.tree.EnhancedForLoopTree;
import com.sun.source.tree.ErroneousTree;
import com.sun.source.tree.ExpressionStatementTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.ForLoopTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.InstanceOfTree;
import com.sun.source.tree.IntersectionTypeTree;
import com.sun.source.tree.LabeledStatementTree;
import com.sun.source.tree.LambdaExpressionTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MemberReferenceTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.NewArrayTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.ParameterizedTypeTree;
import com.sun.source.tree.ParenthesizedTree;
import com.sun.source.tree.PrimitiveTypeTree;
import com.sun.source.tree.ReturnTree;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.SwitchTree;
import com.sun.source.tree.SynchronizedTree;
import com.sun.source.tree.ThrowTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TreeVisitor;
import com.sun.source.tree.TryTree;
import com.sun.source.tree.TypeCastTree;
import com.sun.source.tree.TypeParameterTree;
import com.sun.source.tree.UnaryTree;
import com.sun.source.tree.UnionTypeTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.tree.WhileLoopTree;
import com.sun.source.tree.WildcardTree;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.Context;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Name;
import javax.lang.model.element.QualifiedNameable;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.tools.JavaFileObject;
import org.netbeans.api.java.source.GeneratorUtilities;
import org.netbeans.api.java.source.TreeMaker;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.modules.java.source.builder.ASTService;
import org.netbeans.modules.java.source.builder.CommentHandlerService;
import org.netbeans.modules.java.source.builder.QualIdentTree;
import org.netbeans.modules.java.source.builder.TreeFactory;
import org.netbeans.modules.java.source.pretty.ImportAnalysis2;
import org.netbeans.modules.java.source.query.CommentHandler;
import org.netbeans.modules.java.source.save.ElementOverlay;

public class ImmutableTreeTranslator
implements TreeVisitor<Tree, Object> {
    public Element currentSym;
    protected TreeFactory make;
    protected Elements elements;
    protected CommentHandler comments;
    protected ASTService model;
    private ElementOverlay overlay;
    private ImportAnalysis2 importAnalysis;
    private Map<Tree, Object> tree2Tag;
    private TreeMaker tmaker;
    private WorkingCopy copy;

    public ImmutableTreeTranslator(WorkingCopy copy) {
        this.copy = copy;
        if (copy != null) {
            this.tmaker = copy.getTreeMaker();
        }
    }

    public void attach(Context context, ImportAnalysis2 importAnalysis, Map<Tree, Object> tree2Tag) {
        this.make = TreeFactory.instance(context);
        this.elements = JavacElements.instance((Context)context);
        this.comments = CommentHandlerService.instance(context);
        this.model = ASTService.instance(context);
        this.overlay = (ElementOverlay)context.get(ElementOverlay.class);
        this.importAnalysis = importAnalysis;
        this.tree2Tag = tree2Tag;
    }

    public void release() {
        this.make = null;
        this.comments = null;
        this.model = null;
        this.overlay = null;
        this.importAnalysis = null;
    }

    public Tree translate(Tree tree) {
        if (tree == null) {
            return null;
        }
        Tree t = (Tree)tree.accept((TreeVisitor)this, (Object)null);
        if (this.tree2Tag != null && tree != t && this.tmaker != null) {
            t = this.tmaker.asReplacementOf(t, tree, true);
            this.tree2Tag.put(t, this.tree2Tag.get((Object)tree));
        }
        return t;
    }

    public <T extends Tree> T translateClassRef(T tree) {
        return (T)this.translate((Tree)tree);
    }

    public final <T extends Tree> List<T> translateClassRef(List<T> trees) {
        if (trees == null || trees.isEmpty()) {
            return trees;
        }
        ArrayList<Tree> newTrees = new ArrayList<Tree>();
        boolean changed = false;
        for (Tree t : trees) {
            Tree newT = this.translateClassRef((T)t);
            if (newT != t) {
                changed = true;
            }
            if (newT == null) continue;
            newTrees.add(newT);
        }
        return changed ? newTrees : trees;
    }

    public <T extends Tree> T translateStable(T tree) {
        Tree t2 = this.translate((Tree)tree);
        if (t2 != null && t2.getClass() != tree.getClass() && t2.getClass() != tree.getClass() && (tree.getClass() != QualIdentTree.class || t2.getKind() != Tree.Kind.IDENTIFIER && t2.getKind() != Tree.Kind.MEMBER_SELECT)) {
            System.err.println("Rewrite stability problem: got " + t2.getClass() + "\n\t\texpected " + tree.getClass());
            return tree;
        }
        return (T)t2;
    }

    public static boolean isEmpty(Tree t) {
        if (t == null) {
            return true;
        }
        switch (t.getKind()) {
            default: {
                return false;
            }
            case BLOCK: {
                for (StatementTree stat : ((BlockTree)t).getStatements()) {
                    if (ImmutableTreeTranslator.isEmpty((Tree)stat)) continue;
                    return false;
                }
                return true;
            }
            case EMPTY_STATEMENT: 
        }
        return true;
    }

    public <T extends Tree> List<T> translate(List<T> trees) {
        if (trees == null || trees.isEmpty()) {
            return trees;
        }
        ArrayList<Tree> newTrees = new ArrayList<Tree>();
        boolean changed = false;
        for (Tree t : trees) {
            Tree newT = this.translate(t);
            if (newT != t) {
                changed = true;
            }
            if (newT == null) continue;
            newTrees.add(newT);
        }
        return changed ? newTrees : trees;
    }

    public <T extends Tree> List<T> translateStable(List<T> trees) {
        if (trees == null || trees.isEmpty()) {
            return trees;
        }
        ArrayList<Tree> newTrees = new ArrayList<Tree>();
        boolean changed = false;
        for (Tree t : trees) {
            Tree newT = this.translateStable((T)t);
            if (newT != t) {
                changed = true;
            }
            if (newT == null) continue;
            newTrees.add(newT);
        }
        return changed ? newTrees : trees;
    }

    protected <T extends Tree> List<T> optimize(List<T> trees) {
        if (trees == null || trees.isEmpty()) {
            return trees;
        }
        List newTrees = new ArrayList<T>();
        for (Tree t : trees) {
            if (t == null || ImmutableTreeTranslator.isEmpty(t)) continue;
            switch (t.getKind()) {
                case RETURN: 
                case THROW: 
                case BREAK: 
                case CONTINUE: {
                    newTrees.add((T)t);
                    return ImmutableTreeTranslator.equals(trees, newTrees) ? trees : newTrees;
                }
            }
            newTrees.add((T)t);
        }
        return ImmutableTreeTranslator.equals(trees, newTrees) ? trees : newTrees;
    }

    private static <T extends Tree> boolean equals(List<T> list1, List<T> list2) {
        int n = list1.size();
        if (n != list2.size()) {
            return false;
        }
        for (int i = 0; i < n; ++i) {
            if (list1.get(i) == list2.get(i)) continue;
            return false;
        }
        return true;
    }

    public final void copyCommentTo(Tree from1, Tree from2, Tree to) {
        this.copyCommentTo(from1, to);
        if (from1 != from2) {
            this.copyCommentTo(from2, to);
        }
    }

    public final void copyCommentTo(Tree from, Tree to) {
        this.comments.copyComments(from, to);
    }

    int size(List<?> list) {
        return list == null ? 0 : list.size();
    }

    private void copyPosTo(Tree from, Tree to) {
        this.model.setPos(to, this.model.getPos(from));
    }

    private boolean safeEquals(Object o1, Object o2) {
        if (o1 == null && o2 == null) {
            return true;
        }
        if (o1 == null || o2 == null) {
            return false;
        }
        return o1.equals(o2);
    }

    public Tree visitCompilationUnit(CompilationUnitTree tree, Object p) {
        CompilationUnitTree result = this.rewriteChildren(tree);
        return result;
    }

    public Tree visitImport(ImportTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitClass(ClassTree tree, Object p) {
        Element oldSym = this.currentSym;
        this.importAnalysis.classEntered(tree);
        this.currentSym = this.model.getElement((Tree)tree);
        ClassTree result = this.rewriteChildren(tree);
        this.importAnalysis.classLeft();
        this.currentSym = oldSym;
        return result;
    }

    public Tree visitMethod(MethodTree tree, Object p) {
        Element oldSym = this.currentSym;
        this.currentSym = this.model.getElement((Tree)tree);
        MethodTree result = this.rewriteChildren(tree);
        this.currentSym = oldSym;
        return result;
    }

    public Tree visitVariable(VariableTree tree, Object p) {
        Element oldSym = this.currentSym;
        this.currentSym = this.model.getElement((Tree)tree);
        VariableTree result = this.rewriteChildren(tree);
        this.currentSym = oldSym;
        return result;
    }

    public Tree visitEmptyStatement(EmptyStatementTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitBlock(BlockTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitUnionType(UnionTypeTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitDoWhileLoop(DoWhileLoopTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitWhileLoop(WhileLoopTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitForLoop(ForLoopTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitEnhancedForLoop(EnhancedForLoopTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitLabeledStatement(LabeledStatementTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitLambdaExpression(LambdaExpressionTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitSwitch(SwitchTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitCase(CaseTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitSynchronized(SynchronizedTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitTry(TryTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitCatch(CatchTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitConditionalExpression(ConditionalExpressionTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitIf(IfTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitExpressionStatement(ExpressionStatementTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitBreak(BreakTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitContinue(ContinueTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitReturn(ReturnTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitThrow(ThrowTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitAssert(AssertTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitMethodInvocation(MethodInvocationTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitNewClass(NewClassTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitNewArray(NewArrayTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitParenthesized(ParenthesizedTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitAssignment(AssignmentTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitCompoundAssignment(CompoundAssignmentTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitUnary(UnaryTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitBinary(BinaryTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitTypeCast(TypeCastTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitInstanceOf(InstanceOfTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitIntersectionType(IntersectionTypeTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitArrayAccess(ArrayAccessTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitMemberReference(MemberReferenceTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitMemberSelect(MemberSelectTree tree, Object p) {
        if (tree instanceof QualIdentTree) {
            QualIdentTree qit = (QualIdentTree)tree;
            Object el = qit.sym;
            if (el == null) {
                el = this.overlay.resolve(this.model, this.elements, qit.getFQN());
            } else if (el.getKind().isClass() || el.getKind().isInterface() || el.getKind() == ElementKind.PACKAGE) {
                el = this.overlay.resolve(this.model, this.elements, ((QualifiedNameable)el).getQualifiedName().toString());
            }
            return this.importAnalysis.resolveImport(tree, (Element)el);
        }
        return this.rewriteChildren(tree);
    }

    public Tree visitIdentifier(IdentifierTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitLiteral(LiteralTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitPrimitiveType(PrimitiveTypeTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitArrayType(ArrayTypeTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitParameterizedType(ParameterizedTypeTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitTypeParameter(TypeParameterTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitWildcard(WildcardTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitAnnotatedType(AnnotatedTypeTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitAnnotation(AnnotationTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitModifiers(ModifiersTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitErroneous(ErroneousTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public Tree visitOther(Tree tree, Object p) {
        throw new Error("Tree not overloaded: " + (Object)tree);
    }

    protected final CompilationUnitTree rewriteChildren(CompilationUnitTree tree) {
        ExpressionTree pid = (ExpressionTree)this.translate((Tree)tree.getPackageName());
        this.importAnalysis.setCompilationUnit(tree);
        this.importAnalysis.setPackage(tree.getPackageName());
        List imps = this.translate(tree.getImports());
        this.importAnalysis.setImports(imps);
        List<T> annotations = this.translate(tree.getPackageAnnotations());
        List<T> types = this.translate(tree.getTypeDecls());
        Set<? extends Element> newImports = this.importAnalysis.getImports();
        if (this.copy != null && newImports != null && !newImports.isEmpty()) {
            imps = GeneratorUtilities.get(this.copy).addImports(tree, newImports).getImports();
        }
        if (!(annotations.equals(tree.getPackageAnnotations()) && pid == tree.getPackageName() && imps.equals(tree.getImports()) && types.equals(tree.getTypeDecls()))) {
            CompilationUnitTree n = this.make.CompilationUnit(annotations, pid, imps, types, tree.getSourceFile());
            this.model.setElement((Tree)n, this.model.getElement((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
            tree = n;
        }
        return tree;
    }

    protected final ImportTree rewriteChildren(ImportTree tree) {
        Tree qualid = this.translateClassRef((T)tree.getQualifiedIdentifier());
        if (qualid == tree.getQualifiedIdentifier()) {
            qualid = this.translate(tree.getQualifiedIdentifier());
        }
        if (qualid != tree.getQualifiedIdentifier()) {
            ImportTree n = this.make.Import(qualid, tree.isStatic());
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final ClassTree rewriteChildren(ClassTree tree) {
        boolean typeChanged;
        ModifiersTree mods = (ModifiersTree)this.translate((Tree)tree.getModifiers());
        List<T> typarams = this.translateStable(tree.getTypeParameters());
        Tree extending = this.translateClassRef((T)tree.getExtendsClause());
        List<T> implementing = this.translateClassRef(tree.getImplementsClause());
        this.importAnalysis.enterVisibleThroughClasses(tree);
        List<T> defs = this.translate(tree.getMembers());
        boolean bl = typeChanged = !typarams.equals(tree.getTypeParameters()) || extending != tree.getExtendsClause() || !implementing.equals(tree.getImplementsClause());
        if (typeChanged || mods != tree.getModifiers() || !defs.equals(tree.getMembers())) {
            ClassTree n = this.make.Class(mods, tree.getSimpleName(), typarams, extending, implementing, defs);
            if (!typeChanged) {
                this.model.setElement((Tree)n, this.model.getElement((Tree)tree));
                this.model.setType((Tree)n, this.model.getType((Tree)tree));
            }
            this.copyCommentTo((Tree)tree, (Tree)n);
            if (tree.getMembers().size() == defs.size()) {
                this.model.setPos((Tree)n, this.model.getPos((Tree)tree));
            } else {
                this.copyPosTo((Tree)tree, (Tree)n);
            }
            tree = n;
        }
        return tree;
    }

    protected final MethodTree rewriteChildren(MethodTree tree) {
        ModifiersTree mods = (ModifiersTree)this.translate((Tree)tree.getModifiers());
        ExpressionTree restype = (ExpressionTree)this.translateClassRef((T)tree.getReturnType());
        List<T> typarams = this.translateStable(tree.getTypeParameters());
        List<T> params = this.translateStable(tree.getParameters());
        List<T> thrown = this.translate(tree.getThrows());
        ExpressionTree defaultValue = (ExpressionTree)this.translate(tree.getDefaultValue());
        BlockTree body = (BlockTree)this.translate((Tree)tree.getBody());
        if (!(restype == tree.getReturnType() && typarams.equals(tree.getTypeParameters()) && params.equals(tree.getParameters()) && thrown.equals(tree.getThrows()) && mods == tree.getModifiers() && defaultValue == tree.getDefaultValue() && body == tree.getBody())) {
            if ((((JCTree.JCModifiers)mods).flags & 0x1000000000L) != 0) {
                mods = this.make.Modifiers(((JCTree.JCModifiers)mods).flags & -68719476737L, mods.getAnnotations());
            }
            MethodTree n = this.make.Method(mods, tree.getName().toString(), (Tree)restype, typarams, params, thrown, body, defaultValue);
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final VariableTree rewriteChildren(VariableTree tree) {
        ModifiersTree mods = (ModifiersTree)this.translate((Tree)tree.getModifiers());
        ExpressionTree vartype = (ExpressionTree)this.translateClassRef((T)tree.getType());
        ExpressionTree init = (ExpressionTree)this.translate((Tree)tree.getInitializer());
        if (vartype != tree.getType() || mods != tree.getModifiers() || init != tree.getInitializer()) {
            VariableTree n = this.make.Variable(mods, tree.getName().toString(), (Tree)vartype, init);
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final EmptyStatementTree rewriteChildren(EmptyStatementTree tree) {
        return tree;
    }

    protected final BlockTree rewriteChildren(BlockTree tree) {
        List oldStats = tree.getStatements();
        List<T> newStats = this.translate(oldStats);
        if (!newStats.equals(oldStats)) {
            BlockTree n = this.make.Block(newStats, tree.isStatic());
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            if (newStats.size() == oldStats.size() && (oldStats.size() <= 0 || newStats.size() <= 0 || oldStats.get(0) == newStats.get(0) || this.model.getPos((Tree)oldStats.get(0)) >= 0 && this.model.getPos((Tree)newStats.get(0)) >= 0)) {
                this.copyPosTo((Tree)tree, (Tree)n);
            }
            tree = n;
        }
        return tree;
    }

    protected final UnionTypeTree rewriteChildren(UnionTypeTree tree) {
        List<T> newComponents = this.translate(tree.getTypeAlternatives());
        if (newComponents != tree.getTypeAlternatives()) {
            UnionTypeTree n = this.make.UnionType(newComponents);
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final DoWhileLoopTree rewriteChildren(DoWhileLoopTree tree) {
        StatementTree body = (StatementTree)this.translate((Tree)tree.getStatement());
        ExpressionTree cond = (ExpressionTree)this.translate((Tree)tree.getCondition());
        if (body != tree.getStatement() || cond != tree.getCondition()) {
            DoWhileLoopTree n = this.make.DoWhileLoop(cond, body);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final WhileLoopTree rewriteChildren(WhileLoopTree tree) {
        ExpressionTree cond = (ExpressionTree)this.translate((Tree)tree.getCondition());
        StatementTree body = (StatementTree)this.translate((Tree)tree.getStatement());
        if (cond != tree.getCondition() || body != tree.getStatement()) {
            WhileLoopTree n = this.make.WhileLoop(cond, body);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final ForLoopTree rewriteChildren(ForLoopTree tree) {
        List<T> init = this.translate(tree.getInitializer());
        ExpressionTree cond = (ExpressionTree)this.translate((Tree)tree.getCondition());
        List<T> step = this.translate(tree.getUpdate());
        StatementTree body = (StatementTree)this.translate((Tree)tree.getStatement());
        if (!init.equals(tree.getInitializer()) || cond != tree.getCondition() || !step.equals(tree.getUpdate()) || body != tree.getStatement()) {
            if (init != tree.getInitializer()) {
                init = this.optimize(init);
            }
            if (step != tree.getUpdate()) {
                step = this.optimize(step);
            }
            ForLoopTree n = this.make.ForLoop(init, cond, step, body);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            if (tree.getInitializer().size() != init.size() || tree.getUpdate().size() != step.size()) {
                this.model.setPos((Tree)tree, -2);
            } else {
                this.copyPosTo((Tree)tree, (Tree)n);
            }
            tree = n;
        }
        return tree;
    }

    protected final EnhancedForLoopTree rewriteChildren(EnhancedForLoopTree tree) {
        VariableTree var = (VariableTree)this.translate((Tree)tree.getVariable());
        ExpressionTree expr = (ExpressionTree)this.translate((Tree)tree.getExpression());
        StatementTree body = (StatementTree)this.translate((Tree)tree.getStatement());
        if (var != tree.getVariable() || expr != tree.getExpression() || body != tree.getStatement()) {
            EnhancedForLoopTree n = this.make.EnhancedForLoop(var, expr, body);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final LabeledStatementTree rewriteChildren(LabeledStatementTree tree) {
        StatementTree body = (StatementTree)this.translate((Tree)tree.getStatement());
        if (body != tree.getStatement()) {
            LabeledStatementTree n = this.make.LabeledStatement(tree.getLabel(), body);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final LambdaExpressionTree rewriteChildren(LambdaExpressionTree tree) {
        Tree body = this.translate(tree.getBody());
        List<T> parameters = this.translate(tree.getParameters());
        if (body != tree.getBody() || parameters != tree.getParameters()) {
            LambdaExpressionTree n = this.make.LambdaExpression(parameters, body);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final SwitchTree rewriteChildren(SwitchTree tree) {
        ExpressionTree selector = (ExpressionTree)this.translate((Tree)tree.getExpression());
        List<T> cases = this.translateStable(tree.getCases());
        if (selector != tree.getExpression() || !cases.equals(tree.getCases())) {
            SwitchTree n = this.make.Switch(selector, cases);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final CaseTree rewriteChildren(CaseTree tree) {
        ExpressionTree pat = (ExpressionTree)this.translate((Tree)tree.getExpression());
        List<T> stats = this.translate(tree.getStatements());
        if (pat != tree.getExpression() || !stats.equals(tree.getStatements())) {
            if (stats != tree.getStatements()) {
                stats = this.optimize(stats);
            }
            CaseTree n = this.make.Case(pat, stats);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final SynchronizedTree rewriteChildren(SynchronizedTree tree) {
        ExpressionTree lock = (ExpressionTree)this.translate((Tree)tree.getExpression());
        BlockTree body = (BlockTree)this.translate((Tree)tree.getBlock());
        if (lock != tree.getExpression() || body != tree.getBlock()) {
            SynchronizedTree n = this.make.Synchronized(lock, body);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final TryTree rewriteChildren(TryTree tree) {
        List<T> resources = this.translate(tree.getResources());
        BlockTree body = (BlockTree)this.translate((Tree)tree.getBlock());
        List<T> catches = this.translateStable(tree.getCatches());
        BlockTree finalizer = (BlockTree)this.translate((Tree)tree.getFinallyBlock());
        if (body != tree.getBlock() || !catches.equals(tree.getCatches()) || finalizer != tree.getFinallyBlock() || !resources.equals(tree.getResources())) {
            TryTree n = this.make.Try(resources, body, catches, finalizer);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final CatchTree rewriteChildren(CatchTree tree) {
        VariableTree param = this.translateStable((T)tree.getParameter());
        BlockTree body = (BlockTree)this.translate((Tree)tree.getBlock());
        if (param != tree.getParameter() || body != tree.getBlock()) {
            CatchTree n = this.make.Catch(param, body);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final ConditionalExpressionTree rewriteChildren(ConditionalExpressionTree tree) {
        ExpressionTree cond = (ExpressionTree)this.translate((Tree)tree.getCondition());
        ExpressionTree truepart = (ExpressionTree)this.translate((Tree)tree.getTrueExpression());
        ExpressionTree falsepart = (ExpressionTree)this.translate((Tree)tree.getFalseExpression());
        if (cond != tree.getCondition() || truepart != tree.getTrueExpression() || falsepart != tree.getFalseExpression()) {
            ConditionalExpressionTree n = this.make.ConditionalExpression(cond, truepart, falsepart);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final IfTree rewriteChildren(IfTree tree) {
        ExpressionTree cond = (ExpressionTree)this.translate((Tree)tree.getCondition());
        StatementTree thenpart = (StatementTree)this.translate((Tree)tree.getThenStatement());
        StatementTree elsepart = (StatementTree)this.translate((Tree)tree.getElseStatement());
        if (cond != tree.getCondition() || thenpart != tree.getThenStatement() || elsepart != tree.getElseStatement()) {
            IfTree n = this.make.If(cond, thenpart, elsepart);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final ExpressionStatementTree rewriteChildren(ExpressionStatementTree tree) {
        ExpressionTree expr = (ExpressionTree)this.translate((Tree)tree.getExpression());
        if (expr != tree.getExpression()) {
            ExpressionStatementTree n = this.make.ExpressionStatement(expr);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final BreakTree rewriteChildren(BreakTree tree) {
        return tree;
    }

    protected final ContinueTree rewriteChildren(ContinueTree tree) {
        return tree;
    }

    protected final ReturnTree rewriteChildren(ReturnTree tree) {
        ExpressionTree expr = (ExpressionTree)this.translate((Tree)tree.getExpression());
        if (expr != tree.getExpression()) {
            ReturnTree n = this.make.Return(expr);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final ThrowTree rewriteChildren(ThrowTree tree) {
        ExpressionTree expr = (ExpressionTree)this.translate((Tree)tree.getExpression());
        if (expr != tree.getExpression()) {
            ThrowTree n = this.make.Throw(expr);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final AssertTree rewriteChildren(AssertTree tree) {
        ExpressionTree cond = (ExpressionTree)this.translate((Tree)tree.getCondition());
        ExpressionTree detail = (ExpressionTree)this.translate((Tree)tree.getDetail());
        if (cond != tree.getCondition() || detail != tree.getDetail()) {
            AssertTree n = this.make.Assert(cond, detail);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final MethodInvocationTree rewriteChildren(MethodInvocationTree tree) {
        List<T> typeargs = this.translate(tree.getTypeArguments());
        ExpressionTree meth = (ExpressionTree)this.translate((Tree)tree.getMethodSelect());
        List<T> args = this.translate(tree.getArguments());
        if (!typeargs.equals(tree.getTypeArguments()) || meth != tree.getMethodSelect() || !args.equals(tree.getArguments())) {
            if (args != tree.getArguments()) {
                args = this.optimize(args);
            }
            if (typeargs != tree.getTypeArguments()) {
                typeargs = this.optimize(typeargs);
            }
            MethodInvocationTree n = this.make.MethodInvocation(typeargs, meth, args);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            tree = n;
            if (this.size(tree.getTypeArguments()) != this.size(typeargs) && this.size(tree.getArguments()) != this.size(args)) {
                this.model.setPos((Tree)tree, -2);
            } else {
                this.copyPosTo((Tree)tree, (Tree)n);
            }
        }
        return tree;
    }

    protected final NewClassTree rewriteChildren(NewClassTree tree) {
        ExpressionTree encl = (ExpressionTree)this.translate((Tree)tree.getEnclosingExpression());
        List<T> typeargs = this.translate(tree.getTypeArguments());
        ExpressionTree clazz = this.translateClassRef((T)tree.getIdentifier());
        List<T> args = this.translate(tree.getArguments());
        ClassTree def = this.translateStable((T)tree.getClassBody());
        if (encl != tree.getEnclosingExpression() || !typeargs.equals(tree.getTypeArguments()) || clazz != tree.getIdentifier() || !args.equals(tree.getArguments()) || def != tree.getClassBody()) {
            if (args != tree.getArguments()) {
                args = this.optimize(args);
            }
            if (typeargs != tree.getTypeArguments()) {
                typeargs = this.optimize(typeargs);
            }
            NewClassTree n = this.make.NewClass(encl, typeargs, clazz, args, def);
            this.model.setElement((Tree)n, this.model.getElement((Tree)tree));
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            if (this.size(tree.getTypeArguments()) != this.size(typeargs) && this.size(tree.getArguments()) != this.size(args) || def != tree.getClassBody()) {
                this.model.setPos((Tree)n, -2);
            } else {
                this.copyPosTo((Tree)tree, (Tree)n);
            }
            tree = n;
        }
        return tree;
    }

    protected final NewArrayTree rewriteChildren(NewArrayTree tree) {
        ExpressionTree elemtype = (ExpressionTree)this.translateClassRef((T)tree.getType());
        List<T> dims = this.translate(tree.getDimensions());
        List<T> elems = this.translate(tree.getInitializers());
        if (elemtype != tree.getType() || !this.safeEquals(dims, tree.getDimensions()) || !this.safeEquals(elems, tree.getInitializers())) {
            if (elems != tree.getInitializers()) {
                elems = this.optimize(elems);
            }
            if (dims != tree.getDimensions()) {
                dims = this.optimize(dims);
            }
            NewArrayTree n = this.make.NewArray((Tree)elemtype, dims, elems);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            tree = n;
            if (this.size(tree.getDimensions()) != this.size(dims) || this.size(tree.getInitializers()) != this.size(elems)) {
                this.model.setPos((Tree)tree, -2);
            } else {
                this.copyPosTo((Tree)tree, (Tree)n);
            }
        }
        return tree;
    }

    protected final ParenthesizedTree rewriteChildren(ParenthesizedTree tree) {
        ExpressionTree expr = (ExpressionTree)this.translate((Tree)tree.getExpression());
        if (expr != tree.getExpression()) {
            ParenthesizedTree n = this.make.Parenthesized(expr);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final AssignmentTree rewriteChildren(AssignmentTree tree) {
        ExpressionTree lhs = (ExpressionTree)this.translate((Tree)tree.getVariable());
        ExpressionTree rhs = (ExpressionTree)this.translate((Tree)tree.getExpression());
        if (lhs != tree.getVariable() || rhs != tree.getExpression()) {
            AssignmentTree n = this.make.Assignment(lhs, rhs);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final CompoundAssignmentTree rewriteChildren(CompoundAssignmentTree tree) {
        ExpressionTree lhs = (ExpressionTree)this.translate((Tree)tree.getVariable());
        ExpressionTree rhs = (ExpressionTree)this.translate((Tree)tree.getExpression());
        if (lhs != tree.getVariable() || rhs != tree.getExpression()) {
            CompoundAssignmentTree n = this.make.CompoundAssignment(tree.getKind(), lhs, rhs);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final UnaryTree rewriteChildren(UnaryTree tree) {
        ExpressionTree arg = (ExpressionTree)this.translate((Tree)tree.getExpression());
        if (arg != tree.getExpression()) {
            UnaryTree n = this.make.Unary(tree.getKind(), arg);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final BinaryTree rewriteChildren(BinaryTree tree) {
        ExpressionTree lhs = (ExpressionTree)this.translate((Tree)tree.getLeftOperand());
        ExpressionTree rhs = (ExpressionTree)this.translate((Tree)tree.getRightOperand());
        if (lhs != tree.getLeftOperand() || rhs != tree.getRightOperand()) {
            BinaryTree n = this.make.Binary(tree.getKind(), lhs, rhs);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final TypeCastTree rewriteChildren(TypeCastTree tree) {
        Tree clazz = this.translateClassRef((T)tree.getType());
        ExpressionTree expr = (ExpressionTree)this.translate((Tree)tree.getExpression());
        if (clazz != tree.getType() || expr != tree.getExpression()) {
            TypeCastTree n = this.make.TypeCast(clazz, expr);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final InstanceOfTree rewriteChildren(InstanceOfTree tree) {
        ExpressionTree expr = (ExpressionTree)this.translate((Tree)tree.getExpression());
        Tree clazz = this.translateClassRef((T)tree.getType());
        if (expr != tree.getExpression() || clazz != tree.getType()) {
            InstanceOfTree n = this.make.InstanceOf(expr, clazz);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final IntersectionTypeTree rewriteChildren(IntersectionTypeTree tree) {
        List<T> bounds = this.translate(tree.getBounds());
        if (!this.safeEquals(bounds, tree.getBounds())) {
            IntersectionTypeTree n = this.make.IntersectionType(bounds);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final ArrayAccessTree rewriteChildren(ArrayAccessTree tree) {
        ExpressionTree array = (ExpressionTree)this.translate((Tree)tree.getExpression());
        ExpressionTree index = (ExpressionTree)this.translate((Tree)tree.getIndex());
        if (array != tree.getExpression() || index != tree.getIndex()) {
            ArrayAccessTree n = this.make.ArrayAccess(array, index);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final MemberSelectTree rewriteChildren(MemberSelectTree tree) {
        ExpressionTree selected = this.translateClassRef((T)tree.getExpression());
        if (selected != tree.getExpression()) {
            MemberSelectTree n = this.make.MemberSelect(selected, tree.getIdentifier());
            this.model.setElement((Tree)n, this.model.getElement((Tree)tree));
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final IdentifierTree rewriteChildren(IdentifierTree tree) {
        return tree;
    }

    protected final LiteralTree rewriteChildren(LiteralTree tree) {
        return tree;
    }

    protected final PrimitiveTypeTree rewriteChildren(PrimitiveTypeTree tree) {
        return tree;
    }

    protected final ArrayTypeTree rewriteChildren(ArrayTypeTree tree) {
        ExpressionTree elemtype = (ExpressionTree)this.translateClassRef((T)tree.getType());
        if (elemtype != tree.getType()) {
            ArrayTypeTree n = this.make.ArrayType((Tree)elemtype);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final ParameterizedTypeTree rewriteChildren(ParameterizedTypeTree tree) {
        ExpressionTree clazz = (ExpressionTree)this.translateClassRef((T)tree.getType());
        List<T> arguments = this.translate(tree.getTypeArguments());
        if (clazz != tree.getType() || !arguments.equals(tree.getTypeArguments())) {
            if (arguments != tree.getTypeArguments()) {
                arguments = this.optimize(arguments);
            }
            ParameterizedTypeTree n = this.make.ParameterizedType((Tree)clazz, arguments);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            tree = n;
            if (tree.getTypeArguments().size() != arguments.size()) {
                this.model.setPos((Tree)tree, -2);
            } else {
                this.copyPosTo((Tree)tree, (Tree)n);
            }
        }
        return tree;
    }

    protected final TypeParameterTree rewriteChildren(TypeParameterTree tree) {
        List<T> bounds = this.translate(tree.getBounds());
        if (!bounds.equals(tree.getBounds())) {
            bounds = this.optimize(bounds);
            TypeParameterTree n = this.make.TypeParameter(tree.getName(), bounds);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            tree = n;
            if (tree.getBounds().size() != bounds.size()) {
                this.model.setPos((Tree)tree, -2);
            } else {
                this.copyPosTo((Tree)tree, (Tree)n);
            }
        }
        return tree;
    }

    protected final WildcardTree rewriteChildren(WildcardTree tree) {
        Tree type = this.translateClassRef((T)tree.getBound());
        if (type != tree.getBound()) {
            WildcardTree n = this.make.Wildcard(tree.getKind(), type);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final AnnotatedTypeTree rewriteChildren(AnnotatedTypeTree tree) {
        List<T> annotations = this.translate(tree.getAnnotations());
        ExpressionTree underlyingType = (ExpressionTree)this.translate((Tree)tree.getUnderlyingType());
        if (!annotations.equals(tree.getAnnotations()) || underlyingType != tree.getUnderlyingType()) {
            AnnotatedTypeTree n = this.make.AnnotatedType(annotations, (Tree)underlyingType);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final AnnotationTree rewriteChildren(AnnotationTree tree) {
        Tree annotationType = this.translate(tree.getAnnotationType());
        List<T> args = this.translate(tree.getArguments());
        if (annotationType != tree.getAnnotationType() || !args.equals(tree.getArguments())) {
            if (args != tree.getArguments()) {
                args = this.optimize(args);
            }
            AnnotationTree n = tree.getKind() == Tree.Kind.ANNOTATION ? this.make.Annotation(annotationType, args) : this.make.TypeAnnotation(annotationType, args);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            tree = n;
            if (tree.getArguments().size() != args.size()) {
                this.model.setPos((Tree)tree, -2);
            } else {
                this.copyPosTo((Tree)tree, (Tree)n);
            }
        }
        return tree;
    }

    protected final ModifiersTree rewriteChildren(ModifiersTree tree) {
        List<T> annotations = this.translateStable(tree.getAnnotations());
        if (!annotations.equals(tree.getAnnotations())) {
            ModifiersTree n = this.make.Modifiers(tree, annotations);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    protected final ErroneousTree rewriteChildren(ErroneousTree tree) {
        List oldErrs = tree.getErrorTrees();
        List<T> newErrs = this.translate(oldErrs);
        if (!newErrs.equals(oldErrs)) {
            ErroneousTree n = this.make.Erroneous(newErrs);
            this.model.setType((Tree)n, this.model.getType((Tree)tree));
            this.copyCommentTo((Tree)tree, (Tree)n);
            this.copyPosTo((Tree)tree, (Tree)n);
            tree = n;
        }
        return tree;
    }

    private Tree rewriteChildren(MemberReferenceTree node) {
        ExpressionTree qualifierExpression = (ExpressionTree)this.translate((Tree)node.getQualifierExpression());
        List<T> typeArguments = this.translate(node.getTypeArguments());
        if (qualifierExpression != node.getQualifierExpression() || typeArguments != node.getTypeArguments()) {
            MemberReferenceTree n = this.make.MemberReference(node.getMode(), node.getName(), qualifierExpression, typeArguments);
            this.model.setType((Tree)n, this.model.getType((Tree)node));
            this.copyCommentTo((Tree)node, (Tree)n);
            this.copyPosTo((Tree)node, (Tree)n);
            node = n;
        }
        return node;
    }

}

