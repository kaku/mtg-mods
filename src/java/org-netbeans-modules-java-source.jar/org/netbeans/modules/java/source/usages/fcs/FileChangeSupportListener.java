/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.source.usages.fcs;

import java.util.EventListener;
import org.netbeans.modules.java.source.usages.fcs.FileChangeSupportEvent;

public interface FileChangeSupportListener
extends EventListener {
    public void fileCreated(FileChangeSupportEvent var1);

    public void fileDeleted(FileChangeSupportEvent var1);

    public void fileModified(FileChangeSupportEvent var1);
}

