/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.ParameterizedTypeTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  org.netbeans.modules.refactoring.api.AbstractRefactoring
 *  org.netbeans.modules.refactoring.api.Problem
 *  org.netbeans.modules.refactoring.spi.RefactoringElementsBag
 *  org.netbeans.modules.refactoring.spi.RefactoringPlugin
 *  org.netbeans.modules.refactoring.spi.RefactoringPluginFactory
 */
package org.netbeans.modules.java.source.save;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.ParameterizedTypeTree;
import com.sun.source.tree.Tree;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ElementVisitor;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.NestingKind;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.QualifiedNameable;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import org.netbeans.modules.java.source.builder.ASTService;
import org.netbeans.modules.java.source.builder.QualIdentTree;
import org.netbeans.modules.refactoring.api.AbstractRefactoring;
import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.spi.RefactoringElementsBag;
import org.netbeans.modules.refactoring.spi.RefactoringPlugin;
import org.netbeans.modules.refactoring.spi.RefactoringPluginFactory;

public class ElementOverlay {
    private static final Logger LOG = Logger.getLogger(ElementOverlay.class.getName());
    private static final ThreadLocal<ElementOverlay> transactionOverlay = new ThreadLocal();
    private final Map<String, List<String>> class2Enclosed = new HashMap<String, List<String>>();
    private final Map<String, Collection<String>> class2SuperElementTrees = new HashMap<String, Collection<String>>();
    private final Set<String> packages = new HashSet<String>();
    private final Map<String, Set<Modifier>> classes = new HashMap<String, Set<Modifier>>();
    private final Map<String, Element> elementCache = new HashMap<String, Element>();

    public static void beginTransaction() {
        transactionOverlay.set(new ElementOverlay());
        LOG.log(Level.FINE, "transaction started");
    }

    public static void endTransaction() {
        transactionOverlay.set(null);
        LOG.log(Level.FINE, "transaction end");
    }

    public static ElementOverlay getOrCreateOverlay() {
        ElementOverlay overlay = transactionOverlay.get();
        if (overlay == null) {
            overlay = new ElementOverlay();
        }
        return overlay;
    }

    private ElementOverlay() {
    }

    public List<Element> getEnclosedElements(ASTService ast, Elements elements, String parent) {
        LinkedList<Element> result = new LinkedList<Element>();
        List<String> enclosed = this.class2Enclosed.get(parent);
        if (enclosed != null) {
            for (String enc : enclosed) {
                Element el = this.resolve(ast, elements, enc);
                if (el == null) continue;
                result.add(el);
            }
            return result;
        }
        Element parentEl = this.resolve(ast, elements, parent);
        if (parentEl == null) {
            throw new IllegalStateException(parent);
        }
        if (parentEl instanceof FakeTypeElement) {
            TypeElement original = elements.getTypeElement(parent);
            if (original != null) {
                result.addAll(this.wrap(ast, elements, original.getEnclosedElements()));
            }
        } else if (parentEl instanceof FakePackageElement) {
            PackageElement original = elements.getPackageElement(parent);
            if (original != null) {
                result.addAll(this.wrap(ast, elements, original.getEnclosedElements()));
            }
        } else {
            result.addAll(parentEl.getEnclosedElements());
        }
        return result;
    }

    private Element createElement(ASTService ast, Elements elements, String name, Element original) {
        Element el = this.elementCache.get(name);
        if (el == null) {
            if (original != null) {
                if (original.getKind().isClass() || original.getKind().isInterface()) {
                    el = new TypeElementWrapper(ast, elements, (TypeElement)original);
                    this.elementCache.put(name, (TypeElementWrapper)el);
                    return el;
                }
                if (original.getKind() == ElementKind.PACKAGE) {
                    el = new PackageElementWrapper(ast, elements, (PackageElement)original);
                    this.elementCache.put(name, (PackageElementWrapper)el);
                    return el;
                }
                return original;
            }
            int lastDot = name.lastIndexOf(46);
            Name simpleName = elements.getName(name.substring(lastDot + 1));
            Name fqnName = elements.getName(name);
            if (this.classes.containsKey(name)) {
                PackageElement parent = lastDot > 0 ? this.resolve(ast, elements, name.substring(0, lastDot)) : elements.getPackageElement("");
                el = new FakeTypeElement(ast, elements, simpleName, fqnName, name, parent, this.classes.get(name));
                this.elementCache.put(name, (FakeTypeElement)el);
            } else if (this.packages.contains(name)) {
                el = new FakePackageElement(ast, elements, fqnName, name, simpleName);
                this.elementCache.put(name, (FakePackageElement)el);
            } else {
                return null;
            }
        }
        return el;
    }

    public Element getOriginal(Element e) {
        if (e instanceof TypeElementWrapper) {
            return ((TypeElementWrapper)e).delegateTo;
        }
        if (e instanceof PackageElementWrapper) {
            return ((PackageElementWrapper)e).delegateTo;
        }
        return e;
    }

    public Element resolve(ASTService ast, Elements elements, String what) {
        Element result = null;
        if (this.classes.containsKey(what)) {
            result = this.createElement(ast, elements, what, null);
        }
        if (result == null) {
            result = elements.getTypeElement(what);
        }
        if (result == null) {
            result = elements.getPackageElement(what);
        }
        result = this.createElement(ast, elements, what, result);
        return result;
    }

    public void registerClass(String parent, String clazz, ClassTree tree, boolean modified) {
        LinkedHashSet<String> original;
        boolean newOrModified;
        if (clazz == null) {
            return;
        }
        Element myself = ASTService.getElementImpl((Tree)tree);
        boolean bl = newOrModified = myself == null || !myself.getKind().isClass() && !myself.getKind().isInterface() || !((QualifiedNameable)myself).getQualifiedName().contentEquals(clazz);
        if (newOrModified || this.class2Enclosed.containsKey(parent)) {
            List<String> c = this.class2Enclosed.get(parent);
            if (c == null) {
                c = new ArrayList<String>();
                this.class2Enclosed.put(parent, c);
            }
            c.add(clazz);
        }
        if (modified) {
            this.class2Enclosed.put(clazz, new ArrayList());
        }
        Set<String> superFQNs = this.superFQNs(tree);
        boolean hadObject = superFQNs.remove("java.lang.Object");
        if (!newOrModified) {
            original = new LinkedHashSet<String>();
            TypeElement tel = (TypeElement)myself;
            if (tel.getSuperclass() != null && tel.getSuperclass().getKind() == TypeKind.DECLARED) {
                original.add(((TypeElement)((DeclaredType)tel.getSuperclass()).asElement()).getQualifiedName().toString());
            }
            for (TypeMirror intf : tel.getInterfaces()) {
                original.add(((TypeElement)((DeclaredType)intf).asElement()).getQualifiedName().toString());
            }
            original.remove("java.lang.Object");
        } else {
            original = null;
        }
        if (!superFQNs.equals(original)) {
            if (hadObject) {
                superFQNs.add("java.lang.Object");
            }
            EnumSet<Modifier> mods = EnumSet.noneOf(Modifier.class);
            mods.addAll(tree.getModifiers().getFlags());
            this.classes.put(clazz, mods);
            this.class2SuperElementTrees.put(clazz, superFQNs);
        }
    }

    private Set<String> superFQNs(ClassTree tree) {
        String fqn;
        LinkedHashSet<String> superFQNs = new LinkedHashSet<String>(tree.getImplementsClause().size() + 1);
        if (tree.getExtendsClause() != null && (fqn = this.fqnFor(tree.getExtendsClause())) != null) {
            superFQNs.add(fqn);
        }
        for (Tree i : tree.getImplementsClause()) {
            String fqn2 = this.fqnFor(i);
            if (fqn2 == null) continue;
            superFQNs.add(fqn2);
        }
        return superFQNs;
    }

    public void registerPackage(String currentPackage) {
        this.packages.add(currentPackage);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public Iterable<? extends Element> getAllSuperElements(ASTService ast, Elements elements, Element forElement) {
        LinkedList<Element> result = new LinkedList<Element>();
        if (forElement instanceof FakeTypeElement) {
            for (String fqn : this.class2SuperElementTrees.get(((FakeTypeElement)forElement).fqnString)) {
                Element el = this.resolve(ast, elements, fqn);
                if (el != null) {
                    result.add(el);
                    continue;
                }
                Logger.getLogger(ElementOverlay.class.getName()).log(Level.SEVERE, "Cannot resolve {0} to element", fqn);
            }
            return result;
        } else {
            if (!forElement.getKind().isClass() && !forElement.getKind().isInterface()) return result;
            this.addElement(ast, elements, ((TypeElement)forElement).getSuperclass(), result);
            for (TypeMirror i : ((TypeElement)forElement).getInterfaces()) {
                this.addElement(ast, elements, i, result);
            }
        }
        return result;
    }

    private String fqnFor(Tree t) {
        Element el = ASTService.getElementImpl(t);
        if (el != null) {
            if (el.getKind().isClass() || el.getKind().isInterface() || el.getKind() == ElementKind.PACKAGE) {
                return ((QualifiedNameable)el).getQualifiedName().toString();
            }
            Logger.getLogger(ElementOverlay.class.getName()).log(Level.SEVERE, "Not a QualifiedNameable: {0}", el);
            return null;
        }
        if (t instanceof QualIdentTree) {
            return ((QualIdentTree)t).getFQN();
        }
        if (t.getKind() == Tree.Kind.PARAMETERIZED_TYPE) {
            return this.fqnFor(((ParameterizedTypeTree)t).getType());
        }
        Logger.getLogger(ElementOverlay.class.getName()).log(Level.FINE, "No element and no QualIdent");
        return null;
    }

    private void addElement(ASTService ast, Elements elements, TypeMirror tm, List<Element> result) {
        if (tm == null || tm.getKind() != TypeKind.DECLARED) {
            return;
        }
        String fqn = ((TypeElement)((DeclaredType)tm).asElement()).getQualifiedName().toString();
        Element resolved = this.resolve(ast, elements, fqn);
        if (resolved != null) {
            result.add(resolved);
        } else {
            Logger.getLogger(ElementOverlay.class.getName()).log(Level.FINE, "cannot resolve {0}", fqn);
        }
    }

    public Element wrap(ASTService ast, Elements elements, Element original) {
        if (original == null) {
            return null;
        }
        if (original.getKind().isClass() || original.getKind().isInterface()) {
            return this.resolve(ast, elements, ((TypeElement)original).getQualifiedName().toString());
        }
        return original;
    }

    private List<? extends Element> wrap(ASTService ast, Elements elements, Collection<? extends Element> original) {
        ArrayList<Element> result = new ArrayList<Element>(original.size());
        for (Element el : original) {
            Element wrapped = this.wrap(ast, elements, el);
            if (wrapped == null) continue;
            result.add(wrapped);
        }
        return result;
    }

    public void clearElementsCache() {
        this.elementCache.clear();
    }

    public int totalMapsSize() {
        return this.class2Enclosed.size() + this.class2SuperElementTrees.size() + this.classes.size() + this.elementCache.size() + this.packages.size();
    }

    public Collection<? extends Element> getAllVisibleThrough(ASTService ast, Elements elements, String what, ClassTree tree) {
        Element current;
        ArrayList<? extends Element> result = new ArrayList<Element>();
        Element element = current = what != null ? this.resolve(ast, elements, what) : null;
        if (current == null) {
            for (String sup : this.superFQNs(tree)) {
                Element c = this.resolve(ast, elements, sup);
                if (c == null) continue;
                result.add(c);
            }
        } else {
            result.addAll(this.getAllMembers(ast, elements, current));
        }
        return result;
    }

    private Collection<? extends Element> getAllMembers(ASTService ast, Elements elements, Element el) {
        ArrayList<Element> result = new ArrayList<Element>();
        result.addAll(el.getEnclosedElements());
        for (Element parent : this.getAllSuperElements(ast, elements, el)) {
            if (el.equals(parent)) continue;
            result.addAll(this.getAllMembers(ast, elements, parent));
        }
        return result;
    }

    public PackageElement unnamedPackage(ASTService ast, Elements elements) {
        return (PackageElement)this.resolve(ast, elements, "");
    }

    public static class RunLastFactory
    implements RefactoringPluginFactory {
        public RefactoringPlugin createInstance(AbstractRefactoring refactoring) {
            return new RefactoringPlugin(){

                public Problem preCheck() {
                    return null;
                }

                public Problem checkParameters() {
                    return null;
                }

                public Problem fastCheckParameters() {
                    return null;
                }

                public void cancelRequest() {
                    ElementOverlay.endTransaction();
                }

                public Problem prepare(RefactoringElementsBag refactoringElements) {
                    ElementOverlay.endTransaction();
                    return null;
                }
            };
        }

    }

    public static class RunFirstFactory
    implements RefactoringPluginFactory {
        public RefactoringPlugin createInstance(AbstractRefactoring refactoring) {
            return new RefactoringPlugin(){

                public Problem preCheck() {
                    return null;
                }

                public Problem checkParameters() {
                    return null;
                }

                public Problem fastCheckParameters() {
                    return null;
                }

                public void cancelRequest() {
                    ElementOverlay.endTransaction();
                }

                public Problem prepare(RefactoringElementsBag refactoringElements) {
                    ElementOverlay.beginTransaction();
                    return null;
                }
            };
        }

    }

    public static class FQNComputer {
        private final StringBuilder fqn = new StringBuilder();
        private int anonymousCounter = 0;

        public void setCompilationUnit(CompilationUnitTree cut) {
            this.setPackageNameTree(cut.getPackageName());
        }

        public void enterClass(ClassTree ct) {
            if (ct.getSimpleName() == null || ct.getSimpleName().length() == 0 || this.anonymousCounter > 0) {
                ++this.anonymousCounter;
            } else {
                if (this.fqn.length() > 0) {
                    this.fqn.append('.');
                }
                this.fqn.append(ct.getSimpleName());
            }
        }

        public void leaveClass() {
            if (this.anonymousCounter > 0) {
                --this.anonymousCounter;
            } else {
                int dot = Math.max(0, this.fqn.lastIndexOf("."));
                this.fqn.delete(dot, this.fqn.length());
            }
        }

        public String getFQN() {
            if (this.anonymousCounter > 0) {
                return null;
            }
            return this.fqn.toString();
        }

        public void setPackageNameTree(ExpressionTree packageNameTree) {
            this.fqn.delete(0, this.fqn.length());
            if (packageNameTree != null) {
                this.fqn.append(packageNameTree.toString());
            }
        }
    }

    private class PackageElementWrapper
    implements PackageElement {
        private final ASTService ast;
        private final Elements elements;
        private final PackageElement delegateTo;

        public PackageElementWrapper(ASTService ast, Elements elements, PackageElement delegateTo) {
            this.ast = ast;
            this.elements = elements;
            this.delegateTo = delegateTo;
        }

        @Override
        public Name getQualifiedName() {
            return this.delegateTo.getQualifiedName();
        }

        @Override
        public boolean isUnnamed() {
            return this.delegateTo.isUnnamed();
        }

        @Override
        public TypeMirror asType() {
            return this.delegateTo.asType();
        }

        @Override
        public ElementKind getKind() {
            return this.delegateTo.getKind();
        }

        @Override
        public List<? extends AnnotationMirror> getAnnotationMirrors() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public <A extends Annotation> A getAnnotation(Class<A> annotationType) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Set<Modifier> getModifiers() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Name getSimpleName() {
            return this.delegateTo.getSimpleName();
        }

        @Override
        public Element getEnclosingElement() {
            return null;
        }

        @Override
        public List<? extends Element> getEnclosedElements() {
            return ElementOverlay.this.wrap(this.ast, this.elements, this.delegateTo.getEnclosedElements());
        }

        @Override
        public <R, P> R accept(ElementVisitor<R, P> v, P p) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    private class FakePackageElement
    implements PackageElement {
        private final ASTService ast;
        private final Elements elements;
        private final Name fqn;
        private final String fqnString;
        private final Name simpleName;

        public FakePackageElement(ASTService ast, Elements elements, Name fqn, String fqnString, Name simpleName) {
            this.ast = ast;
            this.elements = elements;
            this.fqn = fqn;
            this.fqnString = fqnString;
            this.simpleName = simpleName;
        }

        @Override
        public Name getQualifiedName() {
            return this.fqn;
        }

        @Override
        public boolean isUnnamed() {
            return false;
        }

        @Override
        public TypeMirror asType() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public ElementKind getKind() {
            return ElementKind.PACKAGE;
        }

        @Override
        public List<? extends AnnotationMirror> getAnnotationMirrors() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public <A extends Annotation> A getAnnotation(Class<A> annotationType) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Set<Modifier> getModifiers() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Name getSimpleName() {
            return this.simpleName;
        }

        @Override
        public Element getEnclosingElement() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public List<? extends Element> getEnclosedElements() {
            return ElementOverlay.this.getEnclosedElements(this.ast, this.elements, this.fqnString);
        }

        @Override
        public <R, P> R accept(ElementVisitor<R, P> v, P p) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    private final class TypeElementWrapper
    implements TypeElement {
        private final ASTService ast;
        private final Elements elements;
        private final TypeElement delegateTo;

        public TypeElementWrapper(ASTService ast, Elements elements, TypeElement delegateTo) {
            this.ast = ast;
            this.elements = elements;
            this.delegateTo = delegateTo;
        }

        @Override
        public List<? extends Element> getEnclosedElements() {
            return ElementOverlay.this.wrap(this.ast, this.elements, this.delegateTo.getEnclosedElements());
        }

        @Override
        public NestingKind getNestingKind() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Name getQualifiedName() {
            return this.delegateTo.getQualifiedName();
        }

        @Override
        public TypeMirror getSuperclass() {
            return this.delegateTo.getSuperclass();
        }

        @Override
        public List<? extends TypeMirror> getInterfaces() {
            return this.delegateTo.getInterfaces();
        }

        @Override
        public List<? extends TypeParameterElement> getTypeParameters() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public TypeMirror asType() {
            return this.delegateTo.asType();
        }

        @Override
        public ElementKind getKind() {
            return this.delegateTo.getKind();
        }

        @Override
        public List<? extends AnnotationMirror> getAnnotationMirrors() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public <A extends Annotation> A getAnnotation(Class<A> annotationType) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Set<Modifier> getModifiers() {
            return this.delegateTo.getModifiers();
        }

        @Override
        public Name getSimpleName() {
            return this.delegateTo.getSimpleName();
        }

        @Override
        public Element getEnclosingElement() {
            return ElementOverlay.this.resolve(this.ast, this.elements, ((QualifiedNameable)this.delegateTo.getEnclosingElement()).getQualifiedName().toString());
        }

        @Override
        public <R, P> R accept(ElementVisitor<R, P> v, P p) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    private final class FakeTypeElement
    implements TypeElement {
        private final ASTService ast;
        private final Elements elements;
        private final Name simpleName;
        private final Name fqn;
        private final String fqnString;
        private final Element parent;
        private final Set<Modifier> mods;

        public FakeTypeElement(ASTService ast, Elements elements, Name simpleName, Name fqn, String fqnString, Element parent, Set<Modifier> mods) {
            this.ast = ast;
            this.elements = elements;
            this.simpleName = simpleName;
            this.fqn = fqn;
            this.fqnString = fqnString;
            this.parent = parent;
            this.mods = mods;
        }

        @Override
        public List<? extends Element> getEnclosedElements() {
            return ElementOverlay.this.getEnclosedElements(this.ast, this.elements, this.fqnString);
        }

        @Override
        public NestingKind getNestingKind() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Name getQualifiedName() {
            return this.fqn;
        }

        @Override
        public TypeMirror getSuperclass() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public List<? extends TypeMirror> getInterfaces() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public List<? extends TypeParameterElement> getTypeParameters() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public TypeMirror asType() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public ElementKind getKind() {
            return ElementKind.CLASS;
        }

        @Override
        public List<? extends AnnotationMirror> getAnnotationMirrors() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public <A extends Annotation> A getAnnotation(Class<A> annotationType) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Set<Modifier> getModifiers() {
            return this.mods;
        }

        @Override
        public Name getSimpleName() {
            return this.simpleName;
        }

        @Override
        public Element getEnclosingElement() {
            return this.parent;
        }

        @Override
        public <R, P> R accept(ElementVisitor<R, P> v, P p) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

}

