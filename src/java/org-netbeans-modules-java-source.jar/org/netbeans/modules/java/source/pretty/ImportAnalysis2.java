/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.util.TreeScanner
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.model.JavacElements
 *  com.sun.tools.javac.util.Context
 */
package org.netbeans.modules.java.source.pretty;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.TreeScanner;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.util.Context;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.QualifiedNameable;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import org.netbeans.api.java.source.CodeStyle;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.modules.java.source.JavaSourceAccessor;
import org.netbeans.modules.java.source.builder.ASTService;
import org.netbeans.modules.java.source.builder.TreeFactory;
import org.netbeans.modules.java.source.save.DiffContext;
import org.netbeans.modules.java.source.save.ElementOverlay;

public class ImportAnalysis2 {
    private final ElementOverlay.FQNComputer currentFQN = new ElementOverlay.FQNComputer();
    private Elements elements;
    private TreeFactory make;
    private Set<Element> imports;
    private Set<Element> imported;
    private Stack<Set<Element>> visibleThroughClasses;
    private Map<String, Element> simpleNames2Elements;
    private PackageElement unnamedPackage;
    private Element pack;
    private ASTService model;
    private final ElementOverlay overlay;
    private CompilationUnitTree cut;
    private Map<String, Element> usedImplicitlyImportedClassesCache;
    private Set<String> implicitlyImportedClassNames;
    private Element javaLang;
    private CodeStyle cs;

    public ImportAnalysis2(CompilationInfo info) {
        this(JavaSourceAccessor.getINSTANCE().getJavacTask(info).getContext());
        this.cs = DiffContext.getCodeStyle(info);
    }

    public ImportAnalysis2(Context env) {
        this.elements = JavacElements.instance((Context)env);
        this.make = TreeFactory.instance(env);
        this.model = ASTService.instance(env);
        this.overlay = (ElementOverlay)env.get(ElementOverlay.class);
        this.unnamedPackage = this.overlay != null ? this.overlay.unnamedPackage(this.model, this.elements) : this.elements.getPackageElement("");
    }

    public void setCompilationUnit(CompilationUnitTree cut) {
        this.cut = cut;
    }

    public void setPackage(ExpressionTree packageNameTree) {
        this.currentFQN.setPackageNameTree(packageNameTree);
        if (packageNameTree == null) {
            this.pack = this.unnamedPackage;
            return;
        }
        String packageName = this.getFQN((Tree)packageNameTree);
        this.pack = this.overlay.resolve(this.model, this.elements, packageName);
    }

    public void setImports(List<? extends ImportTree> importsToAdd) {
        this.imports = new HashSet<Element>();
        this.imported = new HashSet<Element>();
        this.simpleNames2Elements = new HashMap<String, Element>();
        this.visibleThroughClasses = new Stack();
        this.usedImplicitlyImportedClassesCache = null;
        for (ImportTree imp : importsToAdd) {
            this.addImport(imp);
        }
        this.implicitlyImportedClassNames = new HashSet<String>();
        this.javaLang = this.overlay.resolve(this.model, this.elements, "java.lang");
        if (this.javaLang != null) {
            for (Element e : this.javaLang.getEnclosedElements()) {
                this.implicitlyImportedClassNames.add(e.getSimpleName().toString());
            }
        }
        if (this.pack != null) {
            for (Element e : this.pack.getEnclosedElements()) {
                this.implicitlyImportedClassNames.add(e.getSimpleName().toString());
            }
        }
    }

    public Set<? extends Element> getImports() {
        return this.imports;
    }

    public void classEntered(ClassTree clazz) {
        Element currentClassElement;
        this.currentFQN.enterClass(clazz);
        HashSet<Object> visible = new HashSet<Object>();
        String what = this.currentFQN.getFQN();
        Element element = currentClassElement = what != null ? this.overlay.resolve(this.model, this.elements, what) : null;
        if (currentClassElement != null) {
            visible.add(currentClassElement);
        }
        this.visibleThroughClasses.push(visible);
    }

    public void enterVisibleThroughClasses(ClassTree clazz) {
        Set<Element> visible = this.visibleThroughClasses.peek();
        visible.addAll(this.overlay.getAllVisibleThrough(this.model, this.elements, this.currentFQN.getFQN(), clazz));
        this.visibleThroughClasses.push(visible);
    }

    public void classLeft() {
        this.visibleThroughClasses.pop();
        this.currentFQN.leaveClass();
    }

    private String getFQN(ImportTree imp) {
        return this.getFQN(imp.getQualifiedIdentifier());
    }

    private String getFQN(Tree expression) {
        final StringBuffer result = new StringBuffer();
        new TreeScanner<Void, Void>(){

            public Void visitMemberSelect(MemberSelectTree tree, Void p) {
                TreeScanner.super.visitMemberSelect(tree, (Object)p);
                result.append('.');
                result.append(tree.getIdentifier().toString());
                return null;
            }

            public Void visitIdentifier(IdentifierTree tree, Void p) {
                result.append(tree.getName().toString());
                return null;
            }
        }.scan(expression, (Object)null);
        return result.toString();
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private void addImport(ImportTree imp) {
        String fqn = this.getFQN(imp);
        if (!imp.isStatic()) {
            Element resolve = this.overlay.resolve(this.model, this.elements, fqn);
            if (resolve != null) {
                this.imported.add(resolve);
                this.simpleNames2Elements.put(resolve.getSimpleName().toString(), resolve);
                return;
            } else {
                void classes22;
                if (!fqn.endsWith(".*")) return;
                fqn = fqn.substring(0, fqn.length() - 2);
                List classes22 = Collections.emptyList();
                Element clazz = this.overlay.resolve(this.model, this.elements, fqn);
                if (clazz != null) {
                    List<TypeElement> classes22 = ElementFilter.typesIn(clazz.getEnclosedElements());
                }
                for (TypeElement te : classes22) {
                    this.imported.add(te);
                    this.simpleNames2Elements.put(te.getSimpleName().toString(), te);
                }
            }
            return;
        } else {
            int dot = fqn.lastIndexOf(46);
            if (dot == -1) return;
            String className = fqn.substring(0, dot);
            String memberName = fqn.substring(dot + 1);
            boolean isStarred = "*".equals(memberName);
            Element resolved = this.overlay.resolve(this.model, this.elements, className);
            if (resolved == null) return;
            boolean added = false;
            for (Element e : resolved.getEnclosedElements()) {
                if (!e.getModifiers().contains((Object)Modifier.STATIC) || !isStarred && !memberName.contains(e.getSimpleName().toString())) continue;
                this.imported.add(e);
                this.simpleNames2Elements.put(e.getSimpleName().toString(), e);
            }
        }
    }

    public ExpressionTree resolveImport(MemberSelectTree orig, Element element) {
        boolean clash;
        if (this.visibleThroughClasses == null || element == null || this.cs != null && this.cs.useFQNs()) {
            return orig;
        }
        if (element.getKind() == ElementKind.PACKAGE) {
            return this.make.MemberSelect(orig.getExpression(), orig.getIdentifier());
        }
        for (Set<Element> els : this.visibleThroughClasses) {
            if (!els.contains(element)) continue;
            return this.make.Identifier(element.getSimpleName());
        }
        String simpleName = element.getSimpleName().toString();
        Element alreadyImported = this.simpleNames2Elements.get(simpleName);
        if (alreadyImported == null) {
            block1 : for (Set<Element> visible : this.visibleThroughClasses) {
                for (Element e : visible) {
                    if (e == null || e.getSimpleName() == null || !simpleName.equals(e.getSimpleName().toString())) continue;
                    alreadyImported = e;
                    break block1;
                }
            }
        }
        boolean bl = clash = alreadyImported != null && !element.equals(alreadyImported);
        if (!clash && (element.getKind().isClass() || element.getKind().isInterface())) {
            Element parent = element.getEnclosingElement();
            if (this.pack != null && this.pack.equals(parent)) {
                return this.make.Identifier(element.getSimpleName());
            }
        }
        if (this.imported.contains(element)) {
            return this.make.Identifier(element.getSimpleName());
        }
        if (this.getPackageOf(element) != null && this.getPackageOf(element).isUnnamed()) {
            if (orig.getExpression().getKind() == Tree.Kind.MEMBER_SELECT) {
                return this.make.MemberSelect(this.resolveImport((MemberSelectTree)orig.getExpression(), element.getEnclosingElement()), element.getSimpleName());
            }
            return orig;
        }
        if (!clash && this.implicitlyImportedClassNames.contains(simpleName)) {
            Element used = this.getUsedImplicitlyImportedClasses().get(simpleName);
            boolean bl2 = clash = used != null && !element.equals(used);
        }
        if (clash) {
            if (element.getEnclosingElement().getKind().isClass() || element.getEnclosingElement().getKind().isInterface() && orig.getExpression().getKind() == Tree.Kind.MEMBER_SELECT) {
                return this.make.MemberSelect(this.resolveImport((MemberSelectTree)orig.getExpression(), element.getEnclosingElement()), orig.getIdentifier());
            }
            return this.make.MemberSelect(orig.getExpression(), orig.getIdentifier());
        }
        if (!element.getKind().isClass() && !element.getKind().isInterface()) {
            ExpressionTree clazz = orig.getExpression();
            if (clazz.getKind() == Tree.Kind.MEMBER_SELECT) {
                clazz = this.resolveImport((MemberSelectTree)clazz, this.overlay.wrap(this.model, this.elements, element.getEnclosingElement()));
            }
            return this.make.MemberSelect(clazz, orig.getIdentifier());
        }
        TypeElement type = (TypeElement)element;
        Element parent = type.getEnclosingElement();
        if ((parent.getKind().isClass() || parent.getKind().isInterface()) && !this.cs.importInnerClasses()) {
            ExpressionTree clazz = orig.getExpression();
            if (clazz.getKind() == Tree.Kind.MEMBER_SELECT) {
                clazz = this.resolveImport((MemberSelectTree)clazz, this.overlay.wrap(this.model, this.elements, parent));
            }
            return this.make.MemberSelect(clazz, orig.getIdentifier());
        }
        if (parent.getKind() == ElementKind.PACKAGE && "java.lang".equals(((PackageElement)parent).getQualifiedName().toString())) {
            return this.make.Identifier(element.getSimpleName());
        }
        IdentifierTree imp = this.make.Identifier(((QualifiedNameable)element).getQualifiedName());
        this.addImport(this.make.Import((Tree)imp, false));
        Element original = this.overlay.getOriginal(element);
        if (original.getEnclosingElement().getKind() == ElementKind.PACKAGE && (!this.cs.useSingleClassImport() || this.checkPackagesForStarImport(((PackageElement)original.getEnclosingElement()).getQualifiedName().toString(), this.cs))) {
            original = original.getEnclosingElement();
        }
        this.imports.add(original);
        return this.make.Identifier(element.getSimpleName());
    }

    private boolean checkPackagesForStarImport(String pkgName, CodeStyle cs) {
        for (String s : cs.getPackagesForStarImport()) {
            if (!(s.endsWith(".*") ? pkgName.startsWith(s = s.substring(0, s.length() - 2)) : pkgName.equals(s))) continue;
            return true;
        }
        return false;
    }

    private PackageElement getPackageOf(Element el) {
        while (el.getKind() != ElementKind.PACKAGE) {
            el = el.getEnclosingElement();
        }
        return (PackageElement)el;
    }

    private Map<String, Element> getUsedImplicitlyImportedClasses() {
        if (this.usedImplicitlyImportedClassesCache != null) {
            return this.usedImplicitlyImportedClassesCache;
        }
        this.usedImplicitlyImportedClassesCache = new HashMap<String, Element>();
        new TreeScanner<Void, Void>(){

            public Void visitIdentifier(IdentifierTree node, Void p) {
                Element e = ImportAnalysis2.this.overlay.wrap(ImportAnalysis2.this.model, ImportAnalysis2.this.elements, ImportAnalysis2.this.model.getElement((Tree)node));
                if (e != null && (ImportAnalysis2.this.javaLang != null && ImportAnalysis2.this.javaLang.equals(e.getEnclosingElement()) || ImportAnalysis2.this.pack != null && ImportAnalysis2.this.pack.equals(e.getEnclosingElement()))) {
                    ImportAnalysis2.this.usedImplicitlyImportedClassesCache.put(e.getSimpleName().toString(), e);
                }
                return null;
            }
        }.scan((Tree)this.cut, (Object)null);
        return this.usedImplicitlyImportedClassesCache;
    }

}

