/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.TreeScanner
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$VarSymbol
 *  com.sun.tools.javac.tree.EndPosTable
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCVariableDecl
 *  org.netbeans.lib.nbjavac.services.NBParserFactory
 *  org.netbeans.lib.nbjavac.services.NBParserFactory$NBJavacParser
 *  org.netbeans.lib.nbjavac.services.NBParserFactory$NBJavacParser$EndPosTableImpl
 */
package org.netbeans.modules.java.source.parsing;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.TreeScanner;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.tree.EndPosTable;
import com.sun.tools.javac.tree.JCTree;
import java.util.List;
import org.netbeans.lib.nbjavac.services.NBParserFactory;

class TranslatePositionsVisitor
extends TreeScanner<Void, Void> {
    private final MethodTree changedMethod;
    private final EndPosTable endPos;
    private final int delta;
    boolean active;
    boolean inMethod;

    public TranslatePositionsVisitor(MethodTree changedMethod, EndPosTable endPos, int delta) {
        assert (changedMethod != null);
        assert (endPos != null);
        this.changedMethod = changedMethod;
        this.endPos = endPos;
        this.delta = delta;
    }

    public Void scan(Tree node, Void p) {
        Integer pos;
        if (this.active && node != null && ((JCTree)node).pos >= 0) {
            ((JCTree)node).pos += this.delta;
        }
        Void result = (Void)TreeScanner.super.scan(node, (Object)p);
        if (this.inMethod && node != null) {
            this.endPos.replaceTree((JCTree)node, null);
        }
        if (this.active && node != null && (pos = Integer.valueOf(this.endPos.replaceTree((JCTree)node, null))) != null) {
            int newPos = pos < 0 ? pos : pos + this.delta;
            ((NBParserFactory.NBJavacParser.EndPosTableImpl)this.endPos).storeEnd((JCTree)node, newPos);
        }
        return result;
    }

    public Void visitCompilationUnit(CompilationUnitTree node, Void p) {
        return (Void)this.scan((Iterable)node.getTypeDecls(), (Object)p);
    }

    public Void visitMethod(MethodTree node, Void p) {
        if (this.active || this.inMethod) {
            this.scan((Tree)node.getModifiers(), p);
            this.scan(node.getReturnType(), p);
            this.scan((Iterable)node.getTypeParameters(), (Object)p);
            this.scan((Iterable)node.getParameters(), (Object)p);
            this.scan((Iterable)node.getThrows(), (Object)p);
        }
        if (node == this.changedMethod) {
            this.inMethod = true;
        }
        if (this.active || this.inMethod) {
            this.scan((Tree)node.getBody(), p);
        }
        if (this.inMethod) {
            this.active = this.inMethod;
            this.inMethod = false;
        }
        if (this.active || this.inMethod) {
            this.scan(node.getDefaultValue(), p);
        }
        return null;
    }

    public Void visitVariable(VariableTree node, Void p) {
        JCTree.JCVariableDecl varDecl = (JCTree.JCVariableDecl)node;
        if (varDecl.sym != null && this.active && varDecl.sym.pos >= 0) {
            varDecl.sym.pos += this.delta;
        }
        return (Void)TreeScanner.super.visitVariable(node, (Object)p);
    }
}

