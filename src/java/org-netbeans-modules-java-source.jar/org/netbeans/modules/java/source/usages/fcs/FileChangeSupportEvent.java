/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package org.netbeans.modules.java.source.usages.fcs;

import java.io.File;
import java.util.EventObject;
import org.netbeans.modules.java.source.usages.fcs.FileChangeSupport;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public final class FileChangeSupportEvent
extends EventObject {
    public static final int EVENT_CREATED = 0;
    public static final int EVENT_DELETED = 1;
    public static final int EVENT_MODIFIED = 2;
    private final int type;
    private final File path;

    FileChangeSupportEvent(FileChangeSupport support, int type, File path) {
        super(support);
        this.type = type;
        this.path = path;
    }

    public int getType() {
        return this.type;
    }

    public File getPath() {
        return this.path;
    }

    public FileObject getFileObject() {
        return FileUtil.toFileObject((File)this.path);
    }

    @Override
    public String toString() {
        return "FCSE[" + "CDM".charAt(this.type) + ":" + this.path + "]";
    }
}

