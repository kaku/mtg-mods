/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.java.platform.JavaPlatform
 *  org.netbeans.api.java.platform.JavaPlatformManager
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 *  org.openide.util.Pair
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.source.parsing;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.api.java.platform.JavaPlatformManager;
import org.netbeans.modules.java.source.parsing.Archive;
import org.netbeans.modules.java.source.parsing.CTSymArchive;
import org.netbeans.modules.java.source.parsing.CachingArchive;
import org.netbeans.modules.java.source.parsing.FileObjectArchive;
import org.netbeans.modules.java.source.parsing.FolderArchive;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import org.openide.util.Pair;
import org.openide.util.Utilities;

public final class CachingArchiveProvider {
    private static final String NAME_RT_JAR = "rt.jar";
    private static final String PATH_CT_SYM = "lib/ct.sym";
    private static final String PATH_RT_JAR_IN_CT_SYM = "META-INF/sym/rt.jar/";
    private static final boolean USE_CT_SYM = !Boolean.getBoolean("CachingArchiveProvider.disableCtSym");
    private static CachingArchiveProvider instance;
    private final Map<URI, Archive> archives = new HashMap<URI, Archive>();
    private final Map<URI, URI> ctSymToJar = new HashMap<URI, URI>();

    public static synchronized CachingArchiveProvider getDefault() {
        if (instance == null) {
            instance = new CachingArchiveProvider();
        }
        return instance;
    }

    static CachingArchiveProvider newInstance() {
        return new CachingArchiveProvider();
    }

    private CachingArchiveProvider() {
    }

    @CheckForNull
    public synchronized Archive getArchive(@NonNull URL root, boolean cacheFile) {
        URI rootURI = CachingArchiveProvider.toURI(root);
        Archive archive = this.archives.get(rootURI);
        if (archive == null && (archive = this.create(root, cacheFile)) != null) {
            this.archives.put(rootURI, archive);
        }
        return archive;
    }

    public synchronized void removeArchive(@NonNull URL root) {
        URI rootURI = CachingArchiveProvider.toURI(root);
        Archive archive = this.archives.remove(rootURI);
        Iterator<Map.Entry<URI, URI>> it = this.ctSymToJar.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<URI, URI> e = it.next();
            if (!e.getValue().equals(rootURI)) continue;
            it.remove();
            break;
        }
        if (archive != null) {
            archive.clear();
        }
    }

    public synchronized void clearArchive(@NonNull URL root) {
        Archive archive = this.archives.get(CachingArchiveProvider.toURI(root));
        if (archive != null) {
            archive.clear();
        }
    }

    @NonNull
    public URL mapCtSymToJar(@NonNull URL archiveOrCtSym) {
        URI result = this.ctSymToJar.get(CachingArchiveProvider.toURI(archiveOrCtSym));
        if (result != null) {
            try {
                return result.toURL();
            }
            catch (MalformedURLException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return archiveOrCtSym;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean hasCtSym(@NonNull URL root) {
        URL fileURL = FileUtil.getArchiveFile((URL)root);
        if (fileURL == null) {
            return false;
        }
        File f = Utilities.toFile((URL)fileURL);
        if (f == null || !f.exists()) {
            return false;
        }
        CachingArchiveProvider cachingArchiveProvider = this;
        synchronized (cachingArchiveProvider) {
            Pair<File, String> res = this.mapJarToCtSym(f, root);
            return res.second() != null;
        }
    }

    synchronized void clear() {
        this.archives.clear();
        this.ctSymToJar.clear();
    }

    private Archive create(URL root, boolean cacheFile) {
        URL inner;
        String protocol = root.getProtocol();
        if ("file".equals(protocol)) {
            File f = Utilities.toFile((URI)URI.create(root.toExternalForm()));
            if (f.isDirectory()) {
                return new FolderArchive(f);
            }
            return null;
        }
        if ("jar".equals(protocol) && "file".equals(protocol = (inner = FileUtil.getArchiveFile((URL)root)).getProtocol())) {
            File f = Utilities.toFile((URI)URI.create(inner.toExternalForm()));
            if (f.isFile()) {
                Pair<File, String> resolved = this.mapJarToCtSym(f, root);
                return resolved.second() == null ? new CachingArchive((File)resolved.first(), (String)resolved.second(), cacheFile) : new CTSymArchive(f, null, (File)resolved.first(), (String)resolved.second());
            }
            return null;
        }
        FileObject fo = URLMapper.findFileObject((URL)root);
        if (fo != null) {
            return new FileObjectArchive(fo);
        }
        return null;
    }

    @NonNull
    private Pair<File, String> mapJarToCtSym(@NonNull File file, @NonNull URL originalRoot) {
        FileObject fo;
        assert (Thread.holdsLock(this));
        if (USE_CT_SYM && "rt.jar".equals(file.getName()) && (fo = FileUtil.toFileObject((File)file)) != null) {
            for (JavaPlatform jp : JavaPlatformManager.getDefault().getInstalledPlatforms()) {
                for (FileObject jdkFolder : jp.getInstallFolders()) {
                    FileObject ctSym;
                    File ctSymFile;
                    if (!FileUtil.isParentOf((FileObject)jdkFolder, (FileObject)fo) || (ctSym = jdkFolder.getFileObject("lib/ct.sym")) == null || (ctSymFile = FileUtil.toFile((FileObject)ctSym)) == null) continue;
                    try {
                        URL root = FileUtil.getArchiveRoot((URL)Utilities.toURI((File)ctSymFile).toURL());
                        this.ctSymToJar.put(new URI(String.format("%s%s", root.toExternalForm(), "META-INF/sym/rt.jar/")), originalRoot.toURI());
                    }
                    catch (MalformedURLException e) {
                        Exceptions.printStackTrace((Throwable)e);
                    }
                    catch (URISyntaxException e) {
                        Exceptions.printStackTrace((Throwable)e);
                    }
                    return Pair.of((Object)ctSymFile, (Object)"META-INF/sym/rt.jar/");
                }
            }
        }
        return Pair.of((Object)file, (Object)null);
    }

    @NonNull
    private static URI toURI(@NonNull URL url) {
        try {
            return url.toURI();
        }
        catch (URISyntaxException ex) {
            throw new IllegalArgumentException(ex);
        }
    }
}

