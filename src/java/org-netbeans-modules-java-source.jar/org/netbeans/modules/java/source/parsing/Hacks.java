/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.util.JCDiagnostic
 *  com.sun.tools.javac.util.JCDiagnostic$DiagnosticFlag
 */
package org.netbeans.modules.java.source.parsing;

import com.sun.tools.javac.util.JCDiagnostic;
import javax.tools.Diagnostic;
import org.netbeans.modules.java.source.parsing.CompilationInfoImpl;

public class Hacks {
    public static boolean isSyntaxError(Diagnostic<?> d) {
        if (d instanceof JCDiagnostic) {
            return ((JCDiagnostic)d).isFlagSet(JCDiagnostic.DiagnosticFlag.SYNTAX);
        }
        if (d instanceof CompilationInfoImpl.RichDiagnostic && ((CompilationInfoImpl.RichDiagnostic)d).getDelegate() instanceof JCDiagnostic) {
            return ((CompilationInfoImpl.RichDiagnostic)d).getDelegate().isFlagSet(JCDiagnostic.DiagnosticFlag.SYNTAX);
        }
        return false;
    }
}

