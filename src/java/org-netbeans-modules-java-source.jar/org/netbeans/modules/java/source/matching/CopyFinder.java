/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.AnnotationTree
 *  com.sun.source.tree.ArrayAccessTree
 *  com.sun.source.tree.ArrayTypeTree
 *  com.sun.source.tree.AssertTree
 *  com.sun.source.tree.AssignmentTree
 *  com.sun.source.tree.BinaryTree
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.BreakTree
 *  com.sun.source.tree.CaseTree
 *  com.sun.source.tree.CatchTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompoundAssignmentTree
 *  com.sun.source.tree.ConditionalExpressionTree
 *  com.sun.source.tree.ContinueTree
 *  com.sun.source.tree.DoWhileLoopTree
 *  com.sun.source.tree.EmptyStatementTree
 *  com.sun.source.tree.EnhancedForLoopTree
 *  com.sun.source.tree.ExpressionStatementTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.ForLoopTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.IfTree
 *  com.sun.source.tree.InstanceOfTree
 *  com.sun.source.tree.LabeledStatementTree
 *  com.sun.source.tree.LambdaExpressionTree
 *  com.sun.source.tree.LiteralTree
 *  com.sun.source.tree.MemberReferenceTree
 *  com.sun.source.tree.MemberReferenceTree$ReferenceMode
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodInvocationTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.NewArrayTree
 *  com.sun.source.tree.NewClassTree
 *  com.sun.source.tree.ParameterizedTypeTree
 *  com.sun.source.tree.ParenthesizedTree
 *  com.sun.source.tree.PrimitiveTypeTree
 *  com.sun.source.tree.ReturnTree
 *  com.sun.source.tree.Scope
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.SwitchTree
 *  com.sun.source.tree.SynchronizedTree
 *  com.sun.source.tree.ThrowTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.TryTree
 *  com.sun.source.tree.TypeCastTree
 *  com.sun.source.tree.TypeParameterTree
 *  com.sun.source.tree.UnaryTree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.tree.WhileLoopTree
 *  com.sun.source.tree.WildcardTree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreeScanner
 *  com.sun.source.util.Trees
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.java.source.matching;

import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.ArrayAccessTree;
import com.sun.source.tree.ArrayTypeTree;
import com.sun.source.tree.AssertTree;
import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BinaryTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.BreakTree;
import com.sun.source.tree.CaseTree;
import com.sun.source.tree.CatchTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompoundAssignmentTree;
import com.sun.source.tree.ConditionalExpressionTree;
import com.sun.source.tree.ContinueTree;
import com.sun.source.tree.DoWhileLoopTree;
import com.sun.source.tree.EmptyStatementTree;
import com.sun.source.tree.EnhancedForLoopTree;
import com.sun.source.tree.ExpressionStatementTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.ForLoopTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.InstanceOfTree;
import com.sun.source.tree.LabeledStatementTree;
import com.sun.source.tree.LambdaExpressionTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MemberReferenceTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.NewArrayTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.ParameterizedTypeTree;
import com.sun.source.tree.ParenthesizedTree;
import com.sun.source.tree.PrimitiveTypeTree;
import com.sun.source.tree.ReturnTree;
import com.sun.source.tree.Scope;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.SwitchTree;
import com.sun.source.tree.SynchronizedTree;
import com.sun.source.tree.ThrowTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TryTree;
import com.sun.source.tree.TypeCastTree;
import com.sun.source.tree.TypeParameterTree;
import com.sun.source.tree.UnaryTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.tree.WhileLoopTree;
import com.sun.source.tree.WildcardTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreeScanner;
import com.sun.source.util.Trees;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.TreeUtilities;

public class CopyFinder
extends TreeScanner<Boolean, TreePath> {
    private final TreePath searchingFor;
    private final CompilationInfo info;
    private final Map<TreePath, VariableAssignments> result = new LinkedHashMap<TreePath, VariableAssignments>();
    private boolean allowGoDeeper = true;
    private Set<VariableElement> variablesWithAllowedRemap = Collections.emptySet();
    private State bindState = State.empty();
    private State preinitializeState = State.empty();
    private boolean allowVariablesRemap = false;
    private boolean nocheckOnAllowVariablesRemap = false;
    private final Cancel cancel;
    private static final String CLASS = "class";
    private final Set<Options> options;
    private Map<String, TypeMirror> designedTypeHack;
    private static final Set<TypeKind> IGNORE_KINDS = EnumSet.of(TypeKind.EXECUTABLE, TypeKind.PACKAGE, TypeKind.ERROR, TypeKind.OTHER);
    private TreePath currentPath;

    private static /* varargs */ Set<Options> options(Options ... options) {
        EnumSet<Options> result = EnumSet.noneOf(Options.class);
        result.addAll(Arrays.asList(options));
        return result;
    }

    private /* varargs */ CopyFinder(TreePath searchingFor, CompilationInfo info, Cancel cancel, Options ... options) {
        this(searchingFor, info, cancel, CopyFinder.options(options));
    }

    private CopyFinder(TreePath searchingFor, CompilationInfo info, Cancel cancel, Set<Options> options) {
        this.searchingFor = searchingFor;
        this.info = info;
        this.cancel = cancel;
        this.options = options;
    }

    public static /* varargs */ Map<TreePath, VariableAssignments> internalComputeDuplicates(CompilationInfo info, Collection<? extends TreePath> searchingFor, TreePath scope, State preinitializedState, Collection<? extends VariableElement> variablesWithAllowedRemap, Cancel cancel, Map<String, TypeMirror> designedTypeHack, Options ... options) {
        Map<TreePath, VariableAssignments> firstMapping;
        TreePath first = searchingFor.iterator().next();
        EnumSet<Options> optionsSet = EnumSet.noneOf(Options.class);
        optionsSet.addAll(Arrays.asList(options));
        if (!optionsSet.contains((Object)Options.ALLOW_GO_DEEPER) && !CopyFinder.sameKind(scope.getLeaf(), first.getLeaf())) {
            return Collections.emptyMap();
        }
        CopyFinder f = new CopyFinder(first, info, cancel, options);
        f.allowGoDeeper = optionsSet.contains((Object)Options.ALLOW_GO_DEEPER);
        f.designedTypeHack = designedTypeHack;
        f.variablesWithAllowedRemap = variablesWithAllowedRemap != null ? new HashSet<VariableElement>(variablesWithAllowedRemap) : Collections.emptySet();
        f.allowVariablesRemap = variablesWithAllowedRemap != null;
        boolean bl = f.nocheckOnAllowVariablesRemap = variablesWithAllowedRemap != null;
        if (preinitializedState != null) {
            f.preinitializeState = preinitializedState;
            f.bindState = State.copyOf(f.preinitializeState);
        }
        if (optionsSet.contains((Object)Options.ALLOW_GO_DEEPER)) {
            f.scan(scope, null);
            firstMapping = f.result;
        } else if (f.scan(scope, first).booleanValue()) {
            firstMapping = Collections.singletonMap(scope, new VariableAssignments(f.bindState));
        } else {
            return Collections.emptyMap();
        }
        boolean statement = StatementTree.class.isAssignableFrom(first.getLeaf().getKind().asInterface());
        assert (statement || searchingFor.size() == 1);
        if (!statement) {
            return firstMapping;
        }
        HashMap<TreePath, VariableAssignments> result = new HashMap<TreePath, VariableAssignments>();
        block0 : for (Map.Entry<TreePath, VariableAssignments> e : firstMapping.entrySet()) {
            TreePath firstOccurrence = e.getKey();
            List<? extends StatementTree> statements = CopyFinder.getStatements(firstOccurrence);
            int occurrenceIndex = statements.indexOf((Object)firstOccurrence.getLeaf());
            if (occurrenceIndex + searchingFor.size() > statements.size()) continue;
            int currentIndex = occurrenceIndex;
            Iterator<? extends TreePath> toProcess = searchingFor.iterator();
            Map variables = new HashMap<String, TreePath>(e.getValue().variables);
            Map multiVariables = new HashMap<String, Collection<? extends TreePath>>(e.getValue().multiVariables);
            Map variables2Names = new HashMap<String, String>(e.getValue().variables2Names);
            Map remapElements = new HashMap<Element, Element>(e.getValue().variablesRemapToElement);
            Map remapTrees = new HashMap<Element, TreePath>(e.getValue().variablesRemapToTrees);
            toProcess.next();
            while (toProcess.hasNext()) {
                ++currentIndex;
                TreePath currentToProcess = toProcess.next();
                CopyFinder ver = new CopyFinder(currentToProcess, info, cancel, options);
                ver.allowGoDeeper = false;
                ver.designedTypeHack = designedTypeHack;
                ver.variablesWithAllowedRemap = variablesWithAllowedRemap != null ? new HashSet<VariableElement>(variablesWithAllowedRemap) : Collections.emptySet();
                ver.allowVariablesRemap = variablesWithAllowedRemap != null;
                ver.nocheckOnAllowVariablesRemap = variablesWithAllowedRemap != null;
                ver.bindState = State.from(variables, multiVariables, variables2Names);
                if (ver.allowVariablesRemap) {
                    ver.bindState = State.from(ver.bindState, remapElements, remapTrees);
                }
                if (!ver.scan(new TreePath(firstOccurrence.getParentPath(), (Tree)statements.get(currentIndex)), currentToProcess).booleanValue()) continue block0;
                variables = ver.bindState.variables;
                multiVariables = ver.bindState.multiVariables;
                variables2Names = ver.bindState.variables2Names;
                remapElements = ver.bindState.variablesRemapToElement;
                remapTrees = ver.bindState.variablesRemapToTrees;
            }
            result.put(e.getKey(), new VariableAssignments(variables, multiVariables, variables2Names, remapElements, remapTrees));
        }
        return result;
    }

    private static boolean sameKind(Tree t1, Tree t2) {
        Tree.Kind k2;
        Tree.Kind k1 = t1.getKind();
        if (k1 == (k2 = t2.getKind())) {
            return true;
        }
        if (CopyFinder.isSingleStatemenBlockAndStatement(t1, t2) || CopyFinder.isSingleStatemenBlockAndStatement(t2, t1)) {
            return true;
        }
        if (k2 == Tree.Kind.BLOCK && StatementTree.class.isAssignableFrom(k1.asInterface())) {
            BlockTree bt = (BlockTree)t2;
            if (bt.isStatic()) {
                return false;
            }
            switch (bt.getStatements().size()) {
                case 1: {
                    return true;
                }
                case 2: {
                    return CopyFinder.isMultistatementWildcardTree((Tree)bt.getStatements().get(0)) || CopyFinder.isMultistatementWildcardTree((Tree)bt.getStatements().get(1));
                }
                case 3: {
                    return CopyFinder.isMultistatementWildcardTree((Tree)bt.getStatements().get(0)) || CopyFinder.isMultistatementWildcardTree((Tree)bt.getStatements().get(2));
                }
            }
            return false;
        }
        if (k1 != Tree.Kind.MEMBER_SELECT && k1 != Tree.Kind.IDENTIFIER || k2 != Tree.Kind.MEMBER_SELECT && k2 != Tree.Kind.IDENTIFIER) {
            return false;
        }
        return CopyFinder.isPureMemberSelect(t1, true) && CopyFinder.isPureMemberSelect(t2, true);
    }

    private static boolean isSingleStatemenBlockAndStatement(Tree t1, Tree t2) {
        Tree.Kind k1 = t1.getKind();
        Tree.Kind k2 = t2.getKind();
        if (k1 == Tree.Kind.BLOCK && ((BlockTree)t1).getStatements().size() == 1 && !((BlockTree)t1).isStatic()) {
            return StatementTree.class.isAssignableFrom(k2.asInterface());
        }
        return false;
    }

    protected TreePath getCurrentPath() {
        return this.currentPath;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected Boolean scan(TreePath path, TreePath param) {
        this.currentPath = path.getParentPath();
        try {
            Boolean bl = this.scan(path.getLeaf(), param);
            return bl;
        }
        finally {
            this.currentPath = null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Boolean scan(Tree node, TreePath p) {
        ParameterizedTypeTree ptt;
        String ident;
        if (this.cancel.isCancelled()) {
            return false;
        }
        if (node == null) {
            if (p == null) {
                return true;
            }
            if (CopyFinder.isMultistatementWildcardTree(p.getLeaf())) {
                return true;
            }
            return false;
        }
        String treeName = null;
        if (p != null && p.getLeaf().getKind() == Tree.Kind.IDENTIFIER) {
            treeName = ((IdentifierTree)p.getLeaf()).getName().toString();
        } else if (p != null && p.getLeaf().getKind() == Tree.Kind.TYPE_PARAMETER && ((TypeParameterTree)p.getLeaf()).getBounds().isEmpty()) {
            treeName = ((TypeParameterTree)p.getLeaf()).getName().toString();
        } else if (p != null && p.getLeaf().getKind() == Tree.Kind.PARAMETERIZED_TYPE && (node.getKind() == Tree.Kind.IDENTIFIER || node.getKind() == Tree.Kind.MEMBER_SELECT) && (ptt = (ParameterizedTypeTree)p.getLeaf()).getTypeArguments().size() == 1 && CopyFinder.isMultistatementWildcardTree((Tree)ptt.getTypeArguments().get(0))) {
            p = new TreePath(p, ptt.getType());
            this.bindState.multiVariables.put(CopyFinder.getWildcardTreeName((Tree)ptt.getTypeArguments().get(0)).toString(), Collections.emptyList());
        }
        if (treeName != null) {
            if (treeName.startsWith("$") && this.options.contains((Object)Options.ALLOW_VARIABLES_IN_PATTERN)) {
                boolean bind;
                TypeMirror designed;
                if (this.bindState.variables2Names.containsKey(treeName)) {
                    if (node.getKind() == Tree.Kind.IDENTIFIER) {
                        return ((IdentifierTree)node).getName().toString().equals(this.bindState.variables2Names.get(treeName));
                    }
                    return false;
                }
                TreePath currentPath = new TreePath(this.getCurrentPath(), node);
                TypeMirror typeMirror = designed = this.designedTypeHack != null ? this.designedTypeHack.get(treeName) : null;
                if (designed != null && designed.getKind() != TypeKind.ERROR) {
                    TypeMirror real = this.info.getTrees().getTypeMirror(currentPath);
                    bind = real != null && !IGNORE_KINDS.contains((Object)real.getKind()) ? (designed.getKind() == TypeKind.DECLARED && real.getKind().ordinal() <= TypeKind.DOUBLE.ordinal() && !((TypeElement)((DeclaredType)designed).asElement()).getQualifiedName().contentEquals("java.lang.Object") ? false : this.info.getTypes().isAssignable(real, designed)) : false;
                } else {
                    boolean bl = bind = designed == null;
                }
                if (bind) {
                    TreePath original = this.bindState.variables.get(treeName);
                    if (original == null) {
                        this.bindState.variables.put(treeName, currentPath);
                        return true;
                    }
                    boolean oldAllowGoDeeper = this.allowGoDeeper;
                    try {
                        this.options.remove((Object)Options.ALLOW_VARIABLES_IN_PATTERN);
                        Boolean bl = this.scan(node, original);
                        return bl;
                    }
                    finally {
                        this.options.add(Options.ALLOW_VARIABLES_IN_PATTERN);
                        this.allowGoDeeper = oldAllowGoDeeper;
                    }
                }
                return false;
            }
            Element remappable = this.info.getTrees().getElement(p);
            if (this.variablesWithAllowedRemap.contains(remappable) && (this.options.contains((Object)Options.ALLOW_REMAP_VARIABLE_TO_EXPRESSION) || node.getKind() == Tree.Kind.IDENTIFIER)) {
                TreePath existing = this.bindState.variablesRemapToTrees.get(remappable);
                if (existing != null) {
                    boolean oldAllowGoDeeper = this.allowGoDeeper;
                    try {
                        this.allowGoDeeper = false;
                        Boolean original = this.superScan(node, existing);
                        return original;
                    }
                    finally {
                        this.allowGoDeeper = oldAllowGoDeeper;
                    }
                }
                TreePath currPath = new TreePath(this.getCurrentPath(), node);
                TypeMirror currType = this.info.getTrees().getTypeMirror(currPath);
                TypeMirror pType = ((VariableElement)remappable).asType();
                if (currType != null && pType != null && (this.nocheckOnAllowVariablesRemap || this.isSameTypeForVariableRemap(currType, pType))) {
                    this.bindState.variablesRemapToTrees.put(remappable, currPath);
                    return true;
                }
                return false;
            }
        }
        if (p != null && CopyFinder.getWildcardTreeName(p.getLeaf()) != null && (ident = CopyFinder.getWildcardTreeName(p.getLeaf()).toString()).startsWith("$") && StatementTree.class.isAssignableFrom(node.getKind().asInterface())) {
            TreePath original = this.bindState.variables.get(ident);
            if (original == null) {
                TreePath currentPath = new TreePath(this.getCurrentPath(), node);
                this.bindState.variables.put(ident, currentPath);
                return true;
            }
            boolean oldAllowGoDeeper = this.allowGoDeeper;
            try {
                Boolean currType = this.scan(node, original);
                return currType;
            }
            finally {
                this.allowGoDeeper = oldAllowGoDeeper;
            }
        }
        if (p != null && CopyFinder.sameKind(node, p.getLeaf())) {
            boolean result;
            boolean bl = result = this.superScan(node, p) == Boolean.TRUE;
            if (result) {
                if (p == this.searchingFor && node != this.searchingFor && this.allowGoDeeper) {
                    this.result.put(new TreePath(this.getCurrentPath(), node), new VariableAssignments(this.bindState));
                    this.bindState = State.copyOf(this.preinitializeState);
                }
                return true;
            }
        }
        if (!this.allowGoDeeper) {
            return false;
        }
        if (p != null && p.getLeaf() == this.searchingFor.getLeaf() || !CopyFinder.sameKind(node, this.searchingFor.getLeaf())) {
            if (this.bindState.multiVariables.isEmpty() || this.bindState.variables.isEmpty() || this.bindState.variables2Names.isEmpty() || this.bindState.variablesRemapToElement.isEmpty() || this.bindState.variablesRemapToTrees.isEmpty()) {
                this.bindState = State.copyOf(this.preinitializeState);
            }
            this.superScan(node, null);
            return false;
        }
        this.allowGoDeeper = false;
        boolean result = this.superScan(node, this.searchingFor) == Boolean.TRUE;
        this.allowGoDeeper = true;
        if (result) {
            if (node != this.searchingFor.getLeaf()) {
                this.result.put(new TreePath(this.getCurrentPath(), node), new VariableAssignments(this.bindState));
                this.bindState = State.copyOf(this.preinitializeState);
            }
            return true;
        }
        this.superScan(node, null);
        return false;
    }

    private Boolean superScan(Tree node, TreePath p) {
        String ident;
        if (p == null) {
            return this.doSuperScan(node, p);
        }
        if (p.getLeaf().getKind() == Tree.Kind.IDENTIFIER && (ident = ((IdentifierTree)p.getLeaf()).getName().toString()).startsWith("$") && this.options.contains((Object)Options.ALLOW_VARIABLES_IN_PATTERN)) {
            return this.scan(node, p);
        }
        if (p.getLeaf().getKind() == Tree.Kind.BLOCK && node.getKind() != Tree.Kind.BLOCK) {
            BlockTree bt = (BlockTree)p.getLeaf();
            switch (bt.getStatements().size()) {
                case 1: {
                    if (CopyFinder.isMultistatementWildcardTree((Tree)bt.getStatements().get(0))) {
                        if (!this.validateMultiVariable((Tree)bt.getStatements().get(0), Collections.singletonList(new TreePath(this.getCurrentPath(), node)))) {
                            return false;
                        }
                        return true;
                    }
                    p = new TreePath(p, (Tree)bt.getStatements().get(0));
                    break;
                }
                case 2: {
                    if (CopyFinder.isMultistatementWildcardTree((Tree)bt.getStatements().get(0))) {
                        if (!this.validateMultiVariable((Tree)bt.getStatements().get(0), Collections.emptyList())) {
                            return false;
                        }
                        p = new TreePath(p, (Tree)bt.getStatements().get(1));
                        break;
                    }
                    if (CopyFinder.isMultistatementWildcardTree((Tree)bt.getStatements().get(1))) {
                        if (!this.validateMultiVariable((Tree)bt.getStatements().get(1), Collections.emptyList())) {
                            return false;
                        }
                        p = new TreePath(p, (Tree)bt.getStatements().get(0));
                        break;
                    }
                    throw new UnsupportedOperationException();
                }
                case 3: {
                    if (CopyFinder.isMultistatementWildcardTree((Tree)bt.getStatements().get(0)) && CopyFinder.isMultistatementWildcardTree((Tree)bt.getStatements().get(2))) {
                        if (!this.validateMultiVariable((Tree)bt.getStatements().get(0), Collections.emptyList())) {
                            return false;
                        }
                        if (!this.validateMultiVariable((Tree)bt.getStatements().get(2), Collections.emptyList())) {
                            return false;
                        }
                        p = new TreePath(p, (Tree)bt.getStatements().get(1));
                        break;
                    }
                    throw new UnsupportedOperationException();
                }
            }
        }
        if (!CopyFinder.sameKind(node, p.getLeaf())) {
            return false;
        }
        return this.doSuperScan(node, p);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Boolean doSuperScan(Tree node, TreePath p) {
        if (node == null) {
            return null;
        }
        TreePath prev = this.currentPath;
        try {
            this.currentPath = new TreePath(this.currentPath, node);
            Boolean bl = (Boolean)TreeScanner.super.scan(node, (Object)p);
            return bl;
        }
        finally {
            this.currentPath = prev;
        }
    }

    private Boolean scan(Tree node, Tree p, TreePath pOrigin) {
        if (node == null && p == null) {
            return true;
        }
        if (node != null && p == null) {
            return false;
        }
        return this.scan(node, new TreePath(pOrigin, p));
    }

    public Boolean visitMethodInvocation(MethodInvocationTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitMethodInvocation(node, (Object)p);
        }
        MethodInvocationTree t = (MethodInvocationTree)p.getLeaf();
        if (!this.scan((Tree)node.getMethodSelect(), (Tree)t.getMethodSelect(), p).booleanValue()) {
            return false;
        }
        if (!this.checkLists(node.getTypeArguments(), t.getTypeArguments(), p)) {
            return false;
        }
        return this.checkLists(node.getArguments(), t.getArguments(), p);
    }

    private <T extends Tree> boolean checkLists(List<? extends T> one, List<? extends T> other, TreePath otherOrigin) {
        if (one == null || other == null) {
            return one == other;
        }
        if (CopyFinder.containsMultistatementTrees(other)) {
            return this.checkListsWithMultistatementTrees(one, 0, other, 0, otherOrigin);
        }
        if (one.size() != other.size()) {
            return false;
        }
        for (int cntr = 0; cntr < one.size(); ++cntr) {
            if (this.scan((Tree)one.get(cntr), (Tree)other.get(cntr), otherOrigin).booleanValue()) continue;
            return false;
        }
        return true;
    }

    public Boolean visitAssert(AssertTree node, TreePath p) {
        if (p == null) {
            TreeScanner.super.visitAssert(node, (Object)p);
            return false;
        }
        AssertTree at = (AssertTree)p.getLeaf();
        if (!this.scan((Tree)node.getCondition(), (Tree)at.getCondition(), p).booleanValue()) {
            return false;
        }
        return this.scan((Tree)node.getDetail(), (Tree)at.getDetail(), p);
    }

    public Boolean visitAssignment(AssignmentTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitAssignment(node, (Object)p);
        }
        AssignmentTree at = (AssignmentTree)p.getLeaf();
        boolean result = this.scan((Tree)node.getExpression(), (Tree)at.getExpression(), p);
        return result && this.scan((Tree)node.getVariable(), (Tree)at.getVariable(), p) != false;
    }

    public Boolean visitCompoundAssignment(CompoundAssignmentTree node, TreePath p) {
        if (p == null) {
            TreeScanner.super.visitCompoundAssignment(node, (Object)p);
            return false;
        }
        CompoundAssignmentTree bt = (CompoundAssignmentTree)p.getLeaf();
        boolean result = this.scan((Tree)node.getExpression(), (Tree)bt.getExpression(), p);
        return result && this.scan((Tree)node.getVariable(), (Tree)bt.getVariable(), p) != false;
    }

    public Boolean visitBinary(BinaryTree node, TreePath p) {
        if (p == null) {
            TreeScanner.super.visitBinary(node, (Object)p);
            return false;
        }
        BinaryTree bt = (BinaryTree)p.getLeaf();
        boolean result = this.scan((Tree)node.getLeftOperand(), (Tree)bt.getLeftOperand(), p);
        return result && this.scan((Tree)node.getRightOperand(), (Tree)bt.getRightOperand(), p) != false;
    }

    private boolean validateMultiVariable(Tree t, List<? extends TreePath> tps) {
        String name = CopyFinder.getWildcardTreeName(t).toString();
        Collection<? extends TreePath> original = this.bindState.multiVariables.get(name);
        if (original == null) {
            this.bindState.multiVariables.put(name, tps);
            return true;
        }
        if (tps.size() != original.size()) {
            return false;
        }
        Iterator<? extends TreePath> orig = original.iterator();
        Iterator<? extends TreePath> current = tps.iterator();
        while (orig.hasNext() && current.hasNext()) {
            if (this.scan(current.next(), orig.next()).booleanValue()) continue;
            return false;
        }
        return true;
    }

    private boolean checkListsWithMultistatementTrees(List<? extends Tree> real, int realOffset, List<? extends Tree> pattern, int patternOffset, TreePath p) {
        while (realOffset < real.size() && patternOffset < pattern.size() && !CopyFinder.isMultistatementWildcardTree(pattern.get(patternOffset))) {
            if (!this.scan(real.get(realOffset), pattern.get(patternOffset), p).booleanValue()) {
                return false;
            }
            ++realOffset;
            ++patternOffset;
        }
        if (realOffset == real.size() && patternOffset == pattern.size()) {
            return true;
        }
        if (patternOffset >= pattern.size()) {
            return false;
        }
        if (CopyFinder.isMultistatementWildcardTree(pattern.get(patternOffset))) {
            if (patternOffset + 1 == pattern.size()) {
                LinkedList<TreePath> tps = new LinkedList<TreePath>();
                for (Tree t : real.subList(realOffset, real.size())) {
                    tps.add(new TreePath(this.getCurrentPath(), t));
                }
                return this.validateMultiVariable(pattern.get(patternOffset), tps);
            }
            LinkedList<TreePath> tps = new LinkedList<TreePath>();
            while (realOffset < real.size()) {
                State backup = State.copyOf(this.bindState);
                if (this.checkListsWithMultistatementTrees(real, realOffset, pattern, patternOffset + 1, p)) {
                    return this.validateMultiVariable(pattern.get(patternOffset), tps);
                }
                this.bindState = backup;
                tps.add(new TreePath(this.getCurrentPath(), real.get(realOffset)));
                ++realOffset;
            }
            return false;
        }
        return false;
    }

    public Boolean visitEmptyStatement(EmptyStatementTree node, TreePath p) {
        if (p == null) {
            TreeScanner.super.visitEmptyStatement(node, (Object)p);
            return false;
        }
        return node.getKind() == p.getLeaf().getKind();
    }

    public Boolean visitBlock(BlockTree node, TreePath p) {
        if (p == null) {
            TreeScanner.super.visitBlock(node, (Object)p);
            return false;
        }
        if (p.getLeaf().getKind() != Tree.Kind.BLOCK) {
            assert (node.getStatements().size() == 1);
            assert (!node.isStatic());
            if (p.getLeaf() == this.searchingFor.getLeaf()) {
                return false;
            }
            return this.checkLists(node.getStatements(), Collections.singletonList(p.getLeaf()), p.getParentPath());
        }
        BlockTree at = (BlockTree)p.getLeaf();
        if (node.isStatic() != at.isStatic()) {
            return false;
        }
        return this.checkLists(node.getStatements(), at.getStatements(), p);
    }

    public Boolean visitBreak(BreakTree node, TreePath p) {
        if (p == null) {
            TreeScanner.super.visitBreak(node, (Object)p);
            return false;
        }
        return true;
    }

    public Boolean visitCase(CaseTree node, TreePath p) {
        if (p == null) {
            TreeScanner.super.visitCase(node, (Object)p);
            return false;
        }
        CaseTree ct = (CaseTree)p.getLeaf();
        if (!this.scan((Tree)node.getExpression(), (Tree)ct.getExpression(), p).booleanValue()) {
            return false;
        }
        return this.checkLists(node.getStatements(), ct.getStatements(), p);
    }

    public Boolean visitCatch(CatchTree node, TreePath p) {
        if (p == null) {
            TreeScanner.super.visitCatch(node, (Object)p);
            return false;
        }
        CatchTree ef = (CatchTree)p.getLeaf();
        if (!this.scan((Tree)node.getParameter(), (Tree)ef.getParameter(), p).booleanValue()) {
            return false;
        }
        return this.scan((Tree)node.getBlock(), (Tree)ef.getBlock(), p);
    }

    public Boolean visitClass(ClassTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitClass(node, (Object)p);
        }
        ClassTree t = (ClassTree)p.getLeaf();
        String name = t.getSimpleName().toString();
        if (!name.isEmpty()) {
            if (!this.scan((Tree)node.getModifiers(), (Tree)t.getModifiers(), p).booleanValue()) {
                return false;
            }
            if (name.startsWith("$")) {
                String existingName = this.bindState.variables2Names.get(name);
                String currentName = node.getSimpleName().toString();
                if (existingName != null) {
                    if (!existingName.equals(currentName)) {
                        return false;
                    }
                } else {
                    this.bindState.variables.put(name, this.getCurrentPath());
                    this.bindState.variables2Names.put(name, currentName);
                }
            } else if (!node.getSimpleName().contentEquals(name)) {
                return false;
            }
            if (!this.checkLists(node.getTypeParameters(), t.getTypeParameters(), p)) {
                return false;
            }
            if (!this.scan(node.getExtendsClause(), t.getExtendsClause(), p).booleanValue()) {
                return false;
            }
            if (!this.checkLists(node.getImplementsClause(), t.getImplementsClause(), p)) {
                return false;
            }
        } else if (node.getSimpleName().length() != 0) {
            return false;
        }
        return this.checkLists(CopyFinder.filterHidden(this.info, this.getCurrentPath(), node.getMembers()), CopyFinder.filterHidden(this.info, p, t.getMembers()), p);
    }

    public Boolean visitConditionalExpression(ConditionalExpressionTree node, TreePath p) {
        if (p == null) {
            TreeScanner.super.visitConditionalExpression(node, (Object)p);
            return false;
        }
        ConditionalExpressionTree t = (ConditionalExpressionTree)p.getLeaf();
        if (!this.scan((Tree)node.getCondition(), (Tree)t.getCondition(), p).booleanValue()) {
            return false;
        }
        if (!this.scan((Tree)node.getFalseExpression(), (Tree)t.getFalseExpression(), p).booleanValue()) {
            return false;
        }
        return this.scan((Tree)node.getTrueExpression(), (Tree)t.getTrueExpression(), p);
    }

    public Boolean visitContinue(ContinueTree node, TreePath p) {
        if (p == null) {
            TreeScanner.super.visitContinue(node, (Object)p);
            return false;
        }
        return true;
    }

    public Boolean visitDoWhileLoop(DoWhileLoopTree node, TreePath p) {
        if (p == null) {
            TreeScanner.super.visitDoWhileLoop(node, (Object)p);
            return false;
        }
        DoWhileLoopTree t = (DoWhileLoopTree)p.getLeaf();
        if (!this.scan((Tree)node.getStatement(), (Tree)t.getStatement(), p).booleanValue()) {
            return false;
        }
        return this.scan((Tree)node.getCondition(), (Tree)t.getCondition(), p);
    }

    public Boolean visitExpressionStatement(ExpressionStatementTree node, TreePath p) {
        if (p == null) {
            TreeScanner.super.visitExpressionStatement(node, (Object)p);
            return false;
        }
        ExpressionStatementTree et = (ExpressionStatementTree)p.getLeaf();
        return this.scan((Tree)node.getExpression(), (Tree)et.getExpression(), p);
    }

    public Boolean visitEnhancedForLoop(EnhancedForLoopTree node, TreePath p) {
        if (p == null) {
            TreeScanner.super.visitEnhancedForLoop(node, (Object)p);
            return false;
        }
        EnhancedForLoopTree ef = (EnhancedForLoopTree)p.getLeaf();
        if (!this.scan((Tree)node.getVariable(), (Tree)ef.getVariable(), p).booleanValue()) {
            return false;
        }
        if (!this.scan((Tree)node.getExpression(), (Tree)ef.getExpression(), p).booleanValue()) {
            return false;
        }
        return this.scan((Tree)node.getStatement(), (Tree)ef.getStatement(), p);
    }

    public Boolean visitForLoop(ForLoopTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitForLoop(node, (Object)p);
        }
        ForLoopTree t = (ForLoopTree)p.getLeaf();
        if (!this.checkLists(node.getInitializer(), t.getInitializer(), p)) {
            return false;
        }
        if (!this.scan((Tree)node.getCondition(), (Tree)t.getCondition(), p).booleanValue()) {
            return false;
        }
        if (!this.checkLists(node.getUpdate(), t.getUpdate(), p)) {
            return false;
        }
        return this.scan((Tree)node.getStatement(), (Tree)t.getStatement(), p);
    }

    public Boolean visitLabeledStatement(LabeledStatementTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitLabeledStatement(node, (Object)p);
        }
        LabeledStatementTree lst = (LabeledStatementTree)p.getLeaf();
        String ident = lst.getLabel().toString();
        if (ident.startsWith("$")) {
            if (this.bindState.variables2Names.containsKey(ident)) {
                if (!node.getLabel().contentEquals(this.bindState.variables2Names.get(ident))) {
                    return false;
                }
            } else {
                this.bindState.variables2Names.put(ident, node.getLabel().toString());
            }
        } else if (!node.getLabel().toString().equals(ident)) {
            return false;
        }
        return this.scan((Tree)node.getStatement(), (Tree)lst.getStatement(), p);
    }

    public Boolean visitIdentifier(IdentifierTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitIdentifier(node, (Object)p);
        }
        switch (this.verifyElements(this.getCurrentPath(), p)) {
            case MATCH_CHECK_DEEPER: {
                if (node.getKind() == p.getLeaf().getKind()) {
                    return true;
                }
                return this.deepVerifyIdentifier2MemberSelect(this.getCurrentPath(), p);
            }
            case MATCH: {
                return true;
            }
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean deepVerifyIdentifier2MemberSelect(TreePath identifier, TreePath memberSelect) {
        for (TreePath thisPath : this.prepareThis(identifier)) {
            State origState = State.copyOf(this.bindState);
            try {
                MemberSelectTree t = (MemberSelectTree)memberSelect.getLeaf();
                if (this.scan(thisPath.getLeaf(), (Tree)t.getExpression(), memberSelect) != Boolean.TRUE) continue;
                boolean bl = true;
                return bl;
            }
            finally {
                this.bindState = origState;
                continue;
            }
        }
        return false;
    }

    public Boolean visitIf(IfTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitIf(node, (Object)p);
        }
        IfTree t = (IfTree)p.getLeaf();
        if (!this.scan((Tree)node.getCondition(), (Tree)t.getCondition(), p).booleanValue()) {
            return false;
        }
        if (!this.scan((Tree)node.getThenStatement(), (Tree)t.getThenStatement(), p).booleanValue()) {
            return false;
        }
        return this.scan((Tree)node.getElseStatement(), (Tree)t.getElseStatement(), p);
    }

    public Boolean visitArrayAccess(ArrayAccessTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitArrayAccess(node, (Object)p);
        }
        ArrayAccessTree t = (ArrayAccessTree)p.getLeaf();
        if (!this.scan((Tree)node.getExpression(), (Tree)t.getExpression(), p).booleanValue()) {
            return false;
        }
        return this.scan((Tree)node.getIndex(), (Tree)t.getIndex(), p);
    }

    public Boolean visitLiteral(LiteralTree node, TreePath p) {
        Object ltValue;
        if (p == null) {
            return (Boolean)TreeScanner.super.visitLiteral(node, (Object)p);
        }
        LiteralTree lt = (LiteralTree)p.getLeaf();
        Object nodeValue = node.getValue();
        if (nodeValue == (ltValue = lt.getValue())) {
            return true;
        }
        if (nodeValue == null || ltValue == null) {
            return false;
        }
        return nodeValue.equals(ltValue);
    }

    public Boolean visitMethod(MethodTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitMethod(node, (Object)p);
        }
        MethodTree t = (MethodTree)p.getLeaf();
        if (!this.scan((Tree)node.getModifiers(), (Tree)t.getModifiers(), p).booleanValue()) {
            return false;
        }
        if (!this.checkLists(node.getTypeParameters(), t.getTypeParameters(), p)) {
            return false;
        }
        if (!this.scan(node.getReturnType(), t.getReturnType(), p).booleanValue()) {
            return false;
        }
        String name = t.getName().toString();
        if (name.startsWith("$")) {
            String existingName = this.bindState.variables2Names.get(name);
            String currentName = node.getName().toString();
            if (existingName != null) {
                if (!existingName.equals(currentName)) {
                    return false;
                }
            } else {
                this.bindState.variables.put(name, this.getCurrentPath());
                this.bindState.variables2Names.put(name, currentName);
            }
        } else if (!node.getName().contentEquals(name)) {
            return false;
        }
        if (!this.checkLists(node.getParameters(), t.getParameters(), p)) {
            return false;
        }
        if (!this.checkLists(node.getThrows(), t.getThrows(), p)) {
            return false;
        }
        if (!this.scan((Tree)node.getBody(), (Tree)t.getBody(), p).booleanValue()) {
            return false;
        }
        return this.scan(node.getDefaultValue(), t.getDefaultValue(), p);
    }

    public Boolean visitModifiers(ModifiersTree node, TreePath p) {
        IdentifierTree ident;
        if (p == null) {
            return (Boolean)TreeScanner.super.visitModifiers(node, (Object)p);
        }
        ModifiersTree t = (ModifiersTree)p.getLeaf();
        ArrayList annotations = new ArrayList(t.getAnnotations());
        IdentifierTree identifierTree = ident = !annotations.isEmpty() && ((AnnotationTree)annotations.get(0)).getAnnotationType().getKind() == Tree.Kind.IDENTIFIER ? (IdentifierTree)((AnnotationTree)annotations.get(0)).getAnnotationType() : null;
        if (ident != null && this.options.contains((Object)Options.ALLOW_VARIABLES_IN_PATTERN)) {
            class CallableTreePath
            extends TreePath
            implements Callable<Object[]> {
                final /* synthetic */ Set val$flags;
                final /* synthetic */ boolean[] val$actualAnnotationsMask;

                public CallableTreePath() {
                    this.val$flags = var3_3;
                    this.val$actualAnnotationsMask = treePath;
                    super(tp.getParentPath(), tp.getLeaf());
                }

                @Override
                public Object[] call() throws Exception {
                    return new Object[]{this.val$flags, this.val$actualAnnotationsMask};
                }
            }
            annotations.remove(0);
            ArrayList real = new ArrayList(node.getAnnotations());
            EnumSet<Modifier> flags = EnumSet.noneOf(Modifier.class);
            flags.addAll(node.getFlags());
            if (!flags.containsAll(t.getFlags())) {
                return false;
            }
            flags.removeAll(t.getFlags());
            Iterator it = annotations.iterator();
            while (it.hasNext()) {
                AnnotationTree at = (AnnotationTree)it.next();
                boolean found = false;
                Iterator it2 = real.iterator();
                while (it2.hasNext()) {
                    AnnotationTree r = (AnnotationTree)it2.next();
                    State orig = State.copyOf(this.bindState);
                    if (this.doSuperScan((Tree)r, new TreePath(p, (Tree)at)) == Boolean.TRUE) {
                        it2.remove();
                        it.remove();
                        found = true;
                        break;
                    }
                    this.bindState = orig;
                }
                if (found) continue;
                return false;
            }
            boolean[] actualAnnotationsMask = new boolean[node.getAnnotations().size()];
            int ai = 0;
            for (AnnotationTree at : node.getAnnotations()) {
                actualAnnotationsMask[ai++] = real.contains((Object)at);
            }
            String name = ident.getName().toString();
            CallableTreePath currentPath = new CallableTreePath(this, this.getCurrentPath(), flags, actualAnnotationsMask);
            TreePath original = this.bindState.variables.get(name);
            if (original == null) {
                this.bindState.variables.put(name, currentPath);
                return true;
            }
            return false;
        }
        if (!this.checkLists(node.getAnnotations(), t.getAnnotations(), p)) {
            return false;
        }
        return node.getFlags().equals(t.getFlags());
    }

    public Boolean visitAnnotation(AnnotationTree node, TreePath p) {
        AssignmentTree at;
        ExpressionTree arg;
        if (p == null) {
            return (Boolean)TreeScanner.super.visitAnnotation(node, (Object)p);
        }
        AnnotationTree t = (AnnotationTree)p.getLeaf();
        List<ExpressionTree> arguments = t.getArguments();
        if (arguments.size() == 1 && (arg = (ExpressionTree)arguments.get(0)).getKind() == Tree.Kind.ASSIGNMENT && (at = (AssignmentTree)arg).getVariable().getKind() == Tree.Kind.IDENTIFIER && CopyFinder.isMultistatementWildcardTree((Tree)at.getExpression()) && ((IdentifierTree)at.getVariable()).getName().contentEquals("value")) {
            arguments = Collections.singletonList(at.getExpression());
        }
        if (!this.checkLists(node.getArguments(), arguments, p)) {
            return false;
        }
        return this.scan(node.getAnnotationType(), t.getAnnotationType(), p);
    }

    public Boolean visitNewArray(NewArrayTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitNewArray(node, (Object)p);
        }
        NewArrayTree t = (NewArrayTree)p.getLeaf();
        if (!this.checkLists(node.getDimensions(), t.getDimensions(), p)) {
            return false;
        }
        if (!this.checkLists(node.getInitializers(), t.getInitializers(), p)) {
            return false;
        }
        return this.scan(node.getType(), t.getType(), p);
    }

    public Boolean visitNewClass(NewClassTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitNewClass(node, (Object)p);
        }
        NewClassTree t = (NewClassTree)p.getLeaf();
        if (!this.scan((Tree)node.getIdentifier(), (Tree)t.getIdentifier(), p).booleanValue()) {
            return false;
        }
        if (!this.scan((Tree)node.getEnclosingExpression(), (Tree)t.getEnclosingExpression(), p).booleanValue()) {
            return false;
        }
        if (!this.checkLists(node.getTypeArguments(), t.getTypeArguments(), p)) {
            return false;
        }
        if (!this.checkLists(node.getArguments(), t.getArguments(), p)) {
            return false;
        }
        return this.scan((Tree)node.getClassBody(), (Tree)t.getClassBody(), p);
    }

    public Boolean visitParenthesized(ParenthesizedTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitParenthesized(node, (Object)p);
        }
        ParenthesizedTree t = (ParenthesizedTree)p.getLeaf();
        return this.scan((Tree)node.getExpression(), (Tree)t.getExpression(), p);
    }

    public Boolean visitReturn(ReturnTree node, TreePath p) {
        if (p == null) {
            TreeScanner.super.visitReturn(node, (Object)p);
            return false;
        }
        ReturnTree at = (ReturnTree)p.getLeaf();
        return this.scan((Tree)node.getExpression(), (Tree)at.getExpression(), p);
    }

    public Boolean visitMemberSelect(MemberSelectTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitMemberSelect(node, (Object)p);
        }
        switch (this.verifyElements(this.getCurrentPath(), p)) {
            case MATCH_CHECK_DEEPER: {
                if (node.getKind() == p.getLeaf().getKind()) {
                    MemberSelectTree t = (MemberSelectTree)p.getLeaf();
                    return this.scan((Tree)node.getExpression(), (Tree)t.getExpression(), p) == Boolean.TRUE;
                }
                return this.deepVerifyIdentifier2MemberSelect(p, this.getCurrentPath());
            }
            case MATCH: {
                return true;
            }
            case NO_MATCH: {
                return false;
            }
        }
        if (node.getKind() != p.getLeaf().getKind()) {
            return false;
        }
        MemberSelectTree t = (MemberSelectTree)p.getLeaf();
        if (!this.scan((Tree)node.getExpression(), (Tree)t.getExpression(), p).booleanValue()) {
            return false;
        }
        String ident = t.getIdentifier().toString();
        if (ident.startsWith("$")) {
            if (this.bindState.variables2Names.containsKey(ident)) {
                return node.getIdentifier().contentEquals(this.bindState.variables2Names.get(ident));
            }
            this.bindState.variables2Names.put(ident, node.getIdentifier().toString());
            return true;
        }
        return node.getIdentifier().toString().equals(t.getIdentifier().toString());
    }

    public Boolean visitSwitch(SwitchTree node, TreePath p) {
        if (p == null) {
            TreeScanner.super.visitSwitch(node, (Object)p);
            return false;
        }
        SwitchTree st = (SwitchTree)p.getLeaf();
        if (!this.scan((Tree)node.getExpression(), (Tree)st.getExpression(), p).booleanValue()) {
            return false;
        }
        return this.checkLists(node.getCases(), st.getCases(), p);
    }

    public Boolean visitSynchronized(SynchronizedTree node, TreePath p) {
        if (p == null) {
            TreeScanner.super.visitSynchronized(node, (Object)p);
            return false;
        }
        SynchronizedTree at = (SynchronizedTree)p.getLeaf();
        if (!this.scan((Tree)node.getExpression(), (Tree)at.getExpression(), p).booleanValue()) {
            return false;
        }
        return this.scan((Tree)node.getBlock(), (Tree)at.getBlock(), p);
    }

    public Boolean visitThrow(ThrowTree node, TreePath p) {
        if (p == null) {
            TreeScanner.super.visitThrow(node, (Object)p);
            return false;
        }
        ThrowTree at = (ThrowTree)p.getLeaf();
        return this.scan((Tree)node.getExpression(), (Tree)at.getExpression(), p);
    }

    public Boolean visitTry(TryTree node, TreePath p) {
        if (p == null) {
            TreeScanner.super.visitTry(node, (Object)p);
            return false;
        }
        TryTree at = (TryTree)p.getLeaf();
        if (!this.checkLists(node.getResources(), at.getResources(), p)) {
            return false;
        }
        if (!this.scan((Tree)node.getBlock(), (Tree)at.getBlock(), p).booleanValue()) {
            return false;
        }
        if (!this.checkLists(node.getCatches(), at.getCatches(), p)) {
            return false;
        }
        return this.scan((Tree)node.getFinallyBlock(), (Tree)at.getFinallyBlock(), p);
    }

    public Boolean visitParameterizedType(ParameterizedTypeTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitParameterizedType(node, (Object)p);
        }
        ParameterizedTypeTree t = (ParameterizedTypeTree)p.getLeaf();
        if (!this.scan(node.getType(), t.getType(), p).booleanValue()) {
            return false;
        }
        return this.checkLists(node.getTypeArguments(), t.getTypeArguments(), p);
    }

    public Boolean visitArrayType(ArrayTypeTree node, TreePath p) {
        if (p == null) {
            TreeScanner.super.visitArrayType(node, (Object)p);
            return false;
        }
        ArrayTypeTree at = (ArrayTypeTree)p.getLeaf();
        return this.scan(node.getType(), at.getType(), p);
    }

    public Boolean visitTypeCast(TypeCastTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitTypeCast(node, (Object)p);
        }
        TypeCastTree t = (TypeCastTree)p.getLeaf();
        if (!this.scan(node.getType(), t.getType(), p).booleanValue()) {
            return false;
        }
        return this.scan((Tree)node.getExpression(), (Tree)t.getExpression(), p);
    }

    public Boolean visitPrimitiveType(PrimitiveTypeTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitPrimitiveType(node, (Object)p);
        }
        PrimitiveTypeTree t = (PrimitiveTypeTree)p.getLeaf();
        return node.getPrimitiveTypeKind() == t.getPrimitiveTypeKind();
    }

    public Boolean visitTypeParameter(TypeParameterTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitTypeParameter(node, (Object)p);
        }
        TypeParameterTree t = (TypeParameterTree)p.getLeaf();
        String name = t.getName().toString();
        if (name.startsWith("$")) {
            String existingName = this.bindState.variables2Names.get(name);
            String currentName = node.getName().toString();
            if (existingName != null) {
                if (!existingName.equals(currentName)) {
                    return false;
                }
            } else {
                this.bindState.variables.put(name, this.getCurrentPath());
                this.bindState.variables2Names.put(name, currentName);
            }
        } else if (!node.getName().contentEquals(name)) {
            return false;
        }
        return this.checkLists(node.getBounds(), t.getBounds(), p);
    }

    public Boolean visitInstanceOf(InstanceOfTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitInstanceOf(node, (Object)p);
        }
        InstanceOfTree t = (InstanceOfTree)p.getLeaf();
        if (!this.scan((Tree)node.getExpression(), (Tree)t.getExpression(), p).booleanValue()) {
            return false;
        }
        return this.scan(node.getType(), t.getType(), p);
    }

    public Boolean visitUnary(UnaryTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitUnary(node, (Object)p);
        }
        UnaryTree t = (UnaryTree)p.getLeaf();
        return this.scan((Tree)node.getExpression(), (Tree)t.getExpression(), p);
    }

    public Boolean visitVariable(VariableTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitVariable(node, (Object)p);
        }
        VariableTree t = (VariableTree)p.getLeaf();
        if (!this.scan((Tree)node.getModifiers(), (Tree)t.getModifiers(), p).booleanValue()) {
            return false;
        }
        if (!this.scan(node.getType(), t.getType(), p).booleanValue()) {
            return false;
        }
        String name = t.getName().toString();
        if (name.startsWith("$")) {
            String existingName = this.bindState.variables2Names.get(name);
            String currentName = node.getName().toString();
            if (existingName != null) {
                if (!existingName.equals(currentName)) {
                    return false;
                }
            } else {
                this.bindState.variables.put(name, this.getCurrentPath());
                this.bindState.variables2Names.put(name, currentName);
            }
        }
        if (this.allowVariablesRemap) {
            VariableElement nodeEl = (VariableElement)this.info.getTrees().getElement(this.getCurrentPath());
            VariableElement pEl = (VariableElement)this.info.getTrees().getElement(p);
            if (nodeEl != null && pEl != null && this.isSameTypeForVariableRemap(nodeEl.asType(), pEl.asType())) {
                this.bindState.variablesRemapToElement.put(pEl, nodeEl);
            }
        }
        return this.scan((Tree)node.getInitializer(), (Tree)t.getInitializer(), p);
    }

    public Boolean visitWhileLoop(WhileLoopTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitWhileLoop(node, (Object)p);
        }
        WhileLoopTree t = (WhileLoopTree)p.getLeaf();
        if (!this.scan((Tree)node.getCondition(), (Tree)t.getCondition(), p).booleanValue()) {
            return false;
        }
        return this.scan((Tree)node.getStatement(), (Tree)t.getStatement(), p);
    }

    public Boolean visitWildcard(WildcardTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitWildcard(node, (Object)p);
        }
        WildcardTree t = (WildcardTree)p.getLeaf();
        return this.scan(node.getBound(), t.getBound(), p);
    }

    public Boolean visitLambdaExpression(LambdaExpressionTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitLambdaExpression(node, (Object)p);
        }
        LambdaExpressionTree t = (LambdaExpressionTree)p.getLeaf();
        if (!this.checkLists(node.getParameters(), t.getParameters(), p)) {
            return false;
        }
        return this.scan(node.getBody(), t.getBody(), p);
    }

    public Boolean visitMemberReference(MemberReferenceTree node, TreePath p) {
        if (p == null) {
            return (Boolean)TreeScanner.super.visitMemberReference(node, (Object)p);
        }
        MemberReferenceTree t = (MemberReferenceTree)p.getLeaf();
        if (!node.getMode().equals((Object)t.getMode())) {
            return false;
        }
        if (!this.scan((Tree)node.getQualifierExpression(), (Tree)t.getQualifierExpression(), p).booleanValue()) {
            return false;
        }
        String ident = t.getName().toString();
        if (ident.startsWith("$")) {
            if (this.bindState.variables2Names.containsKey(ident)) {
                return node.getName().contentEquals(this.bindState.variables2Names.get(ident));
            }
            this.bindState.variables2Names.put(ident, node.getName().toString());
            return true;
        }
        return node.getName().contentEquals(t.getName());
    }

    @NonNull
    protected VerifyResult verifyElements(TreePath node, TreePath p) {
        return this.options.contains((Object)Options.NO_ELEMENT_VERIFY) ? this.unattributedVerifyElements(node, p) : this.fullVerifyElements(node, p);
    }

    @NonNull
    private VerifyResult fullVerifyElements(TreePath node, TreePath p) {
        VerifyResult matchingResult;
        Element nodeEl = this.info.getTrees().getElement(node);
        Element pEl = this.info.getTrees().getElement(p);
        if (nodeEl == null) {
            return pEl == null ? VerifyResult.MATCH : VerifyResult.NO_MATCH_CONTINUE;
        }
        if (!nodeEl.getModifiers().contains((Object)Modifier.STATIC)) {
            matchingResult = nodeEl.getKind().isClass() || nodeEl.getKind().isInterface() ? VerifyResult.MATCH : VerifyResult.MATCH_CHECK_DEEPER;
        } else {
            ExpressionTree selector;
            matchingResult = VerifyResult.MATCH;
            if (p.getLeaf().getKind() == Tree.Kind.MEMBER_SELECT && node.getLeaf().getKind() == Tree.Kind.MEMBER_SELECT && CopyFinder.getWildcardTreeName((Tree)(selector = ((MemberSelectTree)p.getLeaf()).getExpression())) != null) {
                Element nodeSelector = this.info.getTrees().getElement(new TreePath(node, (Tree)((MemberSelectTree)node.getLeaf()).getExpression()));
                matchingResult = nodeSelector != null && (nodeSelector.getKind().isClass() || nodeSelector.getKind().isInterface()) ? VerifyResult.NO_MATCH : VerifyResult.MATCH_CHECK_DEEPER;
            }
        }
        if (nodeEl == pEl) {
            return matchingResult;
        }
        if (nodeEl == null || pEl == null) {
            return VerifyResult.NO_MATCH;
        }
        if (nodeEl.getKind() == pEl.getKind() && nodeEl.getKind() == ElementKind.FIELD && "class".contentEquals(((VariableElement)nodeEl).getSimpleName()) && "class".contentEquals(((VariableElement)pEl).getSimpleName())) {
            return VerifyResult.MATCH_CHECK_DEEPER;
        }
        if (nodeEl.getKind() == pEl.getKind() && nodeEl.getKind() == ElementKind.METHOD && this.info.getElements().overrides((ExecutableElement)nodeEl, (ExecutableElement)pEl, (TypeElement)nodeEl.getEnclosingElement())) {
            return VerifyResult.MATCH_CHECK_DEEPER;
        }
        if (nodeEl.equals(pEl)) {
            return matchingResult;
        }
        if (this.allowVariablesRemap && nodeEl.equals(this.bindState.variablesRemapToElement.get(pEl))) {
            return matchingResult;
        }
        TypeMirror nodeTM = this.info.getTrees().getTypeMirror(node);
        if (nodeTM == null || nodeTM.getKind() == TypeKind.ERROR) {
            return VerifyResult.NO_MATCH_CONTINUE;
        }
        TypeMirror pTM = this.info.getTrees().getTypeMirror(p);
        if (pTM == null || pTM.getKind() == TypeKind.ERROR) {
            return VerifyResult.NO_MATCH_CONTINUE;
        }
        return VerifyResult.NO_MATCH;
    }

    @NonNull
    private VerifyResult unattributedVerifyElements(TreePath node, TreePath p) {
        if (CopyFinder.getSimpleName(node.getLeaf()).contentEquals(CopyFinder.getSimpleName(p.getLeaf()))) {
            boolean pureSelect = CopyFinder.isPureMemberSelect(node.getLeaf(), true) && CopyFinder.isPureMemberSelect(p.getLeaf(), true);
            return pureSelect ? VerifyResult.MATCH : VerifyResult.MATCH_CHECK_DEEPER;
        }
        return VerifyResult.NO_MATCH_CONTINUE;
    }

    protected Iterable<? extends TreePath> prepareThis(TreePath tp) {
        return this.options.contains((Object)Options.NO_ELEMENT_VERIFY) ? this.unattributedPrepareThis(tp) : this.fullPrepareThis(tp);
    }

    private Iterable<? extends TreePath> fullPrepareThis(TreePath tp) {
        LinkedList<TreePath> result = new LinkedList<TreePath>();
        Object lastClass = null;
        for (Scope scope = this.info.getTrees().getScope((TreePath)tp); scope != null && scope.getEnclosingClass() != null; scope = scope.getEnclosingScope()) {
            if (lastClass == scope.getEnclosingClass()) continue;
            ExpressionTree thisTree = this.info.getTreeUtilities().parseExpression("this", new SourcePositions[1]);
            this.info.getTreeUtilities().attributeTree((Tree)thisTree, scope);
            result.add(new TreePath(tp, (Tree)thisTree));
        }
        return result;
    }

    private Iterable<? extends TreePath> unattributedPrepareThis(TreePath tp) {
        ExpressionTree thisTree = this.info.getTreeUtilities().parseExpression("this", new SourcePositions[1]);
        return Collections.singleton(new TreePath(tp, (Tree)thisTree));
    }

    private boolean isSameTypeForVariableRemap(TypeMirror nodeType, TypeMirror pType) {
        return this.info.getTypes().isSameType(nodeType, pType);
    }

    private static Name getSimpleName(Tree t) {
        if (t.getKind() == Tree.Kind.IDENTIFIER) {
            return ((IdentifierTree)t).getName();
        }
        if (t.getKind() == Tree.Kind.MEMBER_SELECT) {
            return ((MemberSelectTree)t).getIdentifier();
        }
        throw new UnsupportedOperationException();
    }

    public static List<? extends StatementTree> getStatements(TreePath firstLeaf) {
        switch (firstLeaf.getParentPath().getLeaf().getKind()) {
            case BLOCK: {
                return ((BlockTree)firstLeaf.getParentPath().getLeaf()).getStatements();
            }
            case CASE: {
                return ((CaseTree)firstLeaf.getParentPath().getLeaf()).getStatements();
            }
        }
        return Collections.singletonList((StatementTree)firstLeaf.getLeaf());
    }

    public static boolean isMultistatementWildcard(@NonNull CharSequence name) {
        return name.charAt(name.length() - 1) == '$';
    }

    public static boolean isMultistatementWildcardTree(Tree tree) {
        CharSequence name = CopyFinder.getWildcardTreeName(tree);
        return name != null && CopyFinder.isMultistatementWildcard(name);
    }

    @CheckForNull
    public static CharSequence getWildcardTreeName(@NonNull Tree t) {
        String name;
        String name2;
        IdentifierTree identTree;
        if (t.getKind() == Tree.Kind.EXPRESSION_STATEMENT && ((ExpressionStatementTree)t).getExpression().getKind() == Tree.Kind.IDENTIFIER) {
            IdentifierTree identTree2 = (IdentifierTree)((ExpressionStatementTree)t).getExpression();
            return identTree2.getName().toString();
        }
        if (t.getKind() == Tree.Kind.IDENTIFIER && (name2 = (identTree = (IdentifierTree)t).getName().toString()).startsWith("$")) {
            return name2;
        }
        if (t.getKind() == Tree.Kind.TYPE_PARAMETER && ((TypeParameterTree)t).getBounds().isEmpty() && (name = ((TypeParameterTree)t).getName().toString()).startsWith("$")) {
            return name;
        }
        return null;
    }

    public static boolean isPureMemberSelect(Tree mst, boolean allowVariables) {
        switch (mst.getKind()) {
            case IDENTIFIER: {
                return allowVariables || ((IdentifierTree)mst).getName().charAt(0) != '$';
            }
            case MEMBER_SELECT: {
                return CopyFinder.isPureMemberSelect((Tree)((MemberSelectTree)mst).getExpression(), allowVariables);
            }
        }
        return false;
    }

    public static List<? extends Tree> filterHidden(CompilationInfo info, TreePath basePath, Iterable<? extends Tree> members) {
        LinkedList<Tree> result = new LinkedList<Tree>();
        for (Tree t : members) {
            if (info.getTreeUtilities().isSynthetic(new TreePath(basePath, t))) continue;
            result.add(t);
        }
        return result;
    }

    public static boolean containsMultistatementTrees(List<? extends Tree> statements) {
        for (Tree t : statements) {
            if (!CopyFinder.isMultistatementWildcardTree(t)) continue;
            return true;
        }
        return false;
    }

    public static interface Cancel {
        public boolean isCancelled();
    }

    public static enum Options {
        ALLOW_VARIABLES_IN_PATTERN,
        ALLOW_REMAP_VARIABLE_TO_EXPRESSION,
        ALLOW_GO_DEEPER,
        NO_ELEMENT_VERIFY;
        

        private Options() {
        }
    }

    public static final class State {
        final Map<String, TreePath> variables;
        final Map<String, Collection<? extends TreePath>> multiVariables;
        final Map<String, String> variables2Names;
        final Map<Element, Element> variablesRemapToElement;
        final Map<Element, TreePath> variablesRemapToTrees;

        private State(Map<String, TreePath> variables, Map<String, Collection<? extends TreePath>> multiVariables, Map<String, String> variables2Names, Map<Element, Element> variablesRemapToElement, Map<Element, TreePath> variablesRemapToTrees) {
            this.variables = variables;
            this.multiVariables = multiVariables;
            this.variables2Names = variables2Names;
            this.variablesRemapToElement = variablesRemapToElement;
            this.variablesRemapToTrees = variablesRemapToTrees;
        }

        public static State empty() {
            return new State(new HashMap<String, TreePath>(), new HashMap<String, Collection<? extends TreePath>>(), new HashMap<String, String>(), new HashMap<Element, Element>(), new HashMap<Element, TreePath>());
        }

        public static State copyOf(State original) {
            return new State(new HashMap<String, TreePath>(original.variables), new HashMap<String, Collection<? extends TreePath>>(original.multiVariables), new HashMap<String, String>(original.variables2Names), new HashMap<Element, Element>(original.variablesRemapToElement), new HashMap<Element, TreePath>(original.variablesRemapToTrees));
        }

        public static State from(State original, Map<Element, Element> variablesRemapToElement, Map<Element, TreePath> variablesRemapToTrees) {
            return new State(new HashMap<String, TreePath>(original.variables), new HashMap<String, Collection<? extends TreePath>>(original.multiVariables), new HashMap<String, String>(original.variables2Names), variablesRemapToElement, variablesRemapToTrees);
        }

        public static State from(Map<String, TreePath> variables, Map<String, Collection<? extends TreePath>> multiVariables, Map<String, String> variables2Names) {
            return new State(variables, multiVariables, variables2Names, new HashMap<Element, Element>(), new HashMap<Element, TreePath>());
        }
    }

    public static final class MethodDuplicateDescription {
        public final TreePath firstLeaf;
        public final int dupeStart;
        public final int dupeEnd;
        public final Map<Element, Element> variablesRemapToElement;
        public final Map<Element, TreePath> variablesRemapToTrees;

        public MethodDuplicateDescription(TreePath firstLeaf, int dupeStart, int dupeEnd, Map<Element, Element> variablesRemapToElement, Map<Element, TreePath> variablesRemapToTrees) {
            this.firstLeaf = firstLeaf;
            this.dupeStart = dupeStart;
            this.dupeEnd = dupeEnd;
            this.variablesRemapToElement = variablesRemapToElement;
            this.variablesRemapToTrees = variablesRemapToTrees;
        }
    }

    public static final class VariableAssignments {
        public final Map<String, TreePath> variables;
        public final Map<String, Collection<? extends TreePath>> multiVariables;
        public final Map<String, String> variables2Names;
        public final Map<Element, Element> variablesRemapToElement;
        public final Map<Element, TreePath> variablesRemapToTrees;

        public VariableAssignments(Map<String, TreePath> variables, Map<String, Collection<? extends TreePath>> multiVariables, Map<String, String> variables2Names) {
            this.variables = variables;
            this.multiVariables = multiVariables;
            this.variables2Names = variables2Names;
            this.variablesRemapToElement = null;
            this.variablesRemapToTrees = null;
        }

        public VariableAssignments(Map<String, TreePath> variables, Map<String, Collection<? extends TreePath>> multiVariables, Map<String, String> variables2Names, Map<Element, Element> variablesRemapToElement, Map<Element, TreePath> variablesRemapToTrees) {
            this.variables = variables;
            this.multiVariables = multiVariables;
            this.variables2Names = variables2Names;
            this.variablesRemapToElement = variablesRemapToElement;
            this.variablesRemapToTrees = variablesRemapToTrees;
        }

        VariableAssignments(State state) {
            this.variables = state.variables;
            this.multiVariables = state.multiVariables;
            this.variables2Names = state.variables2Names;
            this.variablesRemapToElement = state.variablesRemapToElement;
            this.variablesRemapToTrees = state.variablesRemapToTrees;
        }
    }

    private static enum VerifyResult {
        MATCH_CHECK_DEEPER,
        MATCH,
        NO_MATCH_CONTINUE,
        NO_MATCH;
        

        private VerifyResult() {
        }
    }

}

