/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 *  org.openide.util.Pair
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.source.parsing;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.source.parsing.FileManagerTransaction;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.util.Iterators;
import org.openide.util.Pair;
import org.openide.util.Utilities;

class WriteBackTransaction
extends FileManagerTransaction {
    private static final Logger LOG = Logger.getLogger(WriteBackTransaction.class.getName());
    private final Set<File> deleted;
    static boolean disableCache;
    private URL root;
    private Reference<Pair[]> cacheRef;
    private Pair<Map<String, Map<File, CachedFileObject>>, Map<String, Map<File, CachedFileObject>>> contentCache = Pair.of(new HashMap(), new HashMap());
    private volatile boolean memExhausted;
    private Collection<File> workDirs = new HashSet<File>();
    private static final String WORK_SUFFIX = ".work";

    WriteBackTransaction(URL root) {
        super(true);
        this.root = root;
        this.deleted = new HashSet<File>();
        this.createCacheRef();
    }

    @Override
    public void delete(@NonNull File file) {
        assert (file != null);
        this.deleted.add(file);
    }

    private void createCacheRef() {
        this.cacheRef = new CacheRef(this, new Pair[]{this.contentCache});
        this.memExhausted = false;
    }

    @NonNull
    private Collection<File> listDir(@NonNull JavaFileManager.Location location, @NonNull String dir) {
        Map<String, Map<File, CachedFileObject>> cache = this.getCacheLine(location, true);
        Map<File, CachedFileObject> content = cache.get(dir);
        return content == null ? Collections.emptyList() : content.keySet();
    }

    private URL getRootDir() {
        return this.root;
    }

    @NonNull
    private Collection<JavaFileObject> getFileObjects(@NonNull JavaFileManager.Location location, @NonNull String dir) {
        Map<String, Map<File, CachedFileObject>> cache = this.getCacheLine(location, true);
        Map<File, CachedFileObject> content = cache.get(dir);
        return new ArrayList<JavaFileObject>(content.values());
    }

    private void maybeFlush() throws IOException {
        if (disableCache || this.memExhausted) {
            LOG.log(Level.FINE, "Memory exhausted:{0}", this.getRootDir());
            this.flushFiles(false);
            this.memExhausted = false;
            this.createCacheRef();
        }
    }

    @NonNull
    @Override
    Iterable<JavaFileObject> filter(@NonNull JavaFileManager.Location location, @NonNull String packageName, @NonNull Iterable<JavaFileObject> files) {
        Collection<File> added = this.listDir(location, packageName);
        Iterable<JavaFileObject> res = files;
        if (this.deleted.isEmpty() && added.isEmpty()) {
            return res;
        }
        if (added.isEmpty()) {
            return Iterators.filter(res, new Comparable<JavaFileObject>(){

                @Override
                public int compareTo(@NonNull JavaFileObject o) {
                    File f = WriteBackTransaction.toFile(o);
                    return WriteBackTransaction.this.deleted.contains(f) ? 0 : -1;
                }
            });
        }
        Collection<JavaFileObject> toAdd = this.getFileObjects(location, packageName);
        ArrayList<Iterable> chain = new ArrayList<Iterable>(2);
        chain.add(toAdd);
        chain.add(this.deleted.isEmpty() ? res : Iterators.filter(res, new Comparable<JavaFileObject>(){

            @Override
            public int compareTo(@NonNull JavaFileObject o) {
                File f = WriteBackTransaction.toFile(o);
                return WriteBackTransaction.this.deleted.contains(f) ? 0 : -1;
            }
        }));
        return Iterators.chained(chain);
    }

    @NonNull
    @Override
    JavaFileObject createFileObject(@NonNull JavaFileManager.Location location, @NonNull File file, @NonNull File root, @NullAllowed JavaFileFilterImplementation filter, @NullAllowed Charset encoding) {
        String[] pkgNamePair = FileObjects.getFolderAndBaseName(FileObjects.getRelativePath(root, file), File.separatorChar);
        String pname = FileObjects.convertFolder2Package(pkgNamePair[0], File.separatorChar);
        CachedFileObject cfo = this.getFileObject(location, pname, pkgNamePair[1], false);
        if (cfo != null) {
            return cfo;
        }
        String relPath = FileObjects.getRelativePath(root, file);
        File shadowRoot = new File(root.getParent(), root.getName() + ".work");
        File shadowFile = new File(shadowRoot, relPath);
        this.workDirs.add(shadowRoot);
        cfo = new CachedFileObject(this, file, pname, pkgNamePair[1], filter, encoding);
        cfo.setShadowFile(shadowFile);
        this.addFile(location, pname, cfo);
        if (!(shadowRoot.mkdirs() || shadowRoot.exists() || shadowRoot.isDirectory())) {
            throw new IllegalStateException();
        }
        return cfo;
    }

    @CheckForNull
    private CachedFileObject getFileObject(@NonNull JavaFileManager.Location location, @NonNull String dir, @NonNull String file, @NonNull boolean readOnly) {
        Map<String, Map<File, CachedFileObject>> cache = this.getCacheLine(location, readOnly);
        Map<File, CachedFileObject> content = cache.get(dir);
        if (content != null) {
            for (Map.Entry<File, CachedFileObject> en : content.entrySet()) {
                if (!file.equals(en.getKey().getName())) continue;
                return en.getValue();
            }
        }
        return null;
    }

    private void addFile(@NonNull JavaFileManager.Location location, @NonNull String packageName, @NonNull CachedFileObject fo) {
        LOG.log(Level.FINE, "File added to cache:{0}:{1}", new Object[]{fo.getFile(), this.root});
        Map<String, Map<File, CachedFileObject>> cache = this.getCacheLine(location, false);
        Map<File, CachedFileObject> dirContent = cache.get(packageName);
        if (dirContent == null) {
            dirContent = new HashMap<File, CachedFileObject>();
            cache.put(packageName, dirContent);
        }
        dirContent.put(WriteBackTransaction.toFile(fo), fo);
    }

    private Map<String, Map<File, CachedFileObject>> getCacheLine(@NonNull JavaFileManager.Location location, boolean readOnly) {
        if (location == StandardLocation.CLASS_OUTPUT) {
            return (Map)this.contentCache.first();
        }
        if (location == StandardLocation.SOURCE_OUTPUT) {
            return (Map)this.contentCache.second();
        }
        if (readOnly && location == StandardLocation.CLASS_PATH) {
            return (Map)this.contentCache.first();
        }
        throw new IllegalArgumentException("Unsupported Location: " + location);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void commit() throws IOException {
        LOG.log(Level.FINE, "Committed:{0}", this.getRootDir());
        try {
            for (File f : this.deleted) {
                f.delete();
            }
            this.flushFiles(true);
        }
        finally {
            this.clean();
        }
    }

    private void flushFiles(boolean inCommit) throws IOException {
        LOG.log(Level.FINE, "Flushing:{0}", this.getRootDir());
        this.doFlushFiles((Map)this.contentCache.first(), inCommit);
        this.doFlushFiles((Map)this.contentCache.second(), inCommit);
    }

    private void doFlushFiles(@NonNull Map<String, Map<File, CachedFileObject>> cacheLine, boolean inCommit) throws IOException {
        for (Map<File, CachedFileObject> dirContent : cacheLine.values()) {
            for (CachedFileObject cfo : dirContent.values()) {
                cfo.flush(inCommit);
                if (!inCommit) continue;
                cfo.commit();
            }
        }
    }

    @Override
    protected void rollBack() throws IOException {
        this.clean();
    }

    private void clean() {
        for (File d : this.workDirs) {
            FileObjects.deleteRecursively(d);
        }
        this.deleted.clear();
        ((Map)this.contentCache.first()).clear();
        ((Map)this.contentCache.second()).clear();
    }

    @Override
    JavaFileObject readFileObject(@NonNull JavaFileManager.Location location, @NonNull String dirName, @NonNull String relativeName) {
        return this.getFileObject(location, dirName, relativeName, true);
    }

    static File toFile(JavaFileObject o) {
        File f = ((FileObjects.FileBase)o).f;
        return f;
    }

    static class CachedFileObject
    extends FileObjects.FileBase {
        private static final byte[] NOTHING = new byte[0];
        private byte[] content = NOTHING;
        private WriteBackTransaction writer;
        private Pair<FileObjects.FileBase, Boolean> delegate;
        private File shadowFile;

        public CachedFileObject(WriteBackTransaction wb, File file, String pkgName, String name, JavaFileFilterImplementation filter, Charset encoding) {
            super(file, pkgName, name, filter, encoding);
            this.writer = wb;
            this.delegate = Pair.of((Object)((FileObjects.FileBase)FileObjects.fileFileObject(this.f, CachedFileObject.getRootFile(this.getFile(), this.getPackage()), filter, encoding)), (Object)Boolean.FALSE);
        }

        private static File getRootFile(File startFrom, String pkgName) {
            int index = -1;
            while ((index = pkgName.indexOf(46, index + 1)) != -1) {
                startFrom = startFrom.getParentFile();
            }
            return startFrom;
        }

        private JavaFileObject createDelegate() {
            if (this.wasCommitted()) {
                this.delegate = Pair.of((Object)((FileObjects.FileBase)FileObjects.fileFileObject(this.f, CachedFileObject.getRootFile(this.getFile(), this.getPackage()), this.filter, this.encoding)), (Object)Boolean.TRUE);
            } else if (this.wasFlushed()) {
                this.delegate = Pair.of((Object)((FileObjects.FileBase)FileObjects.fileFileObject(this.shadowFile, CachedFileObject.getRootFile(this.shadowFile, this.getPackage()), this.filter, this.encoding)), (Object)Boolean.TRUE);
            }
            return (JavaFileObject)this.delegate.first();
        }

        public File getCurrentFile() {
            return this.shadowFile;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void flush(boolean inCommit) throws IOException {
            if (this.wasFlushed()) {
                return;
            }
            if (this.delegate != null && this.delegate.second() == Boolean.FALSE) {
                if (inCommit) {
                    this.shadowFile = this.f;
                    this.release();
                }
            } else {
                File f;
                if (inCommit) {
                    this.shadowFile = this.f;
                }
                if (!(f = this.getCurrentFile()).getParentFile().mkdirs() && !f.getParentFile().exists()) {
                    throw new IOException(String.format("Cannot create folder: %s", f.getParentFile().getAbsolutePath()));
                }
                FileOutputStream out = new FileOutputStream(f);
                try {
                    out.write(this.content);
                    this.release();
                }
                finally {
                    out.close();
                }
            }
        }

        void setShadowFile(File f) {
            this.shadowFile = f;
        }

        boolean wasCommitted() {
            return this.shadowFile == null || this.shadowFile == this.f;
        }

        boolean wasFlushed() {
            return this.writer == null;
        }

        void commit() throws IOException {
            if (this.wasCommitted()) {
                return;
            }
            this.flush(true);
            File cur = this.getCurrentFile();
            if (this.f.equals(cur)) {
                return;
            }
            if (this.f.exists() && !this.f.delete()) {
                throw new IOException("Cannot delete obsolete sigfile");
            }
            cur.renameTo(this.f);
            this.shadowFile = null;
        }

        void release() {
            this.content = null;
            this.writer = null;
            this.createDelegate();
        }

        @Override
        public boolean delete() {
            if (this.delegate != null && this.delegate.second() == Boolean.TRUE) {
                return ((FileObjects.FileBase)this.delegate.first()).delete();
            }
            if (this.writer != null) {
                this.writer.delete(WriteBackTransaction.toFile(this));
            }
            return true;
        }

        @Override
        public InputStream openInputStream() throws IOException {
            if (this.delegate != null) {
                return ((FileObjects.FileBase)this.delegate.first()).openInputStream();
            }
            return new ByteArrayInputStream(this.content);
        }

        @Override
        public OutputStream openOutputStream() throws IOException {
            this.modify();
            if (this.delegate != null) {
                return ((FileObjects.FileBase)this.delegate.first()).openOutputStream();
            }
            return new ByteArrayOutputStream(){
                boolean closed;

                @Override
                public void close() throws IOException {
                    if (this.closed) {
                        return;
                    }
                    super.close();
                    this.closed = true;
                    CachedFileObject.this.content = this.toByteArray();
                    CachedFileObject.this.writer.maybeFlush();
                }
            };
        }

        private void modify() {
            if (this.delegate != null && this.delegate.second() == Boolean.FALSE) {
                this.delegate = null;
            }
        }

    }

    private static class CacheRef
    extends SoftReference
    implements Runnable {
        private WriteBackTransaction storage;

        public CacheRef(WriteBackTransaction storage, Object referent) {
            super(referent, Utilities.activeReferenceQueue());
            this.storage = storage;
        }

        @Override
        public void run() {
            LOG.fine("Reference freed");
            this.storage.memExhausted = true;
        }
    }

}

