/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.Tree
 */
package org.netbeans.modules.java.source.save;

import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.Tree;
import java.util.List;
import org.netbeans.modules.java.source.save.CasualDiff;
import org.netbeans.modules.java.source.save.DiffContext;
import org.netbeans.modules.java.source.save.PositionEstimator;

final class EstimatorFactory {
    private EstimatorFactory() {
    }

    static PositionEstimator throwz(List<? extends ExpressionTree> oldL, List<? extends ExpressionTree> newL, DiffContext diffContext) {
        return new PositionEstimator.ThrowsEstimator(oldL, newL, diffContext);
    }

    static PositionEstimator implementz(List<? extends Tree> oldL, List<? extends Tree> newL, DiffContext diffContext) {
        return new PositionEstimator.ImplementsEstimator(oldL, newL, diffContext);
    }

    static PositionEstimator extendz(List<? extends Tree> oldL, List<? extends Tree> newL, DiffContext diffContext) {
        return new PositionEstimator.ExtendsEstimator(oldL, newL, diffContext);
    }

    static PositionEstimator statements(List<? extends Tree> oldL, List<? extends Tree> newL, DiffContext diffContext) {
        return new PositionEstimator.MembersEstimator(oldL, newL, diffContext, false);
    }

    static PositionEstimator catches(List<? extends Tree> oldL, List<? extends Tree> newL, boolean hasFinally, DiffContext diffContext) {
        return new PositionEstimator.CatchesEstimator(oldL, newL, hasFinally, diffContext);
    }

    static PositionEstimator cases(List<? extends Tree> oldL, List<? extends Tree> newL, DiffContext diffContext) {
        return new PositionEstimator.CasesEstimator(oldL, newL, diffContext);
    }

    static PositionEstimator members(List<? extends Tree> oldL, List<? extends Tree> newL, DiffContext diffContext) {
        return new PositionEstimator.MembersEstimator(oldL, newL, diffContext, true);
    }

    static PositionEstimator toplevel(List<? extends Tree> oldL, List<? extends Tree> newL, DiffContext diffContext) {
        return new PositionEstimator.TopLevelEstimator(oldL, newL, diffContext);
    }

    static PositionEstimator annotations(List<? extends Tree> oldL, List<? extends Tree> newL, DiffContext diffContext, boolean parameterPrint) {
        if (parameterPrint) {
            return new PositionEstimator.AnnotationsEstimator(oldL, newL, diffContext){

                @Override
                public int prepare(int startPos, StringBuilder aHead, StringBuilder aTail) {
                    int result = super.prepare(startPos, aHead, aTail);
                    aTail.append(" ");
                    return result;
                }

                @Override
                public CasualDiff.LineInsertionType lineInsertType() {
                    return CasualDiff.LineInsertionType.NONE;
                }
            };
        }
        return new PositionEstimator.AnnotationsEstimator(oldL, newL, diffContext);
    }

    static PositionEstimator imports(List<? extends ImportTree> oldL, List<? extends ImportTree> newL, DiffContext diffContext) {
        return new PositionEstimator.ImportsEstimator(oldL, newL, diffContext);
    }

}

