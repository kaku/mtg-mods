/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.util.Trees
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCCompilationUnit
 *  com.sun.tools.javac.util.Context
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.java.source.save;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.Trees;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.Context;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.swing.text.Document;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.source.CodeStyle;
import org.netbeans.api.java.source.Comment;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.PositionConverter;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.java.source.JavaSourceAccessor;
import org.netbeans.modules.java.source.save.BlockSequences;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.openide.filesystems.FileObject;

public class DiffContext {
    public final TokenSequence<JavaTokenId> tokenSequence;
    public final String origText;
    public final CodeStyle style;
    public final Context context;
    public final JCTree.JCCompilationUnit origUnit;
    public final Trees trees;
    public final Document doc;
    public final PositionConverter positionConverter;
    public final FileObject file;
    public final Set<Tree> syntheticTrees;
    public final JCTree.JCCompilationUnit mainUnit;
    public final String mainCode;
    public final int textLength;
    public final BlockSequences blockSequences;
    public final boolean forceInitialComment;
    public Map<Integer, Comment> usedComments = new HashMap<Integer, Comment>();

    public DiffContext(CompilationInfo copy) {
        this(copy, new HashSet<Tree>());
    }

    public DiffContext(CompilationInfo copy, Set<Tree> syntheticTrees) {
        this.tokenSequence = copy.getTokenHierarchy().tokenSequence(JavaTokenId.language());
        this.mainCode = this.origText = copy.getText();
        this.style = DiffContext.getCodeStyle(copy);
        this.context = JavaSourceAccessor.getINSTANCE().getJavacTask(copy).getContext();
        this.mainUnit = this.origUnit = (JCTree.JCCompilationUnit)copy.getCompilationUnit();
        this.trees = copy.getTrees();
        this.doc = copy.getSnapshot().getSource().getDocument(false);
        this.positionConverter = copy.getPositionConverter();
        this.file = copy.getFileObject();
        this.syntheticTrees = syntheticTrees;
        this.textLength = copy.getSnapshot() == null ? Integer.MAX_VALUE : copy.getSnapshot().getOriginalOffset(copy.getSnapshot().getText().length());
        this.blockSequences = new BlockSequences(this.tokenSequence, this.doc, this.textLength);
        this.forceInitialComment = false;
    }

    public DiffContext(CompilationInfo copy, CompilationUnitTree cut, String code, PositionConverter positionConverter, FileObject file, Set<Tree> syntheticTrees, CompilationUnitTree mainUnit, String mainCode) {
        this.tokenSequence = TokenHierarchy.create((CharSequence)code, (Language)JavaTokenId.language()).tokenSequence(JavaTokenId.language());
        this.origText = code;
        this.style = DiffContext.getCodeStyle(copy);
        this.context = JavaSourceAccessor.getINSTANCE().getJavacTask(copy).getContext();
        this.origUnit = (JCTree.JCCompilationUnit)cut;
        this.trees = copy.getTrees();
        this.doc = null;
        this.positionConverter = positionConverter;
        this.file = file;
        this.syntheticTrees = syntheticTrees;
        this.mainUnit = (JCTree.JCCompilationUnit)mainUnit;
        this.mainCode = mainCode;
        this.textLength = copy.getSnapshot() == null ? Integer.MAX_VALUE : copy.getSnapshot().getOriginalOffset(copy.getSnapshot().getText().length());
        this.blockSequences = new BlockSequences(this.tokenSequence, this.doc, this.textLength);
        this.forceInitialComment = true;
    }

    public static final CodeStyle getCodeStyle(CompilationInfo info) {
        if (info != null) {
            FileObject file;
            try {
                Document doc = info.getDocument();
                if (doc != null) {
                    CodeStyle cs = (CodeStyle)doc.getProperty(CodeStyle.class);
                    return cs != null ? cs : CodeStyle.getDefault(doc);
                }
            }
            catch (IOException ioe) {
                // empty catch block
            }
            if ((file = info.getFileObject()) != null) {
                return CodeStyle.getDefault(file);
            }
        }
        return CodeStyle.getDefault((Document)null);
    }
}

