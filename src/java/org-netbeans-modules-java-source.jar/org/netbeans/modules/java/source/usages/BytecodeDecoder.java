/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.source.usages;

import java.util.Iterator;

public class BytecodeDecoder
implements Iterator<byte[]>,
Iterable<byte[]> {
    static final int[] opcodeLengths = new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 3, 2, 3, 3, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 99, 99, 1, 1, 1, 1, 1, 1, 3, 3, 3, 3, 3, 3, 3, 5, 0, 3, 2, 3, 1, 1, 3, 3, 1, 1, 0, 4, 3, 3, 5, 5, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 5, 3, 3, 3, 3, 4, 3, 3, 3, 3, 3, 1, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
    private byte[] code;
    int currentIndex;

    public BytecodeDecoder(byte[] code) {
        this.code = code;
        this.currentIndex = 0;
    }

    @Override
    public Iterator<byte[]> iterator() {
        return this;
    }

    @Override
    public boolean hasNext() {
        if (this.currentIndex < this.code.length) {
            return true;
        }
        if (this.currentIndex != this.code.length) {
            throw new IllegalStateException("Bad end " + this.currentIndex + " vs. " + this.code.length);
        }
        return false;
    }

    @Override
    public byte[] next() {
        int length;
        int opCode = BytecodeDecoder.toInt(this.code[this.currentIndex]);
        if (opCode == 196) {
            int wideInstruction = BytecodeDecoder.toInt(this.code[this.currentIndex + 1]);
            switch (wideInstruction) {
                case 132: {
                    length = 6;
                    break;
                }
                case 21: 
                case 22: 
                case 23: 
                case 24: 
                case 25: 
                case 54: 
                case 55: 
                case 56: 
                case 57: 
                case 58: 
                case 169: {
                    length = 4;
                    break;
                }
                default: {
                    throw new IllegalArgumentException("Bad wide instruction at index " + this.currentIndex + " wide instruction " + wideInstruction);
                }
            }
        } else {
            length = opcodeLengths[opCode];
        }
        if (length == -1) {
            throw new IllegalArgumentException("Bad bytecode at index " + this.currentIndex + " opcode " + opCode);
        }
        if (length == 99) {
            switch (opCode) {
                int start;
                int padd;
                case 171: {
                    padd = 4 - this.currentIndex % 4;
                    start = this.currentIndex + padd + 4;
                    int npairs = BytecodeDecoder.toInt(this.code[start], this.code[start + 1], this.code[start + 2], this.code[start + 3]);
                    length = padd + 8 + npairs * 8;
                    break;
                }
                case 170: {
                    padd = 4 - this.currentIndex % 4;
                    start = this.currentIndex + padd + 4;
                    int low = BytecodeDecoder.toInt(this.code[start], this.code[start + 1], this.code[start + 2], this.code[start + 3]);
                    int high = BytecodeDecoder.toInt(this.code[start + 4], this.code[start + 5], this.code[start + 6], this.code[start + 7]);
                    length = padd + 12 + (high - low + 1) * 4;
                    break;
                }
                default: {
                    throw new IllegalArgumentException("Bad bytecode at index " + this.currentIndex);
                }
            }
        }
        byte[] currCode = new byte[length];
        for (int i = 0; i < length; ++i) {
            currCode[i] = this.code[this.currentIndex + i];
        }
        this.currentIndex += length;
        return currCode;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Byte code is read only");
    }

    static int toInt(byte b) {
        return b & 255;
    }

    static int toInt(byte b1, byte b2) {
        return BytecodeDecoder.toInt(b1) << 8 | BytecodeDecoder.toInt(b2);
    }

    static int toInt(byte b1, byte b2, byte b3, byte b4) {
        return BytecodeDecoder.toInt(b1) << 24 | BytecodeDecoder.toInt(b2) << 16 | BytecodeDecoder.toInt(b3) << 8 | BytecodeDecoder.toInt(b4);
    }
}

