/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.code.Symbol$Completer
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.api.java.queries.JavadocForBinaryQuery
 *  org.netbeans.api.java.queries.JavadocForBinaryQuery$Result
 *  org.netbeans.api.java.queries.SourceForBinaryQuery
 *  org.netbeans.modules.parsing.lucene.support.Convertor
 *  org.netbeans.modules.parsing.lucene.support.Convertors
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.Union2
 */
package org.netbeans.modules.java.source;

import com.sun.tools.javac.code.Symbol;
import java.awt.EventQueue;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.swing.text.ChangedCharSetException;
import javax.tools.JavaFileObject;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.queries.JavadocForBinaryQuery;
import org.netbeans.api.java.queries.SourceForBinaryQuery;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.parsing.CachingArchiveProvider;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.parsing.lucene.support.Convertor;
import org.netbeans.modules.parsing.lucene.support.Convertors;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;
import org.openide.util.Union2;

public class JavadocHelper {
    private static final Logger LOG = Logger.getLogger(JavadocHelper.class.getName());
    private static final RequestProcessor RP = new RequestProcessor(JavadocHelper.class.getName(), 1);
    private static final int DEFAULT_REMOTE_FILE_CONTENT_CACHE_SIZE = 50;
    private static final int REMOTE_FILE_CONTENT_CACHE_SIZE = Integer.getInteger("JavadocHelper.remoteCache.size", 50);
    private static final String PACKAGE_SUMMARY = "package-summary";
    private static final Set<String> knownGoodRoots = Collections.synchronizedSet(new HashSet());

    private JavadocHelper() {
    }

    private static boolean isRemote(URL url) {
        return url.getProtocol().startsWith("http") || url.getProtocol().startsWith("ftp");
    }

    public static InputStream openStream(URL url) throws IOException {
        FileObject f;
        if (url.getProtocol().equals("jar") && (f = URLMapper.findFileObject((URL)url)) != null) {
            return f.getInputStream();
        }
        if (JavadocHelper.isRemote(url)) {
            LOG.log(Level.FINE, "opening network stream: {0}", url);
        }
        return url.openStream();
    }

    public static TextStream getJavadoc(Element element, @NullAllowed Callable<Boolean> cancel) {
        return JavadocHelper.getJavadoc(element, true, cancel);
    }

    public static TextStream getJavadoc(Element element, boolean allowRemoteJavadoc, @NullAllowed Callable<Boolean> cancel) {
        try {
            List<TextStream> res = JavadocHelper.getJavadoc(element, allowRemoteJavadoc ? RemoteJavadocPolicy.USE : RemoteJavadocPolicy.IGNORE, cancel);
            return res.isEmpty() ? null : res.get(0);
        }
        catch (RemoteJavadocException rje) {
            throw new IllegalStateException("Never thrown", rje);
        }
    }

    @NonNull
    public static List<TextStream> getJavadoc(@NonNull Element element, @NonNull RemoteJavadocPolicy remoteJavadocPolicy, @NullAllowed Callable<Boolean> cancel) throws RemoteJavadocException {
        Parameters.notNull((CharSequence)"element", (Object)element);
        Parameters.notNull((CharSequence)"remoteJavadocPolicy", (Object)((Object)remoteJavadocPolicy));
        return JavadocHelper.doGetJavadoc(element, remoteJavadocPolicy, cancel);
    }

    public static TextStream getJavadoc(Element element) {
        return JavadocHelper.getJavadoc(element, null);
    }

    @CheckForNull
    public static String getCharSet(ChangedCharSetException e) {
        String spec = e.getCharSetSpec();
        if (e.keyEqualsCharSet()) {
            return spec;
        }
        int index = spec.indexOf(";");
        if (index != -1) {
            spec = spec.substring(index + 1);
        }
        spec = spec.toLowerCase();
        StringTokenizer st = new StringTokenizer(spec, " \t=", true);
        boolean foundCharSet = false;
        boolean foundEquals = false;
        while (st.hasMoreTokens()) {
            String token = st.nextToken();
            if (token.equals(" ") || token.equals("\t")) continue;
            if (!foundCharSet && !foundEquals && token.equals("charset")) {
                foundCharSet = true;
                continue;
            }
            if (!foundEquals && token.equals("=")) {
                foundEquals = true;
                continue;
            }
            if (foundEquals && foundCharSet) {
                return token;
            }
            foundCharSet = false;
            foundEquals = false;
        }
        return null;
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private static List<TextStream> doGetJavadoc(Element element, final RemoteJavadocPolicy remoteJavadocPolicy, Callable<Boolean> cancel) throws RemoteJavadocException {
        String pageName;
        String pkgName;
        if (element == null) {
            throw new IllegalArgumentException("Cannot pass null as an argument of the SourceUtils.getJavadoc");
        }
        Symbol.ClassSymbol clsSym = null;
        boolean buildFragment = false;
        if (element.getKind() == ElementKind.PACKAGE) {
            List<? extends Element> els = element.getEnclosedElements();
            for (Element e : els) {
                if (!e.getKind().isClass() && !e.getKind().isInterface()) continue;
                clsSym = (Symbol.ClassSymbol)e;
                break;
            }
            if (clsSym == null) {
                return Collections.emptyList();
            }
            pkgName = FileObjects.convertPackage2Folder(((PackageElement)element).getQualifiedName().toString());
            pageName = "package-summary";
        } else {
            Element e = element;
            StringBuilder sb = new StringBuilder();
            while (e.getKind() != ElementKind.PACKAGE) {
                if (e.getKind().isClass() || e.getKind().isInterface()) {
                    if (sb.length() > 0) {
                        sb.insert(0, '.');
                    }
                    sb.insert(0, e.getSimpleName());
                    if (clsSym == null) {
                        clsSym = (Symbol.ClassSymbol)e;
                    }
                }
                e = e.getEnclosingElement();
            }
            if (clsSym == null) {
                return Collections.emptyList();
            }
            pkgName = FileObjects.convertPackage2Folder(((PackageElement)e).getQualifiedName().toString());
            pageName = sb.toString();
            boolean bl = buildFragment = element != clsSym;
        }
        if (clsSym.completer != null) {
            clsSym.complete();
        }
        if (clsSym.classfile != null) {
            try {
                Collection fragment;
                final URL classFile = clsSym.classfile.toUri().toURL();
                final String pkgNameF = pkgName;
                final String pageNameF = pageName;
                Collection collection = fragment = buildFragment ? JavadocHelper.getFragment(element) : Collections.emptySet();
                if (cancel == null) {
                    return JavadocHelper.findJavadoc(classFile, pkgName, pageNameF, fragment, remoteJavadocPolicy);
                }
                Future future = RP.submit((Callable)new Callable<Union2<List<TextStream>, RemoteJavadocException>>(){

                    @Override
                    public Union2<List<TextStream>, RemoteJavadocException> call() throws Exception {
                        try {
                            return Union2.createFirst((Object)JavadocHelper.findJavadoc(classFile, pkgNameF, pageNameF, fragment, remoteJavadocPolicy));
                        }
                        catch (RemoteJavadocException rje) {
                            return Union2.createSecond((Object)rje);
                        }
                    }
                });
                do {
                    if (cancel != null && cancel.call().booleanValue()) {
                        future.cancel(false);
                        break;
                    }
                    try {
                        Union2 res = (Union2)future.get(100, TimeUnit.MILLISECONDS);
                        if (res != null) {
                            if (res.hasFirst()) {
                                return (List)res.first();
                            }
                            assert (res.hasSecond());
                            throw (RemoteJavadocException)res.second();
                        }
                        break;
                    }
                    catch (TimeoutException timeOut) {
                        continue;
                    }
                    break;
                } while (true);
            }
            catch (RemoteJavadocException rje) {
                throw rje;
            }
            catch (Exception e) {
                LOG.log(Level.INFO, null, e);
            }
        }
        return Collections.emptyList();
    }

    private static List<TextStream> findJavadoc(@NonNull URL classFile, @NonNull String pkgName, @NonNull String pageName, @NonNull Collection<? extends CharSequence> fragment, @NonNull RemoteJavadocPolicy remoteJavadocPolicy) throws RemoteJavadocException {
        ArrayList<TextStream> resList;
        block38 : {
            resList = new ArrayList<TextStream>();
            URL sourceRoot = null;
            HashSet<URL> binaries = new HashSet<URL>();
            try {
                FileObject sourceFo;
                FileObject fo = URLMapper.findFileObject((URL)classFile);
                StringTokenizer tk = new StringTokenizer(pkgName, "/");
                for (int i = 0; fo != null && i <= tk.countTokens(); fo = fo.getParent(), ++i) {
                }
                if (fo != null) {
                    URL url = CachingArchiveProvider.getDefault().mapCtSymToJar(fo.toURL());
                    sourceRoot = JavaIndex.getSourceRootForClassFolder(url);
                    if (sourceRoot == null) {
                        binaries.add(url);
                    } else {
                        binaries.add(sourceRoot);
                    }
                }
                if (sourceRoot != null && (sourceFo = URLMapper.findFileObject((URL)sourceRoot)) != null) {
                    ClassPath exec = ClassPath.getClassPath((FileObject)sourceFo, (String)"classpath/execute");
                    ClassPath compile = ClassPath.getClassPath((FileObject)sourceFo, (String)"classpath/compile");
                    ClassPath source = ClassPath.getClassPath((FileObject)sourceFo, (String)"classpath/source");
                    if (exec == null) {
                        exec = compile;
                        compile = null;
                    }
                    if (exec != null && source != null) {
                        HashSet<URL> roots = new HashSet<URL>();
                        for (ClassPath.Entry e2 : exec.entries()) {
                            roots.add(e2.getURL());
                        }
                        if (compile != null) {
                            for (ClassPath.Entry e : compile.entries()) {
                                roots.remove(e.getURL());
                            }
                        }
                        List<FileObject> sourceRoots = Arrays.asList(source.getRoots());
                        block17 : for (URL e3 : roots) {
                            FileObject[] res;
                            for (FileObject r : res = SourceForBinaryQuery.findSourceRoots((URL)e3).getRoots()) {
                                if (!sourceRoots.contains((Object)r)) continue;
                                binaries.add(e3);
                                continue block17;
                            }
                        }
                    }
                }
                for (URL binary : binaries) {
                    URL[] result;
                    JavadocForBinaryQuery.Result javadocResult = JavadocForBinaryQuery.findJavadoc((URL)binary);
                    block20 : for (URL root : result = javadocResult.getRoots()) {
                        boolean speculative;
                        block37 : {
                            URL url;
                            InputStream is;
                            block36 : {
                                boolean useKnownGoodRoots;
                                if (!root.toExternalForm().endsWith("/")) {
                                    LOG.log(Level.WARNING, "JavadocForBinaryQuery.Result: {0} returned non-folder URL: {1}, ignoring", new Object[]{javadocResult.getClass(), root.toExternalForm()});
                                    continue;
                                }
                                boolean isRemote = JavadocHelper.isRemote(root);
                                speculative = false;
                                if (isRemote) {
                                    switch (remoteJavadocPolicy) {
                                        case EXCEPTION: {
                                            throw new RemoteJavadocException(root);
                                        }
                                        case IGNORE: {
                                            continue block20;
                                        }
                                        case USE: {
                                            break;
                                        }
                                        case SPECULATIVE: {
                                            speculative = true;
                                            break;
                                        }
                                        default: {
                                            throw new IllegalArgumentException(remoteJavadocPolicy.name());
                                        }
                                    }
                                }
                                url = new URL(root, pkgName + "/" + pageName + ".html");
                                is = null;
                                String rootS = root.toString();
                                boolean bl = useKnownGoodRoots = result.length == 1 && isRemote;
                                if (useKnownGoodRoots && knownGoodRoots.contains(rootS)) {
                                    LOG.log(Level.FINE, "assumed valid Javadoc stream at {0}", url);
                                } else if (!speculative || !isRemote) {
                                    try {
                                        is = JavadocHelper.openStream(url);
                                        if (!useKnownGoodRoots) break block36;
                                        knownGoodRoots.add(rootS);
                                        LOG.log(Level.FINE, "found valid Javadoc stream at {0}", url);
                                    }
                                    catch (IOException x) {
                                        LOG.log(Level.FINE, "invalid Javadoc stream at {0}: {1}", new Object[]{url, x});
                                        continue;
                                    }
                                }
                            }
                            if (!fragment.isEmpty()) {
                                try {
                                    ArrayList<URL> urls = new ArrayList<URL>(fragment.size());
                                    for (CharSequence f : fragment) {
                                        String encodedfragment = URLEncoder.encode(f.toString(), "UTF-8").replace("+", "%20");
                                        urls.add(new URI(url.toExternalForm() + '#' + encodedfragment).toURL());
                                    }
                                    resList.add(new TextStream(urls, is));
                                    if (speculative) break block37;
                                    break block38;
                                }
                                catch (URISyntaxException x) {
                                    LOG.log(Level.INFO, null, x);
                                    break block37;
                                }
                                catch (UnsupportedEncodingException x) {
                                    LOG.log(Level.INFO, null, x);
                                    break block37;
                                }
                                catch (MalformedURLException x) {
                                    LOG.log(Level.INFO, null, x);
                                    break block37;
                                }
                            }
                            resList.add(new TextStream(Collections.singleton(url), is));
                        }
                        if (speculative) {
                            continue;
                        }
                        break block38;
                    }
                }
            }
            catch (MalformedURLException x) {
                LOG.log(Level.INFO, null, x);
            }
        }
        return resList;
    }

    @NonNull
    private static Collection<? extends CharSequence> getFragment(Element e) {
        FragmentBuilder fb = new FragmentBuilder();
        if (!e.getKind().isClass() && !e.getKind().isInterface()) {
            if (e.getKind() == ElementKind.CONSTRUCTOR) {
                fb.append(e.getEnclosingElement().getSimpleName());
            } else {
                fb.append(e.getSimpleName());
            }
            if (e.getKind() == ElementKind.METHOD || e.getKind() == ElementKind.CONSTRUCTOR) {
                ExecutableElement ee = (ExecutableElement)e;
                fb.append("(");
                Iterator<? extends VariableElement> it = ee.getParameters().iterator();
                while (it.hasNext()) {
                    VariableElement param = it.next();
                    JavadocHelper.appendType(fb, param.asType(), ee.isVarArgs() && !it.hasNext());
                    if (!it.hasNext()) continue;
                    fb.append(", ");
                }
                fb.append(")");
            }
        }
        return fb.getFragments();
    }

    private static void appendType(FragmentBuilder fb, TypeMirror type, boolean varArg) {
        switch (type.getKind()) {
            case ARRAY: {
                JavadocHelper.appendType(fb, ((ArrayType)type).getComponentType(), false);
                fb.append(varArg ? "..." : "[]");
                break;
            }
            case DECLARED: {
                fb.append(((TypeElement)((DeclaredType)type).asElement()).getQualifiedName());
                break;
            }
            default: {
                fb.append(type.toString());
            }
        }
    }

    private static final class FragmentBuilder {
        private static final List<Convertor<CharSequence, CharSequence>> FILTERS;
        private final StringBuilder[] sbs = new StringBuilder[FILTERS.size()];

        FragmentBuilder() {
            for (int i = 0; i < this.sbs.length; ++i) {
                this.sbs[i] = new StringBuilder();
            }
        }

        @NonNull
        FragmentBuilder append(@NonNull CharSequence text) {
            for (int i = 0; i < this.sbs.length; ++i) {
                this.sbs[i].append((CharSequence)FILTERS.get(i).convert((Object)text));
            }
            return this;
        }

        @NonNull
        Collection<? extends CharSequence> getFragments() {
            ArrayList<String> res = new ArrayList<String>(this.sbs.length);
            for (StringBuilder sb : this.sbs) {
                res.add(sb.toString());
            }
            return Collections.unmodifiableCollection(res);
        }

        static {
            ArrayList<Convertor> tmp = new ArrayList<Convertor>();
            tmp.add(Convertors.identity());
            tmp.add(new JDoc8025633());
            FILTERS = Collections.unmodifiableList(tmp);
        }

        private static final class JDoc8025633
        implements Convertor<CharSequence, CharSequence> {
            private JDoc8025633() {
            }

            @NonNull
            public CharSequence convert(@NonNull CharSequence text) {
                StringBuilder sb = new StringBuilder();
                block7 : for (int i = 0; i < text.length(); ++i) {
                    char c = text.charAt(i);
                    switch (c) {
                        case '(': 
                        case ')': 
                        case ',': 
                        case '<': 
                        case '>': {
                            sb.append('-');
                            continue block7;
                        }
                        case ' ': 
                        case '[': {
                            continue block7;
                        }
                        case ']': {
                            sb.append(":A");
                            continue block7;
                        }
                        case '$': {
                            if (i == 0) {
                                sb.append("Z:Z");
                            }
                            sb.append(":D");
                            continue block7;
                        }
                        case '_': {
                            if (i == 0) {
                                sb.append("Z:Z");
                            }
                        }
                        default: {
                            sb.append(c);
                        }
                    }
                }
                return sb.toString();
            }
        }

    }

    public static final class TextStream {
        private static Map<URI, byte[]> remoteFileContentCache = Collections.synchronizedMap(new LinkedHashMap<URI, byte[]>(16, 0.75f, true){

            @Override
            protected boolean removeEldestEntry(Map.Entry<URI, byte[]> eldest) {
                return this.size() > REMOTE_FILE_CONTENT_CACHE_SIZE;
            }
        });
        private final List<? extends URL> urls;
        private final AtomicReference<InputStream> stream = new AtomicReference();
        private byte[] cache;

        public TextStream(@NonNull URL url) {
            Parameters.notNull((CharSequence)"url", (Object)url);
            this.urls = Collections.singletonList(url);
        }

        TextStream(@NonNull Collection<? extends URL> urls) {
            Parameters.notNull((CharSequence)"urls", urls);
            ArrayList<URL> tmpUrls = new ArrayList<URL>(urls.size());
            for (URL u : urls) {
                Parameters.notNull((CharSequence)"urls[]", (Object)u);
                tmpUrls.add(u);
            }
            if (tmpUrls.isEmpty()) {
                throw new IllegalArgumentException("At least one URL has to be given.");
            }
            this.urls = Collections.unmodifiableList(tmpUrls);
        }

        TextStream(@NonNull Collection<? extends URL> urls, InputStream stream) {
            this(urls);
            this.stream.set(stream);
        }

        @CheckForNull
        public URL getLocation() {
            return this.urls.iterator().next();
        }

        @NonNull
        public List<? extends URL> getLocations() {
            return this.urls;
        }

        public void close() {
            InputStream is = this.stream.getAndSet(null);
            if (is != null) {
                try {
                    is.close();
                }
                catch (IOException x) {
                    LOG.log(Level.INFO, null, x);
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public synchronized InputStream openStream() throws IOException {
            if (this.cache != null) {
                LOG.log(Level.FINE, "loaded cached content for {0}", this.getLocation());
                return new ByteArrayInputStream(this.cache);
            }
            assert (!this.isRemote() || !EventQueue.isDispatchThread());
            InputStream uncached = this.stream.getAndSet(null);
            if (this.isRemote()) {
                try {
                    byte[] data;
                    URI fileURI = this.getFileURI();
                    byte[] arrby = data = fileURI == null ? null : remoteFileContentCache.get(fileURI);
                    if (data == null) {
                        if (uncached == null) {
                            uncached = JavadocHelper.openStream(this.getLocation());
                        }
                        ByteArrayOutputStream baos = new ByteArrayOutputStream(20480);
                        FileUtil.copy((InputStream)uncached, (OutputStream)baos);
                        data = baos.toByteArray();
                        if (fileURI != null) {
                            remoteFileContentCache.put(fileURI, data);
                        }
                    }
                    this.cache = data;
                }
                finally {
                    if (uncached != null) {
                        uncached.close();
                    }
                }
                LOG.log(Level.FINE, "cached content for {0} ({1}k)", new Object[]{this.getLocation(), this.cache.length / 1024});
                return new ByteArrayInputStream(this.cache);
            }
            if (uncached == null) {
                uncached = JavadocHelper.openStream(this.getLocation());
            }
            return uncached;
        }

        public boolean isRemote() {
            return JavadocHelper.isRemote(this.getLocation());
        }

        @CheckForNull
        private URI getFileURI() {
            URL location = this.getLocation();
            String surl = location.toString();
            int index = surl.lastIndexOf(35);
            try {
                return index < 0 ? location.toURI() : new URI(surl.substring(0, index));
            }
            catch (URISyntaxException use) {
                return null;
            }
        }

    }

    public static final class RemoteJavadocException
    extends Exception {
        private final URL root;

        public RemoteJavadocException(@NullAllowed URL root) {
            this.root = root;
        }

        @CheckForNull
        public URL getRoot() {
            return this.root;
        }
    }

    public static enum RemoteJavadocPolicy {
        USE,
        IGNORE,
        EXCEPTION,
        SPECULATIVE;
        

        private RemoteJavadocPolicy() {
        }
    }

}

