/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.AnnotationTree
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.CaseTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.EnhancedForLoopTree
 *  com.sun.source.tree.ExpressionStatementTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.ForLoopTree
 *  com.sun.source.tree.IfTree
 *  com.sun.source.tree.MethodInvocationTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.NewArrayTree
 *  com.sun.source.tree.NewClassTree
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.SwitchTree
 *  com.sun.source.tree.SynchronizedTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.TryTree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.tree.WhileLoopTree
 *  com.sun.source.util.DocSourcePositions
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreeScanner
 *  com.sun.source.util.Trees
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.api.JavacTrees
 *  com.sun.tools.javac.main.JavaCompiler
 *  com.sun.tools.javac.util.Context
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.modules.editor.indent.spi.Context
 *  org.netbeans.modules.editor.indent.spi.Context$Region
 *  org.netbeans.modules.editor.indent.spi.ExtraLock
 *  org.netbeans.modules.editor.indent.spi.IndentTask
 *  org.netbeans.modules.editor.indent.spi.IndentTask$Factory
 *  org.netbeans.modules.parsing.api.Embedding
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.impl.Utilities
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 */
package org.netbeans.modules.java.source.save;

import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.CaseTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.EnhancedForLoopTree;
import com.sun.source.tree.ExpressionStatementTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.ForLoopTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.NewArrayTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.SwitchTree;
import com.sun.source.tree.SynchronizedTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TryTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.tree.WhileLoopTree;
import com.sun.source.util.DocSourcePositions;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreeScanner;
import com.sun.source.util.Trees;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.api.JavacTrees;
import com.sun.tools.javac.util.Context;
import java.io.Writer;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticListener;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.ToolProvider;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.source.CodeStyle;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.editor.indent.spi.Context;
import org.netbeans.modules.editor.indent.spi.ExtraLock;
import org.netbeans.modules.editor.indent.spi.IndentTask;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.impl.Utilities;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;

public class Reindenter
implements IndentTask {
    private final org.netbeans.modules.editor.indent.spi.Context context;
    private CodeStyle cs;
    private TokenSequence<JavaTokenId> ts;
    private String text;
    private CompilationUnitTree cut;
    private SourcePositions sp;
    private Map<Integer, Integer> newIndents;
    private int currentEmbeddingStartOffset;
    private Embedding currentEmbedding;

    private Reindenter(org.netbeans.modules.editor.indent.spi.Context context) {
        this.context = context;
    }

    public void reindent() throws BadLocationException {
        this.ts = null;
        this.currentEmbedding = null;
        this.newIndents = new HashMap<Integer, Integer>();
        this.cs = CodeStyle.getDefault(this.context.document());
        for (Context.Region region : this.context.indentRegions()) {
            if (!this.initRegionData(region)) continue;
            HashSet<Integer> linesToAddStar = new HashSet<Integer>();
            LinkedList<Integer> startOffsets = this.getStartOffsets(region);
            ListIterator<Integer> it = startOffsets.listIterator();
            while (it.hasNext()) {
                int originalEndOffset;
                int originalStartOffset = it.next();
                if (it.hasNext()) {
                    originalEndOffset = it.next() - 1;
                    it.previous();
                } else {
                    originalEndOffset = region.getEndOffset();
                }
                int startOffset = this.getEmbeddedOffset(originalStartOffset);
                int endOffset = this.getEmbeddedOffset(originalEndOffset);
                int delta = this.ts.move(startOffset);
                if ((startOffset != 0 && delta <= 0 || !this.ts.moveNext()) && !this.ts.movePrevious() || this.ts.token().id() == JavaTokenId.BLOCK_COMMENT && this.ts.embedded() != null) continue;
                if (this.cs.addLeadingStarInComment() && (this.ts.token().id() == JavaTokenId.BLOCK_COMMENT && this.cs.enableBlockCommentFormatting() || this.ts.token().id() == JavaTokenId.JAVADOC_COMMENT && this.cs.enableJavadocFormatting())) {
                    int idx;
                    String blockCommentLine = this.ts.token().text().toString();
                    if (delta > 0) {
                        idx = blockCommentLine.indexOf(10, delta);
                        blockCommentLine = (idx < 0 ? blockCommentLine.substring(delta) : blockCommentLine.substring(delta, idx)).trim();
                        int off = this.getOriginalOffset(this.ts.offset() + delta - 1);
                        int prevLineStartOffset = this.context.lineStartOffset(off < 0 ? originalStartOffset : off);
                        Integer prevLineIndent = this.newIndents.get(prevLineStartOffset);
                        this.newIndents.put(originalStartOffset, (prevLineIndent != null ? prevLineIndent.intValue() : this.context.lineIndent(prevLineStartOffset)) + (prevLineStartOffset > this.getOriginalOffset(this.ts.offset()) ? 0 : 1));
                    } else {
                        idx = blockCommentLine.lastIndexOf(10);
                        if (idx > 0) {
                            blockCommentLine = blockCommentLine.substring(idx).trim();
                        }
                        this.newIndents.put(originalStartOffset, this.getNewIndent(startOffset, endOffset) + 1);
                    }
                    if (blockCommentLine.startsWith("*")) continue;
                    linesToAddStar.add(originalStartOffset);
                    continue;
                }
                if (delta == 0 && this.ts.moveNext() && this.ts.token().id() == JavaTokenId.LINE_COMMENT) {
                    this.newIndents.put(originalStartOffset, 0);
                    continue;
                }
                this.newIndents.put(originalStartOffset, this.getNewIndent(startOffset, endOffset));
            }
            while (!startOffsets.isEmpty()) {
                char c;
                int startOffset = startOffsets.removeLast();
                Integer newIndent = this.newIndents.get(startOffset);
                if (linesToAddStar.contains(startOffset)) {
                    this.context.modifyIndent(startOffset, 0);
                    this.context.document().insertString(startOffset, "* ", null);
                }
                if (newIndent != null) {
                    this.context.modifyIndent(startOffset, newIndent.intValue());
                }
                if (startOffsets.isEmpty()) continue;
                int len = 0;
                while ((c = this.text.charAt(startOffset - 2 - len)) != '\n' && Character.isWhitespace(c)) {
                    ++len;
                }
                if (len <= 0) continue;
                this.context.document().remove(startOffset - 1 - len, len);
            }
        }
    }

    public ExtraLock indentLock() {
        return "text/x-java".equals(this.context.mimePath()) ? null : new ExtraLock(){

            public void lock() {
                Utilities.acquireParserLock();
            }

            public void unlock() {
                Utilities.releaseParserLock();
            }
        };
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean initRegionData(final Context.Region region) {
        this.currentEmbeddingStartOffset = 0;
        if (this.ts == null || this.currentEmbedding != null && (!this.currentEmbedding.containsOriginalOffset(region.getStartOffset()) || !this.currentEmbedding.containsOriginalOffset(region.getEndOffset()))) {
            if ("text/x-java".equals(this.context.mimePath())) {
                this.ts = TokenHierarchy.get((Document)this.context.document()).tokenSequence(JavaTokenId.language());
                if (this.ts == null) {
                    return false;
                }
                ClassLoader origCL = Thread.currentThread().getContextClassLoader();
                try {
                    Thread.currentThread().setContextClassLoader(Reindenter.class.getClassLoader());
                    JavacTaskImpl javacTask = (JavacTaskImpl)ToolProvider.getSystemJavaCompiler().getTask(null, null, new DiagnosticListener<JavaFileObject>(){

                        @Override
                        public void report(Diagnostic<? extends JavaFileObject> diagnostic) {
                        }
                    }, Collections.singletonList("-proc:none"), null, Collections.emptySet());
                    Context ctx = javacTask.getContext();
                    com.sun.tools.javac.main.JavaCompiler.instance((Context)ctx).genEndPos = true;
                    this.text = this.context.document().getText(0, this.context.document().getLength());
                    this.cut = (CompilationUnitTree)javacTask.parse(new JavaFileObject[]{FileObjects.memoryFileObject("", "", this.text)}).iterator().next();
                    this.sp = JavacTrees.instance((Context)ctx).getSourcePositions();
                }
                catch (Exception ex) {
                    boolean ctx = false;
                    return ctx;
                }
                finally {
                    Thread.currentThread().setContextClassLoader(origCL);
                }
            }
            Source source = Source.create((Document)this.context.document());
            if (source == null) {
                return false;
            }
            for (TokenSequence tseq = TokenHierarchy.get((Document)this.context.document()).tokenSequence(); tseq != null && (region.getStartOffset() == 0 || tseq.moveNext()); tseq = tseq.embedded()) {
                tseq.move(region.getStartOffset());
                if (tseq.language() == JavaTokenId.language() || !tseq.moveNext() && !tseq.movePrevious()) break;
                this.currentEmbeddingStartOffset = tseq.offset();
            }
            try {
                ParserManager.parse(Collections.singletonList(source), (UserTask)new UserTask(){

                    public void run(ResultIterator resultIterator) throws Exception {
                        Parser.Result result = Reindenter.this.findEmbeddedJava(resultIterator, region);
                        if (result != null) {
                            CompilationController controller = CompilationController.get(result);
                            controller.toPhase(JavaSource.Phase.PARSED);
                            Reindenter.this.ts = controller.getTokenHierarchy().tokenSequence(JavaTokenId.language());
                            Reindenter.this.text = controller.getText();
                            Reindenter.this.cut = controller.getCompilationUnit();
                            Reindenter.this.sp = controller.getTrees().getSourcePositions();
                        }
                    }
                });
                if (this.ts == null) {
                    return false;
                }
            }
            catch (ParseException pe) {
                return false;
            }
        }
        return true;
    }

    private Parser.Result findEmbeddedJava(ResultIterator theMess, Context.Region region) throws ParseException {
        LinkedList<Embedding> todo = new LinkedList<Embedding>();
        for (Embedding embedding2 : theMess.getEmbeddings()) {
            if ("text/x-java".equals(embedding2.getMimeType()) && embedding2.containsOriginalOffset(region.getStartOffset()) && embedding2.containsOriginalOffset(region.getEndOffset())) {
                this.currentEmbedding = embedding2;
                return theMess.getResultIterator(this.currentEmbedding).getParserResult();
            }
            todo.add(embedding2);
        }
        for (Embedding embedding2 : todo) {
            Parser.Result result = this.findEmbeddedJava(theMess.getResultIterator(embedding2), region);
            if (result == null) continue;
            return result;
        }
        return null;
    }

    private LinkedList<Integer> getStartOffsets(Context.Region region) throws BadLocationException {
        int lso;
        LinkedList<Integer> offsets = new LinkedList<Integer>();
        int offset = region.getEndOffset();
        while (offset > 0 && (lso = this.context.lineStartOffset(offset)) >= region.getStartOffset()) {
            offsets.addFirst(lso);
            offset = lso - 1;
        }
        return offsets;
    }

    private int getNewIndent(int startOffset, int endOffset) throws BadLocationException {
        LinkedList<? extends Tree> path = this.getPath(startOffset);
        if (path.isEmpty()) {
            return 0;
        }
        Tree last = path.getFirst();
        int lastPos = (int)this.sp.getStartPosition(this.cut, last);
        int currentIndent = this.getCurrentIndent(last, path);
        block0 : switch (last.getKind()) {
            case COMPILATION_UNIT: {
                break;
            }
            case CLASS: 
            case INTERFACE: 
            case ENUM: 
            case ANNOTATION_TYPE: {
                JavaTokenId nextTokenId;
                JavaTokenId prevTokenId;
                Tree member;
                TokenSequence<JavaTokenId> token = this.findFirstNonWhitespaceToken(startOffset, endOffset);
                JavaTokenId javaTokenId = nextTokenId = token != null ? (JavaTokenId)token.token().id() : null;
                if (nextTokenId != null && nextTokenId == JavaTokenId.RBRACE) {
                    if (!this.isLeftBraceOnNewLine(lastPos, startOffset)) break;
                    switch (this.cs.getClassDeclBracePlacement()) {
                        case NEW_LINE_INDENTED: {
                            currentIndent += this.cs.getIndentSize();
                            break block0;
                        }
                        case NEW_LINE_HALF_INDENTED: {
                            currentIndent += this.cs.getIndentSize() / 2;
                        }
                    }
                    break;
                }
                Tree t = null;
                Iterator i$ = ((ClassTree)last).getMembers().iterator();
                while (i$.hasNext() && this.sp.getEndPosition(this.cut, member = (Tree)i$.next()) <= (long)startOffset) {
                    t = member;
                }
                if (t != null) {
                    int i = this.getCurrentIndent(t, path);
                    currentIndent = i < 0 ? currentIndent + (this.cs.indentTopLevelClassMembers() ? this.cs.getIndentSize() : 0) : i;
                    break;
                }
                token = this.findFirstNonWhitespaceToken(startOffset, lastPos);
                JavaTokenId javaTokenId2 = prevTokenId = token != null ? (JavaTokenId)token.token().id() : null;
                if (prevTokenId == null) break;
                switch (prevTokenId) {
                    case LBRACE: {
                        if (path.get(1).getKind() == Tree.Kind.NEW_CLASS && this.isLeftBraceOnNewLine(lastPos, startOffset)) {
                            switch (this.cs.getClassDeclBracePlacement()) {
                                case SAME_LINE: 
                                case NEW_LINE: {
                                    currentIndent += this.cs.getIndentSize();
                                    break block0;
                                }
                                case NEW_LINE_HALF_INDENTED: {
                                    currentIndent += this.cs.getIndentSize() - this.cs.getIndentSize() / 2;
                                }
                            }
                            break block0;
                        }
                        currentIndent += this.cs.indentTopLevelClassMembers() ? this.cs.getIndentSize() : 0;
                        break block0;
                    }
                    case COMMA: {
                        currentIndent = this.getMultilineIndent(((ClassTree)last).getImplementsClause(), path, token.offset(), currentIndent, this.cs.alignMultilineImplements(), true);
                        break block0;
                    }
                    case IDENTIFIER: 
                    case GT: 
                    case GTGT: 
                    case GTGTGT: {
                        if (nextTokenId == null || nextTokenId != JavaTokenId.LBRACE) break block0;
                        switch (this.cs.getClassDeclBracePlacement()) {
                            case NEW_LINE_INDENTED: {
                                currentIndent += this.cs.getIndentSize();
                                break block0;
                            }
                            case NEW_LINE_HALF_INDENTED: {
                                currentIndent += this.cs.getIndentSize() / 2;
                            }
                        }
                        break block0;
                    }
                }
                currentIndent += this.cs.getContinuationIndentSize();
                break;
            }
            case METHOD: {
                JavaTokenId prevTokenId;
                TokenSequence<JavaTokenId> token = this.findFirstNonWhitespaceToken(startOffset, lastPos);
                JavaTokenId javaTokenId = prevTokenId = token != null ? (JavaTokenId)token.token().id() : null;
                if (prevTokenId == null) break;
                switch (prevTokenId) {
                    case COMMA: {
                        List thrws = ((MethodTree)last).getThrows();
                        if (!thrws.isEmpty() && this.sp.getStartPosition(this.cut, (Tree)thrws.get(0)) < (long)token.offset()) {
                            currentIndent = this.getMultilineIndent(thrws, path, token.offset(), currentIndent, this.cs.alignMultilineThrows(), true);
                            break block0;
                        }
                        currentIndent = this.getMultilineIndent(((MethodTree)last).getParameters(), path, token.offset(), currentIndent, this.cs.alignMultilineMethodParams(), true);
                        break block0;
                    }
                    case IDENTIFIER: 
                    case GT: 
                    case GTGT: 
                    case GTGTGT: 
                    case RPAREN: {
                        token = this.findFirstNonWhitespaceToken(startOffset, endOffset);
                        if (token == null || token.token().id() != JavaTokenId.LBRACE) break;
                        switch (this.cs.getMethodDeclBracePlacement()) {
                            case NEW_LINE_INDENTED: {
                                currentIndent += this.cs.getIndentSize();
                                break block0;
                            }
                            case NEW_LINE_HALF_INDENTED: {
                                currentIndent += this.cs.getIndentSize() / 2;
                            }
                        }
                        break block0;
                    }
                }
                token = this.findFirstNonWhitespaceToken(startOffset, endOffset);
                if (token != null && token.token().id() == JavaTokenId.RPAREN) break;
                currentIndent += this.cs.getContinuationIndentSize();
                break;
            }
            case VARIABLE: {
                Tree type = ((VariableTree)last).getType();
                if (type != null && type.getKind() != Tree.Kind.ERRONEOUS) {
                    TokenSequence<JavaTokenId> token;
                    ExpressionTree init = ((VariableTree)last).getInitializer();
                    if (init == null || init.getKind() != Tree.Kind.NEW_ARRAY || (token = this.findFirstNonWhitespaceToken(startOffset, lastPos)) == null || token.token().id() != JavaTokenId.EQ || (token = this.findFirstNonWhitespaceToken(startOffset, endOffset)) == null || token.token().id() != JavaTokenId.LBRACE) {
                        if (this.cs.alignMultilineAssignment()) {
                            int c = this.getColumn(last);
                            if (c < 0) break;
                            currentIndent = c;
                            break;
                        }
                        currentIndent += this.cs.getContinuationIndentSize();
                        break;
                    }
                    switch (this.cs.getOtherBracePlacement()) {
                        case NEW_LINE_INDENTED: {
                            currentIndent += this.cs.getIndentSize();
                            break block0;
                        }
                        case NEW_LINE_HALF_INDENTED: {
                            currentIndent += this.cs.getIndentSize() / 2;
                        }
                    }
                    break;
                }
                if ((last = ((VariableTree)last).getModifiers()) == null) break;
            }
            case MODIFIERS: {
                Tree ann;
                Tree t = null;
                Iterator i$ = ((ModifiersTree)last).getAnnotations().iterator();
                while (i$.hasNext() && this.sp.getEndPosition(this.cut, ann = (Tree)i$.next()) <= (long)startOffset) {
                    t = ann;
                }
                if (t != null && this.findFirstNonWhitespaceToken(startOffset, (int)this.sp.getEndPosition(this.cut, t)) == null) break;
                currentIndent += this.cs.getContinuationIndentSize();
                break;
            }
            case DO_WHILE_LOOP: {
                TokenSequence<JavaTokenId> token = this.findFirstNonWhitespaceToken(startOffset, lastPos);
                if (token == null || EnumSet.of(JavaTokenId.RBRACE, JavaTokenId.SEMICOLON).contains((Object)token.token().id())) break;
                currentIndent = this.getStmtIndent(startOffset, endOffset, EnumSet.of(JavaTokenId.DO), lastPos, currentIndent);
                break;
            }
            case ENHANCED_FOR_LOOP: {
                currentIndent = this.getStmtIndent(startOffset, endOffset, EnumSet.of(JavaTokenId.RPAREN), (int)this.sp.getEndPosition(this.cut, (Tree)((EnhancedForLoopTree)last).getExpression()), currentIndent);
                break;
            }
            case FOR_LOOP: {
                ExpressionStatementTree est;
                StatementTree st;
                LinkedList<Object> forTrees = new LinkedList<Object>();
                Iterator i$ = ((ForLoopTree)last).getInitializer().iterator();
                while (i$.hasNext() && this.sp.getEndPosition(this.cut, (Tree)(st = (StatementTree)i$.next())) <= (long)startOffset) {
                    forTrees.add((Object)st);
                }
                ExpressionTree t = ((ForLoopTree)last).getCondition();
                if (t != null && this.sp.getEndPosition(this.cut, (Tree)t) <= (long)startOffset) {
                    forTrees.add((Object)t);
                }
                i$ = ((ForLoopTree)last).getUpdate().iterator();
                while (i$.hasNext() && this.sp.getEndPosition(this.cut, (Tree)(est = (ExpressionStatementTree)i$.next())) <= (long)startOffset) {
                    forTrees.add((Object)est);
                }
                TokenSequence<JavaTokenId> token = this.findFirstNonWhitespaceToken(startOffset, lastPos);
                if (token != null && token.token().id() == JavaTokenId.SEMICOLON) {
                    currentIndent = this.getMultilineIndent(forTrees, path, token.offset(), currentIndent, this.cs.alignMultilineFor(), true);
                    break;
                }
                currentIndent = this.getStmtIndent(startOffset, endOffset, EnumSet.of(JavaTokenId.RPAREN), forTrees.isEmpty() ? lastPos : (int)this.sp.getEndPosition(this.cut, (Tree)forTrees.getLast()), currentIndent);
                break;
            }
            case IF: {
                TokenSequence<JavaTokenId> token = this.findFirstNonWhitespaceToken(startOffset, endOffset);
                if (token != null && token.token().id() == JavaTokenId.ELSE || (token = this.findFirstNonWhitespaceToken(startOffset, lastPos)) == null || EnumSet.of(JavaTokenId.RBRACE, JavaTokenId.SEMICOLON).contains((Object)token.token().id())) break;
                currentIndent = this.getStmtIndent(startOffset, endOffset, EnumSet.of(JavaTokenId.RPAREN, JavaTokenId.ELSE), (int)this.sp.getEndPosition(this.cut, (Tree)((IfTree)last).getCondition()) - 1, currentIndent);
                break;
            }
            case SYNCHRONIZED: {
                currentIndent = this.getStmtIndent(startOffset, endOffset, EnumSet.of(JavaTokenId.RPAREN), (int)this.sp.getEndPosition(this.cut, (Tree)((SynchronizedTree)last).getExpression()) - 1, currentIndent);
                break;
            }
            case TRY: {
                Tree res;
                TokenSequence<JavaTokenId> token = this.findFirstNonWhitespaceToken(startOffset, endOffset);
                if (token != null && EnumSet.of(JavaTokenId.CATCH, JavaTokenId.FINALLY).contains((Object)token.token().id()) || (token = this.findFirstNonWhitespaceToken(startOffset, lastPos)) == null || token.token().id() == JavaTokenId.RBRACE) break;
                Tree t = null;
                Iterator i$ = ((TryTree)last).getResources().iterator();
                while (i$.hasNext() && this.sp.getEndPosition(this.cut, res = (Tree)i$.next()) <= (long)startOffset) {
                    t = res;
                }
                currentIndent = this.getStmtIndent(startOffset, endOffset, EnumSet.of(JavaTokenId.TRY, JavaTokenId.RPAREN, JavaTokenId.FINALLY), t != null ? (int)this.sp.getEndPosition(this.cut, t) : lastPos, currentIndent);
                break;
            }
            case CATCH: {
                currentIndent = this.getStmtIndent(startOffset, endOffset, EnumSet.of(JavaTokenId.RPAREN), lastPos, currentIndent);
                break;
            }
            case WHILE_LOOP: {
                currentIndent = this.getStmtIndent(startOffset, endOffset, EnumSet.of(JavaTokenId.RPAREN), (int)this.sp.getEndPosition(this.cut, (Tree)((WhileLoopTree)last).getCondition()) - 1, currentIndent);
                break;
            }
            case BLOCK: {
                int i;
                TokenSequence<JavaTokenId> token;
                JavaTokenId nextTokenId;
                boolean isStatic = ((BlockTree)last).isStatic();
                if (isStatic && (token = this.findFirstNonWhitespaceToken(startOffset, lastPos)) != null && token.token().id() == JavaTokenId.STATIC && token.offset() == lastPos) {
                    switch (this.cs.getOtherBracePlacement()) {
                        case NEW_LINE_INDENTED: {
                            currentIndent += this.cs.getIndentSize();
                            break block0;
                        }
                        case NEW_LINE_HALF_INDENTED: {
                            currentIndent += this.cs.getIndentSize() / 2;
                        }
                    }
                    break;
                }
                token = this.findFirstNonWhitespaceToken(startOffset, endOffset);
                JavaTokenId javaTokenId = nextTokenId = token != null ? (JavaTokenId)token.token().id() : null;
                if (nextTokenId == null || nextTokenId != JavaTokenId.RBRACE) {
                    StatementTree t = null;
                    boolean isNextLabeledStatement = false;
                    for (StatementTree st : ((BlockTree)last).getStatements()) {
                        if (this.sp.getEndPosition(this.cut, (Tree)st) > (long)startOffset) {
                            isNextLabeledStatement = st.getKind() == Tree.Kind.LABELED_STATEMENT;
                            break;
                        }
                        t = st;
                    }
                    if (isNextLabeledStatement && this.cs.absoluteLabelIndent()) {
                        currentIndent = 0;
                    } else if (t != null) {
                        int i2 = this.getCurrentIndent((Tree)t, path);
                        currentIndent = i2 < 0 ? currentIndent + this.cs.getIndentSize() : i2;
                    } else if (isStatic) {
                        currentIndent += this.cs.getIndentSize();
                    } else if (this.isLeftBraceOnNewLine(lastPos, startOffset)) {
                        switch (path.get(1).getKind() == Tree.Kind.METHOD ? this.cs.getMethodDeclBracePlacement() : this.cs.getOtherBracePlacement()) {
                            case SAME_LINE: 
                            case NEW_LINE: {
                                currentIndent += this.cs.getIndentSize();
                                break;
                            }
                            case NEW_LINE_HALF_INDENTED: {
                                currentIndent += this.cs.getIndentSize() - this.cs.getIndentSize() / 2;
                            }
                        }
                    } else {
                        int i3 = this.getCurrentIndent(path.get(1), path);
                        currentIndent = (i3 < 0 ? currentIndent : i3) + this.cs.getIndentSize();
                    }
                    if (nextTokenId == null || nextTokenId != JavaTokenId.LBRACE) break;
                    switch (this.cs.getOtherBracePlacement()) {
                        case NEW_LINE_INDENTED: {
                            currentIndent += this.cs.getIndentSize();
                            break block0;
                        }
                        case NEW_LINE_HALF_INDENTED: {
                            currentIndent += this.cs.getIndentSize() / 2;
                        }
                    }
                    break;
                }
                if (isStatic) {
                    switch (this.cs.getOtherBracePlacement()) {
                        case NEW_LINE_INDENTED: {
                            currentIndent += this.cs.getIndentSize();
                            break block0;
                        }
                        case NEW_LINE_HALF_INDENTED: {
                            currentIndent += this.cs.getIndentSize() / 2;
                        }
                    }
                    break;
                }
                if (this.isLeftBraceOnNewLine(lastPos, startOffset) || (i = this.getCurrentIndent(path.get(1), path)) < 0) break;
                currentIndent = i;
                break;
            }
            case SWITCH: {
                CaseTree ct;
                JavaTokenId nextTokenId;
                TokenSequence<JavaTokenId> token = this.findFirstNonWhitespaceToken(startOffset, endOffset);
                JavaTokenId javaTokenId = nextTokenId = token != null ? (JavaTokenId)token.token().id() : null;
                if (nextTokenId != null && nextTokenId == JavaTokenId.RBRACE) {
                    if (!this.isLeftBraceOnNewLine(lastPos, startOffset)) break;
                    switch (this.cs.getOtherBracePlacement()) {
                        case NEW_LINE_INDENTED: {
                            currentIndent += this.cs.getIndentSize();
                            break block0;
                        }
                        case NEW_LINE_HALF_INDENTED: {
                            currentIndent += this.cs.getIndentSize() / 2;
                        }
                    }
                    break;
                }
                CaseTree t = null;
                Iterator i$ = ((SwitchTree)last).getCases().iterator();
                while (i$.hasNext() && this.sp.getEndPosition(this.cut, (Tree)(ct = (CaseTree)i$.next())) <= (long)startOffset) {
                    t = ct;
                }
                if (t != null) {
                    int i = this.getCurrentIndent((Tree)t, path);
                    int n = i < 0 ? currentIndent + (this.cs.indentCasesFromSwitch() ? this.cs.getIndentSize() : 0) : (currentIndent = i);
                    if (nextTokenId != null && EnumSet.of(JavaTokenId.CASE, JavaTokenId.DEFAULT).contains((Object)nextTokenId) || (token = this.findFirstNonWhitespaceToken(startOffset, lastPos)) != null && token.token().id() == JavaTokenId.RBRACE) break;
                    currentIndent += this.cs.getIndentSize();
                    break;
                }
                token = this.findFirstNonWhitespaceToken(startOffset, lastPos);
                if (token != null && token.token().id() == JavaTokenId.LBRACE) {
                    currentIndent += this.cs.indentCasesFromSwitch() ? this.cs.getIndentSize() : 0;
                    break;
                }
                currentIndent = this.getStmtIndent(startOffset, endOffset, EnumSet.of(JavaTokenId.RPAREN), (int)this.sp.getEndPosition(this.cut, (Tree)((SwitchTree)last).getExpression()) - 1, currentIndent);
                break;
            }
            case CASE: {
                StatementTree st;
                StatementTree t = null;
                Iterator i$ = ((CaseTree)last).getStatements().iterator();
                while (i$.hasNext() && this.sp.getEndPosition(this.cut, (Tree)(st = (StatementTree)i$.next())) <= (long)startOffset) {
                    t = st;
                }
                if (t != null) {
                    int i = this.getCurrentIndent((Tree)t, path);
                    currentIndent = i < 0 ? this.getStmtIndent(startOffset, endOffset, EnumSet.of(JavaTokenId.COLON), (int)this.sp.getEndPosition(this.cut, (Tree)((CaseTree)last).getExpression()), currentIndent) : i;
                    break;
                }
                currentIndent = this.getStmtIndent(startOffset, endOffset, EnumSet.of(JavaTokenId.COLON), (int)this.sp.getEndPosition(this.cut, (Tree)((CaseTree)last).getExpression()), currentIndent);
                break;
            }
            case NEW_ARRAY: {
                JavaTokenId prevTokenId;
                JavaTokenId nextTokenId;
                TokenSequence<JavaTokenId> token = this.findFirstNonWhitespaceToken(startOffset, endOffset);
                JavaTokenId javaTokenId = nextTokenId = token != null ? (JavaTokenId)token.token().id() : null;
                if (nextTokenId == JavaTokenId.RBRACE) break;
                token = this.findFirstNonWhitespaceToken(startOffset, lastPos);
                JavaTokenId javaTokenId3 = prevTokenId = token != null ? (JavaTokenId)token.token().id() : null;
                if (prevTokenId == null) break;
                switch (prevTokenId) {
                    case LBRACE: {
                        currentIndent += this.cs.getIndentSize();
                        break block0;
                    }
                    case COMMA: {
                        currentIndent = this.getMultilineIndent(((NewArrayTree)last).getInitializers(), path, token.offset(), currentIndent, this.cs.alignMultilineArrayInit(), false);
                        break block0;
                    }
                    case RBRACKET: {
                        if (nextTokenId != JavaTokenId.LBRACE) break;
                        switch (this.cs.getOtherBracePlacement()) {
                            case NEW_LINE_INDENTED: {
                                currentIndent += this.cs.getIndentSize();
                                break block0;
                            }
                            case NEW_LINE_HALF_INDENTED: {
                                currentIndent += this.cs.getIndentSize() / 2;
                            }
                        }
                        break block0;
                    }
                }
                currentIndent += this.cs.getContinuationIndentSize();
                break;
            }
            case LAMBDA_EXPRESSION: {
                JavaTokenId prevTokenId;
                TokenSequence<JavaTokenId> token = this.findFirstNonWhitespaceToken(startOffset, endOffset);
                JavaTokenId nextTokenId = token != null ? (JavaTokenId)token.token().id() : null;
                token = this.findFirstNonWhitespaceToken(startOffset, lastPos);
                JavaTokenId javaTokenId = prevTokenId = token != null ? (JavaTokenId)token.token().id() : null;
                if (prevTokenId == JavaTokenId.ARROW && nextTokenId == JavaTokenId.LBRACE) {
                    switch (this.cs.getOtherBracePlacement()) {
                        case NEW_LINE_INDENTED: {
                            currentIndent += this.cs.getIndentSize();
                            break block0;
                        }
                        case NEW_LINE_HALF_INDENTED: {
                            currentIndent += this.cs.getIndentSize() / 2;
                        }
                    }
                    break;
                }
                if (nextTokenId == JavaTokenId.RPAREN) break;
                currentIndent = this.getContinuationIndent(path, currentIndent);
                break;
            }
            case NEW_CLASS: {
                JavaTokenId prevTokenId;
                TokenSequence<JavaTokenId> token = this.findFirstNonWhitespaceToken(startOffset, endOffset);
                JavaTokenId nextTokenId = token != null ? (JavaTokenId)token.token().id() : null;
                token = this.findFirstNonWhitespaceToken(startOffset, lastPos);
                JavaTokenId javaTokenId = prevTokenId = token != null ? (JavaTokenId)token.token().id() : null;
                if (prevTokenId == JavaTokenId.RPAREN && nextTokenId == JavaTokenId.LBRACE) {
                    switch (this.cs.getClassDeclBracePlacement()) {
                        case NEW_LINE_INDENTED: {
                            currentIndent += this.cs.getIndentSize();
                            break block0;
                        }
                        case NEW_LINE_HALF_INDENTED: {
                            currentIndent += this.cs.getIndentSize() / 2;
                        }
                    }
                    break;
                }
                if (nextTokenId == JavaTokenId.RPAREN) break;
                currentIndent = this.getContinuationIndent(path, currentIndent);
                break;
            }
            case METHOD_INVOCATION: {
                TokenSequence<JavaTokenId> token = this.findFirstNonWhitespaceToken(startOffset, lastPos);
                if (token != null && token.token().id() == JavaTokenId.COMMA) {
                    currentIndent = this.getMultilineIndent(((MethodInvocationTree)last).getArguments(), path, token.offset(), currentIndent, this.cs.alignMultilineCallArgs(), true);
                    break;
                }
                token = this.findFirstNonWhitespaceToken(startOffset, endOffset);
                if (token != null && token.token().id() == JavaTokenId.RPAREN) break;
                currentIndent = this.getContinuationIndent(path, currentIndent);
                break;
            }
            case ANNOTATION: {
                TokenSequence<JavaTokenId> token = this.findFirstNonWhitespaceToken(startOffset, lastPos);
                if (token != null && token.token().id() == JavaTokenId.COMMA) {
                    currentIndent = this.getMultilineIndent(((AnnotationTree)last).getArguments(), path, token.offset(), currentIndent, this.cs.alignMultilineAnnotationArgs(), true);
                    break;
                }
                token = this.findFirstNonWhitespaceToken(startOffset, endOffset);
                if (token != null && token.token().id() == JavaTokenId.RPAREN) break;
                currentIndent = this.getContinuationIndent(path, currentIndent);
                break;
            }
            case LABELED_STATEMENT: {
                TokenSequence<JavaTokenId> token = this.findFirstNonWhitespaceToken(startOffset, lastPos);
                if (token != null && token.token().id() == JavaTokenId.COLON) break;
                currentIndent = this.getContinuationIndent(path, currentIndent);
                break;
            }
            default: {
                currentIndent = this.getContinuationIndent(path, currentIndent);
            }
        }
        return currentIndent;
    }

    private int getEmbeddedOffset(int offset) {
        return this.currentEmbedding != null ? this.currentEmbedding.getSnapshot().getEmbeddedOffset(offset) : offset;
    }

    private int getOriginalOffset(int offset) {
        return this.currentEmbedding != null ? this.currentEmbedding.getSnapshot().getOriginalOffset(offset) : offset;
    }

    private LinkedList<? extends Tree> getPath(int startOffset) {
        final LinkedList path = new LinkedList();
        if (this.ts.move(startOffset) == 0 && startOffset > 0 || !this.ts.moveNext()) {
            this.ts.movePrevious();
        }
        final int offset = this.ts.token().id() == JavaTokenId.IDENTIFIER || ((JavaTokenId)this.ts.token().id()).primaryCategory().startsWith("keyword") || ((JavaTokenId)this.ts.token().id()).primaryCategory().startsWith("string") || ((JavaTokenId)this.ts.token().id()).primaryCategory().equals("literal") ? this.ts.offset() : startOffset;
        new TreeScanner<Void, Void>(){

            public Void scan(Tree node, Void p) {
                if (node != null && Reindenter.this.sp.getStartPosition(Reindenter.this.cut, node) < (long)offset && Reindenter.this.sp.getEndPosition(Reindenter.this.cut, node) >= (long)offset) {
                    TreeScanner.super.scan(node, (Object)p);
                    if (node.getKind() != Tree.Kind.ERRONEOUS || !path.isEmpty()) {
                        path.add(node);
                    }
                }
                return null;
            }
        }.scan((Tree)this.cut, null);
        if (path.isEmpty() || path.getFirst() == this.cut || this.sp.getEndPosition(this.cut, (Tree)path.getFirst()) > (long)offset) {
            return path;
        }
        if (!path.isEmpty() && this.ts.move(offset) == 0 && this.ts.movePrevious()) {
            switch ((JavaTokenId)this.ts.token().id()) {
                case RPAREN: {
                    if (EnumSet.of(Tree.Kind.ENHANCED_FOR_LOOP, new Tree.Kind[]{Tree.Kind.FOR_LOOP, Tree.Kind.IF, Tree.Kind.WHILE_LOOP, Tree.Kind.DO_WHILE_LOOP, Tree.Kind.TYPE_CAST, Tree.Kind.SYNCHRONIZED}).contains((Object)((Tree)path.getFirst()).getKind())) break;
                    path.removeFirst();
                    break;
                }
                case GT: 
                case GTGT: 
                case GTGTGT: {
                    if (EnumSet.of(Tree.Kind.MEMBER_SELECT, Tree.Kind.CLASS, Tree.Kind.GREATER_THAN).contains((Object)((Tree)path.getFirst()).getKind())) break;
                }
                case SEMICOLON: {
                    if (((Tree)path.getFirst()).getKind() == Tree.Kind.FOR_LOOP && (long)this.ts.offset() <= this.sp.getStartPosition(null, (Tree)((ForLoopTree)path.getFirst()).getUpdate().get(0))) break;
                }
                case RBRACE: {
                    path.removeFirst();
                    switch (((Tree)path.getFirst()).getKind()) {
                        case CATCH: {
                            path.removeFirst();
                        }
                        case METHOD: 
                        case ENHANCED_FOR_LOOP: 
                        case FOR_LOOP: 
                        case IF: 
                        case SYNCHRONIZED: 
                        case TRY: 
                        case WHILE_LOOP: {
                            path.removeFirst();
                        }
                    }
                }
            }
        }
        return path;
    }

    private TokenSequence<JavaTokenId> findFirstNonWhitespaceToken(int startOffset, int endOffset) {
        boolean backward;
        if (startOffset == endOffset) {
            return null;
        }
        this.ts.move(startOffset);
        boolean bl = backward = startOffset > endOffset;
        block3 : while (backward ? this.ts.movePrevious() : this.ts.moveNext()) {
            if (backward && this.ts.offset() < endOffset || !backward && this.ts.offset() > endOffset) {
                return null;
            }
            switch ((JavaTokenId)this.ts.token().id()) {
                case WHITESPACE: 
                case LINE_COMMENT: 
                case BLOCK_COMMENT: 
                case JAVADOC_COMMENT: {
                    continue block3;
                }
            }
            return this.ts;
        }
        return null;
    }

    private TokenSequence<JavaTokenId> findFirstTokenOccurrence(int startOffset, int endOffset, JavaTokenId token) {
        boolean backward;
        if (startOffset == endOffset) {
            return null;
        }
        this.ts.move(startOffset);
        boolean bl = backward = startOffset > endOffset;
        while (backward ? this.ts.movePrevious() : this.ts.moveNext()) {
            if (backward && this.ts.offset() < endOffset || !backward && this.ts.offset() > endOffset) {
                return null;
            }
            if (this.ts.token().id() != token) continue;
            return this.ts;
        }
        return null;
    }

    private boolean isLeftBraceOnNewLine(int startOffset, int endOffset) {
        this.ts.move(startOffset);
        while (this.ts.moveNext()) {
            if (this.ts.offset() >= endOffset) {
                return false;
            }
            if (this.ts.token().id() != JavaTokenId.LBRACE) continue;
            if (!this.ts.movePrevious()) {
                return false;
            }
            return this.ts.token().id() == JavaTokenId.LINE_COMMENT || this.ts.token().id() == JavaTokenId.WHITESPACE && this.ts.token().text().toString().indexOf(10) >= 0;
        }
        return false;
    }

    private int getColumn(Tree tree) throws BadLocationException {
        int startOffset = this.getOriginalOffset((int)this.sp.getStartPosition(this.cut, tree));
        if (startOffset < 0) {
            return -1;
        }
        int lineStartOffset = this.context.lineStartOffset(startOffset);
        return this.getCol(this.context.document().getText(lineStartOffset, startOffset - lineStartOffset));
    }

    private int getCurrentIndent(Tree tree, List<? extends Tree> path) throws BadLocationException {
        int lineStartOffset;
        Integer newIndent;
        int currentIndent;
        int startOffset = -1;
        switch (tree.getKind()) {
            TokenSequence<JavaTokenId> token;
            case METHOD_INVOCATION: {
                MethodInvocationTree mit = (MethodInvocationTree)tree;
                startOffset = (int)this.sp.getEndPosition(this.cut, (Tree)mit.getMethodSelect());
                TokenSequence<JavaTokenId> tokenSequence = token = startOffset >= 0 ? this.findFirstTokenOccurrence(startOffset, (int)this.sp.getEndPosition(this.cut, tree), JavaTokenId.LPAREN) : null;
                if (token == null) break;
                startOffset = token.offset();
                break;
            }
            case NEW_CLASS: {
                NewClassTree nct = (NewClassTree)tree;
                startOffset = (int)this.sp.getEndPosition(this.cut, (Tree)nct.getIdentifier());
                TokenSequence<JavaTokenId> tokenSequence = token = startOffset >= 0 ? this.findFirstTokenOccurrence(startOffset, (int)this.sp.getEndPosition(this.cut, tree), JavaTokenId.LPAREN) : null;
                if (token == null) break;
                startOffset = token.offset();
                break;
            }
            case ANNOTATION: {
                AnnotationTree at = (AnnotationTree)tree;
                startOffset = (int)this.sp.getEndPosition(this.cut, at.getAnnotationType());
                TokenSequence<JavaTokenId> tokenSequence = token = startOffset >= 0 ? this.findFirstTokenOccurrence(startOffset, (int)this.sp.getEndPosition(this.cut, tree), JavaTokenId.LPAREN) : null;
                if (token == null) break;
                startOffset = token.offset();
                break;
            }
            case METHOD: {
                MethodTree mt = (MethodTree)tree;
                startOffset = (int)this.sp.getEndPosition(this.cut, mt.getReturnType());
                if (startOffset < 0) {
                    startOffset = (int)this.sp.getEndPosition(this.cut, (Tree)mt.getModifiers());
                }
                if (startOffset < 0) {
                    startOffset = (int)this.sp.getStartPosition(this.cut, tree);
                }
                TokenSequence<JavaTokenId> tokenSequence = startOffset >= 0 ? this.findFirstTokenOccurrence(startOffset, mt.getBody() != null ? (int)this.sp.getStartPosition(this.cut, (Tree)mt.getBody()) : (int)this.sp.getEndPosition(this.cut, tree), JavaTokenId.LPAREN) : (token = null);
                if (token == null) break;
                startOffset = token.offset();
            }
        }
        if (startOffset < 0) {
            startOffset = (int)this.sp.getStartPosition(this.cut, tree);
        }
        if ((startOffset = this.getOriginalOffset(startOffset)) < 0) {
            startOffset = this.currentEmbeddingStartOffset;
        }
        int n = currentIndent = (newIndent = this.newIndents.get(lineStartOffset = this.context.lineStartOffset(startOffset))) != null ? newIndent.intValue() : this.context.lineIndent(lineStartOffset);
        if (this.cs.absoluteLabelIndent()) {
            Iterator<? extends Tree> it = path.iterator();
            while (it.hasNext()) {
                StatementTree st;
                Tree parent;
                Tree t = it.next();
                if (t.getKind() != Tree.Kind.LABELED_STATEMENT || (int)this.sp.getStartPosition(this.cut, t) != lineStartOffset) continue;
                Tree tree2 = parent = it.hasNext() ? it.next() : null;
                if (parent == null || parent.getKind() != Tree.Kind.BLOCK) break;
                StatementTree stat = null;
                Iterator i$ = ((BlockTree)parent).getStatements().iterator();
                while (i$.hasNext() && this.sp.getEndPosition(this.cut, (Tree)(st = (StatementTree)i$.next())) <= (long)startOffset) {
                    stat = st;
                }
                if (stat != null) {
                    int i = this.getCurrentIndent((Tree)stat, path);
                    currentIndent = i < 0 ? currentIndent + this.cs.getIndentSize() : i;
                    break;
                }
                int i = this.getCurrentIndent(parent, path);
                currentIndent = (i < 0 ? currentIndent : i) + this.cs.getIndentSize();
                break;
            }
        }
        return currentIndent;
    }

    private int getContinuationIndent(LinkedList<? extends Tree> path, int currentIndent) throws BadLocationException {
        for (Tree tree : path) {
            switch (tree.getKind()) {
                case CLASS: 
                case INTERFACE: 
                case ENUM: 
                case ANNOTATION_TYPE: 
                case METHOD: 
                case VARIABLE: 
                case DO_WHILE_LOOP: 
                case ENHANCED_FOR_LOOP: 
                case FOR_LOOP: 
                case IF: 
                case SYNCHRONIZED: 
                case TRY: 
                case WHILE_LOOP: 
                case BLOCK: 
                case SWITCH: 
                case LABELED_STATEMENT: 
                case RETURN: 
                case THROW: 
                case EXPRESSION_STATEMENT: 
                case ASSERT: 
                case CONTINUE: 
                case BREAK: 
                case EMPTY_STATEMENT: {
                    int i = this.getCurrentIndent(tree, path);
                    return (i < 0 ? currentIndent : i) + this.cs.getContinuationIndentSize();
                }
                case LAMBDA_EXPRESSION: 
                case NEW_CLASS: 
                case METHOD_INVOCATION: 
                case ANNOTATION: {
                    return currentIndent + this.cs.getContinuationIndentSize();
                }
                case ASSIGNMENT: 
                case MULTIPLY_ASSIGNMENT: 
                case DIVIDE_ASSIGNMENT: 
                case REMAINDER_ASSIGNMENT: 
                case PLUS_ASSIGNMENT: 
                case MINUS_ASSIGNMENT: 
                case LEFT_SHIFT_ASSIGNMENT: 
                case RIGHT_SHIFT_ASSIGNMENT: 
                case UNSIGNED_RIGHT_SHIFT_ASSIGNMENT: 
                case AND_ASSIGNMENT: 
                case XOR_ASSIGNMENT: 
                case OR_ASSIGNMENT: {
                    if (!this.cs.alignMultilineAssignment()) break;
                    int c = this.getColumn(tree);
                    return c < 0 ? currentIndent : c;
                }
                case AND: 
                case CONDITIONAL_AND: 
                case CONDITIONAL_OR: 
                case DIVIDE: 
                case EQUAL_TO: 
                case GREATER_THAN: 
                case GREATER_THAN_EQUAL: 
                case LEFT_SHIFT: 
                case LESS_THAN: 
                case LESS_THAN_EQUAL: 
                case MINUS: 
                case MULTIPLY: 
                case NOT_EQUAL_TO: 
                case OR: 
                case PLUS: 
                case REMAINDER: 
                case RIGHT_SHIFT: 
                case UNSIGNED_RIGHT_SHIFT: 
                case XOR: {
                    if (!this.cs.alignMultilineBinaryOp()) break;
                    int c = this.getColumn(tree);
                    return c < 0 ? currentIndent : c;
                }
                case CONDITIONAL_EXPRESSION: {
                    if (!this.cs.alignMultilineTernaryOp()) break;
                    int c = this.getColumn(tree);
                    return c < 0 ? currentIndent : c;
                }
            }
        }
        return currentIndent + this.cs.getContinuationIndentSize();
    }

    private int getStmtIndent(int startOffset, int endOffset, Set<JavaTokenId> expectedTokenIds, int expectedTokenOffset, int currentIndent) {
        TokenSequence<JavaTokenId> token = this.findFirstNonWhitespaceToken(startOffset, expectedTokenOffset);
        if (token != null && expectedTokenIds.contains((Object)token.token().id())) {
            token = this.findFirstNonWhitespaceToken(startOffset, endOffset);
            if (token != null && token.token().id() == JavaTokenId.LBRACE) {
                switch (this.cs.getOtherBracePlacement()) {
                    case NEW_LINE_INDENTED: {
                        currentIndent += this.cs.getIndentSize();
                        break;
                    }
                    case NEW_LINE_HALF_INDENTED: {
                        currentIndent += this.cs.getIndentSize() / 2;
                    }
                }
            } else {
                currentIndent += this.cs.getIndentSize();
            }
        } else {
            currentIndent += this.cs.getContinuationIndentSize();
        }
        return currentIndent;
    }

    private int getMultilineIndent(List<? extends Tree> trees, LinkedList<? extends Tree> path, int commaOffset, int currentIndent, boolean align, boolean addContinuationIndent) throws BadLocationException {
        Tree tree = null;
        Tree first = null;
        for (Tree t : trees) {
            if (first == null) {
                first = t;
            }
            if (this.sp.getEndPosition(this.cut, t) > (long)commaOffset) break;
            tree = t;
        }
        if (tree != null && this.findFirstNonWhitespaceToken(commaOffset, (int)this.sp.getEndPosition(this.cut, tree)) == null) {
            int startOffset;
            int firstStartOffset = this.getOriginalOffset((int)this.sp.getStartPosition(this.cut, first));
            int n = startOffset = first == tree ? firstStartOffset : this.getOriginalOffset((int)this.sp.getStartPosition(this.cut, tree));
            if (firstStartOffset < 0 || startOffset < 0) {
                currentIndent = addContinuationIndent ? this.getContinuationIndent(path, currentIndent) : currentIndent + this.cs.getIndentSize();
            } else {
                Integer newIndent;
                int lineStartOffset;
                int firstLineStartOffset = this.context.lineStartOffset(firstStartOffset);
                int n2 = lineStartOffset = firstStartOffset == startOffset ? firstLineStartOffset : this.context.lineStartOffset(startOffset);
                currentIndent = firstLineStartOffset != lineStartOffset ? ((newIndent = this.newIndents.get(lineStartOffset)) != null ? newIndent.intValue() : this.context.lineIndent(lineStartOffset)) : (align ? this.getCol(this.context.document().getText(lineStartOffset, startOffset - lineStartOffset)) : (addContinuationIndent ? this.getContinuationIndent(path, currentIndent) : currentIndent + this.cs.getIndentSize()));
            }
        } else {
            currentIndent = addContinuationIndent ? this.getContinuationIndent(path, currentIndent) : currentIndent + this.cs.getIndentSize();
        }
        return currentIndent;
    }

    private int getCol(String text) {
        int col = 0;
        for (int i = 0; i < text.length(); ++i) {
            char c = text.charAt(i);
            if (c == '\t') {
                col += this.cs.getTabSize();
                col -= col % this.cs.getTabSize();
                continue;
            }
            ++col;
        }
        return col;
    }

    public static class Factory
    implements IndentTask.Factory {
        public IndentTask createTask(org.netbeans.modules.editor.indent.spi.Context context) {
            return new Reindenter(context);
        }
    }

}

