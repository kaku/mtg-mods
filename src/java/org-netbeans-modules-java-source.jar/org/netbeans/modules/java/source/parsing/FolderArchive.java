/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.api.queries.FileEncodingQuery
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.source.parsing;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.tools.JavaFileObject;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.queries.FileEncodingQuery;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.parsing.Archive;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.PrefetchableJavaFileObject;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;

public class FolderArchive
implements Archive {
    private static final Logger LOG = Logger.getLogger(FolderArchive.class.getName());
    private static final boolean normalize = Boolean.getBoolean("FolderArchive.normalize");
    final File root;
    final Charset encoding;
    private boolean sourceRootInitialized;
    private URL sourceRoot;

    public FolderArchive(File root) {
        FileObject file;
        assert (root != null);
        this.root = root;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "creating FolderArchive for {0}", root.getAbsolutePath());
        }
        this.encoding = (file = FileUtil.toFileObject((File)root)) != null ? FileEncodingQuery.getEncoding((FileObject)file) : null;
    }

    @Override
    public Iterable<JavaFileObject> getFiles(String folderName, ClassPath.Entry entry, Set<JavaFileObject.Kind> kinds, JavaFileFilterImplementation filter) throws IOException {
        assert (folderName != null);
        if (folderName.length() > 0) {
            folderName = folderName + '/';
        }
        if (entry == null || entry.includes(folderName)) {
            File[] content;
            File folder = new File(this.root, folderName.replace('/', File.separatorChar));
            if (normalize) {
                folder = FileUtil.normalizeFile((File)folder);
            }
            if ((content = folder.listFiles()) != null) {
                ArrayList<PrefetchableJavaFileObject> result = new ArrayList<PrefetchableJavaFileObject>(content.length);
                for (File f : content) {
                    if (kinds != null && !kinds.contains((Object)FileObjects.getKind(FileObjects.getExtension(f.getName()))) || !f.isFile() || entry != null && !entry.includes(Utilities.toURI((File)f).toURL())) continue;
                    result.add(FileObjects.fileFileObject(f, this.root, filter, this.encoding));
                }
                return Collections.unmodifiableList(result);
            }
        }
        return Collections.emptyList();
    }

    @Override
    public JavaFileObject create(String relativePath, JavaFileFilterImplementation filter) throws UnsupportedOperationException {
        if (File.separatorChar != '/') {
            relativePath = relativePath.replace('/', File.separatorChar);
        }
        File file = new File(this.root, relativePath);
        return FileObjects.fileFileObject(file, this.root, filter, this.encoding);
    }

    @Override
    public void clear() {
    }

    @Override
    public JavaFileObject getFile(@NonNull String name) {
        String path = name.replace('/', File.separatorChar);
        File file = new File(this.root, path);
        if (file.exists()) {
            return FileObjects.fileFileObject(file, this.root, null, this.encoding);
        }
        try {
            URL srcRoot = this.getBaseSourceRoot(Utilities.toURI((File)this.root).toURL());
            if (srcRoot != null && JavaIndex.hasSourceCache(srcRoot, false)) {
                if ("file".equals(srcRoot.getProtocol())) {
                    File folder = Utilities.toFile((URI)srcRoot.toURI());
                    file = new File(folder, path);
                    if (file.exists()) {
                        return FileObjects.fileFileObject(file, folder, null, this.encoding);
                    }
                } else {
                    FileObject resource;
                    FileObject srcRootFo = URLMapper.findFileObject((URL)srcRoot);
                    if (srcRootFo != null && (resource = srcRootFo.getFileObject(name)) != null) {
                        return FileObjects.sourceFileObject(resource, srcRootFo);
                    }
                }
            } else {
                LOG.log(Level.FINE, "No source in: {0}.", srcRoot);
            }
        }
        catch (IOException e) {
            Exceptions.printStackTrace((Throwable)e);
        }
        catch (URISyntaxException e) {
            Exceptions.printStackTrace((Throwable)e);
        }
        return null;
    }

    public String toString() {
        return String.format("%s[folder: %s]", this.getClass().getSimpleName(), this.root.getAbsolutePath());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private URL getBaseSourceRoot(URL binRoot) {
        FolderArchive folderArchive = this;
        synchronized (folderArchive) {
            if (this.sourceRootInitialized) {
                return this.sourceRoot;
            }
        }
        URL tmpSourceRoot = JavaIndex.getSourceRootForClassFolder(binRoot);
        FolderArchive folderArchive2 = this;
        synchronized (folderArchive2) {
            this.sourceRoot = tmpSourceRoot;
            this.sourceRootInitialized = true;
            return this.sourceRoot;
        }
    }
}

