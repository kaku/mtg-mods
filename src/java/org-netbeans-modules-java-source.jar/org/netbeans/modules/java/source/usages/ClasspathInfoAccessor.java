/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.java.source.usages;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.tools.JavaFileManager;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.source.parsing.FileManagerTransaction;
import org.netbeans.modules.java.source.parsing.InferableJavaFileObject;
import org.openide.filesystems.FileObject;

public abstract class ClasspathInfoAccessor {
    private static Logger log = Logger.getLogger(ClasspathInfoAccessor.class.getName());
    private static volatile ClasspathInfoAccessor INSTANCE;

    public static synchronized ClasspathInfoAccessor getINSTANCE() {
        block3 : {
            if (INSTANCE == null) {
                try {
                    Class.forName(ClasspathInfo.class.getName(), true, ClasspathInfo.class.getClassLoader());
                }
                catch (ClassNotFoundException cnfe) {
                    if (!log.isLoggable(Level.SEVERE)) break block3;
                    log.log(Level.SEVERE, cnfe.getMessage(), cnfe);
                }
            }
        }
        return INSTANCE;
    }

    public static void setINSTANCE(ClasspathInfoAccessor aINSTANCE) {
        INSTANCE = aINSTANCE;
    }

    @NonNull
    public abstract JavaFileManager createFileManager(@NonNull ClasspathInfo var1);

    @NonNull
    public abstract FileManagerTransaction getFileManagerTransaction(@NonNull ClasspathInfo var1);

    public abstract ClassPath getCachedClassPath(ClasspathInfo var1, ClasspathInfo.PathKind var2);

    public abstract ClasspathInfo create(ClassPath var1, ClassPath var2, ClassPath var3, JavaFileFilterImplementation var4, boolean var5, boolean var6, boolean var7, boolean var8);

    public abstract ClasspathInfo create(FileObject var1, JavaFileFilterImplementation var2, boolean var3, boolean var4, boolean var5, boolean var6);

    public abstract boolean registerVirtualSource(ClasspathInfo var1, InferableJavaFileObject var2) throws UnsupportedOperationException;

    public abstract boolean unregisterVirtualSource(ClasspathInfo var1, String var2) throws UnsupportedOperationException;
}

