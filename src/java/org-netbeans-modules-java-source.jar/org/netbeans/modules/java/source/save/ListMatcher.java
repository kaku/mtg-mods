/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.lexer.JavaTokenId
 */
package org.netbeans.modules.java.source.save;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Stack;
import org.netbeans.api.java.lexer.JavaTokenId;

public final class ListMatcher<E> {
    private final E[] oldL;
    private final E[] newL;
    private final Stack<ResultItem<E>> result;
    private Comparator<E> measure;

    private ListMatcher(List<? extends E> oldL, List<? extends E> newL, Comparator<E> measure) {
        this(oldL.toArray(), newL.toArray(), measure);
    }

    private ListMatcher(List<? extends E> oldL, List<? extends E> newL) {
        this(oldL.toArray(), newL.toArray());
    }

    private ListMatcher(E[] oldL, E[] newL) {
        this(oldL, newL, null);
    }

    private ListMatcher(E[] oldL, E[] newL, Comparator<E> measure) {
        this.measure = new Comparator<E>(){

            @Override
            public int compare(E first, E second) {
                assert (first != null && second != null);
                if (first == second || first.equals(second)) {
                    return 0;
                }
                return 1000;
            }
        };
        this.oldL = oldL;
        this.newL = newL;
        if (measure != null) {
            this.measure = measure;
        }
        this.result = new Stack();
    }

    public static <T> ListMatcher<T> instance(List<? extends T> oldL, List<? extends T> newL) {
        return new ListMatcher<T>(oldL, newL);
    }

    public static <T> ListMatcher<T> instance(List<? extends T> oldL, List<? extends T> newL, Comparator<T> measure) {
        return new ListMatcher<T>(oldL, newL, measure);
    }

    public static <T> ListMatcher<T> instance(T[] oldL, T[] newL) {
        return new ListMatcher<T>(oldL, newL);
    }

    public boolean match() {
        int ii;
        int jj;
        boolean NEITHER = false;
        boolean UP = true;
        int LEFT = 2;
        int UP_AND_LEFT = 3;
        int UP_AND_LEFT_MOD = 4;
        int n = this.oldL.length;
        int m = this.newL.length;
        int[][] S = new int[n + 1][m + 1];
        int[][] R2 = new int[n + 1][m + 1];
        for (ii = 0; ii <= n; ++ii) {
            S[ii][0] = 0;
            R2[ii][0] = 1;
        }
        for (jj = 0; jj <= m; ++jj) {
            S[0][jj] = 0;
            R2[0][jj] = 2;
        }
        for (ii = 1; ii <= n; ++ii) {
            for (jj = 1; jj <= m; ++jj) {
                if (this.oldL[ii - 1].equals(this.newL[jj - 1])) {
                    S[ii][jj] = S[ii - 1][jj - 1] + 1000;
                    R2[ii][jj] = 3;
                } else {
                    int distance = this.measure.compare(this.oldL[ii - 1], this.newL[jj - 1]);
                    if (distance <= 0) {
                        S[ii][jj] = S[ii - 1][jj - 1] + 1000;
                        R2[ii][jj] = 3;
                    } else if (distance >= 1000) {
                        S[ii][jj] = -1;
                        R2[ii][jj] = 0;
                    } else {
                        S[ii][jj] = S[ii - 1][jj - 1] + (1000 - distance);
                        R2[ii][jj] = 4;
                    }
                }
                if (S[ii - 1][jj] >= S[ii][jj]) {
                    S[ii][jj] = S[ii - 1][jj];
                    R2[ii][jj] = 1;
                }
                if (S[ii][jj - 1] < S[ii][jj]) continue;
                S[ii][jj] = S[ii][jj - 1];
                R2[ii][jj] = 2;
            }
        }
        ii = n;
        jj = m;
        if (!this.result.empty()) {
            this.result.clear();
        }
        while (ii > 0 || jj > 0) {
            E element;
            if (R2[ii][jj] == 3) {
                --jj;
                element = this.oldL[--ii];
                this.result.push(new ResultItem<E>(element, Operation.NOCHANGE));
                continue;
            }
            if (R2[ii][jj] == 4) {
                --ii;
                element = this.newL[--jj];
                this.result.push(new ResultItem<E>(element, Operation.MODIFY));
                continue;
            }
            if (R2[ii][jj] == 1) {
                element = this.oldL[--ii];
                this.result.push(new ResultItem<E>(element, Operation.DELETE));
                continue;
            }
            if (R2[ii][jj] == 2) {
                element = this.newL[--jj];
                this.result.push(new ResultItem<E>(element, Operation.INSERT));
                continue;
            }
            throw new IllegalStateException("ii=" + ii + "; jj=" + jj + "; R=" + Arrays.deepToString((Object[])R2));
        }
        return !this.result.empty();
    }

    public ResultItem<E>[] getResult() {
        int size = this.result.size();
        ResultItem[] temp = new ResultItem[size];
        for (ResultItem<E> item : this.result) {
            temp[--size] = item;
        }
        return temp;
    }

    public ResultItem<E>[] getTransformedResult() {
        Stack copy = (Stack)this.result.clone();
        ArrayList<ResultItem> temp = new ArrayList<ResultItem>(copy.size());
        while (!copy.empty()) {
            ResultItem item = (ResultItem)copy.pop();
            if (item.operation == Operation.DELETE && !copy.empty() && ((ResultItem)copy.peek()).operation == Operation.INSERT) {
                ResultItem nextItem = (ResultItem)copy.pop();
                temp.add(new ResultItem(nextItem.element, Operation.MODIFY));
                continue;
            }
            temp.add(item);
        }
        return temp.toArray(new ResultItem[0]);
    }

    public String printResult(boolean transformed) {
        StringBuffer sb = new StringBuffer(128);
        ResultItem<E>[] temp = transformed ? this.getTransformedResult() : this.getResult();
        for (int i = 0; i < temp.length; ++i) {
            sb.append(temp[i]).append('\n');
        }
        return sb.toString();
    }

    public Separator separatorInstance() {
        return new Separator<E>(this.getTransformedResult(), JavaTokenId.COMMA);
    }

    public static final class Separator<E> {
        static final SItem EMPTY = new SItem(null, 0);
        private final ResultItem<E>[] match;
        private E lastInList;
        private E firstInList;
        private final JavaTokenId separator;
        private SItem[] result;
        private boolean allNew;
        private boolean allOld;

        public Separator(ResultItem<E>[] match, JavaTokenId separator) {
            int i;
            this.match = match;
            this.separator = separator;
            this.lastInList = null;
            for (i = match.length - 1; i > 0; --i) {
                if (match[i].operation == Operation.DELETE) continue;
                this.lastInList = match[i].element;
                break;
            }
            for (i = 0; i < match.length; ++i) {
                if (match[i].operation == Operation.DELETE) continue;
                this.firstInList = match[i].element;
                break;
            }
            this.allOld = true;
            this.allNew = true;
            for (i = 0; i < match.length; ++i) {
                if (match[i].operation == Operation.MODIFY || match[i].operation == Operation.NOCHANGE) {
                    this.allOld = false;
                    this.allNew = false;
                    continue;
                }
                if (match[i].operation == Operation.INSERT) {
                    this.allOld = false;
                    continue;
                }
                if (match[i].operation != Operation.DELETE) continue;
                this.allNew = false;
            }
        }

        private static SItem create(ResultItem item) {
            return Separator.create(item, 0);
        }

        private static SItem create(ResultItem item, int type) {
            return new SItem(item, type);
        }

        public void compute() {
            this.result = new SItem[this.match.length];
            for (int i = this.match.length - 1; i >= 0; --i) {
                int mask;
                if (this.match[i].operation == Operation.DELETE) {
                    if (i == this.match.length - 1) {
                        if (i > 0 && this.match[i - 1].operation == Operation.DELETE) {
                            this.result[i] = Separator.create(this.match[i], this.allOld ? 8 : 0);
                            while (i > 0 && this.match[i - 1].operation == Operation.DELETE) {
                                if (i > 1 && this.match[i - 2].operation == Operation.DELETE) {
                                    this.result[--i] = Separator.create(this.match[i], 2);
                                    continue;
                                }
                                mask = i - 1 == 0 ? 4 : 1;
                                this.result[--i] = Separator.create(this.match[i], mask | 2);
                            }
                            continue;
                        }
                        mask = i > 0 ? 1 : 4;
                        this.result[i] = Separator.create(this.match[i], mask |= this.allOld ? 8 : 0);
                        continue;
                    }
                    this.result[i] = Separator.create(this.match[i], 2);
                    continue;
                }
                if (this.match[i].operation == Operation.INSERT) {
                    if (i == this.match.length - 1) {
                        if (i > 0 && this.match[i - 1].operation == Operation.INSERT) {
                            this.result[i] = Separator.create(this.match[i], this.allNew ? 8 : 0);
                            while (i > 0 && this.match[i - 1].operation == Operation.INSERT) {
                                if (i > 1 && this.match[i - 2].operation == Operation.INSERT) {
                                    this.result[--i] = Separator.create(this.match[i], 2);
                                    continue;
                                }
                                mask = i - 1 == 0 ? 4 : 1;
                                this.result[--i] = Separator.create(this.match[i], mask | 2);
                            }
                            continue;
                        }
                        mask = i > 0 ? 1 : 4;
                        this.result[i] = Separator.create(this.match[i], mask |= this.allNew ? 8 : 0);
                        continue;
                    }
                    this.result[i] = Separator.create(this.match[i], 2);
                    continue;
                }
                this.result[i] = EMPTY;
            }
        }

        public boolean head(int index) {
            return this.result[index].head();
        }

        public boolean prev(int index) {
            return this.result[index].prev();
        }

        public boolean next(int index) {
            return this.result[index].next();
        }

        public boolean tail(int index) {
            return this.result[index].tail();
        }

        public String print() {
            if (this.result != null) {
                StringBuffer sb = new StringBuffer(128);
                for (SItem item : this.result) {
                    if (item == EMPTY) continue;
                    sb.append(item).append('\n');
                }
                return sb.toString();
            }
            return "Result was not computed!";
        }

        private static final class SItem {
            private static final int NONE = 0;
            private static final int PREV = 1;
            private static final int NEXT = 2;
            private static final int HEAD = 4;
            private static final int TAIL = 8;
            private final int type;
            private final ResultItem item;

            private SItem(ResultItem item, int type) {
                this.item = item;
                this.type = type;
            }

            private boolean prev() {
                return (this.type & 1) != 0;
            }

            private boolean next() {
                return (this.type & 2) != 0;
            }

            private boolean head() {
                return (this.type & 4) != 0;
            }

            private boolean tail() {
                return (this.type & 8) != 0;
            }

            public String toString() {
                StringBuffer sb = new StringBuffer();
                if (this.head()) {
                    sb.append("head ");
                }
                if (this.prev()) {
                    sb.append("previous ");
                }
                sb.append(this.item.toString()).append(' ');
                if (this.next()) {
                    sb.append("next ");
                }
                if (this.tail()) {
                    sb.append("tail ");
                }
                return sb.toString();
            }
        }

    }

    public static final class ResultItem<S> {
        public final S element;
        public final Operation operation;

        public ResultItem(S element, Operation operation) {
            this.element = element;
            this.operation = operation;
        }

        public String toString() {
            StringBuffer sb = new StringBuffer(128);
            sb.append('{');
            sb.append((Object)this.operation);
            sb.append("} ");
            sb.append(this.element);
            return sb.toString();
        }
    }

    public static enum Operation {
        INSERT("insert"),
        MODIFY("modify"),
        DELETE("delete"),
        NOCHANGE("nochange");
        
        private final String name;

        private Operation(String name) {
            this.name = name;
        }

        public String toString() {
            return this.name;
        }
    }

}

