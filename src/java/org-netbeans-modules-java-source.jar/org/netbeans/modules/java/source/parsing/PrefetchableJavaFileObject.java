/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.source.parsing;

import java.io.IOException;
import org.netbeans.modules.java.source.parsing.InferableJavaFileObject;

public interface PrefetchableJavaFileObject
extends InferableJavaFileObject {
    public int prefetch() throws IOException;

    public int dispose();
}

