/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.java.source.parsing;

import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.modules.java.source.JavaSourceAccessor;
import org.netbeans.modules.java.source.parsing.CompilationInfoImpl;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.util.Parameters;

public class JavacParserResult
extends Parser.Result {
    private final CompilationInfo info;

    public JavacParserResult(CompilationInfo info) {
        super(JavaSourceAccessor.getINSTANCE().getCompilationInfoImpl(info).getSnapshot());
        Parameters.notNull((CharSequence)"info", (Object)info);
        this.info = info;
    }

    private boolean supports(Class<? extends CompilationInfo> clazz) {
        assert (clazz != null);
        return clazz.isInstance(this.info);
    }

    public <T extends CompilationInfo> T get(Class<T> clazz) {
        if (this.supports(clazz)) {
            return (T)((CompilationInfo)clazz.cast(this.info));
        }
        return null;
    }

    public void invalidate() {
        JavaSourceAccessor.getINSTANCE().invalidate(this.info);
    }
}

