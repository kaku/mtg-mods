/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.LineMap
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.util.DocSourcePositions
 *  com.sun.source.util.JavacTask
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  com.sun.tools.javac.api.ClassNamesForFileOraculum
 *  com.sun.tools.javac.api.DuplicateClassChecker
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.api.JavacTool
 *  com.sun.tools.javac.api.JavacTrees
 *  com.sun.tools.javac.code.Source
 *  com.sun.tools.javac.comp.Env
 *  com.sun.tools.javac.jvm.Target
 *  com.sun.tools.javac.parser.LazyDocCommentTable
 *  com.sun.tools.javac.tree.DocCommentTable
 *  com.sun.tools.javac.tree.EndPosTable
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCBlock
 *  com.sun.tools.javac.tree.JCTree$JCCompilationUnit
 *  com.sun.tools.javac.tree.JCTree$JCMethodDecl
 *  com.sun.tools.javac.util.Abort
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Key
 *  com.sun.tools.javac.util.CouplingAbort
 *  com.sun.tools.javac.util.Log
 *  com.sun.tools.javac.util.Options
 *  com.sun.tools.javac.util.Position
 *  com.sun.tools.javac.util.Position$LineMapImpl
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.queries.SourceLevelQuery
 *  org.netbeans.api.java.queries.SourceLevelQuery$Profile
 *  org.netbeans.api.java.queries.SourceLevelQuery$Result
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenChange
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenHierarchyEvent
 *  org.netbeans.api.lexer.TokenHierarchyEventType
 *  org.netbeans.api.lexer.TokenHierarchyListener
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.lib.editor.util.swing.PositionRegion
 *  org.netbeans.lib.nbjavac.services.CancelAbort
 *  org.netbeans.lib.nbjavac.services.CancelService
 *  org.netbeans.lib.nbjavac.services.NBAttr
 *  org.netbeans.lib.nbjavac.services.NBClassReader
 *  org.netbeans.lib.nbjavac.services.NBClassWriter
 *  org.netbeans.lib.nbjavac.services.NBEnter
 *  org.netbeans.lib.nbjavac.services.NBJavacTrees
 *  org.netbeans.lib.nbjavac.services.NBJavadocEnter
 *  org.netbeans.lib.nbjavac.services.NBJavadocMemberEnter
 *  org.netbeans.lib.nbjavac.services.NBMemberEnter
 *  org.netbeans.lib.nbjavac.services.NBMessager
 *  org.netbeans.lib.nbjavac.services.NBParserFactory
 *  org.netbeans.lib.nbjavac.services.NBResolve
 *  org.netbeans.lib.nbjavac.services.NBTreeMaker
 *  org.netbeans.lib.nbjavac.services.PartialReparser
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.Task
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.impl.Utilities
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$CancelReason
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.ParserResultTask
 *  org.netbeans.modules.parsing.spi.SourceModificationEvent
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.EditorCookie$Observable
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.modules.SpecificationVersion
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Exceptions
 *  org.openide.util.Pair
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.java.source.parsing;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.LineMap;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.DocSourcePositions;
import com.sun.source.util.JavacTask;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import com.sun.tools.javac.api.ClassNamesForFileOraculum;
import com.sun.tools.javac.api.DuplicateClassChecker;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.api.JavacTool;
import com.sun.tools.javac.api.JavacTrees;
import com.sun.tools.javac.comp.Env;
import com.sun.tools.javac.jvm.Target;
import com.sun.tools.javac.parser.LazyDocCommentTable;
import com.sun.tools.javac.tree.DocCommentTable;
import com.sun.tools.javac.tree.EndPosTable;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.Abort;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.CouplingAbort;
import com.sun.tools.javac.util.Log;
import com.sun.tools.javac.util.Options;
import com.sun.tools.javac.util.Position;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.processing.Processor;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyledDocument;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticListener;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.queries.SourceLevelQuery;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.JavaParserResultTask;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenChange;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenHierarchyEvent;
import org.netbeans.api.lexer.TokenHierarchyEventType;
import org.netbeans.api.lexer.TokenHierarchyListener;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.lib.editor.util.swing.PositionRegion;
import org.netbeans.lib.nbjavac.services.CancelAbort;
import org.netbeans.lib.nbjavac.services.CancelService;
import org.netbeans.lib.nbjavac.services.NBAttr;
import org.netbeans.lib.nbjavac.services.NBClassReader;
import org.netbeans.lib.nbjavac.services.NBClassWriter;
import org.netbeans.lib.nbjavac.services.NBEnter;
import org.netbeans.lib.nbjavac.services.NBJavacTrees;
import org.netbeans.lib.nbjavac.services.NBJavadocEnter;
import org.netbeans.lib.nbjavac.services.NBJavadocMemberEnter;
import org.netbeans.lib.nbjavac.services.NBMemberEnter;
import org.netbeans.lib.nbjavac.services.NBMessager;
import org.netbeans.lib.nbjavac.services.NBParserFactory;
import org.netbeans.lib.nbjavac.services.NBResolve;
import org.netbeans.lib.nbjavac.services.NBTreeMaker;
import org.netbeans.lib.nbjavac.services.PartialReparser;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.source.JavaFileFilterQuery;
import org.netbeans.modules.java.source.JavaSourceAccessor;
import org.netbeans.modules.java.source.JavadocEnv;
import org.netbeans.modules.java.source.PostFlowAnalysis;
import org.netbeans.modules.java.source.TreeLoader;
import org.netbeans.modules.java.source.indexing.APTUtils;
import org.netbeans.modules.java.source.indexing.FQN2Files;
import org.netbeans.modules.java.source.parsing.AbstractSourceFileObject;
import org.netbeans.modules.java.source.parsing.ClasspathInfoListener;
import org.netbeans.modules.java.source.parsing.ClasspathInfoProvider;
import org.netbeans.modules.java.source.parsing.CompilationInfoImpl;
import org.netbeans.modules.java.source.parsing.DocPositionRegion;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.FindAnonymousVisitor;
import org.netbeans.modules.java.source.parsing.FindMethodRegionsVisitor;
import org.netbeans.modules.java.source.parsing.JavacFlowListener;
import org.netbeans.modules.java.source.parsing.JavacParserResult;
import org.netbeans.modules.java.source.parsing.MimeTask;
import org.netbeans.modules.java.source.parsing.NewComilerTask;
import org.netbeans.modules.java.source.parsing.NullWriter;
import org.netbeans.modules.java.source.parsing.TranslatePositionsVisitor;
import org.netbeans.modules.java.source.tasklist.CompilerSettings;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;
import org.netbeans.modules.java.source.usages.ClasspathInfoAccessor;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.Task;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.impl.Utilities;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserResultTask;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.modules.SpecificationVersion;
import org.openide.nodes.Node;
import org.openide.util.ChangeSupport;
import org.openide.util.Exceptions;
import org.openide.util.Pair;
import org.openide.util.WeakListeners;

public class JavacParser
extends Parser {
    private static final Logger TIME_LOGGER = Logger.getLogger("TIMER");
    private static final Logger LOGGER = Logger.getLogger(JavacParser.class.getName());
    public static final String MIME_TYPE = "text/x-java";
    private static final PrintWriter DEV_NULL = new PrintWriter(new NullWriter(), false);
    private static final int MAX_DUMPS = Integer.getInteger("org.netbeans.modules.java.source.parsing.JavacParser.maxDumps", 255);
    private static final boolean DISABLE_PARTIAL_REPARSE = Boolean.getBoolean("org.netbeans.modules.java.source.parsing.JavacParser.no_reparse");
    private static final String LOMBOK_DETECTED = "lombokDetected";
    private static final Map<JavaSource.Phase, String> phase2Message = new EnumMap<JavaSource.Phase, String>(JavaSource.Phase.class);
    private final ChangeSupport listeners;
    private final AtomicBoolean parserCanceled;
    private final AtomicBoolean indexCanceled;
    private final boolean privateParser;
    private FileObject file;
    private FileObject root;
    private ClasspathInfo cpInfo;
    private final int sourceCount;
    private final boolean supportsReparse;
    private final List<Pair<DocPositionRegion, MethodTree>> positions;
    private final AtomicReference<Pair<DocPositionRegion, MethodTree>> changedMethod;
    private final DocListener listener;
    private final FilterListener filterListener;
    private final ChangeListener cpInfoListener;
    private CompilationInfoImpl ciImpl;
    private boolean initialized;
    private boolean invalid;
    private Snapshot cachedSnapShot;
    private long parseId;
    private ChangeListener weakCpListener;
    private Reference<JavaSource> currentSource;
    public static boolean DISABLE_SOURCE_LEVEL_DOWNGRADE;

    JavacParser(Collection<Snapshot> snapshots, boolean privateParser) {
        Source source;
        FileObject fo;
        this.listeners = new ChangeSupport((Object)this);
        this.parserCanceled = new AtomicBoolean();
        this.indexCanceled = new AtomicBoolean();
        this.positions = Collections.synchronizedList(new LinkedList());
        this.changedMethod = new AtomicReference();
        this.privateParser = privateParser;
        this.sourceCount = snapshots.size();
        boolean singleJavaFile = this.sourceCount == 1 && "text/x-java".equals(snapshots.iterator().next().getSource().getMimeType());
        this.supportsReparse = singleJavaFile && !DISABLE_PARTIAL_REPARSE;
        EditorCookie.Observable ec = null;
        JavaFileFilterImplementation filter = null;
        if (singleJavaFile && (fo = (source = snapshots.iterator().next().getSource()).getFileObject()) != null) {
            filter = JavaFileFilterQuery.getFilter(fo);
            try {
                DataObject dobj = DataObject.find((FileObject)fo);
                ec = (EditorCookie.Observable)dobj.getCookie(EditorCookie.Observable.class);
                if (ec == null) {
                    LOGGER.log(Level.FINE, String.format("File: %s has no EditorCookie.Observable", FileUtil.getFileDisplayName((FileObject)fo)));
                }
            }
            catch (DataObjectNotFoundException e) {
                LOGGER.log(Level.FINE, "Invalid DataObject", (Throwable)e);
            }
        }
        this.filterListener = filter != null ? new FilterListener(filter) : null;
        this.listener = ec != null ? new DocListener(ec) : null;
        this.cpInfoListener = new ClasspathInfoListener(this.listeners, new Runnable(){

            @Override
            public void run() {
                if (JavacParser.this.sourceCount == 0) {
                    JavacParser.this.invalidate(true);
                }
            }
        });
    }

    private void init(Snapshot snapshot, Task task, boolean singleSource) {
        boolean explicitCpInfo;
        boolean bl = explicitCpInfo = task instanceof ClasspathInfoProvider && ((ClasspathInfoProvider)task).getClasspathInfo() != null;
        if (!this.initialized) {
            Source source = snapshot.getSource();
            FileObject sourceFile = source.getFileObject();
            assert (sourceFile != null);
            this.file = sourceFile;
            ClasspathInfo oldInfo = this.cpInfo;
            this.cpInfo = explicitCpInfo ? ((ClasspathInfoProvider)task).getClasspathInfo() : ClasspathInfo.create(sourceFile);
            ClassPath cp = this.cpInfo.getClassPath(ClasspathInfo.PathKind.SOURCE);
            assert (cp != null);
            this.root = cp.findOwnerRoot(sourceFile);
            if (singleSource) {
                if (oldInfo != null && this.weakCpListener != null) {
                    oldInfo.removeChangeListener(this.weakCpListener);
                    this.weakCpListener = null;
                }
                if (!explicitCpInfo) {
                    this.weakCpListener = WeakListeners.change((ChangeListener)this.cpInfoListener, (Object)this.cpInfo);
                    this.cpInfo.addChangeListener(this.weakCpListener);
                }
                this.initialized = true;
            }
        } else if (singleSource && !explicitCpInfo) {
            assert (this.file != null);
            assert (this.cpInfo != null);
            ClassPath scp = ClassPath.getClassPath((FileObject)this.file, (String)"classpath/source");
            if (scp != this.cpInfo.getClassPath(ClasspathInfo.PathKind.SOURCE)) {
                Project owner = FileOwnerQuery.getOwner((FileObject)this.file);
                Object[] arrobject = new Object[4];
                arrobject[0] = this.file;
                arrobject[1] = owner == null ? "null" : FileUtil.getFileDisplayName((FileObject)owner.getProjectDirectory()) + " (" + owner.getClass() + ")";
                arrobject[2] = this.cpInfo.getClassPath(ClasspathInfo.PathKind.SOURCE);
                arrobject[3] = scp;
                LOGGER.log(Level.WARNING, "ClassPath identity changed for {0}, class path owner: {1} original sourcePath: {2} new sourcePath: {3}", arrobject);
                if (this.weakCpListener != null) {
                    this.cpInfo.removeChangeListener(this.weakCpListener);
                }
                this.cpInfo = ClasspathInfo.create(this.file);
                ClassPath cp = this.cpInfo.getClassPath(ClasspathInfo.PathKind.SOURCE);
                assert (cp != null);
                this.root = cp.findOwnerRoot(this.file);
                this.weakCpListener = WeakListeners.change((ChangeListener)this.cpInfoListener, (Object)this.cpInfo);
                this.cpInfo.addChangeListener(this.weakCpListener);
                JavaSourceAccessor.getINSTANCE().invalidateCachedClasspathInfo(this.file);
            }
        }
    }

    private void init(Task task) {
        if (!this.initialized) {
            ClasspathInfo _tmpInfo = null;
            if (task instanceof ClasspathInfoProvider && (_tmpInfo = ((ClasspathInfoProvider)task).getClasspathInfo()) != null) {
                if (this.cpInfo != null && this.weakCpListener != null) {
                    this.cpInfo.removeChangeListener(this.weakCpListener);
                    this.weakCpListener = null;
                }
            } else {
                throw new IllegalArgumentException("No classpath provided by task: " + (Object)task);
            }
            this.cpInfo = _tmpInfo;
            this.weakCpListener = WeakListeners.change((ChangeListener)this.cpInfoListener, (Object)this.cpInfo);
            this.cpInfo.addChangeListener(this.weakCpListener);
            this.initialized = true;
        }
    }

    private void invalidate(boolean reinit) {
        this.invalid = true;
        if (reinit) {
            this.initialized = false;
        }
    }

    public void parse(Snapshot snapshot, Task task, SourceModificationEvent event) throws ParseException {
        try {
            this.parseImpl(snapshot, task);
        }
        catch (FileObjects.InvalidFileException ife) {
        }
        catch (IOException ioe) {
            throw new ParseException("JavacParser failure", (Throwable)ioe);
        }
    }

    private boolean shouldParse(@NonNull Task task) {
        JavaSource oldSource;
        if (!(task instanceof MimeTask)) {
            this.currentSource = null;
            return true;
        }
        JavaSource newSource = ((MimeTask)task).getJavaSource();
        if (this.invalid) {
            this.currentSource = new WeakReference<JavaSource>(newSource);
            return true;
        }
        JavaSource javaSource = oldSource = this.currentSource == null ? null : this.currentSource.get();
        if (oldSource == null) {
            this.currentSource = new WeakReference<JavaSource>(newSource);
            return true;
        }
        if (newSource.equals(oldSource)) {
            return false;
        }
        this.currentSource = new WeakReference<JavaSource>(newSource);
        return true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void parseImpl(Snapshot snapshot, Task task) throws IOException {
        assert (task != null);
        assert (this.privateParser || Utilities.holdsParserLock());
        ++this.parseId;
        this.parserCanceled.set(false);
        this.indexCanceled.set(false);
        this.cachedSnapShot = snapshot;
        Object[] arrobject = new Object[2];
        arrobject[0] = task.toString();
        arrobject[1] = snapshot == null ? "null" : snapshot.getText();
        LOGGER.log(Level.FINE, "parse: task: {0}\n{1}", arrobject);
        CompilationInfoImpl oldInfo = this.ciImpl;
        boolean success = false;
        try {
            switch (this.sourceCount) {
                case 0: {
                    if (!this.shouldParse(task)) break;
                    this.init(task);
                    this.ciImpl = new CompilationInfoImpl(this.cpInfo);
                    break;
                }
                case 1: {
                    Pair _changedMethod;
                    this.init(snapshot, task, true);
                    boolean needsFullReparse = true;
                    if (this.supportsReparse && (_changedMethod = (Pair)this.changedMethod.getAndSet(null)) != null && this.ciImpl != null) {
                        LOGGER.log(Level.FINE, "\t:trying partial reparse:\n{0}", ((DocPositionRegion)((Object)_changedMethod.first())).getText());
                        boolean bl = needsFullReparse = !JavacParser.reparseMethod(this.ciImpl, snapshot, (MethodTree)_changedMethod.second(), ((DocPositionRegion)((Object)_changedMethod.first())).getText());
                        if (!needsFullReparse) {
                            this.ciImpl.setChangedMethod(_changedMethod);
                        }
                    }
                    if (!needsFullReparse) break;
                    this.positions.clear();
                    this.ciImpl = JavacParser.createCurrentInfo(this, this.file, this.root, snapshot, null, null);
                    LOGGER.fine("\t:created new javac");
                    break;
                }
                default: {
                    this.init(snapshot, task, false);
                    this.ciImpl = JavacParser.createCurrentInfo(this, this.file, this.root, snapshot, this.ciImpl == null ? null : this.ciImpl.getJavacTask(), this.ciImpl == null ? null : this.ciImpl.getDiagnosticListener());
                }
            }
            boolean bl = this.invalid = !(success = true);
        }
        catch (Throwable var7_7) {
            boolean bl = this.invalid = !success;
            if (oldInfo != this.ciImpl && oldInfo != null) {
                oldInfo.dispose();
            }
            throw var7_7;
        }
        if (oldInfo != this.ciImpl && oldInfo != null) {
            oldInfo.dispose();
        }
    }

    public JavacParserResult getResult(Task task) throws ParseException {
        NewComilerTask nct;
        assert (this.privateParser || Utilities.holdsParserLock());
        if (this.ciImpl == null && !this.invalid) {
            throw new IllegalStateException("No CompilationInfoImpl in valid parser");
        }
        LOGGER.log(Level.FINE, "getResult: task:{0}", task.toString());
        boolean isJavaParserResultTask = task instanceof JavaParserResultTask;
        boolean isParserResultTask = task instanceof ParserResultTask;
        boolean isUserTask = task instanceof UserTask;
        boolean isClasspathInfoProvider = task instanceof ClasspathInfoProvider;
        if (this.invalid || isClasspathInfoProvider) {
            ClasspathInfo providedInfo;
            if (this.invalid) {
                LOGGER.fine("Invalid, reparse");
            }
            if (isClasspathInfoProvider && (providedInfo = ((ClasspathInfoProvider)task).getClasspathInfo()) != null && !providedInfo.equals(this.cpInfo)) {
                if (this.sourceCount != 0) {
                    LOGGER.log(Level.FINE, "Task {0} has changed ClasspathInfo form: {1} to:{2}", new Object[]{task, this.cpInfo, providedInfo});
                }
                this.invalidate(true);
            }
            if (this.invalid) {
                assert (this.cachedSnapShot != null || this.sourceCount == 0);
                try {
                    this.parseImpl(this.cachedSnapShot, task);
                }
                catch (FileObjects.InvalidFileException ife) {
                    LOGGER.warning(ife.getMessage());
                    return null;
                }
                catch (IOException ioe) {
                    throw new ParseException("JavacParser failure", (Throwable)ioe);
                }
            }
        }
        JavacParserResult result = null;
        if (isParserResultTask) {
            JavaSource.Phase reachedPhase;
            JavaSource.Phase requiredPhase;
            if (isJavaParserResultTask) {
                requiredPhase = ((JavaParserResultTask)task).getPhase();
            } else {
                requiredPhase = JavaSource.Phase.RESOLVED;
                LOGGER.log(Level.WARNING, "ParserResultTask: {0} doesn''t provide phase, assuming RESOLVED", (Object)task);
            }
            DefaultCancelService cancelService = DefaultCancelService.instance(this.ciImpl.getJavacTask().getContext());
            if (cancelService != null) {
                cancelService.mayCancel.set(true);
            }
            try {
                reachedPhase = this.moveToPhase(requiredPhase, this.ciImpl, true);
            }
            catch (IOException ioe) {
                throw new ParseException("JavacParser failure", (Throwable)ioe);
            }
            finally {
                if (cancelService != null) {
                    cancelService.mayCancel.set(false);
                }
            }
            if (reachedPhase.compareTo(requiredPhase) >= 0) {
                ClassIndexImpl.cancel.set(this.indexCanceled);
                result = new JavacParserResult(JavaSourceAccessor.getINSTANCE().createCompilationInfo(this.ciImpl));
            }
        } else if (isUserTask) {
            result = new JavacParserResult(JavaSourceAccessor.getINSTANCE().createCompilationController(this.ciImpl));
        } else {
            LOGGER.log(Level.WARNING, "Ignoring unknown task: {0}", (Object)task);
        }
        if (task instanceof NewComilerTask && ((nct = (NewComilerTask)task).getCompilationController() == null || nct.getTimeStamp() != this.parseId)) {
            try {
                nct.setCompilationController(JavaSourceAccessor.getINSTANCE().createCompilationController(new CompilationInfoImpl(this, this.file, this.root, null, null, this.cachedSnapShot, true)), this.parseId);
            }
            catch (IOException ioe) {
                throw new ParseException("Javac Failure", (Throwable)ioe);
            }
        }
        return result;
    }

    public void cancel(@NonNull Parser.CancelReason reason, @NonNull SourceModificationEvent event) {
        this.indexCanceled.set(true);
        if (reason == Parser.CancelReason.SOURCE_MODIFICATION_EVENT && event.sourceChanged()) {
            this.parserCanceled.set(true);
        }
    }

    public void resultFinished(boolean isCancelable) {
        if (isCancelable) {
            ClassIndexImpl.cancel.remove();
            this.indexCanceled.set(false);
        }
    }

    public void addChangeListener(ChangeListener changeListener) {
        assert (changeListener != null);
        this.listeners.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        assert (changeListener != null);
        this.listeners.removeChangeListener(changeListener);
    }

    ClasspathInfo getClasspathInfo() {
        return this.cpInfo;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    JavaSource.Phase moveToPhase(JavaSource.Phase phase, CompilationInfoImpl currentInfo, boolean cancellable) throws IOException {
        JavaSource.Phase currentPhase;
        JavaSource.Phase parserError = currentInfo.parserCrashed;
        assert (parserError != null);
        currentPhase = currentInfo.getPhase();
        try {
            long start2;
            if (currentPhase.compareTo(JavaSource.Phase.PARSED) < 0 && phase.compareTo(JavaSource.Phase.PARSED) >= 0 && phase.compareTo(parserError) <= 0) {
                Document doc;
                if (cancellable && this.parserCanceled.get()) {
                    JavaSource.Phase phase2 = JavaSource.Phase.MODIFIED;
                    return phase2;
                }
                start2 = System.currentTimeMillis();
                Iterable trees = currentInfo.getJavacTask().parse(new JavaFileObject[]{currentInfo.jfo});
                if (trees == null) {
                    LOGGER.log(Level.INFO, "Did not parse anything for: {0}", currentInfo.jfo.toUri());
                    JavaSource.Phase phase3 = JavaSource.Phase.MODIFIED;
                    return phase3;
                }
                Iterator it = trees.iterator();
                if (!it.hasNext()) {
                    LOGGER.log(Level.INFO, "Did not parse anything for: {0}", currentInfo.jfo.toUri());
                    JavaSource.Phase phase4 = JavaSource.Phase.MODIFIED;
                    return phase4;
                }
                CompilationUnitTree unit = (CompilationUnitTree)it.next();
                currentInfo.setCompilationUnit(unit);
                assert (!it.hasNext());
                Document document = doc = this.listener == null ? null : this.listener.document;
                if (doc != null && this.supportsReparse) {
                    FindMethodRegionsVisitor v = new FindMethodRegionsVisitor(doc, Trees.instance((JavaCompiler.CompilationTask)currentInfo.getJavacTask()).getSourcePositions(), this.parserCanceled);
                    v.visit((Tree)unit, (Object)null);
                    List<Pair<DocPositionRegion, MethodTree>> list = this.positions;
                    synchronized (list) {
                        this.positions.clear();
                        if (!this.parserCanceled.get()) {
                            this.positions.addAll(v.getResult());
                        }
                    }
                }
                currentPhase = JavaSource.Phase.PARSED;
                long end = System.currentTimeMillis();
                FileObject currentFile = currentInfo.getFileObject();
                TIME_LOGGER.log(Level.FINE, "Compilation Unit", new Object[]{currentFile, unit});
                JavacParser.logTime(currentFile, currentPhase, end - start2);
            }
            if (currentPhase == JavaSource.Phase.PARSED && phase.compareTo(JavaSource.Phase.ELEMENTS_RESOLVED) >= 0 && phase.compareTo(parserError) <= 0) {
                if (cancellable && this.parserCanceled.get()) {
                    JavaSource.Phase start2 = JavaSource.Phase.MODIFIED;
                    return start2;
                }
                start2 = System.currentTimeMillis();
                currentInfo.getJavacTask().enter();
                currentPhase = JavaSource.Phase.ELEMENTS_RESOLVED;
                long end = System.currentTimeMillis();
                JavacParser.logTime(currentInfo.getFileObject(), currentPhase, end - start2);
            }
            if (currentPhase == JavaSource.Phase.ELEMENTS_RESOLVED && phase.compareTo(JavaSource.Phase.RESOLVED) >= 0 && phase.compareTo(parserError) <= 0) {
                if (cancellable && this.parserCanceled.get()) {
                    JavaSource.Phase start3 = JavaSource.Phase.MODIFIED;
                    return start3;
                }
                start2 = System.currentTimeMillis();
                JavacTaskImpl jti = currentInfo.getJavacTask();
                PostFlowAnalysis.analyze(jti.analyze(), jti.getContext());
                currentPhase = JavaSource.Phase.RESOLVED;
                long end = System.currentTimeMillis();
                JavacParser.logTime(currentInfo.getFileObject(), currentPhase, end - start2);
            }
            if (currentPhase == JavaSource.Phase.RESOLVED && phase.compareTo(JavaSource.Phase.UP_TO_DATE) >= 0) {
                currentPhase = JavaSource.Phase.UP_TO_DATE;
            }
        }
        catch (CouplingAbort a) {
            TreeLoader.dumpCouplingAbort(a, null);
            JavaSource.Phase phase5 = currentPhase;
            return phase5;
        }
        catch (CancelAbort ca) {
            currentPhase = JavaSource.Phase.MODIFIED;
            this.invalidate(false);
        }
        catch (Abort abort) {
            parserError = currentPhase;
        }
        catch (IOException ex) {
            currentInfo.parserCrashed = currentPhase;
            JavacParser.dumpSource(currentInfo, ex);
            throw ex;
        }
        catch (RuntimeException ex) {
            parserError = currentPhase;
            JavacParser.dumpSource(currentInfo, ex);
            throw ex;
        }
        catch (Error ex) {
            parserError = currentPhase;
            JavacParser.dumpSource(currentInfo, ex);
            throw ex;
        }
        finally {
            currentInfo.setPhase(currentPhase);
            currentInfo.parserCrashed = parserError;
        }
        return currentPhase;
    }

    private static CompilationInfoImpl createCurrentInfo(JavacParser parser, FileObject file, FileObject root, Snapshot snapshot, JavacTaskImpl javac, DiagnosticListener<JavaFileObject> diagnosticListener) throws IOException {
        CompilationInfoImpl info = new CompilationInfoImpl(parser, file, root, javac, diagnosticListener, snapshot, false);
        if (file != null) {
            Logger.getLogger("TIMER").log(Level.FINE, "CompilationInfo", new Object[]{file, info});
        }
        return info;
    }

    static JavacTaskImpl createJavacTask(FileObject file, FileObject root, ClasspathInfo cpInfo, JavacParser parser, DiagnosticListener<? super JavaFileObject> diagnosticListener, ClassNamesForFileOraculum oraculum, boolean detached) {
        SourceLevelQuery.Result sourceLevel = null;
        if (file != null) {
            if (LOGGER.isLoggable(Level.FINER)) {
                LOGGER.log(Level.FINER, "Created new JavacTask for: {0}", FileUtil.getFileDisplayName((FileObject)file));
            }
            sourceLevel = SourceLevelQuery.getSourceLevel2((FileObject)file);
        }
        FQN2Files dcc = null;
        if (root != null) {
            try {
                dcc = FQN2Files.forRoot(root.toURL());
            }
            catch (IOException ex) {
                LOGGER.log(Level.FINE, null, ex);
            }
        }
        JavacTaskImpl javacTask = JavacParser.createJavacTask(cpInfo, diagnosticListener, sourceLevel != null ? sourceLevel.getSourceLevel() : null, sourceLevel != null ? sourceLevel.getProfile() : null, false, oraculum, dcc, parser == null ? null : new DefaultCancelService(parser), APTUtils.get(root));
        Context context = javacTask.getContext();
        TreeLoader.preRegister(context, cpInfo, detached);
        return javacTask;
    }

    public static JavacTaskImpl createJavacTask(@NonNull ClasspathInfo cpInfo, @NullAllowed DiagnosticListener<? super JavaFileObject> diagnosticListener, @NullAllowed String sourceLevel, @NullAllowed SourceLevelQuery.Profile sourceProfile, @NullAllowed ClassNamesForFileOraculum cnih, @NullAllowed DuplicateClassChecker dcc, @NullAllowed CancelService cancelService, @NullAllowed APTUtils aptUtils) {
        return JavacParser.createJavacTask(cpInfo, diagnosticListener, sourceLevel, sourceProfile, true, cnih, dcc, cancelService, aptUtils);
    }

    private static JavacTaskImpl createJavacTask(@NonNull ClasspathInfo cpInfo, @NullAllowed DiagnosticListener<? super JavaFileObject> diagnosticListener, @NullAllowed String sourceLevel, @NullAllowed SourceLevelQuery.Profile sourceProfile, boolean backgroundCompilation, @NullAllowed ClassNamesForFileOraculum cnih, @NullAllowed DuplicateClassChecker dcc, @NullAllowed CancelService cancelService, @NullAllowed APTUtils aptUtils) {
        ArrayList<String> options = new ArrayList<String>();
        String lintOptions = CompilerSettings.getCommandLine(cpInfo);
        com.sun.tools.javac.code.Source validatedSourceLevel = JavacParser.validateSourceLevel(sourceLevel, cpInfo);
        if (lintOptions.length() > 0) {
            options.addAll(Arrays.asList(lintOptions.split(" ")));
        }
        if (!backgroundCompilation) {
            options.add("-Xjcov");
            options.add("-XDallowStringFolding=false");
            options.add("-XDkeepComments=true");
            if (!$assertionsDisabled && !options.add("-XDdev")) {
                // empty if block
            }
        } else {
            options.add("-XDbackgroundCompilation");
            options.add("-XDcompilePolicy=byfile");
            options.add("-XD-Xprefer=source");
            options.add("-target");
            options.add(validatedSourceLevel.requiredTarget().name);
        }
        options.add("-XDide");
        options.add("-XDsave-parameter-names");
        options.add("-XDsuppressAbortOnBadClassFile");
        options.add("-XDshouldStopPolicy=GENERATE");
        options.add("-g:source");
        options.add("-g:lines");
        options.add("-g:vars");
        options.add("-source");
        options.add(validatedSourceLevel.name);
        if (sourceProfile != null && sourceProfile != SourceLevelQuery.Profile.DEFAULT) {
            options.add("-profile");
            options.add(sourceProfile.getName());
        }
        options.add("-XDdiags=-source");
        options.add("-XDdiagsFormat=%L%m|%L%m|%L%m");
        options.add("-XDbreakDocCommentParsingOnError=false");
        boolean aptEnabled = aptUtils != null && aptUtils.aptEnabledOnScan() && (backgroundCompilation || aptUtils.aptEnabledInEditor()) && !ClasspathInfoAccessor.getINSTANCE().getCachedClassPath(cpInfo, ClasspathInfo.PathKind.SOURCE).entries().isEmpty();
        Collection<? extends Processor> processors = null;
        if (aptEnabled) {
            processors = aptUtils.resolveProcessors(backgroundCompilation);
            if (processors.isEmpty()) {
                aptEnabled = false;
            } else {
                for (Processor p : processors) {
                    if (!"lombok.core.AnnotationProcessor".equals(p.getClass().getName())) continue;
                    options.add("-XDlombokDetected");
                    break;
                }
            }
        }
        if (aptEnabled) {
            for (Map.Entry entry : aptUtils.processorOptions().entrySet()) {
                StringBuilder sb = new StringBuilder();
                sb.append("-A").append((String)entry.getKey());
                if (entry.getValue() != null) {
                    sb.append('=').append((String)entry.getValue());
                }
                options.add(sb.toString());
            }
        } else {
            options.add("-proc:none");
        }
        Context context = new Context();
        NBMessager.preRegister((Context)context, (String)null, (PrintWriter)DEV_NULL, (PrintWriter)DEV_NULL, (PrintWriter)DEV_NULL);
        JavacTaskImpl task = (JavacTaskImpl)JavacTool.create().getTask(null, ClasspathInfoAccessor.getINSTANCE().createFileManager(cpInfo), diagnosticListener, options, null, Collections.emptySet(), context);
        if (aptEnabled) {
            task.setProcessors(processors);
        }
        NBClassReader.preRegister((Context)context, (boolean)(!backgroundCompilation));
        if (cnih != null) {
            context.put(ClassNamesForFileOraculum.class, (Object)cnih);
        }
        if (dcc != null) {
            context.put(DuplicateClassChecker.class, (Object)dcc);
        }
        if (cancelService != null) {
            DefaultCancelService.preRegister(context, cancelService);
        }
        NBAttr.preRegister((Context)context);
        NBClassWriter.preRegister((Context)context);
        NBParserFactory.preRegister((Context)context);
        NBTreeMaker.preRegister((Context)context);
        NBJavacTrees.preRegister((Context)context);
        if (!backgroundCompilation) {
            JavacFlowListener.preRegister(context, task);
            NBJavadocEnter.preRegister((Context)context);
            NBJavadocMemberEnter.preRegister((Context)context);
            JavadocEnv.preRegister(context, cpInfo);
            NBResolve.preRegister((Context)context);
        } else {
            NBEnter.preRegister((Context)context);
            NBMemberEnter.preRegister((Context)context);
        }
        TIME_LOGGER.log(Level.FINE, "JavaC", (Object)context);
        return task;
    }

    @NonNull
    static com.sun.tools.javac.code.Source validateSourceLevel(@NullAllowed String sourceLevel, @NonNull ClasspathInfo cpInfo) {
        Level warnLevel;
        ClassPath bootClassPath = cpInfo.getClassPath(ClasspathInfo.PathKind.BOOT);
        ClassPath classPath = null;
        ClassPath srcClassPath = cpInfo.getClassPath(ClasspathInfo.PathKind.SOURCE);
        com.sun.tools.javac.code.Source[] sources = com.sun.tools.javac.code.Source.values();
        if (sourceLevel == null) {
            sourceLevel = sources[sources.length - 1].name;
            warnLevel = Level.FINE;
        } else {
            warnLevel = Level.WARNING;
        }
        for (com.sun.tools.javac.code.Source source : sources) {
            if (!source.name.equals(sourceLevel)) continue;
            if (DISABLE_SOURCE_LEVEL_DOWNGRADE) {
                return source;
            }
            if (source.compareTo((Enum)com.sun.tools.javac.code.Source.JDK1_4) >= 0 && bootClassPath != null && bootClassPath.findResource("java/lang/AssertionError.class") == null) {
                if (bootClassPath.findResource("java/lang/Object.class") == null) {
                    classPath = cpInfo.getClassPath(ClasspathInfo.PathKind.COMPILE);
                }
                if (!JavacParser.hasResource("java/lang/AssertionError", ClassPath.EMPTY, classPath, srcClassPath)) {
                    LOGGER.log(warnLevel, "Even though the source level of {0} is set to: {1}, java.lang.AssertionError cannot be found on the bootclasspath: {2}\nChanging source level to 1.3", new Object[]{cpInfo.getClassPath(ClasspathInfo.PathKind.SOURCE), sourceLevel, bootClassPath});
                    return com.sun.tools.javac.code.Source.JDK1_3;
                }
            }
            if (source.compareTo((Enum)com.sun.tools.javac.code.Source.JDK1_5) >= 0 && !JavacParser.hasResource("java/lang/StringBuilder", bootClassPath, classPath, srcClassPath)) {
                LOGGER.log(warnLevel, "Even though the source level of {0} is set to: {1}, java.lang.StringBuilder cannot be found on the bootclasspath: {2}\nChanging source level to 1.4", new Object[]{cpInfo.getClassPath(ClasspathInfo.PathKind.SOURCE), sourceLevel, bootClassPath});
                return com.sun.tools.javac.code.Source.JDK1_4;
            }
            if (source.compareTo((Enum)com.sun.tools.javac.code.Source.JDK1_7) >= 0 && !JavacParser.hasResource("java/lang/AutoCloseable", bootClassPath, classPath, srcClassPath)) {
                LOGGER.log(warnLevel, "Even though the source level of {0} is set to: {1}, java.lang.AutoCloseable cannot be found on the bootclasspath: {2}\nTry with resources is unsupported.", new Object[]{cpInfo.getClassPath(ClasspathInfo.PathKind.SOURCE), sourceLevel, bootClassPath});
            }
            if (source.compareTo((Enum)com.sun.tools.javac.code.Source.JDK1_8) >= 0 && !JavacParser.hasResource("java/util/stream/Streams", bootClassPath, classPath, srcClassPath)) {
                LOGGER.log(warnLevel, "Even though the source level of {0} is set to: {1}, java.util.stream.Streams cannot be found on the bootclasspath: {2}\nChanging source level to 1.7", new Object[]{cpInfo.getClassPath(ClasspathInfo.PathKind.SOURCE), sourceLevel, bootClassPath});
                return com.sun.tools.javac.code.Source.JDK1_7;
            }
            return source;
        }
        SpecificationVersion JAVA_12 = new SpecificationVersion("1.2");
        SpecificationVersion specVer = new SpecificationVersion(sourceLevel);
        if (JAVA_12.compareTo((Object)specVer) > 0) {
            return sources[0];
        }
        return sources[sources.length - 1];
    }

    private static boolean hasResource(@NonNull String resourceBase, @NullAllowed ClassPath boot, @NullAllowed ClassPath compile, @NullAllowed ClassPath source) {
        String resourceClass = String.format("%s.class", resourceBase);
        String resourceJava = String.format("%s.java", resourceBase);
        if (boot != null && boot.findResource(resourceClass) == null && source != null && source.findResource(resourceJava) == null && (compile == null || compile.findResource(resourceClass) == null)) {
            return false;
        }
        return true;
    }

    private static void logTime(FileObject source, JavaSource.Phase phase, long time) {
        assert (source != null && phase != null);
        String message = phase2Message.get((Object)phase);
        assert (message != null);
        TIME_LOGGER.log(Level.FINE, message, new Object[]{source, time});
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void dumpSource(CompilationInfoImpl info, Throwable exc) {
        String userDir = System.getProperty("netbeans.user");
        if (userDir == null) {
            return;
        }
        String dumpDir = userDir + "/var/log/";
        String src = info.getText();
        FileObject file = info.getFileObject();
        String fileName = FileUtil.getFileDisplayName((FileObject)file);
        String origName = file.getName();
        File f = new File(dumpDir + origName + ".dump");
        boolean dumpSucceeded = false;
        for (int i = 1; i < MAX_DUMPS && f.exists(); ++i) {
            f = new File(dumpDir + origName + '_' + i + ".dump");
        }
        if (!f.exists()) {
            try {
                FileOutputStream os = new FileOutputStream(f);
                PrintWriter writer = new PrintWriter(new OutputStreamWriter((OutputStream)os, "UTF-8"));
                try {
                    writer.println(src);
                    writer.println("----- Classpath: ---------------------------------------------");
                    ClassPath bootPath = info.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.BOOT);
                    ClassPath classPath = info.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.COMPILE);
                    ClassPath sourcePath = info.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.SOURCE);
                    writer.println("bootPath: " + (bootPath != null ? bootPath.toString() : "null"));
                    writer.println("classPath: " + (classPath != null ? classPath.toString() : "null"));
                    writer.println("sourcePath: " + (sourcePath != null ? sourcePath.toString() : "null"));
                    writer.println("----- Original exception ---------------------------------------------");
                    exc.printStackTrace(writer);
                }
                finally {
                    writer.close();
                    dumpSucceeded = true;
                }
            }
            catch (IOException ioe) {
                LOGGER.log(Level.INFO, "Error when writing parser dump file!", ioe);
            }
        }
        if (dumpSucceeded) {
            try {
                Throwable t = Exceptions.attachMessage((Throwable)exc, (String)("An error occurred during parsing of '" + fileName + "'. Please report a bug against java/source and attach dump file '" + f.getAbsolutePath() + "'."));
                Exceptions.printStackTrace((Throwable)t);
            }
            catch (RuntimeException re) {
                Exceptions.printStackTrace((Throwable)exc);
            }
        } else {
            LOGGER.log(Level.WARNING, "Dump could not be written. Either dump file could not be created or all dump files were already used. Please check that you have write permission to '" + dumpDir + "' and " + "clean all *.dump files in that directory.");
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    private static boolean reparseMethod(CompilationInfoImpl ci, Snapshot snapshot, MethodTree orig, String newBody) throws IOException {
        assert (ci != null);
        FileObject fo = ci.getFileObject();
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.log(Level.FINER, "Reparse method in: {0}", (Object)fo);
        }
        if (((JCTree.JCMethodDecl)orig).localEnv == null) {
            return false;
        }
        JavaSource.Phase currentPhase = ci.getPhase();
        if (JavaSource.Phase.PARSED.compareTo(currentPhase) > 0) {
            return false;
        }
        try {
            CompilationUnitTree cu = ci.getCompilationUnit();
            if (cu == null || newBody == null) {
                return false;
            }
            JavacTaskImpl task = ci.getJavacTask();
            if (Options.instance((Context)task.getContext()).isSet("lombokDetected")) {
                return false;
            }
            PartialReparser pr = PartialReparser.instance((Context)task.getContext());
            JavacTrees jt = JavacTrees.instance((JavaCompiler.CompilationTask)task);
            int origStartPos = (int)jt.getSourcePositions().getStartPosition(cu, (Tree)orig.getBody());
            int origEndPos = (int)jt.getSourcePositions().getEndPosition(cu, (Tree)orig.getBody());
            if (origStartPos < 0) {
                LOGGER.log(Level.WARNING, "Javac returned startpos: {0} < 0", new Object[]{origStartPos});
                return false;
            }
            if (origStartPos > origEndPos) {
                LOGGER.log(Level.WARNING, "Javac returned startpos: {0} > endpos: {1}", new Object[]{origStartPos, origEndPos});
                return false;
            }
            FindAnonymousVisitor fav = new FindAnonymousVisitor();
            fav.scan((Tree)orig.getBody(), (Object)null);
            if (fav.hasLocalClass) {
                if (!LOGGER.isLoggable(Level.FINER)) return false;
                {
                    LOGGER.log(Level.FINER, "Skeep reparse method (old local classes): {0}", (Object)fo);
                }
                return false;
            }
            int firstInner = fav.firstInner;
            int noInner = fav.noInner;
            Context ctx = task.getContext();
            TreeLoader treeLoader = TreeLoader.instance(ctx);
            if (treeLoader != null) {
                treeLoader.startPartialReparse();
            }
            try {
                Log l = Log.instance((Context)ctx);
                l.startPartialReparse();
                JavaFileObject prevLogged = l.useSource(cu.getSourceFile());
                try {
                    DiagnosticListener<JavaFileObject> dl = ci.getDiagnosticListener();
                    assert (dl instanceof CompilationInfoImpl.DiagnosticListenerImpl);
                    ((CompilationInfoImpl.DiagnosticListenerImpl)dl).startPartialReparse(origStartPos, origEndPos);
                    long start = System.currentTimeMillis();
                    HashMap docComments = new HashMap();
                    JCTree.JCBlock block = pr.reparseMethodBody(cu, orig, newBody, firstInner, docComments);
                    LOGGER.log(Level.FINER, "Reparsed method in: {0}", (Object)fo);
                    if (block == null) {
                        LOGGER.log(Level.FINER, "Skeep reparse method, invalid position, newBody: ", newBody);
                        boolean bl = false;
                        return bl;
                    }
                    fav.reset();
                    fav.scan((Tree)block, (Object)null);
                    int newNoInner = fav.noInner;
                    if (fav.hasLocalClass || noInner != newNoInner) {
                        if (LOGGER.isLoggable(Level.FINER)) {
                            LOGGER.log(Level.FINER, "Skeep reparse method (new local classes): {0}", (Object)fo);
                        }
                        boolean bl = false;
                        return bl;
                    }
                    ((LazyDocCommentTable)((JCTree.JCCompilationUnit)cu).docComments).table.keySet().removeAll(fav.docOwners);
                    ((LazyDocCommentTable)((JCTree.JCCompilationUnit)cu).docComments).table.putAll(docComments);
                    long end = System.currentTimeMillis();
                    if (fo != null) {
                        JavacParser.logTime(fo, JavaSource.Phase.PARSED, end - start);
                    }
                    int newEndPos = (int)jt.getSourcePositions().getEndPosition(cu, (Tree)block);
                    int delta = newEndPos - origEndPos;
                    EndPosTable endPos = ((JCTree.JCCompilationUnit)cu).endPositions;
                    TranslatePositionsVisitor tpv = new TranslatePositionsVisitor(orig, endPos, delta);
                    tpv.scan((Tree)cu, null);
                    ((JCTree.JCMethodDecl)orig).body = block;
                    if (JavaSource.Phase.RESOLVED.compareTo(currentPhase) <= 0) {
                        JavacFlowListener fl;
                        start = System.currentTimeMillis();
                        pr.reattrMethodBody(orig, (BlockTree)block);
                        if (LOGGER.isLoggable(Level.FINER)) {
                            LOGGER.log(Level.FINER, "Resolved method in: {0}", (Object)fo);
                        }
                        if (!((CompilationInfoImpl.DiagnosticListenerImpl)dl).hasPartialReparseErrors() && (fl = JavacFlowListener.instance(ctx)) != null && fl.hasFlowCompleted(fo)) {
                            List<Diagnostic> diag;
                            if (LOGGER.isLoggable(Level.FINER) && !(diag = ci.getDiagnostics()).isEmpty()) {
                                LOGGER.log(Level.FINER, "Reflow with errors: {0} {1}", new Object[]{fo, diag});
                            }
                            TreePath tp = TreePath.getPath((CompilationUnitTree)cu, (Tree)orig);
                            Tree t = tp.getParentPath().getLeaf();
                            pr.reflowMethodBody(cu, (ClassTree)t, orig);
                            if (LOGGER.isLoggable(Level.FINER)) {
                                LOGGER.log(Level.FINER, "Reflowed method in: {0}", (Object)fo);
                            }
                        }
                        end = System.currentTimeMillis();
                        if (fo != null) {
                            JavacParser.logTime(fo, JavaSource.Phase.ELEMENTS_RESOLVED, 0);
                            JavacParser.logTime(fo, JavaSource.Phase.RESOLVED, end - start);
                        }
                    }
                    long startM = System.currentTimeMillis();
                    char[] chars = snapshot.getText().toString().toCharArray();
                    ((Position.LineMapImpl)cu.getLineMap()).build(chars, chars.length);
                    LOGGER.log(Level.FINER, "Rebuilding LineMap took: {0}", System.currentTimeMillis() - startM);
                    ((CompilationInfoImpl.DiagnosticListenerImpl)dl).endPartialReparse(delta);
                }
                finally {
                    l.endPartialReparse();
                    l.useSource(prevLogged);
                }
                ci.update(snapshot);
                return true;
            }
            finally {
                if (treeLoader != null) {
                    treeLoader.endPartialReparse();
                }
            }
        }
        catch (CouplingAbort ca) {
            return false;
        }
        catch (Throwable t) {
            if (t instanceof ThreadDeath) {
                throw (ThreadDeath)t;
            }
            boolean a = false;
            if ($assertionsDisabled) return false;
            a = true;
            if (!true) {
                throw new AssertionError();
            }
            if (!a) return false;
            {
                JavacParser.dumpSource(ci, t);
            }
            return false;
        }
    }

    public synchronized void setChangedMethod(Pair<DocPositionRegion, MethodTree> changedMethod) {
        assert (changedMethod != null);
        this.changedMethod.set(changedMethod);
    }

    static {
        phase2Message.put(JavaSource.Phase.PARSED, "Parsed");
        phase2Message.put(JavaSource.Phase.ELEMENTS_RESOLVED, "Signatures Attributed");
        phase2Message.put(JavaSource.Phase.RESOLVED, "Attributed");
        DISABLE_SOURCE_LEVEL_DOWNGRADE = false;
    }

    private final class FilterListener
    implements ChangeListener {
        public FilterListener(JavaFileFilterImplementation filter) {
            filter.addChangeListener(WeakListeners.change((ChangeListener)this, (Object)filter));
        }

        @Override
        public void stateChanged(ChangeEvent event) {
            JavacParser.this.listeners.fireChange();
        }
    }

    private static final class EditorRegistryWeakListener
    extends WeakReference<PropertyChangeListener>
    implements PropertyChangeListener,
    Runnable {
        private Reference<JTextComponent> last;

        private EditorRegistryWeakListener(@NonNull PropertyChangeListener delegate) {
            super(delegate, org.openide.util.Utilities.activeReferenceQueue());
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            PropertyChangeListener delegate = (PropertyChangeListener)this.get();
            JTextComponent lastCandidate = EditorRegistry.focusedComponent();
            if (delegate != null && lastCandidate != null && lastCandidate != this.getLast()) {
                this.setLast(lastCandidate);
                delegate.propertyChange(evt);
            }
        }

        @Override
        public void run() {
            EditorRegistry.removePropertyChangeListener((PropertyChangeListener)this);
        }

        private JTextComponent getLast() {
            return this.last == null ? null : this.last.get();
        }

        private void setLast(JTextComponent last) {
            this.last = new WeakReference<JTextComponent>(last);
        }
    }

    private class DocListener
    implements PropertyChangeListener,
    TokenHierarchyListener {
        private EditorCookie.Observable ec;
        private TokenHierarchyListener lexListener;
        private volatile Document document;

        public DocListener(EditorCookie.Observable ec) {
            assert (ec != null);
            this.ec = ec;
            this.ec.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)this.ec));
            StyledDocument doc = ec.getDocument();
            if (doc != null) {
                TokenHierarchy th = TokenHierarchy.get((Document)doc);
                this.lexListener = (TokenHierarchyListener)WeakListeners.create(TokenHierarchyListener.class, (EventListener)this, (Object)th);
                th.addTokenHierarchyListener(this.lexListener);
                this.document = doc;
            }
            EditorRegistry.addPropertyChangeListener((PropertyChangeListener)new EditorRegistryWeakListener(this));
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("document".equals(evt.getPropertyName())) {
                StyledDocument doc;
                Object old = evt.getOldValue();
                if (old instanceof Document && this.lexListener != null) {
                    TokenHierarchy th = TokenHierarchy.get((Document)((Document)old));
                    th.removeTokenHierarchyListener(this.lexListener);
                    this.lexListener = null;
                }
                if ((doc = this.ec.getDocument()) != null) {
                    TokenHierarchy th = TokenHierarchy.get((Document)doc);
                    this.lexListener = (TokenHierarchyListener)WeakListeners.create(TokenHierarchyListener.class, (EventListener)this, (Object)th);
                    th.addTokenHierarchyListener(this.lexListener);
                    this.document = doc;
                } else {
                    this.document = doc;
                }
            } else if ("focusGained".equals(evt.getPropertyName())) {
                Document focusedDoc;
                Document doc = this.document;
                JTextComponent focused = EditorRegistry.focusedComponent();
                Document document = focusedDoc = focused == null ? null : focused.getDocument();
                if (doc != null) {
                    if (doc == focusedDoc) {
                        JavacParser.this.positions.clear();
                    } else if (JavacParser.this.ciImpl != null) {
                        JavacParser.this.ciImpl.dispose();
                    }
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void tokenHierarchyChanged(TokenHierarchyEvent evt) {
            Pair changedMethod = null;
            if (evt.type() == TokenHierarchyEventType.MODIFICATION) {
                if (JavacParser.this.supportsReparse) {
                    int start = evt.affectedStartOffset();
                    int end = evt.affectedEndOffset();
                    List list = JavacParser.this.positions;
                    synchronized (list) {
                        TokenChange change;
                        for (Pair pe : JavacParser.this.positions) {
                            PositionRegion p = (PositionRegion)pe.first();
                            if (start <= p.getStartOffset() || end >= p.getEndOffset()) continue;
                            changedMethod = pe;
                            break;
                        }
                        if (changedMethod != null && (change = evt.tokenChange(JavaTokenId.language())) != null) {
                            TokenSequence ts = change.removedTokenSequence();
                            if (ts != null) {
                                while (ts.moveNext()) {
                                    switch ((JavaTokenId)ts.token().id()) {
                                        case LBRACE: 
                                        case RBRACE: {
                                            changedMethod = null;
                                        }
                                    }
                                }
                            }
                            if (changedMethod != null) {
                                TokenSequence current = change.currentTokenSequence();
                                current.moveIndex(change.index());
                                for (int i = 0; i < change.addedTokenCount(); ++i) {
                                    current.moveNext();
                                    switch ((JavaTokenId)current.token().id()) {
                                        case LBRACE: 
                                        case RBRACE: {
                                            changedMethod = null;
                                        }
                                    }
                                }
                            }
                        }
                        JavacParser.this.positions.clear();
                        if (changedMethod != null) {
                            JavacParser.this.positions.add(changedMethod);
                        }
                        JavacParser.this.changedMethod.set(changedMethod);
                    }
                }
            } else if (evt.type() == TokenHierarchyEventType.ACTIVITY) {
                JavacParser.this.positions.clear();
                JavacParser.this.changedMethod.set(null);
            }
        }
    }

    private static class DefaultCancelService
    extends CancelService {
        final AtomicBoolean mayCancel = new AtomicBoolean();
        private final JavacParser parser;

        private DefaultCancelService(JavacParser parser) {
            this.parser = parser;
        }

        public static void preRegister(Context context, CancelService cancelServiceToRegister) {
            context.put(cancelServiceKey, (Object)cancelServiceToRegister);
        }

        public static DefaultCancelService instance(Context ctx) {
            assert (ctx != null);
            CancelService cancelService = CancelService.instance((Context)ctx);
            return cancelService instanceof DefaultCancelService ? (DefaultCancelService)cancelService : null;
        }

        public boolean isCanceled() {
            return this.mayCancel.get() && this.parser.parserCanceled.get();
        }
    }

}

