/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 */
package org.netbeans.modules.java.source.parsing;

import java.io.IOException;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.source.parsing.AbstractSourceFileObject;
import org.netbeans.modules.java.source.parsing.SourceFileObject;
import org.netbeans.modules.java.source.parsing.SourceFileObjectProvider;

final class DefaultSourceFileObjectProvider
implements SourceFileObjectProvider {
    DefaultSourceFileObjectProvider() {
    }

    @NonNull
    @Override
    public AbstractSourceFileObject createJavaFileObject(@NonNull AbstractSourceFileObject.Handle handle, @NullAllowed JavaFileFilterImplementation filter, @NullAllowed CharSequence content, boolean renderNow) throws IOException {
        return new SourceFileObject(handle, filter, content, renderNow);
    }
}

