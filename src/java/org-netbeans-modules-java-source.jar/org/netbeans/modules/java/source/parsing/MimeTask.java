/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 */
package org.netbeans.modules.java.source.parsing;

import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.Task;
import org.netbeans.modules.java.source.JavaSourceAccessor;
import org.netbeans.modules.java.source.parsing.ClasspathInfoTask;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.spi.Parser;

public final class MimeTask
extends ClasspathInfoTask {
    private final Task<CompilationController> task;
    private final JavaSource js;

    public MimeTask(JavaSource js, Task<CompilationController> task, ClasspathInfo cpInfo) {
        super(cpInfo);
        assert (js != null);
        assert (task != null);
        this.js = js;
        this.task = task;
    }

    @NonNull
    JavaSource getJavaSource() {
        return this.js;
    }

    public void run(ResultIterator resultIterator) throws Exception {
        Parser.Result result = resultIterator.getParserResult();
        CompilationController cc = CompilationController.get(result);
        assert (cc != null);
        JavaSourceAccessor.getINSTANCE().setJavaSource(cc, this.js);
        this.task.run(cc);
    }
}

