/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.java.source.parsing;

import java.io.IOException;
import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileObject;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.java.source.parsing.PrefetchableJavaFileObject;

public class ForwardingPrefetchableJavaFileObject
extends ForwardingJavaFileObject<PrefetchableJavaFileObject>
implements PrefetchableJavaFileObject {
    public ForwardingPrefetchableJavaFileObject(@NonNull PrefetchableJavaFileObject delegate) {
        super(delegate);
        assert (delegate != null);
    }

    @Override
    public int prefetch() throws IOException {
        return ((PrefetchableJavaFileObject)this.fileObject).prefetch();
    }

    @Override
    public int dispose() {
        return ((PrefetchableJavaFileObject)this.fileObject).dispose();
    }

    @Override
    public String inferBinaryName() {
        return ((PrefetchableJavaFileObject)this.fileObject).inferBinaryName();
    }
}

