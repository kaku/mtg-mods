/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.GlobalPathRegistry
 *  org.netbeans.api.java.queries.AnnotationProcessingQuery
 *  org.netbeans.api.java.queries.AnnotationProcessingQuery$Result
 *  org.netbeans.api.java.queries.SourceForBinaryQuery
 *  org.netbeans.api.java.queries.SourceForBinaryQuery$Result
 *  org.netbeans.spi.java.queries.SourceForBinaryQueryImplementation
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.java.source.classpath;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import javax.swing.event.ChangeListener;
import org.netbeans.api.java.classpath.GlobalPathRegistry;
import org.netbeans.api.java.queries.AnnotationProcessingQuery;
import org.netbeans.api.java.queries.SourceForBinaryQuery;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.spi.java.queries.SourceForBinaryQueryImplementation;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public class CacheSourceForBinaryQueryImpl
implements SourceForBinaryQueryImplementation {
    private String FILE_PROTOCOL = "file";

    public SourceForBinaryQuery.Result findSourceRoots(URL binaryRoot) {
        if (!this.FILE_PROTOCOL.equals(binaryRoot.getProtocol())) {
            return null;
        }
        URL sourceURL = JavaIndex.getSourceRootForClassFolder(binaryRoot);
        R result = null;
        if (sourceURL != null) {
            SourceForBinaryQueryImplementation impl;
            Iterator i$ = Lookup.getDefault().lookupAll(SourceForBinaryQueryImplementation.class).iterator();
            while (i$.hasNext() && ((impl = (SourceForBinaryQueryImplementation)i$.next()) == this || (result = impl.findSourceRoots(sourceURL)) == null)) {
            }
            result = new R(sourceURL, result);
        }
        return result;
    }

    private static class R
    implements SourceForBinaryQuery.Result {
        private final FileObject sourceRoot;
        private final SourceForBinaryQuery.Result delegate;

        public R(URL sourceRootURL, SourceForBinaryQuery.Result delegate) {
            assert (sourceRootURL != null);
            this.sourceRoot = URLMapper.findFileObject((URL)sourceRootURL);
            this.delegate = delegate;
        }

        public void removeChangeListener(ChangeListener l) {
        }

        public void addChangeListener(ChangeListener l) {
        }

        public FileObject[] getRoots() {
            FileObject[] result;
            if (this.delegate != null) {
                result = this.delegate.getRoots();
                if (result.length == 0) {
                    result = this.sourceRoot != null && GlobalPathRegistry.getDefault().getSourceRoots().contains((Object)this.sourceRoot) ? new FileObject[]{this.sourceRoot} : new FileObject[]{};
                }
            } else if (this.sourceRoot == null) {
                result = new FileObject[]{};
            } else {
                FileObject[] aptRoots = R.resolveAptSourceCache(this.sourceRoot);
                if (aptRoots.length == 0) {
                    result = new FileObject[]{this.sourceRoot};
                } else {
                    result = new FileObject[1 + aptRoots.length];
                    result[0] = this.sourceRoot;
                    System.arraycopy(aptRoots, 0, result, 1, aptRoots.length);
                }
            }
            return result;
        }

        private static FileObject[] resolveAptSourceCache(FileObject sourceRoot) {
            try {
                FileObject[] arrfileObject;
                AnnotationProcessingQuery.Result result = AnnotationProcessingQuery.getAnnotationProcessingOptions((FileObject)sourceRoot);
                URL annotationOutputURL = result.sourceOutputDirectory();
                FileObject userAnnotationOutput = annotationOutputURL == null ? null : URLMapper.findFileObject((URL)annotationOutputURL);
                FileObject cacheAnnoationOutput = FileUtil.toFileObject((File)JavaIndex.getAptFolder(sourceRoot.getURL(), false));
                if (userAnnotationOutput == null) {
                    if (cacheAnnoationOutput == null) {
                        arrfileObject = new FileObject[]{};
                    } else {
                        FileObject[] arrfileObject2 = new FileObject[1];
                        arrfileObject = arrfileObject2;
                        arrfileObject2[0] = cacheAnnoationOutput;
                    }
                } else if (cacheAnnoationOutput == null) {
                    FileObject[] arrfileObject3 = new FileObject[1];
                    arrfileObject = arrfileObject3;
                    arrfileObject3[0] = userAnnotationOutput;
                } else {
                    FileObject[] arrfileObject4 = new FileObject[2];
                    arrfileObject4[0] = userAnnotationOutput;
                    arrfileObject = arrfileObject4;
                    arrfileObject4[1] = cacheAnnoationOutput;
                }
                return arrfileObject;
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
                return null;
            }
        }
    }

}

