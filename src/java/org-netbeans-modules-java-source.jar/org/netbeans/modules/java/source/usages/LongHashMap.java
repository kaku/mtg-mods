/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.java.source.usages.LongHashMap.HashIterator
 */
package org.netbeans.modules.java.source.usages;

import java.util.AbstractSet;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

class LongHashMap<K> {
    public static final long NO_VALUE = Long.MIN_VALUE;
    static final int DEFAULT_INITIAL_CAPACITY = 16;
    static final int MAXIMUM_CAPACITY = 1073741824;
    static final float DEFAULT_LOAD_FACTOR = 0.75f;
    transient Entry<K>[] table;
    transient int size;
    int threshold;
    final float loadFactor;
    volatile transient int modCount;
    static final Object NULL_KEY = new Object();
    private transient Set<Entry<K>> entrySet = null;
    volatile transient Set<K> keySet = null;

    public LongHashMap(int initialCapacity, float loadFactor) {
        int capacity;
        if (initialCapacity < 0) {
            throw new IllegalArgumentException("Illegal initial capacity: " + initialCapacity);
        }
        if (initialCapacity > 1073741824) {
            initialCapacity = 1073741824;
        }
        if (loadFactor <= 0.0f || Float.isNaN(loadFactor)) {
            throw new IllegalArgumentException("Illegal load factor: " + loadFactor);
        }
        for (capacity = 1; capacity < initialCapacity; capacity <<= 1) {
        }
        this.loadFactor = loadFactor;
        this.threshold = (int)((float)capacity * loadFactor);
        Entry[] ar = new Entry[capacity];
        this.table = ar;
        this.init();
    }

    public LongHashMap(int initialCapacity) {
        this(initialCapacity, 0.75f);
    }

    public LongHashMap() {
        this.loadFactor = 0.75f;
        this.threshold = 12;
        Entry[] ar = new Entry[16];
        this.table = ar;
        this.init();
    }

    void init() {
    }

    static <T> Object maskNull(T key) {
        return key == null ? NULL_KEY : key;
    }

    static <T> T unmaskNull(T key) {
        return key == NULL_KEY ? null : (T)key;
    }

    static boolean eq(Object x, Object y) {
        return x == y || x.equals(y);
    }

    static int indexFor(int h, int length) {
        return h & length - 1;
    }

    public int size() {
        return this.size;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public long get(Object key) {
        if (key == null) {
            return this.getForNullKey();
        }
        int hash = key.hashCode();
        Entry<K> e = this.table[LongHashMap.indexFor(hash, this.table.length)];
        while (e != null) {
            Object k;
            if (e.key.hashCode() == hash && ((k = e.key) == key || key.equals(k))) {
                return e.value;
            }
            e = e.next;
        }
        return Long.MIN_VALUE;
    }

    private long getForNullKey() {
        int hash = NULL_KEY.hashCode();
        int i = LongHashMap.indexFor(hash, this.table.length);
        Entry<K> e = this.table[i];
        while (e != null) {
            if (e.key == NULL_KEY) {
                return e.value;
            }
            e = e.next;
        }
        return Long.MIN_VALUE;
    }

    public boolean containsKey(Object key) {
        Object k = LongHashMap.maskNull(key);
        int hash = k.hashCode();
        int i = LongHashMap.indexFor(hash, this.table.length);
        Entry<K> e = this.table[i];
        while (e != null) {
            if (e.key.hashCode() == hash && LongHashMap.eq(k, e.key)) {
                return true;
            }
            e = e.next;
        }
        return false;
    }

    public Entry<K> getEntry(Object key) {
        Object k = LongHashMap.maskNull(key);
        int hash = k.hashCode();
        int i = LongHashMap.indexFor(hash, this.table.length);
        Entry<K> e = this.table[i];
        while (!(e == null || e.key.hashCode() == hash && LongHashMap.eq(k, e.key))) {
            e = e.next;
        }
        return e;
    }

    public long put(K key, long value) {
        if (key == null) {
            return this.putForNullKey(value);
        }
        int hash = key.hashCode();
        int i = LongHashMap.indexFor(hash, this.table.length);
        Entry<K> e = this.table[i];
        while (e != null) {
            Object k;
            if (e.key.hashCode() == hash && ((k = e.key) == key || key.equals(k))) {
                long oldValue = e.value;
                e.value = value;
                e.recordAccess(this);
                return oldValue;
            }
            e = e.next;
        }
        ++this.modCount;
        this.addEntry(key, value, i);
        return Long.MIN_VALUE;
    }

    private long putForNullKey(long value) {
        int hash = NULL_KEY.hashCode();
        int i = LongHashMap.indexFor(hash, this.table.length);
        Entry<K> e = this.table[i];
        while (e != null) {
            if (e.key == NULL_KEY) {
                long oldValue = e.value;
                e.value = value;
                e.recordAccess(this);
                return oldValue;
            }
            e = e.next;
        }
        ++this.modCount;
        Object nullKey = NULL_KEY;
        this.addEntry(nullKey, value, i);
        return Long.MIN_VALUE;
    }

    void resize(int newCapacity) {
        Entry<K>[] oldTable = this.table;
        int oldCapacity = oldTable.length;
        if (oldCapacity == 1073741824) {
            this.threshold = Integer.MAX_VALUE;
            return;
        }
        Entry[] newTable = new Entry[newCapacity];
        this.transfer(newTable);
        this.table = newTable;
        this.threshold = (int)((float)newCapacity * this.loadFactor);
    }

    void transfer(Entry<K>[] newTable) {
        Entry<K>[] src = this.table;
        int newCapacity = newTable.length;
        for (int j = 0; j < src.length; ++j) {
            Entry next;
            Entry<K> e = src[j];
            if (e == null) continue;
            src[j] = null;
            do {
                next = e.next;
                int i = LongHashMap.indexFor(e.key.hashCode(), newCapacity);
                e.next = newTable[i];
                newTable[i] = e;
            } while ((e = next) != null);
        }
    }

    public long remove(Object key) {
        Entry<K> e = this.removeEntryForKey(key);
        return e == null ? Long.MIN_VALUE : e.value;
    }

    Entry<K> removeEntryForKey(Object key) {
        Entry<K> prev;
        Object k = LongHashMap.maskNull(key);
        int hash = k.hashCode();
        int i = LongHashMap.indexFor(hash, this.table.length);
        Entry<K> e = prev = this.table[i];
        while (e != null) {
            Entry next = e.next;
            if (e.key.hashCode() == hash && LongHashMap.eq(k, e.key)) {
                ++this.modCount;
                --this.size;
                if (prev == e) {
                    this.table[i] = next;
                } else {
                    prev.next = next;
                }
                e.recordRemoval(this);
                return e;
            }
            prev = e;
            e = next;
        }
        return e;
    }

    Entry<K> removeMapping(Object o) {
        Entry<K> prev;
        if (!(o instanceof Map.Entry)) {
            return null;
        }
        Entry entry = (Entry)o;
        Object k = LongHashMap.maskNull(entry.getKey());
        int hash = k.hashCode();
        int i = LongHashMap.indexFor(hash, this.table.length);
        Entry<K> e = prev = this.table[i];
        while (e != null) {
            Entry next = e.next;
            if (e.key.hashCode() == hash && e.equals(entry)) {
                ++this.modCount;
                --this.size;
                if (prev == e) {
                    this.table[i] = next;
                } else {
                    prev.next = next;
                }
                e.recordRemoval(this);
                return e;
            }
            prev = e;
            e = next;
        }
        return e;
    }

    public void clear() {
        ++this.modCount;
        Entry<K>[] tab = this.table;
        for (int i = 0; i < tab.length; ++i) {
            tab[i] = null;
        }
        this.size = 0;
    }

    public boolean containsValue(Object value) {
        if (value == null) {
            return this.containsNullValue();
        }
        Entry<K>[] tab = this.table;
        for (int i = 0; i < tab.length; ++i) {
            Entry<K> e = tab[i];
            while (e != null) {
                if (value.equals(e.value)) {
                    return true;
                }
                e = e.next;
            }
        }
        return false;
    }

    private boolean containsNullValue() {
        Entry<K>[] tab = this.table;
        for (int i = 0; i < tab.length; ++i) {
            Entry<K> e = tab[i];
            while (e != null) {
                if (e.value == Long.MIN_VALUE) {
                    return true;
                }
                e = e.next;
            }
        }
        return false;
    }

    void addEntry(K key, long value, int bucketIndex) {
        Entry<K> e = this.table[bucketIndex];
        this.table[bucketIndex] = new Entry<K>(key, value, e);
        if (this.size++ >= this.threshold) {
            this.resize(2 * this.table.length);
        }
    }

    Iterator<K> newKeyIterator() {
        return new KeyIterator();
    }

    Iterator<Entry<K>> newEntryIterator() {
        return new EntryIterator();
    }

    public Set<K> keySet() {
        KeySet ks = this.keySet;
        KeySet keySet = ks != null ? ks : (this.keySet = new KeySet());
        return keySet;
    }

    public Set<Entry<K>> entrySet() {
        EntrySet es = this.entrySet;
        EntrySet entrySet = es != null ? es : (this.entrySet = new EntrySet());
        return entrySet;
    }

    int capacity() {
        return this.table.length;
    }

    float loadFactor() {
        return this.loadFactor;
    }

    private class EntrySet
    extends AbstractSet<Entry<K>> {
        private EntrySet() {
        }

        @Override
        public Iterator<Entry<K>> iterator() {
            return LongHashMap.this.newEntryIterator();
        }

        @Override
        public boolean contains(Object o) {
            if (!(o instanceof Map.Entry)) {
                return false;
            }
            Entry e = (Entry)o;
            Entry candidate = LongHashMap.this.getEntry(e.getKey());
            return candidate != null && candidate.equals(e);
        }

        @Override
        public boolean remove(Object o) {
            return LongHashMap.this.removeMapping(o) != null;
        }

        @Override
        public int size() {
            return LongHashMap.this.size;
        }

        @Override
        public void clear() {
            LongHashMap.this.clear();
        }
    }

    private class KeySet
    extends AbstractSet<K> {
        private KeySet() {
        }

        @Override
        public Iterator<K> iterator() {
            return LongHashMap.this.newKeyIterator();
        }

        @Override
        public int size() {
            return LongHashMap.this.size;
        }

        @Override
        public boolean contains(Object o) {
            return LongHashMap.this.containsKey(o);
        }

        @Override
        public boolean remove(Object o) {
            return LongHashMap.this.removeEntryForKey(o) != null;
        }

        @Override
        public void clear() {
            LongHashMap.this.clear();
        }
    }

    private class EntryIterator
    extends org.netbeans.modules.java.source.usages.LongHashMap.HashIterator<Entry<K>> {
        private EntryIterator() {
            super();
        }

        public Entry<K> next() {
            return this.nextEntry();
        }
    }

    private class KeyIterator
    extends LongHashMap<K> {
        private KeyIterator() {
            super();
        }

        public K next() {
            return this.nextEntry().getKey();
        }
    }

    private abstract class HashIterator<E>
    implements Iterator<E> {
        Entry<K> next;
        int expectedModCount;
        int index;
        Entry<K> current;

        HashIterator() {
            this.expectedModCount = LongHashMap.this.modCount;
            Entry<K>[] t = LongHashMap.this.table;
            int i = t.length;
            Entry n = null;
            if (LongHashMap.this.size != 0) {
                while (i > 0 && (n = t[--i]) == null) {
                }
            }
            this.next = n;
            this.index = i;
        }

        @Override
        public boolean hasNext() {
            return this.next != null;
        }

        Entry<K> nextEntry() {
            if (LongHashMap.this.modCount != this.expectedModCount) {
                throw new ConcurrentModificationException();
            }
            Entry<K> e = this.next;
            if (e == null) {
                throw new NoSuchElementException();
            }
            Entry n = e.next;
            Entry<K>[] t = LongHashMap.this.table;
            int i = this.index;
            while (n == null && i > 0) {
                n = t[--i];
            }
            this.index = i;
            this.next = n;
            this.current = e;
            return this.current;
        }

        @Override
        public void remove() {
            if (this.current == null) {
                throw new IllegalStateException();
            }
            if (LongHashMap.this.modCount != this.expectedModCount) {
                throw new ConcurrentModificationException();
            }
            Object k = this.current.key;
            this.current = null;
            LongHashMap.this.removeEntryForKey(k);
            this.expectedModCount = LongHashMap.this.modCount;
        }
    }

    public static class Entry<K> {
        final K key;
        long value;
        Entry<K> next;

        Entry(K k, long v, Entry<K> n) {
            this.value = v;
            this.next = n;
            this.key = k;
        }

        public K getKey() {
            return LongHashMap.unmaskNull(this.key);
        }

        public long getValue() {
            return this.value;
        }

        public long setValue(long newValue) {
            long oldValue = this.value;
            this.value = newValue;
            return oldValue;
        }

        public boolean equals(Object o) {
            Object k2;
            Long v1;
            Object v2;
            if (!(o instanceof Map.Entry)) {
                return false;
            }
            Map.Entry e = (Map.Entry)o;
            K k1 = this.getKey();
            if ((k1 == (k2 = e.getKey()) || k1 != null && k1.equals(k2)) && ((v1 = Long.valueOf(this.getValue())) == (v2 = e.getValue()) || v1 != null && v1.equals(v2))) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return (int)(this.value ^ this.value >>> 32);
        }

        public String toString() {
            return this.getKey() + "=" + this.getValue();
        }

        void recordAccess(LongHashMap<K> m) {
        }

        void recordRemoval(LongHashMap<K> m) {
        }
    }

}

