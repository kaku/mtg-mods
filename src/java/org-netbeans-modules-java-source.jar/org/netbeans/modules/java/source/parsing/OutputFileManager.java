/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.Pair
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.source.parsing;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.source.classpath.AptCacheForSourceQuery;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.parsing.CachingArchiveProvider;
import org.netbeans.modules.java.source.parsing.CachingFileManager;
import org.netbeans.modules.java.source.parsing.FileManagerTransaction;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.SiblingProvider;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Pair;
import org.openide.util.Utilities;

public class OutputFileManager
extends CachingFileManager {
    private static final ClassPath EMPTY_PATH = ClassPathSupport.createClassPath((URL[])new URL[0]);
    private static final String OUTPUT_ROOT = "output-root";
    private static final Logger LOG = Logger.getLogger(OutputFileManager.class.getName());
    private ClassPath scp;
    private ClassPath apt;
    private String outputRoot;
    private Pair<URI, File> cachedClassFolder;
    private final SiblingProvider siblings;
    private final FileManagerTransaction tx;

    public OutputFileManager(@NonNull CachingArchiveProvider provider, @NonNull ClassPath outputClassPath, @NonNull ClassPath sourcePath, @NullAllowed ClassPath aptPath, @NonNull SiblingProvider siblings, @NonNull FileManagerTransaction tx) {
        super(provider, outputClassPath, false, true);
        assert (outputClassPath != null);
        assert (sourcePath != null);
        assert (siblings != null);
        assert (tx != null);
        this.scp = sourcePath;
        this.apt = aptPath == null ? EMPTY_PATH : aptPath;
        this.siblings = siblings;
        this.tx = tx;
    }

    @Override
    public Iterable<JavaFileObject> list(JavaFileManager.Location l, String packageName, Set<JavaFileObject.Kind> kinds, boolean recursive) {
        Iterable<JavaFileObject> sr = super.list(l, packageName, kinds, recursive);
        return this.tx.filter(l, packageName, sr);
    }

    @Override
    public JavaFileObject getJavaFileForOutput(JavaFileManager.Location l, String className, JavaFileObject.Kind kind, javax.tools.FileObject sibling) throws IOException, UnsupportedOperationException, IllegalArgumentException {
        String baseName;
        if (kind != JavaFileObject.Kind.CLASS) {
            throw new IllegalArgumentException();
        }
        File activeRoot = null;
        if (this.outputRoot != null) {
            activeRoot = new File(this.outputRoot);
        } else {
            baseName = FileObjects.convertPackage2Folder(className);
            activeRoot = this.getClassFolderForSource(sibling, baseName);
            if (activeRoot == null && (activeRoot = this.getClassFolderForApt(sibling, baseName)) == null) {
                if (this.scp.getRoots().length > 0) {
                    LOG.log(Level.WARNING, "No output for class: {0} sibling: {1} srcRoots: {2}", new Object[]{className, sibling, this.scp});
                }
                throw new InvalidSourcePath();
            }
        }
        baseName = className.replace('.', File.separatorChar);
        String nameStr = baseName + '.' + "sig";
        File f = new File(activeRoot, nameStr);
        if (OutputFileManager.isValidClassName(className)) {
            return this.tx.createFileObject(l, f, activeRoot, null, null);
        }
        LOG.log(Level.WARNING, "Invalid class name: {0} sibling: {1}", new Object[]{className, sibling});
        return FileObjects.nullWriteFileObject(FileObjects.fileFileObject(f, activeRoot, null, null));
    }

    @Override
    public javax.tools.FileObject getFileForOutput(JavaFileManager.Location l, String pkgName, String relativeName, javax.tools.FileObject sibling) throws IOException, UnsupportedOperationException, IllegalArgumentException {
        URL siblingURL;
        assert (pkgName != null);
        assert (relativeName != null);
        URL uRL = this.siblings.hasSibling() ? this.siblings.getSibling() : (siblingURL = sibling == null ? null : sibling.toUri().toURL());
        if (siblingURL == null) {
            throw new IllegalArgumentException("sibling == null");
        }
        File activeRoot = this.getClassFolderForSourceImpl(siblingURL);
        if (activeRoot == null && (activeRoot = this.getClassFolderForApt(siblingURL)) == null) {
            throw new InvalidSourcePath();
        }
        if (File.separatorChar != '/') {
            relativeName = relativeName.replace('/', File.separatorChar);
        }
        StringBuilder path = new StringBuilder();
        if (pkgName.length() > 0) {
            path.append(FileObjects.convertPackage2Folder(pkgName, File.separatorChar));
            path.append(File.separatorChar);
        }
        path.append(relativeName);
        File file = FileUtil.normalizeFile((File)new File(activeRoot, path.toString()));
        return this.tx.createFileObject(l, file, activeRoot, null, null);
    }

    @Override
    public javax.tools.FileObject getFileForInput(JavaFileManager.Location l, String pkgName, String relativeName) {
        JavaFileObject fo = this.tx.readFileObject(l, pkgName, relativeName);
        if (fo != null) {
            return fo;
        }
        return super.getFileForInput(l, pkgName, relativeName);
    }

    @Override
    public JavaFileObject getJavaFileForInput(JavaFileManager.Location l, String className, JavaFileObject.Kind kind) {
        JavaFileObject fo;
        int dot;
        String dir;
        if (kind == JavaFileObject.Kind.CLASS && (fo = this.tx.readFileObject(l, dir = (dot = className.lastIndexOf(46)) == -1 ? "" : FileObjects.convertPackage2Folder(className.substring(0, dot)), className.substring(dot + 1))) != null) {
            return fo;
        }
        return super.getJavaFileForInput(l, className, kind);
    }

    private static boolean isValidClassName(@NonNull String fqn) {
        int ld = fqn.lastIndexOf(46);
        return fqn.indexOf(60, ld) < 0;
    }

    private File getClassFolderForSource(javax.tools.FileObject sibling, String baseName) throws IOException {
        return sibling == null ? this.getClassFolderForSourceImpl(baseName) : this.getClassFolderForSourceImpl(sibling.toUri().toURL());
    }

    private File getClassFolderForSourceImpl(URL sibling) throws IOException {
        List entries = this.scp.entries();
        int eSize = entries.size();
        if (eSize == 1) {
            return this.getClassFolder(((ClassPath.Entry)entries.get(0)).getURL());
        }
        if (eSize == 0) {
            return null;
        }
        try {
            for (ClassPath.Entry entry : entries) {
                URL rootUrl = entry.getURL();
                if (!FileObjects.isParentOf(rootUrl, sibling)) continue;
                return this.getClassFolder(rootUrl);
            }
        }
        catch (IllegalArgumentException e) {
            String message = String.format("uri: %s", sibling.toString());
            throw (IllegalArgumentException)Exceptions.attachMessage((Throwable)e, (String)message);
        }
        return null;
    }

    private File getClassFolderForSourceImpl(String baseName) throws IOException {
        List entries = this.scp.entries();
        int eSize = entries.size();
        if (eSize == 1) {
            return this.getClassFolder(((ClassPath.Entry)entries.get(0)).getURL());
        }
        if (eSize == 0) {
            return null;
        }
        String[] parentName = this.splitParentName(baseName);
        for (ClassPath.Entry entry : entries) {
            FileObject parentFile;
            FileObject root = entry.getRoot();
            if (root == null || (parentFile = root.getFileObject(parentName[0])) == null || parentFile.getFileObject(parentName[1], "java") == null) continue;
            return this.getClassFolder(entry.getURL());
        }
        return null;
    }

    private File getClassFolderForApt(javax.tools.FileObject sibling, String baseName) throws IOException {
        return sibling == null ? this.getClassFolderForApt(baseName) : this.getClassFolderForApt(sibling.toUri().toURL());
    }

    private File getClassFolderForApt(@NonNull URL surl) {
        for (ClassPath.Entry entry : this.apt.entries()) {
            URL classFolder;
            if (!FileObjects.isParentOf(entry.getURL(), surl) || (classFolder = AptCacheForSourceQuery.getClassFolder(entry.getURL())) == null) continue;
            try {
                return Utilities.toFile((URI)classFolder.toURI());
            }
            catch (URISyntaxException ex) {
                Exceptions.printStackTrace((Throwable)ex);
                continue;
            }
        }
        return null;
    }

    private File getClassFolderForApt(String baseName) {
        String[] parentName = this.splitParentName(baseName);
        for (ClassPath.Entry entry : this.apt.entries()) {
            URL classFolder;
            FileObject parentFile;
            FileObject root = entry.getRoot();
            if (root == null || (parentFile = root.getFileObject(parentName[0])) == null || parentFile.getFileObject(parentName[1], "java") == null || (classFolder = AptCacheForSourceQuery.getClassFolder(entry.getURL())) == null) continue;
            try {
                return Utilities.toFile((URI)classFolder.toURI());
            }
            catch (URISyntaxException ex) {
                Exceptions.printStackTrace((Throwable)ex);
                continue;
            }
        }
        return null;
    }

    private String[] splitParentName(String baseName) {
        String name;
        String parent = null;
        int index = baseName.lastIndexOf(47);
        if (index < 0) {
            parent = "";
            name = baseName;
        } else {
            parent = baseName.substring(0, index);
            name = baseName.substring(index + 1);
        }
        index = name.indexOf(36);
        if (index > 0) {
            name = name.substring(0, index);
        }
        return new String[]{parent, name};
    }

    private File getClassFolder(URL url) throws IOException {
        Pair<URI, File> cacheItem = this.cachedClassFolder;
        URI uri = null;
        try {
            uri = url.toURI();
            if (cacheItem != null && uri.equals(cacheItem.first())) {
                return (File)cacheItem.second();
            }
        }
        catch (URISyntaxException e) {
            LOG.log(Level.FINE, "Not caching class folder for URL: {0}", url);
        }
        File result = JavaIndex.getClassFolder(url, false, false);
        assert (result != null);
        if (uri != null) {
            this.cachedClassFolder = Pair.of((Object)uri, (Object)result);
        }
        return result;
    }

    @Override
    public boolean handleOption(String head, Iterator<String> tail) {
        if ("output-root".equals(head)) {
            if (!tail.hasNext()) {
                throw new IllegalArgumentException();
            }
            this.outputRoot = tail.next();
            if (this.outputRoot.length() <= 0) {
                this.outputRoot = null;
            }
            return true;
        }
        return super.handleOption(head, tail);
    }

    public class InvalidSourcePath
    extends IllegalStateException {
    }

}

