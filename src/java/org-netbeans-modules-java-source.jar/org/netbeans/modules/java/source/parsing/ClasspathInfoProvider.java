/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.source.parsing;

import org.netbeans.api.java.source.ClasspathInfo;

public interface ClasspathInfoProvider {
    public ClasspathInfo getClasspathInfo();
}

