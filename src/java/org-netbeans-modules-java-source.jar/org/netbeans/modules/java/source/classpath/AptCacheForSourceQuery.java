/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Exceptions
 *  org.openide.util.Parameters
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.source.classpath;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.openide.util.Exceptions;
import org.openide.util.Parameters;
import org.openide.util.Utilities;

public final class AptCacheForSourceQuery {
    private static Map<URI, URL> emittedAptFolders = new ConcurrentHashMap<URI, URL>();

    private AptCacheForSourceQuery() {
    }

    public static URL getAptFolder(@NonNull URL sourceRoot) {
        Parameters.notNull((CharSequence)"sourceRoot", (Object)sourceRoot);
        return AptCacheForSourceQuery.getDefaultAptFolder(sourceRoot);
    }

    public static URL getSourceFolder(@NonNull URL aptFolder) {
        Parameters.notNull((CharSequence)"aptFolder", (Object)aptFolder);
        return AptCacheForSourceQuery.getDefaultSourceFolder(aptFolder);
    }

    public static URL getClassFolder(@NonNull URL aptFolder) {
        Parameters.notNull((CharSequence)"aptFolder", (Object)aptFolder);
        return AptCacheForSourceQuery.getDefaultCacheFolder(aptFolder);
    }

    private static URL getDefaultAptFolder(URL sourceRoot) {
        try {
            URI sourceRootURI = AptCacheForSourceQuery.toURI(sourceRoot);
            if (sourceRootURI == null) {
                return null;
            }
            if (emittedAptFolders.containsKey(sourceRootURI)) {
                return sourceRoot;
            }
            File aptFolder = JavaIndex.getAptFolder(sourceRoot, true);
            URI uriResult = Utilities.toURI((File)aptFolder);
            URL result = uriResult.toURL();
            emittedAptFolders.put(uriResult, sourceRoot);
            return result;
        }
        catch (MalformedURLException e) {
            Exceptions.printStackTrace((Throwable)e);
            return null;
        }
        catch (IOException e) {
            Exceptions.printStackTrace((Throwable)e);
            return null;
        }
    }

    private static URL getDefaultSourceFolder(URL aptFolder) {
        URI uri = AptCacheForSourceQuery.toURI(aptFolder);
        return uri == null ? null : emittedAptFolders.get(uri);
    }

    private static URL getDefaultCacheFolder(URL aptFolder) {
        URL sourceRoot = AptCacheForSourceQuery.getDefaultSourceFolder(aptFolder);
        if (sourceRoot != null) {
            try {
                File result = JavaIndex.getClassFolder(sourceRoot);
                return Utilities.toURI((File)result).toURL();
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return null;
    }

    @CheckForNull
    private static URI toURI(@NonNull URL url) {
        try {
            return url.toURI();
        }
        catch (URISyntaxException e) {
            Exceptions.printStackTrace((Throwable)e);
            return null;
        }
    }
}

