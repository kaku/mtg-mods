/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.api.ClassNamesForFileOraculum
 */
package org.netbeans.modules.java.source.usages;

import com.sun.tools.javac.api.ClassNamesForFileOraculum;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.tools.JavaFileObject;

public class ClassNamesForFileOraculumImpl
implements ClassNamesForFileOraculum {
    private final Map<JavaFileObject, List<String>> misplacedSource2FQNs;

    public ClassNamesForFileOraculumImpl(Map<JavaFileObject, List<String>> misplacedSource2FQNs) {
        this.misplacedSource2FQNs = misplacedSource2FQNs;
    }

    public String[] divineClassName(JavaFileObject jfo) {
        if (this.misplacedSource2FQNs.isEmpty()) {
            return null;
        }
        List<String> result = this.misplacedSource2FQNs.get(jfo);
        if (result != null) {
            return result.toArray(new String[result.size()]);
        }
        return null;
    }

    public JavaFileObject[] divineSources(String fqn) {
        if (fqn == null || fqn.length() == 0 || this.misplacedSource2FQNs.isEmpty()) {
            return null;
        }
        fqn = fqn + ".";
        LinkedList<JavaFileObject> jfos = new LinkedList<JavaFileObject>();
        block0 : for (Map.Entry<JavaFileObject, List<String>> entry : this.misplacedSource2FQNs.entrySet()) {
            for (String s : entry.getValue()) {
                if (!s.startsWith(fqn) || s.indexOf(46, fqn.length()) != -1) continue;
                jfos.add(entry.getKey());
                continue block0;
            }
        }
        return jfos.size() > 0 ? jfos.toArray(new JavaFileObject[jfos.size()]) : null;
    }
}

