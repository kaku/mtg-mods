/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.source.parsing;

import javax.tools.JavaFileObject;

public interface InferableJavaFileObject
extends JavaFileObject {
    public String inferBinaryName();
}

