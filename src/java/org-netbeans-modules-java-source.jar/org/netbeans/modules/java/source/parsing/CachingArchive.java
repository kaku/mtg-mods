/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.java.source.parsing;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.tools.JavaFileObject;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.source.parsing.Archive;
import org.netbeans.modules.java.source.parsing.FastJar;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Parameters;

public class CachingArchive
implements Archive,
FileChangeListener {
    private static final Logger LOGGER = Logger.getLogger(CachingArchive.class.getName());
    private final File archiveFile;
    private final boolean keepOpened;
    private final String pathToRootInArchive;
    private ZipFile zipFile;
    byte[] names;
    private int nameOffset = 0;
    static final int[] EMPTY = new int[0];
    private Map<String, Folder> folders;

    public CachingArchive(@NonNull File archiveFile, boolean keepOpened) {
        this(archiveFile, null, keepOpened);
    }

    public CachingArchive(@NonNull File archiveFile, @NullAllowed String pathToRootInArchive, boolean keepOpened) {
        Parameters.notNull((CharSequence)"archiveFile", (Object)archiveFile);
        if (pathToRootInArchive != null) {
            if (!keepOpened) {
                throw new UnsupportedOperationException(String.format("FastJar not supported for relocated root of archive %s, relocation %s", archiveFile.getAbsolutePath(), pathToRootInArchive));
            }
            if (pathToRootInArchive.charAt(pathToRootInArchive.length() - 1) != '/') {
                throw new IllegalArgumentException(String.format("Path to root: %s has to end with /", pathToRootInArchive));
            }
        }
        this.archiveFile = archiveFile;
        this.pathToRootInArchive = pathToRootInArchive;
        this.keepOpened = keepOpened;
        FileUtil.addFileChangeListener((FileChangeListener)this, (File)FileUtil.normalizeFile((File)archiveFile));
    }

    @Override
    public Iterable<JavaFileObject> getFiles(String folderName, ClassPath.Entry entry, Set<JavaFileObject.Kind> kinds, JavaFileFilterImplementation filter) throws IOException {
        Map<String, Folder> folders = this.doInit();
        Folder files = folders.get(folderName);
        if (files == null) {
            return Collections.emptyList();
        }
        assert (!this.keepOpened || this.zipFile != null);
        ArrayList<JavaFileObject> l = new ArrayList<JavaFileObject>(files.idx / files.delta);
        Predicate predicate = kinds == null ? new Tautology() : new HasKind(kinds);
        for (int i = 0; i < files.idx; i += files.delta) {
            JavaFileObject fo = this.create(folderName, files, i, predicate);
            if (fo == null) continue;
            l.add(fo);
        }
        return l;
    }

    @Override
    public JavaFileObject create(String relativePath, JavaFileFilterImplementation filter) {
        throw new UnsupportedOperationException("Write into archives not supported");
    }

    @Override
    public synchronized void clear() {
        this.folders = null;
        this.names = null;
        this.nameOffset = 0;
    }

    @Override
    public JavaFileObject getFile(@NonNull String name) {
        String folder;
        String sn;
        Map<String, Folder> folders = this.doInit();
        int index = name.lastIndexOf(47);
        if (index <= 0) {
            folder = "";
            sn = name;
        } else {
            folder = name.substring(0, index);
            sn = name.substring(index + 1);
        }
        Folder files = folders.get(folder);
        if (files == null) {
            return null;
        }
        assert (!this.keepOpened || this.zipFile != null);
        NameIs predicate = new NameIs(sn);
        for (int i = 0; i < files.idx; i += files.delta) {
            JavaFileObject fo = this.create(folder, files, i, predicate);
            if (fo == null) continue;
            return fo;
        }
        return null;
    }

    public String toString() {
        return String.format("%s[archive: %s]", this.getClass().getSimpleName(), this.archiveFile.getAbsolutePath());
    }

    protected void beforeInit() throws IOException {
    }

    protected short getFlags(@NonNull String dirname) throws IOException {
        return 0;
    }

    protected void afterInit(boolean success) throws IOException {
    }

    protected ZipFile getArchive(short flags) {
        return this.zipFile;
    }

    protected String getPathToRoot(short flags) {
        return this.pathToRootInArchive;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    synchronized Map<String, Folder> doInit() {
        block8 : {
            if (this.folders == null) {
                try {
                    boolean success = false;
                    this.beforeInit();
                    try {
                        this.names = new byte[16384];
                        this.folders = this.createMap(this.archiveFile);
                        this.trunc();
                        success = true;
                    }
                    finally {
                        this.afterInit(success);
                    }
                }
                catch (IOException e) {
                    LOGGER.log(Level.WARNING, "Broken zip file: {0}", this.archiveFile.getAbsolutePath());
                    LOGGER.log(Level.FINE, null, e);
                    this.names = new byte[0];
                    this.nameOffset = 0;
                    this.folders = new HashMap<String, Folder>();
                    if (this.zipFile == null) break block8;
                    try {
                        this.zipFile.close();
                    }
                    catch (IOException ex) {
                        LOGGER.log(Level.WARNING, "Cannot close archive: {0}", this.archiveFile.getAbsolutePath());
                        LOGGER.log(Level.FINE, null, ex);
                    }
                }
            }
        }
        return this.folders;
    }

    private void trunc() {
        assert (Thread.holdsLock(this));
        byte[] newNames = new byte[this.nameOffset];
        System.arraycopy(this.names, 0, newNames, 0, this.nameOffset);
        this.names = newNames;
        Iterator<Folder> it = this.folders.values().iterator();
        while (it.hasNext()) {
            it.next().trunc();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Map<String, Folder> createMap(File file) throws IOException {
        Map map;
        String name;
        if (!file.canRead()) {
            return Collections.emptyMap();
        }
        map = null;
        if (!this.keepOpened) {
            map = new HashMap<String, Folder>();
            try {
                Iterable<? extends FastJar.Entry> e = FastJar.list(file);
                for (FastJar.Entry entry : e) {
                    Folder fld;
                    name = entry.name;
                    int i = name.lastIndexOf(47);
                    String dirname = i == -1 ? "" : name.substring(0, i);
                    String basename = name.substring(i + 1);
                    if (basename.length() == 0) {
                        basename = null;
                    }
                    if ((fld = (Folder)map.get(dirname)) == null) {
                        fld = new Folder(true, this.getFlags(dirname));
                        map.put(new String(dirname).intern(), fld);
                    }
                    if (basename == null) continue;
                    fld.appendEntry(this, basename, entry.getTime(), entry.offset);
                }
            }
            catch (IOException ioe) {
                map = null;
                Logger.getLogger(CachingArchive.class.getName()).log(Level.WARNING, "Fallback to ZipFile: {0}", file.getPath());
            }
        }
        if (map == null) {
            map = new HashMap();
            ZipFile zip = new ZipFile(file);
            try {
                Enumeration<? extends ZipEntry> e = zip.entries();
                while (e.hasMoreElements()) {
                    String dirname;
                    int i;
                    Folder fld;
                    ZipEntry entry;
                    String basename;
                    try {
                        entry = e.nextElement();
                    }
                    catch (IllegalArgumentException iae) {
                        throw new IOException(iae);
                    }
                    name = entry.getName();
                    if (this.pathToRootInArchive != null) {
                        if (!name.startsWith(this.pathToRootInArchive)) continue;
                        i = name.lastIndexOf(47);
                        dirname = i < this.pathToRootInArchive.length() ? "" : name.substring(this.pathToRootInArchive.length(), i);
                        basename = name.substring(i + 1);
                    } else {
                        i = name.lastIndexOf(47);
                        dirname = i == -1 ? "" : name.substring(0, i);
                        basename = name.substring(i + 1);
                    }
                    if (basename.length() == 0) {
                        basename = null;
                    }
                    if ((fld = (Folder)map.get(dirname)) == null) {
                        fld = new Folder(false, this.getFlags(dirname));
                        map.put(new String(dirname).intern(), fld);
                    }
                    if (basename == null) continue;
                    fld.appendEntry(this, basename, entry.getTime(), -1);
                }
            }
            finally {
                if (this.keepOpened) {
                    this.zipFile = zip;
                } else {
                    try {
                        zip.close();
                    }
                    catch (IOException ioe) {
                        Exceptions.printStackTrace((Throwable)ioe);
                    }
                }
            }
        }
        return map;
    }

    private synchronized String getString(int off, int len) {
        if (this.names == null) {
            return null;
        }
        byte[] name = new byte[len];
        System.arraycopy(this.names, off, name, 0, len);
        try {
            return new String(name, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            throw new InternalError("No UTF-8");
        }
    }

    static long join(int higher, int lower) {
        return (long)higher << 32 | (long)lower & 0xFFFFFFFFL;
    }

    private JavaFileObject create(@NonNull String pkg, @NonNull Folder f, @NonNull int off, @NonNull Predicate<String> predicate) {
        String baseName = this.getString(f.indices[off], f.indices[off + 1]);
        if (baseName != null && predicate.apply(baseName)) {
            long mtime = CachingArchive.join(f.indices[off + 3], f.indices[off + 2]);
            if (this.zipFile == null) {
                if (f.delta == 4) {
                    return FileObjects.zipFileObject(this.archiveFile, pkg, baseName, mtime);
                }
                assert (f.delta == 6);
                long offset = CachingArchive.join(f.indices[off + 5], f.indices[off + 4]);
                return FileObjects.zipFileObject(this.archiveFile, pkg, baseName, mtime, offset);
            }
            return FileObjects.zipFileObject(this.getArchive(f.flags), this.getPathToRoot(f.flags), pkg, baseName, mtime);
        }
        return null;
    }

    synchronized int putName(byte[] name) {
        int start = this.nameOffset;
        if (start + name.length > this.names.length) {
            byte[] newNames = new byte[this.names.length * 2 + name.length];
            System.arraycopy(this.names, 0, newNames, 0, start);
            this.names = newNames;
        }
        System.arraycopy(name, 0, this.names, start, name.length);
        this.nameOffset += name.length;
        return start;
    }

    public void fileFolderCreated(FileEvent fe) {
    }

    public void fileDataCreated(FileEvent fe) {
        this.clear();
    }

    public void fileChanged(FileEvent fe) {
        this.clear();
    }

    public void fileDeleted(FileEvent fe) {
        this.clear();
    }

    public void fileRenamed(FileRenameEvent fe) {
        this.clear();
    }

    public void fileAttributeChanged(FileAttributeEvent fe) {
    }

    private static class Tautology
    implements Predicate<String> {
        private Tautology() {
        }

        @Override
        public boolean apply(@NonNull String value) {
            return true;
        }
    }

    private static class NameIs
    implements Predicate<String> {
        private final String name;

        private NameIs(@NonNull String name) {
            Parameters.notNull((CharSequence)"name", (Object)name);
            this.name = name;
        }

        @Override
        public boolean apply(@NonNull String value) {
            return this.name.equals(value);
        }
    }

    private static class HasKind
    implements Predicate<String> {
        private final Set<JavaFileObject.Kind> kinds;

        private HasKind(@NonNull Set<JavaFileObject.Kind> kinds) {
            Parameters.notNull((CharSequence)"kinds", kinds);
            this.kinds = kinds;
        }

        @Override
        public boolean apply(@NonNull String value) {
            return this.kinds.contains((Object)FileObjects.getKind(FileObjects.getExtension(value)));
        }
    }

    private static interface Predicate<T> {
        public boolean apply(@NonNull T var1);
    }

    private static class Folder {
        int[] indices = CachingArchive.EMPTY;
        int idx = 0;
        final short flags;
        final short delta;

        public Folder(boolean fastJar, short flags) {
            this.delta = fastJar ? 6 : 4;
            this.flags = flags;
        }

        void appendEntry(CachingArchive outer, String name, long mtime, long offset) {
            if (this.idx + this.delta > this.indices.length) {
                int[] newInd = new int[2 * this.indices.length + this.delta];
                System.arraycopy(this.indices, 0, newInd, 0, this.idx);
                this.indices = newInd;
            }
            try {
                byte[] bytes = name.getBytes("UTF-8");
                this.indices[this.idx++] = outer.putName(bytes);
                this.indices[this.idx++] = bytes.length;
                this.indices[this.idx++] = (int)(mtime & -1);
                this.indices[this.idx++] = (int)(mtime >> 32);
                if (this.delta == 6) {
                    this.indices[this.idx++] = (int)(offset & -1);
                    this.indices[this.idx++] = (int)(offset >> 32);
                }
            }
            catch (UnsupportedEncodingException e) {
                throw new InternalError("No UTF-8");
            }
        }

        void trunc() {
            if (this.indices.length > this.idx) {
                int[] newInd = new int[this.idx];
                System.arraycopy(this.indices, 0, newInd, 0, this.idx);
                this.indices = newInd;
            }
        }
    }

}

