/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.code.Symbol$MethodSymbol
 *  com.sun.tools.javac.code.Symbol$PackageSymbol
 *  com.sun.tools.javac.code.Symbol$VarSymbol
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCClassDecl
 *  com.sun.tools.javac.tree.JCTree$JCCompilationUnit
 *  com.sun.tools.javac.tree.JCTree$JCFieldAccess
 *  com.sun.tools.javac.tree.JCTree$JCIdent
 *  com.sun.tools.javac.tree.JCTree$JCMethodDecl
 *  com.sun.tools.javac.tree.JCTree$JCNewClass
 *  com.sun.tools.javac.tree.JCTree$JCVariableDecl
 *  com.sun.tools.javac.tree.JCTree$Tag
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Key
 */
package org.netbeans.modules.java.source.builder;

import com.sun.source.tree.Tree;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.Context;
import javax.lang.model.element.Element;
import javax.lang.model.type.TypeMirror;

public final class ASTService {
    private static final Context.Key<ASTService> treeKey = new Context.Key();

    public static synchronized ASTService instance(Context context) {
        ASTService instance = (ASTService)context.get(treeKey);
        if (instance == null) {
            instance = new ASTService(context);
        }
        return instance;
    }

    protected ASTService(Context context) {
        context.put(treeKey, (Object)this);
    }

    public Element getElement(Tree tree) {
        return ASTService.getElementImpl(tree);
    }

    public static Element getElementImpl(Tree tree) {
        if (tree == null) {
            return null;
        }
        switch (tree.getKind()) {
            case COMPILATION_UNIT: {
                return ((JCTree.JCCompilationUnit)tree).packge;
            }
            case ANNOTATION_TYPE: 
            case CLASS: 
            case ENUM: 
            case INTERFACE: {
                return ((JCTree.JCClassDecl)tree).sym;
            }
            case METHOD: {
                return ((JCTree.JCMethodDecl)tree).sym;
            }
            case VARIABLE: {
                return ((JCTree.JCVariableDecl)tree).sym;
            }
            case MEMBER_SELECT: {
                return ((JCTree.JCFieldAccess)tree).sym;
            }
            case IDENTIFIER: {
                return ((JCTree.JCIdent)tree).sym;
            }
            case NEW_CLASS: {
                return ((JCTree.JCNewClass)tree).constructor;
            }
        }
        return null;
    }

    public TypeMirror getType(Tree tree) {
        Element e;
        if (tree == null) {
            return null;
        }
        Object type = ((JCTree)tree).type;
        if (type == null && (e = this.getElement(tree)) != null) {
            type = e.asType();
        }
        return type;
    }

    public void setElement(Tree tree, Element element) {
        switch (((JCTree)tree).getTag()) {
            case TOPLEVEL: {
                ((JCTree.JCCompilationUnit)tree).packge = (Symbol.PackageSymbol)element;
                break;
            }
            case CLASSDEF: {
                ((JCTree.JCClassDecl)tree).sym = (Symbol.ClassSymbol)element;
                break;
            }
            case METHODDEF: {
                ((JCTree.JCMethodDecl)tree).sym = (Symbol.MethodSymbol)element;
                break;
            }
            case VARDEF: {
                ((JCTree.JCVariableDecl)tree).sym = (Symbol.VarSymbol)element;
                break;
            }
            case SELECT: {
                ((JCTree.JCFieldAccess)tree).sym = (Symbol)element;
                break;
            }
            case IDENT: {
                ((JCTree.JCIdent)tree).sym = (Symbol)element;
                break;
            }
            case NEWCLASS: {
                ((JCTree.JCNewClass)tree).constructor = (Symbol)element;
                break;
            }
            default: {
                throw new IllegalArgumentException("invalid tree type: " + (Object)tree.getKind());
            }
        }
    }

    public void setType(Tree tree, TypeMirror type) {
        ((JCTree)tree).type = (Type)type;
    }

    public int getPos(Tree tree) {
        if (tree == null) {
            return -1;
        }
        return ((JCTree)tree).pos;
    }

    public void setPos(Tree tree, int newPos) {
        ((JCTree)tree).pos = newPos;
    }

}

