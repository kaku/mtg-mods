/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.doctree.AttributeTree
 *  com.sun.source.doctree.AttributeTree$ValueKind
 *  com.sun.source.doctree.AuthorTree
 *  com.sun.source.doctree.CommentTree
 *  com.sun.source.doctree.DeprecatedTree
 *  com.sun.source.doctree.DocCommentTree
 *  com.sun.source.doctree.DocRootTree
 *  com.sun.source.doctree.DocTree
 *  com.sun.source.doctree.DocTree$Kind
 *  com.sun.source.doctree.DocTreeVisitor
 *  com.sun.source.doctree.EndElementTree
 *  com.sun.source.doctree.EntityTree
 *  com.sun.source.doctree.ErroneousTree
 *  com.sun.source.doctree.IdentifierTree
 *  com.sun.source.doctree.InheritDocTree
 *  com.sun.source.doctree.LinkTree
 *  com.sun.source.doctree.LiteralTree
 *  com.sun.source.doctree.ParamTree
 *  com.sun.source.doctree.ReferenceTree
 *  com.sun.source.doctree.ReturnTree
 *  com.sun.source.doctree.SeeTree
 *  com.sun.source.doctree.SerialDataTree
 *  com.sun.source.doctree.SerialFieldTree
 *  com.sun.source.doctree.SerialTree
 *  com.sun.source.doctree.SinceTree
 *  com.sun.source.doctree.StartElementTree
 *  com.sun.source.doctree.TextTree
 *  com.sun.source.doctree.ThrowsTree
 *  com.sun.source.doctree.UnknownBlockTagTree
 *  com.sun.source.doctree.UnknownInlineTagTree
 *  com.sun.source.doctree.ValueTree
 *  com.sun.source.doctree.VersionTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.Tree
 *  com.sun.tools.javac.tree.DCTree
 *  com.sun.tools.javac.tree.DCTree$DCReference
 *  com.sun.tools.javac.tree.DCTree$DCUnknownBlockTag
 *  com.sun.tools.javac.tree.DCTree$DCUnknownInlineTag
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.util.List
 *  com.sun.tools.javac.util.Name
 */
package org.netbeans.modules.java.source.transform;

import com.sun.source.doctree.AttributeTree;
import com.sun.source.doctree.AuthorTree;
import com.sun.source.doctree.CommentTree;
import com.sun.source.doctree.DeprecatedTree;
import com.sun.source.doctree.DocCommentTree;
import com.sun.source.doctree.DocRootTree;
import com.sun.source.doctree.DocTree;
import com.sun.source.doctree.DocTreeVisitor;
import com.sun.source.doctree.EndElementTree;
import com.sun.source.doctree.EntityTree;
import com.sun.source.doctree.ErroneousTree;
import com.sun.source.doctree.IdentifierTree;
import com.sun.source.doctree.InheritDocTree;
import com.sun.source.doctree.LinkTree;
import com.sun.source.doctree.LiteralTree;
import com.sun.source.doctree.ParamTree;
import com.sun.source.doctree.ReferenceTree;
import com.sun.source.doctree.ReturnTree;
import com.sun.source.doctree.SeeTree;
import com.sun.source.doctree.SerialDataTree;
import com.sun.source.doctree.SerialFieldTree;
import com.sun.source.doctree.SerialTree;
import com.sun.source.doctree.SinceTree;
import com.sun.source.doctree.StartElementTree;
import com.sun.source.doctree.TextTree;
import com.sun.source.doctree.ThrowsTree;
import com.sun.source.doctree.UnknownBlockTagTree;
import com.sun.source.doctree.UnknownInlineTagTree;
import com.sun.source.doctree.ValueTree;
import com.sun.source.doctree.VersionTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.Tree;
import com.sun.tools.javac.tree.DCTree;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;
import java.util.ArrayList;
import java.util.Map;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.modules.java.source.builder.TreeFactory;
import org.netbeans.modules.java.source.transform.ImmutableTreeTranslator;

public class ImmutableDocTreeTranslator
extends ImmutableTreeTranslator
implements DocTreeVisitor<DocTree, Object> {
    private Map<DocTree, Object> tree2Tag;

    public ImmutableDocTreeTranslator(WorkingCopy copy) {
        super(copy);
    }

    public DocTree translate(DocTree tree) {
        if (tree == null) {
            return null;
        }
        DocTree t = (DocTree)tree.accept((DocTreeVisitor)this, (Object)null);
        if (this.tree2Tag != null && tree != t) {
            this.tree2Tag.put(t, this.tree2Tag.get((Object)tree));
        }
        return t;
    }

    public <T extends DocTree> java.util.List<T> translateDoc(java.util.List<T> trees) {
        if (trees == null || trees.isEmpty()) {
            return trees;
        }
        ArrayList<DocTree> newTrees = new ArrayList<DocTree>();
        boolean changed = false;
        for (DocTree t : trees) {
            DocTree newT = this.translate(t);
            if (newT != t) {
                changed = true;
            }
            if (newT == null) continue;
            newTrees.add(newT);
        }
        return changed ? newTrees : trees;
    }

    protected final DocCommentTree rewriteChildren(DocCommentTree tree) {
        DocCommentTree value = tree;
        java.util.List<T> firstSentence = this.translateDoc(tree.getFirstSentence());
        java.util.List<T> body = this.translateDoc(tree.getBody());
        java.util.List<T> blockTags = this.translateDoc(tree.getBlockTags());
        if (firstSentence != tree.getFirstSentence() || body != tree.getBody() || blockTags != tree.getBlockTags()) {
            value = this.make.DocComment(firstSentence, body, blockTags);
        }
        return value;
    }

    protected final AttributeTree rewriteChildren(AttributeTree tree) {
        AttributeTree value = tree;
        java.util.List<T> vl = this.translateDoc(tree.getValue());
        if (vl != tree.getValue()) {
            value = this.make.Attribute((CharSequence)((Name)tree.getName()), tree.getValueKind(), vl);
        }
        return value;
    }

    protected final AuthorTree rewriteChildren(AuthorTree tree) {
        AuthorTree value = tree;
        java.util.List<T> name = this.translateDoc(tree.getName());
        if (name != tree.getName()) {
            value = this.make.Author(name);
        }
        return value;
    }

    protected final CommentTree rewriteChildren(CommentTree tree) {
        return tree;
    }

    protected final DeprecatedTree rewriteChildren(DeprecatedTree tree) {
        DeprecatedTree value = tree;
        java.util.List body = tree.getBody();
        if (body != tree.getBody()) {
            value = this.make.Deprecated(body);
        }
        return value;
    }

    protected final DocRootTree rewriteChildren(DocRootTree tree) {
        return tree;
    }

    protected final EndElementTree rewriteChildren(EndElementTree tree) {
        return tree;
    }

    protected final EntityTree rewriteChildren(EntityTree tree) {
        return tree;
    }

    protected final ErroneousTree rewriteChildren(ErroneousTree tree) {
        return tree;
    }

    protected final IdentifierTree rewriteChildren(IdentifierTree tree) {
        return tree;
    }

    protected final InheritDocTree rewriteChildren(InheritDocTree tree) {
        return tree;
    }

    protected final LinkTree rewriteChildren(LinkTree tree) {
        LinkTree value = tree;
        java.util.List<T> label = this.translateDoc(tree.getLabel());
        ReferenceTree ref = (ReferenceTree)this.translate((DocTree)tree.getReference());
        if (label != tree.getLabel() || ref != tree.getReference()) {
            value = this.make.Link(ref, label);
        }
        return value;
    }

    protected final LiteralTree rewriteChildren(LiteralTree tree) {
        LiteralTree value = tree;
        TextTree body = (TextTree)this.translate((DocTree)tree.getBody());
        if (body != tree.getBody()) {
            value = tree.getKind() == DocTree.Kind.CODE ? this.make.Code(body) : this.make.Literal(body);
        }
        return value;
    }

    protected final ParamTree rewriteChildren(ParamTree tree) {
        ParamTree value = tree;
        IdentifierTree name = (IdentifierTree)this.translate((DocTree)tree.getName());
        java.util.List<T> description = this.translateDoc(tree.getDescription());
        if (name != tree.getName() || description != tree.getDescription()) {
            value = this.make.Param(tree.isTypeParameter(), name, description);
        }
        return value;
    }

    protected final ReferenceTree rewriteChildren(ReferenceTree tree) {
        DCTree.DCReference refTree = (DCTree.DCReference)tree;
        ReferenceTree value = tree;
        ExpressionTree classReference = (ExpressionTree)this.translate((Tree)refTree.qualifierExpression);
        java.util.List methodParameters = this.translate(refTree.paramTypes);
        if (classReference != refTree.qualifierExpression || methodParameters != refTree.paramTypes) {
            value = this.make.Reference(classReference, (CharSequence)refTree.memberName, methodParameters);
        }
        return value;
    }

    protected final ReturnTree rewriteChildren(ReturnTree tree) {
        ReturnTree value = tree;
        java.util.List<T> description = this.translateDoc(tree.getDescription());
        if (description != tree.getDescription()) {
            value = this.make.Return(description);
        }
        return value;
    }

    protected final SeeTree rewriteChildren(SeeTree tree) {
        SeeTree value = tree;
        java.util.List<T> ref = this.translateDoc(tree.getReference());
        if (ref != tree.getReference()) {
            value = this.make.See(ref);
        }
        return value;
    }

    protected final SerialTree rewriteChildren(SerialTree tree) {
        SerialTree value = tree;
        java.util.List<T> desc = this.translateDoc(tree.getDescription());
        if (desc != tree.getDescription()) {
            value = this.make.Serial(desc);
        }
        return value;
    }

    protected final SerialDataTree rewriteChildren(SerialDataTree tree) {
        SerialDataTree value = tree;
        java.util.List<T> desc = this.translateDoc(tree.getDescription());
        if (desc != tree.getDescription()) {
            value = this.make.SerialData(desc);
        }
        return value;
    }

    protected final SerialFieldTree rewriteChildren(SerialFieldTree tree) {
        SerialFieldTree value = tree;
        IdentifierTree name = (IdentifierTree)this.translate((DocTree)tree.getName());
        java.util.List<T> description = this.translateDoc(tree.getDescription());
        ReferenceTree ref = (ReferenceTree)this.translate((DocTree)tree.getType());
        if (ref != tree.getType() || name != tree.getName() || description != tree.getDescription()) {
            value = this.make.SerialField(name, ref, description);
        }
        return value;
    }

    protected final SinceTree rewriteChildren(SinceTree tree) {
        SinceTree value = tree;
        java.util.List body = tree.getBody();
        if (body != tree.getBody()) {
            value = this.make.Since(body);
        }
        return value;
    }

    protected final StartElementTree rewriteChildren(StartElementTree tree) {
        StartElementTree value = tree;
        java.util.List<T> attributes = this.translateDoc(tree.getAttributes());
        if (attributes != tree.getAttributes()) {
            value = this.make.StartElement((CharSequence)((Name)tree.getName()), attributes, tree.isSelfClosing());
        }
        return value;
    }

    protected final TextTree rewriteChildren(TextTree tree) {
        return tree;
    }

    protected final ThrowsTree rewriteChildren(ThrowsTree tree) {
        ThrowsTree value = tree;
        ReferenceTree exception = (ReferenceTree)this.translate((DocTree)tree.getExceptionName());
        java.util.List<T> description = this.translateDoc(tree.getDescription());
        if (exception != tree.getExceptionName() || description != tree.getDescription()) {
            value = this.make.Throws(exception, description);
        }
        return value;
    }

    protected final UnknownBlockTagTree rewriteChildren(UnknownBlockTagTree tree) {
        UnknownBlockTagTree value = tree;
        java.util.List<T> content = this.translateDoc(tree.getContent());
        if (content != tree.getContent()) {
            value = this.make.UnknownBlockTag((CharSequence)((DCTree.DCUnknownBlockTag)tree).name, tree.getContent());
        }
        return value;
    }

    protected final UnknownInlineTagTree rewriteChildren(UnknownInlineTagTree tree) {
        UnknownInlineTagTree value = tree;
        java.util.List<T> content = this.translateDoc(tree.getContent());
        if (content != tree.getContent()) {
            value = this.make.UnknownInlineTag((CharSequence)((DCTree.DCUnknownInlineTag)tree).name, tree.getContent());
        }
        return value;
    }

    protected final ValueTree rewriteChildren(ValueTree tree) {
        ValueTree value = tree;
        ReferenceTree reference = (ReferenceTree)this.translate((DocTree)tree.getReference());
        if (reference != tree.getReference()) {
            value = this.make.Value(reference);
        }
        return value;
    }

    protected final VersionTree rewriteChildren(VersionTree tree) {
        VersionTree value = tree;
        java.util.List<T> body = this.translateDoc(tree.getBody());
        if (body != tree.getBody()) {
            value = this.make.Version(body);
        }
        return value;
    }

    public DocTree visitAttribute(AttributeTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitAuthor(AuthorTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitComment(CommentTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitDeprecated(DeprecatedTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitDocComment(DocCommentTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitDocRoot(DocRootTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitEndElement(EndElementTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitEntity(EntityTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitErroneous(ErroneousTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitIdentifier(IdentifierTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitInheritDoc(InheritDocTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitLink(LinkTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitLiteral(LiteralTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitParam(ParamTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitReference(ReferenceTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitReturn(ReturnTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitSee(SeeTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitSerial(SerialTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitSerialData(SerialDataTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitSerialField(SerialFieldTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitSince(SinceTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitStartElement(StartElementTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitText(TextTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitThrows(ThrowsTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitUnknownBlockTag(UnknownBlockTagTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitUnknownInlineTag(UnknownInlineTagTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitValue(ValueTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitVersion(VersionTree tree, Object p) {
        return this.rewriteChildren(tree);
    }

    public DocTree visitOther(DocTree tree, Object p) {
        throw new Error("DocTree not overloaded: " + (Object)tree);
    }
}

