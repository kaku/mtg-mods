/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.spi.indexing.PathRecognizer
 */
package org.netbeans.modules.java.source.parsing;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.netbeans.modules.parsing.spi.indexing.PathRecognizer;

public class JavaPathRecognizer
extends PathRecognizer {
    private static final Set<String> MIME_TYPES = Collections.singleton("text/x-java");
    private static final Set<String> SOURCES = Collections.singleton("classpath/source");
    private static final Set<String> BINARIES = Collections.unmodifiableSet(new HashSet<String>(Arrays.asList("classpath/boot", "classpath/compile")));

    public Set<String> getSourcePathIds() {
        return SOURCES;
    }

    public Set<String> getBinaryLibraryPathIds() {
        return BINARIES;
    }

    public Set<String> getLibraryPathIds() {
        return null;
    }

    public Set<String> getMimeTypes() {
        return MIME_TYPES;
    }
}

