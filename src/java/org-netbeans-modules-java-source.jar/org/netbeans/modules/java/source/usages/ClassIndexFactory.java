/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.ClassPath
 */
package org.netbeans.modules.java.source.usages;

import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.source.ClassIndex;

public interface ClassIndexFactory {
    public ClassIndex create(ClassPath var1, ClassPath var2, ClassPath var3);
}

