/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 */
package org.netbeans.modules.java.source.parsing;

import java.io.IOException;
import java.util.Set;
import javax.tools.JavaFileObject;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;

public interface Archive {
    public Iterable<JavaFileObject> getFiles(String var1, ClassPath.Entry var2, Set<JavaFileObject.Kind> var3, JavaFileFilterImplementation var4) throws IOException;

    public JavaFileObject create(String var1, JavaFileFilterImplementation var2) throws UnsupportedOperationException;

    public void clear();

    public JavaFileObject getFile(@NonNull String var1) throws IOException;
}

