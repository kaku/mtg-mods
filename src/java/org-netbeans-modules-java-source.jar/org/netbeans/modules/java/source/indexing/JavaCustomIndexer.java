/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.api.java.queries.AnnotationProcessingQuery
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 *  org.netbeans.modules.parsing.api.indexing.IndexingManager
 *  org.netbeans.modules.parsing.impl.indexing.IndexableImpl
 *  org.netbeans.modules.parsing.impl.indexing.SPIAccessor
 *  org.netbeans.modules.parsing.impl.indexing.friendapi.IndexingController
 *  org.netbeans.modules.parsing.spi.indexing.Context
 *  org.netbeans.modules.parsing.spi.indexing.CustomIndexer
 *  org.netbeans.modules.parsing.spi.indexing.CustomIndexerFactory
 *  org.netbeans.modules.parsing.spi.indexing.ErrorsCache
 *  org.netbeans.modules.parsing.spi.indexing.ErrorsCache$Convertor
 *  org.netbeans.modules.parsing.spi.indexing.ErrorsCache$ErrorKind
 *  org.netbeans.modules.parsing.spi.indexing.Indexable
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.awt.Notification
 *  org.openide.awt.NotificationDisplayer
 *  org.openide.awt.NotificationDisplayer$Category
 *  org.openide.awt.NotificationDisplayer$Priority
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.util.Pair
 *  org.openide.util.Parameters
 *  org.openide.util.TopologicalSortException
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.source.indexing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.swing.Icon;
import javax.tools.Diagnostic;
import javax.tools.FileObject;
import javax.tools.JavaFileObject;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.queries.AnnotationProcessingQuery;
import org.netbeans.api.java.source.ClassIndex;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.source.ElementHandleAccessor;
import org.netbeans.modules.java.source.JBrowseModule;
import org.netbeans.modules.java.source.JavaSourceTaskFactoryManager;
import org.netbeans.modules.java.source.indexing.APTUtils;
import org.netbeans.modules.java.source.indexing.CacheAttributesTransaction;
import org.netbeans.modules.java.source.indexing.CheckSums;
import org.netbeans.modules.java.source.indexing.CompileWorker;
import org.netbeans.modules.java.source.indexing.DiagnosticListenerImpl;
import org.netbeans.modules.java.source.indexing.FQN2Files;
import org.netbeans.modules.java.source.indexing.JavaFileFilterListener;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.indexing.JavaIndexerWorker;
import org.netbeans.modules.java.source.indexing.JavaParsingContext;
import org.netbeans.modules.java.source.indexing.MultiPassCompileWorker;
import org.netbeans.modules.java.source.indexing.OnePassCompileWorker;
import org.netbeans.modules.java.source.indexing.SuperOnePassCompileWorker;
import org.netbeans.modules.java.source.indexing.TransactionContext;
import org.netbeans.modules.java.source.parsing.FileManagerTransaction;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.InferableJavaFileObject;
import org.netbeans.modules.java.source.parsing.PrefetchableJavaFileObject;
import org.netbeans.modules.java.source.parsing.ProcessorGenerated;
import org.netbeans.modules.java.source.parsing.SourceFileManager;
import org.netbeans.modules.java.source.tasklist.TasklistSettings;
import org.netbeans.modules.java.source.usages.ClassIndexEventsTransaction;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;
import org.netbeans.modules.java.source.usages.ClassIndexManager;
import org.netbeans.modules.java.source.usages.ExecutableFilesIndex;
import org.netbeans.modules.java.source.usages.PersistentIndexTransaction;
import org.netbeans.modules.java.source.usages.VirtualSourceProviderQuery;
import org.netbeans.modules.java.source.util.Iterators;
import org.netbeans.modules.parsing.api.indexing.IndexingManager;
import org.netbeans.modules.parsing.impl.indexing.IndexableImpl;
import org.netbeans.modules.parsing.impl.indexing.SPIAccessor;
import org.netbeans.modules.parsing.impl.indexing.friendapi.IndexingController;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.netbeans.modules.parsing.spi.indexing.CustomIndexer;
import org.netbeans.modules.parsing.spi.indexing.CustomIndexerFactory;
import org.netbeans.modules.parsing.spi.indexing.ErrorsCache;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.openide.awt.HtmlBrowser;
import org.openide.awt.Notification;
import org.openide.awt.NotificationDisplayer;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.Pair;
import org.openide.util.Parameters;
import org.openide.util.TopologicalSortException;
import org.openide.util.Utilities;

public class JavaCustomIndexer
extends CustomIndexer {
    static boolean NO_ONE_PASS_COMPILE_WORKER = Boolean.getBoolean(JavaCustomIndexer.class.getName() + ".no.one.pass.compile.worker");
    private static final String SOURCE_PATH = "sourcePath";
    private static final Pattern ANONYMOUS = Pattern.compile("\\$[0-9]");
    private static final ClassPath EMPTY = ClassPathSupport.createClassPath((URL[])new URL[0]);
    private static final int TRESHOLD = 500;
    private static final String WARNING_ICON = "org/netbeans/modules/java/source/resources/icons/warning.png";
    private static final ErrorsCache.Convertor<Diagnostic<?>> ERROR_CONVERTOR = new ErrorConvertorImpl(ErrorsCache.ErrorKind.ERROR);
    private static final ErrorsCache.Convertor<Diagnostic<?>> ERROR_CONVERTOR_NO_BADGE = new ErrorConvertorImpl(ErrorsCache.ErrorKind.ERROR_NO_BADGE);
    private static final Set<String> JDK7AndLaterWarnings = new HashSet<String>(Arrays.asList("compiler.warn.diamond.redundant.args", "compiler.warn.diamond.redundant.args.1", "compiler.note.potential.lambda.found"));

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void index(Iterable<? extends Indexable> files, Context context) {
        JavaIndex.LOG.log(Level.FINE, context.isSupplementaryFilesIndexing() ? "index suplementary({0})" : "index({0})", context.isAllFilesIndexing() ? context.getRootURI() : files);
        TransactionContext txCtx = TransactionContext.get();
        FileManagerTransaction fmTx = txCtx.get(FileManagerTransaction.class);
        assert (fmTx != null);
        ClassIndexEventsTransaction ciTx = txCtx.get(ClassIndexEventsTransaction.class);
        assert (ciTx != null);
        try {
            CompileWorker.ParsingOutput compileResult;
            JavaParsingContext javaContext;
            HashSet<ElementHandle<TypeElement>> removedTypes;
            HashSet<File> removedFiles;
            ArrayList<? extends CompileTuple> toCompile;
            org.openide.filesystems.FileObject root = context.getRoot();
            if (root == null) {
                JavaIndex.LOG.fine("Ignoring request with no root");
                return;
            }
            APTUtils.sourceRootRegistered(context.getRoot(), context.getRootURI());
            ClassPath sourcePath = ClassPath.getClassPath((org.openide.filesystems.FileObject)root, (String)"classpath/source");
            ClassPath bootPath = ClassPath.getClassPath((org.openide.filesystems.FileObject)root, (String)"classpath/boot");
            ClassPath compilePath = ClassPath.getClassPath((org.openide.filesystems.FileObject)root, (String)"classpath/compile");
            if (sourcePath == null || bootPath == null || compilePath == null) {
                txCtx.get(CacheAttributesTransaction.class).setInvalid(true);
                JavaIndex.LOG.log(Level.WARNING, "Ignoring root with no ClassPath: {0}", FileUtil.getFileDisplayName((org.openide.filesystems.FileObject)root));
                return;
            }
            if (!Arrays.asList(sourcePath.getRoots()).contains((Object)root)) {
                txCtx.get(CacheAttributesTransaction.class).setInvalid(true);
                JavaIndex.LOG.log(Level.WARNING, "Source root: {0} is not on its sourcepath", FileUtil.getFileDisplayName((org.openide.filesystems.FileObject)root));
                return;
            }
            if (JavaCustomIndexer.isAptBuildGeneratedFolder(context.getRootURI(), sourcePath)) {
                txCtx.get(CacheAttributesTransaction.class).setInvalid(true);
                JavaIndex.LOG.fine("Ignoring annotation processor build generated folder");
                return;
            }
            if (!files.iterator().hasNext() && !context.isAllFilesIndexing()) {
                boolean success = false;
                try {
                    JavaParsingContext javaContext2 = new JavaParsingContext(context, bootPath, compilePath, sourcePath, Collections.emptySet());
                    try {
                        javaContext2.getClassIndexImpl().setDirty(null);
                    }
                    finally {
                        javaContext2.finish();
                    }
                    success = true;
                }
                finally {
                    if (!success) {
                        JavaIndex.setAttribute(context.getRootURI(), "dirty", Boolean.TRUE.toString());
                    }
                }
            }
            ArrayList javaSources = new ArrayList();
            Collection<? extends CompileTuple> virtualSourceTuples = JavaCustomIndexer.translateVirtualSources(JavaCustomIndexer.splitSources(files, javaSources), context.getRootURI());
            try {
                javaContext = new JavaParsingContext(context, bootPath, compilePath, sourcePath, virtualSourceTuples);
            }
            finally {
                JavaIndex.setAttribute(context.getRootURI(), "dirty", Boolean.TRUE.toString());
            }
            boolean finished = false;
            removedTypes = new HashSet<ElementHandle<TypeElement>>();
            removedFiles = new HashSet<File>();
            toCompile = new ArrayList<CompileTuple>(javaSources.size() + virtualSourceTuples.size());
            compileResult = null;
            try {
                if (context.isAllFilesIndexing()) {
                    JavaCustomIndexer.cleanUpResources(context, fmTx);
                }
                if (javaContext.getClassIndexImpl() == null) {
                    return;
                }
                javaContext.getClassIndexImpl().setDirty(null);
                SourceFileManager.ModifiedFilesTransaction mftx = txCtx.get(SourceFileManager.ModifiedFilesTransaction.class);
                for (Indexable i : javaSources) {
                    CompileTuple tuple = JavaCustomIndexer.createTuple(context, javaContext, i);
                    if (tuple != null) {
                        toCompile.add(tuple);
                    }
                    if (mftx != null) {
                        try {
                            mftx.cacheUpdated(i.getURL().toURI());
                        }
                        catch (URISyntaxException ex) {
                            Exceptions.printStackTrace((Throwable)ex);
                        }
                    }
                    JavaCustomIndexer.clear(context, javaContext, i, removedTypes, removedFiles, fmTx);
                }
                for (CompileTuple tuple : virtualSourceTuples) {
                    JavaCustomIndexer.clear(context, javaContext, tuple.indexable, removedTypes, removedFiles, fmTx);
                }
                toCompile.addAll(virtualSourceTuples);
                ArrayList<? extends CompileTuple> toCompileRound = toCompile;
                boolean round = false;
                while (round++ < 2) {
                    CompileWorker[] WORKERS;
                    CompileWorker[] arrcompileWorker = new CompileWorker[2];
                    arrcompileWorker[0] = toCompileRound.size() < 500 ? new SuperOnePassCompileWorker() : new OnePassCompileWorker();
                    arrcompileWorker[1] = new MultiPassCompileWorker();
                    for (CompileWorker w : WORKERS = arrcompileWorker) {
                        if ((compileResult = w.compile(compileResult, context, javaContext, toCompileRound)) == null || context.isCancelled()) {
                            return;
                        }
                        if (compileResult.success) break;
                    }
                    if (compileResult.aptGenerated.isEmpty()) {
                        ++round;
                        continue;
                    }
                    toCompileRound = new ArrayList(compileResult.aptGenerated.size());
                    SPIAccessor accessor = SPIAccessor.getInstance();
                    for (FileObject fo : compileResult.aptGenerated) {
                        PrefetchableJavaFileObject pfo = (PrefetchableJavaFileObject)fo;
                        Indexable i2 = accessor.create((IndexableImpl)new AptGeneratedIndexable(pfo));
                        CompileTuple ct = new CompileTuple(pfo, i2, false, true, true);
                        toCompileRound.add(ct);
                        toCompile.add(ct);
                    }
                    compileResult.aptGenerated.clear();
                }
                finished = compileResult.success;
                if (compileResult.lowMemory) {
                    String rootName = FileUtil.getFileDisplayName((org.openide.filesystems.FileObject)context.getRoot());
                    JavaIndex.LOG.log(Level.WARNING, "Not enough memory to compile folder: {0}.", rootName);
                    NotificationDisplayer.getDefault().notify(NbBundle.getMessage(JavaCustomIndexer.class, (String)"TITLE_LowMemory"), (Icon)ImageUtilities.loadImageIcon((String)"org/netbeans/modules/java/source/resources/icons/warning.png", (boolean)false), NbBundle.getMessage(JavaCustomIndexer.class, (String)"MSG_LowMemory", (Object)rootName), new ActionListener(){

                        @Override
                        public void actionPerformed(ActionEvent e) {
                            try {
                                URL url = new URL(NbBundle.getMessage(JavaCustomIndexer.class, (String)"URL_LowMemory"));
                                HtmlBrowser.URLDisplayer.getDefault().showURLExternal(url);
                            }
                            catch (MalformedURLException ex) {
                                Exceptions.printStackTrace((Throwable)ex);
                            }
                        }
                    }, NotificationDisplayer.Priority.HIGH, NotificationDisplayer.Category.ERROR);
                }
            }
            finally {
                try {
                    javaContext.finish();
                }
                finally {
                    if (finished) {
                        JavaIndex.setAttribute(context.getRootURI(), "dirty", null);
                    }
                }
            }
            assert (compileResult != null);
            HashSet<ElementHandle<TypeElement>> _at = new HashSet<ElementHandle<TypeElement>>(compileResult.addedTypes);
            HashSet<ElementHandle<TypeElement>> _rt = new HashSet<ElementHandle<TypeElement>>(removedTypes);
            _at.removeAll(removedTypes);
            _rt.removeAll(compileResult.addedTypes);
            compileResult.addedTypes.retainAll(removedTypes);
            if (!context.isSupplementaryFilesIndexing() && !context.isCancelled()) {
                compileResult.modifiedTypes.addAll(_rt);
                Map<URL, Set<URL>> root2Rebuild = JavaCustomIndexer.findDependent(context.getRootURI(), compileResult.modifiedTypes, !_at.isEmpty());
                Set<URL> urls = root2Rebuild.get(context.getRootURI());
                if (urls != null) {
                    if (context.isAllFilesIndexing()) {
                        root2Rebuild.remove(context.getRootURI());
                    } else {
                        for (CompileTuple ct : toCompile) {
                            urls.remove(ct.indexable.getURL());
                        }
                        if (urls.isEmpty()) {
                            root2Rebuild.remove(context.getRootURI());
                        }
                    }
                }
                for (Map.Entry entry : root2Rebuild.entrySet()) {
                    context.addSupplementaryFiles((URL)entry.getKey(), (Collection)entry.getValue());
                }
            }
            try {
                javaContext.store();
            }
            catch (JavaParsingContext.BrokenIndexException bi) {
                JavaIndex.LOG.log(Level.WARNING, "Broken index for root: {0} reason {1}, recovering.", new Object[]{context.getRootURI()});
                PersistentIndexTransaction piTx = txCtx.get(PersistentIndexTransaction.class);
                piTx.setBroken();
            }
            ciTx.addedTypes(context.getRootURI(), _at);
            ciTx.removedTypes(context.getRootURI(), _rt);
            ciTx.changedTypes(context.getRootURI(), compileResult.addedTypes);
            if (!context.checkForEditorModifications()) {
                ciTx.addedCacheFiles(context.getRootURI(), compileResult.createdFiles);
                ciTx.removedCacheFiles(context.getRootURI(), removedFiles);
            }
        }
        catch (IOException ioe) {
            Exceptions.printStackTrace((Throwable)ioe);
        }
    }

    private static List<? extends Indexable> splitSources(Iterable<? extends Indexable> indexables, List<? super Indexable> javaSources) {
        LinkedList<Indexable> virtualSources = new LinkedList<Indexable>();
        for (Indexable indexable : indexables) {
            if (indexable.getURL() == null) continue;
            if (VirtualSourceProviderQuery.hasVirtualSource(indexable)) {
                virtualSources.add(indexable);
                continue;
            }
            javaSources.add((Indexable)indexable);
        }
        return virtualSources;
    }

    private static Collection<? extends CompileTuple> translateVirtualSources(Collection<? extends Indexable> virtualSources, URL rootURL) throws IOException {
        if (virtualSources.isEmpty()) {
            return Collections.emptySet();
        }
        try {
            File root = Utilities.toFile((URI)URI.create(rootURL.toString()));
            return VirtualSourceProviderQuery.translate(virtualSources, root);
        }
        catch (IllegalArgumentException e) {
            JavaIndex.LOG.log(Level.WARNING, "Virtual sources in the root: {0} are ignored due to: {1}", new Object[]{rootURL, e.getMessage()});
            return Collections.emptySet();
        }
    }

    private static CompileTuple createTuple(Context context, JavaParsingContext javaContext, Indexable indexable) {
        org.openide.filesystems.FileObject fo;
        File root = null;
        if (!context.checkForEditorModifications() && "file".equals(indexable.getURL().getProtocol()) && (root = FileUtil.toFile((org.openide.filesystems.FileObject)context.getRoot())) != null) {
            try {
                return new CompileTuple(FileObjects.fileFileObject(indexable, root, javaContext.getJavaFileFilter(), javaContext.getEncoding()), indexable);
            }
            catch (Exception ex) {
                // empty catch block
            }
        }
        return (fo = URLMapper.findFileObject((URL)indexable.getURL())) != null ? new CompileTuple(FileObjects.sourceFileObject(fo, context.getRoot()), indexable) : null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void clearFiles(Context context, Iterable<? extends Indexable> files) {
        TransactionContext txCtx = TransactionContext.get();
        assert (txCtx != null);
        FileManagerTransaction fmTx = txCtx.get(FileManagerTransaction.class);
        assert (fmTx != null);
        ClassIndexEventsTransaction ciTx = txCtx.get(ClassIndexEventsTransaction.class);
        assert (ciTx != null);
        try {
            JavaParsingContext javaContext = new JavaParsingContext(context, true);
            try {
                if (javaContext.getClassIndexImpl() == null) {
                    return;
                }
                if (javaContext.getClassIndexImpl().getType() == ClassIndexImpl.Type.EMPTY) {
                    return;
                }
                HashSet<ElementHandle<TypeElement>> removedTypes = new HashSet<ElementHandle<TypeElement>>();
                HashSet<File> removedFiles = new HashSet<File>();
                for (Indexable i : files) {
                    JavaCustomIndexer.clear(context, javaContext, i, removedTypes, removedFiles, fmTx);
                    ErrorsCache.setErrors((URL)context.getRootURI(), (Indexable)i, Collections.emptyList(), ERROR_CONVERTOR);
                    ExecutableFilesIndex.DEFAULT.setMainClass(context.getRootURI(), i.getURL(), false);
                    javaContext.getCheckSums().remove(i.getURL());
                }
                for (Map.Entry entry : JavaCustomIndexer.findDependent(context.getRootURI(), removedTypes, false).entrySet()) {
                    context.addSupplementaryFiles((URL)entry.getKey(), (Collection)entry.getValue());
                }
                try {
                    javaContext.store();
                }
                catch (JavaParsingContext.BrokenIndexException bi) {
                    JavaIndex.LOG.log(Level.WARNING, "Broken index for root: {0} reason: {1}, recovering.", new Object[]{context.getRootURI(), bi.getMessage()});
                    PersistentIndexTransaction piTx = txCtx.get(PersistentIndexTransaction.class);
                    assert (piTx != null);
                    piTx.setBroken();
                }
                ciTx.removedCacheFiles(context.getRootURI(), removedFiles);
                ciTx.removedTypes(context.getRootURI(), removedTypes);
            }
            finally {
                javaContext.finish();
            }
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    private static void clear(Context context, JavaParsingContext javaContext, Indexable indexable, Set<ElementHandle<TypeElement>> removedTypes, Set<File> removedFiles, @NonNull FileManagerTransaction fmTx) throws IOException {
        File file;
        assert (fmTx != null);
        ArrayList<Pair<String, String>> toDelete = new ArrayList<Pair<String, String>>();
        File classFolder = JavaIndex.getClassFolder(context);
        File aptFolder = JavaIndex.getAptFolder(context.getRootURI(), false);
        String sourceRelative = indexable.getRelativePath();
        LinkedList<Pair> sourceRelativeURLPairs = new LinkedList<Pair>();
        sourceRelativeURLPairs.add(Pair.of((Object)sourceRelative, (Object)indexable.getURL()));
        if (aptFolder.exists() && (file = new File(classFolder, FileObjects.stripExtension(sourceRelative) + '.' + "rapt")).exists()) {
            try {
                for (String fileName : JavaCustomIndexer.readRSFile(file)) {
                    File f = new File(aptFolder, fileName);
                    if (f.exists() && "java".equals(FileObjects.getExtension(f.getName()))) {
                        sourceRelativeURLPairs.add(Pair.of((Object)fileName, (Object)Utilities.toURI((File)f).toURL()));
                    }
                    fmTx.delete(f);
                }
            }
            catch (IOException ioe) {
                Exceptions.printStackTrace((Throwable)ioe);
            }
            fmTx.delete(file);
        }
        for (Pair relURLPair : sourceRelativeURLPairs) {
            boolean cont;
            FilenameFilter filter;
            String ext = FileObjects.getExtension((String)relURLPair.first());
            String withoutExt = FileObjects.stripExtension((String)relURLPair.first());
            boolean dieIfNoRefFile = VirtualSourceProviderQuery.hasVirtualSource(ext);
            file = dieIfNoRefFile ? new File(classFolder, (String)relURLPair.first() + '.' + "rx") : new File(classFolder, withoutExt + '.' + "rs");
            boolean bl = cont = !dieIfNoRefFile;
            if (file.exists()) {
                cont = false;
                try {
                    String binaryName = FileObjects.getBinaryName(file, classFolder);
                    for (String className : JavaCustomIndexer.readRSFile(file)) {
                        File f = new File(classFolder, FileObjects.convertPackage2Folder(className) + '.' + "sig");
                        if (!binaryName.equals(className)) {
                            if (!javaContext.getFQNs().remove(className, (URL)relURLPair.second())) continue;
                            toDelete.add((Pair)Pair.of((Object)className, (Object)relURLPair.first()));
                            removedTypes.add(ElementHandleAccessor.getInstance().create(ElementKind.OTHER, className));
                            removedFiles.add(f);
                            fmTx.delete(f);
                            continue;
                        }
                        cont = !dieIfNoRefFile;
                    }
                }
                catch (IOException ioe) {
                    Exceptions.printStackTrace((Throwable)ioe);
                }
                fmTx.delete(file);
            }
            if (!cont || !(file = new File(classFolder, withoutExt + '.' + "sig")).exists() || !javaContext.getFQNs().remove(FileObjects.getBinaryName(file, classFolder), (URL)relURLPair.second())) continue;
            String fileName = file.getName();
            fileName = fileName.substring(0, fileName.lastIndexOf(46));
            final String[][] patterns = new String[][]{{fileName + '.', "", "sig", "rs", "rapt", "rx"}, {fileName + '$', null, "sig"}};
            File parent = file.getParentFile();
            File[] children = parent.listFiles(filter = new FilenameFilter(){

                @Override
                public boolean accept(File dir, String name) {
                    for (String[] pattern : patterns) {
                        if (!name.startsWith(pattern[0])) continue;
                        String ext = FileObjects.getExtension(name);
                        for (int i = 2; i < pattern.length; ++i) {
                            if (!pattern[i].equals(ext) || pattern[1] != null && name.length() != pattern[0].length() + pattern[i].length()) continue;
                            return true;
                        }
                    }
                    return false;
                }
            });
            if (children == null) continue;
            for (File f : children) {
                String className = FileObjects.getBinaryName(f, classFolder);
                toDelete.add((Pair)Pair.of((Object)className, (Object)null));
                removedTypes.add(ElementHandleAccessor.getInstance().create(ElementKind.OTHER, className));
                removedFiles.add(f);
                fmTx.delete(f);
            }
        }
        javaContext.delete(indexable, toDelete);
    }

    private static void markDirtyFiles(Context context, Iterable<? extends Indexable> files) {
        ClassIndexImpl indexImpl = ClassIndexManager.getDefault().getUsagesQuery(context.getRootURI(), false);
        if (indexImpl != null) {
            for (Indexable i : files) {
                indexImpl.setDirty(i.getURL());
            }
        }
    }

    public static Collection<? extends ElementHandle<TypeElement>> getRelatedTypes(File source, File root) throws IOException {
        boolean cont;
        LinkedList<ElementHandle> result = new LinkedList<ElementHandle>();
        File classFolder = JavaIndex.getClassFolder(root);
        String path = FileObjects.getRelativePath(root, source);
        String ext = FileObjects.getExtension(path);
        String pathNoExt = FileObjects.stripExtension(path);
        boolean dieIfNoRefFile = VirtualSourceProviderQuery.hasVirtualSource(ext);
        File file = dieIfNoRefFile ? new File(classFolder, path + '.' + "rx") : new File(classFolder, pathNoExt + '.' + "rs");
        boolean bl = cont = !dieIfNoRefFile;
        if (file.exists()) {
            cont = false;
            try {
                String binaryName = FileObjects.getBinaryName(file, classFolder);
                for (String className : JavaCustomIndexer.readRSFile(file)) {
                    if (!binaryName.equals(className)) {
                        result.add(ElementHandleAccessor.getInstance().create(ElementKind.CLASS, className));
                        continue;
                    }
                    cont = !dieIfNoRefFile;
                }
            }
            catch (IOException ioe) {
                Exceptions.printStackTrace((Throwable)ioe);
            }
        }
        if (cont && (file = new File(classFolder, pathNoExt + '.' + "sig")).exists()) {
            String fileName = file.getName();
            fileName = fileName.substring(0, fileName.lastIndexOf(46));
            final String[] patterns = new String[]{fileName + '.', fileName + '$'};
            File parent = file.getParentFile();
            FilenameFilter filter = new FilenameFilter(){

                @Override
                public boolean accept(File dir, String name) {
                    if (!name.endsWith("sig")) {
                        return false;
                    }
                    for (int i = 0; i < patterns.length; ++i) {
                        if (!name.startsWith(patterns[i])) continue;
                        return true;
                    }
                    return false;
                }
            };
            for (File f : parent.listFiles(filter)) {
                String className = FileObjects.getBinaryName(f, classFolder);
                result.add(ElementHandleAccessor.getInstance().create(ElementKind.CLASS, className));
            }
        }
        return result;
    }

    static void addAptGenerated(@NonNull Context context, @NonNull JavaParsingContext javaContext, @NonNull CompileTuple source, @NonNull Set<FileObject> aptGenerated) throws IOException {
        Set<FileObject> genSources = javaContext.getProcessorGeneratedFiles().getGeneratedSources(source.indexable.getURL());
        if (genSources != null) {
            aptGenerated.addAll(genSources);
        }
    }

    static void setErrors(Context context, CompileTuple active, DiagnosticListenerImpl errors) {
        if (!active.virtual) {
            Iterable<Diagnostic<? extends JavaFileObject>> filteredErrorsList = Iterators.filter(errors.getDiagnostics(active.jfo), new FilterOutJDK7AndLaterWarnings());
            ErrorsCache.setErrors((URL)context.getRootURI(), (Indexable)active.indexable, filteredErrorsList, active.aptGenerated ? ERROR_CONVERTOR_NO_BADGE : ERROR_CONVERTOR);
        }
    }

    static void brokenPlatform(@NonNull Context ctx, @NonNull Iterable<? extends CompileTuple> files, final @NullAllowed Diagnostic<JavaFileObject> diagnostic) {
        if (diagnostic == null) {
            return;
        }
        Diagnostic<JavaFileObject> error = new Diagnostic<JavaFileObject>(){

            @Override
            public Diagnostic.Kind getKind() {
                return Diagnostic.Kind.ERROR;
            }

            @Override
            public JavaFileObject getSource() {
                return (JavaFileObject)diagnostic.getSource();
            }

            @Override
            public long getPosition() {
                return diagnostic.getPosition();
            }

            @Override
            public long getStartPosition() {
                return diagnostic.getStartPosition();
            }

            @Override
            public long getEndPosition() {
                return diagnostic.getEndPosition();
            }

            @Override
            public long getLineNumber() {
                return diagnostic.getLineNumber();
            }

            @Override
            public long getColumnNumber() {
                return diagnostic.getColumnNumber();
            }

            @Override
            public String getCode() {
                return diagnostic.getCode();
            }

            @Override
            public String getMessage(Locale locale) {
                return diagnostic.getMessage(locale);
            }
        };
        for (CompileTuple file : files) {
            if (file.virtual) continue;
            ErrorsCache.setErrors((URL)ctx.getRootURI(), (Indexable)file.indexable, Collections.singleton(error), ERROR_CONVERTOR);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static Iterable<String> readRSFile(File file) throws IOException {
        LinkedHashSet<String> binaryNames;
        binaryNames = new LinkedHashSet<String>();
        BufferedReader in = new BufferedReader(new InputStreamReader((InputStream)new FileInputStream(file), "UTF-8"));
        try {
            String binaryName;
            while ((binaryName = in.readLine()) != null) {
                binaryNames.add(binaryName);
            }
        }
        finally {
            in.close();
        }
        return binaryNames;
    }

    private static Map<URL, Set<URL>> findDependent(URL root, Collection<ElementHandle<TypeElement>> classes, boolean includeFilesInError) throws IOException {
        Map deps = IndexingController.getDefault().getRootDependencies();
        Map peers = IndexingController.getDefault().getRootPeers();
        HashMap<URL, List<URL>> inverseDeps = new HashMap<URL, List<URL>>();
        for (Map.Entry entry : deps.entrySet()) {
            URL u1 = (URL)entry.getKey();
            List l1 = (List)entry.getValue();
            for (URL u2 : l1) {
                List<URL> l2 = inverseDeps.get(u2);
                if (l2 == null) {
                    l2 = new ArrayList<URL>();
                    inverseDeps.put(u2, l2);
                }
                l2.add(u1);
            }
        }
        return JavaCustomIndexer.findDependent(root, deps, inverseDeps, peers, classes, includeFilesInError, true);
    }

    public static Map<URL, Set<URL>> findDependent(URL root, Map<URL, List<URL>> sourceDeps, Map<URL, List<URL>> inverseDeps, Map<URL, List<URL>> peers, Collection<ElementHandle<TypeElement>> classes, boolean includeFilesInError, boolean includeCurrentSourceRoot) throws IOException {
        LinkedHashMap<URL, Set<URL>> ret = new LinkedHashMap<URL, Set<URL>>();
        Iterator<ElementHandle<TypeElement>> i = classes.iterator();
        while (i.hasNext()) {
            if (!ANONYMOUS.matcher(i.next().getBinaryName()).find()) continue;
            i.remove();
        }
        if (classes.isEmpty() && !includeFilesInError) {
            return ret;
        }
        List depRoots = inverseDeps.get(root);
        try {
            switch (TasklistSettings.getDependencyTracking()) {
                case DISABLED: {
                    if (depRoots == null) {
                        JavaIndex.setAttribute(root, "dirty", Boolean.TRUE.toString());
                    } else {
                        for (URL url : depRoots) {
                            JavaIndex.setAttribute(url, "dirty", Boolean.TRUE.toString());
                        }
                    }
                    return ret;
                }
                case ENABLED_WITHIN_ROOT: {
                    if (depRoots != null) {
                        for (URL url : depRoots) {
                            JavaIndex.setAttribute(url, "dirty", Boolean.TRUE.toString());
                        }
                    }
                    depRoots = Collections.singletonList(root);
                    break;
                }
                case ENABLED_WITHIN_PROJECT: {
                    Project rootPrj = FileOwnerQuery.getOwner((URI)root.toURI());
                    if (depRoots == null) {
                        if (rootPrj == null) {
                            depRoots = Collections.singletonList(root);
                            break;
                        }
                        depRoots = new ArrayList<URL>();
                        depRoots.add(root);
                        int index = depRoots.indexOf(root);
                        depRoots.addAll(index + 1, JavaCustomIndexer.getSrcRootPeers(peers, root));
                        break;
                    }
                    if (rootPrj == null) {
                        for (URL url : depRoots) {
                            JavaIndex.setAttribute(url, "dirty", Boolean.TRUE.toString());
                        }
                        depRoots = Collections.singletonList(root);
                        break;
                    }
                    ArrayList<URL> l = new ArrayList<URL>(depRoots.size());
                    for (URL url : depRoots) {
                        if (FileOwnerQuery.getOwner((URI)url.toURI()) == rootPrj) {
                            l.add(url);
                            continue;
                        }
                        JavaIndex.setAttribute(url, "dirty", Boolean.TRUE.toString());
                    }
                    l.add(root);
                    depRoots = Utilities.topologicalSort(l, inverseDeps);
                    int index = depRoots.indexOf(root);
                    depRoots.addAll(index + 1, JavaCustomIndexer.getSrcRootPeers(peers, root));
                    break;
                }
                case ENABLED: {
                    if (depRoots == null) {
                        depRoots = new ArrayList<URL>();
                        depRoots.add((URL)root);
                    } else {
                        ArrayList<URL> l = new ArrayList<URL>(depRoots);
                        l.add(root);
                        depRoots = Utilities.topologicalSort(l, inverseDeps);
                    }
                    int index = depRoots.indexOf(root);
                    depRoots.addAll(index + 1, JavaCustomIndexer.getSrcRootPeers(peers, root));
                }
            }
        }
        catch (TopologicalSortException ex) {
            JavaIndex.LOG.log(Level.WARNING, "Cycle in the source root dependencies detected: {0}", ex.unsortableSets());
            List part = ex.partialSort();
            part.retainAll(depRoots);
            depRoots = part;
        }
        catch (URISyntaxException urise) {
            depRoots = Collections.singletonList(root);
        }
        LinkedList<ElementHandle<TypeElement>> queue = new LinkedList<ElementHandle<TypeElement>>(classes);
        HashMap bases = new HashMap();
        for (URL depRoot : depRoots) {
            Collection errUrls;
            ClassIndexImpl ciImpl = ClassIndexManager.getDefault().getUsagesQuery(depRoot, true);
            if (ciImpl == null) continue;
            ClassIndex index = ClasspathInfo.create(EMPTY, EMPTY, ClassPathSupport.createClassPath((URL[])new URL[]{depRoot})).getClassIndex();
            ArrayList<Map<URL, List<URL>>> depMaps = new ArrayList<Map<URL, List<URL>>>(2);
            if (sourceDeps != null) {
                depMaps.add(sourceDeps);
            }
            depMaps.add(peers);
            for (Map depMap : depMaps) {
                List dep = (List)depMap.get(depRoot);
                if (dep == null) continue;
                for (URL url : dep) {
                    Set b = (Set)bases.get(url);
                    if (b == null) continue;
                    queue.addAll(b);
                }
            }
            HashSet<ElementHandle<TypeElement>> toHandle = new HashSet<ElementHandle<TypeElement>>();
            while (!queue.isEmpty()) {
                ElementHandle<TypeElement> e = queue.poll();
                if (!toHandle.add(e)) continue;
                queue.addAll(index.getElements(e, EnumSet.of(ClassIndex.SearchKind.IMPLEMENTORS), EnumSet.of(ClassIndex.SearchScope.SOURCE)));
            }
            bases.put(depRoot, toHandle);
            if (!includeCurrentSourceRoot && depRoot.equals(root)) continue;
            HashSet<org.openide.filesystems.FileObject> files = new HashSet<org.openide.filesystems.FileObject>();
            for (ElementHandle e : toHandle) {
                files.addAll(index.getResources(e, EnumSet.complementOf(EnumSet.of(ClassIndex.SearchKind.IMPLEMENTORS)), EnumSet.of(ClassIndex.SearchScope.SOURCE)));
            }
            HashSet<URL> urls = new HashSet<URL>();
            for (org.openide.filesystems.FileObject file : files) {
                urls.add(file.getURL());
            }
            if (includeFilesInError && !(errUrls = ErrorsCache.getAllFilesInError((URL)depRoot)).isEmpty()) {
                urls.addAll(errUrls);
            }
            if (urls.isEmpty()) continue;
            ret.put(depRoot, urls);
        }
        return ret;
    }

    private static void cleanUpResources(@NonNull Context ctx, @NonNull FileManagerTransaction fmTx) throws IOException {
        File classFolder = JavaIndex.getClassFolder(ctx);
        File resourcesFile = new File(classFolder, "resouces.res");
        try {
            for (String fileName : JavaCustomIndexer.readRSFile(resourcesFile)) {
                File f = new File(classFolder, fileName);
                fmTx.delete(f);
            }
            fmTx.delete(resourcesFile);
        }
        catch (IOException ioe) {
            // empty catch block
        }
    }

    private static boolean isAptBuildGeneratedFolder(@NonNull URL root, @NonNull ClassPath srcPath) {
        Parameters.notNull((CharSequence)"root", (Object)root);
        Parameters.notNull((CharSequence)"srcPath", (Object)srcPath);
        for (org.openide.filesystems.FileObject srcRoot : srcPath.getRoots()) {
            if (!root.equals(AnnotationProcessingQuery.getAnnotationProcessingOptions((org.openide.filesystems.FileObject)srcRoot).sourceOutputDirectory())) continue;
            return true;
        }
        return false;
    }

    private static List<? extends URL> getSrcRootPeers(Map<URL, List<URL>> root2Peers, URL rootURL) {
        List result = root2Peers.get(rootURL);
        if (result == null) {
            result = Collections.emptyList();
        }
        JavaIndex.LOG.log(Level.FINE, "Peer source roots for root {0} -> {1}", new Object[]{rootURL, result});
        return result;
    }

    private static boolean ensureSourcePath(@NonNull org.openide.filesystems.FileObject root) throws IOException {
        String srcPathStr;
        ClassPath srcPath = ClassPath.getClassPath((org.openide.filesystems.FileObject)root, (String)"classpath/source");
        if (srcPath != null) {
            StringBuilder sb = new StringBuilder();
            for (ClassPath.Entry entry : srcPath.entries()) {
                sb.append(entry.getURL()).append(' ');
            }
            srcPathStr = sb.toString();
        } else {
            srcPathStr = "";
        }
        return JavaIndex.ensureAttributeValue(root.toURL(), "sourcePath", srcPathStr);
    }

    private static final class MetadataCheck
    extends Check {
        MetadataCheck(@NonNull Context ctx) {
            super(ctx);
        }

        @NonNull
        @Override
        public Boolean call() throws Exception {
            boolean vote = true;
            org.openide.filesystems.FileObject root = this.ctx.getRoot();
            if (root == null) {
                return vote;
            }
            if (APTUtils.get(root).verifyAttributes(this.ctx.getRoot(), false)) {
                vote = false;
            }
            if (JavaCustomIndexer.ensureSourcePath(root)) {
                JavaIndex.LOG.fine("forcing reindex due to source path change");
                vote = false;
            }
            if (JavaIndex.ensureAttributeValue(this.ctx.getRootURI(), "dirty", null)) {
                JavaIndex.LOG.fine("forcing reindex due to dirty root");
                vote = false;
            }
            if (!JavaFileFilterListener.getDefault().startListeningOn(this.ctx.getRoot())) {
                JavaIndex.LOG.fine("Forcing reindex due to changed JavaFileFilter");
                vote = false;
            }
            return vote;
        }
    }

    private static final class IndexCheck
    extends Check {
        private final ClassIndexEventsTransaction cietx;

        IndexCheck(@NonNull Context ctx, @NonNull ClassIndexEventsTransaction cietx) {
            super(ctx);
            Parameters.notNull((CharSequence)"cietx", (Object)cietx);
            this.cietx = cietx;
        }

        @NonNull
        @Override
        public Boolean call() throws Exception {
            boolean classIndexConsistent;
            boolean vote = true;
            ClassIndexImpl uq = ClassIndexManager.getDefault().createUsagesQuery(this.ctx.getRootURI(), true, this.cietx);
            boolean bl = uq != null ? (uq.getState() != ClassIndexImpl.State.NEW ? true : uq.isValid()) : (classIndexConsistent = true);
            if (!classIndexConsistent) {
                vote = false;
            }
            return vote;
        }
    }

    private static abstract class Check
    implements Callable<Boolean> {
        protected final Context ctx;

        protected Check(@NonNull Context ctx) {
            Parameters.notNull((CharSequence)"ctx", (Object)ctx);
            this.ctx = ctx;
        }
    }

    private static final class AptGeneratedIndexable
    implements IndexableImpl {
        private final InferableJavaFileObject jfo;

        AptGeneratedIndexable(@NonNull InferableJavaFileObject jfo) {
            this.jfo = jfo;
        }

        public String getRelativePath() {
            StringBuilder sb = new StringBuilder(FileObjects.convertPackage2Folder(this.jfo.inferBinaryName(), '/'));
            sb.append('.');
            sb.append(FileObjects.getExtension(this.jfo.toUri().getPath()));
            return sb.toString();
        }

        public URL getURL() {
            try {
                return this.jfo.toUri().toURL();
            }
            catch (MalformedURLException ex) {
                throw new IllegalStateException(ex);
            }
        }

        public String getMimeType() {
            return "text/x-java";
        }

        public boolean isTypeOf(String mimeType) {
            return "text/x-java".equals(mimeType);
        }
    }

    private static class FilterOutJDK7AndLaterWarnings
    implements Comparable<Diagnostic<? extends JavaFileObject>> {
        private FilterOutJDK7AndLaterWarnings() {
        }

        @Override
        public int compareTo(Diagnostic<? extends JavaFileObject> o) {
            return JDK7AndLaterWarnings.contains(o.getCode()) ? 0 : -1;
        }
    }

    private static final class ErrorConvertorImpl
    implements ErrorsCache.Convertor<Diagnostic<?>> {
        private final ErrorsCache.ErrorKind errorKind;

        public ErrorConvertorImpl(ErrorsCache.ErrorKind errorKind) {
            this.errorKind = errorKind;
        }

        public ErrorsCache.ErrorKind getKind(Diagnostic<?> t) {
            return t.getKind() == Diagnostic.Kind.ERROR ? this.errorKind : ErrorsCache.ErrorKind.WARNING;
        }

        public int getLineNumber(Diagnostic<?> t) {
            return (int)t.getLineNumber();
        }

        public String getMessage(Diagnostic<?> t) {
            return t.getMessage(null);
        }
    }

    public static final class CompileTuple {
        public final PrefetchableJavaFileObject jfo;
        public final Indexable indexable;
        public final boolean virtual;
        public final boolean index;
        public final boolean aptGenerated;

        public CompileTuple(PrefetchableJavaFileObject jfo, Indexable indexable, boolean virtual, boolean index) {
            this(jfo, indexable, virtual, index, false);
        }

        public CompileTuple(PrefetchableJavaFileObject jfo, Indexable indexable, boolean virtual, boolean index, boolean aptGenerated) {
            this.jfo = jfo;
            this.indexable = indexable;
            this.virtual = virtual;
            this.index = index;
            this.aptGenerated = aptGenerated;
        }

        public CompileTuple(PrefetchableJavaFileObject jfo, Indexable indexable) {
            this(jfo, indexable, false, true);
        }
    }

    public static class Factory
    extends CustomIndexerFactory {
        private static AtomicBoolean javaTaskFactoriesInitialized = new AtomicBoolean(false);

        public Factory() {
            if (!javaTaskFactoriesInitialized.getAndSet(true)) {
                JavaSourceTaskFactoryManager.register();
            }
        }

        public boolean scanStarted(Context context) {
            JavaIndex.LOG.log(Level.FINE, "scan started for root ({0})", context.getRootURI());
            TransactionContext txctx = TransactionContext.beginStandardTransaction(context.getRootURI(), true, context.isAllFilesIndexing(), context.checkForEditorModifications());
            try {
                return JavaIndexerWorker.reduce(Boolean.TRUE, JavaIndexerWorker.Bool.AND, new IndexCheck(context, txctx.get(ClassIndexEventsTransaction.class)), new MetadataCheck(context));
            }
            catch (ExecutionException ee) {
                JavaIndex.LOG.log(Level.WARNING, "Exception while checking cache validity for root: " + context.getRootURI(), ee.getCause());
                return false;
            }
            catch (InterruptedException ie) {
                return false;
            }
        }

        public void scanFinished(Context context) {
            TransactionContext txCtx = TransactionContext.get();
            assert (txCtx != null);
            try {
                if (context.isCancelled()) {
                    txCtx.rollBack();
                } else {
                    txCtx.commit();
                }
            }
            catch (IOException ioe) {
                Exceptions.printStackTrace((Throwable)ioe);
            }
        }

        public CustomIndexer createIndexer() {
            return new JavaCustomIndexer();
        }

        public void filesDeleted(Iterable<? extends Indexable> deleted, Context context) {
            JavaIndex.LOG.log(Level.FINE, "filesDeleted({0})", deleted);
            JavaCustomIndexer.clearFiles(context, deleted);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         * Enabled aggressive block sorting
         * Enabled unnecessary exception pruning
         * Enabled aggressive exception aggregation
         */
        public void rootsRemoved(Iterable<? extends URL> removedRoots) {
            assert (removedRoots != null);
            JavaIndex.LOG.log(Level.FINE, "roots removed: {0}", removedRoots);
            TransactionContext txCtx = TransactionContext.beginTrans().register(ClassIndexEventsTransaction.class, ClassIndexEventsTransaction.create(true));
            try {
                APTUtils.sourceRootUnregistered(removedRoots);
                ClassIndexManager cim = ClassIndexManager.getDefault();
                JavaFileFilterListener ffl = JavaFileFilterListener.getDefault();
                try {
                    HashSet toRefresh = new HashSet();
                    for (URL removedRoot : removedRoots) {
                        if (JBrowseModule.isClosed()) {
                            return;
                        }
                        cim.removeRoot(removedRoot);
                        ffl.stopListeningOn(removedRoot);
                        org.openide.filesystems.FileObject root = URLMapper.findFileObject((URL)removedRoot);
                        if (root == null) {
                            JavaIndex.setAttribute(removedRoot, "dirty", Boolean.TRUE.toString());
                            continue;
                        }
                        JavaCustomIndexer.ensureSourcePath(root);
                    }
                    for (URL removedRoot2 : removedRoots) {
                        toRefresh.remove(removedRoot2);
                    }
                    Iterator i$ = toRefresh.iterator();
                    while (i$.hasNext()) {
                        URL url = (URL)i$.next();
                        IndexingManager.getDefault().refreshIndex(url, null, true);
                    }
                    return;
                }
                catch (IOException e) {
                    Exceptions.printStackTrace((Throwable)e);
                    return;
                }
            }
            finally {
                try {
                    if (JBrowseModule.isClosed()) {
                        txCtx.rollBack();
                    } else {
                        txCtx.commit();
                    }
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }

        public void filesDirty(Iterable<? extends Indexable> dirty, Context context) {
            JavaIndex.LOG.log(Level.FINE, "filesDirty({0})", dirty);
            JavaCustomIndexer.markDirtyFiles(context, dirty);
        }

        @NonNull
        public String getIndexerName() {
            return "java";
        }

        public boolean supportsEmbeddedIndexers() {
            return true;
        }

        public int getIndexVersion() {
            return 14;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof Factory)) {
                return false;
            }
            Factory of = (Factory)((Object)obj);
            return of.getIndexerName().equals(this.getIndexerName()) && of.getIndexVersion() == this.getIndexVersion();
        }

        public int hashCode() {
            return this.getIndexerName().hashCode();
        }
    }

}

