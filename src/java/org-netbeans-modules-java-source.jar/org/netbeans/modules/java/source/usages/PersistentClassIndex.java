/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  org.apache.lucene.analysis.Analyzer
 *  org.apache.lucene.document.Document
 *  org.apache.lucene.document.FieldSelector
 *  org.apache.lucene.index.Term
 *  org.apache.lucene.search.BooleanClause
 *  org.apache.lucene.search.BooleanClause$Occur
 *  org.apache.lucene.search.Query
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.queries.SourceForBinaryQuery
 *  org.netbeans.modules.parsing.lucene.support.Convertor
 *  org.netbeans.modules.parsing.lucene.support.Index
 *  org.netbeans.modules.parsing.lucene.support.Index$Status
 *  org.netbeans.modules.parsing.lucene.support.Index$Transactional
 *  org.netbeans.modules.parsing.lucene.support.Index$WithTermFrequencies
 *  org.netbeans.modules.parsing.lucene.support.Index$WithTermFrequencies$TermFreq
 *  org.netbeans.modules.parsing.lucene.support.IndexManager
 *  org.netbeans.modules.parsing.lucene.support.IndexManager$Action
 *  org.netbeans.modules.parsing.lucene.support.Queries
 *  org.netbeans.modules.parsing.lucene.support.Queries$QueryKind
 *  org.netbeans.modules.parsing.lucene.support.StoppableConvertor
 *  org.netbeans.modules.parsing.lucene.support.StoppableConvertor$Stop
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 *  org.openide.util.Pair
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.java.source.usages;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.tools.javac.api.JavacTaskImpl;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.ElementKind;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.FieldSelector;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.Query;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.queries.SourceForBinaryQuery;
import org.netbeans.api.java.source.ClassIndex;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.Task;
import org.netbeans.modules.java.source.JavaSourceAccessor;
import org.netbeans.modules.java.source.indexing.TransactionContext;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.usages.BinaryAnalyser;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;
import org.netbeans.modules.java.source.usages.DocumentUtil;
import org.netbeans.modules.java.source.usages.PersistentIndexTransaction;
import org.netbeans.modules.java.source.usages.QueryUtil;
import org.netbeans.modules.java.source.usages.SourceAnalyzerFactory;
import org.netbeans.modules.parsing.lucene.support.Convertor;
import org.netbeans.modules.parsing.lucene.support.Index;
import org.netbeans.modules.parsing.lucene.support.IndexManager;
import org.netbeans.modules.parsing.lucene.support.Queries;
import org.netbeans.modules.parsing.lucene.support.StoppableConvertor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import org.openide.util.Pair;
import org.openide.util.Parameters;

public final class PersistentClassIndex
extends ClassIndexImpl {
    private final Index index;
    private final URL root;
    private final File cacheRoot;
    private final ClassIndexImpl.Type beforeInitType;
    private final ClassIndexImpl.Type finalType;
    private final IndexPatch indexPath;
    private Set<String> rootPkgCache;
    private volatile FileObject cachedRoot;
    private static final Logger LOGGER = Logger.getLogger(PersistentClassIndex.class.getName());
    private static final String REFERENCES = "refs";

    private PersistentClassIndex(URL root, File cacheRoot, ClassIndexImpl.Type beforeInitType, ClassIndexImpl.Type finalType) throws IOException, IllegalArgumentException {
        assert (root != null);
        this.root = root;
        this.cacheRoot = cacheRoot;
        this.beforeInitType = beforeInitType;
        this.finalType = finalType;
        this.index = IndexManager.createIndex((File)PersistentClassIndex.getReferencesCacheFolder(cacheRoot), (Analyzer)DocumentUtil.createAnalyzer());
        this.indexPath = new IndexPatch();
    }

    @NonNull
    @Override
    public BinaryAnalyser getBinaryAnalyser() {
        return new BinaryAnalyser(new PIWriter(), this.cacheRoot);
    }

    @NonNull
    @Override
    public SourceAnalyzerFactory.StorableAnalyzer getSourceAnalyser() {
        TransactionContext txCtx = TransactionContext.get();
        assert (txCtx != null);
        PersistentIndexTransaction pit = txCtx.get(PersistentIndexTransaction.class);
        assert (pit != null);
        ClassIndexImpl.Writer writer = pit.getIndexWriter();
        if (writer == null) {
            writer = new PIWriter();
            pit.setIndexWriter(writer);
        }
        return SourceAnalyzerFactory.createStorableAnalyzer(writer);
    }

    @Override
    public ClassIndexImpl.Type getType() {
        return this.getState() == ClassIndexImpl.State.INITIALIZED ? this.finalType : this.beforeInitType;
    }

    @Override
    public boolean isValid() {
        try {
            return this.index.getStatus(true) != Index.Status.INVALID;
        }
        catch (IOException ex) {
            return false;
        }
    }

    @Override
    public FileObject[] getSourceRoots() {
        FileObject[] rootFos;
        if (this.getType() == ClassIndexImpl.Type.SOURCE) {
            FileObject[] arrfileObject;
            FileObject rootFo = URLMapper.findFileObject((URL)this.root);
            if (rootFo == null) {
                arrfileObject = new FileObject[]{};
            } else {
                FileObject[] arrfileObject2 = new FileObject[1];
                arrfileObject = arrfileObject2;
                arrfileObject2[0] = rootFo;
            }
            rootFos = arrfileObject;
        } else {
            rootFos = SourceForBinaryQuery.findSourceRoots((URL)this.root).getRoots();
        }
        return rootFos;
    }

    @Override
    public String getSourceName(String binaryName) throws IOException, InterruptedException {
        try {
            Query q = DocumentUtil.binaryNameQuery(binaryName);
            HashSet names = new HashSet();
            this.index.query(names, DocumentUtil.sourceNameConvertor(), DocumentUtil.sourceNameFieldSelector(), (AtomicBoolean)cancel.get(), new Query[]{q});
            return names.isEmpty() ? null : (String)names.iterator().next();
        }
        catch (IOException e) {
            return this.handleException(null, e, this.root);
        }
    }

    public static ClassIndexImpl create(URL root, File cacheRoot, ClassIndexImpl.Type beforeInitType, ClassIndexImpl.Type finalType) throws IOException, IllegalArgumentException {
        return new PersistentClassIndex(root, cacheRoot, beforeInitType, finalType);
    }

    @Override
    public <T> void search(@NonNull ElementHandle<?> element, final @NonNull Set<? extends ClassIndexImpl.UsageType> usageType, final @NonNull Set<? extends ClassIndex.SearchScopeType> scope, final @NonNull Convertor<? super Document, T> convertor, final @NonNull Set<? super T> result) throws InterruptedException, IOException {
        block6 : {
            Parameters.notNull((CharSequence)"element", element);
            Parameters.notNull((CharSequence)"usageType", usageType);
            Parameters.notNull((CharSequence)"scope", scope);
            Parameters.notNull((CharSequence)"convertor", convertor);
            Parameters.notNull((CharSequence)"result", result);
            final Pair<Convertor<Document, T>, Index> ctu = this.indexPath.getPatch(convertor);
            try {
                final String binaryName = SourceUtils.getJVMSignature(element)[0];
                ElementKind kind = element.getKind();
                if (kind == ElementKind.PACKAGE) {
                    IndexManager.priorityAccess((IndexManager.Action)new IndexManager.Action<Void>(){

                        public Void run() throws IOException, InterruptedException {
                            Query q = QueryUtil.scopeFilter(QueryUtil.createPackageUsagesQuery(binaryName, usageType, BooleanClause.Occur.SHOULD), scope);
                            if (q != null) {
                                PersistentClassIndex.this.index.query((Collection)result, (Convertor)ctu.first(), DocumentUtil.declaredTypesFieldSelector(), ClassIndexImpl.cancel.get(), new Query[]{q});
                                if (ctu.second() != null) {
                                    ((Index)ctu.second()).query((Collection)result, convertor, DocumentUtil.declaredTypesFieldSelector(), ClassIndexImpl.cancel.get(), new Query[]{q});
                                }
                            }
                            return null;
                        }
                    });
                    break block6;
                }
                if (kind.isClass() || kind.isInterface() || kind == ElementKind.OTHER) {
                    if (BinaryAnalyser.OBJECT.equals(binaryName)) {
                        this.getDeclaredTypes("", ClassIndex.NameKind.PREFIX, scope, convertor, result);
                    } else {
                        IndexManager.priorityAccess((IndexManager.Action)new IndexManager.Action<Void>(){

                            public Void run() throws IOException, InterruptedException {
                                Query usagesQuery = QueryUtil.scopeFilter(QueryUtil.createUsagesQuery(binaryName, usageType, BooleanClause.Occur.SHOULD), scope);
                                if (usagesQuery != null) {
                                    PersistentClassIndex.this.index.query((Collection)result, (Convertor)ctu.first(), DocumentUtil.declaredTypesFieldSelector(), ClassIndexImpl.cancel.get(), new Query[]{usagesQuery});
                                    if (ctu.second() != null) {
                                        ((Index)ctu.second()).query((Collection)result, convertor, DocumentUtil.declaredTypesFieldSelector(), ClassIndexImpl.cancel.get(), new Query[]{usagesQuery});
                                    }
                                }
                                return null;
                            }
                        });
                    }
                    break block6;
                }
                throw new IllegalArgumentException(element.toString());
            }
            catch (IOException ioe) {
                this.handleException(null, ioe, this.root);
            }
        }
    }

    @Override
    public <T> void getDeclaredTypes(final @NonNull String simpleName, final @NonNull ClassIndex.NameKind kind, final @NonNull Set<? extends ClassIndex.SearchScopeType> scope, final @NonNull Convertor<? super Document, T> convertor, final @NonNull Collection<? super T> result) throws InterruptedException, IOException {
        final Pair<Convertor<Document, T>, Index> ctu = this.indexPath.getPatch(convertor);
        try {
            IndexManager.priorityAccess((IndexManager.Action)new IndexManager.Action<Void>(){

                public Void run() throws IOException, InterruptedException {
                    Query query = QueryUtil.scopeFilter(Queries.createQuery((String)"simpleName", (String)"ciName", (String)simpleName, (Queries.QueryKind)DocumentUtil.translateQueryKind(kind)), scope);
                    if (query != null) {
                        PersistentClassIndex.this.index.query(result, (Convertor)ctu.first(), DocumentUtil.declaredTypesFieldSelector(), ClassIndexImpl.cancel.get(), new Query[]{query});
                        if (ctu.second() != null) {
                            ((Index)ctu.second()).query(result, convertor, DocumentUtil.declaredTypesFieldSelector(), ClassIndexImpl.cancel.get(), new Query[]{query});
                        }
                    }
                    return null;
                }
            });
        }
        catch (IOException ioe) {
            this.handleException(null, ioe, this.root);
        }
    }

    @Override
    public <T> void getDeclaredElements(final String ident, final ClassIndex.NameKind kind, final Convertor<? super Document, T> convertor, final Map<T, Set<String>> result) throws InterruptedException, IOException {
        final Pair<Convertor<Document, T>, Index> ctu = this.indexPath.getPatch(convertor);
        try {
            IndexManager.priorityAccess((IndexManager.Action)new IndexManager.Action<Void>(){

                public Void run() throws IOException, InterruptedException {
                    Query query = Queries.createTermCollectingQuery((String)"fids", (String)"cifids", (String)ident, (Queries.QueryKind)DocumentUtil.translateQueryKind(kind));
                    Convertor<Term, String> t2s = new Convertor<Term, String>(){

                        public String convert(Term p) {
                            return p.text();
                        }
                    };
                    PersistentClassIndex.this.index.queryDocTerms(result, (Convertor)ctu.first(), (Convertor)t2s, DocumentUtil.declaredTypesFieldSelector(), ClassIndexImpl.cancel.get(), new Query[]{query});
                    if (ctu.second() != null) {
                        ((Index)ctu.second()).queryDocTerms(result, convertor, (Convertor)t2s, DocumentUtil.declaredTypesFieldSelector(), ClassIndexImpl.cancel.get(), new Query[]{query});
                    }
                    return null;
                }

            });
        }
        catch (IOException ioe) {
            this.handleException(null, ioe, this.root);
        }
    }

    @Override
    public void getPackageNames(final String prefix, final boolean directOnly, final Set<String> result) throws InterruptedException, IOException {
        try {
            IndexManager.priorityAccess((IndexManager.Action)new IndexManager.Action<Void>(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                public Void run() throws IOException, InterruptedException {
                    Collection collectInto2;
                    Collection collectInto2;
                    boolean cacheOp = directOnly && prefix.length() == 0;
                    HashSet myPkgs = null;
                    if (cacheOp) {
                        PersistentClassIndex persistentClassIndex = PersistentClassIndex.this;
                        synchronized (persistentClassIndex) {
                            if (PersistentClassIndex.this.rootPkgCache != null) {
                                result.addAll(PersistentClassIndex.this.rootPkgCache);
                                return null;
                            }
                        }
                        myPkgs = new HashSet();
                        collectInto2 = new TeeCollection(myPkgs, result);
                    } else {
                        collectInto2 = result;
                    }
                    Pair<StoppableConvertor<Term, String>, Term> filter = QueryUtil.createPackageFilter(prefix, directOnly);
                    PersistentClassIndex.this.index.queryTerms(collectInto2, (Term)filter.second(), (StoppableConvertor)filter.first(), ClassIndexImpl.cancel.get());
                    if (cacheOp) {
                        PersistentClassIndex persistentClassIndex = PersistentClassIndex.this;
                        synchronized (persistentClassIndex) {
                            if (PersistentClassIndex.this.rootPkgCache == null) {
                                assert (myPkgs != null);
                                PersistentClassIndex.this.rootPkgCache = myPkgs;
                            }
                        }
                    }
                    return null;
                }
            });
        }
        catch (IOException ioe) {
            this.handleException(null, ioe, this.root);
        }
    }

    @Override
    public void getReferencesFrequences(final @NonNull Map<String, Integer> typeFreq, final @NonNull Map<String, Integer> pkgFreq) throws IOException, InterruptedException {
        assert (typeFreq != null);
        assert (pkgFreq != null);
        if (!(this.index instanceof Index.WithTermFrequencies)) {
            throw new IllegalStateException("Index does not support frequencies!");
        }
        try {
            IndexManager.priorityAccess((IndexManager.Action)new IndexManager.Action<Void>(){

                public Void run() throws IOException, InterruptedException {
                    Term startTerm = DocumentUtil.referencesTerm("", null, true);
                    FreqCollector convertor = new FreqCollector(startTerm, typeFreq, pkgFreq);
                    ((Index.WithTermFrequencies)PersistentClassIndex.this.index).queryTermFrequencies(Collections.emptyList(), startTerm, (StoppableConvertor)convertor, ClassIndexImpl.cancel.get());
                    return null;
                }
            });
        }
        catch (IOException ioe) {
            this.handleException(null, ioe, this.root);
        }
    }

    @Override
    public void setDirty(URL url) {
        try {
            this.indexPath.setDirtyFile(url);
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    public String toString() {
        return "PersistentClassIndex[" + this.root.toExternalForm() + "]";
    }

    @Override
    protected final void close() throws IOException {
        this.index.close();
    }

    private static File getReferencesCacheFolder(File cacheRoot) throws IOException {
        File refRoot = new File(cacheRoot, "refs");
        if (!refRoot.exists()) {
            refRoot.mkdir();
        }
        return refRoot;
    }

    private synchronized void resetPkgCache() {
        this.rootPkgCache = null;
    }

    @NonNull
    private FileObject getRoot() {
        FileObject res = this.cachedRoot;
        if (res == null || !res.isValid()) {
            this.cachedRoot = res = URLMapper.findFileObject((URL)this.root);
        }
        return res;
    }

    private static final class FreqCollector
    implements StoppableConvertor<Index.WithTermFrequencies.TermFreq, Void> {
        private final int postfixLen = ClassIndexImpl.UsageType.values().length;
        private final String fieldName;
        private final Map<String, Integer> typeFreq;
        private final Map<String, Integer> pkgFreq;

        FreqCollector(@NonNull Term startTerm, @NonNull Map<String, Integer> typeFreqs, @NonNull Map<String, Integer> pkgFreq) {
            this.fieldName = startTerm.field();
            this.typeFreq = typeFreqs;
            this.pkgFreq = pkgFreq;
        }

        @CheckForNull
        public Void convert(@NonNull Index.WithTermFrequencies.TermFreq param) throws StoppableConvertor.Stop {
            Term term = param.getTerm();
            if (this.fieldName != term.field()) {
                throw new StoppableConvertor.Stop();
            }
            int docCount = param.getFreq();
            String encBinName = term.text();
            String binName = encBinName.substring(0, encBinName.length() - this.postfixLen);
            int dotIndex = binName.lastIndexOf(46);
            String pkgName = dotIndex == -1 ? "" : binName.substring(0, dotIndex);
            Integer typeCount = this.typeFreq.get(binName);
            Integer pkgCount = this.pkgFreq.get(pkgName);
            this.typeFreq.put(binName, typeCount == null ? docCount : docCount + typeCount);
            this.pkgFreq.put(pkgName, pkgCount == null ? docCount : docCount + pkgCount);
            return null;
        }
    }

    private static class NoCallConvertor<F, T>
    implements Convertor<F, T> {
        private NoCallConvertor() {
        }

        public T convert(F p) {
            throw new IllegalStateException();
        }
    }

    private static class FilterConvertor<T>
    implements Convertor<Document, T> {
        private final Set<String> toExclude;
        private final Convertor<? super Document, T> delegate;

        private FilterConvertor(@NonNull Set<String> toExclude, @NonNull Convertor<? super Document, T> delegate) {
            assert (toExclude != null);
            assert (delegate != null);
            this.toExclude = toExclude;
            this.delegate = delegate;
        }

        @CheckForNull
        public T convert(@NonNull Document doc) {
            String rawName = DocumentUtil.getBinaryName(doc);
            return (T)(this.toExclude.contains(rawName) ? null : this.delegate.convert((Object)doc));
        }
    }

    private final class IndexPatch {
        private Index indexPatch;
        private URL dirty;
        private Set<String> typeFilter;

        IndexPatch() {
        }

        <T> Pair<Convertor<? super Document, T>, Index> getPatch(@NonNull Convertor<? super Document, T> delegate) {
            assert (delegate != null);
            try {
                Pair<Index, Set<String>> data = this.updateDirty();
                if (data != null) {
                    return Pair.of(new FilterConvertor((Set)data.second(), delegate), (Object)data.first());
                }
            }
            catch (IOException ioe) {
                Exceptions.printStackTrace((Throwable)ioe);
            }
            return Pair.of(delegate, (Object)null);
        }

        synchronized void setDirtyFile(@NullAllowed URL url) throws IOException {
            this.dirty = url;
            this.indexPatch = null;
            if (url == null) {
                this.typeFilter = null;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @CheckForNull
        private Pair<Index, Set<String>> updateDirty() throws IOException {
            URL url;
            Index result;
            Set<String> filter;
            IndexPatch indexPatch = this;
            synchronized (indexPatch) {
                url = this.dirty;
                filter = this.typeFilter;
                result = this.indexPatch;
            }
            if (url != null) {
                List data;
                FileObject file = url != null ? URLMapper.findFileObject((URL)url) : null;
                JavaSource js = file != null ? JavaSource.forFileObject(file) : null;
                long startTime = System.currentTimeMillis();
                final List[] dataHolder = new List[1];
                if (js != null) {
                    ClassPath scp = js.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.SOURCE);
                    if (scp != null && scp.contains(file)) {
                        js.runUserActionTask(new Task<CompilationController>(){

                            @Override
                            public void run(CompilationController controller) {
                                try {
                                    if (controller.toPhase(JavaSource.Phase.RESOLVED).compareTo(JavaSource.Phase.RESOLVED) < 0) {
                                        return;
                                    }
                                    try {
                                        SourceAnalyzerFactory.SimpleAnalyzer sa = SourceAnalyzerFactory.createSimpleAnalyzer();
                                        dataHolder[0] = sa.analyseUnit(controller.getCompilationUnit(), JavaSourceAccessor.getINSTANCE().getJavacTask(controller));
                                    }
                                    catch (IllegalArgumentException ia) {
                                        ClassPath scp = controller.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.SOURCE);
                                        Object[] arrobject = new Object[2];
                                        arrobject[0] = scp == null ? "<null>" : scp.toString();
                                        arrobject[1] = PersistentClassIndex.this.root.toExternalForm();
                                        throw new IllegalArgumentException(String.format("Provided source path: %s root: %s", arrobject), ia);
                                    }
                                }
                                catch (IOException ioe) {
                                    Exceptions.printStackTrace((Throwable)ioe);
                                }
                            }
                        }, true);
                    } else {
                        LOGGER.log(Level.INFO, "Not updating cache for file {0}, does not belong to classpath {1}", new Object[]{FileUtil.getFileDisplayName((FileObject)file), scp});
                    }
                }
                if ((data = dataHolder[0]) != null) {
                    if (filter == null) {
                        try {
                            filter = new HashSet<String>();
                            assert (file != null);
                            String relPath = FileUtil.getRelativePath((FileObject)PersistentClassIndex.this.getRoot(), (FileObject)file);
                            String clsName = FileObjects.convertFolder2Package(FileObjects.stripExtension(relPath));
                            PersistentClassIndex.this.index.query(filter, DocumentUtil.binaryNameConvertor(), DocumentUtil.declaredTypesFieldSelector(), null, new Query[]{(Query)DocumentUtil.queryClassWithEncConvertor(true).convert((Object)Pair.of((Object)clsName, (Object)relPath))});
                        }
                        catch (InterruptedException ie) {
                            throw new IOException(ie);
                        }
                    }
                    result = IndexManager.createMemoryIndex((Analyzer)DocumentUtil.createAnalyzer());
                    result.store((Collection)data, Collections.emptySet(), DocumentUtil.documentConvertor(), new NoCallConvertor(), false);
                } else {
                    filter = null;
                    result = null;
                }
                IndexPatch ie = this;
                synchronized (ie) {
                    this.dirty = null;
                    this.typeFilter = filter;
                    this.indexPatch = result;
                }
                long endTime = System.currentTimeMillis();
                LOGGER.log(Level.FINE, "PersistentClassIndex.updateDirty took: {0} ms", endTime - startTime);
            }
            if (result != null) {
                assert (filter != null);
                return Pair.of((Object)result, filter);
            }
            assert (filter == null);
            return null;
        }

    }

    private static class TeeCollection<T>
    extends AbstractCollection<T> {
        private Collection<T> primary;
        private Collection<T> secondary;

        TeeCollection(@NonNull Collection<T> primary, @NonNull Collection<T> secondary) {
            this.primary = primary;
            this.secondary = secondary;
        }

        @Override
        public Iterator<T> iterator() {
            throw new UnsupportedOperationException("Not supported operation.");
        }

        @Override
        public int size() {
            return this.primary.size();
        }

        @Override
        public boolean add(T e) {
            boolean result = this.primary.add(e);
            this.secondary.add(e);
            return result;
        }
    }

    private class PIWriter
    implements ClassIndexImpl.Writer {
        PIWriter() {
            if (PersistentClassIndex.this.index instanceof Runnable) {
                ((Runnable)PersistentClassIndex.this.index).run();
            }
        }

        @Override
        public void clear() throws IOException {
            PersistentClassIndex.this.resetPkgCache();
            PersistentClassIndex.this.index.clear();
        }

        @Override
        public void deleteAndFlush(List<Pair<Pair<String, String>, Object[]>> refs, Set<Pair<String, String>> toDelete) throws IOException {
            PersistentClassIndex.this.resetPkgCache();
            if (PersistentClassIndex.this.index instanceof Index.Transactional) {
                ((Index.Transactional)PersistentClassIndex.this.index).txStore(refs, toDelete, DocumentUtil.documentConvertor(), DocumentUtil.queryClassConvertor());
            } else {
                this.deleteAndStore(refs, toDelete);
            }
        }

        @Override
        public void commit() throws IOException {
            if (PersistentClassIndex.this.index instanceof Index.Transactional) {
                ((Index.Transactional)PersistentClassIndex.this.index).commit();
            }
        }

        @Override
        public void rollback() throws IOException {
            if (PersistentClassIndex.this.index instanceof Index.Transactional) {
                ((Index.Transactional)PersistentClassIndex.this.index).rollback();
            }
        }

        @Override
        public void deleteAndStore(List<Pair<Pair<String, String>, Object[]>> refs, Set<Pair<String, String>> toDelete) throws IOException {
            PersistentClassIndex.this.resetPkgCache();
            PersistentClassIndex.this.index.store(refs, toDelete, DocumentUtil.documentConvertor(), DocumentUtil.queryClassConvertor(), true);
        }
    }

}

