/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.source.usages;

import java.net.URL;
import java.util.EventObject;
import java.util.Set;
import org.netbeans.modules.java.source.usages.ClassIndexManager;

public final class ClassIndexManagerEvent
extends EventObject {
    private final Set<? extends URL> roots;

    public ClassIndexManagerEvent(ClassIndexManager source, Set<? extends URL> roots) {
        super(source);
        assert (roots != null);
        this.roots = roots;
    }

    public Set<? extends URL> getRoots() {
        return this.roots;
    }

    @Override
    public String toString() {
        return String.format("%s[source: %s, roots: %s]", ClassIndexManagerEvent.class.getName(), this.getSource(), this.getRoots());
    }
}

