/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.openide.util.Exceptions
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.java.source.parsing;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.tools.FileObject;
import javax.tools.JavaFileObject;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.modules.java.source.parsing.Archive;
import org.netbeans.modules.java.source.parsing.CachingArchiveProvider;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.openide.util.Exceptions;
import org.openide.util.Parameters;

public final class CachingArchiveClassLoader
extends ClassLoader {
    private static final int INI_SIZE = 16384;
    private static final Logger LOG = Logger.getLogger(CachingArchiveClassLoader.class.getName());
    private static final ReentrantReadWriteLock LOCK = new ReentrantReadWriteLock();
    private final Archive[] archives;
    private byte[] buffer;

    private CachingArchiveClassLoader(@NonNull Archive[] archives, ClassLoader parent) {
        super(parent);
        assert (archives != null);
        this.archives = archives;
    }

    @Override
    protected Class<?> findClass(final String name) throws ClassNotFoundException {
        final StringBuilder sb = new StringBuilder(FileObjects.convertPackage2Folder(name, '/'));
        sb.append(JavaFileObject.Kind.CLASS.extension);
        Class c = null;
        try {
            c = (Class)CachingArchiveClassLoader.readAction(new Callable<Class<?>>(){

                @Override
                public Class<?> call() throws Exception {
                    FileObject file = CachingArchiveClassLoader.this.findFileObject(sb.toString());
                    if (file != null) {
                        try {
                            String pack;
                            int len = CachingArchiveClassLoader.this.readJavaFileObject(file);
                            int lastDot = name.lastIndexOf(46);
                            if (lastDot != -1 && CachingArchiveClassLoader.this.getPackage(pack = name.substring(0, lastDot)) == null) {
                                CachingArchiveClassLoader.this.definePackage(pack, null, null, null, null, null, null, null);
                            }
                            return CachingArchiveClassLoader.this.defineClass(name, CachingArchiveClassLoader.this.buffer, 0, len);
                        }
                        catch (FileNotFoundException fnf) {
                            LOG.log(Level.FINE, "Resource: {0} does not exist.", file.toUri());
                        }
                        catch (IOException ioe) {
                            LOG.log(Level.INFO, "Resource: {0} cannot be read.", file.toUri());
                        }
                    }
                    return null;
                }
            });
        }
        catch (Exception e) {
            Exceptions.printStackTrace((Throwable)e);
        }
        return c != null ? c : super.findClass(name);
    }

    @Override
    protected URL findResource(final String name) {
        FileObject file = null;
        try {
            file = (FileObject)CachingArchiveClassLoader.readAction(new Callable<FileObject>(){

                @Override
                public FileObject call() throws Exception {
                    return CachingArchiveClassLoader.this.findFileObject(name);
                }
            });
        }
        catch (Exception e) {
            Exceptions.printStackTrace((Throwable)e);
        }
        if (file != null) {
            try {
                return file.toUri().toURL();
            }
            catch (MalformedURLException ex) {
                LOG.log(Level.INFO, ex.getMessage(), ex);
            }
        }
        return super.findResource(name);
    }

    @Override
    protected Enumeration<URL> findResources(final String name) throws IOException {
        try {
            return (Enumeration)CachingArchiveClassLoader.readAction(new Callable<Enumeration<URL>>(){

                @Override
                public Enumeration<URL> call() throws Exception {
                    Vector<URL> v = new Vector<URL>();
                    for (Archive archive : CachingArchiveClassLoader.this.archives) {
                        JavaFileObject file = archive.getFile(name);
                        if (file == null) continue;
                        v.add(file.toUri().toURL());
                    }
                    return v.elements();
                }
            });
        }
        catch (Exception ex) {
            throw new IOException(ex);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private int readJavaFileObject(FileObject jfo) throws IOException {
        int len;
        assert (LOCK.getReadLockCount() > 0);
        if (this.buffer == null) {
            this.buffer = new byte[16384];
        }
        len = 0;
        InputStream in = jfo.openInputStream();
        try {
            do {
                int l;
                if (this.buffer.length == len) {
                    byte[] nb = new byte[2 * this.buffer.length];
                    System.arraycopy(this.buffer, 0, nb, 0, len);
                    this.buffer = nb;
                }
                if ((l = in.read(this.buffer, len, this.buffer.length - len)) > 0) {
                    len += l;
                    continue;
                }
                break;
                break;
            } while (true);
        }
        finally {
            in.close();
        }
        return len;
    }

    private FileObject findFileObject(String resName) {
        assert (LOCK.getReadLockCount() > 0);
        for (Archive archive : this.archives) {
            try {
                JavaFileObject file = archive.getFile(resName);
                if (file == null) continue;
                return file;
            }
            catch (IOException ex) {
                LOG.log(Level.INFO, "Cannot read: " + archive, ex);
            }
        }
        return null;
    }

    public static ClassLoader forClassPath(@NonNull ClassPath classPath, @NullAllowed ClassLoader parent) {
        Parameters.notNull((CharSequence)"classPath", (Object)classPath);
        List entries = classPath.entries();
        URL[] urls = new URL[entries.size()];
        Iterator eit = entries.iterator();
        int i = 0;
        while (eit.hasNext()) {
            urls[i] = ((ClassPath.Entry)eit.next()).getURL();
            ++i;
        }
        return CachingArchiveClassLoader.forURLs(urls, parent);
    }

    public static ClassLoader forURLs(@NonNull URL[] urls, @NullAllowed ClassLoader parent) {
        Parameters.notNull((CharSequence)"urls", (Object)urls);
        ArrayList<Archive> archives = new ArrayList<Archive>(urls.length);
        for (URL url : urls) {
            Archive arch = CachingArchiveProvider.getDefault().getArchive(url, false);
            if (arch == null) continue;
            archives.add(arch);
        }
        return new CachingArchiveClassLoader(archives.toArray(new Archive[archives.size()]), parent);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static <T> T readAction(@NonNull Callable<T> action) throws Exception {
        Parameters.notNull((CharSequence)"action", action);
        LOCK.readLock().lock();
        try {
            LOG.log(Level.FINE, "Read locked by {0}", Thread.currentThread());
            T t = action.call();
            return t;
        }
        finally {
            LOCK.readLock().unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static <T> T writeAction(@NonNull Callable<T> action) throws Exception {
        Parameters.notNull((CharSequence)"action", action);
        LOCK.writeLock().lock();
        try {
            LOG.log(Level.FINE, "Write locked by {0}", Thread.currentThread());
            T t = action.call();
            return t;
        }
        finally {
            LOCK.writeLock().unlock();
        }
    }

}

