/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.source.save;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.netbeans.modules.java.source.save.Difference;

class ComputeDiff<E> {
    private E[] a;
    private E[] b;
    private List<Difference> diffs = new ArrayList<Difference>();
    private Difference pending;
    private Comparator<E> comparator;
    private TreeMap<Integer, Integer> thresh;
    private int[] sections;

    public ComputeDiff(E[] a, E[] b, Comparator<E> comp, int[] sections) {
        this.a = a;
        this.b = b;
        this.sections = sections;
        this.comparator = comp;
        this.thresh = null;
    }

    public ComputeDiff(E[] a, E[] b, int[] sections) {
        this(a, b, null, sections);
    }

    List<Difference> diff() {
        this.traverseSequences();
        if (this.pending != null) {
            this.diffs.add(this.pending);
        }
        return this.diffs;
    }

    protected void traverseSequences() {
        int ai;
        Integer[] matches = this.getLongestCommonSubsequences();
        int lastA = this.a.length - 1;
        int lastB = this.b.length - 1;
        int bi = 0;
        int lastMatch = matches.length - 1;
        for (ai = 0; ai <= lastMatch; ++ai) {
            Integer bLine = matches[ai];
            if (bLine == null) {
                this.onANotB(ai, bi);
                continue;
            }
            while (bi < bLine) {
                this.onBNotA(ai, bi++);
            }
            this.onMatch(ai, bi++);
        }
        boolean calledFinishA = false;
        boolean calledFinishB = false;
        while (ai <= lastA || bi <= lastB) {
            if (ai == lastA + 1 && bi <= lastB) {
                if (!calledFinishA && this.callFinishedA()) {
                    this.finishedA(lastA);
                    calledFinishA = true;
                } else {
                    while (bi <= lastB) {
                        this.onBNotA(ai, bi++);
                    }
                }
            }
            if (bi == lastB + 1 && ai <= lastA) {
                if (!calledFinishB && this.callFinishedB()) {
                    this.finishedB(lastB);
                    calledFinishB = true;
                } else {
                    while (ai <= lastA) {
                        this.onANotB(ai++, bi);
                    }
                }
            }
            if (ai <= lastA) {
                this.onANotB(ai++, bi);
            }
            if (bi > lastB) continue;
            this.onBNotA(ai, bi++);
        }
    }

    protected boolean callFinishedA() {
        return false;
    }

    protected boolean callFinishedB() {
        return false;
    }

    protected void finishedA(int lastA) {
    }

    protected void finishedB(int lastB) {
    }

    protected void onANotB(int ai, int bi) {
        if (this.pending == null) {
            this.pending = new Difference(ai, ai, bi, -1);
        } else {
            this.pending.setDeleted(ai);
        }
    }

    protected void onBNotA(int ai, int bi) {
        if (this.pending == null) {
            this.pending = new Difference(ai, -1, bi, bi);
        } else {
            this.pending.setAdded(bi);
        }
    }

    protected void onMatch(int ai, int bi) {
        if (this.pending != null) {
            this.diffs.add(this.pending);
            this.pending = null;
        }
    }

    protected boolean equals(E x, E y) {
        return this.comparator == null ? x.equals(y) : this.comparator.compare(x, y) == 0;
    }

    public Integer[] getLongestCommonSubsequences() {
        int bEndOrig;
        int sIndex = 0;
        int sL = this.sections == null ? 0 : this.sections.length;
        int aPrevStart = 0;
        int bPrevStart = 0;
        TreeMap<Integer, Integer> matches = new TreeMap<Integer, Integer>();
        Map bMatches = null;
        bMatches = this.comparator == null ? (this.a.length > 0 && this.a[0] instanceof Comparable ? new TreeMap() : new HashMap()) : new TreeMap(this.comparator);
        do {
            int bEnd;
            int aEnd;
            List positions;
            int aStart = aPrevStart;
            int bStart = bPrevStart;
            if (sIndex < sL) {
                aPrevStart = this.sections[sIndex++];
                aEnd = aPrevStart - 1;
                bPrevStart = this.sections[sIndex++];
                bEnd = bPrevStart - 1;
            } else {
                aEnd = this.a.length - 1;
                bEnd = this.b.length - 1;
            }
            bEndOrig = bEnd;
            while (aStart <= aEnd && bStart <= bEnd && this.equals(this.a[aStart], this.b[bStart])) {
                matches.put(new Integer(aStart++), new Integer(bStart++));
            }
            while (aStart <= aEnd && bStart <= bEnd && this.equals(this.a[aEnd], this.b[bEnd])) {
                matches.put(new Integer(aEnd--), new Integer(bEnd--));
            }
            for (int bi = bStart; bi <= bEnd; ++bi) {
                E element = this.b[bi];
                E key = element;
                positions = (ArrayList<Integer>)bMatches.get(key);
                if (positions == null) {
                    positions = new ArrayList<Integer>();
                    bMatches.put(key, positions);
                }
                positions.add((Integer)new Integer(bi));
            }
            this.thresh = new TreeMap();
            HashMap<Integer, Object[]> links = new HashMap<Integer, Object[]>();
            for (int i = aStart; i <= aEnd; ++i) {
                E aElement = this.a[i];
                positions = (List)bMatches.get(aElement);
                if (positions == null) continue;
                Integer k = new Integer(0);
                ListIterator pit = positions.listIterator(positions.size());
                while (pit.hasPrevious()) {
                    Integer j = (Integer)pit.previous();
                    k = this.insert(j, k);
                    if (k == null) continue;
                    Object[] value = k > 0 ? (Object[])links.get(new Integer(k - 1)) : null;
                    links.put(k, new Object[]{value, new Integer(i), j});
                }
            }
            if (this.thresh.size() <= 0) continue;
            Integer ti = this.thresh.lastKey();
            Object[] link = (Object[])links.get(ti);
            while (link != null) {
                Integer x = (Integer)link[1];
                Integer y = (Integer)link[2];
                matches.put(x, y);
                link = (Object[])link[0];
            }
        } while (bEndOrig < this.b.length - 1);
        return ComputeDiff.toArray(matches);
    }

    protected static Integer[] toArray(TreeMap map) {
        int size = map.size() == 0 ? 0 : 1 + (Integer)map.lastKey();
        Integer[] ary = new Integer[size];
        for (Integer idx : map.keySet()) {
            Integer val;
            ary[idx.intValue()] = val = (Integer)map.get(idx);
        }
        return ary;
    }

    protected static boolean isNonzero(Integer i) {
        return i != null && i != 0;
    }

    protected boolean isGreaterThan(Integer index, Integer val) {
        Integer lhs = this.thresh.get(index);
        return lhs != null && val != null && lhs.compareTo(val) > 0;
    }

    protected boolean isLessThan(Integer index, Integer val) {
        Integer lhs = this.thresh.get(index);
        return lhs != null && (val == null || lhs.compareTo(val) < 0);
    }

    protected Integer getLastValue() {
        return this.thresh.get(this.thresh.lastKey());
    }

    protected void append(Integer value) {
        Integer addIdx = null;
        if (this.thresh.size() == 0) {
            addIdx = new Integer(0);
        } else {
            Integer lastKey = this.thresh.lastKey();
            addIdx = new Integer(lastKey + 1);
        }
        this.thresh.put(addIdx, value);
    }

    protected Integer insert(Integer j, Integer k) {
        if (ComputeDiff.isNonzero(k) && this.isGreaterThan(k, j) && this.isLessThan(new Integer(k - 1), j)) {
            this.thresh.put(k, j);
        } else {
            int hi = -1;
            if (ComputeDiff.isNonzero(k)) {
                hi = k;
            } else if (this.thresh.size() > 0) {
                hi = this.thresh.lastKey();
            }
            if (hi == -1 || j.compareTo(this.getLastValue()) > 0) {
                this.append(j);
                k = new Integer(hi + 1);
            } else {
                int lo = 0;
                while (lo <= hi) {
                    int index = (hi + lo) / 2;
                    Integer val = this.thresh.get(new Integer(index));
                    int cmp = j.compareTo(val);
                    if (cmp == 0) {
                        return null;
                    }
                    if (cmp > 0) {
                        lo = index + 1;
                        continue;
                    }
                    hi = index - 1;
                }
                this.thresh.put(new Integer(lo), j);
                k = new Integer(lo);
            }
        }
        return k;
    }
}

