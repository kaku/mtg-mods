/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.util.Exceptions
 *  org.openide.util.Pair
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.source.parsing;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.tools.FileObject;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.java.source.classpath.AptCacheForSourceQuery;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.indexing.TransactionContext;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.openide.util.Exceptions;
import org.openide.util.Pair;
import org.openide.util.Utilities;

public final class ProcessorGenerated
extends TransactionContext.Service {
    private static final Logger LOG = Logger.getLogger(ProcessorGenerated.class.getName());
    private final URL root;
    private final URL aptRoot;
    private final Map<URL, Pair<Set<FileObject>, Set<FileObject>>> generated = new HashMap<URL, Pair<Set<FileObject>, Set<FileObject>>>();
    private File cachedFile;
    private StringBuilder cachedValue;
    private Set<String> cachedResources;
    private boolean cacheChanged;
    private boolean closedTx;

    private ProcessorGenerated(@NullAllowed URL root) {
        this.root = root;
        this.aptRoot = root == null ? null : AptCacheForSourceQuery.getAptFolder(root);
    }

    public Set<FileObject> getGeneratedSources(URL forSource) {
        Pair<Set<FileObject>, Set<FileObject>> res = this.generated.get(forSource);
        return res == null ? null : (Set)res.first();
    }

    public boolean canWrite() {
        return this.root != null;
    }

    @CheckForNull
    public URL findSibling(@NonNull Collection<? extends URL> candidates) {
        URL res = null;
        for (URL candiate : candidates) {
            if (this.root != null && !FileObjects.isParentOf(this.root, candiate)) continue;
            res = candiate;
            break;
        }
        return res;
    }

    public void register(@NonNull URL forSource, @NonNull FileObject file, @NonNull Type type) {
        if (!this.canWrite()) {
            return;
        }
        LOG.log(Level.FINE, "Generated: {0} from: {1} type: {2}", new Object[]{file.toUri(), forSource, type});
        Pair insertInto = this.generated.get(forSource);
        if (insertInto == null) {
            insertInto = Pair.of(new HashSet(), new HashSet());
            this.generated.put(forSource, (Pair)insertInto);
        }
        switch (type) {
            case SOURCE: {
                ((Set)insertInto.first()).add(file);
                break;
            }
            case RESOURCE: {
                ((Set)insertInto.second()).add(file);
                break;
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void commit() throws IOException {
        this.closeTx();
        if (!this.canWrite()) {
            assert (this.generated.isEmpty());
            return;
        }
        try {
            if (!this.generated.isEmpty()) {
                for (Map.Entry<URL, Pair<Set<FileObject>, Set<FileObject>>> entry : this.generated.entrySet()) {
                    URL source = entry.getKey();
                    Pair<Set<FileObject>, Set<FileObject>> gen = entry.getValue();
                    Set genSources = (Set)gen.first();
                    Set genResources = (Set)gen.second();
                    this.commitSource(source, genSources, genResources);
                }
                this.writeResources();
            }
        }
        finally {
            this.clear();
        }
    }

    @Override
    protected void rollBack() throws IOException {
        this.closeTx();
        if (!this.canWrite()) {
            assert (this.generated.isEmpty());
            return;
        }
        this.clear();
    }

    private void clear() {
        this.generated.clear();
        this.cachedFile = null;
        this.cachedResources = null;
        this.cachedValue = null;
        this.cacheChanged = false;
    }

    private void closeTx() {
        if (this.closedTx) {
            throw new IllegalStateException("Already commited or rolled back transaction.");
        }
        this.closedTx = true;
    }

    private void commitSource(@NonNull URL forSource, @NonNull Set<FileObject> genSources, @NonNull Set<FileObject> genResources) {
        try {
            File classCache;
            boolean apt = false;
            URL sourceRootURL = ProcessorGenerated.getOwnerRoot(forSource, this.root);
            if (sourceRootURL == null) {
                URL uRL = sourceRootURL = this.aptRoot != null ? ProcessorGenerated.getOwnerRoot(forSource, this.aptRoot) : null;
                if (sourceRootURL == null) {
                    return;
                }
                apt = true;
            }
            File sourceRoot = Utilities.toFile((URI)sourceRootURL.toURI());
            File file = classCache = apt ? Utilities.toFile((URI)AptCacheForSourceQuery.getClassFolder(sourceRootURL).toURI()) : JavaIndex.getClassFolder(sourceRoot);
            if (!genSources.isEmpty()) {
                File sourceFile = Utilities.toFile((URI)forSource.toURI());
                String relativePath = FileObjects.stripExtension(FileObjects.getRelativePath(sourceRoot, sourceFile));
                File cacheFile = new File(classCache, relativePath + '.' + "rapt");
                if (!cacheFile.getParentFile().exists()) {
                    cacheFile.getParentFile().mkdirs();
                }
                URL aptRootURL = AptCacheForSourceQuery.getAptFolder(sourceRootURL);
                StringBuilder sb = new StringBuilder();
                for (FileObject file2 : genSources) {
                    sb.append(FileObjects.getRelativePath(aptRootURL, file2.toUri().toURL()));
                    sb.append('\n');
                }
                this.writeFile(cacheFile, sb);
            }
            if (!genResources.isEmpty()) {
                File resFile = new File(classCache, "resouces.res");
                HashSet<String> currentResources = new HashSet<String>();
                StringBuilder sb = this.readResources(resFile, currentResources);
                boolean changed = false;
                for (FileObject file3 : genResources) {
                    String resPath = FileObjects.getRelativePath(Utilities.toURI((File)classCache).toURL(), file3.toUri().toURL());
                    if (!currentResources.add(resPath)) continue;
                    sb.append(resPath);
                    sb.append('\n');
                    changed = true;
                }
                if (changed) {
                    this.updateCache(sb, currentResources);
                }
            }
        }
        catch (IOException ioe) {
            Exceptions.printStackTrace((Throwable)ioe);
        }
        catch (URISyntaxException use) {
            Exceptions.printStackTrace((Throwable)use);
        }
    }

    private StringBuilder readResources(@NonNull File file, @NonNull Set<? super String> currentResources) {
        if (this.cachedFile == null) {
            this.cachedValue = this.readFile(file);
            this.cachedResources = new HashSet<String>(Arrays.asList(this.cachedValue.toString().split("\n")));
            this.cachedFile = file;
        }
        assert (this.cachedValue != null);
        assert (this.cachedResources != null);
        assert (this.cachedFile.equals(file));
        currentResources.addAll(this.cachedResources);
        return this.cachedValue;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private StringBuilder readFile(File file) {
        StringBuilder sb;
        block6 : {
            sb = new StringBuilder();
            try {
                InputStreamReader in = new InputStreamReader((InputStream)new FileInputStream(file), "UTF-8");
                try {
                    int len;
                    char[] buffer = new char[1024];
                    while ((len = in.read(buffer)) > 0) {
                        sb.append(buffer, 0, len);
                    }
                }
                finally {
                    in.close();
                }
            }
            catch (IOException ioe) {
                if (sb.length() == 0) break block6;
                sb = new StringBuilder();
            }
        }
        return sb;
    }

    private void writeResources() throws IOException {
        if (this.cacheChanged) {
            assert (this.cachedFile != null);
            assert (this.cachedValue != null);
            this.writeFile(this.cachedFile, this.cachedValue);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void writeFile(@NonNull File file, @NonNull StringBuilder data) throws IOException {
        OutputStreamWriter out = new OutputStreamWriter((OutputStream)new FileOutputStream(file), "UTF-8");
        try {
            out.write(data.toString());
        }
        finally {
            out.close();
        }
    }

    private void updateCache(@NonNull StringBuilder data, @NonNull Set<String> currentResources) {
        assert (data != null);
        assert (currentResources != null);
        this.cachedValue = data;
        this.cachedResources = currentResources;
        this.cacheChanged = true;
    }

    @CheckForNull
    private static URL getOwnerRoot(@NonNull URL source, @NonNull URL root) throws URISyntaxException {
        assert (source != null);
        assert (root != null);
        if (FileObjects.isParentOf(root, source)) {
            return root;
        }
        return null;
    }

    @NonNull
    public static ProcessorGenerated create(@NonNull URL root) {
        return new ProcessorGenerated(root);
    }

    @NonNull
    public static ProcessorGenerated nullWrite() {
        return new ProcessorGenerated(null);
    }

    public static enum Type {
        SOURCE,
        RESOURCE;
        

        private Type() {
        }
    }

}

