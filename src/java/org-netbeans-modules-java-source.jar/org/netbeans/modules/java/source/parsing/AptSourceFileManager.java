/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.java.source.parsing;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.source.classpath.AptCacheForSourceQuery;
import org.netbeans.modules.java.source.parsing.FileManagerTransaction;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.SiblingProvider;
import org.netbeans.modules.java.source.parsing.SourceFileManager;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;

public class AptSourceFileManager
extends SourceFileManager {
    public static final String ORIGIN_FILE = "apt-origin";
    public static final String ORIGIN_SOURCE_ELEMENT_URL = "apt-source-element";
    public static final String ORIGIN_RESOURCE_ELEMENT_URL = "apt-resource-element";
    private final ClassPath userRoots;
    private final SiblingProvider siblings;
    private final FileManagerTransaction fileTx;

    public AptSourceFileManager(@NonNull ClassPath userRoots, @NonNull ClassPath aptRoots, @NonNull SiblingProvider siblings, @NonNull FileManagerTransaction fileTx) {
        super(aptRoots, true);
        assert (userRoots != null);
        assert (siblings != null);
        this.userRoots = userRoots;
        this.siblings = siblings;
        this.fileTx = fileTx;
    }

    @Override
    public Iterable<JavaFileObject> list(JavaFileManager.Location l, String packageName, Set<JavaFileObject.Kind> kinds, boolean recursive) {
        return this.fileTx.filter(l, packageName, super.list(l, packageName, kinds, recursive));
    }

    @Override
    public javax.tools.FileObject getFileForOutput(JavaFileManager.Location l, String pkgName, String relativeName, javax.tools.FileObject sibling) throws IOException, UnsupportedOperationException, IllegalArgumentException {
        if (StandardLocation.SOURCE_OUTPUT != l) {
            throw new UnsupportedOperationException("Only apt output is supported.");
        }
        FileObject aptRoot = this.getAptRoot(sibling);
        if (aptRoot == null) {
            throw new UnsupportedOperationException(this.noAptRootDebug(sibling));
        }
        String nameStr = pkgName.length() == 0 ? relativeName : pkgName.replace('.', File.separatorChar) + File.separatorChar + relativeName;
        File rootFile = FileUtil.toFile((FileObject)aptRoot);
        return this.fileTx.createFileObject(l, new File(rootFile, nameStr), rootFile, null, null);
    }

    @Override
    public JavaFileObject getJavaFileForOutput(JavaFileManager.Location l, String className, JavaFileObject.Kind kind, javax.tools.FileObject sibling) throws IOException, UnsupportedOperationException, IllegalArgumentException {
        if (StandardLocation.SOURCE_OUTPUT != l) {
            throw new UnsupportedOperationException("Only apt output is supported.");
        }
        FileObject aptRoot = this.getAptRoot(sibling);
        if (aptRoot == null) {
            throw new UnsupportedOperationException(this.noAptRootDebug(sibling));
        }
        String nameStr = className.replace('.', File.separatorChar) + kind.extension;
        File rootFile = FileUtil.toFile((FileObject)aptRoot);
        JavaFileObject result = this.fileTx.createFileObject(l, new File(rootFile, nameStr), rootFile, null, null);
        return result;
    }

    @Override
    public boolean handleOption(String head, Iterator<String> tail) {
        return super.handleOption(head, tail);
    }

    private FileObject getAptRoot(javax.tools.FileObject sibling) {
        URL ownerRoot = this.getOwnerRoot(sibling);
        if (ownerRoot == null) {
            return null;
        }
        URL aptRoot = AptCacheForSourceQuery.getAptFolder(ownerRoot);
        return aptRoot == null ? null : URLMapper.findFileObject((URL)aptRoot);
    }

    private URL getOwnerRoot(javax.tools.FileObject sibling) {
        try {
            return this.siblings.hasSibling() ? this.getOwnerRootSib(this.siblings.getSibling()) : (sibling == null ? this.getOwnerRootNoSib() : this.getOwnerRootSib(sibling.toUri().toURL()));
        }
        catch (MalformedURLException ex) {
            Exceptions.printStackTrace((Throwable)ex);
            return null;
        }
    }

    private URL getOwnerRootSib(URL sibling) throws MalformedURLException {
        URL rootURL;
        assert (sibling != null);
        for (ClassPath.Entry entry2 : this.userRoots.entries()) {
            rootURL = entry2.getURL();
            if (!FileObjects.isParentOf(rootURL, sibling)) continue;
            return rootURL;
        }
        for (ClassPath.Entry entry2 : this.sourceRoots.entries()) {
            rootURL = entry2.getURL();
            if (!FileObjects.isParentOf(rootURL, sibling)) continue;
            return rootURL;
        }
        return null;
    }

    private URL getOwnerRootNoSib() {
        List entries = this.userRoots.entries();
        return entries.size() == 1 ? ((ClassPath.Entry)entries.get(0)).getURL() : null;
    }

    private String noAptRootDebug(javax.tools.FileObject sibling) {
        StringBuilder sb = new StringBuilder("No apt root for source root: ");
        sb.append(this.getOwnerRoot(sibling));
        sb.append(" sibling: ");
        if (this.siblings.hasSibling()) {
            sb.append(this.siblings.getSibling());
        } else if (sibling != null) {
            sb.append(sibling.toUri());
        } else {
            sb.append("none");
        }
        return sb.toString();
    }
}

