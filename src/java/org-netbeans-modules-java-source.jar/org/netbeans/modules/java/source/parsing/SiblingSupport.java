/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Pair
 */
package org.netbeans.modules.java.source.parsing;

import java.net.URL;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.java.source.parsing.SiblingProvider;
import org.netbeans.modules.java.source.parsing.SiblingSource;
import org.openide.util.Pair;

public final class SiblingSupport
implements SiblingSource {
    private static final Logger LOG = Logger.getLogger(SiblingSupport.class.getName());
    private final Stack<Pair<URL, Boolean>> siblings = new Stack();
    private final SiblingProvider provider;

    private SiblingSupport() {
        this.provider = new Provider();
    }

    @Override
    public void push(@NonNull URL sibling, boolean inSourceRoot) {
        assert (sibling != null);
        this.siblings.push((Pair)Pair.of((Object)sibling, (Object)inSourceRoot));
        LOG.log(Level.FINE, "Pushed sibling: {0} size: {1}", new Object[]{sibling, this.siblings.size()});
    }

    @Override
    public URL pop() {
        Pair<URL, Boolean> removed = this.siblings.pop();
        if (LOG.isLoggable(Level.FINEST)) {
            StackTraceElement[] td = Thread.currentThread().getStackTrace();
            LOG.log(Level.FINEST, "Poped sibling: {0} size: {1} caller:\n{2}", new Object[]{removed, this.siblings.size(), SiblingSupport.formatCaller(td)});
        } else {
            LOG.log(Level.FINE, "Poped sibling: {0} size: {1}", new Object[]{removed, this.siblings.size()});
        }
        return (URL)removed.first();
    }

    @Override
    public SiblingProvider getProvider() {
        return this.provider;
    }

    public static SiblingSource create() {
        return new SiblingSupport();
    }

    private static String formatCaller(StackTraceElement[] elements) {
        StringBuilder sb = new StringBuilder();
        for (StackTraceElement element : elements) {
            sb.append(String.format("%s.%s (%s:%d)\n", element.getClassName(), element.getMethodName(), element.getFileName(), element.getLineNumber()));
        }
        return sb.toString();
    }

    private final class Provider
    implements SiblingProvider {
        private Provider() {
        }

        @Override
        public URL getSibling() {
            Pair result = (Pair)SiblingSupport.this.siblings.peek();
            LOG.log(Level.FINER, "Returns sibling: {0} in source root? {1}", new Object[]{result.first(), result.second()});
            return (URL)result.first();
        }

        @Override
        public boolean hasSibling() {
            boolean result = !SiblingSupport.this.siblings.isEmpty();
            LOG.log(Level.FINER, "Has sibling: {0}", new Object[]{result});
            return result;
        }

        @Override
        public boolean isInSourceRoot() {
            return (Boolean)((Pair)SiblingSupport.this.siblings.peek()).second();
        }
    }

}

