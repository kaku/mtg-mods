/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.swing.PositionRegion
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.java.source.parsing;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.lib.editor.util.swing.PositionRegion;
import org.openide.util.Exceptions;

public final class DocPositionRegion
extends PositionRegion {
    private final Reference<Document> doc;

    public DocPositionRegion(Document doc, int startPos, int endPos) throws BadLocationException {
        super(doc, startPos, endPos);
        assert (doc != null);
        this.doc = new WeakReference<Document>(doc);
    }

    public String getText() {
        final String[] result = new String[1];
        final Document doc = this.doc.get();
        if (doc != null) {
            doc.render(new Runnable(){

                @Override
                public void run() {
                    try {
                        result[0] = doc.getText(DocPositionRegion.this.getStartOffset(), DocPositionRegion.this.getLength());
                    }
                    catch (BadLocationException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
            });
        }
        return result[0];
    }

}

