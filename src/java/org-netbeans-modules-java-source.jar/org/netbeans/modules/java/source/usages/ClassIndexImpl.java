/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.document.Document
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.modules.parsing.lucene.support.Convertor
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.Pair
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.source.usages;

import java.io.IOException;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.TypeElement;
import org.apache.lucene.document.Document;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.source.ClassIndex;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.modules.java.source.usages.BinaryAnalyser;
import org.netbeans.modules.java.source.usages.ClassIndexFactory;
import org.netbeans.modules.java.source.usages.ClassIndexImplEvent;
import org.netbeans.modules.java.source.usages.ClassIndexImplListener;
import org.netbeans.modules.java.source.usages.SourceAnalyzerFactory;
import org.netbeans.modules.parsing.lucene.support.Convertor;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.Pair;
import org.openide.util.Utilities;

public abstract class ClassIndexImpl {
    public static final ThreadLocal<AtomicBoolean> cancel = new ThreadLocal<T>();
    public static ClassIndexFactory FACTORY;
    private static final Logger LOG;
    private State state = State.NEW;
    private final List<WeakReference<ClassIndexImplListener>> listeners = Collections.synchronizedList(new ArrayList());

    public abstract <T> void search(@NonNull ElementHandle<?> var1, @NonNull Set<? extends UsageType> var2, @NonNull Set<? extends ClassIndex.SearchScopeType> var3, @NonNull Convertor<? super Document, T> var4, @NonNull Set<? super T> var5) throws IOException, InterruptedException;

    public abstract <T> void getDeclaredTypes(@NonNull String var1, @NonNull ClassIndex.NameKind var2, @NonNull Set<? extends ClassIndex.SearchScopeType> var3, @NonNull Convertor<? super Document, T> var4, @NonNull Collection<? super T> var5) throws IOException, InterruptedException;

    public abstract <T> void getDeclaredElements(String var1, ClassIndex.NameKind var2, Convertor<? super Document, T> var3, Map<T, Set<String>> var4) throws IOException, InterruptedException;

    public abstract void getPackageNames(String var1, boolean var2, Set<String> var3) throws IOException, InterruptedException;

    public abstract void getReferencesFrequences(@NonNull Map<String, Integer> var1, @NonNull Map<String, Integer> var2) throws IOException, InterruptedException;

    public abstract FileObject[] getSourceRoots();

    public abstract BinaryAnalyser getBinaryAnalyser();

    public abstract SourceAnalyzerFactory.StorableAnalyzer getSourceAnalyser();

    public abstract String getSourceName(String var1) throws IOException, InterruptedException;

    public abstract void setDirty(URL var1);

    public abstract boolean isValid();

    public abstract Type getType();

    protected abstract void close() throws IOException;

    public void addClassIndexImplListener(ClassIndexImplListener listener) {
        assert (listener != null);
        this.listeners.add(new Ref(listener));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeClassIndexImplListener(ClassIndexImplListener listener) {
        assert (listener != null);
        List<WeakReference<ClassIndexImplListener>> list = this.listeners;
        synchronized (list) {
            Iterator<WeakReference<ClassIndexImplListener>> it = this.listeners.iterator();
            while (it.hasNext()) {
                WeakReference<ClassIndexImplListener> lr = it.next();
                ClassIndexImplListener l = lr.get();
                if (listener != l) continue;
                it.remove();
            }
        }
    }

    void typesEvent(@NonNull Collection<? extends ElementHandle<TypeElement>> added, @NonNull Collection<? extends ElementHandle<TypeElement>> removed, @NonNull Collection<? extends ElementHandle<TypeElement>> changed) {
        ClassIndexImplEvent a = added == null || added.isEmpty() ? null : new ClassIndexImplEvent(this, added);
        ClassIndexImplEvent r = removed == null || removed.isEmpty() ? null : new ClassIndexImplEvent(this, removed);
        ClassIndexImplEvent ch = changed == null || changed.isEmpty() ? null : new ClassIndexImplEvent(this, changed);
        this.typesEvent(a, r, ch);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void typesEvent(ClassIndexImplEvent added, ClassIndexImplEvent removed, ClassIndexImplEvent changed) {
        WeakReference[] _listeners;
        List<WeakReference<ClassIndexImplListener>> list = this.listeners;
        synchronized (list) {
            _listeners = this.listeners.toArray(new WeakReference[this.listeners.size()]);
        }
        for (WeakReference lr : _listeners) {
            ClassIndexImplListener l = (ClassIndexImplListener)lr.get();
            if (l == null) continue;
            if (added != null) {
                l.typesAdded(added);
            }
            if (removed != null) {
                l.typesRemoved(removed);
            }
            if (changed == null) continue;
            l.typesChanged(changed);
        }
    }

    public final State getState() {
        return this.state;
    }

    public final void setState(State state) {
        assert (state != null);
        assert (this.state != null);
        if (state.ordinal() < this.state.ordinal()) {
            throw new IllegalArgumentException();
        }
        this.state = state;
    }

    @CheckForNull
    protected final <R, E extends Exception> R handleException(@NullAllowed R ret, @NonNull E e, @NullAllowed URL root) throws Exception {
        if (State.NEW == this.getState()) {
            LOG.log(Level.FINE, "Exception from non initialized index", (Throwable)e);
            return ret;
        }
        throw (Exception)Exceptions.attachMessage(e, (String)("Index state: " + (Object)((Object)this.state) + ", Root: " + root));
    }

    static {
        LOG = Logger.getLogger(ClassIndexImpl.class.getName());
    }

    private class Ref
    extends WeakReference<ClassIndexImplListener>
    implements Runnable {
        public Ref(ClassIndexImplListener listener) {
            super(listener, Utilities.activeReferenceQueue());
        }

        @Override
        public void run() {
            ClassIndexImpl.this.listeners.remove(this);
        }
    }

    public static interface Writer {
        public void clear() throws IOException;

        public void deleteAndStore(List<Pair<Pair<String, String>, Object[]>> var1, Set<Pair<String, String>> var2) throws IOException;

        public void deleteAndFlush(List<Pair<Pair<String, String>, Object[]>> var1, Set<Pair<String, String>> var2) throws IOException;

        public void commit() throws IOException;

        public void rollback() throws IOException;
    }

    public static enum Type {
        EMPTY,
        SOURCE,
        BINARY;
        

        private Type() {
        }
    }

    public static enum UsageType {
        SUPER_CLASS,
        SUPER_INTERFACE,
        FIELD_REFERENCE,
        METHOD_REFERENCE,
        TYPE_REFERENCE;
        

        private UsageType() {
        }
    }

    public static enum State {
        NEW,
        INITIALIZED;
        

        private State() {
        }
    }

}

