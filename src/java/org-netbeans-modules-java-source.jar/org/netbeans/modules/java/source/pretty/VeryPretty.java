/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.doctree.AttributeTree
 *  com.sun.source.doctree.AttributeTree$ValueKind
 *  com.sun.source.doctree.AuthorTree
 *  com.sun.source.doctree.CommentTree
 *  com.sun.source.doctree.DeprecatedTree
 *  com.sun.source.doctree.DocCommentTree
 *  com.sun.source.doctree.DocRootTree
 *  com.sun.source.doctree.DocTree
 *  com.sun.source.doctree.DocTree$Kind
 *  com.sun.source.doctree.DocTreeVisitor
 *  com.sun.source.doctree.EndElementTree
 *  com.sun.source.doctree.EntityTree
 *  com.sun.source.doctree.ErroneousTree
 *  com.sun.source.doctree.IdentifierTree
 *  com.sun.source.doctree.InheritDocTree
 *  com.sun.source.doctree.LinkTree
 *  com.sun.source.doctree.LiteralTree
 *  com.sun.source.doctree.ParamTree
 *  com.sun.source.doctree.ReferenceTree
 *  com.sun.source.doctree.ReturnTree
 *  com.sun.source.doctree.SeeTree
 *  com.sun.source.doctree.SerialDataTree
 *  com.sun.source.doctree.SerialFieldTree
 *  com.sun.source.doctree.SerialTree
 *  com.sun.source.doctree.SinceTree
 *  com.sun.source.doctree.StartElementTree
 *  com.sun.source.doctree.TextTree
 *  com.sun.source.doctree.ThrowsTree
 *  com.sun.source.doctree.UnknownBlockTagTree
 *  com.sun.source.doctree.UnknownInlineTagTree
 *  com.sun.source.doctree.ValueTree
 *  com.sun.source.doctree.VersionTree
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.CaseTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.LambdaExpressionTree
 *  com.sun.source.tree.LambdaExpressionTree$BodyKind
 *  com.sun.source.tree.MemberReferenceTree
 *  com.sun.source.tree.MemberReferenceTree$ReferenceMode
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.StatementTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.DocSourcePositions
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreeScanner
 *  com.sun.source.util.Trees
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.api.JavacTrees
 *  com.sun.tools.javac.code.BoundKind
 *  com.sun.tools.javac.code.Flags
 *  com.sun.tools.javac.code.Flags$Flag
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$MethodSymbol
 *  com.sun.tools.javac.code.Symbol$PackageSymbol
 *  com.sun.tools.javac.code.Symbol$TypeSymbol
 *  com.sun.tools.javac.code.Symtab
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.code.Type$TypeVar
 *  com.sun.tools.javac.code.TypeTag
 *  com.sun.tools.javac.code.Types
 *  com.sun.tools.javac.main.JavaCompiler
 *  com.sun.tools.javac.tree.DCTree
 *  com.sun.tools.javac.tree.DCTree$DCReference
 *  com.sun.tools.javac.tree.EndPosTable
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCAnnotatedType
 *  com.sun.tools.javac.tree.JCTree$JCAnnotation
 *  com.sun.tools.javac.tree.JCTree$JCArrayAccess
 *  com.sun.tools.javac.tree.JCTree$JCArrayTypeTree
 *  com.sun.tools.javac.tree.JCTree$JCAssert
 *  com.sun.tools.javac.tree.JCTree$JCAssign
 *  com.sun.tools.javac.tree.JCTree$JCAssignOp
 *  com.sun.tools.javac.tree.JCTree$JCBinary
 *  com.sun.tools.javac.tree.JCTree$JCBlock
 *  com.sun.tools.javac.tree.JCTree$JCBreak
 *  com.sun.tools.javac.tree.JCTree$JCCase
 *  com.sun.tools.javac.tree.JCTree$JCCatch
 *  com.sun.tools.javac.tree.JCTree$JCClassDecl
 *  com.sun.tools.javac.tree.JCTree$JCCompilationUnit
 *  com.sun.tools.javac.tree.JCTree$JCConditional
 *  com.sun.tools.javac.tree.JCTree$JCContinue
 *  com.sun.tools.javac.tree.JCTree$JCDoWhileLoop
 *  com.sun.tools.javac.tree.JCTree$JCEnhancedForLoop
 *  com.sun.tools.javac.tree.JCTree$JCErroneous
 *  com.sun.tools.javac.tree.JCTree$JCExpression
 *  com.sun.tools.javac.tree.JCTree$JCExpressionStatement
 *  com.sun.tools.javac.tree.JCTree$JCFieldAccess
 *  com.sun.tools.javac.tree.JCTree$JCForLoop
 *  com.sun.tools.javac.tree.JCTree$JCIdent
 *  com.sun.tools.javac.tree.JCTree$JCIf
 *  com.sun.tools.javac.tree.JCTree$JCImport
 *  com.sun.tools.javac.tree.JCTree$JCInstanceOf
 *  com.sun.tools.javac.tree.JCTree$JCLabeledStatement
 *  com.sun.tools.javac.tree.JCTree$JCLambda
 *  com.sun.tools.javac.tree.JCTree$JCLambda$ParameterKind
 *  com.sun.tools.javac.tree.JCTree$JCLiteral
 *  com.sun.tools.javac.tree.JCTree$JCMemberReference
 *  com.sun.tools.javac.tree.JCTree$JCMethodDecl
 *  com.sun.tools.javac.tree.JCTree$JCMethodInvocation
 *  com.sun.tools.javac.tree.JCTree$JCModifiers
 *  com.sun.tools.javac.tree.JCTree$JCNewArray
 *  com.sun.tools.javac.tree.JCTree$JCNewClass
 *  com.sun.tools.javac.tree.JCTree$JCParens
 *  com.sun.tools.javac.tree.JCTree$JCPrimitiveTypeTree
 *  com.sun.tools.javac.tree.JCTree$JCReturn
 *  com.sun.tools.javac.tree.JCTree$JCSkip
 *  com.sun.tools.javac.tree.JCTree$JCStatement
 *  com.sun.tools.javac.tree.JCTree$JCSwitch
 *  com.sun.tools.javac.tree.JCTree$JCSynchronized
 *  com.sun.tools.javac.tree.JCTree$JCThrow
 *  com.sun.tools.javac.tree.JCTree$JCTry
 *  com.sun.tools.javac.tree.JCTree$JCTypeApply
 *  com.sun.tools.javac.tree.JCTree$JCTypeCast
 *  com.sun.tools.javac.tree.JCTree$JCTypeParameter
 *  com.sun.tools.javac.tree.JCTree$JCTypeUnion
 *  com.sun.tools.javac.tree.JCTree$JCUnary
 *  com.sun.tools.javac.tree.JCTree$JCVariableDecl
 *  com.sun.tools.javac.tree.JCTree$JCWhileLoop
 *  com.sun.tools.javac.tree.JCTree$JCWildcard
 *  com.sun.tools.javac.tree.JCTree$LetExpr
 *  com.sun.tools.javac.tree.JCTree$Tag
 *  com.sun.tools.javac.tree.JCTree$TypeBoundKind
 *  com.sun.tools.javac.tree.JCTree$Visitor
 *  com.sun.tools.javac.tree.TreeInfo
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Convert
 *  com.sun.tools.javac.util.List
 *  com.sun.tools.javac.util.Name
 *  com.sun.tools.javac.util.Names
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.lexer.JavaTokenId
 *  org.netbeans.api.java.platform.JavaPlatform
 *  org.netbeans.api.java.platform.JavaPlatformManager
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.java.source.pretty;

import com.sun.source.doctree.AttributeTree;
import com.sun.source.doctree.AuthorTree;
import com.sun.source.doctree.CommentTree;
import com.sun.source.doctree.DeprecatedTree;
import com.sun.source.doctree.DocCommentTree;
import com.sun.source.doctree.DocRootTree;
import com.sun.source.doctree.DocTree;
import com.sun.source.doctree.DocTreeVisitor;
import com.sun.source.doctree.EndElementTree;
import com.sun.source.doctree.EntityTree;
import com.sun.source.doctree.ErroneousTree;
import com.sun.source.doctree.IdentifierTree;
import com.sun.source.doctree.InheritDocTree;
import com.sun.source.doctree.LinkTree;
import com.sun.source.doctree.LiteralTree;
import com.sun.source.doctree.ParamTree;
import com.sun.source.doctree.ReferenceTree;
import com.sun.source.doctree.ReturnTree;
import com.sun.source.doctree.SeeTree;
import com.sun.source.doctree.SerialDataTree;
import com.sun.source.doctree.SerialFieldTree;
import com.sun.source.doctree.SerialTree;
import com.sun.source.doctree.SinceTree;
import com.sun.source.doctree.StartElementTree;
import com.sun.source.doctree.TextTree;
import com.sun.source.doctree.ThrowsTree;
import com.sun.source.doctree.UnknownBlockTagTree;
import com.sun.source.doctree.UnknownInlineTagTree;
import com.sun.source.doctree.ValueTree;
import com.sun.source.doctree.VersionTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.CaseTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.LambdaExpressionTree;
import com.sun.source.tree.MemberReferenceTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.DocSourcePositions;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreeScanner;
import com.sun.source.util.Trees;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.api.JavacTrees;
import com.sun.tools.javac.code.BoundKind;
import com.sun.tools.javac.code.Flags;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Symtab;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.TypeTag;
import com.sun.tools.javac.code.Types;
import com.sun.tools.javac.main.JavaCompiler;
import com.sun.tools.javac.tree.DCTree;
import com.sun.tools.javac.tree.EndPosTable;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeInfo;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.Convert;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.tools.JavaFileObject;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.lexer.JavaTokenId;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.api.java.platform.JavaPlatformManager;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CodeStyle;
import org.netbeans.api.java.source.Comment;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.java.source.builder.CommentHandlerService;
import org.netbeans.modules.java.source.builder.CommentSetImpl;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.JavacParser;
import org.netbeans.modules.java.source.pretty.CharBuffer;
import org.netbeans.modules.java.source.pretty.DanglingElseChecker;
import org.netbeans.modules.java.source.pretty.WidthEstimator;
import org.netbeans.modules.java.source.query.CommentHandler;
import org.netbeans.modules.java.source.query.CommentSet;
import org.netbeans.modules.java.source.save.CasualDiff;
import org.netbeans.modules.java.source.save.DiffContext;
import org.netbeans.modules.java.source.save.PositionEstimator;
import org.netbeans.modules.java.source.save.Reformatter;
import org.netbeans.modules.java.source.transform.FieldGroupTree;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.openide.util.Exceptions;

public final class VeryPretty
extends JCTree.Visitor
implements DocTreeVisitor<Void, Void> {
    private static final char[] hex = "0123456789ABCDEF".toCharArray();
    private static final String REPLACEMENT = "%[a-z]*%";
    private static final String ERROR = "<error>";
    private final CodeStyle cs;
    public final CharBuffer out;
    private final Names names;
    private final CommentHandler commentHandler;
    private final Symtab symbols;
    private final Types types;
    private final TreeInfo treeinfo;
    private final WidthEstimator widthEstimator;
    private final DanglingElseChecker danglingElseChecker;
    public boolean suppressVariableType;
    public Name enclClassName;
    private int indentSize;
    private int prec;
    private boolean printingMethodParams;
    private DiffContext diffContext;
    private CommentHandlerService comments;
    private int fromOffset = -1;
    private int toOffset = -1;
    private boolean containsError = false;
    private boolean insideAnnotation = false;
    private final Map<Tree, ?> tree2Tag;
    private final Map<Tree, DocCommentTree> tree2Doc;
    private final Map<Object, int[]> tag2Span;
    private final String origText;
    private int initialOffset = 0;
    private Map<JCTree, Integer> overrideStartPositions;
    public Set<Tree> oldTrees = Collections.emptySet();
    public Set<int[]> reindentRegions;
    private boolean commentsEnabled;
    private final Set<Tree> trailingCommentsHandled;
    private final Set<Tree> innerCommentsHandled;
    private static final Logger LOG = Logger.getLogger(CasualDiff.class.getName());
    private static final String[] typeTagNames = new String[TypeTag.values().length];
    private boolean reallyPrintAnnotations;
    private static final String[] flagLowerCaseNames;
    public int conditionStartHack;
    private Set<Tree> precedingCommentsHandled;

    public VeryPretty(DiffContext diffContext) {
        this(diffContext, diffContext.style, null, null, null, null);
    }

    public VeryPretty(DiffContext diffContext, CodeStyle cs) {
        this(diffContext, cs, null, null, null, null);
    }

    public VeryPretty(DiffContext diffContext, CodeStyle cs, Map<Tree, ?> tree2Tag, Map<Tree, DocCommentTree> tree2Doc, Map<?, int[]> tag2Span, String origText) {
        this(diffContext.context, cs, tree2Tag, tree2Doc, tag2Span, origText);
        this.diffContext = diffContext;
    }

    public VeryPretty(DiffContext diffContext, CodeStyle cs, Map<Tree, ?> tree2Tag, Map<Tree, DocCommentTree> tree2Doc, Map<?, int[]> tag2Span, String origText, int initialOffset) {
        this(diffContext, cs, tree2Tag, tree2Doc, tag2Span, origText);
        this.initialOffset = initialOffset;
    }

    private VeryPretty(Context context, CodeStyle cs, Map<Tree, ?> tree2Tag, Map<Tree, DocCommentTree> tree2Doc, Map<?, int[]> tag2Span, String origText) {
        this.reindentRegions = new TreeSet<int[]>(new Comparator<int[]>(){

            @Override
            public int compare(int[] o1, int[] o2) {
                return o1[0] - o2[0];
            }
        });
        this.trailingCommentsHandled = Collections.newSetFromMap(new IdentityHashMap());
        this.innerCommentsHandled = Collections.newSetFromMap(new IdentityHashMap());
        this.conditionStartHack = -1;
        this.precedingCommentsHandled = new HashSet<Tree>();
        this.names = Names.instance((Context)context);
        this.enclClassName = this.names.empty;
        this.commentHandler = CommentHandlerService.instance(context);
        this.symbols = Symtab.instance((Context)context);
        this.types = Types.instance((Context)context);
        this.treeinfo = TreeInfo.instance((Context)context);
        this.widthEstimator = new WidthEstimator(context);
        this.danglingElseChecker = new DanglingElseChecker();
        this.prec = -1;
        this.cs = cs;
        this.out = new CharBuffer(cs.getRightMargin(), cs.getTabSize(), cs.expandTabToSpaces());
        this.indentSize = cs.getIndentSize();
        this.tree2Tag = tree2Tag;
        this.tree2Doc = tree2Doc == null ? Collections.EMPTY_MAP : tree2Doc;
        this.tag2Span = tag2Span;
        this.origText = origText;
        this.comments = CommentHandlerService.instance(context);
    }

    public void setInitialOffset(int offset) {
        this.initialOffset = offset < 0 ? 0 : offset;
    }

    public int getInitialOffset() {
        return this.initialOffset;
    }

    public String toString() {
        return this.out.toString();
    }

    public void toLeftMargin() {
        this.out.toLeftMargin();
    }

    public void reset(int margin, int col) {
        this.out.setLength(0);
        this.out.leftMargin = margin;
        this.out.col = col;
    }

    public int getIndent() {
        return this.out.leftMargin;
    }

    public void setIndent(int indent) {
        this.out.leftMargin = indent;
    }

    public int indent() {
        int old = this.out.leftMargin;
        this.out.leftMargin = old + this.indentSize;
        return old;
    }

    public void undent(int old) {
        this.out.leftMargin = old;
    }

    public void newline() {
        this.out.nlTerm();
    }

    public void blankline() {
        this.out.blanklines(1);
    }

    public int setPrec(int prec) {
        int old = this.prec;
        this.prec = prec;
        return old;
    }

    public final void print(String s) {
        if (s == null) {
            return;
        }
        this.out.append(s);
    }

    public final void print(Name n) {
        if (n == null) {
            return;
        }
        this.out.appendUtf8(n.getByteArray(), n.getByteOffset(), n.getByteLength());
    }

    private void print(javax.lang.model.element.Name n) {
        if (n == null) {
            return;
        }
        this.print(n.toString());
    }

    public void print(JCTree t) {
        if (t == null) {
            return;
        }
        this.blankLines(t, true);
        this.toLeftMargin();
        this.doAccept(t, true);
        this.blankLines(t, false);
    }

    public void print(DCTree t) {
        if (t == null) {
            return;
        }
        this.blankLines(t, true);
        this.toLeftMargin();
        this.doAccept(t);
        this.blankLines(t, false);
    }

    private int getOldPos(JCTree oldT) {
        Integer i;
        if (this.overrideStartPositions != null && (i = this.overrideStartPositions.get((Object)oldT)) != null) {
            return i;
        }
        return TreeInfo.getStartPos((JCTree)oldT);
    }

    public int endPos(JCTree t) {
        return TreeInfo.getEndPos((JCTree)t, (EndPosTable)this.diffContext.origUnit.endPositions);
    }

    private java.util.List<? extends StatementTree> getStatements(Tree tree) {
        switch (tree.getKind()) {
            case BLOCK: {
                return ((BlockTree)tree).getStatements();
            }
            case CASE: {
                return ((CaseTree)tree).getStatements();
            }
        }
        return null;
    }

    private java.util.List<JCTree.JCVariableDecl> printOriginalPartOfFieldGroup(FieldGroupTree fgt) {
        Tree t;
        TreePath parent;
        java.util.List<JCTree.JCVariableDecl> variables = fgt.getVariables();
        TreePath tp = TreePath.getPath((CompilationUnitTree)this.diffContext.origUnit, (Tree)((Tree)variables.get(0)));
        TreePath treePath = parent = tp != null ? tp.getParentPath() : null;
        if (parent == null) {
            return variables;
        }
        java.util.List<? extends StatementTree> statements = this.getStatements(parent.getLeaf());
        if (statements == null) {
            return variables;
        }
        JCTree.JCVariableDecl firstDecl = fgt.getVariables().get(0);
        int startIndex = statements.indexOf((Object)firstDecl);
        if (startIndex < 0) {
            return variables;
        }
        int origCount = 0;
        int s = statements.size();
        for (JCTree t2 : variables) {
            if (startIndex >= s || statements.get(startIndex++) != t2) break;
            ++origCount;
        }
        if (origCount < 2) {
            return variables;
        }
        int firstPos = this.getOldPos((JCTree)firstDecl);
        int groupStart = startIndex;
        while (--groupStart > 0 && (t = (Tree)statements.get((int)(groupStart - true))) instanceof JCTree.JCVariableDecl && this.getOldPos((JCTree)((JCTree.JCVariableDecl)t)) == firstPos) {
        }
        int firstIdentStart = ((JCTree)statements.get((int)groupStart)).pos;
        if (groupStart < startIndex) {
            this.copyToIndented(firstPos, firstIdentStart);
            IdentityHashMap<JCTree, Integer> m = new IdentityHashMap<JCTree, Integer>(origCount);
            for (int i = 0; i < origCount; ++i) {
                m.put((JCTree)variables.get(i), variables.get((int)i).pos);
            }
            this.overrideStartPositions = m;
        }
        this.doPrintOriginalTree(variables.subList(0, origCount), true);
        this.overrideStartPositions = null;
        return variables.subList(origCount, variables.size());
    }

    private void doAccept(JCTree t, boolean printComments) {
        if (!this.handlePossibleOldTrees(Collections.singletonList(t), printComments)) {
            Object tag;
            if (printComments) {
                this.printPrecedingComments(t, true);
            }
            int start = this.toString().length();
            if (t instanceof FieldGroupTree) {
                FieldGroupTree fgt = (FieldGroupTree)t;
                if (fgt.isEnum()) {
                    this.printEnumConstants((java.util.List<? extends JCTree>)List.from((Object[])fgt.getVariables().toArray((T[])new JCTree[0])), !fgt.isEnum() || fgt.moreElementsFollowEnum());
                } else {
                    java.util.List<JCTree.JCVariableDecl> remainder = this.printOriginalPartOfFieldGroup(fgt);
                    boolean firstMember = remainder.size() == fgt.getVariables().size();
                    for (JCTree.JCVariableDecl var : remainder) {
                        this.oldTrees.remove((Object)var);
                        assert (!VeryPretty.isEnumerator((JCTree)var));
                        assert (!this.isSynthetic((JCTree)var));
                        this.printStat((JCTree)var, true, firstMember, true, true);
                        firstMember = false;
                    }
                }
            } else {
                boolean saveComments = this.commentsEnabled;
                this.commentsEnabled = printComments;
                t.accept((JCTree.Visitor)this);
                this.commentsEnabled = saveComments;
            }
            int end = this.toString().length();
            Object k = tag = this.tree2Tag != null ? (Object)this.tree2Tag.get((Object)t) : null;
            if (tag != null) {
                this.tag2Span.put(tag, new int[]{start + this.initialOffset, end + this.initialOffset});
            }
            if (printComments) {
                this.printInnerCommentsAsTrailing(t, true);
                this.printTrailingComments(t, true);
            }
        }
    }

    private void doAccept(DCTree t) {
        t.accept((DocTreeVisitor)this, (Object)null);
    }

    public boolean handlePossibleOldTrees(java.util.List<? extends JCTree> toPrint, boolean includeComments) {
        for (JCTree t : toPrint) {
            if (!this.oldTrees.contains((Object)t)) {
                return false;
            }
            if (t.getKind() != Tree.Kind.ARRAY_TYPE) continue;
            return false;
        }
        if (toPrint.size() > 1) {
            TreePath tp = TreePath.getPath((CompilationUnitTree)this.diffContext.mainUnit, (Tree)((Tree)toPrint.get(0)));
            TreePath parent = tp.getParentPath();
            if (parent == null) {
                return false;
            }
            java.util.List<? extends StatementTree> statements = this.getStatements(parent.getLeaf());
            if (statements == null) {
                return false;
            }
            int startIndex = statements.indexOf((Object)toPrint.get(0));
            if (startIndex < 0) {
                return false;
            }
            for (JCTree t2 : toPrint) {
                if (statements.get(startIndex++) == t2) continue;
                return false;
            }
        }
        this.doPrintOriginalTree(toPrint, includeComments);
        return true;
    }

    private void doPrintOriginalTree(java.util.List<? extends JCTree> toPrint, final boolean includeComments) {
        if (this.out.isWhitespaceLine()) {
            this.toLeftMargin();
        }
        JCTree firstTree = toPrint.get(0);
        JCTree lastTree = toPrint.get(toPrint.size() - 1);
        CommentSet old = this.commentHandler.getComments((Tree)firstTree);
        final int realStart = includeComments ? Math.min(this.getOldPos(firstTree), CasualDiff.commentStart(this.diffContext, old, CommentSet.RelativePosition.PRECEDING, this.getOldPos(firstTree))) : this.getOldPos(firstTree);
        final int newStart = this.toString().length() + this.initialOffset;
        final int[] realEnd = new int[]{this.endPos(lastTree)};
        new TreeScanner<Void, Void>(){

            public Void scan(Tree node, Void p) {
                if (node != null) {
                    Object tag;
                    CommentSetImpl old = VeryPretty.this.comments.getComments(node);
                    if (includeComments) {
                        realEnd[0] = Math.max(realEnd[0], Math.max(CasualDiff.commentEnd(old, CommentSet.RelativePosition.INLINE), CasualDiff.commentEnd(old, CommentSet.RelativePosition.TRAILING)));
                        VeryPretty.this.trailingCommentsHandled.add(node);
                    }
                    Object k = tag = VeryPretty.this.tree2Tag != null ? (Object)VeryPretty.this.tree2Tag.get((Object)node) : null;
                    if (tag != null) {
                        int s = VeryPretty.this.getOldPos((JCTree)node);
                        int e = VeryPretty.this.endPos((JCTree)node);
                        VeryPretty.this.tag2Span.put(tag, new int[]{s - realStart + newStart, e - realStart + newStart});
                    }
                }
                return (Void)TreeScanner.super.scan(node, (Object)p);
            }
        }.scan((Tree)lastTree, null);
        this.copyToIndented(realStart, realEnd[0]);
    }

    private void copyToIndented(int from, int to) {
        if (from == to) {
            return;
        }
        if (from > to || from < 0 || to < 0) {
            LOG.log(Level.INFO, "-----\n" + this.origText + "-----\n");
            LOG.log(Level.INFO, "Illegal values: from = " + from + "; to = " + to + "." + "Please, attach your messages.log to new issue!");
            if (to >= 0) {
                this.eatChars(from - to);
            }
            return;
        }
        if (to > this.origText.length()) {
            LOG.severe("-----\n" + this.origText + "-----\n");
            throw new IllegalArgumentException("Copying to " + to + " is greater then its size (" + this.origText.length() + ").");
        }
        String text = this.origText.substring(from, to);
        int newLine = text.indexOf("\n") + 1;
        boolean wasWhitespaceLine = this.out.isWhitespaceLine();
        if (newLine == 0 && !wasWhitespaceLine) {
            this.print(text);
        } else {
            int start = this.toString().length();
            this.print(text);
            int end = start + text.length();
            int[] arrn = new int[2];
            arrn[0] = this.initialOffset + start + (wasWhitespaceLine ? 0 : newLine);
            arrn[1] = this.initialOffset + end;
            this.reindentRegions.add(arrn);
        }
    }

    public void printPackage(JCTree.JCExpression pid) {
        if (pid != null) {
            this.blankLines(this.cs.getBlankLinesBeforePackage());
            this.print("package ");
            this.printExpr((JCTree)pid);
            this.print(';');
            this.blankLines(this.cs.getBlankLinesAfterPackage());
        }
    }

    public String getMethodHeader(MethodTree t, String s) {
        JCTree.JCMethodDecl tree = (JCTree.JCMethodDecl)t;
        this.printAnnotations(tree.mods.annotations);
        s = this.replace(s, "%annotations");
        this.printFlags(tree.mods.flags);
        s = this.replace(s, "%flags%");
        if (tree.name == this.names.init) {
            this.print(this.enclClassName);
            s = this.replace(s, "%name%");
        } else {
            if (tree.typarams != null) {
                this.printTypeParameters(tree.typarams);
                this.needSpace();
                s = this.replace(s, "%typeparameters%");
            }
            this.print((JCTree)tree.restype);
            s = this.replace(s, "%type%");
            this.out.clear();
            this.print(tree.name);
            s = this.replace(s, "%name%");
        }
        this.print('(');
        this.wrapTrees(tree.params, CodeStyle.WrapStyle.WRAP_NEVER, this.out.col);
        this.print(')');
        s = this.replace(s, "%parameters%");
        if (tree.thrown.nonEmpty()) {
            this.print(" throws ");
            this.wrapTrees(tree.thrown, CodeStyle.WrapStyle.WRAP_NEVER, this.out.col);
            s = this.replace(s, "%throws%");
        }
        return s.replaceAll("%[a-z]*%", "");
    }

    public String getClassHeader(ClassTree t, String s) {
        JCTree.JCClassDecl tree = (JCTree.JCClassDecl)t;
        this.printAnnotations(tree.mods.annotations);
        s = this.replace(s, "%annotations");
        long flags = tree.mods.flags;
        if ((flags & 16384) != 0) {
            this.printFlags(flags & -529);
        } else {
            this.printFlags(flags & -1537);
        }
        s = this.replace(s, "%flags%");
        if ((flags & 512) != 0) {
            this.print("interface ");
            this.print(tree.name);
            s = this.replace(s, "%name%");
            this.printTypeParameters(tree.typarams);
            s = this.replace(s, "%typeparameters%");
            if (tree.implementing.nonEmpty()) {
                this.print(" extends ");
                this.wrapTrees(tree.implementing, CodeStyle.WrapStyle.WRAP_NEVER, this.out.col);
                s = this.replace(s, "%extends%");
            }
        } else {
            if ((flags & 16384) != 0) {
                this.print("enum ");
            } else {
                if ((flags & 1024) != 0) {
                    this.print("abstract ");
                }
                this.print("class ");
            }
            this.print(tree.name);
            s = this.replace(s, "%name%");
            this.printTypeParameters(tree.typarams);
            s = this.replace(s, "%typeparameters%");
            if (tree.extending != null) {
                this.print(" extends ");
                this.print((JCTree)tree.extending);
                s = this.replace(s, "%extends%");
            }
            if (tree.implementing.nonEmpty()) {
                this.print(" implements ");
                this.wrapTrees(tree.implementing, CodeStyle.WrapStyle.WRAP_NEVER, this.out.col);
                s = this.replace(s, "%implements%");
            }
        }
        return s.replaceAll("%[a-z]*%", "");
    }

    public String getVariableHeader(VariableTree t, String s) {
        JCTree.JCVariableDecl tree = (JCTree.JCVariableDecl)t;
        this.printAnnotations(tree.mods.annotations);
        s = this.replace(s, "%annotations");
        this.printFlags(tree.mods.flags);
        s = this.replace(s, "%flags%");
        this.print((JCTree)tree.vartype);
        s = this.replace(s, "%type%");
        this.needSpace();
        this.print(tree.name);
        s = this.replace(s, "%name%");
        return s.replaceAll("%[a-z]*%", "");
    }

    public void visitTopLevel(JCTree.JCCompilationUnit tree) {
        this.printAnnotations(tree.getPackageAnnotations());
        this.printPackage(tree.pid);
        List l = tree.defs;
        ArrayList<JCTree.JCImport> imports = new ArrayList<JCTree.JCImport>();
        while (l.nonEmpty() && ((JCTree)l.head).getTag() == JCTree.Tag.IMPORT) {
            imports.add((JCTree.JCImport)l.head);
            l = l.tail;
        }
        this.printImportsBlock(imports, !l.isEmpty());
        while (l.nonEmpty()) {
            this.printStat((JCTree)l.head, true, false, false, true);
            l = l.tail;
        }
    }

    public void visitImport(JCTree.JCImport tree) {
        this.print("import ");
        if (tree.staticImport) {
            this.print("static ");
        }
        this.print(this.fullName(tree.qualid));
        this.print(';');
    }

    public void visitClassDef(JCTree.JCClassDecl tree) {
        int old;
        Name enclClassNamePrev = this.enclClassName;
        this.enclClassName = tree.name;
        this.toLeftMargin();
        this.printAnnotations(tree.mods.annotations);
        long flags = tree.mods.flags;
        if ((flags & 16384) != 0) {
            this.printFlags(flags & -529);
        } else {
            this.printFlags(flags & -1537);
        }
        if ((flags & 512) != 0 || (flags & 8192) != 0) {
            if ((flags & 8192) != 0) {
                this.print('@');
            }
            this.print("interface ");
            this.print(tree.name);
            this.printTypeParameters(tree.typarams);
            if (tree.implementing.nonEmpty()) {
                this.wrap("extends ", this.cs.wrapExtendsImplementsKeyword());
                this.wrapTrees(tree.implementing, this.cs.wrapExtendsImplementsList(), this.cs.alignMultilineImplements() ? this.out.col : this.out.leftMargin + this.cs.getContinuationIndentSize());
            }
        } else {
            if ((flags & 16384) != 0) {
                this.print("enum ");
            } else {
                if ((flags & 1024) != 0) {
                    this.print("abstract ");
                }
                this.print("class ");
            }
            this.print(tree.name);
            this.printTypeParameters(tree.typarams);
            if (tree.extending != null) {
                this.wrap("extends ", this.cs.wrapExtendsImplementsKeyword());
                this.print((JCTree)tree.extending);
            }
            if (tree.implementing.nonEmpty()) {
                this.wrap("implements ", this.cs.wrapExtendsImplementsKeyword());
                this.wrapTrees(tree.implementing, this.cs.wrapExtendsImplementsList(), this.cs.alignMultilineImplements() ? this.out.col : this.out.leftMargin + this.cs.getContinuationIndentSize());
            }
        }
        int bcol = old = this.cs.indentTopLevelClassMembers() ? this.indent() : this.out.leftMargin;
        switch (this.cs.getClassDeclBracePlacement()) {
            case NEW_LINE: {
                this.newline();
                this.toColExactly(old);
                break;
            }
            case NEW_LINE_HALF_INDENTED: {
                this.newline();
                this.toColExactly(bcol += this.indentSize >> 1);
                break;
            }
            case NEW_LINE_INDENTED: {
                this.newline();
                bcol = this.out.leftMargin;
                this.toColExactly(bcol);
            }
        }
        if (this.cs.spaceBeforeClassDeclLeftBrace()) {
            this.needSpace();
        }
        this.print('{');
        java.util.List<JCTree> members = CasualDiff.filterHidden(this.diffContext, tree.defs);
        if (!members.isEmpty()) {
            this.blankLines(this.enclClassName.isEmpty() ? this.cs.getBlankLinesAfterAnonymousClassHeader() : this.cs.getBlankLinesAfterClassHeader());
            boolean firstMember = true;
            if ((tree.mods.flags & 16384) != 0 && members.get(0) instanceof FieldGroupTree && ((FieldGroupTree)members.get(0)).isEnum()) {
                this.printEnumConstants(((FieldGroupTree)members.get(0)).getVariables(), false);
                firstMember = false;
                members.remove(0);
            }
            for (JCTree t : members) {
                this.printStat(t, true, firstMember, true, true);
                firstMember = false;
            }
            this.blankLines(this.enclClassName.isEmpty() ? this.cs.getBlankLinesBeforeAnonymousClassClosingBrace() : this.cs.getBlankLinesBeforeClassClosingBrace());
        } else {
            this.printEmptyBlockComments((JCTree)tree, false);
        }
        this.toColExactly(bcol);
        this.undent(old);
        this.print('}');
        this.enclClassName = enclClassNamePrev;
    }

    private void printEnumConstants(java.util.List<? extends JCTree> defs, boolean forceSemicolon) {
        boolean first = true;
        boolean hasNonEnumerator = false;
        for (JCTree c : defs) {
            if (VeryPretty.isEnumerator(c)) {
                boolean col = false;
                if (first) {
                    col = true;
                    first = false;
                } else {
                    this.print(this.cs.spaceBeforeComma() ? " ," : ",");
                    switch (this.cs.wrapEnumConstants()) {
                        case WRAP_IF_LONG: {
                            int rm = this.cs.getRightMargin();
                            if (this.widthEstimator.estimateWidth(c, rm - this.out.col) + this.out.col + 1 <= rm) {
                                if (!this.cs.spaceAfterComma()) break;
                                this.print(' ');
                                break;
                            }
                        }
                        case WRAP_ALWAYS: {
                            this.newline();
                            col = true;
                            break;
                        }
                        case WRAP_NEVER: {
                            if (!this.cs.spaceAfterComma()) break;
                            this.print(' ');
                        }
                    }
                }
                this.printStat(c, true, false, col, false);
                continue;
            }
            if (this.isSynthetic(c)) continue;
            hasNonEnumerator = true;
        }
        if (hasNonEnumerator || forceSemicolon) {
            this.print(";");
            this.newline();
        }
    }

    public void visitMethodDef(JCTree.JCMethodDecl tree) {
        if ((tree.mods.flags & 4096) == 0 && tree.name != this.names.init || this.enclClassName != null) {
            Name enclClassNamePrev = this.enclClassName;
            this.enclClassName = null;
            this.printAnnotations(tree.mods.annotations);
            this.printFlags(tree.mods.flags);
            if (tree.typarams != null) {
                this.printTypeParameters(tree.typarams);
                this.needSpace();
            }
            if (tree.name == this.names.init || tree.name.contentEquals((CharSequence)enclClassNamePrev)) {
                this.print(enclClassNamePrev);
            } else {
                this.print((JCTree)tree.restype);
                this.needSpace();
                this.print(tree.name);
            }
            this.print(this.cs.spaceBeforeMethodDeclParen() ? " (" : "(");
            if (this.cs.spaceWithinMethodDeclParens() && tree.params.nonEmpty()) {
                this.print(' ');
            }
            boolean oldPrintingMethodParams = this.printingMethodParams;
            this.printingMethodParams = true;
            this.wrapTrees(tree.params, this.cs.wrapMethodParams(), this.cs.alignMultilineMethodParams() ? this.out.col : this.out.leftMargin + this.cs.getContinuationIndentSize(), true);
            this.printingMethodParams = oldPrintingMethodParams;
            if (this.cs.spaceWithinMethodDeclParens() && tree.params.nonEmpty()) {
                this.needSpace();
            }
            this.print(')');
            if (tree.thrown.nonEmpty()) {
                this.wrap("throws ", this.cs.wrapThrowsKeyword());
                this.wrapTrees(tree.thrown, this.cs.wrapThrowsList(), this.cs.alignMultilineThrows() ? this.out.col : this.out.leftMargin + this.cs.getContinuationIndentSize(), true);
            }
            if (tree.body != null) {
                this.printBlock((JCTree)tree.body, tree.body.stats, this.cs.getMethodDeclBracePlacement(), this.cs.spaceBeforeMethodDeclLeftBrace(), true);
            } else {
                if (tree.defaultValue != null) {
                    this.print(" default ");
                    this.printExpr((JCTree)tree.defaultValue);
                }
                this.print(';');
            }
            this.enclClassName = enclClassNamePrev;
        }
    }

    public void visitVarDef(JCTree.JCVariableDecl tree) {
        boolean notEnumConst = (tree.mods.flags & 16384) == 0;
        this.printAnnotations(tree.mods.annotations);
        if (notEnumConst) {
            this.printFlags(tree.mods.flags);
            if (!this.suppressVariableType) {
                if ((tree.mods.flags & 0x400000000L) != 0) {
                    if (Tree.Kind.ARRAY_TYPE == tree.vartype.getKind()) {
                        this.printExpr((JCTree)((JCTree.JCArrayTypeTree)tree.vartype).elemtype);
                    } else {
                        this.printExpr((JCTree)tree.vartype);
                    }
                    this.print("...");
                } else {
                    this.print((JCTree)tree.vartype);
                }
            }
        }
        if (tree.vartype != null && !this.suppressVariableType) {
            this.needSpace();
        }
        if (!"<error>".contentEquals((CharSequence)tree.name)) {
            this.print(tree.name);
        }
        if (tree.init != null) {
            if (notEnumConst) {
                this.printVarInit(tree);
            } else {
                JCTree.JCNewClass newClsTree = (JCTree.JCNewClass)tree.init;
                if (newClsTree.args.nonEmpty()) {
                    this.print(this.cs.spaceBeforeMethodCallParen() ? " (" : "(");
                    if (this.cs.spaceWithinMethodCallParens()) {
                        this.print(' ');
                    }
                    this.wrapTrees(newClsTree.args, this.cs.wrapMethodCallArgs(), this.cs.alignMultilineCallArgs() ? this.out.col : this.out.leftMargin + this.cs.getContinuationIndentSize());
                    this.print(this.cs.spaceWithinMethodCallParens() ? " )" : ")");
                }
                if (newClsTree.def != null) {
                    Name enclClassNamePrev = this.enclClassName;
                    this.enclClassName = newClsTree.def.name;
                    this.printBlock(null, newClsTree.def.defs, this.cs.getOtherBracePlacement(), this.cs.spaceBeforeClassDeclLeftBrace(), true);
                    this.enclClassName = enclClassNamePrev;
                }
            }
        }
        if (this.prec == -1 && notEnumConst) {
            this.print(';');
        }
    }

    public void printVarInit(final JCTree.JCVariableDecl tree) {
        int col = this.out.col;
        if (!"<error>".contentEquals((CharSequence)tree.name)) {
            col -= tree.name.getByteLength();
        }
        this.wrapAssignOpTree("=", col, new Runnable(){

            @Override
            public void run() {
                VeryPretty.this.printNoParenExpr((JCTree)tree.init);
            }
        });
    }

    public void visitSkip(JCTree.JCSkip tree) {
        this.print(';');
    }

    public void visitBlock(JCTree.JCBlock tree) {
        this.printFlags(tree.flags, false);
        this.printBlock((JCTree)tree, tree.stats, this.cs.getOtherBracePlacement(), (tree.flags & 8) != 0 ? this.cs.spaceBeforeStaticInitLeftBrace() : false, false);
    }

    public void visitDoLoop(JCTree.JCDoWhileLoop tree) {
        boolean prevblock;
        this.print("do");
        if (this.cs.spaceBeforeDoLeftBrace()) {
            this.print(' ');
        }
        this.printIndentedStat((JCTree)tree.body, this.cs.redundantDoWhileBraces(), this.cs.spaceBeforeDoLeftBrace(), this.cs.wrapDoWhileStatement());
        boolean bl = prevblock = tree.body.getKind() == Tree.Kind.BLOCK || this.cs.redundantDoWhileBraces() == CodeStyle.BracesGenerationStyle.GENERATE;
        if (this.cs.placeWhileOnNewLine() || !prevblock) {
            this.newline();
            this.toLeftMargin();
        } else if (this.cs.spaceBeforeWhile()) {
            this.needSpace();
        }
        this.print("while");
        this.print(this.cs.spaceBeforeWhileParen() ? " (" : "(");
        if (this.cs.spaceWithinWhileParens()) {
            this.print(' ');
        }
        this.printNoParenExpr((JCTree)tree.cond);
        this.print(this.cs.spaceWithinWhileParens() ? " );" : ");");
    }

    public void visitWhileLoop(JCTree.JCWhileLoop tree) {
        this.print("while");
        this.print(this.cs.spaceBeforeWhileParen() ? " (" : "(");
        if (this.cs.spaceWithinWhileParens()) {
            this.print(' ');
        }
        this.printNoParenExpr((JCTree)tree.cond);
        this.print(this.cs.spaceWithinWhileParens() ? " )" : ")");
        this.printIndentedStat((JCTree)tree.body, this.cs.redundantWhileBraces(), this.cs.spaceBeforeWhileLeftBrace(), this.cs.wrapWhileStatement());
    }

    public void visitForLoop(JCTree.JCForLoop tree) {
        this.print("for");
        this.print(this.cs.spaceBeforeForParen() ? " (" : "(");
        if (this.cs.spaceWithinForParens()) {
            this.print(' ');
        }
        int col = this.out.col;
        if (tree.init.nonEmpty()) {
            if (((JCTree.JCStatement)tree.init.head).getTag() == JCTree.Tag.VARDEF) {
                this.printNoParenExpr((JCTree)tree.init.head);
                List l = tree.init.tail;
                while (l.nonEmpty()) {
                    JCTree.JCVariableDecl vdef = (JCTree.JCVariableDecl)l.head;
                    this.print(", " + (Object)vdef.name + " = ");
                    this.printNoParenExpr((JCTree)vdef.init);
                    l = l.tail;
                }
            } else {
                this.printExprs(tree.init);
            }
        }
        String sep = this.cs.spaceBeforeSemi() ? " ;" : ";";
        this.print(sep);
        if (tree.cond != null) {
            switch (this.cs.wrapFor()) {
                case WRAP_IF_LONG: {
                    int rm = this.cs.getRightMargin();
                    if (this.widthEstimator.estimateWidth((JCTree)tree.cond, rm - this.out.col) + this.out.col + 1 <= rm) {
                        if (!this.cs.spaceAfterSemi()) break;
                        this.print(' ');
                        break;
                    }
                }
                case WRAP_ALWAYS: {
                    this.newline();
                    this.toColExactly(this.cs.alignMultilineFor() ? col : this.out.leftMargin + this.cs.getContinuationIndentSize());
                    break;
                }
                case WRAP_NEVER: {
                    if (!this.cs.spaceAfterSemi()) break;
                    this.print(' ');
                }
            }
            this.printNoParenExpr((JCTree)tree.cond);
        }
        this.print(sep);
        if (tree.step.nonEmpty()) {
            switch (this.cs.wrapFor()) {
                case WRAP_IF_LONG: {
                    int rm = this.cs.getRightMargin();
                    if (this.widthEstimator.estimateWidth(tree.step, rm - this.out.col) + this.out.col + 1 <= rm) {
                        if (!this.cs.spaceAfterSemi()) break;
                        this.print(' ');
                        break;
                    }
                }
                case WRAP_ALWAYS: {
                    this.newline();
                    this.toColExactly(this.cs.alignMultilineFor() ? col : this.out.leftMargin + this.cs.getContinuationIndentSize());
                    break;
                }
                case WRAP_NEVER: {
                    if (!this.cs.spaceAfterSemi()) break;
                    this.print(' ');
                }
            }
            this.printExprs(tree.step);
        }
        this.print(this.cs.spaceWithinForParens() ? " )" : ")");
        this.printIndentedStat((JCTree)tree.body, this.cs.redundantForBraces(), this.cs.spaceBeforeForLeftBrace(), this.cs.wrapForStatement());
    }

    public void visitLabelled(JCTree.JCLabeledStatement tree) {
        this.toColExactly(this.cs.absoluteLabelIndent() ? 0 : this.out.leftMargin);
        this.print(tree.label);
        this.print(':');
        int old = this.out.leftMargin;
        this.out.leftMargin += this.cs.getLabelIndent();
        this.toColExactly(this.out.leftMargin);
        this.printStat((JCTree)tree.body);
        this.undent(old);
    }

    public void visitLambda(JCTree.JCLambda tree) {
        this.print(this.cs.spaceWithinLambdaParens() && tree.params.nonEmpty() ? "( " : "(");
        boolean oldPrintingMethodParams = this.printingMethodParams;
        this.printingMethodParams = true;
        this.suppressVariableType = tree.paramKind == JCTree.JCLambda.ParameterKind.IMPLICIT;
        this.wrapTrees(tree.params, this.cs.wrapLambdaParams(), this.cs.alignMultilineLambdaParams() ? this.out.col : this.out.leftMargin + this.cs.getContinuationIndentSize(), true);
        this.suppressVariableType = false;
        this.printingMethodParams = oldPrintingMethodParams;
        if (this.cs.spaceWithinLambdaParens() && tree.params.nonEmpty()) {
            this.needSpace();
        }
        this.print(')');
        this.print(this.cs.spaceAroundLambdaArrow() ? " ->" : "->");
        if (tree.getBodyKind() == LambdaExpressionTree.BodyKind.STATEMENT) {
            this.printBlock(tree.body, this.cs.getOtherBracePlacement(), this.cs.spaceAroundLambdaArrow());
        } else {
            int rm = this.cs.getRightMargin();
            switch (this.cs.wrapBinaryOps()) {
                case WRAP_IF_LONG: {
                    if (this.widthEstimator.estimateWidth(tree.body, rm - this.out.col) + this.out.col <= this.cs.getRightMargin()) {
                        if (!this.cs.spaceAroundLambdaArrow()) break;
                        this.print(' ');
                        break;
                    }
                }
                case WRAP_ALWAYS: {
                    this.newline();
                    this.toColExactly(this.out.leftMargin + this.cs.getContinuationIndentSize());
                    break;
                }
                case WRAP_NEVER: {
                    if (!this.cs.spaceAroundLambdaArrow()) break;
                    this.print(' ');
                }
            }
            this.printExpr(tree.body, -1);
        }
    }

    public void visitSwitch(JCTree.JCSwitch tree) {
        this.print("switch");
        this.print(this.cs.spaceBeforeSwitchParen() ? " (" : "(");
        if (this.cs.spaceWithinSwitchParens()) {
            this.print(' ');
        }
        this.printNoParenExpr((JCTree)tree.selector);
        this.print(this.cs.spaceWithinSwitchParens() ? " )" : ")");
        int bcol = this.out.leftMargin;
        switch (this.cs.getOtherBracePlacement()) {
            case NEW_LINE: {
                this.newline();
                this.toColExactly(bcol);
                break;
            }
            case NEW_LINE_HALF_INDENTED: {
                this.newline();
                this.toColExactly(bcol += this.indentSize >> 1);
                break;
            }
            case NEW_LINE_INDENTED: {
                this.newline();
                this.toColExactly(bcol += this.indentSize);
            }
        }
        if (this.cs.spaceBeforeSwitchLeftBrace()) {
            this.needSpace();
        }
        this.print('{');
        if (tree.cases.nonEmpty()) {
            this.newline();
            this.printStats(tree.cases);
            this.toColExactly(bcol);
        }
        this.print('}');
    }

    public void visitCase(JCTree.JCCase tree) {
        int old = this.cs.indentCasesFromSwitch() ? this.indent() : this.out.leftMargin;
        this.toLeftMargin();
        if (tree.pat == null) {
            this.print("default");
        } else {
            this.print("case ");
            this.printNoParenExpr((JCTree)tree.pat);
        }
        this.print(':');
        this.newline();
        this.indent();
        this.printStats(tree.stats);
        this.undent(old);
    }

    public void visitSynchronized(JCTree.JCSynchronized tree) {
        this.print("synchronized");
        this.print(this.cs.spaceBeforeSynchronizedParen() ? " (" : "(");
        if (this.cs.spaceWithinSynchronizedParens()) {
            this.print(' ');
        }
        this.printNoParenExpr((JCTree)tree.lock);
        this.print(this.cs.spaceWithinSynchronizedParens() ? " )" : ")");
        this.printBlock((JCTree)tree.body, this.cs.getOtherBracePlacement(), this.cs.spaceBeforeSynchronizedLeftBrace());
    }

    public void visitTry(JCTree.JCTry tree) {
        this.print("try");
        if (!tree.getResources().isEmpty()) {
            this.print(" (");
            Iterator it = tree.getResources().iterator();
            while (it.hasNext()) {
                JCTree r = (JCTree)it.next();
                this.oldTrees.remove((Object)r);
                this.printPrecedingComments(r, false);
                this.printExpr(r, 0);
                this.printTrailingComments(r, false);
                if (!it.hasNext()) continue;
                this.print(";");
            }
            this.print(") ");
        }
        this.printBlock((JCTree)tree.body, this.cs.getOtherBracePlacement(), this.cs.spaceBeforeTryLeftBrace());
        List l = tree.catchers;
        while (l.nonEmpty()) {
            this.printStat((JCTree)l.head);
            l = l.tail;
        }
        if (tree.finalizer != null) {
            this.printFinallyBlock(tree.finalizer);
        }
    }

    public void visitCatch(JCTree.JCCatch tree) {
        if (this.cs.placeCatchOnNewLine()) {
            this.newline();
            this.toLeftMargin();
        } else if (this.cs.spaceBeforeCatch()) {
            this.needSpace();
        }
        this.print("catch");
        this.print(this.cs.spaceBeforeCatchParen() ? " (" : "(");
        if (this.cs.spaceWithinCatchParens()) {
            this.print(' ');
        }
        this.printNoParenExpr((JCTree)tree.param);
        this.print(this.cs.spaceWithinCatchParens() ? " )" : ")");
        this.printBlock((JCTree)tree.body, this.cs.getOtherBracePlacement(), this.cs.spaceBeforeCatchLeftBrace());
    }

    public void visitConditional(JCTree.JCConditional tree) {
        int rm;
        this.printExpr((JCTree)tree.cond, 2);
        switch (this.cs.wrapTernaryOps()) {
            case WRAP_IF_LONG: {
                rm = this.cs.getRightMargin();
                if (this.widthEstimator.estimateWidth((JCTree)tree.truepart, rm - this.out.col) + this.out.col + 1 <= rm) {
                    if (!this.cs.spaceAroundTernaryOps()) break;
                    this.print(' ');
                    break;
                }
            }
            case WRAP_ALWAYS: {
                this.newline();
                this.toColExactly(this.out.leftMargin + this.cs.getContinuationIndentSize());
                break;
            }
            case WRAP_NEVER: {
                if (!this.cs.spaceAroundTernaryOps()) break;
                this.print(' ');
            }
        }
        this.print(this.cs.spaceAroundTernaryOps() ? "? " : "?");
        this.printExpr((JCTree)tree.truepart, 3);
        switch (this.cs.wrapTernaryOps()) {
            case WRAP_IF_LONG: {
                rm = this.cs.getRightMargin();
                if (this.widthEstimator.estimateWidth((JCTree)tree.falsepart, rm - this.out.col) + this.out.col + 1 <= rm) {
                    if (!this.cs.spaceAroundTernaryOps()) break;
                    this.print(' ');
                    break;
                }
            }
            case WRAP_ALWAYS: {
                this.newline();
                this.toColExactly(this.out.leftMargin + this.cs.getContinuationIndentSize());
                break;
            }
            case WRAP_NEVER: {
                if (!this.cs.spaceAroundTernaryOps()) break;
                this.print(' ');
            }
        }
        this.print(this.cs.spaceAroundTernaryOps() ? ": " : ":");
        this.printExpr((JCTree)tree.falsepart, 3);
    }

    public void visitIf(JCTree.JCIf tree) {
        boolean prevblock;
        this.print("if");
        this.print(this.cs.spaceBeforeIfParen() ? " (" : "(");
        if (this.cs.spaceWithinIfParens()) {
            this.print(' ');
        }
        this.printNoParenExpr((JCTree)tree.cond);
        this.print(this.cs.spaceWithinIfParens() ? " )" : ")");
        boolean bl = prevblock = tree.thenpart.getKind() == Tree.Kind.BLOCK && this.cs.redundantIfBraces() != CodeStyle.BracesGenerationStyle.ELIMINATE || this.cs.redundantIfBraces() == CodeStyle.BracesGenerationStyle.GENERATE;
        if (tree.elsepart != null && this.danglingElseChecker.hasDanglingElse((JCTree)tree.thenpart)) {
            this.printBlock((JCTree)tree.thenpart, this.cs.getOtherBracePlacement(), this.cs.spaceBeforeIfLeftBrace());
            prevblock = true;
        } else {
            this.printIndentedStat((JCTree)tree.thenpart, this.cs.redundantIfBraces(), this.cs.spaceBeforeIfLeftBrace(), this.cs.wrapIfStatement());
        }
        if (tree.elsepart != null) {
            this.printElse(tree, prevblock);
        }
    }

    public void printElse(JCTree.JCIf tree, boolean prevblock) {
        if (this.cs.placeElseOnNewLine() || !prevblock) {
            this.newline();
            this.toLeftMargin();
        } else if (this.cs.spaceBeforeElse()) {
            this.needSpace();
        }
        this.print("else");
        if (tree.elsepart.getKind() == Tree.Kind.IF && this.cs.specialElseIf()) {
            this.needSpace();
            this.printStat((JCTree)tree.elsepart);
        } else {
            this.printIndentedStat((JCTree)tree.elsepart, this.cs.redundantIfBraces(), this.cs.spaceBeforeElseLeftBrace(), this.cs.wrapIfStatement());
        }
    }

    public void visitExec(JCTree.JCExpressionStatement tree) {
        this.printNoParenExpr((JCTree)tree.expr);
        if (this.prec == -1) {
            this.print(';');
        }
    }

    public void visitBreak(JCTree.JCBreak tree) {
        this.print("break");
        if (tree.label != null) {
            this.needSpace();
            this.print(tree.label);
        }
        this.print(';');
    }

    public void visitContinue(JCTree.JCContinue tree) {
        this.print("continue");
        if (tree.label != null) {
            this.needSpace();
            this.print(tree.label);
        }
        this.print(';');
    }

    public void visitReturn(JCTree.JCReturn tree) {
        this.print("return");
        if (tree.expr != null) {
            this.needSpace();
            this.printNoParenExpr((JCTree)tree.expr);
        }
        this.print(';');
    }

    public void visitThrow(JCTree.JCThrow tree) {
        this.print("throw ");
        this.printNoParenExpr((JCTree)tree.expr);
        this.print(';');
    }

    public void visitAssert(JCTree.JCAssert tree) {
        this.print("assert ");
        this.printExpr((JCTree)tree.cond);
        if (tree.detail != null) {
            this.print(this.cs.spaceBeforeColon() ? " :" : ":");
            switch (this.cs.wrapAssert()) {
                case WRAP_IF_LONG: {
                    int rm = this.cs.getRightMargin();
                    if (this.widthEstimator.estimateWidth((JCTree)tree.detail, rm - this.out.col) + this.out.col + 1 <= rm) {
                        if (!this.cs.spaceAfterColon()) break;
                        this.print(' ');
                        break;
                    }
                }
                case WRAP_ALWAYS: {
                    this.newline();
                    this.toColExactly(this.out.leftMargin + this.cs.getContinuationIndentSize());
                    break;
                }
                case WRAP_NEVER: {
                    if (!this.cs.spaceAfterColon()) break;
                    this.print(' ');
                }
            }
            this.printExpr((JCTree)tree.detail);
        }
        this.print(';');
    }

    public void visitApply(JCTree.JCMethodInvocation tree) {
        int prevPrec = this.prec;
        this.prec = 15;
        this.printMethodSelect(tree);
        this.prec = prevPrec;
        this.print(this.cs.spaceBeforeMethodCallParen() ? " (" : "(");
        if (this.cs.spaceWithinMethodCallParens() && tree.args.nonEmpty()) {
            this.print(' ');
        }
        this.wrapTrees(tree.args, this.cs.wrapMethodCallArgs(), this.cs.alignMultilineCallArgs() ? this.out.col : this.out.leftMargin + this.cs.getContinuationIndentSize());
        this.print(this.cs.spaceWithinMethodCallParens() && tree.args.nonEmpty() ? " )" : ")");
    }

    public void printMethodSelect(JCTree.JCMethodInvocation tree) {
        if (tree.meth.getTag() == JCTree.Tag.SELECT) {
            JCTree.JCFieldAccess left = (JCTree.JCFieldAccess)tree.meth;
            this.printExpr((JCTree)left.selected);
            boolean wrapAfterDot = this.cs.wrapAfterDotInChainedMethodCalls();
            if (wrapAfterDot) {
                this.print('.');
            }
            if (left.selected.getTag() == JCTree.Tag.APPLY) {
                switch (this.cs.wrapChainedMethodCalls()) {
                    case WRAP_IF_LONG: {
                        int rm = this.cs.getRightMargin();
                        int estWidth = left.name.length();
                        if (tree.typeargs.nonEmpty()) {
                            estWidth += this.widthEstimator.estimateWidth(tree.typeargs, rm - this.out.col - estWidth) + 2;
                        }
                        if ((estWidth += this.widthEstimator.estimateWidth(tree.args, rm - this.out.col - estWidth) + 2) + this.out.col <= rm) break;
                    }
                    case WRAP_ALWAYS: {
                        this.newline();
                        this.toColExactly(this.out.leftMargin + this.cs.getContinuationIndentSize());
                    }
                }
            }
            if (!wrapAfterDot) {
                this.print('.');
            }
            if (tree.typeargs.nonEmpty()) {
                this.printTypeArguments(tree.typeargs);
            }
            this.print(left.name);
        } else {
            if (tree.typeargs.nonEmpty()) {
                this.printTypeArguments(tree.typeargs);
            }
            this.printExpr((JCTree)tree.meth);
        }
    }

    public void visitNewClass(JCTree.JCNewClass tree) {
        if (tree.encl != null) {
            this.printExpr((JCTree)tree.encl);
            this.print('.');
        }
        this.print("new ");
        if (tree.typeargs.nonEmpty()) {
            this.print("<");
            this.printExprs(tree.typeargs);
            this.print(">");
        }
        if (tree.encl == null) {
            this.print((JCTree)tree.clazz);
        } else if (tree.clazz.type != null) {
            this.print(tree.clazz.type.tsym.name);
        } else {
            this.print((JCTree)tree.clazz);
        }
        this.print(this.cs.spaceBeforeMethodCallParen() ? " (" : "(");
        if (this.cs.spaceWithinMethodCallParens() && tree.args.nonEmpty()) {
            this.print(' ');
        }
        this.wrapTrees(tree.args, this.cs.wrapMethodCallArgs(), this.cs.alignMultilineCallArgs() ? this.out.col : this.out.leftMargin + this.cs.getContinuationIndentSize());
        this.print(this.cs.spaceWithinMethodCallParens() && tree.args.nonEmpty() ? " )" : ")");
        if (tree.def != null) {
            this.printNewClassBody(tree);
        }
    }

    public void printNewClassBody(JCTree.JCNewClass tree) {
        Name enclClassNamePrev = this.enclClassName;
        this.enclClassName = tree.def.name;
        this.printBlock(null, tree.def.defs, this.cs.getOtherBracePlacement(), this.cs.spaceBeforeClassDeclLeftBrace(), true, true);
        this.enclClassName = enclClassNamePrev;
    }

    public void visitNewArray(JCTree.JCNewArray tree) {
        if (tree.elemtype != null) {
            this.print("new ");
            int n = tree.elems != null ? 1 : 0;
            JCTree.JCExpression elemtype = tree.elemtype;
            while (elemtype.getTag() == JCTree.Tag.TYPEARRAY) {
                ++n;
                elemtype = ((JCTree.JCArrayTypeTree)elemtype).elemtype;
            }
            this.printExpr((JCTree)elemtype);
            List l = tree.dims;
            while (l.nonEmpty()) {
                this.print(this.cs.spaceWithinArrayInitBrackets() ? "[ " : "[");
                this.printNoParenExpr((JCTree)l.head);
                this.print(this.cs.spaceWithinArrayInitBrackets() ? " ]" : "]");
                l = l.tail;
            }
            while (--n >= 0) {
                this.print(this.cs.spaceWithinArrayInitBrackets() ? "[ ]" : "[]");
            }
        }
        if (tree.elems != null) {
            if (this.cs.spaceBeforeArrayInitLeftBrace()) {
                this.needSpace();
            }
            this.print('{');
            if (this.cs.spaceWithinBraces()) {
                this.print(' ');
            }
            this.wrapTrees(tree.elems, this.cs.wrapArrayInit(), this.cs.alignMultilineArrayInit() ? this.out.col : this.out.leftMargin + this.cs.getContinuationIndentSize());
            this.print(this.cs.spaceWithinBraces() ? " }" : "}");
        }
    }

    public void visitParens(JCTree.JCParens tree) {
        this.print('(');
        if (this.cs.spaceWithinParens()) {
            this.print(' ');
        }
        this.printExpr((JCTree)tree.expr);
        this.print(this.cs.spaceWithinParens() ? " )" : ")");
    }

    public void visitAssign(final JCTree.JCAssign tree) {
        int col = this.out.col;
        this.printExpr((JCTree)tree.lhs, 2);
        this.wrapAssignOpTree("=", col, new Runnable(){

            @Override
            public void run() {
                VeryPretty.this.printExpr((JCTree)tree.rhs, 1);
            }
        });
    }

    public void visitAssignop(JCTree.JCAssignOp tree) {
        int col = this.out.col;
        this.printExpr((JCTree)tree.lhs, 3);
        if (this.cs.spaceAroundAssignOps()) {
            this.print(' ');
        }
        this.print(this.treeinfo.operatorName(tree.getTag().noAssignOp()));
        this.print('=');
        int rm = this.cs.getRightMargin();
        switch (this.cs.wrapAssignOps()) {
            case WRAP_IF_LONG: {
                if (this.widthEstimator.estimateWidth((JCTree)tree.rhs, rm - this.out.col) + this.out.col <= this.cs.getRightMargin()) {
                    if (!this.cs.spaceAroundAssignOps()) break;
                    this.print(' ');
                    break;
                }
            }
            case WRAP_ALWAYS: {
                this.newline();
                this.toColExactly(this.cs.alignMultilineAssignment() ? col : this.out.leftMargin + this.cs.getContinuationIndentSize());
                break;
            }
            case WRAP_NEVER: {
                if (!this.cs.spaceAroundAssignOps()) break;
                this.print(' ');
            }
        }
        this.printExpr((JCTree)tree.rhs, 2);
    }

    public void visitUnary(JCTree.JCUnary tree) {
        Name opname;
        int ownprec = TreeInfo.opPrec((JCTree.Tag)tree.getTag());
        switch (tree.getTag()) {
            case POS: {
                opname = this.names.fromString("+");
                break;
            }
            case NEG: {
                opname = this.names.fromString("-");
                break;
            }
            default: {
                opname = this.treeinfo.operatorName(tree.getTag());
            }
        }
        if (tree.getTag().ordinal() <= JCTree.Tag.PREDEC.ordinal()) {
            if (this.cs.spaceAroundUnaryOps()) {
                this.needSpace();
                this.print(opname);
                this.print(' ');
            } else {
                this.print(opname);
                if (tree.getTag() == JCTree.Tag.POS && (tree.arg.getTag() == JCTree.Tag.POS || tree.arg.getTag() == JCTree.Tag.PREINC) || tree.getTag() == JCTree.Tag.NEG && (tree.arg.getTag() == JCTree.Tag.NEG || tree.arg.getTag() == JCTree.Tag.PREDEC)) {
                    this.print(' ');
                }
            }
            this.printExpr((JCTree)tree.arg, ownprec);
        } else {
            this.printExpr((JCTree)tree.arg, ownprec);
            if (this.cs.spaceAroundUnaryOps()) {
                this.print(' ');
                this.print(opname);
                this.print(' ');
            } else {
                this.print(opname);
            }
        }
    }

    public void visitBinary(JCTree.JCBinary tree) {
        int ownprec = TreeInfo.opPrec((JCTree.Tag)tree.getTag());
        Name opname = this.treeinfo.operatorName(tree.getTag());
        int col = this.out.col;
        this.printExpr((JCTree)tree.lhs, ownprec);
        if (this.cs.spaceAroundBinaryOps()) {
            this.print(' ');
        }
        this.print(opname);
        boolean needsSpace = this.cs.spaceAroundBinaryOps() || tree.getTag() == JCTree.Tag.PLUS && (tree.rhs.getTag() == JCTree.Tag.POS || tree.rhs.getTag() == JCTree.Tag.PREINC) || tree.getTag() == JCTree.Tag.MINUS && (tree.rhs.getTag() == JCTree.Tag.NEG || tree.rhs.getTag() == JCTree.Tag.PREDEC);
        int rm = this.cs.getRightMargin();
        switch (this.cs.wrapBinaryOps()) {
            case WRAP_IF_LONG: {
                if (this.widthEstimator.estimateWidth((JCTree)tree.rhs, rm - this.out.col) + this.out.col <= this.cs.getRightMargin()) {
                    if (!needsSpace) break;
                    this.print(' ');
                    break;
                }
            }
            case WRAP_ALWAYS: {
                this.newline();
                this.toColExactly(this.cs.alignMultilineBinaryOp() ? col : this.out.leftMargin + this.cs.getContinuationIndentSize());
                break;
            }
            case WRAP_NEVER: {
                if (!needsSpace) break;
                this.print(' ');
            }
        }
        this.printExpr((JCTree)tree.rhs, ownprec + 1);
    }

    public void visitTypeCast(JCTree.JCTypeCast tree) {
        this.print(this.cs.spaceWithinTypeCastParens() ? "( " : "(");
        this.print(tree.clazz);
        this.print(this.cs.spaceWithinTypeCastParens() ? " )" : ")");
        if (this.cs.spaceAfterTypeCast()) {
            this.needSpace();
        }
        if (this.diffContext.origUnit != null && TreePath.getPath((CompilationUnitTree)this.diffContext.origUnit, (Tree)tree.expr) != null) {
            int a = TreeInfo.getStartPos((JCTree)tree.expr);
            int b = TreeInfo.getEndPos((JCTree)tree.expr, (EndPosTable)this.diffContext.origUnit.endPositions);
            this.print(this.diffContext.origText.substring(a, b));
            return;
        }
        this.printExpr((JCTree)tree.expr, 14);
    }

    public void visitTypeUnion(JCTree.JCTypeUnion that) {
        boolean sep = this.cs.spaceAroundBinaryOps();
        this.wrapTrees(that.getTypeAlternatives(), this.cs.wrapDisjunctiveCatchTypes(), this.cs.alignMultilineDisjunctiveCatchTypes() ? this.out.col : this.out.leftMargin + this.cs.getContinuationIndentSize(), false, sep, sep, "|");
    }

    public void visitTypeTest(JCTree.JCInstanceOf tree) {
        this.printExpr((JCTree)tree.expr, 10);
        this.print(" instanceof ");
        this.print(tree.clazz);
    }

    public void visitIndexed(JCTree.JCArrayAccess tree) {
        this.printExpr((JCTree)tree.indexed, 15);
        this.print('[');
        this.printExpr((JCTree)tree.index);
        this.print(']');
    }

    public void visitSelect(JCTree.JCFieldAccess tree) {
        this.printExpr((JCTree)tree.selected, 15);
        this.print('.');
        this.print(tree.name);
    }

    public void visitIdent(JCTree.JCIdent tree) {
        this.print(tree.name);
    }

    public void visitLiteral(JCTree.JCLiteral tree) {
        long end;
        long start;
        if (this.diffContext != null && this.diffContext.origUnit != null && (start = this.diffContext.trees.getSourcePositions().getStartPosition((CompilationUnitTree)this.diffContext.origUnit, (Tree)tree)) >= 0 && (end = this.diffContext.trees.getSourcePositions().getEndPosition((CompilationUnitTree)this.diffContext.origUnit, (Tree)tree)) >= 0 && this.origText != null) {
            this.print(this.origText.substring((int)start, (int)end));
            return;
        }
        if (this.diffContext != null && this.diffContext.mainUnit != null && (start = this.diffContext.trees.getSourcePositions().getStartPosition((CompilationUnitTree)this.diffContext.mainUnit, (Tree)tree)) >= 0 && (end = this.diffContext.trees.getSourcePositions().getEndPosition((CompilationUnitTree)this.diffContext.mainUnit, (Tree)tree)) >= 0 && this.diffContext.mainCode != null) {
            this.print(this.diffContext.mainCode.substring((int)start, (int)end));
            return;
        }
        switch (tree.typetag) {
            case INT: {
                this.print(tree.value.toString());
                break;
            }
            case LONG: {
                this.print(tree.value.toString() + "L");
                break;
            }
            case FLOAT: {
                this.print(tree.value.toString() + "F");
                break;
            }
            case DOUBLE: {
                this.print(tree.value.toString());
                break;
            }
            case CHAR: {
                this.print("'" + VeryPretty.quote(String.valueOf((char)((Number)tree.value).intValue()), '\"') + "'");
                break;
            }
            case CLASS: {
                this.print("\"" + VeryPretty.quote((String)tree.value, '\'') + "\"");
                break;
            }
            case BOOLEAN: {
                this.print(tree.getValue().toString());
                break;
            }
            case BOT: {
                this.print("null");
                break;
            }
            default: {
                this.print(tree.value.toString());
            }
        }
    }

    private static String quote(String val, char keep) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < val.length(); ++i) {
            char c = val.charAt(i);
            if (c != keep) {
                sb.append(Convert.quote((char)c));
                continue;
            }
            sb.append(c);
        }
        return sb.toString();
    }

    static String typeTagName(TypeTag tt) {
        return typeTagNames[tt.ordinal()];
    }

    public void visitTypeIdent(JCTree.JCPrimitiveTypeTree tree) {
        this.print(VeryPretty.typeTagName(tree.typetag));
    }

    public void visitTypeArray(JCTree.JCArrayTypeTree tree) {
        this.printExpr((JCTree)tree.elemtype);
        this.print("[]");
    }

    public void visitTypeApply(JCTree.JCTypeApply tree) {
        this.printExpr((JCTree)tree.clazz);
        this.print('<');
        this.printExprs(tree.arguments);
        this.print('>');
    }

    public void visitAnnotatedType(JCTree.JCAnnotatedType tree) {
        this.printExprs(tree.annotations);
        this.print(' ');
        this.printExpr((JCTree)tree.underlyingType);
    }

    public void visitTypeParameter(JCTree.JCTypeParameter tree) {
        this.print(tree.name);
        if (tree.bounds.nonEmpty()) {
            this.print(" extends ");
            this.printExprs(tree.bounds, " & ");
        }
    }

    public void visitWildcard(JCTree.JCWildcard tree) {
        this.print("" + (Object)tree.kind.kind);
        if (tree.kind.kind != BoundKind.UNBOUND) {
            this.printExpr(tree.inner);
        }
    }

    public void visitModifiers(JCTree.JCModifiers tree) {
        this.printAnnotations(tree.annotations);
        this.printFlags(tree.flags);
    }

    public void visitAnnotation(JCTree.JCAnnotation tree) {
        boolean oldInsideAnnotation = this.insideAnnotation;
        this.insideAnnotation = true;
        if (!this.printAnnotationsFormatted(List.of((Object)tree))) {
            this.print("@");
            this.printExpr(tree.annotationType);
            if (tree.args.nonEmpty()) {
                this.print(this.cs.spaceBeforeAnnotationParen() ? " (" : "(");
                if (this.cs.spaceWithinAnnotationParens()) {
                    this.print(' ');
                }
                this.printExprs(tree.args);
                this.print(this.cs.spaceWithinAnnotationParens() ? " )" : ")");
            }
        }
        this.insideAnnotation = oldInsideAnnotation;
    }

    public void visitForeachLoop(JCTree.JCEnhancedForLoop tree) {
        this.print("for");
        this.print(this.cs.spaceBeforeForParen() ? " (" : "(");
        if (this.cs.spaceWithinForParens()) {
            this.print(' ');
        }
        this.printExpr((JCTree)tree.getVariable());
        String sep = this.cs.spaceBeforeColon() ? " :" : ":";
        this.print(this.cs.spaceAfterColon() ? sep + " " : sep);
        this.printExpr((JCTree)tree.getExpression());
        this.print(this.cs.spaceWithinForParens() ? " )" : ")");
        this.printIndentedStat((JCTree)tree.getStatement(), this.cs.redundantForBraces(), this.cs.spaceBeforeForLeftBrace(), this.cs.wrapForStatement());
    }

    public void visitReference(JCTree.JCMemberReference tree) {
        this.printExpr((JCTree)tree.expr);
        this.print(this.cs.spaceAroundMethodReferenceDoubleColon() ? " :: " : "::");
        if (tree.typeargs != null && !tree.typeargs.isEmpty()) {
            this.print("<");
            this.printExprs(tree.typeargs);
            this.print(">");
        }
        if (tree.getMode() == MemberReferenceTree.ReferenceMode.INVOKE) {
            this.print(tree.name);
        } else {
            this.print("new");
        }
    }

    public void visitLetExpr(JCTree.LetExpr tree) {
        this.print("(let " + (Object)tree.defs + " in " + (Object)tree.expr + ")");
    }

    public void visitErroneous(JCTree.JCErroneous tree) {
        this.print("(ERROR)");
        this.containsError = true;
    }

    public void visitTree(JCTree tree) {
        this.print("(UNKNOWN: " + (Object)tree + ")");
        this.newline();
    }

    private void print(char c) {
        this.out.append(c);
    }

    private void needSpace() {
        this.out.needSpace();
    }

    private void blankLines(int n) {
        this.out.blanklines(n);
    }

    private void blankLines(JCTree tree, boolean before) {
        if (tree == null) {
            return;
        }
        int n = 0;
        switch (tree.getKind()) {
            case ANNOTATION_TYPE: 
            case CLASS: 
            case ENUM: 
            case INTERFACE: {
                int n2 = n = before ? this.cs.getBlankLinesBeforeClass() : this.cs.getBlankLinesAfterClass();
                if (((JCTree.JCClassDecl)tree).defs.nonEmpty() && !before) {
                    n = 0;
                } else {
                    this.out.blanklines(n);
                    this.toLeftMargin();
                }
                return;
            }
            case METHOD: {
                if ((((JCTree.JCMethodDecl)tree).mods.flags & 4096) == 0 && ((JCTree.JCMethodDecl)tree).name != this.names.init || this.enclClassName != null) {
                    n = before ? this.cs.getBlankLinesBeforeMethods() : this.cs.getBlankLinesAfterMethods();
                    this.out.blanklines(n);
                    this.toLeftMargin();
                }
                return;
            }
            case VARIABLE: {
                if (this.enclClassName != null && this.enclClassName != this.names.empty && (((JCTree.JCVariableDecl)tree).mods.flags & 16384) == 0) {
                    n = before ? this.cs.getBlankLinesBeforeFields() : this.cs.getBlankLinesAfterFields();
                    this.out.blanklines(n);
                    if (before) {
                        this.toLeftMargin();
                    }
                }
                return;
            }
        }
    }

    private void blankLines(DCTree tree, boolean before) {
        if (tree == null) {
            return;
        }
        switch (tree.getKind()) {
            case AUTHOR: 
            case DEPRECATED: 
            case EXCEPTION: 
            case PARAM: 
            case RETURN: 
            case SEE: 
            case SERIAL: 
            case SERIAL_DATA: 
            case SERIAL_FIELD: 
            case SINCE: 
            case THROWS: 
            case UNKNOWN_BLOCK_TAG: 
            case VERSION: {
                if (!before) break;
                this.newline();
                this.toLeftMargin();
                this.print(" * ");
                break;
            }
            case DOC_COMMENT: {
                if (before) {
                    this.blankline();
                } else {
                    this.newline();
                }
                this.toLeftMargin();
                break;
            }
        }
    }

    private void toColExactly(int n) {
        if (n < this.out.col) {
            this.newline();
        }
        this.out.toCol(n);
    }

    private void printQualified(Symbol t) {
        if (t.owner != null && t.owner.name.getByteLength() > 0 && !(t.type instanceof Type.TypeVar) && !(t.owner instanceof Symbol.MethodSymbol)) {
            if (t.owner instanceof Symbol.PackageSymbol) {
                this.printAllQualified(t.owner);
            } else {
                this.printQualified(t.owner);
            }
            this.print('.');
        }
        this.print(t.name);
    }

    private void printAllQualified(Symbol t) {
        if (t.owner != null && t.owner.name.getByteLength() > 0) {
            this.printAllQualified(t.owner);
            this.print('.');
        }
        this.print(t.name);
    }

    protected void printTagName(DocTree node) {
        this.out.append("@");
        this.out.append(node.getKind().tagName);
    }

    public Void visitAttribute(AttributeTree node, Void p) {
        String quote;
        this.print(node.getName());
        switch (node.getValueKind()) {
            case EMPTY: {
                return null;
            }
            case UNQUOTED: {
                quote = "";
                break;
            }
            case SINGLE: {
                quote = "'";
                break;
            }
            case DOUBLE: {
                quote = "\"";
                break;
            }
            default: {
                throw new AssertionError();
            }
        }
        this.print("=");
        this.print(quote);
        for (DocTree docTree : node.getValue()) {
            this.doAccept((DCTree)docTree);
        }
        this.print(quote);
        return null;
    }

    public Void visitAuthor(AuthorTree node, Void p) {
        this.printTagName((DocTree)node);
        this.print(" ");
        for (DocTree docTree : node.getName()) {
            this.doAccept((DCTree)docTree);
        }
        return null;
    }

    public Void visitComment(CommentTree node, Void p) {
        this.print(node.getBody());
        return null;
    }

    public Void visitDeprecated(DeprecatedTree node, Void p) {
        this.printTagName((DocTree)node);
        if (!node.getBody().isEmpty()) {
            this.needSpace();
            for (DocTree docTree : node.getBody()) {
                this.doAccept((DCTree)docTree);
            }
        }
        return null;
    }

    public Void visitDocComment(DocCommentTree node, Void p) {
        this.print("/**");
        this.newline();
        this.toLeftMargin();
        this.print(" * ");
        for (DocTree docTree22 : node.getFirstSentence()) {
            this.doAccept((DCTree)docTree22);
        }
        for (DocTree docTree22 : node.getBody()) {
            this.doAccept((DCTree)docTree22);
        }
        for (DocTree docTree22 : node.getBlockTags()) {
            this.newline();
            this.toLeftMargin();
            this.print(" * ");
            this.doAccept((DCTree)docTree22);
        }
        this.newline();
        this.toLeftMargin();
        this.print(" */");
        return null;
    }

    public Void visitDocRoot(DocRootTree node, Void p) {
        this.print("{");
        this.printTagName((DocTree)node);
        this.print("}");
        return null;
    }

    public Void visitStartElement(StartElementTree node, Void p) {
        this.print("<");
        this.print(node.getName());
        java.util.List attrs = node.getAttributes();
        if (!attrs.isEmpty()) {
            this.print(" ");
            for (DocTree docTree : attrs) {
                this.doAccept((DCTree)docTree);
            }
            DocTree last = (DocTree)attrs.get(attrs.size() - 1);
            if (node.isSelfClosing() && last instanceof AttributeTree && ((AttributeTree)last).getValueKind() == AttributeTree.ValueKind.UNQUOTED) {
                this.print(" ");
            }
        }
        if (node.isSelfClosing()) {
            this.print("/");
        }
        this.print(">");
        return null;
    }

    public Void visitEndElement(EndElementTree node, Void p) {
        this.print("</");
        this.print(node.getName());
        this.print(">");
        return null;
    }

    public Void visitEntity(EntityTree node, Void p) {
        this.print("&");
        this.print(node.getName());
        this.print(";");
        return null;
    }

    public Void visitErroneous(ErroneousTree node, Void p) {
        this.print(node.getBody());
        return null;
    }

    public Void visitIdentifier(IdentifierTree node, Void p) {
        this.print(node.getName());
        return null;
    }

    public Void visitInheritDoc(InheritDocTree node, Void p) {
        this.print("{");
        this.printTagName((DocTree)node);
        this.print("}");
        return null;
    }

    public Void visitLink(LinkTree node, Void p) {
        this.print("{");
        this.printTagName((DocTree)node);
        this.print(" ");
        this.doAccept((DCTree)node.getReference());
        if (!node.getLabel().isEmpty()) {
            this.print(" ");
            for (DocTree docTree : node.getLabel()) {
                this.doAccept((DCTree)docTree);
            }
        }
        this.print("}");
        return null;
    }

    public Void visitLiteral(LiteralTree node, Void p) {
        this.print("{");
        this.printTagName((DocTree)node);
        this.print(" ");
        this.doAccept((DCTree)node.getBody());
        this.print("}");
        return null;
    }

    public Void visitParam(ParamTree node, Void p) {
        this.printTagName((DocTree)node);
        this.needSpace();
        if (node.isTypeParameter()) {
            this.print('<');
        }
        this.doAccept((DCTree)node.getName());
        if (node.isTypeParameter()) {
            this.print('>');
        }
        if (!node.getDescription().isEmpty()) {
            this.needSpace();
        }
        for (DocTree docTree : node.getDescription()) {
            this.doAccept((DCTree)docTree);
        }
        return null;
    }

    public Void visitReference(ReferenceTree node, Void p) {
        DCTree.DCReference refNode = (DCTree.DCReference)node;
        if (refNode.qualifierExpression != null) {
            this.print(refNode.qualifierExpression);
        }
        if (refNode.memberName != null) {
            this.print("#");
            this.print(refNode.memberName);
        }
        if (refNode.paramTypes != null) {
            this.print("(");
            boolean first = true;
            for (Tree param : refNode.paramTypes) {
                if (!first) {
                    this.print(", ");
                }
                this.print(param.toString());
                first = false;
            }
            this.print(")");
        }
        return null;
    }

    public Void visitReturn(ReturnTree node, Void p) {
        this.printTagName((DocTree)node);
        this.print(" ");
        for (DocTree docTree : node.getDescription()) {
            this.doAccept((DCTree)docTree);
        }
        return null;
    }

    public Void visitSee(SeeTree node, Void p) {
        this.printTagName((DocTree)node);
        boolean first = true;
        boolean needSep = true;
        for (DocTree t : node.getReference()) {
            if (needSep) {
                this.print(" ");
            }
            needSep = first && t instanceof ReferenceTree;
            first = false;
            this.print((DCTree)t);
        }
        return null;
    }

    public Void visitSerial(SerialTree node, Void p) {
        this.printTagName((DocTree)node);
        if (!node.getDescription().isEmpty()) {
            this.print(" ");
            for (DocTree docTree : node.getDescription()) {
                this.doAccept((DCTree)docTree);
            }
        }
        return null;
    }

    public Void visitSerialData(SerialDataTree node, Void p) {
        this.printTagName((DocTree)node);
        if (!node.getDescription().isEmpty()) {
            this.print(" ");
            for (DocTree docTree : node.getDescription()) {
                this.doAccept((DCTree)docTree);
            }
        }
        return null;
    }

    public Void visitSerialField(SerialFieldTree node, Void p) {
        this.printTagName((DocTree)node);
        this.print(" ");
        this.print((DCTree)node.getName());
        this.print(" ");
        this.print((DCTree)node.getType());
        if (!node.getDescription().isEmpty()) {
            this.print(" ");
            for (DocTree docTree : node.getDescription()) {
                this.doAccept((DCTree)docTree);
            }
        }
        return null;
    }

    public Void visitSince(SinceTree node, Void p) {
        this.printTagName((DocTree)node);
        this.print(" ");
        for (DocTree docTree : node.getBody()) {
            this.doAccept((DCTree)docTree);
        }
        return null;
    }

    public Void visitText(TextTree node, Void p) {
        this.print(node.getBody());
        return null;
    }

    public Void visitThrows(ThrowsTree node, Void p) {
        this.printTagName((DocTree)node);
        this.needSpace();
        this.doAccept((DCTree)node.getExceptionName());
        if (!node.getDescription().isEmpty()) {
            this.needSpace();
            for (DocTree docTree : node.getDescription()) {
                this.doAccept((DCTree)docTree);
            }
        }
        return null;
    }

    public Void visitUnknownBlockTag(UnknownBlockTagTree node, Void p) {
        this.print("@");
        this.print(node.getTagName());
        this.print(" ");
        for (DocTree docTree : node.getContent()) {
            this.doAccept((DCTree)docTree);
        }
        return null;
    }

    public Void visitUnknownInlineTag(UnknownInlineTagTree node, Void p) {
        this.print("{");
        this.print("@");
        this.print(node.getTagName());
        this.print(" ");
        for (DocTree docTree : node.getContent()) {
            this.doAccept((DCTree)docTree);
        }
        this.print("}");
        return null;
    }

    public Void visitValue(ValueTree node, Void p) {
        this.print("{");
        this.printTagName((DocTree)node);
        if (node.getReference() != null) {
            this.print(" ");
            this.print((DCTree)node.getReference());
        }
        this.print("}");
        return null;
    }

    public Void visitVersion(VersionTree node, Void p) {
        this.printTagName((DocTree)node);
        this.print(" ");
        for (DocTree docTree : node.getBody()) {
            this.doAccept((DCTree)docTree);
        }
        return null;
    }

    public Void visitOther(DocTree node, Void p) {
        this.print("(UNKNOWN: " + (Object)node + ")");
        this.newline();
        return null;
    }

    private void adjustSpans(Iterable<? extends Tree> original, String code) {
        if (this.tree2Tag == null) {
            return;
        }
        LinkedList linearized = new LinkedList();
        if ((Boolean)new Linearize().scan(original, linearized) == false != Boolean.TRUE) {
            return;
        }
        try {
            ClassPath empty = ClassPathSupport.createClassPath((URL[])new URL[0]);
            ClasspathInfo cpInfo = ClasspathInfo.create(JavaPlatformManager.getDefault().getDefaultPlatform().getBootstrapLibraries(), empty, empty);
            JavacTaskImpl javacTask = JavacParser.createJavacTask(cpInfo, null, null, null, null, null, null, null);
            Context ctx = javacTask.getContext();
            JavaCompiler.instance((Context)ctx).genEndPos = true;
            CompilationUnitTree tree = (CompilationUnitTree)javacTask.parse(new JavaFileObject[]{FileObjects.memoryFileObject("", "", code)}).iterator().next();
            DocSourcePositions sp = JavacTrees.instance((Context)ctx).getSourcePositions();
            ClassTree clazz = (ClassTree)tree.getTypeDecls().get(0);
            new CopyTags(tree, (SourcePositions)sp).scan((Iterable)clazz.getModifiers().getAnnotations(), linearized);
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    private static String whitespace(int num) {
        StringBuilder res = new StringBuilder(num);
        while (num-- > 0) {
            res.append(' ');
        }
        return res.toString();
    }

    private boolean printAnnotationsFormatted(List<JCTree.JCAnnotation> annotations) {
        if (this.reallyPrintAnnotations) {
            return false;
        }
        VeryPretty del = new VeryPretty(this.diffContext, this.cs, new HashMap(), this.tree2Doc, new HashMap(), this.origText, 0);
        del.reallyPrintAnnotations = true;
        del.printingMethodParams = this.printingMethodParams;
        del.printAnnotations(annotations);
        String str = del.out.toString();
        int col = this.printingMethodParams ? this.out.leftMargin + this.cs.getContinuationIndentSize() : this.out.col;
        str = Reformatter.reformat(str + " class A{}", this.cs, this.cs.getRightMargin() - col);
        str = str.trim().replaceAll("\n", "\n" + VeryPretty.whitespace(col));
        this.adjustSpans((Iterable<? extends Tree>)annotations, str);
        str = str.substring(0, str.lastIndexOf("class")).trim();
        this.print(str);
        return true;
    }

    private void printAnnotations(List<JCTree.JCAnnotation> annotations) {
        if (annotations.isEmpty()) {
            return;
        }
        if (this.printAnnotationsFormatted(annotations)) {
            if (!this.printingMethodParams) {
                this.toColExactly(this.out.leftMargin);
            } else {
                this.out.needSpace();
            }
            return;
        }
        while (annotations.nonEmpty()) {
            this.printNoParenExpr((JCTree)annotations.head);
            if (annotations.tail != null && annotations.tail.nonEmpty()) {
                switch (this.cs.wrapAnnotations()) {
                    case WRAP_IF_LONG: {
                        int rm = this.cs.getRightMargin();
                        if (this.widthEstimator.estimateWidth((JCTree)annotations.tail.head, rm - this.out.col) + this.out.col + 1 <= rm) {
                            this.print(' ');
                            break;
                        }
                    }
                    case WRAP_ALWAYS: {
                        this.newline();
                        this.toColExactly(this.out.leftMargin);
                        break;
                    }
                    case WRAP_NEVER: {
                        this.print(' ');
                    }
                }
            } else if (!this.printingMethodParams) {
                this.toColExactly(this.out.leftMargin);
            }
            annotations = annotations.tail;
        }
    }

    public void printFlags(long flags) {
        this.printFlags(flags, true);
    }

    public void printFinallyBlock(JCTree.JCBlock finalizer) {
        if (this.cs.placeFinallyOnNewLine()) {
            this.newline();
            this.toLeftMargin();
        } else if (this.cs.spaceBeforeFinally()) {
            this.needSpace();
        }
        this.print("finally");
        this.printBlock((JCTree)finalizer, this.cs.getOtherBracePlacement(), this.cs.spaceBeforeFinallyLeftBrace());
    }

    public void printFlags(long flags, boolean addSpace) {
        this.print(VeryPretty.flagNames(flags & -513 & -8193 & -16385));
        if ((flags & 4095) != 0) {
            if (this.cs.placeNewLineAfterModifiers()) {
                this.toColExactly(this.out.leftMargin);
            } else if (addSpace) {
                this.needSpace();
            }
        }
    }

    public static String flagNames(long flags) {
        StringBuilder buf = new StringBuilder();
        String sep = "";
        for (Flags.Flag flag : Flags.asFlagSet((long)(flags &= 8796093026303L))) {
            buf.append(sep);
            String fname = flagLowerCaseNames[flag.ordinal()];
            buf.append(fname);
            sep = " ";
        }
        return buf.toString().trim();
    }

    public void printBlock(JCTree oldT, JCTree newT, Tree.Kind parentKind) {
        switch (parentKind) {
            case ENHANCED_FOR_LOOP: 
            case FOR_LOOP: {
                this.printIndentedStat(newT, this.cs.redundantForBraces(), this.cs.spaceBeforeForLeftBrace(), this.cs.wrapForStatement());
                break;
            }
            case WHILE_LOOP: {
                this.printIndentedStat(newT, this.cs.redundantWhileBraces(), this.cs.spaceBeforeWhileLeftBrace(), this.cs.wrapWhileStatement());
                break;
            }
            case IF: {
                this.printIndentedStat(newT, this.cs.redundantIfBraces(), this.cs.spaceBeforeIfLeftBrace(), this.cs.wrapIfStatement());
                break;
            }
            case DO_WHILE_LOOP: {
                this.printIndentedStat(newT, this.cs.redundantDoWhileBraces(), this.cs.spaceBeforeDoLeftBrace(), this.cs.wrapDoWhileStatement());
                if (this.cs.placeWhileOnNewLine()) {
                    this.newline();
                    this.toLeftMargin();
                    break;
                }
                if (!this.cs.spaceBeforeWhile()) break;
                this.needSpace();
            }
        }
    }

    public void printImportsBlock(java.util.List<? extends JCTree> imports, boolean maybeAppendNewLine) {
        boolean hasImports = !imports.isEmpty();
        CodeStyle.ImportGroups importGroups = null;
        if (hasImports) {
            this.blankLines(Math.max(this.cs.getBlankLinesBeforeImports(), this.diffContext.origUnit.pid != null ? this.cs.getBlankLinesAfterPackage() : 0));
            if (this.cs.separateImportGroups()) {
                importGroups = this.cs.getImportGroups();
            }
        }
        int lastGroup = -1;
        for (JCTree importStat : imports) {
            if (importGroups != null) {
                int group;
                Name name = this.fullName(((JCTree.JCImport)importStat).qualid);
                int n = group = name != null ? importGroups.getGroupId(name.toString(), ((JCTree.JCImport)importStat).staticImport) : -1;
                if (lastGroup >= 0 && lastGroup != group) {
                    this.blankline();
                }
                lastGroup = group;
            }
            this.printStat(importStat);
            this.newline();
        }
        if (hasImports && maybeAppendNewLine) {
            this.blankLines(this.cs.getBlankLinesAfterImports());
        }
    }

    public void eatChars(int count) {
        this.out.eatAwayChars(count);
    }

    private void printExpr(JCTree tree) {
        this.printExpr(tree, 0);
    }

    private void printNoParenExpr(JCTree tree) {
        while (tree instanceof JCTree.JCParens) {
            tree = ((JCTree.JCParens)tree).expr;
        }
        this.printExpr(tree, 0);
    }

    private void printExpr(JCTree tree, int prec) {
        if (tree == null) {
            this.print("/*missing*/");
        } else {
            int prevPrec = this.prec;
            this.prec = prec;
            this.doAccept(tree, this.commentsEnabled);
            this.prec = prevPrec;
        }
    }

    private <T extends JCTree> void printExprs(List<T> trees) {
        String sep = this.cs.spaceBeforeComma() ? " ," : ",";
        this.printExprs(trees, this.cs.spaceAfterComma() ? sep + " " : sep);
    }

    private <T extends JCTree> void printExprs(List<T> trees, String sep) {
        if (trees.nonEmpty()) {
            this.printNoParenExpr((JCTree)trees.head);
            List l = trees.tail;
            while (l.nonEmpty()) {
                this.print(sep);
                this.printNoParenExpr((JCTree)l.head);
                l = l.tail;
            }
        }
    }

    private void printStat(JCTree tree) {
        this.printStat(tree, false, false);
    }

    private void printStat(JCTree tree, boolean member, boolean first) {
        this.printStat(tree, member, first, false, false);
    }

    private void printStat(JCTree tree, boolean member, boolean first, boolean col, boolean nl) {
        if (tree == null) {
            if (col) {
                this.toColExactly(this.out.leftMargin);
            }
            this.print(';');
            if (nl) {
                this.newline();
            }
        } else {
            if (!first) {
                this.blankLines(tree, true);
            }
            if (col) {
                this.toColExactly(this.out.leftMargin);
            }
            this.printInnerCommentsAsTrailing(tree, !member);
            this.printExpr(tree, -1);
            int tag = tree.getTag().ordinal();
            if (JCTree.Tag.APPLY.ordinal() <= tag && tag <= JCTree.Tag.MOD_ASG.ordinal()) {
                this.print(';');
            }
            this.printTrailingComments(tree, !member);
            this.blankLines(tree, false);
            if (nl) {
                this.newline();
            }
        }
    }

    private void printIndentedStat(JCTree tree, CodeStyle.BracesGenerationStyle redundantBraces, boolean spaceBeforeLeftBrace, CodeStyle.WrapStyle wrapStat) {
        if (this.fromOffset >= 0 && this.toOffset >= 0 && (TreeInfo.getStartPos((JCTree)tree) < this.fromOffset || TreeInfo.getEndPos((JCTree)tree, (EndPosTable)this.diffContext.origUnit.endPositions) > this.toOffset)) {
            redundantBraces = CodeStyle.BracesGenerationStyle.LEAVE_ALONE;
        }
        switch (redundantBraces) {
            case GENERATE: {
                this.printBlock(tree, this.cs.getOtherBracePlacement(), spaceBeforeLeftBrace);
                return;
            }
            case ELIMINATE: {
                List t;
                while (tree instanceof JCTree.JCBlock && !(t = ((JCTree.JCBlock)tree).stats).isEmpty() && !t.tail.nonEmpty() && !(t.head instanceof JCTree.JCVariableDecl)) {
                    this.printPrecedingComments(tree, true);
                    tree = (JCTree)t.head;
                }
            }
            case LEAVE_ALONE: {
                if (tree instanceof JCTree.JCBlock) {
                    this.printBlock(tree, this.cs.getOtherBracePlacement(), spaceBeforeLeftBrace);
                    return;
                }
                final int old = this.indent();
                final JCTree toPrint = tree;
                this.wrapTree(wrapStat, spaceBeforeLeftBrace, this.out.leftMargin, new Runnable(){

                    @Override
                    public void run() {
                        VeryPretty.this.printStat(toPrint);
                        VeryPretty.this.undent(old);
                    }
                });
            }
        }
    }

    private void printStats(List<? extends JCTree> trees) {
        this.printStats(trees, false);
    }

    private void printStats(List<? extends JCTree> trees, boolean members) {
        java.util.List<JCTree> filtered = CasualDiff.filterHidden(this.diffContext, trees);
        if (!filtered.isEmpty() && this.handlePossibleOldTrees(filtered, true)) {
            return;
        }
        boolean first = true;
        for (JCTree t : filtered) {
            this.printStat(t, members, first, true, false);
            first = false;
        }
    }

    private void printBlock(JCTree t, CodeStyle.BracePlacement bracePlacement, boolean spaceBeforeLeftBrace) {
        List stats;
        JCTree block;
        if (t instanceof JCTree.JCBlock) {
            block = t;
            stats = ((JCTree.JCBlock)t).stats;
        } else {
            block = null;
            stats = List.of((Object)t);
        }
        this.printBlock(block, stats, bracePlacement, spaceBeforeLeftBrace, true);
    }

    private void printBlock(JCTree tree, List<? extends JCTree> stats, CodeStyle.BracePlacement bracePlacement, boolean spaceBeforeLeftBrace, boolean printComments) {
        this.printBlock(tree, stats, bracePlacement, spaceBeforeLeftBrace, false, printComments);
    }

    private void printBlock(JCTree tree, List<? extends JCTree> stats, CodeStyle.BracePlacement bracePlacement, boolean spaceBeforeLeftBrace, boolean members, boolean printComments) {
        int old;
        if (printComments) {
            this.printPrecedingComments(tree, true);
        }
        int bcol = old = this.indent();
        switch (bracePlacement) {
            case NEW_LINE: {
                this.newline();
                this.toColExactly(old);
                break;
            }
            case NEW_LINE_HALF_INDENTED: {
                this.newline();
                this.toColExactly(bcol += this.indentSize >> 1);
                break;
            }
            case NEW_LINE_INDENTED: {
                this.newline();
                bcol = this.out.leftMargin;
                this.toColExactly(bcol);
            }
        }
        String trailing = null;
        if (this.conditionStartHack != -1) {
            boolean found;
            TokenSequence ts = TokenHierarchy.create((CharSequence)this.toString().substring(this.conditionStartHack), (Language)JavaTokenId.language()).tokenSequence(JavaTokenId.language());
            ts.moveEnd();
            while ((found = ts.movePrevious()) && PositionEstimator.nonRelevant.contains((Object)ts.token().id())) {
            }
            if (found) {
                String content = this.toString();
                trailing = content.substring(this.conditionStartHack + ts.offset() + ts.token().text().length());
                this.out.used -= trailing.length();
                this.out.col -= trailing.length();
            }
        }
        if (spaceBeforeLeftBrace) {
            this.needSpace();
        }
        this.print('{');
        if (trailing != null) {
            this.print(trailing);
        }
        boolean emptyBlock = true;
        List l = stats;
        while (l.nonEmpty()) {
            if (!this.isSynthetic((JCTree)l.head)) {
                emptyBlock = false;
                break;
            }
            l = l.tail;
        }
        if (emptyBlock) {
            this.printEmptyBlockComments(tree, members);
        } else {
            this.innerCommentsHandled.add((Tree)tree);
            java.util.List<Comment> comments = this.commentHandler.getComments((Tree)tree).getComments(CommentSet.RelativePosition.INNER);
            for (Comment c : comments) {
                this.printComment(c, false, members);
            }
            if (members) {
                this.blankLines(this.enclClassName.isEmpty() ? this.cs.getBlankLinesAfterAnonymousClassHeader() : this.cs.getBlankLinesAfterClassHeader());
            } else {
                this.newline();
            }
            this.printStats(stats, members);
        }
        this.toColExactly(bcol);
        this.undent(old);
        this.print('}');
        if (printComments) {
            this.printTrailingComments(tree, true);
        }
    }

    private void printTypeParameters(List<JCTree.JCTypeParameter> trees) {
        if (trees.nonEmpty()) {
            this.print('<');
            this.printExprs(trees);
            this.print('>');
        }
    }

    private void printTypeArguments(List<? extends JCTree.JCExpression> typeargs) {
        if (typeargs.nonEmpty()) {
            this.print('<');
            this.printExprs(typeargs);
            this.print('>');
        }
    }

    private void printPrecedingComments(JCTree tree, boolean printWhitespace) {
        if (!this.precedingCommentsHandled.add((Tree)tree)) {
            return;
        }
        CommentSet commentSet = this.commentHandler.getComments((Tree)tree);
        java.util.List<Comment> pc = commentSet.getComments(CommentSet.RelativePosition.PRECEDING);
        DocCommentTree doc = this.tree2Doc.get((Object)tree);
        if (!pc.isEmpty()) {
            Comment javadoc = null;
            for (Comment comment : pc) {
                if (comment.style() != Comment.Style.JAVADOC) continue;
                javadoc = comment;
            }
            for (Comment c : pc) {
                if (doc != null && c == javadoc) {
                    this.print((DCTree)doc);
                    doc = null;
                    continue;
                }
                this.printComment(c, true, printWhitespace);
            }
        }
        if (doc != null) {
            this.print((DCTree)doc);
        }
    }

    private void printInnerCommentsAsTrailing(JCTree tree, boolean printWhitespace) {
        if (this.innerCommentsHandled.contains((Object)tree)) {
            return;
        }
        CommentSet commentSet = this.commentHandler.getComments((Tree)tree);
        java.util.List<Comment> cl = commentSet.getComments(CommentSet.RelativePosition.INNER);
        this.innerCommentsHandled.add((Tree)tree);
        for (Comment comment : cl) {
            this.printComment(comment, true, printWhitespace);
        }
    }

    private void printTrailingComments(JCTree tree, boolean printWhitespace) {
        if (this.trailingCommentsHandled.contains((Object)tree)) {
            return;
        }
        CommentSet commentSet = this.commentHandler.getComments((Tree)tree);
        java.util.List<Comment> cl = commentSet.getComments(CommentSet.RelativePosition.INLINE);
        for (Comment comment : cl) {
            this.trailingCommentsHandled.add((Tree)tree);
            this.printComment(comment, true, printWhitespace);
        }
        java.util.List<Comment> tc = commentSet.getComments(CommentSet.RelativePosition.TRAILING);
        if (!tc.isEmpty()) {
            this.trailingCommentsHandled.add((Tree)tree);
            for (Comment c : tc) {
                this.printComment(c, false, printWhitespace);
            }
        }
    }

    private void printEmptyBlockComments(JCTree tree, boolean printWhitespace) {
        this.innerCommentsHandled.add((Tree)tree);
        java.util.List<Comment> comments = this.commentHandler.getComments((Tree)tree).getComments(CommentSet.RelativePosition.INNER);
        for (Comment c : comments) {
            this.printComment(c, false, printWhitespace);
        }
    }

    public void printComment(Comment comment, boolean preceding, boolean printWhitespace) {
        this.printComment(comment, preceding, printWhitespace, false);
    }

    public void printComment(Comment comment, boolean preceding, boolean printWhitespace, boolean preventClosingWhitespace) {
        boolean onlyWhitespaces = this.out.isWhitespaceLine();
        if (Comment.Style.WHITESPACE == comment.style()) {
            return;
        }
        String body = comment.getText();
        boolean rawBody = body.length() == 0 || body.charAt(0) != '/';
        LinkedList<CommentLine> lines = new LinkedList<CommentLine>();
        int stpos = -1;
        int limit = body.length();
        for (int i = 0; i < limit; ++i) {
            char c = body.charAt(i);
            if (c == '\n') {
                lines.add(new CommentLine(stpos, stpos < 0 ? 0 : i - stpos, body));
                stpos = -1;
                continue;
            }
            if (c <= ' ' || stpos >= 0) continue;
            stpos = i;
        }
        if (stpos >= 0 && stpos < limit) {
            lines.add(new CommentLine(stpos, limit - stpos, body));
        }
        if (comment.pos() > 0 && comment.endPos() < this.diffContext.origText.length() && this.diffContext.origText.substring(comment.pos() - 1, comment.endPos()).contentEquals("\n" + comment.getText())) {
            if (this.out.lastBlankLines == 0 && !preceding) {
                this.newline();
            }
            this.out.toLineStart();
        } else if (comment.indent() == 0) {
            if (!preceding && this.out.lastBlankLines == 0 && comment.style() != Comment.Style.LINE) {
                this.newline();
            }
            this.out.toLineStart();
        } else if (comment.indent() > 0 && !preceding) {
            if (this.out.lastBlankLines == 0 && comment.style() != Comment.Style.LINE) {
                this.newline();
            }
            this.toLeftMargin();
        } else if (comment.indent() < 0 && !preceding) {
            if (this.out.lastBlankLines == 0) {
                this.newline();
            }
            this.toLeftMargin();
        } else {
            this.needSpace();
        }
        if (rawBody) {
            switch (comment.style()) {
                case LINE: {
                    this.print("// ");
                    break;
                }
                case BLOCK: {
                    this.print("/* ");
                    break;
                }
                case JAVADOC: {
                    if (!onlyWhitespaces) {
                        this.newline();
                    }
                    this.toLeftMargin();
                    this.print("/**");
                    this.newline();
                    this.toLeftMargin();
                    this.print(" * ");
                }
            }
        }
        if (!lines.isEmpty()) {
            ((CommentLine)lines.removeFirst()).print(this.out.col);
        }
        while (!lines.isEmpty()) {
            this.newline();
            this.toLeftMargin();
            CommentLine line = (CommentLine)lines.removeFirst();
            if (rawBody) {
                this.print(" * ");
            } else if (line.body.charAt(line.startPos) == '*') {
                this.print(' ');
            }
            line.print(this.out.col);
        }
        if (rawBody) {
            switch (comment.style()) {
                case BLOCK: {
                    this.print(" */");
                    break;
                }
                case JAVADOC: {
                    this.newline();
                    this.toLeftMargin();
                    this.print(" */");
                    this.newline();
                    this.toLeftMargin();
                }
            }
        }
        if (onlyWhitespaces && !preventClosingWhitespace || comment.style() == Comment.Style.LINE) {
            this.newline();
            if (!preventClosingWhitespace) {
                this.toLeftMargin();
            }
        } else if (!preventClosingWhitespace) {
            this.needSpace();
        }
    }

    private void wrap(String s, CodeStyle.WrapStyle wrapStyle) {
        switch (wrapStyle) {
            case WRAP_IF_LONG: {
                if (s.length() + this.out.col <= this.cs.getRightMargin()) {
                    this.print(' ');
                    break;
                }
            }
            case WRAP_ALWAYS: {
                this.newline();
                this.toColExactly(this.out.leftMargin + this.cs.getContinuationIndentSize());
                break;
            }
            case WRAP_NEVER: {
                this.print(' ');
            }
        }
        this.print(s);
    }

    private <T extends JCTree> void wrapTrees(List<T> trees, CodeStyle.WrapStyle wrapStyle, int wrapIndent) {
        this.wrapTrees(trees, wrapStyle, wrapIndent, false);
    }

    private <T extends JCTree> void wrapTrees(List<T> trees, CodeStyle.WrapStyle wrapStyle, int wrapIndent, boolean wrapFirst) {
        this.wrapTrees(trees, wrapStyle, wrapIndent, wrapFirst, this.cs.spaceBeforeComma(), this.cs.spaceAfterComma(), ",");
    }

    private <T extends JCTree> void wrapTrees(List<T> trees, CodeStyle.WrapStyle wrapStyle, int wrapIndent, boolean wrapFirst, boolean spaceBeforeSeparator, boolean spaceAfterSeparator, String separator) {
        boolean first = true;
        List l = trees;
        while (l.nonEmpty()) {
            if (!first) {
                if (spaceBeforeSeparator) {
                    this.print(' ');
                }
                this.print(separator);
            }
            if (!first || wrapFirst) {
                switch (first && wrapStyle != CodeStyle.WrapStyle.WRAP_NEVER ? CodeStyle.WrapStyle.WRAP_IF_LONG : wrapStyle) {
                    case WRAP_IF_LONG: {
                        int rm = this.cs.getRightMargin();
                        boolean space = spaceAfterSeparator && !first;
                        if (this.widthEstimator.estimateWidth((JCTree)l.head, rm - this.out.col) + this.out.col + (space ? 1 : 0) <= rm) {
                            if (!space) break;
                            this.print(' ');
                            break;
                        }
                    }
                    case WRAP_ALWAYS: {
                        this.newline();
                        this.toColExactly(wrapIndent);
                        break;
                    }
                    case WRAP_NEVER: {
                        if (!spaceAfterSeparator || first) break;
                        this.print(' ');
                    }
                }
            }
            this.printNoParenExpr((JCTree)l.head);
            first = false;
            l = l.tail;
        }
    }

    private void wrapAssignOpTree(final String operator, int col, final Runnable print) {
        final boolean spaceAroundAssignOps = this.cs.spaceAroundAssignOps();
        if (this.cs.wrapAfterAssignOps()) {
            if (spaceAroundAssignOps) {
                this.print(' ');
            }
            this.print(operator);
        }
        this.wrapTree(this.cs.wrapAssignOps(), spaceAroundAssignOps, this.cs.alignMultilineAssignment() ? col : this.out.leftMargin + this.cs.getContinuationIndentSize(), new Runnable(){

            @Override
            public void run() {
                if (!VeryPretty.this.cs.wrapAfterAssignOps()) {
                    VeryPretty.this.print(operator);
                    if (spaceAroundAssignOps) {
                        VeryPretty.this.print(' ');
                    }
                }
                print.run();
            }
        });
    }

    private void wrapTree(CodeStyle.WrapStyle wrapStyle, boolean needsSpaceBefore, int colAfterWrap, Runnable print) {
        switch (wrapStyle) {
            case WRAP_NEVER: {
                if (needsSpaceBefore) {
                    this.needSpace();
                }
                print.run();
                return;
            }
            case WRAP_IF_LONG: {
                int oldhm = this.out.harden();
                int oldc = this.out.col;
                int oldu = this.out.used;
                int oldm = this.out.leftMargin;
                int oldPrec = this.prec;
                try {
                    if (needsSpaceBefore) {
                        this.needSpace();
                    }
                    print.run();
                    this.out.restore(oldhm);
                    return;
                }
                catch (Throwable t) {
                    this.out.restore(oldhm);
                    this.out.col = oldc;
                    this.out.used = oldu;
                    this.out.leftMargin = oldm;
                    this.prec = oldPrec;
                }
            }
            case WRAP_ALWAYS: {
                if (this.out.col > 0) {
                    this.newline();
                }
                this.toColExactly(colAfterWrap);
                print.run();
            }
        }
    }

    public Name fullName(JCTree tree) {
        switch (tree.getTag()) {
            case IDENT: {
                return ((JCTree.JCIdent)tree).name;
            }
            case SELECT: {
                JCTree.JCFieldAccess sel = (JCTree.JCFieldAccess)tree;
                Name sname = this.fullName((JCTree)sel.selected);
                return sname != null && sname.getByteLength() > 0 ? sname.append('.', sel.name) : sel.name;
            }
        }
        return null;
    }

    private boolean isSynthetic(JCTree tree) {
        if (tree.getKind() == Tree.Kind.METHOD) {
            return (((JCTree.JCMethodDecl)tree).mods.flags & 0x1000000000L) != 0;
        }
        if (tree.getKind() == Tree.Kind.EXPRESSION_STATEMENT && this.diffContext.origUnit != null) {
            JCTree.JCExpressionStatement est = (JCTree.JCExpressionStatement)tree;
            if (est.expr.getKind() == Tree.Kind.METHOD_INVOCATION) {
                JCTree.JCMethodInvocation mit = (JCTree.JCMethodInvocation)est.getExpression();
                if (mit.meth.getKind() == Tree.Kind.IDENTIFIER) {
                    JCTree.JCIdent it = (JCTree.JCIdent)mit.getMethodSelect();
                    return it.name == this.names._super && this.diffContext.syntheticTrees.contains((Object)tree);
                }
            }
        }
        return false;
    }

    private static boolean isEnumerator(JCTree tree) {
        return tree.getTag() == JCTree.Tag.VARDEF && (((JCTree.JCVariableDecl)tree).mods.flags & 16384) != 0;
    }

    private String replace(String a, String b) {
        a = a.replace(b, this.out.toString());
        this.out.clear();
        return a;
    }

    static {
        for (TypeTag tt : TypeTag.values()) {
            VeryPretty.typeTagNames[tt.ordinal()] = tt.name().toLowerCase(Locale.ENGLISH);
        }
        flagLowerCaseNames = new String[Flags.Flag.values().length];
        for (TypeTag flag : Flags.Flag.values()) {
            VeryPretty.flagLowerCaseNames[flag.ordinal()] = flag.name().toLowerCase(Locale.ENGLISH);
        }
    }

    private class CommentLine {
        private int startPos;
        private int length;
        private String body;

        CommentLine(int sp, int l, String b) {
            this.length = l;
            this.startPos = this.length == 0 ? 0 : sp;
            this.body = b;
        }

        public void print(int col) {
            if (this.length > 0) {
                int limit = this.startPos + this.length;
                for (int i = this.startPos; i < limit; ++i) {
                    VeryPretty.this.out.append(this.body.charAt(i));
                }
            }
        }
    }

    private final class CopyTags
    extends TreeScanner<Void, java.util.List<Tree>> {
        private final CompilationUnitTree fake;
        private final SourcePositions sp;

        public CopyTags(CompilationUnitTree fake, SourcePositions sp) {
            this.fake = fake;
            this.sp = sp;
        }

        public Void scan(Tree node, java.util.List<Tree> p) {
            if (p.isEmpty()) {
                return null;
            }
            Object tag = VeryPretty.this.tree2Tag.get((Object)p.remove(0));
            if (tag != null) {
                VeryPretty.this.tag2Span.put(tag, new int[]{VeryPretty.this.out.length() + VeryPretty.this.initialOffset + (int)this.sp.getStartPosition(this.fake, node), VeryPretty.this.out.length() + VeryPretty.this.initialOffset + (int)this.sp.getEndPosition(this.fake, node)});
            }
            return (Void)TreeScanner.super.scan(node, p);
        }
    }

    private final class Linearize
    extends TreeScanner<Boolean, java.util.List<Tree>> {
        private Linearize() {
        }

        public Boolean scan(Tree node, java.util.List<Tree> p) {
            p.add(node);
            TreeScanner.super.scan(node, p);
            return VeryPretty.this.tree2Tag.containsKey((Object)node);
        }

        public Boolean reduce(Boolean r1, Boolean r2) {
            return r1 == Boolean.TRUE || r2 == Boolean.TRUE;
        }
    }

}

