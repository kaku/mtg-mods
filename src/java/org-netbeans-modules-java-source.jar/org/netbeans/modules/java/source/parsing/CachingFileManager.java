/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 *  org.openide.util.Exceptions
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.java.source.parsing;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.tools.FileObject;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.source.classpath.CacheClassPath;
import org.netbeans.modules.java.source.parsing.Archive;
import org.netbeans.modules.java.source.parsing.CachingArchiveProvider;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.InferableJavaFileObject;
import org.netbeans.modules.java.source.util.Iterators;
import org.openide.util.Exceptions;
import org.openide.util.WeakListeners;

public class CachingFileManager
implements JavaFileManager,
PropertyChangeListener {
    private final CachingArchiveProvider provider;
    private final boolean cacheFile;
    private final JavaFileFilterImplementation filter;
    private final boolean ignoreExcludes;
    private final ClassPath cp;
    private final boolean allowOutput;
    private static final Logger LOG = Logger.getLogger(CachingFileManager.class.getName());

    public CachingFileManager(CachingArchiveProvider provider, ClassPath cp, boolean cacheFile, boolean ignoreExcludes) {
        this(provider, cp, null, false, cacheFile, ignoreExcludes);
    }

    public CachingFileManager(CachingArchiveProvider provider, ClassPath cp, JavaFileFilterImplementation filter, boolean cacheFile, boolean ignoreExcludes) {
        this(provider, cp, filter, true, cacheFile, ignoreExcludes);
    }

    private CachingFileManager(CachingArchiveProvider provider, ClassPath cp, JavaFileFilterImplementation filter, boolean allowOutput, boolean cacheFile, boolean ignoreExcludes) {
        assert (provider != null);
        assert (cp != null);
        this.provider = provider;
        this.cp = cp;
        if (CacheClassPath.KEEP_JARS) {
            cp.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)cp));
        }
        this.filter = filter;
        this.allowOutput = allowOutput;
        this.cacheFile = cacheFile;
        this.ignoreExcludes = ignoreExcludes;
    }

    @Override
    public Iterable<JavaFileObject> list(JavaFileManager.Location l, String packageName, Set<JavaFileObject.Kind> kinds, boolean recursive) {
        if (recursive) {
            throw new UnsupportedOperationException("Recursive listing is not supported in archives");
        }
        String folderName = FileObjects.convertPackage2Folder(packageName);
        LinkedList<Iterable<JavaFileObject>> idxs = new LinkedList<Iterable<JavaFileObject>>();
        for (ClassPath.Entry entry : this.cp.entries()) {
            try {
                Archive archive = this.provider.getArchive(entry.getURL(), this.cacheFile);
                if (archive != null) {
                    Iterable<JavaFileObject> entries = archive.getFiles(folderName, this.ignoreExcludes ? null : entry, kinds, this.filter);
                    idxs.add(entries);
                    if (!LOG.isLoggable(Level.FINEST)) continue;
                    StringBuilder urls = new StringBuilder();
                    for (JavaFileObject jfo : entries) {
                        urls.append(jfo.toUri().toString());
                        urls.append(", ");
                    }
                    LOG.finest(String.format("cache for %s (%s) package: %s type: %s files: [%s]", l.toString(), entry.getURL().toExternalForm(), packageName, kinds.toString(), urls.toString()));
                    continue;
                }
                if (!LOG.isLoggable(Level.FINEST)) continue;
                LOG.finest(String.format("no cache for: %s", entry.getURL().toExternalForm()));
            }
            catch (IOException e) {
                Exceptions.printStackTrace((Throwable)e);
            }
        }
        return Iterators.chained(idxs);
    }

    @Override
    public FileObject getFileForInput(JavaFileManager.Location l, String pkgName, String relativeName) {
        return this.findFile(pkgName, relativeName);
    }

    @Override
    public JavaFileObject getJavaFileForInput(JavaFileManager.Location l, String className, JavaFileObject.Kind kind) {
        String[] namePair = FileObjects.getParentRelativePathAndName(className);
        if (namePair == null) {
            return null;
        }
        namePair[1] = namePair[1] + kind.extension;
        for (ClassPath.Entry root : this.cp.entries()) {
            try {
                Archive archive = this.provider.getArchive(root.getURL(), this.cacheFile);
                if (archive == null) continue;
                Iterable<JavaFileObject> files = archive.getFiles(namePair[0], this.ignoreExcludes ? null : root, null, this.filter);
                for (JavaFileObject e : files) {
                    if (!namePair[1].equals(e.getName())) continue;
                    return e;
                }
                continue;
            }
            catch (IOException e) {
                Exceptions.printStackTrace((Throwable)e);
                continue;
            }
        }
        return null;
    }

    @Override
    public FileObject getFileForOutput(JavaFileManager.Location l, String pkgName, String relativeName, FileObject sibling) throws IOException, UnsupportedOperationException, IllegalArgumentException {
        List entries;
        if (!this.allowOutput) {
            throw new UnsupportedOperationException("Output is unsupported.");
        }
        JavaFileObject file = this.findFile(pkgName, relativeName);
        if (file == null && !(entries = this.cp.entries()).isEmpty()) {
            String resourceName = FileObjects.getRelativePath(FileObjects.convertPackage2Folder(pkgName), relativeName);
            file = this.provider.getArchive(((ClassPath.Entry)entries.get(0)).getURL(), this.cacheFile).create(resourceName, this.filter);
        }
        return file;
    }

    @Override
    public JavaFileObject getJavaFileForOutput(JavaFileManager.Location l, String className, JavaFileObject.Kind kind, FileObject sibling) throws IOException, UnsupportedOperationException, IllegalArgumentException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void flush() throws IOException {
    }

    @Override
    public void close() throws IOException {
    }

    @Override
    public int isSupportedOption(String string) {
        return -1;
    }

    @Override
    public boolean handleOption(String head, Iterator<String> tail) {
        return false;
    }

    @Override
    public boolean hasLocation(JavaFileManager.Location location) {
        return true;
    }

    @Override
    public ClassLoader getClassLoader(JavaFileManager.Location l) {
        return null;
    }

    @Override
    public String inferBinaryName(JavaFileManager.Location l, JavaFileObject javaFileObject) {
        if (javaFileObject instanceof FileObjects.Base) {
            FileObjects.Base base = (FileObjects.Base)javaFileObject;
            StringBuilder sb = new StringBuilder();
            sb.append(base.getPackage());
            sb.append('.');
            sb.append(base.getNameWithoutExtension());
            return sb.toString();
        }
        if (javaFileObject instanceof InferableJavaFileObject) {
            return ((InferableJavaFileObject)javaFileObject).inferBinaryName();
        }
        return null;
    }

    public static URL[] getClassPathRoots(ClassPath cp) {
        assert (cp != null);
        List entries = cp.entries();
        ArrayList<URL> result = new ArrayList<URL>(entries.size());
        for (ClassPath.Entry entry : entries) {
            result.add(entry.getURL());
        }
        return result.toArray(new URL[result.size()]);
    }

    @Override
    public boolean isSameFile(FileObject fileObject, FileObject fileObject0) {
        return fileObject instanceof FileObjects.FileBase && fileObject0 instanceof FileObjects.FileBase && ((FileObjects.FileBase)fileObject).getFile().equals(((FileObjects.FileBase)fileObject0).getFile());
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("roots".equals(evt.getPropertyName())) {
            this.provider.clear();
        }
    }

    private JavaFileObject findFile(String pkgName, String relativeName) {
        assert (pkgName != null);
        assert (relativeName != null);
        String resourceName = FileObjects.getRelativePath(pkgName, relativeName);
        for (ClassPath.Entry root : this.cp.entries()) {
            try {
                JavaFileObject file;
                Archive archive = this.provider.getArchive(root.getURL(), this.cacheFile);
                if (archive == null || (file = archive.getFile(resourceName)) == null) continue;
                return file;
            }
            catch (IOException e) {
                Exceptions.printStackTrace((Throwable)e);
                continue;
            }
        }
        return null;
    }
}

