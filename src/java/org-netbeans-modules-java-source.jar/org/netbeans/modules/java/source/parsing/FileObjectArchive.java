/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package org.netbeans.modules.java.source.parsing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import javax.tools.JavaFileObject;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.source.parsing.Archive;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public class FileObjectArchive
implements Archive {
    private final FileObject root;

    public FileObjectArchive(FileObject root) {
        this.root = root;
    }

    @Override
    public Iterable<JavaFileObject> getFiles(String folderName, ClassPath.Entry entry, Set<JavaFileObject.Kind> kinds, JavaFileFilterImplementation filter) throws IOException {
        FileObject folder = this.root.getFileObject(folderName);
        if (folder == null || entry != null && !entry.includes(folder)) {
            return Collections.emptySet();
        }
        FileObject[] children = folder.getChildren();
        ArrayList<JavaFileObject> result = new ArrayList<JavaFileObject>(children.length);
        for (FileObject fo : children) {
            if (!fo.isData() || entry != null && !entry.includes(fo) || kinds != null && !kinds.contains((Object)FileObjects.getKind(fo.getExt()))) continue;
            result.add(FileObjects.sourceFileObject(fo, this.root, filter, false));
        }
        return result;
    }

    @Override
    public JavaFileObject create(String relativePath, JavaFileFilterImplementation filter) {
        throw new UnsupportedOperationException("Write not supported");
    }

    @Override
    public void clear() {
    }

    @Override
    public JavaFileObject getFile(String name) throws IOException {
        FileObject file = this.root.getFileObject(name);
        return file == null ? null : FileObjects.sourceFileObject(file, this.root, null, false);
    }

    public String toString() {
        return String.format("%s[folder: %s]", this.getClass().getSimpleName(), FileUtil.getFileDisplayName((FileObject)this.root));
    }
}

