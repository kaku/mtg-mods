/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.CreateFromTemplateAttributesProvider
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.java.source.save;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.openide.filesystems.FileObject;
import org.openide.loaders.CreateFromTemplateAttributesProvider;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.util.Lookup;

public class OverlayTemplateAttributesProvider
implements CreateFromTemplateAttributesProvider {
    public static final String ATTR_ORIG_FILE = OverlayTemplateAttributesProvider.class.getName() + "-orig-file";

    public Map<String, ?> attributesFor(DataObject template, DataFolder target, String name) {
        DataFolder origFile = (DataFolder)target.getPrimaryFile().getAttribute(ATTR_ORIG_FILE);
        if (origFile == null) {
            return null;
        }
        HashMap all = new HashMap();
        for (CreateFromTemplateAttributesProvider provider : Lookup.getDefault().lookupAll(CreateFromTemplateAttributesProvider.class)) {
            Map map = provider.attributesFor(template, origFile, name);
            if (map == null) continue;
            for (Map.Entry e : map.entrySet()) {
                all.put(e.getKey(), e.getValue());
            }
        }
        return all;
    }
}

