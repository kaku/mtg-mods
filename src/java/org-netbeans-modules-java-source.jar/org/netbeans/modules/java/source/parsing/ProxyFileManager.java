/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.api.ClientCodeWrapper
 *  com.sun.tools.javac.api.ClientCodeWrapper$Trusted
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 */
package org.netbeans.modules.java.source.parsing;

import com.sun.tools.javac.api.ClientCodeWrapper;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.tools.FileObject;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.InferableJavaFileObject;
import org.netbeans.modules.java.source.parsing.MemoryFileManager;
import org.netbeans.modules.java.source.parsing.ProcessorGenerated;
import org.netbeans.modules.java.source.parsing.SiblingProvider;
import org.netbeans.modules.java.source.parsing.SiblingSource;
import org.netbeans.modules.java.source.util.Iterators;

@ClientCodeWrapper.Trusted
public final class ProxyFileManager
implements JavaFileManager {
    private static final Logger LOG = Logger.getLogger(ProxyFileManager.class.getName());
    private static final JavaFileManager.Location ALL = new JavaFileManager.Location(){

        @Override
        public String getName() {
            return "ALL";
        }

        @Override
        public boolean isOutputLocation() {
            return false;
        }
    };
    private static final JavaFileManager.Location SOURCE_PATH_WRITE = new JavaFileManager.Location(){

        @Override
        public String getName() {
            return "SOURCE_PATH_WRITE";
        }

        @Override
        public boolean isOutputLocation() {
            return false;
        }
    };
    private final Map<JavaFileManager.Location, JavaFileManager[]> fileManagers;
    private final ProcessorGenerated processorGeneratedFiles;
    private final SiblingSource siblings;
    private final Object ownerThreadLock = new Object();
    private JavaFileObject lastInfered;
    private String lastInferedResult;
    private Thread ownerThread;

    public ProxyFileManager(@NonNull JavaFileManager bootPath, @NonNull JavaFileManager classPath, @NullAllowed JavaFileManager sourcePath, @NullAllowed JavaFileManager aptSources, @NullAllowed JavaFileManager outputhPath, @NullAllowed MemoryFileManager memoryFileManager, @NonNull ProcessorGenerated processorGeneratedFiles, @NonNull SiblingSource siblings) {
        JavaFileManager[] arrjavaFileManager;
        JavaFileManager[] arrjavaFileManager2;
        JavaFileManager[] arrjavaFileManager3;
        JavaFileManager[] arrjavaFileManager4;
        JavaFileManager[] arrjavaFileManager5;
        assert (bootPath != null);
        assert (classPath != null);
        assert (memoryFileManager == null || sourcePath != null);
        assert (processorGeneratedFiles != null);
        assert (siblings != null);
        this.fileManagers = new HashMap<JavaFileManager.Location, JavaFileManager[]>();
        if (outputhPath == null) {
            JavaFileManager[] arrjavaFileManager6 = new JavaFileManager[1];
            arrjavaFileManager5 = arrjavaFileManager6;
            arrjavaFileManager6[0] = classPath;
        } else {
            JavaFileManager[] arrjavaFileManager7 = new JavaFileManager[2];
            arrjavaFileManager7[0] = outputhPath;
            arrjavaFileManager5 = arrjavaFileManager7;
            arrjavaFileManager7[1] = classPath;
        }
        this.fileManagers.put(StandardLocation.CLASS_PATH, arrjavaFileManager5);
        this.fileManagers.put(StandardLocation.PLATFORM_CLASS_PATH, new JavaFileManager[]{bootPath});
        if (sourcePath == null) {
            arrjavaFileManager = new JavaFileManager[]{};
        } else if (memoryFileManager == null) {
            JavaFileManager[] arrjavaFileManager8 = new JavaFileManager[1];
            arrjavaFileManager = arrjavaFileManager8;
            arrjavaFileManager8[0] = sourcePath;
        } else {
            JavaFileManager[] arrjavaFileManager9 = new JavaFileManager[2];
            arrjavaFileManager9[0] = sourcePath;
            arrjavaFileManager = arrjavaFileManager9;
            arrjavaFileManager9[1] = memoryFileManager;
        }
        this.fileManagers.put(StandardLocation.SOURCE_PATH, arrjavaFileManager);
        if (outputhPath == null) {
            arrjavaFileManager2 = new JavaFileManager[]{};
        } else {
            JavaFileManager[] arrjavaFileManager10 = new JavaFileManager[1];
            arrjavaFileManager2 = arrjavaFileManager10;
            arrjavaFileManager10[0] = outputhPath;
        }
        this.fileManagers.put(StandardLocation.CLASS_OUTPUT, arrjavaFileManager2);
        if (aptSources == null) {
            arrjavaFileManager3 = new JavaFileManager[]{};
        } else {
            JavaFileManager[] arrjavaFileManager11 = new JavaFileManager[1];
            arrjavaFileManager3 = arrjavaFileManager11;
            arrjavaFileManager11[0] = aptSources;
        }
        this.fileManagers.put(StandardLocation.SOURCE_OUTPUT, arrjavaFileManager3);
        if (sourcePath == null) {
            arrjavaFileManager4 = new JavaFileManager[]{};
        } else {
            JavaFileManager[] arrjavaFileManager12 = new JavaFileManager[1];
            arrjavaFileManager4 = arrjavaFileManager12;
            arrjavaFileManager12[0] = sourcePath;
        }
        this.fileManagers.put(SOURCE_PATH_WRITE, arrjavaFileManager4);
        IdentityHashMap<JavaFileManager, Object> all = new IdentityHashMap<JavaFileManager, Object>();
        for (JavaFileManager[] jfmsForLoc : this.fileManagers.values()) {
            for (JavaFileManager jfm : jfmsForLoc) {
                all.put(jfm, null);
            }
        }
        this.fileManagers.put(ALL, all.keySet().toArray(new JavaFileManager[all.size()]));
        this.processorGeneratedFiles = processorGeneratedFiles;
        this.siblings = siblings;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @NonNull
    @Override
    public Iterable<JavaFileObject> list(@NonNull JavaFileManager.Location l, @NonNull String packageName, @NonNull Set<JavaFileObject.Kind> kinds, boolean recurse) throws IOException {
        this.checkSingleOwnerThread();
        try {
            JavaFileManager[] fms;
            LinkedList<Iterable<JavaFileObject>> iterables = new LinkedList<Iterable<JavaFileObject>>();
            for (JavaFileManager fm : fms = this.getFileManagers(l)) {
                iterables.add(fm.list(l, packageName, kinds, recurse));
            }
            Iterable<JavaFileObject> result = Iterators.chained(iterables);
            if (LOG.isLoggable(Level.FINER)) {
                StringBuilder urls = new StringBuilder();
                for (JavaFileObject jfo : result) {
                    urls.append(jfo.toUri().toString());
                    urls.append(", ");
                }
                LOG.log(Level.FINER, "List {0} Package: {1} Kinds: {2} -> {3}", new Object[]{l, packageName, kinds, urls});
            }
            Iterable<JavaFileObject> urls = result;
            return urls;
        }
        finally {
            this.clearOwnerThread();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @CheckForNull
    @Override
    public FileObject getFileForInput(@NonNull JavaFileManager.Location l, @NonNull String packageName, @NonNull String relativeName) throws IOException {
        this.checkSingleOwnerThread();
        try {
            JavaFileManager[] fms;
            for (JavaFileManager fm : fms = this.getFileManagers(l)) {
                FileObject result = fm.getFileForInput(l, packageName, relativeName);
                if (result == null) continue;
                FileObject fileObject = result;
                return fileObject;
            }
            JavaFileManager[] arr$ = null;
            return arr$;
        }
        finally {
            this.clearOwnerThread();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @CheckForNull
    @Override
    public FileObject getFileForOutput(@NonNull JavaFileManager.Location l, @NonNull String packageName, @NonNull String relativeName, @NullAllowed FileObject sibling) throws IOException, UnsupportedOperationException, IllegalArgumentException {
        this.checkSingleOwnerThread();
        try {
            JavaFileManager[] fms = this.getFileManagers(l == StandardLocation.SOURCE_PATH ? SOURCE_PATH_WRITE : l);
            assert (fms.length <= 1);
            if (fms.length == 0) {
                FileObject fileObject = null;
                return fileObject;
            }
            FileObject fileObject = this.mark(fms[0].getFileForOutput(l, packageName, relativeName, sibling), l);
            return fileObject;
        }
        finally {
            this.clearOwnerThread();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @CheckForNull
    @Override
    public ClassLoader getClassLoader(@NonNull JavaFileManager.Location l) {
        this.checkSingleOwnerThread();
        try {
            ClassLoader classLoader = null;
            return classLoader;
        }
        finally {
            this.clearOwnerThread();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void flush() throws IOException {
        this.checkSingleOwnerThread();
        try {
            for (JavaFileManager fm : this.getFileManagers(ALL)) {
                fm.flush();
            }
        }
        finally {
            this.clearOwnerThread();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void close() throws IOException {
        this.checkSingleOwnerThread();
        try {
            for (JavaFileManager fm : this.getFileManagers(ALL)) {
                fm.close();
            }
        }
        finally {
            this.clearOwnerThread();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int isSupportedOption(@NonNull String string) {
        this.checkSingleOwnerThread();
        try {
            int n = -1;
            return n;
        }
        finally {
            this.clearOwnerThread();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean handleOption(@NonNull String current, @NonNull Iterator<String> remains) {
        this.checkSingleOwnerThread();
        try {
            if ("apt-origin".equals(current)) {
                if (!remains.hasNext()) {
                    throw new IllegalArgumentException("The apt-source-root requires folder.");
                }
                String sib = remains.next();
                if (sib.length() != 0) {
                    URL sibling = ProxyFileManager.asURL(sib);
                    boolean inSourceRoot = this.processorGeneratedFiles.findSibling(Collections.singleton(sibling)) != null;
                    this.siblings.push(sibling, inSourceRoot);
                } else {
                    this.siblings.pop();
                }
                boolean sibling = true;
                return sibling;
            }
            boolean isSourceElement = "apt-source-element".equals(current);
            if (isSourceElement || "apt-resource-element".equals(current)) {
                if (remains.hasNext()) {
                    Collection<? extends URL> urls = ProxyFileManager.asURLs(remains);
                    URL sibling = this.processorGeneratedFiles.findSibling(urls);
                    boolean inSourceRoot = true;
                    if (sibling == null) {
                        sibling = this.siblings.getProvider().getSibling();
                        inSourceRoot = this.siblings.getProvider().isInSourceRoot();
                    }
                    this.siblings.push(sibling, inSourceRoot);
                    if (LOG.isLoggable(Level.INFO) && isSourceElement && urls.size() > 1) {
                        StringBuilder sb = new StringBuilder();
                        for (URL url : urls) {
                            if (sb.length() > 0) {
                                sb.append(", ");
                            }
                            sb.append(url);
                        }
                        LOG.log(Level.FINE, "Multiple source files passed as ORIGIN_SOURCE_ELEMENT_URL: {0}; using: {1}", new Object[]{sb, this.siblings.getProvider().getSibling()});
                    }
                } else {
                    this.siblings.pop();
                }
                boolean urls = true;
                return urls;
            }
            Collection<String> defensiveCopy = ProxyFileManager.copy(remains);
            for (JavaFileManager m : this.getFileManagers(ALL)) {
                if (!m.handleOption(current, defensiveCopy.iterator())) continue;
                boolean url = true;
                return url;
            }
            boolean arr$ = false;
            return arr$;
        }
        finally {
            this.clearOwnerThread();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean hasLocation(@NonNull JavaFileManager.Location location) {
        this.checkSingleOwnerThread();
        try {
            boolean bl = this.fileManagers.containsKey(location);
            return bl;
        }
        finally {
            this.clearOwnerThread();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @CheckForNull
    @Override
    public JavaFileObject getJavaFileForInput(@NonNull JavaFileManager.Location l, @NonNull String className, @NonNull JavaFileObject.Kind kind) throws IOException {
        this.checkSingleOwnerThread();
        try {
            JavaFileManager[] fms;
            for (JavaFileManager fm : fms = this.getFileManagers(l)) {
                JavaFileObject result = fm.getJavaFileForInput(l, className, kind);
                if (result == null) continue;
                JavaFileObject javaFileObject = result;
                return javaFileObject;
            }
            JavaFileManager[] arr$ = null;
            return arr$;
        }
        finally {
            this.clearOwnerThread();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @CheckForNull
    @Override
    public JavaFileObject getJavaFileForOutput(@NonNull JavaFileManager.Location l, @NonNull String className, @NonNull JavaFileObject.Kind kind, @NonNull FileObject sibling) throws IOException, UnsupportedOperationException, IllegalArgumentException {
        this.checkSingleOwnerThread();
        try {
            JavaFileManager[] fms = this.getFileManagers(l);
            assert (fms.length <= 1);
            if (fms.length == 0) {
                JavaFileObject javaFileObject = null;
                return javaFileObject;
            }
            JavaFileObject result = fms[0].getJavaFileForOutput(l, className, kind, sibling);
            JavaFileObject javaFileObject = this.mark(result, l);
            return javaFileObject;
        }
        finally {
            this.clearOwnerThread();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @CheckForNull
    @Override
    public String inferBinaryName(@NonNull JavaFileManager.Location location, @NonNull JavaFileObject javaFileObject) {
        this.checkSingleOwnerThread();
        try {
            JavaFileManager[] fms;
            String result;
            InferableJavaFileObject ifo;
            assert (javaFileObject != null);
            if (javaFileObject == this.lastInfered) {
                String string = this.lastInferedResult;
                return string;
            }
            if (javaFileObject instanceof InferableJavaFileObject && (result = (ifo = (InferableJavaFileObject)javaFileObject).inferBinaryName()) != null) {
                this.lastInfered = javaFileObject;
                this.lastInferedResult = result;
                String string = result;
                return string;
            }
            for (JavaFileManager fm : fms = this.getFileManagers(location)) {
                result = fm.inferBinaryName(location, javaFileObject);
                if (result == null || result.length() <= 0) continue;
                this.lastInfered = javaFileObject;
                this.lastInferedResult = result;
                String string = result;
                return string;
            }
            JavaFileManager[] arr$ = null;
            return arr$;
        }
        finally {
            this.clearOwnerThread();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean isSameFile(FileObject fileObject, FileObject fileObject0) {
        this.checkSingleOwnerThread();
        try {
            JavaFileManager[] fms;
            for (JavaFileManager fm : fms = this.getFileManagers(ALL)) {
                if (!fm.isSameFile(fileObject, fileObject0)) continue;
                boolean bl = true;
                return bl;
            }
            boolean arr$ = fileObject.toUri().equals(fileObject0.toUri());
            return arr$;
        }
        finally {
            this.clearOwnerThread();
        }
    }

    @CheckForNull
    private <T extends FileObject> T mark(@NullAllowed T result, @NonNull JavaFileManager.Location l) throws MalformedURLException {
        boolean valid = true;
        ProcessorGenerated.Type type = null;
        if (l == StandardLocation.CLASS_OUTPUT) {
            type = ProcessorGenerated.Type.RESOURCE;
        } else if (l == StandardLocation.SOURCE_OUTPUT) {
            type = ProcessorGenerated.Type.SOURCE;
        }
        if (result != null && this.siblings.getProvider().hasSibling() && this.siblings.getProvider().isInSourceRoot()) {
            if (type == ProcessorGenerated.Type.SOURCE) {
                this.processorGeneratedFiles.register(this.siblings.getProvider().getSibling(), (FileObject)result, type);
            } else if (type == ProcessorGenerated.Type.RESOURCE) {
                try {
                    result.openInputStream().close();
                }
                catch (IOException ioe) {
                    this.processorGeneratedFiles.register(this.siblings.getProvider().getSibling(), (FileObject)result, type);
                }
            }
            if (!FileObjects.isValidFileName(result)) {
                LOG.log(Level.WARNING, "Cannot write Annotation Processor generated file: {0} ({1})", new Object[]{result.getName(), result.toUri()});
                valid = false;
            }
        }
        return (T)(result == null ? null : (valid && (this.processorGeneratedFiles.canWrite() || !this.siblings.getProvider().hasSibling()) ? result : FileObjects.nullWriteFileObject((InferableJavaFileObject)result)));
    }

    private JavaFileManager[] getFileManagers(JavaFileManager.Location location) {
        JavaFileManager[] result = this.fileManagers.get(location);
        return result != null ? result : new JavaFileManager[]{};
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void checkSingleOwnerThread() {
        Thread currentThread = Thread.currentThread();
        Object object = this.ownerThreadLock;
        synchronized (object) {
            if (this.ownerThread == null) {
                this.ownerThread = currentThread;
            } else if (this.ownerThread != currentThread) {
                throw new ConcurrentModificationException(String.format("Current owner: %s, New Owner: %s", Arrays.asList(this.ownerThread.getStackTrace()), Arrays.asList(currentThread.getStackTrace())));
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void clearOwnerThread() {
        Object object = this.ownerThreadLock;
        synchronized (object) {
            this.ownerThread = null;
        }
    }

    private static URL asURL(String url) throws IllegalArgumentException {
        try {
            return new URL(url);
        }
        catch (MalformedURLException ex) {
            throw new IllegalArgumentException("Invalid path argument: " + url, ex);
        }
    }

    private static Collection<? extends URL> asURLs(Iterator<? extends String> surls) {
        ArrayDeque<URL> result = new ArrayDeque<URL>();
        while (surls.hasNext()) {
            String surl = surls.next();
            if (!"java".equals(FileObjects.getExtension(surl))) continue;
            result.add(ProxyFileManager.asURL(surl));
        }
        return result;
    }

    private static Collection<String> copy(Iterator<String> from) {
        if (!from.hasNext()) {
            return Collections.emptyList();
        }
        LinkedList<String> result = new LinkedList<String>();
        while (from.hasNext()) {
            result.add(from.next());
        }
        return result;
    }

}

