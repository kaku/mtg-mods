/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.source;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String GeneratedMethodBody() {
        return NbBundle.getMessage(Bundle.class, (String)"GeneratedMethodBody");
    }

    static String OverriddenMethodBody() {
        return NbBundle.getMessage(Bundle.class, (String)"OverriddenMethodBody");
    }

    private void Bundle() {
    }
}

