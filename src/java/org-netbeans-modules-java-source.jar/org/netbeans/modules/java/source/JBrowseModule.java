/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.ModuleInstall
 */
package org.netbeans.modules.java.source;

import org.netbeans.modules.java.source.usages.ClassIndexManager;
import org.openide.modules.ModuleInstall;

public class JBrowseModule
extends ModuleInstall {
    private static volatile boolean closed;

    public void close() {
        super.close();
        closed = true;
        ClassIndexManager.getDefault().close();
    }

    public static boolean isClosed() {
        return closed;
    }
}

