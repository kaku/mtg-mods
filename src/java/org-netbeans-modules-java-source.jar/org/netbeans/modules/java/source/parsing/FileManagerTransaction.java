/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.java.source.parsing;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.CompletionHandler;
import java.nio.charset.Charset;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.source.TreeLoader;
import org.netbeans.modules.java.source.indexing.JavaIndexerWorker;
import org.netbeans.modules.java.source.indexing.TransactionContext;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.PrefetchableJavaFileObject;
import org.netbeans.modules.java.source.parsing.WriteBackTransaction;
import org.openide.filesystems.FileSystem;
import org.openide.util.Parameters;

public abstract class FileManagerTransaction
extends TransactionContext.Service {
    private final boolean writeable;
    private Junction junction;

    protected FileManagerTransaction(boolean writeable) {
        this.writeable = writeable;
    }

    public final boolean canWrite() {
        return this.writeable;
    }

    public abstract void delete(@NonNull File var1);

    @NonNull
    abstract Iterable<JavaFileObject> filter(@NonNull JavaFileManager.Location var1, @NonNull String var2, @NonNull Iterable<JavaFileObject> var3);

    @NonNull
    abstract JavaFileObject createFileObject(@NonNull JavaFileManager.Location var1, @NonNull File var2, @NonNull File var3, @NullAllowed JavaFileFilterImplementation var4, @NullAllowed Charset var5);

    @CheckForNull
    JavaFileObject readFileObject(@NonNull JavaFileManager.Location location, @NonNull String dirName, @NonNull String relativeName) {
        return null;
    }

    @CheckForNull
    final CompletionHandler<Void, Void> getAsyncHandler() {
        return this.junction;
    }

    public static FileManagerTransaction writeBack(URL root) {
        return new WriteBackTransaction(root);
    }

    public static FileManagerTransaction writeThrough() {
        return new WriteThrogh();
    }

    public static FileManagerTransaction read() {
        return new Read();
    }

    public static FileManagerTransaction nullWrite() {
        return new Null();
    }

    public static FileManagerTransaction treeLoaderOnly() {
        return new TreeLoaderOnly();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static Future<Void> runConcurrent(@NonNull FileSystem.AtomicAction action) throws IOException {
        Future<Void> res;
        Parameters.notNull((CharSequence)"action", (Object)action);
        FileManagerTransaction fmtx = TransactionContext.get().get(FileManagerTransaction.class);
        if (fmtx == null) {
            throw new IllegalStateException("No FileManagerTransaction");
        }
        fmtx.fork();
        try {
            action.run();
        }
        finally {
            res = fmtx.join();
        }
        return res;
    }

    private void fork() {
        this.junction = new Junction();
    }

    @NonNull
    private Future<Void> join() {
        Junction result = this.junction;
        this.junction = null;
        assert (result != null);
        return result;
    }

    private static class Junction
    implements Runnable,
    CompletionHandler<Void, Void>,
    Future<Void> {
        private final Lock lck = new ReentrantLock();
        private final Condition cnd = this.lck.newCondition();
        private int running;

        Junction() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            this.lck.lock();
            try {
                ++this.running;
            }
            finally {
                this.lck.unlock();
            }
        }

        @Override
        public void completed(Void result, Void attachment) {
            this.done();
        }

        @Override
        public void failed(Throwable exc, Void attachment) {
            this.done();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void done() {
            this.lck.lock();
            try {
                --this.running;
                if (this.running == 0) {
                    this.cnd.signalAll();
                }
            }
            finally {
                this.lck.unlock();
            }
        }

        @Override
        public boolean cancel(boolean mayInterruptIfRunning) {
            return false;
        }

        @Override
        public boolean isCancelled() {
            return false;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public boolean isDone() {
            this.lck.lock();
            try {
                boolean bl = this.running == 0;
                return bl;
            }
            finally {
                this.lck.unlock();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public Void get() throws InterruptedException, ExecutionException {
            this.lck.lock();
            try {
                while (this.running > 0) {
                    this.cnd.await();
                }
            }
            finally {
                this.lck.unlock();
            }
            return null;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public Void get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            this.lck.lock();
            try {
                while (this.running > 0) {
                    if (this.cnd.await(timeout, unit)) continue;
                    throw new TimeoutException();
                }
            }
            finally {
                this.lck.unlock();
            }
            return null;
        }
    }

    private static final class TreeLoaderOnly
    extends FileManagerTransaction {
        private final FileManagerTransaction nullWrite = TreeLoaderOnly.nullWrite();
        private final FileManagerTransaction writeThrough = TreeLoaderOnly.writeThrough();

        TreeLoaderOnly() {
            super(true);
        }

        @Override
        public void delete(File file) {
            this.getDelegate().delete(file);
        }

        @Override
        Iterable<JavaFileObject> filter(JavaFileManager.Location location, String packageName, Iterable<JavaFileObject> files) {
            return this.getDelegate().filter(location, packageName, files);
        }

        @Override
        JavaFileObject createFileObject(JavaFileManager.Location location, File file, File root, JavaFileFilterImplementation filter, Charset encoding) {
            return this.getDelegate().createFileObject(location, file, root, filter, encoding);
        }

        @Override
        protected void commit() throws IOException {
            this.nullWrite.commit();
            this.writeThrough.commit();
        }

        @Override
        protected void rollBack() throws IOException {
            this.nullWrite.commit();
            this.writeThrough.commit();
        }

        @NonNull
        private FileManagerTransaction getDelegate() {
            return TreeLoader.isTreeLoading() ? this.writeThrough : this.nullWrite;
        }
    }

    private static class Read
    extends FileManagerTransaction {
        private Read() {
            super(false);
        }

        @Override
        public void delete(@NonNull File file) {
            throw new UnsupportedOperationException("Delete not supported, read-only.");
        }

        @NonNull
        @Override
        JavaFileObject createFileObject(@NonNull JavaFileManager.Location location, @NonNull File file, @NonNull File root, @NullAllowed JavaFileFilterImplementation filter, @NullAllowed Charset encoding) {
            throw new UnsupportedOperationException("Create File not supported, read-only.");
        }

        @NonNull
        @Override
        Iterable<JavaFileObject> filter(@NonNull JavaFileManager.Location location, @NonNull String packageName, @NonNull Iterable<JavaFileObject> files) {
            return files;
        }

        @Override
        protected void commit() throws IOException {
        }

        @Override
        protected void rollBack() throws IOException {
        }
    }

    private static class Null
    extends FileManagerTransaction {
        public Null() {
            super(true);
        }

        @Override
        public void delete(@NonNull File file) {
        }

        @NonNull
        @Override
        JavaFileObject createFileObject(@NonNull JavaFileManager.Location location, @NonNull File file, @NonNull File root, @NullAllowed JavaFileFilterImplementation filter, @NullAllowed Charset encoding) {
            PrefetchableJavaFileObject ifo = FileObjects.fileFileObject(file, root, filter, encoding);
            return FileObjects.nullWriteFileObject(ifo);
        }

        @NonNull
        @Override
        Iterable<JavaFileObject> filter(@NonNull JavaFileManager.Location location, @NonNull String packageName, @NonNull Iterable<JavaFileObject> files) {
            return files;
        }

        @Override
        protected void commit() throws IOException {
        }

        @Override
        protected void rollBack() throws IOException {
        }
    }

    private static class WriteThrogh
    extends FileManagerTransaction {
        private WriteThrogh() {
            super(true);
        }

        @Override
        public void delete(@NonNull File file) {
            assert (file != null);
            file.delete();
        }

        @NonNull
        @Override
        Iterable<JavaFileObject> filter(@NonNull JavaFileManager.Location location, @NonNull String packageName, @NonNull Iterable<JavaFileObject> files) {
            return files;
        }

        @Override
        protected void commit() throws IOException {
        }

        @Override
        protected void rollBack() throws IOException {
        }

        @NonNull
        @Override
        JavaFileObject createFileObject(@NonNull JavaFileManager.Location location, @NonNull File file, @NonNull File root, @NullAllowed JavaFileFilterImplementation filter, @NullAllowed Charset encoding) {
            CompletionHandler<Void, Void> handler = this.getAsyncHandler();
            return handler == null || !JavaIndexerWorker.supportsConcurrent() ? FileObjects.fileFileObject(file, root, filter, encoding) : FileObjects.asyncWriteFileObject(file, root, filter, encoding, JavaIndexerWorker.getExecutor(), handler);
        }
    }

}

