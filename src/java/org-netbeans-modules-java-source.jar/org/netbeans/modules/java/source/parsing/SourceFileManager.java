/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Parameters
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.source.parsing;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.modules.java.source.indexing.TransactionContext;
import org.netbeans.modules.java.source.parsing.AbstractSourceFileObject;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.InferableJavaFileObject;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Parameters;
import org.openide.util.Utilities;

public class SourceFileManager
implements JavaFileManager {
    final ClassPath sourceRoots;
    private final boolean ignoreExcludes;
    private static final Logger LOG = Logger.getLogger(SourceFileManager.class.getName());
    private static final ModifiedFiles modifiedFiles = new ModifiedFiles();

    public SourceFileManager(ClassPath sourceRoots, boolean ignoreExcludes) {
        this.sourceRoots = sourceRoots;
        this.ignoreExcludes = ignoreExcludes;
    }

    @Override
    public Iterable<JavaFileObject> list(JavaFileManager.Location l, String packageName, Set<JavaFileObject.Kind> kinds, boolean recursive) {
        ArrayList<JavaFileObject> result = new ArrayList<JavaFileObject>();
        String _name = packageName.replace('.', '/');
        if (_name.length() != 0) {
            _name = _name + '/';
        }
        for (ClassPath.Entry entry : this.sourceRoots.entries()) {
            FileObject root;
            FileObject tmpFile;
            if (!this.ignoreExcludes && !entry.includes(_name) || (root = entry.getRoot()) == null || (tmpFile = root.getFileObject(_name)) == null || !tmpFile.isFolder()) continue;
            Enumeration files = tmpFile.getChildren(recursive);
            while (files.hasMoreElements()) {
                JavaFileObject.Kind kind;
                FileObject file = (FileObject)files.nextElement();
                if (!this.ignoreExcludes && !entry.includes(file) || !kinds.contains((Object)(kind = FileObjects.getKind(file.getExt())))) continue;
                result.add(FileObjects.sourceFileObject(file, root));
            }
        }
        return result;
    }

    @Override
    public javax.tools.FileObject getFileForInput(JavaFileManager.Location l, String pkgName, String relativeName) {
        String rp = FileObjects.getRelativePath(pkgName, relativeName);
        FileObject[] fileRootPair = this.findFile(rp);
        return fileRootPair == null ? null : FileObjects.sourceFileObject(fileRootPair[0], fileRootPair[1]);
    }

    @Override
    public JavaFileObject getJavaFileForInput(JavaFileManager.Location l, String className, JavaFileObject.Kind kind) {
        String[] namePair = FileObjects.getParentRelativePathAndName(className);
        if (namePair == null) {
            return null;
        }
        String ext = kind == JavaFileObject.Kind.CLASS ? "sig" : kind.extension.substring(1);
        for (ClassPath.Entry entry : this.sourceRoots.entries()) {
            FileObject parent;
            FileObject[] children;
            FileObject root = entry.getRoot();
            if (root == null || (parent = root.getFileObject(namePair[0])) == null) continue;
            for (FileObject child : children = parent.getChildren()) {
                if (!namePair[1].equals(child.getName()) || !ext.equalsIgnoreCase(child.getExt()) || !this.ignoreExcludes && !entry.includes(child)) continue;
                return FileObjects.sourceFileObject(child, root);
            }
        }
        return null;
    }

    @Override
    public javax.tools.FileObject getFileForOutput(JavaFileManager.Location l, String pkgName, String relativeName, javax.tools.FileObject sibling) throws IOException, UnsupportedOperationException, IllegalArgumentException {
        if (StandardLocation.SOURCE_PATH != l) {
            throw new UnsupportedOperationException("Only StandardLocation.SOURCE_PATH is supported.");
        }
        String rp = FileObjects.getRelativePath(pkgName, relativeName);
        FileObject[] fileRootPair = this.findFile(rp);
        if (fileRootPair == null) {
            FileObject[] roots = this.sourceRoots.getRoots();
            if (roots.length == 0) {
                return null;
            }
            File rootFile = FileUtil.toFile((FileObject)roots[0]);
            if (rootFile == null) {
                return null;
            }
            return FileObjects.sourceFileObject(Utilities.toURI((File)new File(rootFile, FileObjects.convertFolder2Package(rp, File.separatorChar))).toURL(), roots[0]);
        }
        return FileObjects.sourceFileObject(fileRootPair[0], fileRootPair[1]);
    }

    @Override
    public JavaFileObject getJavaFileForOutput(JavaFileManager.Location l, String className, JavaFileObject.Kind kind, javax.tools.FileObject sibling) throws IOException, UnsupportedOperationException, IllegalArgumentException {
        throw new UnsupportedOperationException("The SourceFileManager does not support write operations.");
    }

    @Override
    public void flush() throws IOException {
    }

    @Override
    public void close() throws IOException {
    }

    @Override
    public int isSupportedOption(String string) {
        return -1;
    }

    @Override
    public boolean handleOption(String head, Iterator<String> tail) {
        return false;
    }

    @Override
    public boolean hasLocation(JavaFileManager.Location location) {
        return true;
    }

    @Override
    public ClassLoader getClassLoader(JavaFileManager.Location l) {
        return null;
    }

    @Override
    public String inferBinaryName(JavaFileManager.Location l, JavaFileObject jfo) {
        block7 : {
            try {
                String result;
                if (jfo instanceof InferableJavaFileObject && (result = ((InferableJavaFileObject)jfo).inferBinaryName()) != null) {
                    return result;
                }
                FileObject fo = URLMapper.findFileObject((URL)jfo.toUri().toURL());
                FileObject root = null;
                if (root == null) {
                    for (FileObject rc : this.sourceRoots.getRoots()) {
                        if (!FileUtil.isParentOf((FileObject)rc, (FileObject)fo)) continue;
                        root = rc;
                    }
                }
                if (root != null) {
                    String relativePath = FileUtil.getRelativePath((FileObject)root, (FileObject)fo);
                    int index = relativePath.lastIndexOf(46);
                    assert (index > 0);
                    String result2 = relativePath.substring(0, index).replace('/', '.');
                    return result2;
                }
            }
            catch (MalformedURLException e) {
                if (!LOG.isLoggable(Level.SEVERE)) break block7;
                LOG.log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return null;
    }

    @Override
    public boolean isSameFile(javax.tools.FileObject a, javax.tools.FileObject b) {
        return a instanceof AbstractSourceFileObject && b instanceof AbstractSourceFileObject && ((AbstractSourceFileObject)a).getHandle().file != null && ((AbstractSourceFileObject)a).getHandle().file.equals((Object)((AbstractSourceFileObject)b).getHandle().file);
    }

    private FileObject[] findFile(String relativePath) {
        for (ClassPath.Entry entry : this.sourceRoots.entries()) {
            FileObject root;
            FileObject file;
            if (!this.ignoreExcludes && !entry.includes(relativePath) || (root = entry.getRoot()) == null || (file = root.getFileObject(relativePath)) == null) continue;
            return new FileObject[]{file, root};
        }
        return null;
    }

    public static ModifiedFiles getModifiedFiles() {
        return modifiedFiles;
    }

    public static ModifiedFilesTransaction newModifiedFilesTransaction(boolean srcIndex, boolean checkForEditorModifications) {
        ModifiedFilesTransaction tx = srcIndex && !checkForEditorModifications ? new PermanentSourceScan(modifiedFiles) : new TransientSourceScan();
        tx.begin();
        return tx;
    }

    private static final class TransientSourceScan
    extends ModifiedFilesTransaction {
        private TransientSourceScan() {
        }

        @Override
        public void cacheUpdated(URI file) {
        }

        @Override
        void begin() {
        }

        @Override
        protected void commit() throws IOException {
        }

        @Override
        protected void rollBack() throws IOException {
        }
    }

    private static final class PermanentSourceScan
    extends ModifiedFilesTransaction {
        private final ModifiedFiles delegate;

        private PermanentSourceScan(@NonNull ModifiedFiles delegate) {
            Parameters.notNull((CharSequence)"delegate", (Object)delegate);
            this.delegate = delegate;
        }

        @Override
        public void cacheUpdated(@NonNull URI file) {
            this.delegate.cacheUpdated(file);
        }

        @Override
        void begin() {
            this.delegate.beginUpdate();
        }

        @Override
        protected void commit() throws IOException {
            this.delegate.commitUpdate();
        }

        @Override
        protected void rollBack() throws IOException {
            this.delegate.rollBackUpdate();
        }
    }

    public static abstract class ModifiedFilesTransaction
    extends TransactionContext.Service {
        public abstract void cacheUpdated(@NonNull URI var1);

        abstract void begin();
    }

    public static final class ModifiedFiles {
        private final Object lock = new Object();
        private final Set<URI> files = new HashSet<URI>();
        private Set<URI> addedFiles;
        private Set<URI> removedFiles;

        private ModifiedFiles() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void beginUpdate() {
            Object object = this.lock;
            synchronized (object) {
                LOG.fine("beginUpdate");
                assert (this.addedFiles == null);
                assert (this.removedFiles == null);
                this.addedFiles = new HashSet<URI>();
                this.removedFiles = new HashSet<URI>();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void commitUpdate() {
            Object object = this.lock;
            synchronized (object) {
                LOG.fine("commitUpdate");
                assert (this.addedFiles != null);
                assert (this.removedFiles != null);
                this.files.removeAll(this.removedFiles);
                this.files.addAll(this.addedFiles);
                this.addedFiles = null;
                this.removedFiles = null;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void rollBackUpdate() throws IOException {
            Object object = this.lock;
            synchronized (object) {
                LOG.fine("rollBackUpdate");
                this.addedFiles = null;
                this.removedFiles = null;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void cacheUpdated(@NonNull URI file) {
            Parameters.notNull((CharSequence)"file", (Object)file);
            Object object = this.lock;
            synchronized (object) {
                LOG.log(Level.FINE, "cacheUpdated: {0}", file);
                assert (this.removedFiles != null);
                this.removedFiles.add(file);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void fileModified(@NonNull URI file) {
            Parameters.notNull((CharSequence)"file", (Object)file);
            Object object = this.lock;
            synchronized (object) {
                LOG.log(Level.FINE, "fileModified: {0}", file);
                Set<URI> addInto = this.addedFiles != null ? this.addedFiles : this.files;
                addInto.add(file);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public boolean isModified(@NonNull URI file) {
            Parameters.notNull((CharSequence)"file", (Object)file);
            Object object = this.lock;
            synchronized (object) {
                boolean res = this.files.contains(file) || this.addedFiles != null && this.addedFiles.contains(file);
                LOG.log(Level.FINE, "isModified: {0} -> {1}", new Object[]{file, res});
                return res;
            }
        }
    }

}

