/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.tools.javac.api.ClientCodeWrapper
 *  com.sun.tools.javac.api.ClientCodeWrapper$Trusted
 *  com.sun.tools.javac.api.DiagnosticFormatter
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.JCDiagnostic
 *  com.sun.tools.javac.util.Log
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.Pair
 */
package org.netbeans.modules.java.source.parsing;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.MethodTree;
import com.sun.tools.javac.api.ClientCodeWrapper;
import com.sun.tools.javac.api.DiagnosticFormatter;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.JCDiagnostic;
import com.sun.tools.javac.util.Log;
import java.io.IOException;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.Document;
import javax.swing.text.StyledDocument;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticListener;
import javax.tools.JavaFileObject;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.modules.java.source.JavaFileFilterQuery;
import org.netbeans.modules.java.source.parsing.AbstractSourceFileObject;
import org.netbeans.modules.java.source.parsing.DocPositionRegion;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.JavacParser;
import org.netbeans.modules.parsing.api.Snapshot;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Pair;

public final class CompilationInfoImpl {
    private JavaSource.Phase phase = JavaSource.Phase.MODIFIED;
    private CompilationUnitTree compilationUnit;
    private JavacTaskImpl javacTask;
    private DiagnosticListener<JavaFileObject> diagnosticListener;
    private final ClasspathInfo cpInfo;
    private Pair<DocPositionRegion, MethodTree> changedMethod;
    private final FileObject file;
    private final FileObject root;
    final AbstractSourceFileObject jfo;
    private Snapshot snapshot;
    private final JavacParser parser;
    private final boolean isClassFile;
    private final boolean isDetached;
    JavaSource.Phase parserCrashed = JavaSource.Phase.UP_TO_DATE;
    private final Map<CompilationInfo.CacheClearPolicy, Map<Object, Object>> userCache = new EnumMap<CompilationInfo.CacheClearPolicy, Map<Object, Object>>(CompilationInfo.CacheClearPolicy.class);

    CompilationInfoImpl(JavacParser parser, FileObject file, FileObject root, JavacTaskImpl javacTask, DiagnosticListener<JavaFileObject> diagnosticListener, Snapshot snapshot, boolean detached) throws IOException {
        assert (parser != null);
        this.parser = parser;
        this.cpInfo = parser.getClasspathInfo();
        assert (this.cpInfo != null);
        this.file = file;
        this.root = root;
        this.snapshot = snapshot;
        assert (file == null || snapshot != null);
        this.jfo = file != null ? FileObjects.sourceFileObject(file, root, JavaFileFilterQuery.getFilter(file), snapshot.getText()) : null;
        this.javacTask = javacTask;
        this.diagnosticListener = diagnosticListener;
        this.isClassFile = false;
        this.isDetached = detached;
    }

    CompilationInfoImpl(ClasspathInfo cpInfo) {
        assert (cpInfo != null);
        this.parser = null;
        this.file = null;
        this.root = null;
        this.jfo = null;
        this.snapshot = null;
        this.cpInfo = cpInfo;
        this.isClassFile = false;
        this.isDetached = false;
    }

    CompilationInfoImpl(ClasspathInfo cpInfo, FileObject file, FileObject root) throws IOException {
        assert (cpInfo != null);
        assert (file != null);
        assert (root != null);
        this.parser = null;
        this.file = file;
        this.root = root;
        this.jfo = FileObjects.sourceFileObject(file, root);
        this.snapshot = null;
        this.cpInfo = cpInfo;
        this.isClassFile = true;
        this.isDetached = false;
    }

    void update(Snapshot snapshot) throws IOException {
        assert (snapshot != null);
        this.jfo.update(snapshot.getText());
        this.snapshot = snapshot;
    }

    public Snapshot getSnapshot() {
        return this.snapshot;
    }

    public JavaSource.Phase getPhase() {
        return this.phase;
    }

    public Pair<DocPositionRegion, MethodTree> getChangedTree() {
        return this.changedMethod;
    }

    public CompilationUnitTree getCompilationUnit() {
        if (this.jfo == null) {
            throw new IllegalStateException();
        }
        if (this.phase.compareTo(JavaSource.Phase.PARSED) < 0) {
            throw new IllegalStateException("Cannot call getCompilationUnit() if current phase < JavaSource.Phase.PARSED. You must call toPhase(Phase.PARSED) first.");
        }
        return this.compilationUnit;
    }

    public String getText() {
        if (!this.hasSource()) {
            throw new IllegalStateException();
        }
        try {
            return this.jfo.getCharContent(false).toString();
        }
        catch (IOException ioe) {
            Exceptions.printStackTrace((Throwable)ioe);
            return null;
        }
    }

    public TokenHierarchy<?> getTokenHierarchy() {
        if (!this.hasSource()) {
            throw new IllegalStateException();
        }
        try {
            return this.jfo.getTokenHierarchy();
        }
        catch (IOException ioe) {
            Exceptions.printStackTrace((Throwable)ioe);
            return null;
        }
    }

    public List<Diagnostic> getDiagnostics() {
        if (this.jfo == null) {
            throw new IllegalStateException();
        }
        Collection errors = ((DiagnosticListenerImpl)this.diagnosticListener).getErrors(this.jfo).values();
        List partialReparseErrors = ((DiagnosticListenerImpl)this.diagnosticListener).partialReparseErrors;
        List affectedErrors = ((DiagnosticListenerImpl)this.diagnosticListener).affectedErrors;
        int errorsSize = 0;
        for (Collection err : errors) {
            errorsSize += err.size();
        }
        ArrayList<Diagnostic> localErrors = new ArrayList<Diagnostic>(errorsSize + (partialReparseErrors == null ? 0 : partialReparseErrors.size()) + (affectedErrors == null ? 0 : affectedErrors.size()));
        DiagnosticFormatter formatter = Log.instance((Context)this.javacTask.getContext()).getDiagnosticFormatter();
        for (Collection err2 : errors) {
            for (Diagnostic d : err2) {
                localErrors.add(RichDiagnostic.wrap(d, formatter));
            }
        }
        if (partialReparseErrors != null) {
            for (Diagnostic d : partialReparseErrors) {
                localErrors.add(RichDiagnostic.wrap(d, formatter));
            }
        }
        if (affectedErrors != null) {
            for (Diagnostic d : affectedErrors) {
                localErrors.add(RichDiagnostic.wrap(d, formatter));
            }
        }
        return localErrors;
    }

    public ClasspathInfo getClasspathInfo() {
        return this.cpInfo;
    }

    public JavacParser getParser() {
        return this.parser;
    }

    public FileObject getFileObject() {
        return this.file;
    }

    public FileObject getRoot() {
        return this.root;
    }

    public boolean isClassFile() {
        return this.isClassFile;
    }

    public Document getDocument() {
        if (this.file == null) {
            return null;
        }
        if (!this.file.isValid()) {
            return null;
        }
        try {
            DataObject od = DataObject.find((FileObject)this.file);
            EditorCookie ec = (EditorCookie)od.getCookie(EditorCookie.class);
            if (ec != null) {
                return ec.getDocument();
            }
            return null;
        }
        catch (DataObjectNotFoundException e) {
            Logger.getLogger(CompilationInfoImpl.class.getName()).log(Level.FINE, null, (Throwable)e);
            return null;
        }
    }

    public JavaSource.Phase toPhase(JavaSource.Phase phase) throws IOException {
        if (phase == JavaSource.Phase.MODIFIED) {
            throw new IllegalArgumentException("Invalid phase: " + (Object)((Object)phase));
        }
        if (!this.hasSource()) {
            JavaSource.Phase currentPhase = this.getPhase();
            if (currentPhase.compareTo(phase) < 0) {
                this.setPhase(phase);
                if (currentPhase == JavaSource.Phase.MODIFIED) {
                    this.getJavacTask().parse();
                }
                currentPhase = phase;
            }
            return currentPhase;
        }
        JavaSource.Phase currentPhase = this.parser.moveToPhase(phase, this, false);
        return currentPhase.compareTo(phase) < 0 ? currentPhase : phase;
    }

    public synchronized JavacTaskImpl getJavacTask() {
        if (this.javacTask == null) {
            this.diagnosticListener = new DiagnosticListenerImpl(this.jfo);
            this.javacTask = JavacParser.createJavacTask(this.file, this.root, this.cpInfo, this.parser, this.diagnosticListener, null, this.isDetached);
        }
        return this.javacTask;
    }

    public Object getCachedValue(Object key) {
        for (Map<Object, Object> c : this.userCache.values()) {
            Object res = c.get(key);
            if (res == null) continue;
            return res;
        }
        return null;
    }

    public void putCachedValue(Object key, Object value, CompilationInfo.CacheClearPolicy clearPolicy) {
        for (Map<Object, Object> c : this.userCache.values()) {
            c.remove(key);
        }
        Map<Object, Object> c2 = this.userCache.get((Object)clearPolicy);
        if (c2 == null) {
            c2 = new HashMap<Object, Object>();
            this.userCache.put(clearPolicy, c2);
        }
        c2.put(key, value);
    }

    public void taskFinished() {
        this.userCache.remove((Object)CompilationInfo.CacheClearPolicy.ON_TASK_END);
    }

    public void dispose() {
        this.userCache.clear();
    }

    DiagnosticListener<JavaFileObject> getDiagnosticListener() {
        return this.diagnosticListener;
    }

    void setPhase(JavaSource.Phase phase) {
        assert (phase != null);
        this.phase = phase;
    }

    void setChangedMethod(Pair<DocPositionRegion, MethodTree> changedMethod) {
        this.changedMethod = changedMethod;
        this.userCache.remove((Object)CompilationInfo.CacheClearPolicy.ON_TASK_END);
        this.userCache.remove((Object)CompilationInfo.CacheClearPolicy.ON_CHANGE);
    }

    void setCompilationUnit(CompilationUnitTree compilationUnit) {
        assert (compilationUnit != null);
        this.compilationUnit = compilationUnit;
    }

    private boolean hasSource() {
        return this.jfo != null && !this.isClassFile;
    }

    static final class RichDiagnostic
    implements Diagnostic {
        private final JCDiagnostic delegate;
        private final DiagnosticFormatter<JCDiagnostic> formatter;

        public RichDiagnostic(JCDiagnostic delegate, DiagnosticFormatter<JCDiagnostic> formatter) {
            this.delegate = delegate;
            this.formatter = formatter;
        }

        @Override
        public Diagnostic.Kind getKind() {
            return this.delegate.getKind();
        }

        public Object getSource() {
            return this.delegate.getSource();
        }

        @Override
        public long getPosition() {
            return this.delegate.getPosition();
        }

        @Override
        public long getStartPosition() {
            return this.delegate.getStartPosition();
        }

        @Override
        public long getEndPosition() {
            return this.delegate.getEndPosition();
        }

        @Override
        public long getLineNumber() {
            return this.delegate.getLineNumber();
        }

        @Override
        public long getColumnNumber() {
            return this.delegate.getColumnNumber();
        }

        @Override
        public String getCode() {
            return this.delegate.getCode();
        }

        @Override
        public String getMessage(Locale locale) {
            return this.formatter.format((Diagnostic)this.delegate, locale);
        }

        public String toString() {
            return this.delegate.toString();
        }

        JCDiagnostic getDelegate() {
            return this.delegate;
        }

        public static Diagnostic wrap(Diagnostic d, DiagnosticFormatter<JCDiagnostic> df) {
            if (d instanceof JCDiagnostic) {
                return new RichDiagnostic((JCDiagnostic)d, df);
            }
            return d;
        }
    }

    @ClientCodeWrapper.Trusted
    static class DiagnosticListenerImpl
    implements DiagnosticListener<JavaFileObject> {
        private final Map<JavaFileObject, TreeMap<Integer, Collection<Diagnostic<? extends JavaFileObject>>>> source2Errors;
        private final JavaFileObject jfo;
        private volatile List<Diagnostic<? extends JavaFileObject>> partialReparseErrors;
        private volatile boolean partialReparseRealErrors;
        private volatile List<Diagnostic<? extends JavaFileObject>> affectedErrors;
        private volatile int currentDelta;

        public DiagnosticListenerImpl(JavaFileObject jfo) {
            this.jfo = jfo;
            this.source2Errors = new HashMap<JavaFileObject, TreeMap<Integer, Collection<Diagnostic<? extends JavaFileObject>>>>();
        }

        @Override
        public void report(Diagnostic<? extends JavaFileObject> message) {
            if (this.partialReparseErrors != null) {
                if (this.jfo != null && this.jfo == message.getSource()) {
                    this.partialReparseErrors.add(message);
                    if (message.getKind() == Diagnostic.Kind.ERROR) {
                        this.partialReparseRealErrors = true;
                    }
                }
            } else {
                TreeMap<Integer, Collection<Diagnostic<? extends JavaFileObject>>> errors = this.getErrors(message.getSource());
                Collection<Diagnostic<? extends JavaFileObject>> diags = errors.get((int)message.getPosition());
                if (diags == null) {
                    diags = new ArrayList<Diagnostic<? extends JavaFileObject>>();
                    errors.put((int)message.getPosition(), diags);
                }
                diags.add(message);
            }
        }

        private TreeMap<Integer, Collection<Diagnostic<? extends JavaFileObject>>> getErrors(JavaFileObject file) {
            TreeMap errors = this.source2Errors.get(file);
            if (errors == null) {
                errors = new TreeMap();
                this.source2Errors.put(file, errors);
            }
            return errors;
        }

        final boolean hasPartialReparseErrors() {
            return this.partialReparseErrors != null && this.partialReparseRealErrors;
        }

        final void startPartialReparse(int from, int to) {
            if (this.partialReparseErrors == null) {
                this.partialReparseErrors = new ArrayList<Diagnostic<? extends JavaFileObject>>();
                TreeMap<Integer, Collection<Diagnostic<? extends JavaFileObject>>> errors = this.getErrors(this.jfo);
                errors.subMap(from, to).clear();
                SortedMap<Integer, Collection<Diagnostic<? extends JavaFileObject>>> tail = errors.tailMap(to);
                this.affectedErrors = new ArrayList<Diagnostic<? extends JavaFileObject>>(tail.size());
                Iterator<Map.Entry<Integer, Collection<Diagnostic<? extends JavaFileObject>>>> it = tail.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry<Integer, Collection<Diagnostic<? extends JavaFileObject>>> e = it.next();
                    for (Diagnostic<? extends JavaFileObject> d : e.getValue()) {
                        JCDiagnostic diagnostic = (JCDiagnostic)d;
                        if (diagnostic == null) {
                            throw new IllegalStateException("#184910: diagnostic == null " + DiagnosticListenerImpl.mapArraysToLists(Thread.getAllStackTraces()));
                        }
                        this.affectedErrors.add(new D(diagnostic));
                    }
                    it.remove();
                }
            } else {
                this.partialReparseErrors.clear();
            }
            this.partialReparseRealErrors = false;
        }

        final void endPartialReparse(int delta) {
            this.currentDelta += delta;
        }

        private static <A, B> Map<A, List<B>> mapArraysToLists(Map<? extends A, B[]> map) {
            HashMap<A, List<B>> result = new HashMap<A, List<B>>();
            for (Map.Entry<A, B[]> entry : map.entrySet()) {
                result.put(entry.getKey(), Arrays.asList(entry.getValue()));
            }
            return result;
        }

        private final class D
        implements Diagnostic {
            private final JCDiagnostic delegate;

            public D(JCDiagnostic delegate) {
                assert (delegate != null);
                this.delegate = delegate;
            }

            @Override
            public Diagnostic.Kind getKind() {
                return this.delegate.getKind();
            }

            public Object getSource() {
                return this.delegate.getSource();
            }

            @Override
            public long getPosition() {
                long ret = this.delegate.getPosition();
                if (this.delegate.hasFixedPositions()) {
                    ret += (long)DiagnosticListenerImpl.this.currentDelta;
                }
                return ret;
            }

            @Override
            public long getStartPosition() {
                long ret = this.delegate.getStartPosition();
                if (this.delegate.hasFixedPositions()) {
                    ret += (long)DiagnosticListenerImpl.this.currentDelta;
                }
                return ret;
            }

            @Override
            public long getEndPosition() {
                long ret = this.delegate.getEndPosition();
                if (this.delegate.hasFixedPositions()) {
                    ret += (long)DiagnosticListenerImpl.this.currentDelta;
                }
                return ret;
            }

            @Override
            public long getLineNumber() {
                return -1;
            }

            @Override
            public long getColumnNumber() {
                return -1;
            }

            @Override
            public String getCode() {
                return this.delegate.getCode();
            }

            @Override
            public String getMessage(Locale locale) {
                return this.delegate.getMessage(locale);
            }
        }

    }

}

