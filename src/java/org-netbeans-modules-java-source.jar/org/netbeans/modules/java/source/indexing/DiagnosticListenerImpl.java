/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.source.indexing;

import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticListener;
import javax.tools.JavaFileObject;

class DiagnosticListenerImpl
implements DiagnosticListener<JavaFileObject> {
    private static final Logger ERROR_LOG = Logger.getLogger(DiagnosticListenerImpl.class.getName() + "-errors");
    private final Map<URI, List<Diagnostic<? extends JavaFileObject>>> diagnostics = new HashMap<URI, List<Diagnostic<? extends JavaFileObject>>>();

    DiagnosticListenerImpl() {
    }

    @Override
    public void report(Diagnostic<? extends JavaFileObject> d) {
        assert (DiagnosticListenerImpl.logDiagnostic(d));
        JavaFileObject source = d.getSource();
        if (source != null) {
            List<Diagnostic<? extends JavaFileObject>> current = this.diagnostics.get(source.toUri());
            if (current == null) {
                current = new LinkedList<Diagnostic<? extends JavaFileObject>>();
                this.diagnostics.put(source.toUri(), current);
            }
            current.add(d);
        }
    }

    public List<Diagnostic<? extends JavaFileObject>> getDiagnostics(JavaFileObject file) {
        List result = this.diagnostics.remove(file.toUri());
        if (result == null) {
            result = Collections.emptyList();
        }
        return result;
    }

    public void cleanDiagnostics() {
        this.diagnostics.clear();
    }

    private static boolean logDiagnostic(Diagnostic<? extends JavaFileObject> d) {
        Level logLevel = DiagnosticListenerImpl.findLogLevel(d);
        if (ERROR_LOG.isLoggable(logLevel)) {
            ERROR_LOG.log(logLevel, d.getSource().toUri().toASCIIString() + ":" + d.getCode() + ":" + d.getLineNumber() + ":" + d.getMessage(null), new Exception());
        }
        return true;
    }

    private static Level findLogLevel(Diagnostic<? extends JavaFileObject> d) {
        if (d.getKind() == Diagnostic.Kind.ERROR) {
            return Level.FINE;
        }
        return Level.FINER;
    }
}

