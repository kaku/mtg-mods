/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.source.pretty;

import java.io.IOException;
import java.io.Writer;

public final class CharBuffer {
    char[] chars = new char[10];
    int used;
    int col;
    int maxcol;
    static final int UNLIMITED = 999999;
    int rightMargin = 72;
    int tabSize = 8;
    boolean expandTabs = true;
    int leftMargin = 0;
    int hardRightMargin = 999999;
    int lastBlankLines = 0;
    int lastObserved;
    private static RuntimeException err = new IndexOutOfBoundsException();

    public final int length() {
        return this.used;
    }

    public void setLength(int l) {
        if (l < this.used) {
            this.used = l < 0 ? 0 : l;
        }
    }

    public CharBuffer() {
    }

    public CharBuffer(int rm, int ts, boolean et) {
        this();
        this.rightMargin = rm;
        this.tabSize = ts;
        this.expandTabs = et;
    }

    public final boolean hasMargin() {
        return this.hardRightMargin != 999999;
    }

    public int harden() {
        int ret = this.hardRightMargin;
        this.hardRightMargin = this.rightMargin;
        return ret;
    }

    public void restore(int n) {
        this.hardRightMargin = n;
    }

    private final void needRoom(int n) {
        if (this.chars.length <= this.used + n) {
            char[] nc = new char[(this.used + n) * 2];
            System.arraycopy(this.chars, 0, nc, 0, this.used);
            this.chars = nc;
        }
    }

    private void columnOverflowCheck() {
        if ((this.col > this.hardRightMargin || this.maxcol > this.hardRightMargin) && this.hardRightMargin != 999999) {
            throw err;
        }
    }

    void toCol(int n) {
        if (!this.expandTabs) {
            while (this.col + this.tabSize - this.col % this.tabSize <= n) {
                this.append('\t');
            }
        }
        while (this.col < n) {
            this.append(' ');
        }
    }

    void toLeftMargin() {
        this.toCol(this.leftMargin);
    }

    void toColExactly(int n) {
        if (n < 0) {
            n = 0;
        }
        if (n < this.col) {
            this.nlTerm();
        }
        this.toCol(n);
    }

    void notRightOf(int n) {
        if (n > this.col) {
            this.needSpace();
        } else {
            this.toColExactly(n);
        }
    }

    void ctlChar() {
        switch (this.chars[this.used - 1]) {
            case '\n': {
                if (this.hardRightMargin != 999999) {
                    throw err;
                }
                if (this.col > this.maxcol) {
                    this.maxcol = this.col;
                }
                this.col = 0;
                break;
            }
            case '\t': {
                this.col += this.tabSize - this.col % this.tabSize;
                break;
            }
            case '\b': {
                if (this.col > this.maxcol) {
                    this.maxcol = this.col;
                }
                --this.col;
            }
        }
        this.columnOverflowCheck();
    }

    private void append0(char c) {
        this.chars[this.used++] = c;
        if (c > ' ') {
            this.lastBlankLines = 0;
        }
        if (c < ' ') {
            this.ctlChar();
        } else if (++this.col > this.hardRightMargin) {
            this.columnOverflowCheck();
        }
    }

    public final void append(char c) {
        this.needRoom(1);
        this.append0(c);
    }

    public final void append(char[] b) {
        if (b != null) {
            this.append(b, 0, b.length);
        }
    }

    public final void append(char[] b, int off, int len) {
        if (b != null) {
            this.needRoom(len);
            while (--len >= 0) {
                this.append0(b[off++]);
            }
        }
    }

    public void append(String s) {
        int len = s.length();
        this.needRoom(len);
        for (int i = 0; i < len; ++i) {
            this.append0(s.charAt(i));
        }
    }

    public void append(CharBuffer cb) {
        this.append(cb.chars, 0, cb.used);
    }

    public void appendUtf8(byte[] src, int i, int len) {
        int limit = i + len;
        while (i < limit) {
            int b;
            if ((b = src[i++] & 255) >= 224) {
                b = (b & 15) << 12;
                b |= (src[i++] & 63) << 6;
                b |= src[i++] & 63;
            } else if (b >= 192) {
                b = (b & 31) << 6;
                b |= src[i++] & 63;
            }
            this.append((char)b);
        }
    }

    public char[] toCharArray() {
        char[] nm = new char[this.used];
        System.arraycopy(this.chars, 0, nm, 0, this.used);
        return nm;
    }

    public void copyClear(CharBuffer cb) {
        char[] t = this.chars;
        this.chars = cb.chars;
        this.used = cb.used;
        cb.chars = t;
        cb.used = 0;
    }

    public void appendClear(CharBuffer cb) {
        if (this.used == 0) {
            this.copyClear(cb);
        } else {
            this.append(cb);
            cb.used = 0;
        }
    }

    public void clear() {
        this.used = 0;
        this.col = 0;
        this.maxcol = 0;
    }

    public int width() {
        return this.col > this.maxcol ? this.col : this.maxcol;
    }

    public String toString() {
        return new String(this.chars, 0, this.used);
    }

    public void writeTo(Writer w) throws IOException {
        w.write(this.chars, 0, this.used);
    }

    public String substring(int off, int end) {
        return new String(this.chars, off, end - off);
    }

    public void to(Writer w) throws IOException {
        this.to(w, 0, this.used);
    }

    public void to(Writer w, int st, int len) throws IOException {
        w.write(this.chars, st, len);
    }

    public boolean equals(Object o) {
        if (o instanceof String) {
            String s = (String)o;
            if (s.length() != this.used) {
                return false;
            }
            int i = this.used;
            while (--i >= 0) {
                if (this.chars[i] == s.charAt(i)) continue;
                return false;
            }
            return true;
        }
        return o == this;
    }

    public boolean equals(char[] o) {
        if (o.length != this.used) {
            return false;
        }
        int i = this.used;
        while (--i >= 0) {
            if (this.chars[i] == o[i]) continue;
            return false;
        }
        return true;
    }

    public void trim() {
        int st;
        int end = this.used;
        for (st = 0; st < end && this.chars[st] <= ' '; ++st) {
        }
        while (st < end && this.chars[end - 1] <= ' ') {
            --end;
        }
        if (st >= end) {
            this.used = 0;
        } else {
            this.used = end - st;
            if (st > 0) {
                System.arraycopy(this.chars, st, this.chars, 0, this.used);
            }
        }
    }

    public boolean endsWith(String s) {
        int len = s.length();
        if (len > this.used) {
            return false;
        }
        int i = this.used;
        while (--len >= 0) {
            if (this.chars[i] != s.charAt(len)) {
                return false;
            }
            --i;
        }
        return true;
    }

    public boolean startsWith(String s) {
        int len = s.length();
        if (len > this.used) {
            return false;
        }
        while (--len >= 0) {
            if (this.chars[len] == s.charAt(len)) continue;
            return false;
        }
        return true;
    }

    public void needSpace() {
        int t = this.used;
        if (t > 0 && this.chars[t - 1] > ' ') {
            this.append(' ');
        }
    }

    private void trimTo(int newUsed) {
        this.used = newUsed;
    }

    public void nlTerm() {
        if (this.hasMargin()) {
            this.needSpace();
        } else {
            int t = this.used;
            if (t <= 0) {
                return;
            }
            while (t > 0 && this.chars[t - 1] <= ' ') {
                --t;
            }
            this.trimTo(t);
            this.append('\n');
        }
    }

    public void toLineStart() {
        if (this.hasMargin()) {
            this.needSpace();
        } else {
            int t = this.used;
            if (t <= 0) {
                return;
            }
            while (t > 0 && this.chars[t - 1] <= ' ' && this.chars[t - 1] != '\n') {
                --t;
            }
            this.trimTo(t);
            this.col = 0;
        }
    }

    public void eatAwayChars(int count) {
        int nCol;
        if (this.used <= 0) {
            return;
        }
        for (nCol = 0; count > nCol && this.chars[this.used - nCol] != '\n'; ++nCol) {
        }
        this.col = nCol;
        this.trimTo(this.used - nCol);
    }

    public void _blanklines(int n) {
        int numBlankLines = n = Math.max(this.lastBlankLines, n);
        if (this.hasMargin()) {
            this.needSpace();
        } else {
            this.nlTerm();
            while (n-- > 0) {
                this.append('\n');
            }
        }
        this.lastBlankLines = numBlankLines;
    }

    public void blanklines(int n) {
        int numBlankLines = n = Math.max(this.lastBlankLines, n);
        if (this.hasMargin()) {
            this.needSpace();
        } else {
            int t = this.used;
            if (t <= 0) {
                while (n-- > 0) {
                    this.append('\n');
                }
                this.lastBlankLines = numBlankLines;
                return;
            }
            int l = n + 1;
            int c = 0;
            int lastNl = -1;
            int[] nlpos = new int[l];
            int nli = 0;
            while (t > 0 && this.chars[t - 1] <= ' ') {
                if (this.chars[t - 1] == '\n') {
                    if (lastNl == -1) {
                        lastNl = t;
                    }
                    ++c;
                    nlpos[nli] = t;
                    nli = (nli + 1) % l;
                }
                --t;
            }
            if (c == l) {
                this.used = lastNl - 1;
                this.append('\n');
            } else if (c > l) {
                int p = 0;
                if (n > 0) {
                    p = nli;
                }
                this.used = nlpos[p] - 1;
                this.append('\n');
            } else {
                if (lastNl > -1) {
                    this.used = lastNl;
                }
                while (c < l) {
                    ++c;
                    this.append('\n');
                }
            }
        }
        this.lastBlankLines = numBlankLines;
    }

    public boolean isWhitespaceLine() {
        if (this.col > 0) {
            for (int pos = this.used - 1; pos >= 0; --pos) {
                if (this.chars[pos] == '\n') {
                    return true;
                }
                if (Character.isWhitespace(this.chars[pos])) continue;
                return false;
            }
        }
        return true;
    }

    public int getCol() {
        return this.col;
    }
}

