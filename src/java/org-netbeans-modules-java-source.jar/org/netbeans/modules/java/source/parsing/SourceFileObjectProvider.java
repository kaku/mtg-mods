/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation
 */
package org.netbeans.modules.java.source.parsing;

import java.io.IOException;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.java.preprocessorbridge.spi.JavaFileFilterImplementation;
import org.netbeans.modules.java.source.parsing.AbstractSourceFileObject;

public interface SourceFileObjectProvider {
    @NonNull
    public AbstractSourceFileObject createJavaFileObject(@NonNull AbstractSourceFileObject.Handle var1, @NullAllowed JavaFileFilterImplementation var2, @NullAllowed CharSequence var3, boolean var4) throws IOException;
}

