/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.TreeScanner
 *  org.netbeans.lib.nbjavac.services.NBTreeMaker
 *  org.netbeans.lib.nbjavac.services.NBTreeMaker$IndexedClassDecl
 */
package org.netbeans.modules.java.source.parsing;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.TreeScanner;
import java.util.HashSet;
import java.util.Set;
import javax.lang.model.element.Name;
import org.netbeans.lib.nbjavac.services.NBTreeMaker;

class FindAnonymousVisitor
extends TreeScanner<Void, Void> {
    int firstInner = -1;
    int noInner;
    boolean hasLocalClass;
    final Set<Tree> docOwners = new HashSet<Tree>();
    private Mode mode = Mode.COLLECT;

    FindAnonymousVisitor() {
    }

    public final void reset() {
        this.firstInner = -1;
        this.noInner = 0;
        this.hasLocalClass = false;
        this.mode = Mode.CHECK;
    }

    public Void visitClass(ClassTree node, Void p) {
        if (this.firstInner == -1) {
            this.firstInner = ((NBTreeMaker.IndexedClassDecl)node).index;
        }
        if (node.getSimpleName().length() != 0) {
            this.hasLocalClass = true;
        }
        ++this.noInner;
        this.handleDoc((Tree)node);
        return (Void)TreeScanner.super.visitClass(node, (Object)p);
    }

    public Void visitMethod(MethodTree node, Void p) {
        this.handleDoc((Tree)node);
        return (Void)TreeScanner.super.visitMethod(node, (Object)p);
    }

    public Void visitVariable(VariableTree node, Void p) {
        this.handleDoc((Tree)node);
        return (Void)TreeScanner.super.visitVariable(node, (Object)p);
    }

    private void handleDoc(Tree tree) {
        if (this.mode == Mode.COLLECT) {
            this.docOwners.add(tree);
        }
    }

    private static enum Mode {
        COLLECT,
        CHECK;
        

        private Mode() {
        }
    }

}

