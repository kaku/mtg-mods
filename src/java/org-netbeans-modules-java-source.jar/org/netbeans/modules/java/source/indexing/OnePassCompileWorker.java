/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.code.Symbol$TypeSymbol
 *  com.sun.tools.javac.code.Type
 *  com.sun.tools.javac.code.TypeTag
 *  com.sun.tools.javac.code.Types
 *  com.sun.tools.javac.model.JavacElements
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCClassDecl
 *  com.sun.tools.javac.tree.JCTree$JCCompilationUnit
 *  com.sun.tools.javac.tree.TreeScanner
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.CouplingAbort
 *  com.sun.tools.javac.util.FatalError
 *  com.sun.tools.javac.util.JCDiagnostic
 *  com.sun.tools.javac.util.Log
 *  com.sun.tools.javac.util.MissingPlatformError
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.queries.SourceLevelQuery
 *  org.netbeans.api.java.queries.SourceLevelQuery$Profile
 *  org.netbeans.lib.nbjavac.services.CancelAbort
 *  org.netbeans.lib.nbjavac.services.CancelService
 *  org.netbeans.modules.parsing.lucene.support.LowMemoryWatcher
 *  org.netbeans.modules.parsing.spi.indexing.Context
 *  org.netbeans.modules.parsing.spi.indexing.Indexable
 *  org.netbeans.modules.parsing.spi.indexing.SuspendStatus
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.Pair
 */
package org.netbeans.modules.java.source.indexing;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.TypeTag;
import com.sun.tools.javac.code.Types;
import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeScanner;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.CouplingAbort;
import com.sun.tools.javac.util.FatalError;
import com.sun.tools.javac.util.JCDiagnostic;
import com.sun.tools.javac.util.Log;
import com.sun.tools.javac.util.MissingPlatformError;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.tools.FileObject;
import javax.tools.JavaFileObject;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.queries.SourceLevelQuery;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.lib.nbjavac.services.CancelAbort;
import org.netbeans.lib.nbjavac.services.CancelService;
import org.netbeans.modules.java.source.TreeLoader;
import org.netbeans.modules.java.source.indexing.APTUtils;
import org.netbeans.modules.java.source.indexing.CheckSums;
import org.netbeans.modules.java.source.indexing.CompileWorker;
import org.netbeans.modules.java.source.indexing.DiagnosticListenerImpl;
import org.netbeans.modules.java.source.indexing.FQN2Files;
import org.netbeans.modules.java.source.indexing.JavaCustomIndexer;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.indexing.JavaParsingContext;
import org.netbeans.modules.java.source.indexing.SourcePrefetcher;
import org.netbeans.modules.java.source.parsing.FileManagerTransaction;
import org.netbeans.modules.java.source.parsing.FileObjects;
import org.netbeans.modules.java.source.parsing.JavacParser;
import org.netbeans.modules.java.source.parsing.OutputFileManager;
import org.netbeans.modules.java.source.parsing.PrefetchableJavaFileObject;
import org.netbeans.modules.java.source.usages.ClassNamesForFileOraculumImpl;
import org.netbeans.modules.java.source.usages.ExecutableFilesIndex;
import org.netbeans.modules.parsing.lucene.support.LowMemoryWatcher;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.netbeans.modules.parsing.spi.indexing.SuspendStatus;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Pair;

final class OnePassCompileWorker
extends CompileWorker {
    OnePassCompileWorker() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    CompileWorker.ParsingOutput compile(CompileWorker.ParsingOutput previous, final org.netbeans.modules.parsing.spi.indexing.Context context, JavaParsingContext javaContext, Collection<? extends JavaCustomIndexer.CompileTuple> files) {
        HashSet<FileObject> aptGenerated;
        HashSet<File> createdFiles;
        HashSet<ElementHandle<TypeElement>> modifiedTypes;
        HashSet<Indexable> finished;
        HashSet<ElementHandle<TypeElement>> addedTypes;
        HashMap<JavaFileObject, List<String>> file2FQNs;
        block52 : {
            DiagnosticListenerImpl dc;
            boolean nop;
            LowMemoryWatcher mem;
            LinkedList<Object> units;
            ClassPath bootPath;
            HashMap<PrefetchableJavaFileObject, Pair> jfo2units;
            JavacTaskImpl jt;
            block51 : {
                file2FQNs = previous != null ? previous.file2FQNs : new HashMap<JavaFileObject, List<String>>();
                addedTypes = previous != null ? previous.addedTypes : new HashSet<ElementHandle<TypeElement>>();
                createdFiles = previous != null ? previous.createdFiles : new HashSet<File>();
                finished = previous != null ? previous.finishedFiles : new HashSet<Indexable>();
                modifiedTypes = previous != null ? previous.modifiedTypes : new HashSet<ElementHandle<TypeElement>>();
                aptGenerated = previous != null ? previous.aptGenerated : new HashSet<FileObject>();
                ClassNamesForFileOraculumImpl cnffOraculum = new ClassNamesForFileOraculumImpl(file2FQNs);
                mem = LowMemoryWatcher.getInstance();
                dc = new DiagnosticListenerImpl();
                jfo2units = new HashMap<PrefetchableJavaFileObject, Pair>();
                units = new LinkedList<Object>();
                jt = null;
                nop = true;
                SuspendStatus suspendStatus = context.getSuspendStatus();
                SourcePrefetcher sourcePrefetcher = SourcePrefetcher.create(files, suspendStatus);
                do {
                    if (sourcePrefetcher.hasNext()) {
                        JavaCustomIndexer.CompileTuple tuple = sourcePrefetcher.next();
                        try {
                            if (tuple == null) continue;
                            nop = false;
                            if (context.isCancelled()) {
                                CompileWorker.ParsingOutput parsingOutput = null;
                                return parsingOutput;
                            }
                            try {
                                if (mem.isLowMemory()) {
                                    jt = null;
                                    units = null;
                                    dc.cleanDiagnostics();
                                    mem.free();
                                }
                                if (jt == null) {
                                    jt = JavacParser.createJavacTask(javaContext.getClasspathInfo(), dc, javaContext.getSourceLevel(), javaContext.getProfile(), cnffOraculum, javaContext.getFQNs(), new CancelService(){

                                        public boolean isCanceled() {
                                            return context.isCancelled() || mem.isLowMemory();
                                        }
                                    }, tuple.aptGenerated ? null : APTUtils.get(context.getRoot()));
                                }
                                for (CompilationUnitTree cut : jt.parse(new JavaFileObject[]{tuple.jfo})) {
                                    if (units != null) {
                                        Pair unit = Pair.of((Object)cut, (Object)tuple);
                                        units.add((Object)unit);
                                        jfo2units.put(tuple.jfo, unit);
                                    }
                                    this.computeFQNs(file2FQNs, cut, tuple);
                                }
                                Log.instance((Context)jt.getContext()).nerrors = 0;
                            }
                            catch (CancelAbort ca) {
                                if (!context.isCancelled() || !JavaIndex.LOG.isLoggable(Level.FINEST)) continue;
                                JavaIndex.LOG.log(Level.FINEST, "OnePassCompileWorker was canceled in root: " + FileUtil.getFileDisplayName((org.openide.filesystems.FileObject)context.getRoot()), (Throwable)ca);
                            }
                            catch (Throwable t) {
                                if (JavaIndex.LOG.isLoggable(Level.WARNING)) {
                                    ClassPath bootPath2 = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.BOOT);
                                    ClassPath classPath2 = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.COMPILE);
                                    ClassPath sourcePath = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.SOURCE);
                                    Object[] arrobject = new Object[5];
                                    arrobject[0] = tuple.indexable.getURL().toString();
                                    arrobject[1] = FileUtil.getFileDisplayName((org.openide.filesystems.FileObject)context.getRoot());
                                    arrobject[2] = bootPath2 == null ? null : bootPath2.toString();
                                    arrobject[3] = classPath2 == null ? null : classPath2.toString();
                                    arrobject[4] = sourcePath == null ? null : sourcePath.toString();
                                    String message = String.format("OnePassCompileWorker caused an exception\nFile: %s\nRoot: %s\nBootpath: %s\nClasspath: %s\nSourcepath: %s", arrobject);
                                    JavaIndex.LOG.log(Level.WARNING, message, t);
                                }
                                if (t instanceof ThreadDeath) {
                                    throw (ThreadDeath)t;
                                }
                                jt = null;
                                units = null;
                                dc.cleanDiagnostics();
                                mem.free();
                            }
                            continue;
                        }
                        finally {
                            sourcePrefetcher.remove();
                            continue;
                        }
                    }
                    break block51;
                    break;
                } while (true);
                finally {
                    try {
                        sourcePrefetcher.close();
                    }
                    catch (IOException ioe) {
                        Exceptions.printStackTrace((Throwable)ioe);
                    }
                }
            }
            if (nop) {
                return CompileWorker.ParsingOutput.success(file2FQNs, addedTypes, createdFiles, finished, modifiedTypes, aptGenerated);
            }
            if (units == null || JavaCustomIndexer.NO_ONE_PASS_COMPILE_WORKER) {
                return CompileWorker.ParsingOutput.failure(file2FQNs, addedTypes, createdFiles, finished, modifiedTypes, aptGenerated);
            }
            JavaCustomIndexer.CompileTuple active = null;
            Iterable processors = jt != null ? jt.getProcessors() : null;
            boolean aptEnabled = processors != null && processors.iterator().hasNext();
            try {
                ArrayDeque<Future<Void>> barriers = new ArrayDeque<Future<Void>>();
                while (!units.isEmpty()) {
                    if (context.isCancelled()) {
                        return null;
                    }
                    Pair unit = (Pair)units.removeFirst();
                    active = (JavaCustomIndexer.CompileTuple)unit.second();
                    if (finished.contains((Object)active.indexable)) continue;
                    if (mem.isLowMemory()) {
                        units = null;
                        mem.free();
                        return CompileWorker.ParsingOutput.lowMemory(file2FQNs, addedTypes, createdFiles, finished, modifiedTypes, aptGenerated);
                    }
                    final Iterable types = jt.enterTrees(Collections.singletonList(unit.first()));
                    if (jfo2units.remove(active.jfo) != null) {
                        class ScanNested
                        extends TreeScanner {
                            Set<Pair<CompilationUnitTree, JavaCustomIndexer.CompileTuple>> dependencies;
                            final /* synthetic */ Types val$ts;
                            final /* synthetic */ HashMap val$jfo2units;
                            final /* synthetic */ Set val$finished;
                            final /* synthetic */ Indexable val$activeIndexable;

                            ScanNested() {
                                this.val$ts = var2_2;
                                this.val$jfo2units = var3_3;
                                this.val$finished = var4_4;
                                this.val$activeIndexable = var5_5;
                                this.dependencies = new LinkedHashSet<Pair<CompilationUnitTree, JavaCustomIndexer.CompileTuple>>();
                            }

                            public void visitClassDef(JCTree.JCClassDecl node) {
                                if (node.sym != null) {
                                    Type st = this.val$ts.supertype(node.sym.type);
                                    boolean envForSuperTypeFound = false;
                                    while (!envForSuperTypeFound && st != null && st.hasTag(TypeTag.CLASS)) {
                                        Symbol.ClassSymbol c = st.tsym.outermostClass();
                                        Pair u = (Pair)this.val$jfo2units.remove(c.sourcefile);
                                        if (u != null && !this.val$finished.contains((Object)((JavaCustomIndexer.CompileTuple)u.second()).indexable) && !((JavaCustomIndexer.CompileTuple)u.second()).indexable.equals((Object)this.val$activeIndexable)) {
                                            if (this.dependencies.add((Pair)u)) {
                                                this.scan((JCTree)((JCTree.JCCompilationUnit)u.first()));
                                            }
                                            envForSuperTypeFound = true;
                                        }
                                        st = this.val$ts.supertype(st);
                                    }
                                }
                                super.visitClassDef(node);
                            }
                        }
                        Types ts = Types.instance((Context)jt.getContext());
                        Indexable activeIndexable = active.indexable;
                        ScanNested scanner = new ScanNested(this, ts, jfo2units, finished, activeIndexable);
                        scanner.scan((JCTree)((JCTree.JCCompilationUnit)unit.first()));
                        if (!scanner.dependencies.isEmpty()) {
                            units.addFirst((Object)unit);
                            for (Pair<CompilationUnitTree, JavaCustomIndexer.CompileTuple> pair : scanner.dependencies) {
                                units.addFirst(pair);
                            }
                            continue;
                        }
                    }
                    if (mem.isLowMemory()) {
                        units = null;
                        mem.free();
                        return CompileWorker.ParsingOutput.lowMemory(file2FQNs, addedTypes, createdFiles, finished, modifiedTypes, aptGenerated);
                    }
                    jt.analyze(types);
                    if (aptEnabled) {
                        JavaCustomIndexer.addAptGenerated(context, javaContext, active, aptGenerated);
                    }
                    if (mem.isLowMemory()) {
                        units = null;
                        mem.free();
                        return CompileWorker.ParsingOutput.lowMemory(file2FQNs, addedTypes, createdFiles, finished, modifiedTypes, aptGenerated);
                    }
                    javaContext.getFQNs().set(types, active.indexable.getURL());
                    boolean[] main = new boolean[1];
                    if (javaContext.getCheckSums().checkAndSet(active.indexable.getURL(), types, (Elements)jt.getElements()) || context.isSupplementaryFilesIndexing()) {
                        javaContext.analyze(Collections.singleton(unit.first()), jt, (JavaCustomIndexer.CompileTuple)unit.second(), addedTypes, main);
                    } else {
                        HashSet aTypes = new HashSet();
                        javaContext.analyze(Collections.singleton(unit.first()), jt, (JavaCustomIndexer.CompileTuple)unit.second(), aTypes, main);
                        addedTypes.addAll(aTypes);
                        modifiedTypes.addAll(aTypes);
                    }
                    ExecutableFilesIndex.DEFAULT.setMainClass(context.getRoot().getURL(), active.indexable.getURL(), main[0]);
                    JavaCustomIndexer.setErrors(context, active, dc);
                    final boolean virtual = active.virtual;
                    final JavacTaskImpl jtFin = jt;
                    barriers.offer(FileManagerTransaction.runConcurrent(new FileSystem.AtomicAction(){

                        public void run() throws IOException {
                            Iterable generatedFiles = jtFin.generate(types);
                            if (!virtual) {
                                for (JavaFileObject generated : generatedFiles) {
                                    if (!(generated instanceof FileObjects.FileBase)) continue;
                                    createdFiles.add(((FileObjects.FileBase)generated).getFile());
                                }
                            }
                        }
                    }));
                    Log.instance((Context)jt.getContext()).nerrors = 0;
                    finished.add(active.indexable);
                }
                for (Future barrier : barriers) {
                    barrier.get();
                }
                return CompileWorker.ParsingOutput.success(file2FQNs, addedTypes, createdFiles, finished, modifiedTypes, aptGenerated);
            }
            catch (CouplingAbort ca) {
                TreeLoader.dumpCouplingAbort(ca, null);
            }
            catch (OutputFileManager.InvalidSourcePath isp) {
                if (JavaIndex.LOG.isLoggable(Level.FINEST)) {
                    bootPath = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.BOOT);
                    ClassPath classPath = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.COMPILE);
                    ClassPath sourcePath = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.SOURCE);
                    Object[] arrobject = new Object[5];
                    arrobject[0] = active.jfo.toUri().toString();
                    arrobject[1] = FileUtil.getFileDisplayName((org.openide.filesystems.FileObject)context.getRoot());
                    arrobject[2] = bootPath == null ? null : bootPath.toString();
                    arrobject[3] = classPath == null ? null : classPath.toString();
                    arrobject[4] = sourcePath == null ? null : sourcePath.toString();
                    String message = String.format("OnePassCompileWorker caused an exception\nFile: %s\nRoot: %s\nBootpath: %s\nClasspath: %s\nSourcepath: %s", arrobject);
                    JavaIndex.LOG.log(Level.FINEST, message, isp);
                }
            }
            catch (MissingPlatformError mpe) {
                if (JavaIndex.LOG.isLoggable(Level.FINEST)) {
                    bootPath = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.BOOT);
                    ClassPath classPath = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.COMPILE);
                    ClassPath sourcePath = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.SOURCE);
                    Object[] arrobject = new Object[5];
                    arrobject[0] = active.jfo.toUri().toString();
                    arrobject[1] = FileUtil.getFileDisplayName((org.openide.filesystems.FileObject)context.getRoot());
                    arrobject[2] = bootPath == null ? null : bootPath.toString();
                    arrobject[3] = classPath == null ? null : classPath.toString();
                    arrobject[4] = sourcePath == null ? null : sourcePath.toString();
                    String message = String.format("OnePassCompileWorker caused an exception\nFile: %s\nRoot: %s\nBootpath: %s\nClasspath: %s\nSourcepath: %s", arrobject);
                    JavaIndex.LOG.log(Level.FINEST, message, (Throwable)mpe);
                }
                JavaCustomIndexer.brokenPlatform(context, files, mpe.getDiagnostic());
            }
            catch (CancelAbort ca) {
                if (mem.isLowMemory()) {
                    units = null;
                    mem.free();
                    return CompileWorker.ParsingOutput.lowMemory(file2FQNs, addedTypes, createdFiles, finished, modifiedTypes, aptGenerated);
                }
                if (JavaIndex.LOG.isLoggable(Level.FINEST)) {
                    JavaIndex.LOG.log(Level.FINEST, "OnePassCompileWorker was canceled in root: " + FileUtil.getFileDisplayName((org.openide.filesystems.FileObject)context.getRoot()), (Throwable)ca);
                }
            }
            catch (Throwable t) {
                Level level;
                if (t instanceof ThreadDeath) {
                    throw (ThreadDeath)t;
                }
                Level level2 = level = t instanceof FatalError ? Level.FINEST : Level.WARNING;
                if (!JavaIndex.LOG.isLoggable(level)) break block52;
                ClassPath bootPath3 = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.BOOT);
                ClassPath classPath3 = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.COMPILE);
                ClassPath sourcePath = javaContext.getClasspathInfo().getClassPath(ClasspathInfo.PathKind.SOURCE);
                Object[] arrobject = new Object[5];
                arrobject[0] = active.jfo.toUri().toString();
                arrobject[1] = FileUtil.getFileDisplayName((org.openide.filesystems.FileObject)context.getRoot());
                arrobject[2] = bootPath3 == null ? null : bootPath3.toString();
                arrobject[3] = classPath3 == null ? null : classPath3.toString();
                arrobject[4] = sourcePath == null ? null : sourcePath.toString();
                String message = String.format("OnePassCompileWorker caused an exception\nFile: %s\nRoot: %s\nBootpath: %s\nClasspath: %s\nSourcepath: %s", arrobject);
                JavaIndex.LOG.log(level, message, t);
            }
        }
        return CompileWorker.ParsingOutput.failure(file2FQNs, addedTypes, createdFiles, finished, modifiedTypes, aptGenerated);
    }

}

