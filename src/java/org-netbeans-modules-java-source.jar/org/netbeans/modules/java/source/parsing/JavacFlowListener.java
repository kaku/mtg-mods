/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.util.TaskEvent
 *  com.sun.source.util.TaskEvent$Kind
 *  com.sun.source.util.TaskListener
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.tree.JCTree
 *  com.sun.tools.javac.tree.JCTree$JCCompilationUnit
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javac.util.Context$Key
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.java.source.parsing;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.util.TaskEvent;
import com.sun.source.util.TaskListener;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.Context;
import java.net.URI;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import javax.tools.JavaFileObject;
import org.openide.filesystems.FileObject;

class JavacFlowListener {
    protected static final Context.Key<JavacFlowListener> flowListenerKey = new Context.Key();
    private final Set<URI> flowCompleted = new HashSet<URI>();

    public static JavacFlowListener instance(Context context) {
        JavacFlowListener flowListener = (JavacFlowListener)context.get(flowListenerKey);
        return flowListener != null ? flowListener : null;
    }

    static void preRegister(Context context, JavacTaskImpl jti) {
        context.put(flowListenerKey, (Object)new JavacFlowListener(context, jti));
    }

    private JavacFlowListener(Context context, JavacTaskImpl jti) {
        jti.setTaskListener((TaskListener)new TaskListenerImpl());
    }

    final boolean hasFlowCompleted(FileObject fo) {
        if (fo == null) {
            return false;
        }
        try {
            return this.flowCompleted.contains(fo.getURL().toURI());
        }
        catch (Exception e) {
            return false;
        }
    }

    private class TaskListenerImpl
    implements TaskListener {
        public void started(TaskEvent e) {
        }

        public void finished(TaskEvent e) {
            JCTree.JCCompilationUnit toplevel;
            if (e.getKind() == TaskEvent.Kind.ANALYZE && (toplevel = (JCTree.JCCompilationUnit)e.getCompilationUnit()) != null && toplevel.sourcefile != null) {
                JavacFlowListener.this.flowCompleted.add(toplevel.sourcefile.toUri());
            }
        }
    }

}

