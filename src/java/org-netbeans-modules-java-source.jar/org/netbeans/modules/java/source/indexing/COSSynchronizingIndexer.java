/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.modules.parsing.impl.indexing.IndexerCache
 *  org.netbeans.modules.parsing.impl.indexing.IndexerCache$IndexerInfo
 *  org.netbeans.modules.parsing.spi.indexing.Context
 *  org.netbeans.modules.parsing.spi.indexing.CustomIndexer
 *  org.netbeans.modules.parsing.spi.indexing.CustomIndexerFactory
 *  org.netbeans.modules.parsing.spi.indexing.Indexable
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.source.indexing;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.modules.java.source.usages.BuildArtifactMapperImpl;
import org.netbeans.modules.parsing.impl.indexing.IndexerCache;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.netbeans.modules.parsing.spi.indexing.CustomIndexer;
import org.netbeans.modules.parsing.spi.indexing.CustomIndexerFactory;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;

public class COSSynchronizingIndexer
extends CustomIndexer {
    private static final Logger LOG = Logger.getLogger(COSSynchronizingIndexer.class.getName());

    protected void index(Iterable<? extends Indexable> files, Context context) {
        URL rootURL = context.getRootURI();
        if (FileUtil.getArchiveFile((URL)rootURL) != null) {
            return;
        }
        if (!BuildArtifactMapperImpl.isUpdateResources(BuildArtifactMapperImpl.getTargetFolder(rootURL))) {
            return;
        }
        Set<String> javaMimeTypes = COSSynchronizingIndexer.gatherJavaMimeTypes();
        LinkedList<File> updated = new LinkedList<File>();
        ClassPath srcPath = ClassPath.getClassPath((FileObject)context.getRoot(), (String)"classpath/source");
        for (Indexable i : files) {
            if (javaMimeTypes.contains(i.getMimeType())) continue;
            try {
                URL url = i.getURL();
                if (url == null) continue;
                FileObject resource = srcPath.findResource(i.getRelativePath());
                if (resource == null) {
                    LOG.log(Level.INFO, "File {0} not on source path {1}, root {2}", new Object[]{i.getURL(), srcPath, context.getRoot()});
                    continue;
                }
                if (!FileUtil.isParentOf((FileObject)context.getRoot(), (FileObject)resource)) continue;
                updated.add(Utilities.toFile((URI)url.toURI()));
            }
            catch (URISyntaxException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        try {
            File sourceRootFile = Utilities.toFile((URI)context.getRootURI().toURI());
            if (!context.checkForEditorModifications()) {
                BuildArtifactMapperImpl.classCacheUpdated(context.getRootURI(), sourceRootFile, Collections.emptyList(), updated, true);
            }
        }
        catch (URISyntaxException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    public static Set<String> gatherJavaMimeTypes() {
        HashSet<String> mimeTypes = new HashSet<String>();
        Collection indexers = IndexerCache.getCifCache().getIndexersByName("java");
        if (indexers != null) {
            for (IndexerCache.IndexerInfo i : indexers) {
                mimeTypes.addAll(i.getMimeTypes());
            }
        } else {
            LOG.warning("No java indexer found.");
        }
        return mimeTypes;
    }

    public static final class Factory
    extends CustomIndexerFactory {
        public CustomIndexer createIndexer() {
            return new COSSynchronizingIndexer();
        }

        public boolean supportsEmbeddedIndexers() {
            return true;
        }

        public void filesDeleted(Iterable<? extends Indexable> deleted, Context context) {
            if (BuildArtifactMapperImpl.getTargetFolder(context.getRootURI()) == null) {
                return;
            }
            LinkedList<File> deletedFiles = new LinkedList<File>();
            for (Indexable d : deleted) {
                try {
                    deletedFiles.add(Utilities.toFile((URI)d.getURL().toURI()));
                }
                catch (URISyntaxException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            try {
                File sourceRootFile = Utilities.toFile((URI)context.getRootURI().toURI());
                BuildArtifactMapperImpl.classCacheUpdated(context.getRootURI(), sourceRootFile, deletedFiles, Collections.emptyList(), true);
            }
            catch (URISyntaxException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }

        public void filesDirty(Iterable<? extends Indexable> dirty, Context context) {
        }

        public String getIndexerName() {
            return COSSynchronizingIndexer.class.getName();
        }

        public int getIndexVersion() {
            return 1;
        }
    }

}

