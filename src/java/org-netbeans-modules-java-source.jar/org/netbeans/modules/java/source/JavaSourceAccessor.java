/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.impl.Utilities
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.ParserResultTask
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.netbeans.modules.parsing.spi.TaskIndexingMode
 *  org.openide.filesystems.FileObject
 *  org.openide.text.PositionRef
 *  org.openide.util.Exceptions
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.java.source;

import com.sun.tools.javac.api.JavacTaskImpl;
import java.io.IOException;
import java.util.Collection;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.tools.JavaFileObject;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.source.CancellableTask;
import org.netbeans.api.java.source.ClassIndex;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.java.source.JavaParserResultTask;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.api.java.source.PositionConverter;
import org.netbeans.modules.java.source.parsing.ClasspathInfoProvider;
import org.netbeans.modules.java.source.parsing.CompilationInfoImpl;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.impl.Utilities;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserResultTask;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.TaskIndexingMode;
import org.openide.filesystems.FileObject;
import org.openide.text.PositionRef;
import org.openide.util.Exceptions;
import org.openide.util.Parameters;

public abstract class JavaSourceAccessor {
    private Map<CancellableTask<CompilationInfo>, ParserResultTask<?>> tasks = new IdentityHashMap();
    private static volatile JavaSourceAccessor INSTANCE;

    public static synchronized JavaSourceAccessor getINSTANCE() {
        if (INSTANCE == null) {
            try {
                Class.forName("org.netbeans.api.java.source.JavaSource", true, JavaSourceAccessor.class.getClassLoader());
                assert (INSTANCE != null);
            }
            catch (ClassNotFoundException e) {
                Exceptions.printStackTrace((Throwable)e);
            }
        }
        return INSTANCE;
    }

    public static void setINSTANCE(JavaSourceAccessor instance) {
        assert (instance != null);
        INSTANCE = instance;
    }

    private int translatePriority(JavaSource.Priority priority) {
        assert (priority != null);
        int tmp = priority == JavaSource.Priority.MAX ? 0 : (priority == JavaSource.Priority.MIN ? Integer.MAX_VALUE : priority.ordinal() * 100);
        return tmp;
    }

    public void revalidate(JavaSource js) {
        Collection<Source> sources = this.getSources(js);
        assert (sources != null);
        if (sources.size() == 1) {
            Utilities.revalidate((Source)sources.iterator().next());
        }
    }

    public boolean isJavaCompilerLocked() {
        return Utilities.holdsParserLock();
    }

    public void lockJavaCompiler() {
        Utilities.acquireParserLock();
    }

    public void unlockJavaCompiler() {
        Utilities.releaseParserLock();
    }

    public void addPhaseCompletionTask(JavaSource js, CancellableTask<CompilationInfo> task, JavaSource.Phase phase, JavaSource.Priority priority, TaskIndexingMode taskIndexingMode) {
        Collection<Source> sources = this.getSources(js);
        assert (sources.size() == 1);
        int pp = this.translatePriority(priority);
        if (this.tasks.keySet().contains(task)) {
            throw new IllegalArgumentException(String.format("Task: %s is already scheduled", task.toString()));
        }
        CancelableTaskWrapper hanz = new CancelableTaskWrapper(task, pp, phase, js, taskIndexingMode);
        this.tasks.put(task, (CancelableTaskWrapper)hanz);
        Utilities.addParserResultTask((ParserResultTask)hanz, (Source)sources.iterator().next());
    }

    public void removePhaseCompletionTask(JavaSource js, CancellableTask<CompilationInfo> task) {
        Collection<Source> sources = this.getSources(js);
        assert (sources.size() == 1);
        ParserResultTask hanz = this.tasks.remove(task);
        if (hanz == null) {
            throw new IllegalArgumentException(String.format("Task: %s is not scheduled", task.toString()));
        }
        Utilities.removeParserResultTask(hanz, (Source)sources.iterator().next());
    }

    public void rescheduleTask(JavaSource js, CancellableTask<CompilationInfo> task) {
        Collection<Source> sources = this.getSources(js);
        assert (sources.size() == 1);
        ParserResultTask hanz = this.tasks.get(task);
        if (hanz != null) {
            Utilities.rescheduleTask(hanz, (Source)sources.iterator().next());
        }
    }

    public abstract Collection<Source> getSources(JavaSource var1);

    public abstract void setJavaSource(CompilationInfo var1, JavaSource var2);

    public abstract JavacTaskImpl getJavacTask(CompilationInfo var1);

    public abstract CompilationController createCompilationController(Source var1) throws IOException, ParseException;

    public abstract long createTaggedCompilationController(JavaSource var1, long var2, Object[] var4) throws IOException;

    public abstract JavaSource create(ClasspathInfo var1, PositionConverter var2, Collection<? extends FileObject> var3) throws IllegalArgumentException;

    public abstract CompilationInfo createCompilationInfo(CompilationInfoImpl var1);

    public abstract CompilationController createCompilationController(CompilationInfoImpl var1);

    public abstract void invalidateCachedClasspathInfo(FileObject var1);

    public abstract CompilationInfoImpl getCompilationInfoImpl(CompilationInfo var1);

    @NonNull
    public abstract String generateReadableParameterName(@NonNull String var1, @NonNull Set<String> var2);

    public abstract void invalidate(CompilationInfo var1);

    public static boolean holdsParserLock() {
        return Utilities.holdsParserLock();
    }

    public abstract ModificationResult.Difference createDifference(ModificationResult.Difference.Kind var1, PositionRef var2, PositionRef var3, String var4, String var5, String var6);

    public abstract ModificationResult.Difference createNewFileDifference(JavaFileObject var1, String var2);

    public abstract ModificationResult createModificationResult(Map<FileObject, List<ModificationResult.Difference>> var1, Map<?, int[]> var2);

    public abstract ElementUtilities createElementUtilities(@NonNull JavacTaskImpl var1);

    public abstract Map<FileObject, List<ModificationResult.Difference>> getDiffsFromModificationResult(ModificationResult var1);

    public abstract Map<?, int[]> getTagsFromModificationResult(ModificationResult var1);

    public abstract ClassIndex createClassIndex(@NonNull ClassPath var1, @NonNull ClassPath var2, @NonNull ClassPath var3, boolean var4);

    private static class CancelableTaskWrapper
    extends JavaParserResultTask
    implements ClasspathInfoProvider {
        private final JavaSource javaSource;
        private final int priority;
        private final CancellableTask<CompilationInfo> task;

        public CancelableTaskWrapper(@NonNull CancellableTask<CompilationInfo> task, int priority, @NonNull JavaSource.Phase phase, @NonNull JavaSource javaSource, @NonNull TaskIndexingMode taskIndexingMode) {
            super(phase, taskIndexingMode);
            assert (phase != null);
            assert (javaSource != null);
            this.task = task;
            this.priority = priority;
            this.javaSource = javaSource;
        }

        public int getPriority() {
            return this.priority;
        }

        public Class<? extends Scheduler> getSchedulerClass() {
            return null;
        }

        public void cancel() {
            this.task.cancel();
        }

        public void run(@NonNull Parser.Result result, SchedulerEvent event) {
            Parameters.notNull((CharSequence)"result", (Object)result);
            CompilationInfo info = CompilationInfo.get(result);
            if (info == null) {
                throw new IllegalArgumentException(String.format("Result %s [%s] does not provide CompilationInfo", result.toString(), result.getClass().getName()));
            }
            try {
                JavaSourceAccessor.getINSTANCE().setJavaSource(info, this.javaSource);
                this.task.run(info);
            }
            catch (Exception ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }

        @Override
        public ClasspathInfo getClasspathInfo() {
            return this.javaSource.getClasspathInfo();
        }

        public String toString() {
            return this.getClass().getSimpleName() + "[task: " + this.task + ", phase: " + (Object)((Object)this.getPhase()) + ", priority: " + this.priority + "]";
        }
    }

}

