/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.source.usages;

import java.util.EventObject;
import javax.lang.model.element.TypeElement;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.modules.java.source.usages.ClassIndexImpl;

public class ClassIndexImplEvent
extends EventObject {
    private final Iterable<? extends ElementHandle<TypeElement>> types;

    ClassIndexImplEvent(ClassIndexImpl source, Iterable<? extends ElementHandle<TypeElement>> types) {
        super(source);
        assert (types != null);
        this.types = types;
    }

    public Iterable<? extends ElementHandle<TypeElement>> getTypes() {
        return this.types;
    }
}

