/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.platform.JavaPlatform
 *  org.netbeans.api.java.platform.JavaPlatformManager
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.Task
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.ParserResultTask
 *  org.netbeans.modules.parsing.spi.SourceModificationEvent
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Parameters
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.java.source.parsing;

import java.io.IOException;
import java.net.URL;
import java.util.logging.Logger;
import javax.swing.event.ChangeListener;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.api.java.platform.JavaPlatformManager;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.JavaParserResultTask;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.modules.java.source.JavaSourceAccessor;
import org.netbeans.modules.java.source.parsing.ClasspathInfoListener;
import org.netbeans.modules.java.source.parsing.ClasspathInfoProvider;
import org.netbeans.modules.java.source.parsing.CompilationInfoImpl;
import org.netbeans.modules.java.source.parsing.JavacParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.Task;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserResultTask;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.ChangeSupport;
import org.openide.util.Parameters;
import org.openide.util.WeakListeners;

public class ClassParser
extends Parser {
    public static final String MIME_TYPE = "application/x-class-file";
    private static final ClassPath EMPTY_PATH = ClassPathSupport.createClassPath((URL[])new URL[0]);
    private static final Logger LOGGER = Logger.getLogger(Parser.class.getName());
    private final ChangeSupport changeSupport;
    private final ClasspathInfoListener cpInfoListener;
    private CompilationInfoImpl ciImpl;
    private Snapshot lastSnapshot;
    private ClasspathInfo info;
    private ChangeListener wl;

    ClassParser() {
        this.changeSupport = new ChangeSupport((Object)this);
        this.cpInfoListener = new ClasspathInfoListener(this.changeSupport, null);
    }

    public void parse(Snapshot snapshot, Task task, SourceModificationEvent event) throws ParseException {
        FileObject root;
        ClassPath srcPath;
        ClassPath compilePath;
        ClassPath bootPath;
        assert (snapshot != null);
        this.lastSnapshot = snapshot;
        Source source = snapshot.getSource();
        assert (source != null);
        FileObject file = source.getFileObject();
        assert (file != null);
        if (this.info == null) {
            if (task instanceof ClasspathInfoProvider) {
                this.info = ((ClasspathInfoProvider)task).getClasspathInfo();
            }
            if (this.info == null) {
                ClassPath executePath;
                ClassPath srcPath2;
                bootPath = ClassPath.getClassPath((FileObject)file, (String)"classpath/boot");
                if (bootPath == null) {
                    bootPath = JavaPlatformManager.getDefault().getDefaultPlatform().getBootstrapLibraries();
                }
                if ((compilePath = ClassPath.getClassPath((FileObject)file, (String)"classpath/compile")) == null) {
                    compilePath = EMPTY_PATH;
                }
                if ((executePath = ClassPath.getClassPath((FileObject)file, (String)"classpath/execute")) == null) {
                    executePath = EMPTY_PATH;
                }
                if ((srcPath2 = ClassPath.getClassPath((FileObject)file, (String)"classpath/source")) == null) {
                    srcPath2 = EMPTY_PATH;
                }
                this.info = ClasspathInfo.create(bootPath, ClassPathSupport.createProxyClassPath((ClassPath[])new ClassPath[]{compilePath, executePath}), srcPath2);
            }
            assert (this.info != null);
            this.wl = WeakListeners.change((ChangeListener)this.cpInfoListener, (Object)this.info);
            this.info.addChangeListener(this.wl);
        }
        if ((root = ClassPathSupport.createProxyClassPath((ClassPath[])new ClassPath[]{bootPath = this.info.getClassPath(ClasspathInfo.PathKind.BOOT), compilePath = this.info.getClassPath(ClasspathInfo.PathKind.COMPILE), srcPath = this.info.getClassPath(ClasspathInfo.PathKind.SOURCE)}).findOwnerRoot(file)) == null) {
            throw new ParseException(String.format("The file %s is not owned by provided classpaths, boot: %s, compile: %s, src: %s", FileUtil.getFileDisplayName((FileObject)file), bootPath.toString(), compilePath.toString(), srcPath.toString()));
        }
        try {
            this.ciImpl = new CompilationInfoImpl(this.info, file, root);
        }
        catch (IOException ioe) {
            throw new ParseException("ClassParser failure", (Throwable)ioe);
        }
    }

    public Parser.Result getResult(Task task) throws ParseException {
        assert (this.ciImpl != null);
        boolean isParserResultTask = task instanceof ParserResultTask;
        boolean isJavaParserResultTask = task instanceof JavaParserResultTask;
        boolean isUserTask = task instanceof UserTask;
        JavacParserResult result = null;
        if (isParserResultTask) {
            ClasspathInfo taskProvidedCpInfo;
            JavaSource.Phase currentPhase = this.ciImpl.getPhase();
            JavaSource.Phase requiredPhase = isJavaParserResultTask ? ((JavaParserResultTask)task).getPhase() : JavaSource.Phase.RESOLVED;
            if (task instanceof ClasspathInfoProvider && (taskProvidedCpInfo = ((ClasspathInfoProvider)task).getClasspathInfo()) != null && !taskProvidedCpInfo.equals(this.info)) {
                assert (this.info != null);
                assert (this.wl != null);
                this.info.removeChangeListener(this.wl);
                this.info = null;
                this.parse(this.lastSnapshot, task, null);
            }
            if (currentPhase.compareTo(requiredPhase) < 0) {
                this.ciImpl.setPhase(requiredPhase);
            }
            result = new JavacParserResult(JavaSourceAccessor.getINSTANCE().createCompilationInfo(this.ciImpl));
        } else if (isUserTask) {
            result = new JavacParserResult(JavaSourceAccessor.getINSTANCE().createCompilationController(this.ciImpl));
        } else {
            LOGGER.warning("Ignoring unknown task: " + (Object)task);
        }
        return result;
    }

    public void cancel() {
    }

    public void addChangeListener(ChangeListener changeListener) {
        Parameters.notNull((CharSequence)"changeListener", (Object)changeListener);
        this.changeSupport.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        Parameters.notNull((CharSequence)"changeListener", (Object)changeListener);
        this.changeSupport.removeChangeListener(changeListener);
    }
}

