/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.platform.JavaPlatform
 *  org.netbeans.api.java.platform.JavaPlatformManager
 *  org.netbeans.api.progress.ProgressUtils
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.openide.awt.StatusDisplayer
 *  org.openide.cookies.OpenCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectExistsException
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.loaders.MultiDataObject
 *  org.openide.loaders.MultiFileLoader
 *  org.openide.nodes.CookieSet
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java;

import java.net.URL;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.api.java.platform.JavaPlatformManager;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.modules.java.BinaryElementOpen;
import org.netbeans.modules.java.JavaNode;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.openide.awt.StatusDisplayer;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public final class ClassDataObject
extends MultiDataObject {
    public ClassDataObject(FileObject pf, MultiFileLoader loader) throws DataObjectExistsException {
        super(pf, loader);
        this.getCookieSet().add((Node.Cookie)new OpenSourceCookie());
    }

    public Node createNodeDelegate() {
        return new JavaNode((DataObject)this, false);
    }

    public Lookup getLookup() {
        return this.getCookieSet().getLookup();
    }

    private final class OpenSourceCookie
    implements OpenCookie {
        private OpenSourceCookie() {
        }

        public void open() {
            final AtomicBoolean cancel = new AtomicBoolean();
            ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

                @Override
                public void run() {
                    try {
                        ClasspathInfo cpInfo;
                        ClassPath bootPath;
                        FileObject fo = ClassDataObject.this.getPrimaryFile();
                        FileObject binaryRoot = null;
                        String resourceName = null;
                        ClassPath cp = ClassPath.getClassPath((FileObject)fo, (String)"classpath/compile");
                        if (cp == null || (binaryRoot = cp.findOwnerRoot(fo)) == null) {
                            cp = ClassPath.getClassPath((FileObject)fo, (String)"classpath/execute");
                            if (cp != null) {
                                binaryRoot = cp.findOwnerRoot(fo);
                                resourceName = cp.getResourceName(fo, '.', false);
                            }
                        } else if (binaryRoot != null) {
                            resourceName = cp.getResourceName(fo, '.', false);
                        }
                        if ((bootPath = ClassPath.getClassPath((FileObject)fo, (String)"classpath/boot")) == null) {
                            bootPath = JavaPlatformManager.getDefault().getDefaultPlatform().getBootstrapLibraries();
                        }
                        if (cancel.get()) {
                            return;
                        }
                        FileObject resource = null;
                        ElementHandle<TypeElement> handle = resourceName != null ? ElementHandle.createTypeElementHandle(ElementKind.CLASS, resourceName.replace('/', '.')) : null;
                        ClasspathInfo classpathInfo = cpInfo = cp != null && bootPath != null ? ClasspathInfo.create(bootPath, cp, ClassPathSupport.createClassPath((URL[])new URL[0])) : null;
                        if (binaryRoot != null) {
                            resource = SourceUtils.getFile(handle, cpInfo);
                        }
                        if (cancel.get()) {
                            return;
                        }
                        if (resource != null) {
                            DataObject sourceFile = DataObject.find((FileObject)resource);
                            OpenCookie oc = (OpenCookie)sourceFile.getCookie(OpenCookie.class);
                            if (oc != null) {
                                oc.open();
                            } else {
                                Logger.getLogger(ClassDataObject.class.getName()).log(Level.WARNING, "SourceFile: {0} has no OpenCookie", FileUtil.getFileDisplayName((FileObject)resource));
                            }
                        } else {
                            BinaryElementOpen beo = (BinaryElementOpen)Lookup.getDefault().lookup(BinaryElementOpen.class);
                            if (!(beo != null && handle != null && cpInfo != null && beo.open(cpInfo, handle, cancel) || cancel.get())) {
                                if (resourceName == null) {
                                    resourceName = fo.getName();
                                }
                                StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(ClassDataObject.class, (String)"TXT_NoSources", (Object)resourceName.replace('/', '.')));
                            }
                        }
                    }
                    catch (DataObjectNotFoundException nf) {
                        Exceptions.printStackTrace((Throwable)nf);
                    }
                }
            }, (String)NbBundle.getMessage(ClassDataObject.class, (String)"TXT_OpenClassFile"), (AtomicBoolean)cancel, (boolean)false);
        }

    }

}

