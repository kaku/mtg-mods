/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java;

import java.util.concurrent.atomic.AtomicBoolean;
import javax.lang.model.element.Element;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.ElementHandle;

public interface BinaryElementOpen {
    public boolean open(ClasspathInfo var1, ElementHandle<? extends Element> var2, AtomicBoolean var3);
}

