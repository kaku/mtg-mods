/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.actions.Openable
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.queries.SourceForBinaryQuery
 *  org.netbeans.api.java.queries.SourceJavadocAttacher
 *  org.netbeans.api.java.queries.SourceJavadocAttacher$AttachmentListener
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.URLMapper
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 */
package org.netbeans.modules.java.classfile;

import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import org.netbeans.api.actions.Openable;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.queries.SourceForBinaryQuery;
import org.netbeans.api.java.queries.SourceJavadocAttacher;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.URLMapper;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

public class AttachSourcePanel
extends JPanel {
    private static final RequestProcessor RP = new RequestProcessor(AttachSourcePanel.class);
    private final URL root;
    private final URL file;
    private final String binaryName;
    private JButton jButton1;
    private JLabel jLabel1;

    public AttachSourcePanel(@NonNull URL root, @NonNull URL file, @NonNull String binaryName) {
        assert (root != null);
        assert (file != null);
        assert (binaryName != null);
        this.root = root;
        this.file = file;
        this.binaryName = binaryName;
        this.initComponents();
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.jButton1 = new JButton();
        this.jLabel1.setText(NbBundle.getMessage(AttachSourcePanel.class, (String)"AttachSourcePanel.jLabel1.text"));
        this.jButton1.setText(NbBundle.getMessage(AttachSourcePanel.class, (String)"AttachSourcePanel.jButton1.text"));
        this.jButton1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                AttachSourcePanel.this.attachSources(evt);
            }
        });
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addContainerGap().addComponent(this.jLabel1, -1, 608, 32767).addGap(22, 22, 22).addComponent(this.jButton1).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel1).addComponent(this.jButton1, -2, 25, -2)));
    }

    private void attachSources(ActionEvent evt) {
        this.jButton1.setEnabled(false);
        RP.execute(new Runnable(){

            @Override
            public void run() {
                SourceJavadocAttacher.attachSources((URL)AttachSourcePanel.this.root, (SourceJavadocAttacher.AttachmentListener)new SourceJavadocAttacher.AttachmentListener(){

                    public void attachmentSucceeded() {
                        ClassPath cp;
                        FileObject newFileFo;
                        FileObject[] fos;
                        boolean success = false;
                        FileObject rootFo = URLMapper.findFileObject((URL)AttachSourcePanel.this.root);
                        FileObject fileFo = URLMapper.findFileObject((URL)AttachSourcePanel.this.file);
                        if (rootFo != null && fileFo != null && (fos = SourceForBinaryQuery.findSourceRoots((URL)AttachSourcePanel.this.root).getRoots()).length > 0 && (newFileFo = (cp = ClassPathSupport.createClassPath((FileObject[])fos)).findResource(AttachSourcePanel.this.binaryName + ".java")) != null) {
                            try {
                                EditorCookie ec = (EditorCookie)DataObject.find((FileObject)fileFo).getLookup().lookup(EditorCookie.class);
                                Openable open = (Openable)DataObject.find((FileObject)newFileFo).getLookup().lookup(Openable.class);
                                if (ec != null && open != null) {
                                    ec.close();
                                    open.open();
                                    success = true;
                                }
                            }
                            catch (DataObjectNotFoundException ex) {
                                Exceptions.printStackTrace((Throwable)ex);
                            }
                        }
                        if (!success) {
                            this.enableAttach();
                        }
                    }

                    public void attachmentFailed() {
                        this.enableAttach();
                    }

                    private void enableAttach() {
                        SwingUtilities.invokeLater(new Runnable(){

                            @Override
                            public void run() {
                                AttachSourcePanel.this.jButton1.setEnabled(true);
                            }
                        });
                    }

                });
            }

        });
    }

}

