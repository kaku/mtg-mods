/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.AnnotationTree
 *  com.sun.source.tree.AssignmentTree
 *  com.sun.source.tree.BlockTree
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ExpressionTree
 *  com.sun.source.tree.IdentifierTree
 *  com.sun.source.tree.ImportTree
 *  com.sun.source.tree.LiteralTree
 *  com.sun.source.tree.MemberSelectTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.NewArrayTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.TypeParameterTree
 *  com.sun.source.tree.VariableTree
 *  com.sun.tools.classfile.Attribute
 *  com.sun.tools.classfile.Attributes
 *  com.sun.tools.classfile.ClassFile
 *  com.sun.tools.classfile.Code_attribute
 *  com.sun.tools.classfile.ConstantPool
 *  com.sun.tools.classfile.ConstantPoolException
 *  com.sun.tools.classfile.Descriptor
 *  com.sun.tools.classfile.Instruction
 *  com.sun.tools.classfile.Method
 *  com.sun.tools.classfile.Opcode
 *  com.sun.tools.javac.api.JavacTaskImpl
 *  com.sun.tools.javac.code.Symbol
 *  com.sun.tools.javac.code.Symbol$ClassSymbol
 *  com.sun.tools.javac.util.Context
 *  com.sun.tools.javap.ClassWriter
 *  com.sun.tools.javap.CodeWriter
 *  com.sun.tools.javap.ConstantWriter
 *  com.sun.tools.javap.Context
 *  com.sun.tools.javap.Messages
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.java.classfile;

import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.NewArrayTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TypeParameterTree;
import com.sun.source.tree.VariableTree;
import com.sun.tools.classfile.Attribute;
import com.sun.tools.classfile.Attributes;
import com.sun.tools.classfile.ClassFile;
import com.sun.tools.classfile.Code_attribute;
import com.sun.tools.classfile.ConstantPool;
import com.sun.tools.classfile.ConstantPoolException;
import com.sun.tools.classfile.Descriptor;
import com.sun.tools.classfile.Instruction;
import com.sun.tools.classfile.Method;
import com.sun.tools.classfile.Opcode;
import com.sun.tools.javac.api.JavacTaskImpl;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javap.ClassWriter;
import com.sun.tools.javap.CodeWriter;
import com.sun.tools.javap.ConstantWriter;
import com.sun.tools.javap.Messages;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.AnnotationValueVisitor;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.AbstractElementVisitor6;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.JavaFileObject;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.Comment;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.ModificationResult;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.TreeMaker;
import org.netbeans.api.java.source.WorkingCopy;
import org.netbeans.modules.java.source.JavaSourceAccessor;
import org.netbeans.modules.java.source.builder.CommentHandlerService;
import org.netbeans.modules.java.source.indexing.JavaIndex;
import org.netbeans.modules.java.source.query.CommentSet;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;

public class CodeGenerator {
    private static final Logger LOG = Logger.getLogger(CodeGenerator.class.getName());
    private static final Set<ElementKind> UNUSABLE_KINDS = EnumSet.of(ElementKind.PACKAGE);
    private static final byte VERSION = 2;
    private static final String HASH_ATTRIBUTE_NAME = "origin-hash";
    private static final String DISABLE_ERRORS = "disable-java-errors";
    static final String CLASSFILE_ROOT = "classfile-root";
    static final String CLASSFILE_BINNAME = "classfile-binaryName";
    private static final Map<List<ElementKind>, Set<Modifier>> IMPLICIT_MODIFIERS = new HashMap<List<ElementKind>, Set<Modifier>>();

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static FileObject generateCode(final ClasspathInfo cpInfo, final ElementHandle<? extends Element> toOpenHandle) {
        if (UNUSABLE_KINDS.contains((Object)toOpenHandle.getKind())) {
            return null;
        }
        try {
            FileObject file = FileUtil.createMemoryFileSystem().getRoot().createData("test.java");
            OutputStream out = file.getOutputStream();
            try {
                FileUtil.copy((InputStream)new ByteArrayInputStream("".getBytes("UTF-8")), (OutputStream)out);
            }
            finally {
                out.close();
            }
            JavaSource js = JavaSource.create(cpInfo, file);
            final FileObject[] result = new FileObject[1];
            final boolean[] sourceGenerated = new boolean[1];
            final URL[] classfileRoot = new URL[1];
            final String[] binaryName = new String[1];
            ModificationResult r = js.runModificationTask(new Task<WorkingCopy>(){

                @Override
                public void run(WorkingCopy wc) throws Exception {
                    TypeElement te;
                    wc.toPhase(JavaSource.Phase.PARSED);
                    ClassPath cp = ClassPathSupport.createProxyClassPath((ClassPath[])new ClassPath[]{cpInfo.getClassPath(ClasspathInfo.PathKind.BOOT), cpInfo.getClassPath(ClasspathInfo.PathKind.COMPILE), cpInfo.getClassPath(ClasspathInfo.PathKind.SOURCE)});
                    Object toOpen = toOpenHandle.resolve(wc);
                    TypeElement typeElement = te = toOpen != null ? wc.getElementUtilities().outermostTypeElement((Element)toOpen) : null;
                    if (te == null) {
                        LOG.info("Cannot resolve element: " + toOpenHandle.toString() + " on classpath: " + cp.toString());
                        return;
                    }
                    String binName = te.getQualifiedName().toString().replace('.', '/');
                    String resourceName = binName + ".class";
                    FileObject resource = cp.findResource(resourceName);
                    if (resource == null) {
                        LOG.info("Cannot find resource: " + resourceName + " on classpath: " + cp.toString());
                        return;
                    }
                    FileObject root = cp.findOwnerRoot(resource);
                    if (root == null) {
                        LOG.info("Cannot find owner of: " + FileUtil.getFileDisplayName((FileObject)resource) + " on classpath: " + cp.toString());
                        return;
                    }
                    classfileRoot[0] = root.getURL();
                    binaryName[0] = binName;
                    File sourceRoot = new File(JavaIndex.getIndex(root.getURL()), "gensrc");
                    FileObject sourceRootFO = FileUtil.createFolder((File)sourceRoot);
                    if (sourceRootFO == null) {
                        LOG.info("Cannot create folder: " + sourceRoot);
                        return;
                    }
                    String path = binName + ".java";
                    FileObject source = sourceRootFO.getFileObject(path);
                    MessageDigest md = MessageDigest.getInstance("MD5");
                    md.update(2);
                    byte[] hashBytes = md.digest(resource.asBytes());
                    StringBuilder hashBuilder = new StringBuilder();
                    for (byte b : hashBytes) {
                        hashBuilder.append(String.format("%02X", Byte.valueOf(b)));
                    }
                    String hash = hashBuilder.toString();
                    if (source != null) {
                        result[0] = source;
                        String existingHash = (String)source.getAttribute("origin-hash");
                        if (hash.equals(existingHash)) {
                            LOG.fine(FileUtil.getFileDisplayName((FileObject)source) + " is up to date, reusing from cache.");
                            return;
                        }
                    }
                    CompilationUnitTree cut = CodeGenerator.generateCode(wc, te);
                    wc.rewrite((Tree)wc.getCompilationUnit(), (Tree)cut);
                    if (source == null) {
                        result[0] = FileUtil.createData((FileObject)sourceRootFO, (String)path);
                        LOG.fine(FileUtil.getFileDisplayName((FileObject)result[0]) + " does not exist, creating.");
                    } else {
                        LOG.fine(FileUtil.getFileDisplayName((FileObject)source) + " is not up to date, regenerating.");
                    }
                    result[0].setAttribute("origin-hash", (Object)hash);
                    sourceGenerated[0] = true;
                }
            });
            if (sourceGenerated[0]) {
                File resultFile = FileUtil.toFile((FileObject)result[0]);
                if (resultFile != null && !resultFile.canWrite()) {
                    resultFile.setWritable(true);
                }
                out = result[0].getOutputStream();
                try {
                    FileUtil.copy((InputStream)new ByteArrayInputStream(r.getResultingSource(file).getBytes("UTF-8")), (OutputStream)out);
                }
                finally {
                    out.close();
                }
                if (resultFile != null) {
                    resultFile.setReadOnly();
                    result[0].setAttribute("disable-java-errors", (Object)true);
                    result[0].setAttribute("classfile-root", (Object)classfileRoot[0]);
                    result[0].setAttribute("classfile-binaryName", (Object)binaryName[0]);
                }
            }
            return result[0];
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
            return null;
        }
    }

    static CompilationUnitTree generateCode(WorkingCopy wc, TypeElement te) {
        TreeMaker make = wc.getTreeMaker();
        Tree clazz = (Tree)new TreeBuilder(make, wc).visit(te);
        CompilationUnitTree cut = make.CompilationUnit((ExpressionTree)make.Identifier(((PackageElement)te.getEnclosingElement()).getQualifiedName()), Collections.emptyList(), Collections.singletonList(clazz), wc.getCompilationUnit().getSourceFile());
        return cut;
    }

    private CodeGenerator() {
    }

    static {
        IMPLICIT_MODIFIERS.put(Arrays.asList(new ElementKind[]{ElementKind.ENUM}), EnumSet.of(Modifier.STATIC, Modifier.ABSTRACT, Modifier.FINAL));
        IMPLICIT_MODIFIERS.put(Arrays.asList(new ElementKind[]{ElementKind.ANNOTATION_TYPE}), EnumSet.of(Modifier.STATIC, Modifier.ABSTRACT));
        IMPLICIT_MODIFIERS.put(Arrays.asList(new ElementKind[]{ElementKind.METHOD, ElementKind.ANNOTATION_TYPE}), EnumSet.of(Modifier.ABSTRACT));
        IMPLICIT_MODIFIERS.put(Arrays.asList(new ElementKind[]{ElementKind.METHOD, ElementKind.INTERFACE}), EnumSet.of(Modifier.ABSTRACT));
    }

    private static final class ConvenientCodeWriter
    extends CodeWriter {
        private final ConstantWriter constantWriter;
        private static final Set<Opcode> INSTRUCTION_WITH_REFERENCE = EnumSet.of(Opcode.LDC, new Opcode[]{Opcode.LDC_W, Opcode.LDC2_W, Opcode.GETSTATIC, Opcode.PUTSTATIC, Opcode.GETFIELD, Opcode.PUTFIELD, Opcode.INVOKEVIRTUAL, Opcode.INVOKESPECIAL, Opcode.INVOKESTATIC, Opcode.INVOKEINTERFACE, Opcode.NEW, Opcode.ANEWARRAY, Opcode.CHECKCAST, Opcode.INSTANCEOF});

        public ConvenientCodeWriter(com.sun.tools.javap.Context context) {
            super(context);
            this.constantWriter = ConstantWriter.instance((com.sun.tools.javap.Context)context);
        }

        public void writeInstr(Instruction instr) {
            if (INSTRUCTION_WITH_REFERENCE.contains((Object)instr.getOpcode())) {
                this.print(String.format("%4d: %-13s ", instr.getPC(), instr.getMnemonic()));
                int constantPoolEntry = instr.getOpcode() == Opcode.LDC ? instr.getUnsignedByte(1) : instr.getUnsignedShort(1);
                this.print(this.constantWriter.stringValue(constantPoolEntry));
                this.println();
            } else {
                super.writeInstr(instr);
            }
        }
    }

    private static final class TreeBuilder
    extends AbstractElementVisitor6<Tree, Void> {
        private final TreeMaker make;
        private final WorkingCopy wc;
        private ClassFile cf;
        private Map<String, Method> sig2Method;

        public TreeBuilder(TreeMaker make, WorkingCopy wc) {
            this.make = make;
            this.wc = wc;
        }

        @Override
        public Tree visitPackage(PackageElement e, Void p) {
            throw new UnsupportedOperationException("Not supported.");
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public Tree visitType(TypeElement e, Void p) {
            ClassFile oldCf = this.cf;
            Map<String, Method> oldMethods = this.sig2Method;
            this.cf = null;
            this.sig2Method = new HashMap<String, Method>();
            try {
                block21 : {
                    try {
                        JavaFileObject classfile = ((Symbol.ClassSymbol)e).classfile;
                        if (classfile == null || classfile.getKind() != JavaFileObject.Kind.CLASS) break block21;
                        InputStream in = classfile.openInputStream();
                        try {
                            this.cf = ClassFile.read((InputStream)in);
                            for (Method m : this.cf.methods) {
                                this.sig2Method.put(this.cf.constant_pool.getUTF8Value(m.name_index) + ":" + this.cf.constant_pool.getUTF8Value(m.descriptor.index), m);
                            }
                        }
                        finally {
                            in.close();
                        }
                    }
                    catch (ConstantPoolException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                    catch (IOException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
                LinkedList<Tree> members = new LinkedList<Tree>();
                for (Element m22 : e.getEnclosedElements()) {
                    Tree member = (Tree)this.visit(m22);
                    if (member == null) continue;
                    members.add(member);
                }
                ModifiersTree mods = this.computeMods(e);
                switch (e.getKind()) {
                    case CLASS: {
                        ClassTree m22 = this.addDeprecated(e, (T)this.make.Class(mods, e.getSimpleName(), this.constructTypeParams(e.getTypeParameters()), this.computeSuper(e.getSuperclass()), this.computeSuper(e.getInterfaces()), members));
                        return m22;
                    }
                    case INTERFACE: {
                        ClassTree m22 = this.addDeprecated(e, (T)this.make.Interface(mods, e.getSimpleName(), this.constructTypeParams(e.getTypeParameters()), this.computeSuper(e.getInterfaces()), members));
                        return m22;
                    }
                    case ENUM: {
                        ClassTree m22 = this.addDeprecated(e, (T)this.make.Enum(mods, e.getSimpleName(), this.computeSuper(e.getInterfaces()), members));
                        return m22;
                    }
                    case ANNOTATION_TYPE: {
                        ClassTree m22 = this.addDeprecated(e, (T)this.make.AnnotationType(mods, e.getSimpleName(), members));
                        return m22;
                    }
                }
                throw new UnsupportedOperationException();
            }
            finally {
                this.cf = oldCf;
                this.sig2Method = oldMethods;
            }
        }

        private ModifiersTree computeMods(Element e) {
            Set implicitModifiers = (Set)IMPLICIT_MODIFIERS.get(Arrays.asList(new ElementKind[]{e.getKind()}));
            if (implicitModifiers == null) {
                implicitModifiers = (Set)IMPLICIT_MODIFIERS.get(Arrays.asList(new ElementKind[]{e.getKind(), e.getEnclosingElement().getKind()}));
            }
            EnumSet<Modifier> modifiers = EnumSet.noneOf(Modifier.class);
            modifiers.addAll(e.getModifiers());
            if (implicitModifiers != null) {
                modifiers.removeAll(implicitModifiers);
            }
            LinkedList<AnnotationTree> annotations = new LinkedList<AnnotationTree>();
            for (AnnotationMirror m : e.getAnnotationMirrors()) {
                annotations.add(this.computeAnnotationTree(m));
            }
            return this.make.Modifiers(modifiers, annotations);
        }

        private <T extends Tree> T addDeprecated(Element e, T orig) {
            if (this.wc.getElements().isDeprecated(e)) {
                // empty if block
            }
            return orig;
        }

        private AnnotationTree computeAnnotationTree(AnnotationMirror am) {
            LinkedList<AssignmentTree> params = new LinkedList<AssignmentTree>();
            for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : am.getElementValues().entrySet()) {
                ExpressionTree val = this.createTreeForAnnotationValue(this.make, entry.getValue());
                if (val == null) {
                    LOG.log(Level.WARNING, "Cannot create annotation for: {0}", entry.getValue());
                    continue;
                }
                AssignmentTree vt = this.make.Assignment((ExpressionTree)this.make.Identifier(entry.getKey().getSimpleName()), val);
                params.add(vt);
            }
            return this.make.Annotation(this.make.Type(am.getAnnotationType()), params);
        }

        private Tree computeSuper(TypeMirror superClass) {
            if (superClass == null) {
                return null;
            }
            if (superClass.getKind() == TypeKind.NONE) {
                return null;
            }
            TypeElement jlObject = this.wc.getElements().getTypeElement("java.lang.Object");
            if (this.wc.getTypes().isSameType(superClass, jlObject.asType())) {
                return null;
            }
            return this.make.Type(superClass);
        }

        private List<Tree> computeSuper(List<? extends TypeMirror> superTypes) {
            LinkedList<Tree> sup = new LinkedList<Tree>();
            if (superTypes != null) {
                for (TypeMirror tm : superTypes) {
                    sup.add(this.make.Type(tm));
                }
            }
            return sup;
        }

        private List<? extends TypeParameterTree> constructTypeParams(List<? extends TypeParameterElement> params) {
            LinkedList<TypeParameterTree> result = new LinkedList<TypeParameterTree>();
            for (TypeParameterElement e : params) {
                result.add((TypeParameterTree)this.visit(e));
            }
            return result;
        }

        @Override
        public Tree visitVariable(VariableElement e, Void p) {
            if (e.getKind() == ElementKind.ENUM_CONSTANT) {
                int mods = 16384;
                ModifiersTree modifiers = this.make.Modifiers(mods, Collections.emptyList());
                return this.make.Variable(modifiers, e.getSimpleName().toString(), (Tree)this.make.Identifier(e.getEnclosingElement().getSimpleName().toString()), null);
            }
            ModifiersTree mods = this.computeMods(e);
            LiteralTree init = e.getConstantValue() != null ? this.make.Literal(e.getConstantValue()) : null;
            return this.addDeprecated(e, (T)this.make.Variable(mods, e.getSimpleName(), this.make.Type(e.asType()), (ExpressionTree)init));
        }

        @Override
        public Tree visitExecutable(ExecutableElement e, Void p) {
            Attribute code;
            if (e.getKind() == ElementKind.STATIC_INIT || e.getKind() == ElementKind.INSTANCE_INIT) {
                return null;
            }
            if (this.wc.getElementUtilities().isSynthetic(e)) {
                return null;
            }
            if (e.getEnclosingElement().getKind() == ElementKind.ENUM) {
                TypeMirror param;
                if ("values".equals(e.getSimpleName().toString()) && e.getParameters().isEmpty()) {
                    return null;
                }
                if ("valueOf".equals(e.getSimpleName().toString()) && e.getParameters().size() == 1 && (param = e.getParameters().get(0).asType()).getKind() == TypeKind.DECLARED && "java.lang.String".equals(((TypeElement)((DeclaredType)param).asElement()).getQualifiedName().toString())) {
                    return null;
                }
            }
            ModifiersTree mods = this.computeMods(e);
            Tree returnValue = e.getReturnType() != null ? this.make.Type(e.getReturnType()) : null;
            LinkedList<VariableTree> parameters = new LinkedList<VariableTree>();
            for (VariableElement param : e.getParameters()) {
                parameters.add((VariableTree)this.visit(param));
            }
            LinkedList<ExpressionTree> throwsList = new LinkedList<ExpressionTree>();
            for (TypeMirror t : e.getThrownTypes()) {
                throwsList.add((ExpressionTree)this.make.Type(t));
            }
            if (e.getModifiers().contains((Object)Modifier.ABSTRACT) || e.getModifiers().contains((Object)Modifier.NATIVE)) {
                ExpressionTree def = this.createTreeForAnnotationValue(this.make, e.getDefaultValue());
                return this.addDeprecated(e, (T)this.make.Method(mods, (CharSequence)e.getSimpleName(), returnValue, this.constructTypeParams(e.getTypeParameters()), parameters, throwsList, (BlockTree)null, def));
            }
            MethodTree method = this.make.Method(mods, (CharSequence)e.getSimpleName(), returnValue, this.constructTypeParams(e.getTypeParameters()), parameters, throwsList, "{ }", null);
            String[] signature = SourceUtils.getJVMSignature(ElementHandle.create(e));
            Method m = this.sig2Method.get(signature[1] + ":" + signature[2]);
            CommentHandlerService handler = CommentHandlerService.instance(JavaSourceAccessor.getINSTANCE().getJavacTask(this.wc).getContext());
            CommentSet set = handler.getComments((Tree)method.getBody());
            if (m != null && (code = m.attributes.get("Code")) instanceof Code_attribute) {
                com.sun.tools.javap.Context ctx = new com.sun.tools.javap.Context();
                StringWriter decompiled = new StringWriter();
                PrintWriter w = new PrintWriter(decompiled);
                ctx.put(PrintWriter.class, (Object)w);
                ctx.put(Messages.class, (Object)new Messages(){

                    public /* varargs */ String getMessage(String key, Object ... args) {
                        return "";
                    }

                    public /* varargs */ String getMessage(Locale locale, String key, Object ... args) {
                        return "";
                    }
                });
                ctx.put(ClassWriter.class, (Object)new ClassWriter(ctx){});
                ctx.put(CodeWriter.class, (Object)new ConvenientCodeWriter(ctx));
                CodeWriter codeWriter = CodeWriter.instance((com.sun.tools.javap.Context)ctx);
                codeWriter.writeInstrs((Code_attribute)code);
                codeWriter.writeExceptionTable((Code_attribute)code);
                w.println();
                w.close();
                set.addComment(CommentSet.RelativePosition.INNER, Comment.create(Comment.Style.LINE, "<editor-fold defaultstate=\"collapsed\" desc=\"Compiled Code\">"));
                set.addComment(CommentSet.RelativePosition.INNER, Comment.create(decompiled.toString()));
                set.addComment(CommentSet.RelativePosition.INNER, Comment.create(Comment.Style.LINE, "</editor-fold>"));
            }
            if (!set.hasComments()) {
                set.addComment(CommentSet.RelativePosition.INNER, Comment.create(Comment.Style.LINE, "compiled code"));
            }
            return this.addDeprecated(e, (T)method);
        }

        @Override
        public Tree visitTypeParameter(TypeParameterElement e, Void p) {
            LinkedList<ExpressionTree> bounds = new LinkedList<ExpressionTree>();
            for (TypeMirror b : e.getBounds()) {
                bounds.add((ExpressionTree)this.make.Type(b));
            }
            return this.make.TypeParameter(e.getSimpleName(), bounds);
        }

        private ExpressionTree createTreeForAnnotationValue(final TreeMaker make, AnnotationValue def) {
            if (def == null) {
                return null;
            }
            return (ExpressionTree)def.accept(new AnnotationValueVisitor<ExpressionTree, Void>(){

                @Override
                public ExpressionTree visit(AnnotationValue av, Void p) {
                    throw new UnsupportedOperationException("Not supported yet.");
                }

                @Override
                public ExpressionTree visit(AnnotationValue av) {
                    throw new UnsupportedOperationException("Not supported yet.");
                }

                @Override
                public ExpressionTree visitBoolean(boolean b, Void p) {
                    return make.Literal(b);
                }

                @Override
                public ExpressionTree visitByte(byte b, Void p) {
                    return make.Literal(Byte.valueOf(b));
                }

                @Override
                public ExpressionTree visitChar(char c, Void p) {
                    return make.Literal(Character.valueOf(c));
                }

                @Override
                public ExpressionTree visitDouble(double d, Void p) {
                    return make.Literal(d);
                }

                @Override
                public ExpressionTree visitFloat(float f, Void p) {
                    return make.Literal(Float.valueOf(f));
                }

                @Override
                public ExpressionTree visitInt(int i, Void p) {
                    return make.Literal(i);
                }

                @Override
                public ExpressionTree visitLong(long i, Void p) {
                    return make.Literal(i);
                }

                @Override
                public ExpressionTree visitShort(short s, Void p) {
                    return make.Literal(s);
                }

                @Override
                public ExpressionTree visitString(String s, Void p) {
                    return make.Literal(s);
                }

                @Override
                public ExpressionTree visitType(TypeMirror t, Void p) {
                    return make.MemberSelect((ExpressionTree)make.Type(t), "class");
                }

                @Override
                public ExpressionTree visitEnumConstant(VariableElement c, Void p) {
                    return make.QualIdent(c);
                }

                @Override
                public ExpressionTree visitAnnotation(AnnotationMirror a, Void p) {
                    return TreeBuilder.this.computeAnnotationTree(a);
                }

                @Override
                public ExpressionTree visitArray(List<? extends AnnotationValue> vals, Void p) {
                    LinkedList<ExpressionTree> values = new LinkedList<ExpressionTree>();
                    for (AnnotationValue v : vals) {
                        ExpressionTree val = TreeBuilder.this.createTreeForAnnotationValue(make, v);
                        if (val == null) {
                            LOG.log(Level.WARNING, "Cannot create annotation for: {0}", v);
                            continue;
                        }
                        values.add(val);
                    }
                    return make.NewArray(null, Collections.emptyList(), values);
                }

                @Override
                public ExpressionTree visitUnknown(AnnotationValue av, Void p) {
                    throw new UnsupportedOperationException("Not supported yet.");
                }
            }, null);
        }

    }

}

