/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.Trees
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.java.classfile;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.lang.model.element.Element;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.java.source.UiUtils;
import org.netbeans.modules.java.BinaryElementOpen;
import org.netbeans.modules.java.classfile.CodeGenerator;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public class BinaryElementOpenImpl
implements BinaryElementOpen {
    @Override
    public boolean open(ClasspathInfo cpInfo, final ElementHandle<? extends Element> toOpen, final AtomicBoolean cancel) {
        FileObject source = CodeGenerator.generateCode(cpInfo, toOpen);
        if (source != null) {
            final int[] pos = new int[]{-1};
            try {
                JavaSource.create(cpInfo, source).runUserActionTask(new Task<CompilationController>(){

                    @Override
                    public void run(CompilationController parameter) throws Exception {
                        if (cancel.get()) {
                            return;
                        }
                        parameter.toPhase(JavaSource.Phase.RESOLVED);
                        Object el = toOpen.resolve(parameter);
                        if (el == null) {
                            return;
                        }
                        TreePath p = parameter.getTrees().getPath(el);
                        if (p == null) {
                            return;
                        }
                        pos[0] = (int)parameter.getTrees().getSourcePositions().getStartPosition(p.getCompilationUnit(), p.getLeaf());
                    }
                }, true);
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            if (pos[0] != -1 && !cancel.get()) {
                return this.open(source, pos[0]);
            }
            return false;
        }
        return false;
    }

    private boolean open(FileObject source, int pos) {
        return UiUtils.open(source, pos);
    }

}

