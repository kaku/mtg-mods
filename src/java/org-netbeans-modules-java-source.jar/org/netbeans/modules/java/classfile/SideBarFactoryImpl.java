/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.SideBarFactory
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.loaders.DataObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.java.classfile;

import java.net.URL;
import javax.swing.JComponent;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.SideBarFactory;
import org.netbeans.modules.java.classfile.AttachSourcePanel;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;

public class SideBarFactoryImpl
implements SideBarFactory {
    public JComponent createSideBar(JTextComponent target) {
        Object binaryName;
        Object classFileRoot;
        Document doc = target.getDocument();
        Object origin = doc.getProperty("stream");
        FileObject originFile = origin instanceof DataObject ? ((DataObject)origin).getPrimaryFile() : (origin instanceof FileObject ? (FileObject)origin : null);
        if (originFile != null) {
            classFileRoot = originFile.getAttribute("classfile-root");
            binaryName = originFile.getAttribute("classfile-binaryName");
        } else {
            binaryName = null;
            classFileRoot = null;
        }
        if (classFileRoot instanceof URL && binaryName instanceof String) {
            try {
                return new AttachSourcePanel((URL)classFileRoot, originFile.getURL(), (String)binaryName);
            }
            catch (FileStateInvalidException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return null;
    }
}

