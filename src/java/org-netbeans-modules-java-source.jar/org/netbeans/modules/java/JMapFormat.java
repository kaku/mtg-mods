/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.MapFormat
 */
package org.netbeans.modules.java;

import java.util.Map;
import java.util.StringTokenizer;
import org.openide.util.MapFormat;

public class JMapFormat
extends MapFormat {
    private String cdel = ",";
    static final long serialVersionUID = 5503640004816201285L;

    public JMapFormat(Map map) {
        super(map);
    }

    protected Object processKey(String key) {
        StringTokenizer st = new StringTokenizer(key, this.cdel, true);
        String[] data = new String[4];
        int i = 0;
        while (i < 4 && st.hasMoreTokens()) {
            String temp = st.nextToken();
            if (temp.equals("$")) {
                ++i;
                continue;
            }
            data[i] = temp;
        }
        Object obj = super.processKey(data[0]);
        if (obj instanceof String && data[1] != null) {
            String name = (String)obj;
            if (name.endsWith(data[1])) {
                if (data[2] == null) {
                    data[2] = "";
                }
                return name.substring(0, name.length() - data[1].length()) + data[2];
            }
            if (data[3] != null) {
                return data[3];
            }
            return obj;
        }
        return obj;
    }

    public String getCondDelimiter() {
        return this.cdel;
    }

    public void setCondDelimiter(String cdel) {
        this.cdel = cdel;
    }
}

