/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.ModifiersTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.Tree$Kind
 *  com.sun.source.tree.TreeVisitor
 *  com.sun.source.util.TreeScanner
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.api.queries.FileBuiltQuery
 *  org.netbeans.api.queries.FileBuiltQuery$Status
 *  org.netbeans.modules.classfile.ClassFile
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$Status
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataNode
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 *  org.openide.nodes.PropertySupport
 *  org.openide.nodes.PropertySupport$ReadOnly
 *  org.openide.nodes.PropertySupport$ReadWrite
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.java;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TreeVisitor;
import com.sun.source.util.TreeScanner;
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.queries.FileBuiltQuery;
import org.netbeans.modules.classfile.ClassFile;
import org.netbeans.modules.java.source.usages.ExecutableFilesIndex;
import org.netbeans.spi.java.loaders.RenameHandler;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;

public final class JavaNode
extends DataNode
implements ChangeListener {
    private static final long serialVersionUID = -7396485743899766258L;
    private static final String JAVA_ICON_BASE = "org/netbeans/modules/java/resources/class.png";
    private static final String CLASS_ICON_BASE = "org/netbeans/modules/java/resources/clazz.gif";
    private static final String ABSTRACT_CLASS_ICON_BASE = "org/netbeans/modules/java/resources/abstract_class_file.png";
    private static final String INTERFACE_ICON_BASE = "org/netbeans/modules/java/resources/interface_file.png";
    private static final String ENUM_ICON_BASE = "org/netbeans/modules/java/resources/enum_file.png";
    private static final String ANNOTATION_ICON_BASE = "org/netbeans/modules/java/resources/annotation_file.png";
    private static final String EXECUTABLE_BADGE_URL = "org/netbeans/modules/java/resources/executable-badge.png";
    private static final String NEEDS_COMPILE_BADGE_URL = "org/netbeans/modules/java/resources/needs-compile.png";
    private static final Map<String, Image> IMAGE_CACHE = new ConcurrentHashMap<String, Image>();
    private static final boolean ALWAYS_PREFFER_COMPUTED_ICON = Boolean.getBoolean("JavaNode.prefferComputedIcon");
    private static final Logger LOG = Logger.getLogger(JavaNode.class.getName());
    private FileBuiltQuery.Status status;
    private final boolean isJavaSource;
    private final AtomicReference<Image> isCompiled;
    private final AtomicReference<Image> isExecutable;
    private final AtomicReference<Image> computedIcon;
    private final AtomicReference<FileChangeListener> computedIconListener;
    private ChangeListener executableListener;
    private Node.PropertySet[] propertySets;
    private static final RequestProcessor WORKER = new RequestProcessor("Java Node Badge Processor", 1, false, false);

    public JavaNode(final DataObject jdo, boolean isJavaSource) {
        super(jdo, Children.LEAF);
        this.isJavaSource = isJavaSource;
        this.isCompiled = new AtomicReference();
        this.isExecutable = new AtomicReference();
        this.computedIcon = new AtomicReference();
        this.computedIconListener = new AtomicReference();
        this.setIconBaseWithExtension(isJavaSource ? "org/netbeans/modules/java/resources/class.png" : "org/netbeans/modules/java/resources/clazz.gif");
        Logger.getLogger("TIMER").log(Level.FINE, "JavaNode", new Object[]{jdo.getPrimaryFile(), this});
        WORKER.post((Runnable)IconTask.create(this));
        if (isJavaSource) {
            WORKER.post((Runnable)new BuildStatusTask(this));
            WORKER.post((Runnable)new ExecutableTask(this));
            jdo.addPropertyChangeListener(new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if ("primaryFile".equals(evt.getPropertyName())) {
                        Logger.getLogger("TIMER").log(Level.FINE, "JavaNode", new Object[]{jdo.getPrimaryFile(), this});
                        WORKER.post(new Runnable(){

                            /*
                             * WARNING - Removed try catching itself - possible behaviour change.
                             */
                            @Override
                            public void run() {
                                JavaNode.this.computedIconListener.set(null);
                                JavaNode javaNode = JavaNode.this;
                                synchronized (javaNode) {
                                    JavaNode.this.status = null;
                                    JavaNode.this.executableListener = null;
                                    WORKER.post((Runnable)new BuildStatusTask(JavaNode.this));
                                    WORKER.post((Runnable)new ExecutableTask(JavaNode.this));
                                }
                            }
                        });
                    }
                }

            });
        }
    }

    public void setName(String name) {
        RenameHandler handler = JavaNode.getRenameHandler();
        if (handler == null) {
            super.setName(name);
        } else {
            try {
                handler.handleRename((Node)this, name);
            }
            catch (IllegalArgumentException ioe) {
                super.setName(name);
            }
        }
    }

    private static synchronized RenameHandler getRenameHandler() {
        Collection handlers = Lookup.getDefault().lookupAll(RenameHandler.class);
        if (handlers.size() == 0) {
            return null;
        }
        if (handlers.size() > 1) {
            LOG.warning("Multiple instances of RenameHandler found in Lookup; only using first one: " + handlers);
        }
        return (RenameHandler)handlers.iterator().next();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Node.PropertySet[] getPropertySets() {
        this.getSheet();
        JavaNode javaNode = this;
        synchronized (javaNode) {
            return Arrays.copyOf(this.propertySets, this.propertySets.length);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected final Sheet createSheet() {
        Sheet sheet = super.createSheet();
        if (JavaNode.getRenameHandler() != null) {
            sheet.get("properties").put(this.createNameProperty());
        }
        Sheet.Set ps = new Sheet.Set();
        ps.setName("classpaths");
        ps.setDisplayName(NbBundle.getMessage(JavaNode.class, (String)"LBL_JavaNode_sheet_classpaths"));
        ps.setShortDescription(NbBundle.getMessage(JavaNode.class, (String)"HINT_JavaNode_sheet_classpaths"));
        ps.put(new Node.Property[]{new ClasspathProperty("classpath/compile", NbBundle.getMessage(JavaNode.class, (String)"PROP_JavaNode_compile_classpath"), NbBundle.getMessage(JavaNode.class, (String)"HINT_JavaNode_compile_classpath")), new ClasspathProperty("classpath/execute", NbBundle.getMessage(JavaNode.class, (String)"PROP_JavaNode_execute_classpath"), NbBundle.getMessage(JavaNode.class, (String)"HINT_JavaNode_execute_classpath")), new ClasspathProperty("classpath/boot", NbBundle.getMessage(JavaNode.class, (String)"PROP_JavaNode_boot_classpath"), NbBundle.getMessage(JavaNode.class, (String)"HINT_JavaNode_boot_classpath"))});
        sheet.put(ps);
        Node.PropertySet[] propertySets = sheet.toArray();
        JavaNode javaNode = this;
        synchronized (javaNode) {
            this.propertySets = propertySets;
        }
        return sheet;
    }

    private Node.Property createNameProperty() {
        PropertySupport.ReadWrite<String> p = new PropertySupport.ReadWrite<String>("name", String.class, NbBundle.getMessage(DataObject.class, (String)"PROP_name"), NbBundle.getMessage(DataObject.class, (String)"HINT_name")){

            public String getValue() {
                return JavaNode.this.getName();
            }

            public Object getValue(String key) {
                if ("suppressCustomEditor".equals(key)) {
                    return Boolean.TRUE;
                }
                return PropertySupport.ReadWrite.super.getValue(key);
            }

            public void setValue(String val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
                if (!this.canWrite()) {
                    throw new IllegalAccessException();
                }
                JavaNode.this.setName(val);
            }

            public boolean canWrite() {
                return JavaNode.this.canRename();
            }
        };
        return p;
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        WORKER.post((Runnable)new BuildStatusTask(this));
    }

    public Image getIcon(int type) {
        Image i = this.prefferImage(this.computedIcon.get(), super.getIcon(type), type);
        return this.enhanceIcon(i);
    }

    public Image getOpenedIcon(int type) {
        Image i = super.getOpenedIcon(type);
        return this.enhanceIcon(i);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private Image prefferImage(Image computed, Image parent, int type) {
        Object attrValue;
        if (computed == null) {
            return parent;
        }
        if (!ALWAYS_PREFFER_COMPUTED_ICON && (attrValue = parent.getProperty("url", null)) instanceof URL) {
            String url = attrValue.toString();
            if (this.isJavaSource) {
                if (!url.endsWith("org/netbeans/modules/java/resources/class.png")) return parent;
            } else if (!url.endsWith("org/netbeans/modules/java/resources/clazz.gif")) {
                return parent;
            }
        }
        try {
            FileObject fo = this.getDataObject().getPrimaryFile();
            return fo.getFileSystem().getStatus().annotateIcon(computed, type, Collections.singleton(fo));
        }
        catch (FileStateInvalidException e) {
            // empty catch block
        }
        return computed;
    }

    private Image enhanceIcon(Image i) {
        Image executable;
        Image needsCompile = this.isCompiled.get();
        if (needsCompile != null) {
            i = ImageUtilities.mergeImages((Image)i, (Image)needsCompile, (int)16, (int)0);
        }
        if ((executable = this.isExecutable.get()) != null) {
            i = ImageUtilities.mergeImages((Image)i, (Image)executable, (int)10, (int)6);
        }
        return i;
    }

    private static Image getImage(@NonNull String resourceId, @NullAllowed String annotationTemplate) {
        Image result = IMAGE_CACHE.get(resourceId);
        if (result == null) {
            result = ImageUtilities.loadImage((String)resourceId, (boolean)true);
            if (annotationTemplate != null) {
                URL resourceURL = JavaNode.class.getClassLoader().getResource(resourceId);
                String annotation = MessageFormat.format(annotationTemplate, resourceURL);
                result = ImageUtilities.assignToolTipToImage((Image)result, (String)annotation);
            }
            IMAGE_CACHE.put(resourceId, result);
        }
        return result;
    }

    private static class ExecutableTask
    implements Runnable {
        private final JavaNode node;

        public ExecutableTask(JavaNode node) {
            this.node = node;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            FileObject root;
            ClassPath cp;
            ChangeListener _executableListener;
            JavaNode javaNode = this.node;
            synchronized (javaNode) {
                _executableListener = this.node.executableListener;
            }
            FileObject file = this.node.getDataObject().getPrimaryFile();
            if (_executableListener == null) {
                _executableListener = new ChangeListener(){

                    @Override
                    public void stateChanged(ChangeEvent e) {
                        WORKER.post((Runnable)new ExecutableTask(ExecutableTask.this.node));
                    }
                };
                try {
                    ExecutableFilesIndex.DEFAULT.addChangeListener(file.getURL(), _executableListener);
                }
                catch (FileStateInvalidException ex2) {
                    Exceptions.printStackTrace((Throwable)ex2);
                }
                JavaNode ex2 = this.node;
                synchronized (ex2) {
                    if (this.node.executableListener == null) {
                        this.node.executableListener = _executableListener;
                    }
                }
            }
            FileObject fileObject = root = (cp = ClassPath.getClassPath((FileObject)file, (String)"classpath/source")) != null ? cp.findOwnerRoot(file) : null;
            if (root != null) {
                boolean oldIsExecutable;
                boolean newIsExecutable;
                boolean bl = oldIsExecutable = this.node.isExecutable.getAndSet((newIsExecutable = ExecutableFilesIndex.DEFAULT.isMainClass(root.toURL(), file.toURL())) ? JavaNode.getImage("org/netbeans/modules/java/resources/executable-badge.png", "<img src=\"{0}\">&nbsp;" + NbBundle.getMessage(JavaNode.class, (String)"TP_ExecutableBadge")) : null) != null;
                if (newIsExecutable != oldIsExecutable) {
                    this.node.fireIconChange();
                    this.node.fireOpenedIconChange();
                }
            }
        }

    }

    private static class BuildStatusTask
    implements Runnable {
        private JavaNode node;

        public BuildStatusTask(JavaNode node) {
            this.node = node;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            boolean oldIsCompiled;
            FileBuiltQuery.Status _status = null;
            JavaNode javaNode = this.node;
            synchronized (javaNode) {
                _status = this.node.status;
            }
            if (_status == null) {
                FileObject jf = this.node.getDataObject().getPrimaryFile();
                _status = FileBuiltQuery.getStatus((FileObject)jf);
                JavaNode javaNode2 = this.node;
                synchronized (javaNode2) {
                    if (_status != null && this.node.status == null) {
                        this.node.status = _status;
                        this.node.status.addChangeListener(WeakListeners.change((ChangeListener)this.node, (Object)this.node.status));
                    }
                }
            }
            boolean isPackageInfo = "package-info.java".equals(this.node.getDataObject().getPrimaryFile().getNameExt());
            boolean newIsCompiled = _status != null && !isPackageInfo ? _status.isBuilt() : true;
            boolean bl = oldIsCompiled = this.node.isCompiled.getAndSet(newIsCompiled ? null : JavaNode.getImage("org/netbeans/modules/java/resources/needs-compile.png", "<img src=\"{0}\">&nbsp;" + NbBundle.getMessage(JavaNode.class, (String)"TP_NeedsCompileBadge"))) == null;
            if (newIsCompiled != oldIsCompiled) {
                this.node.fireIconChange();
                this.node.fireOpenedIconChange();
            }
        }
    }

    private static abstract class IconTask
    implements Runnable {
        protected final JavaNode node;

        IconTask(@NonNull JavaNode node) {
            this.node = node;
        }

        @CheckForNull
        abstract String computeIcon(@NonNull FileObject var1);

        @Override
        public final void run() {
            String res = null;
            FileObject file = this.node.getDataObject().getPrimaryFile();
            if (file != null) {
                if (this.node.computedIconListener.get() == null) {
                    FCL l = new FCL(this.node);
                    if (this.node.computedIconListener.compareAndSet(null, l)) {
                        file.addFileChangeListener(FileUtil.weakFileChangeListener((FileChangeListener)l, (Object)file));
                    }
                }
                res = this.computeIcon(file);
            }
            if (res == null) {
                res = this.node.isJavaSource ? "org/netbeans/modules/java/resources/class.png" : "org/netbeans/modules/java/resources/clazz.gif";
            }
            this.node.computedIcon.set(JavaNode.getImage(res, null));
            this.node.fireIconChange();
            this.node.fireOpenedIconChange();
        }

        static IconTask create(@NonNull JavaNode node) {
            return node.isJavaSource ? new SourceIcon(node) : new ClassIcon(node);
        }

        private static final class ClassIcon
        extends IconTask {
            private ClassIcon(@NonNull JavaNode node) {
                super(node);
            }

            @CheckForNull
            @Override
            String computeIcon(@NonNull FileObject file) {
                String res = null;
                try {
                    InputStream in = file.getInputStream();
                    Throwable throwable = null;
                    try {
                        ClassFile cf = new ClassFile(in, false);
                        res = cf.isEnum() ? "org/netbeans/modules/java/resources/enum_file.png" : (cf.isAnnotation() ? "org/netbeans/modules/java/resources/annotation_file.png" : ((cf.getAccess() & 512) == 512 ? "org/netbeans/modules/java/resources/interface_file.png" : ((cf.getAccess() & 1024) == 1024 ? "org/netbeans/modules/java/resources/abstract_class_file.png" : "org/netbeans/modules/java/resources/clazz.gif")));
                    }
                    catch (Throwable x2) {
                        throwable = x2;
                        throw x2;
                    }
                    finally {
                        if (in != null) {
                            if (throwable != null) {
                                try {
                                    in.close();
                                }
                                catch (Throwable x2) {
                                    throwable.addSuppressed(x2);
                                }
                            } else {
                                in.close();
                            }
                        }
                    }
                }
                catch (IOException e) {
                    Exceptions.printStackTrace((Throwable)e);
                }
                return res;
            }
        }

        private static final class SourceIcon
        extends IconTask {
            private SourceIcon(@NonNull JavaNode node) {
                super(node);
            }

            @Override
            String computeIcon(final @NonNull FileObject file) {
                final String[] res = new String[1];
                JavaSource src = JavaSource.forFileObject(file);
                if (src != null) {
                    try {
                        src.runUserActionTask(new Task<CompilationController>(){

                            @Override
                            public void run(CompilationController cc) throws Exception {
                                cc.toPhase(JavaSource.Phase.PARSED);
                                CompilationUnitTree cu = cc.getCompilationUnit();
                                Collection topTypes = (Collection)cu.accept((TreeVisitor)new ClasssFinder(), new ArrayList());
                                for (ClassTree ct : topTypes) {
                                    switch (ct.getKind()) {
                                        case CLASS: {
                                            res[0] = ct.getModifiers().getFlags().contains((Object)Modifier.ABSTRACT) ? "org/netbeans/modules/java/resources/abstract_class_file.png" : "org/netbeans/modules/java/resources/class.png";
                                            break;
                                        }
                                        case INTERFACE: {
                                            res[0] = "org/netbeans/modules/java/resources/interface_file.png";
                                            break;
                                        }
                                        case ENUM: {
                                            res[0] = "org/netbeans/modules/java/resources/enum_file.png";
                                            break;
                                        }
                                        case ANNOTATION_TYPE: {
                                            res[0] = "org/netbeans/modules/java/resources/annotation_file.png";
                                        }
                                    }
                                    if (!file.getName().contentEquals(ct.getSimpleName())) continue;
                                    break;
                                }
                            }
                        }, true);
                    }
                    catch (IOException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
                return res[0];
            }

            private static final class ClasssFinder
            extends TreeScanner<Collection<ClassTree>, Collection<ClassTree>> {
                private ClasssFinder() {
                }

                public Collection<ClassTree> visitCompilationUnit(CompilationUnitTree node, Collection<ClassTree> p) {
                    TreeScanner.super.visitCompilationUnit(node, p);
                    return p;
                }

                public Collection<ClassTree> visitClass(ClassTree node, Collection<ClassTree> p) {
                    p.add(node);
                    return p;
                }
            }

        }

        private static final class FCL
        extends FileChangeAdapter {
            private final JavaNode node;

            FCL(@NonNull JavaNode node) {
                this.node = node;
            }

            public void fileChanged(FileEvent fe) {
                WORKER.post((Runnable)IconTask.create(this.node));
            }
        }

    }

    private final class ClasspathProperty
    extends PropertySupport.ReadOnly<String> {
        private final String id;

        public ClasspathProperty(String id, String displayName, String shortDescription) {
            super(id, String.class, displayName, shortDescription);
            this.id = id;
            this.setValue("oneline", (Object)false);
        }

        public String getValue() {
            ClassPath cp = ClassPath.getClassPath((FileObject)JavaNode.this.getDataObject().getPrimaryFile(), (String)this.id);
            if (cp != null) {
                StringBuffer sb = new StringBuffer();
                for (ClassPath.Entry entry : cp.entries()) {
                    URL u = entry.getURL();
                    String item = u.toExternalForm();
                    if (u.getProtocol().equals("file")) {
                        item = Utilities.toFile((URI)URI.create(item)).getAbsolutePath();
                    } else if (u.getProtocol().equals("jar") && item.endsWith("!/")) {
                        URL embedded = FileUtil.getArchiveFile((URL)u);
                        assert (embedded != null);
                        if (embedded.getProtocol().equals("file")) {
                            item = Utilities.toFile((URI)URI.create(embedded.toExternalForm())).getAbsolutePath();
                        }
                    }
                    if (sb.length() > 0) {
                        sb.append(File.pathSeparatorChar);
                    }
                    sb.append(item);
                }
                return sb.toString();
            }
            return NbBundle.getMessage(JavaNode.class, (String)"LBL_JavaNode_classpath_unknown");
        }
    }

}

