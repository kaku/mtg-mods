/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.loaders.MultiFileLoader
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 */
package org.netbeans.modules.java;

import java.awt.Image;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.beans.SimpleBeanInfo;
import org.openide.loaders.MultiFileLoader;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;

public final class JavaDataLoaderBeanInfo
extends SimpleBeanInfo {
    @Override
    public BeanInfo[] getAdditionalBeanInfo() {
        try {
            return new BeanInfo[]{Introspector.getBeanInfo(MultiFileLoader.class)};
        }
        catch (IntrospectionException ie) {
            Exceptions.printStackTrace((Throwable)ie);
            return null;
        }
    }

    @Override
    public Image getIcon(int type) {
        if (type == 1 || type == 3) {
            return ImageUtilities.loadImage((String)"org/netbeans/modules/java/resources/class.png");
        }
        return ImageUtilities.loadImage((String)"org/netbeans/modules/java/resources/class32.gif");
    }

    @Override
    public PropertyDescriptor[] getPropertyDescriptors() {
        return new PropertyDescriptor[0];
    }
}

