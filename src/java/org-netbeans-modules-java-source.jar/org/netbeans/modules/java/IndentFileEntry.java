/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.queries.FileEncodingQuery
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.FileEntry
 *  org.openide.loaders.FileEntry$Format
 *  org.openide.loaders.MultiDataObject
 *  org.openide.text.FilterDocument
 *  org.openide.text.IndentEngine
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.text.Format;
import javax.swing.JEditorPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.StyledDocument;
import org.netbeans.api.queries.FileEncodingQuery;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.FileEntry;
import org.openide.loaders.MultiDataObject;
import org.openide.text.FilterDocument;
import org.openide.text.IndentEngine;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public abstract class IndentFileEntry
extends FileEntry.Format {
    private static final String NEWLINE = "\n";
    private static final String EA_PREFORMATTED = "org-netbeans-modules-java-preformattedSource";
    private ThreadLocal indentEngine;
    private static final String MAGIC_PREFIX = "//GEN-";

    IndentFileEntry(MultiDataObject dobj, FileObject file) {
        super(dobj, file);
    }

    private EditorKit createEditorKit(String mimeType) {
        EditorKit kit = JEditorPane.createEditorKitForContentType(mimeType);
        if (kit == null) {
            kit = new DefaultEditorKit();
        }
        return kit;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void setIndentEngine(IndentEngine engine) {
        IndentFileEntry indentFileEntry = this;
        synchronized (indentFileEntry) {
            if (this.indentEngine == null) {
                this.indentEngine = new ThreadLocal();
            }
        }
        this.indentEngine.set(engine);
    }

    final void initializeIndentEngine() {
        StyledDocument doc = this.createDocument(this.createEditorKit(this.getFile().getMIMEType()));
        IndentEngine engine = IndentEngine.find((Document)doc);
        this.setIndentEngine(engine);
    }

    private StyledDocument createDocument(EditorKit kit) {
        Document doc = kit.createDefaultDocument();
        if (doc instanceof StyledDocument) {
            return (StyledDocument)doc;
        }
        return new FilterDocument(doc);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public FileObject createFromTemplate(FileObject f, String name) throws IOException {
        String ext = this.getFile().getExt();
        if (name == null) {
            name = FileUtil.findFreeFileName((FileObject)f, (String)this.getFile().getName(), (String)ext);
        }
        FileObject fo = f.createData(name, ext);
        Format frm = this.createFormat(f, name, ext);
        InputStream is = this.getFile().getInputStream();
        Charset encoding = FileEncodingQuery.getEncoding((FileObject)this.getFile());
        InputStreamReader reader = new InputStreamReader(is, encoding);
        BufferedReader r = new BufferedReader(reader);
        StyledDocument doc = this.createDocument(this.createEditorKit(fo.getMIMEType()));
        IndentEngine eng = (IndentEngine)this.indentEngine.get();
        if (eng == null) {
            eng = IndentEngine.find((Document)doc);
        }
        Object attr = this.getFile().getAttribute("org-netbeans-modules-java-preformattedSource");
        boolean preformatted = false;
        if (attr != null && attr instanceof Boolean) {
            preformatted = (Boolean)attr;
        }
        try {
            FileLock lock = fo.lock();
            try {
                encoding = FileEncodingQuery.getEncoding((FileObject)fo);
                OutputStream os = fo.getOutputStream(lock);
                OutputStreamWriter w = new OutputStreamWriter(os, encoding);
                try {
                    String current;
                    String line = null;
                    int offset = 0;
                    while ((current = r.readLine()) != null) {
                        if (line != null) {
                            doc.insertString(offset, "\n", null);
                            ++offset;
                        }
                        line = frm.format(current);
                        if (!preformatted || !line.equals(current)) {
                            line = IndentFileEntry.fixupGuardedBlocks(IndentFileEntry.safeIndent(eng, line, doc, offset));
                        }
                        doc.insertString(offset, line, null);
                        offset += line.length();
                    }
                    doc.insertString(doc.getLength(), "\n", null);
                    w.write(doc.getText(0, doc.getLength()));
                }
                catch (BadLocationException e) {}
                finally {
                    w.close();
                }
            }
            finally {
                lock.releaseLock();
            }
        }
        finally {
            r.close();
        }
        FileUtil.copyAttributes((FileObject)this.getFile(), (FileObject)fo);
        fo.setAttribute("template", (Object)null);
        return fo;
    }

    static String fixupGuardedBlocks(String indentedLine) {
        int offset = indentedLine.indexOf("//GEN-");
        if (offset == -1) {
            return indentedLine;
        }
        int firstLineEnd = indentedLine.indexOf(10);
        if (firstLineEnd == -1 || firstLineEnd > offset) {
            return indentedLine;
        }
        int guardedLineEnd = indentedLine.indexOf(10, offset);
        StringBuffer sb = new StringBuffer(indentedLine.length());
        sb.append(indentedLine.substring(0, firstLineEnd));
        if (guardedLineEnd != -1) {
            sb.append(indentedLine.substring(offset, guardedLineEnd));
        } else {
            sb.append(indentedLine.substring(offset));
        }
        sb.append(indentedLine.substring(firstLineEnd, offset));
        if (guardedLineEnd != -1) {
            sb.append(indentedLine.substring(guardedLineEnd));
        }
        return sb.toString();
    }

    public static String safeIndent(IndentEngine engine, String text, StyledDocument doc, int offset) {
        if (engine == null) {
            return text;
        }
        try {
            StringWriter writer = new StringWriter();
            Writer indentator = engine.createWriter((Document)doc, offset, (Writer)writer);
            indentator.write(text);
            indentator.close();
            return writer.toString();
        }
        catch (Exception ex) {
            Exceptions.printStackTrace((Throwable)Exceptions.attachMessage((Throwable)ex, (String)NbBundle.getMessage(IndentFileEntry.class, (String)"EXMSG_IndentationEngineError")));
            return text;
        }
    }
}

