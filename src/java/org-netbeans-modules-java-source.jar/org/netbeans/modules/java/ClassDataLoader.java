/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObjectExistsException
 *  org.openide.loaders.ExtensionList
 *  org.openide.loaders.MultiDataObject
 *  org.openide.loaders.MultiFileLoader
 *  org.openide.loaders.UniFileLoader
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java;

import java.io.IOException;
import org.netbeans.modules.java.ClassDataObject;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.ExtensionList;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.loaders.UniFileLoader;
import org.openide.util.NbBundle;

public final class ClassDataLoader
extends UniFileLoader {
    public static final String CLASS_EXTENSION = "class";

    protected ClassDataLoader() {
        super("org.netbeans.modules.java.ClassDataObject");
        this.getExtensions().addExtension("class");
    }

    protected String actionsContext() {
        return "Loaders/application/x-class-file/Actions/";
    }

    protected MultiDataObject createMultiObject(FileObject primaryFile) throws DataObjectExistsException, IOException {
        if (primaryFile.getExt().equals("class")) {
            return new ClassDataObject(primaryFile, (MultiFileLoader)this);
        }
        return null;
    }

    protected String defaultDisplayName() {
        return NbBundle.getMessage(ClassDataLoader.class, (String)"PROP_ClassLoader_Name");
    }
}

