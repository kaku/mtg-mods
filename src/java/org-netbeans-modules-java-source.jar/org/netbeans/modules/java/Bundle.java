/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String ClassResolver() {
        return NbBundle.getMessage(Bundle.class, (String)"ClassResolver");
    }

    static String JavaResolver_FileChooserName() {
        return NbBundle.getMessage(Bundle.class, (String)"JavaResolver.FileChooserName");
    }

    static String JavaResolver_Name() {
        return NbBundle.getMessage(Bundle.class, (String)"JavaResolver.Name");
    }

    private void Bundle() {
    }
}

