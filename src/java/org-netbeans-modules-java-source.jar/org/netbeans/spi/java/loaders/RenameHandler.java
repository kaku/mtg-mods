/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 */
package org.netbeans.spi.java.loaders;

import org.openide.nodes.Node;

public interface RenameHandler {
    public void handleRename(Node var1, String var2) throws IllegalArgumentException;
}

