/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$InstantiatingIterator
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.java.platform;

import org.netbeans.spi.java.platform.GeneralPlatformInstall;
import org.openide.WizardDescriptor;
import org.openide.filesystems.FileObject;

public abstract class PlatformInstall
extends GeneralPlatformInstall {
    public abstract WizardDescriptor.InstantiatingIterator<WizardDescriptor> createIterator(FileObject var1);

    public abstract boolean accept(FileObject var1);
}

