/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$InstantiatingIterator
 */
package org.netbeans.spi.java.platform;

import org.netbeans.spi.java.platform.GeneralPlatformInstall;
import org.openide.WizardDescriptor;

public abstract class CustomPlatformInstall
extends GeneralPlatformInstall {
    public abstract WizardDescriptor.InstantiatingIterator<WizardDescriptor> createIterator();
}

