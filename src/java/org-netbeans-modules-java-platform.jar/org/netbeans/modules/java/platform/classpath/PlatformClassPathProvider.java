/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.spi.java.classpath.ClassPathProvider
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package org.netbeans.modules.java.platform.classpath;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.api.java.platform.JavaPlatformManager;
import org.netbeans.spi.java.classpath.ClassPathProvider;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public class PlatformClassPathProvider
implements ClassPathProvider {
    private static final Set<? extends String> SUPPORTED_CLASS_PATH_TYPES = new HashSet<String>(Arrays.asList("classpath/source", "classpath/boot", "classpath/compile"));
    private FileObject lastUsedRoot;
    private JavaPlatform lastUsedPlatform;
    private ClassPath emptyCp;

    public ClassPath findClassPath(FileObject fo, String type) {
        JavaPlatform[] platforms;
        if (!SUPPORTED_CLASS_PATH_TYPES.contains(type)) {
            return null;
        }
        if (fo == null || type == null) {
            throw new IllegalArgumentException();
        }
        JavaPlatform lp = this.getLastUsedPlatform(fo);
        if (lp != null) {
            platforms = new JavaPlatform[]{lp};
        } else {
            JavaPlatformManager manager = JavaPlatformManager.getDefault();
            platforms = manager.getInstalledPlatforms();
        }
        for (int i = 0; i < platforms.length; ++i) {
            ClassPath bootClassPath = platforms[i].getBootstrapLibraries();
            ClassPath libraryPath = platforms[i].getStandardLibraries();
            ClassPath sourcePath = platforms[i].getSourceFolders();
            FileObject root = null;
            if ("classpath/source".equals(type) && sourcePath != null && (root = sourcePath.findOwnerRoot(fo)) != null) {
                this.setLastUsedPlatform(root, platforms[i]);
                return sourcePath;
            }
            if ("classpath/boot".equals(type) && (bootClassPath != null && (root = bootClassPath.findOwnerRoot(fo)) != null || sourcePath != null && (root = sourcePath.findOwnerRoot(fo)) != null || libraryPath != null && (root = libraryPath.findOwnerRoot(fo)) != null)) {
                this.setLastUsedPlatform(root, platforms[i]);
                return bootClassPath;
            }
            if (!"classpath/compile".equals(type)) continue;
            if (libraryPath != null && (root = libraryPath.findOwnerRoot(fo)) != null) {
                this.setLastUsedPlatform(root, platforms[i]);
                return libraryPath;
            }
            if ((bootClassPath == null || (root = bootClassPath.findOwnerRoot(fo)) == null) && (sourcePath == null || (root = sourcePath.findOwnerRoot(fo)) == null)) continue;
            return this.getEmptyClassPath();
        }
        return null;
    }

    private synchronized ClassPath getEmptyClassPath() {
        if (this.emptyCp == null) {
            this.emptyCp = ClassPathSupport.createClassPath(Collections.emptyList());
        }
        return this.emptyCp;
    }

    private synchronized void setLastUsedPlatform(FileObject root, JavaPlatform platform) {
        this.lastUsedRoot = root;
        this.lastUsedPlatform = platform;
    }

    private synchronized JavaPlatform getLastUsedPlatform(FileObject file) {
        if (this.lastUsedRoot != null && FileUtil.isParentOf((FileObject)this.lastUsedRoot, (FileObject)file)) {
            return this.lastUsedPlatform;
        }
        return null;
    }
}

