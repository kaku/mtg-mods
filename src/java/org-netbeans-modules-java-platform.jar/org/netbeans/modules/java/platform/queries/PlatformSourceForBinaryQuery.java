/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.api.java.queries.SourceForBinaryQuery
 *  org.netbeans.api.java.queries.SourceForBinaryQuery$Result
 *  org.netbeans.spi.java.queries.SourceForBinaryQueryImplementation2
 *  org.netbeans.spi.java.queries.SourceForBinaryQueryImplementation2$Result
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Exceptions
 *  org.openide.util.Parameters
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.java.platform.queries;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.api.java.platform.JavaPlatformManager;
import org.netbeans.api.java.queries.SourceForBinaryQuery;
import org.netbeans.spi.java.queries.SourceForBinaryQueryImplementation2;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.ChangeSupport;
import org.openide.util.Exceptions;
import org.openide.util.Parameters;
import org.openide.util.WeakListeners;

public class PlatformSourceForBinaryQuery
implements SourceForBinaryQueryImplementation2 {
    private static final String JAR_FILE = "jar:file:";
    private static final String RTJAR_PATH = "/jre/lib/rt.jar!/";
    private static final String SRC_ZIP = "/src.zip";
    private Map<URL, SourceForBinaryQueryImplementation2.Result> cache = new HashMap<URL, SourceForBinaryQueryImplementation2.Result>();

    public SourceForBinaryQueryImplementation2.Result findSourceRoots2(URL binaryRoot) {
        SourceForBinaryQueryImplementation2.Result res = this.cache.get(binaryRoot);
        if (res != null) {
            return res;
        }
        JavaPlatformManager jpm = JavaPlatformManager.getDefault();
        ArrayDeque<JavaPlatform> candidates = new ArrayDeque<JavaPlatform>();
        for (JavaPlatform platform : jpm.getInstalledPlatforms()) {
            if (!PlatformSourceForBinaryQuery.contains(platform, binaryRoot)) continue;
            candidates.add(platform);
        }
        if (!candidates.isEmpty()) {
            res = new Result(jpm, binaryRoot, candidates);
            this.cache.put(binaryRoot, res);
            return res;
        }
        String binaryRootS = binaryRoot.toExternalForm();
        if (binaryRootS.startsWith("jar:file:") && binaryRootS.endsWith("/jre/lib/rt.jar!/")) {
            String srcZipS = binaryRootS.substring(4, binaryRootS.length() - "/jre/lib/rt.jar!/".length()) + "/src.zip";
            try {
                URL srcZip = FileUtil.getArchiveRoot((URL)new URL(srcZipS));
                FileObject fo = URLMapper.findFileObject((URL)srcZip);
                if (fo != null) {
                    return new UnregisteredPlatformResult(fo);
                }
            }
            catch (MalformedURLException mue) {
                Exceptions.printStackTrace((Throwable)mue);
            }
        }
        return null;
    }

    public SourceForBinaryQuery.Result findSourceRoots(URL binaryRoot) {
        return this.findSourceRoots2(binaryRoot);
    }

    static boolean contains(@NonNull JavaPlatform platform, @NonNull URL artifact) {
        for (ClassPath.Entry entry : platform.getBootstrapLibraries().entries()) {
            if (!entry.getURL().equals(artifact)) continue;
            return true;
        }
        return false;
    }

    private static class UnregisteredPlatformResult
    implements SourceForBinaryQueryImplementation2.Result {
        private final FileObject srcRoot;

        private UnregisteredPlatformResult(FileObject fo) {
            Parameters.notNull((CharSequence)"fo", (Object)fo);
            this.srcRoot = fo;
        }

        public FileObject[] getRoots() {
            FileObject[] arrfileObject;
            if (this.srcRoot.isValid()) {
                FileObject[] arrfileObject2 = new FileObject[1];
                arrfileObject = arrfileObject2;
                arrfileObject2[0] = this.srcRoot;
            } else {
                arrfileObject = new FileObject[]{};
            }
            return arrfileObject;
        }

        public void addChangeListener(ChangeListener l) {
        }

        public void removeChangeListener(ChangeListener l) {
        }

        public boolean preferSources() {
            return false;
        }
    }

    private static final class Result
    implements SourceForBinaryQueryImplementation2.Result,
    PropertyChangeListener {
        private final JavaPlatformManager jpm;
        private final URL artifact;
        private final ChangeSupport cs;
        private Map<JavaPlatform, PropertyChangeListener> platforms;

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Result(@NonNull JavaPlatformManager jpm, @NonNull URL artifact, @NonNull Collection<? extends JavaPlatform> platforms) {
            this.cs = new ChangeSupport((Object)this);
            Parameters.notNull((CharSequence)"jpm", (Object)jpm);
            Parameters.notNull((CharSequence)"artifact", (Object)artifact);
            Parameters.notNull((CharSequence)"platforms", platforms);
            this.jpm = jpm;
            this.artifact = artifact;
            Result result = this;
            synchronized (result) {
                this.platforms = new LinkedHashMap<JavaPlatform, PropertyChangeListener>();
                for (JavaPlatform platform : platforms) {
                    PropertyChangeListener l = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)platform);
                    platform.addPropertyChangeListener(l);
                    this.platforms.put(platform, l);
                }
                this.jpm.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)this.jpm));
            }
        }

        @NonNull
        public FileObject[] getRoots() {
            for (JavaPlatform platform : this.platforms.keySet()) {
                ClassPath sourcePath = platform.getSourceFolders();
                FileObject[] sourceRoots = sourcePath.getRoots();
                if (sourceRoots.length <= 0) continue;
                return sourceRoots;
            }
            return new FileObject[0];
        }

        public void addChangeListener(@NonNull ChangeListener listener) {
            Parameters.notNull((CharSequence)"listener", (Object)listener);
            this.cs.addChangeListener(listener);
        }

        public void removeChangeListener(@NonNull ChangeListener listener) {
            Parameters.notNull((CharSequence)"listener", (Object)listener);
            this.cs.removeChangeListener(listener);
        }

        @Override
        public void propertyChange(@NonNull PropertyChangeEvent event) {
            if ("sourceFolders".equals(event.getPropertyName())) {
                this.cs.fireChange();
            } else if ("installedPlatforms".equals(event.getPropertyName()) && this.updateCandidates()) {
                this.cs.fireChange();
            }
        }

        public boolean preferSources() {
            return false;
        }

        private synchronized boolean updateCandidates() {
            boolean affected = false;
            JavaPlatform[] newPlatforms = this.jpm.getInstalledPlatforms();
            HashMap<JavaPlatform, PropertyChangeListener> oldPlatforms = new HashMap<JavaPlatform, PropertyChangeListener>(this.platforms);
            LinkedHashMap<JavaPlatform, PropertyChangeListener> newState = new LinkedHashMap<JavaPlatform, PropertyChangeListener>(newPlatforms.length);
            for (JavaPlatform jp : newPlatforms) {
                PropertyChangeListener l = oldPlatforms.remove(jp);
                if (l != null) {
                    newState.put(jp, l);
                    continue;
                }
                if (!PlatformSourceForBinaryQuery.contains(jp, this.artifact)) continue;
                affected = true;
                l = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)this.jpm);
                jp.addPropertyChangeListener(l);
                newState.put(jp, l);
            }
            for (Map.Entry<JavaPlatform, PropertyChangeListener> e : oldPlatforms.entrySet()) {
                affected = true;
                e.getKey().removePropertyChangeListener(e.getValue());
            }
            this.platforms = newState;
            return affected;
        }
    }

}

