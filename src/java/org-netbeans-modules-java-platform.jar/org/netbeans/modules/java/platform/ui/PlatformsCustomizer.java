/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Iterator
 *  org.openide.awt.Mnemonics
 *  org.openide.cookies.InstanceCookie
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.propertysheet.PropertySheet
 *  org.openide.explorer.view.BeanTreeView
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.BeanNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.FilterNode
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.java.platform.ui;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.api.java.platform.JavaPlatformManager;
import org.netbeans.api.java.platform.Specification;
import org.netbeans.modules.java.platform.InstallerRegistry;
import org.netbeans.modules.java.platform.wizard.PlatformInstallIterator;
import org.netbeans.spi.java.platform.GeneralPlatformInstall;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.cookies.InstanceCookie;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.propertysheet.PropertySheet;
import org.openide.explorer.view.BeanTreeView;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.BeanNode;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.lookup.Lookups;

public class PlatformsCustomizer
extends JPanel
implements PropertyChangeListener,
VetoableChangeListener,
ExplorerManager.Provider {
    private static final Logger LOG = Logger.getLogger(PlatformsCustomizer.class.getName());
    private static final String TEMPLATE = "Templates/Services/Platforms/org-netbeans-api-java-Platform/javaplatform.xml";
    private static final String STORAGE = "Services/Platforms/org-netbeans-api-java-Platform";
    private static final String ATTR_CAN_REMOVE = "can-remove";
    private PlatformCategoriesChildren children;
    private ExplorerManager manager;
    private final JavaPlatform initialPlatform;
    private JButton addButton;
    private JPanel cards;
    private JPanel clientArea;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JPanel jPanel1;
    private JPanel jPanel3;
    private JPanel messageArea;
    private JTextField platformHome;
    private JTextField platformName;
    private BeanTreeView platforms;
    private JButton removeButton;

    public PlatformsCustomizer(JavaPlatform initialPlatform) {
        this.initialPlatform = initialPlatform == null ? JavaPlatformManager.getDefault().getDefaultPlatform() : initialPlatform;
        this.initComponents();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("selectedNodes".equals(evt.getPropertyName())) {
            Node[] nodes = (Node[])evt.getNewValue();
            if (nodes.length != 1) {
                this.selectPlatform(null);
            } else {
                this.selectPlatform(nodes[0]);
                Dialog dialog = (Dialog)SwingUtilities.getAncestorOfClass(Dialog.class, this);
                if (dialog != null) {
                    dialog.pack();
                }
            }
        }
    }

    @Override
    public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
        Node[] nodes;
        if ("selectedNodes".equals(evt.getPropertyName()) && (nodes = (Node[])evt.getNewValue()).length > 1) {
            throw new PropertyVetoException("Invalid length", evt);
        }
    }

    public synchronized ExplorerManager getExplorerManager() {
        if (this.manager == null) {
            this.manager = new ExplorerManager();
            this.manager.setRootContext((Node)new AbstractNode((Children)this.getChildren()));
            this.manager.addPropertyChangeListener((PropertyChangeListener)this);
            this.manager.addVetoableChangeListener((VetoableChangeListener)this);
        }
        return this.manager;
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.expandPlatforms(this.initialPlatform);
    }

    private void initComponents() {
        this.jPanel3 = new JPanel();
        this.jLabel4 = new JLabel();
        this.platforms = new PlatformsView();
        this.addButton = new JButton();
        this.removeButton = new JButton();
        this.cards = new JPanel();
        this.jPanel1 = new JPanel();
        this.jLabel1 = new JLabel();
        this.platformName = new JTextField();
        this.jLabel2 = new JLabel();
        this.platformHome = new JTextField();
        this.clientArea = new JPanel();
        this.messageArea = new JPanel();
        this.jLabel3 = new JLabel();
        this.setLayout(new GridBagLayout());
        this.jLabel4.setText(NbBundle.getMessage(PlatformsCustomizer.class, (String)"TXT_PlatformsHint"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(12, 12, 0, 12);
        this.add((Component)this.jLabel4, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(2, 12, 12, 6);
        this.add((Component)this.platforms, gridBagConstraints);
        ResourceBundle bundle = ResourceBundle.getBundle("org/netbeans/modules/java/platform/ui/Bundle");
        this.platforms.getAccessibleContext().setAccessibleName(bundle.getString("AN_PlatformsCustomizerPlatforms"));
        this.platforms.getAccessibleContext().setAccessibleDescription(bundle.getString("AD_PlatformsCustomizerPlatforms"));
        this.addButton.setMnemonic(ResourceBundle.getBundle("org/netbeans/modules/java/platform/ui/Bundle").getString("MNE_AddPlatform").charAt(0));
        this.addButton.setText(bundle.getString("CTL_AddPlatform"));
        this.addButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                PlatformsCustomizer.this.addNewPlatform(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 12, 0, 6);
        this.add((Component)this.addButton, gridBagConstraints);
        this.addButton.getAccessibleContext().setAccessibleDescription(bundle.getString("AD_AddPlatform"));
        this.removeButton.setMnemonic(ResourceBundle.getBundle("org/netbeans/modules/java/platform/ui/Bundle").getString("MNE_Remove").charAt(0));
        this.removeButton.setText(bundle.getString("CTL_Remove"));
        this.removeButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                PlatformsCustomizer.this.removePlatform(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 6, 0, 6);
        this.add((Component)this.removeButton, gridBagConstraints);
        this.removeButton.getAccessibleContext().setAccessibleDescription(bundle.getString("AD_Remove"));
        this.cards.setLayout(new CardLayout());
        this.jPanel1.setLayout(new GridBagLayout());
        this.jLabel1.setDisplayedMnemonic(ResourceBundle.getBundle("org/netbeans/modules/java/platform/ui/Bundle").getString("MNE_PlatformName").charAt(0));
        this.jLabel1.setLabelFor(this.platformName);
        this.jLabel1.setText(bundle.getString("CTL_PlatformName"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 18;
        this.jPanel1.add((Component)this.jLabel1, gridBagConstraints);
        this.platformName.setColumns(25);
        this.platformName.setEditable(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 6, 0, 0);
        this.jPanel1.add((Component)this.platformName, gridBagConstraints);
        this.platformName.getAccessibleContext().setAccessibleDescription(bundle.getString("AD_PlatformName"));
        this.jLabel2.setDisplayedMnemonic(ResourceBundle.getBundle("org/netbeans/modules/java/platform/ui/Bundle").getString("MNE_PlatformHome").charAt(0));
        this.jLabel2.setLabelFor(this.platformHome);
        this.jLabel2.setText(bundle.getString("CTL_PlatformHome"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 0, 12, 0);
        this.jPanel1.add((Component)this.jLabel2, gridBagConstraints);
        this.platformHome.setColumns(25);
        this.platformHome.setEditable(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 6, 12, 0);
        this.jPanel1.add((Component)this.platformHome, gridBagConstraints);
        this.platformHome.getAccessibleContext().setAccessibleDescription(bundle.getString("AD_PlatformHome"));
        this.clientArea.setLayout(new GridBagLayout());
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.gridheight = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.jPanel1.add((Component)this.clientArea, gridBagConstraints);
        this.cards.add((Component)this.jPanel1, "card2");
        this.messageArea.setLayout(new GridBagLayout());
        this.cards.add((Component)this.messageArea, "card3");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(2, 6, 12, 12);
        this.add((Component)this.cards, gridBagConstraints);
        this.jLabel3.setLabelFor((Component)this.platforms);
        Mnemonics.setLocalizedText((JLabel)this.jLabel3, (String)bundle.getString("TXT_PlatformsList"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(12, 12, 0, 12);
        this.add((Component)this.jLabel3, gridBagConstraints);
        this.getAccessibleContext().setAccessibleDescription(bundle.getString("AD_PlatformsCustomizer"));
    }

    private void removePlatform(ActionEvent evt) {
        Node[] nodes = this.getExplorerManager().getSelectedNodes();
        if (nodes.length != 1) {
            return;
        }
        DataObject dobj = (DataObject)nodes[0].getLookup().lookup(DataObject.class);
        if (dobj == null) {
            assert (false);
            return;
        }
        try {
            dobj.delete();
            this.getChildren().refreshPlatforms();
            this.expandPlatforms(null);
        }
        catch (IOException ioe) {
            Exceptions.printStackTrace((Throwable)ioe);
        }
    }

    private void addNewPlatform(ActionEvent evt) {
        final PlatformsCustomizer self = this;
        RequestProcessor.getDefault().post(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                List<GeneralPlatformInstall> installs = InstallerRegistry.getDefault().getAllInstallers();
                if (installs.isEmpty()) {
                    DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)NbBundle.getMessage(PlatformsCustomizer.class, (String)"ERR_NoPlatformImpl"), 1));
                    return;
                }
                try {
                    WizardDescriptor wiz = new WizardDescriptor((WizardDescriptor.Iterator)PlatformInstallIterator.create());
                    FileObject templateFo = FileUtil.getConfigFile((String)"Templates/Services/Platforms/org-netbeans-api-java-Platform/javaplatform.xml");
                    if (templateFo == null) {
                        StringBuilder sb = new StringBuilder("Broken system filesystem: ");
                        String[] parts = "Templates/Services/Platforms/org-netbeans-api-java-Platform/javaplatform.xml".split("/");
                        FileObject f = FileUtil.getConfigRoot();
                        for (int i = 0; f != null && i < parts.length; f = f.getFileObject((String)parts[i]), ++i) {
                            sb.append(f.getName()).append('/');
                        }
                        throw new IllegalStateException(sb.toString());
                    }
                    DataObject template = DataObject.find((FileObject)templateFo);
                    wiz.putProperty("targetTemplate", (Object)template);
                    DataFolder folder = DataFolder.findFolder((FileObject)FileUtil.getConfigFile((String)"Services/Platforms/org-netbeans-api-java-Platform"));
                    wiz.putProperty("targetFolder", (Object)folder);
                    wiz.putProperty("WizardPanel_autoWizardStyle", (Object)Boolean.TRUE);
                    wiz.putProperty("WizardPanel_contentDisplayed", (Object)Boolean.TRUE);
                    wiz.putProperty("WizardPanel_contentNumbered", (Object)Boolean.TRUE);
                    wiz.setTitle(NbBundle.getMessage(PlatformsCustomizer.class, (String)"CTL_AddPlatformTitle"));
                    wiz.setTitleFormat(new MessageFormat("{0}"));
                    Dialog dlg = DialogDisplayer.getDefault().createDialog((DialogDescriptor)wiz);
                    try {
                        dlg.setVisible(true);
                        if (wiz.getValue() == WizardDescriptor.FINISH_OPTION) {
                            self.getChildren().refreshPlatforms();
                            Set result = wiz.getInstantiatedObjects();
                            self.expandPlatforms(result.isEmpty() ? null : (JavaPlatform)result.iterator().next());
                        }
                    }
                    finally {
                        dlg.dispose();
                    }
                }
                catch (DataObjectNotFoundException dfne) {
                    Exceptions.printStackTrace((Throwable)dfne);
                }
                catch (IOException ioe) {
                    Exceptions.printStackTrace((Throwable)ioe);
                }
            }
        });
    }

    private synchronized PlatformCategoriesChildren getChildren() {
        if (this.children == null) {
            this.children = new PlatformCategoriesChildren();
        }
        return this.children;
    }

    private void selectPlatform(Node pNode) {
        Dimension newSize;
        Dimension updatedSize;
        Component active = null;
        for (Component c : this.cards.getComponents()) {
            if (!c.isVisible() || c != this.jPanel1 && c != this.messageArea) continue;
            active = c;
            break;
        }
        Dimension lastSize = active == null ? null : active.getSize();
        this.clientArea.removeAll();
        this.messageArea.removeAll();
        this.removeButton.setEnabled(false);
        if (pNode == null) {
            ((CardLayout)this.cards.getLayout()).last(this.cards);
            return;
        }
        JPanel target = this.messageArea;
        JPanel owner = this.messageArea;
        JavaPlatform platform = (JavaPlatform)pNode.getLookup().lookup(JavaPlatform.class);
        if (pNode != this.getExplorerManager().getRootContext()) {
            if (platform != null) {
                this.removeButton.setEnabled(this.canRemove(platform, (DataObject)pNode.getLookup().lookup(DataObject.class)));
                if (!platform.getInstallFolders().isEmpty()) {
                    this.platformName.setText(pNode.getDisplayName());
                    for (FileObject installFolder : platform.getInstallFolders()) {
                        File file = FileUtil.toFile((FileObject)installFolder);
                        if (file == null) continue;
                        this.platformHome.setText(file.getAbsolutePath());
                    }
                    target = this.clientArea;
                    owner = this.jPanel1;
                }
            }
            Component component = null;
            if (pNode.hasCustomizer()) {
                component = pNode.getCustomizer();
            }
            if (component == null) {
                PropertySheet sp = new PropertySheet();
                sp.setNodes(new Node[]{pNode});
                component = sp;
            }
            PlatformsCustomizer.addComponent(target, component);
        }
        if (lastSize != null && !(newSize = owner.getPreferredSize()).equals(updatedSize = new Dimension(Math.max(lastSize.width, newSize.width), Math.max(lastSize.height, newSize.height)))) {
            owner.setPreferredSize(updatedSize);
        }
        target.revalidate();
        CardLayout cl = (CardLayout)this.cards.getLayout();
        if (target == this.clientArea) {
            cl.first(this.cards);
        } else {
            cl.last(this.cards);
        }
    }

    private static void addComponent(Container container, Component component) {
        GridBagConstraints c = new GridBagConstraints();
        c.gridy = -1;
        c.gridx = -1;
        c.gridwidth = 0;
        c.gridheight = 0;
        c.fill = 1;
        c.anchor = 18;
        c.weighty = 1.0;
        c.weightx = 1.0;
        ((GridBagLayout)container.getLayout()).setConstraints(component, c);
        container.add(component);
    }

    private boolean canRemove(JavaPlatform platform, DataObject dobj) {
        Object attr;
        FileObject fo;
        if (PlatformsCustomizer.isDefaultPlatform(platform)) {
            return false;
        }
        if (dobj != null && (attr = (fo = dobj.getPrimaryFile()).getAttribute("can-remove")) instanceof Boolean && (Boolean)attr == Boolean.FALSE) {
            return false;
        }
        return true;
    }

    private static boolean isDefaultPlatform(JavaPlatform platform) {
        JavaPlatform defaultPlatform = JavaPlatformManager.getDefault().getDefaultPlatform();
        return defaultPlatform != null && defaultPlatform.equals(platform);
    }

    private void expandPlatforms(JavaPlatform platform) {
        ExplorerManager mgr = this.getExplorerManager();
        Node node = mgr.getRootContext();
        PlatformsCustomizer.expandAllNodes(this.platforms, node, mgr, platform);
    }

    private static void expandAllNodes(BeanTreeView btv, Node node, ExplorerManager mgr, JavaPlatform platform) {
        btv.expandNode(node);
        Children ch = node.getChildren();
        if (ch == Children.LEAF) {
            if (platform != null && platform.equals(node.getLookup().lookup(JavaPlatform.class))) {
                try {
                    mgr.setSelectedNodes(new Node[]{node});
                }
                catch (PropertyVetoException e) {
                    // empty catch block
                }
            }
            return;
        }
        Node[] nodes = ch.getNodes(true);
        for (int i = 0; i < nodes.length; ++i) {
            PlatformsCustomizer.expandAllNodes(btv, nodes[i], mgr, platform);
        }
    }

    private static class PlatformNodeComparator
    implements Comparator<Node> {
        private PlatformNodeComparator() {
        }

        @Override
        public int compare(Node n1, Node n2) {
            return n1.getDisplayName().compareTo(n2.getDisplayName());
        }
    }

    private static class PlatformCategoriesChildren
    extends Children.Keys<PlatformCategoriesDescriptor> {
        private PlatformCategoriesChildren() {
        }

        protected void addNotify() {
            Children.Keys.super.addNotify();
            this.refreshPlatforms();
        }

        protected void removeNotify() {
            Children.Keys.super.removeNotify();
        }

        protected Node[] createNodes(PlatformCategoriesDescriptor key) {
            return new Node[]{new PlatformCategoryNode(key)};
        }

        private void refreshPlatforms() {
            FileObject storage = FileUtil.getConfigFile((String)"Services/Platforms/org-netbeans-api-java-Platform");
            if (storage != null) {
                HashMap<String, PlatformCategoriesDescriptor> categories = new HashMap<String, PlatformCategoriesDescriptor>();
                for (FileObject child : storage.getChildren()) {
                    try {
                        JavaPlatform platform;
                        Node node;
                        DataObject dobj = DataObject.find((FileObject)child);
                        node = dobj.getNodeDelegate();
                        platform = (JavaPlatform)node.getLookup().lookup(JavaPlatform.class);
                        if (platform == null) {
                            InstanceCookie ic = (InstanceCookie)dobj.getLookup().lookup(InstanceCookie.class);
                            if (ic != null) {
                                try {
                                    Object instance = ic.instanceCreate();
                                    if (instance instanceof JavaPlatform) {
                                        node = new FilterNode((Node)new BeanNode(instance), Children.LEAF, Lookups.singleton((Object)instance));
                                        platform = (JavaPlatform)instance;
                                    }
                                }
                                catch (Exception e) {
                                    Exceptions.printStackTrace((Throwable)Exceptions.attachMessage((Throwable)e, (String)("Exception while loading platform:" + FileUtil.getFileDisplayName((FileObject)dobj.getPrimaryFile()))));
                                }
                            } else {
                                LOG.log(Level.FINE, "No platform provided for file: {0}", FileUtil.getFileDisplayName((FileObject)dobj.getPrimaryFile()));
                            }
                        }
                        if (platform != null) {
                            String platformType = platform.getSpecification().getName();
                            if (platformType != null) {
                                String platformTypeDisplayName = platform.getSpecification().getDisplayName();
                                PlatformCategoriesDescriptor platforms = (PlatformCategoriesDescriptor)categories.get(platformType);
                                if (platforms == null) {
                                    platforms = new PlatformCategoriesDescriptor(platformTypeDisplayName);
                                    categories.put(platformType, platforms);
                                }
                                platforms.add(node);
                                continue;
                            }
                            LOG.log(Level.WARNING, "Platform: {0} has invalid specification.", platform.getDisplayName());
                            continue;
                        }
                        LOG.log(Level.WARNING, "Platform node for : {0} has no platform in its lookup.", node.getDisplayName());
                        continue;
                    }
                    catch (DataObjectNotFoundException e) {
                        Exceptions.printStackTrace((Throwable)e);
                    }
                }
                ArrayList keys = new ArrayList(categories.values());
                Collections.sort(keys);
                this.setKeys(keys);
            }
        }
    }

    private static class PlatformCategoryNode
    extends AbstractNode {
        private final PlatformCategoriesDescriptor desc;
        private Node iconDelegate;

        public PlatformCategoryNode(PlatformCategoriesDescriptor desc) {
            super((Children)new PlatformsChildren(desc.getPlatform()));
            this.desc = desc;
            this.iconDelegate = DataFolder.findFolder((FileObject)FileUtil.getConfigRoot()).getNodeDelegate();
        }

        public String getName() {
            return this.desc.getName();
        }

        public String getDisplayName() {
            return this.getName();
        }

        public Image getIcon(int type) {
            return this.iconDelegate.getIcon(type);
        }

        public Image getOpenedIcon(int type) {
            return this.iconDelegate.getOpenedIcon(type);
        }

        public boolean hasCustomizer() {
            return true;
        }

        public Component getCustomizer() {
            return new JPanel();
        }
    }

    private static class PlatformsChildren
    extends Children.Keys<Node> {
        private final List<Node> platforms;

        public PlatformsChildren(List<Node> platforms) {
            this.platforms = platforms;
        }

        protected void addNotify() {
            Children.Keys.super.addNotify();
            this.setKeys(this.platforms);
        }

        protected void removeNotify() {
            Children.Keys.super.removeNotify();
            this.setKeys((Object[])new Node[0]);
        }

        protected Node[] createNodes(Node key) {
            return new Node[]{new FilterNode(key, Children.LEAF)};
        }
    }

    private static class PlatformCategoriesDescriptor
    implements Comparable<PlatformCategoriesDescriptor> {
        private final String categoryName;
        private final List<Node> platforms;
        private boolean changed = false;

        public PlatformCategoriesDescriptor(String categoryName) {
            assert (categoryName != null);
            this.categoryName = categoryName;
            this.platforms = new ArrayList<Node>();
        }

        public String getName() {
            return this.categoryName;
        }

        public List<Node> getPlatform() {
            if (this.changed) {
                Collections.sort(this.platforms, new PlatformNodeComparator());
                this.changed = false;
            }
            return Collections.unmodifiableList(this.platforms);
        }

        public void add(Node node) {
            this.platforms.add(node);
            this.changed = true;
        }

        public int hashCode() {
            return this.categoryName.hashCode();
        }

        public boolean equals(Object other) {
            if (other instanceof PlatformCategoriesDescriptor) {
                PlatformCategoriesDescriptor desc = (PlatformCategoriesDescriptor)other;
                return this.categoryName.equals(desc.categoryName) && this.platforms.size() == desc.platforms.size();
            }
            return false;
        }

        @Override
        public int compareTo(PlatformCategoriesDescriptor desc) {
            return this.categoryName.compareTo(desc.categoryName);
        }
    }

    private static class PlatformsView
    extends BeanTreeView {
        public PlatformsView() {
            this.setPopupAllowed(false);
            this.setDefaultActionAllowed(false);
            this.setRootVisible(false);
            this.tree.setEditable(false);
            this.tree.setShowsRootHandles(false);
            this.setBorder(UIManager.getBorder("Nb.ScrollPane.border"));
            this.setPreferredSize(new Dimension(200, 334));
        }
    }

}

