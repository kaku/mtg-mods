/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.platform;

import java.io.File;
import java.io.FilenameFilter;
import java.util.prefs.Preferences;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.Utilities;

public class PlatformSettings {
    private static final PlatformSettings INSTANCE = new PlatformSettings();
    private static final String PROP_PLATFORMS_FOLDER = "platformsFolder";
    private static final String[] APPLE_JAVAVM_FRAMEWORK_PATHS = new String[]{"/Library/Java/JavaVirtualMachines/", "/System/Library/Java/JavaVirtualMachines/", "/System/Library/Frameworks/JavaVM.framework/Versions/"};

    public String displayName() {
        return NbBundle.getMessage(PlatformSettings.class, (String)"TXT_PlatformSettings");
    }

    private static Preferences getPreferences() {
        return NbPreferences.forModule(PlatformSettings.class);
    }

    public File getPlatformsFolder() {
        File f = null;
        String folderName = PlatformSettings.getPreferences().get("platformsFolder", null);
        if (folderName == null) {
            if (Utilities.isMac()) {
                FilenameFilter filter = new FilenameFilter(){

                    @Override
                    public boolean accept(File dir, String name) {
                        return !name.startsWith(".");
                    }
                };
                for (String path : APPLE_JAVAVM_FRAMEWORK_PATHS) {
                    File tmp = new File(path);
                    if (!tmp.canRead() || tmp.list(filter).length <= 0) continue;
                    f = tmp;
                    break;
                }
            }
            if (f == null) {
                File tmp;
                f = new File(System.getProperty("user.home"));
                while ((tmp = f.getParentFile()) != null) {
                    f = tmp;
                }
            }
        } else {
            f = new File(folderName);
        }
        return f;
    }

    public void setPlatformsFolder(File file) {
        PlatformSettings.getPreferences().put("platformsFolder", file.getAbsolutePath());
    }

    public static synchronized PlatformSettings getDefault() {
        return INSTANCE;
    }

}

