/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$InstantiatingIterator
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.ChangeSupport
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.platform.wizard;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Enumeration;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.java.platform.InstallerRegistry;
import org.netbeans.spi.java.platform.CustomPlatformInstall;
import org.netbeans.spi.java.platform.GeneralPlatformInstall;
import org.openide.WizardDescriptor;
import org.openide.util.ChangeSupport;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

class SelectorPanel
extends JPanel
implements ItemListener {
    private Map<ButtonModel, GeneralPlatformInstall> installersByButtonModels = new IdentityHashMap<ButtonModel, GeneralPlatformInstall>();
    private ButtonGroup group;
    private Panel firer;

    public SelectorPanel(Panel firer) {
        this.firer = firer;
        this.initComponents();
        this.postInitComponents();
        this.setName(NbBundle.getMessage(SelectorPanel.class, (String)"TXT_SelectPlatformTypeTitle"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(SelectorPanel.class, (String)"AD_SelectPlatformType"));
    }

    private void postInitComponents() {
        InstallerRegistry regs = InstallerRegistry.getDefault();
        List<GeneralPlatformInstall> installers = regs.getAllInstallers();
        this.group = new ButtonGroup();
        JLabel label = new JLabel(NbBundle.getMessage(SelectorPanel.class, (String)"TXT_SelectPlatform"));
        label.setDisplayedMnemonic(NbBundle.getMessage(SelectorPanel.class, (String)"AD_SelectPlatform").charAt(0));
        GridBagConstraints c = new GridBagConstraints();
        c.gridy = -1;
        c.gridx = -1;
        c.gridheight = 1;
        c.gridwidth = 0;
        c.fill = 2;
        c.anchor = 18;
        c.weightx = 1.0;
        c.insets = new Insets(12, 12, 6, 12);
        ((GridBagLayout)this.getLayout()).setConstraints(label, c);
        this.add(label);
        Iterator<GeneralPlatformInstall> it = installers.iterator();
        int i = 0;
        while (it.hasNext()) {
            GeneralPlatformInstall pi = it.next();
            JRadioButton button = new JRadioButton(pi.getDisplayName());
            if (i == 0) {
                label.setLabelFor(button);
            }
            button.addItemListener(this);
            this.installersByButtonModels.put(button.getModel(), pi);
            this.group.add(button);
            c = new GridBagConstraints();
            c.gridy = -1;
            c.gridx = -1;
            c.gridheight = 1;
            c.gridwidth = 0;
            c.fill = 2;
            c.anchor = 18;
            c.weightx = 1.0;
            c.insets = new Insets(6, 18, it.hasNext() ? 0 : 12, 12);
            ((GridBagLayout)this.getLayout()).setConstraints(button, c);
            this.add(button);
            ++i;
        }
        JPanel pad = new JPanel();
        c = new GridBagConstraints();
        c.gridy = -1;
        c.gridx = -1;
        c.gridheight = 1;
        c.gridwidth = 0;
        c.fill = 1;
        c.anchor = 18;
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.insets = new Insets(12, 0, 0, 12);
        ((GridBagLayout)this.getLayout()).setConstraints(pad, c);
        this.add(pad);
    }

    private void readSettings() {
        if (this.group.getSelection() == null) {
            Enumeration<AbstractButton> buttonEnum = this.group.getElements();
            assert (buttonEnum.hasMoreElements());
            ((JRadioButton)buttonEnum.nextElement()).setSelected(true);
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        this.firer.cs.fireChange();
    }

    boolean selectInstaller(GeneralPlatformInstall install) {
        assert (install != null);
        for (Map.Entry<ButtonModel, GeneralPlatformInstall> entry : this.installersByButtonModels.entrySet()) {
            if (!entry.getValue().equals(install)) continue;
            ButtonModel model = entry.getKey();
            model.setSelected(true);
            return true;
        }
        return false;
    }

    private void initComponents() {
        this.setLayout(new GridBagLayout());
    }

    public static class Panel
    implements WizardDescriptor.Panel<WizardDescriptor> {
        private final ChangeSupport cs;
        private SelectorPanel component;

        public Panel() {
            this.cs = new ChangeSupport((Object)this);
        }

        public synchronized void removeChangeListener(ChangeListener l) {
            this.cs.removeChangeListener(l);
        }

        public synchronized void addChangeListener(ChangeListener l) {
            this.cs.addChangeListener(l);
        }

        public void readSettings(WizardDescriptor wiz) {
            this.getComponent().readSettings();
        }

        public void storeSettings(WizardDescriptor wiz) {
        }

        public HelpCtx getHelp() {
            return new HelpCtx(SelectorPanel.class);
        }

        public boolean isValid() {
            return this.component != null;
        }

        public SelectorPanel getComponent() {
            if (this.component == null) {
                this.component = new SelectorPanel(this);
            }
            return this.component;
        }

        public GeneralPlatformInstall getInstaller() {
            SelectorPanel c = this.getComponent();
            ButtonModel bm = c.group.getSelection();
            if (bm != null) {
                return (GeneralPlatformInstall)c.installersByButtonModels.get(bm);
            }
            return null;
        }

        public WizardDescriptor.InstantiatingIterator getInstallerIterator() {
            GeneralPlatformInstall platformInstall = this.getInstaller();
            if (platformInstall instanceof CustomPlatformInstall) {
                return ((CustomPlatformInstall)platformInstall).createIterator();
            }
            return null;
        }
    }

}

