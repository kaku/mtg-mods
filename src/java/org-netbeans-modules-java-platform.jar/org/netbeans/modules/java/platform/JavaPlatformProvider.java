/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.platform;

import java.beans.PropertyChangeListener;
import org.netbeans.api.java.platform.JavaPlatform;

public interface JavaPlatformProvider {
    public static final String PROP_INSTALLED_PLATFORMS = "installedPlatforms";

    public JavaPlatform[] getInstalledPlatforms();

    public JavaPlatform getDefaultPlatform();

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

