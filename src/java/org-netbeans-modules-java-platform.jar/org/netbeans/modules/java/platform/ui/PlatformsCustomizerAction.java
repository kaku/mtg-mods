/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.platform.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.netbeans.api.java.platform.PlatformsCustomizer;

public final class PlatformsCustomizerAction
implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        PlatformsCustomizer.showCustomizer(null);
    }
}

