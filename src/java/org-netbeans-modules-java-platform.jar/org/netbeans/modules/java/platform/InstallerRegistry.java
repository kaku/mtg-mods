/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.java.platform;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.spi.java.platform.CustomPlatformInstall;
import org.netbeans.spi.java.platform.GeneralPlatformInstall;
import org.netbeans.spi.java.platform.PlatformInstall;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

public class InstallerRegistry {
    private static final String INSTALLER_REGISTRY_FOLDER = "org-netbeans-api-java/platform/installers";
    private static Reference<InstallerRegistry> defaultInstance = new WeakReference<Object>(null);
    private static final Logger LOG = Logger.getLogger(InstallerRegistry.class.getName());
    private final Lookup lookup;
    private final List<GeneralPlatformInstall> platformInstalls;

    InstallerRegistry() {
        this.lookup = Lookups.forPath((String)"org-netbeans-api-java/platform/installers");
        this.platformInstalls = null;
    }

    InstallerRegistry(GeneralPlatformInstall[] platformInstalls) {
        assert (platformInstalls != null);
        this.platformInstalls = Arrays.asList(platformInstalls);
        this.lookup = null;
    }

    public List<PlatformInstall> getInstallers() {
        return InstallerRegistry.filter(this.getAllInstallers(), PlatformInstall.class);
    }

    public List<CustomPlatformInstall> getCustomInstallers() {
        return InstallerRegistry.filter(this.getAllInstallers(), CustomPlatformInstall.class);
    }

    public List<GeneralPlatformInstall> getAllInstallers() {
        if (this.platformInstalls != null) {
            return this.platformInstalls;
        }
        this.lookup.lookupAll(CustomPlatformInstall.class);
        this.lookup.lookupAll(PlatformInstall.class);
        List<GeneralPlatformInstall> installs = Collections.unmodifiableList(new ArrayList(this.lookup.lookupAll(GeneralPlatformInstall.class)));
        LOG.log(Level.FINE, "Installers: {0}", installs);
        return installs;
    }

    public static InstallerRegistry getDefault() {
        InstallerRegistry regs = defaultInstance.get();
        if (regs != null) {
            return regs;
        }
        regs = new InstallerRegistry();
        defaultInstance = new WeakReference<InstallerRegistry>(regs);
        return regs;
    }

    static InstallerRegistry prepareForUnitTest(GeneralPlatformInstall[] platformInstalls) {
        InstallerRegistry regs = new InstallerRegistry(platformInstalls);
        defaultInstance = new WeakReference<InstallerRegistry>(regs);
        return regs;
    }

    private static <T> List<T> filter(List<?> list, Class<T> clazz) {
        ArrayList<T> result = new ArrayList<T>(list.size());
        for (Object item : list) {
            if (!clazz.isInstance(item)) continue;
            result.add(clazz.cast(item));
        }
        return result;
    }
}

