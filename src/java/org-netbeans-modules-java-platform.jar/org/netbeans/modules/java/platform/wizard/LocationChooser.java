/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$InstantiatingIterator
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.ChangeSupport
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.platform.wizard;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.Border;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileSystemView;
import javax.swing.filechooser.FileView;
import org.netbeans.modules.java.platform.PlatformSettings;
import org.netbeans.spi.java.platform.PlatformInstall;
import org.openide.WizardDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.util.ChangeSupport;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class LocationChooser
extends JFileChooser
implements PropertyChangeListener {
    private static final Dimension PREFERRED_SIZE = new Dimension(600, 340);
    private WizardDescriptor.InstantiatingIterator<WizardDescriptor> iterator;
    private Panel firer;
    private PlatformFileView platformFileView;

    public LocationChooser(Panel firer) {
        this.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.setName(NbBundle.getMessage(LocationChooser.class, (String)"TXT_PlatformFolderTitle"));
        this.setFileSelectionMode(1);
        this.setMultiSelectionEnabled(false);
        this.setControlButtonsAreShown(false);
        this.setFileFilter(new PlatformFileFilter());
        this.firer = firer;
        this.platformFileView = new PlatformFileView(this.getFileSystemView());
        this.setFileView(this.platformFileView);
        this.addPropertyChangeListener(this);
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(LocationChooser.class, (String)"AD_LocationChooser"));
        this.getActionMap().put("cancel", new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Container parent = LocationChooser.this.getParent();
                while ((parent = parent.getParent()) != null && !(parent instanceof Window)) {
                }
                if (parent != null) {
                    ((Window)parent).setVisible(false);
                }
            }
        });
        this.getInputMap(1).put(KeyStroke.getKeyStroke(27, 0), "cancel");
        this.setBorder(null);
    }

    @Override
    public Dimension getPreferredSize() {
        return PREFERRED_SIZE;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("SelectedFileChangedProperty".equals(evt.getPropertyName())) {
            PlatformInstall install;
            FileObject fo;
            this.iterator = null;
            File file = this.getSelectedFile();
            if (file != null && (fo = FileUtil.toFileObject((File)FileUtil.normalizeFile((File)(file = FileUtil.normalizeFile((File)file))))) != null && (install = this.platformFileView.getPlatformInstall()) != null && install.accept(fo)) {
                this.iterator = install.createIterator(fo);
            }
            this.firer.cs.fireChange();
        }
    }

    private boolean valid() {
        return this.getInstaller() != null;
    }

    private void read(WizardDescriptor settings) {
        PlatformSettings ps = PlatformSettings.getDefault();
        if (ps != null) {
            File curDir = ps.getPlatformsFolder();
            if (curDir.equals(this.getCurrentDirectory()) && null != curDir.getParentFile()) {
                this.setCurrentDirectory(curDir.getParentFile());
            }
            this.setCurrentDirectory(curDir);
        }
    }

    private void store(WizardDescriptor settings) {
        PlatformSettings ps;
        File dir = this.getCurrentDirectory();
        if (dir != null && (ps = PlatformSettings.getDefault()) != null) {
            ps.setPlatformsFolder(dir);
        }
    }

    private WizardDescriptor.InstantiatingIterator<WizardDescriptor> getInstaller() {
        return this.iterator;
    }

    private void setPlatformInstall(PlatformInstall platformInstall) {
        this.platformFileView.setPlatformInstall(platformInstall);
    }

    private PlatformInstall getPlatformInstall() {
        return this.platformFileView.getPlatformInstall();
    }

    private static class PlatformFileView
    extends FileView {
        private static final Icon BADGE = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/java/platform/resources/platformBadge.gif", (boolean)false);
        private static final Icon EMPTY = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/java/platform/resources/empty.gif", (boolean)false);
        private FileSystemView fsv;
        private Icon lastOriginal;
        private Icon lastMerged;
        private PlatformInstall platformInstall;

        public PlatformFileView(FileSystemView fsv) {
            this.fsv = fsv;
        }

        @Override
        public Icon getIcon(File _f) {
            File f = FileUtil.normalizeFile((File)_f);
            Icon original = this.fsv.getSystemIcon(f);
            if (original == null) {
                original = EMPTY;
            }
            if (this.isPlatformDir(f)) {
                if (original.equals(this.lastOriginal)) {
                    return this.lastMerged;
                }
                this.lastOriginal = original;
                this.lastMerged = new MergedIcon(original, BADGE, -1, -1);
                return this.lastMerged;
            }
            return original;
        }

        public void setPlatformInstall(PlatformInstall platformInstall) {
            this.platformInstall = platformInstall;
        }

        public PlatformInstall getPlatformInstall() {
            return this.platformInstall;
        }

        private boolean isPlatformDir(File f) {
            FileObject fo;
            int osId = Utilities.getOperatingSystem();
            if (osId == 8 || osId == 256) {
                return false;
            }
            FileObject fileObject = fo = f != null ? PlatformFileView.convertToValidDir(f) : null;
            if (fo != null) {
                try {
                    if (Utilities.isUnix() && (fo.getParent() == null || fo.getFileSystem().getRoot().equals((Object)fo.getParent()))) {
                        return false;
                    }
                }
                catch (FileStateInvalidException e) {
                    return false;
                }
                if (this.platformInstall.accept(fo)) {
                    return true;
                }
            }
            return false;
        }

        private static FileObject convertToValidDir(File f) {
            File testFile = new File(f.getPath());
            if (testFile == null || testFile.getParent() == null) {
                return null;
            }
            if (!testFile.isDirectory()) {
                return null;
            }
            FileObject fo = FileUtil.toFileObject((File)FileUtil.normalizeFile((File)f));
            return fo;
        }
    }

    private static class MergedIcon
    implements Icon {
        private Icon icon1;
        private Icon icon2;
        private int xMerge;
        private int yMerge;

        MergedIcon(Icon icon1, Icon icon2, int xMerge, int yMerge) {
            this.icon1 = icon1;
            this.icon2 = icon2;
            if (xMerge == -1) {
                xMerge = icon1.getIconWidth() - icon2.getIconWidth();
            }
            if (yMerge == -1) {
                yMerge = icon1.getIconHeight() - icon2.getIconHeight();
            }
            this.xMerge = xMerge;
            this.yMerge = yMerge;
        }

        @Override
        public int getIconHeight() {
            return Math.max(this.icon1.getIconHeight(), this.yMerge + this.icon2.getIconHeight());
        }

        @Override
        public int getIconWidth() {
            return Math.max(this.icon1.getIconWidth(), this.yMerge + this.icon2.getIconWidth());
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            this.icon1.paintIcon(c, g, x, y);
            this.icon2.paintIcon(c, g, x + this.xMerge, y + this.yMerge);
        }
    }

    public static class Panel
    implements WizardDescriptor.Panel<WizardDescriptor> {
        LocationChooser component;
        private final ChangeSupport cs;

        public Panel() {
            this.cs = new ChangeSupport((Object)this);
        }

        public LocationChooser getComponent() {
            if (this.component == null) {
                this.component = new LocationChooser(this);
            }
            return this.component;
        }

        public HelpCtx getHelp() {
            return new HelpCtx(LocationChooser.class);
        }

        public boolean isValid() {
            return this.getComponent().valid();
        }

        public void readSettings(WizardDescriptor wiz) {
            this.getComponent().read(wiz);
        }

        public void addChangeListener(ChangeListener l) {
            this.cs.addChangeListener(l);
        }

        public void removeChangeListener(ChangeListener l) {
            this.cs.removeChangeListener(l);
        }

        public void storeSettings(WizardDescriptor wiz) {
            this.getComponent().store(wiz);
        }

        WizardDescriptor.InstantiatingIterator<WizardDescriptor> getInstallerIterator() {
            return this.getComponent().getInstaller();
        }

        void setPlatformInstall(PlatformInstall platformInstall) {
            this.getComponent().setPlatformInstall(platformInstall);
        }

        PlatformInstall getPlatformInstall() {
            return this.getComponent().getPlatformInstall();
        }
    }

    private static class PlatformAccessory
    extends JPanel {
        private JTextField tf;

        public PlatformAccessory() {
            this.initComponents();
        }

        private void setType(String type) {
            this.tf.setText(type);
        }

        private void initComponents() {
            this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(LocationChooser.class, (String)"AN_LocationChooserAccessiory"));
            this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(LocationChooser.class, (String)"AD_LocationChooserAccessiory"));
            GridBagLayout l = new GridBagLayout();
            this.setLayout(l);
            JLabel label = new JLabel(NbBundle.getMessage(LocationChooser.class, (String)"TXT_PlatformType"));
            label.setDisplayedMnemonic(NbBundle.getMessage(LocationChooser.class, (String)"MNE_PlatformType").charAt(0));
            GridBagConstraints c = new GridBagConstraints();
            c.gridy = -1;
            c.gridx = -1;
            c.gridwidth = 0;
            c.insets = new Insets(0, 12, 3, 12);
            c.anchor = 18;
            l.setConstraints(label, c);
            this.add(label);
            this.tf = new JTextField();
            this.tf.setColumns(15);
            this.tf.setEditable(false);
            this.tf.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(LocationChooser.class, (String)"AD_PlatformType"));
            c = new GridBagConstraints();
            c.gridy = -1;
            c.gridx = -1;
            c.gridwidth = 0;
            c.insets = new Insets(3, 12, 12, 12);
            c.anchor = 18;
            c.fill = 2;
            c.weightx = 1.0;
            l.setConstraints(this.tf, c);
            this.add(this.tf);
            label.setLabelFor(this.tf);
            JPanel fill = new JPanel();
            c = new GridBagConstraints();
            c.gridy = -1;
            c.gridx = -1;
            c.gridwidth = 0;
            c.insets = new Insets(0, 12, 12, 12);
            c.anchor = 18;
            c.fill = 1;
            c.weighty = 1.0;
            c.weightx = 1.0;
            l.setConstraints(fill, c);
            this.add(fill);
        }
    }

    private static class PlatformFileFilter
    extends FileFilter {
        private PlatformFileFilter() {
        }

        @Override
        public boolean accept(File f) {
            return f.isDirectory();
        }

        @Override
        public String getDescription() {
            return NbBundle.getMessage(LocationChooser.class, (String)"TXT_PlatformFolder");
        }
    }

}

