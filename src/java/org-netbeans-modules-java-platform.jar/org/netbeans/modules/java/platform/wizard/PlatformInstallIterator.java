/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$InstantiatingIterator
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.modules.ModuleInfo
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.java.platform.wizard;

import java.awt.Component;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import javax.swing.JComponent;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.java.platform.InstallerRegistry;
import org.netbeans.modules.java.platform.wizard.LocationChooser;
import org.netbeans.modules.java.platform.wizard.SelectorPanel;
import org.netbeans.spi.java.platform.CustomPlatformInstall;
import org.netbeans.spi.java.platform.GeneralPlatformInstall;
import org.netbeans.spi.java.platform.PlatformInstall;
import org.openide.WizardDescriptor;
import org.openide.modules.ModuleInfo;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class PlatformInstallIterator
implements WizardDescriptor.InstantiatingIterator<WizardDescriptor>,
ChangeListener {
    WizardDescriptor.InstantiatingIterator<WizardDescriptor> typeIterator;
    int panelIndex;
    boolean hasSelectorPanel;
    WizardDescriptor wizard;
    int panelNumber = -1;
    ResourceBundle bundle = NbBundle.getBundle(PlatformInstallIterator.class);
    LocationChooser.Panel locationPanel = new LocationChooser.Panel();
    SelectorPanel.Panel selectorPanel = new SelectorPanel.Panel();
    Collection<ChangeListener> listeners = new ArrayList<ChangeListener>();

    PlatformInstallIterator() {
        this.selectorPanel.addChangeListener(this);
        this.locationPanel.addChangeListener(this);
    }

    public static PlatformInstallIterator create() {
        return new PlatformInstallIterator();
    }

    int getPanelIndex() {
        return this.panelIndex;
    }

    void updatePanelsList(JComponent[] where, WizardDescriptor.InstantiatingIterator<WizardDescriptor> it) {
        LinkedList<String> c = new LinkedList<String>();
        if (this.hasSelectorPanel) {
            c.add(this.bundle.getString("TXT_SelectPlatformTypeTitle"));
        }
        if (this.panelIndex == 1 || this.panelIndex == 2 || this.panelIndex == 0 && this.selectorPanel.getInstallerIterator() == null) {
            c.add(this.bundle.getString("TXT_PlatformFolderTitle"));
        }
        if (it != null) {
            JComponent pc;
            String[] steps;
            WizardDescriptor.Panel p = it.current();
            if (p != null && (steps = (String[])(pc = (JComponent)p.getComponent()).getClientProperty("WizardPanel_contentData")) != null) {
                c.addAll(Arrays.asList(steps));
            }
        } else {
            c.add(this.bundle.getString("TITLE_PlatformLocationUnknown"));
        }
        String[] names = c.toArray(new String[c.size()]);
        for (JComponent comp : where) {
            comp.putClientProperty("WizardPanel_contentData", names);
        }
    }

    public void addChangeListener(ChangeListener l) {
        this.listeners.add(l);
    }

    public WizardDescriptor.Panel<WizardDescriptor> current() {
        if (this.panelIndex == 0) {
            return this.selectorPanel;
        }
        if (this.panelIndex == 1) {
            return this.locationPanel;
        }
        if (this.typeIterator == null) {
            throw new NullPointerException("index: " + this.panelIndex);
        }
        return this.typeIterator.current();
    }

    public boolean hasNext() {
        if (this.panelIndex == 0) {
            GeneralPlatformInstall installer = this.selectorPanel.getInstaller();
            if (installer instanceof CustomPlatformInstall) {
                WizardDescriptor.InstantiatingIterator<WizardDescriptor> it = ((CustomPlatformInstall)installer).createIterator();
                if (it != this.typeIterator) {
                    this.updateIterator(it);
                }
                return this.typeIterator == null ? false : this.typeIterator.current() != null;
            }
            return true;
        }
        if (this.panelIndex == 1) {
            WizardDescriptor.InstantiatingIterator<WizardDescriptor> typeIt = this.locationPanel.getInstallerIterator();
            if (typeIt == null) {
                return false;
            }
            WizardDescriptor.Panel p = typeIt.current();
            return p != null;
        }
        return this.typeIterator.hasNext();
    }

    public boolean hasPrevious() {
        return !(this.panelIndex == 0 || this.panelIndex == 1 && !this.hasSelectorPanel || this.panelIndex == 3 && !this.hasSelectorPanel && this.typeIterator != null && !this.typeIterator.hasPrevious());
    }

    public void initialize(WizardDescriptor wiz) {
        this.wizard = wiz;
        List<GeneralPlatformInstall> installers = InstallerRegistry.getDefault().getAllInstallers();
        if (installers.isEmpty()) {
            Collection infos = Lookup.getDefault().lookupAll(ModuleInfo.class);
            StringBuilder sb = new StringBuilder("No PlatformInstallFound in Lookup, enabled modules:\n");
            for (ModuleInfo info : infos) {
                if (!info.isEnabled()) continue;
                sb.append(info.getDisplayName());
                sb.append('(');
                sb.append(info.getCodeName());
                sb.append(")\n");
            }
            throw new IllegalStateException(sb.toString());
        }
        if (installers.size() > 1) {
            this.panelIndex = 0;
            this.hasSelectorPanel = true;
        } else if (installers.get(0) instanceof CustomPlatformInstall) {
            this.panelIndex = 3;
            this.hasSelectorPanel = false;
            this.typeIterator = ((CustomPlatformInstall)installers.get(0)).createIterator();
            if (this.typeIterator == null) {
                throw new NullPointerException();
            }
        } else {
            this.panelIndex = 1;
            this.hasSelectorPanel = false;
            this.locationPanel.setPlatformInstall((PlatformInstall)installers.get(0));
        }
        this.updatePanelsList(new JComponent[]{(JComponent)this.current().getComponent()}, this.typeIterator);
        this.wizard.setTitle(NbBundle.getMessage(PlatformInstallIterator.class, (String)"TXT_AddPlatformTitle"));
        this.panelNumber = 0;
        this.wizard.putProperty("WizardPanel_contentSelectedIndex", (Object)new Integer(this.panelNumber));
    }

    public Set instantiate() throws IOException {
        return this.typeIterator.instantiate();
    }

    public String name() {
        if (this.panelIndex == 0) {
            return this.bundle.getString("TXT_PlatformSelectorTitle");
        }
        if (this.panelIndex == 1) {
            return this.bundle.getString("TXT_PlatformFolderTitle");
        }
        return this.typeIterator.name();
    }

    public void nextPanel() {
        if (this.panelIndex == 0) {
            if (this.selectorPanel.getInstallerIterator() == null) {
                this.panelIndex = 1;
            } else {
                this.panelIndex = 3;
                if (this.typeIterator == null) {
                    throw new NullPointerException();
                }
            }
        } else if (this.panelIndex == 1) {
            this.panelIndex = 2;
        } else {
            this.typeIterator.nextPanel();
        }
        ++this.panelNumber;
        this.wizard.putProperty("WizardPanel_contentSelectedIndex", (Object)new Integer(this.panelNumber));
    }

    public void previousPanel() {
        if (this.panelIndex == 1) {
            this.panelIndex = 0;
        } else if (this.panelIndex == 2) {
            if (this.typeIterator.hasPrevious()) {
                this.typeIterator.previousPanel();
            } else {
                this.panelIndex = 1;
            }
        } else if (this.panelIndex == 3) {
            if (this.typeIterator.hasPrevious()) {
                this.typeIterator.previousPanel();
            } else {
                this.panelIndex = 0;
            }
        }
        --this.panelNumber;
        this.wizard.putProperty("WizardPanel_contentSelectedIndex", (Object)new Integer(this.panelNumber));
    }

    public void removeChangeListener(ChangeListener l) {
        this.listeners.remove(l);
    }

    public void uninitialize(WizardDescriptor wiz) {
        if (this.typeIterator != null) {
            this.typeIterator.uninitialize(wiz);
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        WizardDescriptor.InstantiatingIterator<WizardDescriptor> it;
        if (e.getSource() == this.locationPanel) {
            it = this.locationPanel.getInstallerIterator();
        } else if (e.getSource() == this.selectorPanel) {
            GeneralPlatformInstall installer = this.selectorPanel.getInstaller();
            if (installer instanceof CustomPlatformInstall) {
                it = ((CustomPlatformInstall)installer).createIterator();
            } else {
                it = null;
                this.locationPanel.setPlatformInstall((PlatformInstall)installer);
            }
        } else {
            assert (false);
            return;
        }
        if (it != this.typeIterator) {
            this.updateIterator(it);
        }
    }

    private void updateIterator(WizardDescriptor.InstantiatingIterator<WizardDescriptor> it) {
        if (this.typeIterator != null) {
            this.typeIterator.uninitialize(this.wizard);
        }
        if (it != null) {
            it.initialize(this.wizard);
            this.updatePanelsList(new JComponent[]{this.selectorPanel.getComponent(), this.locationPanel.getComponent(), (JComponent)it.current().getComponent()}, it);
        } else {
            this.updatePanelsList(new JComponent[]{this.selectorPanel.getComponent(), this.locationPanel.getComponent()}, null);
        }
        this.typeIterator = it;
        this.wizard.putProperty("WizardPanel_contentSelectedIndex", (Object)new Integer(this.panelNumber));
    }
}

