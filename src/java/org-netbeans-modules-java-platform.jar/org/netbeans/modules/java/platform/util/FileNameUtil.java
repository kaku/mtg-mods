/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.platform.util;

public class FileNameUtil {
    public static String computeAbsolutePath(String home, String relative) {
        String separator = System.getProperty("file.separator");
        if (home.endsWith("\\") || home.endsWith("/")) {
            home = home.substring(0, home.length() - 1);
        }
        if (relative.startsWith("\\") || relative.startsWith("/")) {
            relative = relative.substring(1);
        }
        String path = home + separator + relative;
        if (separator.equals("/")) {
            path = path.replace('\\', separator.charAt(0));
        } else if (separator.equals("\\")) {
            path = path.replace('/', separator.charAt(0));
        }
        return path;
    }
}

