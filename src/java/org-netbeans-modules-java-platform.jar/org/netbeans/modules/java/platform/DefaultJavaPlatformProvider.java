/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.InstanceCookie$Of
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.java.platform;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.modules.java.platform.JavaPlatformProvider;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

public class DefaultJavaPlatformProvider
implements JavaPlatformProvider,
FileChangeListener {
    private static final String PLATFORM_STORAGE = "Services/Platforms/org-netbeans-api-java-Platform";
    private static final String DEFAULT_PLATFORM_ATTR = "default-platform";
    private final PropertyChangeSupport pcs;
    private FileObject storageCache;
    private FileObject lastFound;
    private FileChangeListener pathListener;
    private JavaPlatform defaultPlatform;
    private static final Logger LOG = Logger.getLogger(DefaultJavaPlatformProvider.class.getName());

    public DefaultJavaPlatformProvider() {
        this.pcs = new PropertyChangeSupport(this);
    }

    @Override
    public JavaPlatform[] getInstalledPlatforms() {
        ArrayList<JavaPlatform> platforms = new ArrayList<JavaPlatform>();
        FileObject storage = this.getStorage();
        if (storage != null) {
            try {
                for (FileObject platformDefinition : storage.getChildren()) {
                    DataObject dobj = DataObject.find((FileObject)platformDefinition);
                    InstanceCookie ic = (InstanceCookie)dobj.getCookie(InstanceCookie.class);
                    if (ic == null) {
                        LOG.log(Level.WARNING, "The file: {0} has no InstanceCookie", platformDefinition.getNameExt());
                        continue;
                    }
                    if (ic instanceof InstanceCookie.Of) {
                        if (((InstanceCookie.Of)ic).instanceOf(JavaPlatform.class)) {
                            platforms.add((JavaPlatform)ic.instanceCreate());
                            continue;
                        }
                        LOG.log(Level.WARNING, "The file: {0} is not an instance of JavaPlatform", platformDefinition.getNameExt());
                        continue;
                    }
                    Object instance = ic.instanceCreate();
                    if (instance instanceof JavaPlatform) {
                        platforms.add((JavaPlatform)instance);
                        continue;
                    }
                    LOG.log(Level.WARNING, "The file: {0} is not an instance of JavaPlatform", platformDefinition.getNameExt());
                }
            }
            catch (ClassNotFoundException cnf) {
                Exceptions.printStackTrace((Throwable)cnf);
            }
            catch (IOException ioe) {
                Exceptions.printStackTrace((Throwable)ioe);
            }
        }
        return platforms.toArray(new JavaPlatform[platforms.size()]);
    }

    @Override
    public JavaPlatform getDefaultPlatform() {
        if (this.defaultPlatform == null) {
            this.defaultPlatform = this.getDefaultPlatformByHint();
            if (this.defaultPlatform != null) {
                return this.defaultPlatform;
            }
            JavaPlatform[] allPlatforms = this.getInstalledPlatforms();
            for (int i = 0; i < allPlatforms.length; ++i) {
                if (!this.isDefaultPlatform(allPlatforms[i])) continue;
                this.defaultPlatform = allPlatforms[i];
                break;
            }
        }
        return this.defaultPlatform;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.removePropertyChangeListener(listener);
    }

    public void fileFolderCreated(FileEvent fe) {
    }

    public void fileDataCreated(FileEvent fe) {
        this.firePropertyChange();
    }

    public void fileChanged(FileEvent fe) {
        this.firePropertyChange();
    }

    public void fileDeleted(FileEvent fe) {
        this.firePropertyChange();
    }

    public void fileRenamed(FileRenameEvent fe) {
        this.firePropertyChange();
    }

    public void fileAttributeChanged(FileAttributeEvent fe) {
    }

    private void firePropertyChange() {
        this.pcs.firePropertyChange("installedPlatforms", null, null);
    }

    private boolean isDefaultPlatform(JavaPlatform platform) {
        return "default_platform".equals(platform.getProperties().get("platform.ant.name"));
    }

    private JavaPlatform getDefaultPlatformByHint() {
        FileObject storage = this.getStorage();
        if (storage != null) {
            for (FileObject defFile : storage.getChildren()) {
                if (defFile.getAttribute("default-platform") != Boolean.TRUE) continue;
                try {
                    Object instance;
                    DataObject dobj = DataObject.find((FileObject)defFile);
                    InstanceCookie ic = (InstanceCookie)dobj.getCookie(InstanceCookie.class);
                    if (ic != null && (instance = ic.instanceCreate()) instanceof JavaPlatform && this.isDefaultPlatform((JavaPlatform)instance)) {
                        return (JavaPlatform)instance;
                    }
                }
                catch (IOException e) {
                }
                catch (ClassNotFoundException e) {
                    // empty catch block
                }
                return null;
            }
        }
        return null;
    }

    private synchronized FileObject getStorage() {
        if (this.storageCache == null) {
            this.storageCache = FileUtil.getConfigFile((String)"Services/Platforms/org-netbeans-api-java-Platform");
            if (this.storageCache != null) {
                this.storageCache.addFileChangeListener((FileChangeListener)this);
                this.removePathListener();
            } else {
                FileObject current;
                String pathElement;
                String[] path = "Services/Platforms/org-netbeans-api-java-Platform".split("/");
                FileObject lastExist = FileUtil.getConfigRoot();
                String expected = null;
                String[] arr$ = path;
                int len$ = arr$.length;
                for (int i$ = 0; i$ < len$ && (current = lastExist.getFileObject(expected = (pathElement = arr$[i$]))) != null; ++i$) {
                    lastExist = current;
                }
                assert (lastExist != null);
                assert (expected != null);
                this.removePathListener();
                final String expectedFin = expected;
                this.pathListener = new FileChangeAdapter(){

                    public void fileFolderCreated(FileEvent fe) {
                        if (expectedFin.equals(fe.getFile().getName())) {
                            DefaultJavaPlatformProvider.this.firePropertyChange();
                        }
                    }
                };
                this.lastFound = lastExist;
                this.lastFound.addFileChangeListener(this.pathListener);
            }
        }
        return this.storageCache;
    }

    private void removePathListener() {
        if (this.pathListener != null) {
            assert (this.lastFound != null);
            this.lastFound.removeFileChangeListener(this.pathListener);
            this.pathListener = null;
            this.lastFound = null;
        }
    }

}

