/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.modules.Dependency
 *  org.openide.modules.SpecificationVersion
 *  org.openide.util.NbCollections
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.platform;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.api.java.platform.Specification;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.modules.Dependency;
import org.openide.modules.SpecificationVersion;
import org.openide.util.NbCollections;
import org.openide.util.Utilities;

public final class FallbackDefaultJavaPlatform
extends JavaPlatform {
    private static FallbackDefaultJavaPlatform instance;

    private FallbackDefaultJavaPlatform() {
        this.setSystemProperties(NbCollections.checkedMapByFilter((Map)System.getProperties(), String.class, String.class, (boolean)false));
    }

    @Override
    public String getDisplayName() {
        return System.getProperty("java.vm.vendor") + " " + System.getProperty("java.vm.version");
    }

    @Override
    public Map<String, String> getProperties() {
        return Collections.singletonMap("platform.ant.name", "default_platform");
    }

    private static ClassPath sysProp2CP(String propname) {
        String sbcp = System.getProperty(propname);
        if (sbcp == null) {
            return null;
        }
        ArrayList<URL> roots = new ArrayList<URL>();
        StringTokenizer tok = new StringTokenizer(sbcp, File.pathSeparator);
        while (tok.hasMoreTokens()) {
            URL u;
            File f = new File(tok.nextToken());
            if (!f.exists()) continue;
            try {
                File normf = FileUtil.normalizeFile((File)f);
                u = Utilities.toURI((File)normf).toURL();
            }
            catch (MalformedURLException x) {
                throw new AssertionError(x);
            }
            if (FileUtil.isArchiveFile((URL)u)) {
                u = FileUtil.getArchiveRoot((URL)u);
            }
            roots.add(u);
        }
        return ClassPathSupport.createClassPath((URL[])roots.toArray(new URL[roots.size()]));
    }

    private static ClassPath sampleClass2CP(Class prototype) {
        URL[] arruRL;
        CodeSource cs = prototype.getProtectionDomain().getCodeSource();
        if (cs != null) {
            URL[] arruRL2 = new URL[1];
            arruRL = arruRL2;
            arruRL2[0] = cs.getLocation();
        } else {
            arruRL = new URL[]{};
        }
        return ClassPathSupport.createClassPath((URL[])arruRL);
    }

    @Override
    public ClassPath getBootstrapLibraries() {
        ClassPath cp = FallbackDefaultJavaPlatform.sysProp2CP("sun.boot.class.path");
        return cp != null ? cp : FallbackDefaultJavaPlatform.sampleClass2CP(Object.class);
    }

    @Override
    public ClassPath getStandardLibraries() {
        ClassPath cp = FallbackDefaultJavaPlatform.sysProp2CP("java.class.path");
        return cp != null ? cp : FallbackDefaultJavaPlatform.sampleClass2CP(Dependency.class);
    }

    @Override
    public String getVendor() {
        return System.getProperty("java.vm.vendor");
    }

    @Override
    public Specification getSpecification() {
        return new Specification("j2se", Dependency.JAVA_SPEC);
    }

    @Override
    public Collection<FileObject> getInstallFolders() {
        return Collections.singleton(FileUtil.toFileObject((File)FileUtil.normalizeFile((File)new File(System.getProperty("java.home")))));
    }

    @Override
    public FileObject findTool(String toolName) {
        return null;
    }

    @Override
    public ClassPath getSourceFolders() {
        return ClassPathSupport.createClassPath((URL[])new URL[0]);
    }

    @Override
    public List<URL> getJavadocFolders() {
        return Collections.emptyList();
    }

    public static synchronized FallbackDefaultJavaPlatform getInstance() {
        if (instance == null) {
            instance = new FallbackDefaultJavaPlatform();
        }
        return instance;
    }
}

