/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.SpecificationVersion
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 */
package org.netbeans.api.java.platform;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.api.java.platform.Profile;
import org.netbeans.api.java.platform.Specification;
import org.netbeans.modules.java.platform.FallbackDefaultJavaPlatform;
import org.netbeans.modules.java.platform.JavaPlatformProvider;
import org.openide.modules.SpecificationVersion;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;

public final class JavaPlatformManager {
    public static final String PROP_INSTALLED_PLATFORMS = "installedPlatforms";
    private static JavaPlatformManager instance = null;
    private Lookup.Result<JavaPlatformProvider> providers;
    private Collection<? extends JavaPlatformProvider> lastProviders = Collections.emptySet();
    private boolean providersValid = false;
    private PropertyChangeListener pListener;
    private Collection<JavaPlatform> cachedPlatforms;
    private final PropertyChangeSupport pcs;

    public JavaPlatformManager() {
        this.pcs = new PropertyChangeSupport(this);
    }

    public static synchronized JavaPlatformManager getDefault() {
        if (instance == null) {
            instance = new JavaPlatformManager();
        }
        return instance;
    }

    public JavaPlatform getDefaultPlatform() {
        for (JavaPlatformProvider provider : this.getProviders()) {
            JavaPlatform defaultPlatform = provider.getDefaultPlatform();
            if (defaultPlatform == null) continue;
            return defaultPlatform;
        }
        return FallbackDefaultJavaPlatform.getInstance();
    }

    public synchronized JavaPlatform[] getInstalledPlatforms() {
        JavaPlatform[] arrjavaPlatform;
        if (this.cachedPlatforms == null) {
            LinkedHashSet<JavaPlatform> _cachedPlatforms = new LinkedHashSet<JavaPlatform>();
            for (JavaPlatformProvider provider : this.getProviders()) {
                _cachedPlatforms.addAll(Arrays.asList(provider.getInstalledPlatforms()));
            }
            this.cachedPlatforms = _cachedPlatforms;
        }
        if (this.cachedPlatforms.isEmpty()) {
            JavaPlatform[] arrjavaPlatform2 = new JavaPlatform[1];
            arrjavaPlatform = arrjavaPlatform2;
            arrjavaPlatform2[0] = FallbackDefaultJavaPlatform.getInstance();
        } else {
            arrjavaPlatform = this.cachedPlatforms.toArray(new JavaPlatform[this.cachedPlatforms.size()]);
        }
        return arrjavaPlatform;
    }

    public JavaPlatform[] getPlatforms(String platformDisplayName, Specification platformSpec) {
        ArrayList<JavaPlatform> result = new ArrayList<JavaPlatform>();
        for (JavaPlatform platform : this.getInstalledPlatforms()) {
            Specification spec;
            String name = platformDisplayName == null ? null : platform.getDisplayName();
            Specification specification = spec = platformSpec == null ? null : platform.getSpecification();
            if (platformDisplayName != null && !name.equalsIgnoreCase(platformDisplayName) || platformSpec != null && !JavaPlatformManager.compatible(spec, platformSpec)) continue;
            result.add(platform);
        }
        return result.toArray(new JavaPlatform[result.size()]);
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        assert (l != null);
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        assert (l != null);
        this.pcs.removePropertyChangeListener(l);
    }

    private void firePropertyChange(String property) {
        this.pcs.firePropertyChange(property, null, null);
    }

    private static boolean compatible(Specification platformSpec, Specification query) {
        String name = query.getName();
        SpecificationVersion version = query.getVersion();
        return !(name != null && !name.equalsIgnoreCase(platformSpec.getName()) || version != null && !version.equals((Object)platformSpec.getVersion()) || !JavaPlatformManager.compatibleProfiles(platformSpec.getProfiles(), query.getProfiles()));
    }

    private static boolean compatibleProfiles(Profile[] platformProfiles, Profile[] query) {
        if (query == null) {
            return true;
        }
        if (platformProfiles == null) {
            return false;
        }
        HashSet<Profile> covered = new HashSet<Profile>();
        for (Profile pattern : query) {
            boolean found = false;
            for (Profile p : platformProfiles) {
                if (!JavaPlatformManager.compatibleProfile(p, pattern)) continue;
                found = true;
                covered.add(p);
            }
            if (found) continue;
            return false;
        }
        return covered.size() == platformProfiles.length;
    }

    private static boolean compatibleProfile(Profile platformProfile, Profile query) {
        String name = query.getName();
        SpecificationVersion version = query.getVersion();
        return !(name != null && !name.equals(platformProfile.getName()) || version != null && !version.equals((Object)platformProfile.getVersion()));
    }

    private synchronized Collection<? extends JavaPlatformProvider> getProviders() {
        if (!this.providersValid) {
            if (this.providers == null) {
                this.providers = Lookup.getDefault().lookupResult(JavaPlatformProvider.class);
                this.providers.addLookupListener(new LookupListener(){

                    public void resultChanged(LookupEvent ev) {
                        JavaPlatformManager.this.resetCache(true);
                        JavaPlatformManager.this.firePropertyChange("installedPlatforms");
                    }
                });
            }
            if (this.pListener == null) {
                this.pListener = new PropertyChangeListener(){

                    @Override
                    public void propertyChange(PropertyChangeEvent evt) {
                        JavaPlatformManager.this.resetCache(false);
                        JavaPlatformManager.this.firePropertyChange("installedPlatforms");
                    }
                };
            }
            Collection instances = this.providers.allInstances();
            HashSet toAdd = new HashSet(instances);
            toAdd.removeAll(this.lastProviders);
            HashSet<? extends JavaPlatformProvider> toRemove = new HashSet<JavaPlatformProvider>(this.lastProviders);
            toRemove.removeAll(instances);
            for (JavaPlatformProvider provider2 : toRemove) {
                provider2.removePropertyChangeListener(this.pListener);
            }
            for (JavaPlatformProvider provider : toAdd) {
                provider.addPropertyChangeListener(this.pListener);
            }
            this.lastProviders = instances;
            this.providersValid = true;
        }
        return this.lastProviders;
    }

    private synchronized void resetCache(boolean resetProviders) {
        this.cachedPlatforms = null;
        this.providersValid &= !resetProviders;
    }

}

