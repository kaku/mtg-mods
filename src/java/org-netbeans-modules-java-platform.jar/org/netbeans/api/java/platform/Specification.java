/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.modules.SpecificationVersion
 */
package org.netbeans.api.java.platform;

import java.util.Locale;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.platform.Profile;
import org.openide.modules.SpecificationVersion;

public final class Specification {
    private final String name;
    private final SpecificationVersion version;
    private final Profile[] profiles;
    private final String displayName;

    public Specification(@NullAllowed String name, @NullAllowed SpecificationVersion version) {
        this(name, version, null, null);
    }

    public Specification(@NullAllowed String name, @NullAllowed SpecificationVersion version, @NullAllowed Profile[] profiles) {
        this(name, version, null, profiles);
    }

    public Specification(@NullAllowed String name, @NullAllowed SpecificationVersion version, @NullAllowed String displayName, @NullAllowed Profile[] profiles) {
        this.name = name;
        this.version = version;
        this.displayName = displayName;
        this.profiles = profiles;
    }

    public final String getName() {
        return this.name;
    }

    public final SpecificationVersion getVersion() {
        return this.version;
    }

    public final Profile[] getProfiles() {
        return this.profiles;
    }

    @NonNull
    public String getDisplayName() {
        if (this.displayName != null) {
            return this.displayName;
        }
        String defaultName = this.getName();
        return defaultName == null ? "" : defaultName.toUpperCase(Locale.ENGLISH);
    }

    public int hashCode() {
        int hc = 0;
        if (this.name != null) {
            hc = this.name.hashCode() << 16;
        }
        if (this.version != null) {
            hc += this.version.hashCode();
        }
        return hc;
    }

    public boolean equals(Object other) {
        if (other instanceof Specification) {
            boolean re;
            Specification os = (Specification)other;
            boolean bl = this.name == null ? os.name == null : (this.name.equals(os.name) && this.version == null ? os.version == null : (re = this.version.equals((Object)os.version)));
            if (!re || this.profiles == null) {
                return re;
            }
            if (os.profiles == null || this.profiles.length != os.profiles.length) {
                return false;
            }
            for (int i = 0; i < os.profiles.length; ++i) {
                re &= this.profiles[i].equals(os.profiles[i]);
            }
            return re;
        }
        return false;
    }

    public String toString() {
        String str = this.name == null ? "" : this.name + " ";
        str = str + (this.version == null ? "" : new StringBuilder().append((Object)this.version).append(" ").toString());
        if (this.profiles != null) {
            str = str + "[";
            for (int i = 0; i < this.profiles.length; ++i) {
                str = str + this.profiles[i] + " ";
            }
            str = str + "]";
        }
        return str;
    }
}

