/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.SpecificationVersion
 */
package org.netbeans.api.java.platform;

import org.openide.modules.SpecificationVersion;

public class Profile {
    private String name;
    private SpecificationVersion version;

    public Profile(String name, SpecificationVersion version) {
        this.name = name;
        this.version = version;
    }

    public final String getName() {
        return this.name;
    }

    public final SpecificationVersion getVersion() {
        return this.version;
    }

    public int hashCode() {
        int hc = 0;
        if (this.name != null) {
            hc = this.name.hashCode() << 16;
        }
        if (this.version != null) {
            hc += this.version.hashCode();
        }
        return hc;
    }

    public boolean equals(Object other) {
        if (other instanceof Profile) {
            Profile op = (Profile)other;
            return this.name == null ? op.name == null : (this.name.equals(op.name) && this.version == null ? op.version == null : this.version.equals((Object)op.version));
        }
        return false;
    }

    public String toString() {
        String str = this.name == null ? "" : this.name;
        str = str + (new StringBuilder().append(" ").append((Object)this.version).toString() == null ? "" : this.version.toString());
        return str;
    }
}

