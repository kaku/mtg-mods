/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 */
package org.netbeans.api.java.platform;

import java.awt.Dialog;
import java.awt.event.ActionListener;
import javax.accessibility.AccessibleContext;
import javax.swing.JButton;
import org.netbeans.api.java.platform.JavaPlatform;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

public final class PlatformsCustomizer {
    private PlatformsCustomizer() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static boolean showCustomizer(JavaPlatform platform) {
        org.netbeans.modules.java.platform.ui.PlatformsCustomizer customizer = new org.netbeans.modules.java.platform.ui.PlatformsCustomizer(platform);
        JButton close = new JButton(NbBundle.getMessage(PlatformsCustomizer.class, (String)"CTL_Close"));
        close.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(PlatformsCustomizer.class, (String)"AD_Close"));
        DialogDescriptor descriptor = new DialogDescriptor((Object)customizer, NbBundle.getMessage(PlatformsCustomizer.class, (String)"TXT_PlatformsManager"), true, new Object[]{close}, (Object)close, 0, new HelpCtx(PlatformsCustomizer.class), null);
        Dialog dlg = null;
        try {
            dlg = DialogDisplayer.getDefault().createDialog(descriptor);
            dlg.setVisible(true);
        }
        finally {
            if (dlg != null) {
                dlg.dispose();
            }
        }
        return true;
    }
}

