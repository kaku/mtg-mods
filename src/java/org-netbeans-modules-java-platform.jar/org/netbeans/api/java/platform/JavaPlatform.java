/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.api.java.platform;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.platform.JavaPlatformManager;
import org.netbeans.api.java.platform.Specification;
import org.openide.filesystems.FileObject;

public abstract class JavaPlatform {
    public static final String PROP_DISPLAY_NAME = "displayName";
    public static final String PROP_SOURCE_FOLDER = "sourceFolders";
    public static final String PROP_JAVADOC_FOLDER = "javadocFolders";
    public static final String PROP_SYSTEM_PROPERTIES = "systemProperties";
    private Map<String, String> sysproperties = Collections.emptyMap();
    private PropertyChangeSupport supp;

    protected JavaPlatform() {
    }

    public abstract String getDisplayName();

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final void addPropertyChangeListener(PropertyChangeListener l) {
        JavaPlatform javaPlatform = this;
        synchronized (javaPlatform) {
            if (this.supp == null) {
                this.supp = new PropertyChangeSupport(this);
            }
        }
        this.supp.addPropertyChangeListener(l);
    }

    public final void removePropertyChangeListener(PropertyChangeListener l) {
        if (this.supp != null) {
            this.supp.removePropertyChangeListener(l);
        }
    }

    public abstract Map<String, String> getProperties();

    public final Map<String, String> getSystemProperties() {
        return this.sysproperties;
    }

    public abstract ClassPath getBootstrapLibraries();

    public abstract ClassPath getStandardLibraries();

    public abstract String getVendor();

    public abstract Specification getSpecification();

    public abstract Collection<FileObject> getInstallFolders();

    public abstract FileObject findTool(String var1);

    public abstract ClassPath getSourceFolders();

    public abstract List<URL> getJavadocFolders();

    public static JavaPlatform getDefault() {
        return JavaPlatformManager.getDefault().getDefaultPlatform();
    }

    protected final void firePropertyChange(String propName, Object oldValue, Object newValue) {
        if (this.supp != null) {
            this.supp.firePropertyChange(propName, oldValue, newValue);
        }
    }

    protected final void setSystemProperties(Map<String, String> sysproperties) {
        this.sysproperties = Collections.unmodifiableMap(sysproperties);
        this.firePropertyChange("systemProperties", null, null);
    }
}

