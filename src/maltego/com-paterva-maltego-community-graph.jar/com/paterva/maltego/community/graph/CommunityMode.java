/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.community.graph;

public class CommunityMode {
    private static boolean _isPublicMode = false;

    public static boolean isIsPublicMode() {
        return _isPublicMode;
    }

    public static void setIsPublicMode(boolean bl) {
        _isPublicMode = bl;
    }
}

