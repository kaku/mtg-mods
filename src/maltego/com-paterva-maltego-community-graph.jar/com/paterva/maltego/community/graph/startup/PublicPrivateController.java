/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 */
package com.paterva.maltego.community.graph.startup;

import com.paterva.maltego.community.graph.CommunityMode;
import com.paterva.maltego.community.graph.startup.PublicPrivatePanel;
import java.awt.Dialog;
import javax.swing.JDialog;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;

public class PublicPrivateController {
    public void show() {
        PublicPrivatePanel publicPrivatePanel = new PublicPrivatePanel();
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)publicPrivatePanel, "Choose a Mode");
        Object[] arrobject = new Object[]{};
        dialogDescriptor.setOptions(arrobject);
        dialogDescriptor.setClosingOptions(arrobject);
        JDialog jDialog = (JDialog)DialogDisplayer.getDefault().createDialog(dialogDescriptor);
        jDialog.setDefaultCloseOperation(0);
        KeyStroke keyStroke = KeyStroke.getKeyStroke(27, 0);
        jDialog.getRootPane().unregisterKeyboardAction(keyStroke);
        jDialog.setVisible(true);
        CommunityMode.setIsPublicMode(publicPrivatePanel.isPublicMode());
    }
}

