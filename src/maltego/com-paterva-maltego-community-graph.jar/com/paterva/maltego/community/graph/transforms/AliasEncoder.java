/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.community.graph.transforms;

import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Random;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

class AliasEncoder {
    private Random _rand = new Random();
    private static final String COMPONENT1 = "KCUgJSslPys";
    private static final String COMPONENT2 = "LT8uICM5";

    public String encode(String string) throws GeneralSecurityException {
        return new BASE64Encoder().encode(this.cryptIt(this.randomChars(6) + string));
    }

    private byte[] cryptIt(String string) throws GeneralSecurityException {
        try {
            String string2 = AliasEncoder.getVector();
            String string3 = AliasEncoder.getKey();
            SecretKeySpec secretKeySpec = new SecretKeySpec(string3.getBytes("UTF-8"), "DESede");
            IvParameterSpec ivParameterSpec = new IvParameterSpec(string2.getBytes("UTF-8"));
            Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
            cipher.init(1, (Key)secretKeySpec, ivParameterSpec);
            return cipher.doFinal(string.getBytes("UTF-8"));
        }
        catch (Exception var2_3) {
            throw new GeneralSecurityException(var2_3);
        }
    }

    private String randomChars(int n) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < n; ++i) {
            stringBuilder.append(this.nextChar());
        }
        return stringBuilder.toString();
    }

    private String nextChar() {
        String string = Integer.toString(this._rand.nextInt(36), 36);
        return this._rand.nextBoolean() ? string : string.toUpperCase();
    }

    private static String getKey() {
        String string = "KCUgJSslPys+IykiKSIoJSs+" + "LT8uICM5".substring(0, 8);
        int n = 76;
        return AliasEncoder.decodeObfuscation(string, n);
    }

    private static String getVector() {
        String string = "LT8uICM50dRSxFMT0KCUgJSslPys";
        String string2 = "" + (char)(string.charAt(5) - '\u0001') + (char)(string.charAt(20) + 16) + string.substring(16, 18).replace(string.charAt(8), 'o') + string.substring(13, 21).replaceFirst("(.).*(.)", "$1$2") + string.indexOf("I") + (char)(string.length() * 3 - "KCUgJSslPys".length() + 1) + Integer.toHexString(string.charAt(5) * 3 + "LT8uICM5".length() * 3 - 6).toUpperCase() + string.charAt(19) + (char)(string.indexOf(115) * 3 - 8);
        int n = 101;
        return AliasEncoder.decodeObfuscation(string2, n);
    }

    private static String decodeObfuscation(String string, int n) {
        String string2;
        BASE64Decoder bASE64Decoder = new BASE64Decoder();
        try {
            string2 = new String(bASE64Decoder.decodeBuffer(string));
        }
        catch (Exception var4_4) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < string2.length(); ++i) {
            stringBuilder.append((char)(string2.charAt(i) ^ n));
        }
        return stringBuilder.toString();
    }
}

