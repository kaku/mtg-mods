/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.GotoUrlAction
 *  com.paterva.maltego.util.ui.LinkLabel
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.community.graph.startup;

import com.paterva.maltego.util.ui.GotoUrlAction;
import com.paterva.maltego.util.ui.LinkLabel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

class PublicPrivatePanel
extends JPanel {
    private boolean _publicMode = true;
    private JButton _privateButton;
    private JButton _publicButton;
    private JLabel _readMoreLabel;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JTextArea jTextArea1;
    private JTextArea jTextArea2;

    public PublicPrivatePanel() {
        this.initComponents();
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        LinkLabel linkLabel = (LinkLabel)this._readMoreLabel;
        linkLabel.setForeground(uIDefaults.getColor("transform-manager-highlight-title-colour1"));
        linkLabel.setHighlightColor(uIDefaults.getColor("transform-manager-highlight-title-colour1").brighter());
        linkLabel.addActionListener((ActionListener)new GotoUrlAction("http://www.paterva.com/redirect/publicmode.html"));
        this._publicButton.setText("");
        this._publicButton.setIcon(ImageUtilities.loadImageIcon((String)"com/paterva/maltego/community/graph/startup/PublicMode.png", (boolean)true));
        this._privateButton.setText("");
        this._privateButton.setIcon(ImageUtilities.loadImageIcon((String)"com/paterva/maltego/community/graph/startup/PrivateMode.png", (boolean)true));
    }

    public boolean isPublicMode() {
        return this._publicMode;
    }

    private void initComponents() {
        this._publicButton = new JButton();
        this._privateButton = new JButton();
        this._readMoreLabel = new LinkLabel();
        this.jLabel1 = new JLabel();
        this.jLabel2 = new JLabel();
        this.jLabel3 = new JLabel();
        this.jTextArea1 = new JTextArea();
        this.jTextArea2 = new JTextArea();
        this.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setMinimumSize(new Dimension(660, 320));
        this.setPreferredSize(new Dimension(660, 320));
        this.setLayout(new GridBagLayout());
        this._publicButton.setText(NbBundle.getMessage(PublicPrivatePanel.class, (String)"PublicPrivatePanel._publicButton.text"));
        this._publicButton.setMaximumSize(new Dimension(160, 160));
        this._publicButton.setMinimumSize(new Dimension(160, 160));
        this._publicButton.setPreferredSize(new Dimension(160, 160));
        this._publicButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                PublicPrivatePanel.this._publicButtonActionPerformed(actionEvent);
            }
        });
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);
        this.add((Component)this._publicButton, gridBagConstraints);
        this._privateButton.setText(NbBundle.getMessage(PublicPrivatePanel.class, (String)"PublicPrivatePanel._privateButton.text"));
        this._privateButton.setMaximumSize(new Dimension(160, 160));
        this._privateButton.setMinimumSize(new Dimension(160, 160));
        this._privateButton.setPreferredSize(new Dimension(160, 160));
        this._privateButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                PublicPrivatePanel.this._privateButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 5;
        gridBagConstraints.ipady = 5;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);
        this.add((Component)this._privateButton, gridBagConstraints);
        this._readMoreLabel.setText(NbBundle.getMessage(PublicPrivatePanel.class, (String)"PublicPrivatePanel._readMoreLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = 12;
        gridBagConstraints.insets = new Insets(0, 8, 0, 8);
        this.add((Component)this._readMoreLabel, gridBagConstraints);
        this.jLabel1.setFont(this.jLabel1.getFont().deriveFont(this.jLabel1.getFont().getStyle() | 1, this.jLabel1.getFont().getSize() + 1));
        this.jLabel1.setText(NbBundle.getMessage(PublicPrivatePanel.class, (String)"PublicPrivatePanel.jLabel1.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 0, 10, 0);
        this.add((Component)this.jLabel1, gridBagConstraints);
        this.jLabel2.setFont(this.jLabel2.getFont().deriveFont(this.jLabel2.getFont().getStyle() | 1, this.jLabel2.getFont().getSize() + 1));
        this.jLabel2.setText(NbBundle.getMessage(PublicPrivatePanel.class, (String)"PublicPrivatePanel.jLabel2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        this.add((Component)this.jLabel2, gridBagConstraints);
        this.jLabel3.setFont(this.jLabel3.getFont().deriveFont(this.jLabel3.getFont().getStyle() | 1, this.jLabel3.getFont().getSize() + 1));
        this.jLabel3.setText(NbBundle.getMessage(PublicPrivatePanel.class, (String)"PublicPrivatePanel.jLabel3.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        this.add((Component)this.jLabel3, gridBagConstraints);
        this.jTextArea1.setBackground(new JPanel().getBackground());
        this.jTextArea1.setColumns(20);
        this.jTextArea1.setLineWrap(true);
        this.jTextArea1.setRows(1);
        this.jTextArea1.setText(NbBundle.getMessage(PublicPrivatePanel.class, (String)"PublicPrivatePanel.jTextArea1.text"));
        this.jTextArea1.setWrapStyleWord(true);
        this.jTextArea1.setMaximumSize(new Dimension(Integer.MAX_VALUE, 50));
        this.jTextArea1.setMinimumSize(new Dimension(300, 50));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new Insets(5, 10, 5, 5);
        this.add((Component)this.jTextArea1, gridBagConstraints);
        this.jTextArea2.setBackground(new JPanel().getBackground());
        this.jTextArea2.setColumns(20);
        this.jTextArea2.setLineWrap(true);
        this.jTextArea2.setRows(1);
        this.jTextArea2.setText(NbBundle.getMessage(PublicPrivatePanel.class, (String)"PublicPrivatePanel.jTextArea2.text"));
        this.jTextArea2.setWrapStyleWord(true);
        this.jTextArea2.setMaximumSize(new Dimension(Integer.MAX_VALUE, 50));
        this.jTextArea2.setMinimumSize(new Dimension(300, 50));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new Insets(5, 5, 5, 10);
        this.add((Component)this.jTextArea2, gridBagConstraints);
    }

    private void _publicButtonActionPerformed(ActionEvent actionEvent) {
        this._publicMode = true;
        this.close();
    }

    private void _privateButtonActionPerformed(ActionEvent actionEvent) {
        this._publicMode = false;
        this.close();
    }

    private void close() {
        Container container = this;
        while (!((container = container.getParent()) instanceof JDialog)) {
        }
        JDialog jDialog = (JDialog)container;
        jDialog.dispose();
    }

}

