/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.runner.api.TransformRunRequestDecorator
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.community.graph.transforms;

import com.paterva.maltego.community.graph.CommunityMode;
import com.paterva.maltego.community.graph.transforms.AliasEncoder;
import com.paterva.maltego.transform.runner.api.TransformRunRequestDecorator;
import com.paterva.maltego.util.StringUtilities;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import org.openide.util.Exceptions;

public class PublicTransformDecorator
extends TransformRunRequestDecorator {
    public String getDecorations(String string) {
        String string2;
        String string3 = "";
        if (CommunityMode.isIsPublicMode() && !StringUtilities.isNullOrEmpty((String)(string2 = System.getProperty("maltego.registeredto.alias", "")))) {
            if (!StringUtilities.isNullOrEmpty((String)string)) {
                string2 = string2 + "|" + string;
            }
            AliasEncoder aliasEncoder = new AliasEncoder();
            try {
                string3 = "&Context=" + URLEncoder.encode(aliasEncoder.encode(string2), "UTF-8");
            }
            catch (UnsupportedEncodingException var5_5) {
                Exceptions.printStackTrace((Throwable)var5_5);
            }
            catch (GeneralSecurityException var5_6) {
                Exceptions.printStackTrace((Throwable)var5_6);
            }
        }
        return string3;
    }
}

