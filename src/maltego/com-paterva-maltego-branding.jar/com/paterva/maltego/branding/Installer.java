/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.Version
 *  org.openide.modules.ModuleInstall
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.branding;

import com.paterva.maltego.util.Version;
import org.openide.modules.ModuleInstall;
import org.openide.util.NbBundle;

public class Installer
extends ModuleInstall {
    public void restored() {
        System.setProperty("maltego.product-name", NbBundle.getMessage(Installer.class, (String)"ProductName"));
        System.setProperty("maltego.branding-code", NbBundle.getMessage(Installer.class, (String)"Branding"));
        System.setProperty("maltego.goto-url-postfix", "_CE");
        Version.setReady((boolean)true);
    }
}

