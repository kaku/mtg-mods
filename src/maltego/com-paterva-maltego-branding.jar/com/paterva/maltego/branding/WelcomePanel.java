/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.fonts.FontUtils
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.branding;

import com.paterva.maltego.util.ui.fonts.FontUtils;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.LayoutManager;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle;
import javax.swing.border.Border;
import org.openide.util.NbBundle;

class WelcomePanel
extends JPanel {
    private JLabel jLabel1;
    private JScrollPane jScrollPane1;
    private JTextArea jTextArea1;

    public WelcomePanel() {
        this.initComponents();
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this.jTextArea1 = new JTextArea();
        this.jLabel1.setFont(FontUtils.defaultStyledScaled((int)1, (float)2.0f));
        this.jLabel1.setText(NbBundle.getMessage(WelcomePanel.class, (String)"WelcomePanel.jLabel1.text"));
        this.jScrollPane1.setBorder(null);
        this.jTextArea1.setEditable(false);
        this.jTextArea1.setBackground(this.getBackground());
        this.jTextArea1.setColumns(20);
        this.jTextArea1.setLineWrap(true);
        this.jTextArea1.setRows(5);
        this.jTextArea1.setText(NbBundle.getMessage(WelcomePanel.class, (String)"WelcomePanel.jTextArea1.text"));
        this.jTextArea1.setWrapStyleWord(true);
        this.jTextArea1.setBorder(null);
        this.jTextArea1.setFocusable(false);
        this.jScrollPane1.setViewportView(this.jTextArea1);
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane1, -1, 385, 32767).addComponent(this.jLabel1)).addContainerGap()));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(this.jLabel1).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.jScrollPane1, -1, 154, 32767).addContainerGap()));
    }
}

