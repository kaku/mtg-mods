/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.ArrayWizardSegment
 *  com.paterva.maltego.util.ui.dialog.DisplayController
 *  com.paterva.maltego.util.ui.dialog.WizardSegment
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.branding;

import com.paterva.maltego.branding.WelcomePanel;
import com.paterva.maltego.util.ui.dialog.ArrayWizardSegment;
import com.paterva.maltego.util.ui.dialog.DisplayController;
import com.paterva.maltego.util.ui.dialog.WizardSegment;
import java.util.Map;
import java.util.prefs.Preferences;
import javax.swing.JComponent;
import org.openide.WizardDescriptor;
import org.openide.util.NbPreferences;

public class WelcomeSegment
extends ArrayWizardSegment {
    private WizardDescriptor.Panel[] _panels;

    private WelcomeSegment(Map map) {
        super(map);
    }

    public WizardDescriptor.Panel[] getPanels() {
        if (this._panels == null) {
            DisplayController displayController = new DisplayController((JComponent)new WelcomePanel());
            displayController.setName("Welcome");
            this._panels = new WizardDescriptor.Panel[]{displayController};
        }
        return this._panels;
    }

    public void handleFinish(WizardDescriptor wizardDescriptor) {
        Preferences preferences = NbPreferences.forModule(WelcomeSegment.class);
        preferences.putBoolean("maltego.ce.welcome-completed", true);
    }

    public static WizardSegment segment(Map map) {
        Preferences preferences = NbPreferences.forModule(WelcomeSegment.class);
        if (preferences.getBoolean("maltego.ce.welcome-completed", false)) {
            return null;
        }
        return new WelcomeSegment(map);
    }
}

