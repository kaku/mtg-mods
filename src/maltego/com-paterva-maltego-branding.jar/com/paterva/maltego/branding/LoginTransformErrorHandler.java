/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.api.TransformMessage
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.runner.api.TransformErrorHandler
 *  com.paterva.maltego.transform.runner.api.TransformRunContext
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  com.paterva.maltego.welcome.login.LoginSegment
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.branding;

import com.paterva.maltego.transform.api.TransformMessage;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.runner.api.TransformErrorHandler;
import com.paterva.maltego.transform.runner.api.TransformRunContext;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import com.paterva.maltego.welcome.login.LoginSegment;
import org.openide.WizardDescriptor;

public class LoginTransformErrorHandler
implements TransformErrorHandler {
    public boolean handle(TransformRunContext transformRunContext, TransformDefinition transformDefinition, TransformMessage transformMessage) {
        if (transformMessage.getCode() == 500) {
            WizardUtilities.runWizard((WizardDescriptor)LoginSegment.createWizard());
            return true;
        }
        return false;
    }
}

