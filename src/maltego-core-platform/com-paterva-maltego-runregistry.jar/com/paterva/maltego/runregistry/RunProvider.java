/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 */
package com.paterva.maltego.runregistry;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.runregistry.item.RunProviderItem;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Set;

public interface RunProvider {
    public static final String PROP_ITEMS_CHANGED = "itemsChanged";

    public int getPosition();

    public List<RunProviderItem> getItems();

    public void run(List<RunProviderItem> var1, GraphID var2, Set<EntityID> var3);

    public boolean isUpdating();

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

