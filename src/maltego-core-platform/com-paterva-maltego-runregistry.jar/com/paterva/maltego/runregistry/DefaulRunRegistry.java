/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.runregistry;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.runregistry.RunProvider;
import com.paterva.maltego.runregistry.RunProviderComparator;
import com.paterva.maltego.runregistry.RunRegistry;
import com.paterva.maltego.runregistry.item.RunProviderItem;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.openide.util.Lookup;

class DefaulRunRegistry
extends RunRegistry {
    private final RunProvider[] _providers = Lookup.getDefault().lookupAll(RunProvider.class).toArray(new RunProvider[0]);

    public DefaulRunRegistry() {
        Arrays.sort(this._providers, new RunProviderComparator());
    }

    @Override
    public List<RunProviderItem> getItems() {
        ArrayList<RunProviderItem> arrayList = new ArrayList<RunProviderItem>();
        for (RunProvider runProvider : this._providers) {
            arrayList.addAll(runProvider.getItems());
        }
        return arrayList;
    }

    @Override
    public void run(List<RunProviderItem> list, GraphID graphID, Set<EntityID> set) {
        for (RunProvider runProvider : this._providers) {
            runProvider.run(list, graphID, set);
        }
    }

    @Override
    public boolean isUpdating() {
        for (RunProvider runProvider : this._providers) {
            if (!runProvider.isUpdating()) continue;
            return true;
        }
        return false;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        for (RunProvider runProvider : this._providers) {
            runProvider.addPropertyChangeListener(propertyChangeListener);
        }
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        for (RunProvider runProvider : this._providers) {
            runProvider.removePropertyChangeListener(propertyChangeListener);
        }
    }

    @Override
    public int getPosition() {
        return 0;
    }
}

