/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.runregistry;

import com.paterva.maltego.runregistry.DefaulRunRegistry;
import com.paterva.maltego.runregistry.RunProvider;
import org.openide.util.Lookup;

public abstract class RunRegistry
implements RunProvider {
    public static RunRegistry _default;

    public static synchronized RunRegistry getDefault() {
        if (_default == null && (RunRegistry._default = (RunRegistry)Lookup.getDefault().lookup(RunRegistry.class)) == null) {
            _default = new DefaulRunRegistry();
        }
        return _default;
    }
}

