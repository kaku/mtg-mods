/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.runregistry;

import com.paterva.maltego.runregistry.RunProvider;
import java.util.Comparator;

public class RunProviderComparator
implements Comparator<RunProvider> {
    @Override
    public int compare(RunProvider runProvider, RunProvider runProvider2) {
        return runProvider.getPosition() - runProvider2.getPosition();
    }
}

