/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.runregistry.item;

import com.paterva.maltego.runregistry.item.AbstractRunnableItem;
import com.paterva.maltego.runregistry.item.RunCategoryItem;

public abstract class AbstractRunCategoryItem
extends AbstractRunnableItem
implements RunCategoryItem {
    @Override
    public boolean canFavorite() {
        return false;
    }

    @Override
    public void setFavorite(boolean bl) {
    }

    @Override
    public boolean isFavorite() {
        return false;
    }
}

