/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 */
package com.paterva.maltego.runregistry.item;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.runregistry.item.AbstractRunProviderItem;
import com.paterva.maltego.runregistry.item.RunnableItem;
import java.util.Set;

public abstract class AbstractRunnableItem
extends AbstractRunProviderItem
implements RunnableItem {
    @Override
    public boolean canRun() {
        return false;
    }

    @Override
    public void run(GraphID graphID, Set<EntityID> set) {
    }

    @Override
    public boolean hasSettings() {
        return false;
    }

    @Override
    public void showSettings() {
    }

    @Override
    public boolean hasConfig() {
        return false;
    }

    @Override
    public void showConfig() {
    }

    @Override
    public boolean canFavorite() {
        return true;
    }
}

