/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.runregistry.item;

import com.paterva.maltego.runregistry.item.RunProviderItem;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collections;
import java.util.List;
import javax.swing.Action;
import javax.swing.Icon;

public abstract class AbstractRunProviderItem
implements RunProviderItem {
    private final PropertyChangeSupport _changeSupport;

    public AbstractRunProviderItem() {
        this._changeSupport = new PropertyChangeSupport(this);
    }

    @Override
    public String getDisplayName() {
        return this.getName();
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public Icon getIcon() {
        return null;
    }

    @Override
    public boolean isExpandedByDefault() {
        return false;
    }

    @Override
    public List<Action> getContextActions() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public List<Action> getToolbarActions() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public boolean isShowIn(String string) {
        return true;
    }

    @Override
    public boolean isRememberPage() {
        return true;
    }

    protected void fireItemChanged() {
        this._changeSupport.firePropertyChange("run.provider.item.changed", null, this);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }
}

