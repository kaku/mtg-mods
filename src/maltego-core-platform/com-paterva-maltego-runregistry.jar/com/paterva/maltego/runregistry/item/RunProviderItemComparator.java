/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.runregistry.item;

import com.paterva.maltego.runregistry.item.RunProviderItem;
import java.util.Comparator;

public class RunProviderItemComparator
implements Comparator<RunProviderItem> {
    @Override
    public int compare(RunProviderItem runProviderItem, RunProviderItem runProviderItem2) {
        return runProviderItem.getDisplayName().compareToIgnoreCase(runProviderItem2.getDisplayName());
    }
}

