/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.runregistry.item;

import java.beans.PropertyChangeListener;
import java.util.List;
import javax.swing.Action;
import javax.swing.Icon;

public interface RunProviderItem {
    public static final String PROP_ITEM_CHANGED = "run.provider.item.changed";
    public static final String COMPONENT_RUN_VIEW = "run-view";
    public static final String COMPONENT_CONTEXT_MENU = "context-menu";
    public static final String BUILTIN_PREFIX = "maltego.builtin.";

    public String getName();

    public String getDisplayName();

    public String getDescription();

    public Icon getIcon();

    public String getLafPrefix();

    public boolean isExpandedByDefault();

    public List<Action> getContextActions();

    public List<Action> getToolbarActions();

    public boolean isShowIn(String var1);

    public boolean isRememberPage();

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

