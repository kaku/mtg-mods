/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 */
package com.paterva.maltego.runregistry.item;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.runregistry.item.RunProviderItem;
import java.util.List;
import java.util.Set;

public interface RunnableItem
extends RunProviderItem {
    public List<? extends RunProviderItem> getChildren();

    public boolean canRun();

    public void run(GraphID var1, Set<EntityID> var2);

    public boolean hasSettings();

    public void showSettings();

    public boolean hasConfig();

    public void showConfig();

    public boolean canFavorite();

    public void setFavorite(boolean var1);

    public boolean isFavorite();
}

