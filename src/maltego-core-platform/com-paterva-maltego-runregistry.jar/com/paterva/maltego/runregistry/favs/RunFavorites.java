/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.runregistry.favs;

import com.paterva.maltego.runregistry.RunProvider;
import com.paterva.maltego.runregistry.item.RunnableItem;
import java.util.List;
import org.openide.util.Lookup;

public abstract class RunFavorites {
    public static RunFavorites _default;

    public static synchronized RunFavorites getDefault() {
        if (_default == null && (RunFavorites._default = (RunFavorites)Lookup.getDefault().lookup(RunFavorites.class)) == null) {
            throw new IllegalStateException("Run favorites implementation not found.");
        }
        return _default;
    }

    public abstract void setFavorites(RunProvider var1, List<RunnableItem> var2);
}

