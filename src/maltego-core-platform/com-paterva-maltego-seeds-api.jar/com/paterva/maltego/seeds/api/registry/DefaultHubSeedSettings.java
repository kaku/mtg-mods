/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.descriptor.TransformSeedRepository
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.DisplayDescriptorList
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.seeds.api.registry;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.registry.HubSeedRegistry;
import com.paterva.maltego.seeds.api.registry.HubSeedSettings;
import com.paterva.maltego.seeds.api.registry.HubSeedSettingsAction;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.descriptor.TransformSeedRepository;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.DisplayDescriptorList;
import com.paterva.maltego.typing.PropertyDescriptor;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;
import org.openide.util.Lookup;
import org.openide.util.Utilities;

class DefaultHubSeedSettings
extends HubSeedSettings {
    private final TransformSeedRepository _repo;
    private final PropertyChangeSupport _changeSupport;
    private final HubSeedRegistry _registry;

    public DefaultHubSeedSettings(TransformSeedRepository transformSeedRepository) {
        this._changeSupport = new PropertyChangeSupport(this);
        this._registry = HubSeedRegistry.getDefault();
        this._repo = transformSeedRepository;
    }

    @Override
    public boolean isInstalled(HubSeedDescriptor hubSeedDescriptor) {
        TransformSeed transformSeed = this._registry.getTransformSeed(hubSeedDescriptor);
        return transformSeed.isEnabled();
    }

    @Override
    public void setInstalled(HubSeedDescriptor hubSeedDescriptor, boolean bl) {
        TransformSeed transformSeed = this._registry.getTransformSeed(hubSeedDescriptor);
        if (transformSeed.isEnabled() != bl) {
            transformSeed.setEnabled(bl);
            this.save(transformSeed);
            this._changeSupport.firePropertyChange("maltego.hub.installed", null, hubSeedDescriptor);
        }
    }

    @Override
    public DisplayDescriptorCollection getGlobalTransformProperties(HubSeedDescriptor hubSeedDescriptor) {
        TransformSeed transformSeed = this._registry.getTransformSeed(hubSeedDescriptor);
        return transformSeed.getGlobalTxProperties();
    }

    @Override
    public DisplayDescriptorCollection getGlobalTransformProperties(HubSeedDescriptor hubSeedDescriptor, TransformDefinition transformDefinition, boolean bl) {
        DisplayDescriptorCollection displayDescriptorCollection;
        DisplayDescriptorList displayDescriptorList = new DisplayDescriptorList();
        DisplayDescriptorCollection displayDescriptorCollection2 = this.getGlobalTransformProperties(hubSeedDescriptor);
        if (displayDescriptorCollection2 != null && (displayDescriptorCollection = transformDefinition.getProperties()) != null) {
            for (DisplayDescriptor displayDescriptor : displayDescriptorCollection2) {
                TransformPropertyDescriptor transformPropertyDescriptor;
                if (displayDescriptor == null || !displayDescriptorCollection.contains((transformPropertyDescriptor = (TransformPropertyDescriptor)displayDescriptor).getName()) || bl && !transformPropertyDescriptor.isAuth()) continue;
                displayDescriptorList.add((DisplayDescriptor)transformPropertyDescriptor);
            }
        }
        return displayDescriptorList;
    }

    @Override
    public DisplayDescriptorCollection getGlobalTransformProperties(TransformDefinition transformDefinition) {
        DisplayDescriptorList displayDescriptorList = new DisplayDescriptorList();
        for (HubSeedDescriptor hubSeedDescriptor : HubSeedRegistry.getDefault().getHubSeeds((TransformDescriptor)transformDefinition)) {
            for (DisplayDescriptor displayDescriptor : this.getGlobalTransformProperties(hubSeedDescriptor, transformDefinition, false)) {
                DisplayDescriptor displayDescriptor2;
                if (displayDescriptor == null || displayDescriptorList.contains(displayDescriptor.getName()) || (displayDescriptor2 = transformDefinition.getProperties().get(displayDescriptor.getName())) == null) continue;
                displayDescriptorList.add(displayDescriptor2);
            }
        }
        return displayDescriptorList;
    }

    @Override
    public DataSource getGlobalTransformSettings(HubSeedDescriptor hubSeedDescriptor) {
        return new HubSettingsDataSource(hubSeedDescriptor);
    }

    @Override
    public void save(HubSeedDescriptor hubSeedDescriptor) {
        this.save(this._registry.getTransformSeed(hubSeedDescriptor));
    }

    @Override
    public void show(HubSeedDescriptor hubSeedDescriptor) {
        HubSeedSettingsAction hubSeedSettingsAction = (HubSeedSettingsAction)Lookup.getDefault().lookup(HubSeedSettingsAction.class);
        if (hubSeedSettingsAction != null) {
            hubSeedSettingsAction.show(hubSeedDescriptor);
        }
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private void save(TransformSeed transformSeed) {
        this._repo.add(transformSeed);
    }

    private class HubSettingsDataSource
    implements DataSource {
        private final HubSeedDescriptor _hubSeed;

        public HubSettingsDataSource(HubSeedDescriptor hubSeedDescriptor) {
            this._hubSeed = hubSeedDescriptor;
        }

        public Object getValue(PropertyDescriptor propertyDescriptor) {
            return this.getDelegate().getValue(propertyDescriptor);
        }

        public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
            DataSource dataSource = this.getDelegate();
            Object object2 = dataSource.getValue(propertyDescriptor);
            if (!Utilities.compareObjects((Object)object2, (Object)object)) {
                dataSource.setValue(propertyDescriptor, object);
                DefaultHubSeedSettings.this._changeSupport.firePropertyChange(propertyDescriptor.getName(), object2, object);
            }
        }

        private DataSource getDelegate() {
            TransformSeed transformSeed = DefaultHubSeedSettings.this._registry.getTransformSeed(this._hubSeed);
            return transformSeed.getGlobalTxSettings();
        }
    }

}

