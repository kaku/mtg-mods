/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  com.paterva.maltego.util.StringUtilities
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.seeds.api;

import com.google.gson.annotations.SerializedName;
import com.paterva.maltego.util.StringUtilities;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="Provider", strict=0)
public class HubSeedProvider {
    @Element(name="Name", required=1)
    @SerializedName(value="Name")
    private String _name;
    @Element(name="Website", required=0)
    @SerializedName(value="Website")
    private String _website;
    @Element(name="Email", required=0)
    @SerializedName(value="Email")
    private String _email;
    @Element(name="Phone", required=0)
    @SerializedName(value="Phone")
    private String _phone;

    public String getName() {
        return this._name;
    }

    public String getWebsite() {
        return StringUtilities.trim((String)this._website);
    }

    public String getEmail() {
        return StringUtilities.trim((String)this._email);
    }

    public String getPhone() {
        return StringUtilities.trim((String)this._phone);
    }
}

