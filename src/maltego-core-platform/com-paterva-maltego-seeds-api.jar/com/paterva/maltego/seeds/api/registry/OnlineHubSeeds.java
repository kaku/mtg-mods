/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.Version
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.seeds.api.registry;

import com.paterva.maltego.util.Version;
import java.net.MalformedURLException;
import java.net.URL;
import org.openide.util.Exceptions;

public class OnlineHubSeeds {
    private static URL _url = OnlineHubSeeds.getDefaultUrl();

    public static synchronized void setUrl(URL uRL) {
        _url = uRL;
    }

    public static synchronized URL getUrl() {
        return _url;
    }

    private static synchronized URL getDefaultUrl() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("http://www.paterva.com/cgi-bin/hub.pl");
        Version.appendToUrl((StringBuilder)stringBuilder);
        URL uRL = null;
        try {
            uRL = new URL(stringBuilder.toString());
        }
        catch (MalformedURLException var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
        }
        return uRL;
    }
}

