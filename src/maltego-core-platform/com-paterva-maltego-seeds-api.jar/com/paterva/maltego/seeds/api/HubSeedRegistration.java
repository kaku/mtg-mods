/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  com.paterva.maltego.util.StringUtilities
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.seeds.api;

import com.google.gson.annotations.SerializedName;
import com.paterva.maltego.util.StringUtilities;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="Registration", strict=0)
public class HubSeedRegistration {
    @Element(name="Website", required=1)
    @SerializedName(value="Website")
    private String _website;

    public String getWebsite() {
        return StringUtilities.trim((String)this._website);
    }
}

