/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  com.paterva.maltego.imgfactoryapi.ImageFactory
 *  com.paterva.maltego.util.ImageCallback
 *  com.paterva.maltego.util.ImageUtils
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.Exceptions
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.seeds.api;

import com.google.gson.annotations.SerializedName;
import com.paterva.maltego.imgfactoryapi.ImageFactory;
import com.paterva.maltego.util.ImageCallback;
import com.paterva.maltego.util.ImageUtils;
import com.paterva.maltego.util.StringUtilities;
import java.awt.Image;
import java.io.IOException;
import javax.swing.ImageIcon;
import org.openide.util.Exceptions;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="Icon", strict=0)
public class HubSeedIcon {
    public static final transient int SIZE = 48;
    @Element(name="Url", required=0)
    @SerializedName(value="Url")
    private String _url;
    @Element(name="Base64", required=0)
    @SerializedName(value="Base64")
    private String _base64;
    private transient ImageIcon _img;

    public String getUrl() {
        return StringUtilities.trim((String)this._url);
    }

    public void setUrl(String string) {
        this._url = StringUtilities.trim((String)string);
        if (!StringUtilities.isNullOrEmpty((String)this._url)) {
            this._base64 = null;
        }
    }

    public String getBase64() {
        return this._base64;
    }

    public ImageIcon getImageIcon(ImageCallback imageCallback) {
        if (this._img == null) {
            String string = this.getUrl();
            if (!StringUtilities.isNullOrEmpty((String)string)) {
                this._img = ImageFactory.getDefault().getImageIcon((Object)string, 48, 48, imageCallback);
            } else {
                try {
                    this._img = new ImageIcon(ImageUtils.base64Decode((String)this._base64));
                }
                catch (IOException var3_3) {
                    Exceptions.printStackTrace((Throwable)var3_3);
                }
            }
        }
        return this._img;
    }
}

