/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.Gson
 *  com.google.gson.GsonBuilder
 *  com.paterva.maltego.util.XmlSerializer
 */
package com.paterva.maltego.seeds.api.serialize;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeeds;
import com.paterva.maltego.util.XmlSerializer;
import java.io.IOException;
import java.io.OutputStream;

public class HubSeedWriter {
    public void writeXml(HubSeeds hubSeeds, OutputStream outputStream) throws IOException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        xmlSerializer.write((Object)hubSeeds, outputStream);
    }

    public String toXml(HubSeeds hubSeeds) throws IOException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        return xmlSerializer.toString((Object)hubSeeds);
    }

    public void writeJson(HubSeeds hubSeeds, OutputStream outputStream) throws IOException {
        String string = this.toJson(hubSeeds);
        outputStream.write(string.getBytes("UTF-8"));
    }

    public String toJson(HubSeeds hubSeeds) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson((Object)hubSeeds);
    }

    public void writeXml(HubSeedDescriptor hubSeedDescriptor, OutputStream outputStream) throws IOException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        xmlSerializer.write((Object)hubSeedDescriptor, outputStream);
    }
}

