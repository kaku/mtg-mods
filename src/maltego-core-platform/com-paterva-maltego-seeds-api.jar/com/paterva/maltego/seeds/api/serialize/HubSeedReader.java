/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.Gson
 *  com.google.gson.JsonSyntaxException
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.XmlSerializationException
 *  com.paterva.maltego.util.XmlSerializer
 *  org.apache.commons.io.IOUtils
 */
package com.paterva.maltego.seeds.api.serialize;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeeds;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.XmlSerializationException;
import com.paterva.maltego.util.XmlSerializer;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.IOUtils;

public class HubSeedReader {
    public HubSeedDescriptor readSeed(InputStream inputStream) throws IOException {
        return this.readSeed(IOUtils.toString((InputStream)inputStream, (String)"UTF-8"));
    }

    public HubSeedDescriptor readSeed(String string) throws IOException {
        HubSeedDescriptor hubSeedDescriptor = null;
        if (!StringUtilities.isNullOrEmpty((String)string)) {
            hubSeedDescriptor = HubSeedReader.isXml(string = string.trim()) ? this.fromXmlSeed(string) : this.fromJsonSeed(string);
        }
        return hubSeedDescriptor;
    }

    public HubSeeds read(InputStream inputStream) throws IOException {
        return this.read(IOUtils.toString((InputStream)inputStream, (String)"UTF-8"));
    }

    public HubSeeds read(String string) throws IOException {
        HubSeeds hubSeeds = null;
        if (!StringUtilities.isNullOrEmpty((String)string)) {
            hubSeeds = HubSeedReader.isXml(string = string.trim()) ? this.fromXml(string) : this.fromJson(string);
        }
        return hubSeeds;
    }

    private HubSeedDescriptor fromJsonSeed(String string) throws JsonSyntaxException {
        Gson gson = new Gson();
        return (HubSeedDescriptor)gson.fromJson(string, HubSeedDescriptor.class);
    }

    private HubSeedDescriptor fromXmlSeed(String string) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        return (HubSeedDescriptor)xmlSerializer.read(HubSeedDescriptor.class, string);
    }

    private HubSeeds fromJson(String string) throws JsonSyntaxException {
        Gson gson = new Gson();
        return (HubSeeds)gson.fromJson(string, HubSeeds.class);
    }

    private HubSeeds fromXml(String string) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        return (HubSeeds)xmlSerializer.read(HubSeeds.class, string);
    }

    private static boolean isXml(String string) {
        return string.startsWith("<");
    }
}

