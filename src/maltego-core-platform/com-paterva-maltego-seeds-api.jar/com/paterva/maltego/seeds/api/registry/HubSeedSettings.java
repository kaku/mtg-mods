/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformSeedRepository
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.seeds.api.registry;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.registry.DefaultHubSeedSettings;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformSeedRepository;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import java.beans.PropertyChangeListener;
import org.openide.util.Lookup;

public abstract class HubSeedSettings {
    public static final String PROP_INSTALLED = "maltego.hub.installed";
    private static HubSeedSettings _default;

    public static synchronized HubSeedSettings getDefault() {
        if (_default == null && (HubSeedSettings._default = (HubSeedSettings)Lookup.getDefault().lookup(HubSeedSettings.class)) == null) {
            _default = new DefaultHubSeedSettings(TransformSeedRepository.getDefault());
        }
        return _default;
    }

    public abstract boolean isInstalled(HubSeedDescriptor var1);

    public abstract void setInstalled(HubSeedDescriptor var1, boolean var2);

    public abstract DisplayDescriptorCollection getGlobalTransformProperties(HubSeedDescriptor var1);

    public abstract DisplayDescriptorCollection getGlobalTransformProperties(HubSeedDescriptor var1, TransformDefinition var2, boolean var3);

    public abstract DisplayDescriptorCollection getGlobalTransformProperties(TransformDefinition var1);

    public abstract DataSource getGlobalTransformSettings(HubSeedDescriptor var1);

    public abstract void save(HubSeedDescriptor var1);

    public abstract void show(HubSeedDescriptor var1);

    public abstract void addPropertyChangeListener(PropertyChangeListener var1);

    public abstract void removePropertyChangeListener(PropertyChangeListener var1);
}

