/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.seeds.api.registry;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;

public interface HubSeedSettingsAction {
    public void show(HubSeedDescriptor var1);
}

