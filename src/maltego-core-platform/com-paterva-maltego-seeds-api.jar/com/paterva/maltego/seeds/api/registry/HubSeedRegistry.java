/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.seeds.api.registry;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeeds;
import com.paterva.maltego.seeds.api.registry.DefaultHubSeedRegistry;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Set;
import org.openide.util.Lookup;

public abstract class HubSeedRegistry {
    public static final String PROP_SEEDS_CHANGED = "seedsChanged";
    public static final String PROP_ONLINE_CHANGED = "onlineChanged";
    private static HubSeedRegistry _default;

    public static synchronized HubSeedRegistry getDefault() {
        if (_default == null && (HubSeedRegistry._default = (HubSeedRegistry)Lookup.getDefault().lookup(HubSeedRegistry.class)) == null) {
            _default = new DefaultHubSeedRegistry();
        }
        return _default;
    }

    public abstract boolean isOnline();

    public abstract HubSeeds getSeeds(boolean var1);

    public abstract void addSeed(HubSeedDescriptor var1);

    public abstract void removeSeed(HubSeedDescriptor var1, boolean var2);

    public abstract TransformSeed getTransformSeed(HubSeedDescriptor var1);

    public abstract HubSeedDescriptor getHubSeed(String var1);

    public abstract HubSeedDescriptor getHubSeed(TransformSeed var1);

    public abstract List<HubSeedDescriptor> getHubSeeds(TransformServerInfo var1);

    public abstract List<HubSeedDescriptor> getHubSeeds(TransformDescriptor var1);

    public abstract Set<TransformServerInfo> getServers(HubSeedDescriptor var1);

    public abstract Set<TransformDefinition> getTransforms(HubSeedDescriptor var1);

    public abstract void addPropertyChangeListener(PropertyChangeListener var1);

    public abstract void removePropertyChangeListener(PropertyChangeListener var1);
}

