/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.JsonSyntaxException
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformRepository
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.descriptor.TransformSeedRepository
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 *  com.paterva.maltego.transform.protocol.v2api.TransformTranslator
 *  com.paterva.maltego.transform.protocol.v2api.messaging.TransformListInputDescriptor
 *  com.paterva.maltego.transform.protocol.v2api.remote.RemoteTransformTranslator
 *  com.paterva.maltego.transform.repository.FSTransformSeedRepository
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.util.ProductRestrictions
 *  com.paterva.maltego.util.XmlSerializationException
 *  com.paterva.maltego.util.http.HttpAgent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.seeds.api.registry;

import com.google.gson.JsonSyntaxException;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeedUrl;
import com.paterva.maltego.seeds.api.HubSeeds;
import com.paterva.maltego.seeds.api.registry.DefaultHubSeedSettings;
import com.paterva.maltego.seeds.api.registry.HubSeedRegistry;
import com.paterva.maltego.seeds.api.registry.OnlineHubSeeds;
import com.paterva.maltego.seeds.api.serialize.HubSeedReader;
import com.paterva.maltego.seeds.api.serialize.HubSeedWriter;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformRepository;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.descriptor.TransformSeedRepository;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import com.paterva.maltego.transform.protocol.v2api.TransformTranslator;
import com.paterva.maltego.transform.protocol.v2api.messaging.TransformListInputDescriptor;
import com.paterva.maltego.transform.protocol.v2api.remote.RemoteTransformTranslator;
import com.paterva.maltego.transform.repository.FSTransformSeedRepository;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.util.ProductRestrictions;
import com.paterva.maltego.util.XmlSerializationException;
import com.paterva.maltego.util.http.HttpAgent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;

public class DefaultHubSeedRegistry
extends HubSeedRegistry {
    private static final String MALTEGO_FOLDER = "Maltego";
    private static final String SEED_FOLDER = "Seeds";
    private static final String EXTENSION = "hub";
    private static final String ATTR_URL = "url";
    private HubSeeds _seeds;
    private final FileObject _configRoot;
    private final TransformSeedRepository _repo;
    private final PropertyChangeSupport _changeSupport;
    private boolean _online;

    public DefaultHubSeedRegistry() {
        this._changeSupport = new PropertyChangeSupport(this);
        this._online = true;
        this._configRoot = FileUtil.getConfigRoot();
        this._repo = TransformSeedRepository.getDefault();
    }

    public DefaultHubSeedRegistry(FileObject fileObject) {
        this._changeSupport = new PropertyChangeSupport(this);
        this._online = true;
        this._configRoot = fileObject;
        this._repo = new FSTransformSeedRepository(fileObject);
    }

    @Override
    public boolean isOnline() {
        return this._online;
    }

    @Override
    public synchronized HubSeeds getSeeds(boolean bl) {
        if (bl || this._seeds == null) {
            HubSeeds hubSeeds = this.downloadSeeds();
            HubSeeds hubSeeds2 = this.loadSeeds();
            List<HubSeedDescriptor> list = this.mergeSeeds(hubSeeds, hubSeeds2);
            this._seeds = new HubSeeds(list);
            this.updateGlobalTxProperties();
            this.createNewSeedsForOldSeeds();
            this.saveSeeds();
            this._online = hubSeeds != null;
            this.firePropertyChange("onlineChanged");
            this.firePropertyChange("seedsChanged");
        }
        return this._seeds;
    }

    @Override
    public synchronized void addSeed(HubSeedDescriptor hubSeedDescriptor) {
        if (hubSeedDescriptor.isCustom() && !ProductRestrictions.urlAllowed((String)hubSeedDescriptor.getHubSeedUrl().getUrl())) {
            throw new IllegalArgumentException("Only *.paterva.com custom hub items are allowed in the free version of Maltego.");
        }
        HubSeeds hubSeeds = this.getSeeds(false);
        for (HubSeedDescriptor hubSeedDescriptor22 : hubSeeds.getSeeds()) {
            if (!hubSeedDescriptor22.equals(hubSeedDescriptor)) continue;
            throw new IllegalArgumentException("Seed already exist: " + hubSeedDescriptor.getDisplayName());
        }
        for (HubSeedDescriptor hubSeedDescriptor22 : hubSeeds.getSeeds()) {
            if (!hubSeedDescriptor22.getHubSeedUrl().getUrl().equals(hubSeedDescriptor.getHubSeedUrl().getUrl())) continue;
            throw new IllegalArgumentException("Seed for URL already exist: " + hubSeedDescriptor.getHubSeedUrl().getUrl());
        }
        hubSeeds.getSeeds().add(hubSeedDescriptor);
        this.saveSeeds();
        this.firePropertyChange("seedsChanged");
    }

    @Override
    public synchronized void removeSeed(HubSeedDescriptor hubSeedDescriptor, boolean bl) {
        HubSeeds hubSeeds = this.getSeeds(false);
        Iterator<HubSeedDescriptor> iterator = hubSeeds.getSeeds().iterator();
        while (iterator.hasNext()) {
            HubSeedDescriptor hubSeedDescriptor2 = iterator.next();
            if (!hubSeedDescriptor2.equals(hubSeedDescriptor)) continue;
            iterator.remove();
            break;
        }
        if (bl) {
            this.removeOldStyleSeed(hubSeedDescriptor);
        }
        this.saveSeeds();
        this.firePropertyChange("seedsChanged");
    }

    @Override
    public synchronized TransformSeed getTransformSeed(HubSeedDescriptor hubSeedDescriptor) {
        return this.getTransformSeed(hubSeedDescriptor, true);
    }

    public synchronized TransformSeed getTransformSeed(HubSeedDescriptor hubSeedDescriptor, boolean bl) {
        FastURL fastURL = new FastURL(hubSeedDescriptor.getHubSeedUrl().getUrl());
        TransformSeed transformSeed = this._repo.get(fastURL);
        if (transformSeed == null && bl) {
            String string = hubSeedDescriptor.getDescription() != null ? hubSeedDescriptor.getDescription() : "";
            DisplayDescriptorCollection displayDescriptorCollection = this.translateGlobalTransformInputs(hubSeedDescriptor);
            transformSeed = new TransformSeed(fastURL, hubSeedDescriptor.getDisplayName(), string, displayDescriptorCollection, null);
            transformSeed.setEnabled(false);
            this._repo.add(transformSeed);
        }
        return transformSeed;
    }

    private DisplayDescriptorCollection translateGlobalTransformInputs(HubSeedDescriptor hubSeedDescriptor) {
        DisplayDescriptorCollection displayDescriptorCollection = null;
        try {
            List<TransformListInputDescriptor> list = hubSeedDescriptor.getTransformInputs();
            RemoteTransformTranslator remoteTransformTranslator = new RemoteTransformTranslator();
            Set set = remoteTransformTranslator.translateInputs(list);
            displayDescriptorCollection = TransformTranslator.translate((Set)set);
        }
        catch (Exception var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        return displayDescriptorCollection;
    }

    private void removeOldStyleSeed(HubSeedDescriptor hubSeedDescriptor) {
        TransformServerRegistry.getDefault().removeSeedUrl(hubSeedDescriptor.getHubSeedUrl().getUrl());
        this._repo.remove(new FastURL(hubSeedDescriptor.getHubSeedUrl().getUrl()));
    }

    private void updateGlobalTxProperties() {
        for (HubSeedDescriptor hubSeedDescriptor : this._seeds.getSeeds()) {
            TransformSeed transformSeed = this.getTransformSeed(hubSeedDescriptor, false);
            if (transformSeed == null) continue;
            DisplayDescriptorCollection displayDescriptorCollection = this.translateGlobalTransformInputs(hubSeedDescriptor);
            transformSeed.setGlobalTxProperties(displayDescriptorCollection);
        }
    }

    private void createNewSeedsForOldSeeds() {
        TransformSeed[] arrtransformSeed = this._repo.getAll();
        ArrayList<HubSeedDescriptor> arrayList = new ArrayList<HubSeedDescriptor>();
        for (TransformSeed transformSeed : arrtransformSeed) {
            String string = transformSeed.getUrl().toString();
            HubSeedDescriptor hubSeedDescriptor = this._seeds.getSeed(null, string);
            if (hubSeedDescriptor != null || !ProductRestrictions.urlAllowed((String)string)) continue;
            HubSeedDescriptor hubSeedDescriptor2 = this.getHubSeed(transformSeed);
            arrayList.add(hubSeedDescriptor2);
        }
        if (!arrayList.isEmpty()) {
            arrayList.addAll(this._seeds.getSeeds());
            this._seeds = new HubSeeds(arrayList);
        }
    }

    @Override
    public HubSeedDescriptor getHubSeed(String string) {
        HubSeeds hubSeeds = this.getSeeds(false);
        return hubSeeds.getSeed(null, string);
    }

    @Override
    public synchronized HubSeedDescriptor getHubSeed(TransformSeed transformSeed) {
        HubSeedDescriptor hubSeedDescriptor = new HubSeedDescriptor();
        hubSeedDescriptor.setCustom(true);
        hubSeedDescriptor.setDisplayName(transformSeed.getName());
        hubSeedDescriptor.setDescription(transformSeed.getDescription());
        hubSeedDescriptor.setUrl(new HubSeedUrl(transformSeed.getUrl().toString()));
        hubSeedDescriptor.setPos(1000000);
        return hubSeedDescriptor;
    }

    @Override
    public synchronized List<HubSeedDescriptor> getHubSeeds(TransformServerInfo transformServerInfo) {
        ArrayList<HubSeedDescriptor> arrayList = new ArrayList<HubSeedDescriptor>();
        HubSeeds hubSeeds = this.getSeeds(false);
        for (String string : transformServerInfo.getSeedUrls()) {
            HubSeedDescriptor hubSeedDescriptor = hubSeeds.getSeed(null, string);
            if (hubSeedDescriptor == null) continue;
            arrayList.add(hubSeedDescriptor);
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    @Override
    public synchronized List<HubSeedDescriptor> getHubSeeds(TransformDescriptor transformDescriptor) {
        HashSet<HubSeedDescriptor> hashSet = new HashSet<HubSeedDescriptor>();
        TransformServerRegistry transformServerRegistry = TransformServerRegistry.getDefault();
        Set set = transformServerRegistry.findServers(transformDescriptor.getName(), false);
        for (TransformServerInfo transformServerInfo : set) {
            hashSet.addAll(this.getHubSeeds(transformServerInfo));
        }
        ArrayList arrayList = new ArrayList(hashSet);
        Collections.sort(arrayList);
        return arrayList;
    }

    @Override
    public synchronized Set<TransformServerInfo> getServers(HubSeedDescriptor hubSeedDescriptor) {
        if (!DefaultHubSeedSettings.getDefault().isInstalled(hubSeedDescriptor)) {
            return Collections.EMPTY_SET;
        }
        HashSet<TransformServerInfo> hashSet = new HashSet<TransformServerInfo>();
        TransformServerRegistry transformServerRegistry = TransformServerRegistry.getDefault();
        for (TransformServerInfo transformServerInfo : transformServerRegistry.getAll()) {
            if (!this.getHubSeeds(transformServerInfo).contains(hubSeedDescriptor)) continue;
            hashSet.add(transformServerInfo);
        }
        return hashSet;
    }

    @Override
    public Set<TransformDefinition> getTransforms(HubSeedDescriptor hubSeedDescriptor) {
        if (!DefaultHubSeedSettings.getDefault().isInstalled(hubSeedDescriptor)) {
            return Collections.EMPTY_SET;
        }
        HashSet<TransformDefinition> hashSet = new HashSet<TransformDefinition>();
        TransformRepository transformRepository = TransformRepositoryRegistry.getDefault().getRepository("Remote");
        for (TransformServerInfo transformServerInfo : this.getServers(hubSeedDescriptor)) {
            for (String string : transformServerInfo.getTransforms()) {
                TransformDefinition transformDefinition = transformRepository.get(string);
                if (transformDefinition == null) continue;
                hashSet.add(transformDefinition);
            }
        }
        return hashSet;
    }

    private List<HubSeedDescriptor> mergeSeeds(HubSeeds hubSeeds, HubSeeds hubSeeds2) {
        ArrayList<HubSeedDescriptor> arrayList = new ArrayList<HubSeedDescriptor>();
        if (hubSeeds != null) {
            arrayList.addAll(hubSeeds.getSeeds());
            for (HubSeedDescriptor hubSeedDescriptor : hubSeeds2.getSeeds()) {
                String string;
                HubSeedDescriptor hubSeedDescriptor2 = hubSeeds.getSeed(hubSeedDescriptor.getName(), hubSeedDescriptor.getHubSeedUrl().getUrl());
                if (hubSeedDescriptor2 == null) {
                    if (!hubSeedDescriptor.isCustom()) {
                        boolean bl = DefaultHubSeedSettings.getDefault().isInstalled(hubSeedDescriptor);
                        if (bl) {
                            hubSeedDescriptor.setCustom(true);
                            arrayList.add(hubSeedDescriptor);
                            continue;
                        }
                        this.removeOldStyleSeed(hubSeedDescriptor);
                        continue;
                    }
                    arrayList.add(hubSeedDescriptor);
                    continue;
                }
                String string2 = hubSeedDescriptor.getHubSeedUrl().getUrl();
                if (string2.equalsIgnoreCase(string = hubSeedDescriptor2.getHubSeedUrl().getUrl())) continue;
                TransformSeedRepository.getDefault().updateUrl(string2, string);
                TransformServerRegistry.getDefault().updateSeedUrl(string2, string);
            }
        } else {
            for (HubSeedDescriptor hubSeedDescriptor : hubSeeds2.getSeeds()) {
                if (hubSeedDescriptor.isCustom()) {
                    arrayList.add(hubSeedDescriptor);
                    continue;
                }
                boolean bl = DefaultHubSeedSettings.getDefault().isInstalled(hubSeedDescriptor);
                if (bl) {
                    arrayList.add(hubSeedDescriptor);
                    continue;
                }
                this.removeOldStyleSeed(hubSeedDescriptor);
            }
        }
        return arrayList;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private synchronized HubSeeds downloadSeeds() {
        HubSeeds hubSeeds = null;
        InputStream inputStream = null;
        URL uRL = OnlineHubSeeds.getUrl();
        try {
            Object object;
            if (uRL.toString().toLowerCase().startsWith("http")) {
                object = new HttpAgent(uRL);
                object.setConnectTimeout(10000);
                object.setReadTimeout(10000);
                object.doGet();
                inputStream = object.getInputStream();
            } else {
                inputStream = uRL.openStream();
            }
            object = new HubSeedReader();
            hubSeeds = object.read(inputStream);
        }
        catch (XmlSerializationException var4_6) {
            Exceptions.printStackTrace((Throwable)var4_6);
        }
        catch (JsonSyntaxException var4_8) {
            Exceptions.printStackTrace((Throwable)var4_8);
        }
        catch (IOException var4_10) {
            NormalException.logStackTrace((Throwable)var4_10);
        }
        catch (Exception var4_12) {
            Exceptions.printStackTrace((Throwable)var4_12);
        }
        finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            }
            catch (IOException var4_11) {}
        }
        return hubSeeds;
    }

    public HubSeeds loadSeeds() {
        ArrayList<HubSeedDescriptor> arrayList = new ArrayList<HubSeedDescriptor>();
        for (FileObject fileObject : this.getSeedFiles()) {
            HubSeedDescriptor hubSeedDescriptor = this.load(fileObject);
            if (hubSeedDescriptor == null || hubSeedDescriptor.isCustom() && !ProductRestrictions.urlAllowed((String)hubSeedDescriptor.getHubSeedUrl().getUrl())) continue;
            arrayList.add(hubSeedDescriptor);
        }
        return new HubSeeds(arrayList);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private HubSeedDescriptor load(FileObject fileObject) {
        InputStream inputStream = null;
        HubSeedDescriptor hubSeedDescriptor = null;
        try {
            inputStream = fileObject.getInputStream();
            HubSeedReader hubSeedReader = new HubSeedReader();
            hubSeedDescriptor = hubSeedReader.readSeed(inputStream);
        }
        catch (IOException var4_6) {
            Exceptions.printStackTrace((Throwable)var4_6);
        }
        finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                }
                catch (IOException var4_7) {}
            }
        }
        return hubSeedDescriptor;
    }

    private void saveSeeds() {
        FileObject[] arrfileObject = this.getSeedFiles();
        for (FileObject fileObject : arrfileObject) {
            try {
                fileObject.delete();
                continue;
            }
            catch (IOException var6_7) {
                Exceptions.printStackTrace((Throwable)var6_7);
            }
        }
        for (HubSeedDescriptor hubSeedDescriptor : this._seeds.getSeeds()) {
            this.save(hubSeedDescriptor);
        }
    }

    private void save(HubSeedDescriptor hubSeedDescriptor) {
        try {
            FileObject fileObject = this.getFile(hubSeedDescriptor.getHubSeedUrl().getUrl());
            if (fileObject != null) {
                DefaultHubSeedRegistry.update(fileObject, hubSeedDescriptor);
            } else {
                FileObject fileObject2 = this.getOrCreateSeedFolder();
                DefaultHubSeedRegistry.update(FileUtilities.createUniqueFile((FileObject)fileObject2, (String)FileUtilities.replaceIllegalChars((String)hubSeedDescriptor.getDisplayName()), (String)"hub"), hubSeedDescriptor);
            }
        }
        catch (IOException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void update(FileObject fileObject, HubSeedDescriptor hubSeedDescriptor) throws IOException {
        fileObject.setAttribute("url", (Object)hubSeedDescriptor.getHubSeedUrl().getUrl());
        BufferedOutputStream bufferedOutputStream = null;
        try {
            bufferedOutputStream = new BufferedOutputStream(fileObject.getOutputStream());
            HubSeedWriter hubSeedWriter = new HubSeedWriter();
            hubSeedWriter.writeXml(hubSeedDescriptor, (OutputStream)bufferedOutputStream);
        }
        finally {
            if (bufferedOutputStream != null) {
                bufferedOutputStream.close();
            }
        }
    }

    private FileObject getFile(String string) {
        for (FileObject fileObject : this.getSeedFiles()) {
            if (!string.equals(fileObject.getAttribute("url"))) continue;
            return fileObject;
        }
        return null;
    }

    private FileObject[] getSeedFiles() {
        ArrayList<FileObject> arrayList = new ArrayList<FileObject>();
        FileObject fileObject = this.getSeedFolder();
        if (fileObject != null) {
            Enumeration enumeration = fileObject.getChildren(false);
            while (enumeration.hasMoreElements()) {
                FileObject fileObject2 = (FileObject)enumeration.nextElement();
                if (fileObject2.isFolder() || !"hub".equals(fileObject2.getExt())) continue;
                arrayList.add(fileObject2);
            }
        }
        return arrayList.toArray((T[])new FileObject[arrayList.size()]);
    }

    private FileObject getSeedFolder() {
        FileObject fileObject = this._configRoot.getFileObject("Maltego");
        if (fileObject != null) {
            return fileObject.getFileObject("Seeds");
        }
        return null;
    }

    private FileObject getOrCreateSeedFolder() throws IOException {
        return FileUtilities.getOrCreate((FileObject)this.getOrCreateMaltegoFolder(), (String)"Seeds");
    }

    private FileObject getOrCreateMaltegoFolder() throws IOException {
        return FileUtilities.getOrCreate((FileObject)this._configRoot, (String)"Maltego");
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        PropertyChangeSupport propertyChangeSupport = this._changeSupport;
        synchronized (propertyChangeSupport) {
            this._changeSupport.addPropertyChangeListener(propertyChangeListener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        PropertyChangeSupport propertyChangeSupport = this._changeSupport;
        synchronized (propertyChangeSupport) {
            this._changeSupport.removePropertyChangeListener(propertyChangeListener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void firePropertyChange(String string) {
        PropertyChangeSupport propertyChangeSupport = this._changeSupport;
        synchronized (propertyChangeSupport) {
            this._changeSupport.firePropertyChange(string, null, null);
        }
    }
}

