/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  com.paterva.maltego.util.StringUtilities
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.seeds.api;

import com.google.gson.annotations.SerializedName;
import com.paterva.maltego.util.StringUtilities;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="Pricing", strict=0)
public class HubSeedPricing {
    @Element(name="Info", required=0)
    @SerializedName(value="Info")
    private String _info;
    @Element(name="Website", required=0)
    @SerializedName(value="Website")
    private String _website;

    public String getInfo() {
        return this._info;
    }

    public String getWebsite() {
        return StringUtilities.trim((String)this._website);
    }
}

