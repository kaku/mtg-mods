/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  org.netbeans.api.sendopts.CommandException
 *  org.netbeans.spi.sendopts.Env
 *  org.netbeans.spi.sendopts.Option
 *  org.netbeans.spi.sendopts.OptionProcessor
 */
package com.paterva.maltego.seeds.api.option;

import com.paterva.maltego.seeds.api.registry.OnlineHubSeeds;
import com.paterva.maltego.util.StringUtilities;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.sendopts.CommandException;
import org.netbeans.spi.sendopts.Env;
import org.netbeans.spi.sendopts.Option;
import org.netbeans.spi.sendopts.OptionProcessor;

public class TransformHubOptionProcessor
extends OptionProcessor {
    private static final Logger LOGGER = Logger.getLogger(TransformHubOptionProcessor.class.getName());
    private static final Option SEEDS_URL = Option.requiredArgument((char)'h', (String)"hub");

    protected Set<Option> getOptions() {
        return Collections.EMPTY_SET;
    }

    protected void process(Env env, Map<Option, String[]> map) throws CommandException {
        String string;
        if (map.containsKey((Object)SEEDS_URL) && !StringUtilities.isNullOrEmpty((String)(string = map.get((Object)SEEDS_URL)[0]))) {
            try {
                URL uRL = new URL(string);
                OnlineHubSeeds.setUrl(uRL);
                LOGGER.log(Level.INFO, "Transform Hub URL: {0}", string);
            }
            catch (MalformedURLException var4_5) {
                LOGGER.log(Level.WARNING, "Invalid Transform Hub URL specified: {0}", string);
            }
        }
    }
}

