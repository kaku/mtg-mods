/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.Version
 *  org.openide.modules.ModuleInstall
 */
package com.paterva.maltego.seeds.api;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeeds;
import com.paterva.maltego.seeds.api.registry.DefaultHubSeedRegistry;
import com.paterva.maltego.util.Version;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.modules.ModuleInstall;

public class Installer
extends ModuleInstall {
    private static final Logger LOG = Logger.getLogger(Installer.class.getName());

    public void restored() {
        if (Version.isReady()) {
            this.downloadHub();
        } else {
            Version.addPropertyChangeListener((PropertyChangeListener)new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                    Version.removePropertyChangeListener((PropertyChangeListener)this);
                    Installer.this.downloadHub();
                }
            });
        }
    }

    private void downloadHub() {
        HubSeeds hubSeeds = DefaultHubSeedRegistry.getDefault().getSeeds(true);
        LOG.log(Level.FINE, "{0} hub items loaded.", hubSeeds.getSeeds().size());
    }

}

