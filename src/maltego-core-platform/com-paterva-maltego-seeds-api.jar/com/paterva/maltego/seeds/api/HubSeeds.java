/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.seeds.api;

import com.google.gson.annotations.SerializedName;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeedUrl;
import java.util.List;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="Mtg", strict=0)
public class HubSeeds {
    @ElementList(name="Seeds", type=HubSeedDescriptor.class, required=1)
    @SerializedName(value="Seeds")
    private List<HubSeedDescriptor> _seeds;

    public HubSeeds() {
    }

    public HubSeeds(List<HubSeedDescriptor> list) {
        this._seeds = list;
    }

    public List<HubSeedDescriptor> getSeeds() {
        return this._seeds;
    }

    public HubSeedDescriptor getSeedForName(String string) {
        if (this._seeds != null && string != null) {
            for (HubSeedDescriptor hubSeedDescriptor : this._seeds) {
                if (!string.equals(hubSeedDescriptor.getName())) continue;
                return hubSeedDescriptor;
            }
        }
        return null;
    }

    public HubSeedDescriptor getSeedForUrl(String string) {
        if (this._seeds != null && string != null) {
            for (HubSeedDescriptor hubSeedDescriptor : this._seeds) {
                HubSeedUrl hubSeedUrl = hubSeedDescriptor.getHubSeedUrl();
                if (hubSeedUrl == null || !string.equals(hubSeedUrl.getUrl())) continue;
                return hubSeedDescriptor;
            }
        }
        return null;
    }

    public HubSeedDescriptor getSeed(String string, String string2) {
        if (this._seeds != null) {
            for (HubSeedDescriptor hubSeedDescriptor : this._seeds) {
                if (!hubSeedDescriptor.isSame(string, string2)) continue;
                return hubSeedDescriptor;
            }
        }
        return null;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("Seeds:{\n");
        for (HubSeedDescriptor hubSeedDescriptor : this._seeds) {
            stringBuilder.append("   ").append(hubSeedDescriptor).append("\n");
        }
        stringBuilder.append("}\n");
        return stringBuilder.toString();
    }
}

