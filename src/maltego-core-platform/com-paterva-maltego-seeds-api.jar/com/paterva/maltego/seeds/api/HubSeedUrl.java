/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  com.paterva.maltego.util.StringUtilities
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 *  org.simpleframework.xml.Text
 */
package com.paterva.maltego.seeds.api;

import com.google.gson.annotations.SerializedName;
import com.paterva.maltego.util.StringUtilities;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name="Url", strict=0)
public class HubSeedUrl {
    @Attribute(name="Visible", required=0)
    @SerializedName(value="Visible")
    private boolean _visible = false;
    @Text(required=1)
    @SerializedName(value="Url")
    private String _url;

    public HubSeedUrl() {
    }

    public HubSeedUrl(String string) {
        this._url = string;
    }

    public boolean isVisible() {
        return this._visible;
    }

    public void setVisible(boolean bl) {
        this._visible = bl;
    }

    public String getUrl() {
        return StringUtilities.trim((String)this._url);
    }

    public void setUrl(String string) {
        this._url = StringUtilities.trim((String)string);
    }

    public int hashCode() {
        int n = 5;
        n = 23 * n + (this._url != null ? this._url.hashCode() : 0);
        return n;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        HubSeedUrl hubSeedUrl = (HubSeedUrl)object;
        if (this._url != null) {
            if (!this._url.equalsIgnoreCase(hubSeedUrl._url)) return false;
            return true;
        }
        if (hubSeedUrl._url == null) return true;
        return false;
    }
}

