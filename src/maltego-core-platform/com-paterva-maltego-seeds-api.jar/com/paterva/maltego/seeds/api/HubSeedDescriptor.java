/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  com.paterva.maltego.transform.protocol.v2api.messaging.TransformListInputDescriptor
 *  com.paterva.maltego.util.StringUtilities
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.seeds.api;

import com.google.gson.annotations.SerializedName;
import com.paterva.maltego.seeds.api.HubSeedIcon;
import com.paterva.maltego.seeds.api.HubSeedPricing;
import com.paterva.maltego.seeds.api.HubSeedProvider;
import com.paterva.maltego.seeds.api.HubSeedRegistration;
import com.paterva.maltego.seeds.api.HubSeedUrl;
import com.paterva.maltego.transform.protocol.v2api.messaging.TransformListInputDescriptor;
import com.paterva.maltego.util.StringUtilities;
import java.util.Date;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="Seed", strict=0)
public class HubSeedDescriptor
implements Comparable<HubSeedDescriptor> {
    @Attribute(name="Name", required=0)
    @SerializedName(value="Name")
    private String _name;
    @Attribute(name="Pos", required=1)
    @SerializedName(value="Pos")
    private int _pos;
    @Attribute(name="Display", required=1)
    @SerializedName(value="Display")
    private String _displayName;
    @Element(name="Url", required=1)
    @SerializedName(value="Url")
    private HubSeedUrl _url;
    @Element(name="Description", required=0)
    @SerializedName(value="Description")
    private String _description;
    @Element(name="Details", required=0)
    @SerializedName(value="Details")
    private String _details;
    @Element(name="Modified", required=0)
    @SerializedName(value="Modified")
    private long _modified;
    @Element(name="Icon", required=0)
    @SerializedName(value="Icon")
    private HubSeedIcon _icon;
    @Element(name="Provider", required=0)
    @SerializedName(value="Provider")
    private HubSeedProvider _provider;
    @Element(name="Registration", required=0)
    @SerializedName(value="Registration")
    private HubSeedRegistration _registration;
    @Element(name="Pricing", required=0)
    @SerializedName(value="Pricing")
    private HubSeedPricing _pricing;
    @Element(name="Custom", required=0)
    @SerializedName(value="Custom")
    private boolean _custom = false;
    @ElementList(name="Inputs", type=TransformListInputDescriptor.class, required=0)
    @SerializedName(value="Inputs")
    private List<TransformListInputDescriptor> _txinputs;

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public int getPos() {
        return this._pos;
    }

    public void setPos(int n) {
        this._pos = n;
    }

    public String getDisplayName() {
        return this._displayName;
    }

    public void setDisplayName(String string) {
        this._displayName = string;
    }

    public HubSeedUrl getHubSeedUrl() {
        return this._url;
    }

    public void setUrl(HubSeedUrl hubSeedUrl) {
        this._url = hubSeedUrl;
    }

    public String getDescription() {
        return this._description;
    }

    public void setDescription(String string) {
        this._description = string;
    }

    public String getDetails() {
        return this._details;
    }

    public void setDetails(String string) {
        this._details = string;
    }

    public Date getModified() {
        return new Date(this._modified);
    }

    public HubSeedIcon getIcon() {
        return this._icon;
    }

    public void setIcon(HubSeedIcon hubSeedIcon) {
        this._icon = hubSeedIcon;
    }

    public HubSeedProvider getProvider() {
        return this._provider;
    }

    public HubSeedRegistration getRegistration() {
        return this._registration;
    }

    public HubSeedPricing getPricing() {
        return this._pricing;
    }

    public void setCustom(boolean bl) {
        this._custom = bl;
    }

    public boolean isCustom() {
        return this._custom;
    }

    public void setTransformInputs(List<TransformListInputDescriptor> list) {
        this._txinputs = list;
    }

    public List<TransformListInputDescriptor> getTransformInputs() {
        return this._txinputs;
    }

    @Override
    public int compareTo(HubSeedDescriptor hubSeedDescriptor) {
        if (this.isCustom()) {
            if (!hubSeedDescriptor.isCustom()) {
                return 1;
            }
            return this.getDisplayName().compareToIgnoreCase(hubSeedDescriptor.getDisplayName());
        }
        return this._pos - hubSeedDescriptor.getPos();
    }

    public int hashCode() {
        int n = 7;
        n = 97 * n + (this._url != null ? this._url.hashCode() : 0);
        return n;
    }

    public boolean isSame(String string, String string2) {
        if (!StringUtilities.isNullOrEmpty((String)this._name) && !StringUtilities.isNullOrEmpty((String)string)) {
            return this._name.equals(string);
        }
        return this._url.getUrl().equalsIgnoreCase(string2);
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        HubSeedDescriptor hubSeedDescriptor = (HubSeedDescriptor)object;
        return this.isSame(hubSeedDescriptor.getName(), hubSeedDescriptor.getHubSeedUrl().getUrl());
    }

    public String toString() {
        return "{Name:" + this._name + ";URL:" + this._url.getUrl() + "}";
    }
}

