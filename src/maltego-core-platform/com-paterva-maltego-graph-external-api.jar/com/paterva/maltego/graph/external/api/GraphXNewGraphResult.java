/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.external.api;

public class GraphXNewGraphResult {
    private final int _gid;
    private final String _name;

    public GraphXNewGraphResult(int n, String string) {
        this._gid = n;
        this._name = string;
    }

    public int getGid() {
        return this._gid;
    }

    public String getName() {
        return this._name;
    }
}

