/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.graph.external.api;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.external.api.GraphXDescriptor;
import com.paterva.maltego.graph.external.api.GraphXException;
import java.util.List;
import org.openide.util.Lookup;

public abstract class GraphXRegistry {
    private static GraphXRegistry _default;

    public static synchronized GraphXRegistry getDefault() {
        if (_default == null && (GraphXRegistry._default = (GraphXRegistry)Lookup.getDefault().lookup(GraphXRegistry.class)) == null) {
            throw new IllegalStateException("No GraphXRegistry found.");
        }
        return _default;
    }

    public abstract List<GraphXDescriptor> getGraphXDescriptors();

    public abstract GraphXDescriptor getGraphXDescriptor(int var1) throws GraphXException;

    public abstract GraphID getGraphID(int var1) throws GraphXException;
}

