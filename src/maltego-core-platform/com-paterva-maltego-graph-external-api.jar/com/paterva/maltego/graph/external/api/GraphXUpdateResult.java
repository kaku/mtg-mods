/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 */
package com.paterva.maltego.graph.external.api;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class GraphXUpdateResult {
    private final Map<MaltegoEntity, MaltegoEntity> _entityMap = new HashMap<MaltegoEntity, MaltegoEntity>();
    private final Map<MaltegoLink, MaltegoLink> _linkMap = new HashMap<MaltegoLink, MaltegoLink>();

    public void putEntity(MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2) {
        this._entityMap.put(maltegoEntity, maltegoEntity2);
    }

    public void putEntities(Map<MaltegoEntity, MaltegoEntity> map) {
        this._entityMap.putAll(map);
    }

    public void putLink(MaltegoLink maltegoLink, MaltegoLink maltegoLink2) {
        this._linkMap.put(maltegoLink, maltegoLink2);
    }

    public void putLinks(Map<MaltegoLink, MaltegoLink> map) {
        this._linkMap.putAll(map);
    }

    public Map<MaltegoEntity, MaltegoEntity> getEntityMap() {
        return Collections.unmodifiableMap(this._entityMap);
    }

    public Map<MaltegoLink, MaltegoLink> getLinkMap() {
        return Collections.unmodifiableMap(this._linkMap);
    }
}

