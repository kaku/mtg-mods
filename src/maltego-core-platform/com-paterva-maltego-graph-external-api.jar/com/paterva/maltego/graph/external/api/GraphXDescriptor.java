/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.external.api;

public class GraphXDescriptor {
    private final int _id;
    private final String _name;

    public GraphXDescriptor(int n, String string) {
        this._id = n;
        this._name = string;
    }

    public int getId() {
        return this._id;
    }

    public String getName() {
        return this._name;
    }
}

