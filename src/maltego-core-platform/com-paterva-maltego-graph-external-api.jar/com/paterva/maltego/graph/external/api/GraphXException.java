/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.external.api;

public class GraphXException
extends Exception {
    public GraphXException(String string) {
        super(string);
    }

    public GraphXException(String string, Throwable throwable) {
        super(string, throwable);
    }

    public GraphXException(Throwable throwable) {
        super(throwable);
    }
}

