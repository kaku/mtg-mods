/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.MaltegoGraphSnippet
 *  com.paterva.maltego.matching.api.GraphMatchStrategy
 *  com.paterva.maltego.merging.GraphMergeStrategy
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.graph.external.api;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.MaltegoGraphSnippet;
import com.paterva.maltego.graph.external.api.GraphXDescriptor;
import com.paterva.maltego.graph.external.api.GraphXException;
import com.paterva.maltego.graph.external.api.GraphXListener;
import com.paterva.maltego.graph.external.api.GraphXNewGraphResult;
import com.paterva.maltego.graph.external.api.GraphXTransactionListener;
import com.paterva.maltego.graph.external.api.GraphXUpdateResult;
import com.paterva.maltego.matching.api.GraphMatchStrategy;
import com.paterva.maltego.merging.GraphMergeStrategy;
import java.util.List;
import org.openide.util.Lookup;

public abstract class GraphXEntryPoint {
    private static GraphXEntryPoint _default;

    public static synchronized GraphXEntryPoint getDefault() {
        if (_default == null && (GraphXEntryPoint._default = (GraphXEntryPoint)Lookup.getDefault().lookup(GraphXEntryPoint.class)) == null) {
            throw new IllegalStateException("GraphX API implementation not found.");
        }
        return _default;
    }

    public abstract List<GraphXDescriptor> getGraphs() throws GraphXException;

    public abstract GraphID getGraphID(int var1, boolean var2) throws GraphXException;

    public abstract GraphXNewGraphResult newGraph(String var1) throws GraphXException;

    public abstract GraphXUpdateResult update(int var1, MaltegoGraphSnippet var2, GraphMatchStrategy var3, GraphMergeStrategy var4) throws GraphXException;

    public abstract void addGraphXListener(GraphXListener var1) throws GraphXException;

    public abstract void removeGraphXListener(GraphXListener var1) throws GraphXException;

    public abstract void addGraphXTransactionListener(int var1, GraphXTransactionListener var2) throws GraphXException;

    public abstract void removeGraphXTransactionListener(int var1, GraphXTransactionListener var2) throws GraphXException;
}

