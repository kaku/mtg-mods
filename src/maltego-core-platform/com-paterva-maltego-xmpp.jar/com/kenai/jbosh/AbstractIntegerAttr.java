/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.AbstractAttr;
import com.kenai.jbosh.BOSHException;

abstract class AbstractIntegerAttr
extends AbstractAttr<Integer> {
    protected AbstractIntegerAttr(int n) throws BOSHException {
        super(n);
    }

    protected AbstractIntegerAttr(String string) throws BOSHException {
        super(AbstractIntegerAttr.parseInt(string));
    }

    protected final void checkMinValue(int n) throws BOSHException {
        int n2 = (Integer)this.getValue();
        if (n2 < n) {
            throw new BOSHException("Illegal attribute value '" + n2 + "' provided.  Must be >= " + n);
        }
    }

    private static int parseInt(String string) throws BOSHException {
        try {
            return Integer.parseInt(string);
        }
        catch (NumberFormatException var1_1) {
            throw new BOSHException("Could not parse an integer from the value provided: " + string, var1_1);
        }
    }

    public int intValue() {
        return (Integer)this.getValue();
    }
}

