/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import javax.xml.namespace.QName;

public final class BodyQName {
    static final String BOSH_NS_URI = "http://jabber.org/protocol/httpbind";
    private final QName qname;

    private BodyQName(QName qName) {
        this.qname = qName;
    }

    public static BodyQName create(String string, String string2) {
        return BodyQName.createWithPrefix(string, string2, null);
    }

    public static BodyQName createWithPrefix(String string, String string2, String string3) {
        if (string == null || string.length() == 0) {
            throw new IllegalArgumentException("URI is required and may not be null/empty");
        }
        if (string2 == null || string2.length() == 0) {
            throw new IllegalArgumentException("Local arg is required and may not be null/empty");
        }
        if (string3 == null || string3.length() == 0) {
            return new BodyQName(new QName(string, string2));
        }
        return new BodyQName(new QName(string, string2, string3));
    }

    public String getNamespaceURI() {
        return this.qname.getNamespaceURI();
    }

    public String getLocalPart() {
        return this.qname.getLocalPart();
    }

    public String getPrefix() {
        return this.qname.getPrefix();
    }

    public boolean equals(Object object) {
        if (object instanceof BodyQName) {
            BodyQName bodyQName = (BodyQName)object;
            return this.qname.equals(bodyQName.qname);
        }
        return false;
    }

    public int hashCode() {
        return this.qname.hashCode();
    }

    static BodyQName createBOSH(String string) {
        return BodyQName.createWithPrefix("http://jabber.org/protocol/httpbind", string, null);
    }

    boolean equalsQName(QName qName) {
        return this.qname.equals(qName);
    }
}

