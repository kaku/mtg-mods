/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.BOSHException;
import com.kenai.jbosh.BodyParserResults;

interface BodyParser {
    public BodyParserResults parse(String var1) throws BOSHException;
}

