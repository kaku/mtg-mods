/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.AbstractIntegerAttr;
import com.kenai.jbosh.BOSHException;

final class AttrHold
extends AbstractIntegerAttr {
    private AttrHold(String string) throws BOSHException {
        super(string);
        this.checkMinValue(0);
    }

    static AttrHold createFromString(String string) throws BOSHException {
        if (string == null) {
            return null;
        }
        return new AttrHold(string);
    }
}

