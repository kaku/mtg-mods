/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

final class ZLIBCodec {
    private static final int BUFFER_SIZE = 512;

    private ZLIBCodec() {
    }

    public static String getID() {
        return "deflate";
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static byte[] encode(byte[] arrby) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DeflaterOutputStream deflaterOutputStream = null;
        try {
            deflaterOutputStream = new DeflaterOutputStream(byteArrayOutputStream);
            deflaterOutputStream.write(arrby);
            deflaterOutputStream.close();
            byteArrayOutputStream.close();
            byte[] arrby2 = byteArrayOutputStream.toByteArray();
            return arrby2;
        }
        finally {
            deflaterOutputStream.close();
            byteArrayOutputStream.close();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static byte[] decode(byte[] arrby) throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(arrby);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        InflaterInputStream inflaterInputStream = null;
        try {
            int n;
            inflaterInputStream = new InflaterInputStream(byteArrayInputStream);
            byte[] arrby2 = new byte[512];
            do {
                if ((n = inflaterInputStream.read(arrby2)) <= 0) continue;
                byteArrayOutputStream.write(arrby2, 0, n);
            } while (n >= 0);
            byte[] arrby3 = byteArrayOutputStream.toByteArray();
            return arrby3;
        }
        finally {
            inflaterInputStream.close();
            byteArrayOutputStream.close();
        }
    }
}

