/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

final class GZIPCodec {
    private static final int BUFFER_SIZE = 512;

    private GZIPCodec() {
    }

    public static String getID() {
        return "gzip";
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static byte[] encode(byte[] arrby) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DeflaterOutputStream deflaterOutputStream = null;
        try {
            deflaterOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            deflaterOutputStream.write(arrby);
            deflaterOutputStream.close();
            byteArrayOutputStream.close();
            byte[] arrby2 = byteArrayOutputStream.toByteArray();
            return arrby2;
        }
        finally {
            deflaterOutputStream.close();
            byteArrayOutputStream.close();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static byte[] decode(byte[] arrby) throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(arrby);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        GZIPInputStream gZIPInputStream = null;
        try {
            int n;
            gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
            byte[] arrby2 = new byte[512];
            do {
                if ((n = gZIPInputStream.read(arrby2)) <= 0) continue;
                byteArrayOutputStream.write(arrby2, 0, n);
            } while (n >= 0);
            byte[] arrby3 = byteArrayOutputStream.toByteArray();
            return arrby3;
        }
        finally {
            gZIPInputStream.close();
            byteArrayOutputStream.close();
        }
    }
}

