/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

abstract class AbstractAttr<T extends Comparable>
implements Comparable {
    private final T value;

    protected AbstractAttr(T t) {
        this.value = t;
    }

    public final T getValue() {
        return this.value;
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (object instanceof AbstractAttr) {
            AbstractAttr abstractAttr = (AbstractAttr)object;
            return this.value.equals(abstractAttr.value);
        }
        return false;
    }

    public int hashCode() {
        return this.value.hashCode();
    }

    public String toString() {
        return this.value.toString();
    }

    public int compareTo(Object object) {
        if (object == null) {
            return 1;
        }
        return this.value.compareTo((Object)object);
    }
}

