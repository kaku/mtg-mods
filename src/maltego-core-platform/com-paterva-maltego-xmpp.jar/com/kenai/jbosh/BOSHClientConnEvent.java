/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.BOSHClient;
import com.kenai.jbosh.ComposableBody;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventObject;
import java.util.List;

public final class BOSHClientConnEvent
extends EventObject {
    private static final long serialVersionUID = 1;
    private final boolean connected;
    private final List<ComposableBody> requests;
    private final Throwable cause;

    private BOSHClientConnEvent(BOSHClient bOSHClient, boolean bl, List<ComposableBody> list, Throwable throwable) {
        super(bOSHClient);
        this.connected = bl;
        this.cause = throwable;
        if (this.connected) {
            if (throwable != null) {
                throw new IllegalStateException("Cannot be connected and have a cause");
            }
            if (list != null && list.size() > 0) {
                throw new IllegalStateException("Cannot be connected and have outstanding requests");
            }
        }
        this.requests = list == null ? Collections.emptyList() : Collections.unmodifiableList(new ArrayList<ComposableBody>(list));
    }

    static BOSHClientConnEvent createConnectionEstablishedEvent(BOSHClient bOSHClient) {
        return new BOSHClientConnEvent(bOSHClient, true, null, null);
    }

    static BOSHClientConnEvent createConnectionClosedEvent(BOSHClient bOSHClient) {
        return new BOSHClientConnEvent(bOSHClient, false, null, null);
    }

    static BOSHClientConnEvent createConnectionClosedOnErrorEvent(BOSHClient bOSHClient, List<ComposableBody> list, Throwable throwable) {
        return new BOSHClientConnEvent(bOSHClient, false, list, throwable);
    }

    public BOSHClient getBOSHClient() {
        return (BOSHClient)this.getSource();
    }

    public boolean isConnected() {
        return this.connected;
    }

    public boolean isError() {
        return this.cause != null;
    }

    public Throwable getCause() {
        return this.cause;
    }

    public List<ComposableBody> getOutstandingRequests() {
        return this.requests;
    }
}

