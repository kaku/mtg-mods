/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.AbstractAttr;

final class AttrCharsets
extends AbstractAttr<String> {
    private final String[] charsets;

    private AttrCharsets(String string) {
        super(string);
        this.charsets = string.split("\\ +");
    }

    static AttrCharsets createFromString(String string) {
        if (string == null) {
            return null;
        }
        return new AttrCharsets(string);
    }

    boolean isAccepted(String string) {
        for (String string2 : this.charsets) {
            if (!string2.equalsIgnoreCase(string)) continue;
            return true;
        }
        return false;
    }
}

