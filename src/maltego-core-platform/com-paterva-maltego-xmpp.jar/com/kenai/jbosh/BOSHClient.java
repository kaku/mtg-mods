/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.AbstractBody;
import com.kenai.jbosh.AttrMaxPause;
import com.kenai.jbosh.AttrPause;
import com.kenai.jbosh.AttrPolling;
import com.kenai.jbosh.AttrRequests;
import com.kenai.jbosh.AttrSessionID;
import com.kenai.jbosh.AttrVersion;
import com.kenai.jbosh.Attributes;
import com.kenai.jbosh.BOSHClientConfig;
import com.kenai.jbosh.BOSHClientConnEvent;
import com.kenai.jbosh.BOSHClientConnListener;
import com.kenai.jbosh.BOSHClientRequestListener;
import com.kenai.jbosh.BOSHClientResponseListener;
import com.kenai.jbosh.BOSHException;
import com.kenai.jbosh.BOSHMessageEvent;
import com.kenai.jbosh.BodyQName;
import com.kenai.jbosh.CMSessionParams;
import com.kenai.jbosh.ComposableBody;
import com.kenai.jbosh.HTTPExchange;
import com.kenai.jbosh.HTTPResponse;
import com.kenai.jbosh.HTTPSender;
import com.kenai.jbosh.RequestIDSequence;
import com.kenai.jbosh.ServiceLib;
import com.kenai.jbosh.TerminalBindingCondition;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class BOSHClient {
    private static final Logger LOG = Logger.getLogger(BOSHClient.class.getName());
    private static final String TERMINATE = "terminate";
    private static final String ERROR = "error";
    private static final String INTERRUPTED = "Interrupted";
    private static final String UNHANDLED = "Unhandled Exception";
    private static final String NULL_LISTENER = "Listener may not b enull";
    private static final int DEFAULT_EMPTY_REQUEST_DELAY = 100;
    private static final int EMPTY_REQUEST_DELAY = Integer.getInteger(BOSHClient.class.getName() + ".emptyRequestDelay", 100);
    private static final int DEFAULT_PAUSE_MARGIN = 500;
    private static final int PAUSE_MARGIN = Integer.getInteger(BOSHClient.class.getName() + ".pauseMargin", 500);
    private static final boolean ASSERTIONS;
    private final Set<BOSHClientConnListener> connListeners = new CopyOnWriteArraySet<BOSHClientConnListener>();
    private final Set<BOSHClientRequestListener> requestListeners = new CopyOnWriteArraySet<BOSHClientRequestListener>();
    private final Set<BOSHClientResponseListener> responseListeners = new CopyOnWriteArraySet<BOSHClientResponseListener>();
    private final ReentrantLock lock = new ReentrantLock();
    private final Condition notEmpty = this.lock.newCondition();
    private final Condition notFull = this.lock.newCondition();
    private final Condition drained = this.lock.newCondition();
    private final BOSHClientConfig cfg;
    private final Runnable procRunnable;
    private final Runnable emptyRequestRunnable;
    private final HTTPSender httpSender;
    private final AtomicReference<ExchangeInterceptor> exchInterceptor;
    private final RequestIDSequence requestIDSeq;
    private final ScheduledExecutorService schedExec;
    private Thread procThread;
    private ScheduledFuture emptyRequestFuture;
    private CMSessionParams cmParams;
    private Queue<HTTPExchange> exchanges;
    private SortedSet<Long> pendingResponseAcks;
    private Long responseAck;
    private List<ComposableBody> pendingRequestAcks;

    private BOSHClient(BOSHClientConfig bOSHClientConfig) {
        this.procRunnable = new Runnable(){

            @Override
            public void run() {
                BOSHClient.this.processMessages();
            }
        };
        this.emptyRequestRunnable = new Runnable(){

            @Override
            public void run() {
                BOSHClient.this.sendEmptyRequest();
            }
        };
        this.httpSender = ServiceLib.loadService(HTTPSender.class);
        this.exchInterceptor = new AtomicReference();
        this.requestIDSeq = new RequestIDSequence();
        this.schedExec = Executors.newSingleThreadScheduledExecutor();
        this.exchanges = new LinkedList<HTTPExchange>();
        this.pendingResponseAcks = new TreeSet<Long>();
        this.responseAck = -1;
        this.pendingRequestAcks = new ArrayList<ComposableBody>();
        this.cfg = bOSHClientConfig;
        this.init();
    }

    public static BOSHClient create(BOSHClientConfig bOSHClientConfig) {
        if (bOSHClientConfig == null) {
            throw new IllegalArgumentException("Client configuration may not be null");
        }
        return new BOSHClient(bOSHClientConfig);
    }

    public BOSHClientConfig getBOSHClientConfig() {
        return this.cfg;
    }

    public void addBOSHClientConnListener(BOSHClientConnListener bOSHClientConnListener) {
        if (bOSHClientConnListener == null) {
            throw new IllegalArgumentException("Listener may not b enull");
        }
        this.connListeners.add(bOSHClientConnListener);
    }

    public void removeBOSHClientConnListener(BOSHClientConnListener bOSHClientConnListener) {
        if (bOSHClientConnListener == null) {
            throw new IllegalArgumentException("Listener may not b enull");
        }
        this.connListeners.remove(bOSHClientConnListener);
    }

    public void addBOSHClientRequestListener(BOSHClientRequestListener bOSHClientRequestListener) {
        if (bOSHClientRequestListener == null) {
            throw new IllegalArgumentException("Listener may not b enull");
        }
        this.requestListeners.add(bOSHClientRequestListener);
    }

    public void removeBOSHClientRequestListener(BOSHClientRequestListener bOSHClientRequestListener) {
        if (bOSHClientRequestListener == null) {
            throw new IllegalArgumentException("Listener may not b enull");
        }
        this.requestListeners.remove(bOSHClientRequestListener);
    }

    public void addBOSHClientResponseListener(BOSHClientResponseListener bOSHClientResponseListener) {
        if (bOSHClientResponseListener == null) {
            throw new IllegalArgumentException("Listener may not b enull");
        }
        this.responseListeners.add(bOSHClientResponseListener);
    }

    public void removeBOSHClientResponseListener(BOSHClientResponseListener bOSHClientResponseListener) {
        if (bOSHClientResponseListener == null) {
            throw new IllegalArgumentException("Listener may not b enull");
        }
        this.responseListeners.remove(bOSHClientResponseListener);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void send(ComposableBody composableBody) throws BOSHException {
        CMSessionParams cMSessionParams;
        HTTPExchange hTTPExchange;
        this.assertUnlocked();
        if (composableBody == null) {
            throw new IllegalArgumentException("Message body may not be null");
        }
        this.lock.lock();
        try {
            this.blockUntilSendable(composableBody);
            if (!this.isWorking() && !BOSHClient.isTermination(composableBody)) {
                throw new BOSHException("Cannot send message when session is closed");
            }
            long l = this.requestIDSeq.getNextRID();
            ComposableBody composableBody2 = composableBody;
            cMSessionParams = this.cmParams;
            if (cMSessionParams == null && this.exchanges.isEmpty()) {
                composableBody2 = this.applySessionCreationRequest(l, composableBody);
            } else {
                composableBody2 = this.applySessionData(l, composableBody);
                if (this.cmParams.isAckingRequests()) {
                    this.pendingRequestAcks.add(composableBody2);
                }
            }
            hTTPExchange = new HTTPExchange(composableBody2);
            this.exchanges.add(hTTPExchange);
            this.notEmpty.signalAll();
            this.clearEmptyRequest();
        }
        finally {
            this.lock.unlock();
        }
        AbstractBody abstractBody = hTTPExchange.getRequest();
        HTTPResponse hTTPResponse = this.httpSender.send(cMSessionParams, abstractBody);
        hTTPExchange.setHTTPResponse(hTTPResponse);
        this.fireRequestSent(abstractBody);
    }

    public boolean pause() {
        AttrMaxPause attrMaxPause;
        this.assertUnlocked();
        this.lock.lock();
        attrMaxPause = null;
        try {
            if (this.cmParams == null) {
                boolean bl = false;
                return bl;
            }
            attrMaxPause = this.cmParams.getMaxPause();
            if (attrMaxPause == null) {
                boolean bl = false;
                return bl;
            }
        }
        finally {
            this.lock.unlock();
        }
        try {
            this.send(ComposableBody.builder().setAttribute(Attributes.PAUSE, attrMaxPause.toString()).build());
        }
        catch (BOSHException var2_4) {
            LOG.log(Level.FINEST, "Could not send pause", var2_4);
        }
        return true;
    }

    public void disconnect() throws BOSHException {
        this.disconnect(ComposableBody.builder().build());
    }

    public void disconnect(ComposableBody composableBody) throws BOSHException {
        if (composableBody == null) {
            throw new IllegalArgumentException("Message body may not be null");
        }
        ComposableBody.Builder builder = composableBody.rebuild();
        builder.setAttribute(Attributes.TYPE, "terminate");
        this.send(builder.build());
    }

    public void close() {
        this.dispose(new BOSHException("Session explicitly closed by caller"));
    }

    CMSessionParams getCMSessionParams() {
        this.lock.lock();
        try {
            CMSessionParams cMSessionParams = this.cmParams;
            return cMSessionParams;
        }
        finally {
            this.lock.unlock();
        }
    }

    void drain() {
        this.lock.lock();
        try {
            LOG.finest("Waiting while draining...");
            while (this.isWorking() && (this.emptyRequestFuture == null || this.emptyRequestFuture.isDone())) {
                try {
                    this.drained.await();
                }
                catch (InterruptedException var1_1) {
                    LOG.log(Level.FINEST, "Interrupted", var1_1);
                }
            }
            LOG.finest("Drained");
        }
        finally {
            this.lock.unlock();
        }
    }

    void setExchangeInterceptor(ExchangeInterceptor exchangeInterceptor) {
        this.exchInterceptor.set(exchangeInterceptor);
    }

    private void init() {
        this.assertUnlocked();
        this.lock.lock();
        try {
            this.httpSender.init(this.cfg);
            this.procThread = new Thread(this.procRunnable);
            this.procThread.setDaemon(true);
            this.procThread.setName(BOSHClient.class.getSimpleName() + "[" + System.identityHashCode(this) + "]: Receive thread");
            this.procThread.start();
        }
        finally {
            this.lock.unlock();
        }
    }

    private void dispose(Throwable throwable) {
        this.assertUnlocked();
        this.lock.lock();
        try {
            if (this.procThread == null) {
                return;
            }
            this.procThread = null;
        }
        finally {
            this.lock.unlock();
        }
        if (throwable == null) {
            this.fireConnectionClosed();
        } else {
            this.fireConnectionClosedOnError(throwable);
        }
        this.lock.lock();
        try {
            this.clearEmptyRequest();
            this.exchanges = null;
            this.cmParams = null;
            this.pendingResponseAcks = null;
            this.pendingRequestAcks = null;
            this.notEmpty.signalAll();
            this.notFull.signalAll();
            this.drained.signalAll();
        }
        finally {
            this.lock.unlock();
        }
        this.httpSender.destroy();
        this.schedExec.shutdownNow();
    }

    private static boolean isPause(AbstractBody abstractBody) {
        return abstractBody.getAttribute(Attributes.PAUSE) != null;
    }

    private static boolean isTermination(AbstractBody abstractBody) {
        return "terminate".equals(abstractBody.getAttribute(Attributes.TYPE));
    }

    private TerminalBindingCondition getTerminalBindingCondition(int n, AbstractBody abstractBody) {
        this.assertLocked();
        if (BOSHClient.isTermination(abstractBody)) {
            String string = abstractBody.getAttribute(Attributes.CONDITION);
            return TerminalBindingCondition.forString(string);
        }
        if (this.cmParams != null && this.cmParams.getVersion() == null) {
            return TerminalBindingCondition.forHTTPResponseCode(n);
        }
        return null;
    }

    private boolean isImmediatelySendable(AbstractBody abstractBody) {
        this.assertLocked();
        if (this.cmParams == null) {
            return this.exchanges.isEmpty();
        }
        AttrRequests attrRequests = this.cmParams.getRequests();
        if (attrRequests == null) {
            return true;
        }
        int n = attrRequests.intValue();
        if (this.exchanges.size() < n) {
            return true;
        }
        if (this.exchanges.size() == n && (BOSHClient.isTermination(abstractBody) || BOSHClient.isPause(abstractBody))) {
            return true;
        }
        return false;
    }

    private boolean isWorking() {
        this.assertLocked();
        return this.procThread != null;
    }

    private void blockUntilSendable(AbstractBody abstractBody) {
        this.assertLocked();
        while (this.isWorking() && !this.isImmediatelySendable(abstractBody)) {
            try {
                this.notFull.await();
            }
            catch (InterruptedException var2_2) {
                LOG.log(Level.FINEST, "Interrupted", var2_2);
            }
        }
    }

    private ComposableBody applySessionCreationRequest(long l, ComposableBody composableBody) throws BOSHException {
        this.assertLocked();
        ComposableBody.Builder builder = composableBody.rebuild();
        builder.setAttribute(Attributes.TO, this.cfg.getTo());
        builder.setAttribute(Attributes.XML_LANG, this.cfg.getLang());
        builder.setAttribute(Attributes.VER, AttrVersion.getSupportedVersion().toString());
        builder.setAttribute(Attributes.WAIT, "60");
        builder.setAttribute(Attributes.HOLD, "1");
        builder.setAttribute(Attributes.RID, Long.toString(l));
        this.applyRoute(builder);
        this.applyFrom(builder);
        builder.setAttribute(Attributes.ACK, "1");
        builder.setAttribute(Attributes.SID, null);
        return builder.build();
    }

    private void applyRoute(ComposableBody.Builder builder) {
        this.assertLocked();
        String string = this.cfg.getRoute();
        if (string != null) {
            builder.setAttribute(Attributes.ROUTE, string);
        }
    }

    private void applyFrom(ComposableBody.Builder builder) {
        this.assertLocked();
        String string = this.cfg.getFrom();
        if (string != null) {
            builder.setAttribute(Attributes.FROM, string);
        }
    }

    private ComposableBody applySessionData(long l, ComposableBody composableBody) throws BOSHException {
        this.assertLocked();
        ComposableBody.Builder builder = composableBody.rebuild();
        builder.setAttribute(Attributes.SID, this.cmParams.getSessionID().toString());
        builder.setAttribute(Attributes.RID, Long.toString(l));
        this.applyResponseAcknowledgement(builder, l);
        return builder.build();
    }

    private void applyResponseAcknowledgement(ComposableBody.Builder builder, long l) {
        this.assertLocked();
        if (this.responseAck.equals(-1)) {
            return;
        }
        Long l2 = l - 1;
        if (this.responseAck.equals(l2)) {
            return;
        }
        builder.setAttribute(Attributes.ACK, this.responseAck.toString());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void processMessages() {
        LOG.log(Level.FINEST, "Processing thread starting");
        try {
            HTTPExchange hTTPExchange;
            while ((hTTPExchange = this.nextExchange()) != null) {
                ExchangeInterceptor exchangeInterceptor = this.exchInterceptor.get();
                if (exchangeInterceptor != null) {
                    HTTPExchange hTTPExchange2 = exchangeInterceptor.interceptExchange(hTTPExchange);
                    if (hTTPExchange2 == null) {
                        LOG.log(Level.FINE, "Discarding exchange on request of test hook: RID=" + hTTPExchange.getRequest().getAttribute(Attributes.RID));
                        this.lock.lock();
                        try {
                            this.exchanges.remove(hTTPExchange);
                            continue;
                        }
                        finally {
                            this.lock.unlock();
                            continue;
                        }
                    }
                    hTTPExchange = hTTPExchange2;
                }
                this.processExchange(hTTPExchange);
            }
        }
        finally {
            LOG.log(Level.FINEST, "Processing thread exiting");
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private HTTPExchange nextExchange() {
        HTTPExchange hTTPExchange;
        this.assertUnlocked();
        Thread thread = Thread.currentThread();
        hTTPExchange = null;
        this.lock.lock();
        try {
            while (thread.equals(this.procThread)) {
                hTTPExchange = this.exchanges.peek();
                if (hTTPExchange == null) {
                    try {
                        this.notEmpty.await();
                    }
                    catch (InterruptedException var3_3) {
                        LOG.log(Level.FINEST, "Interrupted", var3_3);
                    }
                }
                if (hTTPExchange == null) continue;
                break;
            }
        }
        finally {
            this.lock.unlock();
        }
        return hTTPExchange;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void processExchange(HTTPExchange hTTPExchange) {
        CMSessionParams cMSessionParams;
        Object object;
        ArrayList<Object> arrayList;
        int n;
        AbstractBody abstractBody;
        this.assertUnlocked();
        try {
            HTTPResponse hTTPResponse = hTTPExchange.getHTTPResponse();
            abstractBody = hTTPResponse.getBody();
            n = hTTPResponse.getHTTPStatus();
        }
        catch (BOSHException var5_5) {
            LOG.log(Level.FINEST, "Could not obtain response", var5_5);
            this.dispose(var5_5);
            return;
        }
        catch (InterruptedException var5_6) {
            LOG.log(Level.FINEST, "Interrupted", var5_6);
            this.dispose(var5_6);
            return;
        }
        this.fireResponseReceived(abstractBody);
        AbstractBody abstractBody2 = hTTPExchange.getRequest();
        arrayList = null;
        this.lock.lock();
        try {
            if (this.cmParams == null) {
                this.cmParams = CMSessionParams.fromSessionInit(abstractBody2, abstractBody);
                this.fireConnectionEstablished();
            }
            cMSessionParams = this.cmParams;
            this.checkForTerminalBindingConditions(abstractBody, n);
            if (BOSHClient.isTermination(abstractBody)) {
                this.lock.unlock();
                this.dispose(null);
                return;
            }
            if (BOSHClient.isRecoverableBindingCondition(abstractBody)) {
                if (arrayList == null) {
                    arrayList = new ArrayList<Object>(this.exchanges.size());
                }
                for (HTTPExchange hTTPExchange22 : this.exchanges) {
                    object = new HTTPExchange(hTTPExchange22.getRequest());
                    arrayList.add((HTTPExchange)object);
                }
                for (HTTPExchange hTTPExchange2 : arrayList) {
                    this.exchanges.add(hTTPExchange2);
                }
            } else {
                this.processRequestAcknowledgements(abstractBody2, abstractBody);
                this.processResponseAcknowledgementData(abstractBody2);
                Iterator iterator = this.processResponseAcknowledgementReport(abstractBody);
                if (iterator != null && arrayList == null) {
                    arrayList = new ArrayList(1);
                    arrayList.add(iterator);
                    this.exchanges.add((HTTPExchange)((Object)iterator));
                }
            }
        }
        catch (BOSHException var8_12) {
            LOG.log(Level.FINEST, "Could not process response", var8_12);
            this.lock.unlock();
            this.dispose(var8_12);
            return;
        }
        finally {
            if (this.lock.isHeldByCurrentThread()) {
                try {
                    this.exchanges.remove(hTTPExchange);
                    if (this.exchanges.isEmpty()) {
                        this.scheduleEmptyRequest(this.processPauseRequest(abstractBody2));
                    }
                    this.notFull.signalAll();
                }
                finally {
                    this.lock.unlock();
                }
            }
        }
        if (arrayList != null) {
            for (HTTPExchange hTTPExchange3 : arrayList) {
                object = this.httpSender.send(cMSessionParams, hTTPExchange3.getRequest());
                hTTPExchange3.setHTTPResponse((HTTPResponse)object);
                this.fireRequestSent(hTTPExchange3.getRequest());
            }
        }
    }

    private void clearEmptyRequest() {
        this.assertLocked();
        if (this.emptyRequestFuture != null) {
            this.emptyRequestFuture.cancel(false);
            this.emptyRequestFuture = null;
        }
    }

    private long getDefaultEmptyRequestDelay() {
        this.assertLocked();
        AttrPolling attrPolling = this.cmParams.getPollingInterval();
        long l = attrPolling == null ? (long)EMPTY_REQUEST_DELAY : (long)attrPolling.getInMilliseconds();
        return l;
    }

    private void scheduleEmptyRequest(long l) {
        this.assertLocked();
        if (l < 0) {
            throw new IllegalArgumentException("Empty request delay must be >= 0 (was: " + l + ")");
        }
        this.clearEmptyRequest();
        if (!this.isWorking()) {
            return;
        }
        if (LOG.isLoggable(Level.FINER)) {
            LOG.finer("Scheduling empty request in " + l + "ms");
        }
        try {
            this.emptyRequestFuture = this.schedExec.schedule(this.emptyRequestRunnable, l, TimeUnit.MILLISECONDS);
        }
        catch (RejectedExecutionException var3_2) {
            LOG.log(Level.FINEST, "Could not schedule empty request", var3_2);
        }
        this.drained.signalAll();
    }

    private void sendEmptyRequest() {
        this.assertUnlocked();
        LOG.finest("Sending empty request");
        try {
            this.send(ComposableBody.builder().build());
        }
        catch (BOSHException var1_1) {
            this.dispose(var1_1);
        }
    }

    private void assertLocked() {
        if (ASSERTIONS) {
            if (!this.lock.isHeldByCurrentThread()) {
                throw new AssertionError((Object)"Lock is not held by current thread");
            }
            return;
        }
    }

    private void assertUnlocked() {
        if (ASSERTIONS) {
            if (this.lock.isHeldByCurrentThread()) {
                throw new AssertionError((Object)"Lock is held by current thread");
            }
            return;
        }
    }

    private void checkForTerminalBindingConditions(AbstractBody abstractBody, int n) throws BOSHException {
        TerminalBindingCondition terminalBindingCondition = this.getTerminalBindingCondition(n, abstractBody);
        if (terminalBindingCondition != null) {
            throw new BOSHException("Terminal binding condition encountered: " + terminalBindingCondition.getCondition() + "  (" + terminalBindingCondition.getMessage() + ")");
        }
    }

    private static boolean isRecoverableBindingCondition(AbstractBody abstractBody) {
        return "error".equals(abstractBody.getAttribute(Attributes.TYPE));
    }

    private long processPauseRequest(AbstractBody abstractBody) {
        this.assertLocked();
        if (this.cmParams != null && this.cmParams.getMaxPause() != null) {
            try {
                AttrPause attrPause = AttrPause.createFromString(abstractBody.getAttribute(Attributes.PAUSE));
                if (attrPause != null) {
                    long l = attrPause.getInMilliseconds() - PAUSE_MARGIN;
                    if (l < 0) {
                        l = EMPTY_REQUEST_DELAY;
                    }
                    return l;
                }
            }
            catch (BOSHException var2_3) {
                LOG.log(Level.FINEST, "Could not extract", var2_3);
            }
        }
        return this.getDefaultEmptyRequestDelay();
    }

    private void processRequestAcknowledgements(AbstractBody abstractBody, AbstractBody abstractBody2) {
        this.assertLocked();
        if (!this.cmParams.isAckingRequests()) {
            return;
        }
        if (abstractBody2.getAttribute(Attributes.REPORT) != null) {
            return;
        }
        String string = abstractBody2.getAttribute(Attributes.ACK);
        Long l = string == null ? Long.valueOf(Long.parseLong(abstractBody.getAttribute(Attributes.RID))) : Long.valueOf(Long.parseLong(string));
        if (LOG.isLoggable(Level.FINEST)) {
            LOG.finest("Removing pending acks up to: " + l);
        }
        Iterator<ComposableBody> iterator = this.pendingRequestAcks.iterator();
        while (iterator.hasNext()) {
            AbstractBody abstractBody3 = iterator.next();
            Long l2 = Long.parseLong(abstractBody3.getAttribute(Attributes.RID));
            if (l2.compareTo(l) > 0) continue;
            iterator.remove();
        }
    }

    private void processResponseAcknowledgementData(AbstractBody abstractBody) {
        this.assertLocked();
        Long l = Long.parseLong(abstractBody.getAttribute(Attributes.RID));
        if (this.responseAck.equals(-1)) {
            this.responseAck = l;
        } else {
            this.pendingResponseAcks.add(l);
            Long l2 = this.responseAck;
            while (l2.equals(this.pendingResponseAcks.first())) {
                this.responseAck = l2;
                this.pendingResponseAcks.remove(l2);
                l2 = l2 + 1;
            }
        }
    }

    private HTTPExchange processResponseAcknowledgementReport(AbstractBody abstractBody) throws BOSHException {
        Object object;
        this.assertLocked();
        String string = abstractBody.getAttribute(Attributes.REPORT);
        if (string == null) {
            return null;
        }
        Long l = Long.parseLong(string);
        Long l2 = Long.parseLong(abstractBody.getAttribute(Attributes.TIME));
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Received report of missing request (RID=" + l + ", time=" + l2 + "ms)");
        }
        Iterator<ComposableBody> iterator = this.pendingRequestAcks.iterator();
        Object object2 = null;
        while (iterator.hasNext() && object2 == null) {
            object = iterator.next();
            Long l3 = Long.parseLong(object.getAttribute(Attributes.RID));
            if (!l.equals(l3)) continue;
            object2 = object;
        }
        if (object2 == null) {
            throw new BOSHException("Report of missing message with RID '" + string + "' but local copy of that request was not found");
        }
        object = new HTTPExchange((AbstractBody)object2);
        this.exchanges.add((HTTPExchange)object);
        this.notEmpty.signalAll();
        return object;
    }

    private void fireRequestSent(AbstractBody abstractBody) {
        this.assertUnlocked();
        BOSHMessageEvent bOSHMessageEvent = null;
        for (BOSHClientRequestListener bOSHClientRequestListener : this.requestListeners) {
            if (bOSHMessageEvent == null) {
                bOSHMessageEvent = BOSHMessageEvent.createRequestSentEvent(this, abstractBody);
            }
            try {
                bOSHClientRequestListener.requestSent(bOSHMessageEvent);
            }
            catch (Exception var5_5) {
                LOG.log(Level.WARNING, "Unhandled Exception", var5_5);
            }
        }
    }

    private void fireResponseReceived(AbstractBody abstractBody) {
        this.assertUnlocked();
        BOSHMessageEvent bOSHMessageEvent = null;
        for (BOSHClientResponseListener bOSHClientResponseListener : this.responseListeners) {
            if (bOSHMessageEvent == null) {
                bOSHMessageEvent = BOSHMessageEvent.createResponseReceivedEvent(this, abstractBody);
            }
            try {
                bOSHClientResponseListener.responseReceived(bOSHMessageEvent);
            }
            catch (Exception var5_5) {
                LOG.log(Level.WARNING, "Unhandled Exception", var5_5);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fireConnectionEstablished() {
        boolean bl = this.lock.isHeldByCurrentThread();
        if (bl) {
            this.lock.unlock();
        }
        try {
            BOSHClientConnEvent bOSHClientConnEvent = null;
            for (BOSHClientConnListener bOSHClientConnListener : this.connListeners) {
                if (bOSHClientConnEvent == null) {
                    bOSHClientConnEvent = BOSHClientConnEvent.createConnectionEstablishedEvent(this);
                }
                try {
                    bOSHClientConnListener.connectionEvent(bOSHClientConnEvent);
                }
                catch (Exception var5_5) {
                    LOG.log(Level.WARNING, "Unhandled Exception", var5_5);
                }
            }
        }
        finally {
            if (bl) {
                this.lock.lock();
            }
        }
    }

    private void fireConnectionClosed() {
        this.assertUnlocked();
        BOSHClientConnEvent bOSHClientConnEvent = null;
        for (BOSHClientConnListener bOSHClientConnListener : this.connListeners) {
            if (bOSHClientConnEvent == null) {
                bOSHClientConnEvent = BOSHClientConnEvent.createConnectionClosedEvent(this);
            }
            try {
                bOSHClientConnListener.connectionEvent(bOSHClientConnEvent);
            }
            catch (Exception var4_4) {
                LOG.log(Level.WARNING, "Unhandled Exception", var4_4);
            }
        }
    }

    private void fireConnectionClosedOnError(Throwable throwable) {
        this.assertUnlocked();
        BOSHClientConnEvent bOSHClientConnEvent = null;
        for (BOSHClientConnListener bOSHClientConnListener : this.connListeners) {
            if (bOSHClientConnEvent == null) {
                bOSHClientConnEvent = BOSHClientConnEvent.createConnectionClosedOnErrorEvent(this, this.pendingRequestAcks, throwable);
            }
            try {
                bOSHClientConnListener.connectionEvent(bOSHClientConnEvent);
            }
            catch (Exception var5_5) {
                LOG.log(Level.WARNING, "Unhandled Exception", var5_5);
            }
        }
    }

    static {
        String string = BOSHClient.class.getSimpleName() + ".assertionsEnabled";
        boolean bl = false;
        if (System.getProperty(string) == null) {
            if (!$assertionsDisabled) {
                bl = true;
                if (!true) {
                    throw new AssertionError();
                }
            }
        } else {
            bl = Boolean.getBoolean(string);
        }
        ASSERTIONS = bl;
    }

    static abstract class ExchangeInterceptor {
        ExchangeInterceptor() {
        }

        abstract HTTPExchange interceptExchange(HTTPExchange var1);
    }

}

