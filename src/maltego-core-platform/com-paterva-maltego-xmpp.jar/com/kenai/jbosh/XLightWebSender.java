/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.xlightweb.client.HttpClient
 */
package com.kenai.jbosh;

import com.kenai.jbosh.AbstractBody;
import com.kenai.jbosh.BOSHClientConfig;
import com.kenai.jbosh.CMSessionParams;
import com.kenai.jbosh.HTTPResponse;
import com.kenai.jbosh.HTTPSender;
import com.kenai.jbosh.XLightWebResponse;
import java.io.IOException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLContext;
import org.xlightweb.client.HttpClient;

final class XLightWebSender
implements HTTPSender {
    private static final Logger LOG = Logger.getLogger(XLightWebSender.class.getName());
    private final Lock lock = new ReentrantLock();
    private HttpClient client;
    private BOSHClientConfig cfg;

    XLightWebSender() {
    }

    @Override
    public void init(BOSHClientConfig bOSHClientConfig) {
        this.lock.lock();
        try {
            this.cfg = bOSHClientConfig;
            this.client = bOSHClientConfig.getSSLContext() != null ? new HttpClient(bOSHClientConfig.getSSLContext()) : new HttpClient();
            if (this.cfg != null && this.cfg.getProxyHost() != null && this.cfg.getProxyPort() != 0) {
                this.client.setProxyHost(this.cfg.getProxyHost());
                this.client.setProxyPort(this.cfg.getProxyPort());
            }
        }
        finally {
            this.lock.unlock();
        }
    }

    @Override
    public void destroy() {
        this.lock.lock();
        try {
            if (this.client != null) {
                this.client.close();
            }
        }
        catch (IOException var1_1) {
            LOG.log(Level.FINEST, "Ignoring exception on close", var1_1);
        }
        finally {
            this.client = null;
            this.cfg = null;
            this.lock.unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public HTTPResponse send(CMSessionParams cMSessionParams, AbstractBody abstractBody) {
        BOSHClientConfig bOSHClientConfig;
        HttpClient httpClient;
        this.lock.lock();
        try {
            httpClient = this.client;
            bOSHClientConfig = this.cfg;
        }
        finally {
            this.lock.unlock();
        }
        return new XLightWebResponse(httpClient, bOSHClientConfig, cMSessionParams, abstractBody);
    }
}

