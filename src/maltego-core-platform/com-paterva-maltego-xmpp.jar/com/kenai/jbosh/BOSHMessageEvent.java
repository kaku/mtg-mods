/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.AbstractBody;
import com.kenai.jbosh.BOSHClient;
import java.util.EventObject;

public final class BOSHMessageEvent
extends EventObject {
    private static final long serialVersionUID = 1;
    private final AbstractBody body;

    private BOSHMessageEvent(Object object, AbstractBody abstractBody) {
        super(object);
        if (abstractBody == null) {
            throw new IllegalArgumentException("message body may not be null");
        }
        this.body = abstractBody;
    }

    static BOSHMessageEvent createRequestSentEvent(BOSHClient bOSHClient, AbstractBody abstractBody) {
        return new BOSHMessageEvent(bOSHClient, abstractBody);
    }

    static BOSHMessageEvent createResponseReceivedEvent(BOSHClient bOSHClient, AbstractBody abstractBody) {
        return new BOSHMessageEvent(bOSHClient, abstractBody);
    }

    public AbstractBody getBody() {
        return this.body;
    }
}

