/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.AbstractBody;
import com.kenai.jbosh.AttrAccept;
import com.kenai.jbosh.AttrAck;
import com.kenai.jbosh.AttrCharsets;
import com.kenai.jbosh.AttrHold;
import com.kenai.jbosh.AttrInactivity;
import com.kenai.jbosh.AttrMaxPause;
import com.kenai.jbosh.AttrPolling;
import com.kenai.jbosh.AttrRequests;
import com.kenai.jbosh.AttrSessionID;
import com.kenai.jbosh.AttrVersion;
import com.kenai.jbosh.AttrWait;
import com.kenai.jbosh.Attributes;
import com.kenai.jbosh.BOSHException;
import com.kenai.jbosh.BodyQName;

final class CMSessionParams {
    private final AttrSessionID sid;
    private final AttrWait wait;
    private final AttrVersion ver;
    private final AttrPolling polling;
    private final AttrInactivity inactivity;
    private final AttrRequests requests;
    private final AttrHold hold;
    private final AttrAccept accept;
    private final AttrMaxPause maxPause;
    private final AttrAck ack;
    private final AttrCharsets charsets;
    private final boolean ackingRequests;

    private CMSessionParams(AttrSessionID attrSessionID, AttrWait attrWait, AttrVersion attrVersion, AttrPolling attrPolling, AttrInactivity attrInactivity, AttrRequests attrRequests, AttrHold attrHold, AttrAccept attrAccept, AttrMaxPause attrMaxPause, AttrAck attrAck, AttrCharsets attrCharsets, boolean bl) {
        this.sid = attrSessionID;
        this.wait = attrWait;
        this.ver = attrVersion;
        this.polling = attrPolling;
        this.inactivity = attrInactivity;
        this.requests = attrRequests;
        this.hold = attrHold;
        this.accept = attrAccept;
        this.maxPause = attrMaxPause;
        this.ack = attrAck;
        this.charsets = attrCharsets;
        this.ackingRequests = bl;
    }

    static CMSessionParams fromSessionInit(AbstractBody abstractBody, AbstractBody abstractBody2) throws BOSHException {
        AttrAck attrAck = AttrAck.createFromString(abstractBody2.getAttribute(Attributes.ACK));
        String string = abstractBody.getAttribute(Attributes.RID);
        boolean bl = attrAck != null && ((String)attrAck.getValue()).equals(string);
        return new CMSessionParams(AttrSessionID.createFromString(CMSessionParams.getRequiredAttribute(abstractBody2, Attributes.SID)), AttrWait.createFromString(CMSessionParams.getRequiredAttribute(abstractBody2, Attributes.WAIT)), AttrVersion.createFromString(abstractBody2.getAttribute(Attributes.VER)), AttrPolling.createFromString(abstractBody2.getAttribute(Attributes.POLLING)), AttrInactivity.createFromString(abstractBody2.getAttribute(Attributes.INACTIVITY)), AttrRequests.createFromString(abstractBody2.getAttribute(Attributes.REQUESTS)), AttrHold.createFromString(abstractBody2.getAttribute(Attributes.HOLD)), AttrAccept.createFromString(abstractBody2.getAttribute(Attributes.ACCEPT)), AttrMaxPause.createFromString(abstractBody2.getAttribute(Attributes.MAXPAUSE)), attrAck, AttrCharsets.createFromString(abstractBody2.getAttribute(Attributes.CHARSETS)), bl);
    }

    private static String getRequiredAttribute(AbstractBody abstractBody, BodyQName bodyQName) throws BOSHException {
        String string = abstractBody.getAttribute(bodyQName);
        if (string == null) {
            throw new BOSHException("Connection Manager session creation response did not include required '" + bodyQName.getLocalPart() + "' attribute");
        }
        return string;
    }

    AttrSessionID getSessionID() {
        return this.sid;
    }

    AttrWait getWait() {
        return this.wait;
    }

    AttrVersion getVersion() {
        return this.ver;
    }

    AttrPolling getPollingInterval() {
        return this.polling;
    }

    AttrInactivity getInactivityPeriod() {
        return this.inactivity;
    }

    AttrRequests getRequests() {
        return this.requests;
    }

    AttrHold getHold() {
        return this.hold;
    }

    AttrAccept getAccept() {
        return this.accept;
    }

    AttrMaxPause getMaxPause() {
        return this.maxPause;
    }

    AttrAck getAck() {
        return this.ack;
    }

    AttrCharsets getCharsets() {
        return this.charsets;
    }

    boolean isAckingRequests() {
        return this.ackingRequests;
    }
}

