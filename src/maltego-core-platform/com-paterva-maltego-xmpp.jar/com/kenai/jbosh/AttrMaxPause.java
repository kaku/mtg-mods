/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.AbstractIntegerAttr;
import com.kenai.jbosh.BOSHException;
import java.util.concurrent.TimeUnit;

final class AttrMaxPause
extends AbstractIntegerAttr {
    private AttrMaxPause(String string) throws BOSHException {
        super(string);
        this.checkMinValue(1);
    }

    static AttrMaxPause createFromString(String string) throws BOSHException {
        if (string == null) {
            return null;
        }
        return new AttrMaxPause(string);
    }

    public int getInMilliseconds() {
        return (int)TimeUnit.MILLISECONDS.convert(this.intValue(), TimeUnit.SECONDS);
    }
}

