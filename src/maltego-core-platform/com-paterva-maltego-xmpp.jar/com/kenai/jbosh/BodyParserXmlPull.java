/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.xmlpull.v1.XmlPullParser
 *  org.xmlpull.v1.XmlPullParserException
 *  org.xmlpull.v1.XmlPullParserFactory
 */
package com.kenai.jbosh;

import com.kenai.jbosh.AbstractBody;
import com.kenai.jbosh.BOSHException;
import com.kenai.jbosh.BodyParser;
import com.kenai.jbosh.BodyParserResults;
import com.kenai.jbosh.BodyQName;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.ref.SoftReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

final class BodyParserXmlPull
implements BodyParser {
    private static final Logger LOG = Logger.getLogger(BodyParserXmlPull.class.getName());
    private static final ThreadLocal<SoftReference<XmlPullParser>> XPP_PARSER = new ThreadLocal<SoftReference<XmlPullParser>>(){

        @Override
        protected SoftReference<XmlPullParser> initialValue() {
            return new SoftReference<Object>(null);
        }
    };

    BodyParserXmlPull() {
    }

    @Override
    public BodyParserResults parse(String string) throws BOSHException {
        Exception exception2;
        Exception exception2;
        BodyParserResults bodyParserResults = new BodyParserResults();
        try {
            XmlPullParser xmlPullParser = BodyParserXmlPull.getXmlPullParser();
            xmlPullParser.setInput((Reader)new StringReader(string));
            int n = xmlPullParser.getEventType();
            while (n != 1) {
                BodyQName bodyQName;
                if (n == 2) {
                    if (LOG.isLoggable(Level.FINEST)) {
                        LOG.finest("Start tag: " + xmlPullParser.getName());
                    }
                } else {
                    n = xmlPullParser.next();
                    continue;
                }
                String string2 = xmlPullParser.getPrefix();
                if (string2 == null) {
                    string2 = "";
                }
                String string3 = xmlPullParser.getNamespace();
                String string4 = xmlPullParser.getName();
                QName qName = new QName(string3, string4, string2);
                if (LOG.isLoggable(Level.FINEST)) {
                    LOG.finest("Start element: ");
                    LOG.finest("    prefix: " + string2);
                    LOG.finest("    URI: " + string3);
                    LOG.finest("    local: " + string4);
                }
                if (!(bodyQName = AbstractBody.getBodyQName()).equalsQName(qName)) {
                    throw new IllegalStateException("Root element was not '" + bodyQName.getLocalPart() + "' in the '" + bodyQName.getNamespaceURI() + "' namespace.  (Was '" + string4 + "' in '" + string3 + "')");
                }
                for (int i = 0; i < xmlPullParser.getAttributeCount(); ++i) {
                    String string5;
                    String string6 = xmlPullParser.getAttributeNamespace(i);
                    if (string6.length() == 0) {
                        string6 = xmlPullParser.getNamespace(null);
                    }
                    if ((string5 = xmlPullParser.getAttributePrefix(i)) == null) {
                        string5 = "";
                    }
                    String string7 = xmlPullParser.getAttributeName(i);
                    String string8 = xmlPullParser.getAttributeValue(i);
                    BodyQName bodyQName2 = BodyQName.createWithPrefix(string6, string7, string5);
                    if (LOG.isLoggable(Level.FINEST)) {
                        LOG.finest("        Attribute: {" + string6 + "}" + string7 + " = '" + string8 + "'");
                    }
                    bodyParserResults.addBodyAttributeValue(bodyQName2, string8);
                }
            }
            return bodyParserResults;
        }
        catch (RuntimeException var4_4) {
            exception2 = var4_4;
        }
        catch (XmlPullParserException var4_5) {
            exception2 = var4_5;
        }
        catch (IOException var4_6) {
            exception2 = var4_6;
        }
        throw new BOSHException("Could not parse body:\n" + string, exception2);
    }

    private static XmlPullParser getXmlPullParser() {
        SoftReference<XmlPullParser> softReference = XPP_PARSER.get();
        XmlPullParser xmlPullParser = softReference.get();
        if (xmlPullParser == null) {
            try {
                XmlPullParserFactory xmlPullParserFactory = XmlPullParserFactory.newInstance();
                xmlPullParserFactory.setNamespaceAware(true);
                xmlPullParserFactory.setValidating(false);
                xmlPullParser = xmlPullParserFactory.newPullParser();
                softReference = new SoftReference<XmlPullParser>(xmlPullParser);
                XPP_PARSER.set(softReference);
                return xmlPullParser;
            }
            catch (Exception var3_3) {
                Exception exception = var3_3;
                throw new IllegalStateException("Could not create XmlPull parser", exception);
            }
        }
        return xmlPullParser;
    }

}

