/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import java.net.URI;
import javax.net.ssl.SSLContext;

public final class BOSHClientConfig {
    private final URI uri;
    private final String to;
    private final String from;
    private final String lang;
    private final String route;
    private final String proxyHost;
    private final int proxyPort;
    private final SSLContext sslContext;
    private final boolean compressionEnabled;

    private BOSHClientConfig(URI uRI, String string, String string2, String string3, String string4, String string5, int n, SSLContext sSLContext, boolean bl) {
        this.uri = uRI;
        this.to = string;
        this.from = string2;
        this.lang = string3;
        this.route = string4;
        this.proxyHost = string5;
        this.proxyPort = n;
        this.sslContext = sSLContext;
        this.compressionEnabled = bl;
    }

    public URI getURI() {
        return this.uri;
    }

    public String getTo() {
        return this.to;
    }

    public String getFrom() {
        return this.from;
    }

    public String getLang() {
        return this.lang;
    }

    public String getRoute() {
        return this.route;
    }

    public String getProxyHost() {
        return this.proxyHost;
    }

    public int getProxyPort() {
        return this.proxyPort;
    }

    public SSLContext getSSLContext() {
        return this.sslContext;
    }

    boolean isCompressionEnabled() {
        return this.compressionEnabled;
    }

    public static final class Builder {
        private final URI bURI;
        private final String bDomain;
        private String bFrom;
        private String bLang;
        private String bRoute;
        private String bProxyHost;
        private int bProxyPort;
        private SSLContext bSSLContext;
        private Boolean bCompression;

        private Builder(URI uRI, String string) {
            this.bURI = uRI;
            this.bDomain = string;
        }

        public static Builder create(URI uRI, String string) {
            if (uRI == null) {
                throw new IllegalArgumentException("Connection manager URI must not be null");
            }
            if (string == null) {
                throw new IllegalArgumentException("Target domain must not be null");
            }
            String string2 = uRI.getScheme();
            if (!"http".equals(string2) && !"https".equals(string2)) {
                throw new IllegalArgumentException("Only 'http' and 'https' URI are allowed");
            }
            return new Builder(uRI, string);
        }

        public static Builder create(BOSHClientConfig bOSHClientConfig) {
            Builder builder = new Builder(bOSHClientConfig.getURI(), bOSHClientConfig.getTo());
            builder.bFrom = bOSHClientConfig.getFrom();
            builder.bLang = bOSHClientConfig.getLang();
            builder.bRoute = bOSHClientConfig.getRoute();
            builder.bProxyHost = bOSHClientConfig.getProxyHost();
            builder.bProxyPort = bOSHClientConfig.getProxyPort();
            builder.bSSLContext = bOSHClientConfig.getSSLContext();
            builder.bCompression = bOSHClientConfig.isCompressionEnabled();
            return builder;
        }

        public Builder setFrom(String string) {
            if (string == null) {
                throw new IllegalArgumentException("Client ID must not be null");
            }
            this.bFrom = string;
            return this;
        }

        public Builder setXMLLang(String string) {
            if (string == null) {
                throw new IllegalArgumentException("Default language ID must not be null");
            }
            this.bLang = string;
            return this;
        }

        public Builder setRoute(String string, String string2, int n) {
            if (string == null) {
                throw new IllegalArgumentException("Protocol cannot be null");
            }
            if (string.contains(":")) {
                throw new IllegalArgumentException("Protocol cannot contain the ':' character");
            }
            if (string2 == null) {
                throw new IllegalArgumentException("Host cannot be null");
            }
            if (string2.contains(":")) {
                throw new IllegalArgumentException("Host cannot contain the ':' character");
            }
            if (n <= 0) {
                throw new IllegalArgumentException("Port number must be > 0");
            }
            this.bRoute = string + ":" + string2 + ":" + n;
            return this;
        }

        public Builder setProxy(String string, int n) {
            if (string == null || string.length() == 0) {
                throw new IllegalArgumentException("Proxy host name cannot be null or empty");
            }
            if (n <= 0) {
                throw new IllegalArgumentException("Proxy port must be > 0");
            }
            this.bProxyHost = string;
            this.bProxyPort = n;
            return this;
        }

        public Builder setSSLContext(SSLContext sSLContext) {
            if (sSLContext == null) {
                throw new IllegalArgumentException("SSL context cannot be null");
            }
            this.bSSLContext = sSLContext;
            return this;
        }

        public Builder setCompressionEnabled(boolean bl) {
            this.bCompression = bl;
            return this;
        }

        public BOSHClientConfig build() {
            String string = this.bLang == null ? "en" : this.bLang;
            int n = this.bProxyHost == null ? 0 : this.bProxyPort;
            boolean bl = this.bCompression == null ? false : this.bCompression;
            return new BOSHClientConfig(this.bURI, this.bDomain, this.bFrom, string, this.bRoute, this.bProxyHost, n, this.bSSLContext, bl);
        }
    }

}

