/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.BodyQName;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

public abstract class AbstractBody {
    AbstractBody() {
    }

    public final Set<BodyQName> getAttributeNames() {
        Map<BodyQName, String> map = this.getAttributes();
        return Collections.unmodifiableSet(map.keySet());
    }

    public final String getAttribute(BodyQName bodyQName) {
        Map<BodyQName, String> map = this.getAttributes();
        return map.get(bodyQName);
    }

    public abstract Map<BodyQName, String> getAttributes();

    public abstract String toXML();

    static BodyQName getBodyQName() {
        return BodyQName.createBOSH("body");
    }
}

