/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.AbstractBody;
import com.kenai.jbosh.BOSHException;
import com.kenai.jbosh.BodyQName;
import com.kenai.jbosh.StaticBody;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ComposableBody
extends AbstractBody {
    private static final Pattern BOSH_START = Pattern.compile("<(?:(?:[^:\t\n\r >]+:)|(?:\\{[^\\}>]*?}))?body(?:[\t\n\r ][^>]*?)?(/>|>)");
    private final Map<BodyQName, String> attrs;
    private final String payload;
    private final AtomicReference<String> computed = new AtomicReference();

    private ComposableBody(Map<BodyQName, String> map, String string) {
        this.attrs = map;
        this.payload = string;
    }

    static ComposableBody fromStaticBody(StaticBody staticBody) throws BOSHException {
        String string;
        String string2 = staticBody.toXML();
        Matcher matcher = BOSH_START.matcher(string2);
        if (!matcher.find()) {
            throw new BOSHException("Could not locate 'body' element in XML.  The raw XML did not match the pattern: " + BOSH_START);
        }
        if (">".equals(matcher.group(1))) {
            int n = matcher.end();
            int n2 = string2.lastIndexOf("</");
            if (n2 < n) {
                n2 = n;
            }
            string = string2.substring(n, n2);
        } else {
            string = "";
        }
        return new ComposableBody(staticBody.getAttributes(), string);
    }

    public static Builder builder() {
        return new Builder();
    }

    public Builder rebuild() {
        return Builder.fromBody(this);
    }

    @Override
    public Map<BodyQName, String> getAttributes() {
        return Collections.unmodifiableMap(this.attrs);
    }

    @Override
    public String toXML() {
        String string = this.computed.get();
        if (string == null) {
            string = this.computeXML();
            this.computed.set(string);
        }
        return string;
    }

    public String getPayloadXML() {
        return this.payload;
    }

    private String escape(String string) {
        return string.replace("'", "&apos;");
    }

    private String computeXML() {
        BodyQName bodyQName = ComposableBody.getBodyQName();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<");
        stringBuilder.append(bodyQName.getLocalPart());
        for (Map.Entry<BodyQName, String> entry : this.attrs.entrySet()) {
            stringBuilder.append(" ");
            BodyQName bodyQName2 = entry.getKey();
            String string = bodyQName2.getPrefix();
            if (string != null && string.length() > 0) {
                stringBuilder.append(string);
                stringBuilder.append(":");
            }
            stringBuilder.append(bodyQName2.getLocalPart());
            stringBuilder.append("='");
            stringBuilder.append(this.escape(entry.getValue()));
            stringBuilder.append("'");
        }
        stringBuilder.append(" ");
        stringBuilder.append("xmlns");
        stringBuilder.append("='");
        stringBuilder.append(bodyQName.getNamespaceURI());
        stringBuilder.append("'>");
        if (this.payload != null) {
            stringBuilder.append(this.payload);
        }
        stringBuilder.append("</body>");
        return stringBuilder.toString();
    }

    public static final class Builder {
        private Map<BodyQName, String> map;
        private boolean doMapCopy;
        private String payloadXML;

        private Builder() {
        }

        private static Builder fromBody(ComposableBody composableBody) {
            Builder builder = new Builder();
            builder.map = composableBody.getAttributes();
            builder.doMapCopy = true;
            builder.payloadXML = composableBody.payload;
            return builder;
        }

        public Builder setPayloadXML(String string) {
            if (string == null) {
                throw new IllegalArgumentException("payload XML argument cannot be null");
            }
            this.payloadXML = string;
            return this;
        }

        public Builder setAttribute(BodyQName bodyQName, String string) {
            if (this.map == null) {
                this.map = new HashMap<BodyQName, String>();
            } else if (this.doMapCopy) {
                this.map = new HashMap<BodyQName, String>(this.map);
                this.doMapCopy = false;
            }
            if (string == null) {
                this.map.remove(bodyQName);
            } else {
                this.map.put(bodyQName, string);
            }
            return this;
        }

        public Builder setNamespaceDefinition(String string, String string2) {
            BodyQName bodyQName = BodyQName.createWithPrefix("http://www.w3.org/XML/1998/namespace", string, "xmlns");
            return this.setAttribute(bodyQName, string2);
        }

        public ComposableBody build() {
            if (this.map == null) {
                this.map = new HashMap<BodyQName, String>();
            }
            if (this.payloadXML == null) {
                this.payloadXML = "";
            }
            return new ComposableBody(this.map, this.payloadXML);
        }
    }

}

