/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.AbstractAttr;
import com.kenai.jbosh.BOSHException;

final class AttrAccept
extends AbstractAttr<String> {
    private final String[] encodings;

    private AttrAccept(String string) {
        super(string);
        this.encodings = string.split("[\\s,]+");
    }

    static AttrAccept createFromString(String string) throws BOSHException {
        if (string == null) {
            return null;
        }
        return new AttrAccept(string);
    }

    boolean isAccepted(String string) {
        for (String string2 : this.encodings) {
            if (!string2.equalsIgnoreCase(string)) continue;
            return true;
        }
        return false;
    }
}

