/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import java.util.HashMap;
import java.util.Map;

final class TerminalBindingCondition {
    private static final Map<String, TerminalBindingCondition> COND_TO_INSTANCE = new HashMap<String, TerminalBindingCondition>();
    private static final Map<Integer, TerminalBindingCondition> CODE_TO_INSTANCE = new HashMap<Integer, TerminalBindingCondition>();
    static final TerminalBindingCondition BAD_REQUEST = TerminalBindingCondition.createWithCode("bad-request", "The format of an HTTP header or binding element received from the client is unacceptable (e.g., syntax error).", 400);
    static final TerminalBindingCondition HOST_GONE = TerminalBindingCondition.create("host-gone", "The target domain specified in the 'to' attribute or the target host or port specified in the 'route' attribute is no longer serviced by the connection manager.");
    static final TerminalBindingCondition HOST_UNKNOWN = TerminalBindingCondition.create("host-unknown", "The target domain specified in the 'to' attribute or the target host or port specified in the 'route' attribute is unknown to the connection manager.");
    static final TerminalBindingCondition IMPROPER_ADDRESSING = TerminalBindingCondition.create("improper-addressing", "The initialization element lacks a 'to' or 'route' attribute (or the attribute has no value) but the connection manager requires one.");
    static final TerminalBindingCondition INTERNAL_SERVER_ERROR = TerminalBindingCondition.create("internal-server-error", "The connection manager has experienced an internal error that prevents it from servicing the request.");
    static final TerminalBindingCondition ITEM_NOT_FOUND = TerminalBindingCondition.createWithCode("item-not-found", "(1) 'sid' is not valid, (2) 'stream' is not valid, (3) 'rid' is larger than the upper limit of the expected window, (4) connection manager is unable to resend response, (5) 'key' sequence is invalid.", 404);
    static final TerminalBindingCondition OTHER_REQUEST = TerminalBindingCondition.create("other-request", "Another request being processed at the same time as this request caused the session to terminate.");
    static final TerminalBindingCondition POLICY_VIOLATION = TerminalBindingCondition.createWithCode("policy-violation", "The client has broken the session rules (polling too frequently, requesting too frequently, sending too many simultaneous requests).", 403);
    static final TerminalBindingCondition REMOTE_CONNECTION_FAILED = TerminalBindingCondition.create("remote-connection-failed", "The connection manager was unable to connect to, or unable to connect securely to, or has lost its connection to, the server.");
    static final TerminalBindingCondition REMOTE_STREAM_ERROR = TerminalBindingCondition.create("remote-stream-error", "Encapsulated transport protocol error.");
    static final TerminalBindingCondition SEE_OTHER_URI = TerminalBindingCondition.create("see-other-uri", "The connection manager does not operate at this URI (e.g., the connection manager accepts only SSL or TLS connections at some https: URI rather than the http: URI requested by the client).");
    static final TerminalBindingCondition SYSTEM_SHUTDOWN = TerminalBindingCondition.create("system-shutdown", "The connection manager is being shut down. All active HTTP sessions are being terminated. No new sessions can be created.");
    static final TerminalBindingCondition UNDEFINED_CONDITION = TerminalBindingCondition.create("undefined-condition", "Unknown or undefined error condition.");
    private final String cond;
    private final String msg;

    private TerminalBindingCondition(String string, String string2) {
        this.cond = string;
        this.msg = string2;
    }

    private static TerminalBindingCondition create(String string, String string2) {
        return TerminalBindingCondition.createWithCode(string, string2, null);
    }

    private static TerminalBindingCondition createWithCode(String string, String string2, Integer n) {
        if (string == null) {
            throw new IllegalArgumentException("condition may not be null");
        }
        if (string2 == null) {
            throw new IllegalArgumentException("message may not be null");
        }
        if (COND_TO_INSTANCE.get(string) != null) {
            throw new IllegalStateException("Multiple definitions of condition: " + string);
        }
        TerminalBindingCondition terminalBindingCondition = new TerminalBindingCondition(string, string2);
        COND_TO_INSTANCE.put(string, terminalBindingCondition);
        if (n != null) {
            if (CODE_TO_INSTANCE.get(n) != null) {
                throw new IllegalStateException("Multiple definitions of code: " + n);
            }
            CODE_TO_INSTANCE.put(n, terminalBindingCondition);
        }
        return terminalBindingCondition;
    }

    static TerminalBindingCondition forString(String string) {
        return COND_TO_INSTANCE.get(string);
    }

    static TerminalBindingCondition forHTTPResponseCode(int n) {
        return CODE_TO_INSTANCE.get(n);
    }

    String getCondition() {
        return this.cond;
    }

    String getMessage() {
        return this.msg;
    }
}

