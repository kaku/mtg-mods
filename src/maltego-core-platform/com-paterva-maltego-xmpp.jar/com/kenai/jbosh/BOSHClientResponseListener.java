/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.BOSHMessageEvent;

public interface BOSHClientResponseListener {
    public void responseReceived(BOSHMessageEvent var1);
}

