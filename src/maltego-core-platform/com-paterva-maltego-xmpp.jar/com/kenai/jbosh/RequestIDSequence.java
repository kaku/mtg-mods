/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import java.security.SecureRandom;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

final class RequestIDSequence {
    private static final int MAX_BITS = 53;
    private static final int INCREMENT_BITS = 32;
    private static final long MIN_INCREMENTS = 0x100000000L;
    private static final long MAX_INITIAL = 9007194959773696L;
    private static final long MASK = 0x1FFFFFFFFFFFFFL;
    private static final SecureRandom RAND = new SecureRandom();
    private static final Lock LOCK = new ReentrantLock();
    private AtomicLong nextRequestID = new AtomicLong();

    RequestIDSequence() {
        this.nextRequestID = new AtomicLong(this.generateInitialValue());
    }

    public long getNextRID() {
        return this.nextRequestID.getAndIncrement();
    }

    private long generateInitialValue() {
        long l;
        LOCK.lock();
        try {
            while ((l = RAND.nextLong() & 0x1FFFFFFFFFFFFFL) > 9007194959773696L) {
            }
        }
        finally {
            LOCK.unlock();
        }
        return l;
    }
}

