/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.AbstractBody;
import com.kenai.jbosh.HTTPResponse;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

final class HTTPExchange {
    private static final Logger LOG = Logger.getLogger(HTTPExchange.class.getName());
    private final AbstractBody request;
    private final Lock lock = new ReentrantLock();
    private final Condition ready = this.lock.newCondition();
    private HTTPResponse response;

    HTTPExchange(AbstractBody abstractBody) {
        if (abstractBody == null) {
            throw new IllegalArgumentException("Request body cannot be null");
        }
        this.request = abstractBody;
    }

    AbstractBody getRequest() {
        return this.request;
    }

    void setHTTPResponse(HTTPResponse hTTPResponse) {
        this.lock.lock();
        try {
            if (this.response != null) {
                throw new IllegalStateException("HTTPResponse was already set");
            }
            this.response = hTTPResponse;
            this.ready.signalAll();
        }
        finally {
            this.lock.unlock();
        }
    }

    HTTPResponse getHTTPResponse() {
        this.lock.lock();
        try {
            while (this.response == null) {
                try {
                    this.ready.await();
                }
                catch (InterruptedException var1_1) {
                    LOG.log(Level.FINEST, "Interrupted", var1_1);
                }
            }
            HTTPResponse hTTPResponse = this.response;
            return hTTPResponse;
        }
        finally {
            this.lock.unlock();
        }
    }
}

