/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.AbstractAttr;
import com.kenai.jbosh.BOSHException;

final class AttrAck
extends AbstractAttr<String> {
    private AttrAck(String string) throws BOSHException {
        super(string);
    }

    static AttrAck createFromString(String string) throws BOSHException {
        if (string == null) {
            return null;
        }
        return new AttrAck(string);
    }
}

