/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.AbstractBody;
import com.kenai.jbosh.BOSHClientConfig;
import com.kenai.jbosh.CMSessionParams;
import com.kenai.jbosh.HTTPResponse;

interface HTTPSender {
    public void init(BOSHClientConfig var1);

    public void destroy();

    public HTTPResponse send(CMSessionParams var1, AbstractBody var2);
}

