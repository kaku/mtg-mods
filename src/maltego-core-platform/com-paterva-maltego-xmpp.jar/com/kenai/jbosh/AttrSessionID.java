/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.AbstractAttr;

final class AttrSessionID
extends AbstractAttr<String> {
    private AttrSessionID(String string) {
        super(string);
    }

    static AttrSessionID createFromString(String string) {
        return new AttrSessionID(string);
    }
}

