/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

public class BOSHException
extends Exception {
    private static final long serialVersionUID = 1;

    public BOSHException(String string) {
        super(string);
    }

    public BOSHException(String string, Throwable throwable) {
        super(string, throwable);
    }
}

