/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.AbstractIntegerAttr;
import com.kenai.jbosh.BOSHException;

final class AttrWait
extends AbstractIntegerAttr {
    private AttrWait(String string) throws BOSHException {
        super(string);
        this.checkMinValue(1);
    }

    static AttrWait createFromString(String string) throws BOSHException {
        if (string == null) {
            return null;
        }
        return new AttrWait(string);
    }
}

