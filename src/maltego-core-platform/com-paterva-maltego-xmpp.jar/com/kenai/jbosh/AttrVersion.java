/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.AbstractAttr;
import com.kenai.jbosh.BOSHException;

final class AttrVersion
extends AbstractAttr<String>
implements Comparable {
    private static final AttrVersion DEFAULT;
    private final int major;
    private final int minor;

    private AttrVersion(String string) throws BOSHException {
        super(string);
        int n = string.indexOf(46);
        if (n <= 0) {
            throw new BOSHException("Illegal ver attribute value (not in major.minor form): " + string);
        }
        String string2 = string.substring(0, n);
        try {
            this.major = Integer.parseInt(string2);
        }
        catch (NumberFormatException var4_4) {
            throw new BOSHException("Could not parse ver attribute value (major ver): " + string2, var4_4);
        }
        if (this.major < 0) {
            throw new BOSHException("Major version may not be < 0");
        }
        String string3 = string.substring(n + 1);
        try {
            this.minor = Integer.parseInt(string3);
        }
        catch (NumberFormatException var5_6) {
            throw new BOSHException("Could not parse ver attribute value (minor ver): " + string3, var5_6);
        }
        if (this.minor < 0) {
            throw new BOSHException("Minor version may not be < 0");
        }
    }

    static AttrVersion getSupportedVersion() {
        return DEFAULT;
    }

    static AttrVersion createFromString(String string) throws BOSHException {
        if (string == null) {
            return null;
        }
        return new AttrVersion(string);
    }

    int getMajor() {
        return this.major;
    }

    int getMinor() {
        return this.minor;
    }

    @Override
    public int compareTo(Object object) {
        if (object instanceof AttrVersion) {
            AttrVersion attrVersion = (AttrVersion)object;
            if (this.major < attrVersion.major) {
                return -1;
            }
            if (this.major > attrVersion.major) {
                return 1;
            }
            if (this.minor < attrVersion.minor) {
                return -1;
            }
            if (this.minor > attrVersion.minor) {
                return 1;
            }
            return 0;
        }
        return 0;
    }

    static {
        try {
            DEFAULT = AttrVersion.createFromString("1.8");
        }
        catch (BOSHException var0) {
            throw new IllegalStateException(var0);
        }
    }
}

