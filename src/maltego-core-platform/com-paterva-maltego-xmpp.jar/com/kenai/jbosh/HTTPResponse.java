/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.AbstractBody;
import com.kenai.jbosh.BOSHException;

interface HTTPResponse {
    public void abort();

    public int getHTTPStatus() throws InterruptedException, BOSHException;

    public AbstractBody getBody() throws InterruptedException, BOSHException;
}

