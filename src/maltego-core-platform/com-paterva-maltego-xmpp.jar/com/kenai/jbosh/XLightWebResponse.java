/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.xlightweb.BlockingBodyDataSource
 *  org.xlightweb.IFutureResponse
 *  org.xlightweb.IHttpRequest
 *  org.xlightweb.IHttpResponse
 *  org.xlightweb.PostRequest
 *  org.xlightweb.client.HttpClient
 */
package com.kenai.jbosh;

import com.kenai.jbosh.AbstractBody;
import com.kenai.jbosh.AttrAccept;
import com.kenai.jbosh.BOSHClientConfig;
import com.kenai.jbosh.BOSHException;
import com.kenai.jbosh.CMSessionParams;
import com.kenai.jbosh.GZIPCodec;
import com.kenai.jbosh.HTTPResponse;
import com.kenai.jbosh.StaticBody;
import com.kenai.jbosh.ZLIBCodec;
import java.io.IOException;
import java.net.URI;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.xlightweb.BlockingBodyDataSource;
import org.xlightweb.IFutureResponse;
import org.xlightweb.IHttpRequest;
import org.xlightweb.IHttpResponse;
import org.xlightweb.PostRequest;
import org.xlightweb.client.HttpClient;

final class XLightWebResponse
implements HTTPResponse {
    private static final String ACCEPT_ENCODING = "Accept-Encoding";
    private static final String ACCEPT_ENCODING_VAL = ZLIBCodec.getID() + ", " + GZIPCodec.getID();
    private static final String CONTENT_ENCODING = "Content-Encoding";
    private static final String CHARSET = "UTF-8";
    private static final String CONTENT_TYPE = "text/xml; charset=utf-8";
    private final Lock lock = new ReentrantLock();
    private final IFutureResponse future;
    private IHttpResponse httpResp;
    private AbstractBody resp;
    private BOSHException toThrow;

    XLightWebResponse(HttpClient httpClient, BOSHClientConfig bOSHClientConfig, CMSessionParams cMSessionParams, AbstractBody abstractBody) {
        IFutureResponse iFutureResponse;
        try {
            AttrAccept attrAccept;
            String string = abstractBody.toXML();
            byte[] arrby = string.getBytes("UTF-8");
            String string2 = null;
            if (bOSHClientConfig.isCompressionEnabled() && cMSessionParams != null && (attrAccept = cMSessionParams.getAccept()) != null) {
                if (attrAccept.isAccepted(ZLIBCodec.getID())) {
                    string2 = ZLIBCodec.getID();
                    arrby = ZLIBCodec.encode(arrby);
                } else if (attrAccept.isAccepted(GZIPCodec.getID())) {
                    string2 = GZIPCodec.getID();
                    arrby = GZIPCodec.encode(arrby);
                }
            }
            attrAccept = new PostRequest(bOSHClientConfig.getURI().toString(), "text/xml; charset=utf-8", arrby);
            if (string2 != null) {
                attrAccept.setHeader("Content-Encoding", string2);
            }
            attrAccept.setTransferEncoding(null);
            attrAccept.setContentLength(arrby.length);
            if (bOSHClientConfig.isCompressionEnabled()) {
                attrAccept.setHeader("Accept-Encoding", ACCEPT_ENCODING_VAL);
            }
            iFutureResponse = httpClient.send((IHttpRequest)attrAccept);
        }
        catch (IOException var6_6) {
            this.toThrow = new BOSHException("Could not send request", var6_6);
            iFutureResponse = null;
        }
        this.future = iFutureResponse;
    }

    @Override
    public void abort() {
        if (this.future != null) {
            this.future.cancel(true);
        }
    }

    @Override
    public int getHTTPStatus() throws InterruptedException, BOSHException {
        this.awaitResponse();
        return this.httpResp.getStatus();
    }

    @Override
    public AbstractBody getBody() throws InterruptedException, BOSHException {
        this.awaitResponse();
        return this.resp;
    }

    private void awaitResponse() throws InterruptedException, BOSHException {
        this.lock.lock();
        try {
            if (this.toThrow != null) {
                throw this.toThrow;
            }
            if (this.httpResp != null) {
                return;
            }
            this.httpResp = this.future.getResponse();
            byte[] arrby = this.httpResp.getBlockingBody().readBytes();
            String string = this.httpResp.getHeader("Content-Encoding");
            if (ZLIBCodec.getID().equalsIgnoreCase(string)) {
                arrby = ZLIBCodec.decode(arrby);
            } else if (GZIPCodec.getID().equalsIgnoreCase(string)) {
                arrby = GZIPCodec.decode(arrby);
            }
            String string2 = new String(arrby, "UTF-8");
            this.resp = StaticBody.fromString(string2);
        }
        catch (IOException var1_2) {
            this.toThrow = new BOSHException("Could not obtain response", var1_2);
            throw this.toThrow;
        }
        finally {
            this.lock.unlock();
        }
    }
}

