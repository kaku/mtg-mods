/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.BodyQName;
import java.util.HashMap;
import java.util.Map;

final class BodyParserResults {
    private final Map<BodyQName, String> attrs = new HashMap<BodyQName, String>();

    BodyParserResults() {
    }

    void addBodyAttributeValue(BodyQName bodyQName, String string) {
        this.attrs.put(bodyQName, string);
    }

    Map<BodyQName, String> getAttributes() {
        return this.attrs;
    }
}

