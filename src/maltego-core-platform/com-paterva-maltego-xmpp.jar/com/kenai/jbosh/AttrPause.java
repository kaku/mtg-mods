/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.AbstractIntegerAttr;
import com.kenai.jbosh.BOSHException;
import java.util.concurrent.TimeUnit;

final class AttrPause
extends AbstractIntegerAttr {
    private AttrPause(String string) throws BOSHException {
        super(string);
        this.checkMinValue(1);
    }

    static AttrPause createFromString(String string) throws BOSHException {
        if (string == null) {
            return null;
        }
        return new AttrPause(string);
    }

    public int getInMilliseconds() {
        return (int)TimeUnit.MILLISECONDS.convert(this.intValue(), TimeUnit.SECONDS);
    }
}

