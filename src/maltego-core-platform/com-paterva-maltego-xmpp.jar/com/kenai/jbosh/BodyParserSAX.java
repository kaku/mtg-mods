/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.AbstractBody;
import com.kenai.jbosh.BOSHException;
import com.kenai.jbosh.BodyParser;
import com.kenai.jbosh.BodyParserResults;
import com.kenai.jbosh.BodyQName;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

final class BodyParserSAX
implements BodyParser {
    private static final Logger LOG = Logger.getLogger(BodyParserSAX.class.getName());
    private static final SAXParserFactory SAX_FACTORY = SAXParserFactory.newInstance();
    private static final ThreadLocal<SoftReference<SAXParser>> PARSER;

    BodyParserSAX() {
    }

    @Override
    public BodyParserResults parse(String string) throws BOSHException {
        Exception exception2;
        Exception exception2;
        BodyParserResults bodyParserResults = new BodyParserResults();
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(string.getBytes());
            SAXParser sAXParser = BodyParserSAX.getSAXParser();
            sAXParser.parse((InputStream)byteArrayInputStream, (DefaultHandler)new Handler(sAXParser, bodyParserResults));
            return bodyParserResults;
        }
        catch (SAXException var4_4) {
            exception2 = var4_4;
        }
        catch (IOException var4_5) {
            exception2 = var4_5;
        }
        throw new BOSHException("Could not parse body:\n" + string, exception2);
    }

    private static SAXParser getSAXParser() {
        SoftReference<SAXParser> softReference = PARSER.get();
        SAXParser sAXParser = softReference.get();
        if (sAXParser == null) {
            Exception exception2;
            Exception exception2;
            try {
                sAXParser = SAX_FACTORY.newSAXParser();
                softReference = new SoftReference<SAXParser>(sAXParser);
                PARSER.set(softReference);
                return sAXParser;
            }
            catch (ParserConfigurationException var3_2) {
                exception2 = var3_2;
            }
            catch (SAXException var3_3) {
                exception2 = var3_3;
            }
            throw new IllegalStateException("Could not create SAX parser", exception2);
        }
        sAXParser.reset();
        return sAXParser;
    }

    static {
        SAX_FACTORY.setNamespaceAware(true);
        SAX_FACTORY.setValidating(false);
        PARSER = new ThreadLocal<SoftReference<SAXParser>>(){

            @Override
            protected SoftReference<SAXParser> initialValue() {
                return new SoftReference<Object>(null);
            }
        };
    }

    private static final class Handler
    extends DefaultHandler {
        private final BodyParserResults result;
        private final SAXParser parser;
        private String defaultNS = null;

        private Handler(SAXParser sAXParser, BodyParserResults bodyParserResults) {
            this.parser = sAXParser;
            this.result = bodyParserResults;
        }

        @Override
        public void startElement(String string, String string2, String string3, Attributes attributes) {
            BodyQName bodyQName;
            if (LOG.isLoggable(Level.FINEST)) {
                LOG.finest("Start element: " + string3);
                LOG.finest("    URI: " + string);
                LOG.finest("    local: " + string2);
            }
            if (!(bodyQName = AbstractBody.getBodyQName()).getNamespaceURI().equals(string) || !bodyQName.getLocalPart().equals(string2)) {
                throw new IllegalStateException("Root element was not '" + bodyQName.getLocalPart() + "' in the '" + bodyQName.getNamespaceURI() + "' namespace.  (Was '" + string2 + "' in '" + string + "')");
            }
            for (int i = 0; i < attributes.getLength(); ++i) {
                String string4 = attributes.getURI(i);
                if (string4.length() == 0) {
                    string4 = this.defaultNS;
                }
                String string5 = attributes.getLocalName(i);
                String string6 = attributes.getValue(i);
                if (LOG.isLoggable(Level.FINEST)) {
                    LOG.finest("    Attribute: {" + string4 + "}" + string5 + " = '" + string6 + "'");
                }
                BodyQName bodyQName2 = BodyQName.create(string4, string5);
                this.result.addBodyAttributeValue(bodyQName2, string6);
            }
            this.parser.reset();
        }

        @Override
        public void startPrefixMapping(String string, String string2) {
            if (string.length() == 0) {
                if (LOG.isLoggable(Level.FINEST)) {
                    LOG.finest("Prefix mapping: <DEFAULT> => " + string2);
                }
                this.defaultNS = string2;
            } else if (LOG.isLoggable(Level.FINEST)) {
                LOG.info("Prefix mapping: " + string + " => " + string2);
            }
        }
    }

}

