/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import com.kenai.jbosh.AbstractBody;
import com.kenai.jbosh.BOSHException;
import com.kenai.jbosh.BodyParser;
import com.kenai.jbosh.BodyParserResults;
import com.kenai.jbosh.BodyQName;
import com.kenai.jbosh.ServiceLib;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Map;

final class StaticBody
extends AbstractBody {
    private static final BodyParser PARSER = ServiceLib.loadService(BodyParser.class);
    private static final int BUFFER_SIZE = 1024;
    private final Map<BodyQName, String> attrs;
    private final String raw;

    private StaticBody(Map<BodyQName, String> map, String string) {
        this.attrs = map;
        this.raw = string;
    }

    public static StaticBody fromStream(InputStream inputStream) throws BOSHException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            int n;
            byte[] arrby = new byte[1024];
            do {
                if ((n = inputStream.read(arrby)) <= 0) continue;
                byteArrayOutputStream.write(arrby, 0, n);
            } while (n >= 0);
        }
        catch (IOException var2_3) {
            throw new BOSHException("Could not read body data", var2_3);
        }
        return StaticBody.fromString(byteArrayOutputStream.toString());
    }

    public static StaticBody fromString(String string) throws BOSHException {
        BodyParserResults bodyParserResults = PARSER.parse(string);
        return new StaticBody(bodyParserResults.getAttributes(), string);
    }

    @Override
    public Map<BodyQName, String> getAttributes() {
        return Collections.unmodifiableMap(this.attrs);
    }

    @Override
    public String toXML() {
        return this.raw;
    }
}

