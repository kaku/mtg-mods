/*
 * Decompiled with CFR 0_118.
 */
package com.kenai.jbosh;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

final class ServiceLib {
    private static final Logger LOG = Logger.getLogger(ServiceLib.class.getName());

    private ServiceLib() {
    }

    static <T> T loadService(Class<T> class_) {
        List<String> list = ServiceLib.loadServicesImplementations(class_);
        for (String string : list) {
            T t = ServiceLib.attemptLoad(class_, string);
            if (t == null) continue;
            if (LOG.isLoggable(Level.FINEST)) {
                LOG.finest("Selected " + class_.getSimpleName() + " implementation: " + t.getClass().getName());
            }
            return t;
        }
        throw new IllegalStateException("Could not load " + class_.getName() + " implementation");
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static List<String> loadServicesImplementations(Class class_) {
        ArrayList<String> arrayList;
        arrayList = new ArrayList<String>();
        String string = System.getProperty(class_.getName());
        if (string != null) {
            arrayList.add(string);
        }
        ClassLoader classLoader = ServiceLib.class.getClassLoader();
        URL uRL = classLoader.getResource("META-INF/services/" + class_.getName());
        InputStream inputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader bufferedReader = null;
        try {
            String string2;
            inputStream = uRL.openStream();
            inputStreamReader = new InputStreamReader(inputStream);
            bufferedReader = new BufferedReader(inputStreamReader);
            while ((string2 = bufferedReader.readLine()) != null) {
                if (string2.matches("\\s*(#.*)?")) continue;
                arrayList.add(string2.trim());
            }
        }
        catch (IOException var8_9) {
            LOG.log(Level.WARNING, "Could not load services descriptor: " + uRL.toString(), var8_9);
        }
        finally {
            ServiceLib.finalClose(bufferedReader);
            ServiceLib.finalClose(inputStreamReader);
            ServiceLib.finalClose(inputStream);
        }
        return arrayList;
    }

    private static <T> T attemptLoad(Class<T> class_, String string) {
        ReflectiveOperationException reflectiveOperationException2;
        ReflectiveOperationException reflectiveOperationException2;
        Level level;
        if (LOG.isLoggable(Level.FINEST)) {
            LOG.finest("Attempting service load: " + string);
        }
        try {
            Class class_2 = Class.forName(string);
            if (!class_.isAssignableFrom(class_2)) {
                if (LOG.isLoggable(Level.WARNING)) {
                    LOG.warning(class_2.getName() + " is not assignable to " + class_.getName());
                }
                return null;
            }
            return class_.cast(class_2.newInstance());
        }
        catch (ClassNotFoundException var4_3) {
            level = Level.FINEST;
            reflectiveOperationException2 = var4_3;
        }
        catch (InstantiationException var4_4) {
            level = Level.WARNING;
            reflectiveOperationException2 = var4_4;
        }
        catch (IllegalAccessException var4_5) {
            level = Level.WARNING;
            reflectiveOperationException2 = var4_5;
        }
        LOG.log(level, "Could not load " + class_.getSimpleName() + " instance: " + string, reflectiveOperationException2);
        return null;
    }

    private static void finalClose(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            }
            catch (IOException var1_1) {
                LOG.log(Level.FINEST, "Could not close: " + closeable, var1_1);
            }
        }
    }
}

