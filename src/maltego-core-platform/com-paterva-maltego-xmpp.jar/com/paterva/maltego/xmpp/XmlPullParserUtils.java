/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.xmlpull.v1.XmlPullParser
 *  org.xmlpull.v1.XmlPullParserException
 */
package com.paterva.maltego.xmpp;

import com.paterva.maltego.xmpp.XmlUtil;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class XmlPullParserUtils {
    public static String toString(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            XmlPullParserUtils.toXml(xmlPullParser, byteArrayOutputStream);
        }
        finally {
            byteArrayOutputStream.close();
        }
        return byteArrayOutputStream.toString();
    }

    public static void toXml(XmlPullParser xmlPullParser, OutputStream outputStream) throws XmlPullParserException, IOException {
        XmlPullParserUtils.toXml(xmlPullParser, outputStream, true);
    }

    public static void toXml(XmlPullParser xmlPullParser, OutputStream outputStream, boolean bl) throws XmlPullParserException, IOException {
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        XmlPullParserUtils.toXml(xmlPullParser, outputStreamWriter, bl);
        outputStreamWriter.flush();
    }

    public static String toXml(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        String string = XmlPullParserUtils.toXml(xmlPullParser, true);
        return string;
    }

    public static String toXml(XmlPullParser xmlPullParser, boolean bl) throws XmlPullParserException, IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        XmlPullParserUtils.toXml(xmlPullParser, byteArrayOutputStream);
        byteArrayOutputStream.close();
        return byteArrayOutputStream.toString();
    }

    public static void toXml(XmlPullParser xmlPullParser, Writer writer) throws XmlPullParserException, IOException {
        XmlPullParserUtils.toXml(xmlPullParser, writer, true);
    }

    public static void toXml(XmlPullParser xmlPullParser, Writer writer, boolean bl) throws XmlPullParserException, IOException {
        boolean bl2;
        int n = xmlPullParser.getDepth();
        int n2 = -1;
        StringBuilder stringBuilder = new StringBuilder();
        boolean bl3 = xmlPullParser.getEventType() == 4;
        int n3 = bl ? xmlPullParser.getEventType() : xmlPullParser.next();
        boolean bl4 = bl2 = n3 == 3;
        while (n3 != 1 && !bl2) {
            switch (n3) {
                case 2: {
                    XmlPullParserUtils.flushStartTag(stringBuilder, writer);
                    stringBuilder = XmlPullParserUtils.createStartTag(xmlPullParser);
                    break;
                }
                case 3: {
                    boolean bl5 = bl2 = n == xmlPullParser.getDepth();
                    if (bl2 && bl3) break;
                    if (n2 == 2) {
                        XmlPullParserUtils.addEndTag(stringBuilder);
                        stringBuilder = XmlPullParserUtils.flushStartTag(stringBuilder, writer);
                        break;
                    }
                    stringBuilder = XmlPullParserUtils.flushStartTag(stringBuilder, writer);
                    XmlPullParserUtils.write(writer, XmlPullParserUtils.createNestedEndTag(xmlPullParser));
                    break;
                }
                case 9: {
                    stringBuilder = XmlPullParserUtils.flushStartTag(stringBuilder, writer);
                    XmlPullParserUtils.write(writer, String.format("<!--%s-->", xmlPullParser.getText()));
                    break;
                }
                case 4: {
                    stringBuilder = XmlPullParserUtils.flushStartTag(stringBuilder, writer);
                    XmlPullParserUtils.write(writer, XmlUtil.escape(xmlPullParser.getText()));
                }
            }
            n2 = xmlPullParser.getEventType();
            if (bl2) continue;
            n3 = xmlPullParser.next();
        }
    }

    private static void addEndTag(StringBuilder stringBuilder) {
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        stringBuilder.append("/>");
    }

    private static StringBuilder flushStartTag(StringBuilder stringBuilder, Writer writer) throws IOException {
        if (stringBuilder != null) {
            XmlPullParserUtils.write(writer, stringBuilder.toString());
        }
        return null;
    }

    private static void write(Writer writer, String string) throws IOException {
        writer.append(string);
    }

    private static StringBuilder createStartTag(XmlPullParser xmlPullParser) throws XmlPullParserException {
        StringBuilder stringBuilder = new StringBuilder("<");
        stringBuilder.append(XmlPullParserUtils.getName(xmlPullParser));
        XmlPullParserUtils.appendNamespaces(xmlPullParser, stringBuilder);
        XmlPullParserUtils.appendAttributes(xmlPullParser, stringBuilder);
        stringBuilder.append(">");
        return stringBuilder;
    }

    private static String createNestedEndTag(XmlPullParser xmlPullParser) {
        return String.format("</%s>", XmlPullParserUtils.getName(xmlPullParser));
    }

    private static String getName(XmlPullParser xmlPullParser) {
        return XmlPullParserUtils.getName(xmlPullParser.getName(), xmlPullParser.getPrefix());
    }

    private static String getName(String string, String string2) {
        if (string2 == null) {
            return string;
        }
        return String.format("%s:%s", string2, string);
    }

    private static void appendNamespaces(XmlPullParser xmlPullParser, StringBuilder stringBuilder) throws XmlPullParserException {
        int n = xmlPullParser.getNamespaceCount(xmlPullParser.getDepth() - 1);
        int n2 = xmlPullParser.getNamespaceCount(xmlPullParser.getDepth());
        for (int i = n; i < n2; ++i) {
            String string = xmlPullParser.getNamespacePrefix(i);
            String string2 = xmlPullParser.getNamespaceUri(i);
            string = string == null ? "xmlns" : "xmlns:" + string;
            stringBuilder.append(String.format(" %s=\"%s\"", string, string2));
        }
    }

    private static void appendAttributes(XmlPullParser xmlPullParser, StringBuilder stringBuilder) {
        for (int i = 0; i < xmlPullParser.getAttributeCount(); ++i) {
            String string = xmlPullParser.getAttributePrefix(i);
            String string2 = xmlPullParser.getAttributeName(i);
            String string3 = xmlPullParser.getAttributeValue(i);
            stringBuilder.append(String.format(" %s=\"%s\"", XmlPullParserUtils.getName(string2, string), XmlUtil.escape(string3)));
        }
    }
}

