/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.jivesoftware.smack.util.StringUtils
 */
package com.paterva.maltego.xmpp;

import org.jivesoftware.smack.util.StringUtils;

class XmlUtil {
    private XmlUtil() {
    }

    public static String escape(String string) {
        return StringUtils.escapeForXML((String)string);
    }
}

