/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphUserData
 *  com.paterva.maltego.graph.GraphViewManager
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.view.DefaultGraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  yguard.A.A.D
 *  yguard.A.I.SA
 */
package com.paterva.maltego.graph.wrapper;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.view.DefaultGraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.graph.wrapper.GraphStructureWrapper;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import yguard.A.A.D;
import yguard.A.I.SA;

public class MaltegoGraphManager {
    private MaltegoGraphManager() {
    }

    public static GraphWrapper createWrapper(GraphID graphID) {
        DefaultGraphStoreView defaultGraphStoreView = null;
        try {
            defaultGraphStoreView = new DefaultGraphStoreView(graphID);
        }
        catch (GraphStoreException var2_2) {
            throw new IllegalArgumentException("Failed to create default graph store for graphID '" + (Object)graphID + "'", (Throwable)var2_2);
        }
        return MaltegoGraphManager.createWrapper(graphID, (GraphStoreView)defaultGraphStoreView);
    }

    public static GraphWrapper createWrapper(GraphID graphID, GraphStoreView graphStoreView) {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        GraphWrapper graphWrapper = (GraphWrapper)graphUserData.get((Object)(string = GraphWrapper.class.getName()));
        if (graphWrapper != null) {
            throw new IllegalArgumentException("Wrapper already created for graph " + (Object)graphID);
        }
        GraphStoreViewRegistry.getDefault().register("Main", graphID, graphStoreView);
        SA sA = GraphViewManager.getDefault().createViewGraph(graphID);
        graphWrapper = new GraphWrapper(graphID, (D)sA, graphStoreView);
        graphUserData.put((Object)string, (Object)graphWrapper);
        return graphWrapper;
    }

    public static GraphWrapper getWrapper(D d) {
        GraphID graphID = GraphViewManager.getDefault().getGraphID((SA)d);
        return MaltegoGraphManager.getWrapper(graphID);
    }

    public static GraphWrapper getWrapper(GraphID graphID) {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        GraphWrapper graphWrapper = (GraphWrapper)graphUserData.get((Object)(string = GraphWrapper.class.getName()));
        if (graphWrapper == null) {
            throw new IllegalArgumentException("Wrapper does not exist for graph " + (Object)graphID);
        }
        return graphWrapper;
    }

    public static boolean exists(GraphID graphID) {
        GraphWrapper graphWrapper = null;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID, (boolean)false);
        if (graphUserData != null) {
            String string = GraphWrapper.class.getName();
            graphWrapper = (GraphWrapper)graphUserData.get((Object)string);
        }
        return graphWrapper != null;
    }

    public static D get(GraphWrapper graphWrapper) {
        if (graphWrapper instanceof GraphStructureWrapper) {
            GraphWrapper graphWrapper2 = graphWrapper;
            return graphWrapper2.getGraph();
        }
        return null;
    }
}

