/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphViewManager
 *  yguard.A.A.D
 *  yguard.A.I.SA
 */
package com.paterva.maltego.graph.wrapper;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphViewManager;
import yguard.A.A.D;
import yguard.A.I.SA;

public class GraphIDProvider {
    private GraphIDProvider() {
    }

    public static GraphID forGraph(SA sA) {
        return GraphViewManager.getDefault().getGraphID(sA);
    }

    public static GraphID forGraph(D d) {
        return GraphViewManager.getDefault().getGraphID((SA)d);
    }
}

