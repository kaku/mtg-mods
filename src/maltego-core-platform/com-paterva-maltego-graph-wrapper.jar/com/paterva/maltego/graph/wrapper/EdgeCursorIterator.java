/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.LinkID
 *  yguard.A.A.D
 *  yguard.A.A.H
 *  yguard.A.A.Z
 */
package com.paterva.maltego.graph.wrapper;

import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.wrapper.GraphStructureWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import java.util.Iterator;
import yguard.A.A.D;
import yguard.A.A.H;
import yguard.A.A.Z;

public class EdgeCursorIterator
implements Iterator<LinkID>,
Iterable<LinkID> {
    private Z _cursor;
    private GraphStructureWrapper _wrapper;

    public EdgeCursorIterator(D d, Z z) {
        this(MaltegoGraphManager.getWrapper(d), z);
    }

    public EdgeCursorIterator(GraphStructureWrapper graphStructureWrapper, Z z) {
        this._wrapper = graphStructureWrapper;
        this._cursor = z;
    }

    @Override
    public boolean hasNext() {
        return this._cursor.ok();
    }

    @Override
    public LinkID next() {
        LinkID linkID = this._wrapper.linkID(this._cursor.D());
        this._cursor.next();
        return linkID;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public Iterator<LinkID> iterator() {
        return this;
    }

    public int size() {
        return this._cursor.size();
    }
}

