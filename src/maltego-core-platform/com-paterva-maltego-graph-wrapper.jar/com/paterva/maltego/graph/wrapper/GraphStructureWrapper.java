/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.HashMapGuidDataAdapter
 *  yguard.A.A.A
 *  yguard.A.A.D
 *  yguard.A.A.H
 *  yguard.A.A.I
 *  yguard.A.A.J
 *  yguard.A.A.K
 *  yguard.A.A.Y
 *  yguard.A.A.Z
 */
package com.paterva.maltego.graph.wrapper;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.HashMapGuidDataAdapter;
import com.paterva.maltego.graph.wrapper.EdgeCursorIterator;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import java.util.HashSet;
import java.util.Set;
import yguard.A.A.A;
import yguard.A.A.D;
import yguard.A.A.H;
import yguard.A.A.I;
import yguard.A.A.J;
import yguard.A.A.K;
import yguard.A.A.Y;
import yguard.A.A.Z;

public abstract class GraphStructureWrapper {
    private static final String NODE_MAP_PROVIDER_KEY = "maltego.NodeMap";
    private static final String EDGE_MAP_PROVIDER_KEY = "maltego.EdgeMap";
    private static final String ENTITY_MAP_PROVIDER_KEY = "maltego.EntityMap";
    private static final String LINK_MAP_PROVIDER_KEY = "maltego.LinkMap";
    private final D _graph;
    private final I _nodes;
    private final J _edges;
    private final HashMapGuidDataAdapter<EntityID, Y> _entities;
    private final HashMapGuidDataAdapter<LinkID, H> _links;

    protected GraphStructureWrapper(D d) {
        this._graph = d;
        this._nodes = GraphStructureWrapper.getNodeMap(d);
        this._edges = GraphStructureWrapper.getEdgeMap(d);
        this._entities = GraphStructureWrapper.getEntityMap(d);
        this._links = GraphStructureWrapper.getLinkMap(d);
    }

    private static HashMapGuidDataAdapter<EntityID, Y> getEntityMap(D d) {
        HashMapGuidDataAdapter hashMapGuidDataAdapter = (HashMapGuidDataAdapter)d.getDataProvider((Object)"maltego.EntityMap");
        if (hashMapGuidDataAdapter == null) {
            hashMapGuidDataAdapter = new HashMapGuidDataAdapter();
            d.addDataProvider((Object)"maltego.EntityMap", (K)hashMapGuidDataAdapter);
        }
        return hashMapGuidDataAdapter;
    }

    private static HashMapGuidDataAdapter<LinkID, H> getLinkMap(D d) {
        HashMapGuidDataAdapter hashMapGuidDataAdapter = (HashMapGuidDataAdapter)d.getDataProvider((Object)"maltego.LinkMap");
        if (hashMapGuidDataAdapter == null) {
            hashMapGuidDataAdapter = new HashMapGuidDataAdapter();
            d.addDataProvider((Object)"maltego.LinkMap", (K)hashMapGuidDataAdapter);
        }
        return hashMapGuidDataAdapter;
    }

    private static I getNodeMap(D d) {
        I i = (I)d.getDataProvider((Object)"maltego.NodeMap");
        if (i == null) {
            i = d.createNodeMap();
            d.addDataProvider((Object)"maltego.NodeMap", (K)i);
        }
        return i;
    }

    private static J getEdgeMap(D d) {
        J j = (J)d.getDataProvider((Object)"maltego.EdgeMap");
        if (j == null) {
            j = d.createEdgeMap();
            d.addDataProvider((Object)"maltego.EdgeMap", (K)j);
        }
        return j;
    }

    protected D graph() {
        return this._graph;
    }

    protected I nodeMap() {
        return this._nodes;
    }

    protected J edgeMap() {
        return this._edges;
    }

    protected HashMapGuidDataAdapter<EntityID, Y> entityMap() {
        return this._entities;
    }

    protected HashMapGuidDataAdapter<LinkID, H> linkMap() {
        return this._links;
    }

    public EntityID entityID(Y y) {
        return (EntityID)this.nodeMap().get((Object)y);
    }

    public MaltegoEntity entity(Y y) {
        EntityID entityID = this.entityID(y);
        return this.getEntity(entityID);
    }

    public LinkID linkID(H h) {
        return (LinkID)this.edgeMap().get((Object)h);
    }

    public MaltegoLink link(H h) {
        return this.getLink(this.linkID(h));
    }

    public Y node(EntityID entityID) {
        return (Y)this.entityMap().get((Guid)entityID);
    }

    public H edge(LinkID linkID) {
        return (H)this.linkMap().get((Guid)linkID);
    }

    public D getGraph() {
        return this._graph;
    }

    public boolean containsEntity(EntityID entityID) {
        return this._entities.get((Guid)entityID) != null;
    }

    public boolean containsLink(LinkID linkID) {
        return this._links.get((Guid)linkID) != null;
    }

    public Iterable<LinkID> incomingIDs(EntityID entityID) {
        return new EdgeCursorIterator(this, this.node(entityID).M());
    }

    public Iterable<LinkID> outgoingIDs(EntityID entityID) {
        return new EdgeCursorIterator(this, this.node(entityID).G());
    }

    public Iterable<LinkID> linkIDs(EntityID entityID) {
        return new EdgeCursorIterator(this, this.node(entityID).I());
    }

    public Set<LinkID> linkIDs(A a) {
        EdgeCursorIterator edgeCursorIterator = new EdgeCursorIterator(this, a.\u00dd());
        HashSet<LinkID> hashSet = new HashSet<LinkID>(a.size());
        while (edgeCursorIterator.hasNext()) {
            hashSet.add(edgeCursorIterator.next());
        }
        return hashSet;
    }

    public EntityID sourceID(LinkID linkID) {
        Y y;
        H h = this.edge(linkID);
        if (h != null && (y = h.X()) != null) {
            return this.entityID(y);
        }
        return null;
    }

    public EntityID targetID(LinkID linkID) {
        Y y;
        H h = this.edge(linkID);
        if (h != null && (y = h.V()) != null) {
            return this.entityID(y);
        }
        return null;
    }

    public MaltegoEntity getEntity(EntityID entityID) {
        return GraphStoreHelper.getEntity(this._graph, entityID);
    }

    public MaltegoLink getLink(LinkID linkID) {
        return GraphStoreHelper.getLink(this._graph, linkID);
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("Graph ");
        stringBuilder.append(System.identityHashCode(this)).append("\n Entities->yNodes\n");
        for (EntityID entityID : this._entities.keys()) {
            stringBuilder.append("  ").append((Object)entityID);
            stringBuilder.append("->").append((Object)this.node(entityID)).append("\n");
        }
        return stringBuilder.toString();
    }
}

