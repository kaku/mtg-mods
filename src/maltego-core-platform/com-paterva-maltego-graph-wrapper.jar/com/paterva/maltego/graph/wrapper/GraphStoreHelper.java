/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.GraphLink
 *  com.paterva.maltego.core.GraphPart
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphDataStoreWriter
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.query.part.EntitiesDataQuery
 *  com.paterva.maltego.graph.store.query.part.EntityDataQuery
 *  com.paterva.maltego.graph.store.query.part.EntitySectionsQuery
 *  com.paterva.maltego.graph.store.query.part.LinkDataQuery
 *  com.paterva.maltego.graph.store.query.part.LinkSectionsQuery
 *  com.paterva.maltego.graph.store.query.part.LinksDataQuery
 *  com.paterva.maltego.graph.store.query.part.PartDataQuery
 *  com.paterva.maltego.graph.store.query.part.PartSectionsQuery
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.typing.DataSource
 *  org.openide.util.Exceptions
 *  yguard.A.A.D
 *  yguard.A.A.E
 *  yguard.A.A.H
 *  yguard.A.A.Y
 *  yguard.A.A.Z
 *  yguard.A.I.SA
 */
package com.paterva.maltego.graph.wrapper;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.GraphLink;
import com.paterva.maltego.core.GraphPart;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphDataStoreWriter;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.query.part.EntitiesDataQuery;
import com.paterva.maltego.graph.store.query.part.EntityDataQuery;
import com.paterva.maltego.graph.store.query.part.EntitySectionsQuery;
import com.paterva.maltego.graph.store.query.part.LinkDataQuery;
import com.paterva.maltego.graph.store.query.part.LinkSectionsQuery;
import com.paterva.maltego.graph.store.query.part.LinksDataQuery;
import com.paterva.maltego.graph.store.query.part.PartDataQuery;
import com.paterva.maltego.graph.store.query.part.PartSectionsQuery;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.typing.DataSource;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.openide.util.Exceptions;
import yguard.A.A.D;
import yguard.A.A.E;
import yguard.A.A.H;
import yguard.A.A.Y;
import yguard.A.A.Z;
import yguard.A.I.SA;

public class GraphStoreHelper {
    private GraphStoreHelper() {
    }

    public static GraphPart getPart(DataSource dataSource) {
        return null;
    }

    public static Set<EntityID> getEntityIDs(GraphID graphID) {
        return GraphStoreHelper.getEntities(graphID).keySet();
    }

    public static Set<EntityID> getEntityIDs(D d) {
        return GraphStoreHelper.getEntities(d).keySet();
    }

    public static Set<LinkID> getLinkIDs(GraphID graphID) {
        return GraphStoreHelper.getLinks(graphID).keySet();
    }

    public static Set<LinkID> getLinkIDs(D d) {
        return GraphStoreHelper.getLinks(d).keySet();
    }

    public static MaltegoEntity getEntity(GraphEntity graphEntity) {
        return GraphStoreHelper.getEntity(GraphStoreHelper.getDataStoreReader(graphEntity.getGraphID()), (EntityID)graphEntity.getID());
    }

    public static MaltegoEntity getEntity(GraphID graphID, EntityID entityID) {
        return GraphStoreHelper.getEntity(GraphStoreHelper.getDataStoreReader(graphID), entityID);
    }

    public static MaltegoEntity getEntity(D d, EntityID entityID) {
        return GraphStoreHelper.getEntity(GraphStoreHelper.getDataStoreReader(d), entityID);
    }

    public static MaltegoEntity getEntity(GraphDataStoreReader graphDataStoreReader, EntityID entityID) {
        try {
            return graphDataStoreReader.getEntity(entityID);
        }
        catch (GraphStoreException var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
            return null;
        }
    }

    public static Map<EntityID, MaltegoEntity> getEntities(GraphID graphID) {
        EntitiesDataQuery entitiesDataQuery = new EntitiesDataQuery();
        entitiesDataQuery.setAllIDs(true);
        return GraphStoreHelper.getEntities(GraphStoreHelper.getDataStoreReader(graphID), entitiesDataQuery);
    }

    public static Map<EntityID, MaltegoEntity> getEntities(D d) {
        EntitiesDataQuery entitiesDataQuery = new EntitiesDataQuery();
        entitiesDataQuery.setAllIDs(true);
        return GraphStoreHelper.getEntities(GraphStoreHelper.getDataStoreReader(d), entitiesDataQuery);
    }

    public static Map<EntityID, MaltegoEntity> getEntities(GraphID graphID, Collection<EntityID> collection) {
        if (collection.size() == 1) {
            EntityID entityID = collection.iterator().next();
            MaltegoEntity maltegoEntity = GraphStoreHelper.getEntity(graphID, entityID);
            return Collections.singletonMap(entityID, maltegoEntity);
        }
        EntitiesDataQuery entitiesDataQuery = new EntitiesDataQuery();
        entitiesDataQuery.setIDs(GraphStoreHelper.getIdSet(collection));
        return GraphStoreHelper.getEntities(GraphStoreHelper.getDataStoreReader(graphID), entitiesDataQuery);
    }

    public static Map<EntityID, MaltegoEntity> getEntities(D d, Collection<EntityID> collection) {
        EntitiesDataQuery entitiesDataQuery = new EntitiesDataQuery();
        entitiesDataQuery.setIDs(GraphStoreHelper.getIdSet(collection));
        return GraphStoreHelper.getEntities(GraphStoreHelper.getDataStoreReader(d), entitiesDataQuery);
    }

    public static Map<EntityID, MaltegoEntity> getEntities(GraphDataStoreReader graphDataStoreReader, EntitiesDataQuery entitiesDataQuery) {
        try {
            return graphDataStoreReader.getEntities(entitiesDataQuery);
        }
        catch (GraphStoreException var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
            return Collections.EMPTY_MAP;
        }
    }

    public static Set<MaltegoEntity> getMaltegoEntityClones(Collection<GraphEntity> collection) {
        HashSet<MaltegoEntity> hashSet = new HashSet<MaltegoEntity>();
        for (GraphEntity graphEntity : collection) {
            hashSet.add(GraphStoreHelper.getEntity(graphEntity).createClone());
        }
        return hashSet;
    }

    public static Set<MaltegoEntity> getMaltegoEntities(GraphID graphID) {
        return new HashSet<MaltegoEntity>(GraphStoreHelper.getEntities(graphID).values());
    }

    public static Set<MaltegoEntity> getMaltegoEntities(D d) {
        return new HashSet<MaltegoEntity>(GraphStoreHelper.getEntities(d).values());
    }

    public static Set<MaltegoEntity> getMaltegoEntities(GraphID graphID, Collection<EntityID> collection) {
        return new HashSet<MaltegoEntity>(GraphStoreHelper.getEntities(graphID, collection).values());
    }

    public static Set<MaltegoEntity> getMaltegoEntities(D d, Collection<EntityID> collection) {
        return new HashSet<MaltegoEntity>(GraphStoreHelper.getEntities(d, collection).values());
    }

    public static MaltegoLink getLink(GraphLink graphLink) {
        return GraphStoreHelper.getLink(GraphStoreHelper.getDataStoreReader(graphLink.getGraphID()), (LinkID)graphLink.getID());
    }

    public static MaltegoLink getLink(GraphID graphID, LinkID linkID) {
        return GraphStoreHelper.getLink(GraphStoreHelper.getDataStoreReader(graphID), linkID);
    }

    public static MaltegoLink getLink(D d, LinkID linkID) {
        return GraphStoreHelper.getLink(GraphStoreHelper.getDataStoreReader(d), linkID);
    }

    public static MaltegoLink getLink(GraphDataStoreReader graphDataStoreReader, LinkID linkID) {
        try {
            return graphDataStoreReader.getLink(linkID);
        }
        catch (GraphStoreException var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
            return null;
        }
    }

    public static Map<LinkID, MaltegoLink> getLinks(GraphID graphID) {
        LinksDataQuery linksDataQuery = new LinksDataQuery();
        linksDataQuery.setAllIDs(true);
        return GraphStoreHelper.getLinks(GraphStoreHelper.getDataStoreReader(graphID), linksDataQuery);
    }

    public static Map<LinkID, MaltegoLink> getLinks(D d) {
        LinksDataQuery linksDataQuery = new LinksDataQuery();
        linksDataQuery.setAllIDs(true);
        return GraphStoreHelper.getLinks(GraphStoreHelper.getDataStoreReader(d), linksDataQuery);
    }

    public static Map<LinkID, MaltegoLink> getLinks(GraphID graphID, Collection<LinkID> collection) {
        LinksDataQuery linksDataQuery = new LinksDataQuery();
        linksDataQuery.setIDs(GraphStoreHelper.getIdSet(collection));
        return GraphStoreHelper.getLinks(GraphStoreHelper.getDataStoreReader(graphID), linksDataQuery);
    }

    public static Map<LinkID, MaltegoLink> getLinks(D d, Collection<LinkID> collection) {
        LinksDataQuery linksDataQuery = new LinksDataQuery();
        linksDataQuery.setIDs(GraphStoreHelper.getIdSet(collection));
        return GraphStoreHelper.getLinks(GraphStoreHelper.getDataStoreReader(d), linksDataQuery);
    }

    public static Map<LinkID, MaltegoLink> getLinks(GraphDataStoreReader graphDataStoreReader, LinksDataQuery linksDataQuery) {
        try {
            return graphDataStoreReader.getLinks(linksDataQuery);
        }
        catch (GraphStoreException var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
            return Collections.EMPTY_MAP;
        }
    }

    public static Set<MaltegoLink> getMaltegoLinks(GraphID graphID) {
        return new HashSet<MaltegoLink>(GraphStoreHelper.getLinks(graphID).values());
    }

    public static Set<MaltegoLink> getMaltegoLinks(D d) {
        return new HashSet<MaltegoLink>(GraphStoreHelper.getLinks(d).values());
    }

    public static Set<MaltegoLink> getMaltegoLinks(GraphID graphID, Collection<LinkID> collection) {
        return new HashSet<MaltegoLink>(GraphStoreHelper.getLinks(graphID, collection).values());
    }

    public static Set<MaltegoLink> getMaltegoLinks(D d, Collection<LinkID> collection) {
        return new HashSet<MaltegoLink>(GraphStoreHelper.getLinks(d, collection).values());
    }

    public static MaltegoPart getPart(GraphPart graphPart) {
        if (graphPart instanceof GraphEntity) {
            return GraphStoreHelper.getEntity((GraphEntity)graphPart);
        }
        return GraphStoreHelper.getLink((GraphLink)graphPart);
    }

    public static GraphDataStore getDataStore(GraphPart graphPart) {
        return GraphStoreHelper.getGraphStore(graphPart.getGraphID()).getGraphDataStore();
    }

    public static GraphDataStoreReader getDataStoreReader(D d) {
        return GraphStoreHelper.getGraphStore(GraphIDProvider.forGraph(d)).getGraphDataStore().getDataStoreReader();
    }

    public static GraphDataStoreReader getDataStoreReader(GraphPart graphPart) {
        return GraphStoreHelper.getDataStoreReader(graphPart.getGraphID());
    }

    public static GraphDataStoreReader getDataStoreReader(GraphID graphID) {
        return GraphStoreHelper.getGraphStore(graphID).getGraphDataStore().getDataStoreReader();
    }

    public static GraphDataStoreWriter getDataStoreWriter(GraphPart graphPart) {
        return GraphStoreHelper.getGraphStore(graphPart.getGraphID()).getGraphDataStore().getDataStoreWriter();
    }

    public static GraphStore getGraphStore(GraphID graphID) {
        try {
            return GraphStoreRegistry.getDefault().forGraphID(graphID);
        }
        catch (GraphStoreException var1_1) {
            Exceptions.printStackTrace((Throwable)var1_1);
            return null;
        }
    }

    public static <ID extends Guid> Set<ID> getIdSet(Collection<ID> collection) {
        return collection instanceof Set ? (Set)collection : new HashSet<ID>(collection);
    }

    public static <ID extends Guid> Set<ID> getIds(Collection<? extends MaltegoPart<ID>> collection) {
        HashSet<Guid> hashSet = new HashSet<Guid>(collection.size());
        for (MaltegoPart<ID> maltegoPart : collection) {
            hashSet.add(maltegoPart.getID());
        }
        return hashSet;
    }

    public static <ID extends Guid> Set<ID> getIdsFor(Collection<? extends GraphPart<ID>> collection) {
        HashSet<Guid> hashSet = new HashSet<Guid>(collection.size());
        for (GraphPart<ID> graphPart : collection) {
            hashSet.add(graphPart.getID());
        }
        return hashSet;
    }

    public static Set<GraphEntity> createTempGraphFor(Collection<MaltegoEntity> collection) {
        HashSet<GraphEntity> hashSet = new HashSet<GraphEntity>();
        SA sA = new SA();
        GraphID graphID = GraphIDProvider.forGraph(sA);
        GraphStore graphStore = GraphStoreHelper.getGraphStore(graphID);
        try {
            graphStore.getGraphDataStore().getDataStoreWriter().putEntities(collection);
            for (MaltegoEntity maltegoEntity : collection) {
                hashSet.add(new GraphEntity(graphID, (EntityID)maltegoEntity.getID()));
            }
        }
        catch (GraphStoreException var5_6) {
            Exceptions.printStackTrace((Throwable)var5_6);
        }
        return hashSet;
    }

    public static <T> Map<MaltegoLink, T> mapLinkIDsToLinks(D d, Map<LinkID, T> map) {
        HashMap<MaltegoLink, T> hashMap = new HashMap<MaltegoLink, T>(map.size());
        for (Map.Entry<LinkID, T> entry : map.entrySet()) {
            LinkID linkID = entry.getKey();
            MaltegoLink maltegoLink = GraphStoreHelper.getLink(d, linkID);
            hashMap.put(maltegoLink, entry.getValue());
        }
        return hashMap;
    }

    public static Map<MaltegoLink, LinkEntityIDs> getConnectionLinks(GraphID graphID, Set<EntityID> set, Set<LinkID> set2) {
        Map<LinkID, LinkEntityIDs> map = GraphStoreHelper.getConnectionsForIDs(graphID, set, set2);
        return GraphStoreHelper.getConnectionLinks(graphID, map);
    }

    public static Map<MaltegoLink, LinkEntityIDs> getConnectionLinks(GraphID graphID, Map<LinkID, LinkEntityIDs> map) {
        HashMap<MaltegoLink, LinkEntityIDs> hashMap = new HashMap<MaltegoLink, LinkEntityIDs>(map.size());
        GraphDataStoreReader graphDataStoreReader = GraphStoreHelper.getDataStoreReader(graphID);
        try {
            LinksDataQuery linksDataQuery = new LinksDataQuery();
            linksDataQuery.setIDs(map.keySet());
            LinkDataQuery linkDataQuery = new LinkDataQuery();
            linkDataQuery.setAllProperties(true);
            linkDataQuery.setAllSections(true);
            linksDataQuery.setPartDataQuery((PartDataQuery)linkDataQuery);
            Map map2 = graphDataStoreReader.getLinks(linksDataQuery);
            for (Map.Entry entry : map2.entrySet()) {
                LinkID linkID = (LinkID)entry.getKey();
                MaltegoLink maltegoLink = (MaltegoLink)entry.getValue();
                hashMap.put(maltegoLink, map.get((Object)linkID));
            }
        }
        catch (GraphStoreException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        return hashMap;
    }

    public static Map<MaltegoLink, LinkEntityIDs> getModelConnections(GraphID graphID, Collection<MaltegoLink> collection) {
        HashMap<MaltegoLink, LinkEntityIDs> hashMap = new HashMap<MaltegoLink, LinkEntityIDs>();
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            Map map = graphStructureReader.getEntities(GraphStoreHelper.getIds(collection));
            for (MaltegoLink maltegoLink : collection) {
                hashMap.put(maltegoLink, (LinkEntityIDs)map.get((Object)maltegoLink.getID()));
            }
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        return hashMap;
    }

    public static Map<LinkID, LinkEntityIDs> getConnectionsForIDs(GraphID graphID, Set<EntityID> set, Set<LinkID> set2) {
        Map map = null;
        try {
            HashSet<LinkID> hashSet = new HashSet<LinkID>(set2);
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            hashSet.addAll(graphStructureReader.getLinks(set));
            map = graphStructureReader.getEntities(hashSet);
        }
        catch (GraphStoreException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        return map;
    }

    public static Set<MaltegoEntity> cloneEntities(D d, Set<EntityID> set) {
        HashSet<MaltegoEntity> hashSet = new HashSet<MaltegoEntity>();
        for (EntityID entityID : set) {
            hashSet.add(GraphStoreHelper.getEntity(d, entityID).createClone());
        }
        return hashSet;
    }

    public static Set<MaltegoLink> cloneLinks(D d, Set<LinkID> set) {
        HashSet<MaltegoLink> hashSet = new HashSet<MaltegoLink>();
        for (LinkID linkID : set) {
            hashSet.add(GraphStoreHelper.getLink(d, linkID).createClone());
        }
        return hashSet;
    }

    public static Set<EntityID> getEntityIDs(D d, E e) {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper(d);
        e.toFirst();
        while (e.ok()) {
            hashSet.add(graphWrapper.entityID(e.B()));
            e.next();
        }
        return hashSet;
    }

    public static Set<LinkID> getLinkIDs(D d, Z z) {
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper(d);
        z.toFirst();
        while (z.ok()) {
            hashSet.add(graphWrapper.linkID(z.D()));
            z.next();
        }
        return hashSet;
    }

    public static Map<EntityID, Boolean> isPinned(GraphID graphID, Set<EntityID> set) {
        Map map = null;
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            map = graphStructureReader.getPinned(set);
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        return map;
    }

    public static Collection<MaltegoEntity> getMaltegoEntitiesWithAttachments(GraphID graphID) {
        Collection collection = Collections.EMPTY_SET;
        try {
            GraphDataStoreReader graphDataStoreReader = GraphStoreHelper.getDataStoreReader(graphID);
            EntitiesDataQuery entitiesDataQuery = new EntitiesDataQuery();
            EntityDataQuery entityDataQuery = new EntityDataQuery();
            entityDataQuery.setPropertyNames(Collections.singleton("atts"));
            entityDataQuery.setMatchProperties(Collections.singletonMap("hasAttachments", true));
            entityDataQuery.setSections((PartSectionsQuery)new EntitySectionsQuery());
            entitiesDataQuery.setPartDataQuery((PartDataQuery)entityDataQuery);
            collection = graphDataStoreReader.getEntities(entitiesDataQuery).values();
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return collection;
    }

    public static Collection<MaltegoLink> getMaltegoLinksWithAttachments(GraphID graphID) {
        Collection collection = Collections.EMPTY_SET;
        try {
            GraphDataStoreReader graphDataStoreReader = GraphStoreHelper.getDataStoreReader(graphID);
            LinksDataQuery linksDataQuery = new LinksDataQuery();
            LinkDataQuery linkDataQuery = new LinkDataQuery();
            linkDataQuery.setPropertyNames(Collections.singleton("atts"));
            linkDataQuery.setMatchProperties(Collections.singletonMap("hasAttachments", true));
            linkDataQuery.setSections((PartSectionsQuery)new LinkSectionsQuery());
            linksDataQuery.setPartDataQuery((PartDataQuery)linkDataQuery);
            collection = graphDataStoreReader.getLinks(linksDataQuery).values();
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return collection;
    }
}

