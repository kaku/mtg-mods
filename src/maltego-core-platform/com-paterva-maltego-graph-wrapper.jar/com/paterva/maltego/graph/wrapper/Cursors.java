/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  yguard.A.A.D
 *  yguard.A.A.H
 *  yguard.A.A.Y
 *  yguard.A.A.Z
 */
package com.paterva.maltego.graph.wrapper;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import java.util.Iterator;
import yguard.A.A.D;
import yguard.A.A.H;
import yguard.A.A.Y;
import yguard.A.A.Z;

public final class Cursors {
    private Cursors() {
    }

    public static Iterable<Y> sourceNodes(Z z) {
        return new SourceIterator(z);
    }

    public static Iterable<Y> targetNodes(Z z) {
        return new TargetIterator(z);
    }

    public static Iterable<Y> parents(Y y) {
        return Cursors.sourceNodes(y.M());
    }

    public static Iterable<Y> children(Y y) {
        return Cursors.targetNodes(y.G());
    }

    public static Iterable<EntityID> parentEntities(Y y) {
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper(y.H());
        return new EntityIterator(graphWrapper, new SourceIterator(y.M()));
    }

    public static Iterable<EntityID> childEntities(Y y) {
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper(y.H());
        return new EntityIterator(graphWrapper, new TargetIterator(y.G()));
    }

    private static class EntityIterator
    implements Iterator<EntityID>,
    Iterable<EntityID> {
        private Iterator<Y> _nodes;
        private GraphWrapper _wrapper;

        public EntityIterator(GraphWrapper graphWrapper, Iterator<Y> iterator) {
            this._nodes = iterator;
            this._wrapper = graphWrapper;
        }

        @Override
        public boolean hasNext() {
            return this._nodes.hasNext();
        }

        @Override
        public EntityID next() {
            Y y = this._nodes.next();
            return this._wrapper.entityID(y);
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public Iterator<EntityID> iterator() {
            return this;
        }
    }

    private static abstract class EdgeCursorIterator
    implements Iterator<Y>,
    Iterable<Y> {
        private Z _ec;

        public EdgeCursorIterator(Z z) {
            this._ec = z;
        }

        @Override
        public boolean hasNext() {
            return this._ec.ok();
        }

        @Override
        public Y next() {
            Y y = this.node(this._ec.D());
            this._ec.next();
            return y;
        }

        protected abstract Y node(H var1);

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public Iterator<Y> iterator() {
            return this;
        }
    }

    private static class SourceIterator
    extends EdgeCursorIterator {
        public SourceIterator(Z z) {
            super(z);
        }

        @Override
        protected Y node(H h) {
            return h.X();
        }
    }

    private static class TargetIterator
    extends EdgeCursorIterator {
        public TargetIterator(Z z) {
            super(z);
        }

        @Override
        protected Y node(H h) {
            return h.V();
        }
    }

}

