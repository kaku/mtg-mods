/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.GraphLink
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.HashMapGuidDataAdapter
 *  com.paterva.maltego.graph.UserDataHolder
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.selection.SelectionState
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutMods
 *  com.paterva.maltego.graph.store.layout.GraphLayoutReader
 *  com.paterva.maltego.graph.store.layout.GraphLayoutStore
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  org.openide.util.Exceptions
 *  yguard.A.A.D
 *  yguard.A.A.H
 *  yguard.A.A.I
 *  yguard.A.A.J
 *  yguard.A.A.K
 *  yguard.A.A.Y
 *  yguard.A.I.BA
 *  yguard.A.I.HA
 *  yguard.A.I.SA
 *  yguard.A.I.q
 *  yguard.A.J.M
 *  yguard.A.J.R
 */
package com.paterva.maltego.graph.wrapper;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.GraphLink;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.HashMapGuidDataAdapter;
import com.paterva.maltego.graph.UserDataHolder;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.selection.SelectionState;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutMods;
import com.paterva.maltego.graph.store.layout.GraphLayoutReader;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.wrapper.GraphStructureWrapper;
import java.awt.Point;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;
import yguard.A.A.D;
import yguard.A.A.H;
import yguard.A.A.I;
import yguard.A.A.J;
import yguard.A.A.K;
import yguard.A.A.Y;
import yguard.A.I.BA;
import yguard.A.I.HA;
import yguard.A.I.SA;
import yguard.A.I.q;
import yguard.A.J.M;
import yguard.A.J.R;

public class GraphWrapper
extends GraphStructureWrapper {
    private static final Logger LOG = Logger.getLogger(GraphWrapper.class.getName());
    private final GraphID _graphID;
    private final List<EventListener> _propertyChangeListeners = Collections.synchronizedList(new LinkedList());
    private final GraphStoreView _graphStoreView;
    private GraphLayoutListener _layoutListener;
    private GraphStructureListener _structureListener;
    private boolean _isSyncing = false;

    protected GraphWrapper(GraphID graphID, D d, GraphStoreView graphStoreView) {
        super(d);
        this._graphID = graphID;
        this._graphStoreView = graphStoreView;
        this._structureListener = new GraphStructureListener();
        this._layoutListener = new GraphLayoutListener();
        this._graphStoreView.getGraphStructureStore().addPropertyChangeListener((PropertyChangeListener)this._structureListener);
        this._graphStoreView.getGraphLayoutStore().addPropertyChangeListener((PropertyChangeListener)this._layoutListener);
        GraphLifeCycleManager.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("graphClosing".equals(propertyChangeEvent.getPropertyName()) && propertyChangeEvent.getNewValue().equals((Object)GraphWrapper.this._graphID)) {
                    GraphLifeCycleManager.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
                    GraphWrapper.this._graphStoreView.getGraphStructureStore().removePropertyChangeListener((PropertyChangeListener)GraphWrapper.this._structureListener);
                    GraphWrapper.this._graphStoreView.getGraphLayoutStore().removePropertyChangeListener((PropertyChangeListener)GraphWrapper.this._layoutListener);
                    GraphWrapper.this._structureListener = null;
                    GraphWrapper.this._layoutListener = null;
                }
            }
        });
    }

    public void beginUpdate() {
        this.graph().firePreEvent();
        this.getGraphStoreView().beginUpdate();
    }

    public void endUpdate() {
        this.getGraphStoreView().endUpdate(null);
        this.graph().firePostEvent();
    }

    private HA getRealizer(Y y) {
        SA sA;
        BA bA;
        D d = this.getGraph();
        if (d instanceof SA && (bA = (sA = (SA)d).getRealizer(y)) instanceof HA) {
            return (HA)bA;
        }
        return null;
    }

    private q getRealizer(H h) {
        D d = this.getGraph();
        if (d instanceof SA) {
            SA sA = (SA)d;
            return sA.getRealizer(h);
        }
        return null;
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._propertyChangeListeners.add(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._propertyChangeListeners.remove(propertyChangeListener);
    }

    public boolean isCollectionNode(EntityID entityID) {
        try {
            return this.getGraphStoreView().getModelViewMappings().isOnlyViewEntity(entityID);
        }
        catch (GraphStoreException var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
            return false;
        }
    }

    public boolean isCollectionNodeLink(LinkID linkID) {
        try {
            return this.getGraphStoreView().getModelViewMappings().isOnlyViewLink(linkID);
        }
        catch (GraphStoreException var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
            return false;
        }
    }

    protected void firePropertyChange(String string, Object object, Object object2) {
        PropertyChangeListener[] arrpropertyChangeListener = this._propertyChangeListeners.toArray(new PropertyChangeListener[0]);
        for (int i = 0; i < arrpropertyChangeListener.length; ++i) {
            arrpropertyChangeListener[i].propertyChange(new PropertyChangeEvent(this, string, object, object2));
        }
    }

    private void yFilesMemLeakWorkaround(D d, Y y) {
        I i;
        Object object;
        K k = d.getDataProvider((Object)"y.view.ModelViewManager.VIEW_2_MODEL_NODES");
        if (k instanceof I && (object = (i = (I)k).get((Object)y)) != null) {
            i.set((Object)y, (Object)null);
            K k2 = d.getDataProvider((Object)"y.view.ModelViewManager.MODEL_2_VIEW_NODES");
            if (k2 instanceof I) {
                i = (I)k2;
                i.set(object, (Object)null);
            }
        }
    }

    private void yFilesMemLeakWorkaround(D d, H h) {
        J j;
        Object object;
        K k = d.getDataProvider((Object)"y.view.ModelViewManager.VIEW_2_MODEL_EDGES");
        if (k instanceof J && (object = (j = (J)k).get((Object)h)) != null) {
            j.set((Object)h, (Object)null);
            K k2 = d.getDataProvider((Object)"y.view.ModelViewManager.MODEL_2_VIEW_EDGES");
            if (k2 instanceof J) {
                j = (J)k2;
                j.set(object, (Object)null);
            }
        }
    }

    private void cleanupRemovedNode(Y y) {
        SA sA;
        BA bA;
        D d = this.getGraph();
        if (d instanceof SA && (bA = (sA = (SA)d).getRealizer(y)) instanceof HA) {
            ((HA)bA).setUserData((Object)null);
        }
        this.yFilesMemLeakWorkaround(d, y);
    }

    private void cleanupEdgeRemoved(H h) {
        q q2;
        SA sA;
        D d = this.getGraph();
        if (d instanceof SA && (q2 = (sA = (SA)d).getRealizer(h)) instanceof UserDataHolder) {
            ((UserDataHolder)q2).setUserData((Object)null);
        }
        this.yFilesMemLeakWorkaround(d, h);
    }

    public GraphDataStore getGraphDataStore() {
        return this.getGraphStoreView().getGraphDataStore();
    }

    public GraphStructureStore getGraphStructureStore() {
        return this.getGraphStoreView().getGraphStructureStore();
    }

    public GraphLayoutStore getGraphLayoutStore() {
        return this.getGraphStoreView().getGraphLayoutStore();
    }

    public GraphStoreView getGraphStoreView() {
        return this._graphStoreView;
    }

    private GraphID getGraphID() {
        return this._graphID;
    }

    private R listToYPointPath(List<Point> list) {
        ArrayList<M> arrayList = new ArrayList<M>(list.size());
        for (Point point : list) {
            arrayList.add(new M((double)point.x, (double)point.y));
        }
        return new R(arrayList);
    }

    public boolean isSyncing() {
        return this._isSyncing;
    }

    public void setIsSyncing(boolean bl) {
        this._isSyncing = bl;
    }

    private class GraphLayoutListener
    implements PropertyChangeListener {
        private GraphLayoutListener() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            SA sA = (SA)GraphWrapper.this.graph();
            sA.firePreEvent();
            try {
                Object object;
                EntityID entityID;
                Y y;
                GraphLayoutMods graphLayoutMods = (GraphLayoutMods)propertyChangeEvent.getNewValue();
                for (Map.Entry entry2 : graphLayoutMods.getCenters().entrySet()) {
                    entityID = (EntityID)entry2.getKey();
                    object = (Point)entry2.getValue();
                    if (object == null) continue;
                    y = GraphWrapper.this.node(entityID);
                    if (y != null) {
                        sA.setCenter(y, (double)object.x, (double)object.y);
                        continue;
                    }
                    LOG.log(Level.WARNING, "Node not found for entity {0}", (Object)entityID);
                }
                for (Map.Entry entry2 : graphLayoutMods.getPaths().entrySet()) {
                    entityID = (LinkID)entry2.getKey();
                    object = (List)entry2.getValue();
                    if (object == null) continue;
                    y = GraphWrapper.this.edge((LinkID)entityID);
                    if (y != null) {
                        R r = GraphWrapper.this.listToYPointPath((List)object);
                        sA.setPath((H)y, r);
                        sA.setSourcePointRel((H)y, r.L());
                        sA.setTargetPointRel((H)y, r.I());
                        continue;
                    }
                    LOG.log(Level.WARNING, "Edge not found for link {0}", (Object)entityID);
                }
            }
            finally {
                sA.firePostEvent();
                sA.updateViews();
            }
        }
    }

    private class GraphStructureListener
    implements PropertyChangeListener {
        private GraphStructureListener() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            GraphID graphID;
            GraphWrapper.this.graph().firePreEvent();
            boolean bl = false;
            try {
                graphID = GraphWrapper.this.getGraphID();
                GraphStructureMods graphStructureMods = (GraphStructureMods)propertyChangeEvent.getNewValue();
                for (LinkID linkID2 : graphStructureMods.getLinksRemoved().keySet()) {
                    this.onLinkRemoved(linkID2);
                    bl = true;
                }
                for (LinkID linkID2 : graphStructureMods.getEntitiesRemoved()) {
                    this.onEntityRemoved((EntityID)linkID2);
                    bl = true;
                }
                if (!graphStructureMods.getEntitiesAdded().isEmpty()) {
                    this.onEntitiesAdded(graphStructureMods.getEntitiesAdded(), graphID);
                    bl = true;
                }
                if (!graphStructureMods.getLinksAdded().isEmpty()) {
                    this.onLinksAdded(graphStructureMods.getLinksAdded(), graphID);
                    bl = true;
                }
            }
            finally {
                GraphWrapper.this.graph().firePostEvent();
                if (bl) {
                    graphID = (SA)GraphWrapper.this.graph();
                    graphID.updateViews();
                }
            }
        }

        private void onLinksAdded(Set<LinkID> set, GraphID graphID) {
            try {
                GraphStructureReader graphStructureReader = GraphWrapper.this.getGraphStructureStore().getStructureReader();
                Map map = graphStructureReader.getEntities(set);
                GraphStoreView graphStoreView = GraphWrapper.this.getGraphStoreView();
                GraphLayoutReader graphLayoutReader = graphStoreView.getGraphLayoutStore().getLayoutReader();
                Map map2 = graphLayoutReader.getPaths(set);
                for (LinkID linkID : set) {
                    List list;
                    GraphSelection graphSelection;
                    LinkEntityIDs linkEntityIDs = (LinkEntityIDs)map.get((Object)linkID);
                    SA sA = (SA)GraphWrapper.this.graph();
                    H h = sA.createEdge(GraphWrapper.this.node(linkEntityIDs.getSourceID()), GraphWrapper.this.node(linkEntityIDs.getTargetID()));
                    GraphWrapper.this.edgeMap().set((Object)h, (Object)linkID);
                    GraphWrapper.this.linkMap().put((Guid)linkID, (Object)h);
                    q q2 = GraphWrapper.this.getRealizer(h);
                    if (q2 instanceof UserDataHolder) {
                        ((UserDataHolder)q2).setUserData((Object)new GraphLink(graphID, linkID));
                    }
                    if ((graphSelection = GraphSelection.forGraph((GraphID)graphID)).isSelectedInView(linkID) && !GraphWrapper.this.isSyncing()) {
                        sA.setSelected(h, true);
                    }
                    if ((list = (List)map2.get((Object)linkID)) == null) continue;
                    R r = GraphWrapper.this.listToYPointPath(list);
                    sA.setPath(h, r);
                    sA.setSourcePointRel(h, r.L());
                    sA.setTargetPointRel(h, r.I());
                }
            }
            catch (GraphStoreException var3_4) {
                Exceptions.printStackTrace((Throwable)var3_4);
            }
        }

        private void onEntitiesAdded(Set<EntityID> set, GraphID graphID) {
            SA sA = (SA)GraphWrapper.this.graph();
            GraphStoreView graphStoreView = GraphWrapper.this.getGraphStoreView();
            GraphLayoutReader graphLayoutReader = graphStoreView.getGraphLayoutStore().getLayoutReader();
            Map map = Collections.EMPTY_MAP;
            try {
                map = graphLayoutReader.getCenters(set);
            }
            catch (GraphStoreException var7_7) {
                Exceptions.printStackTrace((Throwable)var7_7);
            }
            GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
            for (EntityID entityID : set) {
                Point point = (Point)map.get((Object)entityID);
                Y y = point == null ? sA.createNode() : sA.createNode((double)point.x, (double)point.y);
                GraphWrapper.this.nodeMap().set((Object)y, (Object)entityID);
                GraphWrapper.this.entityMap().put((Guid)entityID, (Object)y);
                HA hA = GraphWrapper.this.getRealizer(y);
                if (hA instanceof HA) {
                    hA.setUserData((Object)new GraphEntity(graphID, entityID));
                }
                if (graphSelection.getViewSelectionState(entityID) == SelectionState.NO || GraphWrapper.this.isSyncing()) continue;
                sA.setSelected(y, true);
            }
        }

        private void onEntityRemoved(EntityID entityID) {
            Y y = GraphWrapper.this.node(entityID);
            if (y != null) {
                GraphWrapper.this.graph().removeNode(y);
                GraphWrapper.this.nodeMap().set((Object)y, (Object)null);
                GraphWrapper.this.cleanupRemovedNode(y);
            }
            GraphWrapper.this.entityMap().remove((Guid)entityID);
        }

        private void onLinkRemoved(LinkID linkID) {
            H h = GraphWrapper.this.edge(linkID);
            if (h != null) {
                GraphWrapper.this.graph().removeEdge(h);
                GraphWrapper.this.edgeMap().set((Object)h, (Object)null);
                GraphWrapper.this.cleanupEdgeRemoved(h);
            }
            GraphWrapper.this.linkMap().remove((Guid)linkID);
        }
    }

}

