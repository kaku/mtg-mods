/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreWriter
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutStore
 *  com.paterva.maltego.graph.store.layout.GraphLayoutWriter
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.structure.GraphStructureWriter
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.graph.wrapper;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreWriter;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.layout.GraphLayoutWriter;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.structure.GraphStructureWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;

public class GraphStoreWriter {
    private static final Logger LOG = Logger.getLogger(GraphStoreWriter.class.getName());

    public static void addEntity(GraphID graphID, MaltegoEntity maltegoEntity) {
        GraphStoreWriter.addEntities(graphID, Collections.singleton(maltegoEntity));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void addEntities(GraphID graphID, Collection<MaltegoEntity> collection) {
        if (LOG.isLoggable(Level.FINEST)) {
            LOG.log(Level.FINEST, "Adding Entities to {0}:", (Object)graphID);
            for (MaltegoEntity graphLayoutWriter : collection) {
                LOG.finest(graphLayoutWriter.toString());
            }
        }
        Object object = null;
        try {
            object = GraphStoreRegistry.getDefault().forGraphID(graphID);
            object.beginUpdate();
            GraphLayoutWriter graphLayoutWriter = object.getGraphLayoutStore().getLayoutWriter();
            GraphStructureWriter graphStructureWriter = object.getGraphStructureStore().getStructureWriter();
            GraphDataStoreWriter graphDataStoreWriter = object.getGraphDataStore().getDataStoreWriter();
            HashSet<EntityID> hashSet = new HashSet<EntityID>();
            for (MaltegoEntity maltegoEntity : collection) {
                EntityID entityID = (EntityID)maltegoEntity.getID();
                hashSet.add(entityID);
            }
            graphLayoutWriter.addEntities(hashSet);
            graphStructureWriter.add(hashSet);
            graphDataStoreWriter.putEntities(collection);
        }
        catch (GraphStoreException var3_5) {
            Exceptions.printStackTrace((Throwable)var3_5);
        }
        finally {
            if (object != null) {
                object.endUpdate((Object)null);
            }
        }
    }

    public static void addLink(GraphID graphID, MaltegoLink maltegoLink, LinkEntityIDs linkEntityIDs) {
        GraphStoreWriter.addLinks(graphID, Collections.singletonMap(maltegoLink, linkEntityIDs));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void addLinks(GraphID graphID, Map<MaltegoLink, LinkEntityIDs> map) {
        GraphDataStoreWriter graphDataStoreWriter;
        GraphStructureWriter graphStructureWriter;
        if (LOG.isLoggable(Level.FINEST)) {
            LOG.log(Level.FINEST, "Adding Links to {0}:", (Object)graphID);
            for (Map.Entry object2 : map.entrySet()) {
                graphStructureWriter = (MaltegoLink)object2.getKey();
                graphDataStoreWriter = (LinkEntityIDs)object2.getValue();
                LOG.log(Level.FINEST, "{0}: {1}->{2}", new Object[]{graphStructureWriter.getID(), graphDataStoreWriter.getSourceID(), graphDataStoreWriter.getTargetID()});
            }
        }
        Object object = null;
        try {
            object = GraphStoreRegistry.getDefault().forGraphID(graphID);
            object.beginUpdate();
            GraphLayoutWriter graphLayoutWriter = object.getGraphLayoutStore().getLayoutWriter();
            graphStructureWriter = object.getGraphStructureStore().getStructureWriter();
            graphDataStoreWriter = object.getGraphDataStore().getDataStoreWriter();
            HashMap<Guid, LinkEntityIDs> hashMap = new HashMap<Guid, LinkEntityIDs>();
            for (Map.Entry<MaltegoLink, LinkEntityIDs> entry : map.entrySet()) {
                MaltegoLink maltegoLink = entry.getKey();
                LinkEntityIDs linkEntityIDs = entry.getValue();
                hashMap.put(maltegoLink.getID(), linkEntityIDs);
            }
            graphLayoutWriter.addLinks(hashMap.keySet());
            graphStructureWriter.connect(hashMap);
            graphDataStoreWriter.putLinks(map.keySet());
        }
        catch (GraphStoreException var3_5) {
            Exceptions.printStackTrace((Throwable)var3_5);
        }
        finally {
            if (object != null) {
                object.endUpdate((Object)null);
            }
        }
    }

    public static void removeEntities(GraphID graphID, Collection<EntityID> collection) {
        GraphStoreWriter.remove(graphID, collection, Collections.EMPTY_SET);
    }

    public static void removeLinks(GraphID graphID, Collection<LinkID> collection) {
        GraphStoreWriter.remove(graphID, Collections.EMPTY_SET, collection);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void remove(GraphID graphID, Collection<EntityID> collection, Collection<LinkID> collection2) {
        if (LOG.isLoggable(Level.FINEST) && !collection.isEmpty()) {
            LOG.log(Level.FINEST, "Removing Entities from {0}:", (Object)graphID);
            for (EntityID graphStructureWriter : collection) {
                LOG.finest(graphStructureWriter.toString());
            }
        }
        if (LOG.isLoggable(Level.FINEST) && !collection2.isEmpty()) {
            LOG.log(Level.FINEST, "Removing Links from {0}:", (Object)graphID);
            for (LinkID linkID : collection2) {
                LOG.finest(linkID.toString());
            }
        }
        Object object = null;
        try {
            object = GraphStoreRegistry.getDefault().forGraphID(graphID);
            object.beginUpdate();
            GraphStructureWriter graphStructureWriter = object.getGraphStructureStore().getStructureWriter();
            GraphDataStoreWriter graphDataStoreWriter = object.getGraphDataStore().getDataStoreWriter();
            GraphLayoutWriter graphLayoutWriter = object.getGraphLayoutStore().getLayoutWriter();
            graphStructureWriter.removeLinks(collection2);
            Set set = graphStructureWriter.removeEntities(collection);
            set.addAll(collection2);
            graphLayoutWriter.remove(collection, (Collection)set);
            graphDataStoreWriter.removeLinks((Collection)set);
            graphDataStoreWriter.removeEntities(collection);
        }
        catch (GraphStoreException var4_8) {
            Exceptions.printStackTrace((Throwable)var4_8);
        }
        finally {
            if (object != null) {
                object.endUpdate((Object)null);
            }
        }
    }

    public static void clear(GraphID graphID) {
        LOG.log(Level.FINEST, "Clearing graph {0}", (Object)graphID);
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            HashSet<EntityID> hashSet = new HashSet<EntityID>(graphStructureReader.getEntities());
            GraphStoreWriter.removeEntities(graphID, hashSet);
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }
}

