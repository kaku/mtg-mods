/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.util.BulkStatusDisplayer
 *  yguard.A.A.D
 *  yguard.A.A.F
 *  yguard.A.A.H
 *  yguard.A.A.Y
 */
package com.paterva.maltego.graph.wrapper;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.util.BulkStatusDisplayer;
import yguard.A.A.D;
import yguard.A.A.F;
import yguard.A.A.H;
import yguard.A.A.Y;

public class DataAcceptors {

    public static class Link
    implements F {
        private int _count = 0;
        private D _graph;
        private final BulkStatusDisplayer _statusDisplayer = new BulkStatusDisplayer("Loading links (%d)");

        public Link(D d) {
            this._graph = d;
        }

        public void set(Object object, Object object2) {
            H h = (H)object;
            MaltegoLink maltegoLink = (MaltegoLink)object2;
            this._statusDisplayer.increment();
        }

        public void setInt(Object object, int n) {
            throw new UnsupportedOperationException("Not supported.");
        }

        public void setDouble(Object object, double d) {
            throw new UnsupportedOperationException("Not supported.");
        }

        public void setBool(Object object, boolean bl) {
            throw new UnsupportedOperationException("Not supported.");
        }
    }

    public static class Entity
    implements F {
        private int _count = 0;
        private D _graph;
        private final BulkStatusDisplayer _statusDisplayer = new BulkStatusDisplayer("Loading entities (%d)");

        public Entity(D d) {
            this._graph = d;
        }

        public void set(Object object, Object object2) {
            Y y = (Y)object;
            MaltegoEntity maltegoEntity = (MaltegoEntity)object2;
            this._statusDisplayer.increment();
        }

        public void setInt(Object object, int n) {
            throw new UnsupportedOperationException("Not supported.");
        }

        public void setDouble(Object object, double d) {
            throw new UnsupportedOperationException("Not supported.");
        }

        public void setBool(Object object, boolean bl) {
            throw new UnsupportedOperationException("Not supported.");
        }
    }

}

