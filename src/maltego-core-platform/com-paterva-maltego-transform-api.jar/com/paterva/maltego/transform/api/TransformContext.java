/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.api;

import com.paterva.maltego.transform.api.TransformMessage;

public interface TransformContext {
    public void updateProgress(String var1);

    public void updateProgress(String var1, int var2);

    public void updateProgress(int var1);

    public void updateProgress(String var1, boolean var2);

    public void updateProgress(String var1, int var2, boolean var3);

    public void updateProgress(int var1, boolean var2);

    public void log(String var1, TransformMessage.Severity var2);

    public boolean isCancelled();

    public Object getInput(String var1);

    public <T> T getInput(String var1, T var2);
}

