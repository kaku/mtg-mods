/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 */
package com.paterva.maltego.transform.api;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.transform.api.TransformContext;
import com.paterva.maltego.transform.api.TransformException;

public interface Transform {
    public void transform(GraphID var1, TransformContext var2) throws TransformException;
}

