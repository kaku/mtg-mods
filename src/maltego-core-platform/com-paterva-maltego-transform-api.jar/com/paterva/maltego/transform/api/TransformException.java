/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.api;

public class TransformException
extends Exception {
    public TransformException() {
    }

    public TransformException(String string) {
        super(string);
    }

    public TransformException(String string, Throwable throwable) {
        super(string, throwable);
    }
}

