/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.api;

import java.util.Date;

public class TransformMessage {
    private Date _timestamp;
    private String _text;
    private Severity _severity;
    private int _code;
    private boolean _log = false;

    public TransformMessage(String string) {
        this(null, Severity.Info, string);
    }

    public TransformMessage(Severity severity, String string) {
        this(null, severity, string);
    }

    public TransformMessage(Date date, Severity severity, String string) {
        this(date, severity, string, 0, false);
    }

    public TransformMessage(Date date, Severity severity, String string, boolean bl) {
        this(date, severity, string, 0, bl);
    }

    public TransformMessage(Date date, Severity severity, String string, int n) {
        this(date, severity, string, n, false);
    }

    public TransformMessage(Date date, Severity severity, String string, int n, boolean bl) {
        this._timestamp = date;
        this._severity = severity;
        this._text = string;
        this._code = n;
        this._log = bl;
    }

    public Date getTimestamp() {
        return this._timestamp;
    }

    public void setTimestamp(Date date) {
        this._timestamp = date;
    }

    public String getText() {
        return this._text;
    }

    public void setText(String string) {
        this._text = string;
    }

    public Severity getSeverity() {
        return this._severity;
    }

    public void setSeverity(Severity severity) {
        this._severity = severity;
    }

    public int getCode() {
        return this._code;
    }

    public void setCode(int n) {
        this._code = n;
    }

    public boolean mustLog() {
        return this._log;
    }

    public void setMustLog(boolean bl) {
        this._log = bl;
    }

    public static enum Severity {
        Debug,
        Info,
        Warning,
        Error;
        

        private Severity() {
        }
    }

}

