/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 */
package com.paterva.maltego.matching;

import com.paterva.maltego.matching.AndMatchingRuleDescriptor;
import com.paterva.maltego.matching.MatchingRule;
import com.paterva.maltego.matching.MatchingRuleFactory;
import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import java.util.ArrayList;
import java.util.Collection;

public class AndMatchingRuleFactory
extends MatchingRuleFactory {
    @Override
    public boolean isSupported(MatchingRuleDescriptor matchingRuleDescriptor) {
        return matchingRuleDescriptor instanceof AndMatchingRuleDescriptor;
    }

    @Override
    public MatchingRule create(MatchingRuleDescriptor matchingRuleDescriptor) {
        if (matchingRuleDescriptor instanceof AndMatchingRuleDescriptor) {
            AndMatchingRuleDescriptor andMatchingRuleDescriptor = (AndMatchingRuleDescriptor)matchingRuleDescriptor;
            ArrayList<MatchingRule> arrayList = new ArrayList<MatchingRule>();
            for (MatchingRuleDescriptor matchingRuleDescriptor2 : andMatchingRuleDescriptor.getRules()) {
                MatchingRule matchingRule = MatchingRuleFactory.createFrom(matchingRuleDescriptor2);
                arrayList.add(matchingRule);
            }
            return new MatchingRule.And(arrayList.toArray(new MatchingRule[arrayList.size()]));
        }
        throw new IllegalArgumentException("Descriptor not supported: " + (Object)matchingRuleDescriptor);
    }
}

