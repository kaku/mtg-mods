/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 */
package com.paterva.maltego.matching;

import com.paterva.maltego.matching.LayeredMatchingRuleDescriptor;
import com.paterva.maltego.matching.MatchingRule;
import com.paterva.maltego.matching.MatchingRuleFactory;
import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import java.util.ArrayList;

public class LayeredMatchingRuleFactory
extends MatchingRuleFactory {
    @Override
    public boolean isSupported(MatchingRuleDescriptor matchingRuleDescriptor) {
        return matchingRuleDescriptor instanceof LayeredMatchingRuleDescriptor;
    }

    @Override
    public MatchingRule create(MatchingRuleDescriptor matchingRuleDescriptor) {
        if (matchingRuleDescriptor instanceof LayeredMatchingRuleDescriptor) {
            LayeredMatchingRuleDescriptor layeredMatchingRuleDescriptor = (LayeredMatchingRuleDescriptor)matchingRuleDescriptor;
            ArrayList<MatchingRule> arrayList = new ArrayList<MatchingRule>();
            for (MatchingRuleDescriptor matchingRuleDescriptor2 : layeredMatchingRuleDescriptor.getRules()) {
                MatchingRule matchingRule = MatchingRuleFactory.createFrom(matchingRuleDescriptor2);
                arrayList.add(matchingRule);
            }
            return new MatchingRule.Layered(arrayList.toArray(new MatchingRule[arrayList.size()]));
        }
        throw new IllegalArgumentException("Descriptor not supported: " + (Object)matchingRuleDescriptor);
    }
}

