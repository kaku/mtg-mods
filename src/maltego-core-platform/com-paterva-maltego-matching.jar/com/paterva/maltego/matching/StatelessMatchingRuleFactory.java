/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 */
package com.paterva.maltego.matching;

import com.paterva.maltego.matching.MatchingRule;
import com.paterva.maltego.matching.MatchingRuleFactory;
import com.paterva.maltego.matching.StatelessMatchingRuleDescriptor;
import com.paterva.maltego.matching.api.MatchingRuleDescriptor;

public class StatelessMatchingRuleFactory
extends MatchingRuleFactory {
    @Override
    public boolean isSupported(MatchingRuleDescriptor matchingRuleDescriptor) {
        return matchingRuleDescriptor instanceof StatelessMatchingRuleDescriptor;
    }

    @Override
    public MatchingRule create(MatchingRuleDescriptor matchingRuleDescriptor) {
        if (StatelessMatchingRuleDescriptor.Id.equals((Object)matchingRuleDescriptor)) {
            return MatchingRule.Id;
        }
        if (StatelessMatchingRuleDescriptor.Type.equals((Object)matchingRuleDescriptor)) {
            return MatchingRule.Type;
        }
        if (StatelessMatchingRuleDescriptor.Value.equals((Object)matchingRuleDescriptor)) {
            return MatchingRule.Value;
        }
        if (StatelessMatchingRuleDescriptor.Default.equals((Object)matchingRuleDescriptor)) {
            return MatchingRule.Default;
        }
        if (StatelessMatchingRuleDescriptor.PropertiesExceptAttachments.equals((Object)matchingRuleDescriptor)) {
            return MatchingRule.PropertiesExceptAttachments;
        }
        throw new IllegalArgumentException("Unsupported stateless descriptor: " + (Object)matchingRuleDescriptor);
    }
}

