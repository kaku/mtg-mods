/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 */
package com.paterva.maltego.matching;

import com.paterva.maltego.matching.api.MatchingRuleDescriptor;

public class LayeredMatchingRuleDescriptor
implements MatchingRuleDescriptor {
    private final MatchingRuleDescriptor[] _rules;

    public /* varargs */ LayeredMatchingRuleDescriptor(MatchingRuleDescriptor ... arrmatchingRuleDescriptor) {
        this._rules = arrmatchingRuleDescriptor;
    }

    public MatchingRuleDescriptor[] getRules() {
        return this._rules;
    }
}

