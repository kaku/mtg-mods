/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 */
package com.paterva.maltego.matching;

import com.paterva.maltego.matching.api.MatchingRuleDescriptor;

public class StatelessMatchingRuleDescriptor
implements MatchingRuleDescriptor {
    public static final MatchingRuleDescriptor Id = new StatelessMatchingRuleDescriptor();
    public static final MatchingRuleDescriptor Type = new StatelessMatchingRuleDescriptor();
    public static final MatchingRuleDescriptor Value = new StatelessMatchingRuleDescriptor();
    public static final MatchingRuleDescriptor Default = new StatelessMatchingRuleDescriptor();
    public static final MatchingRuleDescriptor PropertiesExceptAttachments = new StatelessMatchingRuleDescriptor();
}

