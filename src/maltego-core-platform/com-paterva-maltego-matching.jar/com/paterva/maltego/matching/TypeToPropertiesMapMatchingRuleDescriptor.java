/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 */
package com.paterva.maltego.matching;

import com.paterva.maltego.matching.AndMatchingRuleDescriptor;
import com.paterva.maltego.matching.LayeredMatchingRuleDescriptor;
import com.paterva.maltego.matching.PropertyMatchingRuleDescriptor;
import com.paterva.maltego.matching.StatelessMatchingRuleDescriptor;
import com.paterva.maltego.matching.TypeMapMatchingRuleDescriptor;
import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class TypeToPropertiesMapMatchingRuleDescriptor
extends TypeMapMatchingRuleDescriptor {
    public void update(String string, Collection<String> collection, boolean bl) {
        ArrayList<MatchingRuleDescriptor> arrayList = new ArrayList<MatchingRuleDescriptor>();
        if (bl) {
            arrayList.add(StatelessMatchingRuleDescriptor.Value);
        }
        if (collection != null) {
            for (String string2 : collection) {
                arrayList.add(new PropertyMatchingRuleDescriptor(string2));
            }
        }
        if (arrayList.size() > 0) {
            this.getMap().put(string, new LayeredMatchingRuleDescriptor(new AndMatchingRuleDescriptor(arrayList)));
        }
    }
}

