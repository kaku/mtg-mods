/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.matching;

import com.paterva.maltego.matching.MatchingRule;
import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.openide.util.Lookup;

public abstract class MatchingRuleFactory {
    private static Map<MatchingRuleDescriptor, MatchingRule> _cache;
    private static final Collection<? extends MatchingRuleFactory> _factories;

    public abstract boolean isSupported(MatchingRuleDescriptor var1);

    public abstract MatchingRule create(MatchingRuleDescriptor var1);

    public static void setCacheEnabled(boolean bl) {
        _cache = bl ? new HashMap<K, V>() : null;
    }

    public static MatchingRule createFrom(MatchingRuleDescriptor matchingRuleDescriptor) {
        MatchingRule matchingRule = null;
        if (_cache != null) {
            matchingRule = _cache.get((Object)matchingRuleDescriptor);
        }
        if (matchingRule == null) {
            for (MatchingRuleFactory matchingRuleFactory : _factories) {
                if (!matchingRuleFactory.isSupported(matchingRuleDescriptor) || (matchingRule = matchingRuleFactory.create(matchingRuleDescriptor)) == null || _cache == null) continue;
                _cache.put(matchingRuleDescriptor, matchingRule);
            }
        }
        if (matchingRule == null) {
            throw new IllegalArgumentException("No factory found to create MatchingRule from " + (Object)matchingRuleDescriptor);
        }
        return matchingRule;
    }

    static {
        _factories = Lookup.getDefault().lookupAll(MatchingRuleFactory.class);
    }
}

