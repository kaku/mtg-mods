/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 */
package com.paterva.maltego.matching;

import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import java.util.Collection;

public class AndMatchingRuleDescriptor
implements MatchingRuleDescriptor {
    private final Collection<MatchingRuleDescriptor> _rules;

    public AndMatchingRuleDescriptor(Collection<MatchingRuleDescriptor> collection) {
        this._rules = collection;
    }

    public Collection<MatchingRuleDescriptor> getRules() {
        return this._rules;
    }
}

