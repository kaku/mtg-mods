/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 */
package com.paterva.maltego.matching;

import com.paterva.maltego.matching.api.MatchingRuleDescriptor;

public class PropertyMatchingRuleDescriptor
implements MatchingRuleDescriptor {
    private final String _property;

    public PropertyMatchingRuleDescriptor(String string) {
        this._property = string;
    }

    public String getProperty() {
        return this._property;
    }
}

