/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.IdentityProvider
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.types.Attachments
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.matching;

import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.IdentityProvider;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.types.Attachments;
import com.paterva.maltego.util.StringUtilities;
import java.util.ArrayList;
import java.util.Collection;
import java.util.SortedMap;
import java.util.TreeMap;
import org.openide.util.Utilities;

public interface MatchingRule {
    public static final int MATCH = 1;
    public static final int MISMATCH = -1;
    public static final int INDETERMINATE = 0;
    public static final MatchingRule Id = new Id();
    public static final MatchingRule Type = new Type();
    public static final MatchingRule Value = new Value();
    public static final MatchingRule Default = new Default();
    public static final MatchingRule PropertiesExceptAttachments = new PropertiesExceptAttachments();

    public int match(SpecRegistry var1, TypedPropertyBag var2, TypedPropertyBag var3);

    public static class PropertiesExceptAttachments
    implements MatchingRule {
        @Override
        public int match(SpecRegistry specRegistry, TypedPropertyBag typedPropertyBag, TypedPropertyBag typedPropertyBag2) {
            PropertyDescriptorCollection propertyDescriptorCollection = typedPropertyBag.getProperties();
            PropertyDescriptorCollection propertyDescriptorCollection2 = typedPropertyBag2.getProperties();
            if (propertyDescriptorCollection.size() != propertyDescriptorCollection2.size()) {
                return -1;
            }
            for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
                if (propertyDescriptor == null || Attachments.class.equals((Object)propertyDescriptor.getType())) continue;
                PropertyDescriptor propertyDescriptor2 = typedPropertyBag2.getProperties().get(propertyDescriptor.getName());
                if (propertyDescriptor2 != null) {
                    Object object;
                    Object object2 = typedPropertyBag.getValue(propertyDescriptor);
                    if (Utilities.compareObjects((Object)object2, (Object)(object = typedPropertyBag2.getValue(propertyDescriptor2)))) continue;
                    return -1;
                }
                return -1;
            }
            return 1;
        }
    }

    public static class Property
    implements MatchingRule {
        private String _property;

        public Property(String string) {
            this._property = string;
        }

        @Override
        public int match(SpecRegistry specRegistry, TypedPropertyBag typedPropertyBag, TypedPropertyBag typedPropertyBag2) {
            PropertyDescriptor propertyDescriptor;
            Object object;
            Object object2;
            PropertyDescriptor propertyDescriptor2 = typedPropertyBag.getProperties().get(this._property);
            if (propertyDescriptor2 != null && (propertyDescriptor = typedPropertyBag2.getProperties().get(this._property)) != null && (object2 = typedPropertyBag.getValue(propertyDescriptor2)) != null && (object = typedPropertyBag2.getValue(propertyDescriptor)) != null) {
                return object2.equals(object) ? 1 : -1;
            }
            return 0;
        }
    }

    public static class Value
    implements MatchingRule {
        @Override
        public int match(SpecRegistry specRegistry, TypedPropertyBag typedPropertyBag, TypedPropertyBag typedPropertyBag2) {
            Object object = InheritanceHelper.getValue((SpecRegistry)specRegistry, (TypedPropertyBag)typedPropertyBag);
            Object object2 = InheritanceHelper.getValue((SpecRegistry)specRegistry, (TypedPropertyBag)typedPropertyBag2);
            if (object != null) {
                if (object2 != null) {
                    return object.equals(object2) ? 1 : -1;
                }
                if (StringUtilities.isNullString((Object)object)) {
                    return 1;
                }
            } else if (object2 == null || StringUtilities.isNullString((Object)object2)) {
                return 1;
            }
            return 0;
        }
    }

    public static class Default
    implements MatchingRule {
        @Override
        public int match(SpecRegistry specRegistry, TypedPropertyBag typedPropertyBag, TypedPropertyBag typedPropertyBag2) {
            int n = Id.match(specRegistry, typedPropertyBag, typedPropertyBag2);
            if (n != 1 && (n = Type.match(specRegistry, typedPropertyBag, typedPropertyBag2)) == 1) {
                n = Value.match(specRegistry, typedPropertyBag, typedPropertyBag2);
            }
            return n;
        }
    }

    public static class And
    implements MatchingRule {
        private MatchingRule[] _rules;

        public /* varargs */ And(MatchingRule ... arrmatchingRule) {
            this._rules = arrmatchingRule;
        }

        @Override
        public int match(SpecRegistry specRegistry, TypedPropertyBag typedPropertyBag, TypedPropertyBag typedPropertyBag2) {
            boolean bl = false;
            for (MatchingRule matchingRule : this._rules) {
                int n = matchingRule.match(specRegistry, typedPropertyBag, typedPropertyBag2);
                if (n == 0) {
                    return 0;
                }
                if (n != -1) continue;
                bl = true;
                break;
            }
            if (bl) {
                return -1;
            }
            return 1;
        }
    }

    public static class Type
    implements MatchingRule {
        @Override
        public int match(SpecRegistry specRegistry, TypedPropertyBag typedPropertyBag, TypedPropertyBag typedPropertyBag2) {
            return typedPropertyBag.getTypeName().equals(typedPropertyBag2.getTypeName()) ? 1 : -1;
        }
    }

    public static class Id
    implements MatchingRule {
        @Override
        public int match(SpecRegistry specRegistry, TypedPropertyBag typedPropertyBag, TypedPropertyBag typedPropertyBag2) {
            if (typedPropertyBag instanceof IdentityProvider && typedPropertyBag2 instanceof IdentityProvider) {
                IdentityProvider identityProvider = (IdentityProvider)typedPropertyBag;
                IdentityProvider identityProvider2 = (IdentityProvider)typedPropertyBag2;
                return Utilities.compareObjects((Object)identityProvider.getID(), (Object)identityProvider2.getID()) ? 1 : -1;
            }
            return 0;
        }
    }

    public static class TypeToPropertiesMap
    extends TypeMap {
        public void update(String string, Collection<String> collection, boolean bl) {
            ArrayList<MatchingRule> arrayList = new ArrayList<MatchingRule>();
            if (bl) {
                arrayList.add(MatchingRule.Value);
            }
            if (collection != null) {
                for (String string2 : collection) {
                    Property property = new Property(string2);
                    arrayList.add(property);
                }
            }
            if (arrayList.size() > 0) {
                this.put(string, new Layered(new And(arrayList.toArray(new MatchingRule[arrayList.size()]))));
            }
        }
    }

    public static class TypeMap
    implements MatchingRule {
        private SortedMap<String, MatchingRule> _map;

        public void put(String string, MatchingRule matchingRule) {
            if (this._map == null) {
                this._map = new TreeMap<String, MatchingRule>();
            }
            this._map.put(string, matchingRule);
        }

        public MatchingRule get(String string) {
            return this._map != null ? this._map.get(string) : null;
        }

        @Override
        public int match(SpecRegistry specRegistry, TypedPropertyBag typedPropertyBag, TypedPropertyBag typedPropertyBag2) {
            if (!typedPropertyBag.getTypeName().equals(typedPropertyBag2.getTypeName())) {
                return -1;
            }
            if (this._map == null) {
                return 0;
            }
            MatchingRule matchingRule = this._map.get(typedPropertyBag.getTypeName());
            if (matchingRule == null) {
                return 0;
            }
            return matchingRule.match(specRegistry, typedPropertyBag, typedPropertyBag2);
        }
    }

    public static class Layered
    implements MatchingRule {
        private MatchingRule[] _rules;

        public /* varargs */ Layered(MatchingRule ... arrmatchingRule) {
            this._rules = arrmatchingRule;
        }

        @Override
        public int match(SpecRegistry specRegistry, TypedPropertyBag typedPropertyBag, TypedPropertyBag typedPropertyBag2) {
            for (MatchingRule matchingRule : this._rules) {
                int n = matchingRule.match(specRegistry, typedPropertyBag, typedPropertyBag2);
                if (n == 0) continue;
                return n;
            }
            return 0;
        }
    }

}

