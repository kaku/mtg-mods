/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 */
package com.paterva.maltego.matching;

import com.paterva.maltego.matching.MatchingRule;
import com.paterva.maltego.matching.MatchingRuleFactory;
import com.paterva.maltego.matching.TypeMapMatchingRuleDescriptor;
import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import java.util.Map;
import java.util.Set;

public class TypeMapMatchingRuleFactory
extends MatchingRuleFactory {
    @Override
    public boolean isSupported(MatchingRuleDescriptor matchingRuleDescriptor) {
        return matchingRuleDescriptor instanceof TypeMapMatchingRuleDescriptor;
    }

    @Override
    public MatchingRule create(MatchingRuleDescriptor matchingRuleDescriptor) {
        if (matchingRuleDescriptor instanceof TypeMapMatchingRuleDescriptor) {
            TypeMapMatchingRuleDescriptor typeMapMatchingRuleDescriptor = (TypeMapMatchingRuleDescriptor)matchingRuleDescriptor;
            MatchingRule.TypeMap typeMap = new MatchingRule.TypeMap();
            for (Map.Entry<String, MatchingRuleDescriptor> entry : typeMapMatchingRuleDescriptor.getMap().entrySet()) {
                String string = entry.getKey();
                MatchingRuleDescriptor matchingRuleDescriptor2 = entry.getValue();
                MatchingRule matchingRule = MatchingRuleFactory.createFrom(matchingRuleDescriptor2);
                typeMap.put(string, matchingRule);
            }
            return typeMap;
        }
        throw new IllegalArgumentException("Descriptor not supported: " + (Object)matchingRuleDescriptor);
    }
}

