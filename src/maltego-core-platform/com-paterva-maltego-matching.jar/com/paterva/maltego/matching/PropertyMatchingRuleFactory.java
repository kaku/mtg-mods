/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 */
package com.paterva.maltego.matching;

import com.paterva.maltego.matching.MatchingRule;
import com.paterva.maltego.matching.MatchingRuleFactory;
import com.paterva.maltego.matching.PropertyMatchingRuleDescriptor;
import com.paterva.maltego.matching.api.MatchingRuleDescriptor;

public class PropertyMatchingRuleFactory
extends MatchingRuleFactory {
    @Override
    public boolean isSupported(MatchingRuleDescriptor matchingRuleDescriptor) {
        return matchingRuleDescriptor instanceof PropertyMatchingRuleDescriptor;
    }

    @Override
    public MatchingRule create(MatchingRuleDescriptor matchingRuleDescriptor) {
        if (matchingRuleDescriptor instanceof PropertyMatchingRuleDescriptor) {
            PropertyMatchingRuleDescriptor propertyMatchingRuleDescriptor = (PropertyMatchingRuleDescriptor)matchingRuleDescriptor;
            return new MatchingRule.Property(propertyMatchingRuleDescriptor.getProperty());
        }
        throw new IllegalArgumentException("Descriptor not supported: " + (Object)matchingRuleDescriptor);
    }
}

