/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 */
package com.paterva.maltego.matching;

import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import java.util.HashMap;
import java.util.Map;

public class TypeMapMatchingRuleDescriptor
implements MatchingRuleDescriptor {
    private Map<String, MatchingRuleDescriptor> _map;

    public synchronized Map<String, MatchingRuleDescriptor> getMap() {
        if (this._map == null) {
            this._map = new HashMap<String, MatchingRuleDescriptor>();
        }
        return this._map;
    }
}

