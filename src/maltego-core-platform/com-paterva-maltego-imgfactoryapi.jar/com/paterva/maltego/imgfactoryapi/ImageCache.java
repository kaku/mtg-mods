/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.util.ImageCallback
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.imgfactoryapi;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.imgfactoryapi.ImageFactory;
import com.paterva.maltego.util.ImageCallback;
import java.awt.Image;
import javax.swing.ImageIcon;
import org.openide.util.Lookup;

public abstract class ImageCache
extends ImageFactory {
    private static ImageCache _default;
    public static final int MAX_SIZE = 240;

    public static ImageCache getDefault() {
        if (_default == null && (ImageCache._default = (ImageCache)Lookup.getDefault().lookup(ImageCache.class)) == null) {
            throw new IllegalStateException("No icon cache found.");
        }
        return _default;
    }

    public abstract void addImageIcon(Object var1, ImageIcon var2);

    public abstract ImageIcon resizeImageIcon(GraphID var1, Object var2, ImageIcon var3, int var4, int var5, ImageCallback var6);

    public abstract void addImage(Object var1, Image var2);

    public abstract Image resizeImage(GraphID var1, Object var2, Image var3, int var4, int var5, ImageCallback var6);

    public abstract boolean contains(Object var1);

    public abstract void remove(Object var1);

    public abstract boolean mustResize(ImageIcon var1, int var2, int var3);
}

