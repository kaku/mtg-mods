/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.IconSize
 *  com.paterva.maltego.util.ImageUtils
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.imgfactoryapi;

import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.util.IconSize;
import com.paterva.maltego.util.ImageUtils;
import com.paterva.maltego.util.StringUtilities;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.EnumMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class IconUtils {
    public static String createUniqueIconName(IconRegistry iconRegistry, String string) {
        string = IconUtils.makeValidIconName(string);
        while (iconRegistry.exists(string)) {
            string = string + "_";
        }
        return string;
    }

    public static String makeValidIconName(String string) {
        return StringUtilities.replaceDigitsWithWords((String)string).replaceAll("[^a-zA-Z _]", "_");
    }

    public static Image fixSize(Image image, IconSize iconSize) {
        BufferedImage bufferedImage = ImageUtils.createBufferedImage((Image)image);
        if (IconUtils.mustResize(bufferedImage, iconSize)) {
            image = ImageUtils.smartSize((BufferedImage)bufferedImage, (double)iconSize.getSize(), (double)iconSize.getSize());
        }
        return image;
    }

    public static boolean mustResize(BufferedImage bufferedImage, IconSize iconSize) {
        return bufferedImage.getWidth() != iconSize.getSize() || bufferedImage.getHeight() != iconSize.getSize();
    }

    public static void resizeAndAddIcon(Map<IconSize, Image> map, Image image, IconSize iconSize) {
        map.put(iconSize, IconUtils.fixSize(image, iconSize));
    }

    public static void createMissingIcons(Map<IconSize, Image> map) {
        Image image = IconUtils.getLargest(map);
        for (IconSize iconSize : IconSize.values()) {
            Image image2 = map.get((Object)iconSize);
            if (image2 != null) continue;
            map.put(iconSize, IconUtils.fixSize(image, iconSize));
        }
    }

    public static Image getLargest(Map<IconSize, Image> map) {
        IconSize[] arriconSize = IconSize.values();
        for (int i = arriconSize.length - 1; i >= 0; --i) {
            Image image = map.get((Object)arriconSize[i]);
            if (image == null) continue;
            return image;
        }
        return null;
    }

    public static String findIconName(IconRegistry iconRegistry, Image image, IconSize iconSize) {
        BufferedImage bufferedImage = ImageUtils.createBufferedImage((Image)image);
        Set<String> set = iconRegistry.getAllIconNames();
        for (String string : set) {
            try {
                Image image2 = iconRegistry.loadImage(string, iconSize);
                BufferedImage bufferedImage2 = ImageUtils.createBufferedImage((Image)image2);
                if (!ImageUtils.equals((BufferedImage)bufferedImage, (BufferedImage)bufferedImage2)) continue;
                return string;
            }
            catch (IOException var7_8) {
                Logger.getLogger(IconUtils.class.getName()).log(Level.SEVERE, null, var7_8);
                continue;
            }
        }
        return null;
    }

    public static Map<IconSize, Image> createSizedIcons(Image image) {
        EnumMap<IconSize, Image> enumMap = new EnumMap<IconSize, Image>(IconSize.class);
        for (IconSize iconSize : IconSize.values()) {
            enumMap.put(iconSize, IconUtils.fixSize(image, iconSize));
        }
        return enumMap;
    }
}

