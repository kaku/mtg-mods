/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.util.ImageCallback
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.imgfactoryapi;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.imgfactoryapi.ImageCache;
import com.paterva.maltego.util.ImageCallback;
import java.awt.Image;
import javax.swing.ImageIcon;
import org.openide.util.Lookup;

public abstract class ImageFactory {
    private static ImageFactory _default;

    public static ImageFactory getDefault() {
        if (_default == null && (ImageFactory._default = (ImageFactory)Lookup.getDefault().lookup(ImageCache.class)) == null) {
            throw new IllegalStateException("No icon factory found.");
        }
        return _default;
    }

    public abstract ImageIcon getImageIcon(GraphID var1, Object var2, ImageCallback var3);

    public ImageIcon getImageIcon(Object object, ImageCallback imageCallback) {
        return this.getImageIcon(null, object, imageCallback);
    }

    public abstract ImageIcon getImageIcon(GraphID var1, Object var2, int var3, int var4, ImageCallback var5);

    public ImageIcon getImageIcon(Object object, int n, int n2, ImageCallback imageCallback) {
        return this.getImageIcon(null, object, n, n2, imageCallback);
    }

    public abstract Image getImage(GraphID var1, Object var2, ImageCallback var3);

    public Image getImage(Object object, ImageCallback imageCallback) {
        return this.getImage(null, object, imageCallback);
    }

    public abstract Image getImage(GraphID var1, Object var2, int var3, int var4, ImageCallback var5);

    public Image getImage(Object object, int n, int n2, ImageCallback imageCallback) {
        return this.getImage(null, object, n, n2, imageCallback);
    }
}

