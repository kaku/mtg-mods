/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphUserData
 *  com.paterva.maltego.util.IconSize
 *  com.paterva.maltego.util.ImageUtils
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.imgfactoryapi;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.imgfactoryapi.IconCatalog;
import com.paterva.maltego.imgfactoryapi.IconNameMapping;
import com.paterva.maltego.util.IconSize;
import com.paterva.maltego.util.ImageUtils;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import javax.imageio.ImageIO;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public abstract class IconRegistry {
    public static final String PROP_ADDED = "addedIcon";
    public static final String PROP_REMOVED_ICON = "removedIcon";
    public static final String PROP_REMOVED_CATEGORY = "removedCategory";
    public static final String PROP_RENAMED = "renamedIcon";
    private static IconRegistry _default;

    public static synchronized IconRegistry getDefault() {
        if (_default == null && (IconRegistry._default = (IconRegistry)Lookup.getDefault().lookup(IconRegistry.class)) == null) {
            throw new IllegalStateException("No icon registries found.");
        }
        return _default;
    }

    public static synchronized IconRegistry forGraphID(GraphID graphID) {
        IconRegistry iconRegistry = null;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID, (boolean)false);
        if (graphUserData != null) {
            String string = IconRegistry.class.getName();
            iconRegistry = (IconRegistry)graphUserData.get((Object)string);
        }
        return iconRegistry != null ? iconRegistry : IconRegistry.getDefault();
    }

    public static synchronized void associate(GraphID graphID, IconRegistry iconRegistry) {
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        String string = IconRegistry.class.getName();
        graphUserData.put((Object)string, (Object)iconRegistry);
    }

    public abstract void add(String var1, String var2, Image var3, Image var4, Image var5, Image var6) throws IOException;

    public abstract void add(String var1, String var2, FileObject var3, FileObject var4, FileObject var5, FileObject var6) throws IOException;

    public abstract void remove(String var1) throws IOException;

    public abstract void removeCategory(String var1) throws IOException;

    public abstract Set<String> getCategories();

    public abstract Set<String> getIconNames(String var1);

    public abstract void rename(String var1, String var2) throws IOException;

    public abstract void replace(String var1, IconSize var2, BufferedImage var3) throws IOException;

    public abstract Image loadImageAfterMapping(String var1, IconSize var2) throws IOException;

    public Image loadImage(String string, IconSize iconSize) throws IOException {
        String string2 = IconNameMapping.getNewIconName(string);
        if (string2 != null) {
            string = string2;
        }
        return this.loadImageAfterMapping(string, iconSize);
    }

    public abstract void addPropertyChangeListener(PropertyChangeListener var1);

    public abstract void removePropertyChangeListener(PropertyChangeListener var1);

    public void add(String string, String string2, Map<IconSize, Image> map) throws IOException {
        if (map.size() != IconSize.values().length) {
            throw new IllegalArgumentException("Not all sizes supplied for icon.");
        }
        this.add(string, string2, map.get((Object)IconSize.TINY), map.get((Object)IconSize.SMALL), map.get((Object)IconSize.MEDIUM), map.get((Object)IconSize.LARGE));
    }

    public Map<IconSize, Image> loadImages(String string) throws IOException {
        EnumMap<IconSize, Image> enumMap = new EnumMap<IconSize, Image>(IconSize.class);
        for (IconSize iconSize : IconSize.values()) {
            enumMap.put(iconSize, this.loadImage(string, iconSize));
        }
        return enumMap;
    }

    public String[] getSizeNames(String string) {
        String[] arrstring = new String[IconSize.values().length];
        int n = 0;
        for (IconSize iconSize : IconSize.values()) {
            arrstring[n++] = string + iconSize.getPostfix();
        }
        return arrstring;
    }

    public boolean isSame(String string, String string2) {
        return string.toLowerCase().equals(string2.toLowerCase());
    }

    public String getCategory(String string) {
        String string2 = IconNameMapping.getNewIconName(string);
        if (string2 != null) {
            string = string2;
        }
        for (String string3 : this.getCategories()) {
            Set<String> set = this.getIconNames(string3);
            for (String string4 : set) {
                if (!this.isSame(string4, string)) continue;
                return string3;
            }
        }
        return null;
    }

    public boolean exists(String string) {
        return this.getCategory(string) != null;
    }

    public boolean categoryExists(String string) {
        for (String string2 : this.getCategories()) {
            if (!this.isSame(string, string2)) continue;
            return true;
        }
        return false;
    }

    public Set<String> getAllIconNames() {
        HashSet<String> hashSet = new HashSet<String>();
        for (String string : this.getCategories()) {
            hashSet.addAll(this.getIconNames(string));
        }
        return hashSet;
    }

    public static class Composite
    extends IconRegistry {
        private IconRegistry[] _iconRegistries;
        private IconRegistry _writeDelegate;

        public /* varargs */ Composite(IconRegistry ... arriconRegistry) {
            this._iconRegistries = arriconRegistry;
            this._writeDelegate = this._iconRegistries[0];
        }

        public IconRegistry[] getRegistries() {
            return this._iconRegistries;
        }

        public void setWriteDelegate(IconRegistry iconRegistry) {
            this._writeDelegate = iconRegistry;
        }

        public IconRegistry getWriteDelegate() {
            return this._writeDelegate;
        }

        @Override
        public void add(String string, String string2, Image image, Image image2, Image image3, Image image4) throws IOException {
            this._writeDelegate.add(string, string2, image, image2, image3, image4);
        }

        @Override
        public void add(String string, String string2, FileObject fileObject, FileObject fileObject2, FileObject fileObject3, FileObject fileObject4) throws IOException {
            this._writeDelegate.add(string, string2, fileObject, fileObject2, fileObject3, fileObject4);
        }

        @Override
        public void remove(String string) throws IOException {
            this._writeDelegate.remove(string);
        }

        @Override
        public void removeCategory(String string) throws IOException {
            this._writeDelegate.removeCategory(string);
        }

        @Override
        public void rename(String string, String string2) throws IOException {
            this._writeDelegate.rename(string, string2);
        }

        @Override
        public void replace(String string, IconSize iconSize, BufferedImage bufferedImage) throws IOException {
            this._writeDelegate.replace(string, iconSize, bufferedImage);
        }

        @Override
        public Set<String> getCategories() {
            LinkedHashSet<String> linkedHashSet = new LinkedHashSet<String>();
            for (IconRegistry iconRegistry : this._iconRegistries) {
                linkedHashSet.addAll(iconRegistry.getCategories());
            }
            return linkedHashSet;
        }

        @Override
        public Set<String> getIconNames(String string) {
            HashSet<String> hashSet = new HashSet<String>();
            for (IconRegistry iconRegistry : this._iconRegistries) {
                Set<String> set = iconRegistry.getIconNames(string);
                if (set == null) continue;
                hashSet.addAll(set);
            }
            return hashSet;
        }

        @Override
        public Image loadImageAfterMapping(String string, IconSize iconSize) throws IOException {
            for (IconRegistry iconRegistry : this._iconRegistries) {
                Image image = iconRegistry.loadImage(string, iconSize);
                if (image == null) continue;
                return image;
            }
            return null;
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
            this._writeDelegate.addPropertyChangeListener(propertyChangeListener);
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
            this._writeDelegate.removePropertyChangeListener(propertyChangeListener);
        }
    }

    public static class Memory
    extends IconRegistry {
        private Map<String, Image> _icons;
        private IconCatalog _catalog;
        private PropertyChangeSupport _support;

        public Memory() {
            this._support = new PropertyChangeSupport(this);
            this._icons = new HashMap<String, Image>();
            this._catalog = new IconCatalog();
        }

        @Override
        public void add(String string, String string2, Image image, Image image2, Image image3, Image image4) throws IOException {
            this._icons.put(string2, image);
            this._icons.put(string2 + 24, image2);
            this._icons.put(string2 + 32, image3);
            this._icons.put(string2 + 48, image4);
            this._catalog.getOrCreateIconNames(string).add(string2);
            this._support.firePropertyChange("addedIcon", null, string2);
        }

        @Override
        public void add(String string, String string2, FileObject fileObject, FileObject fileObject2, FileObject fileObject3, FileObject fileObject4) throws IOException {
            this.add(string2, fileObject);
            this.add(string2 + 24, fileObject2);
            this.add(string2 + 32, fileObject3);
            this.add(string2 + 48, fileObject4);
            this._catalog.getOrCreateIconNames(string).add(string2);
            this._support.firePropertyChange("addedIcon", null, string2);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void add(String string, FileObject fileObject) throws IOException {
            BufferedImage bufferedImage = null;
            InputStream inputStream = fileObject.getInputStream();
            try {
                bufferedImage = ImageIO.read(inputStream);
            }
            finally {
                if (inputStream != null) {
                    inputStream.close();
                }
            }
            if (bufferedImage == null) {
                throw new IOException("Unable to load image: " + fileObject.getPath());
            }
            this._icons.put(string, bufferedImage);
        }

        @Override
        public void remove(String string) throws IOException {
            String[] arrstring;
            for (String string2 : arrstring = this.getSizeNames(string)) {
                this._icons.remove(string2);
            }
            this._catalog.removeIcon(string);
            this._support.firePropertyChange("removedIcon", string, null);
        }

        @Override
        public void removeCategory(String string) throws IOException {
            Set<String> set = this.getIconNames(string);
            if (set != null) {
                for (String string2 : set) {
                    for (IconSize iconSize : IconSize.values()) {
                        this._icons.remove(string2 + iconSize.getPostfix());
                    }
                }
            }
            this._catalog.removeCategory(string);
            this._support.firePropertyChange("removedCategory", string, null);
        }

        @Override
        public Set<String> getCategories() {
            return this._catalog.getCategories();
        }

        @Override
        public Set<String> getIconNames(String string) {
            return this._catalog.getIconNames(string);
        }

        @Override
        public void rename(String string, String string2) throws IOException {
            for (IconSize iconSize : IconSize.values()) {
                String string3 = string + iconSize.getPostfix();
                String string4 = string2 + iconSize.getPostfix();
                Image image = this._icons.remove(string3);
                if (image == null) continue;
                this._icons.put(string4, image);
            }
            Set<String> set = this.getIconNames(this.getCategory(string));
            set.remove(string);
            set.add(string2);
            this._support.firePropertyChange("renamedIcon", string, string2);
        }

        @Override
        public void replace(String string, IconSize iconSize, BufferedImage bufferedImage) throws IOException {
            if (bufferedImage.getWidth() != iconSize.getSize() || bufferedImage.getHeight() != iconSize.getSize()) {
                bufferedImage = ImageUtils.smartSize((BufferedImage)bufferedImage, (double)iconSize.getSize());
            }
            this._icons.put(string + iconSize.getPostfix(), bufferedImage);
        }

        @Override
        public Image loadImageAfterMapping(String string, IconSize iconSize) throws IOException {
            return this._icons.get(string + iconSize.getPostfix());
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
            this._support.addPropertyChangeListener(propertyChangeListener);
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
            this._support.removePropertyChangeListener(propertyChangeListener);
        }
    }

    public static abstract class ReadOnly
    extends IconRegistry {
        @Override
        public void add(String string, String string2, Image image, Image image2, Image image3, Image image4) throws IOException {
            this.readOnlyError();
        }

        @Override
        public void add(String string, String string2, FileObject fileObject, FileObject fileObject2, FileObject fileObject3, FileObject fileObject4) throws IOException {
            this.readOnlyError();
        }

        @Override
        public void remove(String string) throws IOException {
            this.readOnlyError();
        }

        @Override
        public void removeCategory(String string) throws IOException {
            this.readOnlyError();
        }

        @Override
        public void rename(String string, String string2) throws IOException {
            this.readOnlyError();
        }

        @Override
        public void replace(String string, IconSize iconSize, BufferedImage bufferedImage) throws IOException {
            this.readOnlyError();
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
            this.readOnlyError();
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
            this.readOnlyError();
        }

        private void readOnlyError() throws UnsupportedOperationException {
            throw new UnsupportedOperationException("Attempted to modify a read-only icon registry.");
        }
    }

}

