/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.imgfactoryapi;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class IconCatalog {
    private Map<String, Set<String>> _categories = new LinkedHashMap<String, Set<String>>();

    public Set<String> getCategories() {
        return this._categories.keySet();
    }

    public Set<String> getIconNames(String string) {
        for (Map.Entry<String, Set<String>> entry : this._categories.entrySet()) {
            if (!this.lowerEquals(entry.getKey(), string)) continue;
            return entry.getValue();
        }
        return null;
    }

    public Set<String> getOrCreateIconNames(String string) {
        Set<String> set = this.getIconNames(string);
        if (set == null) {
            set = new HashSet<String>();
            this._categories.put(string, set);
        }
        return set;
    }

    public String getActualIconName(String string) {
        for (Map.Entry<String, Set<String>> entry : this._categories.entrySet()) {
            for (String string2 : entry.getValue()) {
                if (!this.lowerEquals(string2, string)) continue;
                return string2;
            }
        }
        return null;
    }

    public void removeCategory(String string) {
        this._categories.remove(string);
    }

    public void removeIcon(String string) {
        for (Map.Entry<String, Set<String>> entry : this._categories.entrySet()) {
            entry.getValue().remove(string);
        }
    }

    public boolean lowerEquals(String string, String string2) {
        return string.toLowerCase().equals(string2.toLowerCase());
    }
}

