/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.feeder;

import com.paterva.maltego.feeder.DefaultFeederRegistry;
import com.paterva.maltego.feeder.Feeder;
import java.util.List;
import org.openide.util.Lookup;

public abstract class FeederRegistry {
    private static FeederRegistry _default;

    public static synchronized FeederRegistry getDefault() {
        if (_default == null && (FeederRegistry._default = (FeederRegistry)Lookup.getDefault().lookup(FeederRegistry.class)) == null) {
            _default = new DefaultFeederRegistry();
        }
        return _default;
    }

    public abstract List<Feeder> getFeeders();
}

