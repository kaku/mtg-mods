/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.feeder;

import com.paterva.maltego.feeder.Feeder;
import com.paterva.maltego.feeder.FeederRegistry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.openide.util.Lookup;

public class DefaultFeederRegistry
extends FeederRegistry {
    @Override
    public List<Feeder> getFeeders() {
        return new ArrayList<Feeder>(Lookup.getDefault().lookupAll(Feeder.class));
    }
}

