/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.GroupCollection;
import com.paterva.maltego.typing.collections.CompoundGroupCollection;

public class GroupCollections {
    private GroupCollections() {
    }

    public static /* varargs */ GroupCollection add(GroupCollection ... arrgroupCollection) {
        return new CompoundGroupCollection(arrgroupCollection);
    }
}

