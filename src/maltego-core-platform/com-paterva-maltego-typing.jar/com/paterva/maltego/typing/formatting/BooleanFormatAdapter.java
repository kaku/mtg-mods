/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.typing.formatting;

import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.FormatAdapter;
import com.paterva.maltego.typing.editors.BooleanFormat;
import com.paterva.maltego.util.StringUtilities;
import java.text.Format;

public class BooleanFormatAdapter
implements FormatAdapter {
    @Override
    public String getFormatString(Format format) {
        if (format == null) {
            return null;
        }
        BooleanFormat booleanFormat = (BooleanFormat)format;
        String[] arrstring = new String[]{booleanFormat.getTrueValue(), booleanFormat.getFalseValue()};
        return Converter.convertTo((Object)arrstring, String[].class);
    }

    @Override
    public Format getFormat(String string) {
        if (StringUtilities.isNullOrEmpty((String)string)) {
            return null;
        }
        String[] arrstring = (String[])Converter.convertFrom(string, String[].class);
        if (arrstring == null || arrstring.length != 2) {
            return null;
        }
        return new BooleanFormat(arrstring[0], arrstring[1]);
    }
}

