/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.typing.formatting;

import com.paterva.maltego.typing.FormatAdapter;
import com.paterva.maltego.util.StringUtilities;
import java.text.Format;
import java.text.SimpleDateFormat;

public class SimpleDateFormatAdapter
implements FormatAdapter {
    @Override
    public String getFormatString(Format format) {
        if (format == null) {
            return null;
        }
        SimpleDateFormat simpleDateFormat = (SimpleDateFormat)format;
        return simpleDateFormat.toPattern();
    }

    @Override
    public Format getFormat(String string) {
        if (StringUtilities.isNullOrEmpty((String)string)) {
            return null;
        }
        return new SimpleDateFormat(string);
    }
}

