/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  org.simpleframework.xml.transform.Matcher
 *  org.simpleframework.xml.transform.Transformer
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.StringConverter;
import com.paterva.maltego.typing.transforms.TransformMatcher;
import com.paterva.maltego.util.StringUtilities;
import org.simpleframework.xml.transform.Matcher;
import org.simpleframework.xml.transform.Transformer;

public class DefaultStringConverter
implements StringConverter {
    private Transformer _transformer;
    private static DefaultStringConverter _default;

    public DefaultStringConverter(Matcher matcher) {
        this._transformer = new Transformer(matcher);
    }

    public static DefaultStringConverter instance() {
        if (_default == null) {
            _default = new DefaultStringConverter(new TransformMatcher());
        }
        return _default;
    }

    @Override
    public Object convertFrom(String string, Class class_) throws IllegalArgumentException {
        if (String.class.equals((Object)class_)) {
            return string;
        }
        if (StringUtilities.isNullOrEmpty((String)string)) {
            return null;
        }
        try {
            return this._transformer.read(string, class_);
        }
        catch (Exception var3_3) {
            throw new IllegalArgumentException("Cannot convert value \"" + string + "\" to type \"" + class_.getSimpleName() + "\"", var3_3);
        }
    }

    @Override
    public String convertTo(Object object, Class class_) throws IllegalArgumentException {
        if (object == null) {
            return null;
        }
        try {
            return this._transformer.write(object, class_);
        }
        catch (Exception var3_3) {
            throw new IllegalArgumentException("Cannot convert value \"" + object + "\" of type \"" + class_.getSimpleName() + "\" to type \"string\"", var3_3);
        }
    }
}

