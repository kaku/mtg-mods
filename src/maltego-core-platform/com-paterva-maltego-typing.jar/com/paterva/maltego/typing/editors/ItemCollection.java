/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editors;

import com.paterva.maltego.typing.Converter;
import java.util.ArrayList;

public class ItemCollection {
    private Class _valueClass;
    private ArrayList<Pair> _pairs = new ArrayList();

    public ItemCollection(Class class_) {
        if (class_ == null) {
            throw new IllegalArgumentException("Value type cannot be null");
        }
        this._valueClass = class_;
    }

    public Class getType() {
        return this._valueClass;
    }

    public void add(String string, Object object) {
        this._pairs.add(new Pair(this, string, object));
    }

    public void add(Object object) {
        this._pairs.add(new Pair(this, object));
    }

    public Object getValue(Object object) {
        if (object instanceof Pair) {
            Pair pair = (Pair)object;
            return pair.getValue();
        }
        return object;
    }

    public Object getItem(Object object) {
        for (Pair pair : this._pairs) {
            if (pair == null || !(object == null ? pair.getValue() == null : object.equals(pair.getValue()))) continue;
            return pair;
        }
        return null;
    }

    public Object[] toArray() {
        return this._pairs.toArray(new Pair[this._pairs.size()]);
    }

    private class Pair {
        private String _name;
        private Object _value;
        final /* synthetic */ ItemCollection this$0;

        public Pair(ItemCollection itemCollection) {
            this.this$0 = itemCollection;
        }

        public Pair(ItemCollection itemCollection, String string, Object object) {
            this.this$0 = itemCollection;
            this._name = string;
            this._value = object;
        }

        public Pair(ItemCollection itemCollection, Object object) {
            this(itemCollection, null, object);
        }

        public String getName() {
            return this._name;
        }

        public void setName(String string) {
            this._name = string;
        }

        public Object getValue() {
            return this._value;
        }

        public void setValue(Object object) {
            this._value = object;
        }

        public String toString() {
            if (this._name == null) {
                if (this._value == null) {
                    return "(null)";
                }
                return Converter.convertTo(this._value, this.this$0._valueClass);
            }
            return this._name;
        }

        public int hashCode() {
            if (this._value == null) {
                return super.hashCode();
            }
            return this._value.hashCode();
        }

        public boolean equals(Object object) {
            if (object == null) {
                return false;
            }
            if (this.getClass() != object.getClass()) {
                return false;
            }
            Pair pair = (Pair)object;
            if (!(this._value == pair._value || this._value != null && this._value.equals(pair._value))) {
                return false;
            }
            return true;
        }
    }

}

