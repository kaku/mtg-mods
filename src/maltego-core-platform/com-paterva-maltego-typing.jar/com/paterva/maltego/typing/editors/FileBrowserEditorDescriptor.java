/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editors;

import com.paterva.maltego.typing.EditorDescriptor;

public class FileBrowserEditorDescriptor
extends EditorDescriptor {
    private boolean _selectDirectories;
    private boolean _selectFiles;
    private String _filterTitle;
    private String[] _extensions;

    public boolean isSelectDirectories() {
        return this._selectDirectories;
    }

    public void setSelectDirectories(boolean bl) {
        this._selectDirectories = bl;
    }

    public boolean isSelectFiles() {
        return this._selectFiles;
    }

    public void setSelectFiles(boolean bl) {
        this._selectFiles = bl;
    }

    public String getFilterTitle() {
        return this._filterTitle;
    }

    public void setFilterTitle(String string) {
        this._filterTitle = string;
    }

    public String[] getExtensions() {
        return this._extensions;
    }

    public void setExtensions(String[] arrstring) {
        this._extensions = arrstring;
    }
}

