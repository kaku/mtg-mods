/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editors;

import com.paterva.maltego.typing.FormattedConverter;
import java.text.Format;
import java.util.ArrayList;
import java.util.Iterator;

public class OptionItemCollection
implements Iterable<OptionItem> {
    private Class _valueClass;
    private Format _format;
    private ArrayList<OptionItem> _pairs = new ArrayList();

    public OptionItemCollection(Class class_, Format format) {
        if (class_ == null) {
            throw new IllegalArgumentException("Value type cannot be null");
        }
        this._valueClass = class_;
        this._format = format;
    }

    public Class getType() {
        return this._valueClass;
    }

    public void add(String string, Object object) {
        this._pairs.add(new OptionItem(this, string, object));
    }

    public void add(Object object) {
        this._pairs.add(new OptionItem(this, object));
    }

    public Object getValue(OptionItem optionItem) {
        return optionItem.getValue();
    }

    public OptionItem getItem(Object object) {
        for (OptionItem optionItem : this._pairs) {
            if (optionItem == null || !(object == null ? optionItem.getValue() == null : object.equals(optionItem.getValue()))) continue;
            return optionItem;
        }
        return null;
    }

    public OptionItem[] toArray() {
        return this._pairs.toArray(new OptionItem[this._pairs.size()]);
    }

    @Override
    public Iterator<OptionItem> iterator() {
        return this._pairs.iterator();
    }

    public Format getFormat() {
        return this._format;
    }

    public void setFormat(Format format) {
        this._format = format;
    }

    public class OptionItem {
        private String _name;
        private Object _value;
        final /* synthetic */ OptionItemCollection this$0;

        public OptionItem(OptionItemCollection optionItemCollection) {
            this.this$0 = optionItemCollection;
        }

        public OptionItem(OptionItemCollection optionItemCollection, String string, Object object) {
            this.this$0 = optionItemCollection;
            this._name = string;
            this._value = object;
        }

        public OptionItem(OptionItemCollection optionItemCollection, Object object) {
            this(optionItemCollection, null, object);
        }

        public String getName() {
            return this._name;
        }

        public void setName(String string) {
            this._name = string;
        }

        public Object getValue() {
            return this._value;
        }

        public void setValue(Object object) {
            this._value = object;
        }

        public String toString() {
            if (this._name == null) {
                if (this._value == null) {
                    return "(null)";
                }
                return FormattedConverter.convertTo(this._value, this.this$0._valueClass, this.this$0.getFormat());
            }
            return this._name;
        }

        public int hashCode() {
            if (this._value == null) {
                return super.hashCode();
            }
            return this._value.hashCode();
        }

        public boolean equals(Object object) {
            if (object == null) {
                return false;
            }
            if (this.getClass() != object.getClass()) {
                return false;
            }
            OptionItem optionItem = (OptionItem)object;
            if (!(this._value == optionItem._value || this._value != null && this._value.equals(optionItem._value))) {
                return false;
            }
            return true;
        }
    }

}

