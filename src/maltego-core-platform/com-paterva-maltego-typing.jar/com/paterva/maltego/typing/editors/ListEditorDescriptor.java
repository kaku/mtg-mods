/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editors;

import com.paterva.maltego.typing.EditorDescriptor;
import com.paterva.maltego.typing.editors.ItemCollection;

public class ListEditorDescriptor
extends EditorDescriptor {
    private Class _listType;
    private boolean _allowAdding;
    private ItemCollection _items;

    public ListEditorDescriptor(Class class_) {
        if (class_ == null) {
            throw new IllegalArgumentException("List type cannot be null");
        }
        this._listType = class_;
    }

    public boolean isAllowAdding() {
        return this._allowAdding;
    }

    public void setAllowAdding(boolean bl) {
        this._allowAdding = bl;
    }

    public ItemCollection getItems() {
        if (this._items == null) {
            this._items = new ItemCollection(this._listType);
        }
        return this._items;
    }
}

