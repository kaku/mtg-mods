/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editors;

import com.paterva.maltego.typing.EditorDescriptor;
import com.paterva.maltego.typing.editors.OptionItemCollection;
import java.text.Format;

public class OptionEditorDescriptor
extends EditorDescriptor {
    private Class _listType;
    private boolean _userSpecified;
    private OptionItemCollection _items;
    private Format _format;

    public OptionEditorDescriptor(Class class_) {
        this(class_, null);
    }

    public OptionEditorDescriptor(Class class_, Format format) {
        if (class_ == null) {
            throw new IllegalArgumentException("List type cannot be null");
        }
        this._listType = class_;
        this._format = format;
    }

    public boolean isUserSpecified() {
        return this._userSpecified;
    }

    public void setUserSpecified(boolean bl) {
        this._userSpecified = bl;
    }

    public OptionItemCollection getItems() {
        if (this._items == null) {
            this._items = new OptionItemCollection(this._listType, this._format);
        }
        return this._items;
    }
}

