/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editors;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;

public class BooleanFormat
extends Format {
    private String _trueValue;
    private String _falseValue;

    public BooleanFormat(String string, String string2) {
        this._trueValue = string;
        this._falseValue = string2;
    }

    @Override
    public StringBuffer format(Object object, StringBuffer stringBuffer, FieldPosition fieldPosition) {
        if (object == null) {
            return stringBuffer;
        }
        Boolean bl = (Boolean)object;
        String string = bl != false ? this.getTrueValue() : this.getFalseValue();
        stringBuffer.insert(fieldPosition.getBeginIndex(), string);
        return stringBuffer;
    }

    @Override
    public Object parseObject(String string, ParsePosition parsePosition) {
        if (string == null) {
            return null;
        }
        return string.indexOf(this.getTrueValue(), parsePosition.getIndex()) == parsePosition.getIndex();
    }

    public String getTrueValue() {
        return this._trueValue;
    }

    public void setTrueValue(String string) {
        this._trueValue = string;
    }

    public String getFalseValue() {
        return this._falseValue;
    }

    public void setFalseValue(String string) {
        this._falseValue = string;
    }
}

