/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.typing.serializer;

import com.paterva.maltego.typing.serializer.EditorStub;
import com.paterva.maltego.typing.serializer.OptionItemStub;
import java.util.ArrayList;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="OptionEditor", strict=0)
class OptionEditorStub
extends EditorStub {
    @Attribute(name="userSpecified", required=0)
    private boolean _userSpecified = false;
    @ElementList(inline=1, type=OptionItemStub.class)
    private List<OptionItemStub> _listItems;

    OptionEditorStub() {
    }

    public boolean isUserSpecified() {
        return this._userSpecified;
    }

    public void setUserSpecified(boolean bl) {
        this._userSpecified = bl;
    }

    public List<OptionItemStub> getOptionItems() {
        if (this._listItems == null) {
            this._listItems = new ArrayList<OptionItemStub>();
        }
        return this._listItems;
    }
}

