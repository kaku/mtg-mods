/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.serializer;

import com.paterva.maltego.typing.Evaluator;

public class EvaluatorFactory {
    private static Evaluator.Replacement _replacement;

    public static Evaluator create(String string) {
        if ("maltego.replace".equals(string)) {
            return EvaluatorFactory.getReplacementEvaluator();
        }
        return null;
    }

    public static Evaluator createForDefaultValue(String string) {
        if (Evaluator.Replacement.isSuitableFor(string)) {
            return EvaluatorFactory.getReplacementEvaluator();
        }
        return null;
    }

    private static synchronized Evaluator getReplacementEvaluator() {
        if (_replacement == null) {
            _replacement = new Evaluator.Replacement();
        }
        return _replacement;
    }
}

