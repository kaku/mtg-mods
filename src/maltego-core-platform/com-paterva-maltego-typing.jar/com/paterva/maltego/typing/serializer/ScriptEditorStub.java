/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.typing.serializer;

import com.paterva.maltego.typing.serializer.EditorStub;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name="ScriptEditor", strict=0)
class ScriptEditorStub
extends EditorStub {
    @Attribute(name="language", required=0)
    private String _language;

    ScriptEditorStub() {
    }

    public String getLanguage() {
        return this._language;
    }

    public void setLanguage(String string) {
        this._language = string;
    }
}

