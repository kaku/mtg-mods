/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.typing.serializer;

import com.paterva.maltego.typing.serializer.EditorStub;
import org.simpleframework.xml.Root;

@Root(name="PasswordEditor", strict=0)
class PasswordEditorStub
extends EditorStub {
    PasswordEditorStub() {
    }
}

