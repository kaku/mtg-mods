/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.serializer;

import com.paterva.maltego.typing.Group;
import com.paterva.maltego.typing.GroupCollection;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.SuperGroup;
import com.paterva.maltego.typing.serializer.FieldGroupStub;
import com.paterva.maltego.typing.serializer.FieldGroupsStub;
import com.paterva.maltego.typing.serializer.MainGroupStub;
import com.paterva.maltego.typing.serializer.SubGroupStub;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class GroupsTranslator {
    GroupsTranslator() {
    }

    public GroupDefinitions translate(FieldGroupsStub fieldGroupsStub) {
        Object object;
        Object object2;
        if (fieldGroupsStub == null) {
            return null;
        }
        GroupDefinitions groupDefinitions = new GroupDefinitions();
        List<MainGroupStub> list = fieldGroupsStub.getMainGroups();
        if (list != null) {
            object = list.iterator();
            while (object.hasNext()) {
                object2 = (MainGroupStub)object.next();
                groupDefinitions.getTopLevelSuperGroups().add(this.translate((MainGroupStub)object2));
            }
        }
        if ((object = fieldGroupsStub.getSubGroups()) != null) {
            object2 = object.iterator();
            while (object2.hasNext()) {
                SubGroupStub subGroupStub = (SubGroupStub)object2.next();
                groupDefinitions.getTopLevelGroups().add(this.translate(subGroupStub));
            }
        }
        return groupDefinitions;
    }

    private Group translate(SubGroupStub subGroupStub) {
        Group group = new Group(subGroupStub.getName(), subGroupStub.getDisplayName(), subGroupStub.getDescription());
        return group;
    }

    private SuperGroup translate(MainGroupStub mainGroupStub) {
        SuperGroup superGroup = new SuperGroup(mainGroupStub.getName(), mainGroupStub.getDisplayName(), mainGroupStub.getDescription());
        for (SubGroupStub subGroupStub : mainGroupStub.getSubGroups()) {
            superGroup.getSubGroups().add(this.translate(subGroupStub));
        }
        return superGroup;
    }

    public FieldGroupsStub translate(GroupDefinitions groupDefinitions) {
        if (groupDefinitions == null) {
            return null;
        }
        FieldGroupsStub fieldGroupsStub = new FieldGroupsStub();
        if (groupDefinitions.getTopLevelGroups() != null) {
            for (Group group : groupDefinitions.getTopLevelGroups()) {
                fieldGroupsStub.getSubGroups().add(this.translate(group));
            }
        }
        if (groupDefinitions.getTopLevelSuperGroups() != null) {
            for (Group group : groupDefinitions.getTopLevelSuperGroups()) {
                fieldGroupsStub.getMainGroups().add(this.translate((SuperGroup)group));
            }
        }
        return fieldGroupsStub;
    }

    private SubGroupStub translate(Group group) {
        SubGroupStub subGroupStub = new SubGroupStub();
        this.update(subGroupStub, group);
        return subGroupStub;
    }

    private void update(FieldGroupStub fieldGroupStub, Group group) {
        fieldGroupStub.setName(group.getName());
        if (!group.getName().equals(group.getDisplayName())) {
            fieldGroupStub.setDisplayName(group.getDisplayName());
        }
        if (!group.getDisplayName().equals(group.getDescription())) {
            fieldGroupStub.setDescription(group.getDescription());
        }
    }

    private MainGroupStub translate(SuperGroup superGroup) {
        MainGroupStub mainGroupStub = new MainGroupStub();
        this.update(mainGroupStub, superGroup);
        if (superGroup.getSubGroups() != null) {
            for (Group group : superGroup.getSubGroups()) {
                mainGroupStub.getSubGroups().add(this.translate(group));
            }
        }
        return mainGroupStub;
    }
}

