/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.typing.serializer;

import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.EditorDescriptor;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.editors.FileBrowserEditorDescriptor;
import com.paterva.maltego.typing.editors.OptionEditorDescriptor;
import com.paterva.maltego.typing.editors.OptionItemCollection;
import com.paterva.maltego.typing.editors.PasswordEditorDescriptor;
import com.paterva.maltego.typing.serializer.EditorStub;
import com.paterva.maltego.typing.serializer.FileBrowserEditorStub;
import com.paterva.maltego.typing.serializer.OptionEditorStub;
import com.paterva.maltego.typing.serializer.OptionItemStub;
import com.paterva.maltego.typing.serializer.PasswordEditorStub;
import com.paterva.maltego.typing.serializer.UnresolvedReferenceException;
import com.paterva.maltego.util.StringUtilities;
import java.text.Format;
import java.util.List;

class EditorTranslator {
    EditorTranslator() {
    }

    public EditorDescriptor translate(TypeDescriptor typeDescriptor, EditorStub editorStub, DisplayDescriptor displayDescriptor) throws UnresolvedReferenceException {
        if (editorStub == null) {
            return null;
        }
        if (editorStub instanceof OptionEditorStub) {
            return this.translate(typeDescriptor, (OptionEditorStub)editorStub, displayDescriptor);
        }
        if (editorStub instanceof PasswordEditorStub) {
            return this.translate((PasswordEditorStub)editorStub);
        }
        if (editorStub instanceof FileBrowserEditorStub) {
            return this.translate((FileBrowserEditorStub)editorStub);
        }
        throw new UnresolvedReferenceException("Cannot resolve editor type: " + editorStub);
    }

    private OptionEditorDescriptor translate(TypeDescriptor typeDescriptor, OptionEditorStub optionEditorStub, DisplayDescriptor displayDescriptor) {
        Class class_ = typeDescriptor.getComponentType();
        OptionEditorDescriptor optionEditorDescriptor = new OptionEditorDescriptor(class_, displayDescriptor.getFormat());
        optionEditorDescriptor.setUserSpecified(optionEditorStub.isUserSpecified());
        if (optionEditorStub.getOptionItems() != null) {
            for (OptionItemStub optionItemStub : optionEditorStub.getOptionItems()) {
                Object object;
                String string = optionItemStub.getName();
                String string2 = optionItemStub.getText();
                if (StringUtilities.isNullOrEmpty((String)string2) && !String.class.equals((Object)class_)) {
                    object = null;
                } else {
                    TypeDescriptor typeDescriptor2 = TypeRegistry.getDefault().getType(class_);
                    object = typeDescriptor2.convert(string2);
                }
                optionEditorDescriptor.getItems().add(string, object);
            }
        }
        return optionEditorDescriptor;
    }

    public EditorStub translate(EditorDescriptor editorDescriptor) {
        if (editorDescriptor instanceof OptionEditorDescriptor) {
            return this.translate((OptionEditorDescriptor)editorDescriptor);
        }
        if (editorDescriptor instanceof FileBrowserEditorDescriptor) {
            return this.translate((FileBrowserEditorDescriptor)editorDescriptor);
        }
        if (editorDescriptor instanceof PasswordEditorDescriptor) {
            return this.translate((PasswordEditorDescriptor)editorDescriptor);
        }
        return null;
    }

    private OptionEditorStub translate(OptionEditorDescriptor optionEditorDescriptor) {
        OptionEditorStub optionEditorStub = new OptionEditorStub();
        optionEditorStub.setUserSpecified(optionEditorDescriptor.isUserSpecified());
        if (optionEditorDescriptor.getItems() != null) {
            for (OptionItemCollection.OptionItem optionItem : optionEditorDescriptor.getItems()) {
                String string = optionItem.getName();
                Object object = optionItem.getValue();
                String string2 = object == null ? "" : Converter.convertTo(object, optionEditorDescriptor.getItems().getType());
                OptionItemStub optionItemStub = new OptionItemStub();
                optionItemStub.setName(string);
                optionItemStub.setText(string2);
                optionEditorStub.getOptionItems().add(optionItemStub);
            }
        }
        return optionEditorStub;
    }

    private PasswordEditorDescriptor translate(PasswordEditorStub passwordEditorStub) {
        return new PasswordEditorDescriptor();
    }

    private PasswordEditorStub translate(PasswordEditorDescriptor passwordEditorDescriptor) {
        return new PasswordEditorStub();
    }

    private FileBrowserEditorDescriptor translate(FileBrowserEditorStub fileBrowserEditorStub) {
        FileBrowserEditorDescriptor fileBrowserEditorDescriptor = new FileBrowserEditorDescriptor();
        fileBrowserEditorDescriptor.setExtensions((String[])Converter.convertFrom(fileBrowserEditorStub.getFileNameFilter(), String[].class));
        fileBrowserEditorDescriptor.setFilterTitle(fileBrowserEditorStub.getFilterDisplayName());
        fileBrowserEditorDescriptor.setSelectDirectories(fileBrowserEditorStub.isSelectDirectory());
        fileBrowserEditorDescriptor.setSelectFiles(fileBrowserEditorStub.isSelectFile());
        return fileBrowserEditorDescriptor;
    }

    private FileBrowserEditorStub translate(FileBrowserEditorDescriptor fileBrowserEditorDescriptor) {
        FileBrowserEditorStub fileBrowserEditorStub = new FileBrowserEditorStub();
        if (fileBrowserEditorDescriptor.getExtensions() != null && fileBrowserEditorDescriptor.getExtensions().length > 0) {
            fileBrowserEditorStub.setFileNameFilter(Converter.convertTo((Object)fileBrowserEditorDescriptor.getExtensions(), String[].class));
        }
        fileBrowserEditorStub.setFilterDisplayName(fileBrowserEditorDescriptor.getFilterTitle());
        fileBrowserEditorStub.setSelectDirectory(fileBrowserEditorDescriptor.isSelectDirectories());
        fileBrowserEditorStub.setSelectFile(fileBrowserEditorDescriptor.isSelectFiles());
        return fileBrowserEditorStub;
    }
}

