/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.XmlSerializationException
 */
package com.paterva.maltego.typing.serializer;

import com.paterva.maltego.util.XmlSerializationException;

public class UnresolvedReferenceException
extends XmlSerializationException {
    public UnresolvedReferenceException() {
    }

    public UnresolvedReferenceException(String string) {
        super(string);
    }
}

