/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.serializer;

import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.DisplayDescriptorList;
import com.paterva.maltego.typing.EditorDescriptor;
import com.paterva.maltego.typing.Evaluator;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.ValidationDescriptor;
import com.paterva.maltego.typing.serializer.DisplayDescriptorStub;
import com.paterva.maltego.typing.serializer.EditorStub;
import com.paterva.maltego.typing.serializer.EditorTranslator;
import com.paterva.maltego.typing.serializer.EvaluatorFactory;
import com.paterva.maltego.typing.serializer.FieldStub;
import com.paterva.maltego.typing.serializer.RangeValidatorStub;
import com.paterva.maltego.typing.serializer.UnresolvedReferenceException;
import com.paterva.maltego.typing.serializer.ValidatorStub;
import com.paterva.maltego.typing.serializer.ValidatorTranslator;
import java.text.Format;
import java.util.LinkedList;
import java.util.List;

public class DisplayDescriptorTranslator {
    private EditorTranslator _editorTranslator;
    private ValidatorTranslator _validatorTranslator;

    public List<FieldStub> translate(DisplayDescriptorEnumeration displayDescriptorEnumeration) throws UnresolvedReferenceException {
        LinkedList<FieldStub> linkedList = new LinkedList<FieldStub>();
        for (DisplayDescriptor displayDescriptor : displayDescriptorEnumeration) {
            FieldStub fieldStub = new FieldStub();
            linkedList.add(this.translate(displayDescriptor, fieldStub));
        }
        return linkedList;
    }

    public FieldStub translate(DisplayDescriptor displayDescriptor, FieldStub fieldStub) throws UnresolvedReferenceException {
        this.update(fieldStub, (PropertyDescriptor)displayDescriptor);
        this.update(fieldStub, displayDescriptor);
        return fieldStub;
    }

    private void update(FieldStub fieldStub, PropertyDescriptor propertyDescriptor) {
        fieldStub.setName(propertyDescriptor.getName());
        fieldStub.setDisplayName(propertyDescriptor.getDisplayName());
        if (!propertyDescriptor.getDisplayName().equals(propertyDescriptor.getDescription())) {
            fieldStub.setDescription(propertyDescriptor.getDescription());
        }
        fieldStub.setType(DisplayDescriptorTranslator.getType(propertyDescriptor.getType()).getTypeName());
        fieldStub.setHidden(propertyDescriptor.isHidden());
        fieldStub.setNullable(propertyDescriptor.isNullable());
        fieldStub.setReadonly(propertyDescriptor.isReadonly());
    }

    private void update(FieldStub fieldStub, DisplayDescriptor displayDescriptor) throws UnresolvedReferenceException {
        fieldStub.setDisplayName(displayDescriptor.getDisplayName());
        fieldStub.setEvaluator(displayDescriptor.getEvaluator() == null ? null : displayDescriptor.getEvaluator().getName().toString());
        fieldStub.getDisplayDescriptor().setGroup(displayDescriptor.getGroupName());
        fieldStub.getDisplayDescriptor().setFormat(DisplayDescriptorTranslator.getType(displayDescriptor.getType()).getFormatString(displayDescriptor.getFormat()));
        if (displayDescriptor.getDefaultValue() != null && !displayDescriptor.getDefaultValue().equals(displayDescriptor.getTypeDescriptor().getDefaultValue())) {
            fieldStub.setDefaultValue(Converter.convertTo(displayDescriptor.getDefaultValue(), displayDescriptor.getType()));
        }
        fieldStub.setSampleValue(Converter.convertTo(displayDescriptor.getSampleValue(), displayDescriptor.getType()));
        fieldStub.setValidator(this.validatorTranslator().translate(displayDescriptor.getValidator()));
        fieldStub.getDisplayDescriptor().setEditor(this.editorTranslator().translate(displayDescriptor.getEditor()));
    }

    private static TypeDescriptor getType(Class class_) {
        return TypeRegistry.getDefault().getType(class_);
    }

    public DisplayDescriptorCollection translate(List<FieldStub> list) throws UnresolvedReferenceException {
        DisplayDescriptorList displayDescriptorList = new DisplayDescriptorList();
        if (list != null) {
            for (FieldStub fieldStub : list) {
                displayDescriptorList.add(this.translate(fieldStub));
            }
        }
        return displayDescriptorList;
    }

    public DisplayDescriptor translate(FieldStub fieldStub) throws UnresolvedReferenceException {
        String string = fieldStub.getType();
        TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(string);
        if (typeDescriptor == null) {
            throw new UnresolvedReferenceException("The data type " + string + " is not known.");
        }
        DisplayDescriptor displayDescriptor = new DisplayDescriptor(typeDescriptor.getType(), fieldStub.getName(), fieldStub.getDisplayName());
        if (fieldStub.getDefaultValue() == null) {
            displayDescriptor.setDefaultValue(typeDescriptor.getDefaultValue());
        } else {
            displayDescriptor.setDefaultValue(typeDescriptor.convert(fieldStub.getDefaultValue()));
        }
        displayDescriptor.setSampleValue(typeDescriptor.convert(fieldStub.getSampleValue()));
        displayDescriptor.setHidden(fieldStub.isHidden());
        displayDescriptor.setNullable(fieldStub.isNullable());
        displayDescriptor.setReadonly(fieldStub.isReadonly());
        displayDescriptor.setEvaluator(EvaluatorFactory.create(fieldStub.getEvaluator()));
        displayDescriptor.setDescription(fieldStub.getDescription());
        if (fieldStub.getRangeValidator() != null) {
            displayDescriptor.setValidator(this.validatorTranslator().translate(typeDescriptor, (ValidatorStub)fieldStub.getRangeValidator()));
        }
        if (fieldStub.getDisplayDescriptor() != null) {
            DisplayDescriptorStub displayDescriptorStub = fieldStub.getDisplayDescriptor();
            displayDescriptor.setFormat(typeDescriptor.getFormat(displayDescriptorStub.getFormat()));
            displayDescriptor.setSortOrder(displayDescriptorStub.getSortIndex());
            displayDescriptor.setGroupName(displayDescriptorStub.getGroup());
            if (displayDescriptorStub.getEditor() != null) {
                displayDescriptor.setEditor(this.editorTranslator().translate(typeDescriptor, displayDescriptorStub.getEditor(), displayDescriptor));
            }
        }
        return displayDescriptor;
    }

    private EditorTranslator editorTranslator() {
        if (this._editorTranslator == null) {
            this._editorTranslator = new EditorTranslator();
        }
        return this._editorTranslator;
    }

    private ValidatorTranslator validatorTranslator() {
        if (this._validatorTranslator == null) {
            this._validatorTranslator = new ValidatorTranslator();
        }
        return this._validatorTranslator;
    }
}

