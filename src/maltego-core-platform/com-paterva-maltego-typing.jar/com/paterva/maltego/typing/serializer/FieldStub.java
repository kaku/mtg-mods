/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.typing.serializer;

import com.paterva.maltego.typing.serializer.DisplayDescriptorStub;
import com.paterva.maltego.typing.serializer.RangeValidatorStub;
import com.paterva.maltego.typing.serializer.ValidatorStub;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="Field", strict=0)
public class FieldStub {
    @Attribute(name="name")
    private String _name;
    private String _displayName;
    @Attribute(name="type")
    private String _type;
    @Attribute(name="nullable", required=0)
    private boolean _nullable = true;
    @Attribute(name="hidden", required=0)
    private boolean _hidden = false;
    @Attribute(name="readonly", required=0)
    private boolean _readonly = false;
    @Attribute(name="description", required=0)
    private String _description;
    @Attribute(name="evaluator", required=0)
    private String _evaluator;
    private ValidatorStub _validator;
    private DisplayDescriptorStub _displayDescriptor;
    @Element(name="DefaultValue", required=0)
    private String _defaultValue;
    @Element(name="SampleValue", required=0)
    private String _sampleValue;

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    @Attribute(name="displayName", required=0)
    public String getDisplayName() {
        if (this.getName().equals(this._displayName)) {
            return null;
        }
        return this._displayName;
    }

    @Attribute(name="displayName", required=0)
    public void setDisplayName(String string) {
        this._displayName = string;
    }

    public String getType() {
        return this._type;
    }

    public void setType(String string) {
        this._type = string;
    }

    public boolean isNullable() {
        return this._nullable;
    }

    public void setNullable(boolean bl) {
        this._nullable = bl;
    }

    public boolean isHidden() {
        return this._hidden;
    }

    public void setHidden(boolean bl) {
        this._hidden = bl;
    }

    public String getDescription() {
        return this._description;
    }

    public void setDescription(String string) {
        this._description = string;
    }

    public boolean isReadonly() {
        return this._readonly;
    }

    public void setReadonly(boolean bl) {
        this._readonly = bl;
    }

    @Element(name="RangeValidator", required=0)
    public RangeValidatorStub getRangeValidator() {
        if (this.getValidator() instanceof RangeValidatorStub) {
            return (RangeValidatorStub)this.getValidator();
        }
        return null;
    }

    @Element(name="RangeValidator", required=0)
    public void setRangeValidator(RangeValidatorStub rangeValidatorStub) {
        this._validator = rangeValidatorStub;
    }

    public ValidatorStub getValidator() {
        return this._validator;
    }

    public void setValidator(ValidatorStub validatorStub) {
        this._validator = validatorStub;
    }

    public DisplayDescriptorStub getDisplayDescriptor() {
        if (this._displayDescriptor == null) {
            this._displayDescriptor = new DisplayDescriptorStub();
        }
        return this._displayDescriptor;
    }

    @Element(name="DisplayDescriptor", required=0)
    public DisplayDescriptorStub getDisplayDescriptorInternal() {
        if (this._displayDescriptor != null && this._displayDescriptor.isDefault()) {
            return null;
        }
        return this._displayDescriptor;
    }

    @Element(name="DisplayDescriptor", required=0)
    public void setDisplayDescriptorInternal(DisplayDescriptorStub displayDescriptorStub) {
        this._displayDescriptor = displayDescriptorStub;
    }

    public String getDefaultValue() {
        return this._defaultValue;
    }

    public void setDefaultValue(String string) {
        this._defaultValue = string;
    }

    public String getSampleValue() {
        return this._sampleValue;
    }

    public void setSampleValue(String string) {
        this._sampleValue = string;
    }

    public String getEvaluator() {
        return this._evaluator;
    }

    public void setEvaluator(String string) {
        this._evaluator = string;
    }
}

