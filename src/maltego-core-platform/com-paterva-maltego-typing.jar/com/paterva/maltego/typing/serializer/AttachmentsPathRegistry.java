/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.serializer;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AttachmentsPathRegistry {
    private static Map<Integer, String> _paths = Collections.EMPTY_MAP;
    private static Lock _lock = new ReentrantLock();

    public static Map<Integer, String> getPaths() {
        return Collections.unmodifiableMap(_paths);
    }

    public static void setPaths(Map<Integer, String> map) {
        _paths = map;
    }

    public static void lock() {
        _lock.lock();
    }

    public static void unlock() {
        _lock.unlock();
    }
}

