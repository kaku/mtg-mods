/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.XmlSerializationException
 *  com.paterva.maltego.util.XmlSerializer
 */
package com.paterva.maltego.typing.serializer;

import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.PropertyConfiguration;
import com.paterva.maltego.typing.serializer.DisplayDescriptorTranslator;
import com.paterva.maltego.typing.serializer.FieldGroupsStub;
import com.paterva.maltego.typing.serializer.FieldStub;
import com.paterva.maltego.typing.serializer.GroupsTranslator;
import com.paterva.maltego.typing.serializer.PropertiesStub;
import com.paterva.maltego.util.XmlSerializationException;
import com.paterva.maltego.util.XmlSerializer;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class FieldsSerializer {
    public PropertiesStub createSerializationStub(PropertyConfiguration propertyConfiguration) throws XmlSerializationException {
        DisplayDescriptorTranslator displayDescriptorTranslator = new DisplayDescriptorTranslator();
        GroupsTranslator groupsTranslator = new GroupsTranslator();
        List<FieldStub> list = displayDescriptorTranslator.translate(propertyConfiguration.getProperties());
        FieldGroupsStub fieldGroupsStub = groupsTranslator.translate(propertyConfiguration.getGroups());
        PropertiesStub propertiesStub = new PropertiesStub(list, fieldGroupsStub);
        propertiesStub.setDisplayValueProperty(FieldsSerializer.getSpecialProperty(propertyConfiguration, "displayValue"));
        propertiesStub.setImageProperty(FieldsSerializer.getSpecialProperty(propertyConfiguration, "image"));
        propertiesStub.setValueProperty(FieldsSerializer.getSpecialProperty(propertyConfiguration, "value"));
        return propertiesStub;
    }

    public void write(OutputStream outputStream, PropertyConfiguration propertyConfiguration) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        xmlSerializer.write((Object)this.createSerializationStub(propertyConfiguration), outputStream);
    }

    private static String getSpecialProperty(PropertyConfiguration propertyConfiguration, String string) {
        DisplayDescriptor displayDescriptor = propertyConfiguration.getSpecialProperty(string);
        if (displayDescriptor == null) {
            return null;
        }
        return displayDescriptor.getName();
    }

    public PropertyConfiguration readSerializationStub(PropertiesStub propertiesStub) throws XmlSerializationException {
        if (propertiesStub == null) {
            return null;
        }
        GroupsTranslator groupsTranslator = new GroupsTranslator();
        DisplayDescriptorTranslator displayDescriptorTranslator = new DisplayDescriptorTranslator();
        PropertyConfiguration propertyConfiguration = new PropertyConfiguration(displayDescriptorTranslator.translate(propertiesStub.getFields()), groupsTranslator.translate(propertiesStub.getGroups()));
        FieldsSerializer.addSpecialProperty(propertyConfiguration, "image", propertiesStub.getImageProperty());
        FieldsSerializer.addSpecialProperty(propertyConfiguration, "value", propertiesStub.getValueProperty());
        FieldsSerializer.addSpecialProperty(propertyConfiguration, "displayValue", propertiesStub.getDisplayValueProperty());
        return propertyConfiguration;
    }

    public PropertyConfiguration read(InputStream inputStream) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        PropertiesStub propertiesStub = (PropertiesStub)xmlSerializer.read(PropertiesStub.class, inputStream);
        return this.readSerializationStub(propertiesStub);
    }

    private static void addSpecialProperty(PropertyConfiguration propertyConfiguration, String string, String string2) {
        DisplayDescriptor displayDescriptor = propertyConfiguration.getProperties().get(string2);
        if (displayDescriptor != null) {
            propertyConfiguration.setSpecialProperty(string, displayDescriptor);
        }
    }
}

