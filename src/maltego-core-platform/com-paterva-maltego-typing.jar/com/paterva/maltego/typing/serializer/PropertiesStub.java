/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.typing.serializer;

import com.paterva.maltego.typing.serializer.FieldGroupsStub;
import com.paterva.maltego.typing.serializer.FieldStub;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="Properties", strict=0)
public class PropertiesStub {
    @Attribute(name="image", required=0)
    private String _imageProperty;
    @Attribute(name="value", required=0)
    private String _valueProperty;
    @Attribute(name="displayValue", required=0)
    private String _displayValueProperty;
    @Element(name="Groups", required=0)
    private FieldGroupsStub _groups;
    @ElementList(name="Fields", type=FieldStub.class, required=0)
    private List<FieldStub> _fields;

    public PropertiesStub() {
    }

    public PropertiesStub(List<FieldStub> list, FieldGroupsStub fieldGroupsStub) {
        this._groups = fieldGroupsStub;
        this._fields = list;
    }

    public FieldGroupsStub getGroups() {
        return this._groups;
    }

    public void setGroups(FieldGroupsStub fieldGroupsStub) {
        this._groups = fieldGroupsStub;
    }

    public List<FieldStub> getFields() {
        return this._fields;
    }

    public void setFields(List<FieldStub> list) {
        this._fields = list;
    }

    public String getImageProperty() {
        return this._imageProperty;
    }

    public void setImageProperty(String string) {
        this._imageProperty = string;
    }

    public String getValueProperty() {
        return this._valueProperty;
    }

    public void setValueProperty(String string) {
        this._valueProperty = string;
    }

    public String getDisplayValueProperty() {
        return this._displayValueProperty;
    }

    public void setDisplayValueProperty(String string) {
        this._displayValueProperty = string;
    }
}

