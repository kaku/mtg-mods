/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.serializer;

import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.ValidationDescriptor;
import com.paterva.maltego.typing.serializer.RangeValidatorStub;
import com.paterva.maltego.typing.serializer.UnresolvedReferenceException;
import com.paterva.maltego.typing.serializer.ValidatorStub;

class ValidatorTranslator {
    ValidatorTranslator() {
    }

    public ValidationDescriptor translate(TypeDescriptor typeDescriptor, ValidatorStub validatorStub) throws UnresolvedReferenceException {
        if (validatorStub == null) {
            return null;
        }
        if (validatorStub instanceof RangeValidatorStub) {
            return this.translate(typeDescriptor, (RangeValidatorStub)validatorStub);
        }
        throw new UnresolvedReferenceException("Cannot resolve validator type: " + validatorStub);
    }

    private ValidationDescriptor translate(TypeDescriptor typeDescriptor, RangeValidatorStub rangeValidatorStub) {
        return null;
    }

    public ValidatorStub translate(ValidationDescriptor validationDescriptor) {
        return null;
    }
}

