/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.typing.serializer;

import com.paterva.maltego.typing.serializer.DateEditorStub;
import com.paterva.maltego.typing.serializer.EditorStub;
import com.paterva.maltego.typing.serializer.FileBrowserEditorStub;
import com.paterva.maltego.typing.serializer.OptionEditorStub;
import com.paterva.maltego.typing.serializer.PasswordEditorStub;
import com.paterva.maltego.typing.serializer.ScriptEditorStub;
import com.paterva.maltego.util.StringUtilities;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="DisplayDescriptor", strict=0)
class DisplayDescriptorStub {
    @Attribute(name="index", required=0)
    private int _index;
    @Attribute(name="group", required=0)
    private String _group;
    @Attribute(name="format", required=0)
    private String _format;
    private EditorStub _editor;

    DisplayDescriptorStub() {
    }

    public int getSortIndex() {
        return this._index;
    }

    public void setSortIndex(int n) {
        this._index = n;
    }

    public String getGroup() {
        return this._group;
    }

    public void setGroup(String string) {
        this._group = string;
    }

    @Element(name="OptionEditor", required=0)
    public OptionEditorStub getListEditor() {
        if (this.getEditor() instanceof OptionEditorStub) {
            return (OptionEditorStub)this.getEditor();
        }
        return null;
    }

    @Element(name="OptionEditor", required=0)
    public void setListEditor(OptionEditorStub optionEditorStub) {
        this._editor = optionEditorStub;
    }

    @Element(name="DateEditor", required=0)
    public DateEditorStub getDateEditor() {
        if (this.getEditor() instanceof DateEditorStub) {
            return (DateEditorStub)this.getEditor();
        }
        return null;
    }

    @Element(name="DateEditor", required=0)
    public void setDateEditor(DateEditorStub dateEditorStub) {
        this._editor = dateEditorStub;
    }

    @Element(name="ScriptEditor", required=0)
    public ScriptEditorStub getScriptEditor() {
        if (this.getEditor() instanceof ScriptEditorStub) {
            return (ScriptEditorStub)this.getEditor();
        }
        return null;
    }

    @Element(name="ScriptEditor", required=0)
    public void setScriptEditor(ScriptEditorStub scriptEditorStub) {
        this._editor = scriptEditorStub;
    }

    @Element(name="FileBrowserEditor", required=0)
    public FileBrowserEditorStub getFileBrowserEditor() {
        if (this.getEditor() instanceof FileBrowserEditorStub) {
            return (FileBrowserEditorStub)this.getEditor();
        }
        return null;
    }

    @Element(name="FileBrowserEditor", required=0)
    public void setFileBrowserEditor(FileBrowserEditorStub fileBrowserEditorStub) {
        this._editor = fileBrowserEditorStub;
    }

    @Element(name="PasswordEditor", required=0)
    public PasswordEditorStub getStringEditor() {
        if (this.getEditor() instanceof PasswordEditorStub) {
            return (PasswordEditorStub)this.getEditor();
        }
        return null;
    }

    @Element(name="PasswordEditor", required=0)
    public void setStringEditor(PasswordEditorStub passwordEditorStub) {
        this._editor = passwordEditorStub;
    }

    public EditorStub getEditor() {
        return this._editor;
    }

    public void setEditor(EditorStub editorStub) {
        this._editor = editorStub;
    }

    boolean isDefault() {
        return this._index == 0 && StringUtilities.isNullOrEmpty((String)this._group) && this._editor == null;
    }

    public String getFormat() {
        return this._format;
    }

    public void setFormat(String string) {
        this._format = string;
    }
}

