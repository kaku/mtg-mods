/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.typing.serializer;

import com.paterva.maltego.typing.serializer.EditorStub;
import com.paterva.maltego.typing.serializer.ListItemStub;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="ListEditor", strict=0)
class ListEditorStub
extends EditorStub {
    @Attribute(name="allowAdd", required=0)
    private boolean _allowAdd;
    @ElementList(inline=1, type=ListItemStub.class)
    private List<ListItemStub> _listItems;

    ListEditorStub() {
    }

    public boolean isAllowAdd() {
        return this._allowAdd;
    }

    public void setAllowAdd(boolean bl) {
        this._allowAdd = bl;
    }

    public List<ListItemStub> getListItems() {
        return this._listItems;
    }

    public void setListItems(List<ListItemStub> list) {
        this._listItems = list;
    }
}

