/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.typing.serializer;

import com.paterva.maltego.typing.serializer.ValidatorStub;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name="RangeValidator", strict=0)
class RangeValidatorStub
extends ValidatorStub {
    @Attribute(name="min", required=0)
    private String _min;
    @Attribute(name="max", required=0)
    private String _max;

    RangeValidatorStub() {
    }

    public String getMin() {
        return this._min;
    }

    public void setMin(String string) {
        this._min = string;
    }

    public String getMax() {
        return this._max;
    }

    public void setMax(String string) {
        this._max = string;
    }
}

