/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.typing.serializer;

import com.paterva.maltego.typing.serializer.FieldGroupStub;
import com.paterva.maltego.typing.serializer.SubGroupStub;
import java.util.ArrayList;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="SuperGroup", strict=0)
public class MainGroupStub
extends FieldGroupStub {
    @ElementList(entry="Group", inline=1, type=SubGroupStub.class, required=0)
    private ArrayList<SubGroupStub> _subGroups;

    public ArrayList<SubGroupStub> getSubGroups() {
        if (this._subGroups == null) {
            this._subGroups = new ArrayList();
        }
        return this._subGroups;
    }

    public void setSubGroups(ArrayList<SubGroupStub> arrayList) {
        this._subGroups = arrayList;
    }
}

