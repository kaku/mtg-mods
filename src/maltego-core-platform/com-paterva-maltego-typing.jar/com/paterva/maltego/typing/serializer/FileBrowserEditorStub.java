/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.typing.serializer;

import com.paterva.maltego.typing.serializer.EditorStub;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name="FileBrowserEditor", strict=0)
class FileBrowserEditorStub
extends EditorStub {
    @Attribute(name="selectFile", required=0)
    private boolean _selectFile = true;
    @Attribute(name="selectDirectory", required=0)
    private boolean _selectDirectory = true;
    @Attribute(name="filter", required=0)
    private String _fileNameFilter;
    @Attribute(name="displayName", required=0)
    private String _filterDisplayName;

    FileBrowserEditorStub() {
    }

    public boolean isSelectFile() {
        return this._selectFile;
    }

    public void setSelectFile(boolean bl) {
        this._selectFile = bl;
    }

    public boolean isSelectDirectory() {
        return this._selectDirectory;
    }

    public void setSelectDirectory(boolean bl) {
        this._selectDirectory = bl;
    }

    public String getFileNameFilter() {
        return this._fileNameFilter;
    }

    public void setFileNameFilter(String string) {
        this._fileNameFilter = string;
    }

    public String getFilterDisplayName() {
        return this._filterDisplayName;
    }

    public void setFilterDisplayName(String string) {
        this._filterDisplayName = string;
    }
}

