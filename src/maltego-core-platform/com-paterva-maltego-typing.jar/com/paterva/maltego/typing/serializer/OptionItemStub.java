/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 *  org.simpleframework.xml.Text
 */
package com.paterva.maltego.typing.serializer;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name="OptionItem", strict=0)
class OptionItemStub {
    @Text(required=0)
    private String _text;
    @Attribute(name="name", required=0)
    private String _name;

    OptionItemStub() {
    }

    public String getText() {
        return this._text;
    }

    public void setText(String string) {
        this._text = string;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }
}

