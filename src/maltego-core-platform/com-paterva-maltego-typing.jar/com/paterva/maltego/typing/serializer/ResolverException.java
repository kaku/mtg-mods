/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.XmlSerializationException
 */
package com.paterva.maltego.typing.serializer;

import com.paterva.maltego.util.XmlSerializationException;

public class ResolverException
extends XmlSerializationException {
    public ResolverException() {
    }

    public ResolverException(String string) {
        super(string);
    }
}

