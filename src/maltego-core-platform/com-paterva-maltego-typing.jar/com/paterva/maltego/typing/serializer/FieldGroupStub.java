/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.typing.serializer;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name="Group", strict=0)
public class FieldGroupStub {
    @Attribute(name="name")
    private String _name;
    @Attribute(name="displayName", required=0)
    private String _displayName;
    @Attribute(name="description", required=0)
    private String _description;

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public String getDisplayName() {
        return this._displayName;
    }

    public void setDisplayName(String string) {
        this._displayName = string;
    }

    public String getDescription() {
        return this._description;
    }

    public void setDescription(String string) {
        this._description = string;
    }
}

