/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.typing.serializer;

import com.paterva.maltego.typing.serializer.EditorStub;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name="DateEditor", strict=0)
class DateEditorStub
extends EditorStub {
    @Attribute(name="format", required=0)
    private String _format;

    DateEditorStub() {
    }

    public String getFormat() {
        return this._format;
    }

    public void setFormat(String string) {
        this._format = string;
    }
}

