/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.typing.serializer;

import com.paterva.maltego.typing.serializer.MainGroupStub;
import com.paterva.maltego.typing.serializer.SubGroupStub;
import java.util.ArrayList;
import java.util.List;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="Groups", strict=0)
public class FieldGroupsStub {
    @ElementList(entry="SuperGroup", inline=1, type=MainGroupStub.class, required=0)
    private List<MainGroupStub> _mainGroups;
    @ElementList(entry="Group", inline=1, type=SubGroupStub.class, required=0)
    private List<SubGroupStub> _subGroups;

    public List<MainGroupStub> getMainGroups() {
        if (this._mainGroups == null) {
            this._mainGroups = new ArrayList<MainGroupStub>();
        }
        return this._mainGroups;
    }

    public List<SubGroupStub> getSubGroups() {
        if (this._subGroups == null) {
            this._subGroups = new ArrayList<SubGroupStub>();
        }
        return this._subGroups;
    }
}

