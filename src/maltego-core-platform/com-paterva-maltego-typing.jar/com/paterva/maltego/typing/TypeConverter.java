/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

public interface TypeConverter {
    public Object convert(String var1) throws IllegalArgumentException;

    public String convert(Object var1) throws IllegalArgumentException;
}

