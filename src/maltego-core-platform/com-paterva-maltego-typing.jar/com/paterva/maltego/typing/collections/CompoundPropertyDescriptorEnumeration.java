/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.collections.CompoundIterator
 */
package com.paterva.maltego.typing.collections;

import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorEnumeration;
import com.paterva.maltego.util.collections.CompoundIterator;
import java.util.Iterator;

public class CompoundPropertyDescriptorEnumeration
implements PropertyDescriptorEnumeration {
    protected PropertyDescriptorEnumeration[] _collections;

    public /* varargs */ CompoundPropertyDescriptorEnumeration(PropertyDescriptorEnumeration ... arrpropertyDescriptorEnumeration) {
        if (arrpropertyDescriptorEnumeration.length == 0) {
            throw new IllegalArgumentException("At least one collection needs to be specified");
        }
        this._collections = arrpropertyDescriptorEnumeration;
    }

    @Override
    public Iterator<PropertyDescriptor> iterator() {
        return new CompoundIterator(this._collections);
    }

    @Override
    public boolean contains(PropertyDescriptor propertyDescriptor) {
        for (PropertyDescriptorEnumeration propertyDescriptorEnumeration : this._collections) {
            if (!propertyDescriptorEnumeration.contains(propertyDescriptor)) continue;
            return true;
        }
        return false;
    }

    @Override
    public boolean contains(String string) {
        for (PropertyDescriptorEnumeration propertyDescriptorEnumeration : this._collections) {
            if (!propertyDescriptorEnumeration.contains(string)) continue;
            return true;
        }
        return false;
    }

    @Override
    public PropertyDescriptor get(String string) {
        for (PropertyDescriptorEnumeration propertyDescriptorEnumeration : this._collections) {
            PropertyDescriptor propertyDescriptor = propertyDescriptorEnumeration.get(string);
            if (propertyDescriptor == null) continue;
            return propertyDescriptor;
        }
        return null;
    }
}

