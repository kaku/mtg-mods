/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.collections.CompoundIterator
 */
package com.paterva.maltego.typing.collections;

import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.util.collections.CompoundIterator;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class CompoundDisplayDescriptorCollection
implements DisplayDescriptorCollection {
    private DisplayDescriptorCollection[] _collections;

    public /* varargs */ CompoundDisplayDescriptorCollection(DisplayDescriptorCollection ... arrdisplayDescriptorCollection) {
        this._collections = arrdisplayDescriptorCollection;
    }

    @Override
    public boolean add(DisplayDescriptor displayDescriptor) {
        return this._collections[0].add(displayDescriptor);
    }

    public boolean remove(DisplayDescriptor displayDescriptor) {
        return this._collections[0].remove(displayDescriptor);
    }

    public void removeAll(DisplayDescriptorCollection displayDescriptorCollection) {
        this._collections[0].removeAll(displayDescriptorCollection);
    }

    public void addAll(DisplayDescriptorCollection displayDescriptorCollection) {
        this._collections[0].addAll(displayDescriptorCollection);
    }

    @Override
    public int size() {
        int n = 0;
        for (DisplayDescriptorCollection displayDescriptorCollection : this._collections) {
            n += displayDescriptorCollection.size();
        }
        return n;
    }

    @Override
    public boolean isEmpty() {
        for (DisplayDescriptorCollection displayDescriptorCollection : this._collections) {
            if (displayDescriptorCollection.isEmpty()) continue;
            return false;
        }
        return true;
    }

    @Override
    public boolean contains(Object object) {
        if (object instanceof DisplayDescriptor) {
            return this.contains((DisplayDescriptor)object);
        }
        return false;
    }

    @Override
    public Object[] toArray() {
        Object[] arrobject = new Object[this.size()];
        int n = 0;
        for (DisplayDescriptorCollection displayDescriptorCollection : this._collections) {
            Object[] arrobject2 = displayDescriptorCollection.toArray();
            CompoundDisplayDescriptorCollection.copy(arrobject2, arrobject, n);
            n += arrobject2.length;
        }
        return arrobject;
    }

    private static <T> void copy(T[] arrT, T[] arrT2, int n) {
        for (int i = 0; i < arrT.length; ++i) {
            arrT2[i + n] = arrT[i];
        }
    }

    @Override
    public <T> T[] toArray(T[] arrT) {
        int n = 0;
        for (DisplayDescriptorCollection displayDescriptorCollection : this._collections) {
            Object[] arrobject = displayDescriptorCollection.toArray();
            CompoundDisplayDescriptorCollection.copy(arrobject, arrT, n);
            n += arrobject.length;
        }
        return arrT;
    }

    @Override
    public boolean remove(Object object) {
        if (object instanceof DisplayDescriptor) {
            return this.remove((DisplayDescriptor)object);
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean addAll(Collection<? extends DisplayDescriptor> collection) {
        return this._collections[0].addAll(collection);
    }

    @Override
    public boolean addAll(int n, Collection<? extends DisplayDescriptor> collection) {
        return this._collections[0].addAll(n, collection);
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return this._collections[0].removeAll(collection);
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return this._collections[0].removeAll(collection);
    }

    @Override
    public void clear() {
        this._collections[0].clear();
    }

    @Override
    public DisplayDescriptor get(int n) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public DisplayDescriptor set(int n, DisplayDescriptor displayDescriptor) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void add(int n, DisplayDescriptor displayDescriptor) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public DisplayDescriptor remove(int n) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int indexOf(Object object) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int lastIndexOf(Object object) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ListIterator<DisplayDescriptor> listIterator() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ListIterator<DisplayDescriptor> listIterator(int n) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<DisplayDescriptor> subList(int n, int n2) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Iterator<DisplayDescriptor> iterator() {
        return new CompoundIterator(this._collections);
    }

    @Override
    public boolean contains(DisplayDescriptor displayDescriptor) {
        for (DisplayDescriptorCollection displayDescriptorCollection : this._collections) {
            if (!displayDescriptorCollection.contains(displayDescriptor)) continue;
            return true;
        }
        return false;
    }

    @Override
    public DisplayDescriptor get(String string) {
        for (DisplayDescriptor displayDescriptor : this) {
            if (!displayDescriptor.getName().equals(string)) continue;
            return displayDescriptor;
        }
        return null;
    }

    @Override
    public boolean contains(String string) {
        for (DisplayDescriptor displayDescriptor : this) {
            if (!displayDescriptor.getName().equals(string)) continue;
            return true;
        }
        return false;
    }
}

