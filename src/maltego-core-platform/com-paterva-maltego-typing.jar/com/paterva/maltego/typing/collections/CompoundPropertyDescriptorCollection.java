/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.collections;

import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptorEnumeration;
import com.paterva.maltego.typing.collections.CompoundPropertyDescriptorEnumeration;
import java.util.Collection;

public class CompoundPropertyDescriptorCollection
extends CompoundPropertyDescriptorEnumeration
implements PropertyDescriptorCollection {
    public /* varargs */ CompoundPropertyDescriptorCollection(PropertyDescriptorCollection ... arrpropertyDescriptorCollection) {
        super(arrpropertyDescriptorCollection);
    }

    @Override
    public boolean add(PropertyDescriptor propertyDescriptor) {
        return ((PropertyDescriptorCollection)this._collections[0]).add(propertyDescriptor);
    }

    @Override
    public boolean remove(PropertyDescriptor propertyDescriptor) {
        return ((PropertyDescriptorCollection)this._collections[0]).remove(propertyDescriptor);
    }

    @Override
    public boolean remove(String string) {
        return ((PropertyDescriptorCollection)this._collections[0]).remove(string);
    }

    @Override
    public void removeAll(PropertyDescriptorCollection propertyDescriptorCollection) {
        ((PropertyDescriptorCollection)this._collections[0]).removeAll(propertyDescriptorCollection);
    }

    @Override
    public void addAll(PropertyDescriptorCollection propertyDescriptorCollection) {
        ((PropertyDescriptorCollection)this._collections[0]).addAll(propertyDescriptorCollection);
    }

    @Override
    public int size() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isEmpty() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean contains(Object object) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T> T[] toArray(T[] arrT) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean remove(Object object) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean addAll(Collection<? extends PropertyDescriptor> collection) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}

