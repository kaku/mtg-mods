/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.collections.CompoundIterator
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.typing.collections;

import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.util.collections.CompoundIterator;
import java.util.Iterator;
import org.openide.util.Exceptions;

public class CompoundDisplayDescriptorEnumeration
implements DisplayDescriptorEnumeration {
    private DisplayDescriptorEnumeration[] _collections;

    public /* varargs */ CompoundDisplayDescriptorEnumeration(DisplayDescriptorEnumeration ... arrdisplayDescriptorEnumeration) {
        for (int i = 0; i < arrdisplayDescriptorEnumeration.length; ++i) {
            if (arrdisplayDescriptorEnumeration[i] != null) continue;
            try {
                throw new NullPointerException("DisplayDescriptorEnumeration may not be null: index=" + i);
            }
            catch (NullPointerException var3_3) {
                Exceptions.printStackTrace((Throwable)var3_3);
            }
        }
        this._collections = arrdisplayDescriptorEnumeration;
    }

    @Override
    public Iterator<DisplayDescriptor> iterator() {
        return new CompoundIterator(this._collections);
    }

    @Override
    public boolean contains(DisplayDescriptor displayDescriptor) {
        for (DisplayDescriptorEnumeration displayDescriptorEnumeration : this._collections) {
            if (!displayDescriptorEnumeration.contains(displayDescriptor)) continue;
            return true;
        }
        return false;
    }

    @Override
    public DisplayDescriptor get(String string) {
        for (DisplayDescriptor displayDescriptor : this) {
            if (!displayDescriptor.getName().equals(string)) continue;
            return displayDescriptor;
        }
        return null;
    }

    @Override
    public boolean contains(String string) {
        for (DisplayDescriptor displayDescriptor : this) {
            if (!displayDescriptor.getName().equals(string)) continue;
            return true;
        }
        return false;
    }
}

