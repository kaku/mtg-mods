/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.collections.CompoundIterator
 */
package com.paterva.maltego.typing.collections;

import com.paterva.maltego.typing.Group;
import com.paterva.maltego.typing.GroupCollection;
import com.paterva.maltego.util.collections.CompoundIterator;
import java.util.Iterator;

public class CompoundGroupCollection
implements GroupCollection {
    private GroupCollection[] _groups;

    public /* varargs */ CompoundGroupCollection(GroupCollection ... arrgroupCollection) {
        this._groups = arrgroupCollection;
    }

    @Override
    public void add(Group group) {
        throw new UnsupportedOperationException("Collection is read-only.");
    }

    @Override
    public void remove(Group group) {
        throw new UnsupportedOperationException("Collection is read-only.");
    }

    @Override
    public Group get(String string) {
        for (GroupCollection groupCollection : this._groups) {
            Group group = groupCollection.get(string);
            if (group == null) continue;
            return group;
        }
        return null;
    }

    @Override
    public Iterator<Group> iterator() {
        return new CompoundIterator(this._groups);
    }

    @Override
    public boolean contains(String string) {
        for (GroupCollection groupCollection : this._groups) {
            if (!groupCollection.contains(string)) continue;
            return true;
        }
        return false;
    }
}

