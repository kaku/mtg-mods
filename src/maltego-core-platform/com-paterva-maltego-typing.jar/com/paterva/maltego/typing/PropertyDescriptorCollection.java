/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorEnumeration;
import java.util.Collection;

public interface PropertyDescriptorCollection
extends Collection<PropertyDescriptor>,
PropertyDescriptorEnumeration {
    @Override
    public boolean add(PropertyDescriptor var1);

    public boolean remove(PropertyDescriptor var1);

    public boolean remove(String var1);

    public void removeAll(PropertyDescriptorCollection var1);

    public void addAll(PropertyDescriptorCollection var1);
}

