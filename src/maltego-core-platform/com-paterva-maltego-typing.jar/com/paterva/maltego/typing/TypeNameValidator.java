/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

public class TypeNameValidator {
    public static String checkName(String string) {
        for (char c : string.toCharArray()) {
            if (Character.isLetter(c) || c == '.' || Character.isDigit(c) || c == '-') continue;
            return "Only alphabetic characters, full stops, dashes and digits are allowed in the unique type name.";
        }
        return null;
    }
}

