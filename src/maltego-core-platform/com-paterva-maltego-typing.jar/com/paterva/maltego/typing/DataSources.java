/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.PropertyDescriptor;
import java.util.HashMap;
import javax.swing.event.ChangeListener;
import org.openide.util.ChangeSupport;
import org.openide.util.Utilities;

public class DataSources {
    private static DataSource EMPTY;

    private DataSources() {
    }

    public static DataSource empty() {
        if (EMPTY == null) {
            EMPTY = new DataSource(){

                @Override
                public Object getValue(PropertyDescriptor propertyDescriptor) {
                    return null;
                }

                @Override
                public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
                }
            };
        }
        return EMPTY;
    }

    public static /* varargs */ DataSource proxy(DataSource ... arrdataSource) {
        return new Proxy(arrdataSource);
    }

    public static /* varargs */ DataSource readOnlyProxy(DataSource ... arrdataSource) {
        return new ReadOnlyProxy(arrdataSource);
    }

    public static DataSource singleton(String string, Object object) {
        return new Singleton(string, object);
    }

    public static class Listenable
    implements DataSource {
        private final DataSource _delegate;
        private final ChangeSupport _changeSupport;

        public Listenable(DataSource dataSource) {
            this._changeSupport = new ChangeSupport((Object)this);
            this._delegate = dataSource;
        }

        @Override
        public Object getValue(PropertyDescriptor propertyDescriptor) {
            return this._delegate.getValue(propertyDescriptor);
        }

        @Override
        public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
            Object object2 = this._delegate.getValue(propertyDescriptor);
            if (!Utilities.compareObjects((Object)object2, (Object)object)) {
                this._delegate.setValue(propertyDescriptor, object);
                this._changeSupport.fireChange();
            }
        }

        public void addChangeListener(ChangeListener changeListener) {
            this._changeSupport.addChangeListener(changeListener);
        }

        public void removeChangeListener(ChangeListener changeListener) {
            this._changeSupport.removeChangeListener(changeListener);
        }
    }

    public static class Map
    extends HashMap<PropertyDescriptor, Object>
    implements DataSource {
        @Override
        public Object getValue(PropertyDescriptor propertyDescriptor) {
            return this.get(propertyDescriptor);
        }

        @Override
        public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
            this.put(propertyDescriptor, object);
        }
    }

    public static class ReadOnlyProxy
    implements DataSource {
        private DataSource[] _delegates;

        public /* varargs */ ReadOnlyProxy(DataSource ... arrdataSource) {
            this._delegates = arrdataSource;
        }

        @Override
        public Object getValue(PropertyDescriptor propertyDescriptor) {
            for (DataSource dataSource : this._delegates) {
                Object object = dataSource.getValue(propertyDescriptor);
                if (object == null) continue;
                return object;
            }
            return null;
        }

        @Override
        public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
            throw new UnsupportedOperationException("This data source is read only");
        }
    }

    public static class Proxy
    implements DataSource {
        private DataSource[] _delegates;

        public /* varargs */ Proxy(DataSource ... arrdataSource) {
            this._delegates = arrdataSource;
        }

        @Override
        public Object getValue(PropertyDescriptor propertyDescriptor) {
            for (DataSource dataSource : this._delegates) {
                Object object = dataSource.getValue(propertyDescriptor);
                if (object == null) continue;
                return object;
            }
            return null;
        }

        @Override
        public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
            if (this._delegates.length > 0) {
                this._delegates[0].setValue(propertyDescriptor, object);
            }
        }
    }

    private static class Singleton
    implements DataSource {
        private String _key;
        private Object _value;

        private Singleton(String string, Object object) {
            this._key = string;
            this._value = object;
        }

        @Override
        public Object getValue(PropertyDescriptor propertyDescriptor) {
            if (propertyDescriptor.getName().equals(this._key)) {
                return this._value;
            }
            return null;
        }

        @Override
        public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
        }
    }

}

