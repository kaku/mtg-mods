/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.Group;
import com.paterva.maltego.typing.GroupCollection;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class GroupSet
implements GroupCollection {
    private Map<String, Group> _groups = new HashMap<String, Group>();

    @Override
    public void add(Group group) {
        this._groups.put(group.getName(), group);
    }

    @Override
    public void remove(Group group) {
        this._groups.remove(group.getName());
    }

    @Override
    public Iterator<Group> iterator() {
        return this._groups.values().iterator();
    }

    @Override
    public Group get(String string) {
        return this._groups.get(string);
    }

    @Override
    public boolean contains(String string) {
        return this._groups.containsKey(string);
    }
}

