/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.Converter;
import java.lang.reflect.Array;
import java.text.Format;
import java.text.ParseException;

public class FormattedConverter {
    private FormattedConverter() {
    }

    public static String convertTo(Object object, Class class_, Format format) {
        if (format == null) {
            return Converter.convertTo(object, class_);
        }
        if (object == null) {
            return null;
        }
        if (class_.isArray()) {
            int n = Array.getLength(object);
            String[] arrstring = new String[n];
            Class class_2 = class_.getComponentType();
            for (int i = 0; i < n; ++i) {
                Object object2 = Array.get(object, i);
                arrstring[i] = FormattedConverter.convertTo(object2, class_2, format);
            }
            return FormattedConverter.convertTo(arrstring, String[].class, null);
        }
        return format.format(object);
    }

    public static Object convertFrom(String string, Class class_, Format format) throws ParseException {
        if (format == null) {
            return Converter.convertFrom(string, class_);
        }
        if (string == null) {
            return null;
        }
        if (class_.isArray()) {
            String[] arrstring = (String[])FormattedConverter.convertFrom(string, String[].class, null);
            Class class_2 = class_.getComponentType();
            Object object = Array.newInstance(class_2, arrstring.length);
            for (int i = 0; i < arrstring.length; ++i) {
                Array.set(object, i, FormattedConverter.convertFrom(arrstring[i], class_2, format));
            }
            return object;
        }
        return format.parseObject(string);
    }
}

