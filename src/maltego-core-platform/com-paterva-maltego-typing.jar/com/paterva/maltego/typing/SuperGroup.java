/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.Group;
import com.paterva.maltego.typing.GroupCollection;
import com.paterva.maltego.typing.GroupSet;

public class SuperGroup
extends Group {
    private GroupCollection _subGroups = new GroupSet();

    public SuperGroup(String string) {
        super(string, string);
    }

    public SuperGroup(String string, String string2) {
        super(string, string2, string2);
    }

    public SuperGroup(String string, String string2, String string3) {
        super(string, string2, string3);
    }

    public GroupCollection getSubGroups() {
        return this._subGroups;
    }
}

