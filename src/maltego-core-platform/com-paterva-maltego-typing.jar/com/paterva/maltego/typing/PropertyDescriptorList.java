/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

public class PropertyDescriptorList
implements PropertyDescriptorCollection {
    private LinkedList<PropertyDescriptor> _list = new LinkedList();

    @Override
    public Iterator<PropertyDescriptor> iterator() {
        return this._list.iterator();
    }

    @Override
    public boolean add(PropertyDescriptor propertyDescriptor) {
        return this._list.add(propertyDescriptor);
    }

    @Override
    public boolean remove(PropertyDescriptor propertyDescriptor) {
        return this._list.remove(propertyDescriptor);
    }

    @Override
    public boolean remove(String string) {
        Iterator<PropertyDescriptor> iterator = this._list.iterator();
        while (iterator.hasNext()) {
            PropertyDescriptor propertyDescriptor = iterator.next();
            if (!propertyDescriptor.getName().equals(string)) continue;
            iterator.remove();
            return true;
        }
        return false;
    }

    @Override
    public void removeAll(PropertyDescriptorCollection propertyDescriptorCollection) {
        for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
            this.remove(propertyDescriptor);
        }
    }

    @Override
    public void addAll(PropertyDescriptorCollection propertyDescriptorCollection) {
        for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
            this.add(propertyDescriptor);
        }
    }

    @Override
    public boolean contains(PropertyDescriptor propertyDescriptor) {
        return this._list.contains(propertyDescriptor);
    }

    @Override
    public PropertyDescriptor get(String string) {
        for (PropertyDescriptor propertyDescriptor : this._list) {
            if (!propertyDescriptor.getName().equals(string)) continue;
            return propertyDescriptor;
        }
        return null;
    }

    @Override
    public boolean contains(String string) {
        PropertyDescriptor propertyDescriptor = this.get(string);
        return propertyDescriptor != null;
    }

    @Override
    public int size() {
        return this._list.size();
    }

    @Override
    public boolean isEmpty() {
        return this._list.isEmpty();
    }

    @Override
    public boolean contains(Object object) {
        return this._list.contains(object);
    }

    @Override
    public Object[] toArray() {
        return this._list.toArray();
    }

    @Override
    public <T> T[] toArray(T[] arrT) {
        return this._list.toArray(arrT);
    }

    @Override
    public boolean remove(Object object) {
        return this._list.remove(object);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return this._list.containsAll(collection);
    }

    @Override
    public boolean addAll(Collection<? extends PropertyDescriptor> collection) {
        return this._list.addAll(collection);
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return this._list.removeAll(collection);
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return this._list.retainAll(collection);
    }

    @Override
    public void clear() {
        this._list.clear();
    }
}

