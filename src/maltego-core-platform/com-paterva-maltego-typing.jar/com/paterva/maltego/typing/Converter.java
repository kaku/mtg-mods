/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import java.lang.reflect.Array;

public class Converter {
    public static Class getPrimitiveType(Class class_) {
        if (class_.isArray()) {
            Class class_2 = class_.getComponentType();
            if (class_2.isPrimitive()) {
                return class_;
            }
            if (class_2 == Integer.class) {
                return int[].class;
            }
            if (class_2 == Float.class) {
                return float[].class;
            }
            if (class_2 == Double.class) {
                return double[].class;
            }
            if (class_2 == Boolean.class) {
                return boolean[].class;
            }
        } else {
            if (class_.isPrimitive()) {
                return class_;
            }
            if (class_ == Integer.class) {
                return Integer.TYPE;
            }
            if (class_ == Double.class) {
                return Double.TYPE;
            }
            if (class_ == Float.class) {
                return Float.TYPE;
            }
            if (class_ == Byte.class) {
                return Byte.TYPE;
            }
            if (class_ == Boolean.class) {
                return Boolean.TYPE;
            }
            if (class_ == Short.class) {
                return Short.TYPE;
            }
            if (class_ == Long.class) {
                return Long.TYPE;
            }
            if (class_ == Character.class) {
                return Character.TYPE;
            }
        }
        return null;
    }

    public static Class getReferenceType(Class class_) {
        if (class_.isArray()) {
            Class class_2 = class_.getComponentType();
            if (!class_2.isPrimitive()) {
                return class_;
            }
            if (class_2 == Integer.TYPE) {
                return Integer[].class;
            }
            if (class_2 == Float.TYPE) {
                return Float[].class;
            }
            if (class_2 == Double.TYPE) {
                return Double[].class;
            }
            if (class_2 == Boolean.TYPE) {
                return Boolean[].class;
            }
        } else {
            if (!class_.isPrimitive()) {
                return class_;
            }
            if (class_ == Integer.TYPE) {
                return Integer.class;
            }
            if (class_ == Double.TYPE) {
                return Double.class;
            }
            if (class_ == Float.TYPE) {
                return Float.class;
            }
            if (class_ == Byte.TYPE) {
                return Byte.class;
            }
            if (class_ == Boolean.TYPE) {
                return Boolean.class;
            }
            if (class_ == Short.TYPE) {
                return Short.class;
            }
            if (class_ == Long.TYPE) {
                return Long.class;
            }
            if (class_ == Character.TYPE) {
                return Character.class;
            }
        }
        return null;
    }

    public static boolean isAssignableFrom(Class class_, Class class_2) {
        if (class_ == null || class_2 == null) {
            return false;
        }
        return Converter.getReferenceType(class_).isAssignableFrom(Converter.getReferenceType(class_2));
    }

    public static Object changeArrayType(Object object, Class class_) {
        if (object == null) {
            return null;
        }
        if (!object.getClass().isArray()) {
            throw new IllegalArgumentException("objectArray parameter must be an array");
        }
        int n = Array.getLength(object);
        Object[] arrobject = (Object[])Array.newInstance(class_, n);
        for (int i = 0; i < n; ++i) {
            arrobject[i] = Array.get(object, i);
        }
        return arrobject;
    }

    public static Object convertFrom(String string, Class class_) throws IllegalArgumentException {
        return Converter.descriptor(class_).convert(string);
    }

    public static String convertTo(Object object, Class class_) throws IllegalArgumentException {
        return Converter.descriptor(class_).convert(object);
    }

    public static Object convertFrom(String string, String string2) throws IllegalArgumentException {
        return Converter.descriptor(string2).convert(string);
    }

    public static String convertTo(Object object, String string) throws IllegalArgumentException {
        return Converter.descriptor(string).convert(object);
    }

    public static Class getClassForType(String string) throws IllegalArgumentException {
        return Converter.descriptor(string).getType();
    }

    public static Object convert(Object object, Class class_) {
        return Converter.convert(object, object.getClass(), class_);
    }

    public static Object convert(Object object, Class class_, Class class_2) {
        if (class_2.isPrimitive() || class_2.isArray() && class_2.getComponentType().isPrimitive()) {
            class_ = Converter.getPrimitiveType(class_);
        }
        TypeDescriptor typeDescriptor = Converter.descriptor(class_);
        TypeDescriptor typeDescriptor2 = Converter.descriptor(class_2);
        String string = typeDescriptor.convert(object);
        return typeDescriptor2.convert(string);
    }

    private static TypeDescriptor descriptor(String string) {
        TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(string);
        if (typeDescriptor == null) {
            throw new IllegalArgumentException("No type descriptor found for type name " + string);
        }
        return typeDescriptor;
    }

    private static TypeDescriptor descriptor(Class class_) {
        TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(class_);
        if (typeDescriptor == null) {
            throw new IllegalArgumentException("No type descriptor found for type " + class_);
        }
        return typeDescriptor;
    }
}

