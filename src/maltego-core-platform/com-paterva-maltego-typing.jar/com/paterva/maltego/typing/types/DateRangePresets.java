/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.types;

import com.paterva.maltego.typing.types.DateTime;
import com.paterva.maltego.typing.types.FixedDateRange;
import com.paterva.maltego.typing.types.PresetsRelative;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateRangePresets {
    public static FixedDateRange getPresetDateRange(PresetsRelative presetsRelative) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        return new FixedDateRange(DateRangePresets.getPresetFromDate(presetsRelative, gregorianCalendar), DateRangePresets.getPresetToDate(presetsRelative, gregorianCalendar));
    }

    private static DateTime getPresetFromDate(PresetsRelative presetsRelative, Calendar calendar) {
        DateTime dateTime = new DateTime();
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setFirstDayOfWeek(2);
        gregorianCalendar.setTime(calendar.getTime());
        boolean bl = true;
        if (presetsRelative != null) {
            switch (presetsRelative) {
                case PRESETS_TODAY: {
                    break;
                }
                case PRESETS_WEEK_TO_DATE: 
                case PRESETS_BUSINESS_WEEK_TO_DATE: {
                    while (gregorianCalendar.get(7) != 2) {
                        gregorianCalendar.add(5, -1);
                    }
                    break;
                }
                case PRESETS_MONTH_TO_DATE: {
                    gregorianCalendar.set(5, gregorianCalendar.getActualMinimum(5));
                    break;
                }
                case PRESETS_YEAR_TO_DATE: {
                    gregorianCalendar.set(6, gregorianCalendar.getActualMinimum(6));
                    break;
                }
                case PRESETS_YESTERDAY: {
                    gregorianCalendar.add(5, -1);
                    break;
                }
                case PRESETS_PREV_WEEK: 
                case PRESETS_PREV_BUSINESS_WEEK: {
                    gregorianCalendar.add(5, -7);
                    while (gregorianCalendar.get(7) != 2) {
                        gregorianCalendar.add(5, -1);
                    }
                    break;
                }
                case PRESETS_PREV_MONTH: {
                    gregorianCalendar.add(2, -1);
                    gregorianCalendar.set(5, gregorianCalendar.getActualMinimum(5));
                    break;
                }
                case PRESETS_PREV_YEAR: {
                    gregorianCalendar.add(1, -1);
                    gregorianCalendar.set(6, gregorianCalendar.getActualMinimum(6));
                    break;
                }
                case PRESETS_LAST_15_MIN: {
                    gregorianCalendar.add(12, -15);
                    bl = false;
                    break;
                }
                case PRESETS_LAST_60_MIN: {
                    gregorianCalendar.add(12, -60);
                    bl = false;
                    break;
                }
                case PRESETS_LAST_4_HOURS: {
                    gregorianCalendar.add(11, -4);
                    bl = false;
                    break;
                }
                case PRESETS_LAST_24_HOURS: {
                    gregorianCalendar.add(11, -24);
                    bl = false;
                    break;
                }
                case PRESETS_LAST_7_DAYS: {
                    gregorianCalendar.add(5, -7);
                    bl = false;
                    break;
                }
                case PRESETS_LAST_30_DAYS: {
                    gregorianCalendar.add(5, -30);
                    bl = false;
                }
            }
        }
        if (bl) {
            DateRangePresets.setTimeToBeginningOfDay(gregorianCalendar);
        }
        dateTime.setDate(gregorianCalendar.getTime());
        return dateTime;
    }

    private static DateTime getPresetToDate(PresetsRelative presetsRelative, Calendar calendar) {
        DateTime dateTime = new DateTime();
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setFirstDayOfWeek(2);
        gregorianCalendar.setTime(calendar.getTime());
        if (presetsRelative != null) {
            switch (presetsRelative) {
                case PRESETS_TODAY: 
                case PRESETS_WEEK_TO_DATE: {
                    break;
                }
                case PRESETS_BUSINESS_WEEK_TO_DATE: {
                    if (gregorianCalendar.get(7) != 7 && gregorianCalendar.get(7) != 1) break;
                    gregorianCalendar.set(7, 6);
                    DateRangePresets.setTimeToEndofDay(gregorianCalendar);
                    break;
                }
                case PRESETS_MONTH_TO_DATE: 
                case PRESETS_YEAR_TO_DATE: {
                    break;
                }
                case PRESETS_YESTERDAY: {
                    gregorianCalendar.add(5, -1);
                    DateRangePresets.setTimeToEndofDay(gregorianCalendar);
                    break;
                }
                case PRESETS_PREV_WEEK: {
                    gregorianCalendar.add(5, -7);
                    while (gregorianCalendar.get(7) != 1) {
                        gregorianCalendar.add(5, 1);
                    }
                    DateRangePresets.setTimeToEndofDay(gregorianCalendar);
                    break;
                }
                case PRESETS_PREV_BUSINESS_WEEK: {
                    gregorianCalendar.add(5, -7);
                    while (gregorianCalendar.get(7) != 2) {
                        gregorianCalendar.add(5, -1);
                    }
                    while (gregorianCalendar.get(7) != 6) {
                        gregorianCalendar.add(5, 1);
                    }
                    DateRangePresets.setTimeToEndofDay(gregorianCalendar);
                    break;
                }
                case PRESETS_PREV_MONTH: {
                    gregorianCalendar.add(2, -1);
                    gregorianCalendar.set(5, gregorianCalendar.getActualMaximum(5));
                    DateRangePresets.setTimeToEndofDay(gregorianCalendar);
                    break;
                }
                case PRESETS_PREV_YEAR: {
                    gregorianCalendar.add(1, -1);
                    gregorianCalendar.set(6, gregorianCalendar.getActualMaximum(6));
                    DateRangePresets.setTimeToEndofDay(gregorianCalendar);
                    break;
                }
            }
        }
        dateTime.setDate(gregorianCalendar.getTime());
        return dateTime;
    }

    private static void setTimeToBeginningOfDay(Calendar calendar) {
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
    }

    private static void setTimeToEndofDay(Calendar calendar) {
        calendar.set(11, 23);
        calendar.set(12, 59);
        calendar.set(13, 59);
        calendar.set(14, 999);
    }

}

