/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FileUtilities
 */
package com.paterva.maltego.typing.types;

import com.paterva.maltego.util.FileUtilities;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class BinaryFile {
    private byte[] _data;
    private String _filename;

    public BinaryFile(String string, byte[] arrby) {
        this._filename = string;
        this._data = arrby;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static BinaryFile create(File file) throws FileNotFoundException, IOException {
        ByteArrayOutputStream byteArrayOutputStream = null;
        FileInputStream fileInputStream = null;
        InputStreamReader inputStreamReader = null;
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            fileInputStream = new FileInputStream(file);
            inputStreamReader = new InputStreamReader(fileInputStream);
            while (inputStreamReader.ready()) {
                byteArrayOutputStream.write(inputStreamReader.read());
            }
            BinaryFile binaryFile = new BinaryFile(file.getName(), byteArrayOutputStream.toByteArray());
            return binaryFile;
        }
        finally {
            if (byteArrayOutputStream != null) {
                try {
                    byteArrayOutputStream.close();
                }
                catch (IOException var5_5) {}
            }
            if (inputStreamReader != null) {
                try {
                    inputStreamReader.close();
                }
                catch (IOException var5_6) {}
            }
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                }
                catch (IOException var5_7) {}
            }
        }
    }

    public byte[] getData() {
        return this._data;
    }

    public void setData(byte[] arrby) {
        this._data = arrby;
    }

    public String getFilename() {
        return this._filename;
    }

    public void setFilename(String string) {
        this._filename = string;
    }

    public String getExtension() {
        return FileUtilities.getFileExtension((String)this._filename);
    }
}

