/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.types;

public class TimeSpan {
    private long _milliseconds;

    public TimeSpan(long l) {
        this._milliseconds = l;
    }

    public long getMilliseconds() {
        return this._milliseconds;
    }

    public void setMilliseconds(long l) {
        this._milliseconds = l;
    }

    public String toString() {
        return Long.toString(this._milliseconds);
    }

    public static TimeSpan parse(String string) {
        try {
            long l = Long.parseLong(string);
            return new TimeSpan(l);
        }
        catch (NumberFormatException var1_2) {
            return null;
        }
    }

    public int hashCode() {
        int n = 3;
        n = 23 * n + (int)(this._milliseconds ^ this._milliseconds >>> 32);
        return n;
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        TimeSpan timeSpan = (TimeSpan)object;
        if (this._milliseconds != timeSpan._milliseconds) {
            return false;
        }
        return true;
    }
}

