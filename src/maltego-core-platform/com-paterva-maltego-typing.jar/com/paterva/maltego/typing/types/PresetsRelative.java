/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.types;

public enum PresetsRelative {
    PRESETS_TODAY("Today"),
    PRESETS_WEEK_TO_DATE("Week to date"),
    PRESETS_BUSINESS_WEEK_TO_DATE("Business week to date"),
    PRESETS_MONTH_TO_DATE("Month to date"),
    PRESETS_YEAR_TO_DATE("Year to date"),
    PRESETS_YESTERDAY("Yesterday"),
    PRESETS_PREV_WEEK("Previous week"),
    PRESETS_PREV_BUSINESS_WEEK("Previous business week"),
    PRESETS_PREV_MONTH("Previous month"),
    PRESETS_PREV_YEAR("Previous year"),
    PRESETS_LAST_15_MIN("Last 15 minutes"),
    PRESETS_LAST_60_MIN("Last 60 minutes"),
    PRESETS_LAST_4_HOURS("Last 4 hours"),
    PRESETS_LAST_24_HOURS("Last 24 hours"),
    PRESETS_LAST_7_DAYS("Last 7 days"),
    PRESETS_LAST_30_DAYS("Last 30 days");
    
    private final String _presetRelative;

    private PresetsRelative(String string2) {
        this._presetRelative = string2;
    }

    public String getDisplayValue() {
        return this._presetRelative;
    }

    public String toString() {
        return this.getDisplayValue();
    }
}

