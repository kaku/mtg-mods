/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.typing.types;

import com.paterva.maltego.typing.types.DateRangePresets;
import com.paterva.maltego.typing.types.DateTime;
import com.paterva.maltego.typing.types.FixedDateRange;
import com.paterva.maltego.typing.types.PresetsRelative;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import org.openide.util.Exceptions;

public class DateRange {
    private static final SimpleDateFormat TO_STRING_SDF = DateTime.getDefaultFormat();
    private static final SimpleDateFormat TO_DISPLAY_STRING_SDF = new SimpleDateFormat("EEE, dd MMM yyyy, HH:mm:ss.SSS zzz");
    private static final String DATE_TIME_RANGE_PREFIX = "From ";
    private static final String DATE_TIME_RANGE_DIVIDER = " to ";
    private FixedDateRange _fixedDateRange;
    private PresetsRelative _presetRelativeItem = null;
    private boolean _isRelativeDateRange = false;

    public DateRange() {
        this(PresetsRelative.PRESETS_TODAY);
    }

    public DateRange(long l, long l2) {
        this(new Date(l), new Date(l2));
    }

    public DateRange(Date date, Date date2) {
        this(new FixedDateRange(date, date2));
    }

    public DateRange(DateTime dateTime, DateTime dateTime2) {
        this(new FixedDateRange(dateTime, dateTime2));
    }

    public DateRange(FixedDateRange fixedDateRange) {
        this(fixedDateRange, PresetsRelative.PRESETS_TODAY, false);
    }

    public DateRange(PresetsRelative presetsRelative) {
        this(DateRangePresets.getPresetDateRange(presetsRelative), presetsRelative, true);
    }

    public DateRange(Date date, Date date2, PresetsRelative presetsRelative, boolean bl) {
        this(new FixedDateRange(date, date2), presetsRelative, bl);
    }

    public DateRange(DateTime dateTime, DateTime dateTime2, PresetsRelative presetsRelative, boolean bl) {
        this(new FixedDateRange(dateTime, dateTime2), presetsRelative, bl);
    }

    public DateRange(FixedDateRange fixedDateRange, PresetsRelative presetsRelative, boolean bl) {
        this._fixedDateRange = fixedDateRange;
        this._presetRelativeItem = presetsRelative;
        this._isRelativeDateRange = bl;
    }

    public static DateRange createCopy(DateRange dateRange) {
        if (dateRange == null) {
            throw new IllegalArgumentException("DateRange.createCopy: input DateRange may not be null");
        }
        DateRange dateRange2 = new DateRange(dateRange.getFromDate(), dateRange.getToDate(), dateRange.getRelativeItem(), dateRange.isRelative());
        return dateRange2;
    }

    public boolean isValidRange() {
        boolean bl = true;
        if (!this.isRelative()) {
            bl = this.getFromDate().getDate().before(this.getToDate().getDate());
        }
        return bl;
    }

    public FixedDateRange getFixedDateRange() {
        return this._fixedDateRange;
    }

    public boolean isRelative() {
        return this._isRelativeDateRange;
    }

    public PresetsRelative getRelativeItem() {
        return this._presetRelativeItem;
    }

    public DateTime getFromDate() {
        return this._fixedDateRange.getFromDate();
    }

    public long getFromTime() {
        return this.getFromDate().getTime();
    }

    public DateTime getToDate() {
        return this._fixedDateRange.getToDate();
    }

    public long getToTime() {
        return this.getToDate().getTime();
    }

    public String toString() {
        return this.toStringHandler(TO_STRING_SDF);
    }

    public String toDisplayString() {
        return this.toStringHandler(TO_DISPLAY_STRING_SDF);
    }

    private String toStringHandler(SimpleDateFormat simpleDateFormat) {
        if (this.isRelative()) {
            return this.getRelativeItem().getDisplayValue();
        }
        return "From " + simpleDateFormat.format(this.getFromDate().getDate()) + " to " + simpleDateFormat.format(this.getToDate().getDate());
    }

    public static DateRange parse(String string) {
        DateRange dateRange = null;
        if (string != null) {
            string = string.trim();
            boolean bl = false;
            for (PresetsRelative object2 : PresetsRelative.values()) {
                if (!object2.getDisplayValue().equals(string)) continue;
                bl = true;
                dateRange = new DateRange(object2);
                break;
            }
            if (!bl && string.startsWith("From ") && string.contains(" to ")) {
                try {
                    Date date;
                    int n = string.indexOf(" to ");
                    String string2 = string.substring("From ".length(), n).trim();
                    DateTime dateTime = DateTime.parse(string2);
                    Date date2 = dateTime == null ? null : dateTime.getDate();
                    String string3 = string.substring(n + " to ".length()).trim();
                    DateTime dateTime2 = DateTime.parse(string3);
                    Date date3 = date = dateTime2 == null ? null : dateTime2.getDate();
                    if (date2 != null && date != null) {
                        dateRange = new DateRange(date2, date);
                    }
                }
                catch (Exception var3_5) {
                    Exceptions.printStackTrace((Throwable)var3_5);
                }
            }
        }
        return dateRange;
    }

    public int hashCode() {
        int n = 7;
        n = 97 * n + Objects.hashCode(this._fixedDateRange);
        n = 97 * n + Objects.hashCode((Object)this._presetRelativeItem);
        n = 97 * n + (this._isRelativeDateRange ? 1 : 0);
        return n;
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        DateRange dateRange = (DateRange)object;
        if (this.isRelative() != dateRange.isRelative()) {
            return false;
        }
        if (this.isRelative() ? this._presetRelativeItem != dateRange._presetRelativeItem && (this._presetRelativeItem == null || !this._presetRelativeItem.equals((Object)dateRange._presetRelativeItem)) : this._fixedDateRange != dateRange._fixedDateRange && (this._fixedDateRange == null || !this._fixedDateRange.equals(dateRange._fixedDateRange))) {
            return false;
        }
        return true;
    }

    public boolean isCopyEquals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        DateRange dateRange = (DateRange)object;
        if (!Objects.equals(this._fixedDateRange, dateRange._fixedDateRange)) {
            return false;
        }
        if (!Objects.equals((Object)this._presetRelativeItem, (Object)dateRange._presetRelativeItem)) {
            return false;
        }
        if (this.isRelative() != dateRange.isRelative()) {
            return false;
        }
        return true;
    }
}

