/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.types;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTime {
    public static final String DATETIME_TIME_FORMAT_STRING = "HH:mm:ss.S";
    private Date _date;

    public DateTime() {
        this._date = new Date();
    }

    public DateTime(Date date) {
        this._date = date;
    }

    public DateTime(long l) {
        this(new Date(l));
    }

    public Date getDate() {
        return this._date;
    }

    public void setDate(Date date) {
        this._date = date;
    }

    public long getTime() {
        return this._date.getTime();
    }

    public String toString() {
        return DateTime.getDefaultFormat().format(this._date);
    }

    public static SimpleDateFormat getDefaultFormat() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S Z");
    }

    public static DateTime parse(String string) {
        Date date = null;
        try {
            date = DateTime.getDefaultFormat().parse(string);
        }
        catch (ParseException var2_2) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S z");
            try {
                date = simpleDateFormat.parse(string);
            }
            catch (ParseException var4_5) {
                simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
                try {
                    date = simpleDateFormat.parse(string);
                }
                catch (ParseException var5_6) {
                    // empty catch block
                }
            }
        }
        DateTime dateTime = null;
        if (date != null) {
            dateTime = new DateTime(date);
        }
        return dateTime;
    }

    public int hashCode() {
        int n = 7;
        n = 37 * n + (this._date != null ? this._date.hashCode() : 0);
        return n;
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        DateTime dateTime = (DateTime)object;
        if (!(this._date == dateTime._date || this._date != null && this._date.equals(dateTime._date))) {
            return false;
        }
        return true;
    }
}

