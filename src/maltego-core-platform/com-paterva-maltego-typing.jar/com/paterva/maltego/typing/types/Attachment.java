/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.FileStore
 */
package com.paterva.maltego.typing.types;

import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.FileStore;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Attachment
implements Comparable<Attachment> {
    private int _id;
    private FastURL _source;

    public Attachment(int n, FastURL fastURL) {
        this._id = n;
        this._source = fastURL;
    }

    public int getId() {
        return this._id;
    }

    public FastURL getSource() {
        return this._source;
    }

    public String getFileName() {
        try {
            return FileStore.getDefault().get(this._id).getName();
        }
        catch (Exception var1_1) {
            Logger.getLogger(Attachment.class.getName()).log(Level.SEVERE, null, var1_1);
            return null;
        }
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        Attachment attachment = (Attachment)object;
        if (this._id != attachment._id) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int n = 7;
        n = 89 * n + this._id;
        return n;
    }

    public String toString() {
        return "Attachment{id=" + this._id + "}";
    }

    @Override
    public int compareTo(Attachment attachment) {
        return this.getFileName().compareTo(attachment.getFileName());
    }
}

