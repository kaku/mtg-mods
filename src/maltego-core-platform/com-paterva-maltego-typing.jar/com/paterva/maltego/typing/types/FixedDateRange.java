/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.types;

import com.paterva.maltego.typing.types.DateTime;
import java.util.Date;
import java.util.Objects;

public class FixedDateRange {
    private DateTime _fromDate;
    private DateTime _toDate;

    public FixedDateRange(Date date, Date date2) {
        this(new DateTime(date), new DateTime(date2));
    }

    public FixedDateRange(DateTime dateTime, DateTime dateTime2) {
        this.checkDateTime(dateTime, dateTime2);
        this._fromDate = dateTime;
        this._toDate = dateTime2;
    }

    public DateTime getFromDate() {
        return this._fromDate;
    }

    public DateTime getToDate() {
        return this._toDate;
    }

    public void setFromDate(DateTime dateTime) {
        this.checkDateTime(dateTime);
        this._fromDate = dateTime;
    }

    public void setToDate(DateTime dateTime) {
        this.checkDateTime(dateTime);
        this._toDate = dateTime;
    }

    private /* varargs */ void checkDateTime(DateTime ... arrdateTime) {
        for (DateTime dateTime : arrdateTime) {
            if (dateTime != null && dateTime.getDate() != null) continue;
            throw new IllegalArgumentException("FixedDateRange: from/to DateTime/Date values may not be null");
        }
    }

    public int hashCode() {
        int n = 3;
        n = 23 * n + Objects.hashCode(this._fromDate);
        n = 23 * n + Objects.hashCode(this._toDate);
        return n;
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        FixedDateRange fixedDateRange = (FixedDateRange)object;
        if (!Objects.equals(this._fromDate, fixedDateRange._fromDate)) {
            return false;
        }
        if (!Objects.equals(this._toDate, fixedDateRange._toDate)) {
            return false;
        }
        return true;
    }
}

