/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.types;

import com.paterva.maltego.typing.types.Attachment;
import java.util.ArrayList;
import java.util.Collection;

public class Attachments
extends ArrayList<Attachment> {
    private Attachment _primaryImage;

    public Attachments(Attachments attachments) {
        super(attachments);
        this._primaryImage = attachments._primaryImage;
    }

    public Attachments() {
    }

    public Attachments(int n) {
        super(n);
    }

    public Attachments(Collection<? extends Attachment> collection) {
        super(collection);
    }

    public Attachment getPrimaryImage() {
        return this._primaryImage;
    }

    public void setPrimaryImage(Attachment attachment) {
        if (attachment != null && !this.contains(attachment)) {
            throw new IllegalArgumentException("Primary image attachment not found.");
        }
        this._primaryImage = attachment;
    }

    @Override
    public Attachment remove(int n) {
        Attachment attachment = (Attachment)ArrayList.super.remove(n);
        if (attachment != null && attachment.equals(this._primaryImage)) {
            this._primaryImage = null;
        }
        return attachment;
    }

    @Override
    public boolean remove(Object object) {
        if (object != null && object.equals(this._primaryImage)) {
            this._primaryImage = null;
        }
        return ArrayList.super.remove(object);
    }

    @Override
    protected void removeRange(int n, int n2) {
        ArrayList.super.removeRange(n, n2);
        if (!this.contains(this._primaryImage)) {
            this._primaryImage = null;
        }
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        Attachments attachments = (Attachments)object;
        if (!(this._primaryImage == attachments._primaryImage || this._primaryImage != null && this._primaryImage.equals(attachments._primaryImage))) {
            return false;
        }
        if (!ArrayList.super.equals(object)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int n = ArrayList.super.hashCode();
        n = 97 * n + (this._primaryImage != null ? this._primaryImage.hashCode() : 0);
        return n;
    }
}

