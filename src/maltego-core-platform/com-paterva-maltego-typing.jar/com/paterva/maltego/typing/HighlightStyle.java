/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

public enum HighlightStyle {
    Normal,
    Medium,
    High;
    

    private HighlightStyle() {
    }
}

