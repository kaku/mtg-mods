/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.PropertyDescriptor;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class Evaluator {
    private static final Pattern _pattern = Pattern.compile("(\\$property\\((.*?)\\))", 2);
    private static final Pattern _trimPattern = Pattern.compile("(\\$trim\\((.*?)\\))", 2);

    public abstract Object evaluate(Object var1, Object var2, DataSource var3);

    public abstract List<PropertyDescriptor> getLinkedProperties(Object var1);

    public abstract String getName();

    public String toString() {
        return this.getName();
    }

    public static class Replacement
    extends Evaluator {
        public static boolean isSuitableFor(String string) {
            return _pattern.matcher(string).matches() || _trimPattern.matcher(string).matches();
        }

        @Override
        public Object evaluate(Object object, Object object2, DataSource dataSource) {
            ArrayList<PropertyDescriptor> arrayList = new ArrayList<PropertyDescriptor>();
            String string = Replacement.parse((String)object, arrayList);
            Object[] arrobject = new Object[arrayList.size()];
            for (int i = 0; i < arrayList.size(); ++i) {
                arrobject[i] = dataSource.getValue(arrayList.get(i));
            }
            String string2 = String.format(string, arrobject);
            string2 = Replacement.parseTrim(string2);
            return string2;
        }

        private static String parse(String string, List<PropertyDescriptor> list) {
            String string2;
            StringBuffer stringBuffer = new StringBuffer();
            String string3 = "%";
            String string4 = "$s";
            int n = 0;
            Matcher matcher = _pattern.matcher(string);
            while (matcher.find()) {
                list.add(new PropertyDescriptor(String.class, matcher.group(2)));
                string2 = string.substring(n, matcher.start());
                stringBuffer.append(string2);
                stringBuffer.append(string3);
                stringBuffer.append(list.size());
                stringBuffer.append(string4);
                n = matcher.end();
            }
            string2 = string.substring(n, string.length());
            stringBuffer.append(string2);
            return stringBuffer.toString();
        }

        private static String parseTrim(String string) {
            String string2;
            int n = 0;
            Matcher matcher = _trimPattern.matcher(string);
            StringBuffer stringBuffer = new StringBuffer();
            while (matcher.find()) {
                string2 = string.substring(n, matcher.start());
                stringBuffer.append(string2);
                stringBuffer.append(Replacement.trimWhitespaceAndCommas(matcher.group(2)));
                n = matcher.end();
            }
            string2 = string.substring(n, string.length());
            stringBuffer.append(string2);
            return stringBuffer.toString();
        }

        private static String trimWhitespaceAndCommas(String string) {
            string = Pattern.compile("^[\\s,]*").matcher(string).replaceAll("");
            string = Pattern.compile("[\\s,]*$").matcher(string).replaceAll("");
            return string;
        }

        @Override
        public List<PropertyDescriptor> getLinkedProperties(Object object) {
            ArrayList<PropertyDescriptor> arrayList = new ArrayList<PropertyDescriptor>();
            Replacement.parse((String)object, arrayList);
            return arrayList;
        }

        @Override
        public String getName() {
            return "maltego.replace";
        }
    }

}

