/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ListMap
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.util.ListMap;
import java.util.Map;

public class PropertyConfiguration {
    private GroupDefinitions _groups;
    private DisplayDescriptorEnumeration _properties;
    private Map<String, DisplayDescriptor> _specialProperties;

    public PropertyConfiguration() {
        this(null, null);
    }

    public PropertyConfiguration(DisplayDescriptorEnumeration displayDescriptorEnumeration) {
        this(displayDescriptorEnumeration, null);
    }

    public PropertyConfiguration(DisplayDescriptorEnumeration displayDescriptorEnumeration, GroupDefinitions groupDefinitions) {
        this(displayDescriptorEnumeration, groupDefinitions, null);
    }

    public PropertyConfiguration(DisplayDescriptorEnumeration displayDescriptorEnumeration, GroupDefinitions groupDefinitions, Map<String, DisplayDescriptor> map) {
        this._properties = displayDescriptorEnumeration;
        this._groups = groupDefinitions;
        this._specialProperties = map;
    }

    public DisplayDescriptor getSpecialProperty(String string) {
        if (this._specialProperties == null) {
            return null;
        }
        return this._specialProperties.get(string);
    }

    public void setSpecialProperty(String string, DisplayDescriptor displayDescriptor) {
        if (this._specialProperties == null) {
            this._specialProperties = new ListMap();
        }
        this._specialProperties.put(string, displayDescriptor);
    }

    public Map<String, DisplayDescriptor> getSpecialProperties() {
        return this._specialProperties;
    }

    public GroupDefinitions getGroups() {
        return this._groups;
    }

    public void setGroups(GroupDefinitions groupDefinitions) {
        this._groups = groupDefinitions;
    }

    public DisplayDescriptorEnumeration getProperties() {
        return this._properties;
    }

    public void setProperties(DisplayDescriptorEnumeration displayDescriptorEnumeration) {
        this._properties = displayDescriptorEnumeration;
    }
}

