/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import java.text.Format;

public interface FormatAdapter {
    public String getFormatString(Format var1);

    public Format getFormat(String var1);
}

