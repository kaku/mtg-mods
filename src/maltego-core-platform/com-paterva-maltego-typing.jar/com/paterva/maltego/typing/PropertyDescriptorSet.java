/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class PropertyDescriptorSet
extends AbstractSet<PropertyDescriptor>
implements PropertyDescriptorCollection {
    private Map<String, PropertyDescriptor> _list = new LinkedHashMap<String, PropertyDescriptor>();

    public PropertyDescriptorSet() {
    }

    public PropertyDescriptorSet(Collection<PropertyDescriptor> collection) {
        AbstractSet.super.addAll(collection);
    }

    public boolean isCopy(PropertyDescriptorSet propertyDescriptorSet) {
        if (propertyDescriptorSet == null) {
            return false;
        }
        if (this.isEmpty() && propertyDescriptorSet.isEmpty()) {
            return true;
        }
        if (this.size() != propertyDescriptorSet.size()) {
            return false;
        }
        for (String string : this._list.keySet()) {
            if (propertyDescriptorSet.contains(string)) continue;
            return false;
        }
        return true;
    }

    @Override
    public boolean add(PropertyDescriptor propertyDescriptor) {
        if (!this._list.containsKey(propertyDescriptor.getName())) {
            this._list.put(propertyDescriptor.getName(), propertyDescriptor);
            return true;
        }
        return false;
    }

    @Override
    public Iterator<PropertyDescriptor> iterator() {
        return this._list.values().iterator();
    }

    @Override
    public int size() {
        return this._list.size();
    }

    @Override
    public PropertyDescriptor get(String string) {
        return this._list.get(string);
    }

    @Override
    public boolean contains(String string) {
        return this._list.containsKey(string);
    }

    @Override
    public boolean remove(PropertyDescriptor propertyDescriptor) {
        return this._list.remove(propertyDescriptor.getName()) != null;
    }

    @Override
    public boolean remove(String string) {
        return this._list.remove(string) != null;
    }

    @Override
    public void removeAll(PropertyDescriptorCollection propertyDescriptorCollection) {
        this._list.clear();
    }

    @Override
    public void addAll(PropertyDescriptorCollection propertyDescriptorCollection) {
        for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
            this._list.put(propertyDescriptor.getName(), propertyDescriptor);
        }
    }

    @Override
    public boolean contains(PropertyDescriptor propertyDescriptor) {
        return this._list.containsKey(propertyDescriptor.getName());
    }
}

