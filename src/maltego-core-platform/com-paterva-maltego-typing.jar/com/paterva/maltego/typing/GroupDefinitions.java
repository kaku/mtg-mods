/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.GroupCollection;
import com.paterva.maltego.typing.GroupSet;

public class GroupDefinitions {
    private GroupCollection _topLevelGroups;
    private GroupCollection _topLevelSuperGroups;

    public GroupDefinitions() {
    }

    public GroupDefinitions(GroupCollection groupCollection, GroupCollection groupCollection2) {
        this._topLevelGroups = groupCollection;
        this._topLevelSuperGroups = groupCollection2;
    }

    public GroupCollection getTopLevelGroups() {
        if (this._topLevelGroups == null) {
            this._topLevelGroups = new GroupSet();
        }
        return this._topLevelGroups;
    }

    public GroupCollection getTopLevelSuperGroups() {
        if (this._topLevelSuperGroups == null) {
            this._topLevelSuperGroups = new GroupSet();
        }
        return this._topLevelSuperGroups;
    }
}

