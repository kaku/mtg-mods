/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ListMap
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.PropertyConfiguration;
import com.paterva.maltego.util.ListMap;
import java.util.Map;

public class SpecialPropertiesConfiguration
extends PropertyConfiguration {
    private Map<String, DisplayDescriptor> _specialProperties;

    public SpecialPropertiesConfiguration() {
        this(null, null);
    }

    public SpecialPropertiesConfiguration(DisplayDescriptorEnumeration displayDescriptorEnumeration) {
        this(displayDescriptorEnumeration, null);
    }

    public SpecialPropertiesConfiguration(DisplayDescriptorEnumeration displayDescriptorEnumeration, GroupDefinitions groupDefinitions) {
        this(displayDescriptorEnumeration, groupDefinitions, null);
    }

    public SpecialPropertiesConfiguration(DisplayDescriptorEnumeration displayDescriptorEnumeration, GroupDefinitions groupDefinitions, Map<String, DisplayDescriptor> map) {
        super(displayDescriptorEnumeration, groupDefinitions);
        this._specialProperties = map;
    }

    @Override
    public DisplayDescriptor getSpecialProperty(String string) {
        if (this._specialProperties == null) {
            return null;
        }
        return this._specialProperties.get(string);
    }

    @Override
    public void setSpecialProperty(String string, DisplayDescriptor displayDescriptor) {
        if (this._specialProperties == null) {
            this._specialProperties = new ListMap();
        }
        this._specialProperties.put(string, displayDescriptor);
    }

    @Override
    public Map<String, DisplayDescriptor> getSpecialProperties() {
        return this._specialProperties;
    }
}

