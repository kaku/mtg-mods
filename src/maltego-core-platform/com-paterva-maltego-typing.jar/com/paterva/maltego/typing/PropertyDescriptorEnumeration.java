/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.PropertyDescriptor;

public interface PropertyDescriptorEnumeration
extends Iterable<PropertyDescriptor> {
    public boolean contains(PropertyDescriptor var1);

    public boolean contains(String var1);

    public PropertyDescriptor get(String var1);
}

