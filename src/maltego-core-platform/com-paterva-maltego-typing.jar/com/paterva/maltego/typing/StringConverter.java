/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

public interface StringConverter {
    public Object convertFrom(String var1, Class var2) throws IllegalArgumentException;

    public String convertTo(Object var1, Class var2) throws IllegalArgumentException;
}

