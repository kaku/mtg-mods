/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ColorUtilities
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.DefaultStringConverter;
import com.paterva.maltego.typing.FormatAdapter;
import com.paterva.maltego.typing.StringConverter;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.formatting.BooleanFormatAdapter;
import com.paterva.maltego.typing.formatting.SimpleDateFormatAdapter;
import com.paterva.maltego.typing.formatting.TimeSpanFormatAdapter;
import com.paterva.maltego.typing.types.Attachments;
import com.paterva.maltego.typing.types.DateRange;
import com.paterva.maltego.typing.types.DateTime;
import com.paterva.maltego.typing.types.InternalFile;
import com.paterva.maltego.typing.types.TimeSpan;
import com.paterva.maltego.util.ColorUtilities;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.StringUtilities;
import java.awt.Color;
import java.awt.Image;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import org.openide.util.Exceptions;

public class TypeRegistry {
    private static TypeRegistry _default;
    private final Map<String, TypeDescriptor> _nameMap = new LinkedHashMap<String, TypeDescriptor>();
    private final Map<Class, TypeDescriptor> _classMap = new LinkedHashMap<Class, TypeDescriptor>();

    public static TypeRegistry getDefault() {
        if (_default == null) {
            _default = new TypeRegistry();
            TypeRegistry.registerDefaults(_default);
        }
        return _default;
    }

    private TypeRegistry() {
    }

    public void registerType(TypeDescriptor typeDescriptor) {
        this._nameMap.put(typeDescriptor.getTypeName(), typeDescriptor);
        this._classMap.put(typeDescriptor.getType(), typeDescriptor);
    }

    public void deregisterType(String string) {
        TypeDescriptor typeDescriptor = this._nameMap.get(string);
        if (typeDescriptor != null) {
            this._classMap.remove(typeDescriptor.getType());
        }
    }

    public void deregisterType(Class class_) {
        TypeDescriptor typeDescriptor = this._classMap.get(class_);
        if (typeDescriptor != null) {
            this._nameMap.remove(typeDescriptor.getTypeName());
        }
    }

    public TypeDescriptor getType(String string) {
        return this._nameMap.get(string);
    }

    public TypeDescriptor getType(Class class_) {
        return this._classMap.get(class_);
    }

    public TypeDescriptor[] getTypes() {
        Collection<TypeDescriptor> collection = this._nameMap.values();
        return collection.toArray(new TypeDescriptor[collection.size()]);
    }

    public void registerType(String string, Class class_) {
        this.registerType(new TypeDescriptor(string, class_));
    }

    public void registerType(String string, Class class_, StringConverter stringConverter) {
        this.registerType(new TypeDescriptor(string, class_, stringConverter));
    }

    public void registerType(String string, Class class_, StringConverter stringConverter, Object object) {
        this.registerType(new TypeDescriptor(string, class_, stringConverter, object));
    }

    public void registerType(String string, Class class_, Object object) {
        this.registerType(new TypeDescriptor(string, class_, object));
    }

    public static void registerDefaults(TypeRegistry typeRegistry) {
        typeRegistry.registerType("string", String.class, "");
        typeRegistry.registerType("char", Character.TYPE, Character.valueOf('0'));
        typeRegistry.registerType("int", Integer.TYPE, 0);
        typeRegistry.registerType("double", Double.TYPE, 0.0);
        typeRegistry.registerType("float", Float.TYPE, Float.valueOf(0.0f));
        typeRegistry.registerType("byte", Byte.TYPE, Byte.valueOf(0));
        TypeDescriptor typeDescriptor = new TypeDescriptor("date", Date.class, new DateConverter(), null);
        typeDescriptor.setFormatAdapter(new SimpleDateFormatAdapter());
        typeRegistry.registerType(typeDescriptor);
        TypeDescriptor typeDescriptor2 = new TypeDescriptor("boolean", Boolean.TYPE, false);
        typeDescriptor2.setFormatAdapter(new BooleanFormatAdapter());
        typeRegistry.registerType(typeDescriptor2);
        typeRegistry.registerType("file", File.class, null);
        typeRegistry.registerType("image", Image.class, new ImageURLConverter(), null);
        typeRegistry.registerType("url", FastURL.class, new FastURLConverter());
        typeRegistry.registerType("color", Color.class, new ColorConverter(), null);
        typeRegistry.registerType("attachments", Attachments.class, null);
        typeRegistry.registerType("internalfile", InternalFile.class, null);
        TypeDescriptor typeDescriptor3 = new TypeDescriptor("datetime", DateTime.class, new DateTimeConverter(), null);
        typeDescriptor3.setFormatAdapter(new SimpleDateFormatAdapter());
        typeRegistry.registerType(typeDescriptor3);
        TypeDescriptor typeDescriptor4 = new TypeDescriptor("daterange", DateRange.class, new DateRangeConverter(), null);
        typeDescriptor4.setFormatAdapter(new SimpleDateFormatAdapter());
        typeRegistry.registerType(typeDescriptor4);
        TypeDescriptor typeDescriptor5 = new TypeDescriptor("timespan", TimeSpan.class, new TimeSpanConverter(), null);
        typeDescriptor5.setFormatAdapter(new TimeSpanFormatAdapter());
        typeRegistry.registerType(typeDescriptor5);
        typeRegistry.registerType("int[]", int[].class, new int[0]);
        typeRegistry.registerType("string[]", String[].class, new String[0]);
        typeRegistry.registerType("boolean[]", boolean[].class, new boolean[0]);
        typeRegistry.registerType("double[]", double[].class, new double[0]);
        typeRegistry.registerType("float[]", float[].class, new float[0]);
        TypeDescriptor typeDescriptor6 = new TypeDescriptor("boolean[]", boolean[].class, new boolean[0]);
        typeDescriptor6.setFormatAdapter(typeDescriptor2.getFormatAdapter());
        typeRegistry.registerType(typeDescriptor6);
        TypeDescriptor typeDescriptor7 = new TypeDescriptor("date[]", Date[].class, new Date[0]);
        typeDescriptor7.setFormatAdapter(typeDescriptor.getFormatAdapter());
        typeRegistry.registerType(typeDescriptor7);
    }

    public static String getDefaultDateFormat() {
        return "yyyy-MM-dd";
    }

    private static class TimeSpanConverter
    implements StringConverter {
        private TimeSpanConverter() {
        }

        @Override
        public Object convertFrom(String string, Class class_) throws IllegalArgumentException {
            if (!StringUtilities.isNullOrEmpty((String)string)) {
                return TimeSpan.parse(string);
            }
            return null;
        }

        @Override
        public String convertTo(Object object, Class class_) throws IllegalArgumentException {
            if (object instanceof TimeSpan) {
                return object.toString();
            }
            return DefaultStringConverter.instance().convertTo(object, class_);
        }
    }

    private static class DateRangeConverter
    implements StringConverter {
        private DateRangeConverter() {
        }

        @Override
        public Object convertFrom(String string, Class class_) throws IllegalArgumentException {
            if (!StringUtilities.isNullOrEmpty((String)string)) {
                return DateRange.parse(string);
            }
            return null;
        }

        @Override
        public String convertTo(Object object, Class class_) throws IllegalArgumentException {
            if (object instanceof DateRange) {
                return object.toString();
            }
            return DefaultStringConverter.instance().convertTo(object, class_);
        }
    }

    private static class DateTimeConverter
    implements StringConverter {
        private DateTimeConverter() {
        }

        @Override
        public Object convertFrom(String string, Class class_) throws IllegalArgumentException {
            if (!StringUtilities.isNullOrEmpty((String)string)) {
                return DateTime.parse(string);
            }
            return null;
        }

        @Override
        public String convertTo(Object object, Class class_) throws IllegalArgumentException {
            if (object instanceof DateTime) {
                return object.toString();
            }
            return DefaultStringConverter.instance().convertTo(object, class_);
        }
    }

    private static class DateConverter
    implements StringConverter {
        private SimpleDateFormat _format;

        private DateConverter() {
        }

        protected synchronized SimpleDateFormat getFormat() {
            if (this._format == null) {
                this._format = new SimpleDateFormat(TypeRegistry.getDefaultDateFormat());
            }
            return this._format;
        }

        @Override
        public Object convertFrom(String string, Class class_) throws IllegalArgumentException {
            if (!StringUtilities.isNullOrEmpty((String)string)) {
                Date date = null;
                try {
                    date = this.getFormat().parse(string);
                }
                catch (ParseException var4_4) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S z");
                    try {
                        simpleDateFormat.parse(string);
                    }
                    catch (ParseException var6_6) {
                        // empty catch block
                    }
                }
                return date;
            }
            return null;
        }

        @Override
        public String convertTo(Object object, Class class_) throws IllegalArgumentException {
            if (object instanceof Date) {
                return this.getFormat().format((Date)object);
            }
            return DefaultStringConverter.instance().convertTo(object, class_);
        }
    }

    private static class FastURLConverter
    implements StringConverter {
        private FastURLConverter() {
        }

        @Override
        public Object convertFrom(String string, Class class_) throws IllegalArgumentException {
            if (!StringUtilities.isNullOrEmpty((String)string)) {
                return new FastURL(string);
            }
            return null;
        }

        @Override
        public String convertTo(Object object, Class class_) throws IllegalArgumentException {
            if (object instanceof FastURL) {
                return object.toString();
            }
            if (FastURL.class.equals((Object)class_)) {
                return null;
            }
            return DefaultStringConverter.instance().convertTo(object, class_);
        }
    }

    private static class ColorConverter
    implements StringConverter {
        private ColorConverter() {
        }

        @Override
        public Object convertFrom(String string, Class class_) throws IllegalArgumentException {
            Object object = null;
            if (!StringUtilities.isNullOrEmpty((String)string)) {
                try {
                    object = Color.decode(string);
                    if (object == null) {
                        object = DefaultStringConverter.instance().convertFrom(string, class_);
                    }
                }
                catch (NumberFormatException var4_4) {
                    Exceptions.printStackTrace((Throwable)var4_4);
                    object = Color.BLACK;
                }
            }
            return object;
        }

        @Override
        public String convertTo(Object object, Class class_) throws IllegalArgumentException {
            if (object instanceof Color) {
                Color color = (Color)object;
                return ColorUtilities.encode((Color)color);
            }
            return DefaultStringConverter.instance().convertTo(object, class_);
        }
    }

    private static class ImageURLConverter
    implements StringConverter {
        private ImageURLConverter() {
        }

        @Override
        public Object convertFrom(String string, Class class_) throws IllegalArgumentException {
            Object object = null;
            if (string != null) {
                if (string.startsWith("http")) {
                    try {
                        object = new URL(string);
                    }
                    catch (MalformedURLException var4_4) {
                        // empty catch block
                    }
                }
                if (object == null) {
                    object = DefaultStringConverter.instance().convertFrom(string, class_);
                }
            }
            return object;
        }

        @Override
        public String convertTo(Object object, Class class_) throws IllegalArgumentException {
            if (object instanceof URL) {
                return object.toString();
            }
            return DefaultStringConverter.instance().convertTo(object, class_);
        }
    }

}

