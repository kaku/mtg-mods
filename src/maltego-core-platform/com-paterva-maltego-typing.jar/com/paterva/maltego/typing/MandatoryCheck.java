/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptorList;

public class MandatoryCheck {
    private MandatoryCheck() {
    }

    public PropertyDescriptorCollection getMissingProperties(PropertyDescriptorCollection propertyDescriptorCollection, DataSource dataSource) {
        PropertyDescriptorList propertyDescriptorList = new PropertyDescriptorList();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
            if (propertyDescriptor.isNullable() || dataSource.getValue(propertyDescriptor) != null) continue;
            propertyDescriptorList.add(propertyDescriptor);
        }
        return propertyDescriptorList;
    }
}

