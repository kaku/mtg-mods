/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptorCollectionAdapter;
import com.paterva.maltego.typing.PropertyDescriptorList;
import com.paterva.maltego.typing.collections.CompoundDisplayDescriptorEnumeration;
import com.paterva.maltego.typing.collections.CompoundPropertyDescriptorCollection;
import java.util.AbstractSet;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

public final class PropertyDescriptors {
    private PropertyDescriptors() {
    }

    public static PropertyDescriptorCollection emptySet() {
        return new Empty();
    }

    public static PropertyDescriptorCollection singleton(PropertyDescriptor propertyDescriptor) {
        return new Singleton(propertyDescriptor);
    }

    public static PropertyDescriptorCollection add(PropertyDescriptorCollection propertyDescriptorCollection, PropertyDescriptorCollection propertyDescriptorCollection2) {
        return new CompoundPropertyDescriptorCollection(propertyDescriptorCollection, propertyDescriptorCollection2);
    }

    public static PropertyDescriptorCollection add(DisplayDescriptorEnumeration displayDescriptorEnumeration, PropertyDescriptorCollection propertyDescriptorCollection) {
        return new CompoundPropertyDescriptorCollection(new PropertyDescriptorCollectionAdapter(displayDescriptorEnumeration), propertyDescriptorCollection);
    }

    public static PropertyDescriptorCollection add(PropertyDescriptorCollection propertyDescriptorCollection, DisplayDescriptorCollection displayDescriptorCollection) {
        return new CompoundPropertyDescriptorCollection(propertyDescriptorCollection, new PropertyDescriptorCollectionAdapter(displayDescriptorCollection));
    }

    public static DisplayDescriptorEnumeration add(DisplayDescriptorEnumeration displayDescriptorEnumeration, DisplayDescriptorEnumeration displayDescriptorEnumeration2) {
        return new CompoundDisplayDescriptorEnumeration(displayDescriptorEnumeration, displayDescriptorEnumeration2);
    }

    public static PropertyDescriptorCollection substract(PropertyDescriptorCollection propertyDescriptorCollection, PropertyDescriptorCollection propertyDescriptorCollection2) {
        PropertyDescriptorList propertyDescriptorList = new PropertyDescriptorList();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
            if (propertyDescriptorCollection2.contains(propertyDescriptor)) continue;
            propertyDescriptorList.add(propertyDescriptor);
        }
        return propertyDescriptorList;
    }

    private static class Singleton
    extends AbstractSet<PropertyDescriptor>
    implements PropertyDescriptorCollection {
        private Set<PropertyDescriptor> _list;

        public Singleton(PropertyDescriptor propertyDescriptor) {
            this._list = Collections.singleton(propertyDescriptor);
        }

        @Override
        public boolean add(PropertyDescriptor propertyDescriptor) {
            return false;
        }

        @Override
        public Iterator<PropertyDescriptor> iterator() {
            return this._list.iterator();
        }

        @Override
        public int size() {
            return this._list.size();
        }

        @Override
        public PropertyDescriptor get(String string) {
            for (PropertyDescriptor propertyDescriptor : this._list) {
                if (!propertyDescriptor.getName().equals(string)) continue;
                return propertyDescriptor;
            }
            return null;
        }

        @Override
        public boolean contains(String string) {
            PropertyDescriptor propertyDescriptor = this.get(string);
            return propertyDescriptor != null;
        }

        @Override
        public boolean remove(PropertyDescriptor propertyDescriptor) {
            return false;
        }

        @Override
        public boolean remove(String string) {
            return false;
        }

        @Override
        public void removeAll(PropertyDescriptorCollection propertyDescriptorCollection) {
        }

        @Override
        public void addAll(PropertyDescriptorCollection propertyDescriptorCollection) {
        }

        @Override
        public boolean contains(PropertyDescriptor propertyDescriptor) {
            return this._list.contains(propertyDescriptor);
        }
    }

    private static class Empty
    extends AbstractSet<PropertyDescriptor>
    implements PropertyDescriptorCollection {
        @Override
        public boolean add(PropertyDescriptor propertyDescriptor) {
            return false;
        }

        @Override
        public Iterator<PropertyDescriptor> iterator() {
            return new Iterator<PropertyDescriptor>(){

                @Override
                public boolean hasNext() {
                    return false;
                }

                @Override
                public PropertyDescriptor next() {
                    return null;
                }

                @Override
                public void remove() {
                }
            };
        }

        @Override
        public int size() {
            return 0;
        }

        @Override
        public PropertyDescriptor get(String string) {
            return null;
        }

        @Override
        public boolean contains(String string) {
            return false;
        }

        @Override
        public boolean remove(PropertyDescriptor propertyDescriptor) {
            return false;
        }

        @Override
        public boolean remove(String string) {
            return false;
        }

        @Override
        public void removeAll(PropertyDescriptorCollection propertyDescriptorCollection) {
        }

        @Override
        public void addAll(PropertyDescriptorCollection propertyDescriptorCollection) {
        }

        @Override
        public boolean contains(PropertyDescriptor propertyDescriptor) {
            return false;
        }

    }

}

