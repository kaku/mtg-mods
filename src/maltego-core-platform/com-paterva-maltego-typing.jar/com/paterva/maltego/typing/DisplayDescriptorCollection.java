/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import java.util.List;

public interface DisplayDescriptorCollection
extends List<DisplayDescriptor>,
DisplayDescriptorEnumeration {
    @Override
    public boolean add(DisplayDescriptor var1);
}

