/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import java.util.Collection;
import java.util.Iterator;

class PropertyDescriptorCollectionAdapter
implements PropertyDescriptorCollection {
    private DisplayDescriptorEnumeration _delegate;

    public PropertyDescriptorCollectionAdapter(DisplayDescriptorEnumeration displayDescriptorEnumeration) {
        this._delegate = displayDescriptorEnumeration;
    }

    @Override
    public Iterator<PropertyDescriptor> iterator() {
        return new IteratorAdapter(this._delegate.iterator());
    }

    @Override
    public boolean add(PropertyDescriptor propertyDescriptor) {
        throw new UnsupportedOperationException("Collection is read-only.");
    }

    @Override
    public boolean remove(PropertyDescriptor propertyDescriptor) {
        throw new UnsupportedOperationException("Collection is read-only.");
    }

    @Override
    public boolean remove(String string) {
        throw new UnsupportedOperationException("Collection is read-only.");
    }

    @Override
    public void removeAll(PropertyDescriptorCollection propertyDescriptorCollection) {
        throw new UnsupportedOperationException("Collection is read-only.");
    }

    @Override
    public void addAll(PropertyDescriptorCollection propertyDescriptorCollection) {
        throw new UnsupportedOperationException("Collection is read-only.");
    }

    @Override
    public boolean contains(PropertyDescriptor propertyDescriptor) {
        return this._delegate.contains(propertyDescriptor.getName());
    }

    @Override
    public PropertyDescriptor get(String string) {
        return this._delegate.get(string);
    }

    @Override
    public boolean contains(String string) {
        return this._delegate.contains(string);
    }

    @Override
    public int size() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isEmpty() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean contains(Object object) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T> T[] toArray(T[] arrT) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean remove(Object object) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean addAll(Collection<? extends PropertyDescriptor> collection) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private static class IteratorAdapter
    implements Iterator<PropertyDescriptor> {
        private Iterator<DisplayDescriptor> _delegate;

        public IteratorAdapter(Iterator<DisplayDescriptor> iterator) {
            this._delegate = iterator;
        }

        @Override
        public boolean hasNext() {
            return this._delegate.hasNext();
        }

        @Override
        public PropertyDescriptor next() {
            return this._delegate.next();
        }

        @Override
        public void remove() {
            this._delegate.remove();
        }
    }

}

