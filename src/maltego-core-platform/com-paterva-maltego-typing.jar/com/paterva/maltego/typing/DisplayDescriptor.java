/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.EditorDescriptor;
import com.paterva.maltego.typing.Evaluator;
import com.paterva.maltego.typing.HighlightStyle;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.ValidationDescriptor;
import java.text.Format;
import java.util.List;

public class DisplayDescriptor
extends PropertyDescriptor {
    private int _sortOrder;
    private boolean _popup;
    private HighlightStyle _highlight;
    private String _description = "";
    private Object _defaultValue;
    private Object _sampleValue;
    private String _groupName;
    private Format _format;
    private Evaluator _evaluator;
    private ValidationDescriptor _validator;
    private EditorDescriptor _editor;
    private TypeDescriptor _typeDescriptor;

    public DisplayDescriptor(Class class_, String string) {
        this(class_, string, string);
    }

    public DisplayDescriptor(Class class_, String string, String string2) {
        super(class_, string, string2);
        TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(class_);
        if (typeDescriptor != null) {
            this._defaultValue = typeDescriptor.getDefaultValue();
            this._sampleValue = typeDescriptor.getDefaultValue();
        }
    }

    public DisplayDescriptor(PropertyDescriptor propertyDescriptor) {
        super(propertyDescriptor);
    }

    public DisplayDescriptor(DisplayDescriptor displayDescriptor) {
        this((PropertyDescriptor)displayDescriptor);
        this._defaultValue = displayDescriptor.getDefaultValue();
        this._sampleValue = displayDescriptor.getSampleValue();
        this._editor = displayDescriptor.getEditor();
        this._sortOrder = displayDescriptor.getSortOrder();
        this._description = displayDescriptor.getDescription();
        this._popup = displayDescriptor.isPopup();
        this._highlight = displayDescriptor.getHighlight();
        this._typeDescriptor = displayDescriptor.getTypeDescriptor();
        this._groupName = displayDescriptor.getGroupName();
    }

    public TypeDescriptor getTypeDescriptor() {
        if (this._typeDescriptor == null) {
            this._typeDescriptor = TypeRegistry.getDefault().getType(this.getType());
        }
        return this._typeDescriptor;
    }

    public Object getDefaultValue() {
        return this._defaultValue;
    }

    public void setDefaultValue(Object object) {
        TypeDescriptor typeDescriptor;
        if (object == null && (typeDescriptor = this.getTypeDescriptor()) != null && typeDescriptor.getDefaultValue() != null) {
            object = typeDescriptor.getDefaultValue();
        }
        this._defaultValue = object;
    }

    public Object getSampleValue() {
        return this._sampleValue;
    }

    public void setSampleValue(Object object) {
        this._sampleValue = object;
    }

    public ValidationDescriptor getValidator() {
        return this._validator;
    }

    public void setValidator(ValidationDescriptor validationDescriptor) {
        this._validator = validationDescriptor;
    }

    public EditorDescriptor getEditor() {
        return this._editor;
    }

    public void setEditor(EditorDescriptor editorDescriptor) {
        this._editor = editorDescriptor;
    }

    public int getSortOrder() {
        return this._sortOrder;
    }

    public void setSortOrder(int n) {
        this._sortOrder = n;
    }

    public boolean isPopup() {
        return this._popup;
    }

    public void setPopup(boolean bl) {
        this._popup = bl;
    }

    public HighlightStyle getHighlight() {
        return this._highlight;
    }

    public void setHighlight(HighlightStyle highlightStyle) {
        this._highlight = highlightStyle;
    }

    @Override
    public String getDescription() {
        return this._description;
    }

    public void setDescription(String string) {
        this._description = string == null ? "" : string;
    }

    public String getGroupName() {
        return this._groupName;
    }

    public void setGroupName(String string) {
        this._groupName = string;
    }

    public Format getFormat() {
        return this._format;
    }

    public void setFormat(Format format) {
        this._format = format;
    }

    public void setEvaluator(Evaluator evaluator) {
        this._evaluator = evaluator;
    }

    public Evaluator getEvaluator() {
        return this._evaluator;
    }

    @Override
    public List<PropertyDescriptor> getLinkedProperties() {
        if (this.getEvaluator() != null) {
            return this.getEvaluator().getLinkedProperties(this.getDefaultValue());
        }
        return super.getLinkedProperties();
    }

    @Override
    protected Object evaluate(Object object, DataSource dataSource) {
        if (this.getEvaluator() != null) {
            return this.getEvaluator().evaluate(this.getDefaultValue(), object, dataSource);
        }
        return super.evaluate(object, dataSource);
    }
}

