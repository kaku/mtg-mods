/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.EditorDescriptor;
import java.util.HashMap;
import java.util.Map;

class DefaultEditorRegistry {
    private static DefaultEditorRegistry _default;
    private Map<String, EditorDescriptor> _nameMap = new HashMap<String, EditorDescriptor>();

    public static DefaultEditorRegistry getInstance() {
        if (_default == null) {
            _default = new DefaultEditorRegistry();
        }
        return _default;
    }

    private DefaultEditorRegistry() {
    }

    public void registerEditor(String string, EditorDescriptor editorDescriptor) {
        this._nameMap.put(string, editorDescriptor);
    }

    public void deregisterEditor(String string) {
        this._nameMap.remove(string);
    }

    public EditorDescriptor getDefaultEditor(String string) {
        return this._nameMap.get(string);
    }
}

