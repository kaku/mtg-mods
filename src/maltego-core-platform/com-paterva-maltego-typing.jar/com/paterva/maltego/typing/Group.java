/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

public class Group {
    private String _name;
    private String _displayName;
    private String _description;

    public Group(String string) {
        this(string, string);
    }

    public Group(String string, String string2) {
        this(string, string2, string2);
    }

    public Group(String string, String string2, String string3) {
        this._name = string;
        this._displayName = string2;
        this._description = string3;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public String getDisplayName() {
        if (this._displayName == null) {
            return this.getName();
        }
        return this._displayName;
    }

    public void setDisplayName(String string) {
        this._displayName = string;
    }

    public String getDescription() {
        if (this._description == null) {
            return this.getDisplayName();
        }
        return this._description;
    }

    public void setDescription(String string) {
        this._description = string;
    }

    public boolean equals(Object object) {
        if (object instanceof Group) {
            return this.equals((Group)object);
        }
        return false;
    }

    public boolean equals(Group group) {
        if (group == null) {
            return false;
        }
        return group.getName().equals(this._name);
    }

    public int hashCode() {
        int n = 7;
        n = 37 * n + (this._name != null ? this._name.hashCode() : 0);
        return n;
    }

    public String toString() {
        return this.getDisplayName();
    }
}

