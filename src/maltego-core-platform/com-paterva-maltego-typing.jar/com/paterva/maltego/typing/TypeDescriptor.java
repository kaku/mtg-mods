/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.DefaultStringConverter;
import com.paterva.maltego.typing.FormatAdapter;
import com.paterva.maltego.typing.StringConverter;
import java.text.Format;

public class TypeDescriptor {
    private String _typeName;
    private Class _class;
    private StringConverter _converter;
    private Object _defaultValue;
    private FormatAdapter _formatAdapter;

    public TypeDescriptor(String string, Class class_) {
        this(string, class_, null);
    }

    public TypeDescriptor(String string, Class class_, Object object) {
        this(string, class_, null, object);
    }

    public TypeDescriptor(String string, Class class_, StringConverter stringConverter) {
        this(string, class_, stringConverter, null);
    }

    public TypeDescriptor(String string, Class class_, StringConverter stringConverter, Object object) {
        this._typeName = string;
        this._class = class_;
        this._converter = stringConverter != null ? stringConverter : DefaultStringConverter.instance();
        this._defaultValue = object;
    }

    public String getTypeName() {
        return this._typeName;
    }

    public Class getType() {
        return this._class;
    }

    public StringConverter getConverter() {
        return this._converter;
    }

    public Object getDefaultValue() {
        return this._defaultValue;
    }

    public boolean isArrayType() {
        return this._class.isArray();
    }

    public Class getComponentType() {
        if (this.isArrayType()) {
            return this._class.getComponentType();
        }
        return this.getType();
    }

    public Object convert(String string) throws IllegalArgumentException {
        return this.getConverter().convertFrom(string, this.getType());
    }

    public String convert(Object object) {
        if (object == null) {
            object = this.getDefaultValue();
        }
        return this.getConverter().convertTo(object, this.getType());
    }

    public Format getFormat(String string) {
        if (this._formatAdapter != null) {
            return this.getFormatAdapter().getFormat(string);
        }
        return null;
    }

    public String getFormatString(Format format) {
        if (this._formatAdapter != null) {
            return this.getFormatAdapter().getFormatString(format);
        }
        return null;
    }

    public FormatAdapter getFormatAdapter() {
        return this._formatAdapter;
    }

    public void setFormatAdapter(FormatAdapter formatAdapter) {
        this._formatAdapter = formatAdapter;
    }

    public String toString() {
        return this.getTypeName();
    }
}

