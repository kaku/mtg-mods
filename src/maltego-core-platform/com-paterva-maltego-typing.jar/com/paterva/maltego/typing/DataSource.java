/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.PropertyDescriptor;

public interface DataSource {
    public Object getValue(PropertyDescriptor var1);

    public void setValue(PropertyDescriptor var1, Object var2);
}

