/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import java.util.LinkedList;

public class DisplayDescriptorList
extends LinkedList<DisplayDescriptor>
implements DisplayDescriptorCollection {
    public DisplayDescriptorList() {
    }

    public DisplayDescriptorList(Iterable<DisplayDescriptor> iterable) {
        for (DisplayDescriptor displayDescriptor : iterable) {
            this.add(displayDescriptor);
        }
    }

    @Override
    public boolean add(DisplayDescriptor displayDescriptor) {
        if (displayDescriptor == null) {
            throw new IllegalArgumentException("DisplayDescriptor may not be null.");
        }
        return LinkedList.super.add(displayDescriptor);
    }

    @Override
    public boolean contains(DisplayDescriptor displayDescriptor) {
        return LinkedList.super.contains(displayDescriptor);
    }

    @Override
    public DisplayDescriptor get(String string) {
        for (DisplayDescriptor displayDescriptor : this) {
            if (!displayDescriptor.getName().equals(string)) continue;
            return displayDescriptor;
        }
        return null;
    }

    @Override
    public boolean contains(String string) {
        for (DisplayDescriptor displayDescriptor : this) {
            if (!displayDescriptor.getName().equals(string)) continue;
            return true;
        }
        return false;
    }
}

