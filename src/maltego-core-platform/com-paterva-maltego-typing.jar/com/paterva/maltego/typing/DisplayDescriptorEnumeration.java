/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.DisplayDescriptor;

public interface DisplayDescriptorEnumeration
extends Iterable<DisplayDescriptor> {
    public boolean contains(DisplayDescriptor var1);

    public DisplayDescriptor get(String var1);

    public boolean contains(String var1);
}

