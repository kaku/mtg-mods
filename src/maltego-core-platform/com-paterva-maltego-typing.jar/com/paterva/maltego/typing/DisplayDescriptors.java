/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.DisplayDescriptorList;

public class DisplayDescriptors {
    private DisplayDescriptors() {
    }

    public static DisplayDescriptorEnumeration singleton(DisplayDescriptor displayDescriptor) {
        DisplayDescriptorList displayDescriptorList = new DisplayDescriptorList();
        displayDescriptorList.add(displayDescriptor);
        return displayDescriptorList;
    }
}

