/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FastURL
 *  org.simpleframework.xml.transform.Transform
 */
package com.paterva.maltego.typing.transforms;

import com.paterva.maltego.util.FastURL;
import org.simpleframework.xml.transform.Transform;

class FastURLTransform
implements Transform {
    FastURLTransform() {
    }

    public Object read(String string) throws Exception {
        return new FastURL(string);
    }

    public String write(Object object) throws Exception {
        if (object == null) {
            return null;
        }
        return ((FastURL)object).toString();
    }
}

