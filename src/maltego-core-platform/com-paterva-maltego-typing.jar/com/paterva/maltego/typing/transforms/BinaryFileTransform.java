/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.Base64
 *  com.paterva.maltego.util.StringUtilities
 *  org.simpleframework.xml.transform.Transform
 */
package com.paterva.maltego.typing.transforms;

import com.paterva.maltego.typing.types.BinaryFile;
import com.paterva.maltego.util.Base64;
import com.paterva.maltego.util.StringUtilities;
import org.simpleframework.xml.transform.Transform;

class BinaryFileTransform
implements Transform {
    BinaryFileTransform() {
    }

    public Object read(String string) throws Exception {
        if (StringUtilities.isNullOrEmpty((String)string)) {
            return null;
        }
        int n = string.indexOf(124);
        String string2 = string.substring(0, n);
        byte[] arrby = Base64.decode((String)string.substring(n + 1));
        return new BinaryFile(string2, arrby);
    }

    public String write(Object object) throws Exception {
        if (object == null) {
            return null;
        }
        BinaryFile binaryFile = (BinaryFile)object;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(binaryFile.getFilename());
        stringBuffer.append('|');
        stringBuffer.append(Base64.encodeBytes((byte[])binaryFile.getData()));
        return stringBuffer.toString();
    }
}

