/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.transform.Transform
 */
package com.paterva.maltego.typing.transforms;

import com.paterva.maltego.typing.serializer.AttachmentsPathRegistry;
import com.paterva.maltego.typing.types.InternalFile;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.simpleframework.xml.transform.Transform;

public class InternalFileTransform
implements Transform<InternalFile> {
    private static final Logger LOG = Logger.getLogger(InternalFileTransform.class.getName());

    public InternalFile read(String string) throws Exception {
        LOG.log(Level.FINE, "Read attachments: {0}", string);
        Map<Integer, String> map = AttachmentsPathRegistry.getPaths();
        int n = -1;
        if (map != null) {
            for (Map.Entry<Integer, String> entry : map.entrySet()) {
                if (!string.equals(entry.getValue())) continue;
                n = entry.getKey();
                break;
            }
        }
        return n < 0 ? null : new InternalFile(n);
    }

    public String write(InternalFile internalFile) throws Exception {
        String string = AttachmentsPathRegistry.getPaths().get(internalFile.fileStoreIndex);
        LOG.log(Level.FINE, "Write attachments: {0}", string);
        return string;
    }
}

