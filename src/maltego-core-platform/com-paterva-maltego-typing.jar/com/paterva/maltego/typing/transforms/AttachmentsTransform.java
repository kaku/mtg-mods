/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.StringUtilities
 *  org.simpleframework.xml.transform.Transform
 */
package com.paterva.maltego.typing.transforms;

import com.paterva.maltego.typing.serializer.AttachmentsPathRegistry;
import com.paterva.maltego.typing.types.Attachment;
import com.paterva.maltego.typing.types.Attachments;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.StringUtilities;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.simpleframework.xml.transform.Transform;

public class AttachmentsTransform
implements Transform<Attachments> {
    private static final Logger LOG = Logger.getLogger(AttachmentsTransform.class.getName());

    public Attachments read(String string) throws Exception {
        Map<Integer, String> map = AttachmentsPathRegistry.getPaths();
        if (!StringUtilities.isNullOrEmpty((String)string) && map != null) {
            Object object;
            LOG.log(Level.FINE, "Read attachments: {0}", string);
            String[] arrstring = string.split("\\|");
            int n = Integer.valueOf(arrstring[0]);
            int n2 = -1;
            int n3 = n + 1;
            if (arrstring.length > n3 && !StringUtilities.isNullOrEmpty((String)(object = arrstring[n3].trim()))) {
                n2 = Integer.valueOf((String)object);
            }
            object = new Attachments(n);
            for (int i = 1; i <= n; ++i) {
                String string2 = arrstring[i];
                int n4 = -1;
                for (Map.Entry<Integer, String> entry : map.entrySet()) {
                    if (!string2.equals(entry.getValue())) continue;
                    n4 = entry.getKey();
                    break;
                }
                if (n4 < 0) continue;
                Attachment attachment = new Attachment(n4, null);
                object.add(attachment);
                if (i != n2 + 1) continue;
                object.setPrimaryImage(attachment);
            }
            return object;
        }
        return null;
    }

    public String write(Attachments attachments) throws Exception {
        if (attachments instanceof Attachments) {
            StringBuilder stringBuilder = new StringBuilder();
            Attachments attachments2 = attachments;
            stringBuilder.append(attachments2.size());
            stringBuilder.append('|');
            Map<Integer, String> map = AttachmentsPathRegistry.getPaths();
            int n = 0;
            int n2 = -1;
            for (Attachment attachment : attachments2) {
                String string = map.get(attachment.getId());
                stringBuilder.append(string);
                stringBuilder.append('|');
                if (attachment.equals(attachments2.getPrimaryImage())) {
                    n2 = n;
                }
                ++n;
            }
            if (n2 != -1) {
                stringBuilder.append(n2);
            }
            String string = stringBuilder.toString();
            LOG.log(Level.FINE, "Write attachments: {0}", string);
            return string;
        }
        return null;
    }
}

