/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FastURL
 *  org.simpleframework.xml.transform.Matcher
 *  org.simpleframework.xml.transform.Transform
 */
package com.paterva.maltego.typing.transforms;

import com.paterva.maltego.typing.transforms.AttachmentsTransform;
import com.paterva.maltego.typing.transforms.Base64ImageTransform;
import com.paterva.maltego.typing.transforms.BinaryFileTransform;
import com.paterva.maltego.typing.transforms.FastURLTransform;
import com.paterva.maltego.typing.transforms.InternalFileTransform;
import com.paterva.maltego.typing.types.Attachments;
import com.paterva.maltego.typing.types.BinaryFile;
import com.paterva.maltego.typing.types.InternalFile;
import com.paterva.maltego.util.FastURL;
import java.awt.Image;
import org.simpleframework.xml.transform.Matcher;
import org.simpleframework.xml.transform.Transform;

public class TransformMatcher
implements Matcher {
    public Transform match(Class class_) throws Exception {
        if (Image.class.isAssignableFrom(class_)) {
            return new Base64ImageTransform();
        }
        if (BinaryFile.class.isAssignableFrom(class_)) {
            return new BinaryFileTransform();
        }
        if (Attachments.class.isAssignableFrom(class_)) {
            return new AttachmentsTransform();
        }
        if (InternalFile.class.isAssignableFrom(class_)) {
            return new InternalFileTransform();
        }
        if (FastURL.class.isAssignableFrom(class_)) {
            return new FastURLTransform();
        }
        return null;
    }
}

