/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ImageUtils
 *  com.paterva.maltego.util.StringUtilities
 *  org.simpleframework.xml.transform.Transform
 */
package com.paterva.maltego.typing.transforms;

import com.paterva.maltego.util.ImageUtils;
import com.paterva.maltego.util.StringUtilities;
import java.awt.Image;
import org.simpleframework.xml.transform.Transform;

class Base64ImageTransform
implements Transform {
    Base64ImageTransform() {
    }

    public Object read(String string) throws Exception {
        if (StringUtilities.isNullOrEmpty((String)string)) {
            return null;
        }
        return ImageUtils.base64Decode((String)string);
    }

    public String write(Object object) throws Exception {
        if (object == null) {
            return null;
        }
        return ImageUtils.base64Encode((Image)((Image)object), (String)"png");
    }
}

