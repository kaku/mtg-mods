/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.Group;

public interface GroupCollection
extends Iterable<Group> {
    public void add(Group var1);

    public void remove(Group var1);

    public Group get(String var1);

    public boolean contains(String var1);
}

