/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.DataSource;
import java.util.ArrayList;
import java.util.List;

public class PropertyDescriptor
implements Comparable {
    private Class _type;
    private String _name;
    private String _displayName;
    private boolean _nullable = true;
    private boolean _hidden = false;
    private boolean _readonly = false;
    private String _htmlDisplayName = null;
    private String _image = null;

    public PropertyDescriptor(Class class_, String string) {
        this(class_, string, string);
    }

    public PropertyDescriptor(Class class_, String string, String string2) {
        if (string == null) {
            throw new IllegalArgumentException("Type cannot be null");
        }
        if (class_ == null) {
            throw new IllegalArgumentException("Name cannot be null");
        }
        this._type = class_;
        this._name = string;
        this._displayName = string2;
    }

    public PropertyDescriptor(PropertyDescriptor propertyDescriptor) {
        this(propertyDescriptor.getType(), propertyDescriptor.getName(), propertyDescriptor.getDisplayName());
        this._nullable = propertyDescriptor.isNullable();
        this._hidden = propertyDescriptor.isHidden();
        this._readonly = propertyDescriptor.isReadonly();
    }

    public Class getType() {
        return this._type;
    }

    public String getName() {
        return this._name;
    }

    public String getDisplayName() {
        if (this._displayName == null) {
            return this._name;
        }
        return this._displayName;
    }

    public void setDisplayName(String string) {
        this._displayName = string;
    }

    public boolean isNullable() {
        return this._nullable;
    }

    public void setNullable(boolean bl) {
        this._nullable = bl;
    }

    public boolean isHidden() {
        return this._hidden;
    }

    public void setHidden(boolean bl) {
        this._hidden = bl;
    }

    public boolean isReadonly() {
        return this._readonly;
    }

    public void setReadonly(boolean bl) {
        this._readonly = bl;
    }

    public String getDescription() {
        return this._displayName;
    }

    public String toString() {
        return this.getDisplayName();
    }

    public boolean equals(PropertyDescriptor propertyDescriptor) {
        if (propertyDescriptor == null) {
            return false;
        }
        return this._name.equals(propertyDescriptor.getName());
    }

    public boolean equals(Object object) {
        if (object instanceof PropertyDescriptor) {
            return this.equals((PropertyDescriptor)object);
        }
        return false;
    }

    public int hashCode() {
        return this._name.hashCode();
    }

    public String getHtmlDisplayName() {
        return this._htmlDisplayName;
    }

    public String getImage() {
        return this._image;
    }

    public void setHtmlDisplayName(String string) {
        this._htmlDisplayName = string;
    }

    public void setImage(String string) {
        this._image = string;
    }

    public int compareTo(Object object) {
        if (object instanceof PropertyDescriptor) {
            return this.getName().compareTo(((PropertyDescriptor)object).getName());
        }
        return -1;
    }

    public void refreshValues(DataSource dataSource) {
        dataSource.setValue(this, this.evaluate(dataSource.getValue(this), dataSource));
    }

    public List<PropertyDescriptor> getLinkedProperties() {
        return new ArrayList<PropertyDescriptor>(0);
    }

    protected Object evaluate(Object object, DataSource dataSource) {
        return object;
    }
}

