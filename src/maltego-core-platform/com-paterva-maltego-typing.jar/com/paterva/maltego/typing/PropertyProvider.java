/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.PropertyDescriptorCollection;

public interface PropertyProvider
extends DataSource {
    public PropertyDescriptorCollection getProperties();
}

