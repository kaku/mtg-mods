/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformApiKeyProvider
 *  com.paterva.maltego.transform.descriptor.TransformApiKeyProvider$Mapped
 *  com.paterva.maltego.transform.descriptor.TransformApiKeyProvider$UrlWildcard
 */
package com.paterva.maltego.branding.activationapi;

import com.paterva.maltego.branding.activationapi.License;
import com.paterva.maltego.transform.descriptor.TransformApiKeyProvider;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class LicensedApiKeyProvider
extends TransformApiKeyProvider.Mapped {
    private static TransformApiKeyProvider.UrlWildcard PATERVA = new TransformApiKeyProvider.UrlWildcard("http://*.paterva.com:*/*");

    protected Map<String, String> getDefaultMap() {
        try {
            License license = License.read();
            return license.getApiKeys();
        }
        catch (License.ParsingException var1_2) {
        }
        catch (IOException var1_3) {
            // empty catch block
        }
        return Collections.emptyMap();
    }

    protected String getDefaultKey(String string) {
        try {
            Iterator iterator;
            if (PATERVA.matches(new URL(string)) && (iterator = this.getKeyMap().entrySet().iterator()).hasNext()) {
                Map.Entry entry = iterator.next();
                return (String)entry.getValue();
            }
        }
        catch (MalformedURLException var2_3) {
            // empty catch block
        }
        return null;
    }
}

