/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.branding.activationapi;

import com.paterva.maltego.branding.activationapi.License;
import com.paterva.maltego.branding.activationapi.ValidLicenseCallback;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Activation {
    public static boolean checkLicense(ValidLicenseCallback validLicenseCallback) {
        License license;
        try {
            license = License.read();
        }
        catch (License.ParsingException var2_2) {
            return false;
        }
        catch (IOException var2_3) {
            return false;
        }
        if (license.isValid()) {
            Activation.updateProperties(license);
            if (validLicenseCallback != null) {
                validLicenseCallback.valid(license);
            }
            return true;
        }
        return false;
    }

    public static void updateProperties(License license) {
        System.setProperty("maltego.registeredto.license-key", license.getLicenseKey());
        System.setProperty("maltego.registeredto.fullname", license.getFullName());
        System.setProperty("maltego.registeredto.email", license.getEmailAddress());
        System.setProperty("maltego.registeredto.firstname", license.getFirstName());
        System.setProperty("maltego.registeredto.lastname", license.getLastName());
        System.setProperty("maltego.registeredto.country", license.getCountry());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d MMM yyyy");
        System.setProperty("maltego.registeredto.validfrom", simpleDateFormat.format(license.getValidFrom()));
        System.setProperty("maltego.registeredto.validto", simpleDateFormat.format(license.getValidTo()));
    }
}

