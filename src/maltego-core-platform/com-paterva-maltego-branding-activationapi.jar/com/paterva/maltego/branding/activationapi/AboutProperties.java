/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.branding.activationapi;

public class AboutProperties {
    public static final String PROP_LICENSE_KEY = "maltego.registeredto.license-key";
    public static final String PROP_FULLNAME = "maltego.registeredto.fullname";
    public static final String PROP_EMAIL = "maltego.registeredto.email";
    public static final String PROP_FIRSTNAME = "maltego.registeredto.firstname";
    public static final String PROP_LASTNAME = "maltego.registeredto.lastname";
    public static final String PROP_COUNTRY = "maltego.registeredto.country";
    public static final String PROP_VALIDFROM = "maltego.registeredto.validfrom";
    public static final String PROP_VALIDTO = "maltego.registeredto.validto";
}

