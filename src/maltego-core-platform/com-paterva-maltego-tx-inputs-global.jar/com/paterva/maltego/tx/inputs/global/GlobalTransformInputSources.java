/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.tx.inputs.global;

import com.paterva.maltego.tx.inputs.global.GlobalTransformInputSource;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

public class GlobalTransformInputSources {
    private final Set<GlobalTransformInputSource> _sources;

    public GlobalTransformInputSources(Set<GlobalTransformInputSource> set) {
        this._sources = Collections.unmodifiableSet(set);
    }

    public Set<GlobalTransformInputSource> getSources() {
        return this._sources;
    }

    public int hashCode() {
        int n = 7;
        n = 17 * n + Objects.hashCode(this._sources);
        return n;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        GlobalTransformInputSources globalTransformInputSources = (GlobalTransformInputSources)object;
        return Objects.equals(this._sources, globalTransformInputSources._sources);
    }
}

