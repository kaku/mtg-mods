/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DisplayDescriptor
 */
package com.paterva.maltego.tx.inputs.global;

import com.paterva.maltego.typing.DisplayDescriptor;
import java.util.Objects;

public class GlobalTransformInputDescriptor {
    private final DisplayDescriptor _input;

    public GlobalTransformInputDescriptor(DisplayDescriptor displayDescriptor) {
        this._input = displayDescriptor;
    }

    public DisplayDescriptor getInput() {
        return this._input;
    }

    public int hashCode() {
        int n = 7;
        n = 79 * n + Objects.hashCode((Object)this._input);
        return n;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        GlobalTransformInputDescriptor globalTransformInputDescriptor = (GlobalTransformInputDescriptor)object;
        return Objects.equals((Object)this._input, (Object)globalTransformInputDescriptor._input);
    }
}

