/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.repository.serializer.TransformPropertyStub
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.tx.inputs.global.serialize;

import com.paterva.maltego.transform.repository.serializer.TransformPropertyStub;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="GlobalInput", strict=0)
public class GlobalTransformInputStub {
    @Element(name="Property", required=1)
    private TransformPropertyStub _property;
    @Element(name="Value", required=0)
    private String _value;

    public TransformPropertyStub getProperty() {
        return this._property;
    }

    public void setProperty(TransformPropertyStub transformPropertyStub) {
        this._property = transformPropertyStub;
    }

    public String getValue() {
        return this._value;
    }

    public void setValue(String string) {
        this._value = string;
    }
}

