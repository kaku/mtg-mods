/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.TypeDescriptor
 */
package com.paterva.maltego.tx.inputs.global;

import com.paterva.maltego.tx.inputs.global.GlobalTransformInputDescriptor;
import com.paterva.maltego.tx.inputs.global.GlobalTransformInputGroup;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.TypeDescriptor;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class GlobalTransformInputs {
    private final List<GlobalTransformInputGroup> _groups;

    public GlobalTransformInputs(List<GlobalTransformInputGroup> list) {
        this._groups = Collections.unmodifiableList(list);
    }

    public List<GlobalTransformInputGroup> getGroups() {
        return this._groups;
    }

    public int hashCode() {
        int n = 7;
        n = 67 * n + Objects.hashCode(this._groups);
        return n;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        GlobalTransformInputs globalTransformInputs = (GlobalTransformInputs)object;
        return Objects.equals(this._groups, globalTransformInputs._groups);
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\n");
        for (GlobalTransformInputGroup globalTransformInputGroup : this._groups) {
            stringBuilder.append("=====\nName: ");
            stringBuilder.append(globalTransformInputGroup.getDisplayName());
            stringBuilder.append("\nInputs: ");
            for (GlobalTransformInputDescriptor globalTransformInputDescriptor : globalTransformInputGroup.getInputs()) {
                DisplayDescriptor displayDescriptor = globalTransformInputDescriptor.getInput();
                stringBuilder.append(displayDescriptor.getDisplayName());
                stringBuilder.append(" (").append(displayDescriptor.getTypeDescriptor().getTypeName()).append(")").append(", ");
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString().replace(", \n", "\n");
    }
}

