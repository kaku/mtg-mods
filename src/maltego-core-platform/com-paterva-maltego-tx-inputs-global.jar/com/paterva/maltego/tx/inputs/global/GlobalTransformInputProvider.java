/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.tx.inputs.global;

import com.paterva.maltego.tx.inputs.global.GlobalTransformInputs;
import javax.swing.event.ChangeListener;
import org.openide.util.Lookup;

public abstract class GlobalTransformInputProvider {
    private static GlobalTransformInputProvider _default;

    public static synchronized GlobalTransformInputProvider getDefault() {
        if (_default == null && (GlobalTransformInputProvider._default = (GlobalTransformInputProvider)Lookup.getDefault().lookup(GlobalTransformInputProvider.class)) == null) {
            throw new IllegalStateException("Global transform input provider not found.");
        }
        return _default;
    }

    public abstract GlobalTransformInputs getGlobalInputs();

    public abstract void addChangeListener(ChangeListener var1);

    public abstract void removeChangeListener(ChangeListener var1);
}

