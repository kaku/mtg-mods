/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.tx.inputs.global.registry;

import com.paterva.maltego.tx.inputs.global.registry.DefaultGlobalTransformInputsRepository;
import com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput;
import com.paterva.maltego.typing.DataSource;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import org.openide.util.Lookup;

public abstract class GlobalTransformInputsRepository {
    private static GlobalTransformInputsRepository _default;

    public static synchronized GlobalTransformInputsRepository getDefault() {
        if (_default == null && (GlobalTransformInputsRepository._default = (GlobalTransformInputsRepository)Lookup.getDefault().lookup(GlobalTransformInputsRepository.class)) == null) {
            _default = new DefaultGlobalTransformInputsRepository();
        }
        return _default;
    }

    public abstract GlobalTransformInput get(String var1);

    public abstract void put(GlobalTransformInput var1);

    public abstract void remove(GlobalTransformInput var1);

    public abstract Collection<GlobalTransformInput> getAll();

    public abstract DataSource getData();

    public abstract void addPropertyChangeListener(PropertyChangeListener var1);

    public abstract void removePropertyChangeListener(PropertyChangeListener var1);
}

