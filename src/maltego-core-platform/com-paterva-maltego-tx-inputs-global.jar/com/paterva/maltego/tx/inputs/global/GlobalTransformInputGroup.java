/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.tx.inputs.global;

import com.paterva.maltego.tx.inputs.global.GlobalTransformInputDescriptor;
import com.paterva.maltego.tx.inputs.global.GlobalTransformInputSource;
import com.paterva.maltego.tx.inputs.global.GlobalTransformInputSources;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

public class GlobalTransformInputGroup {
    private final GlobalTransformInputSources _sources;
    private final Set<GlobalTransformInputDescriptor> _inputs;

    public GlobalTransformInputGroup(GlobalTransformInputSources globalTransformInputSources, Set<GlobalTransformInputDescriptor> set) {
        this._sources = globalTransformInputSources;
        this._inputs = Collections.unmodifiableSet(set);
    }

    public GlobalTransformInputSources getSources() {
        return this._sources;
    }

    public Set<GlobalTransformInputDescriptor> getInputs() {
        return this._inputs;
    }

    public String getDisplayName() {
        StringBuilder stringBuilder = new StringBuilder();
        GlobalTransformInputSources globalTransformInputSources = this.getSources();
        boolean bl = true;
        for (GlobalTransformInputSource globalTransformInputSource : globalTransformInputSources.getSources()) {
            if (!bl) {
                stringBuilder.append(", ");
            }
            stringBuilder.append(globalTransformInputSource.getDisplayName());
            bl = false;
        }
        return stringBuilder.toString();
    }

    public int hashCode() {
        int n = 7;
        n = 79 * n + Objects.hashCode(this._sources);
        n = 79 * n + Objects.hashCode(this._inputs);
        return n;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        GlobalTransformInputGroup globalTransformInputGroup = (GlobalTransformInputGroup)object;
        if (!Objects.equals(this._sources, globalTransformInputGroup._sources)) {
            return false;
        }
        return Objects.equals(this._inputs, globalTransformInputGroup._inputs);
    }
}

