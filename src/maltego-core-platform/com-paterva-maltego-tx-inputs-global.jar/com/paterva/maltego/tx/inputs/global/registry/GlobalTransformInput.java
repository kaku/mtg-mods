/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor
 */
package com.paterva.maltego.tx.inputs.global.registry;

import com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor;
import java.util.Objects;

public class GlobalTransformInput {
    private final TransformPropertyDescriptor _descriptor;
    private Object _value;

    public GlobalTransformInput(TransformPropertyDescriptor transformPropertyDescriptor, Object object) {
        this._descriptor = transformPropertyDescriptor;
        this._value = object;
    }

    public TransformPropertyDescriptor getDescriptor() {
        return this._descriptor;
    }

    public Object getValue() {
        return this._value;
    }

    public void setValue(Object object) {
        this._value = object;
    }

    public int hashCode() {
        int n = 5;
        n = 67 * n + Objects.hashCode((Object)this._descriptor);
        return n;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        GlobalTransformInput globalTransformInput = (GlobalTransformInput)object;
        return Objects.equals((Object)this._descriptor, (Object)globalTransformInput._descriptor);
    }

    public String toString() {
        return "GlobalTransformInput{descriptor=" + (Object)this._descriptor + ", value=" + this._value + '}';
    }
}

