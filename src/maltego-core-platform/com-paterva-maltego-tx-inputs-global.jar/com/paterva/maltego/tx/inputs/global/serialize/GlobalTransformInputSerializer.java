/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor
 *  com.paterva.maltego.transform.repository.serializer.TransformDescriptorSerializer
 *  com.paterva.maltego.transform.repository.serializer.TransformPropertyStub
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.util.XmlSerializer
 */
package com.paterva.maltego.tx.inputs.global.serialize;

import com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor;
import com.paterva.maltego.transform.repository.serializer.TransformDescriptorSerializer;
import com.paterva.maltego.transform.repository.serializer.TransformPropertyStub;
import com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput;
import com.paterva.maltego.tx.inputs.global.serialize.GlobalTransformInputStub;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.util.XmlSerializer;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class GlobalTransformInputSerializer {
    public GlobalTransformInput read(InputStream inputStream) throws IOException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        GlobalTransformInputStub globalTransformInputStub = (GlobalTransformInputStub)xmlSerializer.read(GlobalTransformInputStub.class, inputStream);
        return this.translate(globalTransformInputStub);
    }

    public void write(GlobalTransformInput globalTransformInput, OutputStream outputStream) throws IOException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        GlobalTransformInputStub globalTransformInputStub = this.translate(globalTransformInput);
        xmlSerializer.write((Object)globalTransformInputStub, outputStream);
    }

    private GlobalTransformInput translate(GlobalTransformInputStub globalTransformInputStub) throws IOException {
        TransformPropertyStub transformPropertyStub = globalTransformInputStub.getProperty();
        TransformPropertyDescriptor transformPropertyDescriptor = TransformDescriptorSerializer.translate((TransformPropertyStub)transformPropertyStub);
        String string = globalTransformInputStub.getValue();
        Object object = transformPropertyDescriptor.getTypeDescriptor().convert(string);
        return new GlobalTransformInput(transformPropertyDescriptor, object);
    }

    private GlobalTransformInputStub translate(GlobalTransformInput globalTransformInput) throws IOException {
        TransformPropertyDescriptor transformPropertyDescriptor = globalTransformInput.getDescriptor();
        TransformPropertyStub transformPropertyStub = TransformDescriptorSerializer.translate((TransformPropertyDescriptor)transformPropertyDescriptor);
        Object object = globalTransformInput.getValue();
        String string = transformPropertyDescriptor.getTypeDescriptor().convert(object);
        GlobalTransformInputStub globalTransformInputStub = new GlobalTransformInputStub();
        globalTransformInputStub.setProperty(transformPropertyStub);
        globalTransformInputStub.setValue(string);
        return globalTransformInputStub;
    }
}

