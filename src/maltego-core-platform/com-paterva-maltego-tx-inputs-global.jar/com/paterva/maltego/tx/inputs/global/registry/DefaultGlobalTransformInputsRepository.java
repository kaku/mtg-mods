/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.util.FileUtilities
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.tx.inputs.global.registry;

import com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor;
import com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput;
import com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInputsRepository;
import com.paterva.maltego.tx.inputs.global.serialize.GlobalTransformInputSerializer;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.util.FileUtilities;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;

public class DefaultGlobalTransformInputsRepository
extends GlobalTransformInputsRepository {
    private static final Logger LOG = Logger.getLogger(DefaultGlobalTransformInputsRepository.class.getName());
    private static final String FOLDER = "Maltego/Inputs/";
    private static final String INPUT_EXT = "input";
    private final InputsDataSource _dataSource;
    private final PropertyChangeSupport _changeSupport;
    private final FileObject _configRoot;
    private Map<GlobalTransformInput, FileObject> _inputs;

    public DefaultGlobalTransformInputsRepository() {
        this(FileUtil.getConfigRoot());
    }

    public DefaultGlobalTransformInputsRepository(FileObject fileObject) {
        this._dataSource = new InputsDataSource();
        this._changeSupport = new PropertyChangeSupport(this);
        this._configRoot = fileObject;
    }

    @Override
    public GlobalTransformInput get(String string) {
        this.loadInputs();
        for (Map.Entry<GlobalTransformInput, FileObject> entry : this._inputs.entrySet()) {
            GlobalTransformInput globalTransformInput = entry.getKey();
            if (!globalTransformInput.getDescriptor().getName().equals(string)) continue;
            return globalTransformInput;
        }
        return null;
    }

    @Override
    public void put(GlobalTransformInput globalTransformInput) {
        this.getData().setValue((PropertyDescriptor)globalTransformInput.getDescriptor(), globalTransformInput.getValue());
    }

    @Override
    public void remove(GlobalTransformInput globalTransformInput) {
        this.getData().setValue((PropertyDescriptor)globalTransformInput.getDescriptor(), (Object)null);
    }

    @Override
    public Collection<GlobalTransformInput> getAll() {
        this.loadInputs();
        return this._inputs.keySet();
    }

    @Override
    public synchronized DataSource getData() {
        this.loadInputs();
        return this._dataSource;
    }

    private synchronized void loadInputs() {
        if (this._inputs == null) {
            try {
                this._inputs = this.load();
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
                this._inputs = new HashMap<GlobalTransformInput, FileObject>();
            }
        }
    }

    private FileObject getFolder() throws IOException {
        return FileUtilities.getOrCreate((FileObject)this._configRoot, (String)"Maltego/Inputs/");
    }

    private synchronized Map<GlobalTransformInput, FileObject> load() throws IOException {
        HashMap<GlobalTransformInput, FileObject> hashMap = new HashMap<GlobalTransformInput, FileObject>();
        FileObject fileObject = this.getFolder();
        if (fileObject != null) {
            for (FileObject fileObject2 : fileObject.getChildren()) {
                GlobalTransformInput globalTransformInput;
                if (!fileObject2.isData() || (globalTransformInput = this.load(fileObject2)) == null) continue;
                hashMap.put(globalTransformInput, fileObject2);
            }
        }
        return hashMap;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private GlobalTransformInput load(FileObject fileObject) throws IOException {
        InputStream inputStream = null;
        GlobalTransformInput globalTransformInput = null;
        try {
            inputStream = fileObject.getInputStream();
            GlobalTransformInputSerializer globalTransformInputSerializer = new GlobalTransformInputSerializer();
            globalTransformInput = globalTransformInputSerializer.read(inputStream);
            LOG.log(Level.FINE, "Global input loaded: {0}", globalTransformInput);
        }
        catch (Exception var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return globalTransformInput;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private synchronized void save(GlobalTransformInput globalTransformInput) throws IOException {
        TransformPropertyDescriptor transformPropertyDescriptor = globalTransformInput.getDescriptor();
        FileObject fileObject = this.getFolder();
        String string = transformPropertyDescriptor.getName();
        FileObject fileObject2 = this._inputs.get(globalTransformInput);
        if (fileObject2 == null) {
            fileObject2 = FileUtilities.createUniqueFile((FileObject)fileObject, (String)FileUtilities.replaceIllegalChars((String)string), (String)"input");
            this._inputs.put(globalTransformInput, fileObject2);
        }
        OutputStream outputStream = null;
        try {
            LOG.log(Level.FINE, "Saving global input: {0}", (Object)transformPropertyDescriptor);
            outputStream = new BufferedOutputStream(fileObject2.getOutputStream());
            GlobalTransformInputSerializer globalTransformInputSerializer = new GlobalTransformInputSerializer();
            globalTransformInputSerializer.write(globalTransformInput, outputStream);
        }
        finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private class InputsDataSource
    implements DataSource {
        private boolean _isUpdating;

        private InputsDataSource() {
            this._isUpdating = false;
        }

        public Object getValue(PropertyDescriptor propertyDescriptor) {
            GlobalTransformInput globalTransformInput = this.getInput(propertyDescriptor);
            return globalTransformInput == null ? null : globalTransformInput.getValue();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
            block11 : {
                if (!this._isUpdating) {
                    try {
                        this._isUpdating = true;
                        if (!(propertyDescriptor instanceof TransformPropertyDescriptor)) {
                            throw new IllegalArgumentException("Not a TransformPropertyDescriptor: " + (Object)propertyDescriptor);
                        }
                        TransformPropertyDescriptor transformPropertyDescriptor = (TransformPropertyDescriptor)propertyDescriptor;
                        GlobalTransformInput globalTransformInput = this.getInput((PropertyDescriptor)transformPropertyDescriptor);
                        Object object2 = null;
                        boolean bl = true;
                        if (globalTransformInput == null) {
                            globalTransformInput = new GlobalTransformInput(transformPropertyDescriptor, object);
                        } else if (!Objects.equals(globalTransformInput.getValue(), object)) {
                            object2 = globalTransformInput.getValue();
                            globalTransformInput.setValue(object);
                        } else {
                            bl = false;
                        }
                        if (!bl) break block11;
                        try {
                            DefaultGlobalTransformInputsRepository.this.save(globalTransformInput);
                        }
                        catch (IOException var7_7) {
                            Exceptions.printStackTrace((Throwable)var7_7);
                        }
                        DefaultGlobalTransformInputsRepository.this._changeSupport.firePropertyChange(globalTransformInput.getDescriptor().getName(), object2, object);
                    }
                    finally {
                        this._isUpdating = false;
                    }
                }
            }
        }

        private GlobalTransformInput getInput(PropertyDescriptor propertyDescriptor) {
            for (Map.Entry entry : DefaultGlobalTransformInputsRepository.this._inputs.entrySet()) {
                GlobalTransformInput globalTransformInput = (GlobalTransformInput)entry.getKey();
                if (!globalTransformInput.getDescriptor().equals(propertyDescriptor)) continue;
                return globalTransformInput;
            }
            return null;
        }
    }

}

