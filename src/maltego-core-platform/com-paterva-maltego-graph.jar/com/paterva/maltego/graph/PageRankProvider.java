/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  yguard.A.A.D
 *  yguard.A.A.Y
 */
package com.paterva.maltego.graph;

import javax.swing.event.ChangeListener;
import org.openide.util.Lookup;
import yguard.A.A.D;
import yguard.A.A.Y;

public abstract class PageRankProvider {
    private static PageRankProvider _default;

    public static synchronized PageRankProvider getDefault() {
        if (_default == null && (PageRankProvider._default = (PageRankProvider)Lookup.getDefault().lookup(PageRankProvider.class)) == null) {
            _default = new TrivialPageRankProvider();
        }
        return _default;
    }

    public abstract double get(Y var1);

    public abstract void addChangeListener(D var1, ChangeListener var2);

    public abstract void removeChangeListener(D var1, ChangeListener var2);

    private static class TrivialPageRankProvider
    extends PageRankProvider {
        private TrivialPageRankProvider() {
        }

        @Override
        public double get(Y y) {
            return 1.0;
        }

        @Override
        public void addChangeListener(D d, ChangeListener changeListener) {
        }

        @Override
        public void removeChangeListener(D d, ChangeListener changeListener) {
        }
    }

}

