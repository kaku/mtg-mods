/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.Guid
 *  yguard.A.A.F
 *  yguard.A.A.K
 */
package com.paterva.maltego.graph;

import com.paterva.maltego.core.Guid;
import java.util.HashMap;
import java.util.Set;
import yguard.A.A.F;
import yguard.A.A.K;

public class HashMapGuidDataAdapter<ID extends Guid, T>
implements K,
F {
    private final HashMap<ID, T> _map = new HashMap();

    public int size() {
        return this._map.size();
    }

    public Iterable<ID> keys() {
        return this._map.keySet();
    }

    public Object get(Object object) {
        return this.get((ID)((Guid)object));
    }

    public T get(ID ID) {
        return this._map.get(ID);
    }

    public void set(Object object, Object object2) {
        this.put((ID)((Guid)object), (T)object2);
    }

    public void put(ID ID, T t) {
        assert (ID != null);
        assert (this._map != null);
        this._map.put(ID, t);
    }

    public void remove(ID ID) {
        if (ID != null) {
            this._map.remove(ID);
        }
    }

    public int getInt(Object object) {
        throw new UnsupportedOperationException("Not supported");
    }

    public double getDouble(Object object) {
        throw new UnsupportedOperationException("Not supported");
    }

    public boolean getBool(Object object) {
        throw new UnsupportedOperationException("Not supported.");
    }

    public void setInt(Object object, int n) {
        throw new UnsupportedOperationException("Not supported");
    }

    public void setDouble(Object object, double d) {
        throw new UnsupportedOperationException("Not supported");
    }

    public void setBool(Object object, boolean bl) {
        throw new UnsupportedOperationException("Not supported");
    }
}

