/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 */
package com.paterva.maltego.graph;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphLogListener;
import com.paterva.maltego.graph.GraphLogger;
import java.util.ArrayList;
import java.util.List;

public class DefaultGraphLogger
extends GraphLogger {
    private List<GraphLogListener> _listeners = new ArrayList<GraphLogListener>();

    @Override
    public synchronized void log(GraphID graphID, String string) {
        this.fireLogMessage(graphID, string);
    }

    @Override
    public synchronized void addGraphLogListener(GraphLogListener graphLogListener) {
        this._listeners.add(graphLogListener);
    }

    @Override
    public synchronized void removeGraphLogListener(GraphLogListener graphLogListener) {
        this._listeners.remove(graphLogListener);
    }

    private void fireLogMessage(GraphID graphID, String string) {
        for (GraphLogListener graphLogListener : this._listeners) {
            graphLogListener.logMessage(graphID, string);
        }
    }
}

