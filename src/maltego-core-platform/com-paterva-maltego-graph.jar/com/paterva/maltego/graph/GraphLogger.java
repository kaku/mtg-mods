/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.graph;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.DefaultGraphLogger;
import com.paterva.maltego.graph.GraphLogListener;
import org.openide.util.Lookup;

public abstract class GraphLogger {
    private static GraphLogger _default;

    public static synchronized GraphLogger getDefault() {
        if (_default == null && (GraphLogger._default = (GraphLogger)Lookup.getDefault().lookup(GraphLogger.class)) == null) {
            _default = new DefaultGraphLogger();
        }
        return _default;
    }

    public abstract void log(GraphID var1, String var2);

    public abstract void addGraphLogListener(GraphLogListener var1);

    public abstract void removeGraphLogListener(GraphLogListener var1);
}

