/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 */
package com.paterva.maltego.graph.undo;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.undo.Command;

public abstract class GraphCommand
extends Command {
    private final GraphID _graphID;

    public GraphCommand(GraphID graphID) {
        this._graphID = graphID;
    }

    public GraphID getGraphID() {
        return this._graphID;
    }
}

