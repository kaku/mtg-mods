/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.undo;

import com.paterva.maltego.graph.undo.Command;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BatchCommand
extends Command {
    private List<Command> _commands = new ArrayList<Command>();
    private String _description;

    public BatchCommand(String string) {
        this._description = string;
    }

    public void add(Command command) {
        this._commands.add(command);
    }

    public List<Command> getCommands() {
        return Collections.unmodifiableList(this._commands);
    }

    @Override
    public void execute() {
        for (Command command : this._commands) {
            command.execute();
        }
    }

    @Override
    public void undo() {
        for (int i = this._commands.size() - 1; i >= 0; --i) {
            this._commands.get(i).undo();
        }
    }

    @Override
    public String getDescription() {
        return this._description;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder(super.toString());
        stringBuilder.append(" [").append(this._commands.size()).append("]{");
        int n = 0;
        for (Command command : this._commands) {
            stringBuilder.append(command.getDescription());
            if (++n == this._commands.size()) continue;
            stringBuilder.append(",");
        }
        return stringBuilder.append("}").toString();
    }
}

