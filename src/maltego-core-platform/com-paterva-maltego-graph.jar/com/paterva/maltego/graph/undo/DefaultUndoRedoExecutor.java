/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.undo;

import com.paterva.maltego.graph.undo.Command;
import com.paterva.maltego.graph.undo.UndoRedoExecutor;

public class DefaultUndoRedoExecutor
extends UndoRedoExecutor {
    @Override
    public void undo(Command command) {
        command.undo();
    }

    @Override
    public void redo(Command command) {
        command.execute();
    }
}

