/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.undo;

import com.paterva.maltego.graph.undo.Command;

public class IndexedCommand {
    private Integer _index;
    private Command _command;

    public IndexedCommand(Command command, Integer n) {
        this._command = command;
        this._index = n;
    }

    public IndexedCommand(Command command) {
        this(command, null);
    }

    public Integer getIndex() {
        return this._index;
    }

    public Command getCommand() {
        return this._command;
    }
}

