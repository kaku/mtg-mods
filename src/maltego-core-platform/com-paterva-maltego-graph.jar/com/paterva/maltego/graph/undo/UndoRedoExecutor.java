/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.graph.undo;

import com.paterva.maltego.graph.undo.Command;
import com.paterva.maltego.graph.undo.DefaultUndoRedoExecutor;
import org.openide.util.Lookup;

public abstract class UndoRedoExecutor {
    private static UndoRedoExecutor _default;

    public static UndoRedoExecutor getDefault() {
        if (_default == null && (UndoRedoExecutor._default = (UndoRedoExecutor)Lookup.getDefault().lookup(UndoRedoExecutor.class)) == null) {
            _default = new DefaultUndoRedoExecutor();
        }
        return _default;
    }

    public abstract void undo(Command var1);

    public abstract void redo(Command var1);
}

