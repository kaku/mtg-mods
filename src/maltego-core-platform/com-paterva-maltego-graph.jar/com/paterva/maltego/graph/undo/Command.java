/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.undo;

public abstract class Command {
    public abstract void execute();

    public abstract void undo();

    public abstract String getDescription();

    public boolean isSignificant() {
        return true;
    }

    public String toString() {
        return this.getDescription();
    }
}

