/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.graph.undo;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.undo.UndoRedoModel;
import org.openide.util.Lookup;

public abstract class UndoRedoManager {
    private static UndoRedoManager _default;

    public static UndoRedoManager getDefault() {
        if (_default == null && (UndoRedoManager._default = (UndoRedoManager)Lookup.getDefault().lookup(UndoRedoManager.class)) == null) {
            throw new IllegalStateException("No undo redo manager found.");
        }
        return _default;
    }

    public abstract UndoRedoModel create(GraphID var1);

    public abstract UndoRedoModel get(GraphID var1);
}

